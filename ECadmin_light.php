<?php
//date_default_timezone_set('America/Kentucky/Louisville');

$varSwitchFile='/home/ah/allhorse/public_html/control-switch/index.php';
if (file_exists($varSwitchFile)) {
		include_once $varSwitchFile;
	}
	
   function is_between_times_switch( $start = null, $end = null ) {
		$currentTime=strtotime(date("Y-m-d H:i"));
		return ($start <= $currentTime && $currentTime <= $end);
		
    }
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin - USR</title>

</head>

<body>
<div class="container">
	<br />tutu
	<?php
	//echo '<center><h1>Server TimeZone:'.date_default_timezone_get().', <br /> ';
	echo '<center><h1>Server Time:'.date("F j, Y, g:i a").'</h1></center>'; 
	echo '<center><h4>Falcom Server. USRACINF.com </h4></center>';
	$xmlString = file_get_contents($FileXML);
	$xml = simplexml_load_string($xmlString);

	?>
	<br />

     <table class="data table table-condensed table-striped table-bordered ordenable" >
        <tbody>
        <tr>
        <th>Variable Name</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Status..</th>        
        </tr>
<?php

	$switchi=0;
	foreach($SwitchVariable['variableName'] as $variableName){
		$starttime=(string)$SwitchVariable['startTIME'][$switchi];
		$endtime=(string)$SwitchVariable['endTIME'][$switchi];
		$year=$SwitchVariable['Year'][$switchi];
		$start=strtotime($starttime);
		$end=strtotime($endtime);		
		?>
		<tr>
        <td><?php echo $variableName; ?></td>
        <td><?php echo $starttime; ?></td>
        <td><?php echo $endtime; ?></td> 
        <?php if (is_between_times_switch($start, $end))
		{ echo "<td style='background-color: green; color: white'>ON</td>";}
		else{ echo "<td style='background-color: red; color: white'>OFF</td>"; } ?>                        
        </tr>
        <?php
		 $switchi++;
	}


?>
        </tbody>
        </table>

</div> <!--End Container-->        
</body>
</html>