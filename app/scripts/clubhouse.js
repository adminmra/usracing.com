﻿$(function () {

    bindUpcomingRace();

});

var bindUpcomingRace = function () {
    $("#upcomingRace .item").click(function () {
        var mtp = parseInt($(this).find(".mtp_field").text());
        if (mtp == 0) {
            //return;
        }

        var track = $(this).attr("rel").split("_")[0];
        var race = $(this).attr("rel").split("_")[1];

        var url = "/bet?track=" + track + "&race=" + race;
        location.href = url;
    });
};

/* Nav */
$(function () {
    $("#menu li").removeClass("active");
    $("#clubhouseTab").addClass("active");
});

/* Tooltip */
$(function () {
    $.widget("ui.tooltip", $.ui.tooltip, {
        options: {
            content: function () {
                return $(this).prop('title');
            }
        }
    });
    $(document).tooltip({
       position: {
            my: "center bottom-20",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                .addClass("arrow")
                .addClass(feedback.vertical)
                .addClass(feedback.horizontal)
                .appendTo(this);
            }
        }
    });
});