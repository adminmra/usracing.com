  <footer>
        <div class="content-wrapper">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                 <td><ul>
 	                 <li class="th"><h3>My Account</h3></li>
                     <li><a href="/deposit" rel="nofollow">Deposit</a></li>
                     <li><a href="/withdraw" rel="nofollow">Withdraw</a></li>
                     <li><a href="/bet?tab=mybets" rel="nofollow">Betting History</a></li>
                     </ul>
                </td>
                 <td><ul>
 				                <li class="th"><h3>Betting Help</h3></li>
                     <li><a href="/faqs" rel="nofollow">FAQS</a></li>
                     <li><a href="/video-faq" rel="nofollow">Video FAQs</a></li>
                     <li><a href="/racingterms" rel="nofollow">Racing Terms</a></li>
                     <li><a href="/howtowager" rel="nofollow">How to Wager</a></li>
                     </ul>
                </td>
                 <td><ul>
 	                 <li class="th"><h3>US Racing</h3></li>
                     <li><a href="/about" rel="nofollow">About Us</a></li>
                     <li><a href="/terms" rel="nofollow">Terms & Conditions</a></li>     
                     <li><a href="/privacy" rel="nofollow">Privacy Policy</a></li>
                     <li><a href="/responsiblegaming" rel="nofollow">Responsible Gaming</a></li>
                     </ul></td>
                 <td width="35%"><ul>
 				      <li class="th"><h3>Support</h3></li>
                     
                     <li><strong>Toll Free:</strong> 1-844-US RACING (844-877-2246)</li>
                     <li><strong>Email:</strong> <a href="mailto:cs@usracing.com">cs@usracing.com</a></li>

                     </ul>

<ul class="social-icons">
          <li><a class="social_facebook" title="Like us on Facebook" data-original-title="Facebook" href="https://www.facebook.com/betusracing"></a></li>
          <li><a class="social_twitter" title="Follow us on Twitter" data-original-title="Twitter" href="https://twitter.com/betusracing"></a></li>
          <li><a class="social_google" title="Google+" data-original-title="Google+" href="https://www.google.com/+Usracingcom"></a></li>
          
        </ul>
                 </td>
                </tr>
            </table> 
            <div class="row friends">
            <div class="col-md-9">
            <a class="ntra"></a>
            <a class="bloodhorse"></a>        
            <a class="equibase"></a>
            <a class="amazon"></a>
            <a class="ga"></a>
            <a class="credit"></a>
            </div>
            <div class="col-md-3">
            <a class="us" href="/"></a>
            </div>
    </div>
        </div>
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <span class="brand">Copyright <?php echo date('Y'); ?>  <a href="/">US Racing</a>, All Rights Reserved</span>
          </div>
        </div><!--/row--> 
      </div><!--/container--> 
    </div>  
    </footer>

    

    </form>

    <script>


        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-742771-29', 'usracing.com');
        ga('send', 'pageview');

    </script>

</body>
</html>
