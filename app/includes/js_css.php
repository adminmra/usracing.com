<link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link href="/content/content.css" rel="stylesheet"/>
<link href="/content/content1.css" rel="stylesheet"/>


    <script type="text/javascript" src="//use.typekit.net/qaj1kms.js"></script>
    <script type="text/javascript">try { Typekit.load(); } catch (e) { }</script>
    <script type="text/javascript" src="/scripts/jquery.js"></script>
    <script type="text/javascript" src="/scripts/base.js"></script>
    <script type="text/javascript" src="/scripts/jquery-ui-1.10.4.custom.min.js"></script>
        
    <link type="text/css" rel="stylesheet" href="/content/royalslider.css" />
    <link type="text/css" rel="stylesheet" href="/content/clubhouse.css" />
    <script type="text/javascript" src="/scripts/jquery.royalslider.min.js"></script>
    <script type="text/javascript" src="/scripts/clubhouse.js"></script>    

<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="/Content/ie9.css" />
<![endif]-->
<!--[if lt IE 9]>
<script src="Scripts/html5shiv.js"></script>
<![endif]-->
<!--[if !IE 7]>
	<style type="text/css">
		#wrap {display:table;height:100%}
	</style>
<![endif]-->
<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="/Content/ie.css" />    
<![endif]-->
</head>	