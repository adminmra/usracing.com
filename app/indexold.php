<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta charset="utf-8"><meta name="viewport" content="width=1024"><meta name="robots" content="NOINDEX, NOFOLLOW"><title>
	US Racing | Online Horse Betting
</title><link href="https://secure.usracing.com/favicon.ico" rel="shortcut icon" type="image/x-icon"><link href="US%20Racing%20%7C%20Online%20Horse%20Betting_files/css.css" rel="stylesheet">
<link href="US%20Racing%20%7C%20Online%20Horse%20Betting_files/css_002.css" rel="stylesheet">


    <script src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/analytics.js" async=""></script><script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/qaj1kms.js"></script>
    <style type="text/css">.tk-proxima-nova-condensed{font-family:"proxima-nova-condensed",sans-serif;}.tk-adelle-sans{font-family:"adelle-sans",sans-serif;}</style><link href="US%20Racing%20%7C%20Online%20Horse%20Betting_files/d.css" rel="stylesheet"><script type="text/javascript">try { Typekit.load(); } catch (e) { }</script>
    <script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/jquery.js"></script>
    <script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/base.js"></script>
    <script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/jquery-ui-1.js"></script>
        
    <link type="text/css" rel="stylesheet" href="US%20Racing%20%7C%20Online%20Horse%20Betting_files/royalslider.css">
    <link type="text/css" rel="stylesheet" href="US%20Racing%20%7C%20Online%20Horse%20Betting_files/clubhouse.css">
    <script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/jquery_002.js"></script>
    <script type="text/javascript" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/clubhouse.js"></script>    

<!--[if gte IE 9]>
<link rel="stylesheet" type="text/css" href="/Content/ie9.css" />
<![endif]-->
<!--[if lt IE 9]>
<script src="Scripts/html5shiv.js"></script>
<![endif]-->
<!--[if !IE 7]>
	<style type="text/css">
		#wrap {display:table;height:100%}
	</style>
<![endif]-->
<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="/Content/ie.css" />    
<![endif]-->
</head>	
<body>
    <form method="post" action="clubhouse" id="Form1">
<div class="aspNetHidden">
<input name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE3NDQxMTMyNzJkGAMFEGN0bDAwJExvZ2luVmlldzIPD2QCAWQFCWN0bDAwJGx2MQ8PZAIBZAUQY3RsMDAkTG9naW5WaWV3MQ8PZAIBZIJQsqxJxfb4VegY1xhCawA6DwVJ" type="hidden">
</div>


    <div id="wrap">
        <header>
            <div class="content-wrapper">
              <div class="content">                  
                    
                            <img class="logo" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/usracing.png">
                                                          
                        
                   
                    <div id="top">
                        <section id="login">
                            
                                    <ul>
                                        <li><a href="https://secure.usracing.com/myaccount" id="lv1_myAccountLink" class="btn btn-sm btn-default">My Account</a></li>
                                        <li>
                                            <a class="btn btn-sm btn-primary" href="https://secure.usracing.com/logout">Log out</a>
                                        </li>
                                    </ul>
                                
                                
                        </section>
                        <div class="topR">
                            <div class="ph"><i class="glyphicon glyphicon-phone-alt"></i> <span class="phAll">1-844-US RACING <span class="dull">(844-877-2246)</span></span></div>
                        </div>
                    </div>
              </div>  
            </div>
            
                    <div class="nav-wrapper">
                        <nav>
                            <ul id="menu">
                    <li id="betTab"><a href="https://secure.usracing.com/bet">Bet Now</a></li>
                    <li id="todaysRaceTab"><a href="https://secure.usracing.com/todaysrace">Today's Races</a></li>
                    <li id="resultsTab"><a href="https://secure.usracing.com/result">Results</a></li>
                        
                        <li class="active" id="clubhouseTab"><a href="https://secure.usracing.com/clubhouse">Clubhouse</a></li>
                        <li id="depositTab"><a href="https://secure.usracing.com/deposit">Deposit</a></li>
                        </ul>
                    </nav>
                    </div>
                

        </header>
        <div id="body">
            

            <section class="content-wrapper main-content clear-fix">
                
    <div id="clubhouse_holder">
        <div class="float_left">
            

<div id="upcomingRace" class="block">
    <h3>Upcoming Races<span class="right"><a href="https://secure.usracing.com/todaysrace" class="btn btn-sm-x btn-default">View All</a></span></h3>
    <table>
        <tbody><tr class="header">
            <td>Track</td>
            <td class="race_field">Race</td>
            <td>|</td>
            <td class="mtp_field">MTP</td>
        </tr><tr class="item" rel="YON|Day_2"><td>Yonkers Raceway</td><td class="right"># 2 &nbsp;</td><td>|</td><td class="mtp_field">0</td></tr><tr class="item" rel="IR1|Day_7"><td>Ir1 Naas</td><td class="right"># 7 &nbsp;</td><td>|</td><td class="mtp_field">4</td></tr><tr class="item" rel="TP|Day_2"><td>Turfway Park</td><td class="right"># 2 &nbsp;</td><td>|</td><td class="mtp_field">14</td></tr><tr class="item" rel="YON|Day_3"><td>Yonkers Raceway</td><td class="right"># 3 &nbsp;</td><td>|</td><td class="mtp_field">19</td></tr><tr class="item" rel="AQU|Day_2"><td>Aqueduct</td><td class="right"># 2 &nbsp;</td><td>|</td><td class="mtp_field">23</td></tr><tr class="item" rel="LAD|Day_1"><td>Louisiana Downs</td><td class="right"># 1 &nbsp;</td><td>|</td><td class="mtp_field">34</td></tr><tr class="item" rel="YON|Day_4"><td>Yonkers Raceway</td><td class="right"># 4 &nbsp;</td><td>|</td><td class="mtp_field">39</td></tr><tr class="item" rel="TP|Day_3"><td>Turfway Park</td><td class="right"># 3 &nbsp;</td><td>|</td><td class="mtp_field">43</td></tr><tr class="item" rel="AQU|Day_3"><td>Aqueduct</td><td class="right"># 3 &nbsp;</td><td>|</td><td class="mtp_field">52</td></tr><tr class="item" rel="LAD|Day_2"><td>Louisiana Downs</td><td class="right"># 2 &nbsp;</td><td>|</td><td class="mtp_field">58</td></tr>
        
    </tbody></table>
</div>

<script type="text/javascript">
    $(function () {

        setInterval(function () {
            $(".mtp_field").each(function () {
                var n = parseInt($(this).text()) - 1;
                if (n >= 0) {
                    $(this).text(n);
                }
                if (n == 0) {
                    //$(this).parent().css({ "cursor": "default" }).find("td").css("color", "#ccc");
                }
            });

        }, 1000 * 60);

        $("#upcomingRace .view_all").click(function () {
            //location.reload();
        });

        setInterval(fetchUpcomingRace, 1000 * 60 * 2);
    });

    var fetchUpcomingRace = function () {
        $.ajaxSetup({ cache: false });
        $.post("ajaxservice.aspx?do=getUpcomingRace", null, function (r) {
            if (r.indexOf("item") > -1) {
                $("#upcomingRace tr[class=item]").remove();
                $("#upcomingRace tr[class=desc]").remove();
                $("#upcomingRace tr[class=header]").after(r);

                try {
                    bindUpcomingRace();
                }
                catch (e) { }
            }
            else {
                var desc = "<tr class=\"desc\"><td colspan=\"4\" style=\"font-weight:normal;\">Please select a track to place your wager</td></tr>";
                $("#upcomingRace tr[class=item]").remove();
                $("#upcomingRace tr[class=desc]").remove();
                $("#upcomingRace tr[class=header]").after(desc);
            }
            
        });
    };
</script>
            
<div id="bankInfo" class="block">
<h3>Account Management</h3>
    <a href="https://secure.usracing.com/deposit">Deposit Options</a>
    <a href="https://secure.usracing.com/withdraw">Withdraw Funds</a>
    <a href="https://secure.usracing.com/bet?tab=mybets">Betting History</a>
    <a id="am_profile" href="https://secure.usracing.com/myaccount">Update Email</a>
    <a href="https://secure.usracing.com/myaccount?p=pin">Update Pin</a>
    <a href="https://secure.usracing.com/cardonfile">Update Payment Info</a>
    <a href="https://secure.usracing.com/myaccount?p=report">Year End Reports</a>
</div>


        </div>
        <div class="float_right">
            <div id="summary" class="block">
                <h3><span class="left">Welcome Richard</span> <span class="right"><span>Balance:</span> <a href="https://secure.usracing.com/deposit">$0.00</a></span></h3>
            </div>
            <div id="outside">
                <div id="clubhouse" style="width:100%;">

<!-- ==== BUTTONS === -->
<div class="buttons">
	<span class="col-md-4"><a class="btn btn-lrg btn-primary" href="https://secure.usracing.com/bet"><i class="fa fa-ticket"></i> Bet Now</a></span>
	<span class="col-md-4"><a class="btn btn-lrg btn-green" href="https://secure.usracing.com/deposit"><i class="fa fa-credit-card"></i> Deposit</a></span>
	<span class="col-md-4"><a class="btn btn-lrg btn-default" href="https://secure.usracing.com/bet?tab=mybets"><i class="fa fa-clock-o"></i> Pending</a></span>
</div>


<!-- ==== SLIDER === -->
<div id="clubSlider" class="fullWidth clearfix">
	<div style="height: 256.311px;" class="royalSlider rsDefault rsHor rsFade rsWithBullets"><div style="width: 704px; height: 256px;" class="rsOverflow"><div class="rsContainer"><div style="z-index:0;" class="rsSlide "><div style="visibility: visible; opacity: 1; transition: opacity 400ms ease-in-out 0s;" class="rsContent">
        <img style="width: 704px; height: 257px; margin-left: 0px; margin-top: -1px;" class="rsImg rsMainSlideImage" src="US%20Racing%20%7C%20Online%20Horse%20Betting_files/promo-bg.jpg">
        	<div class="infoBlock">
 			<span style="display: block; transform: translate3d(0px, 0px, 0px); opacity: 1; transition-property: -moz-transform, opacity; transition-duration: 200ms; transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);" class="rsABlock headlineMed font45" data-move-effect="top" data-speed="200">Win Big from Carryover Pools!</span>
        	<span style="display: block; transform: translate3d(0px, 0px, 0px); opacity: 1; transition-property: -moz-transform, opacity; transition-duration: 200ms; transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);" class="rsABlock headlineSml" data-move-effect="top" data-speed="200">It aint over til it's over!</span>
        	<button style="display: block; transform: translate3d(0px, 0px, 0px); opacity: 1; transition-property: -moz-transform, opacity; transition-duration: 240ms; transition-timing-function: cubic-bezier(0.39, 0.575, 0.565, 1);" id="modalPop2" type="button" class="rsABlock btn btn-default" data-move-effect="bottom" data-speed="240"><i class="fa fa-hand-o-right"></i> Learn More</button>
           	</div>
   		</div></div></div></div><div class="rsNav rsBullets"><div class="rsNavItem rsBullet rsNavSelected"><span>1</span></div></div><div style="display: none;" class="rsArrow rsArrowLeft rsHidden"><div class="rsArrowIcn"></div></div><div style="display: none;" class="rsArrow rsArrowRight rsHidden"><div class="rsArrowIcn"></div></div><div class="rsNavLabel">Promos</div></div>    
</div>


<!-- ==== CARRY OVERS === -->
<script type="text/javascript">
        var carryover_url = "https://secure.usracing.com/ajaxservice?do=getCarryovers";
        //var carryover_msg = "There is no carryover at the moment.";

        $.get(carryover_url)
            .done(function (r) {
                var arr = $.parseJSON(r);
                renderCarryoverList(arr);
            })
            .fail(function () {
                //$("#carryoverList .loader").html(carryover_msg);
				$("#carryoverList").hide();
				$("#carryoverMsg").show();
            });

        var renderCarryoverList = function (arr) {
            if (arr.length > 0) {
				$("#carryoverMsg").hide();
                $("#carryoverList .loader").remove();				
            }
            else {
				$("#carryoverList .loader").remove();
                //$("#carryoverList .loader").html(carryover_msg);
				$("#carryoverList").hide();
				$("#carryoverMsg").show();
				
            }

            for (var i = 0; i < arr.length; ++i) {
                var obj = arr[i];
                var trackName = obj.trackName;
                var wagerType = obj.wagerType;
                var amount = obj.amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                var dom = $("<tr>")
                    .append("<td>" + trackName + "</td>")
                    .append("<td class=\"center\">" + wagerType + "</td>")
                    .append("<td class=\"right\"><small>$</small> " + amount + "</td>");

                $("#carryoverList").append(dom);
				dom.find('td:contains("Churchill Downs")').parent().remove();		
				dom.find('td:contains("Fair Grounds")').parent().remove();	
				dom.find('td:contains("Calder")').parent().remove();
				dom.find('td:contains("Hoosier")').parent().remove();	
				dom.find('td:contains("Indiana Downs")').parent().remove();	
				dom.find('td:contains("Miami Valley")').parent().remove();	
				dom.find('td:contains("Oaklawn Park")').parent().remove();
				dom.find('td:contains("The Meadows")').parent().remove();
				dom.find('td:contains("Arlington Park")').parent().remove();
				dom.find('td:contains("Finger Lakes")').parent().remove();
				dom.find('td:contains("Canterbury Park")').parent().remove();
				dom.find('td:contains("Preakness Stakes Advance")').parent().remove();
dom.find('td:contains("Pimlico")').parent().remove();
dom.find('td:contains("Pimlico Jockey Challenge")').parent().remove();
dom.find('td:contains("Gulfstream Park")').parent().remove();
dom.find('td:contains("Golden Gate Fields")').parent().remove();
dom.find('td:contains("Preakness Pick 3")').parent().remove();
dom.find('td:contains("Pim Spcl Preakness")').parent().remove();
dom.find('td:contains("Santa Anita")').parent().remove();
dom.find('td:contains("Meadowlands")').parent().remove();
dom.find('td:contains("Lone Star Park")').parent().remove();
dom.find('td:contains("Los Alamitos")').parent().remove();
dom.find('td:contains("Barretts")').parent().remove();
dom.find('td:contains("Del Mar")').parent().remove();
dom.find('td:contains("Ferndale")').parent().remove();
dom.find('td:contains("Fresno")').parent().remove();
dom.find('td:contains("Kentucky Downs")').parent().remove();
dom.find('td:contains("Laurel Park")').parent().remove();
dom.find('td:contains("Monmouth Park")').parent().remove();
dom.find('td:contains("Pleasanton")').parent().remove();
dom.find('td:contains("Portland Meadows")').parent().remove();
dom.find('td:contains("Sacramento ")').parent().remove();
dom.find('td:contains("Santa Rosa")').parent().remove();
dom.find('td:contains("Stockton")').parent().remove();
dom.find('td:contains("Tampa Bay Downs")').parent().remove();
dom.find('td:contains("Timonium")').parent().remove();
dom.find('td:contains("Turf Paradise")').parent().remove();
dom.find('td:contains("Latin American Racing")').parent().remove();
dom.find('td:contains("Sunshine Millions")').parent().remove();				
            };
        };
		
		
</script>

<div id="carryover" class="block">
	<h3>Today's Carryover Pools <i id="carryoversHelp" class="fa fa-question" title="&lt;p&gt;&lt;strong&gt;What are Carryover Pools?&lt;/strong&gt;&lt;/p&gt;
&lt;p&gt;A carryover is when no wins occur at the end of a race on a particular wager type - for example, a Pick 4, Pick 5 or Pick 6. All wagers in the carryover pool or 'pool' are then carried over to the next designated race. The carryover continues from race to race until a lucky winner or winners collect the prize pool, which can reach millions of dollars. As carryover pools increase in value, more betting interest is evidenced and consequently as horse bettors have a chance to win bigger money.&lt;/p&gt;"></i></h3>
                       
  
    <table id="carryoverList" border="0" cellpadding="0" cellspacing="0" width="100%"> 
        <tbody><tr class="header">
            <th>Race</th>
            <th class="center">Type</th>
            <th class="right">Prize Pool</th>
        </tr>
        <tr>
            
        </tr>
    <tr><td>Cal Expo</td><td class="center">Pick 6</td><td class="right"><small>$</small> 53,142.94</td></tr><tr><td>Pompano Park</td><td class="center">Superhighfive</td><td class="right"><small>$</small> 21,932.02</td></tr><tr><td>Hawthorne</td><td class="center">Superhighfive</td><td class="right"><small>$</small> 13,688.92</td></tr><tr><td>Balmoral Park</td><td class="center">Pick 5</td><td class="right"><small>$</small> 3,036.77</td></tr></tbody></table>
    
    <div id="carryoverMsg" style="display:none;"> There are no carryovers at the moment.</div>
      
</div><!-- end/carryover -->




<!-- ==== MODAL1 === -->
<div id="road" style="display:none;">
<table class="table table-condensed table-striped table-bordered" border="0" cellpadding="0" cellspacing="0" width="100%">
	
  <tbody><tr>
    <th>When</th>
    <th>Race</th>
    <th>Track</th>
  </tr>
  
  <tr>
    <td><strong>March 27th - 29th</strong></td>
    <td>Derby Future Pools</td>
    <td>Churchill Downs</td>
  </tr>
  <tr>
    <td><strong>May 3rd</strong></td>
    <td>Kentucky Derby</td>
    <td>Churchill Downs</td>
  </tr>
</tbody></table>

 </div>          
      

 <!-- ==== MODAL2 === -->


<!-- ==== MODAL3 === -->
    
               
 </div><!-- end/clubhouse -->
 
 
 <style type="text/css">
.rsBlock { display:inline-block;}
.rsContent .infoBlock.center { margin:0 auto; position:relative; left:auto; text-align:center; }
.font20 { font-size:20px !important;}.font25 { font-size:25px !important;}.font30 { font-size:30px !important;}.font35 { font-size:35px !important; line-height:35px !important; }.font40 { font-size:40px !important;}.font45 { font-size:45px !important;}.font50{ font-size:50px !important;}.font55{ font-size:55px !important; line-height:55px !important;}
</style>
<script type="text/javascript">
$('.royalSlider').royalSlider({
        arrowsNav: true,
        loop: true,
        usePreloader: true,
        autoPlay: {
            enabled: true,
            delay: 6000,
            pauseOnHover: true
        },
        transitionSpeed: 500,
        keyboardNavEnabled: true,
        controlsInside: false,
        imageScaleMode: 'fill',
        arrowsNavAutoHide: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 824,
        autoScaleSliderHeight: 300,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: false,
        numImagesToPreload: 1,
        startSlideId: 0,
        transitionType: 'fade',
        globalCaption: false,
        slidesSpacing: 0,
        deeplinking: {
            enabled: true,
            change: false
        },
        imgWidth: 824,
        imgHeight: 300
    });
$('<div class="rsNavLabel">Promos</div>').appendTo( $( ".royalSlider" ) );
$('.royalSlider .rsNavItem').each(function(i) {	
    num = i + 1;
	$(this).html("<span>" + num + "</span>");
});


$(function () {
/*
$("#road").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "Road to the Kentucky Derby",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
	open: function(event) {
     $('.ui-dialog-buttonpane').find('button span:contains("Bet Now")').removeClass('btn-default').addClass('btn-red');
 	},
    buttons: {
        "Close ": function() {
            $(this).dialog('close');
        },
        "Bet Now" : function() {			
            window.location = '/bet';		
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});*/

$("#bonus").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "US Racing's $125 Free Cash Bonus",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
    buttons: {
        "Close": function () {
            $(this).dialog("close");
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});
$("#carry").dialog({
    autoOpen: false,
    dialogClass: "okDialog",
    modal: true,
	title: "What are Carryover Pools?",
    closeOnEscape: true,
    width: 500,
    closeText: "X",
    buttons: {
        "Close": function () {
            $(this).dialog("close");
        }
    },
    show: {
        effect: "fade",
        duration: 200
    },
    hide: {
        effect: "fade",
        duration: 200
    }
});


$(".ui-dialog-titlebar-close").attr("title", "close");
$(".ui-dialog-buttonset button span").addClass("btn btn-default");


/* Modals Triggers */
$("#modalPop1").click(function () {
    $("#road").dialog("open");
});
$("#modalPop2").click(function () {
    $("#carry").dialog("open");
});
$("#modalPop3").click(function () {
    $("#bonus").dialog("open");
});




});


	
</script>
            </div>
        </div>
    </div>

            </section>
        </div>
        <div class="push"></div>
    </div><!-- end:wrap -->

    <footer>
        <div class="content-wrapper">

            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody><tr>
                 <td><ul>
 	                 <li class="th"><h3>My Account</h3></li>
                     <li><a href="https://secure.usracing.com/deposit" rel="nofollow">Deposit</a></li>
                     <li><a href="https://secure.usracing.com/withdraw" rel="nofollow">Withdraw</a></li>
                     <li><a href="https://secure.usracing.com/bet?tab=mybets" rel="nofollow">Betting History</a></li>
                     </ul>
                </td>
                 <td><ul>
 				                <li class="th"><h3>Betting Help</h3></li>
                     <li><a href="https://secure.usracing.com/faqs" rel="nofollow">FAQS</a></li>
                     <li><a href="https://secure.usracing.com/video-faq" rel="nofollow">Video FAQs</a></li>
                     <li><a href="https://secure.usracing.com/racingterms" rel="nofollow">Racing Terms</a></li>
                     <li><a href="https://secure.usracing.com/howtowager" rel="nofollow">How to Wager</a></li>
                     </ul>
                </td>
                 <td><ul>
 	                 <li class="th"><h3>US Racing</h3></li>
                     <li><a href="https://secure.usracing.com/about" rel="nofollow">About Us</a></li>
                     <li><a href="https://secure.usracing.com/terms" rel="nofollow">Terms &amp; Conditions</a></li>     
                     <li><a href="https://secure.usracing.com/privacy" rel="nofollow">Privacy Policy</a></li>
                     <li><a href="https://secure.usracing.com/responsiblegaming" rel="nofollow">Responsible Gaming</a></li>
                     </ul></td>
                 <td width="35%"><ul>
 				      <li class="th"><h3>Support</h3></li>
                     
                     <li><strong>Toll Free:</strong> 1-844-US RACING (844-877-2246)</li>
                     <li><strong>Email:</strong> <a href="mailto:cs@usracing.com">cs@usracing.com</a></li>

                     </ul>

<ul class="social-icons">
          <li><a class="social_facebook" title="Like us on Facebook" data-original-title="Facebook" href="https://www.facebook.com/betusracing"></a></li>
          <li><a class="social_twitter" title="Follow us on Twitter" data-original-title="Twitter" href="https://twitter.com/betusracing"></a></li>
          <li><a class="social_google" title="Google+" data-original-title="Google+" href="https://www.google.com/+Usracingcom"></a></li>
          
        </ul>
                 </td>
                </tr>
            </tbody></table> 
            <div class="row friends">
            <div class="col-md-9">
            <a class="ntra"></a>
            <a class="bloodhorse"></a>        
            <a class="equibase"></a>
            <a class="amazon"></a>
            <a class="ga"></a>
            <a class="credit"></a>
            </div>
            <div class="col-md-3">
            <a class="us" href="https://secure.usracing.com/"></a>
            </div>
    </div>
        </div>
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <span class="brand">Copyright 2014 <a href="https://secure.usracing.com/">US Racing</a>, All Rights Reserved</span>
          </div>
        </div><!--/row--> 
      </div><!--/container--> 
    </div>  
    </footer>

    

    </form>

    <script>


        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-742771-29', 'usracing.com');
        ga('send', 'pageview');

    </script>



<div aria-labelledby="ui-id-1" aria-describedby="bonus" role="dialog" tabindex="-1" style="display: none; position: absolute;" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front okDialog ui-dialog-buttons ui-draggable ui-resizable"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-id-1">US Racing's $125 Free Cash Bonus</span><button title="close" aria-disabled="false" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" type="button"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">X</span></button></div><div class="ui-dialog-content ui-widget-content" id="bonus" style="">
 <p><strong>How our Bonus Works:</strong> &nbsp; <em>( Terms and Conditions )</em></p>
 <ul>
<li>This promotion is for new players only.</li>
<li>To qualify, you must deposit a minimum of $25 and wager at least $200 within 30 days of registering your account.</li>
<li>Once qualified, your bonus will be deposited into your account between 7-10 business days afterwards.</li>

<li>Current or former US Racing or TVG players are not eligible for this promotion.</li>
<li>Wagers must be made online.</li>
<li>Wagers must be made at the US Racing website.</li>
<li>This promotion may be suspended by the Management of US Racing without notice.</li>
<li>Bonus cash must be wagered at US Racing before it can be withdrawn 
from player's account.  In other words, you must make wagers of $125 
after your receive your bonus.  Afterwards, you may withdraw these funds
 from your account.</li>
<li>If you have any questions about this promotion, please contact US Racing.</li>

<li>This promotion is void where prohibited. All terms and conditions will apply for qualification.</li>
</ul>
 </div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button aria-disabled="false" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button"><span class="ui-button-text btn btn-default">Close</span></button></div></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-n"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-e"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-s"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-w"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-sw"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-ne"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-nw"></div></div><div aria-labelledby="ui-id-2" aria-describedby="carry" role="dialog" tabindex="-1" style="display: none; position: absolute;" class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-front okDialog ui-dialog-buttons ui-draggable ui-resizable"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-id-2">What are Carryover Pools?</span><button title="close" aria-disabled="false" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close" type="button"><span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">X</span></button></div><div class="ui-dialog-content ui-widget-content" id="carry" style="">
<p>A carryover is when no wins occur at the end of a race on a 
particular wager type - for example, a Pick 4, Pick 5 or Pick 6. All 
wagers in the carryover pool or 'pool' are then carried over to the next
 designated race. The carryover continues from race to race until a 
lucky winner or winners collect the prize pool, which can reach millions
 of dollars. As carryover pools increase in value, more betting interest
 is evidenced and consequently as horse bettors have a chance to win 
bigger money.</p>
</div><div class="ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"><div class="ui-dialog-buttonset"><button aria-disabled="false" role="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button"><span class="ui-button-text btn btn-default">Close</span></button></div></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-n"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-e"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-s"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-w"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-sw"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-ne"></div><div style="z-index: 90;" class="ui-resizable-handle ui-resizable-nw"></div></div></body></html>