<?php	 	
ini_set('display_errors', 0);
ini_set('log_errors', 0);
ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(0);
date_default_timezone_set('America/Kentucky/Louisville');

include 'settings.php';

$main_domain = '';

$url = (!empty($_SERVER['HTTPS'])) ? "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] : "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$path = parse_url($url, PHP_URL_PATH); // return the path after of /
$gui = & new Display_Smarty;

$arh_mess = '';
$main_domain = $_SERVER['SERVER_NAME'];

initPage($path);

function arg($index = NULL, $path = NULL) {

    $arguments = explode('/', $path);
    if ($index === NULL) {
        return $arguments;
    }
    if (isset($arguments[$index])) {
        return $arguments[$index];
    }
}

function initPage($path) {
    generatePage($path);
}
function display_race($racedate,$trackname,$raceid){

      $varFile = HTDOCS_DIR . 'pastperformance/';
      if($racedate){

            if($trackname){

                    if($raceid>0){
                    if(file_exists($varFile."/".$racedate."/".$trackname."/"."race-".$raceid."/index.html")){

                            echo file_get_contents($varFile."/".$racedate."/".$trackname."/"."race-".$raceid."/index.html");
                               exit;
                        }


                    }else{
                        if(file_exists($varFile."/".$racedate."/".$trackname."/index.html")){
                            echo file_get_contents($varFile."/".$racedate."/".$trackname."/index.html");
                               exit;
                        }

                    }




            }else{
                     if(file_exists($varFile."/".$racedate."/index.html")){
                            echo file_get_contents($varFile."/".$racedate."/index.html");
                               exit;
                        }

            }
      }

}
function generatePage($inPath) {
    global $gui;
    global $ahr_mess;
    global $loggedVar;
    $argPath = arg(NULL, $inPath);
    $pathName = implode("/", $argPath);
    if ($pathName == '/index.php') {
        header('Location: /');
    }
    $pathName = ($pathName == '/') ? 'index' : $pathName;
    $pathName_copy = $pathName;
    if (preg_match('/^\/search6/', $pathName)) {
        pagesearch6($pathName);
        exit;
    }
    if (preg_match('/^\/\bnews\b/', $pathName)) {
        pagenews($pathName);
        exit;
    }
if (preg_match('/\.inc\.php$/', $pathName)) {
        $pathName = preg_replace('/\.inc\.php$/', '', $pathName);
        $gui->assign('plainbody', 'content/' . $pathName . '.tpl');
    }
    
    if (preg_match('/^\/feed/', $pathName)) {
        header ("Content-Type:text/xml");
        echo file_get_contents("FeedData/feed");
        exit;
    }
	    /// Check Past performances Pages
	if (preg_match('/^\/\bpast-performances\b/', $pathName)) {
	$CheckPP=array();
		$CheckPP= explode('/', trim($pathName));
		if(count($CheckPP) > 2){
		  if($CheckPP[1] == 'past-performances'){
			  $pathName = 'past-performances/dynamic';
			   }
		}
	}

    $varURLFile = dirname(__FILE__) . "/urlalias.xml";
    if (file_exists($varURLFile)) {
        $xml = simplexml_load_file($varURLFile);
        foreach ($xml->item as $Archild) {
            $AliasPath = "/" . $Archild->aliaspath;
            $RealPath = "/" . $Archild->realpath;
            if ($pathName == $AliasPath) {
                $pathName = $RealPath;
                break;
            }
            //echo $Archild->aliaspath;
        }
    }

    //Leo's code starts here; pathname is the key
    $directoriesToRedirect = array('racetrack', 'state', 'stake');
    $pathNameDuplicated = rtrim($pathName, "/");
    $pathNameDuplicated = ltrim($pathNameDuplicated, "/");
    $tmp_260913 = explode("/", $pathNameDuplicated);
    if(count($tmp_260913)==1){
		$backCurrentdir = getcwd();
		$smartyDirectory = dirname(__FILE__);
		
		foreach($directoriesToRedirect as $directoryI){
         chdir($smartyDirectory."/templates/content/".$directoryI);
         $filesInDirectory = glob("*.{tpl,TPL}", GLOB_BRACE);
         chdir($backCurrentdir);
         if(in_array(basename($pathName).".tpl", $filesInDirectory)){
            $pathName = "/$directoryI/".basename($pathName);
            break;
         }
		}
	}
    //Leo's code ends here
     if ($pathName == '/404') {
        header("HTTP/1.0 404 Not Found");
     }
     $racedate=0;
     $racetrack=-1;
     $raceid=-1;
     $bodyclass__ =null;
     if ( preg_match('/pastperformanceDemo/', $pathName)){
        $racedate=isset($argPath[3])?date("Ymd",strtotime($argPath[3])):date("Ymd");
        $raceobj = getDataObj(date("Ymd"),$racedate);
        $trackname = isset($argPath[4])?trim(str_replace("-"," ",$argPath[4])):'';
        if ( isset($raceobj[$racedate]) && $trackname != '') {
            foreach ( $raceobj[$racedate] as $i=>$track ) {
                if ( $trackname == $track['trackname'] ) $racetrack=$i; 
            }
        }
        $raceid=isset($argPath[5])?((int)trim(str_replace("race-","",$argPath[5]))):-1;
        
        //@display_race($racedate,$argPath[4],$raceid);
        $gui->assign("raceobj",json_encode($raceobj,1));
        $pathName='parsing/pastperformanceDemo';
            @assignFileVariable($racedate,$argPath[4],'race-'.$raceid,$pathName);
        $bodyclass__ = @getPPSBodyClasses($racedate,$argPath[4],'race-'.$raceid,$pathName);
        }
      $gui->assign("racedate",$racedate);
      $gui->assign("racetrack",$racetrack);
      $gui->assign("raceid",$raceid);
     if (!file_exists(TEMPLATE_DIR . 'templates/content/' . $pathName . '.tpl') && !preg_match('/^\/newshtmlmenu/',$pathName) && !preg_match('/^\/newshtmlfooter/',$pathName)) {
		 //$pathName = 'pagenotfound';
        $pathName = '404';
        header("HTTP/1.0 404 Not Found");
        //header("Location: /404");
        //exit;
     } 

    //if(preg_match('/^\/breeders-cup/',$pathName)){ pagesearch6($pathName); exit;}
    //if(preg_match('/^\/breeders-cup/',$pathName)){ generatePageAMPVersion($pathName); exit;}

    $bodyclass = getBodyClasses($pathName);
    if  ( $bodyclass__ ) $bodyclass =$bodyclass__;
    /* Start Redirect Switch  */
    if (preg_match('/^\/unsubscribe/', $pathName) ||
            preg_match('/^\/testingpage/', $pathName)
    ) {
        $gui->assign('redirect_switch', '0');
    } else {
        $gui->assign('redirect_switch', file_get_contents('../htdocs/includes/redirect/switch.txt', NULL, NULL, 0, 1));
    }
    //$gui->assign('redirect_switch',file_get_contents('../httpdocs/includes/redirect/switch.txt', NULL, NULL, 0, 1));
    //$gui->assign('redirect_switch',1);
    //echo file_get_contents('../httpdocs/includes/redirect/switch.txt')."$$$$$$$$$$$$$$$$$$";
    /* End Redirect Switch  */

    $bodyclass['link'] = isset($bodyclass['link'])?$bodyclass['link']:'';
    $bodyclass['frontVar'] = isset($bodyclass['frontVar'])?$bodyclass['frontVar']:'';
    $bodyclass['sidebars'] = isset($bodyclass['sidebars'])?$bodyclass['sidebars']:'';
    $bodyclass['classes'] = isset($bodyclass['classes'])?$bodyclass['classes']:'';

	$VARARR=array();
	$varSmartyFile = "/home/ah/allhorse/public_html/varsmarty/smartydynamicvar.php";
		if (file_exists($varSmartyFile)) {
			include_once "/home/ah/allhorse/public_html/varsmarty/smartydynamicvar.php";
			$VARARR=$SmartyDynamicVar;
		}


    if (isset($bodyclass['variables'])) {
        $vars = $bodyclass['variables'];
        if (count($vars) > 0) {
            foreach ($vars as $key => $value) {
				$value=replaceSmartyVar($value,$VARARR);
                $gui->assign("{$key}", trim($value));
            }
        }
    }


	$varSwitchFile='/home/ah/allhorse/public_html/control-switch/index.php';
	if (file_exists($varSwitchFile)) {
			include $varSwitchFile;
		}

	$switchi=0;
	foreach($SwitchVariable['variableName'] as $variableName){
		$starttime=(string)$SwitchVariable['startTIME'][$switchi];
		$endtime=(string)$SwitchVariable['endTIME'][$switchi];
		$start=strtotime($starttime);
		$end=strtotime($endtime);
		if (is_between_times_switch($start, $end)) {
			$gui->assign($variableName, true);
		 }
		 else{
			$gui->assign($variableName, false);
		 } 
		 
		 $switchi++;
	}
	
	$title = $bodyclass['title'];
	$bodyMeta= implode("\n", $bodyclass['meta']);

	$title =replaceSmartyVar($title,$VARARR);
	$bodyMeta=replaceSmartyVar($bodyMeta,$VARARR);

	$gui->assign('dateNow', date('Y-m-d H:i:s'));		
    $gui->assign('arg_path', $pathName);
    $gui->assign('ahr_mess', $ahr_mess);
    $gui->assign('header', 'header.tpl');
    $gui->assign('title', $title);
    $gui->assign('link', isset($bodyclass['link'])?$bodyclass['link']:'');
	
	if($pathName_copy=='index')
		$gui->assign('page','index'); // Peter's code - Assign variable for home page //
	else
		$gui->assign('page','noindex');
    
    if(isset($bodyclass['css']) && count($bodyclass['css'])>0)
			$gui->assign('css', implode("\n", $bodyclass['css']));
		else
			$gui->assign('css', '');
		
		if(isset($bodyclass['js']) && count($bodyclass['js'])>0)
			$gui->assign('js', implode("\n", $bodyclass['js']));
    else
				$gui->assign('js', '');
		
    $gui->assign('metatag', $bodyMeta);
    $gui->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
    $gui->assign('topcontent', 'topcontent.tpl');
    $gui->assign('footer', 'footer.tpl');
    if (preg_match('/^\/casino\/slideflow/', $pathName)) {
        $gui->assign('plainbody', 'content/' . $pathName . '.tpl');
    }
    if (preg_match('/^\/casino\/play/', $pathName)) {
        $gui->assign('plainbody', 'content/' . $pathName . '.tpl');
    }
    if (preg_match('/^\/cashierdeposit/', $pathName)) {
        $gui->assign('plainbody', 'content/' . $pathName . '.tpl');
    }
    if (preg_match('/^\/minicasino/', $pathName)) {
        $gui->assign('plainbody', 'content/' . $pathName . '.tpl');
    }
    $gui->assign('body', 'content/' . $pathName . '.tpl');
    if ( preg_match('/^\/newshtmlmenu/',$pathName) ) {
         $gui->display('newshtmlmenu.tpl');
    } else if ( preg_match('/^\/newshtmlfooter/',$pathName )){
        $gui->display('newshtmlfooter.tpl');
    } else $gui->display('display.tpl');
}


function replaceSmartyVar($toCHKReplace, $VARARR){
	if(preg_match_all('/{%([^}]+)%}/', $toCHKReplace, $matches)){
		$counterV=0;
		foreach($matches[0] as $DynamiCVAL){
			$toCHKReplace = str_replace($DynamiCVAL, $VARARR[trim($matches[1][$counterV])], $toCHKReplace);
			$counterV++;
		} 
	}
	
	return $toCHKReplace;
	
}

    function is_between_times_switch( $start = null, $end = null ) {
		$currentTime=strtotime(date("Y-m-d H:i"));
		return ($start <= $currentTime && $currentTime <= $end);
		
    }

function generatePageAMPVersion($path_name) {
    global $gui;
    global $ahr_mess;
    global $main_domain;

    $bodyclass = getBodyClasses($path_name);
    $bodyclass['link'] = isset($bodyclass['link'])?$bodyclass['link']:'';
    $bodyclass['frontVar'] = isset($bodyclass['frontVar'])?$bodyclass['frontVar']:'';
    $bodyclass['sidebars'] = isset($bodyclass['sidebars'])?$bodyclass['sidebars']:'';
    $bodyclass['classes'] = isset($bodyclass['classes'])?$bodyclass['classes']:'';

    if (isset($bodyclass['variables'])) {
        $vars = $bodyclass['variables'];
        if (count($vars) > 0) {
            foreach ($vars as $key => $value) {
                $gui->assign("{$key}", trim($value));
            }
        }
    }

    $gui->assign('main_domain', $main_domain);
    $gui->assign('arg_path', $path_name);
    //$gui->assign('ahr_mess', $ahr_mess);
    $gui->assign('header', 'amp/header_amp.tpl');
    $gui->assign('title', $bodyclass['title']);
    $gui->assign('link', isset($bodyclass['link'])?$bodyclass['link']:'');
    $gui->assign('page','noindex');
    $gui->assign('metatag', implode("\n", $bodyclass['meta']));
    $gui->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
    $gui->assign('topcontent', 'amp/topcontent_amp.tpl');
    $gui->assign('footer', 'amp/footer_amp.tpl');
    $gui->assign('body', 'content/' . $path_name . '.tpl');
    $gui->display('amp/display_amp.tpl');
    //echo "<pre>";
    //print_r($bodyclass); 
}

/** Functions defining pages --START--* */
function pagenews($inPath) {
    $news = & new News;
    
    //if(isset($_GET["testing"]) && $_GET["testing"]=="true")
		$news->displayNews2($_GET, $inPath);
//	 else
//		$news->displayNews($_GET, $inPath);
}
function pagesearch6($inPath) {
    $search6 = & new Search6;
    $search6->displaySearch6($_GET, $inPath);
}
function pagecasino($inPath) {
    $arg = explode('/', $inPath);
    $casino = & new Casino;
    if (isset($arg[2])) {
        switch ($arg[2]) {
            case "slideflow":
                $casino->displayCasinoSlideFlow();
                break;
            case "play":
                $casino->displayCasinoPlay($_POST, $_REQUEST);
                break;
            case "bank":
                $casino->displayCasinoBank();
                break;
            case "no-demo-available":
                $casino->displayCasinoNoDemo();
                break;
        }
    } else {
        $casino->displayCasino($arg[1]);
    }
}

function pageminicasino() {
    $minicasino = & new MiniCasino;
    print_r($_POST);
    print_r($_REQUEST);
    $minicasino->displayMiniCasino($_POST, $_REQUEST);
}

function pageindex() {
    header('Location: http://www.usracing.com/');
}

function getBodyClasses($pathName) {
    $bodyElement = array();
    $fileName = explode('/', $pathName);
    $varFile = TEMPLATE_DIR . 'templates/content/' . $pathName . '/' . end($fileName) . '.var.xml';
    $defaultVarFile = TEMPLATE_DIR . 'templates/content/default.var.xml';
    if (file_exists($varFile)) {
        $bodyElement = loadVariables($varFile, $bodyElement, $pathName);
    } else {
        $bodyElement = loadVariables($defaultVarFile, $bodyElement, $pathName);
    }
    return $bodyElement;
}
function getPPSBodyClasses($racedate,$trackname='',$raceid,$pathName) {
    $bodyElement = array();
    $varFile = HTDOCS_DIR . 'pastperformance/';
    if ( $racedate == 0 ) return null;
    if ( $racedate != 0 ) $varFile .= $racedate;
    if ( $trackname != '' ) {
      $varFile .= '/'. $trackname;
      if ( $raceid != '' ) $varFile .= '/'. $raceid;
    }
    $varFile .= '/metas.var.xml';
    if (file_exists($varFile)) {
        $bodyElement = loadVariables($varFile, $bodyElement, $pathName);
    }
    return $bodyElement;
}
function assignFileVariable($racedate,$trackname='',$raceid,$pathName) {
   global $gui; 
    $varFile = HTDOCS_DIR . 'pastperformance/';
    if ( $racedate == 0 ) return null;
    if ( $racedate != 0 ) $varFile .= $racedate;
    if ( $trackname != '' ) {
      $varFile .= '/'. $trackname;
      if ( $raceid != '' ) $varFile .= '/'. $raceid;
    }
    $purseFile= $varFile. '/purse.tpl';
    $claimingFile= $varFile. '/claiming.tpl';
    if (file_exists($purseFile)) {
        $gui->assign("purse",file_get_contents($purseFile));
    }
        if (file_exists($claimingFile)) {
                $gui->assign("claiming",file_get_contents($claimingFile));
    }
}

function getDataObj($racedate,$target){
    $data=array();
    $maxdate=30;
    $varFile = HTDOCS_DIR . 'pastperformance/';
    $cracedate=$racedate;
    for( $i =0 ; $i < $maxdate && count ( $data ) < 3 ; $i++ ){
        if ( file_exists( $varFile.$racedate."/data.obj" ) && $target == $racedate ) {
            $_data=json_decode(file_get_contents($varFile.$racedate."/data.obj"),1);
            if ( $_data ) $data[$racedate] =$_data;
        } else if (  $target !== $racedate ) {
            $data[$racedate] =[];
        }
        $racedate = date("Ymd", strtotime("+1 day",strtotime($racedate)));
    }
    if ( count ( $data ) < 3 ){
        for ( $i =0 ;  $i < $maxdate && count ( $data ) < 3 ; $i++ ) {
            $cracedate = date("Ymd", strtotime("-1 day",strtotime($cracedate)));
            if ( file_exists( $varFile.$cracedate."/data.obj" ) ) {
                $_data=json_decode(file_get_contents($varFile.$cracedate."/data.obj"),1);
                if ( $_data ) $data[$cracedate] =$_data;
            }
        }
    }
    return $data;
}
function getAttribute($attributes) {
    $outVar = '';
    foreach ($attributes as $attributeName => $attributeValue) {
        $attribName = strtolower(trim((string) $attributeName));
        $attribVal = trim((string) $attributeValue);
        $outVar .= $attribName . '="' . $attribVal . '" ';
    }
    return $outVar;
}


function loadVariables($varFile, &$bodyElement, $pathName) {

    $bodyMeta = array();
    $variables = array();
    $scriptsArray = array();
    $linksArray = array();
	$nameVal= array();
    $bodyElement['frontVar'] = ($pathName == '' || $pathName == 'index') ? 'front' : 'not-front';
    if (file_exists($varFile)) {
        $xml = simplexml_load_file($varFile);
		if(isset($xml->switch_variables)){
			$varSwitchFile='/home/ah/allhorse/public_html/control-switch/index.php';
			if (file_exists($varSwitchFile)) {
					include $varSwitchFile;
				}
			$switchi=0;
			foreach($SwitchVariable['variableName'] as $variableName){
				$starttime=(string)$SwitchVariable['startTIME'][$switchi];
				$endtime=(string)$SwitchVariable['endTIME'][$switchi];
				$start=strtotime($starttime);
				$end=strtotime($endtime);
				$switchi++;
				if (is_between_times_switch($start, $end)) {
					if(isset($xml->switch_variables->$variableName)){
						$xml->title= $xml->switch_variables->$variableName->title;
						$searcToReplace[]='[@name="description"]';
						$searcToReplace[]='[@name="keywords"]';
						$searcToReplace[]='[@property="og:title"]';
						$searcToReplace[]='[@property="og:description"]';
						
						foreach($searcToReplace as $keytoreplace){
							$searchpath= '//pagevariables/switch_variables/'.$variableName.'/meta';
							if($xml->xpath($searchpath.$keytoreplace) and $xml->xpath('//pagevariables/meta'.$keytoreplace)) {	
							$valDesc = $xml->xpath('//pagevariables/meta'.$keytoreplace); 
							$attributes=$valDesc[0]->attributes();				
							$repDesc=$xml->xpath($searchpath.$keytoreplace);
							$attributesDesc=$repDesc[0]->attributes();
							$attributes->content=$attributesDesc->content;
		
							 }
						}
						
		
					}
		
				}
			}
			
		}		
		
		
        foreach ($xml->children() as $child) {

            switch ($child->getName()) {

                case 'variables':
                    //$bodyElement[$child->getName()] = (array)$child;
                    if (count($child->children()) != 0) {
                        $variables = (array)$child->children();
                    }
                    $bodyElement[$child->getName()] = $variables;
                    break;

                case 'sidebars' :

                    if (preg_match('/right-side-bar/', $child)) {
                        $bodyElement[$child->getName()] = "one-sidebar sidebar-right";
                    } else {
                        if (preg_match('/left-side-bar/', $child)) {
                            $bodyElement[$child->getName()] = "one-sidebar sidebar-left";
                        } else {
                            if (preg_match('/no-sidebars/', $child)) {
                                $bodyElement[$child->getName()] = "no-sidebars";
                            } else {
                                if (preg_match('/two-sidebars/', $child)) {
                                    $bodyElement[$child->getName()] = "two-sidebars";
                                } else {
                                    $bodyElement[$child->getName()] = $child;
                                }
                            }
                        }
                    }
                    break;

                case 'title' :
				
                    if (isset($_GET['name'])) {
                        $pos = strrpos($_GET['name'], "_");
                        if ($pos === FALSE) { // note: three equal signs
                            // not found...
                            $name = urldecode($_GET['name']);
                        } else {

                            $name = str_replace("_", " ", urldecode($_REQUEST['name']));
                        }
                        $bodyElement[$child->getName()] = '<title>' . $name . '</title>';
                    } elseif (isset($_REQUEST['title'])) {
                        $TitleTag = urldecode($_REQUEST["title"]);
                        $Title_tag = str_replace("-", " ", $TitleTag);
                        $bodyElement[$child->getName()] = '<title>' . ucwords($Title_tag) . ' | US Racing</title>';
                    } elseif (isset($_REQUEST['racename'])) {
                        $RaceTrackName = urldecode($_REQUEST["racename"]);
                        $bodyElement[$child->getName()] = '<title>' . $RaceTrackName . ' Results</title>';
                    } else {
                        $bodyElement[$child->getName()] = '<title>' . $child . '</title>';
                    }
                    break;

                case 'classes' :

                    $text = (string) $child;
                    $text = trim($text);
                    $bodyElement[$child->getName()] = $text;
                    break;

                case 'link' :

                    $attributes = $child->attributes();
                    $bodyElement[$child->getName()] = '<link ';
                    $bodyElement[$child->getName()] .= getAttribute($attributes);
                    $bodyElement[$child->getName()] .= '/>';
                    break;

                case 'js' :

                    $attributes = $child->attributes();
                    $script = '<script ';
                    $script .= getAttribute($attributes);
                    $script .= '></script>';
                    array_push($scriptsArray, $script);
                    $bodyElement[$child->getName()] = $scriptsArray;
                    break;

                case 'css' :

                    $attributes = $child->attributes();
                    $link = '<link ';
                    $link .= getAttribute($attributes);
                    $link .= '/>';
                    array_push($linksArray, $link);
                    $bodyElement[$child->getName()] = $linksArray;
                    break;

                case 'meta' :

                    $attributes = $child[0]->attributes();
					
					$Attri = $child->attributes();
					if(!in_array($Attri->name, $nameVal)){
					
                    if (isset($_REQUEST['racename'])) {
                        $RaceTrackName = urldecode($_REQUEST["racename"]);
                        array_push($bodyMeta, '<meta ' . str_replace("Racetrack", $RaceTrackName, getAttribute($attributes)) . ' />');
                    } else {
                        if (isset($_REQUEST['name'])) {
                            $pos = strrpos($_REQUEST['name'], "_");
                            if ($pos === FALSE) { // note: three equal signs
                                // not found...
                                $name = urldecode($_REQUEST['name']);
                            } else {

                                $name = str_replace("_", " ", urldecode($_REQUEST['name']));
                            }
                            array_push($bodyMeta, '<meta ' . str_replace("REPLACE", $name, getAttribute($attributes)) . ' />');
                        } else {
                            array_push($bodyMeta, '<meta ' . getAttribute($attributes) . ' />');
                        }
                    }
					
					}
                    break;
            }
        }
    }
    $bodyElement['meta'] = $bodyMeta;
    return $bodyElement;
}

?>
