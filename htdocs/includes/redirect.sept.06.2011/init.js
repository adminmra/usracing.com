var rd_internationalURL = 'http://www.gohorsebetting.com/international';

var rd_select = {
		// TwinSpires
		'enable':true,
		'html':'/includes/redirect/step2-4.html', // Overlay Screen
		'affliate':['http://partners.twinspiresaffiliates.com/processing/clickthrgh.asp?btag=a_647b_3']
}

var rd_general = {
		// TVG
		'enable':true,
		'html':'/includes/redirect/step2-1.html', // Overlay Screen
		'affliate':['http://www.linkconnector.com/traffic_affiliate.php?lc=051350037293004262&atid=ahr']
}



var rd_special =[
	{
		// BODOG
		'enable':false,
		'html':'/includes/redirect/step2-2.html', // Overlay Screen
		'affliate':['http://www.bodog.eu/welcome/3752114/horse-betting']
	},
	{	
		// BETUS
		'enable':false,
		'html':'/includes/redirect/step2-3.html', // Overlay Screen
		'affliate':['http://record.partners.betus.com/_juVSIhAYsWNoofBpZ-qTpmNd7ZgqdRLk/4','http://record.partners.betus.com/_juVSIhAYsWMMPRNicYDTVGNd7ZgqdRLk/8']	
	},
	{
		// Betonline
		'enable':false,
		'html':'/includes/redirect/step2-5.html', // Overlay Screen
		'affliate':['http://www.betonline.com/landingpages/belmontsaff/?btag=a_1890b_844&affid=1691']
		//'affliate':['http://partners.betonline.com/processing/clickthrgh.asp?btag=a_1890b_78']
	}
];

// =====   Betonline & SportsInteraction Redirects when Bodog  and Betus are both disabled  ========================== //

var rd_special_fail =[
	{	
		// SportsInteraction 
		'enable':false,
		'html':'/includes/redirect/step2-6.html', // Overlay Screen
		'affliate':['http://www.allhorseracing.com']	
	}
];



var rd_default = {
	'enable':false,
	'html':'/includes/redirect/step2-default.html' // Overlay Screen
}



// =====   You Do Not need to Edit anything below here   ================ //
var rd_domainURL = 'allhorseracing.com';

var rd_defaultTimeOut = .5; // hours
var rd_member = Get_Cookie( rot13('TRACK'));
var rd_tmpRD = Get_Cookie('REDIRECT');

/* Style Variables */
var rd_background_color = 'black';
var rd_box_background_color = 'white';
var rd_box_border_color = 'white';
var rd_box_border_width = '0';
var rd_box_height = "600";
var rd_box_width = "890";

/* US States for TVG */
var rd_states_ = ['CA','FL','ID','IL','KY','LA','MD','MA','MT','NH','NY','ND','OH','OR','PA','VA','WA','WY'];

var rd_states_select_ =['AL','AR','CT','DE','IA','KS','OK','RI','SD','TN','VT','WV','WI']
