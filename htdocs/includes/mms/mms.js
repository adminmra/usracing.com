function tb_remove_ext() {
    eval_script(mark_read_url);
}

function display_msg() {
    if (msg_url != null && msg_url != "") {
        imgLoader = new Image(); // preload image
	    imgLoader.src = tb_pathToImage;
        msg_url += msg_url_params;
        var tb_remove_tmp = tb_remove;
        tb_remove = function() {
            tb_remove_tmp();
            tb_remove_ext();
        };
        tb_show(tb_caption, msg_url, tb_imgGroup);
    }
}

$(document).ready(function(){
    setTimeout(display_msg,1500);
});