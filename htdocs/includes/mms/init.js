function getReqParamValue( reqParamName ) {
    reqParamName = reqParamName.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+reqParamName+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "";
    else
        return results[1];
}

function eval_script(script_src) {
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = script_src;
    document.getElementsByTagName("head")[0].appendChild(script);
    eval(script.innerHTML);
}

function loadMessage() {
	if (getReqParamValue('showMsg') == "yes") {
        eval_script(get_msg_url);
    }
}

var msg_url = "";
var tb_pathToImage = "/includes/mms/loadingAnimation.gif";
var get_msg_url = "https://ctr.gamingsystem.net/acctmgt/pl/postlogin.js?userAction=getMessageUrl";
var mark_read_url = "https://ctr.gamingsystem.net/acctmgt/pl/postlogin.js?userAction=markMessageAsRead";
var box_height = "400";
var box_width = "600";
var msg_url_params = "&TB_iframe=true&KeepThis=true&height=" + box_height + "&width=" + box_width;
var tb_caption = null;
var tb_imgGroup = false;
loadMessage();
$("#TB_iframeContent").attr({src: $("#TB_iframeContent").attr("src")});

