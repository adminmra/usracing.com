function redirect_msg() {
    if (rd_member==null) {
		if(rd_tmpRD==null || rd_tmpRD != rd_domainURL){
			/* Background Style */
			$('body').prepend('<div id="rdOverlay"></div>');
			$('#rdOverlay').css({
				width:'100%',
				height:'100%',
				position:'fixed',
				top:0,
				left:0,
				opacity:0.3,
				filter: 'alpha(opacity=30)',
				background:rd_background_color,
				zIndex:1000
			});
			
			/* Box Style */
			
			$('body').prepend('<div id="rdOverlayBox"></div>');
			var leftBox = ($(window).width() - rd_box_width)/2;
			var topBox = ($(window).height() - rd_box_height)/2;
			$('#rdOverlayBox').css({
				top:topBox + 'px',
				left:leftBox + 'px',
				position:'fixed',
				width:rd_box_width + 'px',
				height:rd_box_height+ 'px',
				border:rd_box_border_width+ 'px ' + 'solid ' + 	rd_box_border_color,
				background : rd_box_background_color,
				zIndex:1001
				
			});
			
			/* Load Step 1 */
			$(document).ready(function(){
				/*$('#rdOverlayBox').load("/includes/redirect/step1.html", function() {
  					setStep1();
				});*/
				$.ajax({
					url: "/includes/redirect/step1.html",
					success: function(data){
						$('#rdOverlayBox').html(data);
						setStep1();
					}
				});
			});			
			/* Redirect Step 1 */
			
			var setStep1 = function(){
				$('#rd_usresidents_submit').click(function(){
					var state = $('#rd_usresidents').val();
					var step2 = false;
					$.each(rd_states_,function(index,value){
						if(state==value)step2=true;
					});
					if(state !=""){
					setStep2(step2,state);	
					}else alert('Please select your state');
				});	
				$('#rd_intlresidents').click(function(){
					dissolveLayer();
				});
				$('#rd_loginsubmit').click(function(){
					var name_ = $('#rd_name').val();
					var pass_ = $('#rd_pass').val()
					if(name_!="" && pass_!=""){
						 setLogin({name:name_,pass:pass_})
					}
				});
				$('#rd_forgot').click(function(){
					forgotPassword();
				});
				$('#rd_forgotLink').click(function(){
					forgotPasswordLink();
				});
				
			};
			
			/* Redirect Step 2 */
			
			var setStep2 = function(step2,state_){
				if(step2){
					$.ajax({
						url: "/includes/redirect/step2-1.html",
						success: function(data){
							$('#rdOverlayBox').html(data);
							$('#rd_betnow').click(function(){
								var name_ = $('#rd_firstname').val();
								var email_ = $('#rd_email').val()
								if(name_!="" && email_!=""){
									var param = { name:name_ , email:email_, state:state_ };
									saveData(param);
								}
								redirect1();
							});
							
						}
					});	
				}else{
					var index = Math.floor(Math.random() * (3 - 2 + 1)) + 2;;
					var url="/includes/redirect/step2-"+index+".html"
					var redirectFunc = "redirect"+index;
					$.ajax({
						url: url,
						success: function(data){
							$('#rdOverlayBox').html(data);
							$('#rd_betnow').click(function(){
								var name_ = $('#rd_firstname').val();
								var email_ = $('#rd_email').val()
								if(name_!="" && email_!=""){
									var param = { name:name_ , email:email_, state:state_ };
									saveData(param);
								}
								//alert(redirectFunc,index);
								if(index==2)redirect2();
								else redirect3();
							});
						}
					});	
				}
				
			};
			
			/* 	Functions */
			
			var setLogin = function(param){
				var exdate=new Date();
				var $username = param.name;
				var $userpass = param.pass;
				Set_Cookie( rot13('UTMU'), rot13($username), 2, "/", "."+rd_domainURL, "" );
				Set_Cookie( rot13('UTMP'), rot13($userpass), 2, "/", "."+rd_domainURL, "" );
				Set_Cookie(rot13('CKTME'),exdate.getTime(), '', '/', "."+rd_domainURL, "" );
				Set_Cookie( rot13('TRACK'), rot13($username), 4320, "/", "."+rd_domainURL, "" );
				
				window.location = loginURL;
	
			}
			var forgotPassword = function(){
				dissolveLayer();	
				window.location = forgotURL;
			}
			var forgotPasswordLink = function(){
				dissolveLayer();	
				window.location = forgotURL;
			}
			
			var saveData = function(param){
				var url ="/includes/redirect/save.php" 
				$.ajax({
					  type: 'GET',
					  url: url,
					  data: param,
					  success: function(data){
						//alert(data)  
					  }
				});
			}
			
			var redirect1 = function(){
				
				var siteIndex=Math.floor(Math.random()*(rd_redirect1.length));
					//alert(rd_redirect1[siteIndex]+siteIndex);
					window.location = rd_redirect1[siteIndex];
			}
			
			var redirect2 = function(){
				var siteIndex=Math.floor(Math.random()*(rd_redirect2.length));
				//	alert(rd_redirect2[siteIndex]+siteIndex);
					window.location = rd_redirect2[siteIndex];
			}
			
			var redirect3 = function(){
				var siteIndex=Math.floor(Math.random()*(rd_redirect3.length));
					//alert(rd_redirect3[siteIndex]+siteIndex);
					window.location = rd_redirect3[siteIndex];
			}
			
			/* Dissolve */
			
			var dissolveLayer = function(){

				$('#rdOverlay').remove();
				$('#rdOverlayBox').remove();
				Set_Cookie( 'REDIRECT', rd_domainURL, rd_defaultTimeOut, "/", "." + rd_domainURL, "" );

			}
		}
	}
}

$(document).ready(function(){
    	setTimeout(redirect_msg,1500);
});
