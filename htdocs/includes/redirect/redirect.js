/* 	Functions */
var state_ = null;
var index_ = null;
var twin_ = null;

var loadPage = function(url_,div_){
	$.ajax({
		url: url_,
		success: function(data){
			$(div_).html(data);
		}
	});
}

var setLogin = function(param){
	var exdate=new Date();
	var $username = param.name;
	var $userpass = param.pass;
	Set_Cookie( rot13('UTMU'), rot13($username), 2, "/", "."+rd_domainURL, "" );
	Set_Cookie( rot13('UTMP'), rot13($userpass), 2, "/", "."+rd_domainURL, "" );
	Set_Cookie(rot13('CKTME'),exdate.getTime(), '', '/', "."+rd_domainURL, "" );
	Set_Cookie( rot13('TRACK'), rot13($username), 4320, "/", "."+rd_domainURL, "" );
	
	window.location = loginURL;

}
var forgotPassword = function(){
	dissolveLayer();	
	window.location = forgotURL;
}
var forgotPasswordLink = function(){
	dissolveLayer();	
	window.location = forgotURL;
}


var rd_redirect=function(){
	if(index_!=null){
		var siteIndex=Math.floor(Math.random()*(rd_special[index_]['affliate'].length));
		var param = { site:rd_special[index_]['affliate'][siteIndex],redirect:index_ };
	}
	else{
		if(twin_!=null)
		{
			var siteIndex=Math.floor(Math.random()*(rd_select['affliate'].length));
			var param = { site:rd_select['affliate'][siteIndex],redirect:1};
		}
		else
		{	var siteIndex=Math.floor(Math.random()*(rd_general['affliate'].length));
			var param = { site:rd_general['affliate'][siteIndex],redirect:1};
		}
	}
	saveCSV(param);
}	
var saveData = function(param){
	var url="/includes/redirect/save.php";
	if(param["email"] !=""){
	 $.ajax({                                         
							 url: url,
							 data:param,
							 method:'GET',
							 timeout:2000,
							 success: function(data){
								//rd_redirect();
							  },
							 error: function(){
								//rd_redirect();
							 }
							 });
	}else{
		//rd_redirect();
	}
}
var saveDefault = function(param){
	var url="/includes/redirect/save.php";
	if(param["name"] !="" && param["email"] !=""){
	 $.ajax({                                         
							 url: url,
							 data:param,
							 method:'GET',
							 timeout:2000,
							 success: function(data){
								//dissolveLayer();	
							  },
							 error: function(){
								//dissolveLayer();	
							 }
							 });
	}else{
		//dissolveLayer();	
	}
}
var saveCSV = function(param){
	var url="/includes/redirect/fwrite.php";
	if(param["site"] !="" ){
	 $.ajax({                                         
							 url: url,
							 data:param,
							 method:'GET',
							 timeout:2000,
							 success: function(data){
								 window.location = param["site"];
							  },
							 error: function(){
								 window.location = param["site"];
							 }
							 });
	}else{
	window.location = param["site"];
	}
}

/* Dissolve */

var dissolveLayer = function(){

	$('#rdOverlay').remove();
	$('#rdOverlayBox').remove();
	Set_Cookie( 'REDIRECT', rd_domainURL, rd_defaultTimeOut, "/", "." + rd_domainURL, "" );

}
			
/* Redirect Step 2 */

var setStep2 = function(step2,state){
	state_ = state;
	if(step2 =='twin'){
                index_ = null;
                twin_=1;
                loadPage(rd_select.html,'#rdOverlayBox');
        }
	else if(step2 =='trv'){
		index_ = null;
		twin_ = null;
		loadPage(rd_general.html,'#rdOverlayBox');	
	}else{
		var tmpArray = new Array();
		for(var i in rd_special){
			if(rd_special[i].enable)tmpArray.push(i)	
		}
		if(tmpArray.length>0){
			var index = Math.floor(Math.random() * tmpArray.length)
			index_ = tmpArray[index];
			loadPage(rd_special[index_].html,'#rdOverlayBox');	
		}else{
			var tmpArrayFail = new Array();
			for(var i in rd_special_fail){
				if(rd_special_fail[i].enable)tmpArrayFail.push(i)	
			}
			if(tmpArrayFail.length>0){
				var indexFail = Math.floor(Math.random() * tmpArrayFail.length)
				index_ = tmpArrayFail[indexFail];
				loadPage(rd_special_fail[index_].html,'#rdOverlayBox');	
			}else{
				if(rd_default.enable){
					loadPage(rd_default.html,'#rdOverlayBox');	
				}else{
					dissolveLayer();
				}
			}
		}
	}
};

			var setStep2N = function(step2,param){
				state_ = param["state"];
					if(step2 =='twin'){
									index_ = null;
									twin_=1;
									saveData(param);		
							}
						else if(step2 =='trv'){
							index_ = null;
							twin_ = null;
							saveData(param);		
						}else{
								var tmpArray = new Array();
								for(var i in rd_special){
									if(rd_special[i].enable)tmpArray.push(i)	
								}
								if(tmpArray.length>0){
									var index = Math.floor(Math.random() * tmpArray.length)
									index_ = tmpArray[index];
									rd_special_=1;
									saveData(param);			
								}else{
										var tmpArrayFail = new Array();
										for(var i in rd_special_fail){
											if(rd_special_fail[i].enable)tmpArrayFail.push(i)	
										}
										if(tmpArrayFail.length>0){
											var indexFail = Math.floor(Math.random() * tmpArrayFail.length)
											index_ = tmpArrayFail[indexFail];
											rd_special_fail_=1;
											saveData(param);		
										}else{
											if(rd_default.enable){
												rd_fail_other_us=1
												saveData(param);	
												//alert('we can\'t process right now from your area. \n Please Try back Later.');		
											}else{
												rd_fail_other_us=1;
												saveData(param);	
												//saveData(param);		
												//alert('we can\'t process right now from your area. \n Please Try back Later.');
											}
										}
								}
						}
						///save data //
					//saveData(param);							
						//}					
				}
			
			var setStep2NS = function(step2,param){

									saveData(param);		
	
				}

function redirect_msg() {

			/* Background Style */
			
			$('body').prepend('<div id="rdOverlay"></div>');
			$('#rdOverlay').css({
				width:'100%',
				height:'100%',
				position:'fixed',
				top:0,
				left:0,
				opacity:0.7,
				filter: 'alpha(opacity=70)',
				background:'#000000',
				zIndex:1000
			});
			
			/* Box Style */
			
			$('body').prepend('<div id="rdOverlayBox"></div>');
			var leftBox = ($(window).width() - rd_box_width)/2;
			var topBox = ($(window).height() - rd_box_height)/2;
			$('#rdOverlayBox').css({
				top:topBox + 'px',
				left:leftBox + 'px',
				position:'fixed',
				width:rd_box_width + 'px',
				height:rd_box_height+ 'px',
				border:'0',
				background:'transparent',
				zIndex:1001
				
			});

			/* Load Step 1 */
			
			loadPage("/includes/redirect/step1.html",'#rdOverlayBox');
			
		
}

$(document).ready(function(){
    	redirect_msg();
});
