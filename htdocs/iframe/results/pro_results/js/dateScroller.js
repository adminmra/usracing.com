var positionLeft;
var windowWidth;
var sliderWidth;
var activeCenter;

var scroll = {
    init: function(dateWrap){
    $(dateWrap).mCustomScrollbar({
        horizontalScroll:true,
        scrollButtons:{
            enable:true
        },
        callbacks:{
            onScroll:function(){
                $(".mCSB_buttonRight").removeClass("disabled");
                $(".mCSB_buttonLeft").removeClass("disabled");
            },
            onTotalScroll:function(){
                $(".mCSB_buttonRight").addClass("disabled");
            },
            onTotalScrollBack:function(){
                $(".mCSB_buttonLeft").addClass("disabled");
            },
            onTotalScrollOffset: 20,
            onTotalScrollBackOffset: 20
        }
    });

    scroll.update(dateWrap);

    windowWidth = $(window).width();
    if(windowWidth < 768){
        positionLeft = $(".activeDate").position();
        sliderWidth = $(dateWrap).width();
        activeCenter = positionLeft.left - 20;
        $(dateWrap).mCustomScrollbar("scrollTo", activeCenter,{
            scrollInertia:0
        });
    };

    $( ".date" ).each(function() {
        var dateTxt = $(this).text();
        if ((dateTxt >= 10) && (dateTxt < 20)){
            if(dateTxt == 11){
                $(this).addClass("pushDateEleven");
            }else{
                $(this).addClass("pullDate");
            }
        }else if ((dateTxt >= 20) && (dateTxt <= 31)){
            if((dateTxt == 21) || (dateTxt == 31)){
                $(this).addClass("pushDateOne");
            }else{
                $(this).addClass("pushDate");
            }
        }
    });

    $(window).resize(function () {
        setTimeout(function(){
            windowWidth = $(window).width();
            sliderWidth = $(dateWrap).width();
            positionLeft = $(".activeDate").position();
            if(windowWidth >= 768){
                if(positionLeft !== undefined){
                    activeCenter = positionLeft.left - ((sliderWidth / 2) - 20);
                    $(dateWrap).mCustomScrollbar("scrollTo", activeCenter);
                }
            }else if(windowWidth  < 767){
                if(positionLeft !== undefined){
                    activeCenter = positionLeft.left - 20;
                    $(dateWrap).mCustomScrollbar("scrollTo", activeCenter);
                }
            }
        },500);
    });
    },

    update: function(dateWrap) {
        positionLeft = $(".activeDate").position();
        sliderWidth = $(dateWrap).width();
        if(positionLeft !== undefined){
        activeCenter = positionLeft.left - ((sliderWidth / 2) - 20);
        $(dateWrap).mCustomScrollbar("scrollTo", activeCenter,{
            scrollInertia:0
        });
        }
    },

    destroy: function(dateWrap) {
        $(dateWrap).mCustomScrollbar("destroy");
    }
};
