var DRFPRO = DRFPRO || {};
DRFPRO.appConst = DRFPRO.appConst || {};
DRFPRO.getValue = DRFPRO.getValue || null;
DRFPRO.getValueBets = DRFPRO.getValueBets || null;
$(window).on('scroll', function() {
    DRFPRO.appConst.position = $(document).scrollTop();
});

$(document).ready(function() {
    $('#home').addClass('active');
    _placeHolderIE();
    _userNav();
    _searchBox();
    _hideRaceReplayOnDevice();
    _betsIframeTouch();

    /*bet now btn set default event inside of accordion click*/
    $('body').on("click", ".btnEntriesRace.entriesBtn", function(event) {
        event.stopPropagation();
    });

    $('body').on("click", ".favouritesMsg .icon-favClose", function() {
        var cookieName = 'favClose',
            days = 20 * 356 ,
            cookieValue = true;

        $(this).parents('.favouritesMsg').hide();
        DRFPRO.utils.setCookie(cookieName, cookieValue, days);
        DRFPRO.favClose = true;
    });

    $(".formWrap #username").keypress(function() {
        $(".formWrap").find(".formErrorWrap").css('display', 'none');
    });
    $(".formWrap #password").keypress(function() {
        $(".formWrap").find(".formErrorWrap").css('display', 'none');
    });
});

var _placeHolderIE = function() {
    $('input[type="text"],input[type="email"],input[type="password"],textarea').placeholder_labels();
};
var _userNav = function() {
    $(".userName").click(function() {
        if($(".userNav").hasClass("userNavActive")) {
            $(".userNav").removeClass("userNavActive");
        } else {
            $(".userNav").addClass("userNavActive");
        }
        return false;
    });
    /*onbody click close DRF.com sub menu*/
    $('body').click(function() {
        $(".userNav").removeClass("userNavActive");
    });
};


/* Search onclick open and close */
var _searchBox = function() {
    function close() {
        $('.menuNav .subMenu').removeClass('subMenuActive');
        $('.searchBox').animate({
            height: '0'
        }, function(){
            $('.searchWrap').removeClass('searchOpen');
        });
    }

    function open() {
        $('.menuNav .subMenu').removeClass('subMenuActive');
        $('.searchWrap').addClass('searchOpen');
        $('.searchBox').animate({
            height : '42px'
        });
    }

    $('.icon-search').click(function() {
        if($('.searchWrap').hasClass('searchOpen')) {
            close();
        } else {
            open();
        }
    });
    $('body').click(function() {
        if($('.searchWrap').hasClass('searchOpen')) {
            $('.searchBox').animate({
                height: '0'
            }, function(){
                $('.searchWrap').removeClass('searchOpen');
            });
        }
    });
    $('.searchWrap').click(function(event) {
        event.stopPropagation();
    });
};
var _homePopup = function() {
    var overlay = $('<div class="popupOverlay"></div>');
    overlay.show();
    overlay.prependTo(document.body);
    $('.popup').show();
    $('.popupClose').click(function() {
        $('.popup').hide();
        overlay.prependTo(document.body).remove();
        return false;
    });
};

DRFPRO.showActiveLiveOdds = function() {
    var windowWidth = $(window).width();
    if(windowWidth <= 1024) {
        var tabLiveOdds = $('.oddsDetailsTab .tabLiveOdds');
        if(tabLiveOdds.next().hasClass('activeTab')) {
            tabLiveOdds.next().removeClass('activeTab');
            tabLiveOdds.addClass('activeTab');
            $('.tabContentWrap .tabContent').hide();
            $('.tabContentWrap .tabLiveOdds').next().removeClass("activeContent");
            $('.tabContentWrap .tabLiveOdds').addClass("activeContent");
            $('.tabContentWrap .tabLiveOdds').show();
        }
    }
}
$(window).resize(function() {
    var windowWidth = $(window).width();
    if(windowWidth > 767) {
        if($('#mMenu').is(':visible')) {
            $('.responsiveButton').trigger('click');
        }
    }
    if(windowWidth > 1024) {
        var tabLiveOdds = $('.oddsDetailsTab .tabLiveOdds');
        if(tabLiveOdds.hasClass('activeTab')) {
            tabLiveOdds.next().addClass('activeTab');
            tabLiveOdds.removeClass('activeTab');
            $('.tabContentWrap .tabContent').hide();
            $('.tabContentWrap .tabLiveOdds').next().show();
            var currentTab = tabLiveOdds.next().find('a').attr('href');
            DRFPRO.liveodds.activeTab = currentTab;
        }
    }
});

DRFPRO.showHideTrack = function(e, el, collectionLength) {

    var clickedEl = $(e.currentTarget);
    if(clickedEl.attr("id") == "myTrack") {
        DRFPRO.setMyTrack = true;
        if(DRFPRO.trackCount > 0) {

            clickedEl.addClass('activeTrack');
            $('ul.favouritesMsg').hide();
            $('#allTrack').removeClass('activeTrack');
            $(el).find('.high').show();
            $(el).find('.low').hide();

        } else {
            return false;
        }

    } else {
        DRFPRO.setMyTrack = false;
        if(collectionLength == 0) {
            $(".favouritesMsg").hide();
        }
        else {
            clickedEl.addClass('activeTrack');
            $('#myTrack').removeClass('activeTrack');
            $(el).find('li').show();
            if(DRFPRO.trackCount > 0) {
                $('#myTrack').removeClass('disabled');
                $('ul.favouritesMsg').hide();
            }
            else {
                $('#myTrack').addClass('disabled');
                if(DRFPRO.favClose == false && !DRFPRO.utils.readCookie('favClose') == true) {
                    $("ul.favouritesMsg").show();
                } else {
                    $("ul.favouritesMsg").hide();
                }

            }
        }

    }
};
DRFPRO.boldText = function() {
    var len = $(".entriesRaceDisc").length;
    for(i = 0; i < len; i++) {
        var str = $(".entriesRaceDisc").eq(i).text();
        var res = str.split("|");
        res[(res.length) - 1] = res[(res.length) - 1] + "</span>";
        var purse = res[(res.length) - 1].split("$");
        purse[1] = "<span>$" + purse[1] + "</span>";
        res[(res.length) - 1] = purse.join("");
        res[(res.length) - 1].replace("Purse:", "Purse:<span>")
        res[(res.length) - 2] = "<span class='raceDisctxtUp'>" + res[(res.length) - 2];
        $(".entriesRaceDisc").eq(i).html(res.join("|"));
    }

};
/*----------------------------function for adding suffix after numbers----------------------*/

DRFPRO.enableMytrack = function(parent) {
    if(DRFPRO.setMyTrack == true) {
        $('#myTrack').addClass('activeTrack').show();
        $('#allTrack').removeClass('activeTrack').show();
    }
    var selectedView = $('li.activeTrack');
    if(selectedView.attr('id') == 'myTrack') {
        if((DRFPRO.trackCount) == 0) {
            if(DRFPRO.favClose == false && !DRFPRO.readCookie('favClose') == true) {
                $("ul.favouritesMsg").show();
            } else {
                $("ul.favouritesMsg").hide();
            }
            DRFPRO.setMyTrack = false;
            $('#myTrack').addClass('disabled');
            $('#allTrack').addClass('activeTrack');
            $('#myTrack').removeClass('activeTrack');
        } else {
            $('#myTrack').removeClass('disabled');
            parent.$('.high').show();
            parent.$('.low').hide();
        }

    } else {
        if((DRFPRO.trackCount) == 0) {
            $('#myTrack').addClass('disabled');
            if(DRFPRO.favClose == false && !DRFPRO.utils.readCookie('favClose') == true) {
                $("ul.favouritesMsg").show();
            } else {
                $("ul.favouritesMsg").hide();
            }
        } else {
            $('#myTrack').removeClass('disabled');
        }

    }
};
/*function displays date in day, month date year format */
DRFPRO.displayDate = function(unformattedDate) {
    var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var monthNames = [ "January", "February", "March", "April", "May", "Jun", "July", "August", "September", "October",
        "November", "December" ];
    unformattedDate = unformattedDate.split(/\D/);
    var date = new Date(unformattedDate[2], --unformattedDate[0], unformattedDate[1]);
    return dayNames[date.getDay()] + ', ' + monthNames[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
};

DRFPRO.dateType = function(selectedDateString, currentDateString) {
    if(typeof selectedDateString !== "undefined" && typeof currentDateString !== "undefined") {
        selectedDateString = selectedDateString.split(/\D/);
        currentDateString = currentDateString.split(/\D/);

        selectedDate = new Date(selectedDateString[2], --selectedDateString[0], selectedDateString[1]);
        currentDate = new Date(currentDateString[2], --currentDateString[0], currentDateString[1]);

        if(selectedDate > currentDate) {
            return "Future";
        } else if(selectedDate < currentDate) {
            return "Previous"
        } else {
            return "Today\'s";
        }
    }

};

DRFPRO.dateDifference = function(selectedDateString, currentDateString) {
    if(typeof selectedDateString !== "undefined" && typeof currentDateString !== "undefined") {
        selectedDateString = selectedDateString.split(/\D/);
        currentDateString = currentDateString.split(/\D/);

        selectedDate = new Date(selectedDateString[2], --selectedDateString[0], selectedDateString[1]);
        currentDate = new Date(currentDateString[2], --currentDateString[0], currentDateString[1]);

        return currentDate - selectedDate;
    }
};

DRFPRO.setActiveDate = function(date,dateWrap) {
    if(typeof date !== "undefined") {
        var dateString = date.split(/\D/),
            isActiveDate;
        var selector1 = dateString[0] + '-' + dateString[1].slice(1, 2) + '-' + dateString[2];
        var selector2 = dateString[0].slice(1, 2) + '-' + dateString[1] + '-' + dateString[2];
        var selector3 = dateString[0].slice(1, 2) + '-' + dateString[1].slice(1, 2) + '-' + dateString[2];
        $('.activeDate').removeClass('activeDate');
        isActiveDate = false;
        if($(" #" + date).length > 0) {
            $(" #" + date).parent('li').addClass('activeDate');
            isActiveDate = true;
        } else if($(" #" + selector1).length > 0) {
            $(" #" + selector1).parent('li').addClass('activeDate');
            isActiveDate = true;
        } else if($(" #" + selector2).length > 0) {
            $(" #" + selector2).parent('li').addClass('activeDate');
            isActiveDate = true;
        } else if($(" #" + selector3).length > 0) {
            $(" #" + selector3).parent('li').addClass('activeDate');
            isActiveDate = true;
        }
        if(isActiveDate) {
            scroll.update(dateWrap);
        }
    }

};
DRFPRO.showPageNotFound = function() {
    /*
    var html = [
        '<div class="pageNotFound">',
        '<div class="error404">',
        '<span class="errorNo no4">4</span><span class="errorNo no0">0</span><span class="errorNo no4">4</span>',
        '</div>',
        '<h2>This page just SCRATCHED!</h2>',
        '<span>The page you are looking for might have been removed,',
        'had its URL changed, or is temporarily unavailable.</span>',
        '<div class="btnBack">',
        '<a href="javascript:history.go(-1)">Go Back!</a>',
        '</div>',
        '</div>'
    ].join('');
    $('.pageData').append(html);
    $('.pageData').addClass('pageDataFull').removeClass('pageData');
    $('.pageDataFull').parents('.contentWrap').addClass('errorWrap');

    $('.loading').fadeOut(550);
    */
};

DRFPRO.removePageNotFound = function() {

    $('.pageDataFull').find('.pageNotFound').remove();
    $('.pageDataFull').parents('.contentWrap').removeClass('errorWrap');
    $('.pageDataFull').addClass('pageData').removeClass('pageDataFull');

};

DRFPRO.showLocalDataStoreNotEnabled = function() {
    var html = [
        '<div class="noScript">',
        '<h2><span class="drfPro"><span>DRF</span> PRO</span> is best experienced with cookies Enabled.</h2>',
        '<span class="refreshPage">(Refresh the page once you enable the cookies)</span>'

    ].join('');
    $('.pageData').append(html);
    $('.pageData').addClass('pageDataFull').removeClass('pageData');
    $('.pageDataFull').parents('.contentWrap').addClass('errorWrap');

    $('.loading').fadeOut(550);

};

DRFPRO.startLoader = function() {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    $('.loading').show();
};

DRFPRO.triggerLink = function(href) {
    var roots = ['/entries', '/results', '/live_odds'];
    _.each(roots, function(root) {
        href = href.replace(root, '');
    })

    url = href.replace(/^\//, '').replace('\#\!\/', '');
    // Instruct Backbone to trigger routing events
    app.navigate(url, { trigger : true });
};

DRFPRO.scrachIndication = function(race) {

    scratchIndicators = ['E', 'G', 'O', 'S', 'T', 'V', 'Y'];
    horseScratchIndicator = race.scratchIndicator;

    race['scratchReason'] = horseScratchIndicator;

    if($.inArray(horseScratchIndicator, scratchIndicators) >= 0) {

        if(horseScratchIndicator !== 'Y') {
            race['scratchReason'] = horseScratchIndicator;
        } else {
            race['scratchReason'] = '';
        }
        race['scratchIndicator'] = 'Y';
    }
    return race;
},

    DRFPRO.updateAds = function() {
	  if(typeof googletag != "undefined") {
		googletag.cmd.push(function() {
			googletag.pubads().refresh();
		});
	  }
    };

/*
 Globally capture clicks. If they are internal and not in the pass
 through list, route them through Backbone's navigate method.
 */

$(document).on("click", "a[href^='/']", function(event) {

    if(!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
        event.preventDefault();
        // Trigger the route change
        var href = $(event.currentTarget).attr('href');

// chain 'or's for other black list routes
//        passThrough = href.indexOf('#pools') >= 0

// Allow shift+click for new tabs, etc. !passThrough &&
        if(!event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey) {
            event.preventDefault();
            // Remove leading slashes and hash bangs (backward compatablility)

            DRFPRO.triggerLink(href);
        }

    }
    return false;
});


DRFPRO.minBoxAmount = function(racePool, wager) {
    var minimumBoxAmount = "";
    _.each(racePool, function(pool) {
        if(pool.poolName == wager) {

            if(pool.minimumBoxAmount == null || pool.minimumBoxAmount == "") {
                minimumBoxAmount = pool.minimumWheelAmount;
            } else {
                minimumBoxAmount = pool.minimumBoxAmount;
            }
        }
    });
    if(minimumBoxAmount == "") {
        if(_.contains(['PICK 3', 'PICK 4', 'PICK 5'], wager) === true) {
            minimumBoxAmount = 1;
        }
        if(_.contains(['EXACTA', 'PICK 6', 'DAILY DOUBLE'], wager) === true) {
            minimumBoxAmount = 2;
        }
    }
    return minimumBoxAmount;
},
    DRFPRO.defaultBaseBet = function(defaultBaseBetAmount, key) {
        var baseBetAmount = "";
        _.each(defaultBaseBetAmount, function(baseBet) {
            if(baseBet.wagerType == key) {
                baseBetAmount = baseBet.baseBetAmount;
            }

        });
        return baseBetAmount;
    },

    DRFPRO.horizontalWager = function(wagerType) {
        var isWager = "";
        if(_.contains(['Daily Double', 'Grand Slam', 'Pick 3', 'Pick 4', 'Pick 5' , 'Pick 6', 'Pick 7'], wagerType) === true) {
            isWager = true;
        }
        return isWager;

    }

DRFPRO.payoffBaseAmount = function(wagerName, wagerType) {

    var isBaseAmount = "";
    _.each(wagerType, function(wager) {
        if(wager.wagerDescription === wagerName) {
            isBaseAmount = wager.baseAmount;
        }

    });

    return isBaseAmount;

}
DRFPRO.supportsHtmlStorage = function() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch(e) {
        return false;
    }
}
DRFPRO.encrypt = function(plainText) {
    var iv = "00000000000000000000000000000000";
    var salt = "00000000000000000000000000000000";
    var keySize = 128;
    var iterationCount = 10000;
    var passPhrase = "drfAuth!@#123456";
    var aesUtil = new AesUtil(keySize, iterationCount);
    return aesUtil.encrypt(salt, iv, passPhrase, plainText);
}

$(document).on('keypress', '#loginForm ul  input', function(event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {
        $('#userLogin').trigger('click');
    }
});

DRFPRO.activeCloserLookHorses = function(horse, closerLooksHorse, tabId) {
    if(closerLooksHorse["'" + tabId + "'"] !== undefined) {
        if(closerLooksHorse["'" + tabId + "'"][horse] !== undefined) {
            return true;
        } else {
            return false;
        }
    }


},
    DRFPRO.checkUserAgent = function() {

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        if(userAgent.match(/iPhone/i)) {
            return 'iOS';

        }
        else if(userAgent.match(/Android/i)) {

            return 'Android';
        }else{
            return 'unknown';
        }
    },
    DRFPRO.getHeaderIframeValue = function(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null

    }

DRFPRO.getValue = DRFPRO.getHeaderIframeValue('header');
DRFPRO.getValueBets = DRFPRO.getHeaderIframeValue('bets');

DRFPRO.ieDetector = function() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");
    if(msie !== -1) {
        return "IE";
    } else {
        return"Other";
    }
}
DRFPRO.jwplayerPost = function(element,link,poster_image){
    jwplayer(element).setup({
        'flashplayer': DRFPRO.utils.getBaseURL() +"/js/vendor/jwplayer/jwplayer.flash.swf",
        'file': link,
        'autostart': 'false',
        'width': "100%",
        'image':poster_image,
        'aspectratio': "12:5"
    });

},
    DRFPRO.checkUserAgent = function() {

        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        if(userAgent.match(/iPhone/i)) {
            return 'iOS';

        }
        else if(userAgent.match(/Android/i)) {

            return 'Android';
        }
    },
    DRFPRO.getHeaderIframeValue = function(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null

    }

DRFPRO.getValue = DRFPRO.getHeaderIframeValue('header');

DRFPRO.ieDetector = function() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");
    if(msie !== -1) {
        return "IE";
    } else {
        return"Other";
    }
}
DRFPRO.jwplayerPost = function(element,link,poster_image){
        jwplayer(element).setup({
            'flashplayer': DRFPRO.utils.getBaseURL() +"/js/vendor/jwplayer/jwplayer.flash.swf",
            'file': link,
            'autostart': 'false',
            'width': "100%",
            'image':poster_image,
            'aspectratio': "12:5"
        });

}
DRFPRO.timeFormat = function(timestamp){
    var dayTimeExtension;
    var dateStamp = new Date(timestamp);
    if (dateStamp.getHours() > 12) {
        dayTimeExtension = "PM";
    } else {
        dayTimeExtension = "AM";
    }
    return dayTimeExtension;
}
/* Race replayes video link generation */
DRFPRO.showRaceReplayVideo = function(attr,platform) {
    var PRIVATE_KEY = 'dr6lg96jsd4h7k1n6d745k6fg',
        desktopReplay_URL = 'http://replays.robertsstream.com/racereplays/replaysflash.php?cust=DRF&stream=',
        mobileReplay_URL = 'http://replays.robertsstream.com/racereplays/replaysmobile.php?race=',
        date = new Date(),
        UTCTimeStamp = Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
            date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds()),
        hash = calcMD5(UTCTimeStamp + PRIVATE_KEY + attr),
        raceVideoUrl, win ,BASE_URL;

    if(platform=='desktop'){
        BASE_URL = desktopReplay_URL;
        return raceVideoUrl = BASE_URL + attr +'&t=' + UTCTimeStamp + '&h=' + hash;
    }else{
        BASE_URL = mobileReplay_URL;
        return raceVideoUrl = BASE_URL + attr +'&cust=DRF'+'&t=' + UTCTimeStamp + '&h=' + hash;
    }
}

DRFPRO.timeFormat = function(timestamp){
    var dayTimeExtension;
    var dateStamp = new Date(timestamp);
    if (dateStamp.getHours() > 12) {
        dayTimeExtension = "PM";
    } else {
        dayTimeExtension = "AM";
    }
    return dayTimeExtension;
}

DRFPRO.localPost = function(postTime){
	var postHour = postTime.substr(0, postTime.indexOf(':'));
    var postMin = postTime.substr((postTime.indexOf(":")+ 1),2);
    var postPM = "PM";
    switch(postHour) {
    case "1":
        postHour = 10;
        postPM = "AM"
        break;
    case "2":
    	postHour = 11;
        postPM = "AM"
        break;
    case "3":
    	postHour = 12;
        break;
    default:
    	postHour = postHour - 3;

    }
    var localPostTime = postHour+":"+postMin+" "+postPM;
    return localPostTime;
}

DRFPRO.playVideo = function(url) {
    if(url!=""){
        window.open(url, '_system');
    }
}

var _hideRaceReplayOnDevice = function() {
    var ua = navigator.userAgent;
    if((ua.match(/Android/i) || ua.match(/webOS/i) || ua.match(/iPhone/i) || ua.match(/iPad/i) || ua.match(/iPod/i) || ua.match(/BlackBerry/i) || ua.match(/Windows Phone/i))) {
        $('.pageData').addClass("touchWrap");
    }
};

var _betsIframeTouch = function() {
    var ua = navigator.userAgent;
    if((ua.match(/Android/i) || ua.match(/webOS/i) || ua.match(/iPhone/i) || ua.match(/iPad/i) || ua.match(/iPod/i) || ua.match(/BlackBerry/i) || ua.match(/Windows Phone/i))) {
        $('.drfBetsIframe').addClass("drfBetsIframeTouchWrap");
    }
};

DRFPRO.encryptUrl = function(e){
    var clickedEl = $(e.currentTarget);
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE");
    var msie = ua.indexOf("MSIE");
    var nonEncodedUrl  = clickedEl.attr('url');
    if(DRFPRO.getValue!=null || DRFPRO.getValueBets!=null) {

        if (msie !== -1) {
            clickedEl.attr('href','http://www.drf.com');
            clickedEl.attr('target','_blank');
        }else{
            clickedEl.attr('href',drfLogin+'?refferUrl='+encodeURIComponent(nonEncodedUrl));
        }
    }else{
        clickedEl.attr('href',drfLogin+'?refferUrl='+encodeURIComponent(nonEncodedUrl));
    }
    return this;

},
DRFPRO.drupalLiveIconClass = function(article){
     if(article.postType=='News' && article.isPremium==true){
         return "icon-drfPlus";
     } else if(article.postType=='Live' && article.isPremium==true) {
         return "icon-drfPlus";
     }/* else {
         return "icon-livePlus";
     }*/
}

var _hideRaceReplayOnDevice = function() {
    var ua = navigator.userAgent;
    if((ua.match(/Android/i) || ua.match(/webOS/i) || ua.match(/iPhone/i) || ua.match(/iPad/i) || ua.match(/iPod/i) || ua.match(/BlackBerry/i) || ua.match(/Windows Phone/i))) {
        $('.pageData').addClass("touchWrap");
    }
};

/*
 *gaRefreshAds :-  Function to refresh Google ads without full page load.
 */

function gaRefreshAds(obj) {
	var url = window.location.href;
	var track=obj.id;
	var country=obj.title;
	var track_id = country+"."+track;

	googletag.pubads().clearTargeting();
	googletag.pubads().setTargeting("APP","drfpro");
	googletag.pubads().setTargeting("URL",url);
	googletag.pubads().setTargeting("CATEGORY","thoroughbred");
	googletag.pubads().setTargeting("TRACK-ID",track_id);
	googletag.cmd.push(googletag.pubads().refresh());

	};

function gaRefreshCalendarAds(obj) {
	//googletag.cmd.push(googletag.pubads().refresh());
    return true;
	};

