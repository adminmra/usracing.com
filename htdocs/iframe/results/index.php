<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!--   <meta HTTP-EQUIV=”Pragma” CONTENT=”no-cache”>-->


<title>Results - Horse Racing | Daily Racing Form - DRF Pro - DRF.com | Aqueduct, Belmont Park,
        Calder Race Course, Churchill Downs, Del Mar, Golden Gate Fields, Gulfstream Park, Hawthorne, Keeneland,
        Monmouth Park, Oaklawn Park, Pimlico, Santa Anita, Saratoga, Tampa Bay Downs, Woodbine</title><meta name="description"
          content="Results for today's race card at Aqueduct, Belmont Park, Calder Race Course, Churchill Downs,
          Del Mar, Golden Gate Fields, Gulfstream Park, Hawthorne, Keeneland, Monmouth Park, Oaklawn Park, Pimlico,
          Santa Anita, Saratoga, Tampa Bay Downs, Woodbine">    <link rel="shortcut icon" href="https://pro.drf.com/images/favicon.ico" type="image/x-icon">
    <!-- Apple touch icons -->
    <link rel="apple-touch-icon" href="https://pro.drf.com/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="https://pro.drf.com/images/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://pro.drf.com/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://pro.drf.com/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://pro.drf.com/images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="57x57" href="https://pro.drf.com/images/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://pro.drf.com/images/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://pro.drf.com/images/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://pro.drf.com/images/apple-touch-icon-152x152.png">
    <!-- Windows 8 Tile Icons -->
    <meta name="msapplication-square70x70logo" content="https://pro.drf.com/images/smalltile.png" />
    <meta name="msapplication-square150x150logo" content="https://pro.drf.com/images/mediumtile.png" />
    <meta name="msapplication-wide310x150logo" content="https://pro.drf.com/images/widetile.png" />
    <meta name="msapplication-square310x310logo" content="https://pro.drf.com/images/largetile.png" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="keyword"
      content="DRF, thoroughbred racing, race program, race, racing, race tracks, racetracks, race entries,
          race results, horse racing, Daily Racing Form, jockeys, trainers, turf, owners, race results, horse,
          horses, race charts, crown, derby, breeders cup">
<meta name="author" content="">

<!-- styles -->
<link href="https://pro.drf.com/css/bootstrap.css" rel="stylesheet">
<link href="https://pro.drf.com/css/style.css" rel="stylesheet">
    <link href="https://pro.drf.com/css/jquery.mCustomScrollbar.css" rel="stylesheet">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>

<script src="https://pro.drf.com/js/vendor/html5shiv.js"></script>
<![endif]-->

<!-- javaScript -->
<script src="https://pro.drf.com/js/vendor/jquery.min.js"></script>
<script src="https://pro.drf.com/js/vendor/jquery.validate.js"></script>
<script src="https://pro.drf.com/js/vendor/jquery.mCustomScrollbar.js"></script>
<script src="https://pro.drf.com/js/vendor/placeholder-labels.js"></script>
<script src="https://pro.drf.com/js/vendor/vs.menu.js"></script>
<script src="https://pro.drf.com/js/vendor/underscore-min.js"></script>
<script src="https://pro.drf.com/js/vendor/backbone-min.js"></script>
<script src="https://pro.drf.com/js/vendor/headerMenus.js"></script>
<script src="https://pro.drf.com/js/app/util.js"></script>
<script type="text/javascript" src="https://pro.drf.com/js/vendor/jwplayer/jwplayer.js"></script>
<script type="text/javascript">jwplayer.key="uRQDKGwO2ODDoJ+ObZHxrcBZExULFnjEGf3PrQ==";</script>
<script src="//cdn.optimizely.com/js/536603184.js"></script>
<script type="text/javascript"> var drfLogin = "https:\/\/access.drf.com";</script>
</head>
<body class="drfBetsIframe">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KXK7B3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KXK7B3');</script>
<!-- End Google Tag Manager -->
<div class="loginDiv">

</div>
<div class="container">
    <!-- start Header -->
    <div class="headerWrap">
        <a class="mback hidden" href="https://pro.drf.com/entries"><i
                class="icon-mBack"></i></a>
            <script>
        $(".btnSubmit").on('click', function (e) {
            e.preventDefault();
            var clickedEl = $(e.currentTarget);
            var textVal = clickedEl.parent().children(':first-child').val();
            window.open("http://www.drf.com/search/apachesolr_search/" + textVal, "_blank");
        });

        $(document).on('keypress', '#searchForm', function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13') {
                $(".btnSubmit").trigger('click');
            }
        });
    </script>
    <!-- end Nav -->
    </div>
<!-- end Header -->    <!-- start Main Content -->
    <div class="contentWrap clearfix">
        <section class="pageData">
            <noscript>
                <div class="noScript">
                    <h2><span class="drfPro"><span>DRF</span> PRO</span> is best experienced with Javascript Enabled.
                    </h2>

                    <div class="btnBack">
                        <a href="http://www.enable-javascript.com/" target="_blank">How to enable Javascript</a>
                        <a href="">Reload</a>
                    </div>
                    <span class="refreshPage">(Click on reload once you enable the javascript)</span>
                </div>
            </noscript>
            <div class="loading" style="display: none;"></div>
            <div class="logoPrint"><img src="/images/logo.jpg" alt="DRF PRO"/>
            </div>
            <!-- start View Content-->
            <div id="DRFPROVClick" style="display:none"></div>
<div class="backButton hidden"><a class="calendarBack" href="/" onclick="gaRefreshBackButtonAds(this);"><i class="icon-arrowRight"></i>Back to Selection</a></div>
<div class = "dateWrap content">
    <ul id="calender">
    </ul>
</div>
<div id="entriesListingWrapper" class="entriesListWrap resultsWrap">
</div>
<div id="entriesDetailsWrapper" class='entriesDtlsWrap resultsWrap'></div>
<script type="text/javascript" src="https://pro.drf.com/js/dateScroller.js"></script>

<script type="text/javascript">
    var  today = "01-07-2019";
    var  currentTime = "13";
</script>
<script src="https://pro.drf.com/js/vendor/MD5.js"></script>
<script type="text/template" id="template-article-articleDetails">
        <li id="<%='article'+nugget.nid%>">
            <div class="newsLeft">
                <div class="imgAuthor">
                    <img src="<%=nugget.authorImage%>" alt="">
                </div>
                <!--<i class="<% var classValue= DRFPRO.drupalLiveIconClass(nugget); %><%=classValue%>"></i> ENTSRV-108 -->
            </div>
            <% if(nugget.postType=="News"){ %>
            <div class="newsCnt clearfix">
                <div class="newsAuthorDate clearfix">
                    <div class="newsAuthor">
                        <%=nugget.authorName+","+nugget.trackName+","+nugget.raceNumber%>
                        <%if(nugget.eventTitle!="" && nugget.eventTitle!=null) { %>
                                    <span><% if(nugget.eventUrl!=null && nugget.eventUrl!='') { %>
                                    <a class="articleEvent" url="<%=nugget.eventUrl%>"
                                       href="javascript:void(0)"><%='-'+nugget
                                        .eventTitle%></a><% }
                                        else {
                                        %>
                                    <span><%='- '+nugget.eventTitle%></span><% }%>
                                    </span><%}%>
                    </div>
                    <div class="newsDate">
                        <%=nugget.postDate%>
                    </div>
                </div>
                <h3><%=nugget.title%></h3>
                <div class="newsDis">
                    <div class="teaser"><% if(nugget.teaser!="") { %><%=nugget.teaser%><% } %></div>
                </div>
                 <a href="<%=nugget.articleUrl%>" target="_blank" >Read <span class="txtReadMore">More</span> <span
                         class="txtReadLess"
                    >Less</span></a>
            </div>
            <% } else if(isPremium==false || (isLoggedIn==true && isSubScribed==true)) { %>
            <div class="newsCnt clearfix">
                <div class="newsAuthorDate clearfix">
                    <div class="newsAuthor">
                       <%=nugget.authorName+","+nugget.trackName+","+nugget.raceNumber%>
                        <%if(nugget.eventTitle!="" && nugget.eventTitle!=null) { %>
                                    <span><% if(nugget.eventUrl!=null && nugget.eventUrl!='') { %>
                                    <a class="articleEvent" url="<%=nugget.eventUrl%>"
                                       href="javascript:void(0)"><%='-'+nugget
                                        .eventTitle%></a><% }
                                        else { %>
                                    <span><%='- '+nugget.eventTitle%></span><% }%>
                                    </span><%}%>
                    </div>
                    <div class="newsDate">
                   <%=nugget.postDate%>
                    </div>
                </div>
                <h3><%=nugget.title%></h3>

                <% if(nugget.photo!="" && nugget.photo!=null) {%>
                <div class="mediaWrap newsImg">
                    <img src="<%=nugget.photo%>" alt="">
                </div><% }  %>
                <% var os = DRFPRO.checkUserAgent(); if(nugget.videoLink!="" && nugget.videoLink!=null) { %>
                    <div class="videoWrapper">
                        <div id="<%=nugget.nid+'jwVideo'%>" class="jwVideo">
                        </div>
                     <% var vimioVideo = "vimeo" ,  nuggetVideoLink = nugget.videoLink;
                        if(nuggetVideoLink.indexOf(vimioVideo) > -1){ %>
                        <div id="<%=nugget.nid+'vimeoVideo'%>" class="vimeoVideo">
                            <iframe width="100%" height="100%" frameborder="0" allowfullscreen="" mozallowfullscreen=""
                                    webkitallowfullscreen=""  src="#"></iframe>
                        </div>
                        <% } %>
                    </div>
                <% } %>
                <div class="newsDis">
                    <div
                            class="teaser <%  if(isLoggedIn && !isSubScribed && isPremium ||(!isLoggedIn && isPremium)){ %> showTeaser<%}%>"><% if(nugget.teaser!="") { %><%=nugget.teaser%><% } %></div>
                    <div  class="newsDisMore">
                       <%=nugget.body%>
                    </div>
                </div>
                <%  if(nugget.body!=""  &&  nugget.body.length > 90) { %>
                <a href="#" class="btnReadMore">Read <span class="txtReadMore">More</span> <span class="txtReadLess"
                        >Less</span></a>
                <%  } %>
            </div>
            <% } else { %>
                        <div class="newsCnt clearfix">
                            <div class="newsAuthorDate clearfix">
                                <div class="newsAuthor">
                                    <%=nugget.authorName+","+nugget.trackName+","+nugget.raceNumber%>
                                    <%if(nugget.eventTitle!="" && nugget.eventTitle!=null) { %>
                                    <span><% if(nugget.eventUrl!=null && nugget.eventUrl!='') { %>
                                    <a class="articleEvent" url="<%=nugget.eventUrl%>"
                                       href="javascript:void(0)"><%='-'+nugget.eventTitle%></a><% }
                                        else { %>
                                    <span><%='- '+nugget.eventTitle%></span><% }%>
                                    </span><%}%>
                                </div>
                                <div class="newsDate">
                                    <%=nugget.postDate%>
                                </div>
                            </div>
                            <h3><%=nugget.title%></h3>
                            <div class="newsDis">
                                <div class="teaser <%  if(isLoggedIn && !isSubScribed && isPremium ||(!isLoggedIn && isPremium)){ %>showTeaser<%}%>"><% if(nugget.teaser!="") {
                                    %><%=nugget.teaser%><% }  %></div>
                                <div  class="newsDisMore">
                                    <div  class="premiumScreenCheck<%=nugget.nid%>">
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="btnReadMore">Read <span class="txtReadMore">More</span> <span class="txtReadLess"
                                    >Less</span></a>
                        </div>
            <% } %>
    </li>
</script>
<script type="text/template" id="template-article-textHeader">
    <div class="entriesDtlsHead clearfix">
        <h2><span><%=DRFPRO.displayDate(selectedDate)%></span>
        </h2>
    </div>
</script>
<script type="text/template" id="template-article-trackName">
    <div class="entriesHeader articleHeader clearfix">
        <div class="entriesDtlsLeft">
            <h4>
                <a href="#" class=<%if(priority === "high") {%>"icon-favorite" <%}else {%> "icon-add-favorite" <%}%>
                id="favorite"></a>

                <%= weather.trackName %>
            </h4>

            <div class="entriesDtlsWeather">
                <%if(weather.forecastHigh !== -999) {%>
                <i class="iconWeather"><img src="/images/iconW<%=weather.forecastIconId%>.png" alt=""/></i>
                <span class="mobileHide"><%=weather.forecastWeatherDescription%> </span>
                <%if(weather.forecastPrecipitation !== "") { %><span class="valPrecip">
                    Precip: <span><%=weather.forecastPrecipitation%>%</span></span><% } %>
                High:
                <%if(weather.forecastHigh !== -999) { %><span> <%=weather.forecastHigh%></span><span
                    class="valDegree">&deg;F</span>
                <% } else {%> <span class="noWeather">-</span> <%}%><span class="divider">|</span> Low:
                <%if(weather.forecastLow !== -999) { %><span> <%=weather.forecastLow%></span><span
                    class="valDegree">&deg;F</span>
                <% } else {%> <span class="noWeather">-</span> <%}%>
                <%}%>
            </div>
        </div>
        <% if(DRFPRO.getValue!=null && weather.trackId === "DMR"){ %>
				<ul class="btnEntriesRace">
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=weather.trackId%>&CY=<%=weather.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=weather.trackId%>&country=<%=weather.country%>&date=<%=dateForPP%>&session=<%=raceKey.dayEvening%>&race=<%=raceKey.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
 			<% }else{ %>
            	<ul class="btnEntriesRace">
                	<% if(DRFPRO.dateType(this.selectedDate,this.currentDate) === "Today\'s"){ %>
                	<li class="btnBetNow"><a class="drfProBetNow" href="https://bets.drf.com/" id=""
                     	target="_blank">BET NOW!</a></li>
                	<% }else{ %>
                	<li class="disabled btnBetNow"><a href="javascript:void(0);" id="">BET NOW!</a></li>
                	<% } %>
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=weather.trackId%>&CY=<%=weather.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
                     <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=weather.trackId%>&country=<%=weather.country%>&date=<%=dateForPP%>&session=<%=raceKey.dayEvening%>&race=<%=raceKey.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
			<% } %>
    </div>
</script>
<script type="text/template" id="template-article-premiumMsg">
  <%if(country !== "HK") {%>
    <ul class="premiumBeyerMsg">
        <li><span class="beyerMessage">This feature is not available with your current subscription level.</span>
            <a href="/entries/premium/<%=trackId%>/country/<%=country%>/date/<%=date%>/Race/<%=raceNumber%>/articleId/<%=articleId%>">Upgrade Here!</a></li>
    </ul>
  <%}%>
</script>
<script type="text/template" id="template-article-nonLoggedInUser">
    <div class="premiumCnt">
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
            <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLogin" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF+</a>
            <%  }else { %>
            <a id="userLogin"
               url="<%=DRFPRO.utils.getBaseURL()+'/race-entries/'+DRFPRO.utils.getReturnURLArticles(trackId,country,date,raceNumber,articleId)%>
              <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>"
                    href="javascript:void(0)" class="btnLogin loginEncryptUrl">Log In to DRF+</a>
            <% }  %>
        </div>
</script>

<script type="text/template" id="template-quickSheet-trackName">
    <div class="entriesHeader clearfix">
        <div class="entriesDtlsLeft">
            <h4>
                <i class=<%if(priority === "high") {%>"icon-favorite" <%}else {%> "icon-add-favorite" <%}%>
                id="favorite"></i>
                <%= weather.trackName %>
            </h4>

            <div class="entriesDtlsWeather">
                <%if(weather.forecastHigh !== -999) {%>
                <i class="iconWeather"><img src="/images/iconW<%=weather.forecastIconId%>.png" alt=""/></i>
                <span class="mobileHide"><%=weather.forecastWeatherDescription%> </span>
                <%if(weather.forecastPrecipitation !== "") { %><span class="valPrecip">
                    Precip: <span><%=weather.forecastPrecipitation%>%</span></span><% } %>
                High:
                <%if(weather.forecastHigh !== -999) { %><span> <%=weather.forecastHigh%></span><span
                    class="valDegree">&deg;F</span>
                <% } else {%> <span class="noWeather">-</span> <%}%><span class="divider">|</span> Low:
                <%if(weather.forecastLow !== -999) { %><span> <%=weather.forecastLow%></span><span
                    class="valDegree">&deg;F</span>
                <% } else {%> <span class="noWeather">-</span> <%}%>
                <%}%>
            </div>
        </div>
        <div class="entriesDtlsRight">
            <a href="http://www1.drf.com/formulator-web/Launch.do" class="btnRaceCard drfProRaceCard"
               target="_blank">Get Race Card </a>
            <a href="javascript:window.print()" class="btnPrint drfProPrint"><i class="icon-print"></i> Print</a>
        </div>
    </div>
</script>
<script type="text/template" id="template-quickSheet-QuickSheetHeader">
	<% var localPost = DRFPRO.localPost(race.proPostTime); %>
    <div class="entriesRaceDtls">
        <div class="entriesRaceHeader clearfix raceActive" id="race<%=race.raceNumber%>">
            <div class="raceHeaderDtl">
                <h3>Race <% if(typeof race.raceNumber !== "undefined") {%><%=race.raceNumber%> <%}%></h3>
				<% if(DRFPRO.getValue!=null){ %> 
                  <span class="mtp">
                    <%if(race.mtpFlag) {%> <span class="mtpBlock">MTP: <span><% if(race.mtpDisplay == '0') { %><%=race.mtpDisplay%><% } else { %><%=race.mtp%><% } %></span></span><%}%>
                      Post: <span><%if(typeof postTimeDisplay === "undefined") {%> <span><%=(DRFPRO.localPost(race.proPostTime)).slice(0,-2)%></span><span class="valPostET"><%=(DRFPRO.localPost(race.proPostTime)).slice(-2)%></span>
                    <%} else {%><%=DRFPRO.localPost(race.postTimeDisplay)%> <%}%></span>
                    <% if(race.mtpDisplay == "OFF"){%> <span class="mtpOff">(OFF)</span> <%}%>
                  </span>
				<% }else{ %>
				  <span class="mtp">
                    <%if(race.mtpFlag) {%> <span class="mtpBlock">MTP: <span><% if(race.mtpDisplay == '0') { %><%=race.mtpDisplay%><% } else { %><%=race.mtp%><% } %></span></span><%}%>
                    Post: <span><%if(typeof postTimeDisplay === "undefined") {%> <%=race.proPostTime%>
                    <%} else {%><%=race.postTimeDisplay%> <%}%><span class="valPostET">ET</span></span>
                    <% if(race.mtpDisplay == "OFF"){%> <span class="mtpOff">(OFF)</span> <%}%>
                  </span>
				<% } %>
                <span class="entriesRaceDiscRight">
                            <%if(typeof race.surfaceDescription !== "undefined" && race.surfaceDescription !==null &&
                    race.surfaceDescription !=="") {%>


                            <span <%if(race.surfaceDescription == "Turf" || race.surfaceDescription == "Inner turf") {%>
                    class="sCell valTurf" ','<% } else {%> class="sCell" <%}%> ><%=race.surfaceDescription%> </span> <%}%>
                </span>
            </div>
            <% if(DRFPRO.getValue!=null && weather.trackId === "DMR"){ %>
				<ul class="btnEntriesRace">
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=weather.trackId%>&CY=<%=weather.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=weather.trackId%>&country=<%=weather.country%>&date=<%=dateForPP%>&session=<%=race.dayEvening%>&race=<%=race.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
 			<% }else{ %>
            	<ul class="btnEntriesRace">
                	<% var dayEvening = this.model.attributes.races.dayEvening; var mtp = this.model.attributes.races.mtp;
                    if( (DRFPRO.dateType(this.selectedDate,this.currentDate) === "Today\'s")
                       && (['DMR', 'CD', 'FG', 'IND', 'AP'].indexOf(weather.trackId) == -1 ) && (mtp > -29) ){
                    %>
                    <li class="btnBetNow"><a class="drfProBetNow" href="https://bets.drf.com/wagers/<%=weather.trackId%>/<%=this.raceNumber%>/<%=weather.country%>/<%=dayEvening%>/?view=basicAnalysis" id=""
                     	target="_blank">BET NOW!</a></li>
                	<% }else{ %>
                	<li class="disabled btnBetNow"><a href="javascript:void(0);" id="">BET NOW!</a></li>					
                	<% } %>
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=weather.trackId%>&CY=<%=weather.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
					    <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=weather.trackId%>&country=<%=weather.country%>&date=<%=dateForPP%>&session=<%=race.dayEvening%>&race=<%=race.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
			<% } %>
        </div>
</script>
<script type="text/template" id="template-quickSheet-raceHeadRow">
    <div class="entriesDtlsbody">
        <ul class="dataTable listHeader">
            <li class="headRow">
                <span class="sCell valentDtlsNum"><span class="valNumHead">#</span></span>
                <span class="sCell valentDtlspp">PP</span>
                <span class="sCell valentDtlsodds">ODDS</span>
                <span class="sCell valentDtlsHorse">Horse <span class="valBeyers">
       <% if(!isLogin){ %>
            <a class="loginEncryptUrl premiumBeyerActive" href="javascript:void(0)"
               url="<%=DRFPRO.utils.getReturnURLQuicksheet(trackId,country,date,raceNumber,programNumber,'','')%>
            <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>" class="premiumBeyers">Last 3 Beyers</a>
            <img class="drfPro" src="/images/DRFPro.png" alt=""/><% }else if(isSubscribe) { %>Last 3 Beyers<% } %></span></span>
                <span class="sCell valentDtlsJockey">Jockey<span> Weight</span></span>
                <span class="sCell valentDtlsTrainer">Trainer</span>
                <span class="sCell valentDtlsClaim">Claim $</span>
                <span class="sCell valentDtlsEquip">Equip.</span>
                <span class="sCell valentDtlsMed">Med.</span>
            </li>
        </ul>
    </div>
</script>
<script type="text/template" id="template-quickSheet-horseTemplate">
    <ul class="dataTable listBody">
        <li class="raceSel">
            <% _.each(horse,function(horses){ horses = DRFPRO.scrachIndication(horses);
            if(progNumber == horses.programNumber) { %>
                    <span class="sCell valentDtlsNum">
                    <span class="oddsNo saddle<%= parseInt(horses.programNumber, 10)%>"><%= horses.programNumber %></span>
                    </span>
                    <span class="sCell valentDtlspp">
                    <%if(horses.scratchIndicator !== "N") {%><%=horses.scratchReason%><%} else {%><%=horses.postPos%> <%}%>
                    </span>
                    <span class="sCell valentDtlsodds"><% if($.trim(horses.morningLineOdds)!=null && $.trim(horses.morningLineOdds)!="") { %><span>ML <%=
                        horses.morningLineOdds %></span><% } %>
                    <%if(typeof horses.horseDataPools !== "undefined" && horses.horseDataPools !== null) { if(horses.horseDataPools[0].fractionalOdds!=0) { %> <%=horses.horseDataPools[0].fractionalOdds%> <% } else {%>SCR <%} }%>
                    </span>
                    <span class="sCell valentDtlsHorse">
                    <span class="entDtlsHorseName" title="<%= horses.horseName%>"><span
                            class="valHorseName"><%= horses.horseName%></span><span
                            class="valBeyers"><% if(typeof horses.lastThreeBeyers !== "undefined") { %><%= horses.lastThreeBeyers%><% } %></span></span>
                    </span>
            <span class="sCell valentDtlsJockey"><% if(horses.jockey.firstNameInitial !=" ")  { %><%= horses.jockey.firstNameInitial %><% } if(horses.jockey.firstNameInitial !=" "  && horses.jockey.lastName !=" " ){ %>. <%} if(horses.jockey.lastName !=" " ) {  %><%=horses.jockey.lastName%><% } %> <span><% if(horses.jockey.firstNameInitial !=" "  && horses.jockey.lastName !=" " && horses.weight !=0 ){ %><%=horses.weight %> Lbs <% } %></span></span>
                    <span class="sCell valentDtlsTrainer"><%if(horses.trainer !== null && horses.trainer.firstNameInitial !== " ") {%>
                    <%=horses.trainer.firstNameInitial+". "+horses.trainer.lastName%>
                    <% }  %> </span>
                    <span class="sCell valentDtlsClaim <%if(horses.claiming <= 0) {%> valClaimCenter <%}%>">
                    <%if(horses.claiming > 0) {%>$<%=horses.ClaimingDisplay%><%} else {%> <%}%>
                    </span>
            <span class="sCell valentDtlsEquip"><%if(($.trim(horses.equipment)).length<=0) {%> -- <%}else {%><%=horses.equipment%> <%}%></span>
            <span class="sCell valentDtlsMed"><%=horses.medication%></span>
            <span class="mobileShow mJockeyTrainer">
                    <span class="entDtlsJockey">
                    <span>J: </span><% if(horses.jockey.firstNameInitial!=" ") { %><%= horses.jockey.firstNameInitial%><%  } if(horses.jockey.firstNameInitial !=" "  && horses.jockey.lastName !=" " ){ %>. <% }   if(horses.jockey.lastName !=" " ) {  %><%=horses.jockey.lastName%><% } %><span class="jockeyWt"><% if(horses.jockey.firstNameInitial !=" "  && horses.jockey.lastName !=" " && horses.weight != 0){ %> <%=horses.weight %> Lbs<% } %></span>
                    </span>
                    <span class="entDtlsTrainer">
                    <span>T: </span><%if(horses.trainer !== null && horses.trainer.firstNameInitial !== " ") {%>
                    <%=horses.trainer.firstNameInitial+". "+horses.trainer.lastName%>
                    <% } %>
                    </span>
                    </span>
            <div class="entRaceDtlsWrap">
                <span class="valMClaiming"><span>Claiming: </span><%if(horses.claiming > 0)
                    {%>$<%=horses.ClaimingDisplay%><% } %></span>
                <span class="valMMed"><span>Med.: </span><%=horses.medication%></span>
                <span class="valMEquip"><span>Equip.: </span><%if(($.trim(horses.equipment)).length>0) { %>
                    <%=horses.equipment%> <% } else { %> -- <% }%></span>
            </div>
            <% } }) %>
        </li>
    </ul>

</script>
<script type="text/template" id="template-quickSheet-SaddleClothScroller">
    <div class="dateWrap quicksheetSlider">
        <ul>
            <% _.each(horse,function(horses){ %>
            <% if(typeof horses.programNumber !=="undefined" && horses.programNumber!="") { %>
            <li
            <% if(progNumber === horses.programNumber) { %>class="activeDate"<%}  %>  ><% if(horses.isQuickSheet ===
            true)
            { %><a
                href="/entries/quickSheet/<%=trackId%>/country/<%=country%>/raceNumber/<%=raceNumber%>/programNumber/<%=$.trim(horses.programNumber)%>/date/<%=date%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% }if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>"><%
            } %><span
                    class="oddsNo saddle<%=parseInt(horses.programNumber, 10)%>"><%=$.trim(horses.programNumber)
                %></span><%
            if(horses.isQuickSheet === true) { %><i class="icon-qSheet"></i></a><% } %></li><% } %>
            <% }); %>
        </ul>
    </div>
</script>
<script type="text/template" id="template-quickSheet-quickSheetData">
   <% if(quickSheetDetails!=null){ %>
    <div class="quickSheetBody">
        <h5>QuickSheet:</h5>
        <% if(keyRaces!=null) { %>
        <div class="quickSheetDtls qsHorsesExitingKey clearfix">
            <h6> HORSES EXITING KEY RACES
                <span>(Runners who last ran in races which produced multiple winners)</span></h6>
            <ul class="quickSheetTable clearfix">
                <li>
                    <span class="qCell valqsHead1">Key Race</span>
                    <%  if(keyRaces.keyRace!=""){ %>
                    <span class="qCell valKeyRace"><%=keyRaces.keyRace%></span>
                    <% } %>
                </li>
                <li>
                    <span class="qCell valqsHead1">Class</span>
                    <% if(keyRaces.class1!=""){%>
                    <span class="qCell valClass"><%=keyRaces.class1%></span>
                    <% }   %>
                </li>
                <li>
                    <span class="qCell valqsHead1">Surface</span>
                    <% if(keyRaces.surface!=""){%>
                    <span class="qCell valSurface"><%=keyRaces.surface%></span>
                    <% }   %>
                </li>
                <li>
                    <span class="qCell valqsHead1">Distance</span>
                    <% if(keyRaces.publishedValue!=""){%>
                    <span class="qCell valDistance"><%=keyRaces.publishedValue%></span>
                    <% }   %>
                </li>
            </ul>
            <ul class="quickSheetTable clearfix">
                <li>
                    <span class="qCell valqsHead">Finish/Beaten Lengths</span>
                    <% if(keyRaces.formattedFinishPartText!=""){%>
                    <span class="qCell valFinish"><%=keyRaces.finishText%><sup><%=keyRaces.formattedFinishPartText
                        %></sup></span>
                    <% }   %>
                </li>
                <li>
                    <span class="qCell valqsHead">Last Race Odds</span>
                    <% if(keyRaces.actualOdds!=""){%>
                    <span class="qCell valLastRace"><%=keyRaces.odds%></span>
                    <% }   %>
                </li>
                <li>
                    <span class="qCell valqsHead">Next-Out Winners/Starters</span>
                    <% if(keyRaces.nextOutWinnerStarters!=""){%>
                    <span class="qCell valNextOut"><%=keyRaces.nextOutWinnerStarters%></span>
                    <% }   %>
                </li>
                <li>
                    <span class="qCell valqsHead">Avg.Next-Out Beyer</span>
                    <% if(keyRaces.avgNextOutBeyer!=""){%>
                    <span class="qCell valAvgNext">
                        <% if(keyRaces.avgNextOutBeyer==0) { %>N/A<% } else { %><%=keyRaces
                        .avgNextOutBeyer%><% } %></span>
                    <% } %>
                </li>
            </ul>
        </div>
        <% } %>

        <% if((quickSheetDetails.workouts!=null) && (quickSheetDetails.workouts.length > 0)) { %>
        <div class="quickSheetDtls qsWorkouts">
            <h6> Trackman Workout Specials <span>(Horses with very sharp recent workouts)</span></h6>
            <ul class="quickSheetTable mobileHide">
                <li class="rowHead">
                    <span class="qCell valDate">Date</span>
                    <span class="qCell valTrack">Track</span>
                    <span class="qCell valSurface">Surface</span>
                    <span class="qCell valDistance">Distance</span>
                    <span class="qCell valTime">Time</span>
                    <span class="qCell valWorkoutRank">Workout Rank</span>
                </li>
                <% _.each(quickSheetDetails.workouts,function(workout){  %>
                <li>
                    <span class="qCell valDate"><%=workout.date%></span>
                    <span class="qCell valTrack"><% if(workout.trackName!=null) { %><%=workout.trackName%><% }else {
                        %><%=workout.trackId%><% } %></span>
                    <span class="qCell valSurface"><span <% if(workout.surface == "Turf") { %>class="valTurf" <%}%>><%=workout.surface%></span></span>
                    <span class="qCell valDistance"><%=workout.distanceDescription%></span>
                    <span class="qCell valTime"><%=workout.time%></span>
                    <span class="qCell valWorkoutRank"><%=workout.workoutRank%></span>
                </li>
                <% }); %>
            </ul>
            <ul class="quickSheetTable mobileShow clearfix">
                <li>
                    <span class="qCell valDate">Date</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){ %>
                    <span class="qCell valDate"><%=workout.date%></span>
                    <% }); %>
                </li>
                <li>
                    <span class="qCell valTrack">Track</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){  if(workout.trackName!=""){%>
                    <span class="qCell valTrack"><%=workout.trackName%></span>
                    <% }  }); %>
                </li>
                <li>
                    <span class="qCell valSurface">Surface</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){ %>
                    <span class="qCell valSurface"><span <% if(workout.surface == "Turf") { %>class="valTurf" <%}%>><%=workout.surface%></span></span>
                    <% }); %>
                </li>
                <li>
                    <span class="qCell valDistance">Distance</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){ %>
                    <span class="qCell valDistance"><%=workout.distanceDescription%></span>
                    <% }) %>
                </li>
                <li>
                    <span class="qCell valTime">Time</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){ %>
                    <span class="qCell valTime"><%=workout.time%></span>
                    <% }) %>
                </li>
                <li>
                    <span class="qCell valWorkoutRank">Workout Rank</span>
                    <% _.each(quickSheetDetails.workouts,function(workout){ %>
                    <span class="qCell valWorkoutRank"><%=workout.workoutRank%></span>
                    <% }) %>
                </li>
            </ul>
        </div>
        <% } %>

        <% if((quickSheetDetails.angles!=null) && (quickSheetDetails.angles.length > 0)) {%>
        <div class="quickSheetDtls">
            <h6> Profitable Trainer Angles</h6>
            <ul class="quickSheetTable">
                <li class="rowHead">
                    <span class="qCell valAngle">Angle</span>
                    <span class="qCell varStarts">Starts</span>
                    <span class="qCell valWin">Win %</span>
                    <span class="qCell valROI">R.O.I</span>
                </li>
                <% _.each(quickSheetDetails.angles,function(angle){ %>
                <li>
                    <span class="qCell valAngle"><%=angle.categoryDesc%></span>
                    <span class="qCell varStarts"><%=angle.starts%></span>
                    <span class="qCell valWin"><%=angle.winPercent%></span>
                    <span class="qCell valROI"><%=angle.roi%></span>
                </li>
                <% }) %>
            </ul>
        </div>
        <% } %>
        <% if(hotTrainerJockey!=null) {%>
        <div class="quickSheetDtls">
            <h6> Winning Trainer / Jockey Combination </h6>
            <ul class="quickSheetTable qsMobile clearfix">
                <li class="rowHead">
                    <span class="qCell valRecord">Recent Record <span>(ROI)</span></span>
                    <span class="qCell valMeet">Recent This <span>Meet(ROI)</span></span>
                    <span class="qCell valYear">Recent This <span>Calendar Year(ROI)</span></span>

                </li>
                <li>
                    <span class="qCell valRecord"><%=hotTrainerJockey.recordLast5Stats+" ("+hotTrainerJockey.roiLast5Starts+")"%></span>
                    <span class="qCell valMeet"><%=hotTrainerJockey.recordThisMeetStats+" ("+hotTrainerJockey.roiThisMeetStarts+")"%></span>
                    <span class="qCell valYear"><%=hotTrainerJockey.recordThisCalStats+" ("+hotTrainerJockey.roiThisCalStarts+")"%></span>
                </li>
            </ul>
        </div>
        <% } %>
        <%  if((horsesCourse!=null) && (horsesCourse.length) > 0) { %>
        <div class="quickSheetDtls">
            <h6> Horse For Course <span>(Runner w/exceptional race record this track)</span></h6>
            <ul class="quickSheetTable qsMobile clearfix">
                <li class="rowHead">
                    <span class="qCell valRecordTrack">Record this Track</span>
                    <span class="qCell valBeyers">Last 3 Beyers</span>
                </li>
                <li>
                    <span class="qCell valRecordTrack"><%=horsesCourse[0].horseStats%></span>
                    <span class="qCell valBeyers"><%=horsesCourse[0].last3BeyerNo%></span>
                </li>
            </ul>
        </div>
        <% } %>
        <% if(distanceSpec!=null) {%>
        <div class="quickSheetDtls">
            <h6> Distance Specialist <span>(Runner w/exceptional record this distance)</span></h6>
            <ul class="quickSheetTable qsMobile clearfix">
                <li class="rowHead">
                    <span class="qCell valRecordTrack">Record Today’s <span>Surface/Distance</span></span>
                    <span class="qCell valBeyers">Last 3 Beyers <span>Surface/Distance</span></span>
                </li>
                <li>
                    <span class="qCell valRecordTrack"><%=distanceSpec.recordThisDistance%></span>
                    <span class="qCell valBeyers"><%=distanceSpec.beyers%></span>
                </li>
            </ul>
        </div>
        <% } %>
        <% if(HotJockey!=null) { %>
        <div class="quickSheetDtls">
            <h6> Hot Jockey </h6>
            <ul class="quickSheetTable qsMobile clearfix">
                <li class="rowHead">
                    <span class="qCell">Record (Last 5 Race Days)</span>
                </li>
                <li>
                    <span class="qCell"><%=HotJockey.jockeyStats%></span>
                </li>
            </ul>
        </div>
        <% } %>
        <% if(HotTrainer!=null) { %>
        <div class="quickSheetDtls">
            <h6> Hot Trainer </h6>
            <ul class="quickSheetTable qsMobile clearfix">
                <li class="rowHead">
                    <span class="qCell">Record (Last 7 Days)</span>
                </li>
                <li>
                    <span class="qCell"><%=HotTrainer.trainerStats%></span>
                </li>
            </ul>
        </div>
        <% } %>
    </div>
    <% } %>
</script>
<script type="text/template" id="template-quickSheet-textHeader">
    <div class="entriesDtlsHead clearfix">
        <h2><span class='titleDetails' <%  if(DRFPRO.getValue!=null) { %>style="display:none" <% } %> ><%=dateType%>
            Entries</span><span><%=DRFPRO.displayDate(selectedDate)%></span>
        </h2>
    </div>
</script>
<!--------------------------------------Changes Details Template  ------------------------------------>

<script type="text/template" id="template-Changes-textHeader">
    <div class="entriesDtlsHead clearfix">
        <h2><%=dateType%> Changes
            <span><%=DRFPRO.displayDate(selectedDate)%> </span>
        </h2>
    </div>
</script>
<script type="text/template" id="template-Changes-trackName">
    <div class="entriesHeader clearfix">
        <div class="entriesDtlsLeft">
            <h4>
                <i class=<%if(priority === "high") {%>"icon-favorite" <%}else {%> "icon-add-favorite" <%}%>
                id="favorite"></i>
                <%= weather.trackName %>
            </h4>

            <div class="entriesDtlsWeather">
                <%if(weather.forecastHigh !== -999) {%>
                <i class="iconWeather"><img src="/images/iconW<%=weather.forecastIconId%>.png" alt=""/></i>
                <span class="mobileHide"><%=weather.forecastWeatherDescription%></span>
                <% if(weather.forecastPrecipitation!=null && weather.forecastPrecipitation!='') { %><span
                    class="valPrecip">Precip:
                <span><%=weather.forecastPrecipitation%>%</span></span><% } %>
                High:
                <%if(weather.forecastHigh !== -999) { %><span> <%=weather.forecastHigh%></span><span
                    class="valDegree">&deg;F</span>
                <% } else {%> <span class="noWeather">-</span> <%}%>
                <span class="divider">|</span> Low:
                <%if(weather.forecastLow !== -999) { %><span> <%=weather.forecastLow%></span><span
                    class="valDegree">&deg;F</span>
                <%} else {%> <span class="noWeather">-</span> <%}%>
                <%}%>
            </div>
        </div>
    </div>
</script>
<script type="text/template" id="template-Changes-changeEntry">
	<% var localPost = DRFPRO.localPost(proPostTime); %>
    <li id="race<%=raceNumber%>"><span class="valchngRace">
                <h3>Race <%=raceNumber%></h3>
                <span class="valchngPost"><% if(typeof postTime!=="undefined" &&  postTime!=="" && postTime!==-999) { %>
				<% if(DRFPRO.getValue!=null){ %>
                  <span>Post: </span><%=DRFPRO.localPost(postTime)%>&nbsp;<span class="valPostET">PT</span>
				<% }else{ %>
				  <span>Post: </span><%=postTime%>&nbsp;<span class="valPostET">ET</span>
				<% } %>
                <%} %>
                </span>
                </span>
                <span class="valchngDisc"><% _.each(changes,function (change) { %>
                <span>  <% if(change.type !== "S") { %>
                <%= change.text%> <%}
                 else { %>
                <span> <%= change.text.replace(/scratched/i, "")%> </span> scratched  <% } %>
                </span>
                <% }); %> </span>
    </li>
</script>


<script type="text/template" id="template-result-textHeader">
    <div class="entriesDtlsHead clearfix">
        <h2><span class='titleDetails'<%  if(DRFPRO.getValue!=null) { %>style="display:none" <% } %> ><%=dateType%>
            Results</span><span><%=DRFPRO.displayDate(selectedDate)%></span>
        </h2>
    </div>
</script>

<!--non premium users template-->
<script type="text/template" id="template-nonPremiumUsers">
		<!-- start upsell -->
	<div class="premiumCnt">
        <div class="loginBody clearfix">
        	<div class="centerUpsell">
        	  <div class="bigAd">
            	<a class="drfProSubscribe" href="http://www.drf.com/free-plus" target="_blank"><img class="subscribeNow" src="/images/bigAd.png"></a>
            	<br>
              </div>
              <div class="smallAd">
            	<a class="drfProSubscribe" href="http://drfbets.com/rewards" target="_blank"><img class="subscribeNow" src="/images/smallAd.png"></a>
            	<br>
              </div>
            </div>
        </div>
    </div>
<!-- end upsell --></script>

<!--non premium users template Articles-->
<script type="text/template" id="template-nonPremiumUsersArticle">
		<!-- start upsell -->
	<div class="premiumCnt">
        <div class="loginBody clearfix">
        	<div class="centerUpsell">
        	  <div class="bigAd">
            	<a class="drfProSubscribe" href="http://www.drf.com/free-plus" target="_blank"><img class="subscribeNow" src="/images/bigAdArticle.png"></a>
            	<br>
              </div>
              <div class="smallAd">
            	<a class="drfProSubscribe" href="http://drfbets.com/rewards" target="_blank"><img class="subscribeNow" src="/images/smallAdArticle.png"></a>
            	<br>
              </div>
            </div>
        </div>
    </div>
<!-- end upsell -->
</script>

<!--This template is used to show the message the user who is not logged in in Analysis & pics and closer looks :START-->
<script type="text/template" id="template-nonLoggedInUser">
    <div class="premiumCnt">
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
           <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLogin" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF.com</a>
            <span class="dontAcc"><span>Don't have an account?</span> <a
                    href="https://store.drf.com/stores/1/register.cfm" target="_blank" >Click here to create one. </a></span>
          <%  }else { %>
            <a
              id="userLogin" url="<%=DRFPRO.utils.getReturnURL(trackId,country,date,raceNumber,tabHref)%>
              <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>"
              href="javascript:void(0)" class="btnLogin loginEncryptUrl">Log In to DRF.com</a><span class="dontAcc"><span>Don't have
                an
                account?</span>
                <a href="https://store.drf.com/stores/1/register.cfm" target="_blank" >Click here to create one. </a></span>
      <% }  %>
        </div>
</script>
<!--This template is used to show the message the user who is not logged in for Analysis & Picks-->
<script type="text/template" id="template-nonLoggedInAnalysis">
		<div class="premiumCnt">
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
           <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLogin" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF.com</a>
            
          <%  }else { %>
            <a
              id="userLogin" url="<%=DRFPRO.utils.getReturnURL(trackId,country,date,raceNumber,tabHref)%>
              <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>"
              href="javascript:void(0)" class="btnLogin loginEncryptUrl">Log In to DRF+</a>
      	<% }  %>
        </div>
</script>

<!--This template is used to show the message the user who is not logged in for Closer Looks-->
<script type="text/template" id="template-nonLoggedInCloser">
		<div class="premiumCnt">
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
           <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLogin" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF+</a>
          <%  }else { %>
            <a
              id="userLogin" url="<%=DRFPRO.utils.getReturnURL(trackId,country,date,raceNumber,tabHref)%>
              <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>"
              href="javascript:void(0)" class="btnLogin loginEncryptUrl">Log In to DRF+</a>
      	<% }  %>
        </div>
</script>

<!--This template is used to show the message the user who is not logged in for Hot Stats-->
<script type="text/template" id="temp-nonLoggedInUser">
		<br>
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
            <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLog" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF+</a>
            <%  }else {%>
            <a  id="userLog"
               href="javascript:void(0)"
               url="<%=DRFPRO.utils.getReturnURLQuicksheet(trackId,country,date,raceNumber,programNumber)%>
            <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>" class="btnLogin loginEncryptUrl">Log In to DRF+</a>
            <% }  %>
        </div> 
		<br>      
</script>

<!--This template is used to show the message when data is not available on analysis page :START-->
<script type="text/template" id="template-noDataAvailableAnalysis">
    <div class="premiumCnt">
        <div class="loginMsg">
            The Analysis & Picks data is not available for this race.
        </div>
</script>

<!--This template is used to show the message when data is not available on quichsheet page :START-->
<script type="text/template" id="template-noDataAvailableQuicksheetTemplate">
    <div class="premiumCnt">
        <div class="loginMsg">
            The Quicksheet data is not available for this runner.
        </div>
</script>

<!--This template is used to show the message when data is not available on article page :START-->
<script type="text/template" id="template-noDataAvailableArticleTemplate">
    <div class="premiumCnt">
        <div class="loginMsg">
            No data available for this post.
        </div>
</script>
<!--This template is used to show the message when data is not available on closer looks :START-->
<script type="text/template" id="template-noDataAvailableCloser">
    <div class="premiumCnt">
        <div class="loginMsg">
            The Closer Looks data is not available for this race.
        </div>
</script>

<script type="text/template" id="template-nonLoggedInUnSubScribedUser">
    <div class="premiumCnt">
        <div class="loginMsg">
            This feature is not available with your current subscription level.
        </div>
</script>
<!--This template is used to show the message the user who is not logged in in Analysis & pics and closure looks :END-->

<!------------------------------  Analysis and pick tab   ------------------------------------------>
<script type="text/template" id="template-Analysis-analysisHeadTemplate">
    <div class="titlePrint">Analysis &amp; Picks</div>
    <% if((typeof bestBet!=="undefined") && (raceNumber==bestBet.bestBetRaceNumber)){ %>
    <div class="analysisHead clearfix">
        <div class="drfPicks">
            <span class="titleAnalysis">DRF Picks:</span><% _.each(drfPicks,function(picks,key){%>
            <span
                class="oddsNo saddle<%=parseInt(picks.programNumber, 10)%>"><%=picks.programNumber%></span><% }); %>
        </div>
        <% if(typeof beyerEdge!== "undefined") {%>
        <div class="beyerEgde">
            <span class="titleAnalysis">Beyer Edge:</span>
            <% _.each(beyerEdge,function(edge){%>
            <span class="oddsNo saddle<%=parseInt(edge, 10)%>"><%=edge%></span><% }); %>
        </div>
        <% } %>
        <div class="bestBet">
            <span class="titleAnalysis">Best Bet:</span>
            <div class="bestBetDtl">
                <span class="oddsNo saddle<%=parseInt(bestBet.bestBetNumber, 10)%>">
                    <%=bestBet.bestBetNumber%>
                </span>
                <span class="bestByName"><%="  "+bestBet.bestBet%></span>
            </div>
        </div>
      </div>
    <% } %>
</script>
<script type="text/template" id="template-Analysis-horseAnalysisTemplate">
    <% if((handicapperName != null && handicapperName!="")){%>
    <div class="horseAnalysis">
        Analysis By: <span class="analysisBy"><%=handicapperName%></span>
    </div>
    <% } %>
    <% if((drfPicksDescription.length!=0) && (drfPicksDescription !='')){%>
    <div class="horseAnalysis horseAnalysisDtl">
           <%=drfPicksDescription%>
    </div>
    <% } %>
</script>
<script type="text/template" id="template-Analysis-betAnalysisTemplate">
   <% if( typeof expertScore !='undefined'){%>
    <div class="betAnalysis">
        <ul>
            <% _.each(expertScore,function(expert) {  if(expert.heading1 !== "CONSENSUS" && expert.heading1 !==null) {
            %>
            <li>
                <div class="betAuthor">By <span class="author"><%=expert.heading1%></span><% if(expert.heading2
                    !==null) { %><%=" "+expert
                    .heading2%><% } %></div>
                <% _.each(expert.horseDetails,function(val) { %>
                <div class="bestBetInfo"><span class="oddsNo saddle<%=parseInt(val.programNumber, 10)%>">
                        <%if(val.programNumber!==null) {   %><%=val.programNumber%><% } %>
                    </span>
                    <span class="betHorse highestBet"><%=val.horseName%></span>
                    <!--<span class="betRank"><%=val.points%></span>-->
                </div>
                <% }); %>
            </li>
            <% } }); %>
        </ul>
        <ul class="bestConsensus">
            <% _.each(expertScore,function(expert,value) { if(expert.heading1 === "CONSENSUS") { %>
            <li>
                <div class="betAuthor"><span class="author">CONSENSUS</span></div>
                <% _.each(expert.horseDetails,function(val) { %>
                <div class="bestBetInfo"><span class="oddsNo saddle<%=parseInt(val.programNumber, 10)%>">
                        <%=val.programNumber%></span><span class="betHorse highestBet"><%=val.horseName%></span>
                    <span class="betRank"><%=val.points%></span></div>
                <% }); %>
                <% } }); %>
            </li>
            <%  if(expertScore!=null) { %>
            <li class="noteConsensus">Consensus Total Based on 5 points for 1 (7 for Best Bet), 2 for 2nd, 1 for 3rd.</li>
            <% } %>
        </ul>
    </div>
    <% } %>
</script>

<!----------------------------------- closerLook view ------------------------------------>
<script type="text/template" id="template-closerLook-closerLooksWrapTemplate">
    <div class="titlePrint">Closer looks</div>
    <ul class="closerLooksWrap">
        <%_.each(horses,function(horse,horseCount){  var horseId = 'closer'+parseInt(horse.programNumber, 10); var closerTabName
        = 'closerLooksTab'+raceNumber;
        %>
        <li
               raceNumber="closerLooksTab<%=raceNumber%>"
               class="closerLooksAccordion <%  if(DRFPRO.activeCloserLookHorses(horseId,closerLooksHorse,closerTabName) == true)
               { %>closerLooksActive <% }  %>"
            id="closer<%= parseInt(horse.programNumber, 10)%>">
            <div class="closerLooksHead">
                    <span class="oddsNo saddle<%= parseInt(horse.programNumber, 10)%>">
                        <% if(typeof horse.programNumber !== "undefined" &&  horse.programNumber!="")
                        { %><%=horse.programNumber%><% } %>
                    </span><%=horse.horseName%><i class="icon-togglePlus"></i>
            </div>
            <div class="closerLooksDtls clearfix"><%=horse.description%>
                <%
                if(horseCount == (horses.length-1)){
                %>
                <div class="descName">
                <%="- "+horse.handicapperName%>
                </div>
                <%}%>
            </div>
        </li>
        <% }); %>
    </ul>
</script>

 <!-- ----------------------------- Race info details tab ---------------------------   -->

<script type="text/template" id="template-raceInfo-raceHeadRow">
    <div class="titlePrint">Race Info</div>
    <ul class="dataTable listHeader">
    <li class="headRow">
        <span class="sCell valentDtlsNum"><span class="valNumHead">#</span></span>
        <span class="sCell valentDtlspp">PP</span>
        <span class="sCell valentDtlsodds">ODDS</span>
        <span class="sCell valentDtlsHorse">Horse <span class="valBeyers nonBlock"><br>
       <% if(!isLogin){%>
            <a class="loginEncryptUrl premiumBeyerActive" href="javascript:void(0)"
               url="<%=DRFPRO.utils.getReturnURL(trackId,country,date,raceNumber,tabHref)%>
            <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>" class="premiumBeyers">(Last 3 Beyers)</a>
            <i><img class="drfPro" src="/images/DRFPro.png" alt=""/></i><% }else if(isSubscribe) { %>(Last 3 Beyers)<% } %></span><span class="fontCapital">(sire - dam | dam sire)</span></span>
        <span class="sCell valentDtlsJockey">Jockey<span> Weight</span></span>
        <span class="sCell valentDtlsTrainer">Trainer</span>
        <span class="sCell valentDtlsClaim">Claim $</span>
        <span class="sCell valentDtlsEquip">Equip.</span>
        <span class="sCell valentDtlsMed">Med.</span>
        <span class="sCell valentDtlsQsheet"> </span>
        </li>
    </ul>
</script>
<script type="text/template" id="template-raceInfo-premiumBeyerMsg">
  <%if(country !== "HK") {%>
    <ul class="premiumBeyerMsg">
        <li><span class="beyerMessage">Last 3 Beyers are not displaying (This feature is not available with your current subscription level) </span>
            <a
        href="http://static.drf.com/plus/plus-pro-upgrade.html">Upgrade Here!</a></li>
    </ul>
  <%}%>
</script>


<!-- ----------------------------- sire details tab ---------------------------   -->

<script type="text/template" id="template-raceInfo-sireTemplate">
<i class="icon-toggleRace"></i>
<span class="sCell valentDtlsNum">
    <span ></span>
    </span>

<span class="sCell valentDtlspp">
    <%if(scratchIndicator !== "N") {%> <%} else {%> <%}%>
    </span>

<span class="sCell valentDtlsodds">
    </span>

<span class="sCell valentDtlsHorse padding">

    <span class="entDtlsHorseName" title="<%= horseName%>">
			<span class="valHorseName tripleWidth notClickable">
					<%
					if(typeof sireLink !="undefined"  && sireLink !=null && typeof sireLink[sireName]!="undefined" && sireLink[sireName]!=null){
						%>
					<a style="font-weight: bold; text-decoration: underline;font-family: sans-serif; color:rgb(250, 97, 97)" href=
					<%=sireLink[sireName] %>
					 target="_blank"><%=sireName %></a> 
					 <%} else {%>  
					 <%=sireName %> 
					 <% } %>
					
					 - <%= damName%> | <%= damSireName%>
			</span>
	</span>


    </span>

<span class="sCell valentDtlsJockey"></span>

<span class="sCell valentDtlsTrainer"></span>

<span class="sCell valentDtlsClaim"></span>
<span class="sCell valentDtlsEquip"></span>
<span class="sCell valentDtlsMed"></span>
       



<span class="sCell valentDtlsQsheet"><% if(typeof isQuickSheet !=="undefined" && isQuickSheet ==
                        true) { %>
                        <a
                                href="/entries/quickSheet/<%=trackId%>/country/<%=country%>/raceNumber/<%=raceNumber%>/programNumber/<%=$.trim(programNumber)%>/date/<%=date%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>"></a><% } %></span>


</script>


<script type="text/template" id="template-raceInfo-horseTemplate">
            <i class="icon-toggleRace"></i>
            <span class="sCell valentDtlsNum <% if(typeof sireName !="undefined"  && sireName !=null) { %>noBorderBottom<% }%>">
                <span class="oddsNo saddle<%= parseInt(programNumber, 10)%>"><%=  programNumber %></span>
                </span>

            <span class="sCell valentDtlspp <% if(typeof sireName !="undefined"  && sireName !=null) { %>noBorderBottom<% }%>">
                <%if(scratchIndicator !== "N") {%><%=scratchReason%><%} else {%><%=postPos%> <%}%>
                </span>

            <span class="sCell valentDtlsodds <% if(typeof sireName !="undefined"  && sireName !=null) { %>noBorderBottom<% }%>">
            <% if($.trim(morningLineOdds)!=null && $.trim(morningLineOdds)!="")
                {%><span>ML
                <%= morningLineOdds %></span><% } %>
                <%if(typeof horseDataPools !== "undefined" && horseDataPools !== null) {
                if(horseDataPools[0].fractionalOdds!=0) { %> <%=horseDataPools[0].fractionalOdds%> <% } else {%>SCR <%} }%>
                </span>

            <span class="sCell valentDtlsHorse <% if(typeof sireName !="undefined"  && sireName !=null) { %>noBorderBottom<% }%>">

                <span class="entDtlsHorseName" title="<%= horseName%>"><span
                        class="valHorseName"><%= horseName%></span><span class="valBeyers"><% if(typeof lastThreeBeyers !== "undefined") { %><%= lastThreeBeyers%><% } %></span></span>


                </span>

            <span class="sCell valentDtlsJockey"><%  if(jockey.firstNameInitial !==" " && jockey.firstName!=="NULL"  && (jockey.lastName !=" " && jockey.lastName !==null)  )  { %><%= jockey.firstNameInitial %><% } if(jockey.firstNameInitial !==" "  && jockey.firstNameInitial !==null && (jockey.lastName !=" " && jockey.lastName !==null)  ){ %>. <%} if(jockey.lastName !=" " ) {  %><%=jockey.lastName%><% } %> <span><% if(jockey.firstNameInitial !=" "  && jockey.lastName !=" " && (weight != 0 && weight!==null) ){ %><%=weight %> Lbs <% } %></span></span>

            <span class="sCell valentDtlsTrainer"><%if(trainer !== null && trainer.lastName!==null && (trainer.firstNameInitial !== " " && trainer.firstNameInitial!=="NULL") ) {%>
                <%=trainer.firstNameInitial+". "+trainer.lastName%>
                <% }  %> </span>

            <span class="sCell valentDtlsClaim <%if(claiming <= 0) {%> valClaimCenter <%}%>">
                <%if(claiming > 0) {%>$<%=ClaimingDisplay%><%} else {%> <%}%>
                </span>
            <span class="sCell valentDtlsEquip"><%if(($.trim(equipment)).length<=0) {%> -- <%}else {%><%=equipment%> <%}%></span>
            <span class="sCell valentDtlsMed"><%=medication%></span>
                    <span class="sCell valentDtlsQsheet"><% if(typeof isQuickSheet !=="undefined" && isQuickSheet ==
                        true) { %>
                        <a class="drfProHotStats" 
                                href="/entries/quickSheet/<%=trackId%>/country/<%=country%>/raceNumber/<%=raceNumber%>/programNumber/<%=$.trim(programNumber)%>/date/<%=date%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>"><i
                                class="icon-hotStat"></i></a><% } %></span>
        </script>
<script type="text/template" id="template-raceInfo-horseMobileViewTemplate">
            <span class="mobileShow mJockeyTrainer">

                <span class="entDtlsJockey">
                <% if((jockey.firstNameInitial!=" " && jockey.firstNameInitial!==null) && jockey.lastName !=" " && jockey.lastName !==null) { %><span>J: </span><% if(jockey.firstName!=="NULL") {  %><%= jockey.firstNameInitial%><% } %><% if(jockey.firstNameInitial !=" "  && jockey.lastName !=" " ){ %>. <% }   if(jockey.lastName !=" " ) {  %><%=jockey.lastName%><% } %><span class="jockeyWt"><% if(jockey.firstNameInitial !=" "  && jockey.lastName !=" " &&  (weight != 0 && weight!==null)){ %> <%=weight %> Lbs<% } %></span><% } %>
                </span>

                <span class="entDtlsTrainer">
                <%if(trainer !== null && trainer.firstNameInitial !== " " && trainer.firstNameInitial!=="NULL" && trainer.lastName!==null) {%><span>T: </span>
                <%=trainer.firstNameInitial+". "+trainer.lastName%>
                <% } %>
                </span>
                
                <span class="entDtlsSire padding-top">
                <span>S: </span>
                <%
				if(typeof sireLink !="undefined"  && sireLink !=null && typeof sireLink[sireName]!="undefined" && sireLink[sireName]!=null){
					%>
					
				<a style="font-weight: bold; text-decoration: underline;font-family: sans-serif; color:rgb(250, 97, 97)" href=
				<%=sireLink[sireName] %>
				 target="_blank"><%=sireName %></a> 
				 <%} else {%>  
				 <%=sireName %> 
				 <% } %>
				
				 - <span>D: </span> <%= damName%> 
                </span>
                
                <span class="entDtlsSire">
                <span>DS: </span> <%= damSireName%>
                </span>

                </span>
                <div class="entRaceDtlsWrap">
                <span class="valMClaiming"><span>Claiming: </span><%if(claiming > 0) {%>$<%=ClaimingDisplay%><% } %></span>
                <span class="valMMed"><span>Med.: </span><%=medication%></span>
                <span class="valMEquip"><span>Equip.: </span><%if(($.trim(equipment)).length>0) { %> <%=equipment%> <% } else { %> -- <% }%></span>
                </div>
        </script>

<script type="text/template" id="template-entriesDetails-raceHeader">
            <div class="entriesRaceHeader clearfix" id="race<%= raceKey.raceNumber%>" data-id="<%= raceKey.raceNumber%>">
                <div class="raceHeaderDtl">
                    <h3>Race <% if(typeof raceNumber === 'undefined') { %><%= raceKey.raceNumber%> <%} else {%> <%= raceNumber %> <%}%></h3>
					<% if(DRFPRO.getValue!=null){ %>
                      <span class="mtp">
                          <%if(mtpFlag) {%> <span class="mtpBlock">MTP:
                          <span><% if(mtpDisplay == '0') { %><%=mtpDisplay%><% } else { %><%=mtp%><% } %>&nbsp;</span></span><%}%>
                          <% if(typeof postTimeDisplay !=="undefined" && postTimeDisplay!=="" || (typeof postTime!=="undefined" && postTime!=="" && postTime!==-999)) { %>
                        Post: <span><%if(typeof postTimeDisplay === 'undefined' || postTimeDisplay===null  && postTime!="") {%> <span><%=(DRFPRO.localPost(postTime)).slice(0,-2) %> </span><span class="valPostET"><%=(DRFPRO.localPost(postTime)).slice(-2) %></span> <%} else {%><%=DRFPRO.localPost(postTimeDisplay)%> <%}%></span>
                          <% } if(mtpDisplay == 'OFF'){%> <span class="mtpOff">(OFF)</span> <%}%>
                      </span>
					<% }else{ %>
					  <span class="mtp">
                          <%if(mtpFlag) {%> <span class="mtpBlock">MTP:
                          <span><% if(mtpDisplay == '0') { %><%=mtpDisplay%><% } else { %><%=mtp%><% } %>&nbsp;</span></span><%}%>
                          <% if(typeof postTimeDisplay !=="undefined" && postTimeDisplay!=="" || (typeof postTime!=="undefined" && postTime!=="" && postTime!==-999)) { %>
                          Post: <span><%if(typeof postTimeDisplay === 'undefined' || postTimeDisplay===null  && postTime!="") {%> <%=postTime%> <%} else {%><%=postTimeDisplay%> <%}%></span><span class="valPostET">ET</span>
                          <% } if(mtpDisplay == 'OFF'){%> <span class="mtpOff">(OFF)</span> <%}%>
                      </span>
					<% } %>
                <span class="entriesRaceDiscRight">
                            <%if(typeof surfaceDescription !== "undefined" && surfaceDescription !==null && surfaceDescription !=="") {%>


                            <span <%if(surfaceDescription == "Turf" || surfaceDescription == "Inner turf") {%> class="sCell valTurf" ','<% } else {%> class="sCell" <%}%> ><%=surfaceDescription%> </span> <%}%>
                </span>

                <div class="mobileShow clearfix"></div>
            <span class="entriesRaceDiscMobile">

                            <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) {  if(raceTypeDescription=="STAKES") { %><span class="raceDisctxtName"><%=raceName%></span><% } } %>

                            <% if(distanceDescription !== "" && raceTypeDescription !== "") { %>

                            <span class="raceDisctxtUp">
                            <% if(distanceDescription !== "" || distanceDescription !==null)  { %><%=aboutDistanceIndicator%> <%=distanceDescription +" | "%>
                            <% } if(raceRestrictionDescription !== null && raceRestrictionDescription !=="") { %><%=raceRestrictionDescription+" | "%>
                            <% } if(sexRestrictionDescription !== "" && sexRestrictionDescription !==null) { %><%=sexRestrictionDescription%>
                            <% } if(ageRestrictionDescription !== "" && ageRestrictionDescription !==null)  {  %>
                            <% if (ageRestrictionDescription.search("Year Olds And Up") !== -1) {  var upText=ageRestrictionDescription.replace("Year Olds And Up","<span></span>");  %><%="| "+upText +" <span class='mobileHide'>|</span> "%><% } else { %><%=" | "+ageRestrictionDescription+" <span class='mobileHide'>|</span> "%><% }  %>
                            </span>

                             <% } %>

                        <span class="raceDisctxt">
                        <%if(typeof grade!=="undefined" && grade!=" " && grade!=null) { if(raceTypeDescription=="STAKES") { %><%="G"+grade%>&nbsp;<% } }%><span class="MClaimingBold"><%=raceTypeDescription%></span><%if(minClaimPrice>0) {%><%=":<span> $"+ minClaimPriceDisplay+"</span>"%><%}%> | Purse: <span>$<%=purseDisplay%></span>
                        </span>
                        <span class="entriesraceDisctxt"><%= wagerText %></span>
                        </span>
            <% }   else {%>

            <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) {  if(raceTypeDescription=="STAKES") { %><span class="raceDisctxtName"><%=raceName%></span><% }  } %><span class="raceDisctxt"><%=raceTypeDescription%></span></span>
           <% }%>

            </span>
           </div>
			
			<% if(DRFPRO.getValue!=null && raceKey.trackId === "DMR"){ %>
				<ul class="btnEntriesRace">
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=raceKey.trackId%>&CY=<%=raceKey.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=raceKey.trackId%>&country=<%=raceKey.country%>&date=<%=dateForPP%>&session=<%=raceKey.dayEvening%>&race=<%=raceKey.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
 			<% }else{ %>
            	<ul class="btnEntriesRace">
                	<% if( (DRFPRO.dateType(this.selectedDate,this.currentDate) === "Today\'s")
                        && (['DMR', 'CD', 'FG', 'IND', 'AP'].indexOf(raceKey.trackId) == -1 ) && (mtp > -29) ){
                    %>
                    <li class="btnBetNow"><a class="drfProBetNow" href="https://bets.drf.com/wagers/<%=raceKey.trackId%>/<%=raceKey.raceNumber%>/<%=raceKey.country%>/<%=raceKey.dayEvening%>/?view=basicAnalysis" id=""
                     	target="_blank">BET NOW!</a></li>
                	<% }else{ %>
                	<li class="disabled btnBetNow"><a href="javascript:void(0);" id="">BET NOW!</a></li>						
                	<% } %>
                    <% if(DRFPRO.isMobile == true) { %>
                        <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/drfPastPerformanceRaces.do?TRK=<%=raceKey.trackId%>&CY=<%=raceKey.country%>&DATE=<%=dateForPP%>" target="_blank">+ GET PP's</a></li>
                    <% }else{ %>
					    <li class="NOmobileHide btnGetPp"><a class="drfProGetPP" href="http://www1.drf.com/formulator-web/Race.do?track=<%=raceKey.trackId%>&country=<%=raceKey.country%>&date=<%=dateForPP%>&session=<%=raceKey.dayEvening%>&race=<%=raceKey.raceNumber%>" target="_blank">+ GET PP's</a></li>
                    <% } %>
            	</ul>
			<% } %>

            <span class="entriesRaceChanges clearfix">
                            <%if(changes.length > 0 && changes[0].text!="No Changes"){%>
                            <span>Changes: </span>
                            <%=changes[0].text%><% if(changes.length > 1) { %><span> ...</span> <%} %> <br>
                            <% } else {%>
                            <span>No Changes</span>
                            <%}%>
                        </span>
            </div>
        </script>
<script type="text/template" id="template-entriesDetails-raceDescription">
            <div class="entriesRaceDisc">
            <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) {  if(raceTypeDescription=="STAKES") { %><span class="raceDisctxtName"><%=raceName%></span><% } } %>
            <% if(distanceDescription !== "") { if(raceTypeDescription !== "") { %>
            <%=aboutDistanceIndicator%> <%=distanceDescription +" | "%>
            <% if(raceRestrictionDescription !== null && raceRestrictionDescription !=="") { %>
            <%=raceRestrictionDescription +" | "%><%} if(sexRestrictionDescription!=="" && sexRestrictionDescription!==null) { %><%=sexRestrictionDescription+" | "%><% } if(ageRestrictionDescription!=="" && ageRestrictionDescription!==null) { %><%=ageRestrictionDescription+" | "%><% } %>
            <span class="raceDisctxtUp"><%if(typeof grade!=="undefined" && grade!=" " && grade!=null) { if(raceTypeDescription=="STAKES") {  %><%="G"+grade%>&nbsp;<% } }%><%=raceTypeDescription%><%if(minClaimPrice > 0) {%>
                <%=": <span>$"+ minClaimPriceDisplay%></span><%}%> | Purse: <span>$<%=purseDisplay%></span>
                </span>
            <span class="raceDisctxt"><%=wagerText%></span>
            <%} } else { %><span class="raceDisctxtUp"><%=raceTypeDescription%></span><% } %>
            </div>
        </script>
<script type="text/template" id="template-entriesDetails-articles">
            <% if(articles!=null) {  if(articlesCount!=null) { %>
            <div class="newsListWrap iosHideNewsWrap" id="RaceNews<%=raceNumber%>">
                <div class="newsHeader">
                    News and Updates <span class="newsCount"><%=articlesCount%></span><% if(articlesCount > 1) { %><a
                        href="/entries/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%>
                        <% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>" class="readAll" target="_blank">Read All</a><% } %>
                </div>
                <% } %>
                <div class="newsListCnt">
                    <ul class="clearfix  <% if(articles.nuggets.length==1) { %>onePost<% } else if(articles.nuggets.length==2) { %>twoPost<% } %>">
                      <% _.each(articles.nuggets ,function(article) { %>
                        <li><h3>
                            <% if(article.postType=="News"){ %>
                            <a  href="<%=article.articleUrl%>" target="_blank"><%=$.trim(article.title)%></a>
                         <% } else { %>
                            <a  href="/entries/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%>/articleId/<%=article.nid%>
                                <% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>"><%=$.trim(article.title)%></a>
                            <%} %>
                            <!--<i class="<% var classValue= DRFPRO.drupalLiveIconClass(article);
                            %><%=classValue%>"></i> ENTSRV-108 --></h3><%=article.teaser%>
                            <div class="postReadMore">
                                <% if(article.postType=="News"){ %>
                                <a href="<%=article.articleUrl%>" target="_blank" >Read <span class="txtReadMore">More</span> <span
                                        class="txtReadLess"
                                        >Less</span></a>
                                <% } else { %>
                                <a
                                        href="/entries/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%>/articleId/<%=article.nid%>
                                        <% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>">Read More</a>
                          <% } %>
                            </div>
                        </li>
                        <% }); %>
                    </ul>
                </div>
            </div>
            <% } %>
        </script>
<script  type="text/template" id="template-entriesDetails-raceDetailsTab">
            <span class="raceDisctxt"><%= wagerText %></span>
                <ul class="tabs clearfix">
                <li <%  if(activeTab==="#raceInfoTab"+trackNo){%> class="activeTab"<%}%>>
                    <a href="#raceInfoTab<%=trackNo%>" trackNo="<%=trackNo%>"
                       selectedDate="<%=selectedDate%>" class="tabLinks">Race Info</a></li>
                <li <%if(activeTab==="#analysisAndPicksTab"+trackNo){%> class="activeTab"<%}%>><a 
                        href="#analysisAndPicksTab<%=trackNo%>" trackNo="<%=trackNo%>"
                        selectedDate="<%=selectedDate%>"class="tabLinks drfProAnalysis">Analysis &
                    Picks</a></li>
                <li <%if(activeTab==="#closerLooksTab"+trackNo){%> class="activeTab"<%}%>><a 
                        href="#closerLooksTab<%=trackNo%>" trackNo="<%=trackNo%>"
                        selectedDate="<%=selectedDate%>" class="tabLinks drfProCloserLook">Closer looks</a></li>
                </ul>
        </script>

        <!-- -----------------------Entries Listing page---------------------------------- -->
<script  type="text/template" id="template-entriesWrap-headerTemplate">
        <div class="entriesHeader clearfix">
        <h4><%=dateType%> Entries <span><%=DRFPRO.displayDate(selectedDate)%> </span></h4>
        <ul class="btnTrack">
            <li id="myTrack" class="displayTracks disabled">My Tracks</li>
            <li id="allTrack" class="displayTracks activeTrack">All Tracks</li>
            </ul>
        </div>
        <div class="entriesBody tabContentWrap">
        <ul class="dataTable listHeader"> <li class="headRow">
            <span class="sCell valTrack"><span class="titleTrackFull">Tracks</span></span>
            <span class="sCell valChanges">Changes</span>
            <span class="sCell valWeather">Weather</span>
            </li></ul>
        <ul class="dataTable favouritesMsg"> <li>
            <span class="sCell message">
                <i class="icon-favArrow"></i>
                Mark tracks as favorites by clicking on the star before the track name
                <i class="icon-favClose"></i>
                </span>
            </li></ul>
        <ul class = "dataTable listBody" id = "trackList"></ul>
        </div>
    </script>
<script type="text/template" id="template-entriesWrap-trackTemplate">
          <li class="trackRow <%=priority%>" id="<%=trackId%>" title="<%=country%>" onclick="gaRefreshAds(this);">
          <span class="sCell valTrack">
          <a href="#"  class=<%if(priority === "high") {%>"icon-favorite" <% DRFPRO.trackCount++;
          }else {%> "icon-add-favorite" <%  }  %> id="favorite"></a>
          <span class="titleTrack">
          <a href="/entries/track/<%=trackId %>/country/<%=country%>/date/<%= date %>" id="<%=trackId %>"><%=trackName %></a>
          <span class="dividerLine"> | </span><a  class="trackView" href="/entries/track/<%=trackId %>/country/<%=country%>/date/<%= date %>" >View</a>
          </span>
          </span>
          <span class="sCell valChanges">
          <% if(typeof changes !== "undefined" && changes !== null && changes.length > 0) { %>
          <span class="mobileHide">
          <a class="raceChangesLink" href="/entries/changes/<%=trackId %>/country/<%=country %>/date/<%= date %>" > Race</a>  </span>
          <span class="Count">
          <a href="/entries/changes/<%=trackId %>/country/<%=country %>/date/<%= date %>/race/<%=changes[0]%>" id="race<%=changes[0]%>">
          <%=changes[0] %></a>
          <% if(changes.length>0) { for(var i=1;i<changes.length;i++) { %>
          <a href="/entries/changes/<%=trackId %>/country/<%=country %>/date/<%= date %>/race/<%=changes[i]%>" id="race<%=changes[i]%>">
           , <%=changes[i] %></a>
          <% if(i === 2 && changes.length > 3) { %><span>...</span><% break; } } }%>
          </span>
          <span class="dots"></span>
          <span class="dividerLine"> |</span>
          <a href="/entries/changes/<%=trackId %>/country/<%=country %>/date/<%= date %>" > View</a>
          </span>
          <% }  else {%><span class="valCenter valNoChange">No changes</span> <% } %>
          </span>
          <span class="sCell valWeather">
          <% if(typeof weather !== "undefined" && weather !== null && weather.forecastHigh !== -999) { %>
          <i class="iconWeather"> <img src="/images/iconW<%=weather.forecastIconId%>.png" alt="" /></i>
          <span>High:</span><%if(weather.forecastHigh !== -999) { %> <%=weather.forecastHigh%><span class="valDegree">&deg;F</span>
          <% } else {%> <span class="noWeather">-</span> <%}%>
          <span>Low:</span><%if(weather !== null && weather.forecastLow !== -999) { %> <%=weather.forecastLow%><span class="valDegree">&deg;F</span>
          <%} else { %> <span class="noWeather">-</span> <%} %>
          <%} else {%> <span class="valCenter">--</span> <% }%>
          </span></li>
  </script>
        <!-- -------------------------- Calender template ------------------------->
<script type="text/template" id="template-calender">
         <li class=<%=className%>>
                <a href = "<%= redirectionUrl%><%= monthIDLink %>-<%= dateLink %>-<%= year %>"
				onclick="gaRefreshCalendarAds(this);" 
                class="date_link" id="<%= monthID %>-<%= date %>-<%= year %>">
                <span class="month"><%= month %></span>
                <span class="date"><%= date %></span>
                <span class="day"> <%= day %></span>
                </a>
                </li>
              </script>

<!-- --------------------------Promotional Screen--------------------------->

<script type="text/template" id="template-promotional-promotionalScreen">
            <div class="promoWrap clearfix">
                <!-- start Main Content -->
                <section class="pageData">
                    <div class="btnSkip">
                        <a href="<%=siteLink%><% if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>">Continue to Site</a>
                    </div>
                    <div class="promoHead">
                        <h2>Try our free mobile app!</h2>
                        <p>See more customized Entries, Results
                            and Live Odds from Daily Racing Form</p>
                    </div>
                    <div class="promoOs btnPlayStore">
                        <a href="https://play.google.com/store/apps/details?id=com.drf.pro" target="_blank">
                            <img src="../images/btn-play-store.png" alt="">
                        </a>
                    </div>
                    <div class="promoOs btnIos">
                        <a href="https://itunes.apple.com/us/app/daily-racing-form-pro/id915817054?mt=8" target="_blank">
                            <img src="../images/btn-ios-store.png" alt="">
                        </a>
                    </div>
                </section>
                <!-- end Main Content -->
            </div>
        </script>
<script type="text/template" id="template-premiumMemberPlans">
            <div class="loginBody clearfix">
               <div class="centerUpsell">
        	  <div class="bigAd">
            	<a href="http://www.drf.com/free-plus" target="_blank"><img src="/images/bigAd.png"></a>
            	<br>
              </div>
              <div class="smallAd">
            	<a href="http://drfbets.com/rewards" target="_blank"><img src="/images/smallAd.png"></a>
            	<br>
              </div>
            </div>                 
          </div>
            
</script>
<!-- ------------------------Race Replayes---------------------------------------------------------->
<script type="text/template" id="template-resultsDetails-raceReplayes">
   <% if(typeof raceReplayId !=="undefined" && raceReplayId!=null && raceReplayId!="") { %>
    <div class="raceReplayWrap" id="raceReplay<%=raceNumber%>">
        <div class="raceReplayHeader">
            Race Replay
            <i class="icon-togglePlus"></i>
        </div>
        <div class="raceReplayCnt">
            <div class="videoWrapper">
                <div class="iframeVideo">
                    <% var replayUrl = DRFPRO.showRaceReplayVideo(raceReplayId,'desktop')%>
                    <iframe id="replay<%=raceNumber%>"  replayUrlLink="<%=replayUrl%>" width="480" height="360"
                            frameborder="0" allowfullscreen="" scrolling="no"></iframe>
                </div>
            </div>
        </div>
    </div>
    <% } %>
</script>

<!-- -------------------------- resultWrapDetails template ------------------------->
<script type="text/template" id="template-resultWrap-raceHeaderTemplate">
    <div class="entriesRaceHeader clearfix" id="Race<%=raceKey.raceNumber%>">
        <ul class="btnEntriesRace mobileHide">
		  <%if(raceKey.country !== "HK") {%>
            <li class="btnRaceChart">
                <a href="http://www1.drf.com/drfPDFChartRacesIndexAction.do?TRK=<%=raceKey.trackId%>&CTY=<%=raceKey.country%>&DATE=<%=date%>&RN=<%=raceKey.raceNumber %>" target="_blank">Race Chart</a>
            </li>
		  <%}%>
        </ul>
        <div class="raceHeaderDtl">
            <h3>Race <%=raceKey.raceNumber %></h3>
            <span class="mtp">
                        <% if(typeof postTime !== "undefined" && postTime !== null && postTime !==-999) { %>
						<% if(DRFPRO.getValue!=null){ %>
                          <span class="mtpBlock">Post: <span><%=(DRFPRO.localPost(postTime)).slice(0, -2)%></span><span class="valPostET"><%=(DRFPRO.localPost(postTime)).slice(-2)%></span></span>
						<% }else{ %>
						  <span class="mtpBlock">Post: <span><%=postTime%></span><span class="valPostET"> ET</span></span>
						<% } %>
                        <% } %>
                        </span>
            <span class="entriesRaceDiscRight">
                        <%if(typeof surfaceDescription !== 'undefined') {%>
                        <span <%if(surfaceDescription === "Turf" || surfaceDescription === "Inner turf") {%> class="sCell valTurf" ','<% } else {%> class="sCell" <%}%> ><%=surfaceDescription%> </span> <%}%>
            <% if(trackConditionDescription){ %> - <%=trackConditionDescription%> <% } %>
            </span>
        </div>
        <div class="mobileShow clearfix"></div>
        <span class="entriesRaceDiscMobile">
                <% if(distanceDescription === "") { %>
                <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) { if(raceTypeDescription=="STAKES") {  %><span class="raceDisctxtName"><%=raceName%></span><% } } %>
                <span class="raceDisctxtUp">
                <% if (raceTypeDescription !== "") {
                 if (raceTypeDescription.search("year olds and up") !== -1) {
                 var upText=raceTypeDescription.replace("year olds and up","<span></span>");  %> <%=upText%><% }
                else { %> <%=raceTypeDescription%> <%}%>
                </span>
                <% } } else { if(distanceDescription !== "")  { %>
                <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) { if(raceTypeDescription=="STAKES") {  %><span class="raceDisctxtName"><%=raceName%></span><% } } %>
                <span class="raceDisctxtUp">
                <%=aboutDistanceIndicator%> <%=distanceDescription+" | " %>
                <%  if(sexRestrictionDescription !== "") { %><%=sexRestrictionDescription+" | "%> <% }
                 if(ageRestrictionDescription !== "")  {
                 if (ageRestrictionDescription.search("Year Olds And Up") !== -1) {
                 var upText=ageRestrictionDescription.replace("Year Olds And Up","<span></span>");  %><%=upText%><% }
                 else { %><%=ageRestrictionDescription%><% } } %>
                </span>
                <span class="raceDisctxt">
                <%if(typeof grade!=="undefined" &&  grade!=" " && grade!=null) { if(raceTypeDescription=="STAKES") { %><%="G"+grade%>&nbsp;<% } }%><% if(raceTypeDescription){ %> <span class="mobileHide">|</span> <span class="MClaimingBold"><%=$.trim(raceTypeDescription)%></span><% }%>
                <% if(typeof minimumClaimPrice !== "undefined" &&  Math.round(minimumClaimPrice) !== 0 && minimumClaimPrice!=="") { %>: <span>$<%=minimumClaimPrice%></span> <% } %>
                <%   if(typeof totalPurse !== "undefined" ||  Math.round(totalPurse) !== 0 || totalPurse !== "") { %> | Purse: <span>$<%=totalPurse%></span><% } } } %>
                </span>
         </span>

    </div>
</script>

<script type="text/template" id="template-resultWrap-raceDescriptionTemplate">
    <div class="entriesRaceDisc">
    <% if(typeof raceName !=="undefined" && raceName !=="" &&  raceName !==null) { if(raceTypeDescription=="STAKES") {  %><span class="raceDisctxtName"><%=raceName%></span><% } } %>
    <% if(distanceDescription === "") { if(raceTypeDescription !== "") { %>
    <span><%=raceTypeDescription%></span>
    <% } } else { if(distanceDescription !== "")  { %><%=aboutDistanceIndicator%> <%=distanceDescription+" | "%>
    <% } if(raceRestrictionDescription !== null && $.trim(raceRestrictionDescription) !== "Open") { %>
    <%=raceRestrictionDescription+" | " %>
    <% }  if(typeof sexRestrictionDescription !== "undefined" &&  sexRestrictionDescription !== "") { %>
    <%=sexRestrictionDescription+" | " %>
    <%  } %>
    <% if(typeof ageRestrictionDescription !== "undefined" && ageRestrictionDescription !== "")  { %>
    <%=ageRestrictionDescription+"| "%><% } %>
    <span class="raceDisctxtUp">
                <%if(typeof grade!=="undefined" && grade!=" " && grade!=null) { if(raceTypeDescription=="STAKES") { %><%="G"+grade%>&nbsp;<% } }%><% if(typeof raceTypeDescription !== "undefined" || raceTypeDescription !== ""){%> <%=raceTypeDescription%><% }%>
                <% if(typeof minimumClaimPrice !== "undefined" && Math.round(minimumClaimPrice) !== 0 && minimumClaimPrice!=="") { %>: $<%=minimumClaimPrice%> <% } %>
                <%   if(Math.round(totalPurse) !== 0 && totalPurse!=="") { %> | Purse:<span> $<%=totalPurse%></span><% } %>
                </span>
    <% } %></div>
</script>
<script type="text/template" id="template-resultWrap-raceFooter">
    <ul class="dataTable listFooter" >
    <li><span class="sCell valentDtlsbtm valResDtlsbtm">
                <div class="valResinfo valResTime">
                <span class="valResTimeinfo">Times:</span>
              <span><% if(fraction !== undefined && fraction !== null && (fraction.fraction1!==null &&  fraction.fraction1!=="" || (fraction.winningTime!=null && fraction.winningTime!=""))) { %>
                <strong>Times in 5ths: </strong>
                <% _.each(fraction,function (fractionList){ if(fractionList && fractionList.timeInFifths !== undefined) { %>
                <%= fractionList.timeInFifths+"  "%>&nbsp;<%  } }); } %>
                </span>
            <span><% if(fraction!== undefined && fraction!=null && (fraction.fraction1!==null &&  fraction.fraction1!=="" || (fraction.winningTime!=null && fraction.winningTime!=""))) { %>
                <strong> Times in 100ths: </strong>
                <% _.each(fraction,function (fractionList){ if(fractionList && fractionList.timeInHundredths !== undefined) { %>
                <%=fractionList.timeInHundredths+"  "%>&nbsp;<% } }); } %>
                </span>
                </div>
                <div class="valResinfo">
            <span class="valResWinnerinfo">Winner Information:</span>
            <span>
                <%if(typeof runners[0]!="undefined") {
                if(typeof runners[0].trainerLastName!="undefined" && runners[0].trainerLastName!=null || typeof runners[0].trainerFirstName!="undefined" && runners[0].trainerFirstName!=null) { %>
                <strong><span class="mobileHide">Winning</span> Trainer: </strong><%= runners[0].trainerLastName+""+runners[0].trainerFirstName %>
                <% } }  if(typeof runners[0]!="undefined")
                { if((typeof runners[0].ownerLastName!="undefined" && runners[0].ownerLastName!=null && runners[0].ownerLastName!="") || (typeof runners[0].ownerFirstName!="undefined" && runners[0].ownerFirstName!=null && runners[0].ownerFirstName!="")) { %> | <strong>Owner: </strong>
                <%=runners[0].ownerLastName+""+runners[0].ownerFirstName %><% } } %>
                </span>
            <span><% if(typeof runners[0]!="undefined") { if(typeof runners[0].sireName!="undefined" && runners[0].sireName!=null) { var sire=runners[0].sireName; %>
                <strong><span class="mobileHide">Winning </span> Sire: </strong><%
if(typeof sireLink !="undefined"  && sireLink !=null && typeof sireLink[sire]!="undefined" && sireLink[sire]!=null){%><a style="font-weight: bold; text-decoration: underline;font-family: sans-serif; color:rgb(250, 97, 97)" href=<%=sireLink[sire] %> target="_blank"><%=runners[0].sireName %></a> <%} else {%>  <%=runners[0].sireName %> <% } %>



                <%  }  }  if(typeof runners[0]!="undefined") { if(typeof runners[0].breederName!="undefined" && runners[0].breederName!=null) { %>
                 | <strong>Breeder: </strong><%=runners[0].breederName %><% } } %>
                </span>
                </div>

                <div class="valResinfo">
            <%if(typeof scratches[0]!=undefined && scratches[0]!=null){%>
            <span><strong>Changes: </strong><% for(i=0;i<scratches.length;i++) { %><%=scratches[i]%>
                <% if(scratches.length!=1 && i!=scratches.length-1){ %>,<%}%> <% } %> Scratched
                </span>
            <%} %>
            <span><% if(claims !== null && typeof claims[0] !== "undefined" &&  claims[0] != null) {%>
                <strong>Claimed: </strong><% for(i=0;i<claims.length;i++) { %>
                <%=claims[i].horseName+" by "+claims[i].trainerLastName+" "+claims[i].trainerFirstName+" ("+claims[i].ownerLastName+")"%>
                <% if(claims.length-1!=i) { %> and <% } } } %>
                </span>
            </div>
            <div class="valResinfo">
                <% if(typeof alsoRan !== "undefined" &&  alsoRan != null) { %>
                <span class="valResUnplaced">Unplaced horses listed in order of finish:</span>
                <span><strong>Also ran: </strong><%=alsoRan%></span><% } %>
                <%if(payoffs.length > 0 && payoffs !== "") {  %>
            </div>
            <div class="valResinfo valRespPayOff">
                <span><strong>Pay offs: </strong>
                <% for(i=0;i<payoffs.length;i++){
                 if(typeof payoffs[i].wagerName!="undefined" && payoffs[i].wagerName != null || payoffs[i].wagerName != " ")  {
                 var baseAmount = DRFPRO.payoffBaseAmount(payoffs[i].wagerName,wagerTypes);
                if(baseAmount!=="" || baseAmount!==0) { %> $<%=baseAmount+"  "%>
                <% } %><%=payoffs[i].wagerName+"  ("+payoffs[i].winningNumbers+ ") "%>
                <% if(payoffs[i].numberOfRights>0) {  %><%="("+payoffs[i].numberOfRights+" Correct) "%><% }
                    %><%="Paid: $"+payoffs[i].payoffAmount+" "  %>
                <% var wagerType = DRFPRO.horizontalWager(payoffs[i].wagerName); if(wagerType === true){ %>
                <%=payoffs[i].wagerName +" Pool $"+payoffs[i].totalPool%> <%}%>
                <% if(payoffs.length!=1 && i!=payoffs.length-1){ %>,
                 <%} } }  %> </span> <%} %>
            </div>
                </span></li>
    </ul>
    <%if(typeof raceRecaps!=="undefined" && raceRecaps!=="" && raceRecaps!==null) { %>
    <div class="raceRecapWrap">
        <div class="raceRecapHead">
            Race Recaps
            <i class="icon-togglePlus"></i>
        </div>
        <div class="raceRecapDtls">
            <p><%=raceRecaps%></p>
        </div>
    </div>
    <% } %>
</script>
<script type="text/template" id="template-resultWrap-raceHeadRow">
    <ul class="btnEntriesRace mobileShow">
	  <%if(country !== "HK") {%>
        <li class="btnRaceChart">
            <a href="http://www1.drf.com/drfPDFChartRacesIndexAction.do?TRK=<%=trackId%>&CTY=<%=country%>&DATE=<%=date%>&RN=<%=raceNumber%>" target="_blank">Race Chart</a>
        </li>
	  <%}%>
    </ul>
    <ul class="dataTable listHeader">
        <li class="headRow">
            <span class="sCell valResDtlsNum"><span class="valNumHead">#</span></span>
            <span class="sCell valResDtlsHorse">Horse<span class="mobileShow">/ Jockey</span></span>
            <span class="sCell valResDtlsJockey">Jockey<span> Weight</span></span>
            <span class="sCell valResDtlsTrainer">Trainer</span>
            <span class="sCell valResDtlsWin">Win $</span>
            <span class="sCell valResDtlsPlace">Place $</span>
            <span class="sCell valResDtlsShow">Show $</span>
<!--            <span class="sCell valResDtlsBeyer">Beyer</span>-->
        </li>
    </ul>
</script>
<script type="text/template" id="template-resultWrap-horseTemplate">
                <span class="sCell valResDtlsNum">
                <span class="oddsNo saddle<%= parseInt($.trim(programNumber),10) %>"><%=programNumber%></span>
                </span>
                <span class="sCell valResDtlsHorse"><span class="valHorseName"><%=horseName %></span><span class="mobileShow">
                <span class="entDtlsJockey"><span>J: </span>
              <% if(jockeyFirstNameInitial!=null) { %><%=jockeyFirstNameInitial+". "%>
                <% } if(jockeyLastName=="") { var part=jockeyFirstName.split(" ");  for(var i=0;i<part.length;i++) {
                if(i!=0) { %><%=""+part[i]%><%  } } } else { %>
                <%=jockeyLastName %><% } %>
                <% if(weightCarried!=="" && weightCarried!==null) { %><span class="jockeyWt"> <%=weightCarried%>Lbs</span><% } %></span>
                <span class="entDtlsTrainer"><span>T: </span>
                <% if(typeof trainerFirstName!="undefined" && trainerFirstName!="") { %>
                <%=trainerFirstName.slice(0,1)+". "%>
                <% if(trainerLastName=="") {  var part=trainerFirstName.split(" ");
                for(var i=0;i<part.length;i++) { if(i!=0) { %><%=" "+part[i]%><%  } } } else { %>
                <%=trainerLastName %><% } } else { %>-<% } %>
                </span>
                </span></span>
        <span class="sCell valResDtlsJockey"><% if(jockeyFirstNameInitial!=null) { %><%=jockeyFirstNameInitial+". "%>
                <% }  if(jockeyLastName=="") { var part=jockeyFirstName.split(" ");for(var i=0;i<part.length;i++) { if(i!=0) { %><%=part[i]%>
                <%  } } } else { %><%=jockeyLastName %><% } %><% if(weightCarried!=="" && weightCarried!==null) { %><span><%=weightCarried%>Lbs</span><% } %>
                </span>
        <span class="sCell valResDtlsTrainer">
                <% if(typeof trainerFirstName!="undefined" && trainerFirstName!="") { %><%=trainerFirstName.slice(0,1)+". "%>
                <% if(trainerLastName=="") {  var part=trainerFirstName.split(" ");  for(var i=0;i<part.length;i++) { if(i!=0) { %>
                <%=" "+part[i]%><%  } } } else { %><%=trainerLastName %><% } } else { %>-<% } %>
                </span>
        <span class="sCell valResDtlsWin"><% if(winPayoff!="0") { %><%=winPayoff.toFixed(2)%><% } else { %>&nbsp;<%} %></span>
        <span class="sCell valResDtlsPlace"><% if(placePayoff!="0") { %><%=placePayoff.toFixed(2) %><% } else { %>&nbsp;<%} %></span>
        <span class="sCell valResDtlsShow"><% if(showPayoff!="0") { %><%=showPayoff.toFixed(2) %><% } else { %>&nbsp;<%} %></span>
      <!--  <span class="sCell valResDtlsBeyer"><% if((typeof beyerValue !== "undefined") && beyerValue!=0 && beyerValue!=null) {
            %>$<%=beyerValue %><% } else {%>--<% } %></span>-->
</script>
<script type="text/template" id="template-resultWrap-parValue">
    <ul class="dataTable listPar">
        <li>
            <span class="sCell valResDtlsParBlank"></span>
            <span class="sCell valResDtlsPar">PAR:</span>
            <span class="sCell valResDtlsParAmt"><%=par%></span>
        </li>
    </ul>
</script>
<!-------------------------------------------------ResultsView ---------------------------------------->
<script type="text/template" id="template-resultList-headerTemplate">
    <div class="entriesHeader clearfix">
        <h4><%=dateType%> Results<span><%=DRFPRO.displayDate(selectedDate)%></span></h4>
        <ul class="btnTrack">
            <li id="myTrack" class="displayTracks disabled">My Tracks</li>
            <li id="allTrack" class="displayTracks activeTrack">All Tracks</li>
        </ul>
    </div>
    <div class="entriesBody">
        <ul class="dataTable listHeader">

            <li class="headRow"><span class="sCell valResTrack"><span class="titleTrackFull">Tracks</span></span>
                <span class="sCell valResCurRace">Current Race</span><span
                        class="sCell valResPrevRaces">Previous Races</span></li>
        </ul>
        <ul class="dataTable favouritesMsg">
            <li><span class="sCell message">
                <i class="icon-favArrow"></i>Mark tracks as favorites by clicking on the star before the track name<i
                    class="icon-favClose"></i>
                </span></li>
        </ul>
        <ul class="dataTable listBody" id="trackList"></ul>
    </div>
</script>
<script type="text/template" id="template-resultList-trackTemplate">
    <li class="trackRow <%=priority%>" id="<%=trackId%>" title="<%=country%>" onclick="gaRefreshAds(this);">
                <span class="sCell valTrack valResTrack">

                <a href="#" class=<%if(priority === "high"){%>"icon-favorite" <% DRFPRO.trackCount++; }else {%> "icon-add-favorite" <%}%> id="favorite"></a>

                <span class="titleTrack">
                <a href="/results/tracks/<%=trackId %>/country/<%=country %>/date/<%= date %>" id="<%=trackId %>"
                   class="track_details"><%=trackName %></a>
                <span class="dividerLine"> | </span><a class="trackView"
                                                       href="/results/tracks/<%=trackId %>/country/<%=country %>/date/<%= date %>">View</a>
                </span>
                </span><% if(typeof currentRace === "undefined" && typeof postTime === "undefined" || currentRace ===
        null && postTime === null) { %>
        <span class="sCell valResCurRace"><span></span>-</span>
                <span class="sCell valResPrevRaces">
                <span class="mobileHide"></span><a href="#" id="race"></a><a href="#" id="race">--</a>
                <span class="dividerLine"></span><a href="#"></a>
                </span><% } else { %>
                <span class="sCell valResCurRace"><% if(currentRace && postTime) { %>
                <span><%=currentRace.ordinalSuffix() %> Post </span><%=postTime %> <span class="valPostET">ET</span><% } else { %><span
                            class="valCenter">--</span>
                <% } %></span>
                <span class="sCell valResPrevRaces">
                <% if(previousRaces !== null && previousRaces.length > 0) { %>
                <span class="mobileHide">Race </span>
                <span class="Count">
                <a href="/results/tracks/<%=trackId %>/country/<%=country %>/date/<%= date %>/Race/<%=previousRaces[0]%>" 
                   id="race<%=previousRaces[0]%>">
                    <%=previousRaces[0] %></a>
                <% if(previousRaces.length>0) { for(var i=1;i<previousRaces.length;i++){ %>
                <a href="/results/tracks/<%=trackId %>/country/<%=country %>/date/<%= date %>/Race/<%=previousRaces[i]%>" 
                   id="race<%=previousRaces[i]%>">, <%=previousRaces[i] %></a>
                <% } }%>
                </span> <%  } else { %>
                <span class="valCenter">--</span>
                <% } %>
                </span> <% } %>
    </li>
</script>
<script type="text/template" id="template-resultsDetails-articles">
    <% if(articles!=null) { %>
    <div class="newsListWrap iosHideNewsWrap" id="RaceNews<%=raceNumber%>">
        <% if(articlesCount!="") { %>
        <div class="newsHeader">
            News and Updates <span class="newsCount"><%=articlesCount%></span><% if(articlesCount > 1) { %><a
                href="/results/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>" class="readAll" target="_blank">Read All</a><% } %>
        </div>
        <% } %>
        <div class="newsListCnt">
            <ul  class="clearfix  <% if(articles.length==1) { %>onePost<% } else if(articles.length==2) { %>twoPost<% } %>">
                <%  _.each(articles ,function(article) { %>
                <li>
                    <h3>
                        <% if(article.postType=="News"){ %>
                        <a  href="<%=article.articleUrl%>" target="_blank"><%=$.trim(article.title)%></a>
                        <% } else { %>
                        <a href="/results/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%>/articleId/<%=article.nid%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>"><%=article.title%></a>
                        <%} %>
                        <!--<i class="<% var classValue= DRFPRO.drupalLiveIconClass(article);
                            %><%=classValue%>"></i> ENTSRV-108 --></h3><%=article.teaser%>
                    <div class="postReadMore">
                        <% if(article.postType=="News"){ %>
                        <a href="<%=article.articleUrl%>" target="_blank" >Read <span class="txtReadMore">More</span> <span
                                class="txtReadLess"
                                >Less</span></a>
                        <% } else { %>
                        <a href="/results/article/<%=trackId%>/raceNumber/<%=raceNumber%>/country/<%=country%>/date/<%=date%>/articleId/<%=article.nid%><% if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) {%><%='?bets='+DRFPRO.getValueBets %> <% } %>">Read More</a>
                        <% } %>
                    </div>
                </li>
                <% }); %>
            </ul>
        </div>
    </div>
    <% } %>
</script>

<!---------------------------------------------Result Article Details-------------------------------------->
<script type="text/template" id="template-result-article-premiumMsg">
    <ul class="premiumBeyerMsg">
        <li><span class="beyerMessage">This feature is not available with your current subscription level.</span>
            <a href="/results/premium/<%=trackId%>/country/<%=country%>/date/<%=date%>/Race/<%=raceNumber%>/articleId/<%=articleId%>">Upgrade Here!</a></li>
    </ul>
</script>
<script type="text/template" id="template-result-article-nonLoggedInUser">
    <div class="premiumCnt">
        <div class="loginMsg loginAccMsg"><span class="alreadyAcc">Already have an account?</span>
            <% var userAgent =DRFPRO.ieDetector(); if(DRFPRO.getValue!=null && userAgent=="IE"){  %>
            <a id="userLogin" href="http://www.drf.com" target="_blank" class="btnLogin">Log In to DRF.com</a>
            <span class="dontAcc"><span>Don't have an account?</span> <a
                    href="https://store.drf.com/stores/1/register.cfm" target="_blank" >Click here to create one. </a></span>
            <%  }else { %>
            <a id="userLogin"
               url="<%=DRFPRO.utils.getBaseURL()+'/race-results/'+DRFPRO.utils.getReturnURLArticles(trackId,country,date,raceNumber,articleId)%>
              <%  if(DRFPRO.getValue!=null) { %><%='?header='+DRFPRO.getValue %> <% } if(DRFPRO.getValueBets!=null) { %>
            <%='?bets='+DRFPRO.getValueBets %><% } %>"
               href="javascript:void(0)" class="btnLogin loginEncryptUrl">Log In to DRF.com</a><span class="dontAcc"><span>Don't have
                an
                account?</span>
                <a href="https://store.drf.com/stores/1/register.cfm" target="_blank" >Click here to create one. </a></span>
            <% }  %>
        </div>
</script><IFRAME SRC="http://www.imiclk.com/cgi/r.cgi?m=3&mid=LP1L9M4b&did=results" FRAMEBORDER="0" SCROLLING="NO" WIDTH="0" HEIGHT="0"> </IFRAME>
<script src = "https://pro.drf.com/js/vendor/MD5.js"></script>
<script src = "https://pro.drf.com/js/custom.js"></script>

<script type=text/javascript>
    $('#results').parent().addClass('active');
    $('#entries').parent().removeClass('active');
    $('#live-odds').parent().removeClass('active');
    if(DRFPRO.getValue!=null){
        $('.backButton').hide();
        $('.mback').hide();
    }
</script>
<div id="radiumResults">
  <!--Radium One Pixel for Results-->
  <script>
  (function(d,p){var a=new Image();a.onload=function() {a=null};a.src=(d.location.protocol=="https:"?"https:":"http:")+"//rs.gwallet.com/r1/pixel/x"+p+"r"+Math.round(1E9*Math.random())})(document,"34408")
  </script>
  <noscript>
    <img src="https://rs.gwallet.com/r1/pixel/x34408" width="0" height="0" style="display:none;visibility:hidden"/>
  </noscript>
  <!--End Radium One Pixel for Results-->
</div>            <!-- end View Content-->

        </section>
    </div>
    <!-- end Main Content -->
</div>
<script>
    $(document).ready(function(){
    $('.loginHide').css('display','none');
    $('.sidr-class-mobileLogin').css('display','none');
        $('.drfLogout').attr('href',DRFPRO.utils.getBaseURL()+'/logout?bets=true');
        $('.sidr-class-drfLogout').attr('href',DRFPRO.utils.getBaseURL()+'/logout?bets=true');
    });
    $('.loginHeader').attr('href','https://access.drf.com?refferUrl='+encodeURIComponent("https://pro.drf.com/users/login"));
</script>
<script type="text/javascript" src="/js/app/abstractObjects.js"></script>
<script type="text/javascript" src="/js/app/results/model.js"></script>
<script type="text/javascript" src="/js/app/results/collection.js"></script>
<script type="text/javascript" src="/js/app/results/view.js"></script>
<script type="text/javascript" src="/js/app/results/router.js"></script>
</body>
</html>
