/**
 * Created by Webonise Lab.
 * User: Priyanka Bhoir <priyanka.bhoir@weboniselab.com>
 * Date: 3/4/14 11:02 PM
 */
var DRFPRO = DRFPRO || {};
DRFPRO.appConst = DRFPRO.appConst || {};

DRFPRO.abstractObjects =  DRFPRO.abstractObjects || {};

DRFPRO.appConst = {
    activeRaceAccordion: null,
    activeHorseAccordion: []
};

DRFPRO.abstractObjects = (function () {
    return {
        trackListFunctions: {
            render: function () {
                var fragment = [],
                    self = this;
                currentTime = this.model.get('currentTime');
                if(DRFPRO.results && currentTime >=12 ){
                    if(DRFPRO.results.actualSelectedDate){
                        DRFPRO.triggerLink('/results/selectedDate/'+DRFPRO.results.actualSelectedDate);
                    }
                }
                this.parent.calender.setDates(this.model.get('dates'));
                this.listBackButtons(this.url);
                if(DRFPRO.isAvailable) {

                    this.$el.html(this.headerTemplate({
                        selectedDate: this.selectedDate,
                        dateType: DRFPRO.dateType(this.selectedDate, this.currentDate)
                    }));

                    if(this.collection.length > 0) {
                        DRFPRO.trackCount = 0;

                        _.each(this.collection.models, function (model) {
                            this.stopListening(model);
                            this.listenTo(model, 'change:priority', this.toggleSuccess);
                            model.set('date', this.selectedDate);
                            fragment.push(this.trackTemplate(model.toJSON()));
                        }, this);
                        $('#trackList').append(fragment.join(""));

                        if(DRFPRO.trackCount > 0) {
                            $('#myTrack').removeClass('disabled');
                            $("ul.favouritesMsg").hide();
                            DRFPRO.favClose = true;
                        } else {
                            this.$('#myTrack').addClass('disabled');
                            if(DRFPRO.favClose === false && !DRFPRO.utils.readCookie('favClose') === true) {
                                $("ul.favouritesMsg").show();
                            } else {
                                $("ul.favouritesMsg").hide();
                            }
                        }
                        DRFPRO.enableMytrack(this);

                    } else {
                        this.$el.append("<div class='errorMsg'>No races are available for the selected date.</div>");
                        $('.entriesBody').hide();
                        $(".favouritesMsg").hide();
                        $('#myTrack').hide();
                        $('#allTrack').hide();
                    }
                } else {
                    this.$el.html("<div class='errorMsg'>No races are available for the selected date.</div>");
                    $('.entriesBody').hide();
                    $('#myTrack').hide();
                    $('#allTrack').hide();
                }
                $('.loading').fadeOut(500);

            },

            showHideTracks: function (e) {
                if($(e.currentTarget).hasClass('disabled')){
                    return false;
                }
                var el = '#trackList';
                var collectionLength = this.collection.length;
                DRFPRO.showHideTrack(e, el, collectionLength);
            },

            selectRow: function (e) {
                var clickedElement = $(e.currentTarget);
                var actualElement = $(e.target);

                if(((actualElement).attr('id')) !== 'favorite' && !(actualElement.is('a'))) {
                    var linkToTrigger = clickedElement.find('.valTrack .titleTrack a').attr('href');
                    DRFPRO.triggerLink(linkToTrigger);
                } else {
                    return true;
                }
            },

            toggleFavorite: function (e) {
                if(typeof app.request !== 'undefined') {
                    app.request.abort();
                }
                var clickedEl = $(e.currentTarget),
                    self = this;
                $('.loading').show();

                if(clickedEl.hasClass('icon-add-favorite')) {
                    $("ul.favouritesMsg .icon-favClose").trigger('click');
                }
                var id = clickedEl.parents('li').prop('id');
                if (window.localStorage) {
                     var model = this.collection.get(id);
                     if(window.localStorage.getItem('favouriteTracks') !== null){
                        var arr=JSON.parse( localStorage.getItem('favouriteTracks'));
                         var index =  $.inArray(id,arr);
                         if (index > -1) {
                             arr.splice(index, 1);
                             model.set('priority','low');
                         }
                         else{
                             arr.push(id);
                             model.set('priority','high');
                         }
                         localStorage.setItem('favouriteTracks',JSON.stringify(arr));
                     }else
                     {
                         localStorage.setItem('favouriteTracks',JSON.stringify([id]));
                         model.set('priority','high');
                     }
                    this.collection.sort();
                }

                return false;
            },

            toggleSuccess: function () {
                this.collection.sort();

            },

            listBackButtons: function (url) {
                $(".entriesDtlsWrap").html('');
                $('.loading').fadeOut(550);
                $(".backButton").addClass('hidden');
                var linkSelectorObj =$("a.mback");
                linkSelectorObj.prop("href", DRFPRO.utils.getBaseURL() + url);
                linkSelectorObj.html('<i class = "icon-mBack"></i> Home');
                linkSelectorObj.addClass('hidden');
            }

        },

        calenderFunctions: {
            setDates: function (dates) {
                if((this.availableDates.difference(dates)).length > 0 || (dates.difference(this.availableDates)).length > 0) {
                    this.availableDates = dates;
                    this.collection.reset();
                    this.collection.add(this.createDateArray(this.availableDates));
                    scroll.destroy('.dateWrap');
                    dates = [];
                    this.render();
                }
            },

            render: function () {
                var self = this,
                    fragment = '';
                this.$el.html('');

                this.collection.each(function (model) {
                    model.set('redirectionUrl',self.redirectionUrl);
                    model.set('monthIDLink', (model.get('monthID') < 10 ? "0" : "") + model.get('monthID'));
                    model.set('dateLink', (model.get('date') < 10 ? "0" : "") + model.get('date'));
                    if(!model.get('className')) {
                        model.set('className','');
                    }
                    fragment += self.template(model.toJSON())
                });
                this.$el.append(fragment);
                if(!DRFPRO.isAvailable) {
                    this.$el.html('');
                } else if(this.$el.children().length > 0) {
                    /*re init calender scroll*/
                    scroll.init('.dateWrap');
                }

                return this;
            },

            /**
             * @description: creates objects of date to be display in calender
             * @param datesAvailable
             * @return {Array}
             */
            createDateArray: function (datesAvailable) {
                this.isAvailable = false;
                var self = this;
                var count = 0;
                var dateArray = new Array();
                if(datesAvailable) {
                    _.each(datesAvailable, function (dateAvailable) {
                        from = dateAvailable.split(/\D/);
                        var date = new Date(from[2], from[0] - 1, from[1])
                        dateArray[count] = DRFPRO.utils.createDateForCalender(date);
                        count = count + 1;
                    });

                } else {
                    var currentDateString = DRFPRO.entries.currentDate.split(/\D/);
                    var currentDate = new Date(currentDateString[2], --currentDateString[0], currentDateString[1]);
                    for(var i = -30; i <= 7; i++) {
                        var date = new Date(new Date().setDate(currentDate.getDate() + i));
                        dateArray[count] = DRFPRO.utils.createDateForCalender(date);
                        count = count + 1;

                    }
                }
                return dateArray;
            }
        },

        trackDetailFunctions: {
            render: function () {
                var self = this,
                    entriesDetailsHeaderView,
                    racesFragment = [],
                    page,
                    isNew = false;
                if(this.model.get('isData') === true && this.model.get('races') !== false) {
                    this.trackDetailsBackButtons();
                    if(_.contains(JSON.parse(localStorage.getItem('favouriteTracks')), this.model.get('weather').trackId) === true){
                        this.model.set('priority','high');
                    }else{
                        this.model.set('priority','low');
                    }

                    this.favoriteTrackModel.set({trackId: this.model.get('weather').trackId,
                        date: this.selectedDate});

                    this.$el.html('');
                    if(DRFPRO.dateType(this.selectedDate, this.currentDate) === 'Future') {
                        this.$el.addClass('entriesDtlsFuture');
                    } else {
                        this.$el.removeClass('entriesDtlsFuture');
                    }


                    page = 'Entries';
                    if(this.resultsPage && this.resultsPage === true){
                        page = 'Results';
                    }

                    this.collection= new this.racesCollection(this.model.get('races'));
                    this.parent.calender.setDates(this.model.get('dates'));


                    if( this.collection.length > 0) {
                        entriesDetailsHeaderView = ['<div>',
                        this.trackHeadTemplate({
                            selectedDate:this.selectedDate,
                            dateType:DRFPRO.dateType(this.selectedDate, this.currentDate)
                        }),
                        this.trackNameTemplate({
                            weather:this.model.get('weather'),
                            priority:this.model.get('priority')
                        }),
                        '</div>'].join('');

                        this.$el.append(entriesDetailsHeaderView);

                        _.each(this.collection.models, function (race) {
                            if(typeof race.get('raceKey') !== "undefined"){
                            var fragment = [
                                '<div class="entriesRaceDtls">',
                                this.renderRaceHeader(race),
                                this.renderRaceBody(race),
                                '</div>'
                            ].join('');
                            racesFragment.push(fragment);
                            }},this);
                        this.$el.append(racesFragment.join(''));

                        if(DRFPRO.appConst.activeRaceAccordion !== null) {
                            this.$('#' + DRFPRO.appConst.activeRaceAccordion).addClass('raceActive');
                            this.$('#' + DRFPRO.appConst.activeRaceAccordion).parents('.entriesRaceDtls').find('.entriesDtlsbody').
                                addClass('RaceDtlsShow');

                            _.each(DRFPRO.appConst.activeHorseAccordion, function (horseView) {
                                self.$(' #' + horseView).addClass('entRaceDtlsPanel');
                                self.$(' #' + horseView).find('.entRaceDtlsWrap').addClass('entRaceDtlsShow');
                            });
                        }

                        if(typeof this.raceId !== "undefined" && this.raceId !== "") {
                            var url= '/results/tracks/' + this.trackId + '/country/' + self.country + '/date/' + this.selectedDate;

                            $('html, body').animate({
                                scrollTop: $('#Race' + this.raceId).offset().top
                            },200);

                            delete this.raceId;
                            DRFPRO.utils.replaceStateHistory(url);
                        }
                    } else {
                        this.$el.html("<div class='errorMsg'>The selected track has no races available on this date.</div>");
                    }
                } else if(this.model.get('races') === false){
                    DRFPRO.showPageNotFound();
                }
                else{
                    this.trackDetailsBackButtons();
                    this.$el.html("<div class='errorMsg'>The selected track has no races available on this date.</div>");
                    $('.loading').fadeOut(550);
                }
                if (DRFPRO.appConst.position ) {
                    window.scrollTo( 0, DRFPRO.appConst.position );
                }
               // $('.loading').fadeOut(550);
                return this;

            },

            renderRaceBody: function (race) {
                var fragment = ['<div class="entriesDtlsbody" >'],
                    wagerTextFragment,
                    runnerList = ['<ul class = "dataTable listBody" >'],
                    footerFragment,
                    runners = race.get('runners'),
                    changes = race.get('changes'),
                    coupledLines = race.get('coupledLines');


                var collection = new this.runnersCollection(runners);
                wagerTextFragment = this.raceHeadRow({wagerText: race.get('wagerText')});

                collection.each(function (model) {
                    runnerList.push(this.renderHorse(model));
                },this);

                runnerList.push('</ul>');

                if((changes && changes.length > 0) || (coupledLines && coupledLines.length > 0)) {
                    footerFragment = this.renderFooter(changes,coupledLines);
                } else if(this.resultsPage && this.resultsPage === true ){
                    footerFragment = this.raceFooterTemplate(race.toJSON());
                }
                fragment.push(wagerTextFragment,(runnerList.join('')),footerFragment,'</div>');
                return fragment.join('');
            },

            renderHorse: function(horseModel){
                var horseDetailHtml = ['<li id="horse'+$.trim(horseModel.get("programNumber"))+'" class = "horseDetail">'],
                    scratchIndicators = ['E', 'G', 'O', 'S', 'T', 'V', 'Y'],
                    horseScratchIndicator = horseModel.get('scratchIndicator');

                horseModel.set('scratchReason',horseScratchIndicator);

                if($.inArray(horseScratchIndicator, scratchIndicators) >= 0) {

                    if(horseScratchIndicator !== 'Y') {
                        horseModel.set('scratchReason', horseScratchIndicator);
                    } else {
                        horseModel.set('scratchReason', '');
                    }
                    horseModel.set('scratchIndicator', 'Y');
                }
                horseDetailHtml.push(this.horseTemplate(horseModel.toJSON()));
                horseDetailHtml.push(this.horseMobileViewTemplate(horseModel.toJSON()),'</li>');
                return(horseDetailHtml.join(''));
            },

            renderFooter: function (changes,coupledLines) {
                var footerHtml = ['<ul class="dataTable listFooter"> <li><span class ="sCell valentDtlsbtm" >'];

                if(coupledLines.length > 0) {
                    _.each(coupledLines, function (coupledHorse) {
                        var coupled = coupledHorse.split(':');
                        footerHtml.push('<span><span>' + coupled[0] + ': </span>' + coupled[1] + '</span>');
                    })
                }

                if(changes.length > 0) {
                    var counter = 1;
                    footerHtml.push('<span class="valentDtlsctd"> <span>Changes: </span><br>');
                    _.each(changes,function (change) {
                        footerHtml.push('<span>');
                        if(change.type !== "S") {
                            footerHtml.push(change.text + '<br>');
                        } else {
                            footerHtml.push('<span class="scratchedHorse">' + change.text.replace(/scratched/i, "") +' </span> scratched <br>');
                        }
                        footerHtml.push('</span>');
                    });
                    footerHtml.push('</span>');
                }
                footerHtml.push('</span></li> </ul>');
                return footerHtml.join('');
            },

            renderRaceHeader: function(raceModel){
                var headerFragment = ['<div class="raceHeader">'];
                    if(typeof raceModel.get('raceKey') !== "undefined"){
                    var  raceKey = raceModel.get('raceKey').raceDate,

                    date = raceKey.year + (raceKey.month < 9 ? "0" : "") + (raceKey.month + 1) + (raceKey.day < 10 ? "0" : "") + raceKey.day;
                    raceModel.set({'dateForPP': date});

                headerFragment.push(this.raceHeaderTemplate(raceModel.toJSON()),
                    this.raceDescriptionTemplate(raceModel.toJSON()),
                    '</div>');
                return (headerFragment.join(''));
                    }

            },

            showRaceDetails: function (e) {
                var clickedElement = $(e.currentTarget);
                var windowWidth = $(window).width();
                var offsetTop = (clickedElement.offset().top)-110;
                if(windowWidth < 768){
                    if(clickedElement.hasClass('raceActive')) {
                        DRFPRO.appConst.activeRaceAccordion = null;
                        DRFPRO.appConst.activeHorseAccordion = [];
                        clickedElement.removeClass('raceActive');
                        clickedElement.parents('.entriesRaceDtls').find('.entriesDtlsbody').removeClass('RaceDtlsShow');
                    }
                    else {
                        $('html,body').animate({scrollTop:offsetTop});
                        DRFPRO.appConst.activeRaceAccordion = clickedElement.attr('id');
                        clickedElement.addClass('raceActive');
                        clickedElement.parents('.entriesRaceDtls').find('.entriesDtlsbody').addClass('RaceDtlsShow');
                    }
                }
            },

            toggleFavorite: function (e) {
                if(typeof app.request !== 'undefined') {
                    app.request.abort();
                }
                $('.loading').show();

                /**  Local storage code for storing tracks which are favorite by user*/
             var id = this.favoriteTrackModel.get('trackId');
                if (window.localStorage) {
                    if(window.localStorage.getItem('favouriteTracks') !== null){


                        var arr = JSON.parse( localStorage.getItem('favouriteTracks'));
                        var index =  $.inArray(id,arr);
                        if (index > -1) {
                            arr.splice(index, 1);
                            this.favoriteTrackModel.set('priority','low');
                        }
                        else{
                            arr.push(id);
                            this.favoriteTrackModel.set('priority','high');
                        }


                        localStorage.setItem('favouriteTracks',JSON.stringify(arr));
                    }else
                    {
                        localStorage.setItem('favouriteTracks',JSON.stringify([id]));
                        this.favoriteTrackModel.set('priority','high');
                    }

                }
                this.toggleSuccess();
                return false;
            },

            toggleSuccess: function () {
                this.priority = this.favoriteTrackModel.get('priority');
                var favoriteIcon = $('#favorite');

                if(this.priority === 'low') {
                    favoriteIcon.removeClass('icon-favorite');
                    favoriteIcon.addClass('icon-add-favorite');

                } else {
                    favoriteIcon.addClass('icon-favorite');
                    favoriteIcon.removeClass('icon-add-favorite');
                }

                $('.loading').fadeOut(550);
            },

            trackDetailsBackButtons: function () {
                $(".entriesListWrap").html('');
                $('.loading').fadeOut(550);
                $('.backButton a').attr('href',  this.url + this.currentDate);
                $(".backButton").removeClass('hidden');
                var linkSelectorObj =$("a.mback");
                linkSelectorObj.removeClass('hidden');
                linkSelectorObj.prop("href", this.url + this.currentDate);
                linkSelectorObj.html('<i class = "icon-mBack"></i>');
            }

        }
    }
})();
