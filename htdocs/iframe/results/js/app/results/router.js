/**
 * Created with JetBrains PhpStorm.
 * User: webonise
 * Date: 17/1/14
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
var DRFPRO = DRFPRO || {};
DRFPRO.results = DRFPRO.results || {};
/* set currentDate to date in required format(format in which date passed to api) */


/*application constant*/
DRFPRO.results.applicationConstant = {
    trackListTimeInterval: 45000
};

//var router_base = ( document.location.pathname.match(/\iframe\/drf\//ig) || [""] )[0];

DRFPRO.results.router = (function () {
    return{
        AppRouter: Backbone.Router.extend({
            routes: {
                "iframe/results/": "resultList",
                "iframe/results/selectedDate/:date": "resultList",
                "iframe/results/tracks/:trackId/country/:country/date/:date": "trackDetails",
                "iframe/results/tracks/:trackId/country/:country/date/:date/Race/:raceId": "trackDetails",
                "iframe/results/article/:trackId/raceNumber/:raceNumber/country/:country/date/:date" : "articleDetails",
                "iframe/results/article/:trackId/raceNumber/:raceNumber/country/:country/date/:date/articleId/:articleId" : "articleDetails",
                "iframe/results/premium/:trackId/country/:country/date/:date/Race/:raceId/articleId/:articleId" : "premiumPlanDetails",
                "iframe/results/results": "backToResults",
                "*path": "pageNotFound"
            },

            initialize: function () {
                var value = DRFPRO.supportsHtmlStorage();
                if(value == false){
                    this.localStorageNotEnabled();
                }else{
                this.calenderWrap = $('.dateWrap');
                this.calenderHtml = "<ul id='calender' ></ul>";
                DRFPRO.results.currentDate = today;
//                DRFPRO.results.selectedDate = DRFPRO.results.currentDate;
                if(currentTime >= 12){
                    DRFPRO.results.selectedDate = DRFPRO.results.currentDate;
                } else {
                    var currentDateString = today.split(/\D/);
                    var currentDate = new Date(currentDateString[2], --currentDateString[0], currentDateString[1]);
                    var previousDate = DRFPRO.utils.createDateForCalender(currentDate,currentTime),
                        dateString;
                   // previousDate.date = previousDate.getDate() -1;
                    dateString = (previousDate.monthID < 10 ? "0" : "") + previousDate.monthID + '-' + (previousDate.date < 10 ? "0" : "") + previousDate.date + '-' + previousDate.year;
                    DRFPRO.results.selectedDate = dateString;
                    DRFPRO.results.actualSelectedDate = DRFPRO.results.currentDate;
                }
                }


            //    DRFPRO.utils.initiateSession();
            },
            /*------------------------Result track list page----------------------------------*/
            resultList: function (date) {
                $("#entriesListingWrapper").addClass("activePage");
                if (typeof this.request !== 'undefined') {
                    this.request.abort();
                }
              //  DRFPRO.utils.initiateSession();
                if (date) {
                    DRFPRO.results.selectedDate = date;
                }
                window.clearInterval(this.interval);

                if( this.resultWrapDetails){
                    this.resultWrapDetails.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("activePage");
                    if(window.localStorage) {
                        if(window.localStorage.getItem('raceNumber') !== null) {
                            var raceNumberVideo = JSON.parse(localStorage.getItem('raceNumber'));
                            if(raceNumberVideo!=null){
                                if($('#'+raceNumberVideo).hasClass('raceReplayActive')){
                                    $('#'+raceNumberVideo).removeClass('raceReplayActive');
                                    $('#'+raceNumberVideo).find("iframe").removeAttr("src");
                                    if(DRFPRO.results.activeRaceReplay.length > 0){
                                        DRFPRO.results.activeRaceReplay =[];
                                    }
                                    localStorage.setItem('raceNumber',null);
                                }
                            }
                        }
                    }
                    delete (this.resultWrapDetails);
                }
                if(this.premiumPlanDetailsView){
                    this.premiumPlanDetailsView.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("premiumBeyerWrap");
                    delete (this.premiumPlanDetailsView);
                }
                if(this.calender) {
                    this.calender.undelegateEvents();
                    this.calender.remove();
                    delete(this.calender);
                }

                if(this.currentView){
                    this.currentView.undelegateEvents();
                }

                var self = this;
                if (!this.calender || this.calender.redirectionUrl.indexOf('/tracks') >= 0) {
                    this.calenderWrap.html(this.calenderHtml);
                    this.calender = new DRFPRO.results.view.CalenderView({
                        el: $('#calender'),
                        parent: this,
                        url: DRFPRO.utils.getBaseURL() + '/results/selectedDate/'
                    });
                }

                this.resultListing = new DRFPRO.results.model.resultsList();
                this.resultListing.setUrl(DRFPRO.results.selectedDate);

                this.currentView = new DRFPRO.results.view.ResultsView({
                    el: $('.entriesListWrap'),
                    parent: this,
                    date: DRFPRO.results.selectedDate,
                    model: this.resultListing

                });

                if( 0 <= DRFPRO.dateDifference(DRFPRO.results.currentDate,DRFPRO.results.selectedDate) >= -1 ) {
                    this.interval = window.setInterval(_.bind(function () {
                        self.request = self.resultListing.fetch();
                    }, this), DRFPRO.results.applicationConstant.trackListTimeInterval);
                }
                this.resultListing.fetch();

                DRFPRO.setActiveDate(DRFPRO.results.selectedDate,'.dateWrap');
                DRFPRO.startLoader();
                //DRFPRO.updateAds();
                //this.googleAnalytic();
            },

            /*------------------------Result details page----------------------------------*/
            trackDetails: function (trackId, country, date, raceId) {

                DRFPRO.appConst.activeRaceAccordion = null;
                $("#entriesDetailsWrapper").addClass("activePage");
                if (typeof this.request !== 'undefined') {
                    this.request.abort();
                }
              //  DRFPRO.utils.initiateSession();
                if (date) {
                    DRFPRO.results.selectedDate = date;
                }
                $(".entriesListWrap").html('');
                window.clearInterval(this.interval);
                if(this.articleDataResultDetailsView) {
                    this.articleDataResultDetailsView.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("newsWrap");
                    delete (this.articleDataResultDetailsView);
                }
                if(this.premiumPlanDetailsView){
                    this.premiumPlanDetailsView.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("premiumBeyerWrap");
                    delete (this.premiumPlanDetailsView);
                }
                if(this.calender) {
                    this.calender.undelegateEvents();
                    this.calender.remove();
                    delete(this.calender);
                }
                if( this.currentView){
                    this.currentView.undelegateEvents();
                    $("#entriesListingWrapper").removeClass("activePage");
                    delete (this.currentView);
                }
                /* adds or reinitialize calender  if no calender is present or calender for different track is present (since Only those dates to be visible which has race schedules)*/
                if (!this.calender || this.calender.track !== trackId) {
                    this.calenderWrap.html(this.calenderHtml);
                    this.calender = new DRFPRO.results.view.CalenderView({
                        el: $("#calender"),
                        parent: this,
                        track: trackId,
                        country: country,
                        date: date,
                        url: DRFPRO.utils.getBaseURL() + '/results/tracks/' + trackId + '/country/' + country + '/date/'
                    });

                }

                if(this.resultWrapDetails){
                    if(window.localStorage) {
                        if(window.localStorage.getItem('raceNumber') !== null) {
                            var raceNumberVideo = JSON.parse(localStorage.getItem('raceNumber'));
                            if(raceNumberVideo!=null){
                                if($('#'+raceNumberVideo).hasClass('raceReplayActive')){
                                    $('#'+raceNumberVideo).removeClass('raceReplayActive');
                                    $('#'+raceNumberVideo).find("iframe").removeAttr("src");
                                    if(DRFPRO.results.activeRaceReplay.length > 0){
                                        DRFPRO.results.activeRaceReplay =[];
                                    }
                                    localStorage.setItem('raceNumber',null);
                                }
                            }
                        }
                    }
                    this.resultWrapDetails.undelegateEvents();
                }

                this.raceListModel = new DRFPRO.results.model.race();
                this.raceListModel.set({'trackId': trackId, 'country': country, 'date': date});

                this.resultWrapDetails = new DRFPRO.results.view.ResultWrapDetailsView({
                    trackId: trackId,
                    el: $('.entriesDtlsWrap'),
                    parent: this,
                    selectedDate: DRFPRO.results.selectedDate,
                    country: country,
                    raceId: raceId,
                    model: this.raceListModel
                });

                var self = this;
                if(DRFPRO.dateType(DRFPRO.results.currentDate,DRFPRO.results.selectedDate) === "Today\'s") {
                    this.interval = window.setInterval(_.bind(function () {
                        self.request = self.raceListModel.fetch();
                    }, this), DRFPRO.results.applicationConstant.trackListTimeInterval);
                }

                this.raceListModel.fetch();
                DRFPRO.setActiveDate(DRFPRO.results.selectedDate,'.dateWrap');
                DRFPRO.startLoader();
                //DRFPRO.updateAds();
                //this.googleAnalytic();
            },
            articleDetails : function(trackId,raceNumber,country,date,articleId) {
                if(typeof this.request !== 'undefined') {
                    this.request.abort();
                }
                if(date) {
                    DRFPRO.results.selectedDate = date;
                }
                var raceNumber = raceNumber || null,
                    self = this;
                window.clearInterval(this.interval);

                if( this.resultWrapDetails){
                    this.resultWrapDetails.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("activePage");
                    delete (this.resultWrapDetails);
                }
                if(this.calender) {
                    this.calender.undelegateEvents();
                    this.calender.remove();
                    delete(this.calender);
                }
                $("#entriesDetailsWrapper").addClass("newsWrap");
                if(this.articleDataResultDetailsView) {
                    this.articleDataResultDetailsView.undelegateEvents();
                }

                this.articlePostData = new DRFPRO.results.model.articlePostDetail();
                this.articlePostData.set('trackId', trackId);
                this.articlePostData.setUrl(trackId,raceNumber,articleId,country,DRFPRO.results.selectedDate);
                this.articleDataResultDetailsView = new DRFPRO.results.view.articleDataResultDetailsView({
                    el : $('.entriesDtlsWrap'),
                    parent : this,
                    selectedDate : date,
                    model : this.articlePostData,
                    raceNumber : raceNumber,
                    articleId : articleId
                });
                this.articlePostData.fetch();

                /*set interval only if  current date is selected*/
                var self = this;
                if(DRFPRO.dateType(DRFPRO.results.currentDate,DRFPRO.results.selectedDate) === "Today\'s") {
                    this.interval = window.setInterval(_.bind(function () {
                        self.request = self.articleData.fetch();
                    }, this), DRFPRO.results.applicationConstant.trackListTimeInterval);
                }

                /*set selected date as active date*/
          //      DRFPRO.setActiveDate(DRFPRO.results.selectedDate,'.dateWrap');
                DRFPRO.startLoader();
            },
            premiumPlanDetails : function(trackId, country, date, raceId ,articleId){

                if(typeof this.request !== 'undefined') {
                    this.request.abort();
                }
                if(date) {
                    DRFPRO.results.selectedDate = date;
                }
                var raceNumber = raceId || null,
                    self = this;
                window.clearInterval(this.interval);

                if(this.resultWrapDetails){
                    this.resultWrapDetails.undelegateEvents();
                    $("#entriesDetailsWrapper").removeClass("activePage");
                    if(window.localStorage) {
                        if(window.localStorage.getItem('raceNumber') !== null) {
                            var raceNumberVideo = JSON.parse(localStorage.getItem('raceNumber'));
                            if(raceNumberVideo!=null){
                                if($('#'+raceNumberVideo).hasClass('raceReplayActive')){
                                    $('#'+raceNumberVideo).removeClass('raceReplayActive');
                                    $('#'+raceNumberVideo).find("iframe").removeAttr("src");
                                    if(DRFPRO.results.activeRaceReplay.length > 0){
                                        DRFPRO.results.activeRaceReplay =[];
                                    }
                                    localStorage.setItem('raceNumber',null);
                                }
                            }
                        }
                    }
                    delete (this.resultWrapDetails);
                }
                if(this.calender){
                    this.calender.undelegateEvents();
                    this.calender.remove();
                    delete (this.calender);
                }
                if(this.premiumPlanDetailsView){
                    this.premiumPlanDetailsView.undelegateEvents();
                }
                $("#entriesDetailsWrapper").addClass("premiumBeyerWrap");
                this.premiumPlanDetailsView = new DRFPRO.results.view.premiumPlanDetailsView({
                    el : $('.entriesDtlsWrap'),
                    parent : this,
                    selectedDate : date,
                    raceNumber : raceNumber,
                    trackId : trackId,
                    country : country,
                    articleId : articleId

                });

                /*set selected date as active date*/
          //      DRFPRO.setActiveDate(DRFPRO.results.selectedDate,'.dateWrap');
                //this.googleAnalytic();
            },

            pageNotFound:function(){
                DRFPRO.showPageNotFound();
            },
            localStorageNotEnabled:function(){
                DRFPRO.showLocalDataStoreNotEnabled();
            },
            googleAnalytic:function(){
                //var url = Backbone.history.root + Backbone.history.getFragment();
                //ga('send','pageview',{page:"/" + url });
            }

        })



    };
})();
var app = new DRFPRO.results.router.AppRouter();
Backbone.history.start({ pushState:true, root:"/"});
