var DRFPRO = DRFPRO || {};
DRFPRO.results = DRFPRO.results || {};
DRFPRO.results.model = (function () {
    return{

        result: Backbone.Model.extend({
            initialize:function(){
                this.id=this.get('trackId');
                if(_.contains(JSON.parse(localStorage.getItem('favouriteTracks')), this.id) === true){
                    this.set('priority','high');
                }else{
                    this.set('priority','low');
                }
            },

            url: function () {
                //return DRFPRO.utils.getBaseURL() + '/entries/toggleFavorite/id/' + this.get('trackId') + '/date/' + this.get('date');
                return DRFPRO.utils.getBaseURL() + '/api-request.php?entries=true&toggleFavorite=true&id=' + this.get('trackId') + '&date=' + this.get('date');
            }
        })

        ,

        runner: Backbone.Model.extend(),

        date: Backbone.Model.extend(),
        articlePostResults: Backbone.Model.extend(),

        race: Backbone.Model.extend({
            url: function () {
                //return DRFPRO.utils.getBaseURL() + '/results/resultDetails/id/' + this.get('trackId') + '/country/' + this.get('country') + '/date/' + this.get('date');
                return DRFPRO.utils.getBaseURL() + '/api-request.php?results=true&resultDetails=true&id=' + this.get('trackId') + '&country=' + this.get('country') + '&date=' + this.get('date');

            }
        })

        ,

        weather: Backbone.Model.extend(),

        ResultDetailsList: Backbone.Model.extend({
            url: function () {
                //return DRFPRO.utils.getBaseURL() + '/results/resultDetails/id/' + this.get('trackId') + '/date/' + this.get('date');
                return DRFPRO.utils.getBaseURL() + 'api-request.php?results=true&resultDetails=true&id=' + this.get('trackId') + '&date=' + this.get('date');
            }
        })

        ,

        resultsList: Backbone.Model.extend({

            setUrl: function (date) {
                //this.url = DRFPRO.utils.getBaseURL() + '/results/getTrackList/date/' + date;
                this.url = DRFPRO.utils.getBaseURL() + '/api-request.php?results=true&getTrackList=true&date=' + date;
            }
        })

        ,

        articlePostDetail: Backbone.Model.extend({

            setUrl: function (trackId,raceNumber,articleId,country,date) {
                //this.url = DRFPRO.utils.getBaseURL() + '/results/articleDetails/id/'+ trackId +'/raceNumber/'+ raceNumber +'/articleId/'+articleId + '/country/' + country +'/date/' + date;
                this.url = DRFPRO.utils.getBaseURL() + '/api-request.php?results=true&articleDetails=true&id='+ trackId +'&raceNumber='+ raceNumber +'&articleId='+articleId + '&country=' + country +'&date=' + date;
            }
        })
    };
})();
