/**
 * Created with JetBrains PhpStorm.
 * User: webonise
 * Date: 16/12/13
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
var DRFPRO = DRFPRO || {};
DRFPRO.results = DRFPRO.results || {};
DRFPRO.results.collection = (function () {
    return{

        resultsCollection: Backbone.Collection.extend({
            model: DRFPRO.results.model.result,
            initialize: function () {
                this.comparator = function (model) {
                    return model.get('priority') + '_' + model.get('trackName');
                };
                this.sort();
            }/*,

            setUrl: function (date) {
                this.url = DRFPRO.utils.getBaseURL() + '/results/getTrackList/date/' + date;
            }*/
        }),

        datesCollection: Backbone.Collection.extend({

            model: DRFPRO.results.model.date,

            initialize: function () {
                this.comparator = function (model) {
                    return model.get('ActualDate');
                }

                this.sort();
            }
        }),

        racesCollection: Backbone.Collection.extend({
            model: DRFPRO.results.model.race,

            setUrl: function (trackId, country, date) {
                this.url = DRFPRO.utils.getBaseURL() + '/results/resultDetails/id/' + trackId + '/country/' + country + '/date/' + date;
            }
        }),

        runnersCollection: Backbone.Collection.extend({

            model: DRFPRO.results.model.runner
        }),
        resultArticlesCollection: Backbone.Collection.extend({

            model: DRFPRO.results.model.articlePostResults,

            initialize: function () {
                this.comparator = function (model) {
                    var raceNumber = Number(($.trim(model.get('raceNumber'))));
                    return raceNumber;
                }
                this.sort();
            }
        })
    }
})();
