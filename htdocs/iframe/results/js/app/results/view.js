var DRFPRO = DRFPRO || {};
DRFPRO.results = DRFPRO.results || {};
DRFPRO.results.activeRaceReplay = [];
DRFPRO.results.activeNewsUpdate =[];
DRFPRO.results.view = (function () {
    return {
        /*-----------------------Primium Plans Details------------------------------------*/
        premiumPlanDetailsView : Backbone.View.extend({
            tagName : 'div',
            premiumPlanTemplate : _.template($('#template-premiumMemberPlans').html()),

            initialize : function(options) {
                this.parent = options.parent;
                this.selectedDate = DRFPRO.results.selectedDate;
                this.currentDate = DRFPRO.results.currentDate;
                this.raceNumber = options.raceNumber;
                this.trackId = options.trackId,
                this.country = options.country,
                this.url = DRFPRO.utils.getBaseURL() + '/results/selectedDate/',
                this.articleId =  options.articleId,
                this.render();

            },
            render : function() {
                if(this.articleId){
                    this.trackArticleBackButtons();
                }else{
                    this.trackDetailsBackButtons();
                }
                this.$el.html(this.premiumPlanTemplate());
                $('html, body').animate({
                    scrollTop : ($('.pageData').offset().top)
                });
            },
            trackDetailsBackButtons : function(weather) {
                $('.loading').fadeOut(550);
                var linkSelectorObj =$("a.mback");
                $(".entriesListWrap").html('');
                $(".backButton").removeClass('hidden');
                linkSelectorObj.removeClass('hidden');
                $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/tracks/' + this.trackId + '/country/' + this.country +
                    '/date/' + this.selectedDate);
                linkSelectorObj.prop("href",DRFPRO.utils.getBaseURL() + '/results/tracks/' + this.trackId + '/country/' + this.country +
                    '/date/' + this.selectedDate);
                if(DRFPRO.getValue !=null) {
                    $('.backButton').css('display','none');
                    $('.mback').css('display','none');
                }
                if(DRFPRO.getValueBets !=null) {
                    $('.backButton').css('display','block');
                    $('.mback').css('display','block');
                    $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/tracks/' + this.trackId + '/country/' + this.country +
                        '/date/' + this.selectedDate+'?bets=true');
                    linkSelectorObj.prop('href',DRFPRO.utils.getBaseURL() + '/results/tracks/' + this.trackId + '/country/' + this.country +
                        '/date/' + this.selectedDate +'?bets=true');
                }
                linkSelectorObj.html('<i class = "icon-mBack"></i>');
            },
            trackArticleBackButtons : function(weather){
                $('.loading').fadeOut(550);
                var linkSelectorObj = $("a.mback");
                $(".entriesListWrap").html('');
                $(".backButton").removeClass('hidden');
                linkSelectorObj.removeClass('hidden');
                if(DRFPRO.getValue != null || DRFPRO.getValueBets != null) {
                    var iframeCheck;
                    $('.backButton').css('display','block');
                    $('.mback').css('display','block');
                    if(DRFPRO.getValue != null) {
                        iframeCheck = '?header=true';
                    } else if(DRFPRO.getValueBets != null) {
                        iframeCheck = '?bets=true';
                    } else {
                        iframeCheck = '';
                    }
                    $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/article/' +this.trackId + '/raceNumber/'+
                    this.raceNumber+'/country/' + this.country +'/date/' + this.selectedDate +  iframeCheck);
                    linkSelectorObj.prop('href',DRFPRO.utils.getBaseURL() + '/results/article/' +this.trackId + '/raceNumber/'+
                    this.raceNumber+'/country/' + this.country +'/date/' + this.selectedDate +  iframeCheck);
                } else {
                    $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/article/' +this.trackId + '/raceNumber/'+
                        this.raceNumber+'/country/' + this.country +'/date/' + this.selectedDate+'/articleId/'+this.articleId);
                    linkSelectorObj.prop('href',DRFPRO.utils.getBaseURL() + '/results/article/'+ this.trackId + '/raceNumber/'+
                        this.raceNumber+'/country/' + this.country +'/date/' + this.selectedDate+'/articleId/'+this.articleId);
                }
                linkSelectorObj.html('<i class = "icon-mBack"></i>');
            }
        }),
        /*---------------------------------Article Details -----------------------------------*/
        articleDataResultDetailsView : Backbone.View.extend({
            tagName : 'div',
            trackHeadArticleTemplate : _.template($('#template-article-textHeader').html()),
            trackNameTemplate : _.template($('#template-quickSheet-trackName').html()),
            articleDetailsTemplate : _.template($('#template-article-articleDetails').html()),
            nonPremiumUsersTemplate : _.template($('#template-result-article-premiumMsg').html()),
            nonLoggedInUserTemplate : _.template($('#template-result-article-nonLoggedInUser').html()),
            nonLoggedInUnSubScribedUser : _.template($('#template-nonLoggedInUnSubScribedUser').html()),
            noDataAvailableQuicksheetTemplate : _.template($('#template-noDataAvailableArticleTemplate').html()),
            events : { 'click .newsDtlsWrap li .btnReadMore' : 'expandPost' ,'click #favorite': 'toggleFavorite',
                'click #drfLiveWebLink':'liveWebLink','click .loginEncryptUrl' : 'encryptUrl',
                'click .articleEvent' : 'articleEventUrl' },
            initialize : function(options) {
                this.articleId = options.articleId;
                this.parent = options.parent;
                this.selectedDate = options.selectedDate;
                this.currentDate = DRFPRO.results.currentDate;
                this.raceId = options.raceId;
                this.trackId = options.trackId;
                this.country = options.country;
                this.raceNumber = options.raceNumber;
                this.resultsPage = true;
                this.url = DRFPRO.utils.getBaseURL() + '/results/selectedDate/'
                this.favoriteTrackModel = new DRFPRO.results.model.result();
                this.listenTo(this.model, 'change', this.render);
                this.listenTo(this.favoriteTrackModel, 'change:priority', this.toggleSuccess);
            },
            render : function() {

                var trackChangesHtml = [],
                    self = this,
                    weather = this.model.get('weather');
                if(typeof this.model.get('articles')!=="undefined" &&  this.model.get('articles') !== false){
                    this.trackDetailsBackButtons(weather);
                    if(_.contains(JSON.parse(localStorage.getItem('favouriteTracks')), this.model.get('weather').trackId) === true) {
                        this.model.set('priority', 'high');
                    } else {
                        this.model.set('priority', 'low');
                    }
                    var isLoggedIn=this.model.get('isLoggedIn');
                    var isSubScribed=this.model.get('isSubscribed');
                    this.collection = new DRFPRO.results.collection.resultArticlesCollection(this.model.get('articles'));

                    if(this.collection.length > 0) {

                        this.favoriteTrackModel.set({trackId : this.model.get('weather').trackId,
                            date : this.selectedDate});
                        var  nuggets = this.model.get('articles').livePostNewsArticlesList ,userChoice =this.model.get('userChoice');
                        var entriesDetailsHeaderView = ['<div>',
                            this.trackHeadArticleTemplate({
                                selectedDate : this.selectedDate,
                                dateType :null,
                                articlePage : true
                            }),
                            this.trackNameTemplate({
                                weather : this.model.get('weather'),
                                priority : this.model.get('priority'),
                                articlePage : true
                            }),
                            '</div>'
                        ].join('');
                        this.$el.html(entriesDetailsHeaderView);
                        var frag = ['<div class="newsDtlsWrap"><ul class="articlePattern"></ul></div>'];
                        this.$el.append(frag);
                        _.each(nuggets,function(nugget){
                            var isData=nugget.isArticleData;
                            var isPremium=nugget.isPremium;
                            var articleSubscription = '.premiumScreenCheck'+nugget.nid;
                            if(isLoggedIn && isSubScribed && isData==true){
                                $('.articlePattern').append(this.articleDetailsTemplate({nugget:nugget,isLoggedIn :isLoggedIn,
                                    isSubScribed:isSubScribed, userChoice:userChoice,isPremium:isPremium}));

                            }else if(isLoggedIn && !isSubScribed &&  isData==true){
                                $('.articlePattern').append(this.articleDetailsTemplate({nugget:nugget,isLoggedIn :isLoggedIn,
                                    isSubScribed:isSubScribed, userChoice:userChoice,isPremium:isPremium}));
                                $(articleSubscription).append(this.nonPremiumUsersTemplate({trackId : this.model.get('weather').trackId,
                                    country : this.model.get('weather').country, date : this.selectedDate,
                                    raceNumber : this.raceNumber, article : "entries", articleId : nugget.nid}));

                            }else if(isLoggedIn && !isSubScribed &&  isData!=true ||
                                ((isLoggedIn && isSubScribed &&  isData!=true ))){
                                $('.articlePattern').append(this.articleDetailsTemplate({nugget:nugget,isLoggedIn :isLoggedIn,
                                    isSubScribed:isSubScribed, userChoice:userChoice,isPremium:isPremium}));
                                $(articleSubscription).append(this.noDataAvailableQuicksheetTemplate());

                            }else {
                                $('.articlePattern').append(this.articleDetailsTemplate({nugget:nugget,isLoggedIn :isLoggedIn,
                                    isSubScribed:isSubScribed, userChoice:userChoice,isPremium:isPremium}));
                                $(articleSubscription).append(this.nonLoggedInUserTemplate({trackId:this.model.get('weather').trackId,
                                    country:this.model.get('weather').country,date:this.selectedDate,
                                    raceNumber:this.raceNumber,programNumber:this.programNumber,article:"results",articleId:nugget.nid}),'</div>');
                            }

                        },this);
                        _.each(nuggets,function(nugget){
                            if(nugget.videoLink!="" && nugget.videoLink!=null) {
                                var vimioVideo = "vimeo" , nuggetVideoLink = nugget.videoLink;
                          //      var index = $.inArray(nuggetVideoLink, vimioVideo);
                                if(nuggetVideoLink.indexOf(vimioVideo) > -1){
                                    $("#"+nugget.nid+"jwVideo").remove();
                                    var videoId = nugget.nid+'vimeoVideo';
                                    if ($('#'+videoId)) {
                                        $('#'+videoId).find('iframe').attr('src',nuggetVideoLink);
                                    }
                                }else{

                                    var videoId = nugget.nid+'jwVideo' ,
                                        videoLink =  nugget.videoLink,
                                        posterImage = nugget.posterImage;
                                    if ($('#'+videoId)) {
                                        DRFPRO.jwplayerPost(videoId,videoLink,posterImage);
                                    }

                                }
                            }
                        });
                        if(typeof this.articleId != "undefined" && this.articleId!=""){
                            userChoice = userChoice==null?this.articleId:userChoice;
                            if(userChoice !== null) {
                                var isPremium = _.some(nuggets,function(e1){
                                    if(e1.nid == userChoice){
                                        return e1.isPremium;
                                    }
                                });
                           /*     if((isLoggedIn && isSubScribed) || (isLoggedIn && !isSubScribed && !isPremium)
                                    || (!isLoggedIn && !isSubScribed && !isPremium)){*/
                                    $('#article' + userChoice).addClass('newsDtlsActive');
                               // }

                                if(isLoggedIn && !isSubScribed && isPremium ||(!isLoggedIn && isPremium)){
                                    $('.newsDtlsActive .teaser').css('display','block');
                                }

                                windowWidth = $(window).width();
                                if(windowWidth < 768) {
                                    $('html, body').animate({
                                        scrollTop : ($('#article' + userChoice).offset().top) - 106
                                    });
                                } else {
                                    $('html, body').animate({
                                        scrollTop : ($('#article' + userChoice).offset().top)
                                    });
                                }
                                DRFPRO.utils.replaceStateHistory('/race-results/article/'+ weather.trackId +'/raceNumber/' + this.raceNumber + '/country/' + weather.country + '/date/' + this.selectedDate);
                            }
                        }
                    } else {
                        this.$el.html('<div class="errorMsg">The selected track has no post available on this date.</div>')
                    }
                } else {
                    // DRFPRO.showPageNotFound();
                    this.$el.html('<div class="errorMsg">The selected track has no post available on this date.</div>')
                }

                $('.loading').fadeOut(500);

            },

            expandPost : function(e){
                e.preventDefault();
                var clickedEl = $(e.currentTarget);
                if(clickedEl.parents('li').hasClass('newsDtlsActive')) {

                    clickedEl.parents('li').removeClass('newsDtlsActive');
                    var articleId = clickedEl.parents('li').attr("id");
                    windowWidth = $(window).width();
                    if(windowWidth < 768) {
                        $('html, body').animate({
                            scrollTop : ($('#' + articleId).offset().top) - 106
                        });
                    } else {
                        $('html, body').animate({
                            scrollTop : ($('#' + articleId).offset().top)
                        });
                    }
                }else {
                    $('.newsDtlsWrap').find('li').removeClass('newsDtlsActive');
                    clickedEl.parents('li').addClass('newsDtlsActive');
                    if(clickedEl.parents('li').find( ".showTeaser" )){
                        $(clickedEl.parents('li').find(".showTeaser")).css("display","block");
                    }
                }
            },

            trackDetailsBackButtons : function(weather) {
                var linkSelectorObj =$("a.mback");
                $(".entriesDtlsWrap").html('');
                $('.loading').fadeOut(550);
                $(".backButton").removeClass('hidden');
                linkSelectorObj.removeClass('hidden');
                if(DRFPRO.getValue !=null) {
                    $('.backButton').css('display','block');
                    $('.mback').css('display','block');
                $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/tracks/' + weather.trackId + '/country/' + weather.country + '/date/' + this.selectedDate +'/Race/'+this.raceNumber+'?header=true');
                linkSelectorObj.prop("href",DRFPRO.utils.getBaseURL() + '/results/tracks/' + weather.trackId + '/country/' + weather.country + '/date/' + this.selectedDate +'/Race/'+this.raceNumber+'?header=true');
                }else{
                    $('.backButton a').attr('href',DRFPRO.utils.getBaseURL() + '/results/tracks/' + weather.trackId + '/country/' + weather.country + '/date/' + this.selectedDate +'/Race/'+this.raceNumber);
                    linkSelectorObj.prop("href",DRFPRO.utils.getBaseURL() + '/results/tracks/' + weather.trackId + '/country/' + weather.country + '/date/' + this.selectedDate +'/Race/'+this.raceNumber);
                }
                linkSelectorObj.html('<i class = "icon-mBack"></i>');


            },

            toggleFavorite : function(e) {
                if(typeof app.request !== 'undefined') {
                    app.request.abort();
                }
                $('.loading').show();
                var id = this.favoriteTrackModel.get('trackId');
                if(window.localStorage) {
                    if(window.localStorage.getItem('favouriteTracks') !== null) {


                        var arr = JSON.parse( localStorage.getItem('favouriteTracks'));
                        var index =  $.inArray(id,arr);
                        if (index > -1) {
                            arr.splice(index, 1);
                            this.favoriteTrackModel.set('priority', 'low');
                        }
                        else {
                            arr.push(id);
                            this.favoriteTrackModel.set('priority', 'high');
                        }


                        localStorage.setItem('favouriteTracks', JSON.stringify(arr));
                    } else {
                        localStorage.setItem('favouriteTracks', JSON.stringify([id]));
                        this.favoriteTrackModel.set('priority', 'high');
                    }

                }
                this.toggleSuccess();
                return false;

            },

            toggleSuccess : function() {
                this.priority = this.favoriteTrackModel.get('priority');
                var favoriteIcon = $('#favorite');

                if(this.priority === 'low') {
                    favoriteIcon.removeClass('icon-favorite');
                    favoriteIcon.addClass('icon-add-favorite');

                } else {
                    favoriteIcon.addClass('icon-favorite');
                    favoriteIcon.removeClass('icon-add-favorite');
                }

                $('.loading').fadeOut(500);
            },
            liveWebLink :function(e){
                e.preventDefault();
                var clickedEl = $(e.currentTarget);
                var internalText = clickedEl.html();
                window.open(internalText,'_blank');
            },
            encryptUrl : function(e){
                DRFPRO.encryptUrl(e);
            },
            articleEventUrl : function(e){
                var clickedEl = $(e.currentTarget);
                var nonEncodedUrl = clickedEl.attr('url');
                clickedEl.attr('target', '_blank');
                clickedEl.attr('href', nonEncodedUrl);
            }
        }),

        /*---------------------------ResultWrapDetailsView-------------------------*/
        ResultWrapDetailsView: Backbone.View.extend({

            events: {'click .entriesRaceHeader': 'showRaceDetails','click #favorite': 'toggleFavorite' ,
                'click .newsListWrap .newsHeader' : 'showHideArticle',
                'click .raceReplayWrap .raceReplayHeader' : 'showRaceReplay'},

            trackHeadTemplate: _.template($('#template-result-textHeader').html()),

            trackNameTemplate: _.template($('#template-quickSheet-trackName').html()),

            raceHeaderTemplate: _.template($('#template-resultWrap-raceHeaderTemplate').html()),

            raceDescriptionTemplate: _.template($('#template-resultWrap-raceDescriptionTemplate').html()),

            articleTemplate : _.template($('#template-resultsDetails-articles').html()),

            raceFooterTemplate: _.template($('#template-resultWrap-raceFooter').html()),

            raceHeadRow: _.template($('#template-resultWrap-raceHeadRow').html()),

            horseTemplate: _.template($('#template-resultWrap-horseTemplate').html()),
            parFragmentTemplate: _.template($('#template-resultWrap-parValue').html()),
            horseMobileViewTemplate: _.template(''),

            initialize: function (options) {
                this.racesCollection = DRFPRO.results.collection.racesCollection;
                this.runnersCollection = DRFPRO.results.collection.runnersCollection;
                this.parent = options.parent;
                this.selectedDate = options.selectedDate;
                this.currentDate = DRFPRO.results.currentDate;
                this.raceId = options.raceId;
                this.trackId = options.trackId;
                this.country = options.country;
                this.resultsPage = true;
                this.url = DRFPRO.utils.getBaseURL() + '/results/selectedDate/'
                this.favoriteTrackModel = new DRFPRO.results.model.result();
                this.listenTo(this.model, 'change', this.render);
                this.listenTo(this.favoriteTrackModel, 'change:priority', this.toggleSuccess);
            },

            render: function () {
                var self = this,
                    entriesDetailsHeaderView,
                    racesFragment = [],
                    page,
                    isNew = false;
                if(this.model.get('isData') === true && this.model.get('races') !== false) {
                    this.trackDetailsBackButtons();
                    if(_.contains(JSON.parse(localStorage.getItem('favouriteTracks')), this.model.get('weather').trackId) === true){
                        this.model.set('priority','high');
                    }else{
                        this.model.set('priority','low');
                    }

                    this.favoriteTrackModel.set({trackId: this.model.get('weather').trackId,
                        date: this.selectedDate});

                    this.$el.html('');


                    this.collection= new this.racesCollection(this.model.get('races'));
                    this.parent.calender.setDates(this.model.get('dates'));


                    if( this.collection.length > 0) {
                        entriesDetailsHeaderView = ['<div>',
                            this.trackHeadTemplate({
                                selectedDate:this.selectedDate,
                                dateType:DRFPRO.dateType(this.selectedDate, this.currentDate)
                            }),
                            this.trackNameTemplate({
                                weather:this.model.get('weather'),
                                priority:this.model.get('priority')
                            }),
                            '</div>'].join('');

                        this.$el.append(entriesDetailsHeaderView);

                        _.each(this.collection.models, function (race) {
                            var fragment = [
                                '<div class="entriesRaceDtls">',
                                this.renderRaceHeader(race),
                                this.renderRaceBody(race),
                                '</div>'
                            ].join('');
                            racesFragment.push(fragment);
                        },this);
                        this.$el.append(racesFragment.join(''));

                        if(DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]===undefined){
                            DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]=[];
                        }
                        if(DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"].length > 0){
                            $('#'+DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]).addClass('newsListActive');
                        }

                        if(typeof this.raceId !== "undefined" && this.raceId !== "") {
                            var url= '/race-results/tracks/' + this.trackId + '/country/' + self.country + '/date/' + this.selectedDate;


                            //  window.history.replaceState("", "", DRFPRO.utils.getBaseURL() + '/results/tracks/' + this.trackId + '/trackName/' + this.trackName + '/country/' + self.country + '/date/' + this.selectedDate);
                            $('html, body').animate({
                                scrollTop: $('#Race' + this.raceId).offset().top
                            },200);

                            delete this.raceId;
                            DRFPRO.utils.replaceStateHistory(url);
                        }
                    } else {
                        this.$el.html("<div class='errorMsg'>The selected track has no races available on this date.</div>");
                    }
                } else if(this.model.get('races') === false){
                    DRFPRO.showPageNotFound();
                }
                else{
                    this.trackDetailsBackButtons();
                    this.$el.html("<div class='errorMsg'>The selected track has no races available on this date.</div>");
                    $('.loading').fadeOut(550);
                }
                if (DRFPRO.appConst.position ) {
                    window.scrollTo( 0, DRFPRO.appConst.position );
                }
                 $('.loading').fadeOut(550);

                return this;

            },

            renderRaceBody: function (race) {
                var fragment = ['<div class="entriesDtlsbody" >'],
                    wagerTextFragment,
                    runnerList = ['<ul class = "dataTable listBody" >'],
                    footerFragment,
                    runners = race.get('runners'),
                    raceTrackId =  race.get('raceKey').trackId,
                    raceRaceNumber =  race.get('raceKey').raceNumber,
                    raceCountry =  race.get('raceKey').country,
                    raceArticles = race.get('articles'),
                    raceReplayId = race.get('raceReplayId'),
                    collection = new this.runnersCollection(runners),
                    articleTemp = this.articleTemplate({'articles' : raceArticles,
                        'articlesCount' : race.get('articlesCount'),
                    'raceNumber':raceRaceNumber, 'trackId':raceTrackId, 'country':raceCountry,
                        'trackName':race.get('trackName'),'date':this.selectedDate});

                wagerTextFragment = this.raceHeadRow({trackId:race.get('raceKey').trackId,
                    country:race.get('raceKey').country,raceNumber:race.get('raceKey').raceNumber,
                    date:race.get('date'),wagerText: this.model.get('wagerText') ,raceReplayId:raceReplayId}),
                        collection.each(function (model) {
                    runnerList.push(this.renderHorse(model));
                },this);
                runnerList.push('</ul>');
              /*  if(race.get('beyerPar')!="0" && race.get('beyerPar')!=""){
                    runnerList.push(this.parFragmentTemplate({par:race.get('beyerPar')}));
                }*/
                    footerFragment = this.raceFooterTemplate(race.toJSON());

                fragment.push(articleTemp,wagerTextFragment,(runnerList.join('')),footerFragment,'</div>');

                return fragment.join('');
            },

            renderHorse: function(horseModel){
               var horseDetailHtml = ['<li id="horse'+$.trim(horseModel.get("programNumber"))+'" class = "horseDetail">'];
                horseDetailHtml.push(this.horseTemplate(horseModel.toJSON()));
                horseDetailHtml.push(this.horseMobileViewTemplate(horseModel.toJSON()),'</li>');
                return(horseDetailHtml.join(''));
            },

            renderRaceHeader: function(raceModel){
                var headerFragment = ['<div class="raceHeader">'],
                    raceKey = raceModel.get('raceKey').raceDate,
                    date = raceKey.year + (raceKey.month < 9 ? "0" : "") + (raceKey.month + 1) + (raceKey.day < 10 ? "0" : "") + raceKey.day
                    raceModel.set({date:date});
                headerFragment.push(this.raceHeaderTemplate(raceModel.toJSON()),
                    this.raceDescriptionTemplate(raceModel.toJSON()),
                    '</div>');
                return (headerFragment.join(''));
            },

            showRaceDetails: function (e) {
                var clickedElement = $(e.currentTarget);
                var windowWidth = $(window).width();
                var offsetTop = (clickedElement.offset().top)-110;
                if(windowWidth < 768){
                if(clickedElement.hasClass('raceActive')) {
                    clickedElement.removeClass('raceActive');
                    clickedElement.parents('.entriesRaceDtls').find('.entriesDtlsbody').removeClass('RaceDtlsShow');
                }
                else {
                    $('html,body').animate({scrollTop:offsetTop});
                    clickedElement.addClass('raceActive');
                    clickedElement.parents('.entriesRaceDtls').find('.entriesDtlsbody').addClass('RaceDtlsShow');
                }
                }
            },

            showRaceReplay: function (e) {
                var clickedElement = $(e.currentTarget);
                var raceNumber =  clickedElement.parent('.raceReplayWrap').attr('id');
                if(clickedElement.parent('.raceReplayWrap').hasClass('raceReplayActive')){
                    $(clickedElement).parent('.raceReplayWrap').removeClass('raceReplayActive');
                    clickedElement.parents('.raceReplayWrap').find("iframe").removeAttr("src");
                    if(DRFPRO.results.activeRaceReplay.length > 0){
                        DRFPRO.results.activeRaceReplay =[];
                    }
                    if(window.localStorage) {
                        if(window.localStorage.getItem('raceNumber') !== null) {
                            localStorage.setItem('raceNumber',null);
                        }
                    }

                }else{
                    var raceReplayUrl =  clickedElement.parents('.raceReplayWrap').find("iframe").attr("replayUrlLink");
                    $(clickedElement).parent('.raceReplayWrap').addClass('raceReplayActive');
                    clickedElement.parents('.raceReplayWrap').find("iframe").attr("src",raceReplayUrl);
                    if(DRFPRO.results.activeRaceReplay.length > 0){
                        $('#'+DRFPRO.results.activeRaceReplay[0]).removeClass('raceReplayActive');
                        DRFPRO.results.activeRaceReplay =[];
                    }
                    if(window.localStorage) {
                          localStorage.setItem('raceNumber',JSON.stringify(raceNumber));
                    }
                    DRFPRO.results.activeRaceReplay.push(raceNumber);

                }
            },

            toggleFavorite: function (e) {
                if(typeof app.request !== 'undefined') {
                    app.request.abort();
                }
                $('.loading').show();

                /**  Local storage code for storing tracks which are favorite by user*/
                var id = this.favoriteTrackModel.get('trackId');
                if (window.localStorage) {
                    if(window.localStorage.getItem('favouriteTracks') !== null){
                        var arr = JSON.parse( localStorage.getItem('favouriteTracks'));
                        var index =  $.inArray(id,arr);
                        if (index > -1) {
                            arr.splice(index, 1);
                            this.favoriteTrackModel.set('priority','low');
                        }
                        else{
                            arr.push(id);
                            this.favoriteTrackModel.set('priority','high');
                        }


                        localStorage.setItem('favouriteTracks',JSON.stringify(arr));
                    }else
                    {
                        localStorage.setItem('favouriteTracks',JSON.stringify([id]));
                        this.favoriteTrackModel.set('priority','high');
                    }

                }
                this.toggleSuccess();
                return false;
            },

            toggleSuccess: function () {
                this.priority = this.favoriteTrackModel.get('priority');
                var favoriteIcon = $('#favorite');

                if(this.priority === 'low') {
                    favoriteIcon.removeClass('icon-favorite');
                    favoriteIcon.addClass('icon-add-favorite');

                } else {
                    favoriteIcon.addClass('icon-favorite');
                    favoriteIcon.removeClass('icon-add-favorite');
                }

                $('.loading').fadeOut(550);
            },

            showHideArticle: function (e) {
                var clickedElement = $(e.currentTarget);
                var raceNumber =  clickedElement.parent('.newsListWrap').attr('id');
                if(clickedElement.parent('.newsListWrap').hasClass('newsListActive')){
                    $(clickedElement).parent('.newsListWrap').removeClass('newsListActive');
                    DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]={}
                }else{
                    $(clickedElement).parent('.newsListWrap').addClass('newsListActive');
                    if(DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"].length > 0){
                        $('#'+DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]).removeClass('newsListActive');
                    }
                    DRFPRO.results.activeNewsUpdate["'"+this.selectedDate+"'"]=raceNumber;
                }

            },

            trackDetailsBackButtons: function () {
                var linkSelectorObj =$("a.mback");
                $(".entriesListWrap").html('');
                $('.loading').fadeOut(550);
                $('.backButton a').attr('href',  this.url + this.currentDate);
                $(".backButton").removeClass('hidden');
                linkSelectorObj.removeClass('hidden');
                linkSelectorObj.prop("href", this.url + this.currentDate);
                linkSelectorObj.html('<i class = "icon-mBack"></i>');
                if(DRFPRO.getValue !=null) {
                    $('.backButton').css('display','none');
                    $('.mback').css('display','none');
                }
                if(DRFPRO.getValueBets !=null) {
                    $('.backButton').css('display','block');
                    $('.mback').css('display','block');
                    $('.backButton a').attr('href', this.url + this.currentDate+'?bets=true');
                    linkSelectorObj.prop('href', this.url + this.currentDate+'?bets=true');
                }
            }

        }),

        /**
         @Description- ResultsView wraps details about all tracks its current race and previous race

         */
        ResultsView: Backbone.View.extend({
            tagName: 'div',

            events: {'click .displayTracks': 'showHideTracks' , 'click #favorite': 'toggleFavorite', 'click .trackRow': 'selectRow' },

            headerTemplate: _.template($('#template-resultList-headerTemplate').html()),

            trackTemplate: _.template($('#template-resultList-trackTemplate').html()),
            initialize: function (options) {
                this.selectedDate = options.date;
                this.currentDate = DRFPRO.results.currentDate;
                this.parent = options.parent;
                this.url = DRFPRO.utils.getBaseURL() + '/results/selectedDate/'+this.selectedDate;
                this.listenTo(this.model, 'change', this.setCollection);

            },

            setCollection: function (){
                if(this.model.get('trackList') === false){
                    DRFPRO.showPageNotFound();
                } else{
                    this.collection = new DRFPRO.results.collection.resultsCollection(this.model.get('trackList'));
                    this.render();
                    this.listenTo(this.collection, 'sort', this.render);
                }
            }

        }),

        //--------------------Calender View------------------------------------------------//
        CalenderView: Backbone.View.extend({
            tagName: "ul",

            template: _.template($('#template-calender').html()),

            initialize: function (options) {
                this.collection = new DRFPRO.results.collection.datesCollection();
                this.parent = options.parent;
                this.redirectionUrl = options.url;
                this.track = options.track;
                this.availableDates = [];
                var self = this;
            }
        })
    };
})();
_.extend(DRFPRO.results.view.ResultsView.prototype, DRFPRO.abstractObjects.trackListFunctions);
_.extend(DRFPRO.results.view.CalenderView.prototype, DRFPRO.abstractObjects.calenderFunctions);
