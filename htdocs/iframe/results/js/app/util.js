var DRFPRO = DRFPRO || {};
DRFPRO.utils =  DRFPRO.utils || {};
DRFPRO.trackCount = 0;
DRFPRO.favClose = false;
DRFPRO.setMyTrack = false;

DRFPRO.utils = (function (){
    return {
        createDateForCalender: function (date,currentTime){
            var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
                dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                selectedDate;
            var selectedDateString;
            if (typeof DRFPRO.entries != 'undefined') {
                selectedDateString = DRFPRO.entries.selectedDate;
            } else if (typeof DRFPRO.results != 'undefined') {
                selectedDateString = DRFPRO.results.selectedDate;
            }
            if(selectedDateString){
                selectedDate = selectedDateString.split(/\D/);
                if(new Date(selectedDate[2], selectedDate[0] - 1, selectedDate[1]) !== "INVALID DATE"){
                    selectedDateString = selectedDate[0] + '-' + selectedDate[1] + '-'+selectedDate[2];
                }
            }


            var dateObject = {};

            if(currentTime <=12){
                date.setDate(date.getDate() - 1);
                dateObject.date = (date.getDate());
            }else{
                dateObject.date = date.getDate();
            }
            dateObject.month = monthNames[date.getMonth()];
            dateObject.monthID = date.getMonth() + 1;
            dateObject.day = dayNames[date.getDay()];
            dateObject.year = date.getFullYear();
            dateObject.ActualDate = date;
            dateStringFormat1 = dateObject.monthID + '-' + dateObject.date + '-' + dateObject.year;
            dateStringFormat2 = dateObject.monthID + '-' + (dateObject.date < 10 ? "0" : "") + dateObject.date + '-' + dateObject.year;
            dateStringFormat3 = (dateObject.monthID < 10 ? "0" : "") + dateObject.monthID + '-' + dateObject.date + '-' + dateObject.year;
            dateStringFormat4 = (dateObject.monthID < 10 ? "0" : "") + dateObject.monthID + '-' + (dateObject.date < 10 ? "0" : "") + dateObject.date + '-' + dateObject.year;
            if (dateStringFormat1 == selectedDateString || dateStringFormat2 == selectedDateString || dateStringFormat3 == selectedDateString || dateStringFormat4 == selectedDateString) {
                dateObject.className = 'activeDate';
                DRFPRO.isAvailable = true;
            }
            return dateObject;
        },
        getBaseURL: function () {
            return BASE_URL;
            //return "http://www.drf.com";
        },

        encrypt : function(plainText) {
            var iv = "00000000000000000000000000000000";
            var salt = "00000000000000000000000000000000";
            var keySize = 128;
            var iterationCount = 10000;
            var passPhrase = "drfAuth!@#123456";
            var aesUtil = new AesUtil(keySize, iterationCount);
            return aesUtil.encrypt(salt, iv, passPhrase, plainText);
        },
        getReturnURL:function(trackId,country,date,raceNumber,tabHref){
            //return '?dest-url='+encodeURIComponent(location.pathname+'/Race/'+raceNumber);
            return DRFPRO.utils.getBaseURL()+'/race-entries/track/'+trackId+'/country/'+country+'/date/'+date+'/Race/'+raceNumber+'/tabHref/'+tabHref; //sdass 8-18-2015
        },
        getReturnUrlNonEncoded:function(trackId,country,date,raceNumber,tabHref){
            //return '?dest-url='+encodeURIComponent(location.pathname+'/Race/'+raceNumber);
            return '?refferUrl='+DRFPRO.utils.encrypt('/race-entries/track/'+trackId+'/country/'+country+'/date/'+date+'/Race/'+raceNumber+'/tabHref/'+tabHref); //sdass 8-18-2015
        },

        getReturnURLQuicksheet:function(trackId,country,date,raceNumber,programNumber,article,articleId){
            //return '?dest-url='+encodeURIComponent(location.pathname+'/Race/'+raceNumber);
            if(typeof article!="undefined" && article!=""){
                if(article=="entries"){
                    var BaseUrl = DRFPRO.utils.getBaseURL()+'/race-entries/article/'; // sdass 8-14-2015
                }else{
                    var BaseUrl = DRFPRO.utils.getBaseURL()+'/race-results/article/';
                }
                return  BaseUrl+trackId+'/raceNumber/'+raceNumber+'/country/'+country+'/date/'+date+'/articleId/'+articleId;
            }else{
                return  DRFPRO.utils.getBaseURL()+'/race-entries/quickSheet/'+trackId+'/country/'+country+'/raceNumber/'+raceNumber+'/programNumber/'+programNumber+'/date/'+date;
            }

        },
        getReturnURLArticles:function(trackId,country,date,raceNumber,articleId){
            return 'article/'+trackId+'/raceNumber/'+raceNumber+
                '/country/'+country+'/date/'+date+'/articleId/'+articleId;
        },

        initiateSession: function () {
            var now = new Date(),
                cookieName = 'DRF_SESSION_ID',
                cookieValue = escape(now.getTime() + 'drf' + Math.floor((Math.random() * 1500) + 1)),
                days = 8;

            DRFPRO.utils.setCookie(cookieName,cookieValue,days)

        },

        readCookie: function (name) {
            var i, c, ca, nameEQ = name + "=";
            ca = document.cookie.split(';');
            for(i = 0; i < ca.length; i++) {
                c = ca[i];
                while(c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }
                if(c.indexOf(nameEQ) == 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return '';
        },

        setCookie: function (name,value,expiresDays) {
            var cookie = DRFPRO.utils.readCookie(name);

            if(cookie == 'undefined' || cookie.length <= 0) {
                var d = new Date();
                d.setTime(d.getTime() + (expiresDays * 24 * 60 * 60 * 1000));
                if(navigator.appName.indexOf("Microsoft") != -1) {
                    var expires = d.toGMTString();
                } else {
                    var expires = " expires=" + d.toGMTString();
                }
                document.cookie = name + "=" + value;
                document.cookie += ("; expires=" + d.toUTCString());
            }
        },
        setLoginCookie: function (name,value) {
            var cookie = DRFPRO.utils.readCookie(name);

            if(cookie == 'undefined' || cookie.length <= 0) {
                cookie = name + "=" + value;
            }
        },


        replaceStateHistory: function (url) { // Closure to protect local variable "var hash"
            if ('replaceState' in history) { // Yay, supported!
                window.history.replaceState(" "," ",url);
            } else {
                return history.location || document.location;
            }
        }
    }
})();

Array.prototype.difference = function (array2){
    var difference = [];
    var self = this;
    if(array2.length > 0 && this.length === 0){
        difference = this;
    }
    $.grep(array2, function (element) {
        if ($.inArray(element, self) == -1)
            difference.push(element);
    });
    return difference;
};
Number.prototype.ordinalSuffix = function () {
    var currentRace = this % 10;
    var suffix;
    if (currentRace == 1 && this != 11) {
        suffix =this + '<sup>st</sup>';
        return suffix;
    }
    if (currentRace == 2 && this != 12) {
        suffix= this + '<sup>nd</sup>';
        return suffix;
    }
    if (currentRace == 3 && this != 13) {
        suffix=this + '<sup>rd</sup>';
        return suffix;
    }
    suffix= this + '<sup>th</sup>';
    return suffix;
};
