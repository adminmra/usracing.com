<?php

    require_once '../simple_html_dom.php';

    function httpGet( $url ) {
        $ch = curl_init();

        curl_setopt( $ch , CURLOPT_URL , $url );
        curl_setopt( $ch , CURLOPT_RETURNTRANSFER , true );
        curl_setopt( $ch , CURLOPT_HEADER , false );

        $output = curl_exec( $ch );

        curl_close( $ch );
        return $output;
    }

    function get_params(){
        $url = "";
        foreach( $_GET as $key => $value) {
            $url .= "{$key}={$value}&";
        }
        return rtrim( $url , "&" );
    }

    $page_results = "";
    if ( isset( $_GET['TRK'] ) and isset( $_GET['CY'] ) and isset( $_GET['DATE'] ) and isset( $_GET['RN'] ) and isset( $_GET['STYLE'] ) ) {

        $_url = "espn.equibase.com/eqbQuickResultsDisplay.cfm?" . get_params();

        $page_results =  httpGet( $_url );

        $page_results = str_replace( "http://www.equibase.com/" , "https://www.equibase.com/", $page_results );
    }
    else{

        //$page_results = httpGet( "espn.equibase.com/eqbQuickResults.cfm?STYLE=ESPN" );

        $page_results = file_get_html( "http://espn.equibase.com/eqbQuickResults.cfm?STYLE=ESPN" );

        $page_results->find( "#sidead_top" , 0 )->innertext = '';

        //$page_results = str_replace( "http://www.equibase.com/banmanpro/a.aspx" , "/eqb_quick_results.php", $page_results );

        $page_results = str_replace( "http://www.equibase.com/" , "https://www.equibase.com/", $page_results );

        $page_results = preg_replace(
                "@eqbQuickResultsDisplay\.cfm.([\w\&\=\/\-]+)@" ,
                 "/iframe/eqb_quick_results.php?$1", $page_results
                );

        print_r( $page_results  );
        exit();
    }

    echo $page_results ;

 ?>
