<?php
	$content = 4;
	$title = "Newsgroup";
//echo dirname(dirname(__FILE__));
if($_SERVER['DOCUMENT_ROOT']==''){
	$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__));
}
	// Import the NNTP Utility
	require($_SERVER['DOCUMENT_ROOT']."/Web-News/webnews/nntp.php");
	require($_SERVER['DOCUMENT_ROOT']."/Web-News/config/webnews.cfg.php");
	// Increase the maximum running time to 2 mins (default is 30sec)
	set_time_limit(120);

	// Start the session before output anything
	session_name($session_name);
	session_start();

	if (is_requested("set")) {	// Save the advanced options into cookies
		$expire = 2147483647;	// Maximum integer
		setcookie("wn_pref_lang", get_request("language"), $expire);
		setcookie("wn_pref_mpp", get_request("msg_per_page"), $expire);
		
		if ($_COOKIE["wn_pref_mpp"] != get_request("msg_per_page")) {
			$change_mpp = TRUE;
		} else {
			$change_mpp = FALSE;
		}

		$_COOKIE["wn_pref_lang"] = get_request("language");
		$_COOKIE["wn_pref_mpp"] = get_request("msg_per_page");
	}

	// Read the messages file
	if (isset($_COOKIE["wn_pref_lang"])) {
		$text_ini = "config/messages_".$_COOKIE["wn_pref_lang"].".ini";
	}
	$messages_ini = read_ini_file($text_ini, true);

	// Perform logout
	if (is_requested("logout")) {
		$user = "";
		$pass = "";
		unset($_SESSION["auth"]);
		$_SESSION["logout"] = TRUE;
		unset($_SESSION["result"]);		// Destroy the subject tree.
			
		header("Location: ".construct_url($logout_url));
		exit;
	} else if (isset($_SESSION["auth"]) && $_SESSION["auth"]) {
		$user = $_SERVER['PHP_AUTH_USER'];
		$pass = $_SERVER['PHP_AUTH_PW'];
	}

	if ($auth_level > 1) {
		if (($auth_level == 3) || (is_requested("compose") && ($auth_level == 2))) {
			// Do HTTP Basic authentication
			if ($_SESSION["logout"] || !isset($_SERVER['PHP_AUTH_USER'])) {
				unset($_SESSION["logout"]);
				header('WWW-Authenticate: Basic realm="'.$realm.'"');
				header('HTTP/1.0 401 Unauthorized');
				echo $messages_ini["authorization"]["login"];
				exit;
			} else {
				// $_SESSION["auth"] must be checked firsr to avoid making too many connections
				if ($_SESSION["auth"] || verify_login($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
					$user = $_SERVER['PHP_AUTH_USER'];
					$pass = $_SERVER['PHP_AUTH_PW'];
					$_SESSION["auth"] = TRUE;
				} else {
					header('WWW-Authenticate: Basic realm="'.$realm.'"');
					header('HTTP/1.0 401 Unauthorized');
					echo $messages_ini["authorization"]["login"];
					exit;
				}
			}
			// Authentication done
		}
	} else {
		$_SESSION["auth"] = TRUE;
	}

	// Create the NNTP object
	$nntp = new NNTP($nntp_server, $user, $pass, $proxy_server, $proxy_port, $proxy_user, $proxy_pass);
	// The quit() function will be called when the script terminate.
	register_shutdown_function(create_function('', 'global $nntp; $nntp->quit();'));

// Load the newsgroups_list
	if (!isset($_SESSION["newsgroups_list"])) {	// Need to update the newsgroups_list first
		$_SESSION["newsgroups_list"] = array();
		foreach ($newsgroups_list as $group) {
			if (strpos($group, "*") !== FALSE) {	// Group name have wildmat, expand it.
				if (!$nntp->connect()) {
					unset($_SESSION["newsgroups_list"]);
					echo "<b>".$messages_ini["error"]["nntp_fail"]."</b><br>";
					echo $nntp->get_error_message()."<br>";
					exit;
				}				

				$group_list = $nntp->get_group_list($group);
				if ($group_list !== FALSE) {
					sort($group_list);
					$_SESSION["newsgroups_list"] = array_merge($_SESSION["newsgroups_list"], $group_list);
				}
			} else {
				$_SESSION["newsgroups_list"][] = $group;
			}
		}		
	}
	$newsgroups_list = $_SESSION["newsgroups_list"];
//print_r($newsgroups_list);
if (!$nntp->connect()) {
		echo "<b>".$messages_ini["error"]["nntp_fail"]."</b><br>";
		echo $nntp->get_error_message()."<br>";
	} else {
				
$message_id="11324";
//$MIME_Message = $nntp->get_article($message_id);
$groupART=$nntp->get_article_list($newsgroups_list[1]);
			$nntp->quit();
//print_r($groupART);
 //$MIME_Message;
 if(count($groupART) > 0)
 {
 	destroy($_SERVER['DOCUMENT_ROOT'].'/Web-News/storecontent/');
 }
 
		foreach($groupART as $valID)
		{
			if(!$nntp->connect())
			{}
			else
			{
				$group_info = $nntp->join_group($newsgroups_list[1]);
				//$response = $nntp->send_request("article 11324");
				$MIME_Message = $nntp->get_article($valID);
				$nntp->quit();
				//print_r($MIME_Message);
				$header = $MIME_Message->get_main_header();
					$part_header = $MIME_Message->get_part_header(1);
				$part_body = $MIME_Message->get_part_body(1);
				$parts = $MIME_Message->get_all_parts();
				//echo "<pre>";
				//print_r($response);
					//print_r($header);
					//print_r($part_header);
					//print_r($parts);
					//print_r($part_body);
				//echo "</pre>";
				$bodypart=explode("\r\n",$parts[0][body]);
				
				$bodyasArray=array();
				foreach($bodypart as $value)
				{
				$ASD=explode(":",$value);	
				$bodyasArray[$ASD[0]]=$ASD[1];
				}
				//print_r($bodyasArray);
				$Scname=explode("@",$header["message-id"]);
				$Secondname=str_replace("<","",$Scname[0]);
				$Name=explode("-",$Secondname);
				//echo $Name[0];
				$part_body = base64_decode($part_body);
				
									
					 $file= $_SERVER['DOCUMENT_ROOT']."/Web-News/storecontent/".$Name[0].".xml";
						$data=$part_body;
						if (!$fp = fopen($file, 'wb')) {
									echo "Could not write to ".$file."<br>";
							}
						else
						{
							//echo "writeable";
						}
							
							fwrite($fp, $data);
							fclose($fp);
				}
//break;
		}
}

function destroy($dir) {

    $mydir = opendir($dir);

    while(false !== ($file = readdir($mydir))) {

        if($file != "." && $file != "..") {

            chmod($dir.$file, 0777);

            if(is_dir($dir.$file)) {

                chdir('.');

               // destroy($dir.$file.'/');
			   echo "a folder";

               // rmdir($dir.$file) or DIE("couldn't delete $dir$file<br />");

            }

            else
						//echo $dir.$file."</br>";
                unlink($dir.$file) or DIE("couldn't delete $dir$file<br />");

        }

    }

    closedir($mydir);

}

?>