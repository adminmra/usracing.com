var coding = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMabcdefghijklmnopqrstuvwxyzabcdefghijklm';
var isAuthDisplayed = false;
showAuthContent();

function user_is_logged_in(){ return Check_Cookie(rot13('UTMU'))};
function logged_in(){
	if(validateDTCOOK(Get_Cookie(rot13('CKTME'))))
	{	
	var cookies = document.cookie;
	if (cookies.indexOf(rot13('UTMU')) != -1) return true;
	else return false;
	}
	else
	{
		Delete_Cookie( rot13("UTMU"), "/", "allhorseracing.com" );
		Delete_Cookie( rot13("UTMP"), "/", "allhorseracing.com" );
		Delete_Cookie( rot13("CKTME"), "/", "allhorseracing.com" );
		return false; 
	}	

}
function Check_Cookie(name)
{
	if(validateDTCOOK(Get_Cookie(rot13('CKTME'))))
	{		
			var cookies = document.cookie;
			if (cookies.indexOf(name) != -1)
			{
				var startpos = cookies.indexOf(name)+name.length+1;
				var endpos = cookies.indexOf(";",startpos)-1;
				if (endpos == -2) endpos = cookies.length;
				return unescape(cookies.substring(startpos,endpos));
			}
			else
			{
				return false; 
			}
	}
	else
	{
		Delete_Cookie( rot13("UTMU"), "/", "allhorseracing.com" );
		Delete_Cookie( rot13("UTMP"), "/", "allhorseracing.com" );
		Delete_Cookie( rot13("CKTME"), "/", "allhorseracing.com" );
		return false; 
	}

}

function showAuthContent(){

var username = Check_Cookie(rot13('UTMU'));

if(!isAuthDisplayed){	 
	try{
		if (document.styleSheets[0].cssRules)
			crossrule=document.styleSheets[0].cssRules
		else if (document.styleSheets[0].rules)
			crossrule=document.styleSheets[0].rules
		var mysheet=document.styleSheets[0]
		var myrules=mysheet.cssRules? mysheet.cssRules: mysheet.rules
		mysheet.crossdelete=mysheet.deleteRule? mysheet.deleteRule : mysheet.removeRule

		if(username){
			mysheet.crossdelete(0)
		}else{
			mysheet.crossdelete(1)
		}
	
 	}catch(err){
		var loggedinVar = getElementsByClassName('logged-in', document, '*')
		var loggedoutVar = getElementsByClassName('logged-out', document, '*')
		if(username){
			for(i in loggedinVar){
				$(loggedinVar[i]).removeClass('logged-in');	
			}
	
		}else{
			for(i in loggedoutVar){
				$(loggedoutVar[i]).removeClass('logged-out');	
			}
		}

 	}
 	isAuthDisplayed = true;
}
/**  	
if(!username){
	$('.page-racebook #col1').css("width","235px");
	$('.page-racebook #col3').css({"width":"auto","margin-left":"237px"});	
	$('.page-sportsbook #col1').css("width","253px");
	$('.page-sportsbook #col3').css({"width":"auto","margin-left":"237px"});	

}else{
	$('.page-virtualderby #col1').css("display","none");
	$('.page-virtualderby #col3').css({"width":"auto","margin":"0"});

}**/
//$('#placeholder').removeClass('placeholder');
}



function getElementsByClassName(searchClass, domNode, tagName) {
    if (domNode == null) domNode = document;
    if (tagName == null) tagName = '*';
    var el = new Array();
    var tags = domNode.getElementsByTagName(tagName);
    var tcl = " "+searchClass+" ";
    for(i=0,j=0; i<tags.length; i++) {
        var test = " " + tags[i].className + " ";
        if (test.indexOf(tcl) != -1)
        el[j++] = tags[i];
    }
    return el;
}
function setUserCookie(){
	var exdate=new Date();
	var $username = $('[name=name]').val();
	var $userpass = $('[name=pass]').val();
	Set_Cookie( rot13('UTMU'), rot13($username), 2, "/", ".allhorseracing.com", "" );
	Set_Cookie( rot13('UTMP'), rot13($userpass), 2, "/", ".allhorseracing.com", "" );
	Set_Cookie(rot13('CKTME'),exdate.getTime(), '', '/', ".allhorseracing.com", "" );
	Set_Cookie( rot13('TRACK'), rot13($username), 4320, "/", ".allhorseracing.com", "" );

}
function Set_Cookie( name, value, expires, path, domain, secure )
{

var today = new Date();
today.setTime( today.getTime() );

if ( expires )
{
expires = expires * 1000 * 60 * 60;
}
var expires_date = new Date( today.getTime() + (expires) );

document.cookie = name + "=" +escape( value ) +
( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) +
( ( path ) ? ";path=" + path : "" ) +
( ( domain ) ? ";domain=" + domain : "" ) +
( ( secure ) ? ";secure" : "" );
}

function Get_Cookie( check_name ) {
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for ( i = 0; i < a_all_cookies.length; i++ )
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );


		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if ( cookie_name == check_name )
		{
			b_cookie_found = true;
			// we need to handle case where cookie has no value but exists (no = sign, that is):
			if ( a_temp_cookie.length > 1 )
			{
				cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// note that in cases where cookie is initialized but no value, null is returned
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}
function Delete_Cookie( name, path, domain ) {
if ( Get_Cookie( name ) ) document.cookie = name + "=" +
( ( path ) ? ";path=" + path : "") +
( ( domain ) ? ";domain=" + domain : "" ) +
";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

function Check_Tracking(){
	if(Check_Cookie(rot13('TRACK'))){
		//alert('existing member');
	}else{

	}
}
Check_Tracking();
function rot13(input) {
    if (!input) return '';
    for (var output = '',i=0;i<input.length;i++) {
        character = input.charAt(i);
        position = coding.indexOf(character);
        if (position > -1)
            character = coding.charAt(position + 13);
        output += character;
    }
    return output;
}

function strstr (haystack, needle, bool) {

    var pos = 0;
    
    haystack += '';
    pos = haystack.indexOf( needle );    if (pos == -1) {
        return false;
    } else{
        if (bool){
            return haystack.substr( 0, pos );        } else{
            return haystack.slice( pos );
        }
    }
}

function explode (delimiter, string, limit) {
 
     var emptyArray = { 0: '' };
    
    // third argument is not required
    if ( arguments.length < 2 ||
        typeof arguments[0] == 'undefined' || typeof arguments[1] == 'undefined' ) {
        return null;
    }
 
    if ( delimiter === '' || delimiter === false || delimiter === null ) {
        return false;
    }
     if ( typeof delimiter == 'function' ||
        typeof delimiter == 'object' ||
        typeof string == 'function' ||
        typeof string == 'object' ) {
        return emptyArray;    }
 
    if ( delimiter === true ) {
        delimiter = '1';
    }    
    if (!limit) {
        return string.toString().split(delimiter.toString());
    } else {
        
        var partA = splitted.splice(0, limit - 1);
        var partB = splitted.join(delimiter.toString());
        partA.push(partB);
        return partA;    }
}
function implode (glue, pieces) {

    var i = '', retVal='', tGlue='';
    if (arguments.length === 1) {        
		pieces = glue;
        glue = '';
    }
    if (typeof(pieces) === 'object') {
        if (pieces instanceof Array) {            
		return pieces.join(glue);
        }
        else {
            for (i in pieces) {
                retVal += tGlue + pieces[i];                
				tGlue = glue;
            }
            return retVal;
        }
    }    else {
        return pieces;
    }
}

function isEntered(e)
{
var characterCode;
if(e && e.which) // NN4 specific code
{
    e = e;
    characterCode = e.which;
}else{
    e = event
    characterCode = e.keyCode; // IE specific code
}

if (characterCode == 13){ //// Enter key is 13
	return true;
}else return false;

}
function gup( name )
		{
  			name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  			var regexS = "[\\?&]"+name+"=([^&#]*)";
  			var regex = new RegExp( regexS );
  			var results = regex.exec( window.location.href );
  			if( results == null )
    			return false;
  			else
    			return results[1];
		}
		
		var currentImage = 1;
var NumOfImages = 5;

function play() {
  setTimeout("move(); play();", 5000);
}

function move() {
  currentImage = currentImage + 1 <= NumOfImages ? currentImage + 1 : 1;
  changeImage();
}

function changeImage() {
  $("#imgRotatingImage").fadeOut('normal', showImage);
}

function showImage() {
  document.getElementById("imgRotatingImage").src = "/Feeds/TrackResults/Cache/" + currentImage + ".jpg";
  $("#imgRotatingImage").fadeIn('fast');
}

var preloadImages = new Array();
function start() {
  for (var i = 1; i <= NumOfImages; i++) {
    preloadImages[i] = new Image();
    preloadImages[i].src = "/Feeds/TrackResults/Cache/" + i + ".jpg";
  }
  play();
}


function validateDTCOOK(CKTIME)
{
	var exdate=new Date();
var diff=(exdate.getTime()-CKTIME);
//alert(diff);
var seconds=((diff/1000)%60);
//alert(parseInt(seconds));
var minutes=((diff/(1000*60))%60);
//alert(parseInt(minutes));
	if(parseInt(minutes) < 240)
	{
		return true;
	}
	else
	{
		return false;
	}
}




    start();

