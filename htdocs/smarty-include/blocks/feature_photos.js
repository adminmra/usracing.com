var currentImage = 1;
var NumOfImages = 5;

function play() {
  setTimeout("move(); play();", 5000);
}

function move() {
  currentImage = currentImage + 1 <= NumOfImages ? currentImage + 1 : 1;
  changeImage();
}

function changeImage() {
  $("#imgRotatingImage").fadeOut('normal', showImage);
}

function showImage() {
  document.getElementById("imgRotatingImage").src = "http://www.dailyracingnews.com/images/homephotos/allhorseracing/" + currentImage + ".jpg";
  $("#imgRotatingImage").fadeIn('fast');
}

var preloadImages = new Array();
function start() {
  for (var i = 1; i <= NumOfImages; i++) {
    preloadImages[i] = new Image();
    preloadImages[i].src = "http://www.dailyracingnews.com/images/homephotos/allhorseracing/" + i + ".jpg";
  }
  play();
}

Drupal.behaviors.rotatingFeaturePhoto = function(context) {
  if ($('#imgRotatingImage').length) {
    start();
  }
}