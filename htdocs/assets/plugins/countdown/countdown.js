var x = setInterval(function () {
    var is_Edge = navigator.userAgent.indexOf("Edge") > -1;
    var is_explorer= typeof document !== 'undefined' && !!document.documentMode && !is_Edge;
    if(is_explorer) {
        var now = new Date().getTime();
    } else {
        var newYorkTime = new Date().toLocaleString("en-US", {
            timeZone: "America/New_York"
        });

        var now = new Date(newYorkTime).getTime();
    }

    var distance = countDownDate - now;

    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    if (days < 0) {
        days = "-";
    }
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    if (hours < 0) {
        hours = "-";
    }
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    if (minutes < 0) {
        minutes = "-";
    }
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (seconds < 0) {
        seconds = "-";
    }

    document.querySelector('#cd-days').innerHTML = days;
    document.querySelector('#cd-hours').innerHTML = hours;
    document.querySelector('#cd-minutes').innerHTML = minutes;
    document.querySelector('#cd-seconds').innerHTML = seconds;
}, 1000);