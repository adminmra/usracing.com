var revapi;
jQuery(document).ready(function() {

	 revapi = jQuery('.tp-banner').revolution(
				{
					delay:4000,
					autoplay:"true",
					startwidth:1440,
					startheight:600,
					autoHeight:"true",
					hideThumbs:10,
					fullWidth:"off",
					forceFullWidth:"off",
					fullScreen: "off",
					touchenabled:"on",
					onHoverStop:"on",
					hideThumbsOnMobile:"on",
					hideBulletsOnMobile:"off",
					hideArrowsOnMobile:"off",
					videoJsPath:"../videojs/",
				});

		});	//ready
