<?php	 	
// Include WordPress
define('WP_USE_THEMES', false);
//exact path for wp-load.php.
// This file is kept in the root of wordpress install
require('/home/ah/usracing.com/htdocs/news/wp-load.php');
//Query wordpress for latest 4 posts
query_posts('showposts=5');
?>

<?php	 	 while (have_posts ()): the_post(); ?>
<?php	 	 echo '<dl class="dl-horizontal story">' ?>
<?php	 	 if ( has_post_thumbnail() ) {
	 $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
	echo '<dt><a href="',  /*$full_image_url[0]*/ the_permalink()  , '" class="thumbnail "><span class="overlay-zoom">', the_post_thumbnail('thumbnail', array('class' => 'img-responsive')) , '<div class="zoom-icon"></div></span></a></dt>';
} 
?>
<?php	 	 echo '<dd>' ?>
<p class="article-title"><a href="<?php	 	 the_permalink(); ?>"><?php	 	 the_title(); ?></a></p>
<?php	 	 the_excerpt(); // Excerpt length is set in the Wordpress theme functions.php file ?> 
 
<?php	 	 echo '</dd>' ?>
<?php	 	 echo '</dl>' ?>
<?php	 	 endwhile; ?>
