+function ($){}Alert.prototype.close = function (e){varthis:$(this) var selector = $this.attr(data-target) if (!selector) { selector = $this.attr(href) selector = selector && selector.replace(/.*(?=#[^\s]*$)/,'') // strip for ie7}var $parent = $(selector)
if (e) e.preventDefault()
if (!$parent.length){$parent:$this.hasClass(alert) ? $this : $this.parent()}// ALERT PLUGIN DEFINITION
// =======================
var old = $.fn.alert
$.fn.alert = function (option){returnthiseachfunctionvarthis:$(this) var data = $this.data(bs.alert) if (!data) $this.data(bs.alert,(data = new Alert(this))) if (typeof option == 'string') data[option].call($this)}$.fn.alert.Constructor = Alert
// ALERT NO CONFLICT
// =================
$.fn.alert.noConflict = function (){$fnalert:old return this}(window.jQuery);
/* ========================================================================
* Bootstrap: button.js v3.0.0
* http://twbs.github.com/bootstrap/javascript.html#buttons
* ========================================================================
* Copyright 2013 Twitter,Inc.
*
* Licensed under the Apache License,Version 2.0 (the License);
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,software
* distributed under the License is distributed on an "AS IS" BASIS,* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ======================================================================== */
+function ($){}Button.DEFAULTS ={loadingtext:loading...}Button.prototype.setState = function (state){vard:disabled var $el = this.$element var val = $el.is(input) ? val : html var data = $el.data() state = state + Text if (!data.resetText) $el.data(resetText,$el[val]()) $el[val](data[state] || this.options[state]) // push to event loop to allow forms to submit setTimeout(function () { state == 'loadingText' ? $el.addClass(d).attr(d,d) : $el.removeClass(d).removeAttr(d); },0)}Button.prototype.toggle = function (){varparent:this.$element.closest([data-toggle="buttons"]) if ($parent.length) { var $input = this.$element.find(input) .prop(checked,!this.$element.hasClass(active)) .trigger(change) if ($input.prop(type) === 'radio') $parent.find(.active).removeClass(active)}// BUTTON PLUGIN DEFINITION
// ========================
var old = $.fn.button
$.fn.button = function (option){returnthiseachfunctionvarthis:$(this) var data = $this.data(bs.button) var options = typeof option == object && option if (!data) $this.data(bs.button,(data = new Button(this,options))) if (option == 'toggle') data.toggle() else if (option) data.setState(option)}$.fn.button.Constructor = Button
// BUTTON NO CONFLICT
// ==================
$.fn.button.noConflict = function (){$fnbutton:old return this}// BUTTON DATA-API
// ===============
$(document).on(click.bs.button.data-api,[data-toggle^=button],function (e){varbtn:$(e.target) if (!$btn.hasClass(btn)) $btn = $btn.closest(.btn) $btn.button(toggle) e.preventDefault()}(window.jQuery);
/* ========================================================================
* Bootstrap: carousel.js v3.0.0
* http://twbs.github.com/bootstrap/javascript.html#carousel
* ========================================================================
* Copyright 2012 Twitter,Inc.
*
* Licensed under the Apache License,Version 2.0 (the License);
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,software
* distributed under the License is distributed on an "AS IS" BASIS,* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ======================================================================== */
+function ($){}Carousel.DEFAULTS ={interval:5000 ,pause: hover ,wrap: true}Carousel.prototype.cycle = function (e){ethispaused:false) this.interval && clearInterval(this.interval) this.options.interval && !this.paused && (this.interval = setInterval($.proxy(this.next,this),this.options.interval)) return this}Carousel.prototype.getActiveIndex = function (){thisactive:this.$element.find(.item.active) this.$items = this.$active.parent().children() return this.$items.index(this.$active)}Carousel.prototype.to = function (pos){varthat:this var activeIndex = this.getActiveIndex() if (pos > (this.$items.length - 1) || pos < 0) return if (this.sliding) return this.$element.one(slid,function () { that.to(pos) }) if (activeIndex == pos) return this.pause().cycle() return this.slide(pos > activeIndex ? 'next' : 'prev',$(this.$items[pos]))}Carousel.prototype.pause = function (e){ethispaused:true) if (this.$element.find('.next,.prev').length && $.support.transition.end) { this.$element.trigger($.support.transition.end) this.cycle(true)}Carousel.prototype.slide = function (type,next){varactive:this.$element.find(.item.active) var $next = next || $active[type]() var isCycling = this.interval var direction = type == next ? left : right var fallback = type == next ? first : last var that = this if (!$next.length) { if (!this.options.wrap) return $next = this.$element.find(.item)[fallback]()}this.sliding = true
isCycling && this.pause()
var e = $.Event(slide.bs.carousel,{relatedtarget:$next[0],direction: direction})
if ($next.hasClass(active)) return
if (this.$indicators.length){thisindicatorsfindactiveremoveclassactivethiselementoneslidfunctionvarnextindicator:$(that.$indicators.children()[that.getActiveIndex()]) $nextIndicator && $nextIndicator.addClass(active)}if ($.support.transition && this.$element.hasClass(slide)){thiselementtriggereifeisdefaultpreventedreturnnextaddclasstypenext0offsetwidth//forcereflowactiveaddclassdirectionnextaddclassdirectionactiveonesupporttransitionendfunctionnextremoveclasstypedirectionjoinaddclassactiveactiveremoveclassactivedirectionjointhatsliding:false setTimeout(function () { that.$element.trigger(slid) },0)}else{thiselementtriggereifeisdefaultpreventedreturnactiveremoveclassactivenextaddclassactivethissliding:false this.$element.trigger(slid)}// CAROUSEL PLUGIN DEFINITION
// ==========================
var old = $.fn.carousel
$.fn.carousel = function (option){returnthiseachfunctionvarthis:$(this) var data = $this.data(bs.carousel) var options = $.extend({},Carousel.DEFAULTS,$this.data(),typeof option == 'object' && option) var action = typeof option == string ? option : options.slide if (!data) $this.data(bs.carousel,(data = new Carousel(this,options))) if (typeof option == 'number') data.to(option) else if (action) data[action]() else if (options.interval) data.pause().cycle()}$.fn.carousel.Constructor = Carousel
// CAROUSEL NO CONFLICT
// ====================
$.fn.carousel.noConflict = function (){$fncarousel:old return this}// CAROUSEL DATA-API
// =================
$(document).on(click.bs.carousel.data-api,'[data-slide], [data-slide-to]',function (e){varthis:$(this),href var $target = $($this.attr(data-target) || (href = $this.attr(href)) && href.replace(/.*(?=#[^\s]+$)/,'')) //strip for ie7 var options = $.extend({},$target.data(),$this.data()) var slideIndex = $this.attr(data-slide-to) if (slideIndex) options.interval = false $target.carousel(options) if (slideIndex = $this.attr(data-slide-to)) { $target.data(bs.carousel).to(slideIndex)})
$(window).on(load,function (){$data-ride:carousel]').each(function () {\A var $carousel = $(this)\A $carousel.carousel($carousel.data())\A })\A })\A\A}(window.jQuery);\A\A/* ========================================================================\A * Bootstrap: dropdown.js v3.0.0\A * http://twbs.github.com/bootstrap/javascript.html#dropdowns\A * ========================================================================\A * Copyright 2012 Twitter,Inc.\A *\A * Licensed under the Apache License,Version 2.0 (the "License");\A * you may not use this file except in compliance with the License.\A * You may obtain a copy of the License at\A *\A * http://www.apache.org/licenses/LICENSE-2.0\A *\A * Unless required by applicable law or agreed to in writing,software\A * distributed under the License is distributed on an "AS IS" BASIS,\A * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.\A * See the License for the specific language governing permissions and\A * limitations under the License.\A * ======================================================================== */\A\A\A+function ($) { "use strict";\A\A // DROPDOWN CLASS DEFINITION\A // =========================\A\A var backdrop = '.dropdown-backdrop'\A var toggle = '[data-toggle=dropdown]'\A var Dropdown = function (element) {\A var $el = $(element).on('click.bs.dropdown',this.toggle)\A }\A\A Dropdown.prototype.toggle = function (e) {\A var $this = $(this)\A\A if ($this.is('.disabled,:disabled')) return\A\A var $parent = getParent($this)\A var isActive = $parent.hasClass('open')\A\A clearMenus()\A\A if (!isActive) {\A if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {\A // if mobile we we use a backdrop because click events don't delegate $('<div class="dropdown-backdrop"/>').insertAfter($(this)).on(click,clearMenus)}Dropdown.prototype.keydown = function (e){if/384027/testekeycodereturnvarthis:$(this) e.preventDefault() e.stopPropagation() if ($this.is('.disabled,:disabled')) return var $parent = getParent($this) var isActive = $parent.hasClass(open) if (!isActive || (isActive && e.keyCode == 27)) { if (e.which == 27) $parent.find(toggle).focus() return $this.click()}function clearMenus(){$backdropremovetoggleeachfunctionevarparent:getParent($(this)) if (!$parent.hasClass(open)) return $parent.trigger(e = $.Event(hide.bs.dropdown)) if (e.isDefaultPrevented()) return $parent.removeClass(open).trigger(hidden.bs.dropdown)}function getParent($this){varselector:$this.attr(data-target) if (!selector) { selector = $this.attr(href) selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/,'') //strip for ie7}// DROPDOWN PLUGIN DEFINITION
// ==========================
var old = $.fn.dropdown
$.fn.dropdown = function (option){returnthiseachfunctionvarthis:$(this) var data = $this.data(dropdown) if (!data) $this.data(dropdown,(data = new Dropdown(this))) if (typeof option == 'string') data[option].call($this)}$.fn.dropdown.Constructor = Dropdown
// DROPDOWN NO CONFLICT
// ====================
$.fn.dropdown.noConflict = function (){$fndropdown:old return this}(window.jQuery);
/* ========================================================================
* Bootstrap: modal.js v3.0.0
* http://twbs.github.com/bootstrap/javascript.html#modals
* ========================================================================
* Copyright 2012 Twitter,Inc.
*
* Licensed under the Apache License,Version 2.0 (the License);
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,software
* distributed under the License is distributed on an "AS IS" BASIS,* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ======================================================================== */
+function ($){}Modal.DEFAULTS ={backdrop:true ,keyboard: true ,show: true}Modal.prototype.toggle = function (_relatedTarget){returnthisthisisshownshow:hide](_relatedTarget)}