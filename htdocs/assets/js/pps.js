    var history_pop = false;
    var viewDate = '';
    var today = new Date();
    var todayViewDate = format(today, "");
    var todayDateString = format(today, "-");
    var formated_date='';
    var track_name='';
    window.addEventListener("popstate", function(e) {
    
        // URL location
        history_pop = true;
        send_to_page();
    });
    
    function send_to_page(page, title, prevent_page_me) {
    
        if (typeof(prevent_page_me) == "undefined" || !prevent_page_me) page_me(page, title);
        viewDate = ( racedate != 0 ) ? racedate : todayViewDate;
        update_formated_date(viewDate);
        tracks = sort_array(trackList[viewDate], sortby);
        process_track_page(racetrack, raceid);
    
    }
    var go_back = function() {
        var page = window.location.pathname + window.location.search;
        page = page.replace(/&?track_id=[^&]*/g, "");
        send_to_page(page);
    
    
    }
    
    var set_page = function(page) {
        if (!page) page = window.location.pathname + window.location.search;
        var pm = page.match(/(.*)\?(.*)$/);
        if (pm && typeof(pm[1]) != "undefined") {
            //page_name= page.split('.html')[0];
            page_name = pm[1].replace(/^\//g, "");
        } else
            page_name = "index";
        //page=page.replace(/\?.*/g,"");
        return page;
    }
    
    function take_over_href() {
        /*
         * elem=elem.replace('#',''); var createMarkup=function() { return {__html:
         * html}; }; ReactDOM.render( <div dangerouslySetInnerHTML={createMarkup()}
         * />, document.getElementById(elem) );
         *
         */
        /*
        $('a').unbind();
        $('a').click(function(evt) {
            evt.preventDefault();
            if ($(this).attr('href')) send_to_page($(this).attr('href'), $(this).text());
        }); */
    
    }
    
    function page_me(page, title) {
        if (page !== undefined && page.indexOf('http') !== -1) {
            window.open(page);
        } else {
            page = set_page(page);
            page_uri = page;
            page_url = page.replace(/\?.*/, '');
            if (page != (window.location.pathname + window.location.search) && !history_pop) {
                if (typeof(title) == "undefined") title = '';
                var History = window.history; // Note: We are using a capital H instead of a lowe
                History.pushState(null, title, page_uri);
            }
            history_pop = false;
            return page_url;
        }
    }
    
    
    
    var sortby = {
        'field': 'trackname',
        'order': 'asc'
    };
    
    function sort_array(arr, sortby) {
    
        if (typeof(sortby) != "undefined")
    
        {
    
            arr = arr.sort(function(a, b)
    
                {
    
                    if (a[sortby.field] < b[sortby.field]) return (sortby.order == 'asc') ? -1 : 1;
    
                    else if (a[sortby.field] > b[sortby.field]) return (sortby.order == 'asc') ? 1 : -1;
    
                    else return 0;
    
                });
    
        }
    
        return arr;
    
    }
    
    var tracks = null;
    
    function format(date, str) {
    
        var d = date.getDate();
    
        var m = date.getMonth() + 1;
    
        var y = date.getFullYear();
    
        return '' + y + str + (m <= 9 ? '0' + m : m) + str + (d <= 9 ? '0' + d : d);
    
    }
    
    function convertCase(str) {
    
        var lower = String(str).toLowerCase();
    
        return lower.replace(/(^| )(\w)/g, function(x) {
    
            return x.toUpperCase();
    
        });
    
    }
    
    function convertDate(dateStr) {
        var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var values = dateStr.split("-");
        return monthNames[Number(values[1]) - 1] + " " + values[0] + ", " + values[2];
    }
    
    function process_track_page(track_id, race_id) {
    
        var pps_date = document.getElementById("pps-track-date-body");
        if ( track_id == -1 && race_id == -1 ) { 
           $(".pps-track-date-header").hide();
            $(".pps-track-menu-header").hide();
        pps_date.innerHTML = '';
            if (Object.keys(trackList).length > 0) {
    
                for (var i in trackList) {
    
                    var year = i.substring(0, 4);
    
                    var month = i.substring(4, 6);
    
                    var day = i.substring(6, 8);
    
                    var dateFormatted = year + "-" + month + "-" + day;
    
    
    
                    var div = document.createElement("div");
    
                    div.setAttribute("id", "pps-track-date-box-" + i);
    
                    div.setAttribute("class", "pps-track-date-box");
    
                    div.setAttribute("viewdate", i);
    
                    var child = document.createElement("div");
    
                    if (todayDateString == dateFormatted) {
    
                        child.setAttribute("class", "pps-track-date-text today");
    
                    } else {
    
                        child.setAttribute("class", "pps-track-date-text");
    
                    }
    
    
    
                    if (todayDateString == dateFormatted) {
    
                        child.innerHTML = "Today";
    
                    } else {
    
                        child.innerHTML = dateFormatted;
    
                    }
    
                    div.append(child);
    
                    pps_date.append(div);
    
                    //viewDate = i; 
    
                }
    
            }
        }
        $(".btn_down").hide();
        process_template_track(track_id, race_id);
        take_over_href();
    
    }
    
    function process_template_track(track_id, race_id) {
    
        var pps_body = document.getElementById("pps-track-menu-body");
    
        $(".pps-track-date-box").removeClass("date-box-selected");
    
        pps_body.innerHTML = '';
        if ( track_id == -1 && race_id == -1 ) { 
    
            $("#pps-tracks").show();
            $(".pps-track-date-header").show();
            $(".pps-track-menu-header").show();
            
            $("#pps-track-date-box-" + viewDate).addClass("date-box-selected");
    
            for (var i in tracks) {
    
                var track = tracks[i];
                 
                var div = document.createElement("div");
    
                div.setAttribute("class", "pps-track-menu-box");
    
                div.setAttribute("track_id", i);
                div.setAttribute("trackname", track['trackname']); 
                var child = document.createElement("div");
    
                child.setAttribute("class", "pps-track-menu-text");
    
                child.innerHTML = track['trackname'];
    
                div.append(child);
    
                pps_body.append(div);
    
    
    
            }
            $("#btn_down").hide();
            $(".pps-track-date-box").off("click");
    
            $(".pps-track-date-box").on("click", function(e) {
    
                var __date = $(this).attr('viewDate');
    
                if ($(this).hasClass("date-box-selected") && todayViewDate != __date) {
    
                    viewDate = update_formated_date(todayViewDate);
  
                } else {
    
                    viewDate = update_formated_date(__date);
    
                }
                window.location.href =window.location.origin+"/"+pathName+"/"+formated_date;
            });
    
            $("#pps-races").hide(200);
    
            $("#pp-sorter").off("click");
    
            $("#pp-sorter").on("click", function(e) {
    
                sortby['order'] = (sortby['order'] == 'asc') ? 'desc' : 'asc';
    
                var text = (sortby['order'] == 'asc') ? " Sort A-Z " : " Sort Z-A ";
    
                $(this).html(text);
                window.location.href =window.location.origin+"/"+pathName+"/"+formated_date; 
                //send_to_page('?viewDate=' + viewDate + '&sortby_order=' + sortby['order']);
    
            });
    
            $(".pps-track-menu-box").off("click");
    
            $(".pps-track-menu-box").on("click", function() {
    
                $("#pps-tracks").hide(200);
                track_name = ($(this).attr("trackname")).split(' ').join('-'); 
                var _track_id = $(this).attr("track_id");
                window.location.href =window.location.origin+"/"+pathName+"/"+formated_date+"/"+track_name; 
    
            });
        }
        process_template_race(track_id,race_id);
        process_template_horses(track_id, race_id);
    
    }
    
    function process_template_race(track_id,race_id) {
    
    
    
        $("#pps-race-back").off("click");
    
        $("#pps-race-back").on("click", function() {
            window.location.href =window.location.origin+"/"+pathName+"/"+formated_date; 
        });
    
    
    
        var races_body = document.getElementById("pps-race-content");
    
        races_body.innerHTML = '';
        if (typeof(track_id) !== "undefined" && track_id != -1 && race_id==-1) {
            var track = tracks[track_id];
            track_name = track['trackname'].split(' ').join('-');
            var race = track['trackrace']["0"];
            $("#pps-race-track-header").html('<div><span class="race-title">' + track['trackname'] + '</span>' + "<br>" + '<span class="race-date">' + convertDate(race['race_date']) + '</span></div>');

            for (var i in track['trackrace']) {
    
                var race = track['trackrace'][i];
    
                var div = create_race_div(parseInt(i) + 1);
    
                div.append(create_race_header(track_id, parseInt(i) + 1));
    
                div.append(create_race_data(track_id, parseInt(i) + 1));
    
                var arrow = document.createElement("div");
    
                arrow.setAttribute("class", "race-arrow race-right");
    
                div.append(arrow);
    
                races_body.append(div);
    
            }
    
            $(".pps-race-box").off("click");
    
            $(".pps-race-box").on("click", function() {
    
                $("#pps-races").hide(200);
                var id = $(this).attr("race_id");
                /*var currenturl=location.href;
                var url=currenturl + "?date=" +id;
                alert ("URL : "+ url);
    
                window.location.replace(url,"_self");*/
    
                window.location.href =window.location.origin+"/"+pathName+"/"+formated_date+"/"+track_name+"/race-"+id; 
    
            });
            $("#btn_down").hide();
            $("#pps-races").show();
        }
    
    }
    
    function process_template_horses(track_id, race_id) {

        var horses_body = document.getElementById("pps-horse-content");
        $("#pps-horse-header").hide();
        
        horses_body.innerHTML = '';
    
        if (typeof(track_id) != "undefined" && typeof(race_id) != "undefined" && parseInt(track_id) >= 0 && parseInt(race_id) >= 0) {
            var track = tracks[track_id];
            track_name = track['trackname'].split(' ').join('-'); 
            var race = track['trackrace'][race_id - 1];
    
            var raceNum = Number([race_id]);
    
    
            for (var i in race['horsedata']) {
    
                var horse = race['horsedata'][i];
    
                var div = create_horse_div(i);
    
                div.append(create_horse_header(horse, horse['program']));
    
                div.append(create_horse_data(horse));
    
                var arrow = document.createElement("div");
    
                arrow.setAttribute("class", "horse-arrow race-right");
    
                div.append(arrow);
    
                horses_body.append(div);
    
    
    
            }
    
            $("#pps-horse-back").off("click");
    
            $("#pps-horse-back").on("click", function() {
    
                $("#pps-horses").hide(200);
   
                window.location.href =window.location.origin+"/"+pathName+"/"+formated_date+"/"+track_name; 
            });
    
            $(".pps-horse-box").off("click");
    
            $(".pps-horse-box").on("click", function() {
    
    
    
                //var id = $(this).attr("horseId");
    
                //horseid = id;
                //window.location.href =window.location.origin+"/"+pathName+"/"+viewDate+"/"+track_id+"/"+id; 
                //under construct
            });
    
            $("#pps-races").hide(200);
    
            $("#pps-horses").show();
            $("#pps-horse-header").show();
            $("#pps-track-date").show();
              $(".btn_down").show();
            process_template_race_header(track_id, race_id);
    
            process_template_race_numb(track_id, race_id);
    
    
    
            process_template_claiming(track_id, race_id);
    
            process_template_bet_opt(track_id, race_id);
        }
    }
    
    function process_template_race_header(track_id, race_id) {
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
    
        var raceNum = Number([race_id]);
    
        var div = "#pps-horse-track-header";
        var x = race['race_date'];
        //var x= format(race['race_date'],"")
        //alert(convertDate(race['race_date']));
        $(div).html(track['trackname'] + "<br>" + race['race_date'] /* + "<br>" + raceNum */ );
    
        $("#pps-horse-track-header").html('<div><span class="race-title">' + track['trackname'] + '</span>' + "<br>" + '<span class="race-date">' + convertDate(race['race_date']) + '</span></div>');
    
    }
    
    function process_template_race_numb(track_id, race_id) {
    
        var raceNum = Number([race_id]);
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
    
        var div = ".pps-race-numb";
    
        var track = tracks[track_id];
    
        $("#carousel-body").html("");
    
        var carousel_html = process_template_race_carousel(track_id, race_id);
    
        document.getElementById("carousel-body").append(carousel_html);
    
        $('.carousel-left').off('click');
    
        $('.carousel-right').off('click');
    
        $('.carousel-left').on('click', function() {
    
            var _race_id = parseInt(race_id) - 1;
    
            if (_race_id == 0) _race_id = Object.keys(track['trackrace']).length;
            window.location.href =window.location.origin+"/"+pathName+"/"+formated_date+"/"+track_name+"/race-"+_race_id; 
    
        });
    
        $('.carousel-right').on('click', function() {
    
            var _race_id = parseInt(race_id) + 1;
    
            if (_race_id > Object.keys(track['trackrace']).length) _race_id = 1;
             window.location.href =window.location.origin+"/"+pathName+"/"+formated_date+"/"+track_name+"/race-"+_race_id; 
    
        });
    
    
    
    }
    
    function process_template_claiming(track_id, race_id) {
    
        var div = ".pps-horse-claiming";
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
        var __claim = ( claiming == -1 ) ? race['claimamt'] : claiming; 
        var __purse = ( purse == -1 ) ? race['purse'] : purse;
        $(div).html('<div><span class="claiming-title">Claiming : </span>' + '<span class="claimamt-value">' + '$' + __claim + '</span>' + ' </div><div> <span class="purse-title">Purse : </span>' + '<span class="purse-value">' + '$' + __purse + '</span></div>');
    
    }
    
    function process_template_bet_opt(track_id, race_id) {
    
        var div = ".pps-horse-bet-opt";
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
    
        $(div).html(convertCase(race['bet_opt']));
    
    }
    
    function process_template_race_carousel(track_id, race_id) {
    
        var ol = document.createElement("ol");
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id];
    
        ol.classList.add("content");
    
        var raceNum = Number([race_id]);
    
        for (var r in track['trackrace']) {
    
            var li = document.createElement("li");
    
            li.innerHTML = '<span class="race"> Race ' + track['trackrace'][r]['race'] + ' </span>' + '<br>' + '<span class="date">' + track['trackrace'][r]['post_time'] + '</span>';
    
            if (r == Number([race_id]) - 1) {
    
                li.classList.toggle("active");
    
            } else {
    
                li.classList.add("inactive");
    
                li.classList.remove("active");
    
            }
    
            ol.appendChild(li);
    
        }
    
        return ol;
    
    }
    
    /*
    
    function create_carousel_div(){
    
        var div = document.createElement("div");
    
        div.setAttribute("id","pps-horse-track-header-carousel");
    
        div.setAttribute("class","pps-header-carousel");
    
        var left = document.createElement("span");
    
        left.setAttribute("class","carousel-left");
    
        div.append(left);
    
        var body = document.createElement("div");
    
        body.setAttribute("class","carousel-body");
    
        div.append(body);
    
        var right = document.createElement("span");
    
        right.setAttribute("class","carousel-right");
    
        div.append(right);      
    
        return div;
    
    }
    
    */
    
    function create_horse_div(i) {
    
        var div = document.createElement("div");
    
        div.setAttribute("class", "pps-horse-box");
    
        div.setAttribute("id", "pps-horse-box-" + i);
    
        div.setAttribute("horseId", i);
    
        return div;
    
    }
    
    function create_race_div(i) {
    
        var div = document.createElement("div");
    
        div.setAttribute("class", "pps-race-box");
    
        div.setAttribute("id", "pps-race-box-" + i);
    
        div.setAttribute("race_id", i);
    
        return div;
    
    }
    
    function create_race_header(track_id, race_id) {
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
    
        var raceNumb = Number(race_id);
    
        var race_header = document.createElement("div");
    
        race_header.setAttribute("class", "pps-race-text-header");
    
        var race_date = document.createElement("div");
    
        var race_text = document.createElement("div");
    
        var race_time = document.createElement("div");
    
        race_text.setAttribute("class", "pps-race-text-header-text");
    
        race_date.setAttribute("class", "pps-race-text-header-date");
    
        race_time.setAttribute("class", "pps-race-text-header-time");
    
        race_text.innerHTML = "Race " + raceNumb;
    
        //race_date.innerHTML = race['race_date'];
    
        race_time.innerHTML = race['post_time'];
    
        race_header.append(race_text);
    
        race_header.append(race_date);
    
        race_header.append(race_time);
    
        return race_header;
    
    }
    
    function create_horse_header(horse, i) {
    
        var horse_header = document.createElement("div");
    
        horse_header.setAttribute("class", "pps-horse-text-header");
    
        var horse_date = document.createElement("div");
    
        var horse_text = document.createElement("div");
    
        horse_text.setAttribute("class", "pps-horse-text-header-text color" + i);
    
        horse_date.setAttribute("class", "pps-horse-text-header-name");
    
        horse_text.innerHTML = i;
    
        horse_date.innerHTML = horse['morn_odds'];
    
        horse_header.append(horse_text);
    
        horse_header.append(horse_date);
    
        return horse_header;
    
    }
    
    function create_race_data(track_id, race_id) {
    
        var track = tracks[track_id];
    
        var race = track['trackrace'][race_id - 1];
    
        var race_data = document.createElement("div");
    
        race_data.setAttribute("class", "pps-race-text-body");
    
        var header = document.createElement("span");
    
        header.setAttribute("class", "pps-race-text-body-header");
    
        header.innerHTML = 'Claiming';
    
        var body = document.createElement("span");
    
        body.setAttribute("class", "pps-race-text ");
    
        var _body = document.createElement("span");
    
        _body.setAttribute("class", "pps-race-text");
    
        _body.innerHTML = convertCase(race['bet_opt']);
         
        body.innerHTML = 'Distance : ' + race['distance'] + race['dist_unit'] + ' - Purse : ' + race['purse'] + ' - Claiming : ' + race['claimamt'];
    
        race_data.append(header);
    
        race_data.append(body);
    
        race_data.append(_body);
    
        return race_data;
    
    }
    
    function create_horse_data(horse) {
    
        var horse_data = document.createElement("div");
    
        horse_data.setAttribute("class", "pps-horse-text-body");
    
        /*horse_data.innerHTML = (style="margin-bottom: 30px !important");*/
    
    
    
        var horse_name = document.createElement("span");
    
        horse_name.setAttribute("class", "pps-horse-text-horse horse");
    
        horse_name.innerHTML = convertCase(horse['horse_name']);
    
        horse_data.append(horse_name);
    
        /*
    
        var breeder = document.createElement("span");
    
        breeder.setAttribute("class","pps-horse-text");
    
        breeder.innerHTML = 'Breeder: ' + horse['breeder'];
    
        horse_data.append(breeder);
    
        */
    
        var owner = document.createElement("span");
    
        owner.setAttribute("class", "pps-horse-text-owner");
    
        owner.innerHTML = horse['owner_name'];
    
        horse_data.append(owner);
    
    
    
        var jockey = document.createElement("span");
    
        jockey.setAttribute("class", "pps-horse-text-jockey");
    
        jockey.innerHTML = horse['jockey']['jock_disp'];
    
        horse_data.append(jockey);
    
    
    
        var trainer = document.createElement("span");
    
        trainer.setAttribute("class", "pps-horse-text-trainer");
    
        trainer.innerHTML = horse['trainer']['tran_disp'];
    
        horse_data.append(trainer);
    
        /*
    
        var sire = document.createElement("span");
    
        sire.setAttribute("class","pps-horse-text");
    
        sire.innerHTML = 'Sire: ' + horse['sire']['sirename'];
    
        horse_data.append(sire);
    
                
    
        var dam = document.createElement("span");
    
        dam.setAttribute("class","pps-horse-text");
    
        dam.innerHTML = 'Dam: ' + horse['dam']['damname'];
    
        horse_data.append(dam);
    
                
    
        var morn_odds = document.createElement("span");
    
        morn_odds.setAttribute("class","pps-horse-text");
    
        morn_odds.innerHTML = 'Morn Odds: ' + horse['morn_odds'];
    
        horse_data.append(morn_odds);
    
        */
    
        return horse_data;
    
    }
   function update_formated_date(_date){
        var dateString = _date.toString();
        var year        = dateString.substring(0,4);
        var month       = dateString.substring(4,6);
        var day         = dateString.substring(6,8);
        formated_date =  year + "-" + month + "-" + day;
        return _date;
   }
    $(function() {
    
        send_to_page();
    
    });
