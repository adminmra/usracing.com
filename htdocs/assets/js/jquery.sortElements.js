//jQuery.fn.sortElements=(function(){var a=[].sort;return function(c,d){d=d||function(){return this};var b=this.map(function(){var f=d.call(this),e=f.parentNode,g=e.insertBefore(document.createTextNode(""),f.nextSibling);return function(){if(e===this){throw new Error("You can't sort elements if any one is a descendant of another.")}e.insertBefore(this,g);e.removeChild(g)}});return a.call(this,c).each(function(e){b[e].call(d.call(this))})}})();

jQuery.fn.sortElements = (function() {
    var a = [].sort;
    return function(c, d) {
        d = d || function() {
            return this
        };
        var b = this.map(function() {
            var f = d.call(this),
                e = f.parentNode,
                g = e.insertBefore(document.createTextNode(""), f.nextSibling);
            return function() {
                if (e === this) {
                    throw new Error("You can't sort elements if any one is a descendant of another.")
                }
                e.insertBefore(this, g);
                e.removeChild(g)
            }
        });
        return a.call(this, c).each(function(e) {
            b[e].call(d.call(this))
        })
    }
})();
