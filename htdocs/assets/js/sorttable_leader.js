function removeString(str){
var res = str.replace("$", "");
var res2 = res.replace(/,/g, "");
return res2;
}
jQuery(document).ready(function() {
    var a = jQuery("table.ordenableResult tbody");
    jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function() {
        var d = jQuery(this),
            c = d.index(),
            b = true;
        d.click(function() {
            a.find("td").filter(function() {
                return jQuery(this).index() === c
            }).sortElements(function(f, e) {
                t_a = removeString(jQuery.text([f]));
                t_b = removeString(jQuery.text([e]));
				
                if(isNaN(t_a) || isNaN(t_b)){
					//alert(t_a);
			return jQuery.text([e]) > jQuery.text([f]) ? b ? -1 : 1 : b ? 1 : -1;
		}
		else{  return parseInt(t_b) > parseInt(t_a) ? b ? -1 : 1 : b ? 1 : -1; }
            }, function() {
                return this.parentNode
            });
            b = !b
        })
    });
    jQuery("table.ordenableResult thead th:first").trigger("click")
});