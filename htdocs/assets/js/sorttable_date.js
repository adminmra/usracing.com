function is_date(str){

return !isNaN(Date.parse(str));
}
jQuery(document).ready(function() {
    var a = jQuery("table.ordenableResult tbody");
    jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function() {
        var d = jQuery(this),
            c = d.index(),
            b = true;
        d.click(function() {
            a.find("td").filter(function() {
                return jQuery(this).index() === c
            }).sortElements(function(f, e) {
                t_a = jQuery.text([f]);
                t_b = jQuery.text([e]);
                if(is_date(t_a) && is_date(t_b)){
			return Date.parse(t_a) < Date.parse(t_b) ? b ? -1 : 1 : b ? 1 : -1;
		}
		else return jQuery.text([f]) > jQuery.text([e]) ? b ? -1 : 1 : b ? 1 : -1
            }, function() {
                return this.parentNode
            });
            b = !b
        })
    });
    jQuery("table.ordenableResult thead th:first").trigger("click")
});