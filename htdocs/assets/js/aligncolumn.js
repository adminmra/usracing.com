function alignColumn(columns, text_align) {
	columns.forEach(function(item, index) {
		var max_width = 0;
		$('table tbody tr td:nth-child('+item+')').each(function(k, v){
			$(this).html('<span>' + $(this).html() + '</span>');
			var current_width = $(this).find("span").width();
			if(current_width > max_width) {
				max_width = current_width;
			}
		});
		$('table tbody tr td:nth-child('+item+') span').css({
			"width": (max_width+16) + "px",
			"display": "block",
			"margin": "auto",
			"text-align": text_align
		});
	});
}

function alignTableColumn(args) {
	//args = [{table:1, column:[{position:1, align:'left'}]}]
	
	args.forEach(function(item, index) {
		columns.forEach(function(item1, index1) {
			var max_width = 0;
			$('table:nth-child('+item.table+') tbody tr td:nth-child('+item1.position+')').each(function(k, v){
				$(this).html('<span>' + $(this).html() + '</span>');
				var current_width = $(this).find("span").width();
				if(current_width > max_width) {
					max_width = current_width;
				}
			});
			$('table:nth-child('+item.table+') tbody tr td:nth-child('+item1.position+') span').css({
				"width": (max_width+16) + "px",
				"display": "block",
				"margin": "auto",
				"text-align": item1.align
			});
		});
	});
}