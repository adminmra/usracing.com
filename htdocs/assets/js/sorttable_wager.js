function removeString(str){
var res = str.replace("$", "");
var res2 = res.replace(/,/g, "");
var res3 = res2.replace('/*/g', "");
var res4 = res3.replace('/#/g', "");
return res4;
}
function addSort_Wager(id){
jQuery(document).ready(function() {
    var a = jQuery("#"+id+" tbody");
    jQuery("#"+id+" thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function() {
        var d = jQuery(this),
            c = d.index(),
            b = true;
        d.click(function() {
            a.find("td").filter(function() {
                return jQuery(this).index() === c
            }).sortElements(function(f, e) {
                t_a = removeString(jQuery.text([f]));
                t_b = removeString(jQuery.text([e]));
				
                if(isNaN(t_a) || isNaN(t_b)){
					//alert(t_a);
			return jQuery.text([e]) > jQuery.text([f]) ? b ? -1 : 1 : b ? 1 : -1;
		}
		else{  return parseInt(t_b) > parseInt(t_a) ? b ? -1 : 1 : b ? 1 : -1; }
            }, function() {
                return this.parentNode
            });
            b = !b
        })
    });
    jQuery("#"+id+" thead th:first").trigger("click")
});

}