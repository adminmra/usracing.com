/*jQuery(document).ready(function(){var a=jQuery("table.ordenableResult tbody");jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function(){var d=jQuery(this),c=d.index(),b=false;d.click(function(){a.find("td").filter(function(){return jQuery(this).index()===c}).sortElements(function(f,e){t_a=jQuery.text([f]);t_b=jQuery.text([e]);if(t_a.indexOf("/")>0&&t_b.indexOf("/")>0){arr_a=t_a.split("/");arr_b=t_b.split("/");itemA_num=parseInt(arr_a[0]);itemB_num=parseInt(arr_b[0]);itemA_den=parseInt(arr_a[1]);itemB_den=parseInt(arr_b[1]);if(itemA_num>=itemA_den){americanA="+"+(itemA_num/itemA_den)*100}else{americanA="-"+(itemA_num/itemA_den)*100}if(itemB_num>=itemB_den){americanB="+"+(itemB_num/itemB_den)*100}else{americanB="-"+(itemB_num/itemB_den)*100}itemA=parseInt(americanA);itemB=parseInt(americanB);return itemA>itemB?b?-1:1:b?1:-1}if((t_a[0]=="-"||t_a[0]=="+")&&(t_b[0]=="-"||t_b[0]=="+")){itemA=parseInt(t_a);itemB=parseInt(t_b);console.log(itemA+" > "+itemB);return itemA>itemB?b?-1:1:b?1:-1}else{return jQuery.text([f])>jQuery.text([e])?b?-1:1:b?1:-1}},function(){return this.parentNode});b=!b})});jQuery("table.ordenableResult tbody th:first").trigger("click")});*/
function is_date(str){

return !isNaN(Date.parse(str));
}
jQuery(document).ready(function() {
    var a = jQuery("table.ordenableResult tbody");
    jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function() {
        var d = jQuery(this),
            c = d.index(),
            b = true;
        d.click(function() {
            a.find("td").filter(function() {
                return jQuery(this).index() === c
            }).sortElements(function(f, e) {
                t_a = jQuery.text([f]);
                t_b = jQuery.text([e]);
                if(isNaN(t_a) || isNaN(t_b)){
			return jQuery.text([e]) > jQuery.text([f]) ? b ? -1 : 1 : b ? 1 : -1;
		}
		else return parseInt(jQuery.text([e])) > parseInt(jQuery.text([f])) ? b ? -1 : 1 : b ? 1 : -1
            }, function() {
                return this.parentNode
            });
            b = !b
        })
    });
    jQuery("table.ordenableResult thead th:first").trigger("click")
});