     $(function () {
		 var CTACounter = 1;	
		 var textCounter = 1;
		 var heroCounter = 0;
		 var mbheroCounter = 0;
		 var refStore="";
		 //var hostname = location.hostname;
		 var hostname = "usracing.com"; 
		 var RefQstring=GetPathnameQstring(window.location.pathname);		 
		 var pathname = window.location.pathname.replace('/','');
		 pathname=pathname.replace(/\//g, '-');
		$("a[href*='ref=']").each(function(i){ 
			var ref= GetParameterValues('ref', this.href);
			//HUNG - 2021-05-01 - Comment out variables are not used "parentclassREF", "mbclassREF", "classREF" (because it causes a page error /preakness-stakes/odds)			
			/*var parentclassREF=$( this ).parents('div').parents('div').get( 0 ).className;
			var mbclassREF=$( this ).parents().get( 0 ).className;
			var classREF=this.className;*/
			var replaceval= "ref="+ref;
			if(ref==""){ref=refStore; if(refStore==""){ ref=pathname; } }else{ refStore=ref;}
			//var replaceref= 'site='+hostname+'&ref='+ref+'&signup-modal=open';
			var replaceref= 'site='+hostname+'&ref='+ref;
			//ref=RefQstring;
			//var replaceref= 'site='+hostname+'&ref='+ref;
/*			if(!$(this).attr('data-ref-val')){ 
			if(classREF.search("btn-sm") >= 0){
			var replaceref= replaceref+'&bl=header&cta=CTA';
			}else if(parentclassREF.search("usrHero") >= 0){
				if(mbclassREF.search("copy-sm") >= 0){
				if(mbheroCounter == 0){ var replaceref= replaceref+'&bl=sm-hero&cta=CTA'; }else{ var replaceref= replaceref+'&bl=sm-hero-'+mbheroCounter+'&cta=CTA'; }
				mbheroCounter++;
				}else{
				if(heroCounter == 0){ var replaceref= replaceref+'&bl=hero&cta=CTA'; }else{var replaceref= replaceref+'&bl=hero-'+heroCounter+'&cta=CTA'; }
				heroCounter++;
				}
			}else if(parentclassREF.search("newHero") >= 0){
				if(heroCounter == 0){ var replaceref= replaceref+'&bl=hero&cta=CTA'; }else{var replaceref= replaceref+'&bl=hero-'+heroCounter+'&cta=CTA'; }
				heroCounter++;				
			}else if(classREF.search("btn-xlrg") >= 0){				
			var replaceref= replaceref+'&cta=CTA-'+CTACounter;
			CTACounter++;
			}else if(classREF.search("joinracebook") >= 0){
			var replaceref= replaceref+'&bl=menu-join-racebook';
			}else if(classREF.search("card_btn") >= 0){
			var replaceref= replaceref+'&bl=block-exciting&cta=CTA';
			}else if(classREF.search("bs-bonus_btn") >= 0){
			var replaceref= replaceref+'&bl=bonus-btn';
			}else if(classREF.search("footSign") >= 0){
			var replaceref= replaceref+'&bl=footer';
			}else if(classREF.search("sliderBtn") >= 0){
			var replaceref= replaceref+'&bl=hero';
			}else{				
			var replaceref= replaceref+'-text-'+textCounter;
			textCounter++;
			}
				
			}else{
			 var refVal=$(this).attr('data-ref-val');			 
			 if(refVal.search("CTA") >= 0){
				var BLVAL=refVal.split("-CTA");
				var replaceref= replaceref+'&bl='+BLVAL[0]+'&cta=CTA';
			 }else{			 	
			 var replaceref= replaceref+'&bl='+refVal;
			 }
			}
			*/
			this.href = this.href.replace(replaceval, replaceref);
					
		});
		$("a[href^='/signup/?ref=']").each(function(){ this.href = this.href.replace("/signup/?ref=", "/signup?ref=usr-"); });	
		$("a[href^='/signup/']").each(function(){ this.href = this.href.replace("/signup/", "/signup?ref=usr-"+pathname); });	
    })
	
	function GetParameterValues(param, urltoSearch) {  
		var url = urltoSearch.slice(urltoSearch.indexOf('?') + 1).split('&');  
		for (var i = 0; i < url.length; i++) {  
			var urlparam = url[i].split('=');  
			if (urlparam[0] == param) {  
				return urlparam[1];  
			}  
		}  
	} 
	function GetPathnameQstring(pathname) { 
		var QStringCreated=""; 
		var QString = pathname.split('/');  
		for (var i = 1; i < QString.length; i++) {  var k=(i-1);
		var refval =(QString[i]=="")? 'index':QString[i];
		QStringCreated =(i==1)? QStringCreated+"&ref="+refval:(k>1)?QStringCreated+"&subpage"+k+"="+refval:QStringCreated = QStringCreated+"&subpage="+refval;
		}		
		return  QStringCreated; 
	}