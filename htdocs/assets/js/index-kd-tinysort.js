!function(L){var K=/iPhone/i,J=/iPod/i,I=/iPad/i,H=/(?=.*\bAndroid\b)(?=.*\bMobile\b)/i,G=/Android/i,F=/(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i,E=/(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i,D=/IEMobile/i,C=/(?=.*\bWindows\b)(?=.*\bARM\b)/i,B=/BlackBerry/i,A=/BB10/i,z=/Opera Mini/i,y=/(CriOS|Chrome)(?=.*\bMobile\b)/i,x=/(?=.*\bFirefox\b)(?=.*\bMobile\b)/i,w=new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)","i"),v=function(d,c){return d.test(c)},u=function(b){var d=b||navigator.userAgent,c=d.split("[FBAN");return"undefined"!=typeof c[1]&&(d=c[0]),c=d.split("Twitter"),"undefined"!=typeof c[1]&&(d=c[0]),this.apple={phone:v(K,d),ipod:v(J,d),tablet:!v(K,d)&&v(I,d),device:v(K,d)||v(J,d)||v(I,d)},this.amazon={phone:v(F,d),tablet:!v(F,d)&&v(E,d),device:v(F,d)||v(E,d)},this.android={phone:v(F,d)||v(H,d),tablet:!v(F,d)&&!v(H,d)&&(v(E,d)||v(G,d)),device:v(F,d)||v(E,d)||v(H,d)||v(G,d)},this.windows={phone:v(D,d),tablet:v(C,d),device:v(D,d)||v(C,d)},this.other={blackberry:v(B,d),blackberry10:v(A,d),opera:v(z,d),firefox:v(x,d),chrome:v(y,d),device:v(B,d)||v(A,d)||v(z,d)||v(x,d)||v(y,d)},this.seven_inch=v(w,d),this.any=this.apple.device||this.android.device||this.windows.device||this.other.device||this.seven_inch,this.phone=this.apple.phone||this.android.phone||this.windows.phone,this.tablet=this.apple.tablet||this.android.tablet||this.windows.tablet,"undefined"==typeof window?this:void 0},t=function(){var b=new u;return b.Class=u,b};"undefined"!=typeof module&&module.exports&&"undefined"==typeof window?module.exports=u:"undefined"!=typeof module&&module.exports&&"undefined"!=typeof window?module.exports=t():"function"==typeof define&&define.amd?define("isMobile",[],L.isMobile=t()):L.isMobile=t()}(this);$(document).ready(function(){if(!isMobile.phone){$("img").each(function(a,b){var c=$(this).attr("data-src-img");$(this).attr("src",c)})}});(function(a,c){if(typeof define==="function"&&define.amd){define("tinysort",b)}else{a.tinysort=c}function b(){return c}}(this,(function(){var l=!1,d,n=null,j=window,p=j.document,b=parseFloat,c=/(-?\d+\.?\d*)\s*$/g,k=/(\d+\.?\d*)\s*$/g,f=[],a=0,o=0,g=String.fromCharCode(4095),e={selector:n,order:"asc",attr:n,data:n,useVal:l,place:"org",returns:l,cases:l,natural:l,forceStrings:l,ignoreDashes:l,sortFunction:n,useFlex:l,emptyEnd:l};function q(P,v){if(E(P)){P=p.querySelectorAll(P)}if(P.length===0){console.warn("No elements to sort")}var r=p.createDocumentFragment(),F=[],L=[],I=[],y,z=[],B,C=true,x=P.length&&P[0].parentNode,A=x.rootNode!==document,M=P.length&&(v===d||v.useFlex!==false)&&!A&&getComputedStyle(x,null).display.indexOf("flex")!==-1;w.apply(n,Array.prototype.slice.call(arguments,1));N();L.sort(K);t();function w(){if(arguments.length===0){O({})}else{h(arguments,function(Q){O(E(Q)?{selector:Q}:Q)})}a=z.length}function O(R){var S=!!R.selector,Q=S&&R.selector[0]===":",T=m(R||{},e);z.push(m({hasSelector:S,hasAttr:!(T.attr===n||T.attr===""),hasData:T.data!==n,hasFilter:Q,sortReturnNumber:T.order==="asc"?1:-1},T))}function N(){h(P,function(X,T){if(!B){B=X.parentNode}else{if(B!==X.parentNode){C=false}}var W=z[0],R=W.hasFilter,Q=W.selector,U=!Q||(R&&X.matchesSelector(Q))||(Q&&X.querySelector(Q)),S=U?L:I,V={elm:X,pos:T,posn:S.length};F.push(V);S.push(V)});y=L.slice(0)}function D(S,R,X){var T=X(S.toString()),V=X(R.toString());for(var Q=0;T[Q]&&V[Q];Q++){if(T[Q]!==V[Q]){var W=Number(T[Q]),U=Number(V[Q]);if(W==T[Q]&&U==V[Q]){return W-U}else{return T[Q]>V[Q]?1:-1}}}return T.length-V.length}function u(U){var X=[],R=0,W=-1,V=0,T,S;while(T=(S=U.charAt(R++)).charCodeAt(0)){var Q=(T==46||(T>=48&&T<=57));if(Q!==V){X[++W]="";V=Q}X[W]+=S}return X}function K(ab,aa){var U=0;if(o!==0){o=0}while(U===0&&o<a){var ac=z[o],Z=ac.ignoreDashes?k:c;h(f,function(ae){var af=ae.prepare;if(af){af(ac)}});if(ac.sortFunction){U=ac.sortFunction(ab,aa)}else{if(ac.order=="rand"){U=Math.random()<0.5?1:-1}else{var S=l,R=H(ab,ac),ad=H(aa,ac),Y=R===""||R===d,W=ad===""||ad===d;if(R===ad){U=0}else{if(ac.emptyEnd&&(Y||W)){U=Y&&W?0:Y?1:-1}else{if(!ac.forceStrings){var T=E(R)?R&&R.match(Z):l,Q=E(ad)?ad&&ad.match(Z):l;if(T&&Q){var X=R.substr(0,R.length-T[0].length),V=ad.substr(0,ad.length-Q[0].length);if(X==V){S=!l;R=b(T[0]);ad=b(Q[0])}}}if(R===d||ad===d){U=0}else{if(!ac.natural||(!isNaN(R)&&!isNaN(ad))){U=R<ad?-1:(R>ad?1:0)}else{U=D(R,ad,u)}}}}}}h(f,function(af){var ae=af.sort;if(ae){U=ae(ac,S,R,ad,U)}});U*=ac.sortReturnNumber;if(U===0){o++}}if(U===0){U=ab.pos>aa.pos?1:-1}return U}function t(){var T=L.length===F.length;if(C&&T){if(M){L.forEach(function(ad,ac){ad.elm.style.order=ac})}else{if(B){B.appendChild(J())}else{console.warn("parentNode has been removed")}}}else{var ab=z[0],U=ab.place,S=U==="org",Z=U==="start",X=U==="end",R=U==="first",V=U==="last";if(S){L.forEach(s);L.forEach(function(ad,ac){G(y[ac],ad.elm)})}else{if(Z||X){var Y=y[Z?0:y.length-1],Q=Y&&Y.elm.parentNode,W=Q&&(Z&&Q.firstChild||Q.lastChild);if(W){if(W!==Y.elm){Y={elm:W}}s(Y);X&&Q.appendChild(Y.ghost);G(Y,J())}}else{if(R||V){var aa=y[R?0:y.length-1];G(s(aa),J())}}}}}function J(){L.forEach(function(Q){r.appendChild(Q.elm)});return r}function s(R){var Q=R.elm,S=p.createElement("div");R.ghost=S;Q.parentNode.insertBefore(S,Q);return R}function G(Q,T){var S=Q.ghost,R=S.parentNode;R.insertBefore(T,S);R.removeChild(S);delete Q.ghost}function H(S,R){var T,Q=S.elm;if(R.selector){if(R.hasFilter){if(!Q.matchesSelector(R.selector)){Q=n}}else{Q=Q.querySelector(R.selector)}}if(R.hasAttr){T=Q.getAttribute(R.attr)}else{if(R.useVal){T=Q.value||Q.getAttribute("value")}else{if(R.hasData){T=Q.getAttribute("data-"+R.data)}else{if(Q){T=Q.textContent}}}}if(E(T)){if(!R.cases){T=T.toLowerCase()}T=T.replace(/\s+/g," ")}if(T===null){T=g}return T}function E(Q){return typeof Q==="string"}return L.map(function(Q){return Q.elm})}function h(v,u){var r=v.length,t=r,s;while(t--){s=r-t-1;u(v[s],s)}}function m(v,t,r){for(var u in t){if(r||v[u]===d){v[u]=t[u]}}return v}function i(r,s,t){f.push({prepare:r,sort:s,sortBy:t})}j.Element&&(function(r){r.matchesSelector=r.matchesSelector||r.mozMatchesSelector||r.msMatchesSelector||r.oMatchesSelector||r.webkitMatchesSelector||function(s){var v=this,t=(v.parentNode||v.document).querySelectorAll(s),u=-1;while(t[++u]&&t[u]!=v){}return !!t[u]}})(Element.prototype);m(i,{loop:h});return m(q,{plugin:i,defaults:e})})()));