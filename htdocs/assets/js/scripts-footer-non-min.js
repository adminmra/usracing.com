//drop down
! function (e, t, o) {
    var i = e();
    e.fn.dropdownHover = function (o) {
        return i = i.add(this.parent()), this.each(function () {
            var n, a = e(this),
                r = a.parent(),
                l = {
                    delay: e(this).data("delay"),
                    instantlyCloseOthers: e(this).data("close-others")
                },
                s = e.extend(!0, {}, {
                    delay: 500,
                    instantlyCloseOthers: !0
                }, o, l);
            r.hover(function (e) {
                if (!r.hasClass("open") && !a.is(e.target)) return !0;
                !0 === s.instantlyCloseOthers && i.removeClass("open"), t.clearTimeout(n), r.addClass("open")
            }, function () {
                n = t.setTimeout(function () {
                    r.removeClass("open")
                }, s.delay)
            }), a.hover(function () {
                !0 === s.instantlyCloseOthers && i.removeClass("open"), t.clearTimeout(n), r.addClass("open")
            }), r.find(".dropdown-submenu").each(function () {
                var o, i = e(this);
                i.hover(function () {
                    t.clearTimeout(o), i.children(".dropdown-menu").show(), i.siblings().children(".dropdown-menu").hide()
                }, function () {
                    var e = i.children(".dropdown-menu");
                    o = t.setTimeout(function () {
                        e.hide()
                    }, s.delay)
                })
            })
        })
    }, e(document).ready(function () {
        e('[data-hover="dropdown"]').dropdownHover()
    })
}(jQuery, this),

//this code is for  the hambuger menu(For dektop) / images resizing  / media resizing  / orontencion change
function (e, t, o, i) {
    var n = o(e),
        a = o(t),
        r = o.fancybox = function () {
            r.open.apply(this, arguments)
        },
        l = null,
        s = t.createTouch !== i,
        c = function (e) {
            return e && e.hasOwnProperty && e instanceof o
        },
        d = function (e) {
            return e && "string" === o.type(e)
        },
        u = function (e) {
            return d(e) && 0 < e.indexOf("%")
        },
        p = function (e, t) {
            var o = parseInt(e, 10) || 0;
            return t && u(e) && (o *= r.getViewport()[t] / 100), Math.ceil(o)
        },
        h = function (e, t) {
            return p(e, t) + "px"
        };
    o.extend(r, {
        version: "2.1.3",
        defaults: {
            padding: 15,
            margin: 20,
            width: 800,
            height: 600,
            minWidth: 100,
            minHeight: 100,
            maxWidth: 9999,
            maxHeight: 9999,
            autoSize: !0,
            autoHeight: !1,
            autoWidth: !1,
            autoResize: !0,
            autoCenter: !s,
            fitToView: !0,
            aspectRatio: !1,
            topRatio: .5,
            leftRatio: .5,
            scrolling: "auto",
            wrapCSS: "",
            arrows: !0,
            closeBtn: !0,
            closeClick: !1,
            nextClick: !1,
            mouseWheel: !0,
            autoPlay: !1,
            playSpeed: 3e3,
            preload: 3,
            modal: !1,
            loop: !0,
            ajax: {
                dataType: "html",
                headers: {
                    "X-fancyBox": !0
                }
            },
            iframe: {
                scrolling: "auto",
                preload: !0
            },
            swf: {
                wmode: "transparent",
                allowfullscreen: "true",
                allowscriptaccess: "always"
            },
            keys: {
                next: {
                    13: "left",
                    34: "up",
                    39: "left",
                    40: "up"
                },
                prev: {
                    8: "right",
                    33: "down",
                    37: "right",
                    38: "down"
                },
                close: [27],
                play: [32],
                toggle: [70]
            },
            direction: {
                next: "left",
                prev: "right"
            },
            scrollOutside: !0,
            index: 0,
            type: null,
            href: null,
            content: null,
            title: null,
            //creates the hamburger
            tpl: {
                wrap: '<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                image: '<img class="fancybox-image" src="{href}" alt="" />',
                iframe: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen' + (o.browser.msie ? ' allowtransparency="true"' : "") + "></iframe>",
                error: '<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
                closeBtn: '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                prev: '<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
            },
            openEffect: "fade",
            openSpeed: 250,
            openEasing: "swing",
            openOpacity: !0,
            openMethod: "zoomIn",
            closeEffect: "fade",
            closeSpeed: 250,
            closeEasing: "swing",
            closeOpacity: !0,
            closeMethod: "zoomOut",
            nextEffect: "elastic",
            nextSpeed: 250,
            nextEasing: "swing",
            nextMethod: "changeIn",
            prevEffect: "elastic",
            prevSpeed: 250,
            prevEasing: "swing",
            prevMethod: "changeOut",
            helpers: {
                overlay: !0,
                title: !0
            },
            onCancel: o.noop,
            beforeLoad: o.noop,
            afterLoad: o.noop,
            beforeShow: o.noop,
            afterShow: o.noop,
            beforeChange: o.noop,
            beforeClose: o.noop,
            afterClose: o.noop
        },
        group: {},
        opts: {},
        previous: null,
        coming: null,
        current: null,
        isActive: !1,
        isOpen: !1,
        isOpened: !1,
        wrap: null,
        skin: null,
        outer: null,
        inner: null,
        player: {
            timer: null,
            isActive: !1
        },
        ajaxLoad: null,
        imgPreload: null,
        transitions: {},
        helpers: {},
        //orientation change mobile
        update: function (e) {
            var t = e && e.type,
                o = !t || "orientationchange" === t;
            o && (clearTimeout(l), l = null), r.isOpen && !l && (l = setTimeout(function () {
                var i = r.current;
                i && !r.isClosing && (r.wrap.removeClass("fancybox-tmp"), (o || "load" === t || "resize" === t && i.autoResize) && r._setDimension(), "scroll" === t && i.canShrink || r.reposition(e), r.trigger("onUpdate"), l = null)
            }, o && !s ? 0 : 300))
        },
        isImage: function (e) {
            return d(e) && e.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp)((\?|#).*)?$)/i)
        },
        isSWF: function (e) {
            return d(e) && e.match(/\.(swf)((\?|#).*)?$/i)
        },
        //hambuger menu
        _start: function (e) {
            var t, i, n = {};
            if (e = p(e), !(t = r.group[e] || null)) return !1;
            if (t = (n = o.extend(!0, {}, r.opts, t)).margin, i = n.padding, "number" === o.type(t) && (n.margin = [t, t, t, t]), "number" === o.type(i) && (n.padding = [i, i, i, i]), n.modal && o.extend(!0, n, {
                    closeBtn: !1,
                    closeClick: !1,
                    nextClick: !1,
                    arrows: !1,
                    mouseWheel: !1,
                    keys: null,
                    helpers: {
                        overlay: {
                            closeClick: !1
                        }
                    }
                }), n.autoSize && (n.autoWidth = n.autoHeight = !0), "auto" === n.width && (n.autoWidth = !0), "auto" === n.height && (n.autoHeight = !0), n.group = r.group, n.index = e, r.coming = n, !1 === r.trigger("beforeLoad")) r.coming = null;
            else {
                if (i = n.type, t = n.href, !i) return r.coming = null, !(!r.current || !r.router || "jumpto" === r.router) && (r.current.index = e, r[r.router](r.direction));
                if (r.isActive = !0, "image" !== i && "swf" !== i || (n.autoHeight = n.autoWidth = !1, n.scrolling = "visible"), "image" === i && (n.aspectRatio = !0), "iframe" === i && s && (n.scrolling = "scroll"), n.wrap = o(n.tpl.wrap).addClass("fancybox-" + (s ? "mobile" : "desktop") + " fancybox-type-" + i + " fancybox-tmp " + n.wrapCSS).appendTo(n.parent || "body"), o.extend(n, {
                        skin: o(".fancybox-skin", n.wrap),
                        outer: o(".fancybox-outer", n.wrap),
                        inner: o(".fancybox-inner", n.wrap)
                    }), o.each(["Top", "Right", "Bottom", "Left"], function (e, t) {
                        n.skin.css("padding" + t, h(n.padding[e]))
                    }), r.trigger("onReady"), "inline" === i || "html" === i) {
                    if (!n.content || !n.content.length) return r._error("content")
                } else if (!t) return r._error("href");
                "image" === i ? r._loadImage() : "ajax" === i ? r._loadAjax() : "iframe" === i ? r._loadIframe() : r._afterLoad()
            }
        },
        _error: function (e) {
            o.extend(r.coming, {
                type: "html",
                autoWidth: !0,
                autoHeight: !0,
                minWidth: 0,
                minHeight: 0,
                scrolling: "no",
                hasError: e,
                content: r.coming.tpl.error
            }), r._afterLoad()
        },
        _afterZoomIn: function () {
            var e = r.current;
            e && (r.isOpen = r.isOpened = !0, r.wrap.css("overflow", "visible").addClass("fancybox-opened"), r.update(), (e.closeClick || e.nextClick && 1 < r.group.length) && r.inner.css("cursor", "pointer").bind("click.fb", function (t) {
                !o(t.target).is("a") && !o(t.target).parent().is("a") && (t.preventDefault(), r[e.closeClick ? "close" : "next"]())
            }), e.closeBtn && o(e.tpl.closeBtn).appendTo(r.skin).bind(s ? "touchstart.fb" : "click.fb", function (e) {
                e.preventDefault(), r.close()
            }), e.arrows && 1 < r.group.length && ((e.loop || 0 < e.index) && o(e.tpl.prev).appendTo(r.outer).bind("click.fb", r.prev), (e.loop || e.index < r.group.length - 1) && o(e.tpl.next).appendTo(r.outer).bind("click.fb", r.next)), r.trigger("afterShow"), e.loop || e.index !== e.group.length - 1 ? r.opts.autoPlay && !r.player.isActive && (r.opts.autoPlay = !1, r.play()) : r.play(!1))
        },
        _afterZoomOut: function (e) {
            e = e || r.current, o(".fancybox-wrap").trigger("onReset").remove(), o.extend(r, {
                group: {},
                opts: {},
                router: !1,
                current: null,
                isActive: !1,
                isOpened: !1,
                isOpen: !1,
                isClosing: !1,
                wrap: null,
                skin: null,
                outer: null,
                inner: null
            }), r.trigger("afterClose", e)
        }
    })
}(window, document, jQuery),
function (e) {
    var t = e.fancybox,
        o = function (t, o, i) {
            return i = i || "", "object" === e.type(i) && (i = e.param(i, !0)), e.each(o, function (e, o) {
                t = t.replace("$" + e, o || "")
            }), i.length && (t += (t.indexOf("?") > 0 ? "&" : "?") + i), t
        };
    t.helpers.media = {
        defaults: {
            youtube: {
                matcher: /(youtube\.com|youtu\.be)\/(watch\?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*)).*/i,
                params: {
                    autoplay: 1,
                    autohide: 1,
                    fs: 1,
                    rel: 0,
                    hd: 1,
                    wmode: "opaque",
                    enablejsapi: 1
                },
                type: "iframe",
                url: "//www.youtube.com/embed/$3"
            },
            vimeo: {
                matcher: /(?:vimeo(?:pro)?.com)\/(?:[^\d]+)?(\d+)(?:.*)/,
                params: {
                    autoplay: 1,
                    hd: 1,
                    show_title: 1,
                    show_byline: 1,
                    show_portrait: 0,
                    fullscreen: 1
                },
                type: "iframe",
                url: "//player.vimeo.com/video/$1"
            },
            metacafe: {
                matcher: /metacafe.com\/(?:watch|fplayer)\/([\w\-]{1,10})/,
                params: {
                    autoPlay: "yes"
                },
                type: "swf",
                url: function (t, o, i) {
                    return i.swf.flashVars = "playerVars=" + e.param(o, !0), "//www.metacafe.com/fplayer/" + t[1] + "/.swf"
                }
            },
            dailymotion: {
                matcher: /dailymotion.com\/video\/(.*)\/?(.*)/,
                params: {
                    additionalInfos: 0,
                    autoStart: 1
                },
                type: "swf",
                url: "//www.dailymotion.com/swf/video/$1"
            },
            twitvid: {
                matcher: /twitvid\.com\/([a-zA-Z0-9_\-\?\=]+)/i,
                params: {
                    autoplay: 0
                },
                type: "iframe",
                url: "//www.twitvid.com/embed.php?guid=$1"
            },
            twitpic: {
                matcher: /twitpic\.com\/(?!(?:place|photos|events)\/)([a-zA-Z0-9\?\=\-]+)/i,
                type: "image",
                url: "//twitpic.com/show/full/$1/"
            },
            instagram: {
                matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
                type: "image",
                url: "//$1/p/$2/media/"
            },
            google_maps: {
                matcher: /maps\.google\.([a-z]{2,3}(\.[a-z]{2})?)\/(\?ll=|maps\?)(.*)/i,
                type: "iframe",
                url: function (e) {
                    return "//maps.google." + e[1] + "/" + e[3] + e[4] + "&output=" + (e[4].indexOf("layer=c") > 0 ? "svembed" : "embed")
                }
            }
        },
        beforeLoad: function (t, i) {
            var n, a, r, l, s = i.href || "",
                c = !1;
            for (n in t)
                if (a = t[n], r = s.match(a.matcher)) {
                    c = a.type, l = e.extend(!0, {}, a.params, i[n] || (e.isPlainObject(t[n]) ? t[n].params : null)), s = "function" === e.type(a.url) ? a.url.call(this, r, l, i) : o(a.url, r, l);
                    break
                } c && (i.href = s, i.type = c, i.autoHeight = !1)
        }
    }
}(jQuery),
function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function (e) {
    var t, o = {
            wheelSpeed: 10,
            wheelPropagation: !1,
            minScrollbarLength: null,
            useBothWheelAxes: !1,
            useKeyboard: !0,
            suppressScrollX: !1,
            suppressScrollY: !1,
            scrollXMarginOffset: 0,
            scrollYMarginOffset: 0
        },
        i = (t = 0, function () {
            var e = t;
            return t += 1, ".perfect-scrollbar-" + e
        });
    e.fn.perfectScrollbar = function (t, n) {
        return this.each(function () {
            var a = e.extend(!0, {}, o),
                r = e(this);
            if ("object" == typeof t ? e.extend(!0, a, t) : n = t, "update" === n) return r.data("perfect-scrollbar-update") && r.data("perfect-scrollbar-update")(), r;
            if ("destroy" === n) return r.data("perfect-scrollbar-destroy") && r.data("perfect-scrollbar-destroy")(), r;
            if (r.data("perfect-scrollbar")) return r.data("perfect-scrollbar");
            r.addClass("ps-container");
            var l, s, c, d, u, p, h, f, m, g, v, w, y, b, x, $ = e("<div class='ps-scrollbar-x-rail'></div>").appendTo(r),
                k = e("<div class='ps-scrollbar-y-rail'></div>").appendTo(r),
                S = e("<div class='ps-scrollbar-x'></div>").appendTo($),
                C = e("<div class='ps-scrollbar-y'></div>").appendTo(k),
                T = parseInt($.css("bottom"), 10),
                _ = parseInt(k.css("right"), 10),
                I = i(),
                L = function (e) {
                    return a.minScrollbarLength && (e = Math.max(e, a.minScrollbarLength)), e
                },
                O = function () {
                    $.css({
                        left: r.scrollLeft(),
                        bottom: T - r.scrollTop(),
                        width: c,
                        display: l ? "inherit" : "none"
                    }), k.css({
                        top: r.scrollTop(),
                        right: _ - r.scrollLeft(),
                        height: d,
                        display: s ? "inherit" : "none"
                    }), S.css({
                        left: f,
                        width: h
                    }), C.css({
                        top: g,
                        height: m
                    })
                },
                E = function () {
                    c = r.width(), d = r.height(), u = r.prop("scrollWidth"), p = r.prop("scrollHeight"), !a.suppressScrollX && u > c + a.scrollXMarginOffset ? (l = !0, h = L(parseInt(c * c / u, 10)), f = parseInt(r.scrollLeft() * (c - h) / (u - c), 10)) : (l = !1, h = 0, f = 0, r.scrollLeft(0)), !a.suppressScrollY && p > d + a.scrollYMarginOffset ? (s = !0, m = L(parseInt(d * d / p, 10)), g = parseInt(r.scrollTop() * (d - m) / (p - d), 10)) : (s = !1, m = 0, g = 0, r.scrollTop(0)), g >= d - m && (g = d - m), f >= c - h && (f = c - h), O()
                },
                H = function (e, t) {
                    var o = r.scrollTop();
                    if (0 === e) {
                        if (!s) return !1;
                        if (0 === o && t > 0 || o >= p - d && 0 > t) return !a.wheelPropagation
                    }
                    var i = r.scrollLeft();
                    if (0 === t) {
                        if (!l) return !1;
                        if (0 === i && 0 > e || i >= u - c && e > 0) return !a.wheelPropagation
                    }
                    return !0
                },
                M = "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch;
            return (v = navigator.userAgent.toLowerCase().match(/(msie) ([\w.]+)/)) && "msie" === v[1] && (y = parseInt(v[2], 10), r.addClass("ie").addClass("ie" + y), 6 === y && (b = function () {
                    e(this).addClass("hover")
                }, x = function () {
                    e(this).removeClass("hover")
                }, r.bind("mouseenter" + I, b).bind("mouseleave" + I, x), $.bind("mouseenter" + I, b).bind("mouseleave" + I, x), k.bind("mouseenter" + I, b).bind("mouseleave" + I, x), S.bind("mouseenter" + I, b).bind("mouseleave" + I, x), C.bind("mouseenter" + I, b).bind("mouseleave" + I, x), O = function () {
                    S.css({
                        left: f + r.scrollLeft(),
                        bottom: T,
                        width: h
                    }), C.css({
                        top: g + r.scrollTop(),
                        right: _,
                        height: m
                    }), S.hide().show(), C.hide().show()
                })), E(), r.bind("scroll" + I, function () {
                    E()
                }),
                function () {
                    var t, o;
                    S.bind("mousedown" + I, function (e) {
                        o = e.pageX, t = S.position().left, $.addClass("in-scrolling"), e.stopPropagation(), e.preventDefault()
                    }), e(document).bind("mousemove" + I, function (e) {
                        $.hasClass("in-scrolling") && (function (e, t) {
                            var o = e + t,
                                i = c - h;
                            f = 0 > o ? 0 : o > i ? i : o;
                            var n = parseInt(f * (u - c) / (c - h), 10);
                            r.scrollLeft(n), k.css({
                                right: _ - n
                            })
                        }(t, e.pageX - o), e.stopPropagation(), e.preventDefault())
                    }), e(document).bind("mouseup" + I, function () {
                        $.hasClass("in-scrolling") && $.removeClass("in-scrolling")
                    }), t = o = null
                }(),
                function () {
                    var t, o;
                    C.bind("mousedown" + I, function (e) {
                        o = e.pageY, t = C.position().top, k.addClass("in-scrolling"), e.stopPropagation(), e.preventDefault()
                    }), e(document).bind("mousemove" + I, function (e) {
                        k.hasClass("in-scrolling") && (function (e, t) {
                            var o = e + t,
                                i = d - m;
                            g = 0 > o ? 0 : o > i ? i : o;
                            var n = parseInt(g * (p - d) / (d - m), 10);
                            r.scrollTop(n), $.css({
                                bottom: T - n
                            })
                        }(t, e.pageY - o), e.stopPropagation(), e.preventDefault())
                    }), e(document).bind("mouseup" + I, function () {
                        k.hasClass("in-scrolling") && k.removeClass("in-scrolling")
                    }), t = o = null
                }(),
                function () {
                    var e = function (e) {
                        e.stopPropagation()
                    };
                    C.bind("click" + I, e), k.bind("click" + I, function (e) {
                        var t = parseInt(m / 2, 10),
                            o = (e.pageY - k.offset().top - t) / (d - m);
                        0 > o ? o = 0 : o > 1 && (o = 1), r.scrollTop((p - d) * o)
                    }), S.bind("click" + I, e), $.bind("click" + I, function (e) {
                        var t = parseInt(h / 2, 10),
                            o = (e.pageX - $.offset().left - t) / (c - h);
                        0 > o ? o = 0 : o > 1 && (o = 1), r.scrollLeft((u - c) * o)
                    })
                }(), M && function () {
                    var t = function (e, t) {
                            r.scrollTop(r.scrollTop() - t), r.scrollLeft(r.scrollLeft() - e), E()
                        },
                        o = {},
                        i = 0,
                        n = {},
                        a = null,
                        l = !1;
                    e(window).bind("touchstart" + I, function () {
                        l = !0
                    }), e(window).bind("touchend" + I, function () {
                        l = !1
                    }), r.bind("touchstart" + I, function (e) {
                        var t = e.originalEvent.targetTouches[0];
                        o.pageX = t.pageX, o.pageY = t.pageY, i = (new Date).getTime(), null !== a && clearInterval(a), e.stopPropagation()
                    }), r.bind("touchmove" + I, function (e) {
                        if (!l && 1 === e.originalEvent.targetTouches.length) {
                            var a = e.originalEvent.targetTouches[0],
                                r = {};
                            r.pageX = a.pageX, r.pageY = a.pageY;
                            var s = r.pageX - o.pageX,
                                c = r.pageY - o.pageY;
                            t(s, c), o = r;
                            var d = (new Date).getTime();
                            n.x = s / (d - i), n.y = c / (d - i), i = d, e.preventDefault()
                        }
                    }), r.bind("touchend" + I, function () {
                        clearInterval(a), a = setInterval(function () {
                            return .01 > Math.abs(n.x) && .01 > Math.abs(n.y) ? void clearInterval(a) : (t(30 * n.x, 30 * n.y), n.x *= .8, void(n.y *= .8))
                        }, 10)
                    })
                }(), r.mousewheel && function () {
                    var e = !1;
                    r.bind("mousewheel" + I, function (t, o, i, n) {
                        a.useBothWheelAxes ? s && !l ? n ? r.scrollTop(r.scrollTop() - n * a.wheelSpeed) : r.scrollTop(r.scrollTop() + i * a.wheelSpeed) : l && !s && (i ? r.scrollLeft(r.scrollLeft() + i * a.wheelSpeed) : r.scrollLeft(r.scrollLeft() - n * a.wheelSpeed)) : (r.scrollTop(r.scrollTop() - n * a.wheelSpeed), r.scrollLeft(r.scrollLeft() + i * a.wheelSpeed)), E(), (e = H(i, n)) && t.preventDefault()
                    }), r.bind("MozMousePixelScroll" + I, function (t) {
                        e && t.preventDefault()
                    })
                }(), a.useKeyboard && (w = !1, r.bind("mouseenter" + I, function () {
                    w = !0
                }), r.bind("mouseleave" + I, function () {
                    w = !1
                }), e(document).bind("keydown" + I, function (e) {
                    if (w) {
                        var t = 0,
                            o = 0;
                        switch (e.which) {
                            case 37:
                                t = -3;
                                break;
                            case 38:
                                o = 3;
                                break;
                            case 39:
                                t = 3;
                                break;
                            case 40:
                                o = -3;
                                break;
                            case 33:
                                o = 9;
                                break;
                            case 32:
                            case 34:
                                o = -9;
                                break;
                            case 35:
                                o = -d;
                                break;
                            case 36:
                                o = d;
                                break;
                            default:
                                return
                        }
                        r.scrollTop(r.scrollTop() - o * a.wheelSpeed), r.scrollLeft(r.scrollLeft() + t * a.wheelSpeed), H(t, o) && e.preventDefault()
                    }
                })), r.data("perfect-scrollbar", r), r.data("perfect-scrollbar-update", E), r.data("perfect-scrollbar-destroy", function () {
                    r.unbind(I), e(window).unbind(I), e(document).unbind(I), r.data("perfect-scrollbar", null), r.data("perfect-scrollbar-update", null), r.data("perfect-scrollbar-destroy", null), S.remove(), C.remove(), $.remove(), k.remove(), S = C = c = d = u = p = h = f = T = m = g = _ = null
                }), r
        })
    }
}),
function (e) {
    function t(t) {
        var o = t || window.event,
            i = [].slice.call(arguments, 1),
            n = 0,
            a = 0,
            r = 0;
        return (t = e.event.fix(o)).type = "mousewheel", o.wheelDelta && (n = o.wheelDelta / 120), o.detail && (n = -o.detail / 3), r = n, void 0 !== o.axis && o.axis === o.HORIZONTAL_AXIS && (r = 0, a = -1 * n), void 0 !== o.wheelDeltaY && (r = o.wheelDeltaY / 120), void 0 !== o.wheelDeltaX && (a = -1 * o.wheelDeltaX / 120), i.unshift(t, n, a, r), (e.event.dispatch || e.event.handle).apply(this, i)
    }
    var o = ["DOMMouseScroll", "mousewheel"];
    if (e.event.fixHooks)
        for (var i = o.length; i;) e.event.fixHooks[o[--i]] = e.event.mouseHooks;
    e.event.special.mousewheel = {
        setup: function () {
            if (this.addEventListener)
                for (var e = o.length; e;) this.addEventListener(o[--e], t, !1);
            else this.onmousewheel = t
        },
        teardown: function () {
            if (this.removeEventListener)
                for (var e = o.length; e;) this.removeEventListener(o[--e], t, !1);
            else this.onmousewheel = null
        }
    }, e.fn.extend({
        mousewheel: function (e) {
            return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
        },
        unmousewheel: function (e) {
            return this.unbind("mousewheel", e)
        }
    })
}(jQuery);
var App = function () {
    return $ = jQuery, {
        init: function () {
            jQuery(".dateCarousel").carousel({
                    interval: 4e3,
                    pause: "hover"
                }), jQuery(".dateCarousel").find(".item:first").addClass("active"), jQuery(".tooltips").tooltip(),
                function () {
                    ! function (e, t, o) {
                        function i(e, t) {
                            var i = this,
                                n = o(i);
                            if (i.value == n.attr("placeholder") && n.hasClass("placeholder"))
                                if (n.data("placeholder-password")) {
                                    if (n = n.hide().next().show().attr("id", n.removeAttr("id").data("placeholder-id")), !0 === e) return n[0].value = t;
                                    n.focus()
                                } else i.value = "", n.removeClass("placeholder"), i == a() && i.select()
                        }

                        function n() {
                            var e, t = this,
                                n = o(t),
                                a = this.id;
                            if ("" == t.value) {
                                if ("password" == t.type) {
                                    if (!n.data("placeholder-textinput")) {
                                        try {
                                            e = n.clone().attr({
                                                type: "text"
                                            })
                                        } catch (t) {
                                            e = o("<input>").attr(o.extend(function (e) {
                                                var t = {},
                                                    i = /^jQuery\d+$/;
                                                return o.each(e.attributes, function (e, o) {
                                                    o.specified && !i.test(o.name) && (t[o.name] = o.value)
                                                }), t
                                            }(this), {
                                                type: "text"
                                            }))
                                        }
                                        e.removeAttr("name").data({
                                            "placeholder-password": n,
                                            "placeholder-id": a
                                        }).bind("focus.placeholder", i), n.data({
                                            "placeholder-textinput": e,
                                            "placeholder-id": a
                                        }).before(e)
                                    }
                                    n = n.removeAttr("id").hide().prev().attr("id", a).show()
                                }
                                n.addClass("placeholder"), n[0].value = n.attr("placeholder")
                            } else n.removeClass("placeholder")
                        }

                        function a() {
                            try {
                                return t.activeElement
                            } catch (e) {}
                        }
                        var r, l, s = "[object OperaMini]" == Object.prototype.toString.call(e.operamini),
                            c = "placeholder" in t.createElement("input") && !s,
                            d = "placeholder" in t.createElement("textarea") && !s,
                            u = o.fn,
                            p = o.valHooks,
                            h = o.propHooks;
                        c && d ? (l = u.placeholder = function () {
                            return this
                        }).input = l.textarea = !0 : ((l = u.placeholder = function () {
                            return this.filter((c ? "textarea" : ":input") + "[placeholder]").not(".placeholder").bind({
                                "focus.placeholder": i,
                                "blur.placeholder": n
                            }).data("placeholder-enabled", !0).trigger("blur.placeholder"), this
                        }).input = c, l.textarea = d, r = {
                            get: function (e) {
                                var t = o(e),
                                    i = t.data("placeholder-password");
                                return i ? i[0].value : t.data("placeholder-enabled") && t.hasClass("placeholder") ? "" : e.value
                            },
                            set: function (e, t) {
                                var r = o(e),
                                    l = r.data("placeholder-password");
                                return l ? l[0].value = t : r.data("placeholder-enabled") ? ("" == t ? (e.value = t, e != a() && n.call(e)) : r.hasClass("placeholder") && i.call(e, !0, t) || (e.value = t), r) : e.value = t
                            }
                        }, c || (p.input = r, h.value = r), d || (p.textarea = r, h.value = r), o(function () {
                            o(t).delegate("form", "submit.placeholder", function () {
                                var e = o(".placeholder", this).each(i);
                                setTimeout(function () {
                                    e.each(n)
                                }, 10)
                            })
                        }), o(e).bind("beforeunload.placeholder", function () {
                            o(".placeholder").each(function () {
                                this.value = ""
                            })
                        }))
                    }(this, document, jQuery)
                }(), jQuery("input, textarea").placeholder();
            var e = {
                setting: {
                    startline: 100,
                    scrollto: 0,
                    scrollduration: 1e3,
                    fadeduration: [500, 100]
                },
                controlHTML: '<img src="/img/up.png" style="width:51px; height:42px" alt="up" />',
                controlattrs: {
                    offsetx: 5,
                    offsety: 5
                },
                anchorkeyword: "#top",
                state: {
                    isvisible: !1,
                    shouldvisible: !1
                },
                scrollup: function () {
                    this.cssfixedsupport || this.$control.css({
                        opacity: 0
                    });
                    var e = isNaN(this.setting.scrollto) ? this.setting.scrollto : parseInt(this.setting.scrollto);
                    e = "string" == typeof e && 1 == jQuery("#" + e).length ? jQuery("#" + e).offset().top : 0, this.$body.animate({
                        scrollTop: e
                    }, this.setting.scrollduration)
                },
                keepfixed: function () {
                    var e = jQuery(window),
                        t = e.scrollLeft() + e.width() - this.$control.width() - this.controlattrs.offsetx,
                        o = e.scrollTop() + e.height() - this.$control.height() - this.controlattrs.offsety;
                    this.$control.css({
                        left: t + "px",
                        top: o + "px"
                    })
                },
                togglecontrol: function () {
                    var e = jQuery(window).scrollTop();
                    this.cssfixedsupport || this.keepfixed(), this.state.shouldvisible = e >= this.setting.startline, this.state.shouldvisible && !this.state.isvisible ? (this.$control.stop().animate({
                        opacity: 1
                    }, this.setting.fadeduration[0]), this.state.isvisible = !0) : 0 == this.state.shouldvisible && this.state.isvisible && (this.$control.stop().animate({
                        opacity: 0
                    }, this.setting.fadeduration[1]), this.state.isvisible = !1)
                },
                init: function () {
                    jQuery(document).ready(function (t) {
                        var o = e,
                            i = document.all;
                        o.cssfixedsupport = !i || i && "CSS1Compat" == document.compatMode && window.XMLHttpRequest, o.$body = window.opera ? "CSS1Compat" == document.compatMode ? t("html") : t("body") : t("html,body"), o.$control = t('<div id="topcontrol">' + o.controlHTML + "</div>").css({
                            position: o.cssfixedsupport ? "fixed" : "absolute",
                            bottom: o.controlattrs.offsety,
                            right: o.controlattrs.offsetx,
                            opacity: 0,
                            cursor: "pointer"
                        }).attr({
                            title: "Scroll Back to Top"
                        }).click(function () {
                            return o.scrollup(), !1
                        }).appendTo("body"), document.all && !window.XMLHttpRequest && "" != o.$control.text() && o.$control.css({
                            width: o.$control.width()
                        }), o.togglecontrol(), t('a[href="' + o.anchorkeyword + '"]').click(function () {
                            return o.scrollup(), !1
                        }), t(window).bind("scroll resize", function (e) {
                            o.togglecontrol()
                        })
                    })
                }
            };
            e.init(), $("#left-nav").length && $("#nav .navbar-header").prepend("<a class='navbar-left' id='ol'  href='#left-nav' ><i class='fa fa-bars'></i><i class='glyphicon glyphicon-remove'></i></a>"), $("select").focus(function () {
                $(this).addClass("selected")
            }), $("select").blur(function () {
                "0" === this.value && $(this).removeClass("selected")
            }), $("#loginButton").click(function () {
                var e = (new Date).getTimezoneOffset() / 60;
                $("#timezone").val(e);
                var t = $("#loginForm"),
                    o = $("#account").val(),
                    i = $("#pin").val(),
                    n = $("#state").val(),
                    a = !0;
                "" == o && ($("#account").parent().addClass("required"), a = !1), "" == i && ($("#pin").parent().addClass("required"), a = !1), "0" == n && ($("#state").parent().addClass("required"), a = !1), a && ($("body > div").hide(), $("#top").show(), $(".login, .login-header, .topR").hide(), $("#processing").show(), $(document).scrollTop(0), setTimeout(function () {
                    t.submit()
                }, 3e3))
            }), $(window).scroll(function (e) {
                $(window).scrollTop() > 200 ? $("#top, #nav, #left-nav-btn").addClass("is_stuck") : $("#top, #nav, #left-nav-btn").removeClass("is_stuck"), e.preventDefault()
            }), $("#ol").sidr({
                name: "left-menu",
                side: "left",
                speed: 300,
                source: "#left-nav",
                renaming: !1,
                onOpen: function () {
                    $("#left-menu").perfectScrollbar(), $("#ol i.fa").hide(), $("#ol i.glyphicon").show(), $(".container").on("click touchstart", function () {
                        $.sidr("close", "left-menu")
                    }), $(window).resize(function () {
                        $.sidr("close", "left-menu")
                    }), $(document).keyup(function (e) {
                        27 === (e.keyCode || e.which) && $.sidr("close", "left-menu")
                    })
                },
                onClose: function () {
                    $("#left-menu").perfectScrollbar("update"), $("#ol i.glyphicon").hide(), $("#ol i.fa").show(), $(".sidr li.active ul").slideUp("normal"), $(".sidr li").removeClass("active")
                }
            }), $("#ol-left").sidr({
                name: "left-menu-main",
                side: "left",
                speed: 300,
                source: "#left-nav",
                renaming: !1,
                onOpen: function () {
                    $("#left-menu-main").perfectScrollbar(), $("#ol-left i.fa, #ol-left span.more").hide(), $("#ol-left i.glyphicon, #ol-left span.exit").show().css("display", "block"), $("#ol-left").addClass("active"), $(".container").on("click touchstart", function () {
                        $.sidr("close", "left-menu-main")
                    }), $(window).resize(function () {
                        $.sidr("close", "left-menu-main")
                    }), $(document).keyup(function (e) {
                        27 === (e.keyCode || e.which) && $.sidr("close", "left-menu-main")
                    })
                },
                onClose: function () {
                    $("#left-menu-main").perfectScrollbar("update"), $("#ol-left i.glyphicon, #ol-left span.exit").hide(), $("#ol-left i.fa, #ol-left span.more").show().css("display", "block"), $(".sidr li.active ul").slideUp("normal"), $("#ol-left").removeClass("active"), $(".sidr li").removeClass("active")
                }
            }), $("#or").sidr({
                name: "nav-side",
                side: "left",
                speed: 300,
                source: ".navbar-collapse",
                renaming: !1,
                onOpen: function () {
                    $("#nav-side").perfectScrollbar(), $("#or span").hide(), $("#nav-side").css("max-height", $(window).height()), $("#or i.glyphicon").show(), $(".container").on("click touchstart", function () {
                        $.sidr("close", "nav-side")
                    }), $(window).resize(function () {
                        $.sidr("close", "nav-side")
                    }), $(document).keyup(function (e) {
                        27 === (e.keyCode || e.which) && $.sidr("close", "nav-side")
                    })
                },
                onClose: function () {
                    $("#nav-side").perfectScrollbar("update"), $("#or i.glyphicon").hide(), $("#or span").show(), $(".sidr li.active ul").slideUp("normal"), $(".sidr li").removeClass("active")
                }
            }), $(".sidr a").click(function () {
                var e = $(this),
                    t = e.closest("ul"),
                    o = t.find(".active"),
                    i = e.closest("li"),
                    n = i.hasClass("active"),
                    a = 0;
                t.find("ul").slideUp(function () {
                    ++a == t.find("ul").length && o.removeClass("active")
                }), n || (i.children("ul").slideDown(), i.addClass("active"), $(".sidr").perfectScrollbar("update"))
            })
        },
        initRoyalSlider: function () {
            try {
                $("#content-slider-1").royalSlider({
                    arrowsNav: !0,
                    usePreloader: !0,
                    autoPlay: {
                        enabled: !0,
                        delay: 3e3,
                        pauseOnHover: !0
                    },
                    arrowsNavAutoHide: !1,
                    fadeinLoadedSlide: !0,
                    controlNavigationSpacing: 0,
                    controlNavigation: "none",
                    blockLoop: !0,
                    keyboardNavEnabled: !0,
                    imageScaleMode: "fill",
                    imageAlignCenter: !0,
                    autoScaleSlider: !0,
                    slidesSpacing: 0,
                    loop: !0,
                    loopRewind: !0,
                    numImagesToPreload: 2,
                    autoScaleSlider: !0,
                    globalCaption: !0,
                    controlsInside: !1,
                    autoScaleSlider: !0,
                    autoScaleSliderWidth: 640,
                    autoScaleSliderHeight: 320,
                    imgWidth: 640,
                    imgHeight: 320
                })
            } catch (e) {
                console.log("#content-slider-1  -> not found")
            }
            try {
                $("#main-slider").royalSlider({
                    arrowsNav: !0,
                    loop: !0,
                    usePreloader: !0,
                    autoPlay: {
                        enabled: !0,
                        delay: 4e3,
                        pauseOnHover: !0
                    },
                    transitionSpeed: 600,
                    keyboardNavEnabled: !0,
                    controlsInside: !0,
                    imageScaleMode: "fill",
                    arrowsNavAutoHide: !0,
                    autoScaleSlider: !0,
                    autoScaleSliderWidth: 1920,
                    autoScaleSliderHeight: 600,
                    controlNavigation: "bullets",
                    thumbsFitInViewport: !1,
                    navigateByClick: !1,
                    numImagesToPreload: 1,
                    startSlideId: 0,
                    transitionType: "slide",
                    globalCaption: !1,
                    slidesSpacing: 0,
                    deeplinking: {
                        enabled: !0,
                        change: !1
                    },
                    imgWidth: 1920,
                    imgHeight: 600
                })
            } catch (e) {
                console.log("#main-slider  -> not found")
            }
            try {
                $("#featured").royalSlider({
                    arrowsNav: !1,
                    fadeinLoadedSlide: !0,
                    controlNavigationSpacing: 0,
                    controlNavigation: "thumbnails",
                    thumbs: {
                        autoCenter: !1,
                        fitInViewport: !0,
                        orientation: "vertical",
                        spacing: 0,
                        paddingBottom: 0
                    },
                    keyboardNavEnabled: !0,
                    imageScaleMode: "fill",
                    imageAlignCenter: !0,
                    slidesSpacing: 0,
                    loop: !1,
                    loopRewind: !0,
                    numImagesToPreload: 2,
                    video: {
                        autoHideArrows: !0,
                        autoHideControlNav: !1,
                        autoHideBlocks: !0
                    },
                    autoScaleSlider: !0,
                    autoScaleSliderWidth: 730,
                    autoScaleSliderHeight: 360,
                    imgWidth: 640,
                    imgHeight: 360
                })
            } catch (e) {
                console.log("#featured  -> not found")
            }
        },
    }
}();

function validate_form(e, t) {
    return e ? ($(t).hide(), 1) : ($(t).show(), 0)
}

function validateEmail(e) {
    return /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/.test(e)
}
$(function () {
    $("#s_form-signup").on("submit", function (e) {
        var t = document.getElementById("s_firstname").value,
            o = document.getElementById("s_lastname").value,
            i = document.getElementById("s_email").value,
            n = document.getElementById("s_ref").value;
        return validate_form(t, "#s_fn_error") ? validate_form(o, "#s_ln_error") ? validate_form(i, "#s_em_error") && validateEmail(i) ? (window.location.href = "/signup?ref=" + n + "&v=v4&first_name=" + t + "&last_name=" + o + "&email=" + i, !1) : ($("#s_email").focus(), !1) : ($("#s_lastname").focus(), !1) : ($("#s_firstname").focus(), !1)
    })
}), jQuery(document).ready(function () {
    App.init(), App.initRoyalSlider();
    try {
        Index.initIndex()
    } catch (e) {
        console.log("assets/js/index.js -> does not imported")
    }
});