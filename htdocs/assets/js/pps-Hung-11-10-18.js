    var viewDate ='';
    var sortby={'field':'trackname','order':'asc'};
    function format(date) {
        var d = date.getDate();
        var m = date.getMonth() + 1;
        var y = date.getFullYear();
        return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
    }
    function process_track_page(){
        var today = new Date();
        var todayDateString = format(today);
        
        var pps_date = document.getElementById("pps-track-date-body");
        pps_date.innerHTML='';
        if ( Object.keys ( trackList ) .length > 0) {
            for ( var i in trackList ) {
                var year = i.substring(0,4);
                var month = i.substring(4,6);
                var day = i.substring(6,8);
                var dateFormatted = year + "-" + month + "-" + day;              
                var div = document.createElement("div");
                div.setAttribute("id","pps-track-date-box-"+i);
                div.setAttribute("class","pps-track-date-box");
                div.setAttribute("viewdate",i);
                var child = document.createElement("div");
			  if(todayDateString == dateFormatted){
                    viewDate = year + month + day;
                	child.setAttribute("class","pps-track-date-text today");
			  }else{
				  child.setAttribute("class","pps-track-date-text");
			  }
                if(todayDateString == dateFormatted){
                    child.innerHTML = "Today";
                }else{
                    child.innerHTML=dateFormatted;
                }
                div.append ( child ) ;
                pps_date.append(div);
            }
        }
        process_template_track();
    }
    function process_template_track(){
        if ( viewDate == '' ) viewDate = today;
        var pps_body = document.getElementById("pps-track-menu-body");
        $(".pps-track-date-box").removeClass("date-box-selected");
        pps_body.innerHTML='';
        var tracks =  sort_array(trackList[viewDate],sortby);
        $("#pps-tracks").show();
        $("#pps-track-date-box-" + viewDate ) .addClass("date-box-selected");
        for ( var i in tracks ) {
            var track = tracks[i];
            var div = document.createElement("div");
            div.setAttribute("class","pps-track-menu-box");
            div.setAttribute("track_id",i);
            var child = document.createElement("div");
            child.setAttribute("class","pps-track-menu-text");
            child.innerHTML=track['trackname'];
            div.append(child);
            pps_body.append(div);

        }
        $(".pps-track-date-box").off("click");
        $(".pps-track-date-box").on("click",function(e){
            var __date = $(this).attr('viewDate');
            
            if ( $(this).hasClass("date-box-selected") && today !=__date){
                viewDate=today;
                process_template_track();
            } else {
                viewDate=__date;
                process_template_track();
            }
        });
        $("#pps-races").hide(200);
        $(".pps-track-menu-box").off("click");
        $(".pps-track-menu-box").on("click",function(e){
            $(this).toggleClass("track-box-selected");
        });
        $("#pp-sorter").off("click");
        $("#pp-sorter").on("click",function(e){
            sortby['order'] = ( sortby['order'] =='asc') ? 'desc' : 'asc';
            var text = ( sortby['order'] =='asc') ? " Sort A-Z " : " Sort Z-A ";
            $(this).html( text) ;
            process_template_track();
        });
        $(".pps-track-menu-box").off("click");
        $(".pps-track-menu-box").on("click",function(){
            $("#pps-tracks").hide(200);
            var trackid = $(this).attr("track_id");
            process_template_race(trackid,0);
        });
    }
    function process_template_race(trackid, raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
	    var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        $("#pps-race-back").off("click");
        $("#pps-race-back").on("click",function(){
            process_template_track();
        });
        $("#pps-race-track-header").html('<div><span class="race-title">' + track['trackname'] + '</span>' + "<br>" + '<span class="race-date">' + (race['race_date']) + '</span></div>');
        var races_body = document.getElementById("pps-race-content");
        races_body.innerHTML='';
        for ( var i in track['trackrace'] ) {
            var race = track['trackrace'][i];
            var div = create_race_div (i);
            div.append(create_race_header ( trackid, i ));
            div.append(create_race_data( trackid, i ));
            var arrow = document.createElement("div");
            arrow.setAttribute("class","race-arrow race-right");
            div.append(arrow);
            races_body.append(div);
        }
        
        $(".pps-race-box").off("click");
        $(".pps-race-box").on("click",function(){
            $("#pps-races").hide(200);
            var id = $(this).attr("raceId");
            process_template_horses(trackid,id);
        });
        $("#pps-races").show();
    
    }
    function  process_template_horses(trackid,raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
        var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        var raceNum = Number([raceid]) + 1;
        process_template_race_header(trackid,raceid);
        process_template_race_numb(trackid,raceid);
        process_template_claiming(trackid,raceid);
        process_template_bet_opt(trackid,raceid);
        var horses_body = document.getElementById("pps-horse-content");
        horses_body.innerHTML='';  
        for ( var i in race['horsedata']  ) {		
            var horse = race['horsedata'][i];
            var div = create_horse_div (i);
            div.append(create_horse_header ( horse ,horse['program']));
            div.append(create_horse_data( horse ));
            horses_body.append(div);
        }
        $("#pps-horse-back").off("click");
        $("#pps-horse-back").on("click",function(){
            $("#pps-horses").hide(200);
            process_template_race(trackid, raceid);
        });
        $(".pps-horse-box").off("click");
        $(".pps-horse-box").on("click",function(){

            var id = $(this).attr("horseId");
            horseid= id;
            process_template_horses(trackid,raceid);
        });
        $("#pps-races").hide(200);
        $("#pps-horses").show();
    }
    function process_template_race_header(trackid,raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
	    var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        var raceNum = Number([raceid]) + 1;
        var div = "#pps-horse-track-header";
        $(div).html('<div><span class="race-title">' + track['trackname'] + '</span><br><span class="race-date">' + race['race_date'] + '</span></div>');
    }
    function process_template_race_numb(trackid,raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
        var raceNum = Number([raceid]) + 1;
        var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        var div = ".pps-race-numb";
        var track = tracks[trackid];
        $("#carousel-body").html("");
        var carousel_html = process_template_race_carousel(trackid,raceid);
        document.getElementById("carousel-body").append(carousel_html);
        $('.carousel-left').on('click', function(){
            raceid=raceid-1;
            if(raceid<0) raceid=Object.keys(track['trackrace']).length-1;
            process_template_horses(trackid,raceid);
        });
        $('.carousel-right').on('click', function(){
            raceid=raceid+1;
            if(raceid>Object.keys(track['trackrace']).length-1) raceid=0;
            process_template_horses(trackid,raceid);
        });
        
    }
    function process_template_claiming(trackid,raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
        var div = ".pps-horse-claiming";
        var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        $(div).html('<div><span class="claiming-title">Claiming : </span>' + '<span class="claimamt-value">' + '$' + race['claimamt'] + '</span>' + ' </div><div> <span class="purse-title">Purse : </span>' +  '<span class="purse-value">' + '$' + race['purse']+ '</span></div>' );
    }
    function process_template_bet_opt(trackid,raceid){
        var tracks =  sort_array(trackList[viewDate],sortby);
        var div = ".pps-horse-bet-opt";
        var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        $(div).html(race['bet_opt']);
    }
    function process_template_race_carousel(trackid,raceid){
        var ol = document.createElement("ol");
	        var tracks =  sort_array(trackList[viewDate],sortby);  
           var track = tracks[trackid];
	var race = track['trackrace'][raceid];
        ol.classList.add("content");
        var raceNum = Number([raceid]);
        for ( var r in track['trackrace'] ) {
            var li = document.createElement("li");
            li.innerHTML = '<span class="race"> Race ' + track['trackrace'][r]['race'] + ' </span>' + '<br>' + '<span class="date">' + track['trackrace'][r]['post_time'] + '</span>';
            if(r == Number([raceid])){
                li.classList.toggle("active");
            }else{
                li.classList.add("inactive");
                li.classList.remove("active");
            }
            ol.appendChild(li);
        }
	return ol;
    }
    /*
    function create_carousel_div(){
        var div = document.createElement("div");
        div.setAttribute("id","pps-horse-track-header-carousel");
        div.setAttribute("class","pps-header-carousel");
        var left = document.createElement("span");
        left.setAttribute("class","carousel-left");
        div.append(left);
        var body = document.createElement("div");
        body.setAttribute("class","carousel-body");
        div.append(body);
        var right = document.createElement("span");
        right.setAttribute("class","carousel-right");
        div.append(right);      
        return div;
    }
    */
    function create_horse_div (i){
        var div = document.createElement("div");
        div.setAttribute("class","pps-horse-box");
        div.setAttribute("id","pps-horse-box-"+i);
        div.setAttribute("horseId",i);
        return div;
    }
    function create_race_div (i){
        var div = document.createElement("div");
        div.setAttribute("class","pps-race-box");
        div.setAttribute("id","pps-race-box-"+i);
        div.setAttribute("raceId",i);
        return div;
    }
    function create_race_header ( trackid,raceid ) {
        var tracks =  sort_array(trackList[viewDate],sortby);
        var track = tracks[trackid];
        var race = track['trackrace'][raceid];
        var raceNumb = Number(raceid) + 1;
        var race_header = document.createElement("div");
        race_header.setAttribute("class","pps-race-text-header" );
        var race_date = document.createElement("div");
        var race_text = document.createElement("div");
        var race_time = document.createElement("div");
        race_text.setAttribute("class","pps-race-text-header-text" );
        race_date.setAttribute("class","pps-race-text-header-date" );
        race_time.setAttribute("class","pps-race-text-header-time" );
        race_text.innerHTML="Race " + raceNumb;
        //race_date.innerHTML = race['race_date'];
        race_time.innerHTML = race['post_time'];
        race_header.append(race_text);
        race_header.append(race_date);
        race_header.append(race_time);
        return race_header;
    }
    function create_horse_header ( horse ,i) {
        var horse_header = document.createElement("div");
        horse_header.setAttribute("class","pps-horse-text-header" );
        var horse_date = document.createElement("div");
        var horse_text = document.createElement("div");
        horse_text.setAttribute("class","pps-horse-text-header-text color" + i);
        horse_date.setAttribute("class","pps-horse-text-header-name" );
        horse_text.innerHTML=i;
        horse_date.innerHTML = horse['horse_name'];
        horse_header.append(horse_text);
        //horse_header.append(horse_date);
        return horse_header;
    }
    function create_race_data( trackid,raceid ){
	      var tracks =  sort_array(trackList[viewDate],sortby);
		var track = tracks[trackid];
        var race = track['trackrace'][raceid];
       var race_data = document.createElement("div");
            race_data.setAttribute("class","pps-race-text-body");
            var header = document.createElement("span");
            header.setAttribute("class","pps-race-text-body-header");
            header.innerHTML='Claiming';
            var body = document.createElement("span");
            body.setAttribute("class","pps-race-text");
            var _body = document.createElement("span");
            _body.setAttribute("class","pps-race-text");
            _body.innerHTML = race['bet_opt'];
            body.innerHTML='Distance : ' + race['distance'] + race['dist_unit'] + ' - Purse : ' +  race['purse'] + ' - Claiming : ' + race['claimamt'];
            race_data.append(header);
            race_data.append(body);
            race_data.append(_body); 
            return race_data;
    }
    function create_horse_data( horse ){
       var horse_data = document.createElement("div");
            horse_data.setAttribute("class","pps-horse-text-body");
	    	 /*horse_data.innerHTML = (style="margin-bottom: 30px !important");*/
            
            var horse_name = document.createElement("span");
            horse_name.setAttribute("class","pps-horse-text-horse");
            horse_name.innerHTML = horse['horse_name'];
            horse_data.append(horse_name);
            /*
            var breeder = document.createElement("span");
            breeder.setAttribute("class","pps-horse-text");
            breeder.innerHTML = 'Breeder: ' + horse['breeder'];
            horse_data.append(breeder);
            */
            var owner = document.createElement("span");
            owner.setAttribute("class","pps-horse-text-owner");
            owner.innerHTML = horse['owner_name'];
            horse_data.append(owner);
            
            var jockey = document.createElement("span");
            jockey.setAttribute("class","pps-horse-text-jockey");
            jockey.innerHTML = horse['jockey']['jock_disp'];
            horse_data.append(jockey);
            
            var trainer = document.createElement("span");
            trainer.setAttribute("class","pps-horse-text-trainer");
            trainer.innerHTML = horse['trainer']['tran_disp'];
            horse_data.append(trainer);
            /*
            var sire = document.createElement("span");
            sire.setAttribute("class","pps-horse-text");
            sire.innerHTML = 'Sire: ' + horse['sire']['sirename'];
            horse_data.append(sire);
            
            var dam = document.createElement("span");
            dam.setAttribute("class","pps-horse-text");
            dam.innerHTML = 'Dam: ' + horse['dam']['damname'];
            horse_data.append(dam);
            
            var morn_odds = document.createElement("span");
            morn_odds.setAttribute("class","pps-horse-text");
            morn_odds.innerHTML = 'Morn Odds: ' + horse['morn_odds'];
            horse_data.append(morn_odds);
            */
            return horse_data;
    }
    function sort_array(arr,sortby){
        if( typeof( sortby ) != "undefined" )
        {
            arr = arr.sort( function( a, b )
            {
                if( a[sortby.field] < b[sortby.field] ) return( sortby.order == 'asc' ) ? -1 : 1;
                else if( a[sortby.field] > b[sortby.field] ) return( sortby.order == 'asc' ) ? 1 : -1;
                else return 0;
            });
        }
        return arr;
    }
    $(function(){
        process_track_page();
    });
