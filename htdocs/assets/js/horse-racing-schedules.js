function process_schedule_table(data,root){
    var self=this;
    this.data=data;
    this.root=root;
    this.process=function(){
        var html = '<div class="table-responsive">';
        html += '<table id="infoEntries" border="0" width="100%" cellpadding="0" cellspacing="0" class="data table table-condensed table-striped table-bordered">'
        html += '<tbody>'
       if ( self.data && Object.keys(self.data)>0 ) {
            html+= '<tr>';
            for ( var i in self.data ) {
                html+='<th><b>' + self.data[i].day  + '<br>' + self.data[i].date + '</b></th>';
            }
            html+= '</tr>';
       }
        html += '</tbody>';
        html += '</table>';
        html += '</div>';
        $("#"+self.root).html(html);
    }
}
