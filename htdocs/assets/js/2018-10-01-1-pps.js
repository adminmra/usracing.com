    var viewDate = "";
    var trackid = "";
    var raceid = "";
    var sortby = {
        field: "trackname",
        order: "asc"
    };
    
    function process_track_page() {
        var f = document.getElementById("pps-track-date-body");
        f.innerHTML = "";
        if (Object.keys(trackList).length > 0) {
            for (var b in trackList) {
                var c = b.substring(0, 4);
                var e = b.substring(4, 6);
                var a = b.substring(6, 8);
                var d = c + "-" + e + "-" + a;
                var h = document.createElement("div");
                h.setAttribute("id", "pps-track-date-box-" + b);
                h.setAttribute("class", "pps-track-date-box");
                h.setAttribute("viewdate", b);
                var g = document.createElement("div");
                g.setAttribute("class", "pps-track-date-text");
                g.innerHTML = d;
                h.append(g);
                f.append(h);
                viewDate = b;
            }
        }
        process_template_track();
    }
    
    function process_template_track() {
        if (viewDate == "") {
            viewDate = today;
        }
        var d = document.getElementById("pps-track-menu-body");
        $(".pps-track-date-box").removeClass("date-box-selected");
        d.innerHTML = "";
        var b = sort_array(trackList[viewDate], sortby);
        $("#pps-tracks").show();
        $("#pps-track-date-box-" + viewDate).addClass("date-box-selected");
        for (var c in b) {
            var a = b[c];
            var f = document.createElement("div");
            f.setAttribute("class", "pps-track-menu-box");
            f.setAttribute("track_id", c);
            var e = document.createElement("div");
            e.setAttribute("class", "pps-track-menu-text");
            e.innerHTML = a.trackname;
            f.append(e);
            d.append(f)
        }
        $(".pps-track-date-box").off("click");
        $(".pps-track-date-box").on("click", function(h) {
            var g = $(this).attr("viewDate");
            if ($(this).hasClass("date-box-selected") && today != g) {
                viewDate = today;
                process_template_track()
            } else {
                viewDate = g;
                process_template_track()
            }
        });
        $("#pps-races").hide(200);
        $(".pps-track-menu-box").off("click");
        $(".pps-track-menu-box").on("click", function(g) {
            $(this).toggleClass("track-box-selected")
        });
        $("#pp-sorter").off("click");
        $("#pp-sorter").on("click", function(g) {
            sortby.order = (sortby.order == "asc") ? "desc" : "asc";
            var h = (sortby.order == "asc") ? " Sort A-Z " : " Sort Z-A ";
            $(this).html(h);
            process_template_track()
        });
        $(".pps-track-menu-box").off("click");
        $(".pps-track-menu-box").on("click", function() {
            $("#pps-tracks").hide(200);
            var g = $(this).attr("track_id");
            trackid = g;
            process_template_race()
        })
    }
    
    function process_template_race() {
        var b = sort_array(trackList[viewDate], sortby);
        var a = b[trackid];
        $("#pps-race-track-header").html(a.trackname);
        var e = document.getElementById("pps-race-content");
        e.innerHTML = "";
        for (var d in a.trackrace) {
            var c = a.trackrace[d];
            var g = create_race_div(d);
            g.append(create_race_header(c, c.race));
            g.append(create_race_data(c));
            var f = document.createElement("div");
            f.setAttribute("class", "race-arrow race-right");
            g.append(f);
            e.append(g)
        }
        $("#pps-race-back").off("click");
        $("#pps-race-back").on("click", function() {
            process_template_track()
        });
        $(".pps-race-box").off("click");
        $(".pps-race-box").on("click", function() {
            $("#pps-races").hide(200);
            var h = $(this).attr("raceId");
            raceid = h;
            process_template_horses()
        });
        $("#pps-races").show()
    }
    
    function process_template_horses() {
        var d = sort_array(trackList[viewDate], sortby);
        var b = d[trackid];
        var f = b.trackrace[raceid];
        var a = Number([raceid]) + 1;
        process_template_race_header(track,race);
        $("#pps-horse-track-header").html("Track: " + b.trackname + " <br> Race #" + a + "<br> Race date: " + f.race_date + "<br> Post time: " + f.post_time + " <br> Purse : " + f.purse + "<br> Distance : " + f.distance + f.dist_unit + "<br> Bet_opt: " + f.bet_opt);
        var c = document.getElementById("pps-horse-content");
        c.innerHTML = "";
        for (var e in f.horsedata) {
            var g = f.horsedata[e];
            var h = create_horse_div(e);
            h.append(create_horse_header(g, g.program));
            h.append(create_horse_data(g));
            c.append(h)
        }
        $("#pps-horse-back").off("click");
        $("#pps-horse-back").on("click", function() {
            $("#pps-horses").hide(200);
            process_template_race()
        });
        $(".pps-horse-box").off("click");
        $(".pps-horse-box").on("click", function() {
            var i = $(this).attr("horseId");
            horseid = i;
            process_template_horses()
        });
        $("#pps-races").hide(200);
        $("#pps-horses").show()
    }
    
    
    function process_template_race_header(track,race){
        var div = "#pps-horse-track-header";
        $(div).html(track['trackname'] + "<br>" + race['race_date']);
        var carousel = create_carousel_div();
        $(div).append(carousel);
        var carousel_html = process_template_race_carousel(track,race);
        $(".carousel-body").append(carousel_html);
        // on click events
        /*
        $(.next).on('click', function(){
                raceid++;
                var carousel_html = process_template_race_carousel(track,race);
                $(".carousel-body").html(carousel_html);
        });
        */
    }
    function process_template_race_carousel(track,race){
        var ol = document.createElement("ol");
        for ( var r in track['trackrace'] ) {
            var li = document.createElement("li");
            li.innerHTML = 'Race ' + track['trackrace'][r]['race'] + '<br>' + track['trackrace'][r]['post_time'];
            ol.appendChild(li);
        }
        return ol;
    }
    function create_carousel_div(){
        var div = document.createElement("div");
        div.setAttribute("id","pps-horse-track-header-carousel");
        div.setAttribute("class","pps-header-carousel");
        var left = document.createElement("span");
        left.setAttribute("class","carousel-left");
        div.append(left);
        var body = document.createElement("div");
        body.setAttribute("class","carousel-body");
        div.append(body);
        var right = document.createElement("span");
        right.setAttribute("class","carousel-right");
        div.append(right);
        return div;
    }
    
    
    function create_horse_div(a) {
        var b = document.createElement("div");
        b.setAttribute("class", "pps-horse-box");
        b.setAttribute("id", "pps-horse-box-" + a);
        b.setAttribute("horseId", a);
        return b
    }
    
    function create_race_div(a) {
        var b = document.createElement("div");
        b.setAttribute("class", "pps-race-box");
        b.setAttribute("id", "pps-race-box-" + a);
        b.setAttribute("raceId", a);
        return b
    }
    
    function create_race_header(c, b) {
        var d = document.createElement("div");
        d.setAttribute("class", "pps-race-text-header");
        var a = document.createElement("div");
        var f = document.createElement("div");
        var e = document.createElement("div");
        f.setAttribute("class", "pps-race-text-header-text");
        a.setAttribute("class", "pps-race-text-header-date");
        e.setAttribute("class", "pps-race-text-header-time");
        f.innerHTML = "Race #" + b;
        a.innerHTML = c.race_date;
        e.innerHTML = c.post_time;
        d.append(f);
        d.append(a);
        d.append(e);
        return d;
    }
    
    function create_horse_header(e, b) {
        var d = document.createElement("div");
        d.setAttribute("class", "pps-horse-text-header");
        var c = document.createElement("div");
        var a = document.createElement("div");
        a.setAttribute("class", "pps-horse-text-header-text");
        c.setAttribute("class", "pps-horse-text-header-name");
        a.innerHTML = b;
        c.innerHTML = e.horse_name;
        d.append(a);
        d.append(c);
        return d
    }
    
    function create_race_data(b) {
        var c = document.createElement("div");
        c.setAttribute("class", "pps-race-text-body");
        var e = document.createElement("span");
        e.setAttribute("class", "pps-race-text-body-header");
        e.innerHTML = "Claiming";
        var a = document.createElement("span");
        a.setAttribute("class", "pps-race-text");
        var d = document.createElement("span");
        d.setAttribute("class", "pps-race-text");
        d.innerHTML = b.bet_opt;
        a.innerHTML = "Distance : " + b.distance + b.dist_unit + " - Purse : " + b.purse + " - Claiming : " + b.claimamt;
        c.append(e);
        c.append(a);
        c.append(d);
        return c
    }
    
    function create_horse_data(c) {
        var i = document.createElement("div");
        i.setAttribute("class", "pps-horse-text-body");
        var e = document.createElement("span");
        e.setAttribute("class", "pps-horse-text");
        e.innerHTML = "Horse: " + c.horse_name;
        i.append(e);
        var h = document.createElement("span");
        h.setAttribute("class", "pps-horse-text");
        h.innerHTML = "Breeder: " + c.breeder;
        i.append(h);
        var a = document.createElement("span");
        a.setAttribute("class", "pps-horse-text");
        a.innerHTML = "Owner: " + c.owner_name;
        i.append(a);
        var k = document.createElement("span");
        k.setAttribute("class", "pps-horse-text");
        k.innerHTML = "Jockey: " + c.jockey["jock_disp"];
        i.append(k);
        var j = document.createElement("span");
        j.setAttribute("class", "pps-horse-text");
        j.innerHTML = "Trainer: " + c.trainer["tran_disp"];
        i.append(j);
        var g = document.createElement("span");
        g.setAttribute("class", "pps-horse-text");
        g.innerHTML = "Sire: " + c.sire["sirename"];
        i.append(g);
        var b = document.createElement("span");
        b.setAttribute("class", "pps-horse-text");
        b.innerHTML = "Dam: " + c.dam["damname"];
        i.append(b);
        var d = document.createElement("span");
        d.setAttribute("class", "pps-horse-text");
        d.innerHTML = "Morn Odds: " + c.morn_odds;
        i.append(d);
        var f = document.createElement("span");
        f.setAttribute("class", "pps-horse-text");
        f.innerHTML = "Stats_data: " + c.stats_data;
        i.append(f);
        return i
    }
    
    function sort_array(a, b) {
        if (typeof(b) != "undefined") {
            a = a.sort(function(d, c) {
                if (d[b.field] < c[b.field]) {
                    return (b.order == "asc") ? -1 : 1
                } else {
                    if (d[b.field] > c[b.field]) {
                        return (b.order == "asc") ? 1 : -1
                    } else {
                        return 0
                    }
                }
            })
        }
        return a
    }
    $(function() {
        process_track_page()
    });
