
// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
requirejs.config({
    "baseUrl": "http://www.usracing.com/assets/captcha/js/vendor",
    "paths": {
      "app": "../app"
    },
    "shim": {
        "backbone": ["jquery", "underscore"],
        "bootstrap": ["jquery"],
        "plugins": ["jquery"]
    }
});

// Load the main app module to start the app
requirejs(["http://www.usracing.com/assets/captcha/js/app/main.js"]);