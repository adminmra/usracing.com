var xmlDoc;

function GetCasinoGames(Type) {
   if (window.XMLHttpRequest)
   {
  	xhttp=new XMLHttpRequest();
   }
   else // Internet Explorer 5/6
   {
  	xhttp=new ActiveXObject("Microsoft.XMLHTTP");
   }
   xhttp.open("GET","/modules/allhorseracing/casino/all.xml",false);
   xhttp.send("");
   xmlDoc=xhttp.responseXML;
   switch (Type) {
      case "MostPopular":
      	 return GetMostPopularGames();
     	 break;
      case "All":
	return GetAllGames();
	break;
      case "New":
	return GetNewGames();
	break;
      case "Table":
	return GetTableGames();
	break;
      case "Slots":
	return GetSlotGames();
	break;
      case "VideoPoker":
	return GetVideoPokerGames();
	break;
   }
}

function GetAllGames() {
	var games = xmlDoc.getElementsByTagName("game");
	return games;
}

function GetMostPopularGames() {
	var arr = new Array();
	var games = xmlDoc.getElementsByTagName("game");
	for (var i=0; i < games.length; i++) {
		for (var u=0; u < games[i].childNodes.length; u++) {
			
			if (games[i].childNodes[u].nodeName == "ispopular") {
				if (games[i].childNodes[u].firstChild.nodeValue == "true") {
					arr.push(games[i]);
					break;
				}
			}
		}
	}
	return arr;
}

function GetNewGames() {
	var arr = new Array();
	var games = xmlDoc.getElementsByTagName("game");
	for (var i=0; i < games.length; i++) {
		for (var u=0; u < games[i].childNodes.length; u++) {
			
			if (games[i].childNodes[u].nodeName == "isnew") {
				if (games[i].childNodes[u].firstChild.nodeValue == "true") {
					arr.push(games[i]);
					break;
				}
			}
		}
	}
	return arr;
}

function GetTableGames() {
	var arr = new Array();
	var games = xmlDoc.getElementsByTagName("game");
	for (var i=0; i < games.length; i++) {
		for (var u=0; u < games[i].childNodes.length; u++) {
			
			if (games[i].childNodes[u].nodeName == "category") {
				if (games[i].childNodes[u].firstChild.nodeValue == "Table") {
					arr.push(games[i]);
					break;
				}
			}
		}
	}
	return arr;
}

function GetSlotGames() {
	var arr = new Array();
	var games = xmlDoc.getElementsByTagName("game");
	for (var i=0; i < games.length; i++) {
		for (var u=0; u < games[i].childNodes.length; u++) {
			
			if (games[i].childNodes[u].nodeName == "category") {
				if (games[i].childNodes[u].firstChild.nodeValue == "Slots") {
					arr.push(games[i]);
					break;
				}
			}
		}
	}
	return arr;
}

function GetVideoPokerGames() {
	var arr = new Array();
	var games = xmlDoc.getElementsByTagName("game");
	for (var i=0; i < games.length; i++) {
		for (var u=0; u < games[i].childNodes.length; u++) {
			
			if (games[i].childNodes[u].nodeName == "category") {
				if (games[i].childNodes[u].firstChild.nodeValue == "VideoPoker") {
					arr.push(games[i]);
					break;
				}
			}
		}
	}
	return arr;
}
