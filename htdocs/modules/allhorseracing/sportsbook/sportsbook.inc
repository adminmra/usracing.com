<?php
     
    //get lines
    $FeedContent = "/var/www/vhosts/gohorsebetting.com/httpdocs/commonlines/bettinglinesAHR.xml";
        
    $xml = Simplexml_load_file($FeedContent);
    $LineTypes = $xml->xpath("LineTypes/@Name");
    
    //get number of columns for current Line Type
    $numColumns = sizeof($CurLineType[0]->Column);
    
    $authenticated = "false";

    if (user_is_logged_in() == 1) {
        $authenticated = "true";
    }
function GetDefaultType()
    {
      if (isset($_GET["line"]))
      {
        return $_GET["line"];
      }
      else
      {
        return '';
      }
    }

    
	function SetSportLines($LinesArray)
	{ 
		//calc number of rows based on 6 columns per row
		$numRows = floor(sizeof($LinesArray) / 5);
		$numRows = ((sizeof($LinesArray) % 5) > 0) ? $numRows+1 : $numRows;
		
		//build table
		echo "<table class=\"header_lines_table\">";
		
		//init start and end positions for first row
		$start = 0;
		$end = 5;
		
		for ($row = 0 ; $row < $numRows ; $row++)
		{
			echo "<tr>";
			
			//build columns
			for ($col = $start ; $col < $end; $col++)
			{
				if ($col < sizeof($LinesArray))
				{
					echo ("<td class=\"header_bg\">");
					echo ("<a class=\"header_link\" href=\"\">".$LinesArray[$col]."</a>");
				}
				else 
				{
					echo("<td>");
				}
				echo ("</td>");
			}
			echo "</tr>";
			
			//re-calc start and end positions
			$start = $end;
			$end = $start + 5 ;
		}
		echo "</table>";
	}
	
	function GetLineItems()
	{
		echo ("<iframe id=\"ODDS\" name=\"ODDS\" scrolling=\"auto\" frameborder=\"0\" width=\"100%\" height=\"1600\">");
		echo ("</iframe>");
	}
?>

<style>
.not-front #content-box
{
padding: 0;
}
</style>
    <link href="/modules/allhorseracing/sportsbook/sportsbook.css" type="text/css" rel="Stylesheet" media="screen" />
    <script type="text/javascript">
      $(document).ready(function(){
          var defHeader = '<?php print GetDefaultType();?>';
          var firstHeader = $(".header_link:eq(0)").text();
          var authenticated = <?php print $authenticated; ?>;
          
          if (defHeader != '')
          {
            $("#ODDS").attr("src","/modules/allhorseracing/sportsbook/linesdetail.php?type=" + defHeader + "&auth=" + authenticated);
          }
          else
          {
            $("#ODDS").attr("src","/modules/allhorseracing/sportsbook/linesdetail.php?type=" + firstHeader + "&auth=" + authenticated);
          }

          $(".header_lines_table tr td a").bind("click",function(){
              $("#ODDS").attr("src","/modules/allhorseracing/sportsbook/linesdetail.php?type=" + $(this).text() + "&auth=" + authenticated);
              return false;
          });

            
      });

        function SetSelectedHeader(cellText)
        {
            $(".header_lines_table td").each(function(){
              if($(this).text() == cellText)
              {
                $(this).removeClass("header_bg");
                $(this).addClass("selected_header");
              }
            });
        }
        
        function ResetHeader()
        {
            $(".header_lines_table td").each(function(){
            $(this).removeClass("selected_header");
            $(this).addClass("header_bg");
        });
      }
    </script>

	<div>
		<?php SetSportLines($LineTypes); ?>
	</div>
	<div>
		<?php GetLineItems(); ?>
	</div>
