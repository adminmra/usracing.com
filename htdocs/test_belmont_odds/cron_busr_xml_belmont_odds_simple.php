<?php
$id_league ="918";
if(isset($_REQUEST['IdLeague']))
{
    $id_league = $_REQUEST['IdLeague'];
}



$banner_ab_true = "on";
$banner_ab_false = "off";


$string = file_get_contents("xml_odds.xml"); 
//$string = file_get_contents("http://www.betusracing.ag/odds.xml"); 
$xml = new SimpleXMLElement($string);

/* Search for <a><b><c> */

?>
<table border="1"> 
    <caption><?php echo $id_league?></caption>
    <tr>   
        <th>Contender</th>    
        <th>American Odds</th>            
        <th>Fractional Odds</th>    
    </tr>
    <?php
    $result = $xml->xpath('/xml/league[@IdLeague="'.$id_league.'"]/game');

   /* echo "<pre>";
    print_r($result);
    echo "<pre>";*/

    while(list( ,$game_node) = each($result)) 
    {
        $game_attributes = $game_node->attributes();     
        ?>
        <tr>
            <td><?php echo $game_attributes['htm']?></td>
            <td>
                <?php
                list( ,$line_node) = each($game_node->children());

                $line_attributes = $line_node->attributes();
                
                echo $line_attributes['oddsh'];
                
                ?>
            </td>
            <td>
                <?php 
                $oodsh = intval($line_attributes['oddsh']);

                if(intval($oodsh)>0)
                {
                    echo float2rat($oodsh/100);     
                }
                else if($oodsh<0)
                {
                    echo float2rat(100/(-$oodsh));
                }
                else
                {
                    echo $line_attributes['oddsh'];   
                }
                ?>
            </td>
        </tr>
        <?php
    }
    ?>    
</table>

<?php

function float2rat($n, $tolerance = 1.e-6) {
    $h1=1; $h2=0;
    $k1=0; $k2=1;
    $b = 1/$n;
    do {
        $b = 1/$b;
        $a = floor($b);
        $aux = $h1; $h1 = $a*$h1+$h2; $h2 = $aux;
        $aux = $k1; $k1 = $a*$k1+$k2; $k2 = $aux;
        $b = $b-$a;
    } while (abs($n-$h1/$k1) > $n*$tolerance);

    return "$h1/$k1";
}