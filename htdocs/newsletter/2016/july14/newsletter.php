<?php  //VARIABLES
	
$tag="July_14";
$message_preview="<Greeting/> Take a simple test to find out how good your handicapping is!";	

$hero_image="https://www.usracing.com/newsletter/2016/july14/hero.jpg";
$hero_image_alt="Computer and Cash";
$hero_title="Take the Test! How Good Is Your Handicapping?";
$hero_content="<Greeting/> <p><div align=left><p>Most serious racing fans have at least heard of John L. Kelly.</p><br> 
<p>While working at Bell Labs in the late '50s, Kelly, a physicist, developed the \"Kelly Criterion,\", a bet optimization formula that was popularized by M.I.T. mathematician -- and famous blackjack player -- Edward O. Thorp (author of \"Beat the Dealer\"). The Kelly Criterion was later used by Thorp, Warren Buffet, Jim Simons and others to great effect on Wall Street.</p><br> 

<p>As a tool for horse bettors, Kelly’s famous proposition is somewhat controversial. In his initial paper \"A New Interpretation of Information Rate,\" Kelly describes a scenario in which a gambler is betting on baseball games -- receiving some of the scores in advance of the sports books (obviously, this was before the Internet, when global communication was more challenging and what one ate for dinner was still considered boring and uninteresting).</p><br> 

<p>But the key distinction is that, in Kelly’s scenario, the odds were set (even). This, of course, is rarely the case with horse racing odds, which can fluctuate wildly in a pari-mutuel system. Worse, as a group, horseplayers are notoriously poor record-keepers and the Kelly Criterion needs -- nay, <i>demands</i> -- accurate records. (This is one of the reasons that many horseplayers who advocate using the Criterion suggest betting a fraction of its suggested outlay on each wager.)</p><br> 

<p>However, there is another application of the Kelly Criterion that I believe many players overlook. Simply put, it is a great tool for assessing the effectiveness of one’s handicapping methods.</p><br> 

<p>Let me explain.</p><br> 

<p>I have often heard that the only thing that matters to profit-seeking gamblers is one’s return on investment (ROI), yet that is not true if one is looking to make a consistent profit at the windows.</p><br> 

<p>Consider: If I offered you 2,000,000-1 odds on a 1,000,000-1 proposition, how tempted would you be to make me up on the deal? What if I told you that you had to bet at least $100,000 (I mean, aside from the fact that I don’t have $2 trillion lying around to pay you off should you win)?</p><br> 

<p>As most of readers undoubtedly realize, such a pact, despite the fact that it offers a <u>100 percent ROI</u>, does not qualify as a good investment, because the chance of success is so low -- and this is where Kelly comes in.</p><br> 

<p>According to the Kelly Criterion, the <i>maximum</i> percentage that one should stake on any single wagering event cannot exceed -- or even <i>equal</i> -- one’s expected success rate. This means that if a bettor has a 10 percent strike rate, he should never stake more than 10 percent of his capital -- even if he is getting odds of gazillion-to-one (my kids assure me that \"gazillion\" is a word, at least as it relates to their desired allowances).</p><br> 

<p>Hence, the suggested Kelly bet can be thought of as one’s edge, or advantage, over the game. And, as such, it can be used to assess the merits of one’s handicapping and/or any methods or angles one might employ.</p><br> 

<p>Because of the variability inherent in the game, I insist on methods that offer me a Kelly advantage of five percent or greater -- and I much prefer techniques with at least a 10 percent Kelly edge (based on extensive database testing). Of course, this generally entails methods with higher winning percentages (40 percent winners is my usual baseline) and, typically, lower odds.</p><br> 

<p>This is in direct contrast to what one hears from many self-appointed betting gurus, who trumpet the \"life-changing\" score as the sole means of conquering the game. It also highlights the difference between deriving a consistent income from the game and contest play, which insist upon better-than-average rates of return.</p><br> 

<p>Find your Kelly Advantage, by clicking the button below.</div></p>";

$button_link="https://www.usracing.com/horse-betting/kelly-advantage-calculator";
$button_content="Find My Kelly Advantage &rarr;";

//Article 1*************************************************

$blog1_title="American Patriot Seeks Upset in Kent Stakes";
$blog1_image_alt="Delaware Park";
$blog1_content="<div align=left><p>Saturday’s Delaware Park program features four stakes races, including the Grade I Delaware Handicap, which drew <b>Paid Up Subscriber</b> and <b>I’m a Chatterbox</b>, and the Grade III Kent Stakes, which lured <b>American Patriot</b>, a Todd Pletcher trainee that I think stands an excellent shot of visiting the winner’s circle.</p><br>

American Patriot was last seen winning an optional claiming event at Belmont Park and is a generous 6-1 on the morning line, despite having the best overall late speed rations in the field. <b>Mighty Mo</b> and <b>Converge</b> look like the biggest threats. The former due to the expected slow pace and the latter as a result of his previous good form against this level (he won the Grade III Palm Beach in just his third lifetime start).</div></p>";

$blog1_link="https://www.usracing.com/odds";
$blog1_link_text="Bet Now &rarr;";
$blog1_image="https://www.usracing.com/newsletter/2016/july14/photo-1.jpg"; 

//Article 2*************************************************

$blog2_title="FREE Race Report Card for July 15-17";	
$blog2_image_alt="Report Cards";
$blog2_content="<div align=left><p>FREE Race Report Cards are available for several tracks and programs this weekend:</p><br>

<p><ul>
  <li>Belmont Park (7/15)</li>
  <li>Del Mar (7/15-7/16)</li>
  <li>Delaware Park (7/16)</li>
  <li>Emerald Downs (7/17)  </li>
</ul></p><br>

<p>To get your <u>FREE</u> Race Report Cards for this weekend, click on the button below.</div></p>";

$blog2_link="https://www.usracing.com/news/analysis/free-race-report-cards-for-july-15-17-2016";
$blog2_link_text="Get Your FREE Race Report Cards &rarr;";
$blog2_image="https://www.usracing.com/newsletter/2016/july14/photo-2.jpg";

//Calendar*************************************************

$stakeslist_image="https://www.usracing.com/newsletter/2016/july14/stakeslist.png";
$stakes_image="https://www.usracing.com/newsletter/2015/dec31/calendar.png";
$stakes_image_alt="This Weekend's Stakes Races Calendar";
$stakes_link="http://www.usracing.com/graded-stakes-races?nl=";
$stakes_title="Stakes Races This Weekend";
$stakes_content=""; // Can't Use

?>	


<!DOCTYPE html>
<html lang="en">
<head>
<title></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
          text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
          margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        td[class="mobile-hide"]{
          display:none;}

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          max-width: 100% !important;
          width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
            margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
            width:80% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

    }
    
    table td, table th { wordwrap:break-word;  }
table > tr > th { border-bottom: none; }
table td.right, table th.right { text-align:right; }
table td.center, table th.center, table td.num { text-align:center; }
.table.table-bordered > tr th:first-child, .table.table-bordered > tr td:first-child { border-left: none; }
.table-condensed thead > tr > th, .table-condensed tbody > tr > th, .table-condensed tfoot > tr > th, .table-condensed thead > tr > td, .table-condensed tbody > tr > td, .table-condensed tfoot > tr > td{ padding:5px 10px; font-size:12px;}
.table-bordered > tbody > tr th { background:#f4f4f4 !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;font-size:12px;}
.table-bordered > tbody > tr th:first-child { border-left:0; }
.table caption { background:#1571BA; padding:10px; font-size:20px; font-weight: 500; text-align:left; border:0; text-align:center; color:#fff;}
.dateUpdated {  font-size:12px; }
table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table tbody>tr>td,.table tbody>tr>th,.table tfoot>tr>td,.table tfoot>tr>th,.table thead>tr>td,.table thead>tr>th{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table caption+thead tr:first-child td,.table caption+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table thead:first-child tr:first-child td,.table thead:first-child tr:first-child th{border-top:0}.table tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed tbody>tr>td,.table-condensed tbody>tr>th,.table-condensed tfoot>tr>td,.table-condensed tfoot>tr>th,.table-condensed thead>tr>td,.table-condensed thead>tr>th{padding:5px}.table-bordered,.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*=col-]{float:none;display:table-column}table td[class*=col-],table th[class*=col-]{float:none;display:table-cell}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8;border-color:#d6e9c6}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6;border-color:#c9e2b3}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede;border-color:#eed3d7}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc;border-color:#e6c1c7}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3;border-color:#fbeed5}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc;border-color:#f8e5be}@media (max-width:768px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;overflow-x:scroll;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0;background-color:#fff}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>thead>tr:last-child>td,.table-responsive>.table-bordered>thead>tr:last-child>th{border-bottom:0}}
</style>
</head>
<body style="margin: 0; padding: 0;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#003158">
            <!-- HIDDEN PREHEADER TEXT -->
            <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
             
             <?php echo($message_preview);?>
             
   </div>
            <div align="center" style="padding: 0px 15px 0px 15px;">
                <table border="0" cellpadding="0" cellspacing="0" width="500" class="wrapper">
                    <!-- LOGO/PREHEADER TEXT -->
                    <tr>
                        <td style="padding: 20px 0px 30px 0px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#003158" width="100" align="left">
                                        <a href="https://www.usracing.com/?nl_logo=<?php echo $tag;?>" target="_blank">
                                           
                                         
                                            <img alt="US Racing" src="https://www.usracing.com/img/usracing.png" width="200" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                                        </a>
                                    </td>
                                    <td bgcolor="#003158" width="400" align="right" class="mobile-hide">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right" style="padding: 0 0 5px 0; font-size: 14px; font-family: Arial, sans-serif; color: #666666; text-decoration: none;"><span style="color: #FFFFFF; text-decoration: none;">1-844-US-RACING</span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
	                    
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              	<td class="padding-copy">
                                    <a href="<?php echo $button_link."?nl-hero=".$tag;?>" target="_blank">
 <img 
 
 src="<?php echo $hero_image;?>" width="500" height="400" border="0" 
 
 alt="<?php echo $hero_image_alt;?>" 
 
 style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">

 
 
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy"><?php echo $hero_title;?></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">	<OpenTracking/>
	<p>     <Greeting/></p><?php echo $hero_content;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <!-- BULLETPROOF BUTTON -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                    <tr>
                                                        <td align="center"><a href="<?php echo $button_link."?nl-btn=".$tag;?>" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; background-color: #c71f24; border-top: 15px solid #c71f24; border-bottom: 15px solid #c71f24; border-left: 35px solid #c71f24; border-right: 35px solid #c71f24; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button"><?php echo $button_content;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- TWO COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#F5F7FA" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <!-- TITLE SECTION AND COPY -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;" class="padding-copy">US Racing Expert Opinion </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">By Chief Editor and Handicapper<br> - Derek Simon</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- TWO COLUMNS -->
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top" style="padding: 0;" class="mobile-wrapper">
                                    <!-- LEFT COLUMN -->
                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" align="left" class="responsive-table">
                                        <tr>
                                            <td style="padding: 20px 0 40px 0;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="center" bgcolor="#F5F7FA" valign="middle"><a href="<?php echo $blog1_link."?nl=".$tag;?>" target="_blank"><img src="<?php echo $blog1_image;?>" width="240" height="180" style="display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 180px;" alt="<?php echo $blog1_image_alt;?>" border="0" class="img-max"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><?php echo $blog1_title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><span class="appleBody"><?php echo $blog1_content; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><a href="<?php echo $blog1_link."?nl=".$tag;?>" style="color: #256F9C; text-decoration: none;"><?php echo $blog1_link_text;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- RIGHT COLUMN -->
                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" align="right" class="responsive-table">
                                        <tr>
                                            <td style="padding: 20px 0 40px 0;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="center" bgcolor="#F5F7FA" valign="middle"><a href="<?php echo $blog2_link."?nl=".$tag;?>" target="_blank"><img src="<?php echo $blog2_image;?>" width="240" height="180" style="display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 180px;" alt="<?php echo $blog2_image_alt;?>" border="0" class="img-max"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><?php echo $blog2_title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><span class="appleBody"><?php echo $blog2_content; ?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><a href="<?php echo $blog2_link."?nl=".$tag;?>" style="color: #256F9C; text-decoration: none;"><?php echo $blog2_link_text;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
 
<!-- Stakes Schedule Section-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#E6E9ED" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" style="padding:0 0 20px 0;" class="responsive-table">
                <!-- TITLE -->
                <tr>
                    <td align="center" style="padding: 0 0 10px 0; font-size: 25px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #333333;" class="padding-copy" colspan="2"><?php echo $stakes_title;?></td>
                </tr>
                <tr>
                    <td valign="top" style="padding: 40px 0 0 0;" class="mobile-hide"><a href="<?php echo $stakes_link?><?php echo $tag;?>" target="_blank"><img src="<?php echo $stakes_image;?>" alt="<?php echo $stakes_image_alt;?>" width="105" height="105" border="0" style="display: block; font-family: Arial; color: #666666; font-size: 14px; width: 105px; height: 105px;"></a></td>
                    <td style="padding: 40px 0 0 0;" class="no-padding">
                        <!-- ARTICLE -->
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td align="left" style="padding: 0 0 5px 25px; font-size: 22px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #333333;" class="padding-copy">
	                                
	                                
	                                
                         
	                                
	                                
<!--Stakes------------------------------------------------------------------------------------------>
	

	        <a href="<?php echo $stakes_link?><?php echo $tag;?>" target="_blank">
 <img 
 
 src="<?php echo $stakeslist_image;?>"  border="0" 
 
 alt="Horse Racing Schedule" 
 
 style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">

 
 
                                    </a>

	
			
<!--END Stakes---------------------------------------------------------------------------------------->


</td>
                            </tr>
                            <tr>
                                <td style="padding:0 0 45px 25px;" align="left" class="padding">
                                    <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center">
                                                <!-- BULLETPROOF BUTTON -->
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                                    <tr>
                                                        <td align="center" style="padding: 0;" class="padding-copy">
                                                            <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                                <tr>
                                                                    <td align="center"><a href="<?php echo $stakes_link?><?php echo $tag;?>" target="_blank" style="font-size: 15px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #256F9C; border-top: 10px solid #256F9C; border-bottom: 10px solid #256F9C; border-left: 20px solid #256F9C; border-right: 20px solid #256F9C; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">See More &rarr;</a></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                            </table>
        </td>
    </tr>
</table>

<!-- FOOTER -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            
                            <tr>
                                <td align="center" style="padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">Be sure to join in on the conversation on <a href="https://www.facebook.com/betusracing/"><img title="Facebook Like Button" alt="Facebook" src="https://www.usracing.com/newsletter/2016/mar10/facebook.png" width="80" height="30" style="vertical-align:middle;"  /></a> and

 <a href="https://twitter.com/betusracing"><img title="Twitter  Button" alt="Twitter" src="https://www.usracing.com/newsletter/2016/mar10/twitter.png" width="80" height="30" style="vertical-align:middle;" /></a>.  <p>Good luck at the races

- Derek</td>  </tr> <?php /*<tr>
<td style="padding: 10px 0 20px 0; font-size: 10px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy" align="center">You have been emailed this letter because you have an account at USRacing.com<br /> To unsubscribe from these emails, click <span style="text-decoration: underline;"><a href="%BASIC:UNSUBLINK%" class="original-only" style="color: #666666; text-decoration: underline;">Unsubscribe</a></span> or reply with UNSUBSCRIBE in the subject or body.</td>
</tr>*/ ?>
</tbody>
</table>

<!-- ANALYTICS-->
<style data-ignore-inlining>@media print{ #_t { background-image: url('https://x1ntfixz.emltrk.com/x1ntfixz?p&d=$Subscriber.EMAIL');}} div.OutlookMessageHeader {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} table.moz-email-headers-table {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} blockquote #_t {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} #MailContainerBody #_t {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')}</style><div id="_t"></div>
<img src="https://x1ntfixz.emltrk.com/x1ntfixz?d=$Subscriber.EMAIL" width="1" height="1" border="0" />
</body>
</html>
