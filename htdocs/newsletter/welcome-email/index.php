<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style>


         @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900'); /*Calling our web font*/
        /* Some resets and issue fixes */
        #outlook a { padding:0; }
        body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }     
        .ReadMsgBody { width: 100%; }
        .ExternalClass {width:100%;} 
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;} 
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}    
        
        /* End reset */
        
        
        /* These are our tablet/medium screen media queries */
        @media screen and (max-width: 630px){
                
                
            /* Display block allows us to stack elements */                      
            *[class="mobile-column"] {display: block;} 
            
            /* Some more stacking elements */
            *[class="mob-column"] {float: none !important;width: 100% !important;}     
                 
            /* Hide stuff */
            *[class="hide"] {display:none !important;}          
            
            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {width:100% !important; height:auto !important;}                    
                
            /* For the 2x2 stack */            
            *[class="condensed"] {padding-bottom:40px !important; display: block;}
            
            /* Centers content on mobile */
            *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}            
            
            /* 100percent width section with 20px padding */
            *[class="100pad"] {width:100% !important; padding:20px;} 
            
            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;} 
            
            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;} 
            
        
        }
            
        
    </style>
    
   
</head>


<div style="background:#e9e8e8;">

<body style="padding:0; margin:0" bgcolor="#e9e8e8">

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
    <tr>
        <td align="center" valign="top">
        
             <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#1571ba" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;" width="640"><img src="https://www.usracing.com/newsletter/welcome-email/images/betusr_white.png" width="220"><br />
                        
                    </td>
                </tr>
            </table>
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td background="https://www.usracing.com/newsletter/welcome-email/images/hero.jpg" bgcolor="#003158" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            <v:fill type="tile" src="https://www.usracing.com/newsletter/welcome-email/images/hero.jpg" color="#3b464e" />
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="color:#FFFFFF; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:34px; font-weight:900;">WELCOME TO BET USRACING</span>
                                                                <br />
                                                                <span style="font-size:16px;">You are seconds away from placing your first bet!
</span>
                                                                <br />
                                                                
                                                                <a href="##" style="color:#FFFFFF; text-decoration:none;">
                                                                <table border="0" cellspacing="0" cellpadding="10" >
                                                                    <tr>
                                                                        <td align="center" style="color:#FFFFFF; font-size:16px;"><a href="https://www.betusracing.ag/login?ref=welcome_letter_v2" style="background-color:#ff0000;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:12px;
    padding:9px 24px;
    text-decoration:none;
    text-shadow:0px 1px 0px #854629;">DEPOSIT NOW TO START PLAYING!</a></td>
                                                                    </tr>
                                                                </table>
                                                                </a>
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f; line-height: 23px;"><font face="'Roboto', Arial, sans-serif">Hi %%First Name%%,<br /><span style="font-size:16px;"> Welcome to Bet usracing, we're here to offer you the best  experience around.<br />  Here is just some of what you will get:</span></font></td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#1571ba" class="100p" height="1">
                <tr>
                    <td width="20" bgcolor="#FFFFFF"></td>
                    <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
                    <td width="20" bgcolor="#ffffff"></td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p" bgcolor="#FFFFFF">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" class="100padtopbottom" width="600">
                            <tr>
                                <td align="left" class="condensed" valign="top">
                                    <table align="left" border="0" cellpadding="10" cellspacing="0" class="mob-column" width="195">
                                        <tr>
                                            <td valign="top" align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top" align="center" class="100padleftright">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="135" align="center"><img src="https://www.usracing.com/newsletter/welcome-email/images/checkmark.png"  width="65" style="display: block;" alt="check mark" /></td>
                                                                    
    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="100padleftright" align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td valign="top" width="135" align="center" style="font-size:16px; font-weight:bold; color:#1571ba;"><font face="'Roboto', Arial, sans-serif">BEST REBATES
GUARANTEED <br /><br />  <span style="color:#8f8f8f; line-height:23px; font-weight:400;" >Get up to 8% daily horse racing rebates. Paid daily into your account.</span>
</font></td>
                         
                                                                   
                                                                </tr>
                                                                
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <table align="left" border="0" cellpadding="10" cellspacing="0" class="mob-column" width="190">
                                        <tr>
                                            <td valign="top" align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top" align="center" class="100padleftright">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="135" align="center">                                         <img src="https://www.usracing.com/newsletter/welcome-email/images/pin.png" width="38" style="display:block;" alt="location pin">
</td>
                                                                   
    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="100padleftright" align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td valign="top" width="135" align="center" style="font-size:16px; font-weight:bold; color:#1571ba;"><font face="'Roboto', Arial, sans-serif">OVER 200 RACETRACKS<br /><br />  <span style="color:#8f8f8f; line-height:23px; font-weight:400;"  >Bet on over 200 racetracks from USA, Canada and across the world.</span></font></td>
                                                                  
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <table align="left" border="0" cellpadding="10" cellspacing="0" class="mob-column" width="190">
                                        <tr>
                                            <td valign="top" align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td valign="top" align="center" class="100padleftright">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td width="135" align="center"><img src="https://www.usracing.com/newsletter/welcome-email/images/mobilephone.png" width="31" style="display:block;" alt="mobile phone" /></td>
                                                                    
    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" class="100padleftright" align="center">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td valign="top" width="135" align="center" style="font-size:16px; font-weight:bold; color:#1571ba;"><font face="'Roboto', Arial, sans-serif">BET ON THE GO, WHEREVER YOU GO<br /><br />  <span style="color:#8f8f8f; line-height:23px; font-weight:400; " >Take all the races with you! Available on phone or tablet at the track, at home, everywhere.</span></font></td>
                                                                    
                                                                    
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    
                                </td>
                                
                                
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#1571ba" class="100p" height="1">
                <tr>
                    <td width="20" bgcolor="#FFFFFF"></td>
                    <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
                    <td width="20" bgcolor="#ffffff"></td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; "><font face="'Roboto', Arial, sans-serif"><span style="color:#8f8f8f; font-size:16px;  font-weight:700;">Deposit Options</span><br />
                       <img class="icons" src="https://www.usracing.com/newsletter/welcome-email/images/echeck.png" width="95" style="padding:20px"> 
                        <br />
                        <span style="font-size:16px; line-height: 25px; font-weight:400; color:#8f8f8f;">The best deposit option is our <strong>ACH Electronic Check method.</strong> Take a personal check, fill it out, take a photo of it along with your picture ID and email it to us. 
The funds will be posted immediately. <br><br><strong>Call 1-844-BET-HORSES (1-844-238-4677)</strong> right now for more details.<br><br>We also accept the following forms of payment:<img src="https://www.usracing.com/newsletter/welcome-email/images/depositoptions.png" width="280" style="padding:20px;">
</span><br></font>
                    </td>
                </tr>
                <tr> <td align="center" style="color:#FFFFFF; font-size:16px;"><a href="https://www.betusracing.ag/login?ref=welcome_letter_v2" style="background-color:#ff0000;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:12px;
    padding:9px 24px;
    text-decoration:none;
    text-shadow:0px 1px 0px #854629;">DEPOSIT NOW !</a></td></tr>
            </table>
            
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#1571ba" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#ffffff;"><font face="'Roboto', Arial, sans-serif"><span style="font-size:16px; font-weight:500;">Your Account Info</span><br />
                        <br />
                        <span style="font-size:16px;">%%First Name%%, below is your account information for your records:</span><br /><br />
                        <span style="font-weight:500; font-size:16px;">Your Account ID is: %%Account%%</span><br /><br />
                        <span style="font-size:16px;">You can sign in with either your Email Address or Account ID and password.
Please save this email for future reference.</span></font>
                    </td>
                </tr>
            </table>
            
               <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;"><font face="'Roboto', Arial, sans-serif"><img src="https://www.usracing.com/newsletter/welcome-email/images/derekpic.png" height="60px"><br />
                        <br />
                        
                        <span style="font-size:16px;">“Bet usracing provides more gaming choices than any other horse racing site, and the quick payouts are great. I know you'll enjoy playing at Bet usracing, especially that rebate. Good Luck at the races.”<br /><br />

Derek Simon<br />
<i>Senior Editor, Handicapper and Horse Player at USRacing.</i></span></font>
                    </td>
                </tr>
            </table>
            
            <table width="640px" border="0" cellspacing="0" cellpadding="20" bgcolor="#e9e8e8" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;"><font face="'Roboto', Arial, sans-serif"><img src="https://www.usracing.com/newsletter/welcome-email/images/helpicon.png" width="25"><br />
                        <br />
                        
                        <span style="font-size:10px;"><strong>Check out our <a href="https://www.usracing.com/getting-started?ref=welcome_v2">getting started guide</a> for some of our most frequently asked question.</strong><br><br>
                                                      You can alway give us a call, at our easy to remember toll-free number, at 1-844-BET-HORSES (1-844-238-4677) to speak with<br> one of our resident experts.<br><br>

Still have questions? Just reply to this email. We'd love to help!</span></font>
                    </td>
                </tr>
            </table>
            <table width="640" class="100p" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="50"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
</body>
</html>
