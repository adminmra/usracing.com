<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style>


         @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900'); /*Calling our web font*/
        /* Some resets and issue fixes */
        #outlook a { padding:0; }
        body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }     
        .ReadMsgBody { width: 100%; }
        .ExternalClass {width:100%;} 
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;} 
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;} 
		a:link {color:#1571ba;}
        
        /* End reset */
        
        
        /* These are our tablet/medium screen media queries */
        @media screen and (max-width: 630px){
                
                
            /* Display block allows us to stack elements */                      
            *[class="mobile-column"] {display: block;} 
            
            /* Some more stacking elements */
            *[class="mob-column"] {float: none !important;width: 100% !important;}     
                 
            /* Hide stuff */
            *[class="hide"] {display:none !important;}          
            
            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {width:100% !important; height:auto !important;}                    
                
            /* For the 2x2 stack */            
            *[class="condensed"] {padding-bottom:40px !important; display: block;}
            
            /* Centers content on mobile */
            *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}            
            
            /* 100percent width section with 20px padding */
            *[class="100pad"] {width:100% !important; padding:20px;} 
            
            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;} 
            
            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;} 
            
        
        }
            
        
    </style>
    
   
</head>


<div style="background:#e9e8e8;">

<body style="padding:0; margin:0" bgcolor="#e9e8e8">

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
    <tr>
        <td align="center" valign="top">
             <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
               
                <tr>
                    <td align="left" style="font-size:16px; "width="640" bgcolor="white">
                       <hr>
                       <img src="https://www.usracing.com/newsletter/newsletter-new/images/logo.png" width="220"><br />
                        
                    </td>
                </tr>
            </table>
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td bgcolor="#fff" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    
                                                    <tr>
                                                        <td align="left" style="color:#1571ba; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:34px; font-weight:900;">ARROGATE HEADLINES
<br>'DUAL IN THE DESERT'</span>
                                                                <br />
                                                                <span style="font-size:16px; color:#8f8f8f;">Suffice it to say that, after a light week of racing -- including no racing at all on Christmas Day -- fans of the Sport of Kings are ready to start getting serious again.
</span>
                                                                <br /><br />
                                                                
                                                                <a href="#" style="color:#FFFFFF; text-decoration:none;">
                                                                <table border="0" cellspacing="0" cellpadding="0" >
                                                                    <tr>
                                                                        <td align="center" style="color:#FFFFFF; font-size:16px;"><a href="#" style="background-color:#ff0000;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:12px;
    padding:9px 24px;
    text-decoration:none;
    ">Click HERE to read more</a></td>
                                                                    </tr>
                                                                </table>
                                                                </a>
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            
            <!----------------- first article start ---------------->
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td bgcolor="#fff" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    
                                                    <tr>
                                                        <td align="left" style="color:#1571ba; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:22px; font-weight:900; text-decoration:underline;">Arrogate: The $29 Million Horse?</span>
                                                                <br /><br />
                                                                <span style="font-size:16px; color:#8f8f8f;">March 25, 2016: A grey three-year-old Unbridled’s Song colt by the name of Arrogate is training toward his debut at Los Alamitos Race Track. March 25, 2017: As a four-year-old, Arrogate has become t... 
																</span><br />
                                                               <a href="#"><span style="font-size:16px; font-weight:900; text-decoration:underline;">Read More >>></span></a>
                                                                <br />
                                                                
                                                                
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
             <!---------- first article end ---------------->
             
              <!----------------- Second article start ---------------->
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td bgcolor="#fff" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    
                                                    <tr>
                                                        <td align="left" style="color:#1571ba; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:22px; font-weight:900; text-decoration:underline;">
Arrogate Rolls to Become<br/>
Richest Racehorse in Dubai World Cup</span>
                                                                <br /><br />
                                                                <span style="font-size:16px; color:#8f8f8f;">In a performance that will no doubt go down in history as one of the most impressive of all time, Juddmonte Farms’ Arrogate captured the 2017 Dubai World Cup (GI) at Meydan Racecourse with a jaw-dro... 
																</span><br />
                                                               <a href="#"><span style="font-size:16px; font-weight:900; text-decoration:underline;">Read More >>></span></a>
                                                                <br />
                                                                
                                                                
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
             <!---------- Second article end ---------------->
             
             <!----------------- Third article start ---------------->
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td bgcolor="#fff" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    
                                                    <tr>
                                                        <td align="left" style="color:#1571ba; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:22px; font-weight:900; text-decoration:underline;">Mind Your Biscuits Takes Aim at 
Golden Shaheen Glory!</span>
                                                                <br /><br />
                                                                <span style="font-size:16px; color:#8f8f8f;">In a performance that will no doubt go down in history as one of the most impressive of all time, Juddmonte Farms’ Arrogate captured the 2017 Dubai World Cup (GI) at Meydan Racecourse with a jaw-dro...
																</span><br />
                                                               <a href="#"><span style="font-size:16px; font-weight:900; text-decoration:underline;">Read More >>></span></a>
                                                                <br />
                                                                
                                                                
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
             <!---------- Third article end ---------------->
             
              <!----------------- STAKES RACES START ---------------->
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td bgcolor="#fff" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>
                                                        <td align="left" style="color:#1571ba; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:22px; font-weight:900;">STAKES RACES THIS WEEKEND </span>
                                                            </font><br /><br />
                                                            <img src="https://www.usracing.com/newsletter/newsletter-new/images/stakesRaces.jpg" /> 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                       
                                                    </tr>	
                                                </table>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
             <!---------- STAKES RACES END ---------------->
            
            
            <!--<table width="640" border="0" cellspacing="0" cellpadding="0" bgcolor="#1571ba" class="100p" height="1">
                <tr>
                    <td width="20" bgcolor="#FFFFFF"></td>
                    <td align="center" height="1" style="line-height:0px; font-size:1px;">&nbsp;</td>
                    <td width="20" bgcolor="#ffffff"></td>
                </tr>
            </table>-->
            
            
           
            <table width="640" class="100p" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="50"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
</body>
</html>
