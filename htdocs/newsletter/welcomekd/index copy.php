<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    
    <style>


         @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900'); /*Calling our web font*/
        /* Some resets and issue fixes */
        #outlook a { padding:0; }
        body{ width:100% !important; -webkit-text; size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; }     
        .ReadMsgBody { width: 100%; }
        .ExternalClass {width:100%;} 
        .backgroundTable {margin:0 auto; padding:0; width:100%;!important;} 
        table td {border-collapse: collapse;}
        .ExternalClass * {line-height: 115%;}
		a:link {color:#1571ba; font-weight: bold;}
        
        /* End reset */
        
        
        /* These are our tablet/medium screen media queries */
        @media screen and (max-width: 630px){
                
                
            /* Display block allows us to stack elements */                      
            *[class="mobile-column"] {display: block;} 
            
            /* Some more stacking elements */
            *[class="mob-column"] {float: none !important;width: 100% !important;}     
                 
            /* Hide stuff */
            *[class="hide"] {display:none !important;}          
            
            /* This sets elements to 100% width and fixes the height issues too, a god send */
            *[class="100p"] {width:100% !important; height:auto !important;}                    
                
            /* For the 2x2 stack */            
            *[class="condensed"] {padding-bottom:40px !important; display: block;}
            
            /* Centers content on mobile */
            *[class="center"] {text-align:center !important; width:100% !important; height:auto !important;}            
            
            /* 100percent width section with 20px padding */
            *[class="100pad"] {width:100% !important; padding:20px;} 
            
            /* 100percent width section with 20px padding left & right */
            *[class="100padleftright"] {width:100% !important; padding:0 20px 0 20px;} 
            
            /* 100percent width section with 20px padding top & bottom */
            *[class="100padtopbottom"] {width:100% !important; padding:20px 0px 20px 0px;} 
            
        
        }
            
        
    </style>
    
   
</head>


<div style="background:#e9e8e8;">

<body style="padding:0; margin:0" bgcolor="#e9e8e8">

<table border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0" width="100%">
    <tr>
        <td align="center" valign="top">
        
             <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#1571ba" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;" width="640"><img src="https://www.usracing.com/newsletter/welcome-email/images/betusr_white.png" width="220"><br />
                        
                    </td>
                </tr>
            </table>
            <table width="640" cellspacing="0" cellpadding="0" bgcolor="#" class="100p">
                <tr>
                    <td background="https://www.usracing.com/newsletter/welcome-email/images/hero.jpg" bgcolor="#003158" width="640" valign="top" class="100p">
                        <!--[if gte mso 9]>
                        <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:640px;">
                            <v:fill type="tile" src="https://www.usracing.com/newsletter/welcome-email/images/hero.jpg" color="#3b464e" />
                            <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                                <![endif]-->
                                <div>
                                    <table width="640" border="0" cellspacing="0" cellpadding="20" class="100p">
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                   
                                                </table>
                                                <table border="0" cellspacing="0" cellpadding="0" width="600" class="100p">
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="color:#FFFFFF; font-size:24px;">
                                                            <font face="'Roboto', Arial, sans-serif">
                                                         <span style="font-size:34px; font-weight:900;">WELCOME TO BET USRACING</span>
                                                                <br />
                                                                <span style="font-size:16px;">You are seconds away from placing your first bet!
</span>
                                                                <br />
                                                                
                                                                <a href="##" style="color:#FFFFFF; text-decoration:none;">
                                                                <table border="0" cellspacing="0" cellpadding="10" >
                                                                    <tr>
                                                                        <td align="center" style="color:#FFFFFF; font-size:16px;"><a href="https://www.betusracing.ag/login?ref=welcome_letter_v2" style="background-color:#ff0000;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:12px;
    padding:9px 24px;
    text-decoration:none;
    text-shadow:0px 1px 0px #854629;">DEPOSIT NOW TO START PLAYING!</a></td>
                                                                    </tr>
                                                                </table>
                                                                </a>
                                                            </font>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="35"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <!--[if gte mso 9]>
                            </v:textbox>
                        </v:rect>
                        <![endif]-->
                    </td>
                </tr>
            </table>
            
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f; line-height: 23px;"><font face="'Roboto', Arial, sans-serif">Hi %%First Name%%,<br /><span style="font-size:16px;"> Welcome to Bet usracing, we're here to offer you the best  experience around.<br />  Here is just some of what you will get:</span></font><br><br>
                    <hr></td>
                </tr>
            </table>
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;"><font face="'Roboto', Arial, sans-serif">
                     <p style="color:#1571ba;"><strong>6 Reasons to Bet the Kentucky Derby
at US Racing</strong></p><br>
                        <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/promos/free-derby-bet?ref=welcome-kd"><strong>Free Kentucky Derby Bet</strong></a><br><br>Place a $10 bet on the finishing time of the Kentucky Derby. If you lose your bet, you get your money back! <a href="https://www.usracing.com/login?ref=welcome-kd">Bet my free bet now.</a></p><br>
                        
                        <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/promos/10-cash-bonus?ref=welcome-kd"><strong>Exceptional Member Bonuses</strong></a><br><br>For your first deposit with US Racing, you'll get an additional 10% cash added to your account absolutely free. Deposit a minimum of $100 and you could qualify to earn an additional $150!<a href="https://www.usracing.com/login?ref=welcome-kd"> Deposit Now!</a></p><br>
                        
                        <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/promos/kd-blackjack-tournament?ref=welcome-kd"><strong>Triple Crown Blackjack Tournament</strong></a><br><br>Play any Blackjack game from May 2nd and June 12th and get ready to compete for real cash. Cash prizes are paid to the top 20 players with the highest payout percentage!<a href="https://www.usracing.com/login?ref=welcome-kd">&nbsp;Play Now!</a></p><br>
                        
                        <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/kentucky-derby-betting?ref=welcome-kd"><strong>Trainer Betting Odds and Exclusive Derby Props</strong></a><br><br>Only at US Racing: you get great odds with amazing payouts on the leading Kentucky Oaks and Derby horses and even the leading jockeys and trainers. Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more.<a href="https://www.usracing.com/login?ref=welcome-kd"><br>Bet Now!</a></p><br>
                        
                        <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/promos/50percent-casino-rebate?ref=welcome-kd"><strong>50% Casino Bonus</strong></a><br><br>Every Thursday at the Casino is special at US Racing because you get 50% Cash returned to your account of any losses! That's right, you will get 50% Cash Back to your account every Thursday.</p><br>
                        
                         <p style="color:#8f8f8f;"><a href="https://www.betusracing.ag/promos/refer-a-friend?ref=welcome-kd"><strong>10% Refer-a-friend bonus</strong></a><br><br>hare the Love. Tell your friends about US Racing and when they join, they will get a 10% bonus is added to their accounts. Not only that, but you'll get a matching 10% in your account too!</p><br>
                        
                        <hr>
                    </td>
                </tr>
            </table>
            
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; "><font face="'Roboto', Arial, sans-serif"><span style="color:#8f8f8f; font-size:16px;  font-weight:700;">Deposit Options</span><br />
                       
                        <span style="font-size:16px; line-height: 25px; font-weight:400; color:#8f8f8f;"><br>We accept the following forms of payment:<img src="https://www.usracing.com/newsletter/welcomekd/images/deposit-options2.png" width="280" style="padding:20px;">
</span><br></font>
                    </td>
                </tr>
                <tr> <td align="center" style="color:#FFFFFF; font-size:16px;"><a href="https://www.betusracing.ag/login?ref=welcome_letter_v2" style="background-color:#ff0000;
    -moz-border-radius:4px;
    -webkit-border-radius:4px;
    border-radius:4px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    font-size:12px;
    padding:9px 24px;
    text-decoration:none;
    text-shadow:0px 1px 0px #854629;">DEPOSIT NOW !</a></td></tr>
            </table>
            
            <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#1571ba" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#ffffff;"><font face="'Roboto', Arial, sans-serif"><span style="font-size:16px; font-weight:500;">Your Account Info</span><br />
                        <br />
                        <span style="font-size:16px;">%%First Name%%, below is your account information for your records:</span><br /><br />
                        <span style="font-weight:500; font-size:16px;">Your Account ID is: %%Account%%</span><br /><br />
                        <span style="font-size:16px;">You can sign in with either your Email Address or Account ID and password.
Please save this email for future reference.</span></font>
                    </td>
                </tr>
            </table>
             
            
               <table width="640" border="0" cellspacing="0" cellpadding="20" bgcolor="#ffffff" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;"><font face="'Roboto', Arial, sans-serif"><img src="https://www.usracing.com/newsletter/welcome-email/images/derekpic.png" height="60px"><br />
                        <br />
                        
                        <span style="font-size:16px;">“Bet usracing provides more gaming choices than any other horse racing site, and the quick payouts are great. I know you'll enjoy playing at Bet usracing, especially that rebate. Good Luck at the races.”<br /><br />

Derek Simon<br />
<i>Senior Editor, Handicapper and Horse Player at USRacing.</i></span></font>
                    </td>
                </tr>
            </table>
            
            <table width="640px" border="0" cellspacing="0" cellpadding="20" bgcolor="#e9e8e8" class="100p">
                <tr>
                    <td align="center" style="font-size:16px; color:#8f8f8f;"><font face="'Roboto', Arial, sans-serif"><img src="https://www.usracing.com/newsletter/welcome-email/images/helpicon.png" width="25"><br />
                        <br />
                        
                        <span style="font-size:10px;"><strong>Check out our <a href="https://www.usracing.com/getting-started?ref=welcome_v2">getting started guide</a> for some of our most frequently asked question.</strong><br><br>
                                                      You can alway give us a call, at our easy to remember toll-free number, at 1-844-BET-HORSES (1-844-238-4677) to speak with<br> one of our resident experts.<br><br>

Still have questions? Just reply to this email. We'd love to help!</span></font>
                    </td>
                </tr>
            </table>
            <table width="640" class="100p" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="50"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
    
</body>
</html>
