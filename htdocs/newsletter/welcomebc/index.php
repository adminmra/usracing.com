<html lang="en">
<head>
<title></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
 <style type="text/css">
       table.t1 {width: 80% border-collapse: collapse; margin-left: auto;
    margin-right: auto;}
    td.td1 { background-color: #e3e3e3; padding: 10.0px 20.0px 10.0px 20.0px;}
   
  </style>
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
          text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
          margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
/*
        td[class="mobile-hide"]{
          display:none;}
*/

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          max-width: 100% !important;
          width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          /* text-align: center; */
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
            margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
            width:80% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 18px !important;
        }

    }
    
    table td, table th { wordwrap:break-word;  }
table > tr > th { border-bottom: none; }
table td.right, table th.right { text-align:right; }
table td.center, table th.center, table td.num { text-align:center; }
.table.table-bordered > tr th:first-child, .table.table-bordered > tr td:first-child { border-left: none; }
.table-condensed thead > tr > th, .table-condensed tbody > tr > th, .table-condensed tfoot > tr > th, .table-condensed thead > tr > td, .table-condensed tbody > tr > td, .table-condensed tfoot > tr > td{ padding:5px 10px; font-size:12px;}
.table-bordered > tbody > tr th { background:#f4f4f4 !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;font-size:12px;}
.table-bordered > tbody > tr th:first-child { border-left:0; }
.table caption { background:#1571BA; padding:10px; font-size:20px; font-weight: 500; text-align:left; border:0; text-align:center; color:#fff;}
.dateUpdated {  font-size:12px; }
table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table tbody>tr>td,.table tbody>tr>th,.table tfoot>tr>td,.table tfoot>tr>th,.table thead>tr>td,.table thead>tr>th{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table caption+thead tr:first-child td,.table caption+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table thead:first-child tr:first-child td,.table thead:first-child tr:first-child th{border-top:0}.table tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed tbody>tr>td,.table-condensed tbody>tr>th,.table-condensed tfoot>tr>td,.table-condensed tfoot>tr>th,.table-condensed thead>tr>td,.table-condensed thead>tr>th{padding:5px}.table-bordered,.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*=col-]{float:none;display:table-column}table td[class*=col-],table th[class*=col-]{float:none;display:table-cell}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8;border-color:#d6e9c6}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6;border-color:#c9e2b3}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede;border-color:#eed3d7}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc;border-color:#e6c1c7}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3;border-color:#fbeed5}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc;border-color:#f8e5be}@media (max-width:768px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;overflow-x:scroll;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0;background-color:#fff}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>thead>tr:last-child>td,.table-responsive>.table-bordered>thead>tr:last-child>th{border-bottom:0}}
</style>
</head>
<body style="margin: 0; padding: 0;">
<!-- HEADER -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td bgcolor="#003158"><!-- HIDDEN PREHEADER TEXT -->
<div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">Hi %%First Name%%<!-- <Property name="Subscriber.FirstName"/> -->, you are seconds away from placing your bet on the Breeders' Cup! Good times are moments away.</div>
<div style="padding: 0px 15px 0px 15px;" align="center">
<table style="width: 500px;" class="wrapper" border="0" cellpadding="0" cellspacing="0"><!-- LOGO/PREHEADER TEXT -->
<tbody>
<tr>
<td style="padding: 20px 0px 30px 0px;" class="logo">
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="left" bgcolor="#003158" width="100"> <center><img alt="US Racing" src="https://www.betusracing.ag/public/assets/images/busr_logo_images/busr_new_logo/busr.png" style="display: block;  font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" height="60" border="0" width="200" /></center> </td>
<td class="mobile-hide" align="right" bgcolor="#003158" width="400">
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 0 0 5px 0; font-size: 14px; font-family: Arial, sans-serif; color: #666666; text-decoration: none;" align="right"><a href="tel:1-844-BET-HORS" style="color: #ffffff; text-decoration: none;">1-844-BET-HORSES</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<!-- ONE COLUMN SECTION -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 70px 15px 70px 15px;" class="section-padding" align="center" bgcolor="#ffffff">
<table style="width: 500px;" class="responsive-table" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><!-- HERO IMAGE -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td class="padding-copy"><a href="https://www.betusracing.ag/login?ref=email-breeders-cup-welcome" target="_blank"> <img src="https://www.usracing.com/newsletter/welcomebc/welcome-breeders-cup.jpg" alt="Welcome to the Breeders' Cup" style="display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max"  border="0" width="500" /> </a></td>
</tr>
<tr>
<td><!-- COPY -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="font-size: 22px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy" align="center">Hi %%First Name%%<!-- <Property name="Subscriber.FirstName"/> -->,</td>
</tr>
<tr>
<td style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #000000;" class="padding-copy" align="center">
<p>Thank you for joining BUSR. Below is your account information for your records:</p>
<table style="width: 80%;" class="t1" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td class="td1" style="color: #666666; font-size: 14px;" valign="top">
<p><strong>Your Account Information:</strong></p>
<p>Your Account ID is: %%Account%%<!-- <Property name="Subscriber.custom.Account"/> --><span class="Apple-converted-space"> </span></p>
<p>You can sign in with either your Email Address or Account ID and password. If you have forgotten your password, <a href="https://www.betusracing.ag/forgot-password">click here</a>.</p></td>
</tr>
</tbody>
</table>
<div style="text-align:left;"><br>
<h2 style=" text-align: center; color: blue;  ">5 Reasons to Bet the Breeders' Cup<br>	at BUSR</h2>



<p style="color: blue;"><strong> Free Breeders' Cup Bet</strong></p>

<p>Place a $10 bet on the finishing time of the Breeders' Cup. If you lose your bet, you get your money back!    <a href="https://www.betusracing.ag/login?ref=email-breeders-cup-welcome"> Bet my free bet now</a>.</p>



<p style="color: blue;"><strong>Exceptional Member Bonuses</strong>

<p>For your first deposit with US Racing, you'll get an additional 10% cash added to your account absolutely free. Deposit a minimum of $100 and you could qualify to earn an additional $150!  <a href="https://www.betusracing.ag/login?ref=email-breeders-cup-welcome"> Deposit Now</a>.</p>

<p  style="color: blue;"><strong>8% Horse Betting Rebate</strong></p>
<p>Paid daily to your account.  Depending on the track you wager and the type of bet, you can earn up to 8% of the amount you bet on each race, win or lose!</p>

<p style="color: blue;"><strong>50% Casino Bonus</strong></p>

<p>Every Thursday at the Casino is special at US Racing because you get 50% Cash returned to your account of any losses! That's right, you will get 50% Cash Back to your account every Thursday.</p>


<p  style="color: blue;"><strong>10% Refer-a-friend bonus</strong></p>
<p>Share the Love.  Tell your friends about US Racing and when they join, they will get  a 10% bonus is added to their accounts. Not only that, but you'll get a matching 10% in your account too! <a href="https://www.betusracing.ag/login?ref=email-breeders-cup-welcome"> Login now</a> and select "Refer a Friend".</p>
<center><a href="https://www.betusracing.ag/login?ref=email-refer-a-friend-for-breeders" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; background-color: #c71f24; border-top: 15px solid #c71f24; border-bottom: 15px solid #c71f24; border-left: 35px solid #c71f24; border-right: 35px solid #c71f24; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Get Started and Login Now</a></center>
</div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td align="center"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- TWO COLUMN SECTION --><!-- COMPACT ARTICLE SECTION -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 70px 15px 70px 15px;" class="section-padding" align="center" bgcolor="#ffffff">
<table style="padding: 0px 0px 20px; width: 500px;" class="responsive-table" border="0" cellpadding="0" cellspacing="0"><!-- TITLE -->
<tbody>
<tr>
<td style="padding: 0 0 10px 0; font-size: 25px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #666666;" class="padding-copy" colspan="2" align="center">Need Help?</td>
</tr>
<tr>
<td class="no-padding"><!-- ARTICLE -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 0 25px 0px 0px; font-size: 12x; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #666666;" class="padding-copy" align="left"><!--Stakes------------------------------------------------------------------------------------------>
<p style="font-size: 14px;">Check out our <a href="https://www.betusracing.ag/help?ref=email-breeders-cup-welcome" target="_blank" style="color: #003158;">getting started guide</a> for some of our most frequently asked questions.</p>
<p style="font-size: 14px;">You can alway give us a call, at our easy to remember toll-free number, at <a href="tel:1-844-BET-HORS" style="color: #003158;">1-844-BET-HORSES</a> to speak with one of our resident experts.</p>
<p style="font-size: 14px;">Still have questions? Just reply to this email. We'd love to help!</p>
<!--END Stakes----------------------------------------------------------------------------------------></td>
<td style="padding: 40px 0 0 0;" class="mobile-hide" valign="top"><a href="https://www.betusracing.ag/contact-us?ref=email-breeders-cup-welcome" target="_blank"><img src="https://www.usracing.com/newsletter/welcome/operator.png" alt="A Resident Expert" style="display: block; font-family: Arial; color: #666666; font-size: 12px; width: 105px; height: 105px;" height="105" border="0" width="105" /></a></td>
</tr>
<tr>
<td style="padding: 0 0 45px 25px;" class="padding" align="left">
<table class="mobile-button-container" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="center"><!-- BULLETPROOF BUTTON -->
<table style="width: 100%;" class="mobile-button-container" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 0;" class="padding-copy" align="center">
<table class="responsive-table" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td align="center"><a href="https://www.betusracing.ag/help?ref=email-breeders-cup-welcome" target="_blank" style="font-size: 15px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background-color: #003158; border-top: 10px solid #003158; border-bottom: 10px solid #003158; border-left: 20px solid #003158; border-right: 20px solid #003158; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button"> Get Help </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<!-- COMPACT ARTICLE SECTION -->
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td style="padding: 20px 15px 70px 15px;" class="section-padding" align="center" bgcolor="#ffffff"><p style="font-size: 14px;font-family: Helvetica, Arial, sans-serif; color: #666666;"> Be sure to join in on the Breeders' Cup conversation on <a href="https://www.facebook.com/betusracing/"><img title="Facebook Like Button" alt="Facebook" src="https://www.usracing.com/newsletter/welcome/facebook.png" width="80" height="30" style="vertical-align:middle;"  /></a> and

 <a href="https://twitter.com/betusracing"><img title="Twitter  Button" alt="Twitter" src="https://www.usracing.com/newsletter/welcome/twitter.png" width="80" height="30" style="vertical-align:middle;" /></a>. 
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td bgcolor="#003158">
<div id="_t"></div>
<img src="https://8up3jo9g.emltrk.com/8up3jo9g?d=$Subscriber.EMAIL" height="1" border="0" width="1" /> <img id="abine_0.08638985328127102:0.7946137492337677" src="https://www.google-analytics.com/collect?v=1&amp;tid=UA-742771-29&amp;cid=&amp;t=event&amp;ec=email&amp;ea=open&amp;el=&amp;cs=welcome-email-KD&amp;cm=email&amp;cn=Welcome&amp;cm1=1&amp;cd1=" /></td>
</tr>
</tbody>
</table>
<!-- FOOTER -->
</body>
</html>