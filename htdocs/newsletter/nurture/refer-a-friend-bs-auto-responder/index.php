<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
        body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
        img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        body{margin:0; padding:0;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
        table{border-collapse:collapse !important;}
        body{height:100% !important; margin:0; padding:0; width:100% !important;}

        /* iOS BLUE LINKS */
        .appleBody a {color:#68440a; text-decoration: none;}
        .appleFooter a {color:#999999; text-decoration: none;}

        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            table[class="wrapper"]{
                width:100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            td[class="logo"]{
                text-align: left;
                padding: 20px 0 20px 0 !important;
            }

            td[class="logo"] img{
                margin:0 auto!important;
            }

            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            td[class="mobile-hide"]{
                display:none;}

            table[class="mobile-hide-table"]{
                display:none; visibility: hidden}

            img[class="mobile-hide"]{
                display: none !important;
            }

            img[class="img-max"]{
                max-width: 100% !important;
                width: 80% !important;
                height:auto !important;
            }

            /* FULL-WIDTH TABLES */
            table[class="responsive-table"]{
                width:100%!important;
            }

            table[class="responsive-table"]{
                width:100%!important;
            }

            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            td[class="padding"]{
                padding: 10px 5% 15px 5% !important;
            }

            td[class="padding-copy"]{
                padding: 10px 5% 10px 5% !important;
                text-align: center;
            }

            td[class="padding-meta"]{
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }

            td[class="no-pad"]{
                padding: 0 0 20px 0 !important;
            }

            td[class="no-padding"]{
                padding: 0 !important;
            }

            td[class="section-padding"]{
                padding: 50px 15px 50px 15px !important;
            }

            td[class="section-padding-bottom-image"]{
                padding: 50px 15px 0 15px !important;
            }

            /* ADJUST BUTTONS ON MOBILE */
            td[class="mobile-wrapper"]{
                padding: 10px 5% 15px 5% !important;
            }

            table[class="mobile-button-container"]{
                margin:0 auto;
                width:100% !important;
            }

            a[class="mobile-button"]{
                width:80% !important;
                padding: 15px !important;
                border: 0 !important;
                font-size: 16px !important;
            }

        }

        table td, table th { wordwrap:break-word;  }
        table > tr > th { border-bottom: none; }
        table td.right, table th.right { text-align:right; }
        table td.center, table th.center, table td.num { text-align:center; }
        .table.table-bordered > tr th:first-child, .table.table-bordered > tr td:first-child { border-left: none; }
        .table-condensed thead > tr > th, .table-condensed tbody > tr > th, .table-condensed tfoot > tr > th, .table-condensed thead > tr > td, .table-condensed tbody > tr > td, .table-condensed tfoot > tr > td{ padding:5px 10px; font-size:12px;}
        .table-bordered > tbody > tr th { background:#f4f4f4 !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;font-size:12px;}
        .table-bordered > tbody > tr th:first-child { border-left:0; }
        .table caption { background:#1571BA; padding:10px; font-size:20px; font-weight: 500; text-align:left; border:0; text-align:center; color:#fff;}
        .dateUpdated {  font-size:12px; }
        table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table tbody>tr>td,.table tbody>tr>th,.table tfoot>tr>td,.table tfoot>tr>th,.table thead>tr>td,.table thead>tr>th{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table caption+thead tr:first-child td,.table caption+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table thead:first-child tr:first-child td,.table thead:first-child tr:first-child th{border-top:0}.table tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed tbody>tr>td,.table-condensed tbody>tr>th,.table-condensed tfoot>tr>td,.table-condensed tfoot>tr>th,.table-condensed thead>tr>td,.table-condensed thead>tr>th{padding:5px}.table-bordered,.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*=col-]{float:none;display:table-column}table td[class*=col-],table th[class*=col-]{float:none;display:table-cell}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8;border-color:#d6e9c6}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6;border-color:#c9e2b3}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede;border-color:#eed3d7}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc;border-color:#e6c1c7}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3;border-color:#fbeed5}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc;border-color:#f8e5be}@media (max-width:768px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;overflow-x:scroll;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0;background-color:#fff}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>thead>tr:last-child>td,.table-responsive>.table-bordered>thead>tr:last-child>th{border-bottom:0}}
    </style>
</head>
<body style="margin: 0; padding: 0; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666; font-size: 16px">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#003158">
            <!-- HIDDEN PREHEADER TEXT -->
            <div style="display: none;color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">

              Hi %%First Name%%, tell your friends about US Racing for the Belmont Stakes and get extra cash into your account!
            </div>
            <div align="center" style="padding: 0px 15px 0px 15px;">
                <table border="0" cellpadding="0" cellspacing="0" width="500" class="wrapper">
                    <!-- LOGO/PREHEADER TEXT -->
                    <tr>
                        <td style="padding: 30px 0px 20px 0px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#003158" width="55%" align="center" style="text-align: center;">
                                                                                   <a href="https://www.usracing.com?ref=refer-a-friend-for-belmont"><img alt="US Racing" src="https://www.usracing.com/img/usracing.png" width="250px" height="53px"  style=" font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px; " border="0"></a>
                                   
                                    </td>
                                
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 10px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="" class="responsive-table">
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table style="width: 100%; max-width: 550px;" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 20px 0 0 0; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666; text-align: left; " class="padding-copy" ><center><a href="https://www.usracing.com?ref=refer-a-friend-for-belmont"><img src="https://www.usracing.com/img/refer-a-friend.jpg" alt="Good Friends" style='width:100%;' ></a></center>
                                                <p>
	                                                <h2 style="color:blue; text-align: center ">Get a Free Cash Bonus for the<br> Belmont Stakes</h2>
                                                <p >Hi %%First Name%%<!-- <Property name="Subscriber.FirstName"/> -->, thanks for joining US Racing.  Did you know that you could get extra cash in your account for the Belmont?</p>

<p>For every friend that you send to US Racing who makes a deposit, they will get a 10% signup bonus in their account.  Not only that, but you'll get a matching 10%  in your account too! <p>  As soon as your friend makes that deposit, your account will be credited too!  <p>
	                                                 <!-- BULLETPROOF BUTTON -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                    <tr>
                                                        <td align="center"><a href="https://www.usracing.com/login?ref=refer-a-friend-for-belmont" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; background-color: #c71f24; border-top: 15px solid #c71f24; border-bottom: 15px solid #c71f24; border-left: 35px solid #c71f24; border-right: 35px solid #c71f24; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Login now to invite your friends</a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
<br>
	                                                
	                                                
                                     <p >You can refer as many friends as you want, no limits. So, share the love with US Racing!</p>


<p>Kind Regards,


<p>Chris Savage<br>
	
US Racing<br>
<a href="https://www.usracing.com?ref=refer-a-friend-for-belmont">www.usracing.com</a><br>
<!-- support@myracingaccount.com<br> -->
1-844-US-RACING<br>
<!-- 1-844-877-2246<br> -->




<p style=" font-size: 10px "> <a href="https://www.usracing.com/refer-a-friend?ref=refer-a-friend-for-belmont">*Terms & Conditions</a> Apply
                                                </p>
                                            
                                               
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- FOOTER -->

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#ffffff; font-size: 14px; ">
    <tr>
        <td bgcolor="#003158" align="center" style="padding: 15px 0px;  color:#ffffff">Toll Free  <a href="tel:1-844-US-RACING" style="color: #FFFFFF; ">1-844-US-RACING</a>  <span style="color:#003158">|</span> <a href="mailto:support@myracingaccount.com"style="color: #FFFFFF; text-decoration: none; ">support@myracingaccount.com</a> <span style="color:#003158">|</span> <a href="https://www.usracing.com?ref=refer-a-friend-for-belmont"style="color: #FFFFFF; text-decoration: none; ">www.usracing.com</a>
        </td>
    </tr>
</table>

</body>
</html>
