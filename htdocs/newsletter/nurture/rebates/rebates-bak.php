<!DOCTYPE html>
<html lang="en">
<head>
<title>Rebates</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
          text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
          margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        td[class="mobile-hide"]{
          display:none;}

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          max-width: 100% !important;
          width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
            margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
            width:80% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

    }
    
    table td, table th { wordwrap:break-word;  }
table > tr > th { border-bottom: none; }
table td.right, table th.right { text-align:right; }
table td.center, table th.center, table td.num { text-align:center; }
.table.table-bordered > tr th:first-child, .table.table-bordered > tr td:first-child { border-left: none; }
.table-condensed thead > tr > th, .table-condensed tbody > tr > th, .table-condensed tfoot > tr > th, .table-condensed thead > tr > td, .table-condensed tbody > tr > td, .table-condensed tfoot > tr > td{ padding:5px 10px; font-size:12px;}
.table-bordered > tbody > tr th { background:#f4f4f4 !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;font-size:12px;}
.table-bordered > tbody > tr th:first-child { border-left:0; }
.table caption { background:#1571BA; padding:10px; font-size:20px; font-weight: 500; text-align:left; border:0; text-align:center; color:#fff;}
.dateUpdated {  font-size:12px; }
table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table tbody>tr>td,.table tbody>tr>th,.table tfoot>tr>td,.table tfoot>tr>th,.table thead>tr>td,.table thead>tr>th{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table caption+thead tr:first-child td,.table caption+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table thead:first-child tr:first-child td,.table thead:first-child tr:first-child th{border-top:0}.table tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed tbody>tr>td,.table-condensed tbody>tr>th,.table-condensed tfoot>tr>td,.table-condensed tfoot>tr>th,.table-condensed thead>tr>td,.table-condensed thead>tr>th{padding:5px}.table-bordered,.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*=col-]{float:none;display:table-column}table td[class*=col-],table th[class*=col-]{float:none;display:table-cell}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8;border-color:#d6e9c6}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6;border-color:#c9e2b3}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede;border-color:#eed3d7}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc;border-color:#e6c1c7}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3;border-color:#fbeed5}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc;border-color:#f8e5be}@media (max-width:768px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;overflow-x:scroll;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0;background-color:#fff}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>thead>tr:last-child>td,.table-responsive>.table-bordered>thead>tr:last-child>th{border-bottom:0}}
</style>
</head>
<body style="margin: 0; padding: 0;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#003158">
            <!-- HIDDEN PREHEADER TEXT -->
            <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
             
             Who says the Derby is all about the horse?  Place your bets on the jockey and trainer to win!  Exclusive to BUSR, you have hundreds of betting options for 142nd Kentucky Derby.             
   </div>
            <div align="center" style="padding: 0px 15px 0px 15px;">
                <table border="0" cellpadding="0" cellspacing="0" width="500" class="wrapper">
                    <!-- LOGO/PREHEADER TEXT -->
                    <tr>
                        <td style="padding: 20px 0px 30px 0px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#003158" width="100" align="left">
                                        <a href="https://www.usracing.com/?nl_logo=jan_6_kentucky_derby_odds_release" target="_blank">
                                           
                                         
                                            <img alt="US Racing" src="https://www.usracing.com/img/usracing.png" width="200" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                                        </a>
                                    </td>
                                    <td bgcolor="#003158" width="400" align="right" class="mobile-hide">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right" style="padding: 0 0 5px 0; font-size: 14px; font-family: Arial, sans-serif; color: #666666; text-decoration: none;"><span style="color: #FFFFFF; text-decoration: none;">1-844-US-RACING</span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 70px 15px 0px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>   
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0"><!--

                            <tr>
                                  <td class="padding-copy">
                                    <a href="?nl-hero=jan_6_kentucky_derby_odds_release" target="_blank">
 <img 
 
 src="" width="500" height="400" border="0" 
 
 alt="" 
 
 style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">

 
 
                                    </a>
                                </td>
                            </tr>
-->
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy">Rebates</td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">    <OpenTracking/>
    											<p style="width: 100%; max-width: 500px;">
    												Are you not getting enough love from your racebook?  Is the outrageous racetrack take killing your performance?  At BUSR your business is valued and this is how.
    											</p>
                                                <p style="width: 100%; max-width: 500px;">
                                                	You will get up to 8% rebate on all racebook  bets that are <strong>paid daily, win or lose!</strong>
                                                </p>
                                                
</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table><!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 30px 15px 0px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <div class='table-responsive'>
                            <table class='data table table-condensed table-striped table-bordered' summary='Horses - Kentucky Derby Winning Jockey' border='1' cellpadding='0' cellspacing='0' width='100%' style="font-family: Helvetica, Arial, sans-serif;">
                                <tbody>
                                    <tr><th class='center'>Track Type</th><th class='center'>Straight Wagers</th><th class='center'>Exotic Wagers</th></tr><tr  ><td align="center"><strong>Category A</strong></td><td align="center">3%</td><td align="center">8%</td></tr>
                                    <tr  ><td align="center"><strong>Category B</strong></td><td align="center">3%</td><td align="center">5%</td></tr>
                                    <tr  ><td align="center"><strong>Category C</strong></td><td align="center">3%</td><td></td></tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
	        </table>
        </td>
    </tr>
    
</table>

<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0px 15px 0px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>   
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">    <OpenTracking/>
                                        <p style="width: 100%; max-width: 500px;">
                                            For Example
                                        </p>
                                        <p style="width: 100%; max-width: 500px;">
                                            If you placed $500 on win, place, or show at Arapahoe  track and $500 in Trifectas and Exactas at Churchill Downs you get  $55  added to your account tomorrow.
                                        </p>
                                        <p style="width:100%; max-width: 500px">
                                        	Is your track listed?
                                        </p>
									</td>
                                </tr>
                            </table>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table><!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 30px 15px 0px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <div class='table-responsive'>
                            <table class='data table table-condensed table-striped table-bordered' summary='Horses - Kentucky Derby Winning Jockey' border='1' cellpadding='0' cellspacing='0' width='100%' style="font-family: Helvetica, Arial, sans-serif;"><caption>
								BUSR Track Categories
                            </caption>
                                <tbody>
                                    <tr><th class='center'>Category A</th><th class='center'>Category B</th><th class='center'>Category C</th></tr>
                                    <tr  >
                                        <td>Aqueduct</td>
                                        <td>Arlington Park </td>
                                        <td>Aintree</td>
                                    </tr> 
                                    <tr  >
                                        <td>Belmont Park</td>
                                        <td>Canterbury Park </td>
                                        <td>Albuquerque</td>
                                    </tr> 
	                                <tr>
                                        <td>Churchill Downs</td>
                                        <td>Charles Town</td>
                                        <td>Arapahoe Park</td>
                                    </tr>
                                    <tr>
	                                    <td>Del Mar</td>
    	                                <td>Delaware Park</td>
        	                            <td>Ascot Racecourse</td>
                                    </tr>
                                    <tr>
	                                    <td>Gulfstream Park</td>
    	                                <td>Delta Downs</td>
        	                            <td>Atlantic City</td>
                                    </tr>
                                    <tr>
                    	                <td>Gulfstream Park West</td>
                        	            <td>Emerald Downs</td>
                            	        <td>Ayr</td>
                                    </tr>
                                    <tr>
	                                    <td>Keenland</td>
    	                                <td>Fair Grounds</td>
        	                            <td>Ballinrobe</td>
                                    </tr>
                 	                <tr>
                    	                <td>Meydan</td>
                        	            <td>Finger Lakes</td>
                            	        <td>Bangor-on-Dee</td>
                                    </tr>
                                    <tr>
	                                    <td>Monmouth Park</td>
    	                                <td>Golden Gate Foelds</td>
        	                            <td>Bath</td>
                                    </tr>
                 	                <tr>
                    	                <td>Parx Racing</td>
                        	            <td>Hoosier Park</td>
                            	        <td>Bellewstown</td>
                                    </tr>
                                    <tr>
	                                    <td>Santa Anita Park</td>
    	                                <td>Laurel Park</td>
        	                            <td>Belterra</td>
                                    </tr>
                                    <tr>
                    	                <td>Saratoga</td>
                        	            <td>Lone Star Park</td>
                            	        <td>Beverly</td>
                                    </tr>
                                    <tr>
	                                    <td>Tampa Bay Downs</td>
    	                                <td>Los Alamitos</td>
        	                            <td>Brighton</td>
                                    </tr>
                                    <tr>
                    	                <td>Tucson</td>
                        	            <td>Los Alamitos Racecourse</td>
                            	        <td>Cal Expo</td>
                                    </tr>
                                    <tr>
	                                    <td>Woodbine</td
    	                                <td>Louisiana Downs</td>
        	                            <td>Carlisle</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Oakland Park</td>
                                        <td>Cartmel</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Penn National</td>
                                        <td>Catterick</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Pimlico</td>
                                        <td>Chantilly Racecourse</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Prairie Meadowns</td>
                                        <td>Chelmsford City</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Presque Isle Downs</td>
                                        <td>Cheltenham</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Sam Houston Race Park</td>
                                        <td>Chepstown</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Sunland Park</td>
                                        <td>Chester</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Turfway Park</td>
                                        <td>Clonmel</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Colonial Downs</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Columbus Park</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Cork Racecourse</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Curragh</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Deauville-La Touques</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Doncaster</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Down Royal</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Downpatrick</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Dundalk</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Durbanville</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Ellis Park</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Epsom Downs</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Evangeline Downs</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Exeter</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fairmount Park</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fairview</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fairyhouse Racecourse</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fakenham</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Ffos Las</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Flamingo</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fontwell</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Fort Erie</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Galway</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Goodwood</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Gowran</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Greyville</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hamilton</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hastings Park</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hawthorne</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Haydock</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hereford</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hexham</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Hialeah Park</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Huntingdon</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Kelso</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Kempton</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Kenilworth</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Kentucky Downs</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Kilbeggan</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Killarney</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Laytown</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Leicester</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Leopardstown</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Limerick</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Lingfield</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Listowel</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Longchamp</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Ludlow</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Mahoning Valley</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Market Rasen</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Meadowlands</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Mountaineer Park</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Musselburgh</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Naas Racecourse</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Navan</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Newbury Racecourse</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Newcastle</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Newmarket</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Newton Abbot</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Nottingham</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Perth</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Pontefract</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Portland Meadows</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Punchestown</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Redcar</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Remington Park</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Ripon</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Roscommon</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Ruidoso Downs</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Saint-Cloud</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Salisbury</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Sandown</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Scottsville</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Sedgefield</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Sligo</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Southwell</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Stratford</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Suffolk Downs</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>SunRay Park</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Taunton</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Thirsk</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Thistledown</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Thurles</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Towcester</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Tramore</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Turf Paradise</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Turffontein</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Uttoxeter</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Vaal</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Warwick</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Wetherby</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Wexford</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Wincanton</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Windsor</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Wolverhampton</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Woodbine Harness</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Worcester</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Yarmouth</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Yavapai Downs</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>Yonkers Raceway Harness</td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>York</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <!-- BULLETPROOF BUTTON -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                    <tr>
                                                        <td align="center"><a href="https://www.usracing.com/kentucky-derby/trainer-betting?nl-btn=jan_6_kentucky_derby_odds_release" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; background-color: #c71f24; border-top: 15px solid #c71f24; border-bottom: 15px solid #c71f24; border-left: 35px solid #c71f24; border-right: 35px solid #c71f24; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button">Deposit Now </a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
        </td>
    </tr>
    
</table>

<style>@media print{ #_t { background-image: url('https://8sgomd8f.emltrk.com/8sgomd8f?p&d=$Subscriber.EMAIL');}} div.OutlookMessageHeader {background-image:url('https://8sgomd8f.emltrk.com/8sgomd8f?f&d=$Subscriber.EMAIL')} table.moz-email-headers-table {background-image:url('https://8sgomd8f.emltrk.com/8sgomd8f?f&d=$Subscriber.EMAIL')} blockquote #_t {background-image:url('https://8sgomd8f.emltrk.com/8sgomd8f?f&d=$Subscriber.EMAIL')} #MailContainerBody #_t {background-image:url('https://8sgomd8f.emltrk.com/8sgomd8f?f&d=$Subscriber.EMAIL')}</style><div id="_t"></div>

</body>
</html>
