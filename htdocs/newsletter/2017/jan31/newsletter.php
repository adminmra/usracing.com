<?php  //VARIABLES
	
$tag="Jan_31-big-game-weekend";
$message_preview="<Greeting/> It's Super Bowl Weekend and here's why you'll want to bet the game at BUSR.";	

$hero_image="https://www.usracing.com/newsletter/2017/jan31/hero.jpg";
$hero_image_alt="Get Your 20% Bonus for the Big Game";
$hero_title="Get Your 20% Bonus for the Big Game";
$hero_content="<Greeting/> <div align=left><p>Get your deposit in early for the Super Bowl this Sunday, Feb. 5, and take advantage of the BUSR Super Bowl Deposit Bonus Promotion. To qualify, all you have to do is:</p><br>
<p>Make you deposit before midnight (Eastern Time) on Thursday, enter the code promo code <b><font color=red>SB20</font></b> and you get a 20% deposit bonus! </p><br>
<p>Can't make a deposit  until Friday? That's OK. Enter the code <b><font color=red>SB15</font></b> and you’ll get a 15% deposit bonus.</p><br>

<p>If you wait till Saturday you can still get a 10% bonus just  enter the code <b><font color=red>SB10</font></b>. Then, on Sunday, you can sit back, place your wagers, enjoy a nice cold beer and enjoy the big game.</p><br>

<p align='center'><font size=1>(Roll-over rules apply, see Clubhouse for details.)</font></p></div>";

$button_link="https://www.betusracing.ag/login";
$button_content="Deposit and get my bonus now &rarr;";

//Article 1*************************************************

$blog1_title="The Surefire Super Bowl Bet No 'Expert' Will Play!";
$blog1_image_alt="Super Bowl";
$blog1_content="<div align=left><p>In gambling it is generally wise to ying when others yang and yang when others ying. In other words, be a contrarian.</p><br>

<p>Well, if there is one thing that most football bettors are in agreement on, it is that Super Bowl LI will be a high scoring affair.</p><br>

<p>At Covers.com, site visitors betting the \"over\" outnumber those betting the \"under\" by nearly a 3-1 margin. It's a little more balanced at VegasInsider.com, but the majority of players (57 percent) still believe that the two teams will score more than 59 combined points.</p><br>

<p>But there are some solid reasons to believe -- and, more importantly, bet -- the opposite:</p><br>

<p><b>1)</b> In 13 previous meetings, these two teams have <i>never</i> scored more than 59 combined points. Now, granted, such historical team match-ups aren't particularly meaningful -- after all, the first meeting took place in 1972 (five years before Tom Brady was born) -- but it is something to ponder. </p><br>

<p><b>2)</b> In Super Bowl games with an over/under mark of 50 points or greater, the \"under\" is 6-3.</p><br>

<p><b>3)</b> This is more subjective, but both Atlanta and New England have great ball-control offenses, especially the former. Against Green Bay in the NFC Championship, Atlanta had scoring drives of 6:30 (touchdown), 5:22 (field goal), 7:32 (touchdown) and 5:04 (touchdown). That's 24 points in nearly a half against a defense considerably inferior to the one the Falcons will face in Houston.</p></div>";

$blog1_link="https://www.betusracing.ag/login";
$blog1_link_text="Click HERE to see all the odds &rarr;";
$blog1_image="https://www.usracing.com/newsletter/2017/jan31/photo-1.jpg"; 

//Article 2*************************************************

$blog2_title="Two Great Super Bowl Prop Bets!";	
$blog2_image_alt="Prop Bets";
$blog2_content="<div align=left><p>According to experts, approximately $4.7 billion -- more than the Gross Domestic Product (GDP) of Fiji -- will be bet on this year's Super Bowl.</p><br>

<p>And each year an increasing amount of cash is staked on the various proposition, or prop, bets. Now, while most proposition betting makes about as much sense as subscribing to <i>Playboy</i> or <i>Playgirl</i> for the articles, there are a few props that serious Super Bowl bettors can sink their teeth into -- mainly, wagers on team and player totals.</p><br>

<p>This year, a couple come instantly to mind:</p><br>

<p>MOST PASS ATTEMPTS<br>
<b>Tom Brady (NE) +1 -130</b><br>
Matt Ryan (ATL) -1 Even</p><br>

<p>Comments: Based on statistical projections, Tom Brady figures to throw the ball more than Matt Ryan, especially since the Falcons have shown a desire and commitment to run the football, particularly in the playoffs. Atlanta averaged 26.3 rush attempts per game during the regular season and have averaged 29.5 rush attempts thus far in the playoffs.</p><br>

<p>TOM BRADY (NE) WILL THROW AN INTERCEPTION<br>
<b>Yes +130</b><br>
No -160</p><br>

<p>Comments: In his first four Super Bowl appearances, Tom Brady threw exactly one pick; in his last two, he's tossed three. In fact, since he turned 30, Brady has thrown 21 interceptions in 19 postseason games.</p><br></div>";

$blog2_link="https://www.betusracing.ag/login";
$blog2_link_text="Click HERE to see all the props &rarr;";
$blog2_image="https://www.usracing.com/newsletter/2017/jan31/photo-2.jpg";

?>	


<!DOCTYPE html>
<html lang="en">
<head>
<title></title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
          text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
          margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        td[class="mobile-hide"]{
          display:none;}

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          max-width: 100% !important;
          width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
            margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
            width:80% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

    }
    
    table td, table th { wordwrap:break-word;  }
table > tr > th { border-bottom: none; }
table td.right, table th.right { text-align:right; }
table td.center, table th.center, table td.num { text-align:center; }
.table.table-bordered > tr th:first-child, .table.table-bordered > tr td:first-child { border-left: none; }
.table-condensed thead > tr > th, .table-condensed tbody > tr > th, .table-condensed tfoot > tr > th, .table-condensed thead > tr > td, .table-condensed tbody > tr > td, .table-condensed tfoot > tr > td{ padding:5px 10px; font-size:12px;}
.table-bordered > tbody > tr th { background:#f4f4f4 !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;font-size:12px;}
.table-bordered > tbody > tr th:first-child { border-left:0; }
.table caption { background:#1571BA; padding:10px; font-size:20px; font-weight: 500; text-align:left; border:0; text-align:center; color:#fff;}
.dateUpdated {  font-size:12px; }
table{max-width:100%;background-color:transparent}th{text-align:left}.table{width:100%;margin-bottom:20px}.table tbody>tr>td,.table tbody>tr>th,.table tfoot>tr>td,.table tfoot>tr>th,.table thead>tr>td,.table thead>tr>th{padding:8px;line-height:1.428571429;vertical-align:top;border-top:1px solid #ddd}.table thead>tr>th{vertical-align:bottom;border-bottom:2px solid #ddd}.table caption+thead tr:first-child td,.table caption+thead tr:first-child th,.table colgroup+thead tr:first-child td,.table colgroup+thead tr:first-child th,.table thead:first-child tr:first-child td,.table thead:first-child tr:first-child th{border-top:0}.table tbody+tbody{border-top:2px solid #ddd}.table .table{background-color:#fff}.table-condensed tbody>tr>td,.table-condensed tbody>tr>th,.table-condensed tfoot>tr>td,.table-condensed tfoot>tr>th,.table-condensed thead>tr>td,.table-condensed thead>tr>th{padding:5px}.table-bordered,.table-bordered>tbody>tr>td,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>td,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border:1px solid #ddd}.table-bordered>thead>tr>td,.table-bordered>thead>tr>th{border-bottom-width:2px}.table-striped>tbody>tr:nth-child(odd)>td,.table-striped>tbody>tr:nth-child(odd)>th{background-color:#f9f9f9}.table-hover>tbody>tr:hover>td,.table-hover>tbody>tr:hover>th{background-color:#f5f5f5}table col[class*=col-]{float:none;display:table-column}table td[class*=col-],table th[class*=col-]{float:none;display:table-cell}.table>tbody>tr.active>td,.table>tbody>tr.active>th,.table>tbody>tr>td.active,.table>tbody>tr>th.active,.table>tfoot>tr.active>td,.table>tfoot>tr.active>th,.table>tfoot>tr>td.active,.table>tfoot>tr>th.active,.table>thead>tr.active>td,.table>thead>tr.active>th,.table>thead>tr>td.active,.table>thead>tr>th.active{background-color:#f5f5f5}.table>tbody>tr.success>td,.table>tbody>tr.success>th,.table>tbody>tr>td.success,.table>tbody>tr>th.success,.table>tfoot>tr.success>td,.table>tfoot>tr.success>th,.table>tfoot>tr>td.success,.table>tfoot>tr>th.success,.table>thead>tr.success>td,.table>thead>tr.success>th,.table>thead>tr>td.success,.table>thead>tr>th.success{background-color:#dff0d8;border-color:#d6e9c6}.table-hover>tbody>tr.success:hover>td,.table-hover>tbody>tr>td.success:hover,.table-hover>tbody>tr>th.success:hover{background-color:#d0e9c6;border-color:#c9e2b3}.table>tbody>tr.danger>td,.table>tbody>tr.danger>th,.table>tbody>tr>td.danger,.table>tbody>tr>th.danger,.table>tfoot>tr.danger>td,.table>tfoot>tr.danger>th,.table>tfoot>tr>td.danger,.table>tfoot>tr>th.danger,.table>thead>tr.danger>td,.table>thead>tr.danger>th,.table>thead>tr>td.danger,.table>thead>tr>th.danger{background-color:#f2dede;border-color:#eed3d7}.table-hover>tbody>tr.danger:hover>td,.table-hover>tbody>tr>td.danger:hover,.table-hover>tbody>tr>th.danger:hover{background-color:#ebcccc;border-color:#e6c1c7}.table>tbody>tr.warning>td,.table>tbody>tr.warning>th,.table>tbody>tr>td.warning,.table>tbody>tr>th.warning,.table>tfoot>tr.warning>td,.table>tfoot>tr.warning>th,.table>tfoot>tr>td.warning,.table>tfoot>tr>th.warning,.table>thead>tr.warning>td,.table>thead>tr.warning>th,.table>thead>tr>td.warning,.table>thead>tr>th.warning{background-color:#fcf8e3;border-color:#fbeed5}.table-hover>tbody>tr.warning:hover>td,.table-hover>tbody>tr>td.warning:hover,.table-hover>tbody>tr>th.warning:hover{background-color:#faf2cc;border-color:#f8e5be}@media (max-width:768px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;overflow-x:scroll;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0;background-color:#fff}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th,.table-responsive>.table-bordered>thead>tr:last-child>td,.table-responsive>.table-bordered>thead>tr:last-child>th{border-bottom:0}}
</style>
</head>
<body style="margin: 0; padding: 0;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#003158">
            <!-- HIDDEN PREHEADER TEXT -->
            <div style="display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
             
             <?php echo($message_preview);?>
             
   </div>
            <div align="center" style="padding: 0px 15px 0px 15px;">
                <table border="0" cellpadding="0" cellspacing="0" width="500" class="wrapper">
                    <!-- LOGO/PREHEADER TEXT -->
                    <tr>
                        <td style="padding: 20px 0px 30px 0px;" class="logo">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td bgcolor="#003158" width="100" align="left">
                                        <a href="https://www.usracing.com/?nl_logo=<?php echo $tag;?>" target="_blank">
                                           
                                         
                                            <img alt="US Racing" src="https://www.usracing.com/img/usracing.png" width="200" height="60" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                                        </a>
                                    </td>
                                    <td bgcolor="#003158" width="400" align="right" class="mobile-hide">
                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="right" style="padding: 0 0 5px 0; font-size: 14px; font-family: Arial, sans-serif; color: #666666; text-decoration: none;"><span style="color: #FFFFFF; text-decoration: none;">1-844-US-RACING</span></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
</table>

<!-- ONE COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <!-- HERO IMAGE -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              	<td class="padding-copy">
                                    <a href="<?php echo $button_link."?nl-hero=".$tag;?>" target="_blank">
 <img 
 
 src="<?php echo $hero_image;?>" width="500" height="400" border="0" 
 
 alt="<?php echo $hero_image_alt;?>" 
 
 style="display: block; color: #666666;  font-family: Helvetica, arial, sans-serif; font-size: 16px;" class="img-max">

 
 
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;" class="padding-copy"><?php echo $hero_title;?></td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy">	<OpenTracking/>
	<p>     <Greeting/></p><?php echo $hero_content;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <!-- BULLETPROOF BUTTON -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center" style="padding: 25px 0 0 0;" class="padding-copy">
                                                <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                                    <tr>
                                                        <td align="center"><a href="<?php echo $button_link."?nl-btn=".$tag;?>" target="_blank" style="font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; background-color: #c71f24; border-top: 15px solid #c71f24; border-bottom: 15px solid #c71f24; border-left: 35px solid #c71f24; border-right: 35px solid #c71f24; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; display: inline-block;" class="mobile-button"><?php echo $button_content;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<!-- TWO COLUMN SECTION -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td bgcolor="#F5F7FA" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
                <tr>
                    <td>
                        <!-- TITLE SECTION AND COPY -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" style="font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333;" class="padding-copy">Expert Opinions on the Big Game</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!-- TWO COLUMNS -->
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top" style="padding: 0;" class="mobile-wrapper">
                                    <!-- LEFT COLUMN -->
                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" align="left" class="responsive-table">
                                        <tr>
                                            <td style="padding: 20px 0 40px 0;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="center" bgcolor="#F5F7FA" valign="middle"><a href="<?php echo $blog1_link."?nl=".$tag;?>" target="_blank"><img src="<?php echo $blog1_image;?>" width="240" height="180" style="display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 180px;" alt="<?php echo $blog1_image_alt;?>" border="0" class="img-max"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><?php echo $blog1_title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><span class="appleBody"><?php echo $blog1_content; ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><a href="<?php echo $blog1_link."?nl=".$tag;?>" style="color: #256F9C; text-decoration: none;"><?php echo $blog1_link_text;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- RIGHT COLUMN -->
                                    <table cellpadding="0" cellspacing="0" border="0" width="47%" align="right" class="responsive-table">
                                        <tr>
                                            <td style="padding: 20px 0 40px 0;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                    <tr>
                                                        <td align="center" bgcolor="#F5F7FA" valign="middle"><a href="<?php echo $blog2_link."?nl=".$tag;?>" target="_blank"><img src="<?php echo $blog2_image;?>" width="240" height="180" style="display: block; color: #666666; font-family: Helvetica, arial, sans-serif; font-size: 13px; width: 240px; height: 180px;" alt="<?php echo $blog2_image_alt;?>" border="0" class="img-max"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 15px 0 0 0; font-family: Arial, sans-serif; color: #333333; font-size: 20px;" bgcolor="#F5F7FA"><?php echo $blog2_title;?></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><span class="appleBody"><?php echo $blog2_content; ?></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" style="padding: 5px 0 0 0; font-family: Arial, sans-serif; color: #666666; font-size: 14px; line-height: 20px;" bgcolor="#F5F7FA"><a href="<?php echo $blog2_link."?nl=".$tag;?>" style="color: #256F9C; text-decoration: none;"><?php echo $blog2_link_text;?></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
 
</td>
                            </tr>
                            <tr>
                                <td style="padding:0 0 45px 25px;" align="left" class="padding">
                                    <table border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                                        <tr>
                                            <td align="center">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                            </table>
        </td>
    </tr>
</table>

<!-- FOOTER -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            
                            <tr>
                                <td align="center" style="padding: 20px 0 20px 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy"> <p>Good luck this weekend! - Tom Franklin</td>  </tr> 
 
<?php /*<tr>
<td style="padding: 10px 0 20px 0; font-size: 10px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;" class="padding-copy" align="center">You have been emailed this letter because you have an account at USRacing.com<br /> To unsubscribe from these emails, click <span style="text-decoration: underline;"><a href="%BASIC:UNSUBLINK%" class="original-only" style="color: #666666; text-decoration: underline;">Unsubscribe</a></span> or reply with UNSUBSCRIBE in the subject or body.</td>
</tr>*/ ?>
</tbody>
</table>

<!-- ANALYTICS-->
<style data-ignore-inlining>@media print{ #_t { background-image: url('https://x1ntfixz.emltrk.com/x1ntfixz?p&d=$Subscriber.EMAIL');}} div.OutlookMessageHeader {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} table.moz-email-headers-table {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} blockquote #_t {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')} #MailContainerBody #_t {background-image:url('https://x1ntfixz.emltrk.com/x1ntfixz?f&d=$Subscriber.EMAIL')}</style><div id="_t"></div>
<img src="https://x1ntfixz.emltrk.com/x1ntfixz?d=$Subscriber.EMAIL" width="1" height="1" border="0" />
</body>
</html>
