<html>
<head>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/additional-methods.js" ></script>
    <meta charset="utf-8"/>
    <link rel='stylesheet' id='OswaldFont-css'  href='//fonts.googleapis.com/css?family=Oswald:400,700,300' type='text/css' media='all' />
<link rel='stylesheet' id='AwesomeFont-css'  href='//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' type='text/css' media='all' />
    <style>


        @import url(https://fonts.googleapis.com/css?family=Roboto);.overlay-text,button[disabled],html input[disabled]{cursor:default}legend,td,th{padding:0}.home-Wrapper,body.home .homeWrapper,body.home .marketing{max-width:1600px!important}#footer-nav ul li,#footer-nav ul li ul a,#share-print .my-btn-new,.big_btn,.caption .my-btn-new,.category-title,.contact-high,.createButton,.featuredButton,.more,.navbar-nav>li,.overlay-text h2,.page_item,.skip-span .skip-button,.title,.ui-widget-header{text-transform:uppercase}#footer-nav ul,.ui-helper-clearfix:after,.wp-pagenavi{clear:both}html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,optgroup,strong{font-weight:700}dfn{font-style:italic}h1{font-size:2em;margin: 1.67em 0;}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{box-sizing:content-box;height:0}pre,textarea{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}.title,.ui-widget-header,a{font-weight:700}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0}table{border-collapse:collapse;border-spacing:0}.ui-dialog .ui-dialog-title{float:left;margin:.1em 0;white-space:nowrap;width:90%;overflow:hidden;text-overflow:ellipsis}.ui-widget-content a{color:#333}.ui-dialog .ui-dialog-titlebar-close{position:absolute;margin:-10px 0 0;padding:1px}.ui-dialog .ui-dialog-content{position:relative;border:0;padding:.5em 1em;background:0 0;overflow:auto}.ui-widget-overlay{position:fixed;top:0;left:0;width:100%;height:100%;background:#000;opacity:.8}.ui-widget{font-family:Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;font-size:1.1em}.ui-front{z-index:100}.ui-dialog{overflow:hidden;position:absolute;left:0;padding:.2em;outline:0;z-index:99999}.ui-widget-header{border:1px solid transparent;background:#FFF;color:#66666E}.navbar-fixed-bottom,.navbar-fixed-top{z-index:100}.ui-dialog .ui-dialog-titlebar-close{right:0;top:2%;width:25px!important;height:25px;border-radius:100%;border:none;background-color:#ADADAD;z-index:2}.ui-widget button,.ui-widget input,.ui-widget select,.ui-widget textarea{font-family:Trebuchet MS,Tahoma,Verdana,Arial,sans-serif;font-size:1em}.ui-dialog .ui-dialog-titlebar-close:before{content:"X";color:#FFF;font-family:monospace;font-weight:700;position:absolute;top:3px;right:33%}.ui-helper-clearfix:after,.ui-helper-clearfix:before{content:"";display:table;border-collapse:collapse}.ui-widget-content{border:1px solid #ddd;background:#f1f1f1}.ui-state-default .ui-icon,span.ui-accordion-header-icon.ui-icon.ui-icon-triangle-1-e{background-image:none}.ui-corner-all,.ui-corner-bottom,.ui-corner-br,.ui-corner-right{border-bottom-right-radius:4px}.ui-corner-all,.ui-corner-bl,.ui-corner-bottom,.ui-corner-left{border-bottom-left-radius:4px}.ui-corner-all,.ui-corner-right,.ui-corner-top,.ui-corner-tr{border-top-right-radius:4px}.ui-corner-all,.ui-corner-left,.ui-corner-tl,.ui-corner-top{border-top-left-radius:4px}h3.ui-accordion-header.ui-state-default.ui-corner-all.ui-accordion-icons{border-left:1px solid #ccc;border-right:1px solid #ccc}@-webkit-keyframes fade-in{from{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0)}to{opacity:1;-webkit-transform:none;transform:none}}@keyframes fade-in{from{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0)}to{opacity:1;-webkit-transform:none;transform:none}}@-webkit-keyframes fade-out{0%{opacity:1;-webkit-transform:none;transform:none}100%{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0)}}@keyframes fade-out{0%{opacity:1;-webkit-transform:none;transform:none}100%{opacity:0;-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0)}}@-webkit-keyframes animate{0%,100%,20%,50%,80%{-webkit-transform:translateY(0)}40%{-webkit-transform:translateY(-30px)}60%{-webkit-transform:translateY(-15px)}}@keyframes animate{0%,100%,20%,50%,80%{-webkit-transform:translateY(0);transform:translateY(0)}40%{-webkit-transform:translateY(-30px);transform:translateY(-30px)}60%{-webkit-transform:translateY(-15px);transform:translateY(-15px)}}@-webkit-keyframes shadow{0%,100%{-webkit-transform:scale(1,1);transform:scale(1,1)}50%{-webkit-transform:scale(1.5,1);transform:scale(1.5,1)}}@keyframes shadow{0%,100%{-webkit-transform:scale(1,1);transform:scale(1,1)}50%{-webkit-transform:scale(1.5,1);transform:scale(1.5,1)}}@font-face{font-family:Oswald,sans-serif;font-style:normal;font-weight:400}body{color:#000;background:#FFF}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{font-family:Oswald,sans-serif!important;color:#757575}.mainSectionsTitle,.page_item{font-family:Oswald,sans-serif;float:left}a{color:#357DB0;text-decoration:none;-webkit-transition:all .3s;transition:all .3s}a:focus,a:hover{color:#0095FF;text-decoration:none!important}.h2,h2{font-size:27px!important}input:-webkit-autofill{-webkit-box-shadow:0 0 0 1000px #f4f3f5 inset}body.page:before{content:"";position:fixed;left:0;right:0;z-index:-1;display:block;background-size:cover;width:100%;height:100%}.home-Wrapper{margin:0 auto!important}.navbar.navbar-default.transparentBackground,body.page .navbar-default{background-color:transparent}strong{color:#000}.homeWrapper,.marketing{margin:90px auto 0!important;max-width:1300px}.message{text-align:center;padding:5% 0 10%;margin:0 auto!important;float:left;width:100%}.message .form-group{margin:0}.page_item{margin:0;list-style:none;position:relative;display:block}.my-btn-new .joinHover,.my-btn-new:hover .openText,.white_content{display:none}.page_item a{color:#333;text-decoration:none;font-weight:400}.current_page_item{color:#999}.current_page_item a:hover{color:#fff}.mainSectionsTitle{text-decoration:none;color:#545454;font-size:31px;width:100%;margin:0;background:inherit;text-align:left;padding:0;-webkit-transition:all .3s;transition:all .3s}a.linksFeatures{color:#666;font-weight:400}.badge{padding:1px 3px}.featured{margin-bottom:10px;padding:0;border-radius:2px;margin-left:0;margin-right:0;padding-left:0!important;padding-right:0!important}.pb-feat{position:absolute;max-width:154px;text-align:center;font-size:16px;-webkit-transition:all .5s ease 0s;transition:all .5s ease 0s;left:10px;bottom:8px;height:39px;margin:0 0 3px;border-radius:5px;border:1px solid #449125;color:#666}.mainSectionsTitle:before{content:">";position:relative;margin-right:3px;top:-2px}.img-100{width:100%}#main-content{background:rgba(0,0,0,.8)}#contentBody h4{color:#000!important;font-family:Oswald,sans-serif!important;font-size:15px!important;text-align:center!important;margin:3px!important}.container{width:100%!important;margin:0!important;padding:0!important}#content{background-color:#fff;opacity:.96;width:100%}#share{float:right;width:auto;padding:5px 0}#share-print{width:auto;top:-36px;margin-right:34px}#share-print .my-btn-new{float:right;width:auto;margin:5px;text-align:right;padding:0 10px;height:auto;box-shadow:none;font-size:12px;border:none;color!important:#949799;background:#9E9E9E!important;border-radius:0;transition:all .3s;-moz-transition:all .3s;-webkit-transition:all .3s}.col_middle2,.col_middle2 h3,.iconBar{float:left;width:100%}.iconBar{padding:10px;margin-bottom:5px;background:#191919;border:1px solid #444}.iconBar img{float:left}.iconBar h3{margin:0;color:#ccc;font-size:11px;font-weight:700;padding:0;text-align:left}.iconBar p{margin:0;color:#ccc;font-size:10px;font-weight:400}.black-border{border:1px solid #000;border-radius:5px}p.seo-links,p.seo-links a{color:#ccc}.actionSlider{margin-top:24px}h5.sponsors{color:#ccc;font-family:Oswald,sans-serif}.bg-custom p,.seo-text{font-family:sans-serif}.img-betting{margin-bottom:5px}.border-row{padding:10px 0;border-bottom:solid 1px #a30327;border-top:solid 1px #a30327;margin-top:10px}.bg-info{background:#000!important}.seo-text{color:#999898}.seo-text h4{font-size:12px;color:#fff}.seo-text p{font-size:11px}.bg-custom{color:#000;padding:0 10px;background:inherit}.bg-custom h3 a,.bg-custom h5 a{color:#a30327}.bg-custom p{font-size:14px}ul.margin-left{margin-left:15px}.col_middle2{font-size:12px;color:#252525;padding:20px;background:rgba(255,255,255,.9);height:auto;text-align:left;margin-bottom:0;margin-left:0;border-radius:0}.col_middle2 h3{margin:0 0 20px;color:inherit;font-size:24px;border-bottom:solid 1px;padding-bottom:6px;height:auto;padding-top:0}.col_middle2 .step1{margin:18px}.col_middle2 .genderStyle{color:#f1f1f1;margin:0 0 0 17px;font-weight:700}.col_middle2 .fnameStyle{color:#f1f1f1;margin:0 0 0 -64px;font-weight:700}.col_middle2 .lnameStyle{color:#f1f1f1;margin:0 0 0 -67px;font-weight:700}.col_middle2 .emailStyle{color:#f1f1f1;margin:0 0 0 -99px;font-weight:700}.col_middle2 .dateStyle{color:#f1f1f1;margin:0 0 0 -48px;font-weight:700}.col_middle2 select{width:47px;font-size:11px;height:20px}.col_middle2 select option{font-size:11px}.col_middle2 .rr{margin:10px 0 5px -25px}.col_middle2 .email,.col_middle2 .fname,.col_middle2 .lname{margin:5px 0 5px -3px;width:135px}.col_middle2 .dateSelect{margin:5px 0 5px -1px;width:43px}.col_middle2 input{margin:5px 0}.col_middle2 .sexIndi{top:-3px;position:relative;font-weight:700}.col_middle2 .validEmail{width:140px;font-size:9px!important;text-align:left;margin:0 0 10px 18px;color:#959595}.col_middle2 .passwordStyle{color:#f1f1f1;margin:0 0 0 -26px;font-weight:700}.col_middle2 .password2Style{color:#f1f1f1;margin:0 0 0 -13px;font-weight:700}.col_middle2 .maidenNameStyle{color:#f1f1f1;margin:0 0 0 -19px;font-weight:700}.col_middle2 .securityStyle{color:#f1f1f1;margin:0 0 0 -26px;font-weight:700}.col_middle2 .password,.col_middle2 .password2{margin:5px 0 5px -3px;width:135px;font-weight:700}.col_middle2 .maidenName{margin:5px 0 5px -3px;width:140px;font-size:9px}.col_middle2 .securityanswer{margin:5px 0 5px -3px;width:130px;font-weight:700}.col_middle2 .streetStyle{color:#f1f1f1;margin:0 0 0 -39px;font-weight:700}.col_middle2 .countryNameStyle{color:#f1f1f1;margin:0 0 0 -84px;font-weight:700}.col_middle2 .cityNameStyle{color:#f1f1f1;margin:0 0 0 -110px;font-weight:700}.col_middle2 .stateNameStyle{color:#f1f1f1;margin:0 0 0 -102px;font-weight:700}.col_middle2 .zipStyle{color:#f1f1f1;margin:0 0 0 -119px;font-weight:700}.col_middle2 .contactStyle{color:#f1f1f1;margin:0 0 0 -45px;font-weight:700}.col_middle2 .hearAbout,.col_middle2 .promoCode{color:#f1f1f1;margin:0;width:155px;font-weight:700}.col_middle2 .street{margin:5px 0 0 -3px;width:135px}.col_middle2 .cityName,.col_middle2 .countryName,.col_middle2 .stateName{margin:5px 0 0 -4px;width:135px}.col_middle2 .zip{margin:5px 0 0 -3px;width:135px}.col_middle2 .contactName{height:19px;margin:11px 19px 0 -6px}.col_middle2 .contactNum{margin:11px 16px 16px -34px;width:80px;float:right}.col_right p{color:#fff}.col_right hr{margin:20px 0;width:168px}.col_left hr{margin:30px 0;width:168px}.col_center{float:left;margin:0 0 13px}.col_center .actual{border-bottom:#d71920 solid 5px}.col_center h4{float:left;color:#f1f1f1;font-size:15px}.col_center img{float:left;width:20%;margin:3px 10px 0 5px}.skip-span{padding:10px;width:100%;background:inherit;border-radius:0;float:left}.skip-span .skip-button{background:#737373;color:#fff;font-size:12px;padding:9px;width:auto;text-decoration:none;float:right;margin:20px 0 0;position:relative;cursor:pointer}.contact-high,.number{font-size:21px;font-weight:700}.skip-span .skip-button:hover{background:#555}.moreInfo{float:left;margin:0 5px 10px 0}.acountEmail{font-weight:700}.contact-high{color:#f1f1f1}.contact-high span{color:#FFFC00}.number{color:#c00;float:right;width:33%;height:auto;text-align:right;margin:-60px 0 0}.number span{color:#efc900}.title{font-size:23px}.flex-control-nav{margin:40px 0!important}#textwidgetLatestSports li{padding:0 10px 10px!important;margin:0 4px 10px 0;float:left;position:relative;border-bottom:1px solid #006eac;width:100%;background:0 0;color:#000}#textwidgetLatestSports h3{font-size:16px}#textwidgetLatestSports p{font-size:11px;color:#333;margin-bottom:20px}#textwidgetLatestSports a.read-more{color:#fff;padding:4px;position:absolute;right:0;bottom:0}#textwidgetLatestSports a.read-more:hover{color:#fff;text-decoration:none}#textwidgetLatestSports .post-thumbnail{width:100%;height:auto;min-width:83px;max-width:82px;max-height:none;float:left;margin:0 10px 0 0;box-shadow:none;border:1px solid #202020}#textwidgetLatestSports h5{font-family:Oswald,sans-serif;font-size:18px}#textwidgetLatestSports h5 a{color:#337ab7!important}#breadcrumbs{padding:5px 10px 12px;color:#333;width:100%;top:130px;font-weight:400;z-index:9}#breadcrumbs a{color:#999;font-weight:400}.category-title{padding-bottom:0;margin-top:10px;margin-bottom:10px}.page-content{float:left;width:70%}.page-content-full{margin:0 auto;width:90%}.entry{font-size:13px;margin-bottom:20px;padding-right:20px}.entry img{width:auto;height:auto}#errormsg{padding:15px!important;line-height:1.3em;text-align:left;background:#8B0000!important;font-size:13px;border:2px solid #b00000;color:#fff!important;font-weight:700}.parent-pageid-135 .entry{padding:0!important}.page-id-135 .bg-custom,.parent-pageid-135 .bg-custom{background:inherit}.imgContainer{height:auto;overflow:hidden;max-height:205px;text-align:center}.createButton{color:#fff;margin:24px 0;border:none;font-size:17px;width:100%;height:56px;text-align:center;float:left;-webkit-border-radius:0;padding:15px 0;font-weight:700;border-radius:2px;background-image:-webkit-linear-gradient(#66aa1e,#459418)!important;background-image:linear-gradient(#66aa1e,#459418)!important;box-shadow:0 2px 0 0 #34620d}#searchform input[type=submit],.sportsContent h2{font-family:Oswald,sans-serif;font-weight:400}.createButton:hover{background-image:-webkit-linear-gradient(#459418,#459418)!important;background-image:linear-gradient(#459418,#459418)!important;box-shadow:0 2px 0 0 #34620d;color:#f1f1f1!important;text-decoration:none!important}.promoAd h4,.promoAd p.caption{background:rgba(0,0,0,.7);text-indent:10px;width:100%;padding:10px;position:absolute}.promoAd h2{color:#f1f1f1!important;font-family:Oswald,sans-serif;border-bottom:1px solid #888;padding-bottom:10px;float:left;margin-top:0;width:100%}.promoAd h4{top:59px;color:#ff8400;text-align:left;font-size:22px}.promoAd p.caption{bottom:28px}.promoAd a.promoAddbutton{background:#a30327;padding:5px;color:#f1f1f1;position:absolute;bottom:0;right:0;margin:10px 0;text-decoration:none}.promoAd a.promoAddbutton:hover{background:#da1743}#searchform input{color:#555;margin:0 0 10px;width:75%;background-color:#f4f3f5;border-top-color:rgba(0,0,0,.1);border-bottom-color:rgba(0,0,0,.1);border-style:outset;border-right-color:rgba(0,0,0,.1);border-left-color:rgba(0,0,0,.1);border-width:1px;padding:12.5px;border-top-left-radius:5px;border-bottom-left-radius:5px}#searchform input[type=submit]{color:#fff;text-transform:uppercase;float:right;margin:0;padding:10px 0;background-color:#006eac;width:25%;border-radius:0 5px 5px 0}#searchform input[type=submit]:hover{background:#286080}#commentform input[type=submit]{color:#555}.sportsContent{width:100%;text-align:justify;float:left;margin:15px 0 0;padding:0 20px 20px}.sportsContent h2{border-bottom:1px solid #444;float:left;width:100%;margin-top:0;padding-bottom:5px;margin-bottom:10px}.overlay-text p{color:#c9c9c9}.overlay-text{position:absolute;border:1px solid #888;top:22px;left:0;background:rgba(0,0,0,.82);color:#fff;padding:20px;transition:all .3s;-moz-transition:all .3s;-webkit-transition:all .3s;width:100%;bottom:0;opacity:0}.overlay-text h2{font-family:oswald;margin:0 0 -15px;padding:0}.spanInfo{margin-top:10px}.captionList{left:0!important;margin-top:75px}.more-info{font-size:16px;margin-top:0;text-align:center;float:right;width:128px;background:#8fc800;background:-webkit-gradient(radial,center center,0,center center,100%,color-stop(0,#8fc800),color-stop(100%,#329900));background:-webkit-radial-gradient(center,ellipse,#8fc800 0,#329900 100%);background:radial-gradient(ellipse at center,#8fc800 0,#329900 100%);filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='$green-l', endColorstr='$green-m', GradientType=1 )}.overlay-text.fade-in{-webkit-animation:fade-in .5s;opacity:1}.overlay-text.fade-out{-webkit-animation:fade-out .3s}.darkBackground{background-color:rgba(0,0,0,.6)!important}.darkBackground a{color:#fff!important}.black_overlay{display:none;position:absolute;top:-72%;left:-31%;width:163%;height:219%;background-color:#000;z-index:1001;-moz-opacity:.9;opacity:.9;filter:alpha(opacity=90)}.white_content{position:absolute;top:10%;left:10%;width:80%;height:auto;color:#010101;padding:16px;border:16px solid #555;background-color:#fff;z-index:1002;overflow:auto}.widget li a,.widget-home li a{border-top:solid 1px #222;width:100%;text-decoration:none}.white_content a{color:#555;font-family:Oswald,sans-serif;font-size:24px;position:absolute;text-decoration:none;top:0;padding:1px 10px;right:0}.white_content a:hover{color:#000}.widget{background:inherit;padding:5px 0;margin:0;float:left;width:100%}.widget h3{margin:0 0 10px;font-size:23px;font-weight:lighter}.textwidget img{width:100%}.widget ul,ol{padding:0}.widget-home li a{padding:2px;background:#202020;margin-left:-5px;font-size:11px;font-weight:100;margin-top:5px}.widget_archive,.widget_categories{background-color:#1a1a1a;padding:11px;border-bottom:solid 5px #006eac;margin-bottom:5px}.widget-home li a:hover{background:0 0;color:#d1d1d1}.widget li{list-style-type:none}.widget li a{background:0 0;float:left;margin-top:0;font-size:12px;padding:5px;position:relative;color:#fff;font-weight:400}.widget li a:hover{background:#202020;color:#d1d1d1}div#bonus_code_form{width:100%;float:left}.linesTitle{text-align:left;font-size:16px!important}body.page-template-landing-page{background-color:#0f0f0f}body.page-template-landing-page:before{background-image:none!important}#register form.form-signin{background:0 0}#register form.form-signin h3{color:#000;text-align:center;padding:0;margin:0}.ui-dialog{top:0!important;overflow-y:scroll!important;-webkit-overflow-scrolling:touch}.alert-danger{margin:0!important;font-size:13px!important;padding:6px!important;text-align:left!important}div#football-loader-container{float:left;width:100%;height:100%;z-index:9990;position:fixed;top:0;background-color:rgba(0,0,0,.7)}.btn{border-radius:3px;box-shadow:rgba(0,0,0,.22) 0 2px 0}.btn-grey,.my-btn-grey{-webkit-filter:none;font-weight:400;font-family:Oswald,sans-serif;box-shadow:0 2px 0 0 #6b6c6b;color:#fff}.btn-primary{border-color:#F80206;margin:5px 0;text-shadow:1px 1px 1px rgba(47,0,1,.78);font-family:Oswald,sans-serif}.btn-success{background-color:#4BA500!important}.my-btn-grey{border-radius:2px;border:0;filter:none;width:90%;margin-left:4%;margin-right:5%;padding:6px;font-size:1.2em;background-image:-webkit-linear-gradient(grey,#9b9b9b)!important;background-image:linear-gradient(grey,#9b9b9b)!important}.btn-green,.btn-grey{margin:0;text-shadow:1px 1px 1px #000}.my-btn-grey:hover{background-image:-webkit-linear-gradient(#9b9b9b,#9b9b9b)!important;background-image:linear-gradient(#9b9b9b,#9b9b9b)!important}.btn-grey{border-radius:5px;border:0;filter:none;padding:6px 31px;font-size:19px!important;background-image:-webkit-linear-gradient(grey,#9b9b9b)!important;background-image:linear-gradient(grey,#9b9b9b)!important}.btn-green,.my-btn-new{border-radius:2px;-webkit-filter:none}.btn-grey:hover{background-image:-webkit-linear-gradient(#9b9b9b,#9b9b9b)!important;background-image:linear-gradient(#9b9b9b,#9b9b9b)!important}.btn-green{border:0;filter:none;padding:10px 20px;font-size:1em;font-weight:400;font-family:Oswald,sans-serif;background-image:-webkit-linear-gradient(#66aa1e,#459418)!important;background-image:linear-gradient(#66aa1e,#459418)!important;box-shadow:0 2px 0 0 #34620d;color:#fff}.joinHere,.my-btn-new{margin:5px 4px 5px 0;font-weight:400;font-family:Oswald,sans-serif;box-shadow:0 2px 0 0 #34620d;color:#fff}.btn-green:hover{background-image:-webkit-linear-gradient(#459418,#459418)!important;background-image:linear-gradient(#459418,#459418)!important;color:#fff}.my-btn-new{border:0;filter:none;width:100%;padding:6px;font-size:1.2em;background-image:-webkit-linear-gradient(#66aa1e,#459418);background-image:linear-gradient(#66aa1e,#459418)}.my-btn-new:hover{color:#fff}.my-btn-new:hover .joinHover{display:inline}.featuredButton{position:absolute;right:5px;bottom:5px;max-width:176px;text-shadow:#000 0 2px 4px;text-align:center;font-size:18px;-webkit-transition:all .5s ease;transition:all .5s ease}.featuredButton span{-webkit-transition:all .5s ease;transition:all .5s ease}.bonuModalContainer a.my-btn-new{position:absolute;right:0;top:0;max-width:100px;color:#fff;font-size:20px;padding:5px;text-align:center}.joinHere{font-size:20px;z-index:9000;max-width:90px;min-width:auto;width:100px;float:right;cursor:pointer;border-radius:2px;border:0;-webkit-filter:none;filter:none;padding:6px;background:#66aa1e;background:-webkit-gradient(linear,left top,left bottom,color-stop(1%,#66aa1e),color-stop(100%,#459418));background:-webkit-linear-gradient(top,#66aa1e 1%,#459418 100%);background:linear-gradient(to bottom,#66aa1e 1%,#459418 100%);filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#66aa1e', endColorstr='#459418', GradientType=0 )}.joinHere:hover{color:#fff;background:#459418;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#459418),color-stop(99%,#66aa1e));background:-webkit-linear-gradient(top,#459418 0,#66aa1e 99%);background:linear-gradient(to bottom,#459418 0,#66aa1e 99%);filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#459418', endColorstr='#66aa1e', GradientType=0 );box-shadow:0 2px 0 0 #34620d}.signup,.signup:hover{box-shadow:0 2px 0 0 #666}.more{background:#05367D!important;border-color:#0C59FF!important;margin:0;font-size:13px}.more:hover{background:#000!important;border-color:#0051FF!important}.signup{background:#9E9E9E}.signup:hover{background:#868686}.brand button.btn-success{background:#45484d!important}#ctl00_WagerContent_rpMsg_ctl02_btn_Continue{background:#a30000;border:none;padding:5px 10px}#ctl00_WagerContent_rpMsg_ctl02_btn_Continue:hover{background:#bc0202}.upperButton button{margin-bottom:5px}.carousel,.return_btn{margin-bottom:10px}.caption .my-btn-new{padding:17px 24px;float:none;font-size:28px;color:#fff;width:100%;z-index:9999;position:relative;font-weight:500;font-family:Oswald;left:30px}.big_btn,.return_btn a{font-family:Oswald,sans-serif;font-weight:400}.caption .my-btn-new:hover{text-decoration:none;color:#fff}.my-btn-new:hover>span{text-shadow:#fff 0 0 10px!important}.return_btn a{font-size:13px;color:#7a7a7a}.return_btn .glyphicon.glyphicon-share-alt{margin-left:10px;font-size:18px;color:#7a7a7a}.big_btn{min-width:200px;padding:15px 25px;display:inline-block;font-size:20px;letter-spacing:1px;color:#fff;line-height:1;text-align:center;text-decoration:none;white-space:nowrap;cursor:pointer;border-radius:2px;-webkit-text-rendering:optimizeLegibility;text-rendering:optimizeLegibility;-webkit-font-smoothing:antialiased;font-smoothing:antialiased;user-select:none;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;-webkit-appearance:none;-moz-appearance:none;appearance:none}.green_b_btn{box-shadow:0 3px 0 0 #34620d;background-image:-webkit-linear-gradient(#66aa1e,#459418);background-image:linear-gradient(#66aa1e,#459418)}.green_b_btn:hover{background-image:-webkit-linear-gradient(#34620d,#459418);background-image:linear-gradient(#34620d,#459418);color:#fff}.gray_b_btn{box-shadow:0 2px 0 0 #6b6c6b;background-image:-webkit-linear-gradient(grey,#9b9b9b);background-image:linear-gradient(grey,#9b9b9b)}.hvr-underline-from-center,.hvr-underline-from-right{box-shadow:0 0 1px transparent;-moz-osx-font-smoothing:grayscale;vertical-align:middle;overflow:hidden}.gray_b_btn:hover{background-image:-webkit-linear-gradient(#9b9b9b,#9b9b9b);background-image:linear-gradient(#9b9b9b,#9b9b9b);color:#fff}.joinNow-pbtn{width:70%;font-size:26px;margin:20px 15%}.wp-pagenavi a,.wp-pagenavi span{text-decoration:none;border:1px solid #BFBFBF;padding:3px 5px;margin:2px;font-weight:400}.wp-pagenavi a:hover,.wp-pagenavi span.current{border-color:#000}.wp-pagenavi span.current{font-weight:400}.boxLoading{color:#fff;width:50px;height:50px;margin:auto;position:absolute;left:0;right:0;top:0;bottom:0;z-index:9991}.boxLoading:before{content:'';width:71px;height:5px;color:#fff;background:#fff;opacity:.5;position:absolute;top:84px;left:-12px;right:0;text-align:center;-webkit-animation:shadow 1.5s linear infinite;animation:shadow 1.5s linear infinite;z-index:9993}.boxLoading:after{content:'';width:50px;height:50px;background:url(/wp-content/themes/MyBookie/img/myb-loading.png);background-size:50px 50px;-webkit-animation:animate 1.5s linear infinite;animation:animate 1.5s linear infinite;position:absolute;top:0;left:0;border-radius:3px}.boxLoading span{float:left;bottom:-50px;position:relative;color:#fff;text-align:center;left:-22px;font-size:26px;font-family:Oswald}.row{margin-right:0!important;margin-left:0!important}.brand{z-index:9}.header-logo{display:block;width:100%;background:url(../img/logo.png?v=0.1) center left no-repeat;text-indent:100%;white-space:nowrap;background-size:contain;max-width:280px;margin:5px}.rowHeader{background:#000;position:relative}#join-form{float:left;width:100%}.affix,.affix-top,.navbar-collapse.in>.navbar-nav>li{width:100%!important}#join-form div{margin:5px 0}.phone-top .fa-phone{color:#7BD41F!important}.hvr-underline-from-center{display:inline-block;-webkit-transform:translateZ(0);transform:translateZ(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;position:relative}.hvr-underline-from-center:before{content:"";position:absolute;z-index:-1;left:50%;right:50%;bottom:0;background:#2098d1;height:4px;-webkit-transition-property:left,right;transition-property:left,right;-webkit-transition-duration:.3s;transition-duration:.3s;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}.hvr-underline-from-center:active:before,.hvr-underline-from-center:focus:before,.hvr-underline-from-center:hover:before{left:0;right:0}.hvr-underline-from-right{display:inline-block;-webkit-transform:translateZ(0);transform:translateZ(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;position:relative}.phone-top{color:#fff;font-size:1.2em;padding:10px;font-weight:400}.menu-main-menu-container{color:#777!important}.navbar{position:relative;min-height:34px;margin-bottom:0;border:1px solid transparent}.affix-top{background-color:transparent}.affix{background-color:rgba(0,0,0,.6)!important}.affix>div a{color:#fff!important}.navbar-nav>li>a{padding-top:5px;padding-bottom:5px;padding-left:10px;font-weight:400}.navbar-nav>li{float:left}.navbar-default{margin:0!important;padding:0;border:none!important;border-radius:0!important;background-color:transparent}.navbar-collapse.in>.navbar-nav>li>a{font-weight:500!important;padding:10px;font-size:15px!important;text-align:left}ul.nav.navbar-nav{font-family:Oswald,sans-serif}.btn-primary,.navbar-default .navbar-nav>.active>a,.navbar-default .navbar-nav>.active>a:focus,.navbar-default .navbar-nav>.active>a:hover{color:#FFF;background:#c00}.page-template-front-casino .navbar-nav>li>a,.page-template-front-racebook .navbar-nav>li>a,body.home .navbar-nav>li>a{color:#fff!important;font-weight:400}#top-link-block.affix-top{position:absolute;bottom:-82px;right:10px}#top-link-block.affix{position:fixed;bottom:18px;right:10px}.modal{text-align:center;padding:0!important}.bottomWelcome form{padding:0}.modal:before{content:'';display:inline-block;height:100%;vertical-align:middle;margin-right:-4px;margin-top:40px}.modal-dialog{display:inline-block;text-align:left}button.close.closeGray span{color:#000;opacity:1}.loginmodal-container{padding:10px 9px;max-width:350px;width:100%!important;margin:0 auto;border-radius:2px;overflow:hidden;font-family:roboto}.loginmodal-container h1{text-align:center;font-size:1.8em;font-family:roboto;margin-bottom:0}label.error{text-indent:39px;text-align:left;margin:0;padding:3px 0;font-size:12px!important;line-height:11px;color:red!important;display:inline-block;font-weight:400}button.login.loginmodal-submit,input.register.registermodal-submit{margin:0 4px 5px 0;padding:6px;font-size:1.5em;font-weight:400;font-family:Oswald,sans-serif;width:100%}input.register.registermodal-submit{border-radius:2px;border:0;-webkit-filter:none;filter:none;background-image:-webkit-linear-gradient(#3276b1,#3276b1);background-image:linear-gradient(#3276b1,#3276b1);box-shadow:0 2px 0 0 #3A4E69;color:#fff}input.register.registermodal-submit:hover{background-image:-webkit-linear-gradient(#3276b1,#365483);background-image:linear-gradient(#3276b1,#365483);box-shadow:0 2px 0 0 #3A4E69;color:#fff}button.login.loginmodal-submit{border-radius:2px;border:0;-webkit-filter:none;filter:none;background-image:-webkit-linear-gradient(#66aa1e,#459418);background-image:linear-gradient(#66aa1e,#459418);box-shadow:0 2px 0 0 #34620d;color:#fff}button.login.loginmodal-submit:hover{background-image:-webkit-linear-gradient(#459418,#459418);background-image:linear-gradient(#459418,#459418);box-shadow:0 2px 0 0 #34620d;color:#fff}.loginmodal-container input[type=text],input[type=password]{background:#fff;border:1px solid #d9d9d9;border-top:1px solid silver;border-radius:4px;padding:0 8px;box-sizing:border-box;-moz-box-sizing:border-box}.loginmodal-container input[type=text]:hover,input[type=password]:hover{border:1px solid #b9b9b9;border-top:1px solid #a0a0a0;box-shadow:inset 0 1px 2px rgba(0,0,0,.1)}.loginmodal{text-align:center;font-size:14px;font-family:Arial,sans-serif;font-weight:700;height:36px;padding:0 8px}.loginmodal-submit{border:0;color:#fff;text-shadow:0 1px rgba(0,0,0,.1);background-color:#4d90fe;padding:17px 0;font-family:roboto;font-size:14px}span.login-modal{color:#666}.loginmodal-submit:hover{border:0;text-shadow:0 1px rgba(0,0,0,.3);background-color:#357ae8}.loginmodal-container a{text-decoration:none;color:#666;text-align:center;display:inline-block;font-weight:400}form{padding:0 20px 10px}.login-help{font-size:14px;text-align:center}.form-control-login,.form-control-login:focus{display:block;width:100%;height:34px;padding:6px 12px;font-size:14px;line-height:1.42857143;color:#555;background-color:#fff;background-image:none;border:1px solid #ccc;border-radius:4px;box-shadow:none;-webkit-transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s;transition:border-color ease-in-out .15s,box-shadow ease-in-out .15s}.forgot-pass,.saveInfo{font-size:12px;margin-top:-10px;padding-right:0;padding-left:0;height:25px}input#remember_account_checkbox{width:15px;float:left;margin-right:5px;position:relative;bottom:12px}.saveInfo{float:left}.forgot-pass{float:right;text-align:right}.dont-have{background-color:rgba(1,1,1,.1);padding:4px 10px 10px;margin-top:5px;border-radius:2px}.modal-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:0;background-color:#000}.login-fields{border-radius:0 4px 4px 0!important}.input-group-addon-login{padding:6px 12px;font-size:14px;font-weight:400;line-height:1;color:#555;text-align:center;background-color:#eee;border:1px solid #ccc;border-radius:4px 0 0 4px;width:1%;white-space:nowrap;vertical-align:middle;display:table-cell}.input-group-addon-login:first-child{border-right:0}@media only screen and (max-width:768px){.modal-dialog{display:inline-block;text-align:left;vertical-align:top}}@media only screen and (max-width:375px){.loginmodal-container a,.saveInfo{font-size:11px}}.carousel{height:100%;width:100%;overflow:hidden}.carousel-caption{z-index:10}.carousel .item{padding:0;background-color:#777}.carousel-inner>.item>a>img{top:0;left:0;width:100%;bottom:0;min-height:auto}.carousel-control{width:4%}.carousel-indicators{bottom:-5px}.carousel-inner>.item>a>img,.carousel-inner>.item>img,.img-responsive,.thumbnail a>img,.thumbnail>img{display:inline!important}.sidebarBanner{border:1px solid #c1c1c1;overflow:hidden}div.sidebarBanner>a.createButton{margin-top:5px!important}footer{font-size:14px;text-align:center;color:#333;margin:5px 0}footer a{color:#0095FF;font-size:14px;font-weight:400;padding:0 10px}#footer-nav ul li a,.footer-submenu h5{font-size:12px;font-family:Oswald,sans-serif}.menu-footer-menu-container{width:80%;margin:auto;padding-top:10px}#footer{color:#aaa;font-family:Oswald,sans-serif;margin:0 auto;padding:20px;width:920px}#footer-nav{float:left;margin-bottom:5px}#footer-nav ul{margin-bottom:5px}#footer-nav ul li{display:inline;float:left;margin:10px 60px 0;padding:3px 10px 3px 0}#footer-nav ul li a{color:#ccc}#footer-nav ul li ul{padding:0;margin:0}#footer-nav ul li ul a{color:#313131;font-size:11px;margin:0}.conectFooter{float:right!important;text-align:right}.footer-submenu ul li a{color:#333;font-size:14px;text-transform:none;float:left;margin-top:3px;font-weight:lighter;font-family:Oswald,sans-serif;width:100%}.footer-submenu ul li a:hover{color:#0095FF!important;text-decoration:none}.footer-submenu ul li{float:left;margin:0 65px 0 0;list-style-type:none;padding:0 5px 0 0}.footer-submenu ul li ul li{clear:both;position:relative;margin:0 -30px;list-style-type:none}.footermargin{margin-bottom:30px}.footerGif{max-width:944px;width:50%;margin-left:25%;margin-right:25%;text-align:center}footer.registerFooter{color:#333}ul#sitemap-categories li a{color:#333!important;font-family:Oswald,sans-serif;list-style:none;text-transform:uppercase;font-weight:400;font-size:11px}.metaslider{margin-bottom:-30px;max-width:100%!important;overflow:hidden}.metasliderSection{padding:0!important}ul.slides{min-height:300px}.flexslider .slides li{height:100%!important}.metaslider .caption-wrap{position:absolute;top:0;left:0;background:rgba(0,0,0,.49)!important;color:#fff;opacity:1!important;margin:0;display:block;width:100%;line-height:1.4em;text-align:center}.landingPageSliderContainer{width:80%!important;margin:0 auto!important;top:0;position:absolute;height:100%!important;bottom:0;left:10%;padding:5% 0!important}.btnContainer,.captionMain,.subCaptionContainer{float:left;width:100%}.captionMain{position:relative;top:0}.metaSliderDownCaption{position:absolute;bottom:26px;width:100%;float:left}.metaSliderDownCaption h4{color:#fff!important;padding:0;font-size:32px!important;margin:0}.captionSlider{font-weight:800;font-size:58px!important;text-shadow:#000 0 0 12px;letter-spacing:9px}h2.captionSlider{color:#fff}.captionGroup{position:absolute!important;top:35%;padding:0 6%!important;float:left;height:auto;left:0;display:block}.captionGroup li{font-size:32px;margin-bottom:4px!important;float:right;width:100%;text-align:left;font-family:Oswald,sans-serif;font-weight:lighter}.captionGroup li span:first-child{font-size:36px}.captionGroup span{color:#4BA500;font-weight:bolder}.captionList{position:relative}.bullet-bonus span.bold{color:#4BA500}.sliderGroup a span{color:#fff;font-weight:500;transition:all .3s;-moz-transition:all .3s;-webkit-transition:all .3s}.metaSliderDownCaption{margin-top:5%}.connectionAbout{right:-14%;position:absolute!important;bottom:43px;margin-top:0;width:40%!important}.connectionAbout h4{color:#fff!important;text-align:left;float:left;width:100%;font-weight:lighter;text-transform:none;font-size:22px;position:relative;margin-bottom:10px;left:0}.connectionAboutUs{padding:0!important;text-align:center;width:38px!important;margin:0 6px}.connectionAboutUs:hover .speechAbout{opacity:1}.linksAbout img{width:100%!important}.linksAbout{left:0;padding:0!important}.speechAbout{opacity:0;position:relative;color:#fff;padding:5px;border-radius:5px;font-size:11px;z-index:9;word-wrap:normal;line-height:1.1em;width:42px;float:left;top:0;left:-2px;-webkit-transition:all .5s ease-in-out;transition:all .5s ease-in-out}.logosAsSeen{max-width:100%;position:absolute!important;left:-97px!important;bottom:-19px!important;min-width:200px}.actionSlider{position:relative!important;top:177px;left:20px;z-index:999}.gameGrid figure,.gameGrid li{position:relative;margin:0}span.bold{font-weight:700}a.bullet-bonus{color:#fff;font-weight:lighter}a.bullet-bonus:hover{color:#fff!important}a.bullet-bonus:focus{color:#fff}.gameGrid{list-style:none;margin:0 auto 0 0;padding:0 0 90px;text-align:center;width:100%}.gameGrid li{display:inline-block;padding-left:0;padding-right:0;text-align:left}.gameGrid figure img{width:100%;display:block;position:relative}.gameGrid figcaption{position:absolute;top:0;left:0;padding:10px 15px;background:#365483}figcaption span{width:70%;font-size:12px;font-weight:400;float:left;color:#fff}.gameGrid figcaption h3{margin:0;padding:0;color:#fff!important}.gameGrid figcaption a,button.gameId{padding:5px 10px;border-radius:2px;display:inline-block;color:#fff;text-align:center}.homeGameRow{width:100%;float:left}.gameGrid figcaption a{background:#459418;font-weight:400}button.gameId{background:#6a6a6a}.casinoCTA .joinHover,.casinoCTA:hover .createText{display:none}#iframe-outer{max-width:640px;max-height:360px;width:100%;height:100%}#iframe-inner{height:56.25%}#iframe-inner iframe{width:100%;height:100%;min-height:373px}.captionUp figure{overflow:hidden}.captionUp figure img{-webkit-transition:-webkit-transform .4s;transition:transform .4s}.captionUp figure.cs-hover img,.no-touch .captionUp figure:hover img{-webkit-transform:translateX(0);-ms-transform:translateX(0);transform:translateX(0)}.captionUp figcaption{height:50%;width:100%;top:auto;bottom:0;opacity:0;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0);-webkit-transition:-webkit-transform .4s,opacity .1s .3s;transition:transform .4s,opacity .1s .3s}.captionUp figcaption a,button.gameId{position:absolute;width:30%;text-decoration:none}.captionUp figure.cs-hover figcaption,.no-touch .captionUp figure:hover figcaption{opacity:1;-webkit-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0);-webkit-transition:-webkit-transform .4s,opacity .1s;transition:transform .4s,opacity .1s}.captionUp figcaption a{top:20px}.captionUp figcaption a:hover{background-color:#8fc800}button.gameId{top:60px;cursor:pointer;border:0}button.gameId:hover{background-color:#868686}.pageError{text-align:center}.pageError h2{color:#AFAFAF;line-height:1.3em}.pageNotfound{margin-bottom:0!important;margin-top:0!important}.pageNotfound .glyphicon{font-size:150px!important}.errorBack{font-size:16px!important;text-align:right}.errorBack .glyphicon{font-size:50px!important}.bonusSetion{position:relative;margin:0;width:100%;float:left;overflow:hidden;height:130px}.bonusSetion .terms{position:absolute;right:5px;color:#fff;background:rgba(0,0,0,.6);cursor:pointer;bottom:44px}.bonusSetion .bonusCTA{position:absolute;right:6px;bottom:67px;font-family:Oswald,sans-serif;color:#f1f1f1;border-radius:0;cursor:pointer;background:#c00;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#c00),color-stop(100%,#6d0019));background:-webkit-linear-gradient(top,#c00 0,#6d0019 100%);background:linear-gradient(to bottom,#c00 0,#6d0019 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#cc0000', endColorstr='#6d0019', GradientType=0);font-size:23px;text-decoration:none;padding:5px;box-shadow:#000 0 0 5px}.bonusSetion .bonusCTA:hover{background:#d71920;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#d71920),color-stop(100%,#47000b));background:-webkit-linear-gradient(top,#d71920 0,#47000b 100%);background:linear-gradient(to bottom,#d71920 0,#47000b 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0000', endColorstr='#47000b', GradientType=0)}.bonusSetion img{border:1px solid #dbdbdb;position:absolute;right:0;width:100%}.bonusSetionComing{position:relative;margin-bottom:8px;float:right;overflow:hidden;height:200px}.bonusSetionComing .terms{position:absolute;right:5px;color:#fff;background:rgba(0,0,0,.6);cursor:pointer;bottom:44px}.bonusSetion .depositNow,.bonusSetionComing .bonusCTAComing{position:absolute;font-family:Oswald,sans-serif;color:#f1f1f1;cursor:pointer}.bonusSetionComing .bonusCTAComing{right:5px;bottom:160px;border-radius:0;background:#c00;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#c00),color-stop(100%,#6d0019));background:-webkit-linear-gradient(top,#c00 0,#6d0019 100%);background:linear-gradient(to bottom,#c00 0,#6d0019 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#cc0000', endColorstr='#6d0019', GradientType=0);font-size:18px;text-decoration:none;padding:1px;box-shadow:#000 0 0 5px}.bonusSetionComing .bonusCTAComing:hover{background:#d71920;background:-webkit-gradient(linear,left top,left bottom,color-stop(0,#d71920),color-stop(100%,#47000b));background:-webkit-linear-gradient(top,#d71920 0,#47000b 100%);background:linear-gradient(to bottom,#d71920 0,#47000b 100%);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0000', endColorstr='#47000b', GradientType=0)}.bonusSetion .depositNow{right:6px;bottom:95px;background:#c00;font-size:20px;padding:5px}.page-template-front-casino .metaslider,.page-template-front-racebook .metaslider{margin-bottom:-70px}.page-template-front-casino a.bullet-bonus{font-size:38px;font-family:Oswald;line-height:1em}.bankingCTA a,.casinoContent h2,.homeContent h2{font-family:Oswald,sans-serif}.page-template-front-casino span.bold{font-size:50px}.page-template-front-casino span.upTo{margin-top:10px;font-size:20px}.page-template-front-casino .captionList{margin:9% 0}.casinoCTA:hover a{background:#262424;background:-webkit-linear-gradient(top,#262424 0,#000 100%);background:linear-gradient(to bottom,#262424 0,#000 100%);border-color:#666}.casinoCTA:hover .joinHover{display:inline;height:60px;font-size:33px;line-height:21px}.casino.featureSection{background:#EAEAEA;padding:10px;float:left;overflow:hidden;position:relative;margin:0;border-bottom:solid 1px #8F8F8F;height:auto;min-height:403px}.casino.featureSection img{width:100%;top:0;max-width:300px}.casino.featureSection h3{color:#333;z-index:900;margin:0 auto;width:100%;padding:10px 20px 5px 0;text-transform:none;font-size:25px;top:0;left:10px;text-align:center}.casino.featureSection h3:before{content:">";position:relative;margin-right:6px;top:-4px}.casinoTop{margin-bottom:0}.casino.featureSection h4{color:#333;font-size:22px;padding-bottom:0}.casino.featureSection p{float:left;font-size:11px}.game-footer,.game-footer a,button#closeGame{font-size:12px}.casino.featureSection a.promoButton{background:#459418;padding:10px;color:#fff;border-radius:2px;position:absolute;top:49%;right:39%;margin:10px 0;text-decoration:none;font-weight:400}.game-footer{padding:5px!important;text-align:right;border-top:0!important;color:#333}.casino.featureSection a.promoButton:hover{background:#da1743}.casino.featureSection a.morePromos{float:left}.casino.casinoGames{float:left;background:inherit}.instantGame{position:relative;float:left;border-left:solid 5px #999;margin:0;padding-left:10px;height:auto}div.blackjackGame,div.rouletteGame,div.wildsevenGame{text-indent:-90000000px;width:100%;margin-bottom:10px;height:108px;float:left}.instantGame h4{color:#f4f4f4!important}div.blackjackGame{background:url(/wp-content/themes/MyBookie/img/blackjack-casino-games-off.jpg) 0 24% no-repeat;background-size:100% auto}div.rouletteGame{background:url(/wp-content/themes/MyBookie/img/roulette-casino-games-off.jpg) 0 24% no-repeat;background-size:100% auto}div.wildsevenGame{background:url(/wp-content/themes/MyBookie/img/slot-casino-games-off.jpg) 0 24% no-repeat;background-size:100% auto}.casino.casinoGames a.casinoPlaynow{background:#a30327;padding:5px;color:#f1f1f1;position:absolute;bottom:-11px;right:3%;margin:10px 0;text-decoration:none}.casino.casinoGames a.casinoPlaynow:hover{background:#da1743}.casino.casinoGames .category-title{margin-top:0}.casinoContent{width:66.6666667%;float:left;text-align:justify;margin:15px 0 0;padding:0 20px 20px}.casinoContent h2{font-weight:400;border-bottom:1px solid #444;float:left;width:100%;margin-top:0;padding-bottom:5px;margin-bottom:10px}.bankingSection{padding:7px 17px 0 0;font-size:12px;margin:0;border-top:solid 1px #f1f1f1}.paymentOption{text-align:left;padding:0 0 10px}.paymentOption img{padding:7px 0;-webkit-border-radius:0}.ui-accordion .ui-accordion-icons{padding-left:.5em!important}.ui-accordion .ui-accordion-header{font-size:14px!important;outline:0}.ui-accordion .ui-accordion-content{padding:0 1px!important;font-size:11px!important;background:0 0!important;border:0;text-align:justify}.ui-accordion .ui-accordion-content p{margin:0}.ui-accordion .ui-accordion-content p a{color:#fff;display:block;font-weight:700;margin:2px 0;padding:9px 4px;background:#252525;font-size:10px}.ui-accordion .ui-accordion-content p a:hover{text-decoration:none;background:#010101}.ui-state-active,.ui-widget-content .ui-state-active,.ui-widget-header .ui-state-active{color:#8fc800!important}.bankingCTA{margin-top:10px}.bankingCTA a{background:#4BA500;color:#fff;font-weight:400;border-radius:2px;padding:6px;box-shadow:0 2px 0 0 #34620d}.post-thumbnail>a>img,.post-thumbnail>img,img.post-thumb{max-width:100%!important;width:100%}#accordion-faq.panel-group{margin-top:20px}#accordion-faq .panel{background-color:#fff;border-right:1px solid transparent;border-left:1px solid transparent;border-bottom:none;border-radius:0;box-shadow:none;margin-bottom:0}#accordion-faq .panel-default>.panel-heading{background-color:#fff;border-color:transparent;color:#333}#accordion-faq .panel-heading{padding:0;border:none}#accordion-faq .panel-title{color:inherit;font-size:16px;line-height:135%;margin-bottom:0;margin-top:0}#accordion-faq .panel-title a,#accordion-faq .panel-title a:hover{padding:10px 45px 10px 15px;font-weight:300;color:#fff;display:block;position:relative;background-color:#333}#accordion-faq .panel-group a,#accordion-faq .panel-group a:hover{color:#fff;background-color:#888}#accordion-faq .glyphicon.glyphicon-accordion{position:absolute;right:20px;top:11px}#accordion-faq .panel-default>.panel-heading+.panel-collapse>.panel-body{border-top-color:transparent}#accordion-faq .panel-group{margin-bottom:0}#accordion-faq .panel-group .panel.panel-default{border:none}#accordion-faq .panel-group .panel{border-radius:0;margin-bottom:0}#accordion-faq .panel-group .panel-body{border-left:solid 1px #ddd;border-bottom:solid 1px #ddd}#accordion-faq .panel-title a .glyphicon-accordion:before{content:"\e114"}#accordion-faq .panel-group .panel-title a .glyphicon-accordion:before{content:"\e082"}#accordion-faq .panel-group .panel-title a.collapsed .glyphicon-accordion:before{content:"\e081"}#accordion-faq .panel-title a.collapsed .glyphicon-accordion:before{content:"\e080"}.homeContent{text-align:justify;color:#000;margin:0 5% 20px;width:90%}.homeContent h2{font-weight:400;float:left;width:100%;margin-top:0;padding-bottom:5px;margin-bottom:5px}.homeContent img{width:100%;height:auto;margin:10px 0}.homeTop{background:url(http://beta.mybookie.ag/wp-content/themes/MyBookie/img/homeHero-sports-football.jpg) center top no-repeat;width:100%;padding:20px;margin:0}.homeHerofeatureCenter{width:26%;margin-right:2%}.homeHerofeatureMain .overlay-text h2{font-size:35px!important;margin-bottom:-10px}.homeHerofeatureMain .heroHomefeatureButton{display:none;left:35%}.homeFeature:hover .mainSectionsTitle{color:#8D8D8D}.widget_recent_entries li a{color:#333}.alignleft a,.alignright a,.read-more{background:#444;box-shadow:0 2px 0 0 #666;padding:10px 14px;margin-top:10px;color:#fff;font-weight:400}.alignleft a:hover,.alignright a:hover,.read-more:hover{color:#fff;background:#333}.heroHomefeatureButton{position:absolute;color:#fff;text-transform:uppercase;display:none;background:#a30000;text-decoration:none;padding:17px 20px;font-size:13px;font-weight:600;text-align:center;bottom:-7%;left:20%}.heroHomefeatureButton:hover{text-decoration:none;color:#fff;background:#bc0202}.as_seen_on{float:left;width:60%;margin-left:20%;margin-right:20%}.bonus_code_formHome{position:absolute;bottom:15px;left:0;max-height:101px;box-shadow:#000 0 8px 12px -5px;background:rgba(0,0,0,.89);color:#fff;border-top:solid 1px #DDD;border-bottom:solid 1px #DDD;padding:13px 39px 0;height:auto}.bonus_code_formHome p.small_text{margin-top:9px;color:#C7C7C7}@-moz-document url-prefix(){.metasliderSection{padding:0 10px 0 0;margin:0}}@-o-document url-prefix(){.metasliderSection{padding:0 10px 0 0;margin:0 0 0 -169px}}#loader{position:fixed;left:0;top:0;width:100%;height:100%;z-index:9999;background:url(/wp-content/themes/MyBookie/images/loader.gif) center no-repeat #000}.joinSection{padding:7px 5px}img.socialIconClass{float:left;width:23%;margin:0 5px;left:0}.social_button_text{float:right;padding:10px}.social_button_text h4{float:right;font-size:12px;color:#333;width:100%;text-align:center}span.phone-middle{text-align:center;color:#666;font-size:1.2em;font-weight:400;padding:20px 0}.phoneText{float:right;line-height:10px;padding-left:11px;border-left:1px solid;margin:9px;color:#fff}.homeSteps{width:50%;margin:0 auto!important;text-align:center}.iconContainer{text-align:center;padding:20px;width:100%;color:#666;max-width:250px;margin:auto;font-weight:600;transition:all .3s;-moz-transition:all .3s;-webkit-transition:all .3s}.iconContainer h2{font-size:35px!important;color:#fff!important}.iconContainer .glyphicon{font-size:64px;display:block;color:#292929}.stepsAction{font-size:17px;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif}.stepNext{font-family:'Glyphicons Halflings';color:#B7B7B7;position:relative}.stepsBackground{margin-top:0;padding:0}.homeHerofeatureLiveBetting,.homeHerofeaturePosts{margin:20px 0}.mobile-image{float:left;padding:0 30px 0 10px}.homebanners{width:97%;margin-left:auto;margin-right:auto}span.phone-link{color:#FFF;background-color:#12A5F4;padding:6px 16px;line-height:2;border-radius:2px;font-size:1.2em}.call2mobile{background-color:#f1f1f1;margin:5px 0}.modal-content{background-color:#f5f5f5;color:#fff;border:1px solid #e2e1e9}.modal-header{background-color:#3276b1;border-top-left-radius:8px;border-top-right-radius:8px}.modal-title{color:#fff!important}.modal-body img{width:100%}.horsesContent{width:100%;text-align:justify;float:left;margin:15px 0 0;padding:0 20px 20px}.horsesContent h2{font-family:Oswald,sans-serif;font-weight:400;border-bottom:1px solid #444;float:left;width:100%;margin-top:0;padding-bottom:5px;margin-bottom:10px}@media screen and (max-width:1600px){.captionGroup{padding:0!important;top:170px}.captionList{left:0!important}.captionGroup li{font-size:28px!important}.captionGroup li:first-child{font-size:31px}}@media screen and (max-width:1450px){.captionGroup li{font-size:22px!important}.captionGroup li:first-child{font-size:26px}}@media only screen and (max-width:1400px){.pb-feat{max-width:118px;height:27px;left:4px}}@media screen and (max-width:1395px){.captionUp figcaption a,button.gameId{font-size:12px}}@media screen and (max-width:1200px){.captionList{left:0!important;margin-top:15px}.captionGroup{padding:0!important;top:30px}.captionSlider{font-size:24px!important;top:-32px;position:relative}.landingPageSliderContainer{padding:0!important;margin-top:107px!important}.captionGroup li{font-size:20px!important}.captionGroup li span:first-child{font-size:23px}.connectionAbout{bottom:138px}.connectionAboutUs{width:35px!important}.captionMain{position:relative;top:0}.featureAction{font-size:16px!important}.featuredButton{font-size:14px!important;width:auto;bottom:0;padding:1% 5%;right:0}}@media only screen and (max-width:1070px){.btn-grey{font-size:12px!important}.captionGroup li{font-size:19px!important}.captionGroup li span:first-child{font-size:21px}}@media screen and (max-width:1024px){.captionUp figcaption{height:auto;overflow:hidden;float:left;width:100%;padding-bottom:20px;top:-2px;position:relative;opacity:1;-webkit-transition:none;transition:none;border-left:2px solid #fff;border-right:2px solid #fff}figcaption span{width:100%;font-size:11px}.gameGrid figcaption h3{font-size:1em;margin:0;padding:0;color:#fff!important}.captionUp figcaption a,button.gameId{top:10px;width:49%;font-size:12px;text-decoration:none;position:relative}.captionUp figcaption a{float:left}button.gameId{float:right;cursor:pointer;border:0}.gameGrid{padding:10px 10px 100px}.gameGrid li{width:100%;min-width:300px}.actionSlider{position:relative;top:64px;left:20px;z-index:999}.caption .my-btn-new{padding:11px!important;font-size:22px}}@media screen and (max-width:992px){.pb-feat{max-width:98%;bottom:43px;height:30px;position:relative;margin-bottom:-50px!important}.blackCaption{margin-top:60px}.captionMain{position:relative;top:16px}.subCaptionContainer{position:relative;top:-20px}.btnContainer{top:29px}.caption .my-btn-new{font-size:22px!important;padding:11px!important;left:0}.mainSectionsTitle{font-size:15px}.metaSliderDownCaption{bottom:24px}.metaSliderDownCaption h4{font-size:25px!important}.captionSlider{font-size:17px!important;top:-82px}.captionGroup{top:5px}.captionGroup li{font-size:15px!important}.captionGroup li span:first-child{font-size:19px}.connectionAboutUs{margin:0 7px;width:21px!important}.connectionAboutUs img{max-width:41px!important;margin:0!important;right:0!important;padding:0 2px!important}.captionList{left:0}.slides{height:300px;overflow:hidden}.metaslider .slides img{width:170%!important}.featureAction{font-size:13px!important}.featuredButton{float:left;margin-left:.5%;margin-right:.5%;position:relative;font-size:12px!important;width:99%;max-width:99%;margin-top:-1px;bottom:0;padding:5px;right:0}}@media only screen and (max-width:883px){.captionList.sliderGroup{margin-left:0;margin-right:0;padding-left:0;padding-right:0}}@media only screen and (max-width:767px){.brand,.carousel-inner>.item>img,.subCaptionContainer{position:relative}.connectionAbout h4,.footerGif,.sliderGroup li{text-align:center}#breadcrumbs{top:100px}.brand{left:39px}.navbar{border-radius:0}.sportsbook-nav{height:0;min-height:0}.darkBackground{background-color:transparent!important}.homeWrapper{margin-top:0!important}body.page .homeWrapper{margin-top:20px!important}.featured{display:table-row;background:0 0;margin-bottom:0;margin-top:0;padding:0;border-radius:2px}.homeSteps{width:100%!important}.iconContainer{padding:8px;font-weight:500}.stepsAction{font-size:14px;line-height:16px}.stepsBackground{padding:0!important;margin:0!important}.carousel .item{padding:0!important;height:100%!important}.carousel-caption p{margin-bottom:20px;font-size:21px;line-height:1.4}.landingPageSliderContainer{margin-top:30px!important}.subCaptionContainer{top:-20px}.mainSectionsTitle{font-size:15px}.sliderGroup li{font-size:20px!important}.slides{height:563px;overflow:hidden}.connectionAboutUs{margin:0 11px;width:21px!important}.connectionAboutUs img{max-width:41px!important;margin:0!important;right:0!important;padding:0 2px!important}.slider{padding-top:0;padding-right:0}.speechAbout img{width:10px!important}.speechAbout{opacity:1;font-size:10px}.linksAbout{float:right!important;left:-36px}.featureAction{font-size:13px!important}.item-detail{height:100%!important;margin-bottom:15px}.table-responsive{border:none!important}.ui-dialog{width:100%!important;overflow-y:scroll!important;left:0!important;top:0!important;-webkit-overflow-scrolling:touch;z-index:99999}.menu-footer-menu-container{width:100%;margin:auto;padding-top:0}.casino.featureSection{border-bottom:0}.casino.featureSection h3{padding:0;font-size:35px}.casino.featureSection a.promoButton{position:relative;float:right}.joinSection{padding-right:0!important;z-index:1}.navbar-nav>li{width:100%}.navbar-default .navbar-nav>li>a{padding:10px;color:#FFF!important}.navbar-collapse{background-color:rgba(1,1,1,.8)}.navbar-default .navbar-toggle{border-color:#000!important;background-color:#000!important;color:#fff!important}.navbar-toggle{float:left;margin-left:5px}button#nav-sportsbook{top:-55px}.navbar-fixed-top .container{position:absolute;top:7px;z-index:0}.btnContainer{top:29px}.caption .my-btn-new{font-size:25px!important;top:0!important;padding:8px 33px}.joinHere{padding:6px 2px!important;width:35%!important;margin-right:6px;float:left;font-size:13px!important;font-family:Arial!important}.footerGif{max-width:944px;width:90%;margin-left:5%;margin-right:5%}footer a{color:#FFF!important;background-color:#9A9A9A;float:left!important;width:100%!important;font-size:14px!important;font-weight:400;padding:5px 0!important;margin-bottom:4px!important}.as_seen_on{float:left;width:100%;margin-left:0;margin-right:0;border-radius:2px}.logosAsSeen{position:relative!important;margin:0 auto!important;left:0!important}.blackCaption{margin-top:60px}.captionMain{position:relative;top:16px}.metasliderSection{top:0}.captionList{left:0}.captionSlider{font-size:24px!important;top:-8px}.caption h2{margin-top:10%}.captionGroup{top:93px}.captionGroup li{font-size:18px!important}.captionGroup li span:first-child{font-size:21px}.actionSlider{left:0;top:-3px!important;position:relative!important}.metaSliderDownCaption{bottom:24px;top:95px;position:relative}.metaSliderDownCaption h4{font-size:30px!important;position:relative;margin:0;padding:0;top:10px}.metaslider .slides img{width:693%!important;position:relative}.connectionAbout{position:relative!important;top:240px;width:100%!important;left:0}}@media only screen and (max-width:480px){.pb-feat{max-width:118px;height:27px;bottom:43px;left:4px}.btnContainer{top:91px!important}.social_button_text{float:right;width:50%;margin:-40px 25% 15px}span.timezone{left:5px!important}div#time-zone-disclaimer{padding-left:0}.item-detail img{max-width:89px}.item-detail span{font-size:23px}#register{padding:10px 10px 0}.captionMain{top:102px}.subCaptionContainer{top:70px;position:relative}.metaSliderDownCaption{top:130px}.caption{position:relative;top:10px}.slides{height:586px;overflow:hidden}.metaslider .slides img{right:290%;position:relative}.blackCaption{margin-top:-35px!important}.landingPageSliderContainer{margin-top:-60px!important;left:0;width:100%!important}.captionSlider{font-size:29px!important;letter-spacing:5px}.captionGroup{top:194px;left:0}.actionSlider{left:0;margin-top:65px}.connectionAbout{position:relative!important;top:383px}.captionGroup li{text-align:center}.captionList{top:-20px;left:0}.linksAbout{width:100%!important;padding:0 0 0 35%!important}.sliderGroup li{font-size:28px!important}.speechAbout{opacity:1;width:40px;padding:3px}.metaslider .slides img{width:667%!important}.connectionAboutUs{width:45px!important;margin:0}.connectionAbout h4{text-align:center;width:100%;left:0}.page-template-front-casino .captionList{margin:19% 0 0}.page-template-front-casino span.bold{font-size:40px}.bg-custom{padding:14px 10px}}@media only screen and (max-width:414px){.pb-feat{bottom:43px}}@media only screen and (max-width:375px){.pb-feat{bottom:43px}}@media only screen and (max-width:320px){.caption .my-btn-new{top:-19px!important;font-size:23px!important}.footerGif{display:none}.pb-feat{bottom:43px}}

            body{
                padding: 10px 9px;
                max-width: 350px;
                width: 100% !important;
                margin: 0 auto;
                border-radius: 2px;
                overflow: hidden;
                font-family: roboto;
                color: white;
                background: linear-gradient(to bottom, #1d537c 0%,#003158 100%);
            }

            #login-form{
                top: 2em;
                position: relative;
                width: 21em;
            }

            .loginmodal-container {
                top: 1em;
                position: relative;
                height: 100%;
            }

            .loginmodal-container img{
                width: 60%;
                top: 0%;
                position: absolute;
                left: 4.5em;
            }

            .loginmodal-container h1{
                position: relative;
                top: -1.8em;
                text-align: center;
                font-size: 1.8em;
                font-family: roboto;
                margin-bottom: 0;
            }
            .submitContainer{
                padding: 0;
                top: 1em;
            }
            input.register.registermodal-submit{
                font-size: 1.9em;
                letter-spacing: 0.03em;
                background: #c71f24;
                box-shadow: none;
                margin: 0px;
            }
            input.register.registermodal-submit:hover{
                background: #c71f24;
                box-shadow: none;
            }
            button.login.loginmodal-submit{
                letter-spacing: 0.04em;
                background: #eaeaea;
                color: #444;
                margin: 0;
                box-shadow: none;
            }
            button.login.loginmodal-submit:hover{
                background: #eaeaea;
                box-shadow: none;
                color: #444;
            }
            span.login-modal{
                color: #b3b8bb;
            }
            .loginmodal-container a{
                color: #b3b8bb;
            }
            .glyphicon.glyphicon-remove{
                color: white;
            }
            .dont-have {
                top: 1em;
                padding: 4px 0px 10px;
                background: none;
                width: 100%;
            }

            .loginmodal-container a{
                width: 100%;
                padding: 0px;
            }

            #login_error_msg{
                top: 2.3em;
                position: relative;

            }

            #login_error_msg .alert.alert-danger.fade.in{
                position: relative;
                width: 97%;
                height: -6px;
                z-index: 1;
                padding: 0px;
                margin: 0px;
                top: 0px;
                left: 0.4em;
            }
            #login_error_msg .alert.alert-danger.fade.in .close{
                position: absolute;
                color: black;
                left: 125px;
                padding: 0px;
                margin: 0px;
                top: 4px;
                font-size: 1.5em;
                font-weight: bold;
            }

    </style>
</head>
<body>
    <!--<div id="login_error_msg"> </div>-->
    <?php
    /*
        include_once("settings.php");
        include_once(DIR_CLASS . "dgs.php");
        $var_form_action = LOGIN_FORM_POST_URL . LOGIN_FORM_POST_PAGE_URL ;
        $Ndgs = new DGS(LOGIN_FORM_POST_URL);
        $finputs = $Ndgs->getloginform(LOGIN_FORM_POST_PAGE_URL);
    */
    ?>
    <div class="loginmodal-container">
        <img src="//www.betusracing.ag/wp-content/themes/betusracing/images/betusracing-logo.png" alt="">
        <div id="login_error_msg"></div>
        <form class="form-horizontal" action="https://ww1.betusracing.ag//Default.aspx" method="post" id="login-form" name="loginform" role="form" onsubmit="remember_password();" novalidate="novalidate">
        <!-- onsubmit="this.ctl00$MainContent$Password.value = this.ctl00$MainContent$Password.value.toUpperCase();" -->

            <input value="/wEPDwULLTIxMzIyODUwNTJkZEVzgCldL/bvlP9rNVq8ljJMc/QauoYddym/gHUIj1kM" name="__VIEWSTATE" type="hidden">
            <input value="CA0B0334" name="__VIEWSTATEGENERATOR" type="hidden">
            <input value="/wEdAAa5UgcADjsa65a5u9RgwfxY/A3KwOea/Z+NvctOzVZTu8+9P3s0w+MeGOPom7ExypCoIatKanHO+uhiF/QUpC21GafnsvrqAplaV+hPPsCrHNQgSBaIyQt7Wv9HWROM1UmLxZssuJZRqEgVXLhEUOrvEuK1jXMzqC+3JauVFLgh7Q==" name="__EVENTVALIDATION" type="hidden">
            <input value="Submit" name="ctl00$MainContent$btnLogin" type="hidden">
            <input value="93" name="ctl00$MainContent$IdBook" type="hidden">

        <div class="form-group">
            <input type="hidden" autofocus="true">
        </div>
        <!-- <div class="forgot"><a href="/password-reset">Forgot?</a></div>-->
        <div class="form-group">
            <div class="input-group login-fields">
            <span class="input-group-addon-login">
                <i class="glyphicon glyphicon-user"></i>
            </span>
            <input name="DISPLAY_ctl00_MainContent_Account" type="text" class="form-control-login login-fields" id="name" required="" tabindex="99" placeholder="Account number / Email" aria-required="true">
            <input id="account_name" type="hidden" name="ctl00$MainContent$Account" class="form-control-login sign-in-account" value="">
            </div>
        </div>
        <!-- <div class="forgot"><a href="/password-reset">Forgot?</a></div>-->
        <div class="form-group">
            <div class="input-group login-fields">
                <span class="input-group-addon-login">
                    <i class="glyphicon glyphicon-lock"></i>
                </span>
            <!--<label class="login-modal" for="password">Password:</label>-->
            <input name="DISPLAY_ctl00_MainContent_Password" type="password" class="form-control-login login-fields" id="password" required="" tabindex="100" placeholder="Password" aria-required="true">
            <input type="hidden" placeholder="Password" maxlength="10" name="ctl00$MainContent$Password" class="form-control-login sign-in-password">
            </div>
        </div>
            <div class="col-xs-6 forgot-pass">
                <a href="#" onclick="_forgot_pass()" >Forgot Password?</a>
             </div>
        <div class="col-xs-6 saveInfo">
            <input type="checkbox" name="remember_account" id="remember_account_checkbox" value="DoRemember" class="form-control-login">
            <span class="login-modal" for="remember_account">Save my information</span>
        </div>
        <div class="col-xs-12 submitContainer">
            <!-- <label class="checkbox"><input type="checkbox"> Remember me</label> -->
            <button type="submit" class="login loginmodal-submit" >Login</button><br>
            <!-- <a href="#" class="col-xs-12 forgotLogin pull-right">Forgot Login?</a> -->
        </div>
        <div class="text-center col-xs-12 dont-have">
            <span>Don't have an account?</span>
            <a class="col-xs-12" href="#" onclick="_redirect_signup()"><input type="button" name="register" class="register registermodal-submit" value="Join Now!"></a>
        </div>
        </form>
    </div>

    <script>
            var processed_form = false;
            var processing_form = false;
            var cs_phone_number = "18448772246";


            /*Trims fields to avoid having empty information*/
            $('#form_modal_signup :input').change(function() {
                $(this).val($(this).val().trim());
            });
            $("[name='DISPLAY_ctl00_MainContent_Account']").change(function() {
                $(this).val($(this).val().trim());
            });

            function getUrlParameter(e) {

                var t = window.location.search.substring(1);

                var n = t.split("&");

                for (var r = 0; r < n.length; r++) {

                    var i = n[r].split("=");

                    if (i[0] == e) {
                        return i[1]
                    }

                }

            }

            var account_name = getUrlParameter('acc');

            if (typeof account_name != 'undefined') {

                var elem = document.getElementById("account_name");

                elem.value = account_name;

            }

            /*$("#login-form").validate({
                rules:{
                    DISPLAY_ctl00_MainContent_Account:{
                        required : true
                    },
                    DISPLAY_ctl00_MainContent_Password:{
                        required : true
                    }
                }
            });*/

            $('#login-form').submit(function (event) {
                event = event || window.event;
                if(!processed_form) {
                    if(event.preventDefault) {
                        event.preventDefault();
                    }
                    else {
                        event.returnValue = false;
                    }

                    if(!processing_form){
                        log_in();
                    }
                }
            });

            function log_in(){
                if($("[name='DISPLAY_ctl00_MainContent_Account']").val().trim().length == 0 || $("[name='DISPLAY_ctl00_MainContent_Password']").val().trim().length == 0 ){
                    return false;
                }
                processing_form = true;
                window.password_to_submit = $("[name='DISPLAY_ctl00_MainContent_Password']").val();
                window.account_to_submit = $("[name='DISPLAY_ctl00_MainContent_Account']").val();
                if(typeof createCookie !== 'undefined' && $('#remember_login').is(':checked')){
                    createCookie('username_login',window.account_to_submit,365);
                }
                $.ajax({
                    url: "https://ww1.betusracing.ag/loginservicejson.php?a=credentials",
                    method: "POST",
                    data: {"p": window.password_to_submit, "u": window.account_to_submit, "pfree": "0" },
                    beforeSend : function(){
                        $(".login-modal-submit").attr('disabled','disabled');
                        $(".login-modal-submit").empty().html("Logging in...");
                    }
                }).done(function (data) {
                    try {
                        var json_data = JSON.parse(data);
                    } catch (e) {
                        console.log("Failed parsing JSON response");
                        var msg = "There was a problem processing your request, if the issue persists please contact customer service at:<br><a href='tel:+1"+cs_phone_number+"'>"+cs_phone_number+"</a>";
                        notify_message('Ooops',msg,'error','#login_error_msg',{color_login_fields:false});
                    }

                    if(json_data.SPacct!=null && json_data.SPpwd!=null) {
                        $("[name='ctl00$MainContent$Password']").val(json_data.SPpwd);
                        $("[name='ctl00$MainContent$Account']").val(json_data.SPacct);
                    }else{
                        changeValuesToBasic();
                    }
                    if(json_data.status==1) {
                        processed_form = true;
                        $('#login-form').trigger("submit");
                        if ( typeof parent.redirect == 'function' ) {
                            parent.redirect();
                        }
                        /*
                        else{
                            setTimeout(function(){
                            window.location.href='https://ww1.betusracing.ag/wager/Bridge.aspx?to=url&url=//www.betusracing.ag/clubhouse/';
                            }, 3500 );
                            $('#login-form').trigger("submit");
                        }
                        */

                    }else{
                        notify_message('Error:',"Invalid account or password",'error','#login_error_msg',{color_login_fields:true});
                    }

                }).fail(function(jqXHR){
                    changeValuesToBasic();
                    var msg = "There was a problem processing your request, if the issue persists please contact customer service at:<br><a href='tel:+1"+cs_phone_number+"'>"+cs_phone_number+"</a>";
                    console.log("WS return error");
                    notify_message('Ooops',msg,'error','#login_error_msg',{color_login_fields:false});
                }).always(function(){
                    $("#football-loader-container").hide();
                    $(".login-modal-submit").removeAttr('disabled');
                    $(".login-modal-submit").empty().html("Sign-in");
                    processing_form = false;
                });

                function changeValuesToBasic(){
                    $("[name='ctl00$MainContent$Password']").val(window.password_to_submit);
                    $("[name='ctl00$MainContent$Account']").val(window.account_to_submit);
                }
            }

            function notify_message(title,message,type,div_to_place,information){
                if(type=='error') {
                    var msg ="<div class='alert alert-danger fade in'>";
                    msg+="<a href='#' class= 'close' data-dismiss='alert' aria-label='close' title='close'>x</a>";
                    msg+=title+" "+message;
                    msg+="</div>";
                    $(div_to_place).empty().append(msg);
                }
            }

    </script>



    <script>

        function _forgot_pass(){
            if ( typeof parent.forgot_pass == 'function' ) {
                parent.forgot_pass();
            }else{
                window.location.href='https://www.usracing.com/login-troubles';
            }
        }

        function _redirect_signup() {
            if ( typeof parent.redirect_signup == 'function' ) {
                parent.redirect_signup();
            }
            else{
                window.location.href='//www.betusracing.ag/signup?=h';
            }
        }


        function remember_password(){
            if (document.getElementsByName('remember_account')[0].checked){
                setCookie("usr", document.getElementsByName("DISPLAY_ctl00_MainContent_Account")[0].value);
                setCookie("pwd", document.getElementsByName("DISPLAY_ctl00_MainContent_Password")[0].value);
            } else {
                removeCookie("usr");
                removeCookie("pwd");
            }
        }
        function setCookie(cname, cvalue) {
            var d = new Date();
            d.setTime(d.getTime() + (365*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + btoa(cvalue) + "; " + expires;
        }
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return atob(c.substring(name.length,c.length));
            }
            return "";
        }
        function removeCookie( name ) {
            document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
        function setUsername(){
            if (getCookie("usr") != "") {
                document.getElementsByName("DISPLAY_ctl00_MainContent_Account")[0].value = getCookie("usr");
            }
        }
        function setPassword(){
            if (getCookie("pwd") != "") {
                document.getElementsByName("DISPLAY_ctl00_MainContent_Password")[0].value = getCookie("pwd");
            }
        }
        function setSelector(){
            if (getCookie("pwd") != ""){
                document.getElementsByName('remember_account')[0].checked = true;
            } else {
                document.getElementsByName('remember_account')[0].checked = false;
            }
        }
        function setLoginFormValues(){
            setUsername();
            setPassword();
            setSelector();
        }
        $( document ).ready(function() {
            /*window.location.href='http://ww1.betusracing.ag/wager/Sports.aspx';*/
            setLoginFormValues();

        });





        /*
        if ( $( "form.form-horizontal#login-form" ).length < 1 ) {
            if ( typeof parent.redirect == 'function' ) {
                parent.redirect_session();
            }
        }
        */

    </script>
</body>
</html>
