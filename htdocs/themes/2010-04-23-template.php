<?php	 	
// $Id$

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to ahr_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: ahr_breadcrumb()
 *
 *   where STARTERKIT is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */

/**
 * Implementation of HOOK_theme().
 */
function ahr_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);

  $hooks['image_preloads'] = array(
    'arguments' => array('images' => array()),
  );

  $hooks['primary_links'] = array(
    'arguments' => array('links' => array()),
    'template' => 'nav-primary-links',
  );

  $hooks['secondary_links'] = array(
    'arguments' => array('links' => array()),
    'template' => 'nav-secondary-links',
  );

  $hooks['copyright'] = array(
    'arguments' => array(),
    'template' => 'copyright-block',
  );

  $hooks['ahr_login_block'] = array(
    'arguments' => array('form' => array()),
    'template' => 'login-box-block',
  );

  $hooks['sf_menu_item'] = array(
    'arguments' => array(),
  );

  $hooks['sf_menu_item_link'] = array(
    'arguments' => array(),
  );

  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}


/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function ahr_preprocess_page(&$vars) {
  $images_path = path_to_theme() .'/images';

  $vars['seo_links'] = menu_navigation_links('menu-seo-menu');
  if (user_is_logged_in()) {
     $vars['topnav_links'] = menu_navigation_links('menu-topnav-loggedin');
  } else {
     $vars['topnav_links'] = menu_navigation_links('menu-topnav-loggedout');
  }
  $vars['copyright'] = theme('copyright');

  if (!user_is_logged_in() && arg(0) != 'user' && arg(0) != 'login') {
    $vars['login_box'] = drupal_get_form('user_login_block', array('#theme' => 'ahr_login_block'));
  }

  $path_common = drupal_get_path('module', 'allhorseracing') .'/common';
  $vars['top_banner'] = '<div id="top-banner-static"></div>';
  drupal_add_js($path_common .'/base.js', 'module', 'header', FALSE, TRUE, FALSE);
  if (user_is_anonymous()) {
               drupal_add_js($path_common .'/showjoin.js', 'module', 'header', FALSE, TRUE, FALSE);
			   //drupal_add_js($path_common .'/showdeposit.js', 'module', 'header', FALSE, TRUE, FALSE);
			   //drupal_add_js($path_common .'/swfobject.js', 'module', 'header', FALSE, TRUE, FALSE);
	       drupal_add_js($path_common .'http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js', 'module', 'header', FALSE, TRUE, FALSE);
	       $vars['top_banner'] = '<div id="flashcontent" style="text-align: right;">
		   
		   <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="690" height="230" id="flashEmbed" align="middle">
				<param name="movie" value="http://www.allhorseracing.com/themes/flash/home-banner-kd.swf" />
				<param name="menu" value="false" />
				<param name="wmode" value="transparent" />
				<param name="bgcolor" value="#FFFFFF" />
				<param name="allowscriptaccess" value="always" />
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="http://www.allhorseracing.com/themes/flash/home-banner-kd.swf" width="690" height="230" align="middle">
				<param name="menu" value="false" />
				<param name="wmode" value="transparent" />
				<param name="bgcolor" value="#FFFFFF" />
				<param name="allowscriptaccess" value="always" />
				<!--<![endif]-->
				<div><a rel="nofollow" target="_blank" href="http://www.adobe.com/go/getflashplayer">
						<img style="margin: 30px 30px 0 0; float: right;"  src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></div>

                <div style="clear: right; margin-right: 30px; padding-top: 20px;">To view this content click on the icon above or <a rel="nofollow" target="_blank" href="http://www.adobe.com/go/getflashplayer"><strong>Click Here</strong></a><br />to upgrade to the latest Flash Player!</div>
				
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>		   
		   </div>';	
  }
 

  //if (drupal_is_front_page()) {
     //Add the script and css for the track results scroll pane, jquery call is in allhorseracing.blocks.inc. Style is added in page.tpl.php
  //   drupal_add_js('themes/scripts/jScrollPane.js', 'module', 'header', FALSE, TRUE, FALSE);
  //} 

  // Image preloads
  $vars['page_image_preloads'] = array(
    $images_path .'/betnow-default.gif',
    $images_path .'/betnow-hover.gif',
    $images_path .'/tracksicon.gif',
    $images_path .'/resultsicon.gif',
    $images_path .'/newsicon.gif',
    $images_path .'/ahr-logo.gif',
    $images_path .'/ahr-logo2.gif',
    $images_path .'/loginbox-bg.gif',
    $images_path .'/tab-tracks.gif',
	$images_path .'/loading.gif',
	$images_path .'/ahr-message-bg.gif',
  );

  // Primary nav bar image preloads
  $vars['navbar_image_preloads'] = array(
    $images_path .'/blockbg.gif',
    $images_path .'/blockactive.gif',
    $images_path .'/blockactive_open.gif',
  );

  // Secondary nav bar image preloads
  $vars['navbar_image_preloads2'] = array(
    $images_path .'/searchbox-bg.gif',
    $images_path .'/searchbox-button.gif',
    $images_path .'/searchbox-button-hover.gif',
  );


  $vars['messages'] = $vars['show_messages'] ? theme('status_messages') : '';

  $vars['primary_links'] = theme('primary_links', $vars['primary_links']);
  $vars['secondary_links'] = theme('secondary_links', $vars['secondary_links']);

  // Retrieving the JS one more time since the above theme functions may have injected new JS
  $vars['scripts'] = drupal_get_js();
}


function ahr_preprocess_block(&$vars) {
  if ($vars['block']->module == 'multiblock') {
    $block = multiblock_get_block($vars['block']->delta);
    //dpm($vars);
  }
  //dpm($vars);
}


/**
 * Theme copyright block
 * @return unknown_type
 */
function ahr_preprocess_copyright(&$vars) {
  $vars['copyright_text'] = t('Copyright 2009 All Horse Racing - This website is owned by Domain Holdings Limited.');
  $vars['logo_image'] = theme('image', path_to_theme().'/images/ahr-logo2.gif', t('All Horse Racing'));
}


function ahr_preprocess_ahr_login_block(&$vars) {

}

function ahr_image_preloads($images) {
  if (!is_array($images) || !count($images)) {
    return;
  }

  $output = '<div class="preLoadimages">';

foreach($images as $image) {
	$output .= theme('image', $image, 'x');
	}
	
  $output .= '</div>';

  return $output;
}



function ahr_preprocess_primary_links(&$vars) {
  $menu_name = variable_get('menu_primary_links_source', 'primary-links');
  $tree = menu_tree_page_data($menu_name);
  $menuTree=sf_menu_tree_output($tree);
	if (user_is_anonymous()) {
		$menuTree=str_replace('<li class="menu-942 depth-0 lastbutone"><a href="/clubhouse" title="Deposit">Deposit</a></li>','',$menuTree);
	}
	else
	{
		$menuTree=str_replace('<li class="menu-942 depth-0 lastbutone"><a href="/clubhouse" title="Deposit" class="active">Deposit</a></li>','<li class="menu-942 depth-0 lastbutone"><a href="javascript:void(0);" onclick="open_banking();" title="Deposit">Deposit</a></li>',$menuTree);
		$menuTree=str_replace('<li class="menu-942 depth-0 lastbutone"><a href="/clubhouse" title="Deposit">Deposit</a></li>','<li class="menu-942 depth-0 lastbutone"><a href="javascript:void(0);" onclick="open_banking();" title="Deposit">Deposit</a></li>',$menuTree);
	}
  $vars['menu'] = $menuTree;
}

function sf_menu_tree_output($tree, $current_depth = 0, $parent_link = NULL) {
  $output = '';
  $items = array();

  // Pull out just the menu items we are going to render so that we
  // get an accurate count for the first/last classes.
  foreach ($tree as $data) {
    if (!$data['link']['hidden']) {
      $items[] = $data;
    }
  }

  $num_items = count($items);
  foreach ($items as $i => $data) {
    $extra_class = NULL;
    if ($i == 0) {
      $extra_class = 'first';
    }
	if ($i == $num_items - 2) {
      $extra_class = 'lastbutone';
    }
	
    if ($i == $num_items - 1) {
      $extra_class = 'last';
    }


    if ($data['below']) {
      $output .= theme('sf_menu_item', $data['link'], $data['link']['has_children'],  sf_menu_tree_output($data['below'], $current_depth+1, $data['link']), $current_depth, $data['link']['in_active_trail'], $extra_class);
    }
    else {
      $output .= theme('sf_menu_item', $data['link'], $data['link']['has_children'], '', $current_depth, $data['link']['in_active_trail'], $extra_class);
    }
  }
  if ($current_depth == 0) {
    return $output ? '<ul class="sf-menu">'. $output .'</ul>' : '';
  }
  else if ($current_depth == 1) {
    $map = allhorseracing_mega_menu_ad_term_map();
    $link_path = drupal_get_path_alias($parent_link['link_path']);
    if ($tid = $map[$link_path]['tid']) {
      $ad = ad($tid, 1, array('ad_display' => 'jquery'));
    }
    return $output ? '<ul><div class="mega-menu">'. $ad .'<div class="inner">'. $output .'</div></div></ul>' : '';
  }
  else {
    return $output;
  }
}

function ahr_sf_menu_item($link, $has_children, $menu = '', $current_depth, $in_active_trail = FALSE, $extra_class = NULL) {
  $class = 'menu-'.$link['mlid'] .' depth-'.$current_depth;

  if (!empty($extra_class)) {
    $class .= ' '.$extra_class;
  }

  $link = theme('sf_menu_item_link', $link);
  if ($current_depth == 0) {
    return '<li class="'. $class .'">'. $link . $menu ."</li>\n";
  }
  else {
    return '<div class="'. $class .'">'. $link . $menu .'</div>'."\n";
  }
}

function ahr_sf_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}


/**
 * Function to display the actual advertisement to the screen.  Wrap it in a
 * theme function to make it possible to customize in your own theme.
 */
function ahr_ad_display($group, $display, $method = 'javascript') {
  static $id = -1;

  // Increment counter for displaying multiple advertisements on the page.
  $id++;

  // The naming convention for the id attribute doesn't allow commas.
  $group = preg_replace('/[,]/', '+', $group);

  if ($method == 'jquery') {
    drupal_add_js("$(document).ready(function(){ try { jQuery(\"div#group-id-$id\").load(\"$display\"); } catch (ex) { } });", 'inline');
    return "\n<div class=\"advertisement group-$group\" id=\"group-id-$id\"></div>\n";
  }
  else if ($method == 'raw') {
    return $display;
  }
  else {
    return "\n<div class=\"advertisement group-$group\" id=\"group-id-$group\">$display</div>\n";
  }
}
