<?php	 	
// $Id$
?>
<script type="text/javascript">
  $(function() {
    $("#searchkeyword").keypress(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        $("#search_button").click();
        return false;
      }
      else {
        return true;
      }
    });
  });
</script>
<div id="submenubar">
  <div class="subindent"></div>
  <div id="submenu" class="hlist">
    <!-- secondary navigation -->
    <?php print theme('links', $links) ?>
  </div>
  <div class="searchbox">
    <form method="get" id="searchform" action="">
      <fieldset class="search"><input type="text" class="box" id="searchkeyword" />
        <button class="btn" id="search_button" title="Submit Search" onclick="return submit_search();">Search</button>
      </fieldset>
    </form>
  </div>
</div>