<div id="<?php	 	 print $block->module.'-'.$id ?>" class="<?php print $classes ?>">
  <div class="box">
    <div class="box-content">
    <?php print $block->content; ?>
    </div>

    <?php print $edit_links; ?>
  </div>
  
</div>
