<?php	 	
// $Id$

/*
 * @file The standard copyright block
 * @see template_preprocess_copyright()
 */
?>
<div id="copyright-region" class="clearfix">
  <div class="copyright">
    <div><?php print $copyright_text ?></div>
    <div class="logo2"><?php print $logo_image ?></div>
  </div>
</div>
