<?php	 	
// $Id$

/*
 * @file
 */
$template_path = base_path() . drupal_get_path('theme', 'ahr');
?>

<div id="news" class="box <?php print $classes ?>">
  <div class="boxheader">
    <div class="social"><a href="<?php print url('news/rss.xml') ?>"><img src="<?php print $template_path .'/images/rss-icon.gif' ?>" alt="rss icon" />Rss</a></div>
    <div class="boxicon"><img src="<?php print $template_path .'/images/newsicon.gif' ?>" alt="racing news icon"/><h1><?php print $block->subject ?></h1></div>
  </div>
  <?php print $block->content ?>
  <div class="boxfooter"><span class="h2"><a href="<?php print url('news') ?>">Read More News</a></span></div>

  <?php print $edit_links; ?>
</div>
