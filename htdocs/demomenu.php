<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-742771-1', 'auto');
  ga('send', 'pageview');
</script>


<title>2019 , 2018  US Racing  | Online Horse Betting</title>
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"  />
<meta name="description" content="2019 Kentucky Derby will be on May 4 , May 4th, 2019, Previous Year 2018"  />
<meta name="keywords" content="2019, 2018, May 4th, 2019 , May 4, horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting"  />



  

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/style.css?v1">
<link rel="stylesheet" type="text/css" href="/assets/css/custom_me.css">

<link rel="stylesheet" type="text/css" href="/assets/css/app.css">

<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css">


<link rel="stylesheet" type="text/css" href="/assets/css/plugins.css?v=2">
<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->

<script async type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script>
<script async type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>


<!-- hide nav script -->

<!-- <script>
var senseSpeed = 35;
var previousScroll = 0;
$(window).scroll(function(event){
   var scroller = $(this).scrollTop();
   if (scroller-senseSpeed > previousScroll){
      $("#nav.is_stuck").filter(':not(:animated)').slideUp();
   } else if (scroller+senseSpeed < previousScroll) {
      $("#nav.is_stuck").filter(':not(:animated)').slideDown();
   }
   previousScroll = scroller;
});
</script>-->






<!--<script type="text/javascript" src="/assets/plugins/jquery-1.10.2.min.js"></script>-->
<!--[if lt IE 9]>
	 <link rel="stylesheet" type="text/css" href="/assets/plugins/iealert/alert.css" />
     <script type='text/javascript' src="/assets/js/html5.js"></script>
     <script type="text/javascript" src="/assets/plugins/iealert/iealert.min.js"></script>
     <script type="text/javascript">$(document).ready(function() { $("body").iealert({  closeBtn: true,  overlayClose: true }); });</script>
<![endif]-->

<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>



   <script src="//code.jquery.com/jquery-1.12.0.min.js"></script> 
  <!-- <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

  <!-- <script src="/assets/js/jquery-1.12.0.custom.js"></script> -->
  <!-- <script src="/assets/js/jquery-1.12.0.custom.min.js"></script> -->
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

 
<script src="/assets/js/index-kd-tinysort.js"></script>





	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126793368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126793368-1');
</script>


<style>





@media screen and (max-width:767px) and (min-width:0px) {
.sidr ul li ul li {
    background: #fff;
    line-height: 40px;
    border-bottom: 0px solid #fff;
}

  .bc-nemu-empty{
    border-bottom: 0px solid #fff;
  }
}

</style>

</head>
<body class="not-front  page page-about section-about ">
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->


<!-- test 1 -->
<!-- Top -->
<div id="top" class="top">
 <div class="container">
	 <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>

  <a class="logo logo-lrg" href="/" ><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting"></a>
 <a class="logo logo-sm " href="/" ><img src="/img/usracing-sm.png" alt="Online Hose Betting"></a>
  <!---->
    <div class="login-header">
    

    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref=" rel="nofollow" >
        <span>Bet Now &nbsp;  </span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>  

 </div>
</div><!--/top-->
<style type="text/css" >
.newDivmenu{ min-width: 400px; }
</style>
<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">


	<!-- Toggle NAV 
	<div class="navbar-header">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    <a id="or" class="navbar-toggle collapsed"><span>EXPLORE</span><i class="glyphicon glyphicon-remove"></i></a>
	</div>-->

    <!-- Nav Itms -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav nav-justified">
        <li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
      
   
    <li><a href="/odds" >Today's Entries</a></li>
        <li><a href="/results"  >US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
       <li><a href="/breeders-cup/challenge" >Breeders' Cup Challenge</a></li>
    
       
     
    
                    <li><a href="/racetracks"  >Racetracks</a></li>
            
     <li><a href="/leaders/horses"  >Top Horses</a></li>
      <li><a href="/leaders/jockeys"  >Top Jockeys</a></li>
       <li><a href="/leaders/trainers"  >Top Trainers</a></li>
             
       
      
     
     
                  </ul>
</li>

<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> Odds<i class="fa fa-angle-down"></i></a>
  <div class="dropdown-menu newDivmenu" >
   <div>
           <div class="bc-menu-title">All Odds</div>
		   <ul>
<li><a href="/odds"  >Today's Horse Racing Odds</a></li>
<li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li> 

       
      
  
      
  <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li>
<li><a href="/grand-national"  >Grand National Odds</a></li>
			
            						
 <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li>
<li class="bc-nemu-last"><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>  
<li class="bc-nemu-empty"><a>&nbsp;</a></li>  
<li class="bc-nemu-empty"><a>&nbsp;</a></li>

</ul>
</div>
<div>
        <div class="bc-menu-title">Breeders' Cup Nov. 3rd</div>
			<ul>
                <li><a href="/breeders-cup/classic"  >Breeders' Cup Odds</a> </li>															<li><a href="/breeders-cup/turf"  >BC Turf  Odds</a></li>
        <li><a href="/breeders-cup/distaff"  >BC Distaff  Odds</a></li>
        <li><a href="/breeders-cup/mile" >BC  Mile Odds</a></li>
        <li><a href="/breeders-cup/sprint"  >BC Sprint Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >BC  Filly and Mare Turf Odds</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >BC  Dirt Mile Odds</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >BC  Turf Sprint Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-sprint"  >BC  Filly and Mare Sprint Odds</a></li>       
		</ul>
		</div>



        <div>
        
        <div class="bc-menu-title">Breeders' Cup Nov. 2nd</div>
		<ul>
        <li><a href="/breeders-cup/juvenile"  >BC  Juvenile  Odds</a></li>
        <li><a href="/breeders-cup/juvenile-turf" >BC  Juvenile Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >BC  Juvenile Fillies Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >BC  Juvenile Fillies Odds</a></li>   
		<li class="bc-nemu-last"><a href="/breeders-cup/juvenile-turf-sprint" >BC  Juvenile Turf Sprint Odds</a></li>	
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>         
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>  


  </ul>
  </div>
  </div>
</li><!--Horse Racing News ================================================ -->   
<li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" > News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>




 
	


	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/breeders-cup">Breeders' Cup News</a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>
<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>





	    

      
      
      
    
      
      
      
      
     </ul>
</li>

     
     

<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >Breeders&#039; Cup <i class="fa fa-angle-down"></i></a>
    <ul class="dropdown-menu newnew" id="double">
        <div class="bc-menu-title">Breeders' Cup</div>
        <li><a href="/bet-on/breeders-cup"  >Bet on Breeders' Cup</a></li>
        <li><a href="/breeders-cup" >2018  Breeders' Cup</a></li>
        <li><a href="/breeders-cup/betting" >Breeders&#039; Cup Betting</a></li>
        <li><a href="/news/breeders-cup" >Latest Updates</a>
        <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
        <li><a href="/news/tag/breeders-cup-contenders/"  >Breeders&#039; Contenders</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/challenge" >Breeders&#039; Cup Challenge</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
                <li class="bc-nemu-empty"><a>&nbsp;</a></li> 
 

        <div class="bc-menu-title">Saturday Nov. 3rd</div>

        		<li><a href="/breeders-cup/classic">Classic <!--<span class=“sub-link-hour”>@ 4PM</span>--></a></li>            
															<li><a class="turfBC" href="/breeders-cup/turf"  >Turf </a></li>
        <li><a href="/breeders-cup/distaff"  > Distaff </a></li>
        <li><a href="/breeders-cup/mile" >Mile</a></li>
        <li><a href="/breeders-cup/sprint"  >Sprint</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >Filly and Mare Turf</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >Dirt Mile</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >Turf Sprint</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/filly-mare-sprint"  >Filly and Mare Sprint</a></li>       
	 



        
        
       

 
        <div class="bc-menu-title">Friday Nov. 2nd</div>
        <li><a href="/breeders-cup/juvenile"  > Juvenile </a></li>
        <li><a href="/breeders-cup/juvenile-turf" >Juvenile Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >Juvenile Fillies Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >Juvenile Fillies</a></li>   
		<li class="bc-nemu-last"><a href="/breeders-cup/juvenile-turf-sprint" >Juvenile Turf Sprint</a></li>	
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>         
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>  

      
      
      
      
    </ul>
</li>



<!-- How to Bet ================================================ -->   
     
     <li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->   
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity" >Betting with Bitcoin</a></li>     
    
     
      
     </ul>
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
     <li><a href="/rebates">8% Rebates</a></li>
     <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
     
           </ul>
</li>
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  
    <li><a href="/about" >About Us</a></li> 
    <li><a href="/awards" >Award Winning News</a></li>
        <li><a href="/best-horse-racing-site" >Best Horse Racing Sites</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks</a></li>
		 <li><a href="/rebates">Rebates</a></li>
		 <li><a href="/support">Contact Us</a></li>
                 
        
        
	 <li><a href="/getting-started" >Getting Started</a></li>
    <li><a href="/signup/?m=Join-Today">Join a Racebook!</a></li>
          </ul>
</li>
    	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->


<!-- test 2 -->
<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>Demo page - Please check at the meta tags </h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

Demo Only

<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->

<div class="calltoaction">

<h2>Bet on <br>Horse Racing!</h2>
<h3>Get up to 8% Rebate Paid Daily!</h3>
<span id="object"><i class="animated floating fa fa-map-marker"></i></span>
  <form class="form-signin" id="s_form-signup" method="post" action="" role="form" novalidate="novalidate">
	<div class="form-group account-group"><input type="hidden" name="ref" value="" id="s_ref"></div>
	<div class="form-group account-group"><input type="hidden" name="cta" value=""></div>
	<div class="form-group account-group"><input type="hidden" name="cta_sub" value=""></div>
	<div class="form-group account-group">	
	  <input  class="form-control" placeholder="First Name" name="first_name" required="" type="text" id="s_firstname">
      <small class="error" style="display: none;" id="s_fn_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input  class="form-control" placeholder="Last Name" name="last_name" required="" type="text" id="s_lastname">
      <small class="error" style="display: none;" id="s_ln_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input  class="form-control" placeholder="Email address" required="" name="email" type="email" id="s_email">
      <small class="error" style="display: none;" id="s_em_error">Please fill out this field</small>
      </div>
				
				
	  <div class="form-group login-group">	
	  <input name="step" value="front" type="hidden">
	  <center>
       <button type="submit"  class="btn btn-sm btn-red" id="signupBtn">&nbsp;&nbsp; Sign Up  &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i></button>
      </center></div>
	</form>
</div>

<script type="text/javascript">
$( document ).ready(function() {
$(".calltoaction #object").delay(500).queue(function(next) {
  $(this).addClass("animated bounceInDown").css("visibility", "visible");
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction h2").addClass("animated tada"); 
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction .fa-angle-right").addClass("animated fadeInLeft").css("visibility", "visible");; 
  next();
});
});
</script>
 
 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

<!-- === Footer ================================================== -->
<div class="footer">
<div class="container">

    <div class="row">

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/off-track-betting">Off Track Betting</a></li>
          <li><a href="/online-horse-wagering" >Online Horse Wagering</a></li>
          <li><a href="/odds">Horse Racing Odds</a></li>
          <li><a href="/bet-on-horses">Bet on Horses</a></li>
         <!-- <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li> -->
          <li><a href="/harness-racing">Harness Racing</a></li>
          <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
          <li><a href="/pegasus-world-cup/odds">Pegasus World Cup</a></li>
          <li><a href="/texas">Texas Horse Betting</a></li>
                    <li><a href="/royal-ascot">Royal Ascot Betting</a></li>

        <li><a href="/dubai-world-cup">Dubai World Cup Betting</a></li> 


<!--<li><a href="/santa-anita" >Santa Anita Horse Racing</a></li>
<li><a href="/keeneland" >Keeneland Horse Racing</a>  </li>
<li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
<li><a href="/delta-downs" >Delta Downs Horse Racing</a></li>
<li><a href="http://www.pickthewinner.com" >Pick the Winner</a>  </li>
<li><a href="http://www.breederscupbetting.com" >Breeders Cup Betting</a>  </li>-->
    </li>
    <li><a href="/breeders-cup/betting" >Breeders' Cup Betting</a>
     <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a>
                 </ul>
      </div> <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
	         <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
          <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
          <li><a href="/kentucky-derby/future-wager">Kentucky Derby Future Wager</a></li>
          <li><a href="/bet-on/kentucky-derby">Bet on Kentucky Derby</a></li>
          <li><a href="/road-to-the-roses">Road to the Roses</a></li>
          <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
          <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
          <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
          <li><a href="/twin-spires">Twin Spires Betting</a></li>



        </ul>
      </div>
        <!--/col-md-3-->
	        <div class="col-md-3 margin-bottom-30">
	        <div class="headline">
	          	<h3>Triple Crown</h3>
	        </div>
		        <ul class="list-unstyled margin-bottom-20">
		        <!--	<li><a href="/kentucky-derby" >Kentucky Derby</a></li>-->
		        	<li><a href="/preakness-stakes" >Preakness Stakes</a></li>
		        	<li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>
		        	<li><a href="/preakness-stakes/betting" >Preakness Stakes Betting</a></li>
		        	<li><a href="/preakness-stakes/odds" >Preakness Stakes Odds</a></li>
					<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>
		          	<li><a href="/preakness-stakes/winners" >Preakness Stakes Winners</a></li>
		           <li><a href="/belmont-stakes" >Belmont Stakes</a></li>
		           <li><a href="/bet-on/belmont-stakes" >Bet on Belmont Stakes </a></li>
		        	<li><a href="/belmont-stakes/betting" >Belmont Stakes Betting</a></li>
		        	<li><a href="/belmont-stakes/odds" >Belmont Stakes Odds</a></li>
					<!--<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
		          	<li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> -->
		          	<li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
		        </ul>
			</div>
      <!--/col-md-3-->



      <!--/col-md-3-->
				   <!--<div class="col-md-3 margin-bottom-30">
				        <div class="headline">
				          <h3>US Horse Racing</h3>
				        </div>
				        <ul class="list-unstyled margin-bottom-20">
				        	<li><a href="/about" >About Us</a></li>-->

				          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
				       <li><a href="#" >Our Guarantee</a></li>
				          	<li><a href="/faqs" >FAQs</a></li>
				            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

				<!--<li><a href="/signup/" >Sign Up Today</a></li>
				        </ul>
						</div> -->
      <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>

        <ul class="social-icons">
          <li><a href="https://www.facebook.com/betusracing" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>
          <li><a href="https://twitter.com/betusracing" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
          <li><a href="https://plus.google.com/+Usracingcom" data-original-title="Google+"  title="Google+" class="social_google" rel="publisher"></a></li>
          <!--
          <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
          -->
        </ul>
        <br>   <br>
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
        	<li><a href="/about" >About Us</a></li>
          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
          	<li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
<li><a href="/signup?ref=" rel="nofollow" >Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
<!--<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>-->

        </ul>



      </div> <!--/col-md-3-->

</div><!--/row-->



<div class="row friends">
	<center>Proudly featured on:
<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center>


     <!--
   <div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>

        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
-->
        </div>
</div>
<!-- /row/friends -->


</div><!--/container-->
</div><!--/footer-->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand"><br><p>US Racing is not a racebook or ADW, and does not accept or place wagers of any type. This website does not endorse or encourage illegal gambling. All information provided by this website is for entertainment purposes only. This website assumes no responsibility for the actions by and makes no representation or endorsement of any activities offered by any reviewed racebook or ADW. Please confirm the wagering regulations in your jurisdiction as they vary from state to state, province to province and country to country.  Any use of this information in violation of federal, state, provincial or local laws is strictly prohibited.</p>
        
        <p>Copyright 2018  <a href="/">US Racing</a>, All Rights Reserved.  </span>  | <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/disclaimer" rel="nofollow">Disclaimer</a> | <a href="/responsible-gaming" rel="nofollow" >Responsible Gambling</a> | <a href="/preakness-stakes/betting" >Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting" >Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting" >Kentucky Derby Betting</a>    </div>
    </div><!--/row-->

  </div><!--/container-->
</div><!--/copyright-->

   

<div class="wp-hide">

<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script src="/assets/js/scripts-footer-min.js"></script>
<!--<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript" src="/assets/js/isIndexImported.js"></script>
 -->
<!-- Google Analytics -->
<!--<script type="text/javascript" src="/assets/js/google-analytics.js"></script> -->
<script async src='//www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->

<!-- Facebook Pixel Code -->
<!--<script type="text/javascript" src="/assets/js/facebook-pixel-code.js"></script> -->
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1710529292584739&ev=PageView&noscript=1" alt="Facebook"/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Bing Ads RP  -->
<!--<script type="text/javascript" src="/assets/js/bingAdsRp.js"></script> -->
<noscript><img alt="Bing" src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<!--<script type="text/javascript" src="/assets/js/luckyorange.js"></script> -->

<!--  Unbounce --->

<script src="//d6782401e72a400fba6ff09252aa6316.js.ubembed.com" async></script>
	
<!-- / Unbounce -->


<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->

</div></body>
</html>
