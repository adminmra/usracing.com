<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
       //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
}  ob_start('ob_gzhandler'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<title>US Racing | Online Horse Betting</title>
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
<link rel="canonical" href="https://www.usracing.com/">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"  />
<meta name="description" content="Bet on horses on over 300 racetracks. Join today to get up to a $500 Cash Bonus instantly and qualify for another $150! Online horse betting with rebates up to 8% paid daily. Learn more here at US Racing."  />
<meta name="keywords" content="online horse betting, horse racing, bet on derby, otb, offtrack, kentucky derby betting, preakness stakes, belmont stakes, horse betting, odds"  />
<meta property="og:title" content="US Racing | Online Horse Betting"  />
<meta property="og:url" content="https://www.usracing.com/"  />
<meta property="og:image" content="https://www.usracing.com/img/best-horse-racing-betting-site.jpg"  />
<meta property="og:description" content="Bet on horses on over 300 racetracks. Join today to get up to a $500 Cash Bonus instantly and qualify for another $150! Online horse betting with rebates up to 8% paid daily. Learn more here at US Racing."  />
<meta name="google-site-verification" content="mT_jDUaAW4wPFFflBMaN7yyiEB4x6aKMyCM0qFtcM1E" />
<meta name="robots" content="noindex, nofollow" >
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
<meta name="theme-color" content="#1571ba">
<link rel="manifest" href="/manifest.json">
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="//code.jquery.com" crossorigin>
<link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

<link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php echo '<style type="text/css" >';
//echo file_get_contents("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/style.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/app.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-ps.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-bs.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-becca.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/sprites.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/as-seen-on-new-tpl-min.css"); 
//echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/plugins.min.css"); 
echo '</style>';
/*
<link rel="stylesheet" type="text/css" href="/assets/css/style.min.css?v2.4.1" >
<link rel="stylesheet" type="text/css" href="/assets/css/app.min.css?v=2.4" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/sprites.min.css?v=1.0" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="/assets/css/as-seen-on-new-tpl-min.css?v=1.3" media="none" onload="if(media!='all')media='all'">

*/
?>
<link rel="preload" href="/assets/css/plugins.min.css?v=2.5" as="style">
<link rel="stylesheet" type="text/css" href="/assets/css/plugins.min.css?v=2.5" media="none" onload="if(media!='all')media='all'">

<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->

<!-- <script async type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script> -->
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<!--<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900&v1" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Roboto:400,700&v1' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700&v1' rel='stylesheet' type='text/css'>-->


<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>


<?php
echo '<script type="text/javascript">';
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/js/jquery-1.12.0.min.js");
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/js/jquery-migrate-1.2.1.min.js"); 
echo '</script>'; 
/*
<script  src="/assets/js/jquery-1.12.0.min.js"  ></script>
<script src="/assets/js/jquery-migrate-1.2.1.min.js"  ></script>
<script defer async src="/assets/js/index-kd-tinysort.js"></script>
*/ 
?>



<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" media="none" onload="if(media!='all')media='all'" >
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>   
<link rel="stylesheet" href="/assets/css/slider.min.css?v=1.5" media="none" onload="if(media!='all')media='all'">
<script async type="text/javascript" src="assets/js/index.js"></script>


<script>
$( document ).ready(function() {
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    var is_Edge = navigator.userAgent.indexOf("Edge") > -1;
    var is_explorer= typeof document !== 'undefined' && !!document.documentMode && !is_Edge;

    var img;
    if (!is_safari && !is_Edge && !is_explorer)  {
       img = "/img/index-tv/as-seen-on.webp";         
    } else {
       img = "/img/index-tv/as-seen-on.png";    
    }
    $(".bloombergmarkets").css('background-image', 'url(' + img + ')');
    $(".bloombergtv").css('background-image', 'url(' + img + ')');
    $(".espn").css('background-image', 'url(' + img + ')');
    $(".forbes").css('background-image', 'url(' + img + ')');
    $(".laweekly").css('background-image', 'url(' + img + ')');
    $(".marketwatch").css('background-image', 'url(' + img + ')');
    $(".seekingalpha").css('background-image', 'url(' + img + ')');
    $(".sportsillustrated").css('background-image', 'url(' + img + ')');
    $(".usatoday").css('background-image', 'url(' + img + ')');
    $(".bleacherreport").css('background-image', 'url(' + img + ')');
    $(".sporttechie").css('background-image', 'url(' + img + ')');
});
</script>


<style>
@media screen and (max-width:767px) and (min-width:0px) {
.sidr ul li ul li {
    background: #fff;
    line-height: 40px;
    border-bottom: 0px solid #fff;
}
  .bc-nemu-empty{
    border-bottom: 0px solid #fff;
  }
}
.centerText{
  margin-right: 0px !important;
  text-align: center !important;
}
</style>


<link rel="apple-touch-icon-precomposed" href="/icon/usr-logo-apple-touch-iphone.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/icon/usr-logo-apple-touch-ipad.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/icon/usr-logo-apple-touch-iphone4.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/icon/usr-logo-apple-touch-ipad-retina.png" />


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K74Z827');</script>
<!-- End Google Tag Manager -->

</head><body class="front index-kd">
<!-- ClickTale Top part -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->


<!-- test 1 -->
<!-- Top -->
<div id="top" class="top">
  <div class="container">
    <div class="navbar-header pull-left">
   <a id="or_" class="navbar-toggle collapsed" onclick="openNav()">
   <span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:0px 0px 0px -10px; padding: 10px; padding-bottom: 11px;"></span>
   <i class="glyphicon glyphicon-remove"></i>
 </a>
 </div>
 <a class="logo logo-lrg" href="/" ><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting"></a>
 <a class="logo logo-sm " href="/" ><img src="/img/usracing-sm.png" alt="Online Hose Betting"></a>
   <!---->
      
<style>
    #signupBtn {
        background-color: #e2001a;
        background-image: -webkit-linear-gradient(top, #e2001a, #991a1e);
        background-image: linear-gradient(to bottom, #e2001a, #991a1e);
        border-radius: 3px;
        border: 0;
        display: inline-block;
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        font-weight: 900;
        outline: none;
        text-align: center;
        text-decoration: none;
        min-width: 100px;
        -webkit-transition: .2s;
        transition: .2s;
        transition: .2s
    }

    #signupBtn:hover {
        background: #d6242a;
    }

    #signupBtn:focus, #signupBtn:hover {
        color:#fff;font-weight:900;
        text-decoration:none;
    }

    #signupBtn:active {
        -webkit-transform:scale(0.98);transform:scale(0.98);
    }
</style>

<div class="login-header">
    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref=index" rel="nofollow">
        <span>Bet Now</span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>
    </div>
 </div><!--/top-->
 <!-- Nav -->
 <div id="nav">
   <a id="navigation" name="navigation"></a>
   <div class="navbar navbar-default" role="navigation">
   <div class="container">
      <!-- Nav Itms -->
     <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav nav-justified">
     <span class="closeNavIcon" onclick="closeNav()" style="display: none">
       <i class="fa fa-times" style="font-size: 24px;"></i>
     </span>
          <li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
      <li><a href="/odds" >Today's Entries</a></li>
        <li><a href="/results"  >US Racing Results</a></li>
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li>
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
                             <li><a href="/racetracks"  >Racetracks</a></li>
                                 </ul>
</li><li id="collapse-bc-desktop-odds" class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> Odds<i class="fa fa-angle-down"></i></a>
       <ul class="dropdown-menu" id="double">
       <li><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>
<li><a href="/kentucky-derby/odds"> Kentucky Derby 2021 Odds</a></li> 

 
 
<li><a href="/odds"  >Today's Horse Racing Odds</a></li>





            </ul>
</li>
         
	 

	<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Kentucky Derby<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu  " id="double">

  <li><a href="/kentucky-derby/free-bet">Get Your FREE BET</a></li>
  <li><a href="/kentucky-derby/betting">Kentucky Derby 2021 Betting</a></li>
  <li><a href="/kentucky-derby/odds">Kentucky Derby 2021 Odds</a></li>
  <li><a href="/kentucky-derby/contenders">Kentucky Derby 2021 Contenders</a></li>
  <li><a href="/bet-on/kentucky-derby">Bet on the 2021 Kentucky Derby</a></li>
 <li><a href="/mint-julep-recipe">Mint Julep Recipe</a></li>
 
       
       


 
  </ul>
</li>
<!-- class="nobroder"--->
    	  



<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" > News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
	<li><a href="/news">Latest Racing News </a></li>
		        
    
    
		<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
	<li><a href="/news//tag/kentucky-derby">Kentucky Derby News</a></li>
	
 </ul>
</li>     <li class="dropdown">  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity" >Betting with Bitcoin</a></li>
</ul>
</li><li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
 
     <li><a href="/promos/cash-bonus-20">$500 New Member Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Cash Bonus</a></li>
     <li><a href="/rebates">8% Rebates</a></li>
               </ul>
</li><li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
  <li><a href="/about" >About Us</a></li>
  <li><a href="/awards" >Award Winning News</a></li>
      <li><a href="/best-horse-racing-site" >Best Horse Racing Sites</a></li>
  <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
  <li><a href="/racetracks">Racetracks</a></li>
	<li><a href="/rebates">Rebates</a></li>
	<li><a href="/support">Contact Us</a></li>
                        <li><a href="/getting-started" >Getting Started</a></li>
  <li><a href="/signup/?ref=index" rel="nofollow">Join a Racebook!</a></li>
          </ul>
</li>        </ul>
   <div class="closeNav" onclick="closeNav();"></div>
    </div><!-- /navbar-collapse -->
   </div>
  </div>
 </div> <!--/#nav-->
 
 
 
 <script>
   window.onload = function () {
     resizeCloseNav();
 
     window.addEventListener("resize", function () {
       resizeCloseNav();
       if (window.innerWidth >= 768) {
         closeNav();
       }
     });
   }
   
   function resizeCloseNav() {
       var width = window.screen.width - 260 + "px";
       document.querySelectorAll(".closeNav")[1].style.width = width;
     }
 
     function openNav() {
       document.querySelector("#nav-side").classList.remove("hideNav");
       document.querySelector("#nav-side").classList.add("displayNav");
       document.querySelector("body").style.overflow = "hidden";
       document.querySelectorAll(".closeNavIcon")[1].style.display = "block";
     }
 
     function closeNav() {
       document.querySelector("#nav-side").classList.add("hideNav");
       document.querySelectorAll(".navbar-nav")[1].classList.add("hideBar");
       document.querySelectorAll(".closeNavIcon")[1].style.display = "none";
       document.querySelector("body").style.overflow = "auto";
       setTimeout(() => {
         document.querySelector("#nav-side").classList.remove("displayNav");
         document.querySelectorAll(".navbar-nav")[1].classList.remove("hideBar");
       }, 500);
     }
 </script>
 <!-- test 2 -->

<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite", 
  "name" : "US Racing",
  "url" : "https://www.usracing.com/",
  "potentialAction" : {
    "@type" : "SearchAction",
    "target" : "https://www.usracing.com/?s={search_term}",
    "query-input" : "required name=search_term"
  }                     
}
</script>
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "legalName" : "US Racing",
  "url" : "https://www.usracing.com/",
  "logo" : "https://www.usracing.com/img/usracing-betonhorses.gif",
  "sameAs" : [ "https://www.facebook.com/usracingtoday",
    "https://twitter.com/usracingtoday",
    "https://www.instagram.com/usracingtoday/"]
}
</script>



 	
<style>
    @media (min-width:481px) {
        .swiper-slide.first {
            background-image: url(/img/heros-index/online-horse-betting.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/md_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/horse-racing-bonus2.jpg)
        }
    }

    @media (max-width:480px) {
        .swiper-slide.first {
            background-image: url(/img/heros-index/online-horse-betting-mobile.jpg)
        }

        .swiper-slide.second {
            background-image: url(/img/heros-index/sm_hero_02.jpg)
        }

        .swiper-slide.third {
            background-image: url(/img/heros-index/sm_hero_01.jpg)
        }
    }

    .sliderBtn.tp-caption.btn:active {
        position: static !important;
        margin-top: 25px !important;
    }
</style>


<div class="slider" style="opacity: 0">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide first">
                <div class="slide first">
                    <span class="large-text tp-caption txtLrg">Online Horse Betting</span>
                    <span class="secondary-text txtMed tp-caption desk">Bet on Over 200 Tracks!</span>
                    <div class="secondary-text txtMed tp-caption mobile column" style="display:flex; justify-content:center">
                        <span>Bet on&nbsp;</span>
                        <span>Over 300 Tracks!</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup/?ref=index-200-tracks"><i
                            class="fa fa-thumbs-o-up left"></i>Sign Up Now</i></a>
                </div>
            </div>
             <div class="swiper-slide third">
                <div class="slide third">
                    <span class="large-text tp-caption txtLrg desk">Get up to a $500 Bonus</span>
                    <div class="large-text tp-caption txtLrg mobile column" style="display:flex; justify-content:center">
                        <span>Get up to a &nbsp;</span>
                        <span>$500 Welcome Bonus</span>
                    </div>
                    <span class="secondary-text txtMed tp-caption">and Qualify for Another $150!</span>
                    <a class="sliderBtn tp-caption btn btn-red" href="/signup?ref=index-instant-cash"><i
                            class="fa fa-star left"></i>Sign Up Now</i></a>
                </div>
            </div>
                    
            <div class="swiper-slide second">
                <div class="slide second">
                    <span class="large-text tp-caption txtLrg">Bet the Kentucky Derby</span>
                    <span class="secondary-text txtMed tp-caption desk">Future Odds are Live for Betting!</span>
                    <div class="secondary-text txtMed tp-caption mobile column" style="display:flex; justify-content:center">
                        <span>Future Odds&nbsp;</span>
                        <span>are Live for Betting!</span>
                    </div>
                    <a class="sliderBtn tp-caption btn btn-red" href="/kentucky-derby/odds"><i
                            class="fa fa-thumbs-o-up left"></i>See the Odds</i></a>
                </div>
            </div>
                   </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-next hide-arrow"></div>
        <div class="swiper-button-prev hide-arrow"></div>
    </div>
</div>

<script>
    $(window).load(function () {
        var swiper = new Swiper('.swiper-container', {
            effect: 'fade',
            slidesPerView: 1,
            loop: true,
            lazy: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            }
        });

        setInterval(function () {
            var $sample = $(".slider");
            if ($sample.is(":hover")) {
                $(".swiper-button-next").removeClass("hide-arrow");
                $(".swiper-button-prev").removeClass("hide-arrow");
            } else {
                $(".swiper-button-next").addClass("hide-arrow");
                $(".swiper-button-prev").addClass("hide-arrow");
            }
        }, 200);

        $(".slider").animate({
            opacity: 1
        }, 1200, function () {});
    });
</script>
 
	    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">         
          <h1 class="kd_heading">Horse Betting</h1>
          <h3 class="kd_subheading">Get up to a $500 Instant Cash Bonus</h3>
           
           <p><a href="/signup?ref=index" rel="nofollow">BUSR</a> is the best site to you can bet on horses.  Why?  Because, all new members can get up to a <a href="/cash-bonus-20">$500 welcome bonus</a>!  We know you'll love it so much that you'll also get another $150 for playing.</p>
           <p>You'll also get rebates on all your horse bets up to 8%, paid to your account daily.  Do you want to bet sports and play the casino?  You can do that too.
           <p>With  more benefits and  bet choices than any other place online, what are you waiting for?</p>
                   
          
          <a href="/odds" class="bs-kd_btn">SEE THE LIVE ODDS</a>

        </div>
      </div>
    </section>
      
		
<style>
  .row {
    margin-right: 0px !important;
    margin-left: 0px !important;
  }
</style>


<section class="bs-bonus">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">


        <h2 class="bs-bonus_heading">20% Sign Up Bonus</h2>
        <h3 class="bs-bonus_subheading">Exceptional New Member Bonuses</h3>
        <p>For your first deposit with BUSR, you'll get an additional 20% cash bonus added to
          your account, absolutely free. No Limits!</p>
        <p>Deposit a minimum of $100 and you can qualify to earn an additional $150! It doesn't get any better. <a href="/signup?ref=index" rel="nofollow" style="color:#fff">Join
          today.</a></p><a href="/signup?ref=index" rel="nofollow" class="bs-bonus_btn">CLAIM YOUR BONUS</a>


      </div>
    </div>
  </div>
</section> 
	<section class="card">
    <div class="card_wrap">
        <div class="card_half card_content">
           <a href="/rebates">
                <img class="icon-bet-kentucky-derby-jockeys card_icon" alt="Horse Racing 2020 Rebates Offer"
                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQMAAABKLAcXAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRQE8AAAV4AAHYKaK4AAAAAElFTkSuQmCC">
              </a>
            <h2 class="card_heading">EARN UP TO AN 8% REBATE ON HORSE RACING WAGERS!</h2>
            <h3 class="kd_subheading">Paid Daily</h3>
            <p style="margin-bottom: 10px">Log into your account and you will find a rebate of up to 8% from the previous days horse wagers. Wagering on Win, Place, Show, or Exotics will earn you a rebate.</p>
            <p>Win now with <a href="/signup?ref=index" rel="nofollow">BUSR!</a></p>
            <br>
            <p><a href="/rebates"
            class="btn-xlrg fixed_cta">Learn More</a></p>
            </div>
        <div class="card_half card_hide-mobile">
            <a href="/signup?ref=index" rel="nofollow">                <img data-src="/img/states/rebates.jpg" class="card_img lazyload" alt="Horse Racing 2020 Rebates Offer">            </a>
        </div>
    </div>
</section> 
	    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img data-src="/img/index/mobile-horse-betting.png?v=6" data-src-img="/img/index/mobile-horse-betting.png?v=6"
            alt="Mobile Horse Betting" class="bc-card_img lazyload">
        </div>
        <div class="bc-card_half bc-card_content">
                    <img class="icon-mobile-horse-betting bc-card_icon" alt="Mobile Horsed Betting Icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEoAAABQAQMAAACEU3HPAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AU0AsAAANwAAFHM3wcAAAAAElFTkSuQmCC">
          <h2 class="bc-card_heading">Bet on Your Phone!</h2>
          <p>Take all the races with you! All tracks are available on phone or tablet at the track, at home, everywhere.
          </p>
          <p>Are you out at the bar with your friends? Bet on the game any time, anywhere. Or, while waiting for your
            friends to arrive, how about playing a few hands of blackjack or roulette! <p> BUSR -
              super easy and super fun.</p><a href="/mobile-horse-betting" class="btn-xlrg fixed_cta">See Your Mobile
              Betting Options</a>
        </div>
      </div>
    </section> 
	      <section class="bc-racetracks">
        <div class="container">
          <h2 class="bc-racetracks_title">

            Choose from <strong>300</strong> Racetracks Worldwide
          </h2>
          <ul class="bc-racetracks_list">
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                                    <img class="icon-thoroughbred-racing bc-racetrack_icon" alt="Thoroughbred Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Thoroughbred</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                                    <img class="icon-harness-racing bc-racetrack_icon" alt="Harness Racing Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Harness</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                                    <img class="icon-quarter-horse-racing bc-racetrack_icon" alt="Quarter Horse Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Quarter Horses</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                                    <img class="icon-international-horse-racing bc-racetrack_icon" alt="International  Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">International</h4>
                </div>
              </a></li>
            <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
                <div class="bc-racetrack">
                                    <img class="icon-greyhound-racing bc-racetrack_icon" alt="Greyhound  Racetracks" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGYAAABmAQMAAAADEXYhAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABRJREFUeNpjYBgFo2AUjIJRMFgAAAWUAAF2+LisAAAAAElFTkSuQmCC">
                  <h4 class="bc-racetrack_title">Greyhound</h4>
                </div>
              </a></li>
          </ul>
        </div>
      </section>	    <section class="bc-card bc-card--hre">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content">
                    <img class="icon-horse-racing-experts bc-card_icon" alt="Horse Racing Experts" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAABFAQMAAADw5mMPAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABFJREFUeNpjYBgFo2AUDBgAAAOBAAH0sYD7AAAAAElFTkSuQmCC">
          <h2 class="bc-card_heading">Horse Racing Experts</h2>
          <p>US Racing's Authors and Handicappers will make you a better player. Get the latest news, odds, race reports
            and betting advice from our experienced group of horse racing writers.</p><a href="/news"
            class="bc-card_btn-red">Read the Latest </a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img data-src="/img/index/horse-racing-experts.png" data-src-img="/img/index/horse-racing-experts.png"
            alt="Expert Handicappers" class="bc-card_img lazyload"></div>
      </div>
    </section> 		
	  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Horse Betting Action!</h2>
            <a href="/signup?ref=index" rel="nofollow" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section> 
	
		 
	 
	 
			 
	 
	  
	   
	  
	  


<style type="text/css">
@media (min-width: 768px) {
  .collapse.dont-collapse-sm {
    display: block;
    height: auto !important;
    visibility: visible;
  }
  
}
@media (max-width: 768px) {
  .visible-mb{display:inline-block !important; }
}

@media(min-width: 992px) {
  .list-unstyled {
    min-height: 200px;
  }
  .footer .list-unstyled {
    min-height: 70px;
  }
}
@media (min-width: 769px){
.footer .col-md-4 {
    clear: none;
}
}
</style>

<br><br>
<center>Proudly featured on:
      <div class="container-fluid as-seen-on">
    <div class="row">
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="espn card_4" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="sportsillustrated card_13" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="usatoday card_15" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="bloombergmarkets card_1" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="bloombergtv card_2" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="forbes card_5" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
    </div>
    <div class="row">
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="marketwatch card_9" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-3">
            <a>
                <img class="bleacherreport card_" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-4 col-sm-2">
            <a>
                <img class="laweekly card_8" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-6 col-sm-3">
            <a>
                <img class="sporttechie card_14" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAABkAQMAAACVTr9NAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABpJREFUeNrtwQENAAAAwqD3T20PBxQAAMCPAQzkAAECRwQ7AAAAAElFTkSuQmCC">
            </a>
        </div>
        <div class="footer_card col-xs-6  col-sm-2">
            <a>
                <img class="seekingalpha card_16" alt=""
                    src="data:as-seen-on/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPoAAAA5AQMAAAAlXqRkAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAABdJREFUeNpjYBgFo2AUjIJRMApGAdUAAAdZAAFwryL9AAAAAElFTkSuQmCC">
            </a>
        </div>
    </div>
</div>  </center>
    <div class="footer">
    <div class="container">

    <div class="row">

      <div class="col-md-4 margin-bottom-30">

        <div class="headline">

          <h3 data-toggle="collapse" data-target="#horsebetting" aria-expanded="false" aria-controls="horsebetting">Horse Betting <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>

        </div>
    <div class="collapse dont-collapse-sm" id="horsebetting">
        <ul class="list-unstyled margin-bottom-20">

       <li><a href="/off-track-betting">Off Track Betting</a></li>


       <li><a href="/odds">Horse Racing Odds</a></li>

        <li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>

        <li><a href="/results">Horse Racing Results</a></li>
        
        <li><a href="/bet-on-horses">Bet on Horses</a></li>

       


       

       

              
  




              

                        

       
       

       
       

       
       

       
       

       
       

       



        </ul>  <!-- End of list -->
        </div>
          <div class="headline">

          <h3 data-toggle="collapse" data-target="#upcoming" aria-expanded="false" aria-controls="upcoming">Horse Racing <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>

        </div>
		<div class="collapse dont-collapse-sm" id="upcoming">
        <ul class="list-unstyled margin-bottom-20">
        <li><a href="/bet-on/triple-crown">Bet On Triple Crown</a></li>
        <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>
        <li><a href="/kentucky-oaks/betting">Kentucky Oaks Betting</a></li>
         
         
                                    
          
          
                  
          
                  
        </ul>
		</div>

      </div> <!--/col-md-3-->

      <div class="col-md-4 margin-bottom-30">

 
      <div class="headline">

      <h3 data-toggle="collapse" data-target="#kdfootermenu" aria-expanded="false" aria-controls="kdfootermenu">Kentucky Derby <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>
      </div>
		<div class="collapse dont-collapse-sm" id="kdfootermenu">
             <ul class="list-unstyled margin-bottom-20">
        <li><a href="/kentucky-derby">Kentucky Derby</a></li>

        <li><a href="/kentucky-derby/betting">Kentucky Derby 2021 Betting</a></li>

        <li><a href="/kentucky-derby/odds">Kentucky Derby 2021 Odds</a></li>

       <li><a href="/kentucky-derby/contenders">Kentucky Derby 2021 Contenders</a></li>

       <li><a href="/kentucky-derby/entries">Kentucky Derby 2021 Entries</a></li>

        <li><a href="/kentucky-derby/winners">Kentucky Derby 2021 Winners</a></li>

        <li><a href="/kentucky-derby/results">Kentucky Derby 2021 Results</a></li>

        <li><a href="/bet-on/kentucky-derby">Bet on the 2021 Kentucky Derby </a></li> 




        <li><a href="/kentucky-derby/prep-races">Kentucky Derby 2021 Prep Races</a></li>

        <li><a href="/kentucky-derby/racing-schedule">Kentucky Derby 2021 Racing Schedule</a></li>



      


</ul>
</div>

           <div class="headline">

      <h3 data-toggle="collapse" data-target="#psfootermenu" aria-expanded="false" aria-controls="psfootermenu">Preakness Stakes <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>
      </div>
		<div class="collapse dont-collapse-sm" id="psfootermenu">
       <ul class="list-unstyled margin-bottom-20">


    <li><a href="/preakness-stakes" >Preakness Stakes 2021</a></li>

    <li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>

    <li><a href="/preakness-stakes/betting" >Preakness Stakes 2021 Betting</a></li>

    <li><a href="/preakness-stakes/odds" >Preakness Stakes 2021 Odds</a></li>

    <li><a href="/preakness-stakes/results" >Preakness Stakes 2021 Results</a></li>

    <li><a href="/preakness-stakes/winners" >Preakness Stakes 2021 Winners</a></li>

	</ul>
	</div>

     </div>

        <!--/col-md-3-->

      <div class="col-md-4 margin-bottom-30">

        <div class="headline">
          <h3 data-toggle="collapse" data-target="#howtobetfootermenu" aria-expanded="false" aria-controls="howtobetfootermenu">How To Bet <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>
        </div>
        <div class="collapse dont-collapse-sm" id="howtobetfootermenu">
          <ul class="list-unstyled margin-bottom-20">
            <li><a href="/horse-betting/exacta" >Exacta Bet</a></li>
            <li><a href="/horse-betting/trifecta" >Trifecta Bet</a></li>
            <li><a href="/horse-betting/box-bet" >Box Bet</a></li>
            <li><a href="/past-performances">Past Perfomances</a></li>
            <li><a href="/horse-betting/key-a-horse" >Key a Horse</a></li>
            <li><a href="/horse-betting/across-the-board" >Across the Board</a></li>
          </ul>
        </div>

        <div class="headline">
           <h3 data-toggle="collapse" data-target="#bsfootermenu" aria-expanded="false" aria-controls="bsfootermenu">Belmont Stakes <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>
        </div>
        <div class="collapse dont-collapse-sm" id="bsfootermenu">
          <ul class="list-unstyled margin-bottom-20">
            <li><a href="/belmont-stakes" >Belmont Stakes 2021 </a></li>
            <li><a href="belmont-stakes/odds" >Belmont Stakes 2021 Odds</a></li>
            <li><a href="belmont-stakes/betting" >Belmont Stakes 2021 Betting</a></li>
          </ul>
        </div>

        
         
        
 
        <div class="headline">
          <h3 data-toggle="collapse" data-target="#usracingfootermenu" aria-expanded="false" aria-controls="usracingfootermenu">US  Racing <span class="icon hidden visible-mb"><i class="fa fa-angle-down" aria-hidden="true" ></i></span></h3>
        </div>
        <div class="collapse dont-collapse-sm" id="usracingfootermenu">
          <ul class="list-unstyled margin-bottom-20">
            <li><a href="/about" >About Us</a></li>


            <li><a href="/signup?ref=index" rel="nofollow" class="footSign"  >Sign Up Today</a></li>

            <li><a href="/usracing-reviews">USRacing.com Reviews</a></li>


          </ul>
        </div>
      </div> <!--/col-md-3-->

</div><!--/row-->


</div>
                    </div>
    </div>
    <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <span class="brand"><br>
            <p>US Racing is not a racebook or ADW, and does not accept or place wagers of any
              type. This website does not endorse or encourage illegal gambling. All information provided by this
              website is for entertainment purposes only. This website assumes no responsibility for the actions by and makes no
              representation or endorsement of any activities offered by any reviewed racebook or ADW. Please confirm
              the wagering regulations in your jurisdiction as they vary from state to state, province to province and
              country to country. Any use of this information in violation of federal, state, provincial or local laws is
              strictly prohibited.</p>
            <p>Copyright 2021 <a href="/">US Racing</a>, All Rights Reserved.
          </span> | <a href="/privacy" rel="nofollow">Privacy Policy</a> | <a href="/terms" rel="nofollow">Terms and
            Conditions</a> | <a href="/disclaimer" rel="nofollow">Disclaimer</a> | <a href="/responsible-gaming"
            rel="nofollow">Responsible Gambling</a> | <a href="/preakness-stakes/betting">Preakness Stakes Betting</a> |
          <a href="/belmont-stakes/betting">Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting">Kentucky
            Derby Betting</a> </div>
      </div>
          </div>
      </div>
    <div class="wp-hide">

<link rel="stylesheet" type="text/css" href="/assets/css/rs_v5.min.css?v=1.0" media="screen" onload="if(media!='all')media='all'" />

<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script  defer type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script  defer src="/assets/js/scripts-footer-min.js"></script>
<script type="text/javascript" src="/assets/js/lazysizes.min.js" async /></script> 	
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->

	
	
	
	</div>
  	<script type="text/javascript" src="/assets/js/ref-replace.js" ></script>  
  </body>
</html>
<?php   ob_end_flush(); ?>
