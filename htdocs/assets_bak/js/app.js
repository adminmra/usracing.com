var App = function () {
  function handleIEFixes() {
    //fix html5 placeholder attribute for ie7 & ie8
    if (jQuery.browser.msie && jQuery.browser.version.substr(0, 1) < 9) { // ie7&ie8
      jQuery('input[placeholder], textarea[placeholder]').each(function () {
        var input = jQuery(this);
        jQuery(input).val(input.attr('placeholder'));
        jQuery(input).focus(function () {
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
        });
        jQuery(input).blur(function () {
          if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.val(input.attr('placeholder'));
          }
        });
      });
    }
  }

  function handleBootstrap() {
    jQuery('.dateCarousel').carousel({
      interval: 4000,
      pause: 'hover'
    });
    jQuery('.dateCarousel').find('.item:first').addClass('active');
    jQuery('.tooltips').tooltip();
    //jQuery('.popovers').popover();
  }
  return {
    init: function () {
      handleBootstrap();
      handleIEFixes();
	  
// Scroll to TOP
var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="/img/up.png" style="width:51px; height:42px" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:false,shouldvisible:false},scrollup:function(){if(!this.cssfixedsupport)this.$control.css({opacity:0});var e=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);if(typeof e=="string"&&jQuery("#"+e).length==1)e=jQuery("#"+e).offset().top;else e=0;this.$body.animate({scrollTop:e},this.setting.scrollduration)},keepfixed:function(){var e=jQuery(window);var t=e.scrollLeft()+e.width()-this.$control.width()-this.controlattrs.offsetx;var n=e.scrollTop()+e.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:t+"px",top:n+"px"})},togglecontrol:function(){var e=jQuery(window).scrollTop();if(!this.cssfixedsupport)this.keepfixed();this.state.shouldvisible=e>=this.setting.startline?true:false;if(this.state.shouldvisible&&!this.state.isvisible){this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]);this.state.isvisible=true}else if(this.state.shouldvisible==false&&this.state.isvisible){this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]);this.state.isvisible=false}},init:function(){jQuery(document).ready(function(e){var t=scrolltotop;var n=document.all;t.cssfixedsupport=!n||n&&document.compatMode=="CSS1Compat"&&window.XMLHttpRequest;t.$body=window.opera?document.compatMode=="CSS1Compat"?e("html"):e("body"):e("html,body");t.$control=e('<div id="topcontrol">'+t.controlHTML+"</div>").css({position:t.cssfixedsupport?"fixed":"absolute",bottom:t.controlattrs.offsety,right:t.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll Back to Top"}).click(function(){t.scrollup();return false}).appendTo("body");if(document.all&&!window.XMLHttpRequest&&t.$control.text()!="")t.$control.css({width:t.$control.width()});t.togglecontrol();e('a[href="'+t.anchorkeyword+'"]').click(function(){t.scrollup();return false});e(window).bind("scroll resize",function(e){t.togglecontrol()})})}};scrolltotop.init()
	  
// Add left nav Icon to Nav if left-nav exists 
if ( $('#left-nav').length ) { 
$( "#nav .navbar-header" ).prepend( "<a class='navbar-left' id='ol'  href='#left-nav' ><i class='fa fa-bars'></i><i class='glyphicon glyphicon-remove'></i></a>" );
}

// LOGIN FORM -------------------------------------- //
      $('select').focus(function () {
        $(this).addClass("selected");
      });
      $('select').blur(function () {
        if (this.value === '0') {
          $(this).removeClass("selected");
        }
      });
      $("#loginButton").click(function () {
        $(function () {
          var timeOffset = new Date().getTimezoneOffset() / 60;
          $("#timezone").val(timeOffset);
        });
        var loginForm = $("#loginForm");
        var account = $("#account").val();
        var pin = $("#pin").val();
        var state = $("#state").val();
        var valid = true;
        if (account == "") {
          $("#account").parent().addClass("required")
          valid = false;
        }
        if (pin == "") {
          $("#pin").parent().addClass("required")
          valid = false;
        }
        if (state == "0") {
          $("#state").parent().addClass("required")
          valid = false;
        }
        if (valid) {
          //$("#loginButton").hide();
          $("#loginStatus").show();
          loginForm.submit();
        }
      });
// STICKY ELEMENTS -------------------------------- //
      var aboveHeight = 240;
      $(window).scroll(function (e) {
        if ($(window).scrollTop() > aboveHeight) {
          $("#top, #nav, #left-nav-btn").addClass("is_stuck");
        } else {
          $("#top, #nav, #left-nav-btn").removeClass("is_stuck");
        }
		e.preventDefault();
      });
// SIDR MENUS -------------------------------------- //
      $('#ol').sidr({
        name: 'left-menu',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
          $("#ol i.fa").hide();
          $("#ol i.glyphicon").show();
          $(".container").on('click touchstart', function () {
            $.sidr('close', 'left-menu');
          });
          $(window).resize(function () {
            $.sidr('close', 'left-menu');
          });
          $(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              $.sidr('close', 'left-menu');
            }
          });
        },
        onClose: function () {
          $("#ol i.glyphicon").hide();
          $("#ol i.fa").show();
          $('.sidr li.active ul').slideUp('normal');
          $('.sidr li').removeClass('active');
        }
      });
      $('#ol-left').sidr({
        name: 'left-menu-main',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
			$('#left-menu-main').perfectScrollbar();
          $("#ol-left i.fa, #ol-left span.more").hide();
          $("#ol-left i.glyphicon, #ol-left span.exit").show().css('display', 'block');
          $("#ol-left").addClass("active");
          $(".container").on('click touchstart', function () {
            $.sidr('close', 'left-menu-main');
          });
          $(window).resize(function () {
            $.sidr('close', 'left-menu-main');
          });
          $(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              $.sidr('close', 'left-menu-main');
            }
          });
        },
        onClose: function () {
		  $('#left-menu-main').perfectScrollbar('update');
          $("#ol-left i.glyphicon, #ol-left span.exit").hide();
          $("#ol-left i.fa, #ol-left span.more").show().css('display', 'block');
          $('.sidr li.active ul').slideUp('normal');
          $("#ol-left").removeClass("active");
          $('.sidr li').removeClass('active');
        }
      });
      $('#or').sidr({
        name: 'nav-side',
        side: 'right',
        speed: 300,
        source: '.navbar-collapse',
        renaming: false,
        onOpen: function () {
		  $('#nav-side').perfectScrollbar();	
          $("#or span").hide();
          $("#or i.glyphicon").show();
          $(".container").on('click touchstart', function () {
            $.sidr('close', 'nav-side');
          });
          $(window).resize(function () {
            $.sidr('close', 'nav-side');
          });
          $(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              $.sidr('close', 'nav-side');
            }
          });
        },
        onClose: function () {
		  $('#nav-side').perfectScrollbar('update');	
          $("#or i.glyphicon").hide();
          $("#or span").show();
          $('.sidr li.active ul').slideUp('normal');
          $('.sidr li').removeClass('active');
        }
      });
      // Sidr Accordian           
      $(".sidr a").click(function () {
        var link = $(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;
        closest_ul.find("ul").slideUp(function () {
          if (++count == closest_ul.find("ul").length)
            parallel_active_links.removeClass("active");
        });
        if (!link_status) {
          closest_li.children("ul").slideDown();
          closest_li.addClass("active");
		  $('.sidr').perfectScrollbar('update');
        }
      });
    },
// SLIDERS  -------------------------------------- //
    initRoyalSlider: function () {
      $('#content-slider-1').royalSlider({
        arrowsNav: true,
        autoPlay: {
          enabled: true,
          delay: 3000,
          pauseOnHover: true
        },
        arrowsNavAutoHide: false,
        fadeinLoadedSlide: true,
        controlNavigationSpacing: 0,
        controlNavigation: 'none',
        blockLoop: true,
        keyboardNavEnabled: true,
        imageScaleMode: 'fill',
        imageAlignCenter: true,
        autoScaleSlider: true,
        slidesSpacing: 0,
        loop: true,
        loopRewind: true,
        numImagesToPreload: 2,
        autoScaleSlider: true,
        globalCaption: true,
        controlsInside: false
      });
      $('#main-slider').royalSlider({
        arrowsNav: true,
        loop: true,
        usePreloader: true,
        autoPlay: {
          enabled: true,
          delay: 4000,
          pauseOnHover: true
        },
        transitionSpeed: 600,
        keyboardNavEnabled: true,
        controlsInside: true,
        imageScaleMode: 'fill',
        arrowsNavAutoHide: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 1920,
        autoScaleSliderHeight: 600,
        controlNavigation: 'bullets',
        thumbsFitInViewport: false,
        navigateByClick: false,
        numImagesToPreload: 1,
        startSlideId: 0,
        transitionType: 'slide',
        globalCaption: false,
        slidesSpacing: 0,
        deeplinking: {
          enabled: true,
          change: false
        },
        imgWidth: 1920,
        imgHeight: 600
      });
      $('#featured').royalSlider({
        arrowsNav: false,
        fadeinLoadedSlide: true,
        controlNavigationSpacing: 0,
        controlNavigation: 'thumbnails',
        thumbs: {
          autoCenter: false,
          fitInViewport: true,
          orientation: 'vertical',
          spacing: 0,
          paddingBottom: 0
        },
        keyboardNavEnabled: true,
        imageScaleMode: 'fill',
        imageAlignCenter: true,
        slidesSpacing: 0,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 2,
        video: {
          autoHideArrows: true,
          autoHideControlNav: false,
          autoHideBlocks: true
        },
        autoScaleSlider: true,
        autoScaleSliderWidth: 730,
        autoScaleSliderHeight: 360,
        imgWidth: 640,
        imgHeight: 360
      });
    },
// FANCYBOX -------------------------------------- //
    initFancybox: function () {
      jQuery(".fancybox-button").fancybox({
        groupAttr: 'data-rel',
        prevEffect: 'none',
        nextEffect: 'none',
        closeBtn: true,
        helpers: {
          title: {
            type: 'inside'
          }
        }
      });
      // youtube video support
      $(".yt-image-link").fancybox({
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
          media: true,
        },
        youtube: {
          autoplay: 1,
          rel: 0,
          showinfo: 0,
          controls: 1
        }
      });
      $(".yt-title-link").fancybox({
        closeBtn: true,
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
          media: true,
        },
        youtube: {
          autoplay: 1,
          rel: 0,
          showinfo: 0,
          controls: 1
        }
      });
    },
  };
}();