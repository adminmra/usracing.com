<?php
	//header('Content-Type:text/plain');
    $httphost = $_SERVER['HTTP_HOST'];
    if($httphost == 'usracing.com' || $httphost == 'www.usracing.com') {
?>
User-agent: Googlebot
Allow: /

User-agent: Googlebot-News
Allow: /

User-agent: *
Disallow: /iframe
Disallow: /ec_checker

User-agent: Twitterbot
Allow: /
Sitemap: https://www.usracing.com/sitemap.xml
<?php
    }
    else {
?>
User-agent: *
Disallow: /
<?php
    }
?>