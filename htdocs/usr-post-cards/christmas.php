<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>USR Christmas</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-K74Z827');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>

    <style>
        .topBanner {
            background-image: url("media/christmas/cover.jpg");
        }

        @media (min-width: 320px) and (max-width: 480px) {
            .topBanner {
                background-image: url("media/christmas/cover_mobile.jpg");
            }
        }
    </style>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="topBanner"></div>
    <main class="container">
        <h2 class="w-100 text-center mt-4 mb-4">Leave a nice message to your friends and family.</h2>
        <div class="row canvas">
            <div>
                <div id="canvasWrapper"></div>
            </div>
            <div class="col form">
                <div>
                    <label for="from">From:</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="from" aria-describedby="basic-addon3" maxlength="25"
                            placeholder="Write here!">
                    </div>
                </div>
                <div>
                    <label for="to">to:</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="to" aria-describedby="basic-addon3" maxlength="25"
                            placeholder="Write here!">
                    </div>
                </div>
                <div>
                    <label>Message:</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="message1" aria-describedby="basic-addon3"
                            maxlength="33" placeholder="Line one">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="message2" aria-describedby="basic-addon3"
                            maxlength="33" placeholder="Line two">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="message3" aria-describedby="basic-addon3"
                            maxlength="33" placeholder="Line three">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="message4" aria-describedby="basic-addon3"
                            maxlength="33" placeholder="Line Four">
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" id="message5" aria-describedby="basic-addon3"
                            maxlength="33" placeholder="Line Five">
                    </div>
                </div>
            </div>
        </div>
        <div class="d-flex flex-column space-between align-items-center mt-4">
            <h1 class="text-center">Save & share this postcard!</h1>
            <div class="d-flex flex-row">
                <a id="shareFacebook" class="export d-flex flex-column align-items-center mr-5">
                    <div id="fb-load" class="loader"></div>
                    <img id="fb-img" src="http://image-service.unbounce.com/https%3A%2F%2Fapp.unbouncepreview.com%2Fpublish%2Fassets%2Fea5778e6-1fac-48f0-858a-7ea8a5f8f125%2Fc80a014e-fb-logo_000000000000000000001.png">
                    <p class="download">Facebook</p>
                </a>
                <a id="shareTwitter" class="export d-flex flex-column align-items-center mr-5">
                    <div id="tw-load" class="loader"></div>
                    <img id="tw-img" src="http://image-service.unbounce.com/https%3A%2F%2Fapp.unbouncepreview.com%2Fpublish%2Fassets%2F225dad36-5853-4fd1-a73a-90d6efabcf12%2F27c49525-tw-logo_000000000000000000001.png">
                    <p class="download">Twitter</p>
                </a>
                <a id="export" class="export d-flex flex-column align-items-center">
                    <img src="//d9hhrg4mnvzow.cloudfront.net/m.usracing.com/thanksgiving-1/528fffc6-download-logo_000000000000000000001.png">
                    <p class="download">Download</p>
                </a>
            </div>
        </div>
    </main>
    <a href="https://www.betusracing.ag/signup?ref=christmas-signup-bonus" target="_blank"
        class="d-flex justify-content-center bottomBanner mt-2">
        <img id="banner-desktop" src="media/christmas/banner.jpg" alt="">
        <img id="banner-mobile" src="media/christmas/banner_mobile.jpg" alt="">
    </a>
    <footer class="d-flex flex-column justify-content-center align-items-center">
        <a href="/" class="home">Continue to usracing.com</a>
        <span class="copyright">Copyright 2019 USRacing.com, All Rights Reserved. </span>
    </footer>

    <script>
        var generated_tweet = encodeURIComponent("@USRacingToday Merry Christmas!");
    </script>
    <script src="js/script.js"></script>
    <script>
        var url_string = window.location.href
        var url = new URL(url_string);
        var param = url.searchParams.get("i");
        if (param == 1)
            image.src = 'media/christmas/usr_xmas-1.jpg';
        else if (param == 2)
            image.src = 'media/christmas/usr_xmas-2.jpg';
        else if (param == 3)
            image.src = 'media/christmas/usr_xmas-3.jpg';
        else if (param == 4)
            image.src = 'media/christmas/usr_xmas-4.jpg';
        else
            image.src = 'media/christmas/usr_xmas-1.jpg';
    </script>
</body>

</html>