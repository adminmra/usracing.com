// CAN\NVAS.js plugin
// ninivert, december 2016
(function (window, document) {
  /**
   * CAN\VAS Plugin - Adding line breaks to canvas
   * @arg {string} [str=Hello World] - text to be drawn
   * @arg {number} [x=0]             - top left x coordinate of the text
   * @arg {number} [y=textSize]      - top left y coordinate of the text
   * @arg {number} [w=canvasWidth]   - maximum width of drawn text
   * @arg {number} [lh=1]            - line height
   * @arg {number} [method=fill]     - text drawing method, if 'none', text will not be rendered
   */

  CanvasRenderingContext2D.prototype.drawBreakingText = function (str, x, y, w, lh, method) {
    // local variables and defaults
    var textSize = parseInt(this.font.replace(/\D/gi, ''));
    var textParts = [];
    var textPartsNo = 0;
    var words = [];
    var currLine = '';
    var testLine = '';
    str = str || '';
    x = x || 0;
    y = y || 0;
    w = w || this.canvas.width;
    lh = lh || 1;
    method = method || 'fill';

    // manual linebreaks
    textParts = str.split('\n');
    textPartsNo = textParts.length;

    // split the words of the parts
    for (var i = 0; i < textParts.length; i++) {
      words[i] = textParts[i].split(' ');
    }

    // now that we have extracted the words
    // we reset the textParts
    textParts = [];

    // calculate recommended line breaks
    // split between the words
    for (var i = 0; i < textPartsNo; i++) {

      // clear the testline for the next manually broken line
      currLine = '';

      for (var j = 0; j < words[i].length; j++) {
        testLine = currLine + words[i][j] + ' ';

        // check if the testLine is of good width
        if (this.measureText(testLine).width > w && j > 0) {
          textParts.push(currLine);
          currLine = words[i][j] + ' ';
        } else {
          currLine = testLine;
        }
      }
      // replace is to remove trailing whitespace
      textParts.push(currLine);
    }

    // render the text on the canvas
    for (var i = 0; i < textParts.length; i++) {
      if (method === 'fill') {
        this.fillText(textParts[i].replace(/((\s*\S+)*)\s*/, '$1'), x, y + (textSize * lh * i));
      } else if (method === 'none') {
        return {
          'textParts': textParts,
          'textHeight': textSize * lh * textParts.length
        };
      } else {
        console.warn('drawBreakingText: ' + method + 'Text() does not exist');
        return false;
      }
    }

    return {
      'textParts': textParts,
      'textHeight': textSize * lh * textParts.length
    };
  };
})(window, document);

var canvas = document.createElement('canvas');
var canvasWrapper = document.getElementById('canvasWrapper');
canvasWrapper.appendChild(canvas);
canvas.width = 500;
canvas.height = 500;
var ctx = canvas.getContext('2d');
var textTop = '';
var textBottom = '';
var textMessage1 = '';
var textMessage2 = '';
var textMessage3 = '';
var textMessage4 = '';
var textMessage5 = '';
var textSizeTop = 22;
var textSizeBottom = 22;
var textSizeMessage = 16;
var image = document.createElement('img');

image.onload = function (ev) {
  // delete and recreate canvas do untaint it
  canvas.outerHTML = '';
  canvas = document.createElement('canvas');
  canvasWrapper.appendChild(canvas);
  ctx = canvas.getContext('2d');
  draw();
};

document.getElementById('from').oninput = function (ev) {
  textTop = this.value;
  draw();
};

document.getElementById('to').oninput = function (ev) {
  textBottom = this.value;
  draw();
};

document.getElementById('message1').oninput = function (ev) {
  textMessage1 = this.value;
  draw();
};

document.getElementById('message2').oninput = function (ev) {
  textMessage2 = this.value;
  draw();
};

document.getElementById('message3').oninput = function (ev) {
  textMessage3 = this.value;
  draw();
};

document.getElementById('message4').oninput = function (ev) {
  textMessage4 = this.value;
  draw();
};

document.getElementById('message5').oninput = function (ev) {
  textMessage5 = this.value;
  draw();
};

document.getElementById('export').onclick = function () {
  var img = canvas.toDataURL('image/jpeg');
  var link = document.createElement("a");
  link.download = 'usr-postcard';
  link.href = img;
  link.click();
};

document.getElementById('shareTwitter').onclick = function () {
  document.querySelector('#tw-load').style.display = 'block';
  document.querySelector('#tw-img').style.display = 'none';

  var img = canvas.toDataURL('image/jpeg');
  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Client-ID 97a0a25ec114a79");

  var formdata = new FormData();
  formdata.append("image", img.split(',')[1]);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: formdata,
    redirect: 'follow'
  };

  fetch('https://api.imgur.com/3/image', requestOptions)
    .then(response => response.text())
    .then(result => tweet(JSON.parse(result).data.link))
    .catch(error => console.log('error', error));
};

function tweet(response) {
  document.querySelector('#tw-img').style.display = 'block';
  document.querySelector('#tw-load').style.display = 'none';

  var imgLink = response.replace('.jpg', '');
  var start_text = 'https://twitter.com/intent/tweet?text=';
  var generated_url = "&url=" + encodeURIComponent(imgLink);
  var url = `${start_text}${generated_tweet}${generated_url}`;
  var popup = window.open(url, 'Share on Twitter', 'height=533,width=533');
  if (window.focus) {
    popup.focus()
  }
}

document.getElementById('shareFacebook').onclick = function () {
  document.querySelector('#fb-load').style.display = 'block';
  document.querySelector('#fb-img').style.display = 'none';

  var img = canvas.toDataURL('image/jpeg');
  var myHeaders = new Headers();
  myHeaders.append("Authorization", "Client-ID 23b0fefe279b52f");

  var formdata = new FormData();
  formdata.append("image", img.split(',')[1]);

  var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: formdata,
    redirect: 'follow'
  };

  fetch('https://api.imgur.com/3/image', requestOptions)
    .then(response => response.text())
    .then(result => shareFb(JSON.parse(result).data.link))
    .catch(error => console.log('error', error));
};

function shareFb(response) {
  document.querySelector('#fb-img').style.display = 'block';
  document.querySelector('#fb-load').style.display = 'none';

  var imgLink = response.replace('.jpg', '');
  var url = `https://www.facebook.com/sharer/sharer.php?u=${imgLink}`;
  var popup = window.open(url, 'Share on Facebook', 'height=533,width=533');
  if (window.focus) {
    popup.focus()
  }
}

function draw() {
  // uppercase the text
  var top = textTop;
  var bottom = textBottom;
  var message1 = textMessage1;
  var message2 = textMessage2;
  var message3 = textMessage3;
  var message4 = textMessage4;
  var message5 = textMessage5;


  // set appropriate canvas size
  canvas.width = image.width;
  canvas.height = image.height;

  // draw the image
  ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
  ctx.lineWidth = canvas.width * 0.004;

  // styles
  ctx.fillStyle = '#fff';

  // draw top text
  ctx.font = "italic bold 22px Lato";
  ctx.drawBreakingText(top, 550, 159, null, 1, 'fill');

  // draw bottom text
  ctx.font = "italic bold 22px Lato";
  ctx.drawBreakingText(bottom, 550, 195, null, 1, 'fill');

  //draw message text
  ctx.font = "italic bold 16px Lato";
  ctx.drawBreakingText(message1, 550, 240, null, 1, 'fill');

  ctx.font = "italic bold 16px Lato";
  ctx.drawBreakingText(message2, 550, 260, null, 1, 'fill');

  ctx.font = "italic bold 16px Lato";
  ctx.drawBreakingText(message3, 550, 280, null, 1, 'fill');

  ctx.font = "italic bold 16px Lato";
  ctx.drawBreakingText(message4, 550, 300, null, 1, 'fill');

  ctx.font = "italic bold 16px Lato";
  ctx.drawBreakingText(message5, 550, 320, null, 1, 'fill');
}