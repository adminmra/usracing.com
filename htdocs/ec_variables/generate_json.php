<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$us_switchies = '/home/ah/allhorse/public_html/control-switch/index.php';
$betus_switchies['variableName'][]= 'dateNow';
$betus_switchies['startTIME'][]=''; 
$betus_switchies['endTIME'][]='';

$server = 'usracing.com';
$list = [];
include_once $us_switchies;
$SwitchVariable['variableName'][]= 'dateNow';
$SwitchVariable['startTIME'][]=''; 
$SwitchVariable['endTIME'][]='';

foreach (get_dir_contents('/home/ah/usracing.com/smarty/templates') as $result) {
    $ext = pathinfo($result, PATHINFO_EXTENSION);
    if ((string) $ext == 'tpl' || (string) $ext == 'php') {
        if (!is_a_backup($result)) {
            $j = 0;
            foreach ($SwitchVariable['variableName'] as $variableName) {
                $list = get_list($variableName, $result, $SwitchVariable, $ext, $j);
                $j++;
            }
        }
    }
}

function is_a_backup($result)
{
    $filename = basename($result);
    $foo = explode(".", $filename);
    foreach ($foo as $each) {
        if ($each == 'bk' || $each == 'bck' || $each == 'backup' || $each == 'bx' || $each == 'demo') {
            return true;
        }
        if (1 === preg_match('~[0-9]~', $each)) {
            return true;
        }
    }
    $foo = explode("-", $filename);
    foreach ($foo as $each) {
        if ($each == 'bk' || $each == 'bck' || $each == 'backup' || $each == 'bx' || $each == 'demo') {
            return true;
        }
        if (1 === preg_match('~[0-9]~', $each)) {
            return true;
        }
    }
    $foo = explode("_", $filename);
    foreach ($foo as $each) {
        if ($each == 'bk' || $each == 'bck' || $each == 'backup' || $each == 'bx' || $each == 'demo') {
            return true;
        }
        if (1 === preg_match('~[0-9]~', $each)) {
            return true;
        }
    }
}

function get_list($search, $result, $SwitchVariable, $ext, $index)
{
    global $list;
    $search = '$' . $search;

    if ($handle = fopen($result, 'r')) {
        $count = 0;
        while (($line = fgets($handle, 4096)) !== false) {
            $count++;
            if (strpos($line, $search) !== false) {
                $foo = new stdClass();
                $foo->path = $result;
                $foo->line = $count;
                $foo->var = $search;
                foreach ($SwitchVariable['variableName'] as $variableName) {
                    $foo->start_time = $SwitchVariable['startTIME'][$index];
                    $foo->end_time = $SwitchVariable['endTIME'][$index];
                }
                if ($ext == 'php') {
                    $init = "($search){";
                    $end = "}";
                } else {
                    $init = "$search";
                    $end = "/if";
                }
                $block = get_block($result, $init, $end, $count);
                $foo->block = htmlspecialchars($block);
                $bool = false;
                foreach ($list as $element) {
                    if ($element->path == $foo->path && $element->line == $foo->line) {
                        if (strlen($element->var) < strlen($foo->var)) {
                            $bool = true;
                            $element->path = $foo->path;
                            $element->line = $foo->line;
                            $element->var = $foo->var;
                            $element->start_time = $foo->start_time;
                            $element->end_time = $foo->end_time;
                            $element->block = $foo->block;
                        }
                    }
                }
                if (!$bool) {
                    array_push($list, $foo);
                }
            }

        }
        fclose($handle);
    }
    return $list;
}

function get_dir_contents($dir, &$results = array())
{
    $files = scandir($dir);

    foreach ($files as $key => $value) {
        $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
        if (!is_dir($path)) {
            $results[] = $path;
        } else if ($value != '.' && $value != '..') {
            get_dir_contents($path, $results);
            $results[] = $path;
        }
    }

    return $results;
}

function get_block($file, $start, $end, $count)
{
    $lines = file($file);
    $string = '';
    $index = 1;
    foreach ($lines as $line) {
        if ($index >= $count) {
            $string = $string . $line;
        }
        $index++;
    }
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) {
        return '';
    }

    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

$fp = fopen('/home/ah/usracing.com/htdocs/ec_variables/result.json', 'w');
fwrite($fp, json_encode($list));
fclose($fp);
unset($SwitchVariable);

ob_start();
include "/home/ah/usracing.com/htdocs/ec_variables/result.json";
$json = ob_get_clean();
echo "usracing.com JSON updated\n";
echo '<pre>';
echo "$json\n";
?>

<html lang="en">
<title>EC Variables - JSON</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
