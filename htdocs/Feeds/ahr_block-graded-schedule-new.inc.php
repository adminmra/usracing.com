<?php	 	
define("SMARTYPATH", "/home/ah/usracing.com/smarty");
define("SITEABSOLUTEURL", "http://www.usracing.com/"); //needs the final slash

if (!function_exists('checkWordsNComplete')) {

	function checkWordsNComplete($cad) {
		$cad_arr = explode(" ", $cad);
		$wc_words = count($cad_arr);
		$new_cad = "";
		for ($i = 0; $i < $wc_words; ++$i) {
			$cur = $cad_arr[$i];
			switch ($cur) {
				case "S": case "s":
					$cur = "Stakes";
					break;
				case "H": case "h":
					$cur = "Handicap";
					break;
			}
			$new_cad .= $cur;
			if($i+1< $wc_words)
				$new_cad .= " ";
		}
		return $new_cad;
	}

}

if (!function_exists('getTrackName')) {

	function getTrackName(&$track) {
		$track = stripslashes($track);
		$tracknameCleaned = strtolower($track);
		$tracknameCleaned = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned));
		$linkForTrackname = '';
		if ($tracknameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $tracknameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/racetrack/" . $tmp1 . "-race-course.tpl";
			if (file_exists($tmp2))
				$linkForTrackname = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForTrackname = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1) . "-race-course";
		}
		return $linkForTrackname;
	}

}

if (!function_exists('getRaceName')) {

	function getRaceName(&$stake) {
		$stake = stripslashes($stake);
		$racenameCleaned = strtolower($stake);
		$racenameCleaned = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned));
		$linkForRacename = '';
		if ($racenameCleaned !== '') {
			$tmp1 = str_replace(" ", "-", $racenameCleaned);
			$tmp2 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . ".tpl";
			$tmp3 = SMARTYPATH . "/templates/content/stake/" . $tmp1 . "-stakes.tpl";
			if (file_exists($tmp2))
				$linkForRacename = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1);
			else if (file_exists($tmp3))
				$linkForRacename = SITEABSOLUTEURL . str_replace(" ", "-", $tmp1) . "-stakes";
		}
		return $linkForRacename;
	}

}

if (!function_exists('chckLink')) {

	function chckLink($link, $openTag, $closeTag, $content, $scheme="none") {
		//<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $linkForTrackname . '" itemprop="url">
		if ($link !== '') {
			$ret = '';
			switch($scheme){
				case "location":
					$ret = $openTag . '<a itemprop="location" itemscope itemtype="http://schema.org/Place" href="' . $link . '" itemprop="url">' . $content . '</a>' . $closeTag;
					break;
				case "name":
					$ret = $openTag . '<a itemprop="url" href="' . $link . '">' . $content . '</a>' . $closeTag;
					break;
				default:
					$ret = $openTag . '<a href="' . $link . '">' . $content . '</a>' . $closeTag;
			}
			return $ret;
		} else {
			return $openTag . $content . $closeTag;
		}
	}

}

if (!function_exists('generateGradedStakeRacesTable')) {
	
	function generateGradedStakeRacesTable($result_top, $horsecol=true){
		?>
		<table class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<th><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
					<?php	 	
					if($horsecol){
						echo "<th><strong>Horse</strong></th>";
					}
					?>
					<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
				<?php	 	
				$counter = 0;
				#$blacklist = array("arlington park", "calder", "churchill downs", "fairgrounds", "hoosier", "indiana downs", "miami valley", "oaklawn park", "the meadows", "pimlico", "gulfstream park", "golden gate fields", "santa anita", "meadowlands", "lone star park", "los alamitos", "fairplex", "barrets", "del mar", "ferndale", "fresno", "kentucky downs", "laurel park", "monmouth park", "pleasanton", "portland meadows", "sacramento", "santa rosa", "stockton", "tampa bay downs", "timonium", "los alamitos tbred");
				$blacklist = array();
				while ($data_top = mysql_fetch_object($result_top)) {
					if(in_array(strtolower(trim($data_top->track)), $blacklist)) continue;
					$updatedate = explode("-", $data_top->racedate);
					$updateas = date("M d", mktime(0, 0, 0, $updatedate[1], $updatedate[2], $updatedate[0]));
					if ($counter % 2 == 1) {
						echo '<tr itemscope="" itemtype="http://schema.org/Event" class="odd">';
					} else {
						echo '<tr itemscope="" itemtype="http://schema.org/Event">';
					}
					$linkForRacename = getRaceName($data_top->racename);
					$linkForTrackname = getTrackName($data_top->track);
					$campoRaceName = chckLink($linkForRacename, '', '', checkWordsNComplete($data_top->racename), 'name');
					$campoTrackName = chckLink($linkForTrackname, '', '', $data_top->track, 'location');
					?>
					<td><time itemprop="startDate" content="<?php	 	 echo date("Y-m-d", strtotime($updateas)); ?>"><?php	 	 echo $updateas; ?></time></td>
					<td itemprop="name"><?php	 	 echo $campoRaceName; ?></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><?php	 	 echo $campoTrackName; ?></span><!-- <?php echo $data_top->track; ?> --></td>
					<!-- <td><?php	 	 echo $data_top->grade; ?></td> -->
					<td itemprop="description"><?php	 	 echo $data_top->purse; ?></td>
					<!-- <td><?php	 	 echo $data_top->age; ?></td> -->
					<!-- 	<td><?php	 	 echo $data_top->ds; ?></td> -->
					<?php	 	
					if($horsecol){
						echo "<td>". ucwords(strtolower(trim($data_top->horse))) . "</td>";
					}
					?>
					<!-- 	<td><?php	 	 echo trim($data_top->jockey); ?></td>
					<td><?php	 	 echo trim($data_top->trainer); ?></td> -->
				</tr>
				<?php	 	
				$counter++;
			}

			?>
			</tbody>
		</table>
		<?php	 	
	}
	
}
?>

<div class="table-responsive">
	<?php	 	
		$currenttime = time();
		$sql_top = "SELECT * FROM graded_schedule WHERE UNIX_TIMESTAMP(racedate)>=$currenttime ORDER BY racedate ";
		$result_top = mysql_query_w($sql_top);
		echo generateGradedStakeRacesTable($result_top, false);
	?>
</div>
