<?php	 		 	
// $Id$

/*
 * @file
 */
$template_path = '/themes/';
$sql_j= "SELECT * FROM top_leader where type = '2' and position = '1' ";
$result_j=mysql_query_w($sql_j);
$data_j=mysql_fetch_object($result_j);
$sql_h= "SELECT * FROM top_leader where type = '1' and position = '1' ";
$result_h=mysql_query_w($sql_h);
$data_h=mysql_fetch_object($result_h);
$sql_t= "SELECT * FROM top_leader where type = '3' and position = '1' ";
$result_t=mysql_query_w($sql_t);
$data_t=mysql_fetch_object($result_t);
?>
<div id="leaders-content">
        
    <div class="horses">
      <div class="leaders-header">Horses</div>
      <div class="leaders-info">
      <div class="leaders-name">1. <?php	 	 echo stripslashes($data_h->name); ?> <a data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem1" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
      <div id="leadersItem1" class="leaders-table collapse">
      
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
				<td id="j-starts"><?php	 	 echo $data_h->starts; ?></td>
                <td id="j-1sts"><?php	 	 echo $data_h->first; ?></td>
                <td id="j-2nds"><?php	 	 echo $data_h->second; ?></td>
                <td id="j-3rds"><?php	 	 echo $data_h->third; ?></td>
                <td id="j-purses"><?php	 	 echo $data_h->purse; ?></td>
              </tr>
            </tbody>
          </table>
        </div><!-- /leadersItem2 -->
      </div><!-- /leaders-info -->
      <div class="blockfooter">
       <a href="/leaders/horses" title="Top Ten Horses" class="btn btn-primary">Top Ten Horses <i class="fa fa-angle-right"></i></a>
	   </div>

    </div><!-- /horses -->
    
    <div class="jockeys">
      <div class="leaders-header">Jockeys</div>
      <div class="leaders-info">
      <div class="leaders-name">1. <?php	 	 echo stripslashes($data_j->name); ?> <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem2" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
      <div id="leadersItem2" class="leaders-table collapse">
      
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
                <td id="j-starts"><?php	 	 echo $data_j->starts; ?></td>
                <td id="j-1sts"><?php	 	 echo $data_j->first; ?></td>
                <td id="j-2nds"><?php	 	 echo $data_j->second; ?></td>
                <td id="j-3rds"><?php	 	 echo $data_j->third; ?></td>
                <td id="j-purses"><?php	 	 echo $data_j->purse; ?></td>
              </tr>
            </tbody>
          </table>
        </div><!-- /leadersItem1 -->
      </div><!-- /leaders-info -->
     <div class="blockfooter">
     <a href="/leaders/jockeys" title="Top Ten Jockeys" class="btn btn-primary">Top Ten Jockeys <i class="fa fa-angle-right"></i></a>
     </div>
 
    </div><!-- /jockeys -->
    
    
    
    <div class="trainers">
      <div class="leaders-header">Trainers</div>
      <div class="leaders-info">
      <div class="leaders-name">1. <?php	 	 echo stripslashes($data_t->name); ?> <a  data-toggle="collapse" data-parent="#leaders-content" href="#leadersItem3" class="btn btn-simple collapsed"><i class="fa fa-angle-down"></i></a></div>
      <div id="leadersItem3" class="leaders-table collapse">
      
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
				<td id="j-starts"><?php	 	 echo $data_t->starts; ?></td>
                <td id="j-1sts"><?php	 	 echo $data_t->first; ?></td>
                <td id="j-2nds"><?php	 	 echo $data_t->second; ?></td>
                <td id="j-3rds"><?php	 	 echo $data_t->third; ?></td>
                <td id="j-purses"><?php	 	 echo $data_t->purse; ?></td>
              </tr>
            </tbody>
          </table>
        </div><!-- /leadersItem3 -->
      </div><!-- /leaders-info -->
      <div class="blockfooter">
      <a href="/leaders/trainers" title="Top Ten Trainers" class="btn btn-primary">Top Ten Trainers <i class="fa fa-angle-right"></i></a>
      </div>

    </div><!-- /trainers -->
    
</div><!-- /leaders-content -->
