<?php	 		 	
include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Feedcommon.php');

$RaceTimes = array();
$DataFeed = new DataFeed();
$wps=array();
$price=array();
$ranklist=array();
$contestent=array();

$Success = $DataFeed->GetTracks();

			if($Success)
					{
						if(empty($DataFeed->OpenTracks)){
			
							//$OpenTracksTable = '';
			
						}else{
			
								$OpenTracks = $DataFeed->OpenTracks;
								$i=0;
								foreach($OpenTracks as $Track){
									if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
										continue;
									}
										
			
										// $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]
											$RaceId = $Track["ID"];
											$racename=$Track["TRACK"];
                							$RaceTimes = $DataFeed->GetTrackRaceTimes($RaceId);				
											if(count($RaceTimes) > 0)
											{
												//echo "here";
												writeracetimes($RaceTimes,$RaceId,$racename);
											}									
			
									}
								}
					}


function writeracetimes($RaceTimes,$Id,$racename)
{
	$File = 'RaceTrackTimes_'.$Id;		
			
		if(CachedHTML($File))
		{}
		else
		{
			$writeoutput ='<!-- ======== BOX ================================================= -->
							<div class="block">
							<h2 class="title-custom">Race Times</h2>
							<div>
									<table id="infoEntries" title="'.$racename.' racing times" description="Racing times for '.$racename.'">
											<tbody>';
		$totalcontestno = count($RaceTimes);

		  for($i=0;$i<$totalcontestno;$i++)
			{ 
			  if($i%3 == 0)
			  {
			  $counter=0;
			  $writeoutput .='<tr>';
			  }
			  $tdclass= '';
			  
			  if($counter%2 == 1)
			  {
			    $tdclass= ' class="odd-disabled" ';
			  }
			  $counter++;
			  
			/*  if($race == $RaceTimes[$i][0])
			  {
			  	if($RaceTimes[$i][2] == "Results")
				{
				//$ranklist=$DataFeed->GetResult($RaceId,$race);
				//$price=$DataFeed->GetPrice($RaceId,$race);
				//$wps=$DataFeed->GetTotal($RaceId,$race);					
				}
			  } */

				$writeoutput .='<td width="5%" align="right" '.$tdclass.'><strong>'.$RaceTimes[$i][0].':</strong></td>';
				$writeoutput .='<td width="9%" align="right" '. $tdclass.'> '.$RaceTimes[$i][1].'</td>';
				$writeoutput .='<td width="20%" '.$tdclass.'>- <a href="?id='.$Id.'&race='.$RaceTimes[$i][0].'&racename='.$racename.'" title="'.$racename.' Results Race '.$RaceTimes[$i][0].'" target="_top">'.$RaceTimes[$i][2].$RaceTimes[$i][3].'</a></td>';
			
			  if($i%3 == 2 or $i == ($totalcontestno-1))
			  {
			  		  $writeoutput .='</tr>';			  
			  }
			  
		  }
		  $writeoutput .=' </tbody></table>
								</div> </div>
								<div id="box-shd"></div><!-- == Shadow outside of block == -->
								<!-- ======== END BOX ============================================== -->';
			//echo $writeoutput;
		Write_Cache($File,$writeoutput);
			
		}

}

?>