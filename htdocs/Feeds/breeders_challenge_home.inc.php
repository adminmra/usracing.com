
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; width: 100%; padding: 0; }
#special tbody { border:0; padding:0; }
#special table { float:left; height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { vertical-align:top; text-align:left; font-weight:bold; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special #infoEntries td.right { text-align: right; }
#special #infoEntries td.center { text-align: center; } 
#special h2.title { background:transparent url('/themes/images/horse-racing-icon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>


<?php	 		 	
$breeder = mysql_query_w("SELECT  * 
								FROM 2011_breeders_challenge
								 where racedate >= '".date('Y-m-d')."' ORDER BY racedate limit 0,10 ");
?>      
<div id="special" class="block" style="padding:10px;">
	<div class="container">

<h2 class="title">Breeders' Cup Challenge Schedule</h2>

<div class="post">
<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
  <tr>
    <th width="13%">Date</th>
    <th>Race</th>
    <th>Track</th>
    <th class="center">Grade</th>
  </tr>
                                        <?php	 	
										$count=0;
										while($rows=mysql_fetch_object($breeder)){
											if($count%2 == 0)
											{
												echo "<tr>";
											}
											else
											{
												echo "<tr bgcolor='#EBEBEB'>";
											}
									
											
															$Date=explode("-",$rows->racedate);
										?>
                                        
                                           	 <td  ><?php	 	 echo date('M d',mktime(0,0,0,$Date[1],$Date[2],$Date[0]));?></td>
										    <td ><?php	 	 echo $rows->race; ?></td>
                                            <td ><?php	 	 echo $rows->track; ?></td>                                            
											 <td ><?php	 	 echo $rows->noms;?></td>
											
                                          </tr>
										  
										 <?php	 	
										 $count++;
										 }
										 ?>
										
                             </tbody>            
                          </table>
</div><!-- end:post -->
</div><!-- end:container -->

<div class="boxfooter" style="padding-top:0;"><a title="Breeders Cup Challenge Schedule" href="/breeders-cup/challenge"> 2013 Breeders' Cup Challenge Schedule</a></div>


</div>
                                          