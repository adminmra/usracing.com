<?php	 		 	
$breeder = mysql_query_w("SELECT  * FROM 2011_breeders_challenge ORDER BY racedate");
?>      
<table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries" >

	<tr >
		<th >Date</th>
		<th >Race</th>
		<th>Track</th>
		<!--th >Distance</th>
		<th >Grade</th>
		<th >Age</th-->
		<th >Winner</th>
	</tr>
	<?php	 	
	$count = 0;
	while ($rows = mysql_fetch_object($breeder)) {
		if ($count % 2 == 0) {
			echo "<tr>";
		} else {
			echo "<tr bgcolor='#EBEBEB'>";
		}
		/* Leo's code starts here */
		$racename_101613 = stripslashes($rows->race);
		$racenameCleaned_101613 = strtolower($racename_101613);
		$racenameCleaned_101613 = trim(preg_replace("/[^ \w]+/", "", $racenameCleaned_101613));
		$linkForRacename = '';
		if ($racenameCleaned_101613 !== '') {
			$tmp1_101613 = str_replace(" ", "-", $racenameCleaned_101613);
			$tmp2_101613 = "../../smarty/templates/content/stake/" . $tmp1_101613 . ".tpl";
			if (file_exists($tmp2_101613))
				$linkForRacename = "http://usracing.com/" . str_replace(" ", "-", $tmp1_101613);
		}

		$trackname_101613 = stripslashes($rows->track);
		$tracknameCleaned_101613 = strtolower($trackname_101613);
		$tracknameCleaned_101613 = trim(preg_replace("/[^ \w]+/", "", $tracknameCleaned_101613));
		$linkForTrackname = '';
		if ($tracknameCleaned_101613 !== '') {
			$tmp1_101613 = str_replace(" ", "-", $tracknameCleaned_101613);
			$tmp2_101613 = "../../smarty/templates/content/racetrack/" . $tmp1_101613 . ".tpl";
			if (file_exists($tmp2_101613))
				$linkForTrackname = "http://usracing.com/" . str_replace(" ", "-", $tmp1_101613);
		}
		/* Leo's code ends here */

		$Date = explode("-", $rows->racedate);
		?>

		<td  ><?php	 	 echo date('M d', mktime(0, 0, 0, $Date[1], $Date[2], $Date[0])); ?></td>
		<td >
			<?php	 	 if ($linkForRacename !== '') { ?>
				<a href="<?php	 	 echo $linkForRacename; ?>"><?php	 	 echo $racename_101613; ?></a>
				<?php	 	
			} else {
				echo $racename_101613;
			}
			//echo $rows->race;
			?>
		</td>
		<td >
			<?php	 	 if ($linkForTrackname !== '') { ?>
				<a href="<?php	 	 echo $linkForTrackname; ?>"><?php	 	 echo $trackname_101613; ?></a>
			<?php	 	
			} else {
				echo $trackname_101613;
			}
			//echo $rows->track; 
			?>
		</td>
		<!--td ><?php	 	 echo $rows->distance; ?></td>
		<td ><?php	 	 echo $rows->noms; ?></td>
		<td ><?php	 	 echo $rows->age; ?></td-->
		<td ><?php	 	 echo $rows->winner; ?></td>
	</tr>

	<?php	 	
	$count++;
}
?>
<tr><td colspan="7" align="left">
		* - tentative date<br>
		** - purse includes Breeders' Cup Stakes funds
	</td></tr>

</table>
</td>
</tr>
</table>
