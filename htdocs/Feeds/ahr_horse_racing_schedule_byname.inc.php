<?php
require_once("db_credentials.php");

$link = mysqli_connect(SERVERUSR,USERUSR,PASSUSR,DBUSR) or die("Error " . mysqli_error($this->link));  		 	
$sql = "SELECT * FROM horse_racing_schedule ORDER BY name ASC, dateScrap ASC, id ASC";
$result = mysqli_query($link, $sql);

$list_schedule = array();

while($data=mysqli_fetch_array($result))
{
	$list_schedule[] = $data;
}
?>
<div class="schedule">
<?php	 	
$last_name ="";
for ($i=0; $i <count($list_schedule) ; $i++)
{
	if($last_name != $list_schedule[$i]['name']){ 
		$last_name = $list_schedule[$i]['name'];
	?>
	<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			<?php echo $last_name; ?>
		</div>
		<div class="sDay">
	<?php 
	}
	?>
			<div class="sRace">
				<span class="race"><?php echo date("l, F d, Y", strtotime($list_schedule[$i]['dateScrap']));?></span>
			</div>
	<?php
	if(((isset($list_schedule[$i+1]['name']))&&($last_name != $list_schedule[$i+1]['name']))||(empty($list_schedule[$i+1]['name'])))
	{ ?>
		</div>
	</div>
	<?php
	}	

}?>
</div>