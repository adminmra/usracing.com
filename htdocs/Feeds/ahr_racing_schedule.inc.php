<?php	 	
//ini_set('error_reporting', E_ALL);
////error_reporting(0);
//ini_set('display_errors', "Off");
//echo realpath("includes/class.xmlreader.php");
 include_once($_SERVER['DOCUMENT_ROOT'] . "/includes/class.xmlreader.php");

	//$xmlurl = $_SERVER['DOCUMENT_ROOT'] . "/FeedData/racexml.xml";
$xmlurl ="http://allhorse.com/programs/horse_racing_schedule/horseracexml.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
//print_r($xml);

?>

<div class="schedule">
	<?php
		$blacklist = array("arlington park", "calder race course", "churchill downs", "fairgrounds", "hoosier", "indiana downs", "miami valley", "oaklawn park", "the meadows", "finger lakes", "canterbury park", "pimlico", "gulfstream park", "golden gate fields", "santa anita", "meadowlands", "lone star park", "los alamitos", "fairplex", "barrets", "del mar", "ferndale", "fresno", "kentucky downs", "laurel park", "monmouth park", "pleasanton", "portland meadows", "sacramento", "santa rosa", "stockton", "tampa bay downs", "timonium", "los alamitos tbred");
		$blacklist = array();

		if(is_array($xml["schedule"]["#"]["scheduledata"])){
			$countDay = 0;
			$daysArr = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
			
			for($i=0;$i<count($xml["schedule"]["#"]["scheduledata"]);$i++){
				?>
				<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> <?php	 	 echo $daysArr[$countDay] . ", " . nextdayformatted(weekfirstday(),$countDay); ?></div>
					<div class="sEvents">
					<?php	 	
					if(is_array($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"])){
						for($j=0;$j<count($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"]);$j++){
							if(in_array(strtolower(trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"][$j]["#"])), $blacklist))continue;
						?>
						<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'><?php	 	 echo  trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"][$j]["#"]);?></div><div class='tsBody'> <?php	 	 echo trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["raceschedule"][$j]["#"]); ?></div>">
							<span class="time"><?php	 	 echo trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["firstrace"][$j]["#"]);?></span>
							<span class="race"><?php	 	 echo trim($xml["schedule"]["#"]["scheduledata"][$i]["#"]["track"][$j]["#"]);?></span>
						</div>
						<?php	 	
						}
					}
					?>
					</div>
				</div>
				<?php	 	
				$countDay++;
			}
		}
	?>
</div>
<script type="text/javascript">
$('.sRace').tooltip({
html: true,
placement: "auto"
});
$('.sRace').on('show.bs.tooltip', function () {
 $(this).addClass("in");
}).on('hide.bs.tooltip', function () {
 $(this).removeClass("in");
})

</script>