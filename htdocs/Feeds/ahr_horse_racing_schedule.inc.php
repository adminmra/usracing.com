<?php
require_once("db_credentials.php");
require_once("scrapRacingScheduleClass.php");

$bh_ins = new HorseRacingScrapper();
$bh_ins->main();
$today=jddayofweek ( cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0 );
$subsdays = $today-1;
$adddays = 7 - $today;
$startdate = strtotime ( '-'.$subsdays.' day' , strtotime ( date('Y-m-d') ) ) ;
$enddate =  strtotime ( '+'.$adddays.' day' , strtotime ( date('Y-m-d') ) ) ;
$startdate = date('Y-m-d',$startdate);

$enddate = date('Y-m-d',$enddate);

$link = mysqli_connect(SERVERUSR,USERUSR,PASSUSR,DBUSR) or die("Error " . mysqli_error($this->link));  		 	
$sql = "SELECT * FROM horse_racing_schedule WHERE dateScrap BETWEEN CAST('$startdate' AS DATE) 
AND CAST('$enddate' AS DATE)
ORDER BY dateScrap ASC, id ASC";
$result = mysqli_query($link, $sql);

$list_schedule = array();

while($data=mysqli_fetch_array($result)) {
	$list_schedule[] = $data;
}
?>
<div class="schedule">
<?php
$last_date ="";
for ($i=0; $i <count($list_schedule) ; $i++)
{
	if($last_date != $list_schedule[$i]['dateScrap']){ 
		$last_date = $list_schedule[$i]['dateScrap'];
	?>
	<div class="sItem">
		<div class="sDay">
			<i class="fa fa-calendar"></i>
			<?php echo date("l, F d, Y", strtotime($last_date));?>
		</div>
		<div class="sEvents">
	<?php 
	}
	?>
			<div class="sRace">
				<span class="race"><?php echo $list_schedule[$i]['name']?></span>
			</div>
	<?php
	if(((isset($list_schedule[$i+1]['dateScrap']))&&($last_date != $list_schedule[$i+1]['dateScrap']))||(empty($list_schedule[$i+1]['dateScrap'])))
	{ ?>
		</div>
	</div>
	<?php
	}

}?>
</div>