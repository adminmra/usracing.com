<?php	 		 	
require_once "dbconnect.inc";
?>
<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>

<div class="block">
<p><h2 class="title-custom">Triple Crown Trail</h2></p>
<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries"  style="width:100%; " title="Triple Crown Trail 2011" summary="Triple Crown Trail 2011">
 <tr>
 <th  style="width: 62px;">Date</th>
 <th style="width: 100px;">Horse</th>
 <th style="width: 10px;">PF</th>
 <th style="width: 48px;">Sire</th>
 <th style="width: 48px;">Mare Sire</th>
 <th style="width: 50px;">Race</th>
 <th style="width: 5px;">Gr</th>
 <th style="width: 40px;">Track</th>
 <th style="width: 40px;">Surface</th>
 <th style="width: 10px;">Dp</th>
 <th style="width: 10px;">Di</th>
 <th style="width: 10px;">CD</th>
 <th style="width: 10px;">Points</th>
 </tr>
 
<?php	 	
$counter=0;
$sql="select * from triplecrowntrial  order by racedate ";
$result=mysql_query_w($sql); 
while($data=mysql_fetch_object($result))
{	
		$horse=ucwords(strtolower($data->horse));
		$stakes=ucwords(strtolower($data->race));
	$sqltrack = " SELECT name FROM racetracks WHERE trackcode like '%".stripslashes($data->track)."%' ";
	$resulttrack=mysql_query_w($sqltrack);
	$numtrack=@mysql_num_rows($resulttrack);
if($numtrack > 0)
{ $datatrack = mysql_fetch_object($resulttrack);}
	
	$sqlstakes = " SELECT * FROM stakes WHERE name like '%".stripslashes($stakes)."%' ";
	$resultstakes=mysql_query_w($sqlstakes);
	$numstakes=@mysql_num_rows($resultstakes);

	  $sqlhorse = " SELECT * FROM horse WHERE horse like '%".stripslashes($horse)."%' ";
	  $resulthorse=mysql_query_w($sqlhorse);
	  $numhorse=@mysql_num_rows($resulthorse);
if($counter%2 == 0)
{
?>

 <tr> 
 <?php	 	
 }
 else
 {
 ?>
 
 <tr class="odd">
 <?php	 	
 }
 ?>
 <td ><?php	 	 $RACEDATE=explode("-",$data->racedate); echo $RACEDATE[1]."/".$RACEDATE[2]; ?></td>
<td><?php	 	 if($numhorse > 0){echo "<a href='/horse?name=".stripslashes($horse)."' title='".stripslashes($horse)."'>".stripslashes($horse)."</a>";}else{echo  stripslashes($horse);}  ?></td>
<td><?php	 	 echo $data->PF; ?></td>
<td><?php	 	 echo ucwords(strtolower($data->sire)); ?></td>
<td><?php	 	 echo ucwords(strtolower($data->maresire)); ?></td>
<td><?php	 	 echo  $stakes;?></td>
<td><?php	 	 echo $data->gr; ?></td>
<td><?php	 	 if($numtrack > 0){echo "<a href='/racetrack?name=".stripslashes($datatrack->name)."' title='".stripslashes($datatrack->name)."'>".stripslashes($datatrack->name)."</a>";}else{echo  stripslashes($data->track);}?></td>
<td><?php	 	 echo $data->surface; ?></td>
<td><?php	 	 echo $data->distance; ?></td>
<td><?php	 	 echo $data->dp; ?></td>
<td><?php	 	 echo $data->di; ?></td>
<td><?php	 	 echo $data->cd; ?></td>
<td><?php	 	 echo $data->points; ?></td>
 </tr>


 <?php	 	
 $counter++;
 }
 ?>
 </table>

</div>
</div>
<div id="box-shd"></div><!-- === SHADOW === -->
						
