<?php	 		 	
$sql = "SELECT * FROM cron_props_matchups ORDER BY id_cron_props_matchups ASC";
$result = mysql_query_w($sql);

?>
<div class="table-responsive">
<table class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Kentucky Derby Odds" summary="Kentucky Derby Odds">
<!-- <tr>
<th> Derby Contenders</th>
<th class="right">Odds to Win</th>

</tr> -->
<tr>
	<td>Team</td>
	<td>Money</td>
	<td>Spread</td>
	<td>Total</td>
</tr>
<?php	 	
$last_category_name ="";
while($data=mysql_fetch_object($result))
{
	$category_name = $data->category_name;
	
	if(strcasecmp($last_category_name, $category_name)!=0)
	{
		$last_category_name = $category_name;
	}
	else
	{
		$last_category_name = "";	
	}

	if(strcasecmp($last_category_name,"")!=0)
	{?>
		<tr style="font-weight:bold">
			<td colspan="4"><?php echo $last_category_name ?></td>
		</tr>
		
	<?php
	}?>
	<tr>				
		<td class="left sortOdds" align="left"><?php	 echo $data->subtitle; ?></td>
		<td class="right sortOdds" align="right"><?php	 echo $data->money; ?></td>
		<td class="right sortOdds" align="right"><?php	 echo $data->spread; ?></td>
		<td class="right sortOdds" align="right"><?php	 echo $data->total; ?></td>
	</tr>
	<?php
}
?>

</table>
</div>