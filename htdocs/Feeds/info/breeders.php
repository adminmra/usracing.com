<?php	 		 	
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/clean.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/imageresize.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/sort.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/class.xmlreader.php";

	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/FeedData/2010BreedersCup.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
	
									?>

<table  width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders' Cup 2010" Summary="Date, Post Time, Distance, Purse for Betting the 2009 Breeders' Cup">
                                            
                                          <tr >
										  	<th width="10%" >Date</th>
                                            <th width="10%" >Post</th>
                                            <th width="47%">Race</th>
                                            <th width="21%">Age</th>
                                            <th width="12%">Distance</th>
                                            <th width="10%">Purse</th>
                                          </tr>
                                        <?php	 	
										$count=0;
	for($i=0;$i<count($xml["allhorse"]["#"]["BreedersCup"]);$i++)
	{
			$DATAARR=array();
			foreach($xml["allhorse"]["#"]["BreedersCup"][$i]["#"] as $key => $value){
				$DATAARR[$key] = str_replace("�","",$value[0]["#"]);
			}
			$rows= (object)$DATAARR;		
			if($count%2 == 0)
				{
					echo "<tr>";
				}
				else
				{
					echo '<tr class="odd">';
				}
				$Date=explode("-",$rows->racedate);
										?>
                                        
                                           	 <td width="51" ><?php	 	 echo date('M d',mktime(0,0,0,$Date[1],$Date[2],$Date[0]));?></td>
										    <td width="51" ><?php	 	 echo $rows->post;?></td>
                                            <td width="226"><a href="/breederscup-race?stakes=<?php	 	 echo stripslashes($rows->name);?>&id=<?php	 	 echo $rows->id;?>" title="<?php	 	 echo stripslashes($rows->name);?>" ><?php	 	 echo stripslashes($rows->name);?></a></td>
                                            <td width="119"><?php	 	 echo $rows->age_weight;?></td>
                                            <td width="66"><?php	 	 echo $rows->distance;?></td>
                                            <td width="53"><?php	 	 if(strlen(trim($rows->purse)) > 0) { echo $rows->purse; } ?></td>
                                          </tr>
										  
										 <?php	 	
										 $count++;
	}
	
	//print_r($rows);
	?>
	</table>
		