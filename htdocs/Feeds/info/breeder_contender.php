<?php	 		 	
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/clean.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/imageresize.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/sort.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/class.xmlreader.php";
if(isset($_REQUEST['id']))
{
	$REMOVEid=explode(":",$_REQUEST['id']);
	$id=$REMOVEid[0];	
	$stakes_name=stripslashes(urldecode($REMOVEid[1]));
}
else
{
	$id=7;
	$stakes_name="Breeders' Cup Classic";
}
	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/FeedData/2010BreedersCup.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
	for($i=0;$i<count($xml["allhorse"]["#"]["BreedersCup"]);$i++)
	{
			
			if($xml["allhorse"]["#"]["BreedersCup"][$i]["#"]["id"][0]["#"] == $id)
			{
			$DATAARR=array();
			foreach($xml["allhorse"]["#"]["BreedersCup"][$i]["#"] as $key => $value){
				$DATAARR[$key] = str_replace("�","",$value[0]["#"]);
			}
			$rows= (object)$DATAARR;
			}
	}
$age=$rows->age_weight;
$posttime=$rows->post;
$resulttime= $rows->resulttime;
$turf=$rows->turf;
$purse=$rows->purse;
$grade=$rows->grade;
$distance=$rows->distance;


									?>
 <!-- ======== STYLES NEEDED TO HIDE DRUPAL BLOCK ELEMENTS ================== -->
<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>
<!-- ============================================================== -->


 <!-- ======== BOX ================================================= -->                             
<div class="block">
<h2 class="title-custom"><?php	 	 echo $stakes_name; ?></h2>
<div> 

  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="<?php	 	 echo $stakes_name; ?>" summary="<?php	 	 echo $stakes_name; ?> Data for the 2009 Breeders Cup">
                                <tr>
                                  <td width="56%" ><strong>Age: </strong><?php	 	 if($age!="") echo $age; else echo "-";?>
                                  </td>
                                  <td width="44%" ><strong>Post Time:</strong> <?php	 	 if($posttime!="") echo $posttime; else echo "-";?></td>
                                </tr>
                                <tr class="odd">
                                  <td ><strong>Distance</strong>: <?php	 	 if($distance!="") echo $distance; else echo "-";?></td>
                                  <td ><strong>Grade: </strong><?php	 	 if($grade!="") echo $grade; else echo "-";?></td>
                                </tr>
                                <tr>
                                  <td ><strong>Purse: </strong><?php	 	 if($purse!="") echo $purse; else echo "-";?></td>
                                  <td ><strong>Turf:</strong> <?php	 	 if($turf!="") echo $turf; else echo "-";?></td>
                                </tr>
                              </table>
							         </div>
				 </div>           
                  <div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->   
<?php	 	
	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/FeedData/breeders_cup_result.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
//print_r($xml);

?>
   <!-- ======== BOX ================================================= -->                             
<div class="block">
<h2 class="title-custom"><?php	 	 echo $stakes_name; ?> Odds and Results</h2>
<div> 

   <table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries"  title=" <?php	 	 echo $stakes_name; ?> Odds and Results" summary=" <?php	 	 echo $stakes_name; ?> Odds and Results for  2007 Breeders' Cup Betting">
                              
                                <tr >
                                <th width="8%">Result</th>
                                  <th width="8%">Time</th>
                                  <th width="5%">Post</th>
                                  <th width="6%">M/L</th>
                              <th width="25%">Horse</th>
                                   <th width="25%">Trainer</th>
                                  <th width="25%">Jockey</th> 
                                </tr>
                                <?php	 	
										 $seeindex=1;


	for($i=0;$i<count($xml["allhorse"]["#"]["breeders_cup_result"]);$i++)
	{
			//print_r($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["race_id"][0]["#"]);
			if($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["race_id"][0]["#"] == $id)
			{
			$DATAARR=array();
			foreach($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"] as $key => $value){
				$DATAARR[$key] = str_replace("�","",$value[0]["#"]);
			}
			$rows= (object)$DATAARR;
			
$numhorse=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numhorse"][0]["#"];
$numtrainer=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numtrainer"][0]["#"];
$numjockey=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numjockey"][0]["#"];
 									if(($seeindex%2)== 1)
											   {
											   echo " <tr >";
											   }
											   else
											   {
											   	echo ' <tr   class="odd">';
											   }

										  ?>
                               
                                <td width="8%"  align="center" ><?php	 	 if($rows->result==0) echo "-"; else echo $rows->result;?></td>
                                  <td width="8%" align="center" ><?php	 	 if($rows->time1!="") echo $rows->time1; else echo "-";?></td>
                                  <td width="5%" align="center" ><?php	 	 if($rows->post==0) echo "-"; else echo $rows->post;?></td>
                                  <td width="6%" align="center" ><?php	 	 if($rows->odds!="") echo $rows->odds; else echo "-";?></td>
                               <td width="21%" ><?php	 	  if($rows->horse!=""){if($numhorse > 0) echo "$rows->horse</a>"; else echo $rows->horse; }else{ echo "-"; }?></td>
                               <td width="25%" ><?php	 	 if($rows->trainer!=""){if($numtrainer > 0) echo "$rows->trainer</a>"; else echo $rows->trainer; }else{ echo "-";}?></td>
                                  <td width="25%" ><?php	 	 if($rows->jockey!=""){if($numjockey > 0) echo "$rows->jockey</a>"; else echo $rows->jockey; }else{ echo "-";}?></td>
                                </tr>
                                <?php	 	 //}
									  
									  $seeindex++;
							
								

			}
	}

//print_r($rows);
?>
<?php	 	
if($seeindex == 1)
								{
								?>
								<tr>
                                   <td align="center"  colspan="7">No entries available now. Check Back soon for the entries on <?php	 	 echo $stakes_name; ?></td>
								</tr>
								<?php	 	
								}
?>
                              </table>
							  </div>
				 </div>           
                  <div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->   