<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <title>Google Maps API Example: Simple Geocoding</title>
    <script src="http://maps.google.com/maps?file=api&amp;v=2.x&amp;key=AIzaSyB1ndfDZ1o26fpjjvGPuwHBZBj24ooqyHg" type="text/javascript"></script>
    <script type="text/javascript">

    var map = null;
    var geocoder = null;

    function initialize() {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("map_canvas"));
        map.setCenter(new GLatLng(37.4419, -122.1419), 13);
        geocoder = new GClientGeocoder();
	    map.removeMapType(G_HYBRID_MAP);
        map.addControl(new GLargeMapControl());
 
        var mapControl = new GMapTypeControl();
        map.addControl(mapControl);
        
      
      }
    }

    function showAddress(address) {
	initialize();
      if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
             // alert(address + " not found");
            } else {
			  map.setCenter(point, 13);
              var marker = new GMarker(point);
              map.addOverlay(marker);
              marker.openInfoWindowHtml(address);
            }
          }
        );
      }
    }
    </script>
  </head>

  <body onload="showAddress('<?php	 		 	 echo $_REQUEST['a'];?>')" onunload="GUnload()" marginheight="0" leftmargin="0" rightmargin="0" marginwidth="0" topmargin="0">
    <div id="map_canvas" style="width: 630px; height: 300px"></div>
  </body>
</html>

