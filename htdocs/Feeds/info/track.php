<?php	 		 	
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', "On");
//require_once LIBS_DIR.'/dbconnect.inc';
include $_SERVER['DOCUMENT_ROOT'] . "/Feeds/info/imageresize.php";
include $_SERVER['DOCUMENT_ROOT'] . "/Feeds/info/clean.php";
include $_SERVER['DOCUMENT_ROOT'] . "/Feeds/info/sort.php";
include_once $_SERVER['DOCUMENT_ROOT'] . "/Feeds/info/class.xmlreader.php";

if (isset($_REQUEST['name'])) {
  $pos = strrpos($_REQUEST['name'], "_");
  if ($pos === FALSE) { // note: three equal signs
    // not found...
    $name = urldecode($_REQUEST['name']);
  }
  else {

    $name = str_replace("_", " ", urldecode($_REQUEST['name']));
  }
}


$xmlurl = $_SERVER['DOCUMENT_ROOT'] . "/FeedData/racetracks.xml";
$xmlreader = new xmlreader1($xmlurl);
$xml = $xmlreader->parse();
//print_r($xml);
//$key = array_search(urldecode($_REQUEST['name']), $xml["allhorse"]["#"]["racetracks"]);
for ($i = 0; $i < count($xml["allhorse"]["#"]["racetracks"]); $i++) {
  if ($xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"] == urldecode(trim($name))) {
    $DATAARR = array();
    foreach ($xml["allhorse"]["#"]["racetracks"][$i]["#"] as $key => $value) {
      $DATAARR[$key] = $value[0]["#"];
    }
    $data = (object) $DATAARR;
  }
}
$xmlurl = $_SERVER['DOCUMENT_ROOT'] . "/FeedData/stakes.xml";
$xmlreader = new xmlreader1($xmlurl);
$xml = $xmlreader->parse();
//print_r($xml);

$DATAARR = array();
$count = 0;
for ($i = 0; $i < count($xml["allhorse"]["#"]["stakes"]); $i++) {
  //echo $data->trackcode;
  if (strtoupper($xml["allhorse"]["#"]["stakes"][$i]["#"]["trackcode"][0]["#"]) == strtoupper(trim($data->trackcode))) {
    foreach ($xml["allhorse"]["#"]["stakes"][$i]["#"] as $key => $value) {
      $DATAARR[$count][$key] = $value[0]["#"];
    }
    $data_stakes = (object) $DATAARR;
    $count++;
  }

}
//print_r($data_stakes);

/*$sql = "select * from racetracks where name like '%".urldecode($name)."%' ";
$result=mysql_query_w($sql);
$data=mysql_fetch_object($result);*/

/*$sqlstakes="select name, id, grade, date, purse from stakes where trackcode='".$data->trackcode."' order by date,name ";
$resultstakes=mysql_query_w($sqlstakes);
$numrow=mysql_num_rows($resultstakes);*/
$numrow = count($data_stakes);

$addressstring = $data->street . " " . $data->street2 . " , " . $data->city . ", " . $data->state . ", " . $data->zip . " , " . $data->country;
//echo $addressstring."fgh";
?>


<!-- ======== STYLES NEEDED TO HIDE DRUPAL BLOCK ELEMENTS ================== -->
<style>
    .not-front #col3 #content-box { padding : 0; }

    .not-front #col3 #content-wrapper { border : none; padding : 0 }

    .not-front #col3 #content-wrapper  h2.title { display : none; }

    .not-front #col3 .box-shd { display : none; }
</style>
<!-- ============================================================== -->



<!-- ======== BOX ================================================= -->
<div class="block">
    <h2 class="title-custom"><?php	 	 echo $name; ?></h2>

    <div>

        <div class="RoundBody RaceTracks">


            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="65%" style="padding: 0 20px 0 34px;">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <th width="175px">Track Length:</th>
                                <td><?php	 	 echo $data->tracklength;?></td>
                            </tr>
                            <tr>
                                <th>Stretch Length:</th>
                                <td><?php	 	 echo $data->stretchlength;?></td>
                            </tr>
                            <tr>
                                <th>Stretch Width:</th>
                                <td><?php	 	 echo $data->stretchwidth;?></td>
                            </tr>
                            <tr>
                                <th>Infield Capacity:</th>
                                <td><?php	 	 if ($data->capinfield == 0) {
                                }
                                else {
                                  echo $data->capinfield;
                                }?></td>
                            </tr>
                            <tr>
                                <th>Clubhouse Capacity:</th>
                                <td><?php	 	 if ($data->capclubhouse == 0) {
                                }
                                else {
                                  echo $data->capclubhouse;
                                }?></td>
                            </tr>
                            <tr>
                                <th>Grand Stand Capacity:</th>
                                <td><?php	 	 if ($data->capgrandstand == 0) {
                                }
                                else {
                                  echo $data->capgrandstand;
                                }?></td>
                            </tr>
                            <tr>
                                <th>Parking Capacity:</th>
                                <td><?php	 	 if ($data->capparking == 0) {
                                }
                                else {
                                  echo $data->capparking;
                                }?></td>
                            </tr>
                            <tr>
                                <th>Price General Admission:</th>
                                <td><?php	 	 echo $data->pricega;?></td>
                            </tr>
                            <tr>
                                <th>Price Clubhouse:</th>
                                <td><?php	 	 echo $data->pricech;?></td>
                            </tr>
                            <tr>
                                <th>Price Turf Club:</th>
                                <td><?php	 	 echo $data->pricetc;?></td>
                            </tr>
                        </table>
                        <br/>
                        <br/>

                        <p>

                        <h2><?php	 	 echo $name; ?> History</h2></p>
                        <p>  <?php	 	 echo just_clean(nl2br($data->history));?></p>
                        <br/><br/>

                        <p>

                        <h2>More About <?php	 	 echo $name; ?></h2></p>
                        <p>  <?php	 	 echo just_clean($data->pointsofinterest);?></p>
                    </td>

                    <td valign="top" width="35%">
                        <div class="TrackInfo">
                          <?php	 	
                          if (strlen($data->piclogo) > 3) {
                            ?>
                              <img src="http://www.otbpicks.net/images/racetracks/<?php	 	 echo $data->piclogo;?>" <?php	 	 echo ImageResizeVirtual("http://www.otbpicks.net/images/racetracks/$data->piclogo", 300); ?>>
                            <?
                          } ?>
                            <br>

                            <p>
                                <b><?php	 	 echo $name; ?></b><br/>
                              <?php	 	 echo $data->street . " " . $data->street2;?><br/><?php	 	 echo $data->city . ", " . $data->state . ", " . $data->zip; ?><br/><?php	 	 echo $data->country; ?>
                            </p>

                            <p>
                              <?php	 	 echo $data->phone ?><br/>
                              <?php	 	 echo $data->phone2 ?><br/>
                            </p>

                            <hr/>

                            <p>
                                <b>Racing Dates: </b> <?php	 	 echo $data->dateopen;?> until <?php	 	 echo $data->dateclosed;?><br/>
                                <b>Number of Racing Days: </b><?php	 	 if ($data->racingdays == 0) {
                            }
                            else {
                              echo $data->racingdays;
                            }?><br/>
                            </p>

                            <hr/>

                            <div style="height:150px; overflow:hidden;  "><?php	 	 include $_SERVER['DOCUMENT_ROOT'] . "/Feeds/info/weather.php";?></div>
                        </div>
                    </td>
                </tr>
            </table>


            <br/>
        </div>
    </div>
</div>

<?php	 	
if ($numrow > 0) {
  ?>
<div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->


<!-- ======== BOX ================================================= -->
<div class="block">
    <h2 class="title-custom"><?php	 	 echo $name; ?> Stakes</h2>

    <div>


        <table border="0" cellpadding="0" cellspacing="0" class="table table-condensed table-striped table-bordered" id="infoEntries">
            <tbody>
            <tr>
                <th>Stakes</th>
                <th width="25">Grade</th>
                <th width="25">Purse</th>
                <th width="100">Date</th>
            </tr>

              <?php	 	
              $count = 0;
              foreach ($data_stakes as $value) {
                //print_r($datastakes);
                $datastakes = (object) $value;
                if ($count % 2 == 1) {
                  echo "<tr>";
                }
                else {
                  echo "<tr class=\"odd\">";
                }
                $racestakesname = str_replace(" ", "_", $datastakes->name);
                ?>
              <td><a href="stakes?name=<?php	 	 echo urlencode($racestakesname);?>" title="<?php	 	 echo $datastakes->name;?>"><?php	 	 echo stripslashes($datastakes->name);?></a></td>
              <td><?php	 	 echo $datastakes->grade;?></td>
              <td><?php	 	 echo $datastakes->purse;?></td>
              <td><?php	 	 $inDate = explode("-", $datastakes->date);if ($inDate[1] > 0 and $inDate[2] > 0 and $inDate[0] > 0) {
                echo date("F d", mktime(0, 0, 0, $inDate[1], $inDate[2], $inDate[0]));
              } ?>
              </td></tr>
                <?php	 	
                $count++;
              }
              ?>
            </tbody>
        </table>
    </div>
</div>
<?php	 	
}
?>

<div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->


<!-- ======== BOX ================================================= -->
<div class="block">
    <h2 class="title-custom">How to get there:</h2>

    <div>

        <div style="padding: 20px 34px;">
            <p><?php	 	 echo just_clean($data->driving); ?></p>
        </div>

        <div style="padding: 0 0 20px 34px;">
            <iframe src="/Feeds/info/map.php?a=<?php	 	 echo $addressstring;?>" height="300" width="630" scrolling="no" allowtransparency="true" frameborder="0"></iframe>
        </div>

    </div>
</div>
<div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->


