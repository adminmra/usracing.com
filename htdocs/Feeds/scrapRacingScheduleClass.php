<?php

/**
 * Author:
 * email: fvillegas at acedevel.com
 * v.2.0
 * Copyright: usracing.com 2015
 */
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//error_reporting(E_ALL);
define("NEWSABSPATH", "/home/ah/allhorse/public_html/programs/news");
require_once("db_credentials.php");
require_once NEWSABSPATH . '/cron/simplehtmldom_1_5/simple_html_dom.php';

class HorseRacingScrapper {
	var $debug;
	var $test;
	var $delay;
	var $link;


	function __construct($debug = false, $test = false, $delay = 5) {
		$this->debug = $debug;
		$this->test = $test;
		$this->delay = $delay;
		$this->link = mysqli_connect(SERVERUSR,USERUSR,PASSUSR,DBUSR) or die("Error " . mysqli_error($this->link)); 
	}

	public function main() {
		$html = file_get_html('https://www.superbook.ag/static/info/_default/horses.html');

		$mainStoryDefined = false;
		foreach ($html->find('table') as $key => $entry) {
			$dateScrapizq=NULL;
			$dateScrapder=NULL;

			$row = $entry->find('tr');
				foreach ($row as $r) {
					$cols = $r->find('td');
					foreach($cols as $key => $dato){
						if(++$key%2!=0){
							if($var = $dato->find('strong',0)){
								$dateScrapizq = $var->innertext;
							}
							else
							{
								$formatText = str_replace("&nbsp;", "", $dato->innertext);

								if(trim($formatText)!=''){
									//Guardar desde aqui en la DB
									$datei = date('Y-m-d',strtotime($dateScrapizq));
									$key = $key = str_replace(array(" ","-"), "", $dato->innertext).$datei;
									$query = "INSERT INTO horse_racing_schedule VALUES (NULL,'$dato->innertext','$datei','','$key')";
									mysqli_query($this->link, $query);

								}
							}
						}
						else
						{
							if($var = $dato->find('strong',0)){
								$dateScrapder = $var->innertext;
							}
							else
							{
								$formatText = str_replace("&nbsp;", "", $dato->innertext);

								if(trim($formatText)!=''){
									$dated = date('Y-m-d',strtotime($dateScrapder));
									$key = str_replace(array(" ","-"), "", $dato->innertext).$dated;
									//Guardar desde aqui en la DB
									$query = "INSERT INTO horse_racing_schedule VALUES (NULL,'$dato->innertext', '$dated', '','$key')";
									mysqli_query($this->link, $query);

								}
							}
						}
					}
				}
			unset($entry);

		}
	}
}
