﻿
// Prototype Extend

String.prototype.ulen = function () {
    var _s = this;
    return _s.replace(/[^\x00-\xff]/g, "aa").length;
};

String.prototype.paramsToJson = function () {
    var _s = this;
    var paraJ = '';
    var paraArr = _s.split('&');
    for (var i = 0; i < paraArr.length; ++i) {
        var paraSubArr = paraArr[i].split('=');
        paraJ += "'" + paraSubArr[0] + "':";
        paraJ += "'" + paraSubArr[1] + "', ";
    }
    paraJ = '{' + paraJ.substring(0, paraJ.length - 2) + '}';
    return eval('(' + paraJ + ')');
};

String.prototype.isPhone = function () {
    var _s = this;
    var re = /^[\d\-\(\)]+$/;
    return re.test(_s);
};

String.prototype.isEmail = function () {
    var _s = this;
    var re = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    return re.test(_s);
};

String.prototype.isZipCode = function () {
    var _s = this;
    var re = /^\d{3,6}$/;
    return re.test(_s);
};

String.prototype.isDate = function () {
    var _s = this;
    var re = /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/;
    return re.test(_s);
};

String.prototype.trim = function () {
    var _s = this;
    _s = _s.replace(/^\s*(.*?)[\s\n]*$/g, "$1");
    return _s;
};

Number.prototype.toComma = function() {
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

// Date
Date.prototype.toNormalString = function () {
    var _d = this;
    var r = _d.getFullYear() + '-';
    r += (((_d.getMonth() + 1).toString().length == 1) ? '0' + (_d.getMonth() + 1).toString() : _d.getMonth() + 1) + '-';
    r += ((_d.getDate().toString().length == 1) ? '0' + _d.getDate().toString() : _d.getDate()) + ' ';
    r += ((_d.getHours().toString().length == 1) ? '0' + _d.getHours().toString() : _d.getHours()) + ':';
    r += ((_d.getMinutes().toString().length == 1) ? '0' + _d.getMinutes().toString() : _d.getMinutes()) + ':';
    r += (_d.getSeconds().toString().length == 1) ? '0' + _d.getSeconds().toString() : _d.getSeconds();
    return r;
};

Date.prototype.toShortTimeString = function () {
    var hours = this.getHours();
    var minutes = this.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
};

// Array
Array.prototype.removeAtIndex = function (dx) {
    var _a = this;
    if (isNaN(dx) || dx > _a.length) {
        return false;
    }
    for (var i = 0, n = 0; i < _a.length; ++i) {
        if (_a[i] != _a[dx]) {
            _a[n++] = _a[i];
        }
    }
    _a.length -= 1;
};

Array.prototype.remove = function (o) {
    for (var i = 0; i < this.length; ++i) {
        if (o == this[i]) {
            this.removeAtIndex(i);
            break;
        }
    }
};

Array.prototype.contains = function (o) {
    for (var i = 0; i < this.length; ++i) {
        if (this[i] == o) {
            return true;
        }
    }
    return false;
};

Array.prototype.objectAtIndex = function (o) {
    for (var i = 0; i < this.length; ++i) {
        if (this[i] == o) {
            return i;
        }
    }
    return -1;
};

Array.prototype.clear = function () {
    this.length = 0;
};

// Math (Static Methods..Late Binding)
Math.rand = function (l, u) {
    return Math.floor((Math.random() * (u - l + 1)) + l);
};


// Cookie Support

function getCookieVal(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1) o
    endstr = document.cookie.length;
    return unescape(document.cookie.substring(offset, endstr));
}

function getCookie(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieVal(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function setCookie(name, value) {
    var argv = setCookie.arguments;
    var argc = setCookie.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + escape(value) +
      ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
      ((path == null) ? "" : ("; path=" + path)) +
      ((domain == null) ? "" : ("; domain=" + domain)) +
      ((secure == true) ? "; secure" : "");
}

function getCookieValTransparent(offset) {
    var endstr = document.cookie.indexOf(";", offset);
    if (endstr == -1)
        endstr = document.cookie.length;
    return document.cookie.substring(offset, endstr);
}

function getCookieTransparent(name) {
    var arg = name + "=";
    var alen = arg.length;
    var clen = document.cookie.length;
    var i = 0;
    while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
            return getCookieValTransparent(j);
        i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break;
    }
    return null;
}

function setCookieTransparent(name, value) {
    var argv = setCookieTransparent.arguments;
    var argc = setCookieTransparent.arguments.length;
    var expires = (argc > 2) ? argv[2] : null;
    var path = (argc > 3) ? argv[3] : null;
    var domain = (argc > 4) ? argv[4] : null;
    var secure = (argc > 5) ? argv[5] : false;
    document.cookie = name + "=" + value +
      ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
      ((path == null) ? "" : ("; path=" + path)) +
      ((domain == null) ? "" : ("; domain=" + domain)) +
      ((secure == true) ? "; secure" : "");
}

function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = GetCookie(name);
    document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString();
}




// Common Functions


var getCheckBox = function (name) {
    var r = '';
    var c = document.getElementsByName(name);
    for (i = 0; i < c.length; i++) {
        if (c[i].checked == true) {
            r = r + c[i].value + ',';
        }
    }
    if (r.length > 0) {
        r = r.substring(0, r.length - 1);
    }
    return r;
};


var addFavor = function () {
    if (!isFF()) {
        window.external.AddFavorite(location.href, document.title)
    }
    else {
        window.sidebar.addPanel(document.title, location.href, document.title);
    }
};



// Common Class



var StringBuilder = function () {
    this._strings = new Array();
};

StringBuilder.prototype.append = function (str) {
    this._strings.push(str);
};

StringBuilder.prototype.toString = function () {
    return this._strings.join("");
};


var QueryString = function () {
    var name, value, i;
    var str = location.href;
    var num = str.indexOf("?");
    str = str.substr(num + 1);
    var arrtmp = str.split("&");
    for (i = 0; i < arrtmp.length; i++) {
        num = arrtmp[i].indexOf("=");
        if (num > 0) {
            name = arrtmp[i].substring(0, num).toLowerCase();
            value = arrtmp[i].substr(num + 1);
            this[name] = value;
        }
    }
};