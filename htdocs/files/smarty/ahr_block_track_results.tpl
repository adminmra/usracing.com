<div id="home-row-3" class="subcolumns home-row">
  <div class="left-col">
    <div class="left-col-inside">
      <div class="block">
        <!-- Start TrackResults -->
<div id="track-results-block" class="listings">
  <ul class="tabs"><!-- the tabs -->
    <li>
      <a href="#"><span class="b1">Todays&#39; Tracks</span>
      <span class="b1-border"></span></a>
    </li>
    <li>
      <a href="#"><span class="b2">Results</span>
      <span class="b2-border"></span></a>
    </li>
  </ul><!-- end tabs -->
  <div class="panes">
    <div class="pane1">
      <div class="items1">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										<tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=BEL&race=3&racename=Belmont Park" title="Belmont Park">Belmont Park</a></td>
          <td>29</td>

          <td>3</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=BPN&race=1&racename=Balmoral Park Night" title="Balmoral Park Night">Balmoral Park Night</a></td>
          <td>99</td>

          <td>1</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=DLD&race=2&racename=Delaware Park" title="Delaware Park">Delaware Park</a></td>
          <td>8</td>

          <td>2</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=FPD&race=1&racename=Fairplex " title="Fairplex ">Fairplex </a></td>
          <td>99</td>

          <td>1</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=FRE&race=5&racename=Freehold Raceway" title="Freehold Raceway">Freehold Raceway</a></td>
          <td>20</td>

          <td>5</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=HRT&race=1&racename=Harrington Raceway" title="Harrington Raceway">Harrington Raceway</a></td>
          <td>99</td>

          <td>1</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=HTN&race=3&racename=Hoosier Park Thoroughbred" title="Hoosier Park Thoroughbred">Hoosier Park Thoroughbred</a></td>
          <td>18</td>

          <td>3</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr><tr class="tracks-cell"><td class="width50perc"><a href="/todayshorseracing?id=IJN&race=1&racename=Indiana Downs (H)" title="Indiana Downs (H)">Indiana Downs (H)</a></td>
          <td>99</td>

          <td>1</td>
          <td class="width67"><a href="/racebook"><img src="/themes/images/betnow-default.gif" onmouseover="this.src='/themes/images/betnow-hover.gif'" onmouseout="this.src='/themes/images/betnow-default.gif';" alt="Bet Now" /></a></td></tr> </tbody></table>      </div><!-- End Items1 -->
      <div class="boxfooter"><span class="bold"><a href="/drupal/tracks">View All Tracks</a></span></div>
    </div><!-- End Pane1 -->
    <div class="pane2">
      <div class="items2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										
							<tr class="tracks-cell"><td class="width50perc" colspan="4">No Closed Tracks. Check Back Later.</td></tr> </tbody></table>      </div><!-- End Items2 -->
      <div class="boxfooter"><span class="bold"><a href="/drupal/tracks">View All Results</a></span></div>
    </div><!-- End Pane2 -->
  </div><!-- /.panes -->
</div><!-- end listings -->
<!-- End TrackResults -->
      </div>
      <div class="box-shd"></div>
    </div>
  </div>
  <div class="right-col">
    <div class="right-col-inside">
      <div class="block ">
        <div id="leaders">
  <div class="boxheader">

<div class="social">
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style">
<a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b5f50dc32998263" class="addthis_button_compact" addthis:url="http://www.allhorseracing.com/leaders/jockeys">Share</a></div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b5f50dc32998263"></script>
<!-- AddThis Button END -->
</div>

    <div class="boxicon"><img src="/drupal/themes/images/leadersicon.gif" alt="Top Leaders" /><h1>Top Leaders</h1></div>
  </div>
  <div id="leaders-content">
    <div class="jockeys">
      <div class="leaders-header">
        <div class="cat">JOCKEYS</div>
        <div class="catlink"><a href="/leaders/jockeys">View the Top Ten</a></div>
      </div>
      <div class="leaders-info">
        <div class="leaders-icon"><img src="/drupal/themes/images/jockeyicon.gif" alt="Jockeys" /></div>
        <div class="leaders-name" id="name1">1. John R. Velazquez</div><br />
        <div class="leaders-table">
          <table width="325" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
                <td id="j-starts">865</td>
                <td id="j-1sts">181</td>
                <td id="j-2nds">125</td>
                <td id="j-3rds">113</td>
                <td id="j-purses">$11,226,583</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="horses">
      <div class="leaders-header">
        <div class="cat">HORSES</div>
        <div class="catlink"><a href="/leaders/horses">View the Top Ten</a></div>
      </div>
      <div class="leaders-info">
        <div class="leaders-icon"><img src="/drupal/themes/images/horsesicon.gif" alt="Horses" /></div>
        <div class="leaders-name" id="name2">1. Super Saver</div><br />
        <div class="leaders-table">
          <table width="325" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
				<td id="j-starts">5</td>
                <td id="j-1sts">1</td>
                <td id="j-2nds">1</td>
                <td id="j-3rds">1</td>
                <td id="j-purses">$1,715,200</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="trainers">
      <div class="leaders-header">
        <div class="cat">TRAINERS</div>
        <div class="catlink"><a href="/leaders/trainers">View the Top Ten</a></div>
      </div>
      <div class="leaders-info">
        <div class="leaders-icon"><img src="/drupal/themes/images/trainersicon.gif" alt="Trainers" /></div>
        <div class="leaders-name" id="name3">1. Todd A. Pletcher</div><br />
        <div class="leaders-table">
          <table width="325" border="0" cellpadding="0" cellspacing="0">
            <tbody>
              <tr class="bold">
                <td class="width53">Starts</td>
                <td class="width53">1sts</td>
                <td class="width53">2nds</td>
                <td class="width53">3rds</td>
                <td class="width113">Purses</td>
              </tr>
              <tr>
				<td id="j-starts">793</td>
                <td id="j-1sts">213</td>
                <td id="j-2nds">128</td>
                <td id="j-3rds">110</td>
                <td id="j-purses">$16,903,703</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
      <div class="box-shd"></div>
    </div>
  </div>
</div>