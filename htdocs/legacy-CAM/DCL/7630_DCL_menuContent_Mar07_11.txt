<div class="sidebar1-menu-item first" onclick="sG('blackjack');"> <span id="ex-blackjack" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Blackjack</span> </div>
<div class="submenu" id="blackjack" style="display: none;">
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('blackjack');">Blackjack</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('blackjacksuper7');">Blackjack Super 7's</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('blackjackswitch');">Blackjack Switch</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('spanishblackjack');">Spanish Blackjack</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('blackjack52');">Blackjack 52</a></div>
</div>
<div class="sidebar1-menu-item" onclick="sG('pokergames');"> <span id="ex-pokergames" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Poker Games</span> </div>
<div class="submenu" id="pokergames" style="display: none;">
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('caribbeanstud');">Caribbean Stud</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('threecardpoker');">Three Card Poker</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('casinoholdem');">Casino Hold&acute;em</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('showdownpoker');">Showdown Poker</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('freeride');">Free Ride</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('paigow');">Pai-Gow Poker</a></div>
</div>
<div class="sidebar1-menu-item" onclick="sG('tablegames');"> <span id="ex-tablegames" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Table Games</span> </div>
<div class="submenu" id="tablegames" style="display: none;">
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('roulette');">American Roulette</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('europeanroulette');">European Roulette</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('craps');">Craps</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('baccarat');">Baccarat</a></div>
</div>
<div class="sidebar1-menu-item" onclick="sG('videopoker');"> <span id="ex-videopoker" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Video Poker</span> </div>
<div class="submenu" id="videopoker" style="display: none;">
	<div style="height:150px; overflow:auto;">
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('deuceswild');">Deuces Wild</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('jacksorbetter');">Jacks or Better</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('jokerswild');">Jokers Wild</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Double_Bonus_Poker');">Double Bonus</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('All_American');">All American</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Five_Draw_Poker');">Five Draw</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Bonus_Poker');">Bonus Poker</a></div>
	</div>
</div>
<div class="sidebar1-menu-item" onclick="sG('mpvideopoker');"> <span id="ex-mpvideopoker" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Multihand Poker</span> </div>
<div class="submenu" id="mpvideopoker" style="display: none;">
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_All_American');">All American</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Bonus_Deluxe');">Bonus Deluxe</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Bonus_Poker');">Bonus Poker</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Deuces_Wild');">Deuces Wild</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Double_Bonus_Poker');">Double Bonus Poker</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Double_Jackpot_Poker');">Double Jackpot Poker</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Jacks_or_Better');">Jacks or Better</a></div>
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Multihand_Joker_Poker');">Joker Poker</a></div>
</div>
<div class="sidebar1-menu-item" onclick="sG('pvideopoker');"> <span id="ex-pvideopoker" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Pyramid Video Poker</span> </div>
<div class="submenu" id="pvideopoker" style="display: none;">
	<div style="height:150px; overflow:auto;">
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Aces_and_Faces_Poker');">Aces and Faces</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Bonus_Poker');">Bonus Poker</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Poker_Bonus_Deluxe');">Bonus Deluxe</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Deuces_Wild');">Deuces Wild</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Double_Jackpot_Poker');">Double Jackpot</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Double_Bonus');">Pyramid Double Bonus</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Double_Jackpot_Poker');">Pyramid Double Jackpot</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Jacks_or_Better');">Pyramid Jacks or Better</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pyramid_Joker_Poker');">Pyramid Joker Poker</a></div>
	</div>
</div>
<div class="sidebar1-menu-item" onclick="sG('5reelslots');"> <span id="ex-5reelslots" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">5-Reel Slots</span> </div>
<div class="submenu" id="5reelslots" style="display: none;">
	<div style="height:150px; overflow:auto;">
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Mr_Vegas');">Mr. Vegas</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Enchanted');">Enchanted</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Barbary_Coast');">Barbary Coast</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Once_Upon_a_Time');">Once Upon a Time</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Slotfather');">The SlotFather</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Aztec_Treasures');">Aztec Treasures</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Heist');">Heist</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Gladiator');">Gladiator</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Three_Wishes');">3 Wishes</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Treasure_Room');">Treasure Room</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Mad_Scientist');">Mad Scientist</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Glam_Life');">Glam Life</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('The_Ghouls');">The Ghouls</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Invaders');">Invaders</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Wizards_Castle');">Wizards Castle</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Pharaoh_King');">Pharaoh King</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Reel_Outlaws');">Reel Outlaws</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Back_in_Time');">Back in Time</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('The_Bees');">The Bees</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Monkey_Money');">Monkey Money</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Out_of_This_World');">Out of This World</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Jackpot_Jamba');">Jackpot Jamba</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Ghouls_Gold');">Ghoul's Gold</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Chase_the_Cheese');">Chase the Cheese</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Triple_Crown');">Triple Crown</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Lucky7');">Lucky7</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Diamond_Dreams');">Diamond Dreams</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Diamond_Progressive');">Progressive Diamond Jackpot</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Mermaids_Pearl');">Mermaids Pearl</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Captain_Cash');">Captain Cash</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('sunkentreasure');">Sunken Treasure</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('hotrod');">Hot Rod</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('reelsports');">Reel Sports</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('tropicalisland');">Tropical Island</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:Redirect('clubseduction');">Club Seduction</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Enchanted');">Enchanted</a></div>
		<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('Scratcherz');">Scratcherz</a></div>
	</div>
</div>
<div class="sidebar1-menu-item" onclick="sG('racing');"> <span id="ex-racing" class="expandable"><img src="{staticPath}/static/DCL/_default/plus_button.gif" width="9" height="9" /></span> <span class="textPos">Horse Racing</span> </div>
<div class="submenu" id="racing" style="display: none;">
	<div class="sidebar1-menu-subitem"><a href="javascript:RedirectTPI('VRB3d');">Virtual Racing</a></div>
</div>