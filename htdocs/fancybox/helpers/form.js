function validateEmail(email) { 
	var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return reg.test(email);
}

$(document).ready(function() {
	$(".modalbox").fancybox();
	$("#contact").submit(function() {
		return false;
	});
	
	$("#send").on("click", function(){
		var emailval = $("#email").val();
		var msgval = $("#zipcode").val();
		var msglen = msgval.length;
		var mailvalid = validateEmail(emailval);
		
		if (mailvalid == false) {
			$("#email").addClass("error");
		}
        $("#email").change(function() {
            $("#email").removeClass("error");
        });
		
		if (msglen < 4) {
			$("#zipcode").addClass("error");
		}
        $("#msg").change(function() {
            $("#zipcode").removeClass("error");
        });
		
		if (mailvalid == true && msglen >= 4) {
			$("#send").replaceWith("<em>submitted...</em>");
			
			$.ajax({
				type: 'POST',
				url: '/capture/index.php',
				data: $("#contact").serialize(),
				success: function(data) {
					alert('sudip');
					if(data == "true") {
						$("#contact").fadeOut("fast", function(){
							$(this).before("<p><strong>Success! You have been subscribed, thanks!</strong></p>");
							setTimeout("$.fancybox.close()", 2000);
						
						});
					}
				}
			});
			
		}
	});
});