<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Who Do You Like in the Sam F. Davis Stakes? | US Racing</title>
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<!-- news libraries here -->
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">


<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="https://www.usracing.com/news/xmlrpc.php">

<!-- All in One SEO Pack 2.3.9.2 by Michael Torbert of Semper Fi Web Design[222,284] -->
<meta name="description"  content="A field of nine 3-year-olds in search of 10 valuable Kentucky Derby (GI) points is expected to line up for Saturday’s 37th running of the Sam F. Davis Stakes" />

<meta name="keywords" itemprop="keywords" content="2017 kentucky derby,kentucky derby odds,fact finding,mccracken,no dozing,tampa bay downs,2017 sam f. davis stakes,tampa bay derby" />

<link rel="canonical" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes" />
<!-- /all in one seo pack -->

<!-- This site is optimized with the Yoast SEO plugin v3.6 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Who Do You Like in the Sam F. Davis Stakes? | US Racing" />
<meta property="og:description" content="A field of nine 3-year-olds in search of 10 valuable Kentucky Derby (GI) points is expected to line up for Saturday’s 37th running of the Sam F. Davis Stakes (GIII) at Tampa Bay Downs, a 1 1/16-mile main track test that serves as the traditional prep for the Tampa Bay Derby (GII), which will be &hellip;" />
<meta property="og:url" content="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes" />
<meta property="og:site_name" content="US Racing" />
<meta property="article:author" content="https://www.facebook.com/redransom" />
<meta property="article:tag" content="2017 Kentucky Derby" />
<meta property="article:tag" content="2017 Sam F. Davis Stakes" />
<meta property="article:tag" content="Fact Finding" />
<meta property="article:tag" content="Kentucky Derby odds" />
<meta property="article:tag" content="McCracken" />
<meta property="article:tag" content="No Dozing" />
<meta property="article:tag" content="Tampa Bay Derby" />
<meta property="article:tag" content="Tampa Bay Downs" />
<meta property="article:section" content="Kentucky Derby Road" />
<meta property="article:published_time" content="2017-02-10T22:42:03+00:00" />
<meta property="og:image" content="https://www.usracing.com/news/wp-content/uploads/2014/03/tampa-bay-downs.png" />
<meta property="og:image:width" content="591" />
<meta property="og:image:height" content="331" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="A field of nine 3-year-olds in search of 10 valuable Kentucky Derby (GI) points is expected to line up for Saturday’s 37th running of the Sam F. Davis Stakes (GIII) at Tampa Bay Downs, a 1 1/16-mile main track test that serves as the traditional prep for the Tampa Bay Derby (GII), which will be [&hellip;]" />
<meta name="twitter:title" content="Who Do You Like in the Sam F. Davis Stakes? | US Racing" />
<meta name="twitter:image" content="https://www.usracing.com/news/wp-content/uploads/2014/03/tampa-bay-downs.png" />
<meta name="twitter:creator" content="@MargaretRansom" />
<!-- / Yoast SEO plugin. -->

<link rel='dns-prefetch' href='//www.usracing.com' />
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="US Racing &raquo; Feed" href="https://www.usracing.com/news/feed" />
<link rel="alternate" type="application/rss+xml" title="US Racing &raquo; Comments Feed" href="https://www.usracing.com/news/comments/feed" />
<link rel="alternate" type="application/rss+xml" title="US Racing &raquo; Who Do You Like in the Sam F. Davis Stakes? Comments Feed" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes/feed" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.usracing.com\/news\/wp-includes\/js\/wp-emoji-release.min.js"}};
			!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='apss-font-awesome-css'  href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Open+Sans&#038;ver=7b91f264723df051729d0a998925e307' type='text/css' media='all' />
<link rel='stylesheet' id='apss-frontend-css-css'  href='https://www.usracing.com/news/wp-content/plugins/accesspress-social-share/css/frontend.css' type='text/css' media='all' />
<link rel='stylesheet' id='sab-plugin-css'  href='https://www.usracing.com/news/wp-content/plugins/simple-author-box/css/simple-author-box.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-pagenavi-css'  href='https://www.usracing.com/news/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' />
<link rel='stylesheet' id='_usr-bootstrap-css'  href='https://www.usracing.com/news/../assets/plugins/bootstrap/css/bootstrap.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='_usr-assets-style-css'  href='https://www.usracing.com/news/../assets/css/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='_usr-style-css'  href='https://www.usracing.com/news/wp-content/themes/usracing/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='_usr-style-plugins-css'  href='https://www.usracing.com/news/../assets/css/plugins.css' type='text/css' media='all' />
<link rel='stylesheet' id='acfpb-public-css'  href='https://www.usracing.com/news/wp-content/plugins/acf-page-builder/includes/../public/css/acfpb_styles.css' type='text/css' media='all' />
<link rel='stylesheet' id='wavesurfer_flat-icons-css'  href='https://www.usracing.com/news/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_flat-icons.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' type='text/css' media='all' />
<script type='text/javascript' src='https://www.usracing.com/news/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='https://www.usracing.com/news/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='https://www.usracing.com/news/../assets/plugins/bootstrap/js/bootstrap.min.js'></script>
<script type='text/javascript' src='https://www.usracing.com/assets/js/news.js'></script>
<link rel='https://api.w.org/' href='https://www.usracing.com/news/\' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.usracing.com/news/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.usracing.com/news/wp-includes/wlwmanifest.xml" /> 

<link rel='shortlink' href='https://www.usracing.com/news' />
<link rel="alternate" type="application/json+oembed" href="https://www.usracing.com/news/" />
<link rel="alternate" type="text/xml+oembed" href="https://www.usracing.com/news/" />
<meta property="fb:app_id" content="825577710919578"/><meta name="news_keywords" content="2017 Kentucky Derby, 2017 Sam F. Davis Stakes, Fact Finding, Kentucky Derby odds, McCracken, No Dozing, Tampa Bay Derby, Tampa Bay Downs" />
<script type="text/javascript">
(function(url){
	if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }
	var addEvent = function(evt, handler) {
		if (window.addEventListener) {
			document.addEventListener(evt, handler, false);
		} else if (window.attachEvent) {
			document.attachEvent('on' + evt, handler);
		}
	};
	var removeEvent = function(evt, handler) {
		if (window.removeEventListener) {
			document.removeEventListener(evt, handler, false);
		} else if (window.detachEvent) {
			document.detachEvent('on' + evt, handler);
		}
	};
	var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');
	var logHuman = function() {
		var wfscr = document.createElement('script');
		wfscr.type = 'text/javascript';
		wfscr.async = true;
		wfscr.src = url + '&r=' + Math.random();
		(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);
		for (var i = 0; i < evts.length; i++) {
			removeEvent(evts[i], logHuman);
		}
	};
	for (var i = 0; i < evts.length; i++) {
		addEvent(evts[i], logHuman);
	}
})('//news.usracing.com/?wordfence_logHuman=1&hid=412CC371468D9A9C56407BF2BEEAC747');
</script><style type="text/css">.saboxplugin-wrap .saboxplugin-gravatar img {-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;-o-border-radius:50%;border-radius:50%;}.saboxplugin-wrap {margin-top:0px;}.saboxplugin-wrap {margin-bottom:0px;}.saboxplugin-wrap .saboxplugin-authorname {font-size:18px; line-height:25px;}.saboxplugin-wrap .saboxplugin-desc {font-size:14px; line-height:21px;}.saboxplugin-wrap .saboxplugin-web {font-size:14px;}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-color {font-size:21px;}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-color:before {width:36px; height:36px; line-height:37px; }.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-grey {font-size:18px;}</style><script type="text/javascript" src="https://www.usracing.com/news/wp-content/themes/usracing/js/put5qvj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//usracing.com/news/wp-content/themes/usracing/css/news.css" rel="stylesheet" type="text/css">

</head>

<body class="single single-post postid-6069 single-format-standard">
	
<!-- Top -->
<div id="top" class="top">
 <div class="container2">
	  <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>
  <a class="logo logo-lrg" href="/" title="US Racing"><img id="logo-header" src="/img/usracing.png" alt="logo"></a>
  <a class="logo logo-sm" href="/" rel="nofollow"><img src="/img/usracing-sm.png"></a>
  <div class="topR">


  </div>

  <div class="login-header">
    <!--
    <a class="btn btn-sm btn-default loginToggle collapsed" data-toggle="collapse" data-target=".login-responsive-collapse" rel="nofollow"><span>Login</span><i class="fa fa-angle-down"></i></a>
    -->

    <a href='//www.betusracing.ag/login' class="btn btn-sm btn-default" id="login_button">Login</a>

    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup/?=h"  rel="nofollow">
        <span>Sign Up</span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>
 </div>
</div><!--/top-->

<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">

	<!-- Toggle NAV -->
	 <!-- <div class="navbar-header">
   <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
     <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    
    <a id="or" class="navbar-toggle collapsed"><span style="display:inline">EXPLORE</span><i class="glyphicon glyphicon-remove" style="display:none"></i></a>
	</div>-->	

    <!-- Nav Itms -->
	<div class="collapse navbar-collapse navbar-responsive-collapse">
	<ul class="nav navbar-nav nav-justified">
	<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
  <!-- 	<li><a href="/upcoming-horse-races"  >Upcoming Horse Races</a></li> -->
    
   
   
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
     <li><a href="/odds" >Today's Entries</a></li>
     <li><a href="/results"  >US Racing Results</a></li> 
    <!--    
     
    
    <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
          <li><a href="/kentucky-derby/odds" >Kentucky Derby Odds</a></li>
           <li><a href="/kentucky-derby/props" >Kentucky Derby Props</a></li>
         <li><a href="/kentucky-oaks/odds" >Kentucky Oaks Odds</a></li>
         <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a></li>
         <li><a href="/bet-on/triple-crown" >Triple Crown Odds</a></li>  -->
           <!-- <li><a href="/casino/breeders-cup-blackjack-tournament"  >BC Blackjack Tournament</a></li> -->
     <li><a href="/racetracks"  >Racetracks</a></li>
     <li><a href="/past-performances" >Free Past Performances</a></li>
     
     
<!--       <li><a href="/news/tag/pegasus-world-cup/" >Pegasus World Cup</a></li>  -->
     
     
    <!-- 
  <li><a href="/news"  >News</a></li>
       <li><a href="/horse-racing-videos"  >Horse Racing Videos</a></li>
 -->
        <!--  <li><a href="/blog/" >US Racing Blog</a></li>
             <li><a href="/signup/?todays-racing=Signup-Today">Sign Up Today!</a></li>  -->
    <!--       
  
    <li><a href="/racetracks"  >Our Racetracks</a></li>
    <li><a href="/news" >Horse Racing News</a></li>
    <li><a href="/about" >About Us</a></li>
    <li><a href="/graded-stakes-races" >Races to Watch</a></li>
    <li><a href="/" title="Online Horse Betting">Online Horse Betting</a>
    <li><a href="/how-to/place-a-bet" title="How To Place A Bet" rel="nofollow">How To Place A Horse Bet</a></li>
    <li><a href="/rebates" title="Horse Raceing Rebates">Rebates</a></li>
    <li><a href="/racetracks" title="Horse Racetracks">Horse Racetracks</a></li>
    <li><a href="/famous-jockeys" title="Famous Horses, Jockeys &amp; Trainers" rel="nofollow">Famous Jockeys</a></li>
    <li><a href="/advance-deposit-wagering" title="Advance Deposit Wagering">Advance Deposit Wagering</a></li>
    <li><a href="/horse-racing-games" title="Horse Racing Games">Horse Racing Game</a></li>
    <li><a href="/haskell-stakes" title="Stakes Races">Stakes Races</a></li>
    <li><a href="/haskell-stakes" title="Haskell Stakes">Haskell Stakes</a></li>
    <li><a href="/travers-stakes" title="Travers Stakes">Travers Stakes</a></li>
    <li><a href="/santa-anita-derby" title="Santa Anita Derby">Santa Anita Derby</a></li>
    <li><a href="/arkansas-derby" title="Arkansas Derby">Arkansas Derby</a></li>
    <li><a href="/illinois-derby" title="Illinois Derby">Illinois Derby</a></li>
    <li><a href="/florida-derby" title="Florida Derby">Florida Derby</a></li>
    <li><a href="/bluegrass-stakes" title="Blue Grass Stakes">Blue Grass Stakes</a></li>
    <li><a href="/racetracks" title="Handicapping">Popular Racetracks</a></li>
    <li><a href="/belmont-park" title="">Belmont Park</a></li>
    <li><a href="/delmar" title="">Delmar</a></li>
    <li><a href="/churchill-downs" title="">Churchill Downs</a></li>
    <li><a href="/gulfstream-park" title="">Gulfstream Park</a></li>
    <li><a href="/pimlico" title="">Pimlico</a></li>
    <li><a href="/santa-anita-park" title="">Santa Anita Park</a></li>
    <li><a href="/saratoga" title="">Saratoga</a></li>
     -->
  </ul>
</li>

<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Latest Odds<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">   
   <!-- 
 <li><a href="/bet-on/american-pharoah"  >Bet American Pharoah</a></li>
       
 -->
<li><a href="/odds"  >Today's Horse Racing Odds</a></li>
<!--   <li><a href="/pegasus-world-cup/odds"  >Pegasus World Cup Odds</a></li>  -->
    <li><a href="/kentucky-derby/odds"  >Kentucky Derby Odds</a></li>
<!--      <li><a href="/kentucky-oaks/odds"  >Kentucky Oaks Odds</a></li>  -->

    <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li>
    <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li>
  <!--   <li><a href="/kentucky-derby/props" >Kentucky Derby Prop Bets</a></li>
    <li><a href="/kentucky-derby/match-races" >Kentucky Derby Match Races</a></li>
    <li><a href="/kentucky-derby/margin-of-victory" >Kentucky Derby Margin of Victory</a></li>

    <li><a href="/preakness-stakes/odds"  >Preakness Stakes Odds</a></li>
    <li><a href="/belmont-stakes/odds"  >Belmont Stakes Odds</a></li>
    <li><a href="/breeders-cup/odds"  >Breeders' Cup Odds</a></li>
 -->

    <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li>
	
  

    <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li> 
     <!--  <li><a href="/melbourne-cup"  >Bet on the Melbourne Cup</a></li>  -->
<!--   <li><a href="/breeders-cup/juvenile"  >BC Juvenile Odds</a></li>													<!--  Saturday  -->
<!--  <li><a href="/breeders-cup/turf"  >BC Turf Odds</a></li>		  -->													<!--  Saturday  -->
<!--  <li><a href="/breeders-cup/juvenile-fillies-turf"  >BC Juvenile Fillies Turf</a></li>	  -->							<!--  Friday  -->
<!--  <li><a href="/breeders-cup/distaff"  >BC Distaff Odds</a></li>			  -->											<!--  Friday  --> 

 <!--     <li><a href="/bet-on/triple-crown" >Triple Crown Odds</a></li>  -->
<!--       <li><a href="/odds/us-presidential-election" >US Presidential Election </a></li>  -->
   
  </ul>
</li><!--Horse Racing News ================================================ -->   
<li class="dropdown"><a href="/news" >Racing News</a>
     
<!-- <ul class="dropdown-menu">

<li><a href="/how-to/wager-on-horses" title="How to wager on Horses">How to Wager on Horses</a></li>
<li><a href="/how-to/bet-on-horses">How to Bet on Horses</a></li>

      
      
     </ul> -->   
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >Kentucky Derby<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
	    <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
	    <li><a href="/news/kentucky-derby-road-to-the-roses">Kentucky Derby News</a></li>
   <li><a href="/kentucky-derby/odds" >Kentucky Derby Odds</a></li>
 
   <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li>
   <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li>
    <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li> 
   <li><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li>
   <!-- 
<li><a href="/road-to-the-roses">Road to the Roses</a></li>
   <li><a href="/twin-spires">Twin Spires</a></li>
   <li><a href="/kentucky-derby/winners" >Kentucky Derby Winners</a></li>
   <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
   <li><a href="/kentucky-derby">Kentucky Derby Facts</a></li>
   <li><a href="/kentucky-derby/future-wager"  >Kentucky Derby Future Wager</a></li>
 -->

<!-- 
   <li><a href="/kentucky-derby/props" >Kentucky Derby Props</a></li>
   <li><a href="/kentucky-derby/match-races">Kentucky Derby Match Races</a></li>
 -->
   
   
   
   
         <!--  <li><a href="/kentucky-derby/jockey-betting" >Bet on Jockeys</a></li>
          <li><a href="/kentucky-derby/trainer-betting" >Bet on Trainers</a></li>  -->
           
         <!--<li><a href="/kentucky-oaks/odds" >Kentucky Oaks Odds</a></li>-->
         
            
      
     
    <!-- <li><a href="/blog/2015-kentucky-derby-post-positions/">Kentucky Derby Post Postions</a></li>
      
    <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
   
    <li><a href="/kentucky-derby/match-races">Kentucky Derby Props Bets</a></li>
       <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Margin of Victory Bets</a></li>
    -->
    
    
	    
    <!--<li><a href="/road-to-the-roses-videos">Road to the Roses Race Videos</a></li>

-->
<!--<li><a href="/kentucky-derby/video"  >Kentucky Derby Video</a></li>
<li><a href="/mint-julep-recipe"  >Mint Julep Recipes</a></li>
-->
 
<!--<li><a href="/kentucky-derby/free-bet"  >FREE DERBY BET</a></li>      -->
    
    
  </ul>
</li><!-- How to Bet ================================================ -->   
<li class="dropdown"><a href="/how-to/bet-on-horses" >How to Bet</a>
     
<!-- <ul class="dropdown-menu">

<li><a href="/how-to/wager-on-horses" title="How to wager on Horses">How to Wager on Horses</a></li>
<li><a href="/how-to/bet-on-horses">How to Bet on Horses</a></li>
<li><a href="/how-to/read-the-daily-racing-form">How to Read the Daily Racing Form</a></li>
<li><a href="/how-to/read-past-performances" >Past Performances</a></li>
<li><a href="/horse-betting/beginner-Tips" title="Beginner Horse Betting Tips" >Beginner Tips</a></li>
<li><a href="/horse-racing-terms" title="Horse Racing Terms and Terminology">Horse racing terms</a></li>



<li><a href="/horse-betting/straight-wager" title="How to Place a Straight Wager" rel="nofollow">Straight Wager</a></li>
<li><a href="/horse-betting/win" title="Win, Place and Show Bets" rel="nofollow">Win - </a><a href="/horse-betting/place" title="Win, Place and Show Bets" rel="nofollow"> Place - </a><a href="/horse-betting/Show" title="Win, Place and Show Bets" rel="nofollow">Show</a></li>

<li><a href="/horse-betting/exacta" title="Exacta Horse Bets" rel="nofollow">Exacta</a></li>
<li><a href="/horse-betting/trifecta" title="Trifecta Horse Bets" rel="nofollow">Trifecta</a></li>
<li><a href="/horse-betting/superfecta" title="Superfecta Horse Bets" rel="nofollow">Superfecta</a></li>
<li><a href="/horse-betting/quinella" title="Quinella Horse Bets" rel="nofollow">Quinella</a></li>
<li><a href="/horse-betting/daily-double" title="Daily Double Horse Bets" rel="nofollow">Daily Double</a></li>
<li><a href="/horse-betting/exotic" title="Exotic Horse Bets" rel="nofollow">Exotic Horse Bet</a></li>





<li><a href="#" title="#" rel="nofollow">Instructional Videos</a></li>
<li><a href="/how-to/bet-on-horses" title="" >How to Bet on Horses</a></li>

<li><a href="/how-to/read-the-daily-racing-form" title="">How to Read the Daily Racing Form</a></li>
<li><a href="/how-to/read-past-performances" title="" rel="nofollow">Past Performances</a></li>

<li><a href="#" title="Advanced Horse Bets" rel="nofollow">Advanced Horse Bets</a></li>
<li><a href="/horse-betting/across-the-board" title="Across the Board Bets" rel="nofollow">Across the Board</a></li>
<li><a href="/horse-betting/place-Pick-all" title="Place Pick All Bets" rel="nofollow">Place Pick All</a></li>
<li><a href="/horse-betting/box-bet" title="Box Horse Bets" rel="nofollow">Box Bets</a></li>
<li><a href="/horse-betting/key-a-Horse" title="Key a Horse" rel="nofollow">Key a Horse</a></li>
<li><a href="/horse-betting/pick-three" title="Pick 3 Horse Bets" rel="nofollow">Pick Three</a></li>
<li><a href="/horse-betting/pick-four" title="Pick 4 Bets" rel="nofollow">Pick Four</a></li>
<li><a href="/horse-betting/pick-six" title="Pick 6 Horse Bets" rel="nofollow">Pick Six</a></li>
<li><a href="/horse-racing-terms" title="Horse Racing Terms and Terminology">Horse Racing Terms</a></li>

<li><a href="#" title="Horse Betting 101" rel="nofollow">Horse Betting 101</a></li>
<li><a href="/horse-betting/beginner-Tips" title="Beginner Horse Betting Tips" rel="nofollow">Beginner Tips</a></li>
<li><a href="/legend" title="" rel="nofollow">Horse Racing Legend</a></li>
<li><a href="/horse-betting/thoroughbred-Tips" title="" rel="nofollow">Thoroughbred Tips</a></li>
<li><a href="/horse-betting/quarter-Horse-Tips" title="" rel="nofollow">Quarter Horse Tips</a></li>
<li><a href="/horse-betting/money-management" title="" rel="nofollow">Money Management</a></li>
<li><a href="/horse-betting/claiming-race" title="Claiming Race" >Claiming Race</a></li>
<li><a href="/horse-betting/maiden-race" title="Maiden Race" rel="nofollow">Maiden Race</a></li>
<li><a href="/horse-betting/allowance-race" title="Allowance Races" rel="nofollow">Allowance Races</a></li>
<li><a href="/horse-betting/starter-allowance" title="Starter Allowance" rel="nofollow">Starter Allowance</a></li>


<li><a href="/horse-betting/handicapping-stakes-races" title="Stakes and handicap" rel="nofollow">Stakes &amp; Handicapping</a></li>

      
      
     </ul> -->   
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
 <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
<!--    <li><a href="/breeders-cup/free-bet">Free Breeders' Cup Bet</a></li>   -->
<!--    <li><a href="/kentucky-derby/free-bet">Free Derby Bet</a></li> 
  <li><a href="/belmont-stakes/free-bet">Free Belmont Bet</a></li> -->
   <li><a href="/promos/casino-rebate">50% Casino Cash Back</a></li>
 <!--    <li><a href="/promos/blackjack-tournament">Triple Crown Blackjack Tournament</a></li> -->
    <li><a href="/rebates">8% Rebates</a></li>
    <li><a href="/refer-a-friend">Refer a Friend Bonus</a></li>
<!--      <li><a href="/promos/derby-wager-guide">Kentucky Derby Betting Guide</a></li> 
         <li><a href="/super-bowl" >Super Bowl Deposit Bonus</a></li>  -->
     <li><a href="/getting-started">Have a Promo Code?</a></li>
 
  </ul>
</li><li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  <li><a href="/getting-started" >Getting Started</a></li>
    <li><a href="/about" >About Us</a></li>
    <!-- <li><a href="/legal-online-horse-betting" title="Legal Horse Betting">Legal Horse Betting</a></li> -->
    <li><a href="/best-horse-racing-site" >Best Horse Racing Site</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks Offered</a></li>
   <!--   <li><a href="/cash-bonus">$150 Signup Bonus</a></li>  -->
    <li><a href="/rebates">Daily Rebates</a></li>
    <li><a href="/payouts">Fast Two Day Payouts</a></li>
    
     <li><a href="/getting-started">Have a Promo Code?</a></li>
  <!--   <li><a href="#">Tell Your Friends</a></li>
   <li><a href="#">Our Guarantee</a></li> -->
   <li><a href="/support">Contact Us</a></li>
    <li><a href="/signup/?m=Join-Today">Join Today!</a></li>
    <!-- <li><a href="/blog" >Blog</a></li> -->
    <!-- 
     <li><a href="/live-horse-racing" >Live Racing Video</a></li>
    <li><a href="/mobile-horse-betting" >Mobile Betting</a></li>
    <li><a href="/racetracks" title="Racetracks">Racetracks</a></li>
    <li><a href="/today" title="Today&#039;s Tracks">Today&#039;s Tracks</a></li>
    <li><a href="/results" title="Horse Racing Results" >Horse Racing Results</a></li>
    <li><a href="/graded-stakes-races" title="Graded Stakes Races" >Graded Stakes Races</a></li>
    <li><a href="/news" title="Horse Racing News">Horse Racing News</a></li>
    <li><a href="/contact" title="Contact Us">Contact Us</a></li>
    <li><a href="/live-video" title="Live Video.">Live Video</a></li>
    <li><a href="/mobile-betting" title="Mobile Horse Betting">Mobile Betting</a></li>
    <li><a href="/handicapping" title="Horse Racing Handicapping">Handicapping</a></li>
     -->
  </ul>
</li>	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->


<!--
<header id="masthead" class="site-header" role="banner">
	<div class="container">
		<div class="row">
			<div class="site-header-inner col-sm-12">

									<a href="" title="" rel="home">
						<img src="" width="" height="" alt="">
					</a>
				

				<div class="site-branding">
					<h1 class="site-title"><a href="" title="" rel="home"></a></h1>
					<h4 class="site-description"></h4>
				</div>

			</div>
		</div>
	</div>
</header>

<nav class="site-navigation">
	<div class="container">
		<div class="row">
			<div class="site-navigation-inner col-sm-12">
				<div class="navbar navbar-default">
					<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				    	<span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>

				    <a class="navbar-brand" href="" title="" rel="home"></a>
				  </div>

	        
				</div>
			</div>
		</div>
	</div>
</nav>-->


<div id="left-col" class="col-medium-9">
<div class="widgetheader">
				<div class="categoriesleft">
				<select name="forma" onchange="location = this.value;">
					 <option value="/news">Latest Racing News</option>
					 <option value="/news/features">US Racing Features</option>
					 <option value="/news/analysis">Racing Analysis</option>
					 <option value="/news/handicapping-reports">Handicapping Reports</option>
					 <option value="/news/breeders-cup">Breeders' Cup News</option>
					 <option value="/news/kentucky-derby-road-to-the-roses">Kentucky Derby Road</option>
					 <option value="/news/recap">Race Recap</option>
					 <option value="/news/nfl">NFL Betting</option>
				</select>
				</div>
				<div class="searchright">
				<div class="widget widget_search"><form role="search" method="get" class="search-form" action="https://www.usracing.com/news/">
	<label>
		<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:">
	</label>
	<input type="submit" class="search-submit" value="Search">
</form>
</div>				</div> 	
			</div>
</div>
<div id="main" class="container2 news-content">
<div class="row">
<div id="left-col" class="col-medium-9">


				


<div class="headline"><h1>Who Do You Like in the Sam F. Davis Stakes?</h1></div>
<div class="content">
<article id="post-6069" class="post-6069 post type-post status-publish format-standard has-post-thumbnail hentry category-kentucky-derby-road-to-the-roses category-analysis category-features tag-2017-kentucky-derby tag-2017-sam-f-davis-stakes tag-fact-finding tag-kentucky-derby-odds tag-mccracken tag-no-dozing tag-tampa-bay-derby tag-tampa-bay-downs">
	

		<p style="text-align: justify;"><img class="alignright wp-image-6062" src="https://www.usracing.com/news/wp-content/uploads/2017/02/Sam-F-Davis-Odds.png" alt="Sam-F-Davis-Odds" width="320" height="477" srcset="https://www.usracing.com/news/wp-content/uploads/2017/02/Sam-F-Davis-Odds.png 392w, https://www.usracing.com/news/wp-content/uploads/2017/02/Sam-F-Davis-Odds-201x300.png 201w" sizes="(max-width: 320px) 100vw, 320px" />A field of nine 3-year-olds in search of 10 valuable Kentucky Derby (GI) points is expected to line up for Saturday’s 37<sup>th</sup> running of the Sam F. Davis Stakes (GIII) at Tampa Bay Downs, a 1 1/16-mile main track test that serves as the traditional prep for the Tampa Bay Derby (GII), which will be held in four weeks. This year the Sam F. Davis will carry a purse of $250,000 and has been set as the 10<sup>th</sup> race on Saturday. Post time for the feature, which is one of three graded stakes on the day, has been set at 4:45 p.m. ET.</p>
<p style="text-align: justify;">The winter weather in Florida continues to be beautiful and warm, and the weekend forecast calls for sunny skies with highs near 80. The only issue that may arise is humidity, which is expected to be fairly high and could create a more muggy, sticky day. Expect the main track to be fast all day and the turf course firm.</p>
<p style="text-align: justify;">In the early days of the Sam F. Davis, it wasn’t usually a serious consideration for trainers with legit colts and geldings headed toward the first Saturday in May at Churchill Downs. All that changed in the early 2000s when a popular runner named Burning Roma, who was fourth in the previous year’s Breeders’ Cup Juvenile (GI), captured the race before winning the Tampa Bay Derby and establishing himself as a legit graded stakes performer who ran at the elite level 36 times. Bluegrass Cat, who was second in the 2006 Kentucky Derby and Belmont Stakes (GI), followed soon after, as did notable names Any Given Saturday, General Quarters, Vinceremos and a year ago, Destin, who would go on to win the Tampa Bay Derby.</p>
<p style="text-align: justify;">Though a classic winner has yet to emerge from this race, a good field will assemble this year with the winner hoping to turn that luck around and be a factor in one of the three jewels of the Triple Crown in the spring.</p>
<p style="text-align: justify;">Tabor, Magnier and Smith’s undefeated Fact Finding skipped last week’s Holy Bull Stakes (GIII) at Gulfstream Park in favor of this spot, perhaps needing more time or perhaps dodging the previously undefeated champion and division leader Classic Empire. Regardless, the gray son of The Factor has found a good spot here to perhaps land a first graded win. Trained by Todd Pletcher, Fact Finding hasn’t done a thing wrong and enters this race off a nice win in the Smooth Air Stakes a month ago. He’s almost always been a good work horse in the mornings, though he doesn’t often go at blistering speeds, and he seems to have a nice adaptable running style that will always help him. His speed, pace and class figures are nothing to snivel at either and Pletcher’s longtime go-to jockey John Velazquez will be in town for the mount when he had multiple choices to ride the big stakes at Gulfstream. All Fact Finding may need is a clean break and trouble-free trip from post position three to get to the wire in front.</p>
<div id="attachment_6059" style="width: 1034px" class="wp-caption aligncenter"><img class="wp-image-6059 size-large" src="https://www.usracing.com/news/wp-content/uploads/2017/02/Fact-Finding-1024x819.jpg" alt="Fact Finding" width="1024" height="819" srcset="https://www.usracing.com/news/wp-content/uploads/2017/02/Fact-Finding-1024x819.jpg 1024w, https://www.usracing.com/news/wp-content/uploads/2017/02/Fact-Finding-375x300.jpg 375w, https://www.usracing.com/news/wp-content/uploads/2017/02/Fact-Finding-768x614.jpg 768w" sizes="(max-width: 1024px) 100vw, 1024px" /><p class="wp-caption-text"><strong>Fact Finding</strong> (photo via NYRA).</p></div>
<p style="text-align: justify;">Whitham Thoroughbreds’ early 2-1 favorite McCraken is also undefeated and making his first start since winning the November 26 Kentucky Jockey Club Stakes (GII) at Churchill Downs. The Ian Wilkes-trained son of Ghostzapper hasn’t been racing, but hasn’t missed a beat, working regularly and impressively all winter at his winter base of Palm Meadows. He is a stone-cold closer who will get a decent pace in front of him to run at, so if he can avoid both traffic and a wide trip turning for home, he’s going to be a tough favorite to beat. While his numbers could use a bit of a boost, it makes sense to think he’s grown up a bit and will improve with maturity and ready for a top performance. Regular jockey Brian Hernandez Jr. will be back aboard and the pair will break from post position eight.</p>
<p style="text-align: justify;">Trainer Rusty Arnold will tighten the girth on Wild Shot, who finished behind McCraken in the Kentucky Jockey Club stakes last out. The Calumet Farm homebred broke his maiden at Churchill Downs in September and, despite not reaching the winner’s circle since, his numbers have improved with each start. It’s hard to tell what he wants to do, distance-wise, as his lone win was at sprinting, but his pedigree indicates he should be fine with added ground. New jockey Robby Albarado will no doubt have him forwardly placed early, but the question mark is if he’ll be able to hold off his rivals who are either faster or more adept at the distance in the stretch.</p>
<p style="text-align: justify;">Roy and Gretchen Jackson’s Lael Stables is hoping to get back on the Triple Crown Trail with No Dozing, a homebred from the first crop of standout young sire Union Rags. He’s another who’s been off since the end of November when he finished second in the nine-furlong Remsen Stakes (GII) and his overall numbers show he belongs with the upper end of this bunch. Trainer Arnaud Delacour is based at Tampa, so this track is this colt’s winter base, and he’s been working regularly for weeks. A mid-pack type, all he may need is a decent pace and clear running room down the stretch under jockey Daniel Centeno.</p>
<p style="text-align: justify;">Tapwrit, a very expensive $1.2 million son of Tapit, has won his last two starts in maiden and non-graded stakes after his 10<sup>th</sup> and last-place debut debacle at Saratoga last summer. Another Todd Pletcher trainee, this colt is a son of the grade 1-winning Successful Appeal mare Appealing Sophie and shouldn’t have any trouble stretching out. His numbers indicate he’ll need to improve some to be competitive, but he looks like he owns some serious room to improve, even off two straight wins. He also has some tactical ability, which will help depending on pace after he and jockey Jose Ortiz break from post seven.</p>
<p style="text-align: justify;">Chance of Luck is a humbly bred local who always gives his best. A listed stakes winner over this surface, this will be his acid test. He’s posted some figures to show he’d be competitive to pick up a check for Owners JJ Brevin Stable, trainer Gerald Bennett and jockey Ron Allen Jr.</p>
<p style="text-align: justify;">Wachtel Stable and Gary Barber’s King and His Court is the most experienced in the bunch with nine career starts. He’s consistent and has three wins and four seconds, but his winning has come in restricted Ontario-bred stakes at Woodbine over the all-weather surface. If he’s meant to be on the Triple Crown trail, he better show trainer Mark Casse in here, but overall it looks like he may be just a cut below the top choices here.</p>
<p style="text-align: justify;">Conrad Farms State of Honor is another Canadian-based runner who was fairly successful on a synthetic surface before posting a nice runner-up finish in the Mucho Macho Man Stakes over Gulfstream Park’s main track last out. He’s got a win and two seconds in his last three and a best performance from the innermost post position seems possible from on the lead as it’s a good bet jockey Julien Leparoux will have to send from down along the rail to avoid traffic early.</p>
<p style="text-align: justify;">Six Gun Salute is a well-bred half-brother to millionaire and Dubai World Cup (GI) winner Well Armed. The potential has always been there, he’s just never really run to it for trainer Eoin Harty. He seems overmatched in here.</p>
<div class="saboxplugin-wrap">
<div class="saboxplugin-gravatar"><img src="https://www.usracing.com/news/wp-content/authors/Margaret_Ransom-6.jpg" class="avatar photo" alt="Margaret Ransom" width="100" height="100" /></div>
<div class="saboxplugin-authorname"><a href="https://www.usracing.com/news/author/margaret_ransom">Margaret Ransom</a></div>
<div class="saboxplugin-desc">
<div class="vcard author"><span class="fn">California native and lifelong horsewoman Margaret Ransom is a graduate of the University of Arizona’s Race Track Industry Program. She got her start in racing working in the publicity departments at Calder Race Course and Hialeah Park, as well as in the racing office at Gulfstream Park in South Florida. She then spent six years in Lexington, KY, at BRISnet.com where she helped create and develop the company’s popular newsletters, Handicapper’s Edge and Bloodstock Journal.</p>
<p>After returning to California, she served six years as the Southern California news correspondent for BloodHorse, assisted in the publicity department at Santa Anita Park and and was a contributor to many other racing publications, including HorsePlayer Magazine and Trainer Magazine. She then spent seven years at HRTV and HRTV.com in various roles as researcher, programming assistant, producer and social media and marketing manager. She has also walked hots and groomed runners, worked the elite sales in Kentucky for top-class consignors and volunteers for several race horse retirement organizations, including CARMA.</p>
<p>Margaret’s very first Breeders’ Cup was at Hollywood Park in 1984 and she has attended more than half of the Breeders’ Cups since. She counts Holy Bull as her favorite horse of all time. She lives in Pasadena with her longtime beau, Tony, three Australian Shepherds and one Golden Retriever.</span></div>
</div>
<div class="clearfix"></div>
<div class="saboxplugin-socials "><a target="_blank" href="https://www.facebook.com/redransom"><span class="saboxplugin-icon-grey saboxplugin-icon-facebook"></span></a><a target="_blank" href="https://twitter.com/MargaretRansom"><span class="saboxplugin-icon-grey saboxplugin-icon-twitter"></span></a></div>
</div>
<div id="wpdevar_comment_3" style="width:100%;text-align:left;">
		<span style="padding: 10px;font-size:22px;font-family:Times New Roman,Times,Georgia,serif;color:#000000;"></span>
		<div class="fb-comments" data-href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes" data-order-by="social" data-numposts="5" data-width="100%" style="display:block;"></div></div><div class='apss-social-share apss-theme-2 clearfix' >


				<div class='apss-facebook apss-single-icon'>
					<a rel='nofollow'  title="Share on Facebook" target='_blank' href='https://www.facebook.com/sharer/sharer.php?u=https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes'>
						<div class='apss-icon-block clearfix'>
							<i class='fa fa-facebook'></i>
							<span class='apss-social-text'>Share on Facebook</span>
							<span class='apss-share'>Share</span>
						</div>
											</a>
				</div>
								<div class='apss-twitter apss-single-icon'>
					<a rel='nofollow'  href="https://twitter.com/intent/tweet?text=Who%20Do%20You%20Like%20in%20the%20Sam%20F.%20Davis%20Stakes%3F&amp;url=https://www.usracing.com/news%2Fnews%2Fkentucky-derby-road-to-the-roses%2Flike-sam-f-davis-stakes&amp;"  title="Share on Twitter" target='_blank'>
						<div class='apss-icon-block clearfix'>
							<i class='fa fa-twitter'></i>
							<span class='apss-social-text'>Share on Twitter</span><span class='apss-share'>Tweet</span>
						</div>
											</a>
				</div>
				
				<div class='apss-linkedin apss-single-icon'>
					<a rel='nofollow'  title="Share on LinkedIn" target='_blank' href='http://www.linkedin.com/shareArticle?mini=true&amp;title=Who%20Do%20You%20Like%20in%20the%20Sam%20F.%20Davis%20Stakes%3F&amp;url=https://www.usracing.com/news/kentucky-derby-road-to-the-roses/like-sam-f-davis-stakes&amp;summary=A+field+of+nine+3-year-olds+in+search+of+10+valuable+Kentucky+Derby+%28GI%29+points+is+expected+to+line+...'>
						<div class='apss-icon-block clearfix'><i class='fa fa-linkedin'></i>
							<span class='apss-social-text'>Share on LinkedIn</span>
							<span class='apss-share'>Share</span>
						</div>
											</a>
				</div>
				</div>		</article><!-- #post-## -->
    <div class="blog-date"> <span class="posted-on">Posted on <span><time class="entry-date published" datetime="2017-02-10T22:42:03+00:00">February 10, 2017</time></span></span></div>

	<footer class="entry-meta">
		
			</footer><!-- .entry-meta -->


</div><!-- end/content -->

		

		
	    
    

	<nav role="navigation" id="nav-below" class="post-navigation">
		<h1 class="screen-reader-text">Post navigation</h1>

	
		<div class="nav-previous"><a href="https://www.usracing.com/news/handicapping-reports/weekend-race-report-cards-2" rel="prev"><span class="meta-nav">&larr;</span> Weekend Race Report Cards</a></div>		
	
	</nav><!-- #nav-below -->
	    <div class="archives-link">
        <a href="https://www.usracing.com/news/archives" > Archives</a>
    </div>
</div> <!-- end/ #left-col -->


	<div id="right-col" class="sidebar col-medium-3">

		

						<aside id="text-7" class="widget panel panel-white widget_text">			<div class="textwidget"><a href="https://www.usracing.com/rebates"><img src="https://www.usracing.com/news/wp-content/uploads/2016/11/rebate-signup-bonus.jpg" alt="8% Rebate and Sign up bonus" /></a></div>
		</aside><aside id="text-8" class="widget panel panel-white widget_text"><div class="panel-heading"><h3 class="widget-title">Categories</h3></div>			<div class="textwidget"><ul>
	<li class="cat-item"><a href="https://www.usracing.com/news">Latest Racing News</a>
</li>
	<li class="cat-item cat-item-356"><a href="https://www.usracing.com/news/features">US Racing Features</a>
</li>
	<li class="cat-item cat-item-355"><a href="https://www.usracing.com/news/analysis" title="Posts related to analysis">Racing Analysis</a>
</li>
	<li class="cat-item cat-item-807"><a href="https://www.usracing.com/news/handicapping-reports">Handicapping & Picks</a>
</li>
	
	<li class="cat-item cat-item-806"><a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses">Kentucky Derby</a>
</li>
<li class="cat-item cat-item-805"><a href="https://www.usracing.com/news/breeders-cup">Breeders' Cup News</a>
</li>
<li class="cat-item"><a href="https://www.usracing.com/news/harness-racing">Harness Racing</a>
</li>
<li class="cat-item cat-item-357"><a href="https://www.usracing.com/news/recap" title="News recap">Race Recaps</a>
</li>
<li class="cat-item cat-item-949"><a href="https://www.usracing.com/news/nfl">Sports Betting</a>
</li>
</ul></div>
		</aside><aside id="text-5" class="widget panel panel-white widget_text"><div class="panel-heading"><h3 class="widget-title">FOLLOW US</h3></div>			<div class="textwidget"><div class="social facebook">
<i class="fa fa-facebook"></i>
<div class="fb-like" data-href="https://www.facebook.com/betusracing/" data-layout="button_count" data-action="like" data-size="small"  data-show-faces="false" data-share="false"  ></div>
</div>



<div class="social twitter">
<i class="fa fa-twitter"></i>
<a href="https://twitter.com/betusracing" class="twitter-follow-button" data-show-count="false">Follow</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>


<!-- script -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.7&appId=758493064170165";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script></div>
		</aside><style>.rpwe-block ul{
list-style: none !important;
margin-left: 0 !important;
padding-left: 0 !important;
}

.rpwe-block li{
border-bottom: 1px solid #eee;
margin-bottom: 10px;
padding-bottom: 10px;
list-style-type: none;
}

.rpwe-block a{
display: inline !important;
text-decoration: none;
}

.rpwe-block h3{
background: none !important;
clear: none;
margin-bottom: 0 !important;
margin-top: 0 !important;
font-weight: 400;
font-size: 12px !important;
line-height: 1.5em;
}

.rpwe-thumb{

box-shadow: none !important;
margin: 2px 10px 2px 0;
padding: 5px !important;
}

.rpwe-summary{
font-size: 14px;
}

.rpwe-time{
color: #bbb;
font-size: 11px;
}

.rpwe-comment{
color: #bbb;
font-size: 11px;
padding-left: 5px;
}

.rpwe-alignleft{
display: inline;
float: left;
}

.rpwe-alignright{
display: inline;
float: right;
}

.rpwe-aligncenter{
display: block;
margin-left: auto;
margin-right: auto;
}

.rpwe-clearfix:before,
.rpwe-clearfix:after{
content: "";
display: table !important;
}

.rpwe-clearfix:after{
clear: both;
}

.rpwe-clearfix{
zoom: 1;
}
</style><aside id="rpwe_widget-2" class="widget panel panel-white rpwe_widget recent-posts-extended"><div class="panel-heading"><h3 class="widget-title">Last Racing News</h3></div><div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="https://www.usracing.com/news/handicapping-reports/weekend-race-report-cards-2"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://www.usracing.com/news/wp-content/uploads/2017/01/reportcard2-45x45.jpg" alt="Weekend Race Report Cards"></a><h3 class="rpwe-title"><a href="https://www.usracing.com/news/handicapping-reports/weekend-race-report-cards-2" title="Permalink to Weekend Race Report Cards" rel="bookmark">Weekend Race Report Cards</a></h3><time class="rpwe-time published" datetime="2017-02-10T08:27:39+00:00">February 10, 2017</time><div class="rpwe-summary">US Racing members can get free Race Report Cards for this weekend&#8217;s action at Aqueduct, Fair Grounds, Gulfstream Park, Oaklawn Park, Santa Anita Park, Sunland &hellip;<a href="https://www.usracing.com/news/handicapping-reports/weekend-race-report-cards-2" class="more-link">Read More »</a></div></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/2017-kentucky-derby-contender-profile-uncontested"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://www.usracing.com/news/wp-content/uploads/2017/02/Oaklawn-Park-Gate-45x45.png" alt="2017 Kentucky Derby Contender Profile: Uncontested"></a><h3 class="rpwe-title"><a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/2017-kentucky-derby-contender-profile-uncontested" title="Permalink to 2017 Kentucky Derby Contender Profile: Uncontested" rel="bookmark">2017 Kentucky Derby Contender Profile: Uncontested</a></h3><time class="rpwe-time published" datetime="2017-02-10T00:20:33+00:00">February 10, 2017</time><div class="rpwe-summary">Purchased for a modest sum of $20,000 in Keeneland’s September 2015 Yearling Sale, Uncontested is an early favorite for all the Derby prep races at &hellip;<a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/2017-kentucky-derby-contender-profile-uncontested" class="more-link">Read More »</a></div></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="https://www.usracing.com/news/analysis/rejuvenated-and-raring-to-go-winning-harness-racing-returns"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://www.usracing.com/news/wp-content/uploads/2017/02/harnessracers-45x45.jpg" alt="Rejuvenated And Raring To Go, Harness Racing Returns"></a><h3 class="rpwe-title"><a href="https://www.usracing.com/news/analysis/rejuvenated-and-raring-to-go-winning-harness-racing-returns" title="Permalink to Rejuvenated And Raring To Go, Harness Racing Returns" rel="bookmark">Rejuvenated And Raring To Go, Harness Racing Returns</a></h3><time class="rpwe-time published" datetime="2017-02-09T22:33:33+00:00">February 9, 2017</time><div class="rpwe-summary">Enduring the aches and pain, sneezes and sniffles, I return after battling a cold last week to get all of you up to speed on &hellip;<a href="https://www.usracing.com/news/analysis/rejuvenated-and-raring-to-go-winning-harness-racing-returns" class="more-link">Read More »</a></div></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/dereks-derby-dozen-ascension-el-areeb"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://www.usracing.com/news/wp-content/uploads/2016/11/BigBrown5-45x45.jpg" alt="Derek’s Derby Dozen: The Ascension of El Areeb"></a><h3 class="rpwe-title"><a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/dereks-derby-dozen-ascension-el-areeb" title="Permalink to Derek’s Derby Dozen: The Ascension of El Areeb" rel="bookmark">Derek’s Derby Dozen: The Ascension of El Areeb</a></h3><time class="rpwe-time published" datetime="2017-02-09T11:25:44+00:00">February 9, 2017</time><div class="rpwe-summary">Despite his impressive win in the Holy Bull, Irish War Cry was unable to crack my “Derby Dozen” list, which rates the top contenders for &hellip;<a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/dereks-derby-dozen-ascension-el-areeb" class="more-link">Read More »</a></div></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/closer-look-irish-war-cry"  rel="bookmark"><img class="rpwe-alignleft rpwe-thumb" src="https://www.usracing.com/news/wp-content/uploads/2017/02/HolyBull2017c-45x45.jpg" alt="A Closer Look at Irish War Cry (video)"></a><h3 class="rpwe-title"><a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/closer-look-irish-war-cry" title="Permalink to A Closer Look at Irish War Cry (video)" rel="bookmark">A Closer Look at Irish War Cry (video)</a></h3><time class="rpwe-time published" datetime="2017-02-09T05:28:35+00:00">February 9, 2017</time><div class="rpwe-summary">Jay Nehf Jay C. Nehf is a Thoroughbred owner and trainer who was born in Chicago, Illinois. His first racetrack job was at Washington Park &hellip;<a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/closer-look-irish-war-cry" class="more-link">Read More »</a></div></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></aside><aside id="text-6" class="widget panel panel-white widget_text"><div class="panel-heading"><h3 class="widget-title">News and Handicapping Reports</h3></div>			<div class="textwidget"><p>
Breaking news, expert analysis and handicapping reports to help you win at the track. Sign up to get the latest insights delivered straight to your inbox.</p>
<div id="email-subscribers-custom">
    <div>
        <div class="showafterform">
            <h3>Thank you for subscribing to US Racing News and Reports.</h3>
        </div>
        <div class="form-subscribe">
                <form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
                        <!-- The following code must be included to ensure your sign-up form works properly. -->
                        <input data-id="ca:input" type="hidden" name="ca" value="6a796fab-de62-40d3-874e-d0ddf9ba3426">
                        <input data-id="list:input" type="hidden" name="list" value="1770667721">
                        <input data-id="source:input" type="hidden" name="source" value="EFD">
                        <input data-id="required:input" type="hidden" name="required" value="list,email,first_name">
                        <input data-id="url:input" type="hidden" name="url" value="">
                        
                        <div class="es_lablebox">Your First Name</div>

                        <div data-id="First Name:p" class="es_textbox">
              
                                <input type="text" name="first_name"  data-id="First Name:input" class="es_textbox_class"/>
                  <label data-id="First Name:label" data-name="first_name" class="ctct-form-required"></label>
                        </div>
                        
                        <div class="es_lablebox">Your Best Email Address</div>
                        
                        <div data-id="Email Address:p"  class="es_textbox">
                                <input type="email" name="email" data-id="Email Address:input" class="es_textbox_class" />
                                <label data-id="Email Address:label" data-name="email" class="ctct-form-required"></label>

                        </div>
                        <div class="es_button">
                                <label></label>
                                <input type="submit" value="Subscribe" class="es_textbox_button"/>
                        </div>
                </form>
        </div>
    </div>
</div></div>
		</aside>			
		</div><!-- end: #right-col --> 


</div><!-- end/row -->
</div><!-- end/container -->

<script>
var localizedErrMap = {};
localizedErrMap['required'] = 		'This field is required.';
localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
localizedErrMap['list'] = 			'Please select at least one email list.';
localizedErrMap['generic'] = 		'This field is invalid.';
localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
    localizedErrMap['state_province'] = 'Select a state/province';
localizedErrMap['selectcountry'] = 	'Select a country';
var postURL = 'https://visitor2.constantcontact.com/api/signup';

jQuery(function(){
       jQuery('#cat option[value=-1]').remove();
       jQuery('#cat option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('#categories-dropdown-3 option[value=-1]').remove();
       jQuery('#categories-dropdown-3 option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
   });
   </script>
   <script type='text/javascript' src='https://www.usracing.com/news/wp-content/themes/usracing/js/signup-form.js'></script>


<!-- === Footer ================================================== -->
<div class="footer">
<div class="container">

    <div class="row">

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/off-track-betting">Off Track Betting</a></li>
          <li><a href="/online-horse-wagering" >Online Horse Wagering</a></li>
          <li><a href="/odds">Horse Racing Odds</a></li>
          <li><a href="/bet-on-horses">Bet on Horses</a></li>
          <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li>
          <li><a href="/harness-racing">Harness Racing</a></li>
          <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
          <li><a href="/texas">Texas Horse Betting</a></li>
        <li><a href="/dubai-world-cup">Dubai World Cup Betting</a></li> 


<!--<li><a href="/santa-anita" >Santa Anita Horse Racing</a></li>
<li><a href="/keeneland" >Keeneland Horse Racing</a>  </li>
<li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
<li><a href="/delta-downs" >Delta Downs Horse Racing</a></li>
<li><a href="http://www.pickthewinner.com" >Pick the Winner</a>  </li>
<li><a href="http://www.breederscupbetting.com" >Breeders Cup Betting</a>  </li>
    <li><a href="/breeders-cup/betting" >Breeders' Cup Betting</a>
     <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a>-->
    </li>
                 </ul>
      </div> <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
          <li><a href="/kentucky-derby/future-wager">Kentucky Derby Future Wager</a></li>
          <li><a href="/bet-on/kentucky-derby">Bet on Kentucky Derby</a></li>
          <li><a href="/road-to-the-roses">Road to the Roses</a></li>
          <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
          <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
          <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
          <li><a href="/twin-spires">Twin Spires Betting</a></li>



        </ul>
      </div>
        <!--/col-md-3-->
	        <div class="col-md-3 margin-bottom-30">
	        <div class="headline">
	          	<h3>Triple Crown</h3>
	        </div>
		        <ul class="list-unstyled margin-bottom-20">
		        	<li><a href="/kentucky-derby" >Kentucky Derby</a></li>
		        	<li><a href="/preakness-stakes" >Preakness Stakes</a></li>
		        	<li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>
		        	<li><a href="/preakness-stakes/betting" >Preakness Stakes Betting</a></li>
					<!--<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>
		          	<li><a href="/preakness-stakes/winners" >Preakness Stakes Winners</a></li> -->
		           <li><a href="/belmont-stakes" >Belmont Stakes</a></li>
		           <li><a href="/bet-on/belmont-stakes" >Bet on Belmont Stakes </a></li>
		        	<li><a href="/belmont-stakes/betting" >Belmont Stakes Betting</a></li>
					<!--<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
		          	<li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> -->
		          	<li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
		        </ul>
			</div>
      <!--/col-md-3-->



      <!--/col-md-3-->
				   <!--<div class="col-md-3 margin-bottom-30">
				        <div class="headline">
				          <h3>US Horse Racing</h3>
				        </div>
				        <ul class="list-unstyled margin-bottom-20">
				        	<li><a href="/about" >About Us</a></li>-->

				          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
				       <li><a href="#" >Our Guarantee</a></li>
				          	<li><a href="/faqs" >FAQs</a></li>
				            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

				<!--<li><a href="/signup/" >Sign Up Today</a></li>
				        </ul>
						</div> -->
      <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>

        <ul class="social-icons">
          <li><a href="https://www.facebook.com/betusracing" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>
          <li><a href="https://twitter.com/betusracing" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
          <li><a href="https://plus.google.com/+Usracingcom" data-original-title="Google+"  title="Google+" class="social_google" rel="publisher"></a></li>
          <!--
          <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
          -->
        </ul>
        <br>   <br>
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
        	<li><a href="/about" >About Us</a></li>
          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
          	<li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
<li><a href="/signup/?=footer" rel="nofollow" >Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>

        </ul>



      </div> <!--/col-md-3-->

</div><!--/row-->



<div class="row friends">
	<center>Proudly featured on:
<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center>


     <!--
   <div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>

        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
-->
        </div>
</div>
<!-- /row/friends -->


</div><!--/container-->
</div><!--/footer-->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand">Copyright 2016 <a href="/">US Racing</a>, All Rights Reserved</span>  <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/responsible-gaming" rel="nofollow" >Responsible Gambling</a> | <a href="/preakness-stakes/betting" >Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting" >Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting" >Kentucky Derby Betting</a>    </div>
    </div><!--/row-->

  </div><!--/container-->
</div><!--/copyright-->



<div class="wp-hide">{include file='scripts-footer.tpl'}</div>

            <!--
				<div class="site-info">
					
					<span class="sep"> | </span>
									</div>--><!-- close .site-info -->

<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<!--<script type="text/javascript" src="/assets/js/app.js"></script> -->
<script type="text/javascript">
jQuery(document).ready(function() {
	/*App.init();
	App.initFancybox();
	App.initRoyalSlider();
    Index.initIndex();    */
    jQuery('#ol').sidr({
        name: 'left-menu',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
		  jQuery('#left-menu').perfectScrollbar();
          jQuery("#ol i.fa").hide();
          jQuery("#ol i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu').perfectScrollbar('update');
          jQuery("#ol i.glyphicon").hide();
          jQuery("#ol i.fa").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#ol-left').sidr({
        name: 'left-menu-main',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
		  jQuery('#left-menu-main').perfectScrollbar();
          jQuery("#ol-left i.fa, #ol-left span.more").hide();
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").show().css('display', 'block');
          jQuery("#ol-left").addClass("active");
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu-main');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu-main').perfectScrollbar('update');
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").hide();
          jQuery("#ol-left i.fa, #ol-left span.more").show().css('display', 'block');
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery("#ol-left").removeClass("active");
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#or').sidr({
        name: 'nav-side',
        side: 'left',
        speed: 300,
        source: '.navbar-collapse',
        renaming: false,
        onOpen: function () {
		  jQuery('#nav-side').perfectScrollbar();
          jQuery("#or span").hide();
          jQuery("#or i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'nav-side');
            }
          });
        },
        onClose: function () {
		  jQuery('#nav-side').perfectScrollbar('update');
          jQuery("#or i.glyphicon").hide();
          jQuery("#or span").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      // Sidr Accordian
      jQuery(".sidr a").click(function () {
        var link = jQuery(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;
        closest_ul.find("ul").slideUp(function () {
          if (++count == closest_ul.find("ul").length)
            parallel_active_links.removeClass("active");
        });
        if (!link_status) {
          closest_li.children("ul").slideDown();
          closest_li.addClass("active");
      	  jQuery('.sidr').perfectScrollbar('update');
        }
    });
});
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest -->
<script type="text/javascript" async src="https://www.usracing.com/news/wp-content/themes/usracing/js/pinit.js"></script>
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'usracing.com');
  ga('send', 'pageview');
</script>
-->

<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
  	App.init();
  	App.initFancybox();
  	App.initRoyalSlider();
    try{
      Index.initIndex();
    }catch(e){ console.log( "assets/js/index.js -> does not imported" ); }
  });
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
ga('require', 'displayfeatures');
ga('send', 'pageview');
ga('require', 'linker');
ga('linker:autoLink', ['betusracing.ag', 'bet.usracing.com'] );
</script>
<script async src='//www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->





<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '906957672685525');
fbq('track', "PageView");


</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=906957672685525&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


<!-- Bing Ads RP  -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4047476"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


<!-- OptinMonster --><script>var om571fe1cc5ae65,om571fe1cc5ae65_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){om571fe1cc5ae65_poll(function(){if(window['om_loaded']){if(!om571fe1cc5ae65){om571fe1cc5ae65=new OptinMonsterApp();return om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;om571fe1cc5ae65=new OptinMonsterApp();om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster --> 




        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=825577710919578&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>	
    <link rel='stylesheet' id='dashicons-css'  href='https://www.usracing.com/news/wp-includes/css/dashicons.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='thickbox-css'  href='https://www.usracing.com/news/wp-includes/js/thickbox/thickbox.css' type='text/css' media='all' />
<script type='text/javascript'>
/* <![CDATA[ */
var frontend_ajax_object = {"ajax_url":"https:\/\/www.usracing.com\/news\/wp-admin\/admin-ajax.php","ajax_nonce":"adfdc4b4dd"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.usracing.com/news/wp-content/plugins/accesspress-social-share/js/frontend.js'></script>
<script type='text/javascript' src='https://www.usracing.com/news/wp-includes/js/wp-embed.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"https:\/\/www.usracing.com\/news\/wp-includes\/js\/thickbox\/loadingAnimation.gif"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.usracing.com/news/wp-includes/js/thickbox/thickbox.js'></script>

</body>
</html>
