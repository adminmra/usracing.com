<html>
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
      <link rel="stylesheet" href="css/normalize.css">
      <link rel="stylesheet" href="css/last-update-style.css">

      <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

      <title>US Racing & Spooky Express</title>

   </head>
   <body>

      <div class="container">

        <!-- HEADER -->
         <div class="row header">

            <div class="col-md-8 col-sm-8 col-xs-12">
               <a href="https://record.webpartners.co/_qgl5wb_A_JxWje1LaFUUKWNd7ZgqdRLk/1/"><img src="img/usracing.png" alt="US Racing" class="img-responsive logo" /></a>
            </div>
            <div class="col-md-4 col-sm-4  col-xs-12">
               <img src="img/logospooky.png" alt="Sponsor" class="img-responsive pull-right brand" />
            </div>

         </div>

         <div class="row marko">
            <div class="pro">
             <img class="hidden-xs img-responsive center-block shield" src="img/Generic-Sports-Landing-Pagewill.png" alt="Start Winning Today">
               <img class="col-xs-6 hidden-sm hidden-md hidden-lg img-responsive block shield " src="img/Generic-Sports-Landing-Pagewillpeq.png" alt="Start Winning Today">

               <div class="row cta">
                 <div class="col-md-12 col-sm-12"><a href="https://record.webpartners.co/_qgl5wb_A_JxWje1LaFUUKWNd7ZgqdRLk/1/" class="fun grow"></a></div>
               </div>

               <div class="row steps">
                  <div class="col-md-3 col-sm-3 step-1">
                     Create your<br>
                     account
                  </div>
                  <div class="col-md-3 col-sm-3 step-2">
                      Enter promo<br>
                      Code: <span>PONIES</span>
                  </div>
                  <div class="col-md-3 col-sm-3 step-3">
                      Go to<br>
                      cashier
                  </div>
                  <div class="col-md-3 col-sm-3 step-4">
                      Fund your<br>
                      account
                  </div>
               </div>
             <!--  <div class="footer text-center">
                  <ul>
                     <!--li><small>as seen on:</small></li-->
                   <!--  <li><img src="img/landings_footer_icon_01.png" alt="Yahoo"></li>
                     <li><img src="img/landings_footer_icon_02.png" alt="daily-news"></li>
                     <li><img src="img/landings_footer_icon_03.png" alt="Bleacher Report"></li>
                     <li><img src="img/landings_footer_icon_04.png" alt="SB Nation"></li>
                     <li><img src="img/landings_footer_icon_05.png" alt="The Denver Post"></li>
                     <li><img src="img/landings_footer_icon_06.png" alt="NBC SPorts"></li>
                     <li><img src="img/landings_footer_icon_07.png" alt="Fox Sports"></li>
                  </ul>
               </div>-->

            </div>

            <div class="col-md-6 hidden-sm hidden-xs"><img src="img/left.png" alt="Football Player" class="img-responsive" />
            </div>
            <div class="col-md-6 hidden-sm hidden-xs"><img src="img/right.png" alt="Football Player" class="img-responsive pull-right" />
            </div>

         </div>

       </div> <!-- container -->

      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
   </body>
</html>
