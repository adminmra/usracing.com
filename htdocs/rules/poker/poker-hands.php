<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
                              <p><b class=bodytext>Poker Hands or Poker Power 
                                Rankings:<br>
                                <br>
                                </b>Below you will find the Poker Hands hierarchy 
                                or Poker Hand Power Rankings and a brief description 
                                of each of the hands. For an online tutorial on 
                                how to play poker, please use the menu board on 
                                the left in the 'Poker School' section. Also you 
                                will find other links to various poker related 
                                information.</p>
                              <p><b class=bodytext>Royal Straight Flush</b><br>
                                Ten, Jack, Queen, King, Ace of the same suit.<br>
                                <img 
            src="images/royal-flush.jpg" 
            border=0 alt="Royal Straight Flush"></p>
                              <p><b class=red>Straight Flush</b><br>
                                Straight with all five cards in the same suit.<br>
                                <img 
            src="images/straight-flush.jpg" 
            border=0 alt="Straight Flush"><br>
                                <br>
                                <b class=red>Four of a kind</b> is next highest. 
                                It consists of the four cards of any one rank 
                                together with any fifth card; for example <br>
                                <img 
            src="images/4-of-a-kind.jpg" border="0" alt="Four of a Kind"><br>
                                constitute four Queens.<br>
                                <br>
                                A <b class=red>full house</b> ranks next; it consists 
                                of any three of one kind and any pair of another 
                                kind, such as <br>
                                <img 
            src="images/full-house.jpg" border="0" alt="Full House"><br>
                                It is referred to by the three-of-a-kind it contains; 
                                the example shown would be "threes full."<br>
                                <br>
                                A <b class=red>flush</b>, ranking next; it consists 
                                of any five cards of the same suit, but not in 
                                sequence, as<br>
                                <img 
            src="images/flush.jpg" border="0" alt="Flush"><br>
                                referred to as a "Ace high flush."<br>
                                <br>
                                A <b class=red>straight</b> consists of any five 
                                cards of two or more suits in sequence of rank, 
                                with the ace ranking either high or low in the 
                                sequence.<br>
                                <img 
            src="images/straight.jpg" border="0" alt="Straight"><br>
                                One does not need to have an ace to have a straight, 
                                of course: any five cards in a row will do. A 
                                straight ranks next under a flush.<br>
                                <br>
                                <b class=red>Three of a kind</b> are any three 
                                cards of the same rank plus two other cards which 
                                do not constitute a pair and do not include the 
                                fourth card of the same rank;<br>
                                <img 
            src="images/3-of-a-kind.jpg" border="0" alt="Three of a Kind"> 
                                <br>
                                would be referred to as "three eights." Or �trip 
                                eights.�<br>
                                <br>
                                <b 
            class=red>Two pair</b>, which ranks next under three of a kind, consists 
                                of two cards of one rank, two cards of another 
                                rank, and any fifth card which is neither of those 
                                ranks; it is referred to by the higher of the 
                                two pairs. Thus,<br>
                                <img 
            src="images/two-pair.jpg" border="0" alt="Two Pair"> <br>
                                would be "Queens up."<br>
                                <br>
                                <b class=red>One pair</b> - any two cards of the 
                                same rank, together with three other cards which 
                                do not combine with the other two to form any 
                                of the higher-ranking hands above:<br>
                                <img src="images/1-pair.jpg" border="0" alt="One Pair"><br>
                                is a pair of kings.<br>
                                <br>
                                <b class=red>High card</b> - when the cards contain 
                                no cards of the same rank or the same suit or 
                                combine to make a sequential run to form any of 
                                the higher-ranking hands above:<br>
                                <img src="images/high-card.jpg" border="0" alt="High Card"><br>
                                the highest card value is used, in this case the 
                                highest card would be the 7. When two or more 
                                players are relying on a high card to win a hand, 
                                the Ace of any suit is in variably considered 
                                to be the highest card. <br>
                              </p>
                              <p>For more information on poker hand rankings we 
                                recommend <a 
            href="http://www.casino.com/poker/article.asp?id=2145">Ranking the 
                                Hands</a></p>
                              <p><a href="poker-download.php">
                              <img src="images/online-poker-download.gif" 
            border=0 alt="Download Poker Now"></a></p>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>