<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
                              <p align="center"><font color="#0958A6" size="3"><b>Seven Card Stud</b></font></p>
                              <p align="center">
                              <a href="javascript:document.downloadForm.submit();">
                              <img border="0" src="images/index.1.jpg" alt="Seven Card Stud"></a></p>
                              <p>This section will inform you on gaming procedures, 
                                rules, policies and limits of All Horse Racing's 
                                game of Seven Card Stud.</p>
                              <p>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                is an abbreviation for Rules.</p>
                              <ul>
                                <li> 
                                  <p><a 
              href="#dealing">Dealing the Game of Seven Card Stud</a></p>
                                <li> 
                                  <p><a href="#1_2">$ 1-2 through $20-40 Seven 
                                    Card Stud Notable Rules �Fixed Limit Games�</a></p>
                                <li> 
                                  <p><a href="#1_3">$ 1 to 3 and $1 to 5 Seven 
                                    Card Stud Notable Rules �Spread Limit Games�</a></p>
                                <li> 
                                  <p><a 
              href="#games">All Horse Racing Seven Card Stud Games</a></p>
                                </li>
                              </ul>
                              <p><b><a name=dealing></a>Dealing the Game of Seven 
                                Card Stud</b></p>
                              <p>
                              <b>Before the Deal:</b> </p>
                              <ul>
                                <li> 
                                  <p>Players will buy-in for the posted amount. 
                                    No short buys are permitted.</p>
                                <li> 
                                  <p>Players will place an ante into the pot by 
                                    selecting �I�m in.� Your ante will be placed 
                                    into the pot by default. You may turn off 
                                    this feature under the settings menu in the 
                                    game.</p>
                                </li>
                              </ul>
                              <p>
                              <b>Opening Deal:</b> <br>
                              The dealer <font color=red><b>always</b></font> 
                              deals first to the player closest to the dealer�s 
                              left and, moving clockwise around the table, will 
                              �deal-in� each player. The players will be dealt 
                              one card face down, then a second card face down, 
                              AND then a third card known as the �door card� will 
                              be dealt face up. A round of betting will occur 
                              starting with the player with the lowest card by 
                              value and suit. This is known as the �force� or 
                              �bring-in� bet. The player who has the bring-in 
                              bet may choose between betting a minimum amount 
                              or a larger amount. </p>
                              <p>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                The player with the lowest card value (ace being 
                                high) will be the force. In the event that two 
                                or more players hold door cards of the same rank, 
                                then the lowest suit will make the determination. 
                                Suits are ranked, from highest to lowest alphabetically, 
                                spades, hearts, diamonds, clubs (i.e., the two 
                                of clubs is the lowest possible card). This is 
                                the only time suits will play a role in the game.<br>
                                <b><br>
                                Fourth Card (4th Street):</b> <br>
                                The next card to be dealt into the game will be 
                                the fourth card the players will receive in their 
                                hand. This is called �4th street.� The dealer 
                                will burn one card off the top of the deck and 
                                then <font color=red><b>always</b></font> deals 
                                to the player closest to the dealer�s left and, 
                                moving clockwise around the table, will deal each 
                                one card face up. A round of betting will occur 
                                starting with the player with the highest two-card 
                                value. This is known as �having the action.� </p>
                              <p>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                The player having the best hand on the board �has 
                                the action� and may make the initial bet of the 
                                round or check to the next player. If there is 
                                a tie between players as to the best hand, then 
                                the player closest to the dealer�s left will have 
                                the action.</p>
                              <p>In fixed limit stud (i.e., a 4-8 game, as opposed 
                                to a 1 to 5 game), if any player displays an open 
                                pair on fourth street, that player may bet the 
                                higher amount of the game limit normally not available 
                                until fifth street.</p>
                              <p>If the player with the action chooses to make 
                                the lower limit bet, any other player still in 
                                the hand may choose to make either the lower limit 
                                or higher limit raise. Once any player has made 
                                the higher limit bet, all further bets and raises 
                                must be made at the that higher level.</p>
                              <p><b>Betting in Seven Card Stud</b></p>
                              <blockquote> 
                                <p class="bodytext"><font color="#FF0000"><b>a)</b></font> 
                                  If you choose not to bet, then you are said 
                                  to �check,� or pass on the option to bet to 
                                  the next player.<br>
                                  <br>
                                  <font color=red><b>b)</b></font> If another 
                                  bets, you may �call� the bet, �raise� or �fold.�<br>
                                  <br>
                                  <font color=red><b>c)</b></font> If a player 
                                  checks and another player makes a bet, the player 
                                  who checked may in turn raise the bet made by 
                                  the other player. This is called a �check &amp; 
                                  raise.�</p>
                              </blockquote>
                              <p class="bodytext">
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                Players have 10 seconds to bet, check, raise or 
                                fold. If the player does not make a choice within 
                                the allotted time, the game will place the player 
                                all-in.</p>
                              <p class="bodytext"><b>Fifth Card (5th Street):</b> 
                                <br>
                                The next card to be dealt into the game will be 
                                the fifth card the players will receive in their 
                                hand. This is called �5th street.� The dealer 
                                will burn one card off the top of the deck and 
                                then <font color=red><b>always</b></font> deals 
                                to the player closest to the dealer�s left and, 
                                moving clockwise around the table will deal each 
                                one card face up. A round of betting will occur 
                                starting with the player with the highest three-card 
                                value. Starting with fifth street, and continuing 
                                through all further streets, all bets and raises 
                                in fixed limit games are made at the higher limit. 
                                <b><br>
                                <br>
                                Sixth Card (6th Street):</b> <br>
                                The next card to be dealt into the game will be 
                                the sixth card the players will receive in their 
                                hand. This is called �6th street.� The dealer 
                                will burn one card off the top of the deck and 
                                then <font color=red><b>always</b></font> deals 
                                to the player closest to the dealer�s left and, 
                                moving clockwise around the table will deal each 
                                one card face up. A round of betting will occur 
                                starting with the player with the highest four-card 
                                value. <b><br>
                                <br>
                                Seventh Card (the River):</b> <br>
                                Almost every poker player uses the term �the river� 
                                or �river� to describe the last card players will 
                                receive in the game. As always, we start with 
                                a burn card. The dealer will deal a card face 
                                down clockwise around the table. </p>
                              <p class="bodytext"><font color=red><b>Exception!</b></font><br>
                                If there are 8 players who played all the way 
                                through the sixth card and are now about to be 
                                dealt their final card of the game, our dealer 
                                will only have one (1) card remaining in the deck. 
                                That�s because 8 X 6 = 48 cards + the 3 burn cards 
                                that we burned on 4th, 5th and 6th streets will 
                                be a sum of 51 cards. Our rule in the case is 
                                as follows:</p>
                              <p class="bodytext">
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                In the event there may not be enough cards remaining 
                                in the deck to deal each player a final card, 
                                the dealer will calculate if the burn cards plus 
                                any cards in the deck will allow each player to 
                                receive a final down card. If the dealer can deal 
                                each player a last card, then the dealer will 
                                do so by taking all the burn cards plus any remaining 
                                cards in the deck, shuffle and then deal the final 
                                card to each player.</p>
                              <p class="bodytext"><font color=red><b>However, 
                                if the dealer calculates that it�s not possible 
                                to give each player his or her own card, then 
                                the dealer will take all the burn cards, shuffle 
                                and turn over a "community card." The folded (muck) 
                                cards may never be used in the game once they 
                                have been folded.</b></font><br>
                              </p>
                              <p class="bodytext"><b>Showdown:</b></p>
                              <p>
                              <b>Who shows first?</b> <br>
                              In a live game, generally players who feel that 
                              they have a good opportunity to win a pot will show 
                              their hand voluntarily. However, in many instances, 
                              players all stare at each other wondering who will 
                              show their cards first. To avoid this situation, 
                              our game will employ the following standard rule:
        </p>
                              <p>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                The determination of which players� cards will 
                                and must be shown first will lie with the player 
                                who had initiated the action or with the person, 
                                who had initiated the last bet, raise or re-raise.</p>
                              <p>This simply means that whoever created the last 
                                action on the river must show his/her cards first. 
                                If the player who had the action checked and all 
                                players checked, then the player who had the initial 
                                option to bet would show first. If a player checks 
                                and another player bets, then the player who bet 
                                will have his/her cards shown first. If a player 
                                checked, another bets and then another player 
                                raises, the raiser�s cards will be shown first.</p>
                              <p>
                              <b>Suppose a Player wins by default. Is the Hand 
                              required to be shown?</b><br>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                              Players that have won a pot do <font color=red><b>not</b></font> 
                              have to show their cards if they were not called.
        </p>
                              <p>This means that if two or more players were playing 
                                and one player bets and the other(s) fold, then, 
                                the player who has won the pot by default will 
                                not under any circumstances have their cards shown 
                                automatically. The winning players may choose 
                                to �<font color=red><b>show</b></font>� or �<span class=red><b><font color="#FF0000">don�t 
                                show</font></b></span>.�</p>
                              <p>Do Players have to show their Cards if they call 
                                a bet or a check on the River?</p>
                              <p>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                Players are not required to show their cards if 
                                and only if they are not the player who created 
                                the last action by betting or raising. If a player 
                                calls a bet and sees that they cannot win, they 
                                may �fold� or �show� their cards. However, if 
                                a player who has stayed in until the end mucks 
                                (fails to show) his hand, other players in the 
                                game may find out the hand by requesting that 
                                a hand history be emailed to them.</p>
                              <p>
                              <b>Who wins?<br>
                              </b> 
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                              In our poker room, as with all, "Cards Speak." That 
                              means our dealer will find the best 5-card hand 
                              on the table and declare it the winner based on 
                              the universal poker hand rankings. </p>
                              <p>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                It is theoretically possible that two or more 
                                players can hold the best five-card hand. If there 
                                is a tie between two or more players, then the 
                                dealer will "split" the pot equally between all 
                                the winning players.</p>
                              <p>
                              <b class=bodytext><a name=1_2></a>$ 1-2 Through 
                              $20-40 Seven Card Stud Notable Rules<br>
                              <br>
                              �Fixed Limit Games� </b> <br>
                              Most commonly called �<font color=red><b>fixed</b></font>� 
                              limit games, this type of game limit structure is 
                              by far the most common betting structure you will 
                              find in the casinos. A fixed limit game is one in 
                              which each bet will remain consistent through several 
                              betting rounds and then change at some predetermined 
                              point in later rounds. All bets will remain equal 
                              to the posted table limit. For example, in a 5-10 
                              Seven-Card Stud game, the bets will be as follows.
        </p>
                              <p>In the early betting rounds, a bet and any raises 
                                must be exactly $5. In later betting rounds, such 
                                as the last round, a bet and any raises must be 
                                exactly $10.</p>
                              <p>
                              <b>How do you bet &amp; raise in a fixed limit Game?</b>
        </p>
                              <blockquote> 
                                <p> <font color=red><b>a)</b></font> Each bet in every betting round 
                                will be in strict compliance with a structured 
                                game. </p>
<p><font color=red><b>b)</b></font> If you choose 
                                  not to bet, then you are said to �check,� or 
                                  pass on the option to bet to the next player.</p>
                                <p><font color=red><b>c)</b></font> If a player 
                                  bets, you may �call� the bet or �fold.�</p>
                                <p><font color=red><b>d)</b></font> If a player 
                                  checks and another player makes a bet, the player 
                                  who checked may then in turn raise the bet made 
                                  by the other player. This is called a �check 
                                  &amp; raise.�</p>
                              </blockquote>
                              <p>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                <font color=red><b>Players have 10 seconds to 
                                bet, check, raise or fold. If the player does 
                                not make a choice, the game will automatically 
                                place the player all-in if such player has an 
                                all-in remaining.</b> </font></p>
                              <p>
                              <b>How many raises will be allowed?</b><br>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                              The general poker rule is a �cap� of three raises 
                              allowed per betting round with three or more players. 
                              However, if there is a �heads up� situation whereby 
                              only two players remain in the game, then raises 
                              are unlimited. </p>
                              <ul>
                                <li> 
                                  <p>The opening round will be a force wager determined 
                                    by the game limit. The player with the lowest 
                                    �door card� is forced to wager the minimum 
                                    or a player may open for the lower limit of 
                                    the game. If another player wishes, they may 
                                    �raise� the bet and �complete� it to the lower 
                                    limit of the game in the event the player 
                                    with the force wagered just the force amount. 
                                    This is not a raise in the traditional sense. 
                                    The player is only �bringing up� the bet to 
                                    its normal minimum. If a player wishes to 
                                    raise, then this will be considered the first 
                                    raise of the game and not the second.</p>
                                <li> 
                                  <p>Fourth Street: The bet shall be the lower 
                                    limit and all raises will be in the same increments 
                                    only.</p>
                                </li>
                              </ul>
                              <p>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                However, an exception occurs on Fourth Street 
                                when there is an open pair on the table. Any player 
                                may bet or raise the lower or higher limit of 
                                the game. This is called the �Fourth Street Rule.�</p>
                              <ul>
                                <li> 
                                  <p>Fifth Street: The bet shall be the higher 
                                    limit and all raises will be the same increments 
                                    only.</p>
                                <li> 
                                  <p>Sixth Street: The bet shall be the higher 
                                    limit and all raises will be the same increments 
                                    only.</p>
                                <li> 
                                  <p>Seventh Street or the river: The bet shall 
                                    be the higher limit and all raises will be 
                                    the same increments only.</p>
                                </li>
                              </ul>
                              <p>
                              <b><a name=1_3></a>$ 1 to 3 and $1 to 5 Seven Card 
                              Stud Notable Rules<br>
                              <br>
                              �Spread Limit Games�</b> <font color=red><br>
                              </font>Spread Limit: The second most commonly found 
                              type of betting structure. This structure means 
                              there is a range of betting amounts permitted. The 
                              easiest example is a $1 to 5 game. Players are allowed 
                              to bet anywhere from $1 through $5 on any bet and 
                              in any round. Another common game is the $1 to 3 
                              game. In casinos, you find the limits written and 
                              used as you see here.<br>
                              <br>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                              Players at these limits may wager any amount on 
                              any round within the limit posted.<br>
                              <br>
                              <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                              Raises in $1 to 3 and $1 to 5 must be equal to or 
                              greater than the previous bet. If in the event there 
                              is a re-raise, then the re-raise amount will be 
                              equal to or greater than the raise amount, but not 
                              to exceed the game limit. </p>
                              <p>For example, if the player in seat #5 bets $2, 
                                the player in seat 6 can only raise $2, $3, $4 
                                or $5. Most commonly you will see a player bet 
                                $2, then another player will say �make it $7� 
                                hence a $5 raise. But, if he wants, he can �make� 
                                or raise the bet to $4 ($2 + $2 raise), $5, $6, 
                                or $7. A player cannot just raise a $1 because 
                                that would violate the rule above.<br>
                                <br>
                                <img 
            src="images/rule.jpg" align=absMiddle border="0" alt="Seven Card Stud Poker Rule"> 
                                While playing fixed limit Seven Card Stud on 4th 
                                street (fourth card dealt) any player may bet 
                                or raise the lower or higher limit of the game 
                                when there is an open pair on the table. This 
                                is called the �Fourth Street Rule.� This does 
                                NOT apply in a spread limit game.</p>
                              <p><b><a name=games></a>All Horse Racing 
                                Seven Card Stud Games</b></p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td width="33%" bgcolor="#FFB500"> 
                                    <p><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif" size="2"><b>$1 
                                      to 5 Stud</b></font></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b><font color="#FFFFFF">$1 
                                      to 3 Stud</font></b></font></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b><font color="#FFFFFF">$1-2 
                                      Stud</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small" bgcolor="#CCCCCC"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Maximum 
                                      number of players:8</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Maximum 
                                      number of players:8</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Maximum 
                                      number of players:8</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Buy-in: 
                                      $30</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Buy-in: 
                                      $30</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Buy-in: 
                                      $20</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Ante: 
                                      $.25</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Ante: 
                                      $.25</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Ante: 
                                      $.25</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Force: 
                                      $1</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Force: 
                                      $1</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Force: 
                                      $.50</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Bet 
                                      amount: $1 to 5 any street.</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Bet 
                                      amount: $1 to 3 any street.</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">4th 
                                      street $1 ($1 or 2 w/ pair)</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Cap 
                                      of 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Cap 
                                      of 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">5th 
                                      street: $2</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Raises 
                                      must be equal to or greater than the previous 
                                      bet amount</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Raises 
                                      must be equal to or greater than the previous 
                                      bet amount</font></b></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">6th 
                                      street: $2</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p>&nbsp;</p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p>&nbsp;</p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">7th 
                                      street: $2</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p>&nbsp;</p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p>&nbsp;</p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>
                              <br>
                              </p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$2-4 
                                      Stud</font></b></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$3-6 
                                      Stud</font></b></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$4-8 
                                      Stud</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr class=tablesBgYellow valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $20</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $30</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $40</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $.25</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $.25</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $.50</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $1</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $1</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $2</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $2 ($2 or 4 w/ pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $3 ($3 or 6 w/ pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street $4 ($4 or 8 w/ pair)</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $4</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $6</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $4</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $6</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $4</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $6</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>
                              <br>
                              </p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p class=red><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$5-10 
                                      Stud</font></b></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p class=red><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$6-12 
                                      Stud</font></b></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p class=red><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$8-16 
                                      Stud</font></b></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $50</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $60</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $80</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $.50</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $.50</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $1</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $2</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $3</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $4</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $5 ($5 or 10 w/ pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $6 ($6 or 12 w/ pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street $8 ($8 or 16 w/ pair)</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $10</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $12</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $16</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $10</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $12</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $16</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $10</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $12</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $16</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>
                              <br>
                              </p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><font color="#FFFFFF"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">$10-20 
                                      Stud</font></b></font></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p class=red><font color="#FFFFFF"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">$15-30 
                                      Stud</font></b></font></p>
                                  </td>
                                  <td class=basic_text_small width="33%" bgcolor="#FFB500"> 
                                    <p><font color="#FFFFFF"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2">$20-40 
                                      Stud</font></b></font></p>
                                  </td>
                                </tr>
                                <tr class=tablesBgYellowLight valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Maximum 
                                      number of players:8</font></b></font></p>
                                  </td>
                                </tr>
                                <tr class=tablesBgYellow valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $100</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $150</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Buy-in: 
                                      $200</font></b></font></p>
                                  </td>
                                </tr>
                                <tr class=tablesBgYellowLight valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $1</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $2</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Ante: 
                                      $2</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $5</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $7</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Force: 
                                      $10</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $10 ($10 or 20 w/pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street: $15 ($15 or 30 w/ pair)</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">4th 
                                      street $20 ($20 or 40 w/ pair)</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $20</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $30</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">5th 
                                      street: $40</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $20</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $30</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">6th 
                                      street: $40</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $20</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $30</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">7th 
                                      street: $40</font></b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font color="#000000"><b><font face="Verdana, Arial, Helvetica, sans-serif" size="1">Cap 
                                      at 3 raises<br>
                                      <br>
                                      Unlimited raises heads up</font></b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>&nbsp;</p>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>