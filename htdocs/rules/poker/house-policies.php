<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
           <p>
            <br>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
                              <p align="center"><font color="#0958A6" size="3"><b class=bodytext>General House Policies</b></font> 
                                <br>
                                <br>
                                &nbsp;<p>This section will inform you on all-in procedures 
                                and general The All Horse Racing Poker 
                                Room house policies. <br>
                                <br>
                                <b class=red>All-In Procedures:</b> <br>
                                <br>
                                <b>What happens if a player runs out of money 
                                during a hand or is disconnected?</b> <br>
                                <br>
                                If a player runs out of money during a hand or 
                                is disconnected by his ISP or system, by no means 
                                does a player have to fold his hand: the player 
                                may go 'all-in.' This means that a player cannot 
                                be forced out of a hand if he/she doesn't have 
                                enough money to complete the hand he/she started. 
                                When a player runs out of money in a hand and 
                                wants to stay in, he/she places what chips he/she 
                                does have into the pot and the game computer automatically 
                                places him/her 'all-in.' <br>
                                <br>
                                As an all-in player, he/she can only contend for 
                                the money in a pot that he/she contributed to. 
                                Using this kind of all-in does not use up one 
                                of your two daily all-ins; those all-ins get used 
                                up only when you fail to act in time, either because 
                                you got distracted from the game, or disconnected 
                                from the game. All other action of remaining 'active' 
                                players goes into a 'side pot' to be fought for 
                                by them. <br>
                                <br>
                                <b>How do you go all-in?</b> <br>
                                <br>
                                Players may go all-in for whole dollar amounts 
                                only. When you make a bet, call a bet or raise 
                                a bet that will reduce your balance to less than 
                                $1, you will be prompted to: 'go all-in?' Yes 
                                or no? <br>
                                <br>
                                <b>Once a player is all-in:</b> 
                              <ol>
                                <li>The player receives all cards as usual. 
                                <li>The all-in player has no option to bet (obviously). 
                                <li>All other money bet after the player is all-in 
                                  cannot be won by the all-in player and will 
                                  be put into a side pot for all active players 
                                  to win. 
                                <li>If the player who is all-in has the action, 
                                  then the dealer will move to the next player 
                                  clockwise. This will continue until an active 
                                  player shows a better top hand. 
                                <li>The player may fold his cards only on the 
                                  river and only after the side pot (if there 
                                  is one) has been awarded to active players. 
                                </li>
                              </ol>
                              <p>
                              <b class=red>Other House Policies</b> </p>
                              <ol>
                                <li> <b>Server Crash or Game freeze:</b> <br>
                                  In the event of a server crash that removes 
                                  all players from the table or prevents the hand 
                                  from continuing, the hand will be considered 
                                  dead (void) and all monies will be returned 
                                  to all players after an investigation is completed 
                                  within 48 hours. 
                                <li> <b>Errors that do not cause a dead hand: 
                                  </b><br>
                                  In the event the game pushes the pot to the 
                                  wrong player or misreads the winning hand, certified 
                                  poker support service members will correct the 
                                  error by adjusting the players' accounts within 
                                  24 hours. 
                                <li> <b>Cheating of any kind:</b> <br>
                                  If management suspects such an activity, the 
                                  player(s) accused will be investigated. Players 
                                  found to have been, or reasonably suspected 
                                  of cheating in any form, will have their account 
                                  privileges terminated throughout Boss Media's 
                                  casinos indefinitely. Using the standard of 
                                  'beyond a reasonable doubt" to indicate cheating, 
                                  The All Horse Racing Poker Room has the 
                                  right to confiscate any funds won while cheating. 
                                  Confiscated funds will be distributed to victim 
                                  players. Boss Media/The All Horse Racing 
                                  Poker Room will never keep confiscated funds. 
                                  Any player who has funds confiscated for cheating 
                                  will be told the screen names of the players 
                                  who have received their money. 
                                <li> <b>Team play or collusion:</b> <br>
                                  Team play or collusion is the act of two or 
                                  more players 'teaming up' to gain a competitive 
                                  advantage by signaling card values or making 
                                  raises and re-raises that are intended to force 
                                  a victim player out of a pot. If customer service 
                                  receives a complaint about such an activity 
                                  or management suspects such activity, the players 
                                  accused/suspected will have their accounts and 
                                  playing activity reviewed by management. Players 
                                  found to have been acting, or reasonably suspected 
                                  of acting as a team, will have their account 
                                  privileges terminated throughout Boss Media's 
                                  casinos indefinitely. If warranted, The All 
                                Horse Racing 
                                  Poker Room has the right to confiscate any funds 
                                  won while acting as a team. Confiscated funds 
                                  will be distributed to victim players. 
                                <li> <b>Violations of house rules - If a player 
                                  commits any of the following acts, he is in 
                                  breach/violation of house rules:</b> <br>
                                  Players may not chat about a hand while it is 
                                  in progress (i.e., a comment like 'I have a 
                                  heart flush draw.' General comments like 'I'm 
                                  going to get my revenge this hand' may be considered 
                                  acceptable); </li>
                              </ol>
                              <p>Players may not offer advice to another player through 
                              the chat during a hand, or urge a player to take 
                              any kind of action; <br>
                              <br>
                              Only English may be written in the chat; <br>
                              <br>
                              Any foul, abusive or threatening language will not 
                              be tolerated on any game table; <br>
                              <br>
                              Players may not chat about any mucked cards they 
                              held during a hand; and <br>
                              <br>
                              Players may not chat about any current cards being 
                              held during a hand. <br>
                              <br>
                              Management will judge the severity of a violation. 
                              The All Horse Racing Poker Room has the right 
                              to suspend or revoke membership privileges. </p>
        <p><br>
&nbsp;</p>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>