<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
                              <p align="center"><font color="#0958A6"><b class=bodytext>
                              <font size="3">Official Poker Room House Rules</font></b><font size="3">
                              </font></font> 
                              <ul>
                                <li>In all disputes in which a ruling, interpretation, 
                                  clarification or intervention is required, all 
                                  decisions made by All Horse Racing Poker 
                                  Room management are final and binding. 
                                <li>Management has the right to close any game 
                                  at any time. 
                                <li>Management has the right to remove a player 
                                  for a time period to be determined by management 
                                  for a violation of a house rule. 
                                <li>In the unlikely event of a software error 
                                  which subsequently rewards a pot to the wrong 
                                  player, All Horse Racing Poker Room has 
                                  the right to adjust players' account balances 
                                  to correct any and all errors. The All Horse 
                                Racing 
                                  Poker Room will contact all affected players 
                                  and present a hand history and detailed explanation 
                                  of the error within 48 hours. 
                                <li>If a player notices that an unlikely error 
                                  of any kind has occurred during a game, the 
                                  player is obliged to contact support services 
                                  (support@All Horse Racing). 
                                <li>In the unlikely event of a server crash that 
                                  removes all players from the table, the hand 
                                  in play will be considered dead (void) and all 
                                  monies will be returned after an investigation 
                                  has been conducted. 
                                <li>In the unlikely event of an error, the hand 
                                  will be dead. Please refer to 'error' under 
                                  our The All Horse Racing Poker Room General 
                                  House Policies. 
                                <li>All players have the right to request a hand 
                                  history in the event of a dispute. 
                                <li>Players may not act as a team. If team play 
                                  (collusion) is suspected, The All Horse Racing 
                                  Poker Room has the right to suspend suspected 
                                  accounts and funds while an investigation is 
                                  conducted. Players found to have been acting, 
                                  or reasonably suspected of acting as a team, 
                                  will have their account privileges indefinitely 
                                  terminated at all casinos operated by Boss Media. 
                                  If warranted, The All Horse Racing Poker 
                                  Room has the right to confiscate any funds won 
                                  while players were acting as a team. Confiscated 
                                  funds will be distributed to victimized players. 
                                <li>No 'board talk' is allowed. Players may not 
                                  chat about a hand while it is in progress. Although 
                                  some comments may be innocuous, the presumption 
                                  will be that any comments about the play of 
                                  a hand are in violation of our rules, and the 
                                  burden of proof will be on the violating player 
                                  to convince us otherwise. To be safe, avoid 
                                  making jokes about the hand you are holding 
                                  or how you intend to play during a hand. 
                                <li>Players may not use chat to offer advice to 
                                  another player during a hand, or urge a player 
                                  to take a particular course of action (e.g., 
                                  it is a rule violation to use chat to say 'Call 
                                  him, he's bluffing!'). 
                                <li>Players may not chat about any folded cards 
                                  they held during a hand. 
                                <li>Players may not chat about any current cards 
                                  they are holding during a hand. 
                                <li>Only the English language may be written using 
                                  chat. 
                                <li>Foul, abusive or threatening language will 
                                  not be tolerated at the gaming tables. Players 
                                  may lose membership privileges for any repeat 
                                  occurrences. 
                                <li>The game is played for 'table stakes' and 
                                  as such, a player may not be forced out of hand 
                                  for a lack of money. To complete the hand, a 
                                  player will be allowed to go All-In. 
                                <li>If a player is disconnected during play for 
                                  any reason, then he will be considered All-In 
                                  if such player has any All-Ins remaining. In 
                                  the event that a player does not have an All-In 
                                  remaining, then the player will be folded and 
                                  lose the funds contributed to the pot. 
                                <li>The All-In limit is one (1) per day. If a 
                                  player goes through the All-In in a day, he 
                                  must send an email to customer service, asking 
                                  that his All-Ins be reset, and explaining the 
                                  reason for the needed additional All-Ins. Customers 
                                  who abuse the All-In policy are subject to having 
                                  their accounts closed. 
                                <li>A 'cap' of three (3) raises is allotted per 
                                  betting round. 
                                <li>If there are only two remaining players, playing 
                                  'heads up,' then raises are unlimited. 
                                <li>Players are allowed to Check and Raise. 
                                <li>The determination of which player's hand will 
                                  and must be shown first during the showdown 
                                  will depend on which player initiated the action 
                                  and/or which player initiated the last bet, 
                                  raise or re-raise. 
                                <li>Players who win a non-called pot are not obligated 
                                  to show their cards. 
                                <li>Players may not fold a checked hand. 
                                <li>Cards 'speak for themselves' when determining 
                                  the winner of a pot. The universal poker hand 
                                  rankings apply, and a list of these rankings 
                                  can be found at the Poker Hand Rankings link. 
                                <li>If two or more players tie by holding equally-strong 
                                  winning five-card hands, the pot will be split 
                                  between the tying players. If there is an odd 
                                  chip, that chip will be broken down to the smallest 
                                  value chips in the game. If the amount to be 
                                  split is still uneven, the player who was in 
                                  earliest position gets the odd chip in flop 
                                  games. In high-low games, the high hand gets 
                                  the odd chip. 
                                <li>The house shall collect a 'rake' not to exceed 
                                  $3 per pot, except in games featuring stakes 
                                  of 1-2 or lower, where the maximum rake is $1. 
                                  Please see 'house rakes' on our website. 
                                <li>The Reserve-Seat and Sit-Out features are 
                                  intended to allow a player to take a small break 
                                  from the action without losing his or her seat. 
                                  Players must return to the game within 10 rounds. 
                                  If a player fails to return to the game, then 
                                  the player will be picked up from the table. 
                                <li>Short buys are not permitted. The only exception 
                                  to this rule is if the player has already bought 
                                  into the game for the acceptable minimum, and 
                                  wants to add chips from his account, but does 
                                  not have enough chips in his account to reach 
                                  the minimum. That player will be allowed to 
                                  add as many chips as he has left in his account. 
                                <li>Straddle bets are not permitted. 
                                <li>Membership to The All Horse Racing 
                                  Poker Room can be revoked at anytime for any 
                                  reason. The funds in a player's account will 
                                  be returned unless the player has been found 
                                  to have cheated another player or players, in 
                                  which case some or all funds may be distributed 
                                  to the victimized players. In no event will 
                                  The All Horse Racing Poker Room simply 
                                  confiscate a player's funds and retain them 
                                  for The All Horse Racing Poker Room. 
                                </li>
                              </ul>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>