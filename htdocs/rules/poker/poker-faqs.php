<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="center"><br>
          <img border="0" src="../images/faqs.gif"></p>
                              <p>We think we know what's on your mind, so please 
                                take a look below at some of the most frequently 
                                asked questions. If you have any questions on 
                                any of the below, or if you did not find what 
                                you were looking for, please feel free to contact 
                                us 24 hours a day at the
                              <a href="../customercare.php">Support Center</a>.<br>
                                <br>
                                <a href="#1"><b 
            >Is this software unique or do other poker sites use the same software?</b></a> 
                                <br>
                                <br>
                                <a 
            href="#2"><b>Will The All Horse Racing Poker Room offer Hi/low 
                                games, Omaha, Tournaments, and Progressive Jackpots?</b></a> 
                                <br>
                                <br>
                                <a 
            href="#3"><b>What kind of computer do I need?</b></a> <br>
                                <br>
                                <a 
            href="#4"><b>What is the recommended screen setting? </b></a><br>
                                <br>
                                <a 
            href="#5"><b>Does your software support Macintosh? </b></a><br>
                                <br>
                                <a 
            href="#6"><b>I have a 21-inch display. Can I get the casino in full 
                                screen size?</b> </a><br>
                                <br>
                                <a 
            href="#7"><b>Can I see what the game looks like before I join or deposit 
                                any money?</b></a> <br>
                                <br>
                                <a 
            href="#8"><b>Will I be secure playing at The All Horse Racing 
                                Poker Room?</b></a> <br>
                                <br>
                                <a href="#9"><b 
            >How can I be sure your card shuffling and random number generator 
                                is truly random?</b></a> <br>
                                <br>
                                <a 
            href="#10"><b>Is this legal in my area?</b></a> <br>
                                <br>
                                <a 
            href="#11"><b>I lost my username and password! What do I do?</b></a> 
                                <br>
                                <br>
                                <a 
            href="#12"><b>How do I change my nickname, username, and password, 
                                address information?</b></a> <br>
                                <br>
                                <a 
            href="#13"><b>How do I join a game and begin to play?</b></a> <br>
                                <br>
                                <a 
            href="#14"><b>How do I know how much is in the pot?</b></a> <br>
                                <br>
                                <a 
            href="#15"><b>Can I request a seat change?</b></a> <br>
                                <br>
                                <a 
            href="#16"><b>How do I get a history of the hands I have played?</b></a> 
                                <br>
                                <br>
                                <a 
            href="#17"><b 
            >What happens if I get disconnected?</b> </a><br>
                                <br>
                                <a 
            href="#18"><b 
            >What about your all-in policy, how does that work? How many all-ins 
                                will be allowed?</b></a> <br>
                                <br>
                                <a 
            href="#22"><b>How do I leave a game or table?</b></a> <br>
                                <br>
                                <a 
            href="#23"><b>How do I minimize the software or screen?</b></a><br>
                                <br>
                                <b><a name="1"></a>Is this software unique or 
                                do other poker sites use the same software?</b> 
                                <br>
                                The All Horse Racing Poker Room software is 
                                one of a kind and you will not find anything close 
                                resembling our software.<br>
                                <br>
                                <b><a name="2"></a>Will The All Horse Racing 
                                Poker Room offer Hi/low games, Omaha, Tournaments, 
                                and Progressive Jackpots?</b> <br>
                                The first version of The All Horse Racing 
                                Poker Room is just one of many to come and is 
                                designed to fulfill the demand of our Hold'em 
                                and Seven Card Stud enthusiasts. With the help 
                                of our expert adviser The All Horse Racing 
                                Poker Room will continue building the site not 
                                to become the biggest poker room on the internet, 
                                but rather the finest in quality, security, reliability 
                                and community activity. <br>
                                <br>
                                Not only do we plan to provide a tournament platform, 
                                special progressive jackpot programs and more 
                                games, but also we will continue to aggressively 
                                expand The All Horse Racing Poker Room with 
                                new innovative ideas and game products. We have 
                                a lot coming to The All Horse Racing Poker 
                                Room! <br>
                                <br>
                                <b><a name="3"></a>What kind of computer do I 
                                need?</b> <br>
                                We recommend a Pentium 133 MHz with a minimum 
                                of 25 MB available on your hard drive and 64 MB 
                                RAM (internal memory). Operating system: Microsoft 
                                Windows 95/98/NT/2000/XP. Please note the software 
                                will not function if Microsoft Winsock proxy service 
                                is running. <br>
                                <br>
                                <b><a name="4"></a>What is the recommended screen 
                                setting?</b> <br>
                                To enjoy the full impact of the The All Horse 
                              Racing 
                                Poker Room environment we recommend a minimum 
                                screen resolution of 800 X 600 and 256 colors. 
                                If you play on a lower resolution screen such 
                                as 640x480 pixels you will have to scroll to use 
                                the game. <br>
                                <br>
                                <b><a name="5"></a>Does your software support 
                                Macintosh?</b> <br>
                                No, Macintosh is not supported at this moment, 
                                we are in the process of developing a Macintosh 
                                version of the software however. Stay tuned for 
                                more. <br>
                                <br>
                                <b><a name="6"></a>I have a 21-inch display. Can 
                                I get the casino in full screen size?</b> <br>
                                Yes, you can select the full screen mode from 
                                the options menu in the The All Horse Racing 
                                Poker Room lobby. <br>
                                <br>
                                <b><a name="7"></a>Can I see what the game looks 
                                like before I join or deposit any money?</b> <br>
                                Absolutely! In fact you can even play for FREE! 
                                <br>
                                Simply download the free software and set up an 
                                account. You do not need to deposit any money 
                                in order to observe or join our beta practice 
                                games. However, you are asked to provide the correct 
                                information in your account. <br>
                                You only have to deposit money when you want to 
                                play the games for real money. After you have 
                                created your account, you will be able to look 
                                around the lobby area and go to tables. <br>
                                <br>
                                <b><a name="8"></a>Will I be secure playing at 
                                The All Horse Racing Poker Room?</b> <br>
                                It is important that you are protected from undesirable 
                                player tactics such as team play (collusion) or 
                                any other sort of undesirable activity. The All 
                              Horse Racing 
                                Poker Room uses reliable security features that 
                                were developed specifically for poker, and we 
                                are committed to do everything we can to protect 
                                your play. Although we continually monitor games 
                                both with automatic systems and live support personnel, 
                                you can help by contacting us (<a href="mailto:support@allhorseracing.com?subject=Help with Poker">support@allhorseracing.com</a>) 
                                if at any time you suspect that one or more players 
                                is colluding or otherwise violating site rules. 
                                <br>
                                <br>
                                <b><a name="9"></a>How can I be sure your card 
                                shuffling and random number generator is truly 
                                random?</b> <br>
                                We would like you to know that we cannot alter 
                                the outcome of the gaming as the software is hard-coded. 
                                Also, we will kindly point out that our software 
                                is provided by Boss Media AB, one of the leading 
                                software suppliers to the online gaming industry 
                                since 1997. <br>
                                <br>
                                As a publicly held company listed on the Stockholm 
                                Exchange, Boss Media must adhere to all regulations 
                                of the exchange and be held accountable to shareholders. 
                                Additionally, TST of Australia, one of the top 
                                independent test firms in the gaming industry, 
                                has tested the platform that the The All Horse 
                              Racing 
                                Poker Room was built on. <br>
                                <br>
                                Lastly, Boss Media clients hold gaming licenses 
                                in Curacao, Kahnawake, Antigua, Isle of Man and 
                                Gibraltar. This would further indicate that the 
                                software platform and random number generator 
                                is truly random and secure, as it had to pass 
                                five (5) governments' gaming boards. The Isle 
                                of Man is well known to be the strictest board 
                                in the industry. <br>
                                <br>
                                <b><a name="10"></a>Is this legal in my area?</b> 
                                <br>
                                You MUST be 18 year of age or older to play with 
                                The All Horse Racing Poker Room. Please beware, 
                                playing poker online may or may not be legal in 
                                your jurisdiction. You should check the laws of 
                                the area in which you live before playing poker 
                                online. <br>
                                <br>
                                <b><a name="11"></a>I lost my username and password! 
                                What do I do?</b> <br>
                                As this information is secure, you will need to 
                                contact us directly via telephone and we will 
                                be happy to help you 24 hours a day. Please feel 
                                free to contact: <br>
                                <a href="../customercare.php">Click here to view how to contact us</a> 
                                <br>
                                <br>
                                <b><a name="12"></a>How do I change my nickname, 
                                username, and password, address information?</b> 
                                <br>
                                You are free to change your personal information 
                                anytime you wish. To change any personal information, 
                                click on Personal Information from the The All 
                              Horse Racing 
                                Poker Room lobby. Change the desired information. 
                                <br>
                                <br>
                                <b><a name="13"></a>How do I join a game and begin 
                                to play?</b> <br>
                                Before you can play, you need to choose a game 
                                and table. This is found in our lobby. After you 
                                have selected which game you would like to play, 
                                simply highlight the table selection and click 
                                'go to table.' You are automatically taken to 
                                the game table. <br>
                                <br>
                                Once you arrive at the table, you will have to 
                                choose which seat you would like to play in. Simply 
                                click 'join' and you will be seated. You will 
                                then be asked how many chips you'd like to buy-into 
                                the table with. The minimum amount will be posted 
                                and you have the choice to take all or only a 
                                portion of your bankroll as long as it meets the 
                                minimum. <br>
                                <br>
                                <b><a name="14"></a>How do I know how much is 
                                in the pot</b>? <br>
                                You can move your mouse over the pot and a pop-up 
                                box will display the exact figure. <br>
                                <br>
                                <b><a name="15"></a>Can I request a seat change?</b> 
                                <br>
                                We are sorry, but it is not possible to request 
                                a seat change. <br>
                                <br>
                                <b><a name="16"></a>How do I get a history of 
                                the hands I have played?</b> <br>
                                You can request a history of your hand by selecting 
                                the 'request hand history' option right at the 
                                table! We will present this information immediately 
                                following the conclusion of the game and we display 
                                the last five hands that you have played. <br>
                                <br>
                                <b><a name="17"></a>What happens if I get disconnected?</b> 
                                <br>
                                The All Horse Racing Poker Room has gone to 
                                considerable lengths to ensure a poker environment 
                                which you can feel comfortable in knowing you 
                                are protected just in case you loose connection 
                                to the game server while playing. If this happens, 
                                you will be treated as being all-in, meaning that 
                                your investment into the pot to that point WILL 
                                BE PROTECTED and you will still be eligible for 
                                your share of the pot if you are to hold the best 
                                cards. <br>
                                <br>
                                <b><a name="18"></a>What about your all-in policy, 
                                how does that work? How many all-ins will be allowed?</b> 
                                <br>
                                All-in protection is designed to protect your 
                                investment into a pot when you run out of money 
                                during a hand, do not act in time, or get disconnected 
                                for any reason. We allow each player at The All 
                              Horse Racing 
                                Poker Room two (2) all-in protections per 24 hours. 
                                Please read the end of this section for important 
                                information. <br>
                                <br>
                                <b><a name="19"></a>Going all-in when you do not 
                                have enough money to continue:</b> <br>
                                If you run out of money during a hand, by no means 
                                do you have to fold the hand. You may go all-in. 
                                This means that you cannot be forced out of a 
                                hand if you do not have enough money to complete 
                                the hand that you started. When you run out of 
                                money or only have enough to call a portion of 
                                the wager, you can go all-in to contend for the 
                                money in a pot that you have contributed to. All 
                                other action of remaining 'active' players goes 
                                into a 'side pot' to be fought for by them. The 
                                reason we allow a player to go all-in is because 
                                all poker rooms play for "table stakes". This 
                                is defined as: all money that a player has at 
                                the beginning of a hand will play during that 
                                hand. <br>
                                <br>
                                <b><a name="20"></a>Going all-in when failing 
                                to act in time:</b> <br>
                                If in the event you fail to act on your hand in 
                                time (10 seconds), the system will assume you 
                                have become unavailable to complete the hand. 
                                As such, placing you all-in will protect your 
                                investment into the pot to that point in the game. 
                                The system will track and record all of your cards 
                                and show them to all players during the showdown. 
                                If you win, your account will be credited accordingly. 
                                You may request a hand history at the table to 
                                review the entire hand for verification of the 
                                outcome. <br>
                                <br>
                                <b><a name="21"></a>Going all-in when disconnected:</b> 
                                <br>
                                We understand that gaming on the Internet can 
                                be unstable from time to time and that a player 
                                can get disconnected from the game server. Regardless 
                                of the reason for the disconnection, we will protect 
                                your investment to that point by placing you all-in. 
                                The system will track and record all of your cards 
                                and show them to all players during the showdown. 
                                If you win, your account will be credited accordingly. 
                                You may request a hand history at the table to 
                                review the entire hand for verification of the 
                                outcome. <br>
                                <br>
                                <b 
            >***Important information to know***</b> <br>
                                If at anytime your all-in status is zero (0) you 
                                are playing at your own risk. This simply means 
                                that you do not have all-in protection and you 
                                are disconnected or fail to act in time, YOUR 
                                HAND WILL BE FOLDED AND YOUR INVESTMENT WILL BE 
                                LOST without any recourse whatsoever. <br>
                                Although we allot you one (1) all-in protections 
                              per 24 hours, we will consider resetting your 
                              protection back if warranted. Simply contact
                              <a href="mailto:support@allhorseracing.com">
                              support@allhorseracing.com</a> 
                                for consideration. However, if The All Horse 
                              Racing 
                                Poker Room suspects abuse of the all-in protection 
                                feature, this may result in loss of membership 
                                or denied reset request. <br>
                                <br>
                                Lastly, sending a request or receiving approval 
                                notification does not imply that you have all-in 
                                protection; you do not have all-in protection 
                                until you see this in your account. <br>
                                <br>
                                If you have any questions, please feel free to 
                                contact us. <br>
                                <br>
                                <b><a name="22"></a>How do I leave a game or table?</b> 
                                <br>
                                When you decide you wish to leave the table, just 
                                click on the "Exit Game" button. If a hand in 
                                which you are playing is currently in progress, 
                                then we suggest you check 'sit out' to allow you 
                                to leave the table when the hand is complete. 
                                Of course, if you fold your hand, you may leave 
                                the game at that time as well. <br>
                                <b><br>
                                <a name="23"></a>How do I minimize the software 
                                or screen?</b> <br>
                                The All Horse Racing Poker Room was built 
                                and best viewed with a minimum screen resolution 
                                setting of 800 x 600 pixels. This will give you 
                                a "full screen" view and is what you are most 
                                likely set to now. However, in some cases this 
                                setting may not allow you to pull up other programs 
                                while playing. <br>
                                <br>
                                To reduce the image to a "normal" view, you will 
                                need to set your screen display to 1024 X 768 
                                to allow you the use other programs while playing. 
                                To change your current settings, you may do the 
                                following: </p>
                              <ul>
                                <li>Click on the Windows Start menu button on 
                                  your desktop 
                                <li>Select Settings 
                                <li>Click on Control Panel 
                                <li>Select Display from the available menu 
                                <li>The Display Properties window will come up 
                                <li>Select Settings 
                                <li>At the bottom right of the window you will 
                                  see a section called Screen Area displaying 
                                  an arrow-like symbol with Less and More on each 
                                  side. 
                                <li>Point with the mouse on the arrow, and change 
                                  your screen resolution to "1024x 768" pixels. 
                                <li>Once you have selected the desired resolution, 
                                  click on Apply and then OK. 
                                <li>Windows will give you a preview of the new 
                                  settings and will prompt you to agree with the 
                                  new settings by clicking on OK (You will have 
                                  approximately 15 seconds to choose before Windows 
                                  will restore the original settings) 
                                <li>If you are unable to select a resolution higher 
                                  than "800 x 600" pixels, you will have to contact 
                                  your manufacturer and request to upgrade your 
                                  graphic card. </li>
                              </ul>
                              <p>Finally you may need to reboot your computer to 
                              ensure that the new settings have been successfully 
                              put in place. </p>
        <p align="left">&nbsp;</p>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>