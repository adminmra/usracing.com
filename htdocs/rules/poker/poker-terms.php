<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
           <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
        <p align="center"><font color="#0958A6"> <b><font size="3">Poker Terms</font></b><font size="3">
        </font></font> 
                              <b><font size="3" color="#0958A6"><br>
                              </font>
                              <br>
                              &nbsp;</b></p>
        <p> 
                              <b>Action:<br>
                              </b>Another term for "betting," that is, to start 
                              the action is to start the betting.<b><br>
                              <br>
                              Ante:<br>
                              </b>A small sum of money, placed in the pot by each 
                              player. Antes are used in Stud and Draw, but not 
                              in Hold'em or Omaha.<br>
                              <br>
                              <b>Big Blind:<br>
                              </b>A bet that must be posted by the player two 
                              seats to the left of the button. It is equal to 
                              the amount of the smaller betting limit in a game, 
                              for example, in a 10-20 game, the big blind would 
                              be $10. <b><br>
                              <br>
                              Blind:<br>
                              </b>Forced bets placed in the pot by the first two 
                              players in front of the dealer button, in Hold'em 
                              and Omaha. See "small blind" and "big blind."<br>
                              <br>
                              <b>Bluff:<br>
                              </b>To bet when you hold a weak hand, hoping that 
                              the intimidation factor of your bet can win the 
                              hand. <b><br>
                              <br>
                              Bring-in:<br>
                              </b>In Stud, a bet that must be made on the very 
                              first betting round. Usually the player showing 
                              the lowest card is forced to make a bet; in some 
                              games, the player showing the highest card is forced. 
                              The bring-in applies only on the very first betting 
                              round, though. On all further rounds, the player 
                              showing the highest hand on board has the OPTION 
                              to bet first, but need not. <b><br>
                              <br>
                              Call:<br>
                              </b>To match a bet that has been made. <b><br>
                              <br>
                              Check:<br>
                              </b>To possess the option to bet, but decline. A 
                              player cannot check once someone else has bet; at 
                              that point, the player must call, raise, or fold. 
                              But if no one has yet bet, a player can check, allowing 
                              the betting option to pass to the next player. <b><br>
                              <br>
                              Check-raise:<br>
                              </b>To check, indicating weakness, with the intention 
                              of raising after someone else bets.<br>
                              Check-raises are allowed in all casino poker games; 
                              in some home games, they are frowned upon. <b><br>
                              <br>
                              Fifth Street:<br>
                              </b>The fifth community card in Hold'em or Omaha 
                              (in these games, 5th street is more often called 
                              "the river."). Also sometimes used to refer to the 
                              fifth card received in 7 Card Stud. <b><br>
                              <br>
                              Flop:<br>
                              </b>In Hold'em or Omaha, the first three community 
                              cards, turned up all at once. <b><br>
                              <br>
                              Fold:<br>
                              </b>To drop out of a hand. <b><br>
                              <br>
                              Fourth Street:<br>
                              </b>The fourth community card in Hold'em or Omaha 
                              (in these games, 4th street is more often called 
                              "the turn."). Also sometimes used to refer to the 
                              fourth card received in 7 Card Stud. <b><br>
                              <br>
                              Hole cards:<br>
                              </b>Cards that are face down and cannot be seen 
                              by the other players.<br>
                              <br>
                              <b>Kicker:<br>
                              </b>Two meanings. </p>
                              <p>1) A single card kept along with a pair, in Draw, 
                                in an attempt to make two pair. For example, someone 
                                might keep 3-3-K, drawing two cards, in the hope 
                                that he might get either a three (for trips) or 
                                a King (making two pair, Kings-up). </p>
                              <p>2) The highest single card held by two players 
                                in Hold'em who each hold the same pair. For example, 
                                if the board in Hold'em is A-10-8-5-2, and Player 
                                One holds A-J as his hand, and Player Two holds 
                                A-Q, each player has a pair of Aces, but Player 
                                Two has a better kicker and would win the hand.</p>
                              <p>
                              <b>Narrowing the Field:<br>
                              </b>To bet or raise in the hopes that you will drive 
                              out some players whose hands are currently worse 
                              than yours, but who might improve if allowed to 
                              stay in.<br>
                              <br>
                              <b>Nuts, The:<br>
                              </b>The best possible hand. This phrase is almost 
                              always used in the context of a particular hand 
                              (otherwise "the nuts" would just be a term for a 
                              royal flush). For example, in Hold'em, a player 
                              holding 8-9 would hold "the nuts" if the flop came 
                              6-7-10. At that moment, the 6-7-8-9-10 straight 
                              is the best possible hand. However, if the Turn 
                              card were a Jack, and the River a Queen, a player 
                              holding A-K would then have the nuts-a 10-J-Q-K-A 
                              straight. <b><br>
                              <br>
                              Pot:<br>
                              </b>The money in the center of the table, being 
                              contested by the players still remaining in the 
                              hand. <b><br>
                              <br>
                              Rake:<br>
                              </b>The amount of money the casino takes from the 
                              pot to make money from the poker game. In low limit 
                              games, the casino usually rakes some percentage 
                              of the pot, usually a maximum of 10% of the pot. 
                              In higher limit games, the casino makes money either 
                              by charging players an hourly fee to play, or by 
                              collecting a fee each time a player holds the button. 
                              <b><br>
                              <br>
                              River:<br>
                              </b>In Hold'em or Omaha, the fifth and final community 
                              card. Also sometimes called fifth street. <b><br>
                              <br>
                              Rock:<br>
                              </b>A player known to be very conservative, who 
                              usually bets or raises only when he has a very powerful 
                              hand. <b> <br>
                              <br>
                              Small Blind:<br>
                              </b>A bet that must be posted by the player one 
                              seat to the left of the button. It is usually equal 
                              to one half of the smaller betting limit in a game, 
                              for example, in a 10-20 game, the small blind would 
                              be $5. Occasionally, the small blind is some other 
                              fraction of the big blind.
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>