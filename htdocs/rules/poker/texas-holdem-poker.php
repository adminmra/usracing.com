<html>
<head>
<?php	 		 	 include "inc_metatags.inc"; ?>
<script language="JavaScript" src="javascripts.js"></script>
<link href="stylesheet_main.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="MM_preloadImages('images/taketour-o.gif')">
<table width="749" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	  <?php	 	 include "inc_menu_insidepage.inc"; ?>
	</td>
  </tr>
  <tr>
    <td><img src="../images/title_pokerroom.jpg"></td>
  </tr>
  <tr>
    <td><table width="749"  border="0" cellspacing="0" cellpadding="0">
      <tr valign="top">
        <td width="10">&nbsp;</td>
        <td width="150"><br>
            <?php	 	 include "../inc_left_poker.inc"; ?><br><br>
    <?php	 	 include "../inc_left_poker101.inc"; ?>
        </td>
        <td width="20">&nbsp;</td>
        <td width="569">
        <p align="right"><br>
          &nbsp;</p>
                              <p align="center"><font color="#0958A6" size="3"><b class=bodytext>Texas Hold�em</b></font></p>
                              <p align="center">
                              <a href="javascript:document.downloadForm.submit();">
                              <img border="0" src="/poker/images/index.2.jpg" alt="Texas Hold'Em"></a></p>
                              <p>This section will inform you on gaming procedures, 
                                rules, policies and limits of All Horse Racing's 
                                game of Texas Hold�em.<br>
                                <br>
                                <img 
            src="images/rule.jpg" 
            align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> is an abbreviation 
                                for Rules. </p>
                              <ul>
                                <li> 
                                  <p><a 
              href="#dealing">Dealing the Game of Texas Hold�em</a></p>
                                <li> 
                                  <p><a 
              href="#rules">Texas Hold�em Blind Rules</a></p>
                                <li> 
                                  <p><a 
              href="#games">All Horse Racing Texas Hold�em Games</a></p>
                                </li>
                              </ul>
                              <p>
                              <b><a name=dealing></a>Dealing the Game of Texas 
                              Hold�em</b> <br>
                              All Texas Hold�em games that are offered at All 
                              Horse Racing 
                              are �fixed� limit games. The object is to create 
                              the best five-card hand using seven cards. </p>
                              <p><b class=red>Before the Deal:</b></p>
                              <ul>
                                <li> 
                                  <p>Players will buy-in for the posted amount.</p>
                                <li> 
                                  <p>Selected players will post blinds.</p>
                                </li>
                              </ul>
                              <p>
                              <b>The Dealer Button:</b> <br>
                              Unlike Seven Card Stud wherein the dealer deals 
                              each opening round clockwise around the table starting 
                              with the player closest to the left, the dealer 
                              in Hold�em will start to deal each game contingent 
                              upon which player has the �button.� The button is 
                              a graphical representation (�D�) of which player 
                              is the �dealer.� Although our dealer will be dealing 
                              the Hold�em game, the player who has the button 
                              placed in front of his seat gets to play his cards 
                              as if he were the actual dealer. When the cards 
                              are dealt to players, they are dealt in a manner 
                              as if the player was actually dealing in a live 
                              environment. <b><br>
                              <br>
                              Blinds:</b> <br>
                              Because we have a player �on the button� we now 
                              ask two players via a specific voice announcement 
                              (just those players will hear the announcement) 
                              to �post the large or small blinds please.� The 
                              blinds serve a purpose similar to antes, in that 
                              they put forced money into the pot that gives players 
                              an incentive to enter the hand. However, only two 
                              players will �post� or �put up� the blinds. </p>
                              <p>The first blind is called the �small blind�. 
                                This bet is usually half the minimum bet of the 
                                game, although in some games, the fraction is 
                                slightly different. In $15-30, the small blind 
                                is $7, and in $5-10, the small blind is $2.</p>
                              <p>So, in a $2-4 game the small blind will be $1. 
                                The second blind is called the �large blind� and 
                                is always the same size as the game�s minimum 
                                bet, e.g., in a 5-10 game, the large blind is 
                                $5.</p>
                              <p>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                                The player directly left of the button will have 
                                the �small blind.� The player directly to the 
                                left of the small blind will have the �large blind� 
                                of the full amount or the lowest game limit.</p>
                              <p>
                              <b>Opening Deal:</b> <br>
                              Now that we have a Button and small and large blinds, 
                              we are ready to deal. The dealer <font color=red><b>always</b></font> 
                              deals from the player closest to the dealer�s left. 
                              Moving clockwise around the table, the game will 
                              �deal-in� each player. The players will be dealt 
                              one card face down, then a second card face down. 
                              A round of betting will occur starting with the 
                              player seated to the left of the large blind. <b><br>
                              <br>
                              Betting on Opening Deal:<br>
                              </b> 
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                              The player seated to the left of the large blind 
                              will always have the action on the opening deal. 
                              This player may not check, but rather can only fold, 
                              call, or raise the amount of the large blind. </p>
                              <p>The game will now advance to each player seated 
                                asking to fold, call, or raise until we reach 
                                the large blind for an action decision. If no 
                                one has raised by the time the play comes back 
                                around to the large blind, the large blind has 
                                the option to �check� his own BLIND wager or raise.</p>
                              <p>Once all players have completed the first round 
                                of wagering, they will proceed to the flop.</p>
                              <p>
                              <b>Flop:</b> <br>
                              The next cards to be dealt into the game will be 
                              the third, fourth and fifth cards in the game. These 
                              three cards will not be dealt to each player, but 
                              rather placed face up in the center of the table.
        </p>
                              <p>But before we �flop� anything, we must burn a 
                                card. The dealer will deal face down one card 
                                into the pot. After the burn card, the dealer 
                                will deal three cards face up in the center of 
                                the poker table. These three cards are called 
                                �community cards� which are available to all players 
                                for potential use to make a poker hand. The area 
                                in which these cards lie on the table is commonly 
                                referred to as the �<font color=red><b>board</b></font>�. 
                              </p>
                              <p>The look of the flop:<br>
                                <br>
                                <img 
            src="images/the-flop.jpg" border="0" alt="The Flop"> </p>
                              <p>Now the flop has landed on the �board� and all 
                                players now have five cards available to make 
                                their hand, the two �hole� cards that were dealt 
                                on the opening round and now three �community 
                                cards� which all players may use. The rule of 
                                the determination of the action is as follows.</p>
                              <p>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                                After the opening deal, the player who is seated 
                                closest to the left of the button shall have the 
                                initial action for the remainder of the game. 
                                If the player who has the button folds, then the 
                                button is still active and will remain in front 
                                of that player�s seat to keep position a constant 
                                throughout that game.</p>
                              <p>The player that has the action may check or bet. 
                                As soon as one player chooses to bet, then the 
                                other players in the hand can no longer check; 
                                they can only fold, call or raise the amount that 
                                is proper for that round (the lower betting limit 
                                on the first round and on the flop, and the higher 
                                betting limit on the turn and the river).</p>
                              <p>
                              <b>The Turn:</b> <br>
                              The �turn� is the fourth card to be dealt onto the 
                              board and the sixth card available to the player. 
                              Some players call this �fourth street.� However, 
                              the most common term used for this round is the 
                              �turn�. As always, the dealer will burn a card and 
                              then deal one card face up onto the board to the 
                              right of the last flop card.<br>
                              <br>
                              The look of the turn:<br>
                              <br>
                              <img 
            src="images/the-turn.jpg" border="0" alt="The Turn or Fourth Street"> 
                              <br>
                              The 8 of clubs is the �turn card� </p>
                              <p>At this point the players have access to the 
                                four cards on the board and their two hole cards. 
                                The game will now declare who has the action, 
                                which always begins with the player still remaining 
                                in the hand who is closest to the left of the 
                                button.</p>
                              <p>The bet on the turn is the higher level of the 
                                betting limit. In a $2-4 game, this would be $4. 
                                All raises will be in $4 increments with a cap 
                                of three raises. If there are just two players 
                                remaining, the number of raises is unlimited at 
                                our real money tables.</p>
                              <p>However, in tournament play, the three-raise 
                                limit applies even if there are only two players 
                                left in a hand.</p>
                              <p>
                              <b><br>
                              The River:</b> <br>
                              The dealer will then place the fifth and final card 
                              on the board. </p>
                              <p>The Flop Cards turn River<br>
                                <br>
                                River Look:<br>
                                <br>
                                <img 
            src="images/the-river.jpg" border="0" alt="The River"> </p>
                              <p>At this point, five cards are on the board and 
                                two hole cards are in the players� hands. The 
                                action again starts with the first player still 
                                remaining in the hand who is closest to the left 
                                of the button. All checks, bets, raises, and folds 
                                will be completed and then a showdown will begin.</p>
                              <p>
                              <b>Who shows first?</b><br>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                              The determination of which players� cards will and 
                              must be shown first will lie with the player who 
                              had initiated the action or with the person who 
                              had initiated the last bet, raise or re-raise. This 
                              simply means that whoever had the last action on 
                              the river must show his/her cards first.<br>
                              <br>
                              <b>Suppose a Player wins by default?<br>
                              </b> 
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                              A player who has a winning hand does not have to 
                              show his/her cards if his/her bet was not called. 
                              <b><br>
                              <br>
                              Does a Player have to show their Cards if they call 
                              a bet on the River?</b><br>
                              <img src="images/rule.jpg" align=absMiddle border="0" alt="Texas Hold'em Poker Rule"> 
                              A player is not required to show their cards if, 
                              and only if, they are not the player who had the 
                              last action. If a player calls a bet and sees that 
                              he/she cannot win, he/she may fold his/her cards. 
                              Players who are curious about the folded hand may 
                              request a hand history to learn it.<br>
                              <br>
                              <b>Who wins?</b> <br>
                              In our poker room, as with all, "cards speak." That 
                              means our dealer will find the best five-card hand 
                              using the five (5) community cards on the board 
                              and the two (2) pocket cards in the player�s hand. 
                              The winner will be decided based on the universal 
                              poker hand rankings. <b><br>
                              <br>
                              <a id=rules name=rules></a>Texas Hold�em Blind Rules</b><br>
                              <img 
            src="images/rule.jpg" 
            align=absMiddle border="0" alt="Texas Hold'em Poker Rule">All players 
                              must pay for their blinds in full before they are 
                              allowed to get the button. Therefore, the player 
                              who had posted the small blind in the prior hand 
                              will receive the button on the next deal of any 
                              game. </p>
                              <ul>
                                <li> 
                                  <p>If in the event we have a <font color=red><b>new 
                                    player</b></font> to the game, then three 
                                    (3) actions will occur. 
                                  <ol>
                                    <li class=listText>If the new player is seated 
                                      left of the blind, then he/she may choose 
                                      to �post� the large blind or �wait.� If 
                                      the player does post, then his/her wager 
                                      is active. 
                                    <li class=listText>If the new player is seated 
                                      in the large blind, then he/she is treated 
                                      as such. 
                                    <li class=listText>If the player is seated 
                                      between the button and any blind, then he/she 
                                      must wait for the button to pass. </li>
                                  </ol>
                                </li>
                              </ul>
                              <p>
                              <b><br>
                              Missed Blinds rules and Procedures:</b> </p>
                              <ul>
                                <li class=listText><font color=red><b>Missed large 
                                  blind</b></font>. If a player misses the large 
                                  blind for any reason, then that player may not 
                                  play in any hands until the sum of all blinds 
                                  are made up. The game will place a �ML� button 
                                  in that seat to declare the missed large blind. 
                                  The game will ask the next active player to 
                                  the left to post the large blind for the hand. 
                                  If the missed player returns to the game before 
                                  the large blind comes back to his/her seat, 
                                  the player is required make up the sum of all 
                                  blinds. �Post all.� The small blind portion 
                                  is dead and must be put into the pot before 
                                  the hand is dealt. However, a player may avoid 
                                  posting dead by waiting for the large blind 
                                  to arrive at his/her seat naturally.<br>
                                <li class=listText><font color=red><b>Missed small 
                                  blind</b></font>. If a player misses the small 
                                  blind for any reason, then that player may not 
                                  play in any hands until the small blind is made 
                                  up. The game will place a �MS� button in that 
                                  seat to declare the missed small blind. <font color=red><b>The 
                                  game will ask the next active player to the 
                                  left of the large blind to post a large blind 
                                  for the hand as well</b></font>. The original 
                                  large blind player will �catch a break� and 
                                  get the button on the next hand. The player 
                                  who had missed the small cannot return until 
                                  after the button has passed. If the missed player 
                                  returns to the game before the large blind comes 
                                  back to his/her seat, the player is required 
                                  make up the small blind. The small blind is 
                                  dead and must be put into the pot before the 
                                  hand is dealt. However, a player may avoid posting 
                                  dead by waiting for the large blind to arrive 
                                  at his/her seat naturally. </li>
                              </ul>
                              <p>
                              <b>What is an all-in?</b> <br>
                              There are two definitions: </p>
                              <ol>
                                <li class=listText>A bet that a player makes that 
                                  uses up all of the chips he has remaining at 
                                  the table. For example, in a 15-30 game, a player 
                                  who only had $7 left at the table would be allowed 
                                  to use that $7 to call or bet. If there is only 
                                  one other player left in the hand at that point, 
                                  the betting has ended for that hand, and the 
                                  system will deal out the remainder of the cards 
                                  quickly and automatically. If there are three 
                                  or more players remaining in the hand when someone 
                                  runs out of money and goes all in, a �side pot� 
                                  is created, which is contested only by the players 
                                  who still have money. It is possible, in multi-way 
                                  hands, for more than one side pot to be created, 
                                  if more than one player runs out of money at 
                                  different times. If you are involved in a side 
                                  pot, you may win that, even if you cannot beat 
                                  the all-in player for the main pot.<br>
                                  <br>
                                  Note that even if you have more chips in your 
                                  account at the cashier, you cannot add more 
                                  money in the middle of a hand. We suggest, if 
                                  your funds run very low, that you add more chips 
                                  to those you have at the table before the next 
                                  hand begins.<br>
                                  <br>
                                  Finally, note that using an all-in in this manner�running 
                                  out of money in the middle of a hand�does not 
                                  use up one of the two all-ins you are allowed 
                                  per day, as explained below. Type 2 all-ins 
                                  are used up only by a failure to respond, not 
                                  by running out of money.<br>
                                  <br>
                                <li class=listText>If a player fails to act in 
                                  time�for example, if an emergency calls him 
                                  to another room in the house, or if he loses 
                                  his connection�he is treated as if he were all-in 
                                  for that hand. This feature protects the money 
                                  the player already has in the pot in case of 
                                  a bad connection or a home emergency. <br>
                                  <br>
                                  Players in All Horse Racing games are 
                                  given two �emergency� all-ins per 24 hour period. 
                                  If you use up your two all-ins, All Horse 
                                Racing 
                                  strongly suggests that you not play until you 
                                  have sent an email to All Horse Racing 
                                  support explaining why you used up two all-ins, 
                                  and requesting an all-in reset. If you play 
                                  with zero all-ins in your account, you could 
                                  lose the money you have in a pot if you lose 
                                  your connection. </li>
                              </ol>
                              <p><b><a id=games name=games></a>All Horse Racing 
                                Texas Hold�em Games</b></p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr class=tablesBgYellowDark valign=top align=middle bgcolor="#666666"> 
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$1-2</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$2-4</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$3-6</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $20</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $30</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $.50</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $1</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $1</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $1</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $2</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $3</b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>&nbsp;</p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$4-8</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$5-10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$6-12</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $40</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $50</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $60</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $2</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $2</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $3</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $4</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $5</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $6</b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>&nbsp;</p>
                              <table cellspacing=0 cellpadding=4 width="100%" border=1 bordercolor="#FFFFFF">
                                <tbody> 
                                <tr valign=top align=middle bgcolor="#666666"> 
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p class=textBlackBold><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$8-16</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p class=textBlackBold><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$10-20</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p class=textBlackBold><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF"><b>$15-30</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $80</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $100</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $150</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $4</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $5</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $7</b></font></p>
                                  </td>
                                </tr>
                                <tr valign=top align=left bgcolor="#CCCCCC"> 
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $8</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $10</b></font></p>
                                  </td>
                                  <td width="33%" class="basic_text_small"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $15</b></font></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="33%" class="basic_text_small" bgcolor="#FFB500"> 
                                    <p class=textBlackBold><b><font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#FFFFFF">$20-40</font></b></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="33%" class="basic_text_small" bgcolor="#CCCCCC"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Maximum 
                                      number of players: 10</b></font></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="33%" class="basic_text_small" bgcolor="#CCCCCC"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Minimum 
                                      Buy-in: $200</b></font></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="33%" class="basic_text_small" bgcolor="#CCCCCC"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Small 
                                      Blind: $10</b></font></p>
                                  </td>
                                </tr>
                                <tr>
                                  <td width="33%" class="basic_text_small" bgcolor="#CCCCCC"> 
                                    <p><font face="Verdana, Arial, Helvetica, sans-serif" size="1" color="#000000"><b>Large 
                                      Blind: $20</b></font></p>
                                  </td>
                                </tr>
                                </tbody> 
                              </table>
                              <p>&nbsp;</p>
        <p><br>
&nbsp;</p>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php	 	 include "inc_footer1.inc"; ?>
	</td>
  </tr>
 
</table>
</body>
</html>