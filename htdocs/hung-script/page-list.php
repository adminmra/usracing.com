<?php

	define('PATH_USR_PAGES', '/home/ah/usracing.com/smarty/templates/content');

	$ignore_folder = array(
		"2018-10-30-breeders-cup",
		"2018-10-31-1-breeders-cup",
		"2018-10-31-breeders-cup",
		"2018-10-30-bak",
		"famous-jockeys copy",
		"kentucky-derby_bkp20190319",
		"kentucky-oaks.bck.4-23-2019.live",
		"kentucky-derby-2017-promos",
		"preakness-stakes.bck.2019-05-02",
		"racetrack copy 1",
		"z.old",
	);
	
	scan_pages(PATH_USR_PAGES, 0);

	function scan_pages($root_path, $level) {
		global $ignore_folder;
		$dirs = loopfolder($root_path);
		$next_level = $level + 1;
		foreach ($dirs as $d) {
			//echo str_repeat("------", $level);
			//echo $d . '<br>';
			$page_name = basename($d);
			if(in_array($page_name, $ignore_folder)) {
				continue;
			}
			$vars = loop_var_xml($d);
			foreach ($vars as $key => $var) {
				//echo str_repeat("------", $level);
				//echo $var['xml']->title; echo '<br>';
				$url = str_replace(PATH_USR_PAGES, 'https://www.usracing.com', dirname($var['path']));
				$title = $var['xml']->title;

				//var_dump($var);
				echo '<a href="'.$url.'" target="_blank">'.$url.'</a>'; echo '<br>';
			}
			scan_pages($d, $next_level);
		}
	}

	function loop_var_xml($path) {
		//echo $path . "/".basename($path).".var.xml"; echo '<br/>';
		$result = array();
		$page_name = basename($path);
		$list = glob($path . "/".$page_name.".var.xml");
		foreach ($list as $l) {
			try {
				//$xml=simplexml_load_file($l) or die("Error: Cannot create object");
				$result[] = array(
					'path' => $l,
					//'xml' => $xml
				);
			} catch (Exception $e) {
			    //echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
		}
		return $result;
	}

	function loopfolder($path) {
		$dirs = glob($path . '/*', GLOB_ONLYDIR);
		return $dirs;
	}
?>