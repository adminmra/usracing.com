<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
<head>
<title>US Racing | Online Horse Betting</title>
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
<link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"  />
<meta name="description" content="US Racing provides online horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit."  />
<meta name="keywords" content="online horse betting, horse racing, bet on derby, otb, offtrack, kentucky derby betting, preakness stakes, belmont stakes, horse betting, odds"  />
<meta property="og:title" content="US Racing | Online Horse Betting"  />
<meta property="og:url" content="https://www.usracing.com/"  />
<meta property="og:image" content="https://www.usracing.com/img/best-horse-racing-betting-site.jpg"  />
<meta property="og:description" content="US Racing provides  horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit."  />
<link rel="stylesheet" type="text/css" href="/assets/plugins/rs/css/settings.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/assets/css/rs_v5.css" media="screen" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="shortcut icon" href="favicon.ico">
<meta name="apple-mobile-web-app-title" content="usracing.com">
<link rel="icon" href="/icon/apple-touch-icon-144x144.png" sizes="144x144" />
<link rel="apple-touch-icon" href="/icon/apple-touch-icon.png"/>
<link rel="apple-touch-icon" sizes="57x57" href="/icon/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="114x114" href="/icon/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="/icon/apple-touch-icon-72x72.png">    
<link rel="apple-touch-icon" sizes="144x144" href="/icon/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="60x60" href="/icon/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="120x120" href="/icon/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="76x76" href="/icon/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="152x152" href="/icon/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/icon/apple-touch-icon-180x180.png">

<!--ADD TO HOME STARTS-->

<link rel="stylesheet" type="text/css" href="/assets/css/addtohomescreen.css">
<script src="/assets/js/addtohomescreen.js"></script>
<script>
addToHomescreen({
   skipFirstVisit: false,
   maxDisplayCount: 1
});
</script>

<!--ADD TO HOME ENDS-->		

<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/style.css?v1">
<link rel="stylesheet" type="text/css" href="/assets/css/custom_me.css">

<link rel="stylesheet" type="text/css" href="/assets/css/app.css">

<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css">

<link rel="stylesheet" type="text/css" href="/assets/css/home_v4.css?v=1">

<link rel="stylesheet" type="text/css" href="/assets/css/plugins.css?v=2">
<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->

<script async type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script>
<script async type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<!-- hide nav script -->

<!-- <script>
var senseSpeed = 35;
var previousScroll = 0;
$(window).scroll(function(event){
   var scroller = $(this).scrollTop();
   if (scroller-senseSpeed > previousScroll){
      $("#nav.is_stuck").filter(':not(:animated)').slideUp();
   } else if (scroller+senseSpeed < previousScroll) {
      $("#nav.is_stuck").filter(':not(:animated)').slideDown();
   }
   previousScroll = scroller;
});
</script>-->


<script src="/assets/js/index-kd.js"></script>
<script src="/assets/js/tinysort.js"></script>


<!--<script type="text/javascript" src="/assets/plugins/jquery-1.10.2.min.js"></script>-->
<!--[if lt IE 9]>
	 <link rel="stylesheet" type="text/css" href="/assets/plugins/iealert/alert.css" />
     <script type='text/javascript' src="/assets/js/html5.js"></script>
     <script type="text/javascript" src="/assets/plugins/iealert/iealert.min.js"></script>
     <script type="text/javascript">$(document).ready(function() { $("body").iealert({  closeBtn: true,  overlayClose: true }); });</script>
<![endif]-->

<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>

<script type="text/javascript" src="assets/plugins/countdown/jbclock.js" ></script>
<script type="text/javascript" src="assets/plugins/rs/js/rs.plugins.min.js" ></script>
<script type="text/javascript" src="assets/plugins/rs/js/rs.revolution.min.js" ></script>
<script type="text/javascript" src="assets/plugins/rs/js/rs.revolution.init.js" ></script>
<script type="text/javascript" src="assets/js/index.js" ></script>
</head><body class="front index-kd">
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->


<!-- test 1 -->
<!-- Top -->
<div id="top" class="top">
 <div class="container">
	 <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>

  <a class="logo logo-lrg" href="/" ><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting"></a>
 <a class="logo logo-sm " href="/" ><img src="/img/usracing-sm.png" alt="Online Hose Betting"></a>
  <!---->
    <div class="login-header">
    <!--
    <a class="btn btn-sm btn-default loginToggle collapsed" data-toggle="collapse" data-target=".login-responsive-collapse" rel="nofollow"><span>Login</span><i class="fa fa-angle-down"></i></a>
    -->

    <a href='//www.betusracing.ag/login?ref=loginupgrade' class="btn btn-sm btn-default" id="login_button">Login</a>

    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref=index" rel="nofollow" >
        <span>Sign Up</span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>  

 </div>
</div><!--/top-->

<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">


	<!-- Toggle NAV 
	<div class="navbar-header">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    <a id="or" class="navbar-toggle collapsed"><span>EXPLORE</span><i class="glyphicon glyphicon-remove"></i></a>
	</div>-->

    <!-- Nav Itms -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav nav-justified">
    <li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
      
   
    <li><a href="/odds" >Today's Entries</a></li>
        <li><a href="/results"  >US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
       <li><a href="/breeders-cup/challenge" >Breeders' Cup Challenge</a></li>
    
                    <li><a href="/racetracks"  >Racetracks</a></li>
            
     <li><a href="/leaders/horses"  >Top Horses</a></li>
      <li><a href="/leaders/jockeys"  >Top Jockeys</a></li>
       <li><a href="/leaders/trainers"  >Top Trainers</a></li>
             
       
      
     
     
                  </ul>
</li>


<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"> Odds<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">   
   <li><a href="/odds"  >Today's Horse Racing Odds</a></li>
<li><a href="/kentucky-derby/odds">2019 Kentucky Derby Odds</a></li> 

<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>
<!--<li><a href="/melbourne-cup"  >Melbourne Cup Odds</a></li> -->
       
      
  
      
  <li><a href="/bet-on/triple-crown">Triple Crown Odds</a></li>
 <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li> 
<li><a href="/grand-national"  >Grand National Odds</a></li>
			
            						
 	

			<li><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>

											
     																		 


  </ul>
</li>
<!--Horse Racing News ================================================ -->   
<li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" > News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>




 
	




	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>





	    

      
      
      
    
      
      
      
      
     </ul>
</li>

     
     





<!-- How to Bet ================================================ -->   
     
     <li class="dropdown">     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >How to Bet<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/how-to/bet-on-horses">How to Bet on Horses </a></li>
<li><a href="/news/horse-betting-101/">Handicapping Tips</a></li>
<!-- <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li>  -->   
<li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity" >Betting with Bitcoin</a></li>     
    
     
      
     </ul>
</li>

     
     


<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Promos <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
     <li><a href="/rebates">8% Rebates</a></li>
     <li><a href="/promos/cash-bonus-10">10% Signup Bonus</a></li>
 <li><a href="/promos/cash-bonus-150">$150 Member Bonus</a></li>
     
           </ul>
</li>
<li class="dropdown">
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="true" >Learn More <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
	  
    <li><a href="/about" >About Us</a></li> 
    <li><a href="/awards" >Award Winning News</a></li>
        <li><a href="/best-horse-racing-site" >Best Horse Racing Sites</a></li>
     <li><a href="/mobile-horse-betting">Mobile Betting</a></li>
     <li><a href="/racetracks">Racetracks</a></li>
		 <li><a href="/rebates">Rebates</a></li>
		 <li><a href="/support">Contact Us</a></li>
                 
        
        
	 <li><a href="/getting-started" >Getting Started</a></li>
    <li><a href="/signup/?m=Join-Today">Join a Racebook!</a></li>
          </ul>
</li>
	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->

<!-- test 2 -->

 <!--=== Home Slider ===-->
<div class="sliderContainer fullWidth clearfix margin-bottom-30">
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>

				


				
<!-- slide KENTUCKY DERBY -->    
<!-- end slide -->



<!-- end slide -->	
				
               
 					<li data-transition="fade" data-slotamount="9" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
										<img src="img/kentuckyderby/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Kentucky Derby Betting">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"> Online Horse Betting 					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"> Bet on  Over 200 Tracks!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup/?ref=slide-rebates" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Sign Up Now
					</a></li>
				
<!-- end slide -->	
<!-- slide YOUNG GUYS - REBATES-->  
             
 					<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/online-horse-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Rebates">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Rebates Paid Daily
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"><!--You Play. We Pay.-->Up to 8%!  The Most Anywhere!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup?ref=slide-200-tracks" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Sign Up Now
					</a></li>
					
<!-- end slide -->
<!-- slide HAPPY MAN PARTY IMAGE -->   
         
					<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" >
					<!-- IMAGE -->
					<!-- <img src="/img/online-horseracing-bonus.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus">--><!-- slide HAPPY MAN --> 
                    <img src="/img/heros/horse-racing-bonus2.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus">
					<!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--$150-->10% Instant Cash Bonus
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-10"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">and Qualify for Another $150! <!--Cash Back and Bonuses from US Racing-->
					</div>
                    
                    <!-- Button -->

					<a href="/signup?ref=slide-instant-cash" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-star left"></i> Sign Up Now
					</a>                    
                    <!-- Learn More Button 
					
					<a href="#" rel="nofollow" class="tp-caption sfl fadeout btn btn-txt"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="130"
						data-speed="400"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
                        data-toggle="modal" data-target="#bonus"
						style="z-index: 4; font-size:22px;">Learn More <i class="fa fa-question-circle"></i>
					</a>-->
					</li>
					
<!-- end slide -->





</ul>
<!--<div class="tp-bannertimer"></div>-->
</div><!-- end/tp-banner-container -->
</div><!-- end/tp-banner -->
</div><!-- end/SliderContainer -->


<!-- Modal1 -->
				 

    <section class="bs-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index/friends.png" alt="" class="bs-friends_img img-responsive"></div>
      </div>
    </section>
    <section class="bs-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bs-testimonial">
            <figure class="bs-testimonial_figure"><img src="/img/index/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="bs-testimonial_img"></figure>
            <div class="bs-testimonial_content">
              <p class="bs-testimonial_p">At BUSR, in addition to all the traditional horse bets, you can bet on individual horse match-ups, sports and awesome casino games on your phone!</p><span class="bs-testimonial_name">Derek Simon</span><span class="bs-testimonial_profile">Senior Editor and  Handicapper  at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
<!------------------------------------------------------------------>      
     <!------------------------------------------------------------------>    
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
                 <h1 class="kd_heading">Horse Betting</h1>
          <h3 class="kd_subheading">With the Most Betting Options Anywhere</h3>
          <p>Bet in the comfort of you home or on the go with BUSR's mobile betting.  Whether you want to bet on horses or sports, you can bet easily with your iPhone. <p> Waiting at the bar for your friends to arrive?  How about playing a few hands of blackjack or roulette!  BUSR - super easy and super fun. </p><a href="/odds?ref=index" class="bs-kd_btn">SEE THE LIVE ODDS</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
    
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-horse-betting-rebates.png" alt="Horse Betting Rebates Icon" class="bs-card_icon">
          <h2 class="bs-card_heading">Up to 8% Horse Racing Rebates</h2>
          <h3 class="bs-card_subheading">Paid Daily Into Your Account</h3>
          <p>Every day when you log into your account you account you will find an 8% or 5% rebate on your of your exotic wagers and a 3% rebate on Win, Place and Show in your account from yesterday's bets!</p>
          <p>You can only win with BUSR!</p><a href="/rebates?ref=index" class="bs-card_btn">Learn More About Rebates</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/horse-betting-rebates.jpg" alt="Horse Betting Rebates" class="bs-card_img"></div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <figure class="bs-testimonial-blue_figure"></figure>
          <div class="bs-testimonial-blue_content">
            <p class="bs-testimonial-blue_p">When it comes to  horse betting, BUSR has you covered. They provide many unique betting options, more than anybody else.</p>          </div>
        </blockquote>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/triple-crown-blackjack-tournament.jpg" alt="Belmont Stakes Blackjack" class="bs-card_img">
        </div>
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-triple-crown-blackjack-tournament.png" alt="Belmont Stakes Tournament" class="bs-card_icon">
          <h2 class="bs-card_heading">Enjoy Over 150 of Your Favorite Casino Games</h2>
          <p>At BUSR you can play video slots, Blackjack, Roulette, Video Poker, Table and Lottery Games, Keno  fully 3D Poker with stunning graphics and 3D animation.</p><p>Play now and enjoy jackpots up to $63,000!</p><a href="/signup??ref=index-Casino-Games" class="bs-card_btn">Play Now</a>
          
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
     
    
    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref=index" rel="nofollow" class="bs-bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section>      
    
    <!------------------------------------------------------------------>  

    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-casino-rebate.png" alt="US Racing Casino Rebate" class="bs-card_icon">
          <h2 class="bs-card_heading">50% Casino Cash Back</h2>
          <h3 class="bs-card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/promos/casino-rebate?ref=index" class="bs-card_btn">Find Out More</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/casino-rebate.jpg" alt="Casino Rebate" class="bs-card_img">
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
      <section class="bc-racetracks">
      <div class="container">
        <h2 class="bc-racetracks_title">
           
          Choose from <strong>211 </strong>different racetracks
        </h2>
        <ul class="bc-racetracks_list">
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-thoroughbred-racing.png" alt="Thoroughbred Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Thoroughbred</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-harness-racing.png" alt="Harness Racing Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Harness</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-quarter-horse-racing.png" alt="Quarter Horse Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Quarter Horses</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-international-horse-racing.png" alt="International  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">International</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-greyhound-racing.png" alt="Greyhound  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Greyhound</h4>
              </div></a></li>
        </ul>
      </div>
    </section>
    
  <!------------------------------------------------------------------>   
    
    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/mobile-horse-betting.png" alt="Mobile Horse Betting" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-mobile-horse-betting.png" alt="Mobile Horsed Betting Icon" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the go!</h2>
          <p>Take all the races with you! All tracks are available on phone or tablet at the track, at home, everywhere.</p> <p>Are you out at the bar with your friends? Bet on the game any time, anywhere.  Or, while waiting for your friends to arrive,  how about playing a few hands of blackjack or roulette! <p> BUSR - super easy and super fun.</p><a href="/mobile-horse-betting?ref=index" class="bc-card_btn-red">See Your Mobile Betting Options</a>
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------> 
    
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-online-sports-betting.png" alt="" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on Sports</h2>
          <h3 class="bc-card_subheading">Anytime and Anywhere!</h3>
          <p>Not only can you bet on horses but  also sports with:

    <br><br>Live In-Game Betting
     <br>Best Player and Team Propositions
   <br> Complete Baseball Betting Odds
   <br>Fights, Politics and Entertainment Bets
 <br>Bet Anywhere With Your Mobile Phone


</p><a href="/signup?ref=index" class="bc-card_btn-red">Bet on sports</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/online-sports-betting.jpg" alt="Online Sports Betting" class="bc-card_img"></div>
      </div>
    </section>

    
 <!------------------------------------------------------------------>    
    
    
    
    
    <!------------------------------------------------------------------>  
   
   
  
    
    <!------------------------------------------------------------------>  
    <section class="bs-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bs-payouts_heading">
               
              fast payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bs-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
    <section class="bc-card bc-card--hre">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-horse-racing-experts.png" alt="Horse Racing Experts" class="bc-card_icon">
          <h2 class="bc-card_heading">Horse Racing Experts</h2>
          <p>US Racing's Authors and Handicappers will make you a better player.  Get the latest news, odds, race reports and betting advice from our experienced group of horse racing writers.</p><a href="/news" class="bc-card_btn-red">Read the Latest </a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/horse-racing-experts.png" alt="Expert Handicappers" class="bc-card_img"></div>
      </div>
    </section>

    
    <!------------------------------------------------------------------> 
      <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Horse Betting Action<br> at BUSR</h2>
            <a href="/signup?ref=index" rel="nofollow" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>  
    

    
    
    </section>


<!-- === Footer ================================================== -->
<div class="footer">
<div class="container">

    <div class="row">

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/off-track-betting">Off Track Betting</a></li>
          <li><a href="/online-horse-wagering" >Online Horse Wagering</a></li>
          <li><a href="/odds">Horse Racing Odds</a></li>
          <li><a href="/bet-on-horses">Bet on Horses</a></li>
         <!-- <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li> -->
          <li><a href="/harness-racing">Harness Racing</a></li>
          <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
          <li><a href="/pegasus-world-cup/odds">Pegasus World Cup</a></li>
          <li><a href="/texas">Texas Horse Betting</a></li>
                    <li><a href="/royal-ascot">Royal Ascot Betting</a></li>

        <li><a href="/dubai-world-cup">Dubai World Cup Betting</a></li> 


<!--<li><a href="/santa-anita" >Santa Anita Horse Racing</a></li>
<li><a href="/keeneland" >Keeneland Horse Racing</a>  </li>
<li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
<li><a href="/delta-downs" >Delta Downs Horse Racing</a></li>
<li><a href="http://www.pickthewinner.com" >Pick the Winner</a>  </li>
<li><a href="http://www.breederscupbetting.com" >Breeders Cup Betting</a>  </li>-->
    </li>
    <li><a href="/breeders-cup/betting" >Breeders' Cup Betting</a>
     <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a>
                 </ul>
      </div> <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
	         <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
          <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
          <li><a href="/kentucky-derby/future-wager">Kentucky Derby Future Wager</a></li>
          <li><a href="/bet-on/kentucky-derby">Bet on Kentucky Derby</a></li>
          <li><a href="/road-to-the-roses">Road to the Roses</a></li>
          <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
          <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
          <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
          <li><a href="/twin-spires">Twin Spires Betting</a></li>



        </ul>
      </div>
        <!--/col-md-3-->
	        <div class="col-md-3 margin-bottom-30">
	        <div class="headline">
	          	<h3>Triple Crown</h3>
	        </div>
		        <ul class="list-unstyled margin-bottom-20">
		        <!--	<li><a href="/kentucky-derby" >Kentucky Derby</a></li>-->
		        	<li><a href="/preakness-stakes" >Preakness Stakes</a></li>
		        	<li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>
		        	<li><a href="/preakness-stakes/betting" >Preakness Stakes Betting</a></li>
		        	<li><a href="/preakness-stakes/odds" >Preakness Stakes Odds</a></li>
					<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>
		          	<li><a href="/preakness-stakes/winners" >Preakness Stakes Winners</a></li>
		           <li><a href="/belmont-stakes" >Belmont Stakes</a></li>
		           <li><a href="/bet-on/belmont-stakes" >Bet on Belmont Stakes </a></li>
		        	<li><a href="/belmont-stakes/betting" >Belmont Stakes Betting</a></li>
		        	<li><a href="/belmont-stakes/odds" >Belmont Stakes Odds</a></li>
					<!--<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
		          	<li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> -->
		          	<li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
		        </ul>
			</div>
      <!--/col-md-3-->



      <!--/col-md-3-->
				   <!--<div class="col-md-3 margin-bottom-30">
				        <div class="headline">
				          <h3>US Horse Racing</h3>
				        </div>
				        <ul class="list-unstyled margin-bottom-20">
				        	<li><a href="/about" >About Us</a></li>-->

				          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
				       <li><a href="#" >Our Guarantee</a></li>
				          	<li><a href="/faqs" >FAQs</a></li>
				            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

				<!--<li><a href="/signup/" >Sign Up Today</a></li>
				        </ul>
						</div> -->
      <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>

        <ul class="social-icons">
          <li><a href="https://www.facebook.com/betusracing" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>
          <li><a href="https://twitter.com/betusracing" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
          <li><a href="https://plus.google.com/+Usracingcom" data-original-title="Google+"  title="Google+" class="social_google" rel="publisher"></a></li>
          <!--
          <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
          -->
        </ul>
        <br>   <br>
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
        	<li><a href="/about" >About Us</a></li>
          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
          	<li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
<li><a href="/signup?ref=index" rel="nofollow" >Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
<!--<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>-->

        </ul>



      </div> <!--/col-md-3-->

</div><!--/row-->



<div class="row friends">
	<center>Proudly featured on:
<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center>


     <!--
   <div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>

        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
-->
        </div>
</div>
<!-- /row/friends -->


</div><!--/container-->
</div><!--/footer-->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand"><br><p>US Racing is not a racebook or ADW, and does not accept or place wagers of any type. This website does not endorse or encourage illegal gambling. All information provided by this website is for entertainment purposes only. This website assumes no responsibility for the actions by and makes no representation or endorsement of any activities offered by any reviewed racebook or ADW. Please confirm the wagering regulations in your jurisdiction as they vary from state to state, province to province and country to country.  Any use of this information in violation of federal, state, provincial or local laws is strictly prohibited.</p>
        
        <p>Copyright 2018  <a href="/">US Racing</a>, All Rights Reserved.  </span>  | <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/disclaimer" rel="nofollow">Disclaimer</a> | <a href="/responsible-gaming" rel="nofollow" >Responsible Gambling</a> | <a href="/preakness-stakes/betting" >Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting" >Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting" >Kentucky Derby Betting</a>    </div>
    </div><!--/row-->

  </div><!--/container-->
</div><!--/copyright-->



<div class="wp-hide">
<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
  	App.init();
  	App.initFancybox();
  	App.initRoyalSlider();
    try{
      Index.initIndex();
    }catch(e){ console.log( "assets/js/index.js -> does not imported" ); }
  });
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
ga('require', 'displayfeatures');
ga('send', 'pageview');
ga('require', 'linker');
ga('linker:autoLink', ['betusracing.ag', 'bet.usracing.com'] );
</script>
<script async src='//www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->





<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1710529292584739', {
em: 'insert_email_variable'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1710529292584739&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Bing Ads RP  -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4047476"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


<!--  Unbounce --->

<script src="//d6782401e72a400fba6ff09252aa6316.js.ubembed.com" async></script>
	
<!-- / Unbounce -->

<!-- Lucky Orange-->
<script> window.lo_use_ip_lookups = true; </script>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 43153;
(function() {
var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
})();
</script>
<!-- End Lucky Orange-->



</div></body>
</html>
