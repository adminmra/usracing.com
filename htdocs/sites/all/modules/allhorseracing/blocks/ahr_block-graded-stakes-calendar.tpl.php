<?php	 	
// $Id$

/*
 * @file
 */
$template_path = base_path() . drupal_get_path('theme', 'ahr');
$zebra = TRUE; $idx = 0;
?>
<div id="graded">
  <div class="boxheader">
    <div class="boxicon"><img src="<?php print $template_path .'/images/gradedicon.gif' ?>" alt="graded stakes icon"/></div>
    <div><h1>Graded Stakes Races</h1></div>
  </div>

  <div id="graded-content">
      <?php
      foreach ($races as $race) {
        if ($idx > 5) {
        	break;
        }
		/*if ($idx == 0) {
			$idx++;
        	continue;
        }*/
        $class = ($zebra)? 'stakes-blue' : 'stakes-yellow';
        $zebra = !$zebra;

        print '<div class="'. $class .'" id="grade-stake-'. $idx .'">'."\n". $race .'</div>';
        $idx++;
      }
    ?>
  </div>
  <div class="boxfooter"><span class="bold"><a href="/races/graded_stakes_schedule">More Graded Stakes Races</a></span></div>
</div>