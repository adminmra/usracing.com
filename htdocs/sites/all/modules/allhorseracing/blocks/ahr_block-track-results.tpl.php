<?php	 	
// $Id$

/*
 * @file
 */
 require_once "/var/www/vhosts/gohorsebetting.com/httpdocs/dbconnect.inc";
 require_once $_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/minitrack.php";
?>
<!-- Start TrackResults -->
<div id="track-results-block" class="listings">
  <ul class="tabs"><!-- the tabs -->
    <li>
      <a href="#"><span class="b1"><?php print t('Todays&#39; Tracks') ?></span>
      <span class="b1-border"></span></a>
    </li>
    <li>
      <a href="#"><span class="b2"><?php print t('Results') ?></span>
      <span class="b2-border"></span></a>
    </li>
  </ul><!-- end tabs -->
  <div class="panes">
    <div class="pane1">
      <div class="items1">
        <?php print OpenTrackTable(); ?>
      </div><!-- End Items1 -->
      <div class="boxfooter"><span class="bold"><?php print l(t('View All Tracks'), 'tracks') ?></span></div>
    </div><!-- End Pane1 -->
    <div class="pane2">
      <div class="items2">
        <?php print CloseTrackTable(); ?>
      </div><!-- End Items2 -->
      <div class="boxfooter"><span class="bold"><?php print l(t('View All Results'), 'tracks') ?></span></div>
    </div><!-- End Pane2 -->
  </div><!-- /.panes -->
</div><!-- end listings -->
<!-- End TrackResults -->
