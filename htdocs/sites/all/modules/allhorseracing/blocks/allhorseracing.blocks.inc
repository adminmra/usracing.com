<?php
// $Id$

/*
 * @file
 */


/**
 * Fetching racing news items from external web service
 */
function template_preprocess_ahr_block_racing_news(&$vars) {
$isDead=0;
 db_set_active('allhorse');
$results = db_query("select * from outguard where id = '6'");		
$results_msg = db_query("select * from outgurd_message where id='1'");
db_set_active('default');
 $row = db_fetch_array($results);
 $rows[] = $row;
 if($row['status']=='dead')
 {
  	$row_msg = db_fetch_array($results_msg);	
	//print $row_msg["default_msg"];
	$vars['articles'] = array();
	$vars['articles'][0] = $row_msg["default_msg"];
	$vars['articles'][1] = "dead";
	$isDead=1;
 }
 else
 {

  drupal_add_js(drupal_get_path('module', 'allhorseracing').'/blocks/feature_photos.js');

  //Get list of articles
  $articles = ahr_get_news_articles(4, 1);
  //kpr($articles);

  $vars['articles'] = array();
  $first = TRUE;

  foreach ($articles as $aid => $article) {
    $vars['articles'][$aid] = theme('ahr_racing_news_article_teaser', $article, $first);
    $first = FALSE;
  }
  }//else switch
}

/**
 * Fetching racing news items from external web service
 */
function template_preprocess_ahr_block_racing_news_mini(&$vars) {
 db_set_active('allhorse');
$results = db_query("select * from outguard where id = '6'");		
$results_msg = db_query("select * from outgurd_message where id='1'");
db_set_active('default');
 $row = db_fetch_array($results);
 $rows[] = $row;
 if($row['status']=='dead')
 {
  	$row_msg = db_fetch_array($results_msg);	
	//print $row_msg["default_msg"];
	$vars['articles'] = array();
	$vars['articles'][$aid] = $row_msg["default_msg"];
 }
 else
 {
  //Get list of articles
  $articles = ahr_get_news_articles(4, 1);
  //kpr($articles);

  $vars['articles'] = array();

  foreach ($articles as $aid => $article) {
    $vars['articles'][$aid] = theme('ahr_racing_news_article_teaser', $article);
  }
  }//else switch
}



/**
 * Fetching contents for Graded Stakes Races block from external db
 */
function template_preprocess_ahr_block_graded_stakes_race_calendar(&$vars) {
  $races = ahr_get_graded_stakes(TRUE,FALSE);

  $vars['races'] = array();
  $idx = 0;
  $limited_reached = 0;
  foreach($races as $id => $race) {
    //$date = date_make_date($row['addedon']);
    //$added_on = format_date($race['date'], 'custom', 'M j');
	$added_on = date('M j',$race['date']);

    $output = '<div class="graded-date"><span class="bold">'. $added_on .'</span></div>' ."\n";

    $output .= '<div class="graded-race">' ."\n";
	$idx = 0;
    foreach ($race['locations'] as $loc) {
	if ($idx > 10) {
		//$limited_reached = 1;
		break;
	}
      $output .= '<p><span class="bold">'. $loc['location'] .':</span> '. ahr_abbr_html($loc['details'], 45) .'</p>'."\n";
	$idx++;
    }
    $output .= '</div>'. "\n";
    $vars['races'][$id] = $output;
    if ($limited_reached == 1) {
		break;
    }
  }

}


/**
 * Today's Tracks and Results tabbed block
 */
function template_preprocess_ahr_block_track_results(&$vars) {
  $max_rows = 8;

  //$vars['open_tracks_table'] = theme('todays_open_tracks_table', $max_rows);
  //$vars['closed_tracks_table'] = theme('todays_closed_tracks_table', $max_rows);
	$vars['open_tracks_table'] ="";
	$vars['closed_tracks_table'] ="";
  allhorseracing_add_tabs();
  
  $js = <<<EOF
    $(function() {
      // setup ul.tabs to work as tabs for each div directly under div.panes
      $("#track-results-block ul.tabs").tabs("div.panes > div");
    });
EOF;


  drupal_add_js($js, 'inline', 'footer');
}


/**
 * Return a themed ad of type ad_countdown.
 *
 * @param @ad
 *   The ad object.
 * @return
 *   A string containing the ad markup.
 */
function theme_ahr_countdown_ad($ad) {
  if (isset($ad->aid) && (isset($ad->filepath) || isset($ad->remote_image))) {
    $output = '<div class="image-advertisement" id="ad-'. $ad->aid .'">';
    if (isset($ad->url) && !empty($ad->url)) {
      $image = theme('ad_image_image', !empty($ad->remote_image) ? $ad->remote_image : $ad->filepath, check_plain($ad->tooltip), check_plain($ad->tooltip));
      $output .= l($image, $ad->redirect .'/@HOSTID___', array('attributes' => ad_link_attributes(), 'absolute' => TRUE, 'html' => TRUE));
    }
    else {
      $output .= theme('ad_image_image', !empty($ad->remote_image) ? $ad->remote_image : $ad->filepath, check_plain($ad->tooltip), check_plain($ad->tooltip));
    }
    $output .= '</div>';
    return $output;
  }
}
