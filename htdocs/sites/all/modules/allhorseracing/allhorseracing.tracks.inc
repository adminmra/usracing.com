<?php
////error_reporting(0);

// $Id$

/*
 * @file
 */

function getTrackDataFeedSingleton() {
  static $service;

  if (!$service) {
    $service = new TrackDataFeed();
  }

  return $service;
}

function getTrackBlockStatus() {
  static $message;

  if (!$message) {
    db_set_active('allhorse');

    $status = db_result(db_query("SELECT status FROM {outguard} WHERE id='2'"));
    $message = db_result(db_query("SELECT user_conf_msg FROM {create_message} where id='104'"));

    db_set_active('default');

    $message = ($status == 'dead') ? $message : TRUE;
  }

  return $message;
}



function theme_todays_open_tracks_table($limit = 8) {
  $status = getTrackBlockStatus();

  $table_open = <<<EOF
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <thead>
            <tr class="tracks-titlebar">
              <th class="width50perc">TRACKS</th>
              <th><span title="Minutes To Post">MTP</span></th>
              <th>RACE</th>
              <th class="width67"></th>
            </tr>
          </thead>
          <tbody>
            <tr class="tracks-spacer">
              <td colspan="4" ></td>
            </tr>
EOF;

  $table_close = <<<EOF
          </tbody>
        </table>
EOF;

  if ($status === TRUE) {
    $DataFeed = getTrackDataFeedSingleton();
    $results = $DataFeed->getTracks('open');
    $template_path = drupal_get_path('theme', 'ahr');

    $i=0;
    foreach ($results as $result) {
      if (strpos($track["TRACK"], "Miami Jai-Alai") !== FALSE){
        continue;
      }

      $i++;
      $tracks[$result['ID']] = $result;

      if ($i >= $limit) {
        break;
      }
    }

    if ($tracks) {
      $output = '';

      foreach ($tracks as $id => $track) {
	  //$link = l($track['TRACK'], 'tracks/'. $id, array('query' => array('racename' => $track['TRACK']), 'title' => str_replace("'","", $track["TRACK"]) . ' Results for Race ' . $track["RACE"] ));
	  $link = '<a href="/racetrack/?id=' . $id . '&race=' . $track['RACE'] . '&racename=' . $track['TRACK'] . '" title="' . str_replace("'","", $track["TRACK"]) . '">' . $track['TRACK'] . '</a>';
        $race = $track['RACE'];
        $mtp = $track['MTP'];
        $bet_now_btn = '<img src="'. url($template_path .'/images/betnow-default.gif') .'" onmouseover="this.src=\''. url($template_path .'/images/betnow-hover.gif') .'\'" onmouseout="this.src=\''. url($template_path .'/images/betnow-default.gif') .'\';" alt="Bet Now" />';
        $bet_now = l($bet_now_btn, 'racebook/', array('html' => TRUE, 'title' => t('Bet Now!'), 'style' => 'color:#000;text-decoration:none;'));
        $output .= <<<EOF
        <tr id="track-$id" class="tracks-cell">
          <td class="width50perc">$link</td>
          <td>$mtp</td>
          <td>$race</td>
          <td class="width67">$bet_now</td>
        </tr>
EOF;
      }
    }
    else {
      $output = '<tr><td colspan="4">'. t('No Open Tracks.  Check Back Later.') .'</td></tr>';
    }
  }
  else {
    $output = '<tr><td colspan="4"><strong>'.stripslashes($message['user_conf_msg']).'</strong></td></tr>';
  }

  return $table_open . $output . $table_close;
}

function theme_todays_closed_tracks_table($limit = 8) {
  $status = getTrackBlockStatus();

  $table_open = <<<EOF
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <thead>
            <tr class="tracks-titlebar">
              <th class="width50perc">TRACKS</th>
              <th><span title="Minutes To Post">MTP</span></th>
              <th>RACE</th>
            </tr>
          </thead>
          <tbody>
            <tr class="tracks-spacer">
              <td colspan="3" ></td>
            </tr>
EOF;

  $table_close = <<<EOF
          </tbody>
        </table>
EOF;

  if ($status === TRUE) {
    $DataFeed = getTrackDataFeedSingleton();
    $results = $DataFeed->getTracks('closed');
    $template_path = drupal_get_path('theme', 'ahr');

    $i=0;
    foreach ($results as $result) {
      if (strpos($track["TRACK"], "Miami Jai-Alai") !== FALSE){
        continue;
      }

      $i++;
      $tracks[$result['ID']] = $result;

      if ($i >= $limit) {
        break;
      }
    }

    if ($tracks) {
      $output = '';

      foreach ($tracks as $id => $track) {
        //$link = l($track['TRACK'], 'tracks/'. $id, array('query' => array('race' => $track['RACE'], 'racename' => $track['TRACK']), 'title' => str_replace("'","", $track["TRACK"]) . ' Results for Race ' . $track["RACE"] ));
	  $link = '<a href="/racetrack/?id=' . $id . '&race=' . $track['RACE'] . '&racename=' . $track['TRACK'] . '" title="' . str_replace("'","", $track["TRACK"]) . '">' . $track['TRACK'] . '</a>';

        $race = $track['RACE'];
        $mtp = $track['MTP'];

        $output .= <<<EOF
        <tr id="track-$id" class="tracks-cell">
          <td class="width50perc">$link</td>
          <td>$mtp</td>
          <td>$race</td>
        </tr>
EOF;
      }
    }
    else {
      $output = '<tr><td colspan="3">'. t('No Closed Tracks.  Check Back Later.') .'</td></tr>';
    }
  }
  else {
    $output = '<tr><td colspan="3"><strong>'.stripslashes($message['user_conf_msg']).'</strong></td></tr>';
  }

  return $table_open . $output . $table_close;
}


class TrackDataFeed {

  /**
   * Class constructor
   */
  function TrackDataFeed() {
    $this->host = '208.158.1.151';
    $this->port = 2766;
    $this->streamSize = 8192;

    $this->CacheTime = 60*25;
  }


  /**
   * Try to init a new socket
   * @return socket connection
   */
  function connect() {
    $this->sh = fsockopen($this->host, $this->port);
    socket_set_timeout ($this->sh,2);
    return $this->sh;
  }

  /**
   *
   * @return unknown_type
   */
  function closeConnection(){
    $sh = $this->sh;
    fclose($sh);
  }


  /**
   * Send command to external data feed service
   * @param $command
   * @param $remove_first
   * @return unknown_type
   */
  function sendCommand($command,$remove_first=true) {
    $this->connect();

    $sh = $this->sh;

    $data = '';

    if($sh && !is_numeric($sh)) {

      fwrite($sh, $command."\r\n");
      # allocate the incoming data stream to the variable $data
      //echo $command;
      if($remove_first) {
        $current_line = fgets($sh); // Remove First Line
      }

      stream_set_blocking($sh, TRUE);
      stream_set_timeout($sh,2);
      $info = stream_get_meta_data($sh);

      while (false !== ($char = fgetc($sh))) {
        $data.=$char;
      }
      /*
       while ((!feof($sh)) && (!$info['timed_out'])) {
       $data .= fgets($sh, 4096);
       $info = stream_get_meta_data($sh);
       ob_flush;
       flush();
       } */

      fclose($sh);
    }
    else {
      //print_r($info);
      //$data .= fgets($sh, 4096);
    }

    $data = str_replace("\r\n","\n",$data);
    //echo "DATA: ".$data."<br/><br/>";

    // REQUEST TIMED OUT.  SERVER UNDER HEAVY LOAD OR NO DATA RETURNED FROM SERVER
    if ($info["timed_out"]==1 && strlen($data)==0) {
      return false;
    }

    // FEEDS DOWN (RETURNING EMPTY RESULT).  HAPPENS AROUND MIDNIGHT
    if (strlen(str_replace("\n","",$data))=="") {
      return false;
    }

    return $data;
  }


  /**
   * Get track data (uses drupal cache)
   * @param $return
   * @return unknown_type
   */
  function getTracks($return = 'all') {
    $OpenTracks = array();
    $ClosedTracks = array();
    $AllTracks = array();
    $data = '';

    $cid = 'ahr_track_data';;
    $cached = cache_get($cid);

    if (!$cached) {
      $data = $this->sendCommand("list performances available");    // Returns FALSE if Command Timed Out

      if($data) {
        cache_set($cid, $data, 'cache', time() + $this->CacheTime);
      }
      else {
        return FALSE; // request timed out, or server updating at midnight
      }
    }
    else {
      $data = $cached->data;
    }

    $Tracks = explode("\n",$data); // Create Array from Each New Line
    array_pop($Tracks); // Remove Last Empty Item
    array_pop($Tracks); // Remove Last Empty Item
    //print_r($Tracks);
    $this->Tracks = $data;

    //Loop through each row...
    foreach ($Tracks as $Track) {
      //Break apart each property in the row...

      $trackProperties = explode(";",$Track);

      // It looks like any race with STARTNET1 isn't being displayed on the production site.
      // if (!strstr("STARNET1",$trackProperties[5]))
      // {
      // I don't know why we're doing this but this same logic was found in the PHP version of the site.
      // We check to see if the element is empty; if it is, we use anothe element.
      // echo "Property 12: ".$trackProperties[12]."<br/>";
      if ($trackProperties[7] == "") {
        $trackProperties[7] = $trackProperties[6];
      }

      if ($trackProperties[3] != 'Woodbine Harness' and $trackProperties[3] != 'Pompano Park') {
        // populate closeRaces object.
        if ($trackProperties[12] == "CLOSED") {
          $ClosedTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
        }
        //populate openRaces object.
        if ($trackProperties[12] == "OPEN") {
          $OpenTracks[]=array("ID"=>$trackProperties[1], "RACE"=>$trackProperties[7], "TRACK"=>$trackProperties[3],"MTP"=>$trackProperties[11]);
        }
      }

      $TrackKey = $trackProperties[1];
      $TrackValue = $trackProperties;
      // Populate for later use. EX: Key=APM, Value=a $of track properties.
      $AllTracks[]=array($TrackKey, $TrackValue);
      // $AllTracks[$trackProperties[1]]=array($TrackKey, $TrackValue);
      // }
    }

    $this->OpenTracks = $OpenTracks;
    $this->ClosedTracks = $ClosedTracks;
    $this->AllTracks = $AllTracks;

    switch(strtolower($return)) {
      case "open":
        return $this->OpenTracks;
        break;
      case "closed":
        return $this->ClosedTracks;
        break;
      default:
        return $this->AllTracks;
        break;
    }
  }


  /**
   * Get race times for the current track. We also pass the current Race selected
   * so that while iterating through all races, we can pull out the current Race Details
   * and store them in the RaceInfo object.
   *
   * @param $Id The current track id.
   * @param $Race The current race
   * @return RaceTime generic list used by the UI.
   */
  function getTrackRaceTimes($Id,$Race='') {
    $this->GetTracks();
    $Success = false;
    $Command = "list contest ".$Id;
    $cid = 'RaceTrackTimes_'.$Id;

    $cached = cache_get($cid);

    if(!$cached) {
      $Data = $this->sendCommand($Command);   // Returns FALSE if Command Timed Out

      if($Data) {
        cache_set($cid, $Data, 'cache', time() + $this->CacheTime);
      }
      else {
        return false; // server updating around midnight, or server connection timed out
      }
    }
    else {
      $data = $cached->data;
    }

    //echo "Size: ".strlen($Data);
    $RaceTimes = explode("\n",$Data);     // Create Array of each line
    array_pop($RaceTimes);
    array_pop($RaceTimes);
    $TrackRaceTimes = array();

    //List<RaceTime> TrackRaceTimes = new List<RaceTime>();
    if ($Data != false) {
      foreach ($RaceTimes as $RaceTime) {
        $RaceProperties = explode(';',$RaceTime);
        $RaceStatus = array();
        $RaceStatus[$RaceProperties[2]] = $RaceProperties[8];
        $Status = "";

        switch($RaceProperties[8]) {
          case "RESULTS":
            $Status = "Results";
            break;
          case "FINAL":
            $Status = "Pending";
            break;
          case "OPEN":
            $Status = "Entries";
            break;
          case "CLOSED":
            $Status = "Entries(CLOSED)";
            break;
          case "REMOVED":
            $Status = "Entries(REMOVED)";
            break;
        }

        $MTP = "";
        $sb = "";

        $Tracks = $this->AllTracks;
        $CurrentTrack = array();

        foreach($Tracks as $Track) {
          if($Track[0]==$Id)
          {
            $CurrentTrack = $Track[1];
          }
        }

        if($_SERVER["REMOTE_ADDR"]=='75.84.125.133') {
          /*print_r($CurrentTrack);
           echo "<br/>Curr[7]: ".$CurrentTrack[7]." - Num2: ".$RaceProperties[2]." - Num8: ".$RaceProperties[8];
           echo "<br/><br/>";*/
        }

        if ($CurrentTrack[7] == $RaceProperties[2] && $RaceProperties[8] != "RESULTS") {
          $sb.="&#45;&nbsp;";
          $sb.=$CurrentTrack[11];
          $sb.="&nbsp;MTP";
          $MTP = $sb;
        }
        //Here's where we pass the current race info to build our RaceInfo object.
        //if ($Race == $RaceProperties[2])  //GetRaceInfo(RaceProperties, CurrentTrack);

        $TrackRaceTimes[]=array($RaceProperties[2], $RaceProperties[7], $Status, $MTP);

      }
    }
    else {
      return "Timed Out";
    }

    return $TrackRaceTimes;
  }


  /**
   * Get all entries for the current race.
   *
   * @param $Id The current track initials.
   * @param $Race The current race.
   * @return RaceEntry generic list used by the UI.
   */
  function getRaceEntries($Id, $Race) {
    $Success1 = false;
    $Success2 = false;

    $Command1 = "";
    $Command2 = "";

    $RaceEntries = array();

    $cid = "RaceEntry_".$Id."_".$Race;
    $cached = cache_get($cid);

    if(!$cached) {
      $Command1 = "LIST CONTESTANTS ".$Id." ".$Race;
      $EntryResponse = $this->sendCommand($Command1);
      $Success1 = $EntryResponse;

      if($Success1) {
        cache_set($cid, $EntryResponse, 'cache', time() + $this->CacheTime);
      }
      else {
        //echo "returning false";
        return false; // server updating around midnight, or server connection timed out
      }
    }
    else {
      $EntryResponse = $cached->data;
      $Success1 = true;
    }

    $EntryResponse = explode("\n",$EntryResponse);      // Create Array of each line
    array_pop($EntryResponse);
    array_pop($EntryResponse);
    $Count = count($EntryResponse);

    if($Count==0) {
      //return false;
    }

    //Store this info to use later...
    $FileTwo = 'ListOdds_'.$Id.'_'.$Race;
    $ContestantInfo = $Success1;


    if(!$this->Cached($FileTwo)) {
      $Header = "BOOKIE;1.6.2;HBPROD1/PMPROD1;". $this->host;
      $Command2 = "LIST ODDS ".$Id." ".$Race;
      $fp = fsockopen($this->host, $this->port);
      stream_set_blocking($fp, TRUE);
      stream_set_timeout($fp,3);
      $info = stream_get_meta_data($fp);

      fwrite($fp,$Command2."\r\n");
      fread($fp,strlen($Header)+2);  // Remove Bookie Line
      $OddsResponse = fread($fp,1000);
      fclose($fp);

    }else{
      $OddsResponse = $this->ReadCache($FileTwo);
    }
    //$OddsResponse = '';


    $Success2 = true;


    if ($Success1 && $Success2)
    {
      $EntryA = array();
      $EntryB = array();

      foreach ($EntryResponse as $Entry)
      {
        $EntryProperties = explode(';',$Entry);
        if (strstr($EntryProperties[5],"NAME="))
        {
          $EntryA[]=$Entry;
        }
        else
        {
          $EntryB[]=$Entry;
        }
      }

      //$CLArray = $OddsResponse[0].Split(';')[6].Split('/');
      $TempCL = explode(';',$OddsResponse);
      $CLArray = explode('/',$TempCL[6]);

      $EntryCounter = 0;

      //This data is craptastic; the same data doesn't always
      //appear in the same column so we're forced to search for it below...

      foreach ($EntryA as $Entry)
      {

        $Horse = "";
        $Jockey = "";
        $Trainer = "";
        $CL = "";
        $ML = "";
        $Equip = "";
        $Med = "";
        $Post = "";
        $ClaimPrize = "";


        $EntryProperties = explode(';',$Entry);
        foreach ($EntryProperties as $Property)
        {
          if (strstr($Property,"NAME=")) $Horse = str_replace("NAME=","",$Property);
          if (strstr($Property,"JOCKEY=")) $Jockey = str_replace("JOCKEY=","",$Property);
          if (strstr($Property,"TRAIN=")) $Trainer = str_replace("TRAIN=","",$Property);
          if (strstr($Property,"POST=")) $Post = str_replace("POST=","",$Property);
          if (strstr($Property,"EQUIP=")) $Equip = str_replace("EQUIP=","",$Property);
          if (strstr($Property,"CLAIM=")) $ClaimPrize = str_replace("CLAIM=","",$Property);
          if (strstr($Property,"MED=")) $Med = str_replace("MED=","",$Property);

        }

        $EntryNumber = $EntryProperties[4];

        //If the post number is blank, use the entry number.
        if ($Post == "") $Post = $EntryNumber;

        $CLTemp = explode(';',$OddsResponse[0]);
        if (isset($CLTemp[7]) && $CLTemp[7] == "FINAL")//ML = SQL QUERY (Don't have SQL access so I can't return this value.
        {
          $CL = $CLArray[$EntryCounter];
        }
        else
        {
          $ML = $CLArray[$EntryCounter];
        }

        $EntryCounter++;

        $Wt = ""; //I don't see this in the data we get back and it's not being populated on the site anywhere that I've found.

        $RaceEntries[]=array($EntryNumber, $Horse, $Jockey, $Trainer, $ML, $CL, $Equip, $Med, $Wt, $this->getMoney($ClaimPrize), $Post);


        if (count($EntryB) > 0)
        {
          foreach ($EntryB as $Item)
          {
            $ItemProperties = explode(';',$Item);
            if ($ItemProperties[4] == $EntryNumber)
            {
              $RaceEntries[]=array(str_replace("DISPLAY=","",$ItemProperties[5]), $Horse, $Jockey, $Trainer, $ML, $CL, $Equip, $Med, $Wt, $this->getMoney($ClaimPrize), $Post);
            }
          }
        }
      }
    }

    return $RaceEntries;
  }


  /**
   * @param $Id
   * @param $Race
   * @return string|multitype:
   */
  function getResult($Id, $Race) {
    $cid = "RaceResult_".$Id."_".$Race;
    $cached = cache_get($cid);

    $resultdata = array();
    $Success1 = false;
    $ranklist=array();

    if(!$cached) {
      $Command1 = "GET RESULT ".$Id." ".$Race;
      $EntryResponse = $this->sendCommand($Command1);
      $Success1 = $EntryResponse;
      //print_r($EntryResponse);
      if($Success1) {
        cache_set($cid, $EntryResponse, $data, time() + $this->CacheTime);
      }
      else {
        //echo "returning false";
        return false; // server updating around midnight, or server connection timed out
      }
    }
    else {
      $EntryResponse = $cached->data;
      $Success1 = true;
    }

    $Result=explode(';',$EntryResponse);
    //print_r($Result);
    if (array_key_exists(5, $Result)) {
      $ranklist = explode("/",$Result[5]);
    }

    return $ranklist;
  }


  /**
   *
   * @param $Id
   * @param $Race
   * @return unknown_type
   */
  function getCachedContest($Id, $Race) {
    $Command1 = "LIST CONTESTANTS ".$Id." ".$Race;
    $EntryResponse = $this->sendCommand($Command1);
    //$File="RaceEntry_".$Id."_".$Race;
    //$EntryResponse = $this->ReadCache($File);
    $Entry=explode("\n",$EntryResponse);
    //$EntryList=explode(";",$Entry[2]);
    $contestentname=array();
    //echo "<pre>";
    foreach($Entry as $entry) {
      if(strlen(trim($entry))  > 0) {
        $EntryA=explode(";",$entry);
        //print_r($EntryA);
        $contestentname[$EntryA[3]]=$EntryA[5];
      }
    }

    return $contestentname;
  }


  /**
   *
   * @param $Id
   * @param $Race
   * @return unknown_type
   */
  function getPrice($Id, $Race) {
    $cid = "RacePrice_".$Id."_".$Race;
    $cached = cache_get($cid);

    $resultdata = array();
    $Success1 = false;

    if(!$cached) {
      $Command1 = "LIST PRICE ".$Id." ".$Race;
      $EntryResponse = $this->sendCommand($Command1);
      $Success1 = $EntryResponse;
      //print_r($EntryResponse);
      if($Success1) {
        cache_set($cid,$EntryResponse, 'cache', time() + $this->CacheTime);
      }
      else {
        //echo "returning false";
        return false; // server updating around midnight, or server connection timed out
      }
    }
    else {
      $EntryResponse = $cached->data;
      $Success1 = true;
    }

    $Entry=explode("\n",$EntryResponse);
    //$EntryList=explode(";",$Entry[2]);
    $DATAA=array();
    foreach($Entry as $entry) {
      if(strlen(trim($entry))  > 0) {
        $EntryA=explode(";",$entry);
        $DATAA[]=$EntryA;
        //$contestentname[$EntryA[3]]=$EntryA[5];
      }
    }

    return $DATAA;
  }


  /**
   *
   * @param $Id
   * @param $Race
   * @return unknown_type
   */
  function getTotal($Id, $Race) {
    $cid = "RaceTotal_".$Id."_".$Race;
    $resultdata = array();
    $Success1 = false;

    $cached = get_cache($cid);

    if (!$cached) {
      $Command1 = "LIST TOTAL ".$Id." ".$Race;
      $EntryResponse = $this->sendCommand($Command1);
      $Success1 = $EntryResponse;
      //print_r($EntryResponse);
      if($Success1) {
        cache_set($cid, $EntryResponse, 'cache', time() + $this->CacheTime);
      }
      else {
        //echo "returning false";
        return false; // server updating around midnight, or server connection timed out
      }
    }
    else {
      $EntryResponse = $cached->data;
      $Success1 = true;
    }

    $Entry=explode("\n",$EntryResponse);
    //$EntryList=explode(";",$Entry[2]);
    $DATAA=array();
    foreach($Entry as $entry) {
      if(strlen(trim($entry))  > 0) {
        $EntryA=explode(";",$entry);
        $DATAA[]=$EntryA;
        //$contestentname[$EntryA[3]]=$EntryA[5];
      }
    }

    return $DATAA;
  }


  /**
   *
   * @param $ClaimPrize
   * @return unknown_type
   */
  function getMoney($ClaimPrize) {
    $number = $ClaimPrize/100;
    $formatted = number_format($number, 2);
    return "$".$formatted;

  }

}

