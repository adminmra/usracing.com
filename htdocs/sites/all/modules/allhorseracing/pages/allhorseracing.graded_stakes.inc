<?php
// $Id$

/*
 * @file
 */

function allhorseracing_graded_stakes_page() {
  return theme('ahr_graded_stakes_page');
}


function template_preprocess_ahr_graded_stakes_page(&$vars) {
  $races = ahr_get_graded_stakes();

  $vars['races'] = $races;
}