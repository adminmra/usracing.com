<?php
// $Id$

/*
 * @file
 */

function allhorseracing_news_page() {
  drupal_add_feed(url('news/rss.xml'), t('Horse Racing News'));
  return theme('ahr_racing_news_page');
}


/**
 * A generic function for generating RSS feeds from a set of articles.
 *
 * @param $aids
 *   An array of article IDs (aid). Defaults to FALSE so empty feeds can be
 *   generated with passing an empty array, if no items are to be added
 *   to the feed.
 * @param $channel
 *   An associative array containing title, link, description and other keys.
 *   The link should be an absolute URL.
 */
function allhorseracing_news_feed($aids = FALSE, $channel = array()) {
  global $base_url, $language;

  $limit = 20;

  if ($aids === FALSE) {
    $articles = ahr_get_news_articles($limit, 1);
    $aids = array_keys($articles);
  }

  $item_length = variable_get('feed_item_length', 'teaser');
  $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

  $items = '';
  foreach ($aids as $aid) {
    // Load the specified node:
    $item = $articles[$aid];
    $item->build_mode = NODE_BUILD_RSS;
    $item->link = url("news/$aid", array('absolute' => TRUE));
    $extra = array('pubDate' => gmdate('r', $item->created));

    // Prepare the item description
    switch ($item_length) {
      case 'fulltext':
        $item_text = $item->body;
        break;
      case 'teaser':
        $item_text = $item->teaser;
        if (!empty($item->readmore)) {
          $item_text .= '<p>'. l(t('read more'), 'news/'. $item->aid, array('absolute' => TRUE, 'attributes' => array('target' => '_blank'))) .'</p>';
        }
        break;
      case 'title':
        $item_text = '';
        break;
    }

    $items .= format_rss_item($item->title, $item->link, $item_text, $extra);
  }

  $channel_defaults = array(
    'version'     => '2.0',
    'title'       => variable_get('site_name', 'Drupal'),
    'link'        => $base_url,
    'description' => variable_get('site_mission', ''),
    'language'    => $language->language
  );
  $channel = array_merge($channel_defaults, $channel);

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss version=\"". $channel["version"] ."\" xml:base=\"". $base_url ."\" ". drupal_attributes($namespaces) .">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
  $output .= "</rss>\n";

  drupal_set_header('Content-Type: application/rss+xml; charset=utf-8');
  print $output;
}


/**
 * News article page view,
 *  @see ahr_news_article_load()
 */
function allhorseracing_news_article_view($article) {
 db_set_active('allhorse');
$results = db_query("select * from outguard where id = '6'");		
$results_msg = db_query("select * from outgurd_message where id='1'");
db_set_active('default');
 $row = db_fetch_array($results);
 $rows[] = $row;
 if($row['status']=='dead')
 {
  	$row_msg = db_fetch_array($results_msg);	
	return $row_msg["default_msg"];
	//$vars['articles'] = array();
	//$vars['articles'][$aid] = $row_msg["default_msg"];
 }
 else
 {
  $breadcrumb = drupal_get_breadcrumb();
  $breadcrumb[] = l(t('Racing News'), 'news');
  drupal_set_breadcrumb($breadcrumb);

  drupal_set_title($article->title);

  if (!$article->aid) {
    return drupal_not_found();
  }

  return theme('ahr_racing_news_article', $article);
  }//else switch
}


function theme_ahr_racing_news_article($article) {
  $output = '<div id="article-'. $article->aid .'" class="article"><div class="inner">';
  $output .= '<div class="news-date">'. date_format_date(date_make_date($article->created, NULL, DATE_UNIX), 'custom', 'F d Y') .'</div>';
  $output .= '<div style="display: block; height: 16px; width:125px; padding:0;" ><!-- AddThis Button BEGIN -->
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b7342703113cb61"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b7342703113cb61"></script>
<!-- AddThis Button END --></div>';
  $output .= '<div class="body">'. $article->body .'</div>';
  $output .= '</div></div>';

  return $output;
}


/**
 * Formatting an article teaser item for the racing news block
 * @param $article An Article result array from Dailyracingnews web services call
 */
function theme_ahr_racing_news_article_teaser($article, $first = FALSE) {
  $class = ($first)? 'story-lng' : 'story';
  $output = '<div class="'. $class .'" id="news-story-'. $article->aid .'">';
  $output .= '<h3 class="news-title"><a href="'. base_path() .'news/'. $article->aid .'?title='. str_replace('%26%2339%3Bs', '',str_replace('%20', '-', rawurlencode(strtolower($article->title)))).'">'. $article->title.'</a></h3>';
  $output .= '<div class="news-date">'. date_format_date(date_make_date($article->created, NULL, DATE_UNIX), 'custom', 'F d Y') .'</div>';
  $output .= '<div class="teaser">'. ahr_abbr_html($article->teaser,90) .'<a href="'. base_path() .'news/'. $article->aid .'?title='. str_replace('%26%2339%3Bs', '',str_replace('%20', '-', rawurlencode(strtolower($article->title)))) .'">Read more</a></div>';
  $output .= '</div>';
  return $output;
}


function template_preprocess_ahr_racing_news_page(&$vars) {
 db_set_active('allhorse');
$results = db_query("select * from outguard where id = '6'");		
$results_msg = db_query("select * from outgurd_message where id='1'");
db_set_active('default');
 $row = db_fetch_array($results);
 $rows[] = $row;
 if($row['status']=='dead')
 {
  	$row_msg = db_fetch_array($results_msg);	
	//return $row_msg["default_msg"];
	$vars['articles'] = array();
	$vars['articles'][] = $row_msg["default_msg"];
 }
 else
 {
  $total = ahr_get_news_total_count();
  $limit = 10;
  $page = (isset($_GET['page'])) ? $_GET['page']+1 : 1;

  $articles = ahr_get_news_articles($limit, $page);

  foreach($articles as $article) {
    $vars['articles'][] = theme('ahr_racing_news_article_teaser', $article);
  }

  $vars['pager'] = ahr_racing_news_page_pager($articles, $limit, $total);
  }// else switch
}

function ahr_racing_news_page_pager($articles, $limit, $total) {
  global $pager_page_array, $pager_total;
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  $element = 0;

  // Convert comma-separated $page to an array, used by other functions.
  $pager_page_array = explode(',', $page);

  $pager_total[$element] = ceil($total / $limit);
  $pager_page_array[$element] = max(0, min((int)$pager_page_array[$element], ((int)$pager_total[$element]) - 1));

  return theme('pager', array(), $limit);
}

