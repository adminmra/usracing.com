<?php	 	
// $Id$

/*
 * @file
 */
?>
<div>
  <table class="graded-stakes-races">
    <caption><?php print t('This Week\'s Graded Stakes Schedule') ?></caption>
    <thead>
      <tr>
        <th><?php print t('Date')?></th>
        <th><?php print t('Graded Stakes') ?></th>
      </tr>
    </thead>
    <tbody>
      <?php
        $zebra = TRUE;
        foreach ($races as $race) {
          $class = ($zebra)? '' : 'AlternateRow';

          print '<tr class="'. $class .'">';

          print '<td class="addedon">'. format_date($race['date'], 'custom', 'M j') .'</td>';

          print '<td class="locations">';
          foreach ($race['locations'] as $loc) {
            print '<p><span class="bold">'. $loc['location'] .':</span> '. $loc['details'] .'</p>'. "\n";
          }
          print '<td>';

          print '</tr>';
          $zebra = !$zebra;
        }
      ?>
    </tbody>
  </table>
</div>
