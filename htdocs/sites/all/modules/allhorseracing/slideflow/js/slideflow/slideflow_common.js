var numPhotos;
var slideFlowSlider;
var slideFlow;
var currentImg;
var preloader;
var fadeInTimeout;
var scrollHandler;
var photos;
var currentEffect;
var slHover;

var SLIDE_TPL = {
    'b_vertical' : false, 'b_watch': true    /* update while dragging */, 'n_controlWidth': 380, 'n_controlHeight': 21,
    'n_sliderWidth': 64, 'n_sliderHeight': 21, 'n_pathLeft' : 0, 'n_pathTop' : 0, 'n_pathLength' : 380-64, 'n_zIndex': 1
}

var SLIDE_INIT = {
    'n_minValue' : 0, 'n_maxValue' : 100, 'n_value' : 0, 'n_step' : 1
}

var SLIDEFLOW_DATA = {
    'imgWidthNormal': 200, 'imgWidthTilted': 111, 'imgHeight': 230, 'slideDistance': 35,
    'onCenterClick': handleSlideClick, 'handleSlideMove': handleSlideMove, 'containerElement': null,
    'pathLeft': '/sites/all/modules/allhorseracing/slideflow/photos/left', 'pathCenter': '/sites/all/modules/allhorseracing/slideflow/photos/center', 'pathRight': '/sites/all/modules/allhorseracing/slideflow/photos/right',
    'transparentImg': '/sites/all/modules/allhorseracing/slideflow/images/transparent.gif', 'cursorOpenHand': '/sites/all/modules/allhorseracing/slideflow/images/openhand.cur', 'cursorClosedHand': '/sites/all/modules/allhorseracing/slideflow/images/closedhand.cur'
}

function handleSlideMove(pos) {
    if (slideFlowSlider)
        slideFlowSlider.f_setValue(pos, false, true /* kein update */);
}

function handleSlideClick(imgNumber) {
    swapPhoto(imgNumber);
}

function handleSlideSeek(pos) {
    slideFlow.disableMoveUpdate();
    slideFlow.glideToPerc(pos);
}

function swapPhoto(photoNumber) {

}

function skipPhoto(offset) {
    photoNumber = currentImg + offset;
    if (photoNumber < 1 || photoNumber > numPhotos)
        return;

    swapPhoto(photoNumber);
    slideFlow.glideToSlide(photoNumber);
}



function handleWheel(delta) {
    if (slideFlow)
        slideFlow.scroll(delta);
}

function handleKeys(evt) {
    evt = (evt) ? evt : ((window.event) ? event : null);
    if (evt) {
        //debugLog("key " + evt.keyCode);
        switch (evt.keyCode) {
            case 40: /* down */
            case 39: /* right */
                skipPhoto(1);
                return false;
                break;
            case 38: /* up */
            case 37: /* left */
                skipPhoto(-1);
                return false;
                break;
         }
    }
}

function initSlideFlow() {
    swapPhoto(1);
    slideFlowSlider.f_show();
    document.onkeydown = handleKeys;
    scrollHandler = new ScrollHandler(handleWheel);
    
    /* Preload slider hover */
    slHover = new Image;
    slHover.src = "/sites/all/modules/allhorseracing/slideflow/images/seekslider-hover.gif";    

    /* IE6 hover fix */
    if (window.ieFixHover) {
         $$('.sliderbutton').each(ieFixHover);
    }
}