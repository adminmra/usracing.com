<?php
// $Id$

/*
 * @file
 */

/**
 *
 */
class TodaysRaces {
  function GetRaces($Limit = 0) {
    if($Limit==0) {
      $sql = "SELECT * FROM schedule WHERE type = 3 ORDER BY addedon DESC";
    } else {
      $sql = "SELECT * FROM schedule WHERE type = 3 ORDER BY addedon DESC LIMIT $Limit";
    }

    db_set_active('allhorse');
    $result = db_query($sql);
    db_set_active('default');

    if(mysql_num_rows($result)==0) {
      return FALSE;
    } else {
      return $result;
    }
  }

  function ShowTodaysRacesOld() {
    $message = '';
    $result = $this->GetRaces(1);
    $row = db_fetch_array($result);

    if($row==false) {
      $message = '
      <tr>
        <td>ddd</td>
        <td align="center">12</td>
        <td align="center">1</td>
        <td align="center">BET NOW</td>
        <td class="dark"></td>
      </tr>
      ';
    }
    else {
      $MainData = str_replace("’","",str_replace("\r\n","\n",$row["data"]));
      $races = explode("\n",$MainData);

      $i=1;
      foreach($races as $race) {
        if($i==0){
          $class='';$classTwo='light';$i=1;}else{$i=0;$class=' class="grey"';$classTwo='dark';
        }
        $Data = explode(" ",strip_tags($race));
        if(isset($Data[0]) && isset($Data[1])) {
          $Time = $Data[0];

          array_shift($Data);

          $Title = ereg_replace("[^A-Za-z0-9 ]", "",implode(' ',$Data));
          $message .= '
          <tr'.$class.'>
            <td width="190">'.$Title.'</td>
            <td width="70" align="center">4</td>
            <td width="125" align="center">11</td>
            <td width="100" align="center">BET NOW</td>
            <td class="'.$classTwo.'"></td>
          </tr>
          ';
        }
      }
    }

    $output = '
    <div id="TabControl">
      <ul class="ui-tabs-nav" style="margin:0;padding:0;float:left;height:19px;margin-left:5px;margin-top:-2px;">
        <li style="margin:0;"><a href="#TodaysTracks" style="padding:0"><span style="padding:0;margin:0;"><img src="/Images/Home/btn_todays_races.png" /></span></a></li>
        <li style="margin:0;"><a href="#Results" style="padding:0"><span style="padding:0;margin:0;"><img src="/Images/Home/btn_results.png" /></span></a></li>
      </ul>
      <img style="margin-left:3px;" src="/Images/Home/Grey_Stripes.png"/>
    </div>
<div class="Box_Orange Todays_Races" style="width: 674px; margin-left: 5px;background:none;">
                                    <div style="background:none;">
                                        <div style="padding-top:0;background:none;">
                                            <div>
                                                <div style="margin-top:-3px;">
                                                    <div>
                                                        <div>
                                                            <div>
                                                                <div class="boxContent">
                                  <div id="Tabs">
                                    <div id="Results"  class="ui-tabs-panel">
                                      No Results yet.
                                    </div>
                                    <div id="TodaysTracks" class="ui-tabs-panel">
                                      <table width="100%" cellspacing="0">
                                        <tbody>
                                          <tr class="headings">
                                            <td>Track</td>
                                            <td align="center">Race</td>
                                            <td align="center">MTP</td>
                                            <td bgcolor="#464646"></td>
                                            <td bgcolor="#464646"></td>
                                          </tr>
                                          '.$message.'
                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
';

    return $output;
  }

  function ShowTodaysRaces()
  {
    include_once('Tracks.inc');
    $DataFeed = new DataFeed;
    $Success = $DataFeed->GetTracks();

    $OpenTracksTable = '';
    $ClosedTracksTable = '';

    if($Success) {

      if(empty($DataFeed->OpenTracks)){

        $OpenTracksTable = '
        <tr>
          <td colspan="5">No Open Tracks.  Check Back Later.</td>
        </tr>
        ';

      }
      else {
        $OpenTracks = $DataFeed->OpenTracks;
        $i=1;
        foreach($OpenTracks as $Track) {
          if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
            continue;
          }
          if($i==0){$class='';$classTwo='light';$i=1;}else{$i=0;$class=' class="grey"';$classTwo='dark';}


          // $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]

          $OpenTracksTable .= '
          <tr>
            <td><a href="/racetrack/?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race ' . $Track["RACE"] . '">'.$Track["TRACK"].'</a></td>
            <td>'.$Track["RACE"].'</td>
            <td>'.$Track["MTP"].'</td>
            <td><a href="/racebook/" titie="Bet Now!" style="color:#000;text-decoration:none;">BET NOW</a></td>
            <td></td>
          </tr>
          ';
        }
      }

      if(empty($DataFeed->ClosedTracks)) {
        $ClosedTracksTable = '
        <tr>
          <td colspan="4">No Closed Tracks. Check Back Later.</td>
        </tr>
        ';
      }
      else {
        $ClosedTracks = $DataFeed->ClosedTracks;
        $i=1;
        foreach($ClosedTracks as $Track) {
          if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false) {
            continue;
          }

          if($i==0) { $class='';$classTwo='light';$i=1;}else{$i=0;$class=' class="grey"';$classTwo='dark'; }

          // $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]

          $ClosedTracksTable .= '
          <tr>
            <td><a href="/racetrack/?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race ' . $Track["RACE"] . '">'.$Track["TRACK"].'</a></td>
            <td>'.$Track["RACE"].'</td>
            <td>'.$Track["MTP"].'</td>
            <td></td>
          </tr>
          ';
        }
      }
    }
    else {
      $sqlmsg= "select * from create_message where id='104'";
      $resultmsg=mysql_query($sqlmsg);
      $datamsg=mysql_fetch_object($resultmsg);
      $OpenTracksTable = '
        <tr>
          <td colspan="5"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
        </tr>
        ';
      $ClosedTracksTable = '
        <tr>
          <td colspan="4"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
        </tr>
        ';
    }

    $output = '
    <div id="TabControl">
      <ul class="ui-tabs-nav">
        <li><a href="#TodaysTracks" title="Horse Racing Entries"><span><img src="/Images/Home/btn_todays_races.png" alt="Horse Racing Entries" /></span></a></li>
        <li><a href="#Results" title="Horse Racing Results"><span><img src="/Images/Home/btn_results.png" alt="Horse Racing Results" /></span></a></li>
      </ul>
      <img style="margin-left:1px;" src="/Images/Home/Grey_Stripes.png" />
    </div>
    <div class="Todays_Races" style="width: 674px; margin-left: 5px;background:none; text-align: left;">
                                    <div style="background:none;">
                                        <div style="padding-top:0;b?ckground:none;">
                                            <div>
                                                <div style="margin-top:-2px;background:none;">
                                                    <div style="background:none;">
                                                        <div style="background:none;">
                                                            <div style="background:none;">
                                                                <div class="boxContent">
                                  <div id="Tabs">
                                    <div id="Results" class="ui-tabs-panel" style="padding:0;">
                                      <table cellspacing="0"  summary="The results for todays horse racing."  title ="Horse Racing Results">
                                        <tbody>
                                          <tr>
                                            <td>Track</td>
                                            <td>Race</td>
                                            <td>MTP</td>
                                            <td></td>
                                          </tr>
                                          '.$ClosedTracksTable.'
                                        </tbody>
                                      </table>
                                    </div>
                                    <div id="TodaysTracks" class="ui-tabs-panel" style="padding:0;">
                                      <table cellspacing="0" summary="Online horse betting with these racing entries."  title ="Horse Racing Entries">
                                        <tbody>
                                          <tr>
                                            <td>Track</td>
                                            <td>Race</td>
                                            <td>MTP</td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                          '.$OpenTracksTable.'
                                        </tbody>
                                      </table>
                                    </div>
                                    <script type="text/javascript">
                                      $(document).ready(function() {
                                        $("div#Results > table > tbody").find("tr:odd").addClass("grey");
                                        $("div#Results > table > tbody").find("tr:first-child").addClass("headings");
                                        $("div#TodaysTracks > table > tbody").find("tr:odd").addClass("grey");
                                        $("div#TodaysTracks > table > tbody").find("tr:first-child").addClass("headings");
                                      });
                                    </script>
                                  </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<script>
  $(document).ready
  (
    function()
    {
      $(\'#Tabs ul\').tabs({ fxFade: true, fxSpeed: \'fast\' });
      //document.getElementById(\'Tabs\').style.display = \'\';
    }
  );
</script>
';

    return $output;
  }

  function ShowTodaysRacesMini() {
    $sql2 ="select * from outguard where id = '2'";
    $result2=mysql_query($sql2);
    $data2=mysql_fetch_object($result2);

    if ($data2->status=='dead')  {
        $sqlmsg= "select * from create_message where id='104'";
    $resultmsg=mysql_query($sqlmsg);
    $datamsg=mysql_fetch_object($resultmsg);

        $OpenTracksTable = '
              <tr>
                <td colspan="4"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
              </tr>
              ';
            $ClosedTracksTable = '
              <tr>
                <td colspan="3"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
              </tr>
              ';
    }
    else
    {
          include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/TrackResults/Tracks.php');
          $DataFeed = new DataFeed;
          $Success = $DataFeed->GetTracks();

          $OpenTracksTable = '';
          $ClosedTracksTable = '';

          if($Success)
          {

            if(empty($DataFeed->OpenTracks)){

              $OpenTracksTable = '
              <tr>
                <td colspan="4">No Open Tracks.  Check Back Later.</td>
              </tr>
              ';

            }else{

                $OpenTracks = $DataFeed->OpenTracks;
                $i=1;
                foreach($OpenTracks as $Track){
                  if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
                    continue;
                  }
                  if($i==0){$class=' class="grey"';$classTwo='light';$i=1;}else{$i=0;$class='';$classTwo='dark';}


                    // $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]

                    $OpenTracksTable .= '
                    <tr>
                      <td><a href="/racetrack/?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '">'.$Track["TRACK"].'</a></td>
                      <td>'.$Track["RACE"].'</td>
                      <td>'.$Track["MTP"].'</td>
                      <td><a href="/racebook/" style="color:#000;text-decoration:none;"><img src="/Images/Racebook/button_arrow.png" /></a></td>
                    </tr>
                    ';


                }
            }



            if(empty($DataFeed->ClosedTracks)){

              $ClosedTracksTable = '
              <tr>
                <td colsp?n="3">No Closed Tracks. Check Back Later.</td>
              </tr>
              ';

            }else{

                $ClosedTracks = $DataFeed->ClosedTracks;
                $i=1;
                foreach($ClosedTracks as $Track){
                  if (strpos($Track["TRACK"], "Miami Jai-Alai") !== false){
                    continue;
                  }
                  if($i==0){$class=' class="grey"';$classTwo='grey';$i=1;}else{$i=0;$class='';$classTwo='dark';}


                    // $Track["TRACK"] , $Track["RACE"] , $Track["MTP"]

                    $ClosedTracksTable .= '
                    <tr>
                      <td><a href="/racetrack/?id='.$Track["ID"].'&race='.$Track["RACE"].'&racename='.$Track["TRACK"].'" title="' . str_replace("'","", $Track["TRACK"]) . ' Results for Race '. $Track["RACE"] . '">'.$Track["TRACK"].'</a></td>
                      <td>'.$Track["RACE"].'</td>
                      <td>'.$Track["MTP"].'</td>
                    </tr>
                    ';


                }
            }

          }
          else
          {
            $sqlmsg= "select * from create_message where id='104'";
              $resultmsg=mysql_query($sqlmsg);
              $datamsg=mysql_fetch_object($resultmsg);

            $OpenTracksTable = '
              <tr>
                <td colspan="4"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
              </tr>
              ';
            $ClosedTracksTable = '
              <tr>
                <td colspan="3"><strong>'.stripslashes($datamsg->user_conf_msg).'</strong></td>
              </tr>
              ';

          }
    }


    $output = '
    <div id="Tabs">
      <div id="Results"  class="ui-tabs-panel" style="padding:7px;">
        <table cellspacing="0" summary="The results for todays horse racing."  title ="Horse Racing Results">
          <tbody>
            <tr>
              <td><b>Track</b></td>
              <td><b>Race</b></td>
              <td><b>MTP</b></td>
            </tr>
            '.$ClosedTracksTable.'
          </tbody>
        </table>
      </div>
      <div id="TodaysTracks" class="ui-tabs-panel" style="padding:7px;">
        <table cellspacing="0"  summary="Online horse betting with these racing entries."  title ="Horse Racing Entries">
          <tbody>
            <tr>
              <td><b>Track</b></td>
              <td><b>Race</b></td>
              <td><b>MTP</b></td>
              <td style="background:transparent none"><b>Bet</b></td>
            </tr>
            '.$OpenTracksTable.'
          </tbody>
        </table>
      </div>
      <script type="text/javascript">
        $(document).ready(function() {
          $("div#Results > table > tbody").find("tr:odd").addClass("grey");
          $("div#Results > table > tbody").find("tr:first-child").addClass("headings");
          $("div#TodaysTracks > table > tbody").find("tr:odd").addClass("grey");
          $("div#TodaysTracks > table > tbody").find("tr:first-child").addClass("headings");
        });
      </script>
    </div>
  <!--</div>-->
';

  $output_two = '
  <div class="Box_White_NoHdr" id="MiniTodaysRaces" style="width: 290px; height: auto ! important; min-height: 300px;">
 <div>
     <div>
         <div>
             <div>
                 <div>
                     <div>
                         <div>
                             <div style="height: auto ! important; min-height: 300px;background-color:#fff;" class="boxContent">
                 <div>
                   <div id="TabControl" class="mini">
                    <ul class="ui-tabs-nav">
                      <li><a href="#TodaysTracks"><span>TODAY\'S RACES</span></a></li>
                      <li><a href="#Results"><span>RESULTS</span></a></li>
                    </ul>
                  </div>
                     </div>
                '.$output.'
                             </div>
                             <div style="clear: both;">
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>
<script>
  $(document).ready
  (
    function()
    {
      $(\'#TabControl ul\').tabs({ fxFade: true, fxSpeed: \'fast\' });
      $(\'#SpotlightOn ul\').tabs({ fxFade: true, fxSpeed: \'fast\' });
      document.getElementById(\'Tabs\').style.display = \'\';
    }
  );
</script>
';

  return $output_two;

  }




}

?>