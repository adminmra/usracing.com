<?php	 	
	
	
	function BuildColumn($index,$LineTypes)
	{
		//Get current Column
		$LineType = $LineTypes[0]->Column[$index];
		
		echo("<div class=\"line_type_column\">");
		foreach($LineType as $Column)
		{
		    if(sizeof($Column) > 0)
		    {
			echo("<div class=\"line_type_container\">");
			}
				if ($Column["Name"] == 'Text')
				{
					echo($Column);
					echo("<br/><br/>");
				}
				else
				{
					if ($Column["Name"] == "Today's Hot Bets")
					{
						echo("<div class=\"today_hotbet_header\">");
					}
					else
					{
						echo("<div class=\"line_type_header\">");
					}
					echo($Column["Name"]);
					echo("</div>");
					
					$lineCounter = 1;
					if (sizeof($Column) > 0)
					{
						echo("<ul class=\"line_type_list\">");
							foreach($Column as $LineType)
							{
								if ($lineCounter % 2 == 0)
								{
									echo("<li class=\"list_item_alt\">");
								}
								else
								{
									echo("<li class=\"list_item\">");
								}
								echo("<a class=\"lin_item_link\" href='". $LineType["Url"] ."'>".$LineType["Name"]."</a>");
								ShowNew($LineType);
								echo("</li>");
								
								$lineCounter = $lineCounter + 1;
							}
						echo("</ul>");
						echo("</div>");
					}
				}
			
		}
		echo("</div>");
	}
	
	function GetLineTypeVal()
	{
		if ($_GET['type'] != "")
		{
			return $_GET['type'];
		}
		return '';
	}
	
	function IsAuthenticated()
	{
		/*if (isset($_GET['auth']) && $_GET['auth'] == 'true')
		{
			return "true";
		}
		else 
		{
			return "false";
		}*/
		return "true";
	}
	
	function ShowNew($LineType)
	{
		if ($LineType["Addedon"] != '')
		{
			$date = explode('-',$LineType["Addedon"]);
			if(mktime(0,0,0,$date['1'],$date['2'],$date['0']) >= mktime(0,0,0,date("m"), date("d")-5,date("Y")))
			{
				echo('&nbsp;<span class="empasis">New!</span>');
			}
		}
	}
	
	function GetLineTypeDetails()
	{
		$CurLineType = GetLineTypeVal();
		
		//get lines
		$FeedContent = "/var/www/vhosts/gohorsebetting.com/httpdocs/commonlines/bettinglinesAHR_sbk5.xml";
			
		$xml = Simplexml_load_file($FeedContent);
		$LineTypes = $xml->xpath("LineTypes/@Name");
		
		//Get current line type
		$CurLineType = $xml->xpath("LineTypes[@Name='". $CurLineType ."']");
		
		//get number of columns for current Line Type
		$numColumns = sizeof($CurLineType[0]->Column);
		
		//build columns
		echo ("<table class=\"sportslines-table\"><tr>");
		for ($i = 0 ; $i < $numColumns ; $i++)
		{
			echo ("<td valign=\"top\">");
			BuildColumn($i,$CurLineType);
			echo ("</td>");
		}
		
		$CommonCol = $xml->xpath("Commoncolumn");
		echo("<td valign=\"top\">");
		BuildColumn(0,$CommonCol);
		echo("</td>");
		
		echo ("</table></tr>");
	}
?>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Untitled Page</title>
    <link href="/sites/all/modules/allhorseracing/sportsbook/sportsbook_sbk5.css" type="text/css" rel="Stylesheet" media="screen" />
    <script type="text/javascript" src="/sites/all/modules/jquery_update/replace/jquery.min.js" ></script>
    
    <script type="text/javascript">
		var isAuthenticated = <?php echo IsAuthenticated(); ?>;
		
		$(document).ready(function(){
			if (!isAuthenticated)
			{
				$(".lin_item_link").bind("click",function(){
					alert("Please join or login to play.");
					return false;
				});
				
			}
			var ifLocation = window.location.href;
			var line = ifLocation.substring(ifLocation.indexOf('type=') + 5).split('&')[0];
			
			parent.ResetHeader();
			parent.SetSelectedHeader(line.replace('%20',' '));
			
		});
		
    </script>
</head>
<body>
	<div style="width:auto;">
		<?php  GetLineTypeDetails(); ?>
	</div>
</body>
</html>


