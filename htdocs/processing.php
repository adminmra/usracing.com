<!DOCTYPE HTML>
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width initial-scale=1, user-scalable=1">
<title>US Racing - Logging you in securely...</title></head>
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" href="assets/css/loggingin.css">
<link rel="stylesheet" href="assets/css/style.css">

</head>

<body>
<div id="wrap">
<div class="top" id="top">
 <div class="container">
 <a href="/" class="logo logo-lrg"><img alt="Online Horse Betting" src="img/usracing.png" id="logo-header"></a>
 <a href="/" class="logo logo-sm"><img alt="Online Hose Betting" src="img/usracing-sm.png"></a>
 </div>
</div>  

<div id="main">
<div class="container">
<div class="row">
<div class="col-md-12 center">
<h3>Logging you in securely...</h3>
<img src="img/usr-loggingin.gif" />
<p>US Racing. The easiest place to bet on horses...</p>

<p style="line-height:1.2em;"><small>If this page appears for more than 5 seconds, <a href="/">click here</a> to reload.</small><p>
</div>
</div>
</div>  

</div><!-- end/main -->
</div><!-- end/wrap -->

<footer>
<div class="copyright" style="line-height:40px; padding:0; ">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand">Copyright <?php echo date('Y'); ?>  <a href="/">US Racing</a>, All Rights Reserved</span></div>
    </div><!--/row--> 
 
  </div><!--/container--> 
</div>
</footer>

</body>
</html>
