<!DOCTYPE html> <html lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#"> 
<head>    
    <title><?php echo $_usr_vars["title"];?></title>

<?php /*
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'> 
<!-- news libraries here --> 
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">   */ ?>
<link rel="profile" href="http://gmpg.org/xfn/11"> 
<link rel="pingback" href="//www.usracing.com/xmlrpc.php">  
<!-- All in One SEO Pack 2.3.9.2 by Michael Torbert of Semper Fi Web Design[-1,-1] --> 
<!-- /all in one seo pack -->  
<!-- This site is optimized with the Yoast SEO plugin v4.2.1 - https://yoast.com/wordpress/plugins/seo/ --> 
<!-- / Yoast SEO plugin. -->  
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin>
<link rel='dns-prefetch' href='//s.w.org' /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Feed" href="//www.usracing.com/feed" /> 
<!-- <link rel="stylesheet" href="/assets/css/as-seen-on-new-tpl-news-min.css?v=1.1"> -->
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Roboto:400,700|Roboto+Condensed:400,700|Lato:400,300,700,900|Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc&display=swap' type='text/css' media='all' />  
<link rel="stylesheet" href="/news/layout/compressed.css?v=1.2">
<link rel="stylesheet" href="/news/layout/custom.css">

<!-- End Author Description -->

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* <!-- <link rel='stylesheet' id='apss-font-awesome-css'  href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css' media='all' />  -->
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc' type='text/css' media='all' /> 
<link rel='stylesheet' id='apss-frontend-css-css'  href='//www.usracing.com/wp-content/plugins/accesspress-social-share/css/frontend.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='wp-pagenavi-css'  href='//www.usracing.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='_usr-bootstrap-css'  href='//www.usracing.com/../assets/plugins/bootstrap/css/bootstrap.min.css' type='text/css' media='all' /> 
<!-- <link rel='stylesheet' id='_usr-assets-style-css'  href='//www.usracing.com/../assets/css/style.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/>  -->
<!-- <link rel='stylesheet' id='_usr-style-css'  href='//www.usracing.com/wp-content/themes/usracing/style.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/>  -->
<link rel='stylesheet' id='_usr-assets-style-css'  href='//www.usracing.com/../assets/css/style.min.css?v2.2' type='text/css' media='all' /> 
<!-- <link rel='stylesheet' id='_usr-style-css'  href='//www.usracing.com/wp-content/themes/usracing/style.css' type='text/css' media='all' />  -->
<link rel='stylesheet' id='_usr-style-plugins-css'  href='//www.usracing.com/../assets/css/plugins.min.css?v2.4' type='text/css' media='all' /> 
<link rel='stylesheet' id='acfpb-public-css'  href='//www.usracing.com/wp-content/plugins/acf-page-builder/includes/../public/css/acfpb_styles.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_font-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_font.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_flat-icons-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_flat-icons.css' type='text/css' media='all' /> */ ?>
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' type='text/css' media='all' /> 
<?php /* <script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery-migrate.min.js'></script> 
<script type='text/javascript' src='//www.usracing.com/../assets/plugins/bootstrap/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='https://www.usracing.com/assets/js/news.js'></script> */ ?>
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="//www.usracing.com/wp-includes/wlwmanifest.xml" />   

<link rel="canonical" href="https://www.usracing.com<?php echo $_SERVER['REQUEST_URI'];?>">
<?php /* 
<script type="text/javascript"> 
    (function(url){     if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }  var addEvent = function(evt, handler) {         if (window.addEventListener) {          document.addEventListener(evt, handler, false);         } else if (window.attachEvent) {            document.attachEvent('on' + evt, handler);      }   };  var removeEvent = function(evt, handler) {      if (window.removeEventListener) {           document.removeEventListener(evt, handler, false);      } else if (window.detachEvent) {            document.detachEvent('on' + evt, handler);      }   };  var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');  var logHuman = function() {         var wfscr = document.createElement('script');       wfscr.type = 'text/javascript';         wfscr.async = true;         wfscr.src = url + '&r=' + Math.random();        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);        for (var i = 0; i < evts.length; i++) {             removeEvent(evts[i], logHuman);         }   };  for (var i = 0; i < evts.length; i++) {         addEvent(evts[i], logHuman);    } })('//www.usracing.com/?wordfence_logHuman=1&hid=7A7E3E091E768F9BB41A4FCDA4E853DE'); 
</script>
<!-- START - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<!-- Facebook Open Graph -->                    
<!-- Google+ / Schema.org -->        
<!-- Twitter Cards -->              
<!-- SEO -->     
<!-- Misc. tags --> 
<!-- END - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<script type="text/javascript" src="//www.usracing.com/wp-content/themes/usracing/js/put5qvj.js"></script>
<!-- <script type="text/javascript">try{Typekit.load();}catch(e){}</script> 2018-09-24 Herbert--> 
<link rel='stylesheet' id='sab-plugin-css'  href='https://www.usracing.com/wp-content/plugins/simple-author-box/css/simple-author-box.min.css' type='text/css' media='all' /> 
<link href="https://www.usracing.com/news/wp-content/themes/usracing/css/news.css" rel="stylesheet" type="text/css" media="none" onload="if(media!='all')media='all'">
<link href="https://www.usracing.com/assets/css/news_custom.css?v1.1" rel="stylesheet" type="text/css" media="none" onload="if(media!='all')media='all'">

*/ ?>
<?php /*<script type='text/javascript'  src='/news/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript'  src='/news/wp-includes/js/jquery/jquery-migrate.min.js'></script>
<script type='text/javascript'  src='/assets/plugins/bootstrap/js/bootstrap.min.js'></script> */ ?>
<script type='text/javascript'  src='/news/layout/jquery-2.2.4.min.js'></script> 	
<?php echo $_usr_vars["metas"];?>  
<meta property="fb:admins" content="1352596260"/>
<meta property="fb:admins" content="1042976107"/>
<?php
$ampUrl= str_replace('/news/','/news/amp/',$_SERVER["REQUEST_URI"]);
$ampcheck='/home/ah/usracing.com/htdocs/'.$ampUrl.'.php';
if (file_exists($ampcheck)) { echo '<link rel="amphtml" href="https://'.$_SERVER['SERVER_NAME'].$ampUrl.'" />'; }
?>



</head>  
    <body class="<?php echo $_usr_vars["classes"];?>"> 
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
        require('/home/ah/allhorse/public_html/menusyncrusr.php');
    ?>
<?php include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
