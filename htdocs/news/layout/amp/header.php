  <!doctype html>
  <html ⚡ lang="en">
    <head>
      <?php echo $amp_vars["metas"]; ?>
      <link rel='dns-prefetch' href='https://cdn.ampproject.org/' /> 
      <link rel='dns-prefetch' href='https://maxcdn.bootstrapcdn.com' /> 
	  <link rel="dns-prefetch" href="https://fonts.gstatic.com/" crossorigin>
      <title><?php echo $pagetitle; ?> | Horse Racing News</title>
      <script async src="https://cdn.ampproject.org/v0.js"></script>
      <script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
      <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
      <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
      <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
      <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
      <script async custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js"></script>
      <!-- Import `amp-list` to get a fresh a list of related articles -->
      <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
      <!-- Import `amp-mustache` as a format template for `amp-list` -->
      <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
      <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
      <script async custom-element="amp-truncate-text" src="https://cdn.ampproject.org/v0/amp-truncate-text-0.1.js"></script>
      <!-- Import `amp-analytics` for tracking usage -->
      <!-- AMP Analytics -->
	  <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
      <link rel="canonical" href="<?php echo $canonical; ?>">
	<?php  if(isset($amp_vars["structure"])){ echo $amp_vars["structure"]; } ?>

<?php include "css.php"; ?>
</head>
    <body>
<!-- Google Tag Manager -->
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-54NV698&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>      
   <amp-sidebar id="sidebar" layout="nodisplay" side="left">
   <p class="navigation_close_button" on="tap:sidebar.close" role="" tabindex="">&times;</p>
  <nav class="ampstart-sidebar-nav ampstart-nav">
<div class="navdiv" >
<amp-accordion layout="container" disable-session-states="" class="ampstart-dropdown" expand-single-section>
		<?php include_once '/home/ah/usracing.com/htdocs/news/layout/amp/header_include.php'; ?>     

</amp-accordion> 
</div>
    
    </nav>   
   </amp-sidebar>

   
<div id="top" class="top">
 <div class="navbar-header" on="tap:sidebar.open" role="" tabindex="" >
<span class="fa fa-bars navicon" aria-hidden="true"   ></span>
</div>
 <div class="container">
 <amp-img src="/img/usracing-sm.png" width="364" height="68" layout="responsive"></amp-img>
 </div>
  <div class="betnow-header" >
    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref=<?php echo $_SERVER["REQUEST_URI"]; ?>" rel="nofollow"><span>Bet Now</span></a>
	</div>
</div>

      <!-- -->
      <main>
      <div class="maincontent" >
      <div class="container" >
      <div class="row" >
      <div class="col-lg-9 col-md-12 col-sm-12" >
