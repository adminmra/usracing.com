<aside id="text-9" class="widget panel panel-white widget_text"><div class="panel-heading"><h3 class="widget-title">News and Handicapping Reports</h3></div>			<div class="textwidget"><div class="newshandicapping">
<p> Breaking news, expert analysis and handicapping reports to help you win at the track. Sign up to get the latest insights delivered straight to your inbox.</p>
<div id="email-subscribers-custom">
<div>
<div class="form-subscribe">
<form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action-xhr="https://visitor2.constantcontact.com/api/signup"> <input data-id="ca:input" type="hidden" name="ca" value="6a796fab-de62-40d3-874e-d0ddf9ba3426"> <input data-id="list:input" type="hidden" name="list" value="1770667721"> <input data-id="source:input" type="hidden" name="source" value="EFD"> <input data-id="required:input" type="hidden" name="required" value="list,email,first_name"> <input data-id="url:input" type="hidden" name="url" value=""> </p>
<div class="es_lablebox">Your First Name</div>
<div data-id="First Name:p" class="es_textbox"> <input type="text" name="first_name" data-id="First Name:input" class="es_textbox_class"/> <label data-id="First Name:label" data-name="first_name" class="ctct-form-required"></label></div>
<div class="es_lablebox">Your Best Email Address</div>
<div data-id="Email Address:p" class="es_textbox"> <input type="email" name="email" data-id="Email Address:input" class="es_textbox_class" /> <label data-id="Email Address:label" data-name="email" class="ctct-form-required"></label> </div>
<div class="es_button"> <label></label> <input type="submit" value="Subscribe" class="es_textbox_button"/></div>
<div submit-success><template type="amp-mustache"></p>
<div class="showafterform">
<h3>Thank you for subscribing to US Racing News and Reports.</h3>
</div>
<p></template></div>
<div submit-error><template type="amp-mustache"></p>
<div class="showafterform"> Error! Thanks for trying please provide valid name and email.</div>
<p></template></div>
</form>
</div>
</div>
</div>
</div>
</div>
		</aside><aside id="custom_html-2" class="widget_text widget panel panel-white widget_custom_html"><div class="textwidget custom-html-widget">          <!--addbutler -->
          <div class="sidewidget">
          <center>
          <!-- News [amp] -->
            <amp-ad width=280 height=404
                type="adbutler"
                data-account="169350"
                data-zone="262833">
            </amp-ad>
            </center>
          </div>
          <!--addbutler --></div></aside><aside id="text-10" class="widget panel panel-white widget_text"><div class="panel-heading"><h3 class="widget-title">Follow Us</h3></div>			<div class="textwidget"><div class="social twitter"><i class="fa fa-twitter"></i><a class="twitter-follow-button" style="width: 234px; height: 20px; background-color: #1b95e0; font-weight: bold; color: #fff; padding: 1.6%; font-family: Helvetica, arial, sans-serif; font-size: 11px; white-space: nowrap; border-radius: 3px; margin-left: 0px; text-decoration: none;" href="https://twitter.com/usracingtoday" target="_blank" rel="noopener noreferrer">Follow @usracingtoday</a></div>
<div class="social instagram"><i class="fa fa-instagram"></i><a style="width: 234px; height: 20px; background-color: #ef4859; font-weight: bold; color: #fff; padding: 1.6%; font-family: Helvetica,arial,sans-serif; font-size: 11px; white-space: nowrap; border-radius: 3px; margin-left: 0; text-decoration: none;" href="https://www.instagram.com/usracingtoday/" target="_blank" rel="noopener noreferrer">Follow us on Instagram</a></div>
<div class="social pinterest"><i class="fa fa-pinterest"></i><span class="PIN_1588930502915_button_follow" data-pin-log="button_follow" data-pin-href="https://www.pinterest.com/usracingtoday/pins/follow/?guid=jXvLK2eE2TnN"><a data-pin-do="buttonFollow" href="https://www.pinterest.com/usracingtoday/" style="color:#000; text-decoration:none;" target="_blank" rel="noopener noreferrer">Follow us</a></span></div>
</div>
		</aside><aside id="rpwe_widget-6" class="widget panel panel-white rpwe_widget recent-posts-extended"><div class="panel-heading"><h3 class="widget-title">Latest Racing News</h3></div><div  class="rpwe-block "><ul class="rpwe-ul"><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="/news/amp/prat-and-rispoli-are-set-to-start-riding-in-new-york-after-the-santa-anita-derby-both-said-they-are-leaving-southern-california-for-better-top-level-stakes-opportunities"  rel="bookmark"><amp-img width="300" height="225" layout="responsive"  class="rpwe-aligncenter rpwe-thumb" src="/news/wp-content/uploads/2022/03/Jockey-Flavien-Prat-celebrates-aboard-Country-House-after-being-declared-the-winner-of-the-145th-Kentucky-Derby.Photo-courtesy-of-John-Minchillo-Associated-Press-300x225.jpg" alt="Jockeying for Position: Prat, Rispoli Heading to New York" ></amp-img></a><h3 class="rpwe-title"><a href="/news/amp/prat-and-rispoli-are-set-to-start-riding-in-new-york-after-the-santa-anita-derby-both-said-they-are-leaving-southern-california-for-better-top-level-stakes-opportunities" title="Permalink to Jockeying for Position: Prat, Rispoli Heading to New York" rel="bookmark">Jockeying for Position: Prat, Rispoli Heading to New York</a></h3><time class="rpwe-time published" datetime="2022-03-11T12:47:01-05:00">March 11, 2022</time></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="/news/amp/5-tips-to-execute-at-the-track"  rel="bookmark"><amp-img width="300" height="225" layout="responsive"  class="rpwe-aligncenter rpwe-thumb" src="/news/wp-content/uploads/2020/09/1M2A1997-scenic-1-scaled-300x225.jpg" alt="5 Tips To Execute At The Track" ></amp-img></a><h3 class="rpwe-title"><a href="/news/amp/5-tips-to-execute-at-the-track" title="Permalink to 5 Tips To Execute At The Track" rel="bookmark">5 Tips To Execute At The Track</a></h3><time class="rpwe-time published" datetime="2022-03-11T10:13:15-05:00">March 11, 2022</time></li><li class="rpwe-li rpwe-clearfix"><a class="rpwe-img" href="/news/amp/tampa-bay-derby-classic-causeway-the-one-to-beat"  rel="bookmark"><amp-img width="300" height="225" layout="responsive"  class="rpwe-aligncenter rpwe-thumb" src="/news/wp-content/uploads/2022/03/Classic-Causeway-shook-off-all-challenges-on-the-lead-to-win-the-Sam-F.-Davis-Stakes.-SV-Photography-300x225.jpg" alt="Tampa Bay Derby: Classic Causeway The One To Beat" ></amp-img></a><h3 class="rpwe-title"><a href="/news/amp/tampa-bay-derby-classic-causeway-the-one-to-beat" title="Permalink to Tampa Bay Derby: Classic Causeway The One To Beat" rel="bookmark">Tampa Bay Derby: Classic Causeway The One To Beat</a></h3><time class="rpwe-time published" datetime="2022-03-10T10:35:15-05:00">March 10, 2022</time></li></ul></div><!-- Generated by http://wordpress.org/plugins/recent-posts-widget-extended/ --></aside>