<div id="left-col" class="col-medium-9">
<!-- test -->
 <?php
	if(isset($_usr_vars['featured_section'])){
		if(file_exists($_usr_vars['featured_section']))
			require $_usr_vars['featured_section'];
	}
	?>
	<div class="headline"><h1><?php echo isset($_usr_vars['h1'])?$_usr_vars['h1']:''; ?></h1></div><!-- end/headline -->
	<div class="taxonomy-description">
		<p></p>
	</div>
	<div class="list-news">
		<?php
		$current_page = isset($_GET['page'])?$_GET['page']:1;
		$_cate = isset($_usr_vars['pagination_slug'])?$_usr_vars['pagination_slug']:'';
		$limit_per_page = 10;

		$bak_cwd = getcwd();
		chdir("_usr_posts");
		//$posts = glob("*");
		$posts = glob("[0-9]*");

		// sort by posted date -- start 	
		//$pattern = '/([1-2][0-9]{3})-(0[1-9]|1[0-2])-([0-2][1-9]|3[0-1])T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])/';
		$pattern = '/([0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]))T([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])/';
                $postx=[];
                foreach ($posts as $file_name){
                        $file_content = file_get_contents($file_name, true);
                        //print_r($file_content);
                         preg_match($pattern,$file_content,$matched);
                              $postx[$matched[0]]=$file_name;

                        //exit;

                }
                uksort($postx, function($a, $b) {
            		$c = @filectime($a);

            		$d = @filectime($b);
            		//return $d > $c;
                        return $b > $a;
                        // $c = preg_replace("/[^0-9]/","", $a);
                        // $d = preg_replace("/[^0-9]/","", $b);
                        // return $d > $c;
                });
                $posts=$postx;
		//end sort		
		$posts_qty = count($posts);
		$bloques_incluidos = 0;
		$bloques_omitidos = 0;
		$bloques_por_omitir = ($current_page-1) * $limit_per_page;

		$i = 0;
		echo '<div class="row">';
		foreach ($posts as $date=> $filename) {
			if($filename == 'index.php') continue;

			$i++;
			if($i<=$bloques_por_omitir){
				continue;
			}

			require  $filename;
			$bloques_incluidos++;

			if($bloques_incluidos%2 == 0){
				echo '</div><!-- closing row -->';
				echo '<div class="row">';
			}

			if($bloques_incluidos>=$limit_per_page) break;
		}
		echo '</div><!-- closing row -->';

		$pages_qty = intval($posts_qty / $limit_per_page);
		if($posts_qty % $limit_per_page > 0){
			$pages_qty += 1;
		}
		?>

		<div class='wp-pagenavi'>
			<?php
			$_prev_page = $current_page - 1;
			if($_prev_page > 0){
				echo "<a class='previouspostslink' rel='prev' href='https://www.usracing.com/news/{$_cate}/page/{$_prev_page}'>« Previous</a>";
			}

			$inicial = $current_page - 4;
			if($inicial<1) $inicial = 1;
			$current_page;
			$final =   $current_page + 4;
			if($final>$pages_qty) $final = $pages_qty;
			?>

			<?php
			echo "<span class='pages'>Page {$current_page} of {$pages_qty}</span>";
			if($inicial != 1){
				echo "<span class='extend'>...</span>";
			}

			for($_pagen=$inicial; $_pagen<=$final; $_pagen++){
				if($current_page == $_pagen){
					echo "<span class='current'>{$_pagen}</span>";
				} else {
					echo "<a class='page larger' href='https://www.usracing.com/news/{$_cate}/page/{$_pagen}'>{$_pagen}</a>";
				}
			}

			if($final != $pages_qty){
				echo "<span class='extend'>...</span>";
			}
			?>

			<?php
			$_next_page = $current_page + 1;
			if($_next_page <= $pages_qty){
				echo "<a class='nextpostslink' rel='next' href='https://www.usracing.com/news/{$_cate}/page/{$_next_page}'>Next »</a>";
			}
			?>
			<a id="archives-link" href="https://www.usracing.com/news/archives" > Archives</a>

			<a id="alltags-link" href="https://www.usracing.com/news/tags/"> Tags</a>

		</div>



	</div><!-- end/content -->
</div> <!-- end/ #left-col -->

<?php
chdir($bak_cwd);
