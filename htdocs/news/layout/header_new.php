<!DOCTYPE html> <html lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#"> 
<head>    
    <title><?php echo $_usr_vars["title"];?></title>


<!-- news libraries here --> 
<link rel="profile" href="http://gmpg.org/xfn/11"> 
<link rel="pingback" href="//www.usracing.com/xmlrpc.php">  
<!-- All in One SEO Pack 2.3.9.2 by Michael Torbert of Semper Fi Web Design[-1,-1] --> 
<!-- /all in one seo pack -->  
<!-- This site is optimized with the Yoast SEO plugin v4.2.1 - https://yoast.com/wordpress/plugins/seo/ --> 
<!-- / Yoast SEO plugin. -->  
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin>
<!-- <link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' /> -->
<link rel='dns-prefetch' href='//s.w.org' /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Feed" href="//www.usracing.com/feed" /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Comments Feed" href="//www.usracing.com/comments/feed" />         
<?php /*<link href='//fonts.googleapis.com/css?family=Roboto:400,700&display=swap' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap' rel='stylesheet' type='text/css'> 
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900&display=swap" rel="stylesheet" type="text/css"> 
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc&display=swap' type='text/css' media='all' />  
*/ ?>
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Roboto:400,700|Roboto+Condensed:400,700|Lato:400,300,700,900|Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc&display=swap' type='text/css' media='all' />  
<link rel="stylesheet" href="/news/layout/compressed.css?v=1.2">
<?php /*<!-- <script type="text/javascript">             
    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.usracing.com\/wp-includes\/js\/wp-emoji-release.min.js"}};             !function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);         
</script> -->   */ ?>   
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--  -->
<!-- <link rel='stylesheet' id='font-awesome-css'  href='/news/layout/font-awesome.min.css' type='text/css' media='all' /> -->

<link rel='https://api.w.org/' href='//www.usracing.com/wp-json/' /> 
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="//www.usracing.com/xmlrpc.php?rsd" /> 
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="//www.usracing.com/wp-includes/wlwmanifest.xml" />   

<link rel="canonical" href="https://www.usracing.com<?php echo $_SERVER['REQUEST_URI'];?>">




<?php echo $_usr_vars["metas"];?>  
<meta property="fb:admins" content="1352596260"/>
<meta property="fb:admins" content="1042976107"/>
<?php
$ampUrl= str_replace('/news/','/news/amp/',$_SERVER["REQUEST_URI"]);
$ampcheck='/home/ah/usracing.com/htdocs/'.$ampUrl.'.php';
if (file_exists($ampcheck)) { echo '<link rel="amphtml" href="https://'.$_SERVER['SERVER_NAME'].$ampUrl.'" />'; }
?>



<script></script>

</head>  
    <body class="<?php echo $_usr_vars["classes"];?>"> 
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
        require('/home/ah/allhorse/public_html/menusyncrusr.php');
    ?>
<?php //include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
<?php include('/home/ah/usracing.com/htdocs/news/layout/hero.php'); ?>