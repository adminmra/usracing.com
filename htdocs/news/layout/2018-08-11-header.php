<!DOCTYPE html> <html lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#"> 
<head>    
    <title><?php echo $_usr_vars["title"];?></title>
<style>
    .edit-link{display:none !important;}
</style>
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'> 
<!-- news libraries here --> 
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">   
<link rel="profile" href="http://gmpg.org/xfn/11"> 
<link rel="pingback" href="//www.usracing.com/xmlrpc.php">  
<!-- All in One SEO Pack 2.3.9.2 by Michael Torbert of Semper Fi Web Design[-1,-1] --> 
<!-- /all in one seo pack -->  
<!-- This site is optimized with the Yoast SEO plugin v4.2.1 - https://yoast.com/wordpress/plugins/seo/ --> 
<!-- / Yoast SEO plugin. -->  
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' /> 
<link rel='dns-prefetch' href='//s.w.org' /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Feed" href="//www.usracing.com/feed" /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Comments Feed" href="//www.usracing.com/comments/feed" />         

<script type="text/javascript">             
    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.usracing.com\/wp-includes\/js\/wp-emoji-release.min.js"}};             !function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);         
</script>       

<style type="text/css"> 
    img.wp-smiley, img.emoji {  display: inline !important;     border: none !important;    box-shadow: none !important;    height: 1em !important;     width: 1em !important;  margin: 0 .07em !important;     vertical-align: -0.1em !important;  background: none !important;    padding: 0 !important; } 
</style> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet' id='apss-font-awesome-css'  href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc' type='text/css' media='all' /> 
<link rel='stylesheet' id='apss-frontend-css-css'  href='//www.usracing.com/wp-content/plugins/accesspress-social-share/css/frontend.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wp-pagenavi-css'  href='//www.usracing.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-bootstrap-css'  href='//www.usracing.com/../assets/plugins/bootstrap/css/bootstrap.min.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-assets-style-css'  href='//www.usracing.com/../assets/css/style.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-style-css'  href='//www.usracing.com/wp-content/themes/usracing/style.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-style-plugins-css'  href='//www.usracing.com/../assets/css/plugins.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='acfpb-public-css'  href='//www.usracing.com/wp-content/plugins/acf-page-builder/includes/../public/css/acfpb_styles.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_font-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_font.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_flat-icons-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_flat-icons.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' type='text/css' media='all' /> 
<script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery-migrate.min.js'></script> 
<script type='text/javascript' src='//www.usracing.com/../assets/plugins/bootstrap/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='https://www.usracing.com/assets/js/news.js'></script> 
<link rel='https://api.w.org/' href='//www.usracing.com/wp-json/' /> 
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="//www.usracing.com/xmlrpc.php?rsd" /> 
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="//www.usracing.com/wp-includes/wlwmanifest.xml" />   
<script type="text/javascript"> 
    (function(url){     if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }  var addEvent = function(evt, handler) {         if (window.addEventListener) {          document.addEventListener(evt, handler, false);         } else if (window.attachEvent) {            document.attachEvent('on' + evt, handler);      }   };  var removeEvent = function(evt, handler) {      if (window.removeEventListener) {           document.removeEventListener(evt, handler, false);      } else if (window.detachEvent) {            document.detachEvent('on' + evt, handler);      }   };  var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');  var logHuman = function() {         var wfscr = document.createElement('script');       wfscr.type = 'text/javascript';         wfscr.async = true;         wfscr.src = url + '&r=' + Math.random();        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);        for (var i = 0; i < evts.length; i++) {             removeEvent(evts[i], logHuman);         }   };  for (var i = 0; i < evts.length; i++) {         addEvent(evts[i], logHuman);    } })('//www.usracing.com/?wordfence_logHuman=1&hid=7A7E3E091E768F9BB41A4FCDA4E853DE'); 
</script>
<!-- START - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<!-- Facebook Open Graph -->                    
<!-- Google+ / Schema.org -->        
<!-- Twitter Cards -->              
<!-- SEO -->     
<!-- Misc. tags --> 
<!-- END - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<script type="text/javascript" src="//www.usracing.com/wp-content/themes/usracing/js/put5qvj.js"></script> 
<script type="text/javascript">try{Typekit.load();}catch(e){}</script> 
<link rel='stylesheet' id='sab-plugin-css'  href='//usracing.com/wp-content/plugins/simple-author-box/css/simple-author-box.min.css' type='text/css' media='all' /> 
<link href="//usracing.com/news/wp-content/themes/usracing/css/news.css" rel="stylesheet" type="text/css">
<?php echo $_usr_vars["metas"];?>  
<meta property="fb:admins" content="1352596260"/>
<meta property="fb:admins" content="1042976107"/>
</head>  
    <body class="<?php echo $_usr_vars["classes"];?>">   

    <!-- Top --> 
    <div id="top" class="top">  
        <div class="container2">      
            <div class="navbar-header pull-left">       
            <!--     <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>     <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>     <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->      
            <a id="or" class="navbar-toggle collapsed">
                <span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span>
                <i class="glyphicon glyphicon-remove"></i>
            </a>
            </div>   
            <a class="logo logo-lrg" href="/" title="US Racing"><img id="logo-header" src="/img/usracing.png" alt="logo"></a>   
            <a class="logo logo-sm" href="/" rel="nofollow"><img src="/img/usracing-sm.png"></a>   

            <div class="topR"></div>    
            <div class="login-header">     
            <!--     <a class="btn btn-sm btn-default loginToggle collapsed" data-toggle="collapse" data-target=".login-responsive-collapse" rel="nofollow"><span>Login</span><i class="fa fa-angle-down"></i></a>     -->

            <a href='//www.betusracing.ag/login' class="btn btn-sm btn-default" id="login_button">Login</a>      
            <a class="btn btn-sm btn-red" id="signupBtn" href="//www.betusracing.ag/signup?ref=news" rel="nofollow" >         
                <span>Sign Up</span> 
                <i class="glyphicon glyphicon-pencil"></i>     
            </a> 
            </div>  </div> </div>

            <!--/top-->  
            <!-- Nav --> 
            <div id="nav">  
                <a id="navigation" name="navigation"></a>   
                <div class="navbar navbar-default" role="navigation">   
                    <div class="container">       
                    <div class="collapse navbar-collapse navbar-responsive-collapse">   
                        <ul class="nav navbar-nav nav-justified">  
                            <?php
                            require('/home/ah/allhorse/public_html/menusyncr.php');
                            ?>

                        </ul>    
                    </div>   
                    </div>  
                </div> 
            </div> 
<?php include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
