<!DOCTYPE html> <html lang="en-US" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://ogp.me/ns/fb#"> 
<head>    
    <title><?php echo $_usr_vars["title"];?></title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-K74Z827');</script>
    <!-- End Google Tag Manager -->
<style>
    .edit-link{display:none !important;}
</style>
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'> 
<!-- news libraries here --> 
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">   
<link rel="profile" href="http://gmpg.org/xfn/11"> 
<link rel="pingback" href="//www.usracing.com/xmlrpc.php">  
<!-- All in One SEO Pack 2.3.9.2 by Michael Torbert of Semper Fi Web Design[-1,-1] --> 
<!-- /all in one seo pack -->  
<!-- This site is optimized with the Yoast SEO plugin v4.2.1 - https://yoast.com/wordpress/plugins/seo/ --> 
<!-- / Yoast SEO plugin. -->  
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel='dns-prefetch' href='//netdna.bootstrapcdn.com' /> 
<link rel='dns-prefetch' href='//s.w.org' /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Feed" href="//www.usracing.com/feed" /> 
<link rel="alternate" type="application/rss+xml" title="Horse Racing News &raquo; Comments Feed" href="//www.usracing.com/comments/feed" />         
<link rel="stylesheet" href="/assets/css/as-seen-on-new-tpl-news-min.css?v=1.1">
<script type="text/javascript">             
    window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.usracing.com\/wp-includes\/js\/wp-emoji-release.min.js"}};             !function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);         
</script>       

<style type="text/css"> 
    img.wp-smiley, img.emoji {  display: inline !important;     border: none !important;    box-shadow: none !important;    height: 1em !important;     width: 1em !important;  margin: 0 .07em !important;     vertical-align: -0.1em !important;  background: none !important;    padding: 0 !important; } 
</style>


<!-- Author Description -->
<style type="text/css">.saboxplugin-wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;border:1px solid #eee;width:100%;clear:both;display:block;overflow:hidden;word-wrap:break-word;position:relative}.saboxplugin-wrap .saboxplugin-gravatar{float:left;padding:20px;}.saboxplugin-wrap .saboxplugin-gravatar img{max-width:100px;height:auto;border-radius:0;}.saboxplugin-wrap .saboxplugin-authorname{font-size:18px;line-height:1;margin:20px 0 0 20px;display:block}.saboxplugin-wrap .saboxplugin-authorname a{text-decoration:none}.saboxplugin-wrap .saboxplugin-authorname a:focus{outline:0}.saboxplugin-wrap .saboxplugin-desc{display:block;margin:5px 20px}.saboxplugin-wrap .saboxplugin-desc a{text-decoration:underline}.saboxplugin-wrap .saboxplugin-desc p{margin:5px 0 12px}.saboxplugin-wrap .saboxplugin-web{margin:0 20px 15px;text-align:left}.saboxplugin-wrap .sab-web-position{text-align:right}.saboxplugin-wrap .saboxplugin-web a{color:#ccc;text-decoration:none}.saboxplugin-wrap .saboxplugin-socials{position:relative;display:block;background:#fcfcfc;padding:5px;border-top:1px solid #eee}.saboxplugin-wrap .saboxplugin-socials a svg{width:20px;height:20px}.saboxplugin-wrap .saboxplugin-socials a svg .st2{fill:#fff}.saboxplugin-wrap .saboxplugin-socials a svg .st1{fill:rgba(0,0,0,.3)}.saboxplugin-wrap .saboxplugin-socials a:hover{opacity:.8;-webkit-transition:opacity .4s;-moz-transition:opacity .4s;-o-transition:opacity .4s;transition:opacity .4s;box-shadow:none!important;-webkit-box-shadow:none!important}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-color{box-shadow:none;padding:0;border:0;-webkit-transition:opacity .4s;-moz-transition:opacity .4s;-o-transition:opacity .4s;transition:opacity .4s;display:inline-block;color:#fff;font-size:0;text-decoration:inherit;margin:5px;-webkit-border-radius:0;-moz-border-radius:0;-ms-border-radius:0;-o-border-radius:0;border-radius:0;overflow:hidden}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-grey{text-decoration:inherit;box-shadow:none;position:relative;display:-moz-inline-stack;display:inline-block;vertical-align:middle;zoom:1;margin:10px 5px;color:#444}.clearfix:after,.clearfix:before{content:' ';display:table;line-height:0;clear:both}.ie7 .clearfix{zoom:1}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-twitch{border-color:#38245c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-addthis{border-color:#e91c00}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-behance{border-color:#003eb0}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-delicious{border-color:#06c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-deviantart{border-color:#036824}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-digg{border-color:#00327c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-dribbble{border-color:#ba1655}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-facebook{border-color:#1e2e4f}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-flickr{border-color:#003576}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-github{border-color:#264874}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-google{border-color:#0b51c5}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-googleplus{border-color:#96271a}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-html5{border-color:#902e13}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-instagram{border-color:#1630aa}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-linkedin{border-color:#00344f}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-pinterest{border-color:#5b040e}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-reddit{border-color:#992900}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-rss{border-color:#a43b0a}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-sharethis{border-color:#5d8420}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-skype{border-color:#00658a}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-soundcloud{border-color:#995200}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-spotify{border-color:#0f612c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-stackoverflow{border-color:#a95009}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-steam{border-color:#006388}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-user_email{border-color:#b84e05}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-stumbleUpon{border-color:#9b280e}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-tumblr{border-color:#10151b}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-twitter{border-color:#0967a0}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-vimeo{border-color:#0d7091}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-windows{border-color:#003f71}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-wordpress{border-color:#0f3647}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-yahoo{border-color:#14002d}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-youtube{border-color:#900}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-xing{border-color:#000202}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-mixcloud{border-color:#2475a0}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-vk{border-color:#243549}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-medium{border-color:#00452c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-quora{border-color:#420e00}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-meetup{border-color:#9b181c}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-goodreads{border-color:#000}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-snapchat{border-color:#999700}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-500px{border-color:#00557f}.saboxplugin-socials.sabox-colored .saboxplugin-icon-color .sab-mastodont{border-color:#185886}.sabox-plus-item{margin-bottom:20px}@media screen and (max-width:480px){.saboxplugin-wrap{text-align:center}.saboxplugin-wrap .saboxplugin-gravatar{float:none;padding:20px 0;text-align:center;margin:0 auto;display:block}.saboxplugin-wrap .saboxplugin-gravatar img{float:none;display:inline-block;display:-moz-inline-stack;vertical-align:middle;zoom:1}.saboxplugin-wrap .saboxplugin-desc{margin:0 10px 20px;text-align:center}.saboxplugin-wrap .saboxplugin-authorname{text-align:center;margin:10px 0 20px}}body .saboxplugin-authorname a,body .saboxplugin-authorname a:hover{box-shadow:none;-webkit-box-shadow:none}a.sab-profile-edit{font-size:16px!important;line-height:1!important}.sab-edit-settings a,a.sab-profile-edit{color:#0073aa!important;box-shadow:none!important;-webkit-box-shadow:none!important}.sab-edit-settings{margin-right:15px;position:absolute;right:0;z-index:2;bottom:10px;line-height:20px}.sab-edit-settings i{margin-left:5px}.saboxplugin-socials{line-height:1!important}.rtl .saboxplugin-wrap .saboxplugin-gravatar{float:right}.rtl .saboxplugin-wrap .saboxplugin-authorname{display:flex;align-items:center}.rtl .saboxplugin-wrap .saboxplugin-authorname .sab-profile-edit{margin-right:10px}.rtl .sab-edit-settings{right:auto;left:0}img.sab-custom-avatar{max-width:75px;}.saboxplugin-wrap .saboxplugin-gravatar img {-webkit-border-radius:50%;-moz-border-radius:50%;-ms-border-radius:50%;-o-border-radius:50%;border-radius:50%;}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-color .st1 {display: none;}.saboxplugin-wrap .saboxplugin-socials .saboxplugin-icon-color svg {border-width: 1px;border-style:solid;}.saboxplugin-wrap {margin-top:0px; margin-bottom:0px; padding: 0px 0px }.saboxplugin-wrap .saboxplugin-authorname {font-size:18px; line-height:25px;}.saboxplugin-wrap .saboxplugin-desc p, .saboxplugin-wrap .saboxplugin-desc {font-size:14px !important; line-height:21px !important;}.saboxplugin-wrap .saboxplugin-web {font-size:14px;}.saboxplugin-wrap .saboxplugin-socials a svg {width:36px;height:36px;}</style>

<!-- End Author Description -->

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet' id='apss-font-awesome-css'  href='//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='apss-font-opensans-css'  href='//fonts.googleapis.com/css?family=Open+Sans&#038;ver=5819f56549acca8a9dab2c4f32f09fbc' type='text/css' media='all' /> 
<link rel='stylesheet' id='apss-frontend-css-css'  href='//www.usracing.com/wp-content/plugins/accesspress-social-share/css/frontend.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='wp-pagenavi-css'  href='//www.usracing.com/wp-content/plugins/wp-pagenavi/pagenavi-css.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='_usr-bootstrap-css'  href='//www.usracing.com/../assets/plugins/bootstrap/css/bootstrap.min.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-assets-style-css'  href='//www.usracing.com/../assets/css/style.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='_usr-style-css'  href='//www.usracing.com/wp-content/themes/usracing/style.css' type='text/css' media='all' media="none" onload="if(media!='all')media='all'"/> 
<link rel='stylesheet' id='_usr-assets-style-css'  href='//www.usracing.com/../assets/css/style.min.css?v2.2' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-style-css'  href='//www.usracing.com/wp-content/themes/usracing/style.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='_usr-style-plugins-css'  href='//www.usracing.com/../assets/css/plugins.min.css?v2.4' type='text/css' media='all' /> 
<link rel='stylesheet' id='acfpb-public-css'  href='//www.usracing.com/wp-content/plugins/acf-page-builder/includes/../public/css/acfpb_styles.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_font-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_font.css' type='text/css' media='all' /> 
<link rel='stylesheet' id='wavesurfer_flat-icons-css'  href='//www.usracing.com/wp-content/plugins/wavesurfer-wp/css/wavesurfer-wp_flat-icons.css' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' type='text/css' media='all' /> 
<script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery.js'></script>
<script type='text/javascript' src='//www.usracing.com/wp-includes/js/jquery/jquery-migrate.min.js'></script> 
<script type='text/javascript' src='//www.usracing.com/../assets/plugins/bootstrap/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='https://www.usracing.com/assets/js/news.js'></script> 
<link rel='https://api.w.org/' href='//www.usracing.com/wp-json/' /> 
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="//www.usracing.com/xmlrpc.php?rsd" /> 
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="//www.usracing.com/wp-includes/wlwmanifest.xml" />   

<link rel="canonical" href="https://www.usracing.com<?php echo $_SERVER['REQUEST_URI'];?>">

<script type="text/javascript"> 
    (function(url){     if(/(?:Chrome\/26\.0\.1410\.63 Safari\/537\.31|WordfenceTestMonBot)/.test(navigator.userAgent)){ return; }  var addEvent = function(evt, handler) {         if (window.addEventListener) {          document.addEventListener(evt, handler, false);         } else if (window.attachEvent) {            document.attachEvent('on' + evt, handler);      }   };  var removeEvent = function(evt, handler) {      if (window.removeEventListener) {           document.removeEventListener(evt, handler, false);      } else if (window.detachEvent) {            document.detachEvent('on' + evt, handler);      }   };  var evts = 'contextmenu dblclick drag dragend dragenter dragleave dragover dragstart drop keydown keypress keyup mousedown mousemove mouseout mouseover mouseup mousewheel scroll'.split(' ');  var logHuman = function() {         var wfscr = document.createElement('script');       wfscr.type = 'text/javascript';         wfscr.async = true;         wfscr.src = url + '&r=' + Math.random();        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(wfscr);        for (var i = 0; i < evts.length; i++) {             removeEvent(evts[i], logHuman);         }   };  for (var i = 0; i < evts.length; i++) {         addEvent(evts[i], logHuman);    } })('//www.usracing.com/?wordfence_logHuman=1&hid=7A7E3E091E768F9BB41A4FCDA4E853DE'); 
</script>
<!-- START - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<!-- Facebook Open Graph -->                    
<!-- Google+ / Schema.org -->        
<!-- Twitter Cards -->              
<!-- SEO -->     
<!-- Misc. tags --> 
<!-- END - Facebook Open Graph, Google+ and Twitter Card Tags 2.0.7 -->  
<script type="text/javascript" src="//www.usracing.com/wp-content/themes/usracing/js/put5qvj.js"></script>
<!-- <script type="text/javascript">try{Typekit.load();}catch(e){}</script> 2018-09-24 Herbert--> 
<link rel='stylesheet' id='sab-plugin-css'  href='https://www.usracing.com/wp-content/plugins/simple-author-box/css/simple-author-box.min.css' type='text/css' media='all' /> 
<link href="https://www.usracing.com/news/wp-content/themes/usracing/css/news.css" rel="stylesheet" type="text/css" media="none" onload="if(media!='all')media='all'">
<?php //echo $_usr_vars["metas"];?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Get the latest 2021 Breeders' Cup news and updates on horses, jockeys, trainers and contenders of the Thoroughbred World Championships on November 5th and 6th">

<meta name="keywords" content="Breeders Cup Odds, Breeders Cup betting, Breeders Cup Results, Breeders Cup Santa Anita, BC 2020, 2021 Breeders Cup, Breeders Cup 2021, Online Horse Betting, Bet on Horses, Thorougbred Betting">

<meta property="fb:admins" content="1352596260"/>
<meta property="fb:admins" content="1042976107"/>


<style>
    #signupBtn {
        background-color: #e2001a;
        background-image: -webkit-linear-gradient(top, #e2001a, #991a1e);
        background-image: linear-gradient(to bottom, #e2001a, #991a1e);
        border-radius: 3px;
        border: 0;
        display: inline-block;
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        font-weight: 900;
        outline: none;
        text-align: center;
        text-decoration: none;
        min-width: 100px;
        -webkit-transition: .2s;
        transition: .2s;
        transition: .2s
    }

    #signupBtn:hover {
        background: #d6242a;
    }

    #signupBtn:focus, #signupBtn:hover {
        color:#fff;font-weight:900;
        text-decoration:none;
    }

    #signupBtn:active {
        -webkit-transform:scale(0.98);transform:scale(0.98);
    }
</style>
<!-- MailMunch Code -->
<script src="//a.mailmunch.co/app/v1/site.js" id="mailmunch-script" data-mailmunch-site-id="706684" async="async"></script>
</head>  
    <body class="<?php echo $_usr_vars["classes"];?>"> 
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php
        require('/home/ah/allhorse/public_html/menusyncrusr.php');
    ?>
<?php include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
