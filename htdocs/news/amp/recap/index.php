<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $_usr_vars = array(
	'title' => 'Race Recap | Horse Racing News',
	'h1' => 'Race Recap',
	'visible_latest_news' => false,
	'visible_cta_handicapping' => true,
	'classes' => 'archive category category-analysis category-355',
	'pagination_slug' => 'recap',	
	'metas' => '',
); 
$amp_vars["metas"]='<meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="description" content="US Racing Analaysis"/><meta name="robots" content="noodp"/><meta property="fb:app_id" content="825577710919578"/><meta property="og:locale" content="en_US"/><meta property="og:site_name" content="Horse Racing News"/><meta property="og:title" content="Horse Racing News Analaysis"/><meta property="og:url" content="https://www.usracing.com"/><meta property="og:type" content="website"/><meta property="og:description" content="US Racing has the latest award winning horse racing news and analysis."/><meta itemprop="name" content="Horse Racing News"/><meta itemprop="description" content="US Racing has the latest award winning horse racing news and analysis."/><meta name="twitter:title" content="Horse Racing News"/><meta name="twitter:url" content="https://www.usracing.com"/><meta name="twitter:description" content="US Racing has the latest award winning horse racing news and analysis."/><meta name="twitter:card" content="summary_large_image"/><meta name="description" content="US Racing has the latest award winning horse racing news and analysis."/>';

?>
<?php $canonical='https://www.usracing.com/news/recap'; ?>
<?php $pagetitle='Race Recap'; ?>
<?php //include "/home/ah/usracing.com/htdocs/news/layout/header.php";?>
<?php //include("/home/ah/usracing.com/htdocs/news/layout/cta_handicapping_form.php"); ?>
<?php // include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/amp/header.php"); ?>

<div id="main" class="container2 news-content"><div class="row_post">
<?php include "/home/ah/usracing.com/htdocs/news/layout/amp/body_category_cat.php";?>
<?php //include "/home/ah/usracing.com/htdocs/news/layout/sidebar.php";?>
</div><!-- end/row --></div><!-- end/main -->
<?php include("/home/ah/usracing.com/htdocs/news/layout/amp/footer.php"); ?>

<?php //include("/home/ah/usracing.com/htdocs/news/layout/footer.php");
//echo file_get_contents('https://' . $_SERVER['SERVER_NAME'] . '/newshtmlfooter');
?>
