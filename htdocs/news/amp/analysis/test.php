<?php header("Content-Type: text/html; charset=utf-8"); ?>
<?php $amp_vars["metas"]='<meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="description" content="Cont&#8230;" /><meta property="og:locale" content="en_US" /><meta property="og:type" content="website" /><meta property="og:title" content="Test | Horse Racing News" /><meta property="og:description" content="Cont&#8230;" /><meta property="og:url" content="https://www.usracing.com/news/analysis/test" /><meta property="og:site_name" content="Horse Racing News" /><meta name="twitter:card" content="summary_large_image" /><meta name="twitter:title" content="Test | Horse Racing News" /><meta name="twitter:description" content="Cont&#8230;" /><meta name="robots" content="index, follow" /><meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" /><meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" /><meta property="og:locale" content="en_US" /><meta property="og:type" content="article" /><meta property="og:title" content="Test | Horse Racing News" /><meta property="og:description" content="Cont Admin" /><meta property="og:url" content="https://www.usracing.com/news" /><meta property="og:site_name" content="Horse Racing News" /><meta property="article:published_time" content="2021-08-18T19:25:39+00:00" /><meta property="fb:app_id" content="825577710919578" />'; ?>
<?php $amp_vars["structure"]='<script type="application/ld+json">
{
      "@context": "http://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": "https://www.usracing.com/news/amp/analysis/test",
      "headline": "Test",
      "datePublished": "2021-08-18 19:25:39",
      "dateModified": "2021-08-18 19:25:39",
      "description": "",
      "author": {
        "@type": "Person",
        "name": "Admin"
      },
      "publisher": {
        "@type": "Organization",
        "name": "US Racing",
        "logo": {
          "@type": "ImageObject",
          "url": "https://www.usracing.com/img/us-racing.png",
          "width": 508,
          "height": 103
        }
      },
      "image": {
        "@type": "ImageObject",
        "url": ""
      }
    }
      </script>'; ?>
<?php $canonical='https://www.usracing.com/news/analysis/test'; ?>
<?php $pagetitle='Test'; ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/amp/header.php"); ?>
<div class="headline"><h1>Test</h1></div> <div class="blog-date-author"><time class="entry-date published" datetime="2021-08-18T19:25:39+00:00">August 18, 2021</time></span></span></div><article id="post-13037" class="post-13037 post type-post status-publish format-standard hentry category-analysis"> 	  		<p>Cont</p>  <div class="saboxplugin-wrap" itemtype="http://schema.org/Person" itemscope itemprop="author"><div class="saboxplugin-gravatar"><amp-img  width="100" height="100"  alt='' src='https://1.gravatar.com/avatar/1612f6231d1073db91e024069bea7910?s=100&amp;d=http%3A%2F%2F1.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D100&amp;r=G' class='avatar avatar-100 photo' height='100' width='100'   layout="responsive" data-amp-auto-lightbox-disable ></amp-img></div><div class="saboxplugin-authorname"><a href="https://www.usracing.com/news/author/guillermo" class="vcard author" rel="author" itemprop="url"><span class="fn" itemprop="name">Admin</span></a></div><amp-truncate-text layout="fixed-height" height="15em" ><div class="saboxplugin-desc"><div itemprop="description"></div></div><div class="clearfix"></div><a slot="collapsed" class="readlink" >Read More</a> <a slot="expanded" class="readmore">Read Less</a></amp-truncate-text> </div><div class="apss-social-share apss-theme-2 clearfix" ><div  class="apss-social-center" >  				<div class='apss-facebook apss-single-icon'>  					<a rel='nofollow'  title="Share on Facebook" target='_blank' href='https://www.facebook.com/sharer/sharer.php?u=https://www.usracing.com/news/analysis/test'>  						<div class='apss-icon-block clearfix'>  							<i class='fab fa-facebook-f'></i>  							<span class='apss-social-text'>Share on Facebook</span>  							<span class='apss-share'>Share</span>  						</div>  											</a>  				</div>  								<div class='apss-twitter apss-single-icon'>  					<a rel='nofollow'  href="https://twitter.com/intent/tweet?text=Test&amp;url=https://www.usracing.com/news%2Fnews%2Fanalysis%2Ftest&amp;"  title="Share on Twitter" target='_blank'>  						<div class='apss-icon-block clearfix'>  							<i class='fab fa-twitter'></i>  							<span class='apss-social-text'>Share on Twitter</span><span class='apss-share'>Tweet</span>  						</div>  											</a>  				</div>  								<div class='apss-email apss-single-icon'>  					<a rel='nofollow' class='share-email-popup' title="Share it on Email" target='_blank' href='mailto:?subject=Please visit this link https://www.usracing.com/news/analysis/test&amp;body=Hey Buddy!, I found this information for you: "Test". Here is the website link: https://www.usracing.com/news/analysis/test. Thank you.'>  						<div class='apss-icon-block clearfix'>  							<i class='fas fa-envelope'></i>  							<span class='apss-social-text'>Send email</span>  							<span class='apss-share'>Mail</span>  						</div></div>  					</a>  				</div>    				</div>		</article><?php include("/home/ah/usracing.com/htdocs/news/layout/amp/footer.php"); ?>
