<?php header("Content-Type: text/html; charset=utf-8"); ?>
<?php $_usr_vars=array (
  'title' => 'Kentucky Derby Future Wager | Horse Racing News',
  'h1' => 'Kentucky Derby Future Wager',
  'visible_latest_news' => false,
  'visible_cta_handicapping' => true,
  'classes' => 'archive tag',
  'cta_form' => true,
  'pagination_slug' => 'tag/kentucky-derby-future-wager',
); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/header.php"); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/cta_handicapping_form.php"); ?>
<div id="main" class="container2 news-content"><div class="row">
<?php include("/home/ah/usracing.com/htdocs/news/layout/body_category.php"); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/sidebar.php"); ?>
</div><!-- end/row --></div><!-- end/main -->
<?php include("/home/ah/usracing.com/htdocs/news/layout/footer.php"); ?>
