<?php header("Content-Type: text/html; charset=utf-8");
 $_usr_vars = array (
  'title' => '﻿﻿  Jeff Ruby Steaks Draws Twelve Looking for Derby Points | Horse Racing News',
  'visible_latest_news' => true,
  'visible_cta_handicapping' => true,
  'classes' => 'single single-post postid-10334 single-format-standard logged-in admin-bar no-customize-support',
  'cta_form' => false,
  'metas' => '<meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta property="fb:app_id" content="398008810636127"/><meta property="fb:admins" content="1352596260"/><meta property="fb:admins" content="1042976107"/><meta name="news_keywords" content="Archagellos, Hazit, Jeff Ruby Steaks, Kentucky Derby preps, Ride A Comet, Road to the Kentucky Derby" /><meta property="og:locale" content="en_US"/><meta property="og:site_name" content="Horse Racing News"/><meta property="og:title" content="﻿﻿ Jeff Ruby Steaks Draws Twelve Looking for Derby Points"/><meta property="og:url" content="https://www.usracing.com/news/analysis/%ef%bb%bf%ef%bb%bf-jeff-ruby-steaks-draws-twelve-looking-derby-points"/><meta property="og:type" content="article"/><meta property="og:description" content="A full field of 12 is expected to head postward in Saturday’s inaugural running of the Jeff Ruby Steaks (GIII) at Turfway Park, this year’s rebranding of the Kentucky Derby (GI) prep race known previously as the Spiral Stakes, Jim Beam Stakes, Lane’s End Stakes and, once, the galleryfurniture.com St"/><meta property="og:image" content="https://www.usracing.com/news/wp-content/uploads/2018/03/Archaggelos-Michael-Burns-Photography.jpg"/><meta property="og:image:width" content="650"/><meta property="og:image:height" content="433"/><meta property="article:published_time" content="2018-03-16T14:00:43+00:00"/><meta property="article:modified_time" content="2018-03-16T14:00:43+00:00" /><meta property="og:updated_time" content="2018-03-16T14:00:43+00:00" /><meta property="article:section" content="Racing Analysis"/><meta property="article:author" content="https://www.facebook.com/redransom"/><meta itemprop="name" content="﻿﻿ Jeff Ruby Steaks Draws Twelve Looking for Derby Points"/><meta itemprop="description" content="A full field of 12 is expected to head postward in Saturday’s inaugural running of the Jeff Ruby Steaks (GIII) at Turfway Park, this year’s rebranding of the Kentucky Derby (GI) prep race known previously as the Spiral Stakes, Jim Beam Stakes, Lane’s End Stakes and, once, the galleryfurniture.com St"/><meta itemprop="image" content="https://www.usracing.com/news/wp-content/uploads/2018/03/Archaggelos-Michael-Burns-Photography.jpg"/><meta name="twitter:title" content="﻿﻿ Jeff Ruby Steaks Draws Twelve Looking for Derby Points"/><meta name="twitter:url" content="https://www.usracing.com/news/analysis/%ef%bb%bf%ef%bb%bf-jeff-ruby-steaks-draws-twelve-looking-derby-points"/><meta name="twitter:description" content="A full field of 12 is expected to head postward in Saturday’s inaugural running of the Jeff Ruby Steaks (GIII) at Turfway Park, this year’s rebranding of the Kentucky Derby (GI) prep race known previously as the Spiral Stakes, Jim Beam Stakes, Lane’s End Stakes and, once, the galleryfurniture.com St"/><meta name="twitter:image" content="https://www.usracing.com/news/wp-content/uploads/2018/03/Archaggelos-Michael-Burns-Photography.jpg"/><meta name="twitter:card" content="summary_large_image"/><meta name="twitter:creator" content="@https://twitter.com/MargaretRansom"/><meta name="description" content="A full field of 12 is expected to head postward in Saturday’s inaugural running of the Jeff Ruby Steaks (GIII) at Turfway Park, this year’s rebranding of the Kentucky Derby (GI) prep race known previously as the Spiral Stakes, Jim Beam Stakes, Lane’s End Stakes and, once, the galleryfurniture.com St"/>',
) ?><?php include("/home/ah/usracing.com/htdocs/news/layout/header.php")?> <div id="left-col" class="col-medium-9"> <div class="widgetheader"> 				<div class="categoriesleft"> 				<select name="forma" onchange="location = this.value;"> 					 <option value="/news">Latest Racing News</option> 					 <option value="/news/features">US Racing Features</option> 					 <option value="/news/analysis">Racing Analysis</option> 					 <option value="/news/handicapping-reports">Handicapping Reports</option> 					 <option value="/news/breeders-cup">Breeders' Cup News</option> 					 <option value="/news/kentucky-derby-road-to-the-roses">Kentucky Derby Road</option> 					 <option value="/news/recap">Race Recap</option> 					 <option value="/news/nfl">NFL Betting</option> 				</select> 				</div> 				<div class="searchright"> 				<div class="widget widget_search"><form role="search" method="get" class="search-form" action="https://www.usracing.com/news/"> 	<label> 		<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:"> 	</label> 	<input type="submit" class="search-submit" value="Search"> </form> </div>				</div> 	 			</div> </div> <div id="main" class="container2 news-content"> <div class="row"> <div id="left-col" class="col-medium-9">   				   <div class="headline"><h1>  Jeff Ruby Steaks Draws Twelve Looking for Derby Points</h1></div> <div class="content"> <article id="post-10334" class="post-10334 post type-post status-publish format-standard has-post-thumbnail hentry category-analysis tag-archagellos tag-hazit tag-jeff-ruby-steaks tag-kentucky-derby-preps tag-ride-a-comet tag-road-to-the-kentucky-derby"> 	  		<p style="text-align: justify;">A full field of 12 is expected to head postward in Saturday’s inaugural running of the Jeff Ruby Steaks (GIII) at Turfway Park, this year’s rebranding of the Kentucky Derby (GI) prep race known previously as the Spiral Stakes, Jim Beam Stakes, Lane’s End Stakes and, once, the galleryfurniture.com Stakes.</p> <p style="text-align: justify;">Jeff Ruby’s Steakhouse has been a big supporter of horse racing for years now and fans can frequently see the company’s logo on the pants of many jockeys, including Hall of Famers Gary Stevens and Mike Smith.</p> <p style="text-align: justify;">The nine-furlong Jeff Ruby Steaks is the second-to-last race offering just 20 points to the winner (the Lexington Stakes at Keeneland in late April is the last), as the values are increasing, so anyone hoping to gain any points for their runner needs a victory to consider continuing on the <a href="https://www.usracing.com/kentucky-derby/prep-races">Kentucky Derby Trail</a>.</p> <p style="text-align: justify;">The race is probably best known for Animal Kingdom, who captured the nine-furlong synthetic test as his final prep for the Run for the Roses, but that’s not to say some good horses haven’t won the race as part of their tremendous careers. Hard Spun, who placed in all three Triple Crown races, won here in 2007 and added his name to a list that includes dual classic winners Hansel and Prairie Bayou, Derby winner Lil E Tee and Preakness Stakes winner Hansel.</p> <p style="text-align: justify;">Saturday’s weather in Northern Kentucky is expected to be a little dicey with rain and thundershowers likely and a high only reaching into the mid- to upper-40s. Since the main track is made of Polytrack, the only effect the weather will have will likely be on the patrons.</p> <p style="text-align: justify;">Eclipse Thoroughbreds, Magnier, Tabor and Smith’s Hazit is the lukewarm co-morning line favorite off a decent second in a Tampa Bay Downs allowance race just more than a month ago. He only owns a maiden win and was unsuccessful in stakes company twice before (including a dull eighth in the Breeders’ Cup Juvenile), but got a little winter freshening and has been training well at trainer Todd Pletcher’s Palm Beach Downs winter base. This $430,000 son of War Front has posted some decent speed, pace and class figures and looks to be on the improve.</p> <p style="text-align: justify;">He drew a decent post to get to what appears to be his preferred spot on the lead. He makes his synthetic debut, but so do many of his chief rivals, and he’s bred to like the distance and may be rounding into form at the right time. Southern California jockey Drayden VanDyke rides for the first time.</p> <p style="text-align: justify;">Michael Dickinson will tighten the girth on Monticule’s homebred Archagellos, who is a half-brother (by Temple City) to 2008 Kentucky Derby winner Big Brown. The Canadian Grade 3 winner is coming off a layoff of more than three months, but, as any racing historian knows, a layoff is really no problem for a horseman like Dickinson (remember Da Hoss?). His last was a disappointing fourth-place finish, but he did have a tough trip and was wide most of the way, so anything smoother moves him up. The distance should be of little concern and he’s a winner over Woodbine’s synthetic surface, which adds to his training regimen at his trainer’s Tapeta Farm. He could be improving at the right time, though his overall figures could be better. Rafael Hernandez rides again and they’ll get the pace they like in front of them, so this may be the case of a clean trip and putting it all together at the right time.</p> <p style="text-align: justify;">John C. Oxley and Meadowview Farm’s Ride A Comet is carrying a two-race win streak into Saturday’s contest, including a turf allowance last time at Fair Grounds. The son of Candy Ride and the Grade 1 winner Appealing Zophie (Successful Appeal) has posted some big numbers, including a big 94 Brisnet speed rating for his maiden score, and he is a winner already around two turns. Mark Casse trains the colt, who has had some very impressive works since his last race more than two months ago. How he handles the synthetic is the question, though he clearly likes the turf. And though the 11 hole isn’t ideal, he’s not a frontrunner, so jockey Gabriel Saez will have some time to guide him into a good pace-stalking position before heading into the first turn.</p> <p style="text-align: justify;">Pletcher will also saddle Pony Up, who had a terrible trip in the Holy Bull Stakes (GIII) last time out and is probably better than that. The son of Aikenite seems to be a turf type, so the synthetic attempt here makes sense. And if he runs to his potential (mid-90 speed figures are well within his wheelhouse), he’s dangerous for an upset from off the pace.</p> <p style="text-align: justify;">Blended Citizen ships in from California and a third-place finish behind the filly Paved in the El Camino Real Derby last out. It’s hard to say where this former claimer really fits in the grand scheme of the Derby picture, but he’s a good horse and seems to do well on a synthetic surface. The Doug O’Neill trainee will have jockey Kyle Frey back aboard and a decent pace in front of them.</p> <p style="text-align: justify;">Mugaritz was fourth in the El Camino Real Derby last out and the former claimer has definitely risen above expectations. Trainer Jonathon Wong has always been high on this gelding since getting him back in the fall and his best may earn him a piece.</p> <p style="text-align: justify;">Magicalmeister won the John Battaglia Memorial Stakes here last time from off the pace and clearly likes the surface. He faces a jump in class competition, but the Jim Champman-trained son of Bodemeister is improving and may be sitting on the best race of his career if he continues to do so.</p> <p style="text-align: justify;">Sky Promise has a lot of starts (ten) and only one win. He does hit the board more than half the time and, if he runs his race, may pick up another in-the-money finish even if a win from his innermost post position seems tough.</p> <p style="text-align: justify;">Trainer Mike Maker has saddled the winner of this race four times and seeks a fifth with Sarah and Ken Ramsey’s undefeated stakes winner Cash Call Kitten. The son of Kitten’s Joy took the Sage of Monticello Stakes at Gulfstream Park last out, which was a 7 ½-furlong turf event, but he’s routed before and, as a turf horse, may like this synthetic. His numbers overall are a little flat, which makes him tough to like when there are others who look to have more to offer, but he’s been training very well all winter.</p> <p style="text-align: justify;">Arawak was third in the Battaglia Memorial last out and is a winner over this track. The well-traveled Wesley Ward trainee has struggled to find the winner’s circle when jumping into stakes company.</p> <p style="text-align: justify;">Dreamer’s Point hasn’t won since breaking his maiden last September, but he’s logged some decent finishes in allowance and optional claiming company. He won’t need a ton of improvement in his synthetic debut to be a factor for trainer Ian Wilkes and jockey Chris Landeros.</p> <p style="text-align: justify;">Zanesville ships in off a nice first-level allowance romp at Delta Downs. He has a nice work over this track, but this is quite a leap in class off that last. If he runs to his maiden score at Fair Grounds he may have a better shot to pick up a share.</p> <div class="saboxplugin-wrap"> <div class="saboxplugin-gravatar"><img src="https://www.usracing.com/news/wp-content/authors/Margaret_Ransom-6.jpg" class="avatar photo" alt="Margaret Ransom" width="100" height="100" /></div> <div class="saboxplugin-authorname"><a href="https://www.usracing.com/news/author/margaret_ransom">Margaret Ransom</a></div> <div class="saboxplugin-desc"> <div class="vcard author"><span class="fn">California native and lifelong horsewoman Margaret Ransom is a graduate of the University of Arizona’s Race Track Industry Program. She got her start in racing working in the publicity departments at Calder Race Course and Hialeah Park, as well as in the racing office at Gulfstream Park in South Florida. She then spent six years in Lexington, KY, at BRISnet.com, where she helped create and develop the company’s popular newsletters: Handicapper’s Edge and Bloodstock Journal.</p> <p>After returning to California, she served six years as the Southern California news correspondent for BloodHorse, assisted in the publicity department at Santa Anita Park and was a contributor to many other racing publications, including HorsePlayer Magazine and Trainer Magazine. She then spent seven years at HRTV and HRTV.com in various roles as researcher, programming assistant, producer and social media and marketing manager. </p> <p>She has also walked hots and groomed runners, worked the elite sales in Kentucky for top-class consignors and volunteers for several racehorse retirement organizations, including CARMA.</p> <p>In 2016, Margaret was the recipient of the prestigious Stanley Bergstein Writing Award, sponsored by Team Valor, and was an Eclipse Award honorable mention for her story, “The Shocking Untold Story of Maria Borell,” which appeared on USRacing.com. The article and subsequent stories helped save 43 abandoned and neglected Thoroughbreds in Kentucky and also helped create a new animal welfare law known as the “Borell Law.”</p> <p>Margaret’s very first Breeders’ Cup was at Hollywood Park in 1984 and she has attended more than half of the Breeders’ Cups since. She counts Holy Bull and Arrogate as her favorite horses of all time. She lives in Pasadena with her longtime beau, Tony, two Australian Shepherds and one Golden Retriever.</span></div> </div> <div class="clearfix"></div> <div class="saboxplugin-socials"><a target="_blank" href="https://www.facebook.com/redransom"><span class="saboxplugin-icon-grey saboxplugin-icon-facebook"></span></a><a target="_blank" href="https://twitter.com/MargaretRansom"><span class="saboxplugin-icon-grey saboxplugin-icon-twitter"></span></a></div> </div> <div id="wpdevar_comment_1" style="width:100%;text-align:left;">  		<span style="padding: 10px;font-size:22px;font-family:Times New Roman,Times,Georgia,serif;color:#000000;"></span>  		<div class="fb-comments" data-href="https://www.usracing.com/news/analysis/%ef%bb%bf%ef%bb%bf-jeff-ruby-steaks-draws-twelve-looking-derby-points" data-order-by="social" data-numposts="5" data-width="100%" style="display:block;"></div></div><div class='apss-social-share apss-theme-2 clearfix' >      				<div class='apss-facebook apss-single-icon'>  					<a rel='nofollow'  title="Share on Facebook" target='_blank' href='https://www.facebook.com/sharer/sharer.php?u=https://www.usracing.com/news/analysis/%ef%bb%bf%ef%bb%bf-jeff-ruby-steaks-draws-twelve-looking-derby-points'>  						<div class='apss-icon-block clearfix'>  							<i class='fa fa-facebook'></i>  							<span class='apss-social-text'>Share on Facebook</span>  							<span class='apss-share'>Share</span>  						</div>  											</a>  				</div>  								<div class='apss-twitter apss-single-icon'>  					<a rel='nofollow'  href="https://twitter.com/intent/tweet?text=%EF%BB%BF%EF%BB%BF%20%20Jeff%20Ruby%20Steaks%20Draws%20Twelve%20Looking%20for%20Derby%20Points&amp;url=https://www.usracing.com/news%2Fnews%2Fanalysis%2F%25ef%25bb%25bf%25ef%25bb%25bf-jeff-ruby-steaks-draws-twelve-looking-derby-points&amp;"  title="Share on Twitter" target='_blank'>  						<div class='apss-icon-block clearfix'>  							<i class='fa fa-twitter'></i>  							<span class='apss-social-text'>Share on Twitter</span><span class='apss-share'>Tweet</span>  						</div>  											</a>  				</div>  				  				<div class='apss-linkedin apss-single-icon'>  					<a rel='nofollow'  title="Share on LinkedIn" target='_blank' href='http://www.linkedin.com/shareArticle?mini=true&amp;title=%EF%BB%BF%EF%BB%BF%20%20Jeff%20Ruby%20Steaks%20Draws%20Twelve%20Looking%20for%20Derby%20Points&amp;url=https://www.usracing.com/news/analysis/%ef%bb%bf%ef%bb%bf-jeff-ruby-steaks-draws-twelve-looking-derby-points&amp;summary=A+full+field+of+12+is+expected+to+head+postward+in+Saturday%E2%80%99s+inaugural+running+of+the+Jeff+Ruby+S...'>  						<div class='apss-icon-block clearfix'><i class='fa fa-linkedin'></i>  							<span class='apss-social-text'>Share on LinkedIn</span>  							<span class='apss-share'>Share</span>  						</div>  											</a>  				</div>  				</div>		</article><!-- #post-## -->     <div class="blog-date"> <span class="posted-on">Posted on <span><time class="entry-date published" datetime="2018-03-16T14:00:43+00:00">March 16, 2018</time></span></span></div>  	<footer class="entry-meta"> 		 			</footer><!-- .entry-meta -->   </div><!-- end/content -->  		  		 	           	<nav role="navigation" id="nav-below" class="post-navigation"> 		<h1 class="screen-reader-text">Post navigation</h1>  	 		<div class="nav-previous"><a href="https://www.usracing.com/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-simon-says" rel="prev"><span class="meta-nav">&larr;</span> Simon Says: Stop It! Justify Is Not Going to Win the Kentucky Derby!</a></div>		 	 	</nav><!-- #nav-below --> 	    <div class="archives-link">         <a href="https://www.usracing.com/news/archives" > Archives</a>     </div> </div> <!-- end/ #left-col -->   	<?php include("/home/ah/usracing.com/htdocs/news/layout/sidebar.php")?><!-- end: #right-col -->    </div><!-- end/row --> </div><!-- end/container -->  <?php include("/home/ah/usracing.com/htdocs/news/layout/footer.php")?>