<?php header("Content-Type: text/html; charset=utf-8"); ?>
<?php $_usr_vars=array (
  'title' => 'Matt Hess, Author At Horse Racing News',
  'h1' => 'Author: <span class=\'vcard\'>Matt Hess</span>',
  'visible_latest_news' => true,
  'visible_cta_handicapping' => true,
  'classes' => 'single single-post postid-8454 single-format-standard',
  'cta_form' => true,
  'pagination_slug' => 'author/matt_hess',
); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/header.php"); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/cta_handicapping_form.php"); ?>
<div id="main" class="container2 news-content"><div class="row">
<?php include("/home/ah/usracing.com/htdocs/news/layout/body_category.php"); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/sidebar.php"); ?>
</div><!-- end/row --></div><!-- end/main -->
<?php include("/home/ah/usracing.com/htdocs/news/layout/footer.php"); ?>
