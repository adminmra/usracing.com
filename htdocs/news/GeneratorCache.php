<?php 

//error_reporting( E_ALL );
//ini_set( "display_errors" , 1 );
//ini_set("error_log", "php-error.log");
require_once '/home/ah/news.usracing.com/wp-load.php'; //https://wordpress.stackexchange.com/questions/59377/including-wp-load-php-after-another-include-generates-an-error

class GeneratorCache{
	private $url_news_source;
	private $wp_load_path;
	private $DIRNAME ,  $DIR_BET_NEWS;

	public function __construct() {
		$this->url_news_source = "http://news.usracing.com/news/";
		$this->wp_load_path    = "/home/ah/news.usracing.com/wp-load.php";
		$this->DIRNAME         = "/home/ah/usracing.com/htdocs/news/";
		$this->DIR_BET_NEWS    = "/home/ah/ace.betusracing.ag/clubhouse/news/";
	}

	public function generate_footer(){
		ob_start();     
		get_footer();
		$footer = ob_get_clean();
		$footer = str_replace('http://news.usracing.com', '//www.usracing.com', $footer);
		$footer = str_replace('https://news.usracing.com', '//www.usracing.com', $footer);
		$footer = str_replace('//news.usracing.com', '//www.usracing.com', $footer);
		$footer = str_replace('/news.usracing.com', '/www.usracing.com', $footer);
        $footer = preg_replace('~{\$ref}~',"news",$footer);
		file_put_contents("{$this->DIRNAME}layout/footer.php", $footer);
	}

	public function generate_header(){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		ob_start();
		get_header();
		$header = ob_get_clean();
		$dom_header = str_get_html($header);
		$metas = $dom_header->find('meta');
		// lo sigte elimina todos los meta tags encontrados
		foreach($metas as $meta){$meta->outertext = '';}
		$dom_header->find('title', 0)->innertext = '<?php echo $_usr_vars["title"];?>';
		// agrega la variable de metas al final del head
		$dom_header->find('head', 0)->last_child()->innertext .= '<?php echo $_usr_vars["metas"];?>';
		$dom_header->find('body',0)->class = '<?php echo $_usr_vars["classes"];?>';
		$header = $dom_header->save();
		$header = str_replace('http://news.usracing.com', '//www.usracing.com', $header);
		$header = str_replace('https://news.usracing.com', '//www.usracing.com', $header);
		$header = str_replace('//news.usracing.com', '//www.usracing.com', $header);
		$header = str_replace('/news.usracing.com', '/www.usracing.com', $header);
		file_put_contents("{$this->DIRNAME}layout/header.php", $header);
	}

	public function generate_pagination(){
		ob_start();
		wp_pagenavi();
		$pagination = ob_get_clean();
		$pagination = $this->string_replace($pagination);
		file_put_contents("{$this->DIRNAME}layout/pagination.php", $pagination);
	}

	public function generate_sidebar_latest(){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		ob_start();
		get_sidebar();
		$sidebar = ob_get_clean();
		$dom_sidebar = str_get_html($sidebar);
		$content = $dom_sidebar->find('aside[id=rpwe_widget-2]', 0)->outertext;
		$styles_section = $dom_sidebar->find('aside[id=rpwe_widget-2]', 0)->prev_sibling();
		$content = $this->string_replace($content);
		$output = $styles_section . $content;
		file_put_contents("{$this->DIRNAME}layout/sidebar/latest_news.php", $output);
	}

	public function generate_sidebar_categories(){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		ob_start();
		get_sidebar();
		$sidebar = ob_get_clean();
		$dom_sidebar = str_get_html( $sidebar );
		$content = $dom_sidebar->find('aside[id=text-8]', 0)->outertext;
		file_put_contents("{$this->DIRNAME}layout/sidebar/categories.php", $content);

	}

	public function generate_sidebar_ads(){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		ob_start();
		get_sidebar();
		$sidebar = ob_get_clean();
		$dom_sidebar = str_get_html( $sidebar );
		$content = $dom_sidebar->find('aside[id=text-7]', 0)->outertext;
		file_put_contents("{$this->DIRNAME}layout/sidebar/ads.php", $content);

	}

	public function generate_sidebar_cta_handicapping(){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		ob_start();
		get_sidebar();
		$sidebar = ob_get_clean();
		$dom_sidebar = str_get_html( $sidebar );
		$content = $dom_sidebar->find('aside[id=text-6]', 0)->outertext;
		file_put_contents("{$this->DIRNAME}layout/sidebar/cta_handicapping.php", $content);

	}

	private function drop_cta_handicapping($file_content){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		$doc_html = str_get_html( $file_content );
		if(($doc_html->find('div[id=cta_handicapping_form]', 0))) {
			$doc_html->find('div[id=cta_handicapping_form]', 0)->outertext = '@layout/cta_handicapping@';
		}
		$out = $doc_html->save();
		return $out;
	}

	private function drop_header_section($file_content){
		/*require_once($this->DIRNAME . 'simple_html_dom.php');
		$doc_html = str_get_html( $file_content );

		$doc_html->find('div[id=top]',0)->outertext = ''; 
		$doc_html->find('div[id=nav]',0)->outertext = '@layout/header@';
		$out = $doc_html->save();
		return $out;*/
		$html_str = explode('<!--point-->', $file_content);
		return "@g_vars@@layout/header@{$html_str[1]}@layout/footer@";
	}

	private function drop_pagination_section($file_content){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		$doc_html = str_get_html( $file_content );

		if(($doc_html->find('div.wp-pagenavi', 0))) {
			//$doc_html->find('div.wp-pagenavi', 0)->outertext = '@layout/pagination@';
			$doc_html->find('div.wp-pagenavi', 0)->outertext = '';
		}
		$out = $doc_html->save();
		return $out;
	}

	private function drop_sidebar_section($file_content){
		require_once($this->DIRNAME . 'simple_html_dom.php');
		$doc_html = str_get_html($file_content);

		// if(($doc_html->find('aside[id=rpwe_widget-2]', 0))){
		// 	$doc_html->find('aside[id=rpwe_widget-2]', 0)->outertext = '@layout/latest_news@';
		// }

		$doc_html->find('div[id=main] div[id=right-col]', 0)->outertext = '@layout/sidebar@';
		$out = $doc_html->save();
		return $out;
	}

	private function replace_vars_in_content($content){
		$header_path = '<?php include("'. $this->DIRNAME . 'layout/header.php")?>';
		$footer_path = '<?php include("'. $this->DIRNAME . 'layout/footer.php")?>';
		$pagination_path = '<?php include("' . $this->DIRNAME . 'layout/pagination.php")?>';
		$latest_path = '<?php include("' . $this->DIRNAME . 'layout/sidebar.php")?>';
		$cta_form = '<?php include("' . $this->DIRNAME . 'layout/cta_handicapping_form.php")?>';

		$str_in = array($header_path, $footer_path, $latest_path, $cta_form);
		$str_out = array('@layout/header@', '@layout/footer@', '@layout/sidebar@', '@layout/cta_handicapping@');
		$new_srt = str_replace($str_out, $str_in, $content);
		return $new_srt;
	}

	public function join_vars($content, $vars){
		$in = '<?php header("Content-Type: text/html; charset=utf-8");'.PHP_EOL.' $_usr_vars = ' . var_export($vars, true) . ' ?>';
		$out = str_replace('@g_vars@', $in, $content);
		return $out;
	}

	private function save_file( $file_tpl_content , $file_tpl_path, $vars=array() ){

		require_once($this->DIRNAME . 'simple_html_dom.php');
		$dom_content = str_get_html($file_tpl_content);

		//try{
			$metas = $dom_content->find('meta');
		// } catch(Exception $e){
		// 	var_dump($dom_content);
		// 	echo "*{$file_tpl_content}*<br /> \n";
		// 	echo "\n";
		// }
		$dom_str = '';
		if (!(isset($vars['metas']))) {
			foreach($metas as $meta){$dom_str .= $meta; }
            $dom_str = str_replace('http://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('https://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('//news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('/news.usracing.com', '/www.usracing.com', $dom_str);
			$vars['metas'] = $dom_str;
		}

		// if(($dom_content->find('div.wp-pagenavi', 0))){
		// 	$pagination = $dom_content->find('div.wp-pagenavi', 0)->innertext;
		// 	$vars['pagination'] = $pagination;
		// }

		$file_tpl_content = $this->drop_header_section($file_tpl_content);
		$file_tpl_content = $this->drop_pagination_section($file_tpl_content);
		$file_tpl_content = $this->drop_sidebar_section($file_tpl_content);
		$file_tpl_content = $this->drop_cta_handicapping($file_tpl_content);

		if (!(count($vars)==0)){
			$file_tpl_content = $this->join_vars($file_tpl_content, $vars);
		} else {
			$vars = array(
				'title' => 'Horse Racing News | US Racing',
				'visible_latest_news' => false,
				'visible_cta_handicapping' => false
				); 
			$file_tpl_content = $this->join_vars($file_tpl_content, $vars);
		}
		$file_tpl_content = $this->replace_vars_in_content($file_tpl_content);

		$this->makeDir( dirname( $file_tpl_path ) );
		//echo $file_tpl_path . "\n" ;

		if ( is_string ( $file_tpl_content ) && ! empty( $file_tpl_content ) ) {
			$file_tpl_content = $this->string_replace( $file_tpl_content );
			file_put_contents( $file_tpl_path , $file_tpl_content ) ;
		}
	}

	private function string_replace( $content ){
		$content = str_replace( "http%3A%2F%2Fnews.usracing.com/news" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http://news.usracing.com/news"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http:\\/\\/news.usracing.com/news"   , "https:\\/\\/www.usracing.com\\/news" , $content );

		$content = str_replace( "http%3A%2F%2Fnews.usracing.com" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http://news.usracing.com"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http:\\/\\/news.usracing.com"   , "https:\\/\\/www.usracing.com\\/news" , $content );

		$content = str_replace( "http://www.usracing.com/"        , "https://www.usracing.com"            , $content );
		$content = str_replace( "http://1.gravatar"              , "https://1.gravatar" , $content );

				/// delete wp-json api
		$content = preg_replace( "@wp-json.*\"@"   , "\""    , $content );
		$content = preg_replace( "@wp-json.*\'@"   , "\'"    , $content );
		$content = preg_replace( "@/news/\?p=\d+@" , "/news" , $content );
		$content = str_replace( "cing.comnews" , "cing.com/news" , $content );

		return $content;
	}

	private function makeDir( $path ) {

		$htaccess = $this->DIRNAME . ".htaccess";
		if ( ! is_dir( $path ) ) {
			mkdir( $path , 0755 , true );
			copy( $htaccess , $path . "/.htaccess" );
		}

		return true;
	}

	private function copyHtaccess($path){
		$path = dirname($path);
		$htaccess = $this->DIRNAME . ".htaccess";     
		if( !file_exists($path . "/.htaccess") ){
			copy( $htaccess , $path . "/.htaccess" );
		}
	}

	private function get_content_news( $url_news ){
		/*
		$options = array(
				'http' => array( 'method' => "GET" )
		);

		$context = stream_context_create( $options );
		$out = file_get_contents( $url_news , false , $context );
		*/

		$curl = curl_init();
		curl_setopt( $curl , CURLOPT_URL , $url_news );
		curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true );
		curl_setopt( $curl , CURLOPT_HEADER , false );
		$out = curl_exec( $curl );
		$httpCode = curl_getinfo( $curl , CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ( ! $out || $httpCode != 200 ) {
			return false;
		}
		$out = $this->string_replace( $out );
		return $out;
	}

	private function next_page_exist( $html_string ){

		require_once( $this->DIRNAME . 'simple_html_dom.php' );
		$semaphore_loop = str_get_html( $html_string );

		if ( ! $semaphore_loop ) {
			return false;
		}

		$semaphore_loop = $semaphore_loop->find( "div.wp-pagenavi a.nextpostslink" );
		return ( count( $semaphore_loop ) < 1  ) ? false : true ;
	}

	// Esta funcion recibe un array que serian las categorias pertenecientes a un post
	// dicha function ejecutara la cache por cada elemento del array de categorias
	public function categories_by_post($cats=array()){
		if (!$cats) return false;
		foreach ($cats as $c) {
			$this->category_index_page($c->slug, $c->cat_name);  
		} 
	}

	public function get_classes_css($_id){
		global $wp_query;
		$query = new WP_Query( array( 'p' => $_id, 'post_type' => 'any' ) ); 
		$wp_query = $query;
		$classes = implode(' ', get_body_class());
		wp_reset_postdata();
		return $classes;
	}

	// Genera el cache de un post pasandole como argumento el id
	public function post_cache_by_id($p_id){
		if (!$p_id) return false;
		$hide_post_from_news=0;
		if (metadata_exists( 'post', $p_id, 'featured_post' ) ) { }else{ update_post_meta( $p_id, 'featured_post', '1' ); }
		$hide_post_from_news=get_post_meta($p_id, 'hide_post_from_news', true );
		if($hide_post_from_news == 1){ update_post_meta( $p_id, 'featured_post', '0' ); }		
		
		$arg = array('p'=>$p_id, 'post_type'=>'any');
		$query = new WP_Query($arg);
		if (!$query->have_posts()) return;
		while ($query->have_posts()){
			$query->the_post(); 
			ob_start();
			get_template_part( 'content', get_post_format() );
			$excerpt = ob_get_clean();
            // save excerpt under news directory
			if($hide_post_from_news == 0){
            $this->save_excerpts($excerpt, "{$this->DIRNAME}_usr_posts/{$p_id}");
			}
			//global $wp_query;
			//$wp_query = $query;
			//$cla = get_body_class();
			//$classes = implode(' ', get_body_class());

			$vars = array(
				'title' => "{$query->post->post_title} | Horse Racing News",
				'visible_latest_news' => true,
				'visible_cta_handicapping' => true,
				'classes' => $this->get_classes_css($p_id),
				'cta_form' => false
				); 
			$source_post_link = get_permalink();
			$categories_in_this_post = get_the_category();
			$this->categories_by_post($categories_in_this_post);

			// get_the_author_meta('user_nicename'); falla I dunno
			$author = get_userdata($query->post->post_author);
			$this->author_index_page($author->display_name, $author->user_nicename);
			$this->save_excerpts($excerpt, "{$this->DIRNAME}author/{$author->user_nicename}/_usr_posts/{$p_id}");

			// Tags belong to Post
			$belong_tags = get_the_tags($p_id);
			foreach($belong_tags as $tag){
				$tag_path = "{$this->DIRNAME}tag/{$tag->slug}/_usr_posts/{$p_id}";
				$this->tag_index_page( $tag->name, $tag->slug );
				$this->save_excerpts($excerpt, $tag_path);
			}

			// For Archives post
			$date = explode('-', $query->post->post_date);
			$this->archives_index_page( $date[0] , $date[1] );
			$this->save_excerpts($excerpt, "{$this->DIRNAME}{$date[0]}/{$date[1]}/_usr_posts/{$p_id}");

			foreach ($categories_in_this_post as $c) {
				$source_to_get = "{$this->url_news_source}{$c->slug}/{$query->post->post_name}"; 
				if (!($c->slug == 'news')){
					$path_to_save = "{$this->DIRNAME}{$c->slug}/{$query->post->post_name}.php"; 
					$path_excerpt = "{$this->DIRNAME}{$c->slug}/_usr_posts/{$p_id}"; 
				}else {
					$path_to_save = "{$this->DIRNAME}{$query->post->post_name}.php"; 
					$path_excerpt = "{$this->DIRNAME}_usr_posts/{$p_id}"; 
				}

				$out = $this->get_content_news($source_post_link);
				//file_put_contents('/home/ah/usracing.com/htdocs/news/current-dev/prueba.php', $out);
				$this->save_excerpts($excerpt, $path_excerpt);
				$this->save_file($out, $path_to_save, $vars);
			}
		}
		wp_reset_postdata();
	}

	public function save_excerpts($body, $path){
		$this->makeDir( dirname( $path ) );
		if ( is_string ( $body ) && ! empty( $body ) ) {
			$body = str_replace('http://news.usracing.com', '//www.usracing.com', $body);
			$body = str_replace('https://news.usracing.com', '//www.usracing.com', $body);
			$body = str_replace('//news.usracing.com', '//www.usracing.com', $body);
			$body = str_replace('/news.usracing.com', '/www.usracing.com', $body);
			file_put_contents( $path , $body ) ;
		}
	}

	// Genera el index page de una categoria pasandole como argumento el slug de una categoria
	private function category_index_page($category_slug="", $category_name=""){
		if (!$category_slug || !$category_name || $category_slug == 'news') return false;

		if( file_exists( "{$this->DIRNAME}{$category_slug}/index.php" ) ) return false;

		global $wp_query;
		$query = new WP_Query(array( 'category_name' => $category_slug ));
		$wp_query = $query;
		$classes = implode(' ', get_body_class());
		wp_reset_postdata();

		$vars = array(
			'title' => "$category_name | Horse Racing News",
			'h1' => "$category_name",
			'visible_latest_news' => true,
			'visible_cta_handicapping' => true,
			'classes' => $classes,
			'cta_form' => true,
			'pagination_slug' => "{$category_slug}"
			); 

		$this->makeDir( "{$this->DIRNAME}{$category_slug}" );
		$path_to_save = "{$this->DIRNAME}{$category_slug}/index.php";

		$output  = '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$output .= '<?php $_usr_vars='.var_export($vars, TRUE) . '; ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/header.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/cta_handicapping_form.php"); ?>'."\n";
		$output .= '<div id="main" class="container2 news-content"><div class="row">'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/body_category.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/sidebar.php"); ?>'."\n";
		$output .= '</div><!-- end/row --></div><!-- end/main -->'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/footer.php"); ?>'."\n";
		file_put_contents( $path_to_save , $output ) ;
	}

	// Genera el index page de un author pasando la respectiva url del author, y su nicename
	private function author_index_page($author_name, $author_slug){
		if (!$author_name || !$author_slug) return false;

		if( file_exists( "{$this->DIRNAME}author/{$author_slug}/index.php" ) ) return false;

		$classes = implode(' ', get_body_class());
		wp_reset_postdata();
		$vars = array(
			'title' => "{$author_name}, Author At Horse Racing News",
			'h1' => "Author: <span class='vcard'>{$author_name}</span>",
			'visible_latest_news' => true,
			'visible_cta_handicapping' => true,
			'classes' => $classes,
			'cta_form' => true,
			'pagination_slug' => "author/{$author_slug}"
			);

		$this->makeDir( "{$this->DIRNAME}author/{$author_slug}" );
		$path_to_save = "{$this->DIRNAME}author/{$author_slug}/index.php"; 
		$this->copyHtaccess($path_to_save);

		$output  = '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$output .= '<?php $_usr_vars='.var_export($vars, TRUE) . '; ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/header.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/cta_handicapping_form.php"); ?>'."\n";
		$output .= '<div id="main" class="container2 news-content"><div class="row">'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/body_category.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/sidebar.php"); ?>'."\n";
		$output .= '</div><!-- end/row --></div><!-- end/main -->'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/footer.php"); ?>'."\n";
		file_put_contents( $path_to_save , $output ) ;
	}

	private function archives_index_page( $year, $month ){
		if ( empty($year) || empty($month) ) return false;

		if( file_exists( "{$this->DIRNAME}{$year}/{$month}/index.php" ) ) return false;

		$months_lit = array("01"=>"January", "02"=>"February", "03"=>"March", "04"=>"April", "05"=>"May", "06"=>"June", "07"=>"July", "08"=>"August", "09"=>"September", "10"=>"October", "11"=>"November", "12"=>"December");

		$vars = array(
			'title' => "{$months_lit[$month]} {$year} | Horse Racing News",
			'h1' => "<i class='fa fa-calendar'></i> Articles for:  <span>{$months_lit[$month]} {$year}</span>",
			'visible_latest_news' => false,
			'visible_cta_handicapping' => true,
			'classes' => 'archive date',
			'cta_form' => true ,
			'pagination_slug' => "{$year}/{$month}"
			);

		$this->makeDir( "{$this->DIRNAME}{$year}/{$month}" );
		$path_to_save = "{$this->DIRNAME}{$year}/{$month}/index.php"; 
		$this->copyHtaccess($path_to_save);

		$output  = '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$output .= '<?php $_usr_vars='.var_export($vars, TRUE) . '; ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/header.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/cta_handicapping_form.php"); ?>'."\n";
		$output .= '<div id="main" class="container2 news-content"><div class="row">'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/body_category.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/sidebar.php"); ?>'."\n";
		$output .= '</div><!-- end/row --></div><!-- end/main -->'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/footer.php"); ?>'."\n";
		file_put_contents( $path_to_save , $output ) ;
	}

	private function tag_index_page($tag_name, $tag_slug){
		if ( empty($tag_name) || empty($tag_slug) ) return false;

		if( file_exists( "{$this->DIRNAME}tag/{$tag_slug}/index.php" ) ) return false;

		$vars = array(
			'title' => "{$tag_name} | Horse Racing News",
			'h1' => $tag_name,
			'visible_latest_news' => false,
			'visible_cta_handicapping' => true,
			'classes' => 'archive tag',
			'cta_form' => true,
			'pagination_slug' => "tag/{$tag_slug}"
			);

		$this->makeDir( "{$this->DIRNAME}tag/{$tag_slug}" );
		$path_to_save = "{$this->DIRNAME}tag/{$tag_slug}/index.php"; 
		$this->copyHtaccess($path_to_save);

		$output  = '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$output .= '<?php $_usr_vars='.var_export($vars, TRUE) . '; ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/header.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/cta_handicapping_form.php"); ?>'."\n";
		$output .= '<div id="main" class="container2 news-content"><div class="row">'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/body_category.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/sidebar.php"); ?>'."\n";
		$output .= '</div><!-- end/row --></div><!-- end/main -->'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/footer.php"); ?>'."\n";
		file_put_contents( $path_to_save , $output ) ;
	}

	public function generate_home_index_only($flag=false)
	{
		$output = $this->get_content_news('http://news.usracing.com/');
		$filename = $this->DIRNAME . "index.php";
		$vars = array(
			'title' => 'Horse Racing News | US Racing',
			'visible_latest_news' => false,
			'visible_cta_handicapping' => false,
			'classes' => 'home blog',
			'cta_form' => true,
			'pagination_slug' => 'news',
			'featured_section' => $this->DIRNAME.'_home_featured_post.php',
			'metas' => '<meta charset="UTF-8"><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="description" content="US Racing"/><meta name="robots" content="noodp"/><meta property="fb:app_id" content="825577710919578"/><meta property="og:locale" content="en_US"/><meta property="og:site_name" content="Horse Racing News"/><meta property="og:title" content="Horse Racing News"/><meta property="og:url" content="https://www.usracing.com"/><meta property="og:type" content="website"/><meta property="og:description" content="US Racing has the latest award winning horse racing news and analysis."/><meta itemprop="name" content="Horse Racing News"/><meta itemprop="description" content="US Racing has the latest award winning horse racing news and analysis."/><meta name="twitter:title" content="Horse Racing News"/><meta name="twitter:url" content="https://www.usracing.com"/><meta name="twitter:description" content="US Racing has the latest award winning horse racing news and analysis."/><meta name="twitter:card" content="summary_large_image"/><meta name="description" content="US Racing has the latest award winning horse racing news and analysis."/>'
			);

		$path_to_save = "{$this->DIRNAME}index.php"; 

		$output  = '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$output .= '<?php $_usr_vars='.var_export($vars, TRUE) . '; ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/header.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/cta_handicapping_form.php"); ?>'."\n";
		$output .= '<div id="main" class="container2 news-content"><div class="row">'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/body_category.php"); ?>'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/sidebar.php"); ?>'."\n";
		$output .= '</div><!-- end/row --></div><!-- end/main -->'."\n";
		$output .= '<?php include("'.$this->DIRNAME.'layout/footer.php"); ?>'."\n";
		file_put_contents( $path_to_save , $output ) ;
	}

	public function generate_home_featured_post()
	{
		require_once($this->DIRNAME . 'simple_html_dom.php');
		//$html = file_get_html('http://news.usracing.com');
		$file_home_content=$this->get_content_news('http://news.usracing.com');
		$html=str_get_html($file_home_content);
		$ret = $html->find('div[class=featured-news]', 0);

		$output = $ret->outertext;
		if(!empty($output)){
			$output = str_replace('http://news.usracing.com', '//www.usracing.com', $output);
			$output = str_replace('https://news.usracing.com', '//www.usracing.com', $output);
			$output = str_replace('//news.usracing.com', '//www.usracing.com', $output);
			$output = str_replace('/news.usracing.com', '/www.usracing.com', $output);
			file_put_contents($this->DIRNAME.'_home_featured_post.php', $output);
		}
	}

	public function cache_tags_page()
	{
		$output = $this->get_content_news('http://news.usracing.com/news/tags');
		$vars = array(
			'title' => 'Tags | Horse Racing News',
			'visible_latest_news' => true,
			'visible_cta_handicapping' => false,
			'classes' => 'page page-id-5299 page-child parent-pageid-3831 page-template page-template-tags-page page-template-tags-page-php logged-in admin-bar no-customize-support'
			); 
		$this->save_file($output, '/home/ah/usracing.com/htdocs/news/tags/index.php', $vars);
	}

	public function all_posts(){
		define( 'WP_USE_THEMES' , false );
		$args = array(
			'orderby' => 'ASC',
			'posts_per_page'=>-1
			);

		$query = new WP_Query( $args );
		if ( $query->have_posts() ){
			$index_category = array();
			while ( $query->have_posts() ){
				$query->the_post() ;
				foreach ( get_the_category( $query->post->ID ) as $category ) {
					if ($category->slug == 'news') continue;
					$category_path = $this->DIRNAME . $category->slug;

					if ( ! in_array( $category->slug , $index_category ) ) {
						$source = $this->url_news_source . $category->slug ;
						$output =  $this->get_content_news( $source );
						$this->save_file( $output , $category_path . "/index.php" ) ;
						$index_page = 2;
						while ( $this->next_page_exist( $output ) ) {
							$output = $this->get_content_news( $source . "/page/" . $index_page );
							$filename = $category_path . "/page/{$index_page}.php";
							$this->save_file( $output , $filename ) ;

							$index_page += 1;
						}
						array_push( $index_category , $category->slug );
					}

					$source = $this->url_news_source . $category->slug . "/" . $query->post->post_name;
					$output  =  $this->get_content_news( $source );
					/* If category is news save post under /home/ah/htdocs/news directory */
					if ($category->slug == 'news') {
						$filename_less = $this->DIRNAME . $query->post->post_name . ".php";
						$this->post_under_news_category($source,$filename_less);
					}
					$filename = $category_path . "/" . $query->post->post_name . ".php";
					$this->save_file( $output , $filename ) ;
				}
			}
		}
		wp_reset_postdata();
	}

	public function rsync_resources_for_news(){
		exec( "/usr/bin/rsync -avz --delete /home/ah/news.usracing.com/wp-content/ /home/ah/usracing.com/htdocs/news/wp-content/" );
		exec( "/usr/bin/rsync -avz --delete /home/ah/news.usracing.com/wp-includes/ /home/ah/usracing.com/htdocs/news/wp-includes/" );
		exec( "/usr/bin/rsync -avz --delete /home/ah/news.usracing.com/wp-content/ ".$this->DIR_BET_NEWS."/wp-content/" );
		exec( "/usr/bin/rsync -avz --delete /home/ah/news.usracing.com/wp-includes/ ".$this->DIR_BET_NEWS."/wp-includes/" );
	}

	public function generar_todos_los_posts(){
		global $wpdb;

		$sql="SELECT id FROM $wpdb->posts
		WHERE $wpdb->posts.post_status = 'publish' 
    AND $wpdb->posts.post_type = 'post'
    ORDER BY $wpdb->posts.post_date DESC";

		$posts = $wpdb->get_results($sql);

		$i = 0;
		$lim = count($posts);
		foreach ($posts as $post){
			$this->post_cache_by_id($post->id);
			$i++;
			echo 'Processing ' .$post->id. " ... done {$i}/{$lim} \n";
		}

		echo "End of Script\n";
	}

    public function generate_archives_page(){
		$output = $this->get_content_news('http://news.usracing.com/news/archives');
		$path_to_save = $this->DIRNAME . "archives.php";
		$vars = array(
			'title' => 'Archives | Horse Racing News',
			'visible_latest_news' => true,
			'visible_cta_handicapping' => false,
			'classes' => 'page page-id-4886 page-child parent-pageid-3831 page-template page-template-archives page-template-archives-php',
			'cta_form' => false
	    );
        $this->save_file($output, $path_to_save, $vars);
    }

    public function sync_cache()
    {
        $origin = "{$this->DIRNAME}";
        $destiny = "ubuntu@172.31.44.53:{$this->DIRNAME}";
        $cmd = "/usr/bin/rsync -avz --delete -e 'ssh -i /home/ah/usracing.pem' {$origin} {$destiny}";
        ob_start();
        passthru($cmd, $status);
        $exit_code = ob_get_clean();
        if ($status == 0) {
            // success
            echo "Success synced news.\n";
        } else {
            // failure
            echo "Error , please check.\n";
        }
    }
}
