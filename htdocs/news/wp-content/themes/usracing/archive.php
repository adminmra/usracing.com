<?php	 	
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _usr
 */

get_header();

include 'subscribe-ctc-form.php';
?>
<div id="left-col" class="col-medium-9">
<div class="widgetheader">
	<div class="categoriesleft">
		<?php //the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
		<select name="forma" onchange="location = this.value;">
			 <option value="/news">Latest Racing News</option>
			 <option value="/news/features">US Racing Features</option>
			 <option value="/news/analysis">Racing Analysis</option>
			 <option value="/news/handicapping-reports">Handicapping Reports</option>
			 <option value="/news/breeders-cup">Breeders' Cup News</option>
			 <option value="/news/kentucky-derby-road-to-the-roses">Kentucky Derby Road</option>
			 <option value="/news/recap">Race Recap</option>
			 <option value="/news/nfl">NFL Betting</option>
		</select>
	</div>
	<div class="searchright">
		<?php the_widget( 'WP_Widget_Search' ); ?>
	</div> 	
</div>
</div>
<div id="main" class="container2 news-content">
<div class="row">
<div id="left-col" class="col-medium-9">
<?php	 	 // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?> <?php	 	 if ( have_posts() ) : ?> 
<?php if (is_category( 823 ) ){?>
<div class="featured-news">
				<?php
					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => -1,
					  //'post_status'    => 'publish',
					  'meta_query'     => array(
					    array(
					      'key'     => 'featured_post',
					      'value'     => '1',
					    )
					  )
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
					  		$the_query->the_post();
					  		echo '<div class="img-featured">';
					  		echo "<a href='". get_the_permalink()."'>";
							echo the_post_thumbnail('large');
							echo "</a>";
							echo '</div>';
							echo '<div class="info-news">';
							echo '<span class="category">';
							echo the_category();
							echo '</span>';
							echo '</div>';
							echo '<div class="desc-featured"><h1>';
							echo "<a href='". get_the_permalink()."'>";
							echo the_title();
							echo "</a>";
							echo '</h1>';
                                                        
							echo '<div class="postedby-featured">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">By <span class="name-post">' .  get_the_author() . '</span> ' . $date_post.'</div>';
							echo "</div>";
                                                        
							echo '<p>';
							echo the_excerpt();
							echo '</p></div>';
							
					  }
					} else {
					  // no posts found
					}
					wp_reset_postdata(); 
				?>
				</div>
			<?php } ?>
<div class="headline"><h1> <?php	 	
					

						if ( is_category() ) :
							
							single_cat_title();
	
						elseif ( is_tag() ) :
							single_tag_title();
	
						elseif ( is_author() ) :
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( 'Author: %s', 'usr' ), '<span class="vcard">' . get_the_author() . '</span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();
	
						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'usr' ), '<span>' . get_the_date() . '</span>' );
	
						elseif ( is_month() ) :
							printf( __( '<i class="fa fa-calendar"></i> Articles for:  %s', 'usr' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
	
						elseif ( is_year() ) :
							printf( __( '<i class="fa fa-calendar"></i> %s', 'usr' ), '<span>' . get_the_date( 'Y' ) . ' Articles</span>' );
	
						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'usr');
	
						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'usr' );
	
						else :
							_e( 'Archives', 'usr' );
	
						endif;
?></h1></div><!-- end/headline --> 
<?php	 	
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
<div class="list-news"> 
<?php $column=1; 	  // start the loop. ?>
<?php	 	 while ( have_posts() ) : the_post(); ?> 
		<?php	
			if($column % 2 != 0){
				echo '<div class="row">';
			}
		?>
	<?php	 	
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
?>
<?php
			if($column % 2 == 0){
				echo '</div>';
			}
			$column++;
			if ($column == 11) { break; }
		?>
<?php	 	 endwhile; ?> 
<?php 
			if($column % 2 == 0){
				echo '</div>';
			}
		?>
<?php wp_pagenavi(); ?>
<a id="archives-link" href="<?php echo get_home_url(); ?>/archives" > Archives</a>

<?php	 	 else : ?>
<?php	 	 get_template_part( 'no-results', 'archive' ); ?>
<?php	 	 endif; ?>
</div><!-- end/content --> 
</div> <!-- end/ #left-col --> 

<?php	 	 get_sidebar(); ?> </div><!-- end/row --> 
</div><!-- end/container --> 
<script type='text/javascript'>
    jQuery(function(){
     jQuery('#cat option[value=-1]').remove();
     jQuery('#cat option').each(function(k,v){
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
     });
     jQuery('#categories-dropdown-3 option[value=-1]').remove();
     jQuery('#categories-dropdown-3 option').each(function(k,v){
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
     });
    jQuery('.wp-pagenavi').append(jQuery('#archives-link'));
   });
   
</script>
<?php	 	 get_footer(); ?> 
