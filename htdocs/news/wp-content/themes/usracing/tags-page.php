<?php	 		 	

error_reporting(E_ALL);
ini_set('display_errors', '1');
/**
 * Template Name: Tags-All
 * @package _usr
 */

get_header(); 
include '/home/ah/allhorse/public_html/usracing/news/hero.php';
?>
<div class="news-content" style="margin-top:15px">	
	<div class="inner-news container2">
    <div id="left-col" class="col-medium-9"> 
    <div id="page-tag-all">
    <div class="headline"><h1>Tags</h1></div>
    <div class="container">
    <div class="row">
        <?php
        // Retrieve all tags 
        $tags = get_tags();

        $tags_by_name = array();
        foreach ($tags as $t) { $tags_by_name[] = $t->name;}
        // asort - because return array with index original array $tags
        asort($tags_by_name);

        $tags_sorted = array();

        foreach ($tags_by_name as $k => $v) {
            $obj = new StdClass;
           $obj->name = ucwords($v);
           $obj->slug = $tags[$k]->slug;
           $tags_sorted[] = $obj; 
        }

        $len = count($tags_sorted);
        //sort($tags);
        $all = array_chunk($tags_sorted, ceil($len/2));
        //$firsthalf = array_slice($tags, 0, $len / 2);
        //$secondhalf = array_slice($tags, $len / 2);

        $firsthalf = $all[0];
        $secondhalf = $all[1];
        echo "<ul class='col-sm-4'>";
        foreach ($firsthalf as $tag) {
            $link_tag = "/news/tag/{$tag->slug}"; 
            echo "<li><a href=\"$link_tag\">{$tag->name}</a></li>";
        }
        echo "</ul>";

        echo "<ul class='col-sm-4'>";
        foreach ($secondhalf as $tag) {
            $link_tag = "/news/tag/{$tag->slug}"; 
            echo "<li><a href=\"$link_tag\">{$tag->name}</a></li>";
        }
        echo "</ul>"; ?>
    </div>
    </div>
    </div><!-- END ID PAGE-TAG-ALL -->
	</div>
	<?php	 	 get_sidebar(); ?>
    </div>
</div>
<?php	 	 get_footer(); ?>
