<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _usr
 */
?>
<!--point-->
<?php	 	// include ('/home/ah/usracing.com/smarty/templates/footer.tpl') ?>
<?php
$responseFoot = file_get_contents("https://www.usracing.com/newshtmlfooter", false, stream_context_create($arrContextOptions));
$responseFoot=str_replace('href="','rel="nofollow" target="_blank" href="//www.usracing.com', $responseFoot);
echo $responseFoot; ?>

            <!--
				<div class="site-info">
					<?php	 	 //do_action( '_usr_credits' ); ?>

					<span class="sep"> | </span>
					<?php	 	 //printf( __( 'Theme: %1$s by %2$s.', 'usr' ), 'usr', '<a href="http://www.usracing.com" rel="designer">US Racing</a>' ); ?>
				</div>--><!-- close .site-info -->

<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<!--<script type="text/javascript" src="/assets/js/app.js"></script> -->
<script type="text/javascript">
jQuery(document).ready(function() {
	/*App.init();
	App.initFancybox();
	App.initRoyalSlider();
    Index.initIndex();    */
    jQuery('#ol').sidr({
        name: 'left-menu',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        displace: false,
        onOpen: function () {
		  jQuery('#left-menu').perfectScrollbar();
          jQuery("#ol i.fa").hide();
          jQuery("#ol i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu').perfectScrollbar('update');
          jQuery("#ol i.glyphicon").hide();
          jQuery("#ol i.fa").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#ol-left').sidr({
        name: 'left-menu-main',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        displace: false,
        onOpen: function () {
		  jQuery('#left-menu-main').perfectScrollbar();
          jQuery("#ol-left i.fa, #ol-left span.more").hide();
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").show().css('display', 'block');
          jQuery("#ol-left").addClass("active");
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu-main');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu-main').perfectScrollbar('update');
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").hide();
          jQuery("#ol-left i.fa, #ol-left span.more").show().css('display', 'block');
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery("#ol-left").removeClass("active");
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#or').sidr({
        name: 'nav-side',
        side: 'left',
        speed: 300,
        source: '.navbar-collapse',
        renaming: false,
        displace: false,
        onOpen: function () {
		  jQuery('#nav-side').perfectScrollbar();
          jQuery("#or span").hide();
          jQuery("#or i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'nav-side');
            }
          });
        },
        onClose: function () {
		  jQuery('#nav-side').perfectScrollbar('update');
          jQuery("#or i.glyphicon").hide();
          jQuery("#or span").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      // Sidr Accordian
      jQuery(".sidr a").click(function () {
        var link = jQuery(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;
        closest_ul.find("ul").slideUp(function () {
          if (++count == closest_ul.find("ul").length)
            parallel_active_links.removeClass("active");
        });
        if (!link_status) {
          closest_li.children("ul").slideDown();
          closest_li.addClass("active");
      	  jQuery('.sidr').perfectScrollbar('update');
        }
    });
});
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest -->
<script type="text/javascript" async src="<?php echo get_home_url() ?>/wp-content/themes/usracing/js/pinit.js"></script>
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'usracing.com');
  ga('send', 'pageview');
</script>
-->
<?php /*
  $scripts_footer_content = file_get_contents( '/home/ah/usracing.com/smarty/templates/scripts-footer.tpl' );
  $re = "/{\/?literal}/";
  $scripts_footer_content = preg_replace( $re , "" ,  $scripts_footer_content );
  echo $scripts_footer_content; */
 ?>


<?php wp_footer(); ?>
<!-- CTA Handicapping -->
<script type='text/javascript'>
   var localizedErrMap = {};
   localizedErrMap['required'] = 		'This field is required.';
   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
   localizedErrMap['list'] = 			'Please select at least one email list.';
   localizedErrMap['generic'] = 		'This field is invalid.';
   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
	localizedErrMap['state_province'] = 'Select a state/province';
   localizedErrMap['selectcountry'] = 	'Select a country';
   var postURL = 'https://visitor2.constantcontact.com/api/signup';
</script>
<script type='text/javascript' src='<?php echo get_site_url(); ?>/wp-content/themes/usracing/js/signup-form.js'></script>
<!-- End CTA -->

</body>
</html>
