<?php	 	
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _usr
 */
get_header();

include 'subscribe-ctc-form.php';

?>
<div id="main" class="news-content">	
	<div class="inner-news container2">
		<div id="left-col" class="col-medium-9">
			<div class="widgetheader">
				<div class="categoriesleft">
				<?php //the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
				<select name="forma" onchange="location = this.value;">
					 <option value="/news">Latest Racing News</option>
					 <option value="/news/features">US Racing Features</option>
					 <option value="/news/analysis">Racing Analysis</option>
					 <option value="/news/handicapping-reports">Handicapping Reports</option>
					 <option value="/news/breeders-cup">Breeders' Cup News</option>
					 <option value="/news/kentucky-derby-road-to-the-roses">Kentucky Derby Road</option>
					 <option value="/news/recap">Race Recap</option>
					 <option value="/news/nfl">NFL Betting</option>
				</select>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
			<div class="featured-news">
				<!-- test tis as -->
                 <?php
					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => 1,
					  'post_status'    => 'publish',                                         
					  'meta_query'     => array(
					    array(
					      'key'     => 'featured_post',
					      'value'     => '1',
					    )
					  )
					);
					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
					  		$the_query->the_post();
					  		echo '<div class="img-featured">';
					  		echo "<a href='". get_the_permalink()."'>";
							echo the_post_thumbnail('large');
							echo "</a>";
							echo '</div>';
							echo '<div class="info-news">';
							echo '<span class="category">';
							echo the_category();
							echo '</span>';
							echo '</div>';
							echo '<div class="desc-featured"><h1>';
							echo "<a href='". get_the_permalink()."'>";
							echo the_title();
							echo "</a>";
							echo '</h1>';
                            echo '<!-- test -->';                            
							echo '<div class="postedby-featured">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">By <span class="name-post">' .  get_the_author() . '</span> ' . $date_post.'</div>';
							echo "</div>";
                                                        
							echo '<p>';
							echo the_excerpt();
							echo '</p></div>';
							
					  }
					} else {
					  // no posts found
					}
					wp_reset_postdata(); 
				?>
				</div>
	<div class="headline"><h1>US Racing New and Handicapping Reports</h1></div><!-- end/headline -->	
	<?php	 	 if ( have_posts() ) : ?>

	<div class="list-news">
		<?php $column=1; 	  // start the loop. ?>
		<?php	 	 while ( have_posts() ) : the_post(); ?>
		<?php	
			if($column % 2 != 0){
				echo '<div class="row '.$column.'">';
			}
		?>
			<?php	 	
				/* Include the Post-Format-specific template for the content.
				 * If you want to overload this in a child theme then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );
			?>
		<?php
			if($column % 2 == 0){
				echo '</div>';
			}
			$column++;
			if ($column == 11) { break; }
		?>
		<?php	 	 endwhile; ?>
		<?php 
			if($column % 2 == 0){
				echo '</div>';
			}
		?>
		

	<?php	 	 else : ?>

		<?php	 	 get_template_part( 'no-results', 'index' ); ?>

	<?php	 	 endif; ?>

	</div> <!-- end list-news -->
	<?php wp_pagenavi(); ?>
        <a id="archives-link" href="<?php echo get_home_url(); ?>/archives" > Archives</a>
        <a id="alltags-link" href="<?php echo get_home_url(); ?>/tags/" > Tags</a>
        

	</div> <!-- end/ #left-col -->
	<?php	 	 get_sidebar(); ?>
	</div><!-- end/container2 -->
</div><!-- end/news-content -->

<script type='text/javascript'>
   
   jQuery(function(){
       jQuery('#cat option[value=-1]').remove();
       jQuery('#cat option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('#categories-dropdown-3 option[value=-1]').remove();
       jQuery('#categories-dropdown-3 option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('.wp-pagenavi').append(jQuery('#archives-link'));
       jQuery('.wp-pagenavi').append(jQuery('#alltags-link'));

   });
   
</script>
<script type='text/javascript'>
    var p = document.querySelector('head');
    var i = document.querySelector('meta[name="description"]'); 
    p.removeChild(i);
</script>

<?php	 	 get_footer(); ?>
