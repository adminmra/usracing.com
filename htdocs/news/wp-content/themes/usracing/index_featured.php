<?php
/**
Template name: Featured Posts
 */

get_header(); ?>

<div class="subscribe-section col-md-12 col-sm-12 col-xs-12">
	<div class="inner-subscribe-section container">
			<h2>Horse Racing Subscribe Blog</h2>
			<p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </p>
			<div class="form-subscribe">
				<form method="post" action="">
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="text" name="firstname" placeholder="Your First Name" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="email" name="email" placeholder="Your Best Email Address" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="submit" value="Subscribe Now" />
                    </div>
				</form>
			</div>
	</div>
</div>
<div id="main" class="news-content">
	<div class="inner-news container2">
		<div id="left-col" class="col-md-9">
			<div class="headline"><h1>Horse Racing News</h1></div><!-- end/headline -->
				<div class="content">
					<div class="featured-news">
					<?php

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => 1,
					  'post_status'    => 'publish',
					  'meta_query'     => array(
					    array(
					      'key'     => 'featured_post',
					      'value'     => '1',
					    )
					  )
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
					  		$the_query->the_post();
					  		echo '<div class="img-featured">';
							echo the_post_thumbnail();
							echo '</div>';
							echo '<div class="info-news">';
							echo '<span class="category">';
							echo the_category();
							echo '</span>';
							echo '</div>';
							echo '<div class="desc-featured"><h1>';
							echo the_title();
							echo '</h1>';
							echo '<p>';
							echo the_excerpt();
							echo '</p></div>';
							echo '<div class="postedby-featured">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">By <span class="name-post">' .  get_the_author() . '</span> ' . $date_post.'</div>';
							echo "</div>";
					  }
					} else {
					  // no posts found
					}
					wp_reset_postdata(); 
					?>
					</div>
					<!--h1> Lastest Posts</h1-->
					<div class="list-news">
						<?php

					//  6 Latest Posts

					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => 10,
        			  'orderby' => 'most_recent',
					  'post_status'    => 'publish',
					  'paged'     => $paged,
					  
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {

					  	$the_query->the_post();

					  	echo '<div class="item-news col-md-6 col-xs-12">';
							//get_template_part(  'content'  , $the_query->the_post() );
					  		echo '<article class="story">';
					  		echo '<div class="img-news">';
					  		echo '<a href="'. get_permalink() .'">';
					  		if(the_post_thumbnail()!= null){
					  			echo the_post_thumbnail();
					  		}else{
					  			echo '<img src="/wp-content/themes/usracing/images/placeholder.jpg" />';
					  		}
							echo '</a>';
							echo '</div>';
							echo '<div class="info-news">';
							echo '<span class="category">';
							echo the_category();
							echo '</span>';
							echo '</div>';
							echo '<div class="title-news">';
							echo '<h3><a href="'. get_permalink() .'">';
							$title_r = get_the_title();
							echo substr($title_r, 0, 20);
							echo '...</a></h3>';
							echo '<p>';
							$content_r = get_the_excerpt();
							echo substr($content_r, 0, 200);
							echo '...</p>';
							echo '</div>';
							echo '<div class="postedby-news">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">';
							echo 'By <span class="name-author">';
							echo the_author_link();
							echo  '</span> ';
							echo $date_post;
							echo '</div>';
							echo "</div>";

						echo '</div>';

					  }

					  echo '<nav>';
				               next_posts_link( 'Previous Page' ); 
				               previous_posts_link( 'Next page' );
				            echo '</nav>';

					} else {
					  // no posts found
					}
					/* Restore original Post Data */
					wp_reset_postdata(); ?>

				</div>

				<div class="news-button"><a href="/news" class="card_btn--default">See all news</a></div>
				</div> <!-- end/content -->
				<?php	 	 _usr_content_nav( 'nav-below' ); ?>
		</div> <!-- end/ #left-col -->
		<?php	 	 get_sidebar(); ?>
	</div><!-- end/row -->
</div><!-- end/container -->
<?php	 	 get_footer(); ?>