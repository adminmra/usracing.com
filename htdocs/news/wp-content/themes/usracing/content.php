	<?php	 	
/**
 * @package _usr
 */
?>


<?php	 	 // Styling Tip! 

// Want to wrap for example the post content in blog listings with a thin outline in Bootstrap style?
// Just add the class "panel" to the article tag here that starts below. 
// Simply replace post_class() with post_class('panel') and check your site!   
// Remember to do this for all content templates you want to have this, 
// for example content-single.php for the post single view. ?>

<div class="item-news col-medium-6 col-small-12">
<article id="post-<?php	 	 the_ID(); ?>" class="story" <?php	 	 //post_class(); ?>>
	
    <?php	 	 
    echo '<div class="img-news">';
	echo "<a href='". get_the_permalink()."'>";
	if ( has_post_thumbnail() ) { 		
		//echo "<a href='" . wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) . "' class='thumbnail fancybox-button zoomer'>";
		//echo "<span class='overlay-zoom'>";
	  	the_post_thumbnail('thumbnail', array('class' => 'img-responsive'));
	  	//echo "<div class='zoom-icon'></div></span>";
	  	
	}else{
		echo '<img src="'.get_site_url().'/wp-content/themes/usracing/images/placeholder.jpg" />';
	}
	echo "</a></div>";
?>
	<div class="entry-meta">
		<?php	 	 if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php	 	
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ' | ', 'usr' ) );
				if ( $categories_list && _usr_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php	 	 printf( __( '%1$s', 'usr' ), $categories_list ); ?>
			</span>
			<?php	 	 endif; // End if categories ?>

			<?php	 	
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ' | ', 'usr' ) );
				if ( $tags_list ) :
			?>
			<!--span class="tags-links">
				<?php	 	 printf( __( 'Tagged %1$s', 'usr' ), $tags_list ); ?>
			</span-->
			<?php	 	 endif; // End if $tags_list ?>
		<?php	 	 endif; // End if 'post' == get_post_type() ?>

		<?php	 	 //if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<!--<span class="comments-link"><?php	 	 //comments_popup_link( __( 'Leave a comment', 'usr' ), __( '1 Comment', 'usr' ), __( '% Comments', 'usr' ) ); ?></span>-->
		<?php	 	 //endif; ?>

		<?php	 	 edit_post_link( __( 'Edit', 'usr' ), '<span class="btn btn-simple edit-link">', '</span>' ); ?>
	</div><!-- .entry-meta -->
    <div class="info-news">
		<h3 class="blog-title"><a href="<?php	 	 the_permalink(); ?>" rel="bookmark">
			<?php 
				 the_title();
			?>
		</a></h3>
            <?php
		echo '<div class="postedby-news">';
		the_author_image( $author_id = get_the_author_id() );
		//$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , false) ;
		$date_post = '<span class="date-posts">' .get_the_date('F j, Y').'</span>';
		echo '<div class="info-postedby">';
		echo '<span class="wrap-name">By <span class="name-author">';
		echo the_author_posts_link();
		echo  '</span></span> ';
		//echo $date_post;
		echo '</div>';
		echo "</div>";
            ?>
		<?php	 	 if ( 'post' == get_post_type() ) : ?>
		
		<?php	 	 endif; ?>
	

	<?php	 	 if ( is_search() || is_archive() ) : // Only display Excerpts for Search and Archive Pages ?>
	<div class="teaser">
		<?php	 
			$content_r = get_the_excerpt();
			echo substr($content_r, 0, 200);
			if(strlen($content_r)>200){
				echo '...';	 
			}
		?>
	</div><!-- .entry-summary -->
	<?php	 	 else : ?>
	<div class="teaser">
		<?php	 	 //the_excerpt();//the_content( __( 'Read Article <span class="meta-nav">&rarr;</span>', 'usr' ) ); 
			$content_r = get_the_excerpt();
			echo substr($content_r, 0, 200);
			if(strlen($content_r)>200){
				echo '...';	 
			}
		?>
		<?php	 	
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'usr' ),
				'after'  => '</div>',
			) );
		?>
	</div>
    <div class="blog-date" style="display:block">
			<?php	 	 _usr_posted_on(); ?>
		</div> 
	</div> 
	<?php	 	 endif; ?>
	<footer>
		
	</footer>
	
</article><!-- #post-## -->
</div>
