<?php
/**
* _usr functions and definitions
*
* @package _usr
*/
//fix for cookie error while login.
setcookie(TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN);
if ( SITECOOKIEPATH != COOKIEPATH )
	setcookie(TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 1200; /* pixels */

if ( ! function_exists( '_usr_setup' ) ) :
/**
 * Set up theme defaults and register support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _usr_setup() {
    global $cap, $content_width;

    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();

    if ( function_exists( 'add_theme_support' ) ) {

		/**
		 * Add default posts and comments RSS feed links to head
		*/
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Enable support for Post Thumbnails on posts and pages
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		*/
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 300, 225, true );
		//add_image_size( 'featured-image', 300, 300, true );

		/**
		 * Enable support for Post Formats
		*/
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

		/**
		 * Setup the WordPress core custom background feature.
		*/
		add_theme_support( 'custom-background', apply_filters( '_usr_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

    }

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _usr, use a find and replace
	 * to change 'usr' to the name of your theme in all the template files
	*/
	load_theme_textdomain( 'usr', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	*/
    register_nav_menus( array(
        'primary'  => __( 'Header bottom menu', 'usr' ),
    ) );

}
endif; // _usr_setup
add_action( 'after_setup_theme', '_usr_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */

function _usr_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'usr' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget panel panel-white %2$s">',
		'before_title'  => '<div class="panel-heading"><h3 class="widget-title">',
		'after_title'   => '</h3></div>',
		'after_widget'  => '</aside>'
	) );
}
add_action( 'widgets_init', '_usr_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function _usr_scripts() {


    // load bootstrap css
    wp_enqueue_style( '_usr-bootstrap', '/../assets/plugins/bootstrap/css/bootstrap.min.css' );

	// load shared USR styles
	wp_enqueue_style( '_usr-assets-style', '/../assets/css/style.css' );
    // load WP theme styles

    wp_enqueue_style( '_usr-style', get_stylesheet_uri() );
	// load plugin styles
	wp_enqueue_style( '_usr-style-plugins', '/../assets/css/plugins.css' );
	// load jquery js
 	// wp_enqueue_script('_usr-jquery', '/../assets/plugins/jquery-1.10.2.min.js');
    // load bootstrap js
    wp_enqueue_script('_usr-bootstrapjs', '/../assets/plugins/bootstrap/js/bootstrap.min.js');

    // load bootstrap wp js
    //wp_enqueue_script( '_usr-bootstrapwp', get_template_directory_uri() . '/includes/js/bootstrap-wp.js', array('jquery') );

    //wp_enqueue_script( '_usr-skip-link-focus-fix', get_template_directory_uri() . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( '_usr-keyboard-image-navigation', get_template_directory_uri() . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
    }

    wp_enqueue_script( '_usr-news' , 'https://www.usracing.com/assets/js/news.js' );

}
add_action( 'wp_enqueue_scripts', '_usr_scripts' );

/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
//require get_template_directory() . '/includes/extras.php';

/**
 * Customizer additions.
 */
//require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/includes/jetpack.php';

/**
 * Load Jetpack compatibility file.
 */
//require get_template_directory() . '/includes/bootstrap-wp-navwalker.php';


/*  Excerpt More...
function new_excerpt_more($more) {
       global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

Excerpt Length
function custom_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
*/


/**  CUSTOM   */
// hide category posts for recap

function exclude_category( $query ) {
    if ( $query->is_home ) {
        $query->set( 'cat' , '-946' );
    }
    return $query;
}
add_filter( 'pre_get_posts' , 'exclude_category' );




function posts_template_for_home_usracing( $post_id ) {
    require_once "/home/ah/usracing.com/htdocs/news/NewsGen.php";
    NewsGen::save_id_post( $post_id );
    return $post_id;
}

add_filter( 'publish_post'   , 'posts_template_for_home_usracing' );
add_action( 'trash_post'     , 'posts_template_for_home_usracing' );
add_action( 'wp_trash_post'  , 'posts_template_for_home_usracing' );
add_action( 'delete_post'    , 'posts_template_for_home_usracing' );
add_action( 'wp_delete_post' , 'posts_template_for_home_usracing' );



// Strips width/height from Post Thumbnails
function remove_img_attr ($html) {
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}

add_filter( 'post_thumbnail_html', 'remove_img_attr' );

function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

/* Open Graph Meta tags */
function doctype_opengraph($output) {
   return $output . ' 
   xmlns:og="http://opengraphprotocol.org/schema/"';
}
add_filter('language_attributes', 'doctype_opengraph');

function fb_opengraph() {
  global $post;
    
   if(is_single()) {
      if(has_post_thumbnail($post->ID)) {
         $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
         $img_src = $img_src[0];
      } else {
         $img_src = get_stylesheet_directory_uri() . '/images/default.png';
      }   

      /*if($excerpt = $post->post_excerpt) {
         $excerpt = strip_tags($post->post_excerpt);
         $excerpt = str_replace("", "'", $excerpt);
      } else {
         $excerpt = get_bloginfo('description');
      }*/

   ?>  
 
    <meta property="og:title" content="<?php echo the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo $img_src; ?>"/>
 
    <?php
   } else {
      return;
   }
}
add_action('wp_head', 'fb_opengraph', 5);
