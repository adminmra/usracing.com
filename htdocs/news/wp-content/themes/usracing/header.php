<?php
/**
 * @package _usr
 */
?>
<!DOCTYPE html>
<html <?php	 	 language_attributes(); ?>>
<head>
<meta charset="<?php	 	 bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<!-- news libraries here -->
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">


<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php	 	 bloginfo( 'pingback_url' ); ?>">
<?php	 	 wp_head(); ?>
<script type="text/javascript" src="<?php echo get_home_url(); ?>/wp-content/themes/usracing/js/put5qvj.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link rel='stylesheet' id='sab-plugin-css'  href='//usracing.com/wp-content/plugins/simple-author-box/css/simple-author-box.min.css' type='text/css' media='all' />
<link href="//usracing.com/news/wp-content/themes/usracing/css/news.css" rel="stylesheet" type="text/css">

</head>

<body <?php	 	 body_class(); ?>>
	<?php	 	 do_action( 'before' ); ?>
<?php /*
<!-- Top -->
<div id="top" class="top">
 <div class="container2">
	  <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>
  <a class="logo logo-lrg" href="/" title="US Racing"><img id="logo-header" src="/img/usracing.png" alt="logo"></a>
  <a class="logo logo-sm" href="/" rel="nofollow"><img src="/img/usracing-sm.png"></a>
  <div class="topR">


  </div>
   
 </div>
</div><!--/top-->
*/ ?>
<?php
$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);  

$response = file_get_contents("https://www.usracing.com/newshtmlmenu", false, stream_context_create($arrContextOptions));
$response=str_replace('href="','rel="nofollow" target="_blank" href="//www.usracing.com', $response);
echo $response; ?>

<?php /*
<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">

	<!-- Toggle NAV -->
	 <!-- <div class="navbar-header">
   <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
     <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    
    <a id="or" class="navbar-toggle collapsed"><span style="display:inline">EXPLORE</span><i class="glyphicon glyphicon-remove" style="display:none"></i></a>
	</div>-->	

    <!-- Nav Itms -->
	<div class="collapse navbar-collapse navbar-responsive-collapse">
	<ul class="nav navbar-nav nav-justified">
	<?php
// replaces Smarty comment tag with HTML comment tag
/*
function callback($buffer) {
  $buffer = str_replace("{*", "<!-- ", $buffer);
  return (str_replace("*}", " -->", $buffer));
}

ob_start("callback");
*/ 
/*
//Fernando updated this on Sunday May 8th - start
$menu_includes = array();
//$menu_entries = file_get_contents('/home/ah/usracing.com/smarty/templates/primarymenu.tpl');
$menu_entries = file_get_contents('https://www.usracing.com/newshtmlmenu');
$re = "@{include file='(.+)'}|<li.+@";
preg_match_all($re, $menu_entries, $menu_includes);

$_menu_entry = "";
foreach( $menu_includes[0] as $menu_entry){
  preg_match( "@file='(.+)'@" , $menu_entry , $_menu_entry );

  if ( isset( $_menu_entry[1] ) ) {
    include ('/home/ah/usracing.com/smarty/templates/' . $_menu_entry[1] );
  }
  else
    echo $menu_entry;
}
/*Fernando updated this on Sunday May 8th - end*/

/* Fernando commented this
include ('/home/ah/usracing.com/smarty/templates/primarymenu/horsebetting.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/blog.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/kentuckyderby.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/futures.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/howtobet.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/promos.tpl');
include ('/home/ah/usracing.com/smarty/templates/primarymenu/about.tpl');

ob_end_flush(); 

	?>
	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->
*/ ?>

<!--
<header id="masthead" class="site-header" role="banner">
	<div class="container">
		<div class="row">
			<div class="site-header-inner col-sm-12">

				<?php	 	 //$header_image = get_header_image();
				//if ( ! empty( $header_image ) ) { ?>
					<a href="<?php	 	 //echo esc_url( home_url( '/' ) ); ?>" title="<?php	 	 //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php	 	 //header_image(); ?>" width="<?php	 	 //echo get_custom_header()->width; ?>" height="<?php	 	 //echo get_custom_header()->height; ?>" alt="">
					</a>
				<?php	 	 // } // end if ( ! empty( $header_image ) ) ?>


				<div class="site-branding">
					<h1 class="site-title"><a href="<?php	 	 //echo esc_url( home_url( '/' ) ); ?>" title="<?php	 	// echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php	 	 //bloginfo( 'name' ); ?></a></h1>
					<h4 class="site-description"><?php	 	 //bloginfo( 'description' ); ?></h4>
				</div>

			</div>
		</div>
	</div>
</header>

<nav class="site-navigation">
	<div class="container">
		<div class="row">
			<div class="site-navigation-inner col-sm-12">
				<div class="navbar navbar-default">
					<div class="navbar-header">
				    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
				    	<span class="sr-only">Toggle navigation</span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				      <span class="icon-bar"></span>
				    </button>

				    <a class="navbar-brand" href="<?php	 	 //echo esc_url( home_url( '/' ) ); ?>" title="<?php	 	 //echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php	 	 //bloginfo( 'name' ); ?></a>
				  </div>

	        <?php	 	 /*wp_nav_menu(
                array(
                    'theme_location' => 'primary',
                    'container_class' => 'collapse navbar-collapse navbar-responsive-collapse',
                    'menu_class' => 'nav navbar-nav',
                    'fallback_cb' => '',
                    'menu_id' => 'main-menu',
                    'walker' => new wp_bootstrap_navwalker()
                )
            ); */?>

				</div>
			</div>
		</div>
	</div>
</nav>-->

<!--point-->
