<?php	 		 	
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _usr
 */

get_header(); ?>

<div id="main" class="news-content">	
    <div class="inner-news container2">
        <div class="row">
          <div id="left-col" class="col-md-12">                               
                    
          <h1 style="color: #1e74b9; font-family: 'Lato', sans-serif;">Page Not Found</h1>



          <div class="content">
          <!-- --------------------- content starts here ---------------------- -->

          <p>Hmm.  It looks like we forgot something.</p>
          <p><a href="/" rel="nofollow">Go to the Home page</a></p>


          <p><img class="img-responsive" title="" alt="" src="https://www.usracing.com/images/forgotsomething.jpg"  border="0" width="100%" /></p>

          <p>(But we still love you.)</p>
          <!-- ------------------------ content ends -------------------------- -->
          </div> <!-- end/ content -->
          </div> <!-- end/ #left-col --> 
        </div><!-- end/row -->
    </div>	
</div>
<?php	 	 get_footer(); ?>
