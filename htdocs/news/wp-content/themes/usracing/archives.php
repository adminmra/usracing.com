<?php
/*
Template Name: Archives
*/
?>

<?php
get_header(); ?>

<div id="main" class="news-content">	
    <div class="inner-news container2">
        <div id="left-col" class="col-medium-9">
            <div class="widgetheader">
				<div class="categoriesleft">
				<?php the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
            
	<?php	 	 // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>	
		<header class="page-header">
			<h1 class="page-title">Archives by Month:</h1>
		</header><!-- .page-header -->

		<div class="page-content">
                    <p> <?php the_post(); ?> </p>
                    		<div class="page-header">
                    <ul>
			<?php wp_get_archives('type=monthly'); ?>
                    </ul>
		     
		</div><!-- .page-content -->

        </div>
                    </div>	

<?php	 	 get_sidebar(); ?>
        
</div>
</div>
<script>
jQuery(function(){
       jQuery('#cat option[value=-1]').remove();
       jQuery('#cat option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('#categories-dropdown-3 option[value=-1]').remove();
       jQuery('#categories-dropdown-3 option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
   });
   </script>
<?php	 	 get_footer(); ?>
