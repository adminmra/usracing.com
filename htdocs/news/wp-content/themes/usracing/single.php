<?php	 	
/**
 * The Template for displaying all single posts.
 *
 * @package _usr
 */

get_header(); ?>
<div id="left-col" class="col-medium-9">
<div class="widgetheader">
				<div class="categoriesleft">
				<select name="forma" onchange="location = this.value;">
					 <option value="/news">Latest Racing News</option>
					 <option value="/news/features">US Racing Features</option>
					 <option value="/news/analysis">Racing Analysis</option>
					 <option value="/news/handicapping-reports">Handicapping Reports</option>
					 <option value="/news/breeders-cup">Breeders' Cup News</option>
					 <option value="/news/kentucky-derby-road-to-the-roses">Kentucky Derby Road</option>
					 <option value="/news/recap">Race Recap</option>
					 <option value="/news/nfl">NFL Betting</option>
				</select>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
</div>
<div id="main" class="container2 news-content">
<div class="row">
<div id="left-col" class="col-medium-9">


	<?php	 	 while ( have_posts() ) : the_post(); ?>
	<?php /*
	<div class="hero-image">
		<?php the_post_thumbnail( 'medium_large' ); ?>
	</div>
	*/ ?>
		<?php	 	 get_template_part( 'content', 'single' ); ?>

		

		<?php	 	
			// If comments are open or we have at least one comment, load up the comment template
			//if ( comments_open() || '0' != get_comments_number() )
				//comments_template();
		?>

	<?php	 	 endwhile; // end of the loop. ?>
    
    

<?php	 	 _usr_content_nav( 'nav-below' ); ?>
    <div class="archives-link">
        <a href="<?php echo get_home_url(); ?>/archives" > Archives</a>
    </div>
</div> <!-- end/ #left-col -->

<?php	 	 get_sidebar(); ?>


</div><!-- end/row -->
</div><!-- end/container -->

<?php	 	 get_footer(); ?>
