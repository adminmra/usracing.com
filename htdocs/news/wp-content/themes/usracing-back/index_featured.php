<?php
/**
Template name: Featured Posts
 */

get_header(); ?>

<div class="subscribe-section col-md-12 col-sm-12 col-xs-12">
	<div class="inner-subscribe-section container">
			<h2>Horse Racing Subscribe Blog</h2>
			<p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </p>
			<div class="form-subscribe">
				<form method="post" action="">
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="text" name="firstname" placeholder="Your First Name" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="email" name="email" placeholder="Your Best Email Address" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="submit" value="Subscribe to the Blog" />
                    </div>
				</form>
			</div>
	</div>
</div>
<div id="main" class="news-content">
	<div class="inner-news container">
		<div id="left-col" class="col-md-9">
			<div class="headline"><h1>Horse Racing News</h1></div><!-- end/headline -->
				<div class="content">
					<div class="featured-news">
					<?php

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => -1,
					  //'post_status'    => 'publish',
					  'meta_query'     => array(
					    array(
					      'key'     => 'featured_post',
					      'value'     => '1',
					    )
					  )
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
							get_template_part(  'content'  , $the_query->the_post() );
							echo '<div class="postedby-featured">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">By <span class="name-post">' .  get_the_author() . '</span> ' . $date_post.'</div>';
							echo "</div>";
							//the_author_image_url($author_id = null);

					  }
					} else {
					  // no posts found
					}
					wp_reset_postdata(); 
					?>
					</div>
					<h1> Lastest Posts</h1>
					<div class="list-news">
						
							<?php

					//  6 Latest Posts

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => 9,
        		'orderby' => 'most_recent',
					  'post_status'    => 'publish',
					  
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
					  	the_post();
					  	/*echo '<pre>';
					  	//echo get_the_content();
					  	echo get_the_excerpt();
					  	echo '</pre>';*/

					  	echo '<div class="item-news col-md-6 col-xs-12">';
							get_template_part(  'content'  , $the_query->the_post() );
					  		//echo get_the_excerpt();
							echo '<div class="postedby-news">';
							the_author_image( $author_id = get_the_author_id() );
							$date_post = the_date( 'F j, Y' , '<span class="date-posts">' , '</span>' , FALSE) ;
							echo '<div class="info-postedby">By <span class="name-post">' .  get_the_author() . '</span> ' . $date_post.'</div>';
							echo "</div>";

						echo '</div>';
					  }
					} else {
					  // no posts found
					}
					/* Restore original Post Data */
					wp_reset_postdata(); ?>

				</div>
				</div> <!-- end/content -->
				<?php	 	 _usr_content_nav( 'nav-below' ); ?>
		</div> <!-- end/ #left-col -->
		<?php	 	 get_sidebar(); ?>
	</div><!-- end/row -->
</div><!-- end/container -->
<?php	 	 get_footer(); ?>