<?php
/**
Template name: Featured Posts b
 */

get_header(); ?>

<div class="subscribe-section col-md-12 col-sm-12 col-xs-12">
		<div class="inner-subscribe-section container">
			<h2>Horse Racing Subscribe Blog</h2>
			<p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </p>
			<div class="form-subscribe">
				<form method="post" action="">
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="text" name="firstname" placeholder="Your First Name" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="email" name="email" placeholder="Your Best Email Address" />
                    </div>
                    <div class="input-field col-md-4 col-xs-12">
					    <input type="submit" value="Subscribe to the Blog" />
                    </div>
				</form>
			</div>
		</div>
	</div>

<div id="main" class="news-content">
<div class="inner-news container">
	<div class="row">
		<div id="left-col" class="col-md-9">
			<div class="headline"><h1>Horse Racing News</h1></div><!-- end/headline -->
				<div class="content">

					<?php

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => -1,
					  //'post_status'    => 'publish',
					  'meta_query'     => array(
					    array(
					      'key'     => 'featured_post',
					      'value'     => '1',
					    )
					  )
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {

					  	if($the_query->has_post_thumbnail()) {
							echo 'si';
						}else{
							echo 'no';
						}
					    /*echo $the_query->the_post();
					    echo '<li>' . get_the_title() . '</li>';
					    */
					   echo "<pre>";
					   $author = get_the_author_id();
					   print_r( $author );
					   echo "</pre>";
					   	echo get_the_ID();

					   		$url_image = get_the_post_thumbnail_url( 'full' );  
					   		echo $url_image;
					   		echo 'here';
					   		echo '<img src="'.the_post_thumbnail_url().'"/>';
					   		echo 'here2';
					   	

						

							get_template_part(  'partials/content'  , $the_query->the_post() );
							the_author_image( $author_id = $author );
							$date_post = the_date( 'F j, Y' , '<h2>' , '</h2>' , FALSE) ;
							echo "By " .  get_the_author() . " - " . $date_post;
							//the_author_image_url($author_id = null);

					  }
					} else {
					  // no posts found
					}wp_reset_postdata(); ?>

					<h1> Lastest Posts</h1>

					<?php

					//  6 Latest Posts

					$args = array(
					  'post_type'      => 'post',
					  'posts_per_page' => 6,
        		'orderby' => 'most_recent',
					  'post_status'    => 'publish',
					  /*'meta_query'     => array(
					    array(
					      'key'     => 'featured_post' ,
					      'compare' => '!=',
					      'value'   =>	'1',
					    )
					  )*/
					);

					// The Query
					$the_query = new WP_Query( $args );

					// The Loop
					if ( $the_query->have_posts() ) {
					  while ( $the_query->have_posts() ) {
					  	if(has_post_thumbnail()) {
							echo 'si111';
							
						}else{
							echo 'no000';
						}

						echo '<img src="'.the_post_thumbnail_url().'"/>';
					    /*echo $the_query->the_post();
					    echo '<li>' . get_the_title() . '</li>';
					    */
							get_template_part(  'content'  , $the_query->the_post() );



					  }
					} else {
					  // no posts found
					}
					/* Restore original Post Data */
					wp_reset_postdata(); ?>


				</div> <!-- end/content -->
				<?php	 	 _usr_content_nav( 'nav-below' ); ?>
		</div> <!-- end/ #left-col -->
		<?php	 	 get_sidebar(); ?>
	</div><!-- end/row -->
</div><!-- end/container -->
</div><!-- end/news-content -->
<?php	 	 get_footer(); ?>