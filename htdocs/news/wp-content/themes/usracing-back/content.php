<?php	 	
/**
 * @package _usr
 */
?>


<?php	 	 // Styling Tip! 

// Want to wrap for example the post content in blog listings with a thin outline in Bootstrap style?
// Just add the class "panel" to the article tag here that starts below. 
// Simply replace post_class() with post_class('panel') and check your site!   
// Remember to do this for all content templates you want to have this, 
// for example content-single.php for the post single view. ?>


<article id="post-<?php	 	 the_ID(); ?>" class="story" <?php	 	 //post_class(); ?>>
	
    <?php	 	 
if ( has_post_thumbnail() ) { 		
	echo "<dt><a href='" . wp_get_attachment_url( get_post_thumbnail_id($post->ID) ) . "' class='thumbnail fancybox-button zoomer'><span class='overlay-zoom'>";
  	the_post_thumbnail('thumbnail', array('class' => 'img-responsive'));
  	echo "<div class='zoom-icon'></div></span></a></dt>";
} 
?>

    <dd>
		<h3 class="blog-title"><a href="<?php	 	 the_permalink(); ?>" rel="bookmark"><?php	 	 the_title(); ?></a></h3>

		<?php	 	 if ( 'post' == get_post_type() ) : ?>
		
		<?php	 	 endif; ?>
	

	<?php	 	 if ( is_search() || is_archive() ) : // Only display Excerpts for Search and Archive Pages ?>
	<div class="teaser">
		<?php	 	 the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php	 	 else : ?>
	<div class="teaser">
		<?php	 	 the_excerpt();//the_content( __( 'Read Article <span class="meta-nav">&rarr;</span>', 'usr' ) ); ?>
		<?php	 	
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'usr' ),
				'after'  => '</div>',
			) );
		?>
	</div>
    <div class="blog-date">
			<?php	 	 _usr_posted_on(); ?>
		</div> 
    </dd> 
	<?php	 	 endif; ?>

	<footer class="entry-meta">
		<?php	 	 if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search ?>
			<?php	 	
				/* translators: used between list items, there is a space after the comma */
				$categories_list = get_the_category_list( __( ', ', 'usr' ) );
				if ( $categories_list && _usr_categorized_blog() ) :
			?>
			<span class="cat-links">
				<?php	 	 printf( __( 'Posted in %1$s', 'usr' ), $categories_list ); ?>
			</span>
			<?php	 	 endif; // End if categories ?>

			<?php	 	
				/* translators: used between list items, there is a space after the comma */
				$tags_list = get_the_tag_list( '', __( ', ', 'usr' ) );
				if ( $tags_list ) :
			?>
			<span class="tags-links">
				<?php	 	 printf( __( 'Tagged %1$s', 'usr' ), $tags_list ); ?>
			</span>
			<?php	 	 endif; // End if $tags_list ?>
		<?php	 	 endif; // End if 'post' == get_post_type() ?>

		<?php	 	 //if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
		<!--<span class="comments-link"><?php	 	 //comments_popup_link( __( 'Leave a comment', 'usr' ), __( '1 Comment', 'usr' ), __( '% Comments', 'usr' ) ); ?></span>-->
		<?php	 	 //endif; ?>

		<?php	 	 edit_post_link( __( 'Edit', 'usr' ), '<span class="btn btn-simple edit-link">', '</span>' ); ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-## -->
