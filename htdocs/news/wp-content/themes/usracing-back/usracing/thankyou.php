<?php	 	
/**
 * Template Name: Thank you Page
 *
 * @package _usr
 */

get_header(); ?>

<div id="main" class="full-width thanyou">
<div class="row">
<div class="inner-page">


	<?php	 	 while ( have_posts() ) : the_post(); ?>
	<div class="hero-image">
		<?php the_post_thumbnail( 'medium_large' ); ?>
	</div>
		<?php	 	 //get_template_part( 'content', 'single' ); ?>

			<h1>Thank You</h1>
			<p>Thanks for subscribing to the Horse Racing News and Handicapping Report.</p>

		<?php	 	
			// If comments are open or we have at least one comment, load up the comment template
			//if ( comments_open() || '0' != get_comments_number() )
				//comments_template();
		?>

	<?php	 	 endwhile; // end of the loop. ?>
    
    

<?php	 	// _usr_content_nav( 'nav-below' ); ?>
</div> <!-- end/ inner page -->
</div><!-- end/row -->
</div><!-- end/container -->



<?php	 	 get_footer(); ?>