<?php	 	
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _usr
 */

get_header();
?>
<div class="subscribe-section col-md-12 col-sm-12 col-xs-12">
	<div class="inner-subscribe-section container3">
			<h2>News and Handicapping Reports</h2>
			<p>
                Breaking news, expert analysis and handicapping reports to help you win at the track.   Sign up to get the latest insights delivered straight to your inbox.
            </p>
            <div class="showafterform"><h3>Thanks for subscribing to the US Racing!</h3></div>
			<div class="form-subscribe">
				<form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
					<!-- The following code must be included to ensure your sign-up form works properly. -->
					<input data-id="ca:input" type="hidden" name="ca" value="6a796fab-de62-40d3-874e-d0ddf9ba3426">
					<input data-id="list:input" type="hidden" name="list" value="1770667721">
					<input data-id="source:input" type="hidden" name="source" value="EFD">
					<input data-id="required:input" type="hidden" name="required" value="list,email,first_name">
					<input data-id="url:input" type="hidden" name="url" value="">

					<div data-id="First Name:p" class="input-field col-md-4 col-xs-12">
						<label data-id="First Name:label" data-name="first_name" class="ctct-form-required"></label>
						<input type="text" name="first_name" placeholder="Your First Name" data-id="First Name:input" />
					</div>

					<div data-id="Email Address:p"  class="input-field col-md-4 col-xs-12">
						<label data-id="Email Address:label" data-name="email" class="ctct-form-required"></label>
						<input type="email" name="email" placeholder="Your Best Email Address" data-id="Email Address:input"  />
					</div>
					<div class="input-field col-md-4 col-xs-12">
						<label></label>
						<input type="submit" value="Subscribe Now" />
					</div>
				</form>
			</div>
	</div>
</div>
<div id="left-col" class="col-medium-9">
<div class="widgetheader">
				<div class="categoriesleft">
				<?php the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
</div>
<div id="main" class="container2 news-content">
<div class="row">
<div id="left-col" class="col-medium-9">
<?php	 	 // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?> <?php	 	 if ( have_posts() ) : ?> 
<div class="headline"><h1> <?php	 	
						if ( is_category() ) :
							single_cat_title();
	
						elseif ( is_tag() ) :
							single_tag_title();
	
						elseif ( is_author() ) :
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( 'Author: %s', 'usr' ), '<span class="vcard">' . get_the_author() . '</span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();
	
						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'usr' ), '<span>' . get_the_date() . '</span>' );
	
						elseif ( is_month() ) :
							printf( __( '<i class="fa fa-calendar"></i> Articles for:  %s', 'usr' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
	
						elseif ( is_year() ) :
							printf( __( '<i class="fa fa-calendar"></i> %s', 'usr' ), '<span>' . get_the_date( 'Y' ) . ' Articles</span>' );
	
						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'usr');
	
						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'usr' );
	
						else :
							_e( 'Archives', 'usr' );
	
						endif;
?></h1></div><!-- end/headline --> 
<?php	 	
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
<div class="list-news"> 
<?php $column=1; 	  // start the loop. ?>
<?php	 	 while ( have_posts() ) : the_post(); ?> 
		<?php	
			if($column % 2 != 0){
				echo '<div class="row">';
			}
		?>
	<?php	 	
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
?>
<?php
			if($column % 2 == 0){
				echo '</div>';
			}
			$column++;
		?>
<?php	 	 endwhile; ?> 
<?php 
			if($column % 2 == 0){
				echo '</div>';
			}
		?>
<?php wp_pagenavi(); ?>
<a id="archives-link" href="<?php echo get_home_url(); ?>/archives" > Archives</a>

<?php	 	 else : ?>
<?php	 	 get_template_part( 'no-results', 'archive' ); ?>
<?php	 	 endif; ?>
</div><!-- end/content --> 
</div> <!-- end/ #left-col --> 

<?php	 	 get_sidebar(); ?> </div><!-- end/row --> 
</div><!-- end/container --> 
<script type='text/javascript'>
   var localizedErrMap = {};
   localizedErrMap['required'] = 		'This field is required.';
   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
   localizedErrMap['list'] = 			'Please select at least one email list.';
   localizedErrMap['generic'] = 		'This field is invalid.';
   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
	localizedErrMap['state_province'] = 'Select a state/province';
   localizedErrMap['selectcountry'] = 	'Select a country';
   var postURL = 'https://visitor2.constantcontact.com/api/signup';
   
    jQuery(function(){
     jQuery('#cat option[value=-1]').remove();
     jQuery('#cat option').each(function(k,v){
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
     });
     jQuery('#categories-dropdown-3 option[value=-1]').remove();
     jQuery('#categories-dropdown-3 option').each(function(k,v){
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
         jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
     });
    jQuery('.wp-pagenavi').append(jQuery('#archives-link'));
   });
   
</script>
<script type='text/javascript' src='<?php echo get_site_url(); ?>/wp-content/themes/usracing/js/signup-form.js'></script>

<?php	 	 get_footer(); ?> 