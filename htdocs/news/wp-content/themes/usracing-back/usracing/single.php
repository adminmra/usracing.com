<?php	 	
/**
 * The Template for displaying all single posts.
 *
 * @package _usr
 */

get_header(); ?>
<div id="left-col" class="col-medium-9">
<div class="widgetheader">
				<div class="categoriesleft">
				<?php the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
</div>
<div id="main" class="container2 news-content">
<div class="row">
<div id="left-col" class="col-medium-9">


	<?php	 	 while ( have_posts() ) : the_post(); ?>
	<div class="hero-image">
		<?php the_post_thumbnail( 'medium_large' ); ?>
	</div>
		<?php	 	 get_template_part( 'content', 'single' ); ?>

		

		<?php	 	
			// If comments are open or we have at least one comment, load up the comment template
			//if ( comments_open() || '0' != get_comments_number() )
				//comments_template();
		?>

	<?php	 	 endwhile; // end of the loop. ?>
    
    

<?php	 	 _usr_content_nav( 'nav-below' ); ?>
    <div class="archives-link">
        <a href="<?php echo get_home_url(); ?>/archives" > Archives</a>
    </div>
</div> <!-- end/ #left-col -->

<?php	 	 get_sidebar(); ?>


</div><!-- end/row -->
</div><!-- end/container -->

<script>
var localizedErrMap = {};
localizedErrMap['required'] = 		'This field is required.';
localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
localizedErrMap['list'] = 			'Please select at least one email list.';
localizedErrMap['generic'] = 		'This field is invalid.';
localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
    localizedErrMap['state_province'] = 'Select a state/province';
localizedErrMap['selectcountry'] = 	'Select a country';
var postURL = 'https://visitor2.constantcontact.com/api/signup';

jQuery(function(){
       jQuery('#cat option[value=-1]').remove();
       jQuery('#cat option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('#categories-dropdown-3 option[value=-1]').remove();
       jQuery('#categories-dropdown-3 option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
   });
   </script>
   <script type='text/javascript' src='<?php echo get_site_url(); ?>/wp-content/themes/usracing/js/signup-form.js'></script>

<?php	 	 get_footer(); ?>