<?php	 		 	
/**
 * The template for displaying Search Results pages.
 *
 * @package _usr
 */

get_header(); ?>
	
<div id="search-page" class="news-content">	
	<div class="inner-news container2">
	<?php	 	 if ( have_posts() ) : ?>

		<div id="left-col" class="col-medium-9">
			<header class="page-header">
			<h1 class="page-title"><?php	 	 printf( __( 'Search Results for: %s', 'usr' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		</header><!-- .page-header --> 

		<div class="list-news">
		<?php $column=1; 	  // start the loop. ?>
		<?php	 	 while ( have_posts() ) : the_post(); ?>
		<?php	
			if($column % 2 != 0){
				echo '<div class="row">';
			}
		?>
		<?php	 	 get_template_part( 'content', 'search' ); ?>
		<?php
			if($column % 2 == 0){
				echo '</div>';
			}
			$column++;
		?>
		<?php endwhile; ?>
		<?php 
			if($column % 2 == 0){
				echo '</div>';
			}
		?>
		<?php wp_pagenavi(); ?>

		<?php	 	 else : ?>

			<?php	 	 get_template_part( 'no-results', 'search' ); ?>

		<?php	 	 endif; // end of loop. ?>
		</div>
		</div>
		<?php	 	 get_sidebar(); ?>
	</div>
</div>

<?php	 	 get_sidebar(); ?>
<?php	 	 get_footer(); ?>