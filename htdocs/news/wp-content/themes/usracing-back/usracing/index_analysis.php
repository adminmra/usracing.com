<?php
/**
Template name: Posts of Analysis
 */

get_header(); ?>


<div id="main" class="container">
	<div class="row">
		<div id="left-col" class="col-md-9">
			<div class="headline"><h1>Horse Racing Analysis 123</h1></div><!-- end/headline -->
				<div class="content">
					<?php
					$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
					query_posts( "cat=355&posts_per_page=10&paged=$paged" );

					?>
					<?php	 	 if ( have_posts() ) : ?>

						<?php	 	 /* Start the Loop */ ?>
						<?php	 	 while ( have_posts() ) : the_post(); ?>

							<?php
								/* Include the Post-Format-specific template for the content.
								 * If you want to overload this in a child theme then includ
								 echo get_query_var( 'paged' );e a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
							?>

						<?php	 	 endwhile; ?>



					<?php	 	 else : ?>

						<?php	 	 get_template_part( 'no-results', 'index' ); ?>

					<?php	 	 endif; ?>

					<?php
					//	comments_template('',true);
					//?>
				</div> <!-- end/content -->
				<?php	 	 _usr_content_nav( 'nav-below' ); ?>
		</div> <!-- end/ #left-col -->
		<?php	 	 get_sidebar(); ?>
	</div><!-- end/row -->
</div><!-- end/container -->
<?php	 	 get_footer(); ?>