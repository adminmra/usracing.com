<?php	 		 	
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package _usr
 */

get_header(); ?>

<div id="main" class="news-content">	
    <div class="inner-news container2">
        <div id="left-col" class="col-medium-9">
            <div class="widgetheader">
				<div class="categoriesleft">
				<?php the_widget( 'WP_Widget_Categories', 'dropdown=1&hierarchical=1' ); ?>
				</div>
				<div class="searchright">
				<?php the_widget( 'WP_Widget_Search' ); ?>
				</div> 	
			</div>
            
	<?php	 	 // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?>	
	<section class="content-padder error-404 not-found">

		<header class="page-header">
			<h2 class="page-title"><?php	 	 _e( 'Oops! Something went wrong here.', 'usr' ); ?></h2>
		</header><!-- .page-header -->

		<div class="page-content">

			<p><?php	 	 _e( 'Nothing could be found at this location. Maybe try a search?', 'usr' ); ?></p>

			<?php	 	 //get_search_form(); ?>

		</div><!-- .page-content -->

	</section><!-- .content-padder -->
        </div>
<?php	 	 get_sidebar(); ?>
        
    </div>	
</div>
<div id="left-col" class="col-medium-9">

</div>
<script>
jQuery(function(){
       jQuery('#cat option[value=-1]').remove();
       jQuery('#cat option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
       jQuery('#categories-dropdown-3 option[value=-1]').remove();
       jQuery('#categories-dropdown-3 option').each(function(k,v){
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
           jQuery(v).html(jQuery(v).html().replace('&nbsp;',''));
       });
   });
   </script>
<?php	 	 get_footer(); ?>