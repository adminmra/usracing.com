<?php	 	
/**
 * The Template for displaying all single posts.
 *
 * @package _usr
 */

get_header(); ?>

<div id="main" class="container news-content">
<div class="row">
<div id="left-col" class="col-md-9">


	<?php	 	 while ( have_posts() ) : the_post(); ?>
		<?php	 	 get_template_part( 'content', 'single' ); ?>
		<?php	 	
			// If comments are open or we have at least one comment, load up the comment template
			//if ( comments_open() || '0' != get_comments_number() )
				//comments_template();
		?>

	<?php	 	 endwhile; // end of the loop. ?>
    
    

<?php	 	 _usr_content_nav( 'nav-below' ); ?>
</div> <!-- end/ #left-col -->

<?php	 	 get_sidebar(); ?>


</div><!-- end/row -->
</div><!-- end/container -->



<?php	 	 get_footer(); ?>