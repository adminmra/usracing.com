<?php	 	
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _usr
 */

get_header();
?>

<div id="main" class="container news-content">
<div class="row">
<div id="left-col" class="col-md-9">

<?php	 	 // add the class "panel" below here to wrap the content-padder in Bootstrap style ;) ?> <?php	 	 if ( have_posts() ) : ?> 
<div class="headline"><h1> <?php	 	
						if ( is_category() ) :
							single_cat_title();
	
						elseif ( is_tag() ) :
							single_tag_title();
	
						elseif ( is_author() ) :
							/* Queue the first post, that way we know
							 * what author we're dealing with (if that is the case).
							*/
							the_post();
							printf( __( 'Author: %s', 'usr' ), '<span class="vcard">' . get_the_author() . '</span>' );
							/* Since we called the_post() above, we need to
							 * rewind the loop back to the beginning that way
							 * we can run the loop properly, in full.
							 */
							rewind_posts();
	
						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'usr' ), '<span>' . get_the_date() . '</span>' );
	
						elseif ( is_month() ) :
							printf( __( '<i class="fa fa-calendar"></i> Articles for:  %s', 'usr' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
	
						elseif ( is_year() ) :
							printf( __( '<i class="fa fa-calendar"></i> %s', 'usr' ), '<span>' . get_the_date( 'Y' ) . ' Articles</span>' );
	
						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'usr');
	
						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'usr' );
	
						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'usr' );
	
						else :
							_e( 'Archives', 'usr' );
	
						endif;
?></h1></div><!-- end/headline --> 
<?php	 	
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
<div class="content"> 
<?php	 	 /* Start the Loop */ ?>
<?php	 	 while ( have_posts() ) : the_post(); ?> <?php	 	
					/* Include the Post-Format-specific template for the content.
					 * If you want to overload this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );
?>
<?php	 	 endwhile; ?> 
<?php	 	 _usr_content_nav( 'nav-below' ); ?>
<?php	 	 else : ?>
<?php	 	 get_template_part( 'no-results', 'archive' ); ?>
<?php	 	 endif; ?>
</div><!-- end/content --> 
</div> <!-- end/ #left-col --> 

<?php	 	 get_sidebar(); ?> </div><!-- end/row --> 
</div><!-- end/container --> 

<?php	 	 get_footer(); ?> 