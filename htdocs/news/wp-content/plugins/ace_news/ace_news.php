<?php
/**
 * @package Ace_Newsa
 * @version 1.6
 */
/*
Plugin Name: Ace News
Plugin URI: http://acedevel.com
Description: Some tools for integrating news cache generation.
Author: Luis Fernando Villegas
Version: 1.6
Author URI: http://acedevel.com
*/

//https://wordpress.stackexchange.com/questions/134664/what-is-correct-way-to-hook-when-update-post
// if you don't add 3 as as 4th argument, this will not work as expected
add_action( 'save_post', 'ace_save_post_function', 10, 3 );

function ace_save_post_function( $post_ID, $post, $update ) {
	if(!$update){
		return;
	}
	
	if($post->post_status != "publish"){
		return;
	}

	$date = date("Y-m-d H:m");
	$output = "{$date}: {$post_ID}\n";
	//$output .= print_r($post, true) . "\n";
 	file_put_contents("ace_logs.txt", $output, FILE_APPEND);

	require '/home/ah/usracing.com/htdocs/news/GeneratorCache.php';
	$c = new GeneratorCache();
	$c->post_cache_by_id($post_ID);
	$c->generate_sidebar_latest();
	$c->generate_home_featured_post();
	$c->rsync_resources_for_news();
}

add_action( 'before_delete_post', 'ace_delete_post_function');

function ace_delete_post_function( $post_ID ) {
	$post = get_post($post_ID); 

	if($post->post_status != "trash"){
		return;
	}

	$archivo = $post->post_name.".php";
	$archivo = str_replace('__trashed.php', '.php', $archivo);
	
	$comando1 = 'find /home/ah/usracing.com/htdocs/news -iname ' . $post_ID . ' -exec rm {} \;';
	$comando2 = 'find /home/ah/usracing.com/htdocs/news -iname ' . $archivo . ' -exec rm {} \;';

	shell_exec($comando1);
	shell_exec($comando2);

	file_put_contents("ace_logs_deleted.txt", $post_ID . " - " . $archivo . "\n", FILE_APPEND);
}
