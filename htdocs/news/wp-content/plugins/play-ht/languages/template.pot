# Copyright (C) 2020 Play.ht
# This file is distributed under the same license as the Play.ht package.
msgid ""
msgstr ""
"Project-Id-Version: Play.ht 3.0.16\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wp-plugin\n"
"POT-Creation-Date: 2020-08-12 11:09:26+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2020-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"
"X-Generator: grunt-wp-i18n 1.0.3\n"

#: includes/classes/Ajax_Handler.php:35 includes/classes/Ajax_Handler.php:63
msgid "Error, Please provide a valid data"
msgstr ""

#: includes/classes/Backend.php:79
msgid "Play"
msgstr ""

#: includes/classes/Backend.php:213
msgid "You Cannot convert unsaved article!"
msgstr ""

#: includes/classes/Backend.php:349
msgid "Play - Convert this post to audio"
msgstr ""

#: includes/classes/Play_Admin_notice.php:31
msgid "The Article is sent successfully, it's being converted."
msgstr ""

#: includes/classes/Play_Requests.php:178
#: includes/classes/Play_Requests.php:193
#: includes/classes/Play_Requests.php:353
#: includes/classes/Play_Requests.php:506
msgid "Your article is sent in order to be converted."
msgstr ""

#: includes/classes/Play_Requests.php:337
#: includes/classes/Play_Requests.php:490
msgid "Your request is being processed."
msgstr ""

#: includes/classes/Play_Requests.php:368
#: includes/classes/Play_Requests.php:521 views/back-end/metabox_content.php:8
#: views/back-end/play_posts_filters.php:5
msgid "Select Language"
msgstr ""

#: includes/classes/Play_Requests.php:369
#: includes/classes/Play_Requests.php:522 views/back-end/metabox_content.php:18
#: views/back-end/play_posts_filters.php:11
msgid "Select Voice"
msgstr ""

#: includes/classes/Play_Requests.php:370
#: includes/classes/Play_Requests.php:523
msgid "Convert"
msgstr ""

#: includes/functions.php:80
msgid ""
"You must login first to be able to use play.ht to add audio to your "
"articles."
msgstr ""

#: includes/functions.php:87
msgid "Your subscription is over, you need to renew your subscription."
msgstr ""

#: includes/functions.php:92
msgid ""
"You've already used your own quota, please upgrade in order to convert more "
"articles."
msgstr ""

#: init.php:143
msgid "Template <code>%s</code> File not found, calculated path: <code>%s</code>"
msgstr ""

#: views/back-end/crisp_chat.php:1
msgid "Contact us"
msgstr ""

#: views/back-end/main_view_subscribed.php:14
msgid "Account"
msgstr ""

#: views/back-end/main_view_subscribed.php:15
msgid "Stats"
msgstr ""

#: views/back-end/main_view_subscribed.php:16
msgid "Words"
msgstr ""

#: views/back-end/main_view_subscribed.php:17
msgid "Article Player"
msgstr ""

#: views/back-end/main_view_subscribed.php:18
msgid "Post Types"
msgstr ""

#: views/back-end/main_view_subscribed.php:19
msgid "Subscribers"
msgstr ""

#: views/back-end/main_view_subscribed.php:20
msgid "Page Player"
msgstr ""

#: views/back-end/main_view_subscribed.php:21
msgid "Help"
msgstr ""

#: views/back-end/main_view_subscribed.php:22
msgid "See How to add audio to your articles"
msgstr ""

#: views/back-end/main_view_subscribed.php:29
msgid "Words remaining"
msgstr ""

#: views/back-end/main_view_subscribed.php:32
msgid "Buy Words"
msgstr ""

#: views/back-end/main_view_subscribed.php:36
msgid "Words usage"
msgstr ""

#: views/back-end/main_view_subscribed.php:37
msgid "Article Title"
msgstr ""

#: views/back-end/main_view_subscribed.php:37
msgid "Voice"
msgstr ""

#: views/back-end/main_view_subscribed.php:38
#: views/back-end/main_view_subscribed.php:42
#: views/back-end/main_view_subscribed.php:101
msgid "Loading..."
msgstr ""

#: views/back-end/main_view_subscribed.php:46
msgid "Period:"
msgstr ""

#: views/back-end/main_view_subscribed.php:47
msgid "Select starting date"
msgstr ""

#: views/back-end/main_view_subscribed.php:48
msgid "Select end date"
msgstr ""

#: views/back-end/main_view_subscribed.php:52
msgid "Article:"
msgstr ""

#: views/back-end/main_view_subscribed.php:53
msgid "Show stats for a specific article..."
msgstr ""

#: views/back-end/main_view_subscribed.php:90
msgid "Facing an issue? Please let us know in the chat."
msgstr ""

#: views/back-end/metabox_content.php:25
#: views/back-end/play_posts_filters.php:15
msgid "Narration Style"
msgstr ""

#: views/back-end/metabox_content.php:33
#: views/back-end/play_posts_filters.php:19
msgid "Convert to Audio"
msgstr ""

#: views/back-end/play_main_view.php:3
msgid "Convert Your Articles To Audio "
msgstr ""

#: views/back-end/play_main_view.php:7
msgid ""
"You will need first to Connect the WordPress Plugin with your Play.ht "
"account, click the below button to get started."
msgstr ""

#: views/back-end/play_main_view.php:11
msgid "Get Started"
msgstr ""

#: views/back-end/play_main_view.php:20
msgid ""
"Play adds a beautiful Embeddable audio player to your articles and "
"generates an iTunes compatible auto-updated feed for your users"
msgstr ""

#. Author of the plugin/theme
msgid "Play.ht"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Play.ht for Wordpress is a magical tool for converting your blog posts, "
"pages and courses to audio to increase content accessibility, user "
"engagement and time on page metrics."
msgstr ""

#. Author URI of the plugin/theme
msgid ""
"https://play.ht/wordpress/?utm_source=wordpress&utm_medium=plugin&utm_"
"campaign=plugin-page"
msgstr ""