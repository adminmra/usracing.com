<?php

namespace Wordpress\Play;


class Play_Requests extends Component
{


	/**
	 * Constructor
	 *
	 * @return void
	 */
	protected function init()
	{
		parent::init();

		// conversion scripts
		add_action('admin_enqueue_scripts', [&$this, 'load_admin_play_requests']);
	}

	/**
	 * Load scripts for requests
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function load_admin_play_requests()
	{
		// load path for scripts
		$load_path = WPPP_URI . (Helpers::is_script_debugging() ? 'assets/src/' : 'assets/dist/');
		$images_path = WPPP_URI . 'assets/images/';
		$post_author = [];
		$post_id = '';

		// handle back url upon register and subscription, load script to save appId and uid
		if (isset($_GET['appId']) && isset($_GET['uid']) && isset($_GET['page']) && 'play-welcome-page' == $_GET['page']) {

			$appId  = filter_input(INPUT_GET, 'appId', FILTER_SANITIZE_STRING);
			$userId = filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
			$blog   = site_url();

			if (preg_match("/https?:/", site_url())) {
				$blog = preg_replace('/https?:\/\//', '', site_url());
			}

			// save play query params
			update_option('wppp_blog_appId', $appId);
			update_option('wppp_blog_userId', $userId);

			// flag 1 (user connected to play)
			update_option('wppp_status_flag', 1);

			// add blog domain to remote
			wp_enqueue_script('add_blog_domain', $load_path . 'js/add_blog_domain.js', [
				'jquery',
			], wppp_version(), true);

			// localizing script.
			wp_localize_script('add_blog_domain', 'wppp_blog', [
				'ajax_url' => Add_Origin_URL,
				'user_id'  => $userId,
				'app_id'   => $appId,
				'bolg'     => $blog,
			]);

			wp_enqueue_script('helpers', $load_path . 'js/helpers.js', [], wppp_version(), true);

			// register firebase script.
			wp_register_script('firebase_script', 'https://www.gstatic.com/firebasejs/4.2.0/firebase.js', [], wppp_version(), true);

			// initialize firebase
			wp_enqueue_script('initialize_firebase', $load_path . 'js/initialize_firebase.js', [
				'jquery',
				'firebase_script',
			], wppp_version(), true);

			wp_enqueue_script('retrieve_user_data', $load_path . 'js/retrieve_user_data.js', [
				'jquery',
				'firebase_script',
				'helpers',
			], wppp_version(), true);

			// localizing script.
			wp_localize_script('retrieve_user_data', 'wppp_retrieve', [
				'user_id'  => get_option('wppp_blog_userId'),
				'app_id'  => get_option('wppp_blog_appId'),
				'ajax_url' => admin_url('admin-ajax.php'),
			]);

			// send referral theme
			wp_enqueue_script('referral_theme', $load_path . 'js/referral_theme.js', [
				'jquery',
			], wppp_version(), true);

			// localizing script.
			wp_localize_script('referral_theme', 'wppp_theme', [
				'ajax_url' => REFERRAL_URL,
				'user_id'  => $userId,
				'theme'    => get_template(),
			]);
		}

		// load conversion script only in edit
		if ('post-new.php' === $GLOBALS['pagenow'] || isset($_GET['action']) && isset($_GET['action']) && $_GET['action'] === 'edit') {

			$post_id = filter_input(INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT);
			// google analytics script
			wp_enqueue_script('analytics', $load_path . 'js/analytics.js', [
				'jquery',
			], wppp_version(), true);

			// regeneratorRuntime polyfill
			wp_enqueue_script('regenerator_runtime', $load_path . 'js/regenerator_runtime.js', [], wppp_version(), true);

			wp_enqueue_script('crisp_chat', $load_path . 'js/crisp_chat.js', [], wppp_version(), true);
			wp_enqueue_script('sweetalert', $load_path . 'js/sweetalert.min.js', [], wppp_version(), true);
			// register sentry
			wp_register_script('sentry_script', 'https://browser.sentry-cdn.com/5.7.1/bundle.min.js', [], wppp_version(), true);
			// initialize sentry
			wp_enqueue_script('initialize_sentry', $load_path . 'js/initialize_sentry.js', [
				'sentry_script',
			], wppp_version(), true);

			// Editor scripts
			wp_register_script('react_script', 'https://unpkg.com/react@16.8.6/umd/react.production.min.js', [], wppp_version(), true);
			wp_register_script('react_dom_script', 'https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js', [], wppp_version(), true);
			wp_register_script('editor_script', 'https://d1553uxug7aswy.cloudfront.net/js/editor.min.2.7.js', [], wppp_version(), true);

			wp_enqueue_script('save_conversion_data', $load_path . 'js/save_conversion_data.js', [
				'jquery',
				'regenerator_runtime',
				'firebase_script',
				'sweetalert',
				'analytics',
				'initialize_sentry',
				'crisp_chat',
				'react_script',
				'react_dom_script',
				'editor_script'
			], wppp_version(), true);

			wp_enqueue_script('popups', $load_path . 'js/popups.js', [
				'jquery',
				'regenerator_runtime',
				'analytics',
			], wppp_version(), true);

			wp_localize_script('popups', 'wppp_popups_data', [
				'default_language'         => 'english-us',
			]);


			wp_enqueue_style('additional_style', $load_path . 'css/additional_style.css', [], wppp_version());

			$credits = maybe_unserialize(get_option('wppp_play_user_data'));
			$credits = $credits['usage'];

			$post = get_post($post_id);
			$post_meta = maybe_unserialize(get_post_meta($post_id, 'play_podcast_data', true));
			if ($post_meta) {
				$post_meta['title'] = $post->post_title;
				$posts_meta['published_date'] = $post->post_date_gmt;
				$post_meta['image'] = wp_get_attachment_url(get_post_thumbnail_id($post_id), 'thumbnail');
			}

			$author_id = $post->post_author;
			$post_author['author_image'] 		= maybe_unserialize(get_the_author_meta('avatar', $author_id));
			$post_author['author_name'] 		= maybe_unserialize(get_the_author_meta('nickname', $author_id));
			$post_author['author_firstname'] 	= maybe_unserialize(get_the_author_meta('user_firstname', $author_id));
			$post_author['author_lastname'] 	= maybe_unserialize(get_the_author_meta('user_lastname', $author_id));

			wp_localize_script('save_conversion_data', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'post_id'           => $post_id,
				'article_url'       => wp_get_shortlink($post_id),
				'request_processed' => __('Your article is sent in order to be converted.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'post'				=> $post_meta,
				'current_credits'	=> $credits,
				'post_author'		=> $post_author,
				'post_title'		=> $post->post_title,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
			]);

			wp_localize_script('popups', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'post_id'           => $post_id,
				'article_url'       => wp_get_shortlink($post_id),
				'request_processed' => __('Your article is sent in order to be converted.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'post'				=> $post_meta,
				'current_credits'	=> $credits,
				'post_author'		=> $post_author,
				'post_title'		=> $post->post_title,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
			]);

			wp_localize_script('save_conversion_data', 'wppp_images', [
				'settings' 	=> $images_path . "settings.png",
				'checked'	=> $images_path . "wp-checked.png",
				'warning'	=> $images_path . "wp-warning.png",
				'cancel'	=> $images_path . "wp-cancel.png",
				'close' 	=> $images_path . "close.png",
				'audio'		=> $images_path . "audio-grey.png",
				'no_audio'	=> $images_path . "no-audio-grey.png",
				'no_credits'	=> $images_path . "no_credits.png",
				'audio_error'	=> $images_path . "audio_error.png",
				'shortcodeExplainer' => $images_path . "shortcode-explainer.gif",
			]);
		}

		$current_screen = get_current_screen();
		$post_type_a = get_option('playht_type_switch');
	
		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
		}

		if('edit' === $current_screen->base && (($post_type_a == 1 || empty($post_type_a)) && (get_post_type( get_the_ID() ) == 'post' || get_post_type( get_the_ID() ) == 'page')) ){
		/*if ('edit' === $current_screen->base && ('post' === $current_screen->post_type || 'page' === $current_screen->post_type || 'playht' === $current_screen->post_type || 'article' === $current_screen->post_type || 'news' === $current_screen->post_type || 'lp_lesson' === $current_screen->post_type || 'sfwd-lessons' === $current_screen->post_type || 'sfwd-courses' === $current_screen->post_type || 'sfwd-quiz' === $current_screen->post_type || 'sfwd-topic' === $current_screen->post_type || 'sfwd-question' === $current_screen->post_type)) {*/

			// regeneratorRuntime polyfill
			wp_enqueue_script('regenerator_runtime', $load_path . 'js/regenerator_runtime.js', [], wppp_version(), true);

			// google analytics script
			wp_enqueue_script('analytics', $load_path . 'js/analytics.js', [
				'jquery',
			], wppp_version(), true);
			wp_enqueue_script('sweetalert', $load_path . 'js/sweetalert.min.js', [], wppp_version(), true);
			wp_enqueue_script('crisp_chat', $load_path . 'js/crisp_chat.js', [], wppp_version(), true);
			// register sentry
			wp_register_script('sentry_script', '//cdn.ravenjs.com/3.23.1/raven.min.js', [], wppp_version(), true);
			// initialize sentry
			wp_enqueue_script('initialize_sentry', $load_path . 'js/initialize_sentry.js', [
				'sentry_script',
			], wppp_version(), true);

			wp_enqueue_script('wp_api', $load_path . 'js/wpapi.min.js', [], wppp_version(), true);

			// Editor scripts
			wp_register_script('react_script', 'https://unpkg.com/react@16.8.6/umd/react.production.min.js', [], wppp_version(), true);
			wp_register_script('react_dom_script', 'https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js', [], wppp_version(), true);
			wp_register_script('editor_script', 'https://d1553uxug7aswy.cloudfront.net/js/editor.min.2.7.js', [], wppp_version(), true);
			

			wp_enqueue_script('save_conversion_data', $load_path . 'js/save_conversion_data.js', [
				'jquery',
				'regenerator_runtime',
				'firebase_script',
				'sweetalert',
				'analytics',
				'initialize_sentry',
				'crisp_chat',
				'react_script',
				'react_dom_script',
				'editor_script',
				'wp_api',
			], wppp_version(), true);

			// get site url so we can use it in the client to init the rest API
			$site_url = get_site_url();

			// get the current user selected screen items_per_page option
			$screen = get_current_screen();
			$option = $screen->get_option('per_page', 'option');
			$user = get_current_user_id();
			$current_post_type = $current_screen->post_type;

			$post_type_a = get_option('playht_type_switch');
			 if(is_array($post_type_a)){
				$flip_array = array_keys($post_type_a);
			}
	        //var_dump($post_type_a);
	        if(($post_type_a == 1 || empty($post_type_a)) && ($current_post_type === 'page' || $current_post_type === 'post')) {
				$per_page = get_user_meta($user, $option, true);
				if ( empty ( $per_page ) || $per_page < 1 ) {
					$per_page = $screen->get_option( 'per_page', 'default' );
				}
			}elseif(in_array($current_post_type, $flip_array)){
				$per_page = get_user_meta($user, $option, true);
				if ( empty ( $per_page ) || $per_page < 1 ) {
					$per_page = $screen->get_option( 'per_page', 'default' );
				}
			}else{
				$per_page = -1;
			}
			
			$paged = (get_query_var('paged')) ? (get_query_var('paged') -1) : 0;
			$postOffset = $paged * $per_page;

			$args = array(
				'post_type' 	=> $current_post_type, //sfwd-lessons // lp_lesson
				'post_status' 	=> 'any',
				'offset'      	=> $postOffset,
				'posts_per_page' => $per_page,
				'order_by' => 'date',
				'order' => $current_post_type === 'page' ? 'ASC' : 'DESC' // default order for posts is date DESC, pages is date ASC
			);
			$post_list = get_posts($args);
			$posts_meta = array();
			$posts_authors = array();
			foreach ($post_list as $post) {
				$id = $post->ID;
				$author_id = $post->post_author;
				if (get_post_meta($id, 'play_podcast_data', true) === "") {
					$posts_meta[$id] = maybe_unserialize(get_post_meta($id));
				} else {
					$posts_meta[$id] = maybe_unserialize(get_post_meta($id, 'play_podcast_data', true));
				}
				$posts_meta[$id]['title'] = $post->post_title;
				$posts_meta[$id]['published_date'] = $post->post_date_gmt;
				if ($posts_meta[$id]['published_date'] === "0000-00-00 00:00:00") {
					$posts_meta[$id]['published_date'] = $post->post_modified_gmt;
				}
				$post_meta[$id]['image'] = wp_get_attachment_url(get_post_thumbnail_id($id), 'thumbnail');

				$posts_authors[$id]['author_image'] = maybe_unserialize(get_avatar_url($author_id, 146));
				$posts_authors[$id]['author_name'] = maybe_unserialize(get_the_author_meta('nickname', $author_id));
				$posts_authors[$id]['author_firstname'] = maybe_unserialize(get_the_author_meta('user_firstname', $author_id));
				$posts_authors[$id]['author_lastname'] = maybe_unserialize(get_the_author_meta('user_lastname', $author_id));
			}

			$credits = maybe_unserialize(get_option('wppp_play_user_data'));
			$credits = $credits['usage'];

			wp_localize_script('save_conversion_data', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'current_post_type' => $current_post_type,
				'rest_endpoint' => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' ),
				'request_processed' => __('Your request is being processed.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'home_url'			=> home_url('?p='),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'posts'				=> $posts_meta,
				'current_credits'	=> $credits,
				'posts_authors'		=> $posts_authors,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
				// 'fposts'			=> $post_list,
			]);

			wp_localize_script('popups', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'post_id'           => $post_id,
				'article_url'       => wp_get_shortlink($post_id),
				'request_processed' => __('Your article is sent in order to be converted.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'post'				=> $post_meta,
				'current_credits'	=> $credits,
				'post_author'		=> $post_author,
				'post_title'		=> $post->post_title,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
			]);

			wp_localize_script('quick_edit_box', 'wppp_quick_edit', [
				'admin_url'    => admin_url('admin-ajax.php'),
				'langs'        => get_option('wppp_language_list'),
				'langs_voices' => get_option('wppp_voices_list'),
				'select_lang'  => __('Select Language', WPPP_DOMAIN),
				'select_voice' => __('Select Voice', WPPP_DOMAIN),
				'convert'      => __('Convert', WPPP_DOMAIN),
				'can_convert'  => wppp_conversion_check(),
			]);

			wp_localize_script('save_conversion_data', 'wppp_images', [
				'settings' 	=> $images_path . "settings.png",
				'checked'	=> $images_path . "wp-checked.png",
				'warning'	=> $images_path . "wp-warning.png",
				'cancel'	=> $images_path . "wp-cancel.png",
				'close' 	=> $images_path . "close.png",
				'audio'		=> $images_path . "audio-grey.png",
				'no_audio'	=> $images_path . "no-audio-grey.png",
				'no_credits'	=> $images_path . "no_credits.png",
				'audio_error'	=> $images_path . "audio_error.png",
				'shortcodeExplainer' => $images_path . "shortcode-explainer.gif",
			]);
		}elseif('edit' === $current_screen->base && (($post_type_a != 1 || !empty($post_type_a)) && in_array($current_screen->post_type, $flip_array))){
			// regeneratorRuntime polyfill
			wp_enqueue_script('regenerator_runtime', $load_path . 'js/regenerator_runtime.js', [], wppp_version(), true);

			// google analytics script
			wp_enqueue_script('analytics', $load_path . 'js/analytics.js', [
				'jquery',
			], wppp_version(), true);
			wp_enqueue_script('sweetalert', $load_path . 'js/sweetalert.min.js', [], wppp_version(), true);
			wp_enqueue_script('crisp_chat', $load_path . 'js/crisp_chat.js', [], wppp_version(), true);
			// register sentry
			wp_register_script('sentry_script', '//cdn.ravenjs.com/3.23.1/raven.min.js', [], wppp_version(), true);
			// initialize sentry
			wp_enqueue_script('initialize_sentry', $load_path . 'js/initialize_sentry.js', [
				'sentry_script',
			], wppp_version(), true);

			wp_enqueue_script('wp_api', $load_path . 'js/wpapi.min.js', [], wppp_version(), true);

			// Editor scripts
			wp_register_script('react_script', 'https://unpkg.com/react@16.8.6/umd/react.production.min.js', [], wppp_version(), true);
			wp_register_script('react_dom_script', 'https://unpkg.com/react-dom@16.8.6/umd/react-dom.production.min.js', [], wppp_version(), true);
			wp_register_script('editor_script', 'https://d1553uxug7aswy.cloudfront.net/js/editor.min.2.7.js', [], wppp_version(), true);
			

			wp_enqueue_script('save_conversion_data', $load_path . 'js/save_conversion_data.js', [
				'jquery',
				'regenerator_runtime',
				'firebase_script',
				'sweetalert',
				'analytics',
				'initialize_sentry',
				'crisp_chat',
				'react_script',
				'react_dom_script',
				'editor_script',
				'wp_api',
			], wppp_version(), true);

			// get site url so we can use it in the client to init the rest API
			$site_url = get_site_url();

			// get the current user selected screen items_per_page option
			$screen = get_current_screen();
			$option = $screen->get_option('per_page', 'option');

			$user = get_current_user_id();
			$per_page = get_user_meta($user, $option, true);
			if ( empty ( $per_page ) || $per_page < 1 ) {
				$per_page = $screen->get_option( 'per_page', 'default' );

			}

			$current_post_type = $current_screen->post_type;
			$paged = (get_query_var('paged')) ? (get_query_var('paged') -1) : 0;
			$postOffset = $paged * $per_page;

			$post_type_a = get_option('playht_type_switch');
			if(is_array($post_type_a)){
				$flip_array = array_keys($post_type_a);
			}else{
				$flip_array = array();
			}

			$args = array(
				'post_type' 	=> $current_post_type, //sfwd-lessons // lp_lesson
				'post_status' 	=> 'any',
				'offset'      	=> $postOffset,
				'posts_per_page' => (in_array($current_post_type, $flip_array)) ? $per_page : -1,
				'order_by' => 'date',
				'order' => $current_post_type === 'page' ? 'ASC' : 'DESC' // default order for posts is date DESC, pages is date ASC
			);
			$post_list = get_posts($args);
			$posts_meta = array();
			$posts_authors = array();
			foreach ($post_list as $post) {
				$id = $post->ID;
				$author_id = $post->post_author;
				if (get_post_meta($id, 'play_podcast_data', true) === "") {
					$posts_meta[$id] = maybe_unserialize(get_post_meta($id));
				} else {
					$posts_meta[$id] = maybe_unserialize(get_post_meta($id, 'play_podcast_data', true));
				}
				$posts_meta[$id]['title'] = $post->post_title;
				$posts_meta[$id]['published_date'] = $post->post_date_gmt;
				if ($posts_meta[$id]['published_date'] === "0000-00-00 00:00:00") {
					$posts_meta[$id]['published_date'] = $post->post_modified_gmt;
				}
				$post_meta[$id]['image'] = wp_get_attachment_url(get_post_thumbnail_id($id), 'thumbnail');

				$posts_authors[$id]['author_image'] = maybe_unserialize(get_avatar_url($author_id, 146));
				$posts_authors[$id]['author_name'] = maybe_unserialize(get_the_author_meta('nickname', $author_id));
				$posts_authors[$id]['author_firstname'] = maybe_unserialize(get_the_author_meta('user_firstname', $author_id));
				$posts_authors[$id]['author_lastname'] = maybe_unserialize(get_the_author_meta('user_lastname', $author_id));
			}

			$credits = maybe_unserialize(get_option('wppp_play_user_data'));
			$credits = $credits['usage'];

			wp_localize_script('save_conversion_data', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'current_post_type' => $current_post_type,
				'rest_endpoint' => esc_url_raw( rest_url() ),
				'nonce' => wp_create_nonce( 'wp_rest' ),
				'request_processed' => __('Your request is being processed.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'home_url'			=> home_url('?p='),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'posts'				=> $posts_meta,
				'current_credits'	=> $credits,
				'posts_authors'		=> $posts_authors,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
				// 'fposts'			=> $post_list,
			]);

			wp_localize_script('popups', 'wppp_conv_data', [
				'admin_url'         => admin_url('admin-ajax.php'),
				'post_id'           => $post_id,
				'article_url'       => wp_get_shortlink($post_id),
				'request_processed' => __('Your article is sent in order to be converted.', WPPP_DOMAIN),
				'can_convert'       => wppp_conversion_check(),
				'appId'				=> get_option('wppp_blog_appId'),
				'userId'			=> get_option('wppp_blog_userId'),
				'post'				=> $post_meta,
				'current_credits'	=> $credits,
				'post_author'		=> $post_author,
				'post_title'		=> $post->post_title,
				'readAlongEnabled'  => get_option('playHt_readalong_switch')
			]);

			wp_localize_script('quick_edit_box', 'wppp_quick_edit', [
				'admin_url'    => admin_url('admin-ajax.php'),
				'langs'        => get_option('wppp_language_list'),
				'langs_voices' => get_option('wppp_voices_list'),
				'select_lang'  => __('Select Language', WPPP_DOMAIN),
				'select_voice' => __('Select Voice', WPPP_DOMAIN),
				'convert'      => __('Convert', WPPP_DOMAIN),
				'can_convert'  => wppp_conversion_check(),
			]);

			wp_localize_script('save_conversion_data', 'wppp_images', [
				'settings' 	=> $images_path . "settings.png",
				'checked'	=> $images_path . "wp-checked.png",
				'warning'	=> $images_path . "wp-warning.png",
				'cancel'	=> $images_path . "wp-cancel.png",
				'close' 	=> $images_path . "close.png",
				'audio'		=> $images_path . "audio-grey.png",
				'no_audio'	=> $images_path . "no-audio-grey.png",
				'no_credits'	=> $images_path . "no_credits.png",
				'audio_error'	=> $images_path . "audio_error.png",
				'shortcodeExplainer' => $images_path . "shortcode-explainer.gif",
			]);
		}
		if (isset($_GET['play'])) {
			if ('edit' === $current_screen->base && $_GET['play'] == 'del_podcast') {
				$this->handle_delete_process(($_GET['post']));
			}
		}
	}

	/**
	 * Handle podcast deletion
	 *
	 * @param $post_id
	 */
	public function handle_delete_process($post_id)
	{
		update_post_meta($post_id, 'deleted_podcast', get_post_meta($_GET['post'], 'play_podcast_data', true));
		delete_post_meta($post_id, 'play_podcast_data');
	}

	/**
	 * Get list of PLAY languages
	 *
	 * @return mixed|string
	 */
	public static function get_langs_list()
	{
		$languages = array(
			'English (US)' => array('label'    => 'English (US)', 'value'    	=> 'english-us',),
			'English (AU)' => array('label'    => 'English (AU)', 'value'    	=> 'english-au',),
			'English (UK)' => array('label'    => 'English (UK)', 'value'    	=> 'english-uk',),
			'English (Indian)' => array('label'    => 'English (Indian)', 'value'    	=> 'english-indian',),
			'English (Welsh)' => array('label'    => 'English (Welsh)', 'value'    	=> 'english-welsh',),
			'French' => array('label'    => 'French', 'value'    	=> 'french',),
			'French (Canada)' => array('label'    => 'French (Canada)', 'value'    	=> 'french-canada',),
			'German' => array('label'    => 'German', 'value'    	=> 'german',),
			'Dutch' => array('label'    => 'Dutch', 'value'    	=> 'dutch',),
			'Italian' => array('label'    => 'Italian', 'value'    	=> 'italian',),
			'Japanese' => array('label'    => 'Japanese', 'value'    	=> 'japanese',),
			'Korean' => array('label'    => 'Korean', 'value'    	=> 'korean',),
			'Portuguese (BR)' => array('label'    => 'Portuguese (BR)', 'value'    	=> 'portuguese-br',),
			'Portuguese' => array('label'    => 'Portuguese', 'value'    	=> 'portuguese',),
			'Spanish' => array('label'    => 'Spanish', 'value'    	=> 'spanish',),
			'Spanish (US)' => array('label'    => 'Spanish (US)', 'value'    	=> 'spanish-us',),
			'Spanish (MX)' => array('label'    => 'Spanish (MX)', 'value'    	=> 'spanish-mx',),
			'Swedish' => array('label'    => 'Swedish', 'value'    	=> 'swedish',),
			'Turkish' => array('label'    => 'Turkish', 'value'    	=> 'turkish',),
			'Welsh' => array('label'    => 'Welsh', 'value'    	=> 'welsh',),
			'Danish' => array('label'    => 'Danish', 'value'    	=> 'danish',),
			'Icelandic' => array('label'    => 'Icelandic', 'value'    	=> 'icelandic',),
			'Norwegian' => array('label'    => 'Norwegian', 'value'    	=> 'norwegian',),
			'Polish' => array('label'    => 'Polish', 'value'    	=> 'polish',),
			'Romanian' => array('label'    => 'Romanian', 'value'    	=> 'romanian',),
			'Russian' => array('label'    => 'Russian', 'value'    	=> 'russian',),
			'Arabic' => array('label'    => 'Arabic', 'value'    	=> 'arabic',),
			'Chinese' => array('label'    => 'Chinese', 'value'    	=> 'chinese',),
			'Hindi' => array('label'    => 'Hindi', 'value'    	=> 'hindi',),
			'Vietnamese' => array('label'    => 'Vietnamese', 'value'    	=> 'vietnamese',),
			'Filipino' => array('label'    => 'Filipino', 'value'    	=> 'filipino',),
			'Indonesian' => array('label'    => 'Indonesian', 'value'    	=> 'indonesian',),
			'Czech' => array('label'    => 'Czech', 'value'    	=> 'czech',),
			'Greek' => array('label'    => 'Greek', 'value'    	=> 'greek',),
			'Hungarian' => array('label'    => 'Hungarian', 'value'    	=> 'hungarian',),
			'Slovak' => array('label'    => 'Slovak', 'value'    	=> 'slovak',),
			'Ukrainian' => array('label'    => 'Ukrainian', 'value'    	=> 'ukrainian',),
			'Finnish' => array('label'    => 'Finnish', 'value'    	=> 'finnish',),
		);
		return $languages;
	}


	/**
	 * Get List of voices
	 *
	 * @return mixed|string
	 */
	public static function get_langs_voices_array()
	{
		$voi = array(
			array('value'    => 'Noah', 'name' => 'Noah', 'lang'    	=> 'english-us',),
			array('value'    => 'Matthew', 'name' => 'Matthew', 'lang'    	=> 'english-us',),
			array('value'    => 'Jessica', 'name' => 'Jessica', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-A', 'name' => 'James', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-B', 'name' => 'Robert', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-C', 'name' => 'Patricia', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-D', 'name' => 'Richard', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-E', 'name' => 'Elizabeth', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Wavenet-F', 'name' => 'Linda', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Standard-B', 'name' => 'Joseph', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Standard-C', 'name' => 'Sarah', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Standard-D', 'name' => 'Charles', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US-Standard-E', 'name' => 'Nancy', 'lang'    	=> 'english-us',),
			array('value'    => 'Joanna', 'name' => 'Joanna', 'lang'    	=> 'english-us',),
			array('value'    => 'Joey', 'name' => 'Joey', 'lang'    	=> 'english-us',),
			array('value'    => 'Salli', 'name' => 'Salli', 'lang'    	=> 'english-us',),
			array('value'    => 'Scarlett', 'name' => 'Scarlett', 'lang'    	=> 'english-us',),
			array('value'    => 'Lucas', 'name' => 'Lucas', 'lang'    	=> 'english-us',),
			array('value'    => 'Karen', 'name' => 'Karen', 'lang'    	=> 'english-us',),
			array('value'    => 'Victoria', 'name' => 'Victoria', 'lang'    	=> 'english-us',),
			array('value'    => 'Ariana', 'name' => 'Ariana', 'lang'    	=> 'english-us',),
			array('value'    => 'Jennifer', 'name' => 'Jennifer', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_AllisonVoice', 'name' => 'Samantha', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_LisaVoice', 'name' => 'Lisa', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_MichaelVoice', 'name' => 'Michael', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_AllisonV3Voice', 'name' => 'Grace', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_LisaV3Voice', 'name' => 'Camila', 'lang'    	=> 'english-us',),
			array('value'    => 'en-US_MichaelV3Voice', 'name' => 'Mark', 'lang'    	=> 'english-us',),
			array('value'    => 'en-GB-Standard-A', 'name' => 'Amelia', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Standard-B', 'name' => 'Jack', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Standard-C', 'name' => 'Mia', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Standard-D', 'name' => 'Thomas', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Wavenet-A', 'name' => 'Isla', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Wavenet-B', 'name' => 'Harry', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Wavenet-C', 'name' => 'Hannah', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB-Wavenet-D', 'name' => 'Charlie', 'lang'    	=> 'english-uk',),
			array('value'    => 'Amy', 'name' => 'Amy', 'lang'    	=> 'english-uk',),
			array('value'    => 'Brian', 'name' => 'Brian', 'lang'    	=> 'english-uk',),
			array('value'    => 'Emma', 'name' => 'Emma', 'lang'    	=> 'english-uk',),
			array('value'    => 'Emilia', 'name' => 'Emilia', 'lang'    	=> 'english-uk',),
			array('value'    => 'Logan', 'name' => 'Logan', 'lang'    	=> 'english-uk',),
			array('value'    => 'Anna', 'name' => 'Anna', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB_KateVoice', 'name' => 'Kate', 'lang'    	=> 'english-uk',),
			array('value'    => 'en-GB_KateV3Voice', 'name' => 'Lily', 'lang'    	=> 'english-uk',),
			array('value'    => 'Raveena', 'name' => 'Raveena', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Standard-A', 'name' => 'Alisha', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Standard-B', 'name' => 'Sai', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Standard-C', 'name' => 'Ayaan', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Wavenet-A', 'name' => 'Amara', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Wavenet-B', 'name' => 'Vivaan', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-IN-Wavenet-C', 'name' => 'Aditya', 'lang'    	=> 'english-indian',),
			array('value'    => 'en-AU-Wavenet-A', 'name' => 'Chloe', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Wavenet-B', 'name' => 'Ethan', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Wavenet-C', 'name' => 'Ava', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Wavenet-D', 'name' => 'Jackson', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Standard-A', 'name' => 'Charlotte', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Standard-B', 'name' => 'Oliver', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Standard-C', 'name' => 'Olivia', 'lang'    	=> 'english-au',),
			array('value'    => 'en-AU-Standard-D', 'name' => 'William', 'lang'    	=> 'english-au',),
			array('value'    => 'Nicole', 'name' => 'Nicole', 'lang'    	=> 'english-au',),
			array('value'    => 'Russell', 'name' => 'Russell', 'lang'    	=> 'english-au',),
			array('value'    => 'Geraint', 'name' => 'Geraint', 'lang'    	=> 'english-welsh',),
			array('value'    => 'Zeina', 'name' => 'Zeina', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-AR_OmarVoice', 'name' => 'Omar', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Wavenet-A', 'name' => 'Aisha', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Wavenet-B', 'name' => 'Amjad', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Wavenet-C', 'name' => 'Hazem', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Standard-A', 'name' => 'Alya', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Standard-B', 'name' => 'Idris', 'lang'    	=> 'arabic',),
			array('value'    => 'ar-XA-Standard-C', 'name' => 'Jalal', 'lang'    	=> 'arabic',),
			array('value'    => 'Zhiyu', 'name' => 'Zhiyu', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Standard-A', 'name' => 'Yu Yan', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Standard-B', 'name' => 'Zhang Wei', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Standard-C', 'name' => 'Wang Fang', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Wavenet-A', 'name' => 'Zhi Ruo', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Wavenet-B', 'name' => 'Li Wei', 'lang'    	=> 'chinese',),
			array('value'    => 'cmn-CN-Wavenet-C', 'name' => 'Li Na', 'lang'    	=> 'chinese',),
			array('value'    => 'cs-CZ-Standard-A', 'name' => 'Adina', 'lang'    	=> 'czech',),
			array('value'    => 'cs-CZ-Wavenet-A', 'name' => 'Adriana', 'lang'    	=> 'czech',),
			array('value'    => 'Gwyneth', 'name' => 'Gwyneth', 'lang'    	=> 'welsh',),
			array('value'    => 'Mads', 'name' => 'Mads', 'lang'    	=> 'danish',),
			array('value'    => 'Naja', 'name' => 'Naja', 'lang'    	=> 'danish',),
			array('value'    => 'da-DK-Standard-A', 'name' => 'Agathe', 'lang'    	=> 'danish',),
			array('value'    => 'da-DK-Wavenet-A', 'name' => 'Agnes', 'lang'    	=> 'danish',),
			array('value'    => 'de-DE-Standard-A', 'name' => 'Lara', 'lang'    	=> 'german',),
			array('value'    => 'de-DE-Standard-B', 'name' => 'Leon', 'lang'    	=> 'german',),
			array('value'    => 'de-DE-Wavenet-A', 'name' => 'Marie', 'lang'    	=> 'german',),
			array('value'    => 'de-DE-Wavenet-B', 'name' => 'Otto', 'lang'    	=> 'german',),
			array('value'    => 'de-DE-Wavenet-C', 'name' => 'Mila', 'lang'    	=> 'german',),
			array('value'    => 'de-DE-Wavenet-D', 'name' => 'Kurt', 'lang'    	=> 'german',),
			array('value'    => 'Hans', 'name' => 'Hans', 'lang'    	=> 'german',),
			array('value'    => 'Marlene', 'name' => 'Marlene', 'lang'    	=> 'german',),
			array('value'    => 'Vicki', 'name' => 'Vicki', 'lang'    	=> 'german',),
			array('value'    => 'de-DE_BirgitVoice', 'name' => 'Birgit', 'lang'    	=> 'german',),
			array('value'    => 'de-DE_DieterVoice', 'name' => 'Dieter', 'lang'    	=> 'german',),
			array('value'    => 'de-DE_BirgitV3Voice', 'name' => 'Lina', 'lang'    	=> 'german',),
			array('value'    => 'de-DE_DieterV3Voice', 'name' => 'Elias', 'lang'    	=> 'german',),
			array('value'    => 'el-GR-Standard-A', 'name' => 'Gaia', 'lang'    	=> 'greek',),
			array('value'    => 'el-GR-Wavenet-A', 'name' => 'Hera', 'lang'    	=> 'greek',),
			array('value'    => 'es-ES-Standard-A', 'name' => 'Nora', 'lang'    	=> 'spanish',),
			array('value'    => 'Conchita', 'name' => 'Conchita', 'lang'    	=> 'spanish',),
			array('value'    => 'Enrique', 'name' => 'Enrique', 'lang'    	=> 'spanish',),
			array('value'    => 'es-ES_EnriqueVoice', 'name' => 'Diego', 'lang'    	=> 'spanish',),
			array('value'    => 'es-ES_LauraVoice', 'name' => 'Laura', 'lang'    	=> 'spanish',),
			array('value'    => 'es-ES_EnriqueV3Voice', 'name' => 'Leonardo', 'lang'    	=> 'spanish',),
			array('value'    => 'es-ES_LauraV3Voice', 'name' => 'Lucia', 'lang'    	=> 'spanish',),
			array('value'    => 'Lucia', 'name' => 'Lucia', 'lang'    	=> 'spanish',),
			array('value'    => 'es-LA_SofiaVoice', 'name' => 'Valentina', 'lang'    	=> 'spanish-mx',),
			array('value'    => 'es-LA_SofiaV3Voice', 'name' => 'Luciana', 'lang'    	=> 'spanish-mx',),
			array('value'    => 'Mia', 'name' => 'Mia', 'lang'    	=> 'spanish-mx',),
			array('value'    => 'Lupe', 'name' => 'Lupe', 'lang'    	=> 'spanish-us',),
			array('value'    => 'Gabriella', 'name' => 'Gabriella', 'lang'    	=> 'spanish-us',),
			array('value'    => 'Isabella', 'name' => 'Isabella', 'lang'    	=> 'spanish-us',),
			array('value'    => 'Miguel', 'name' => 'Miguel', 'lang'    	=> 'spanish-us',),
			array('value'    => 'Penelope', 'name' => 'Penelope', 'lang'    	=> 'spanish-us',),
			array('value'    => 'es-US_SofiaVoice', 'name' => 'Sofia', 'lang'    	=> 'spanish-us',),
			array('value'    => 'es-US_SofiaV3Voice', 'name' => 'Valeria', 'lang'    	=> 'spanish-us',),
			array('value'    => 'fi-FI-Standard-A', 'name' => 'Anja', 'lang'    	=> 'finnish',),
			array('value'    => 'fi-FI-Wavenet-A', 'name' => 'Anneli', 'lang'    	=> 'finnish',),
			array('value'    => 'fil-PH-Standard-A', 'name' => 'Dalisay', 'lang'    	=> 'filipino',),
			array('value'    => 'fil-PH-Wavenet-A', 'name' => 'Analyn', 'lang'    	=> 'filipino',),
			array('value'    => 'fr-CA-Standard-A', 'name' => 'Lola', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Standard-B', 'name' => 'Gabriel', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Standard-C', 'name' => 'Camille', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Standard-D', 'name' => 'Arthur', 'lang'    	=> 'french-canada',),
			array('value'    => 'Chantal', 'name' => 'Chantal', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Wavenet-A', 'name' => 'Brigitte', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Wavenet-B', 'name' => 'Victor', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Wavenet-C', 'name' => 'Charlotte', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-CA-Wavenet-D', 'name' => 'Jules', 'lang'    	=> 'french-canada',),
			array('value'    => 'fr-FR-Standard-A', 'name' => 'Louise', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Standard-B', 'name' => 'Paul', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Standard-C', 'name' => 'Emma', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Standard-D', 'name' => 'Nathan', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Wavenet-A', 'name' => 'Adele', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Wavenet-B', 'name' => 'Raphael', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Wavenet-C', 'name' => 'Lina', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR-Wavenet-D', 'name' => 'Victor', 'lang'    	=> 'french',),
			array('value'    => 'Celine', 'name' => 'Celine', 'lang'    	=> 'french',),
			array('value'    => 'Mathieu', 'name' => 'Mathieu', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR_ReneeVoice', 'name' => 'Renee', 'lang'    	=> 'french',),
			array('value'    => 'Lea', 'name' => 'Léa', 'lang'    	=> 'french',),
			array('value'    => 'fr-FR_ReneeV3Voice', 'name' => 'Alice', 'lang'    	=> 'french',),
			array('value'    => 'Aditi', 'name' => 'Aditi', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Standard-A', 'name' => 'Esha', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Standard-B', 'name' => 'Ansh', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Standard-C', 'name' => 'Krishna', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Wavenet-A', 'name' => 'Deepa', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Wavenet-B', 'name' => 'Ishaan', 'lang'    	=> 'hindi',),
			array('value'    => 'hi-IN-Wavenet-C', 'name' => 'Rudra', 'lang'    	=> 'hindi',),
			array('value'    => 'hu-HU-Standard-A', 'name' => 'Adel', 'lang'    	=> 'hungarian',),
			array('value'    => 'hu-HU-Wavenet-A', 'name' => 'Aliz', 'lang'    	=> 'hungarian',),
			array('value'    => 'id-ID-Standard-A', 'name' => 'Farah', 'lang'    	=> 'indonesian',),
			array('value'    => 'id-ID-Standard-B', 'name' => 'Angga', 'lang'    	=> 'indonesian',),
			array('value'    => 'id-ID-Standard-C', 'name' => 'Farel', 'lang'    	=> 'indonesian',),
			array('value'    => 'id-ID-Wavenet-A', 'name' => 'Cindy', 'lang'    	=> 'indonesian',),
			array('value'    => 'id-ID-Wavenet-B', 'name' => 'Andy', 'lang'    	=> 'indonesian',),
			array('value'    => 'id-ID-Wavenet-C', 'name' => 'Aditya', 'lang'    	=> 'indonesian',),
			array('value'    => 'Dora', 'name' => 'Dora', 'lang'    	=> 'icelandic',),
			array('value'    => 'Karl', 'name' => 'Karl', 'lang'    	=> 'icelandic',),
			array('value'    => 'it-IT-Standard-A', 'name' => 'Giulia', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Wavenet-A', 'name' => 'Martina', 'lang'    	=> 'italian',),
			array('value'    => 'Carla', 'name' => 'Carla', 'lang'    	=> 'italian',),
			array('value'    => 'Giorgio', 'name' => 'Giorgio', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT_FrancescaVoice', 'name' => 'Francesca', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT_FrancescaV3Voice', 'name' => 'Angelina', 'lang'    	=> 'italian',),
			array('value'    => 'Bianca', 'name' => 'Bianca', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Standard-B', 'name' => 'Amara', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Standard-C', 'name' => 'Simone', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Standard-D', 'name' => 'Diego', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Wavenet-B', 'name' => 'Editta', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Wavenet-C', 'name' => 'Lorenzo', 'lang'    	=> 'italian',),
			array('value'    => 'it-IT-Wavenet-D', 'name' => 'Mattia', 'lang'    	=> 'italian',),
			array('value'    => 'ja-JP-Standard-A', 'name' => 'Hana', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Wavenet-A', 'name' => 'Yui', 'lang'    	=> 'japanese',),
			array('value'    => 'Mizuki', 'name' => 'Mizuki', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP_EmiVoice', 'name' => 'Emi', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP_EmiV3Voice', 'name' => 'Airi', 'lang'    	=> 'japanese',),
			array('value'    => 'Takumi', 'name' => 'Takumi', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Standard-B', 'name' => 'Himari', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Standard-C', 'name' => 'Daiki', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Standard-D', 'name' => 'Eito', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Wavenet-B', 'name' => 'Hina', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Wavenet-C', 'name' => 'Akira', 'lang'    	=> 'japanese',),
			array('value'    => 'ja-JP-Wavenet-D', 'name' => 'Aito', 'lang'    	=> 'japanese',),
			array('value'    => 'ko-KR-Standard-A', 'name' => 'Seo-yeon', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Wavenet-A', 'name' => 'Ha-yoon', 'lang'    	=> 'korean',),
			array('value'    => 'Seoyeon', 'name' => 'Seoyeon', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Standard-B', 'name' => 'Bo-young', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Standard-C', 'name' => 'Baul', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Standard-D', 'name' => 'Chae-koo', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Wavenet-B', 'name' => 'Chan-sook', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Wavenet-C', 'name' => 'Cheol', 'lang'    	=> 'korean',),
			array('value'    => 'ko-KR-Wavenet-D', 'name' => 'Chang-min', 'lang'    	=> 'korean',),
			array('value'    => 'Liv', 'name' => 'Liv', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-no-Standard-E', 'name' => 'Agot', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Standard-A', 'name' => 'Anja', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Standard-B', 'name' => 'Filip', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Standard-C', 'name' => 'Hella', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Standard-D', 'name' => 'Jakob', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-no-Wavenet-E', 'name' => 'Karina', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Wavenet-A', 'name' => 'Lise', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Wavenet-B', 'name' => 'Aksel', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Wavenet-C', 'name' => 'Margit', 'lang'    	=> 'norwegian',),
			array('value'    => 'nb-NO-Wavenet-D', 'name' => 'Mathias', 'lang'    	=> 'norwegian',),
			array('value'    => 'nl-NL-Standard-A', 'name' => 'Sophie', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Wavenet-A', 'name' => 'Julia', 'lang'    	=> 'dutch',),
			array('value'    => 'Lotte', 'name' => 'Lotte', 'lang'    	=> 'dutch',),
			array('value'    => 'Ruben', 'name' => 'Ruben', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL_EmmaVoice', 'name' => 'Emma', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL_LiamVoice', 'name' => 'Liam', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Standard-B', 'name' => 'Bram', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Standard-C', 'name' => 'Lucas', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Standard-D', 'name' => 'Tess', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Standard-E', 'name' => 'Fenna', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Wavenet-B', 'name' => 'Daan', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Wavenet-C', 'name' => 'Luuk', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Wavenet-D', 'name' => 'Zoe', 'lang'    	=> 'dutch',),
			array('value'    => 'nl-NL-Wavenet-E', 'name' => 'Eva', 'lang'    	=> 'dutch',),
			array('value'    => 'Ewa', 'name' => 'Ewa', 'lang'    	=> 'polish',),
			array('value'    => 'Jacek', 'name' => 'Jacek', 'lang'    	=> 'polish',),
			array('value'    => 'Jan', 'name' => 'Jan', 'lang'    	=> 'polish',),
			array('value'    => 'Maja', 'name' => 'Maja', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Standard-E', 'name' => 'Lena', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Standard-A', 'name' => 'Zofia', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Standard-B', 'name' => 'Kacper', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Standard-C', 'name' => 'Filip', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Standard-D', 'name' => 'Aleksandra', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Wavenet-A', 'name' => 'Alicja', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Wavenet-B', 'name' => 'Szymon', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Wavenet-C', 'name' => 'Antoni', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Wavenet-D', 'name' => 'Nikola', 'lang'    	=> 'polish',),
			array('value'    => 'pl-PL-Wavenet-E', 'name' => 'Oliwia', 'lang'    	=> 'polish',),
			array('value'    => 'pt-BR-Standard-A', 'name' => 'Maria', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'Ricardo', 'name' => 'Ricardo', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'Vitoria', 'name' => 'Vitoria', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'pt-BR_IsabelaVoice', 'name' => 'Isabela', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'pt-BR_IsabelaV3Voice', 'name' => 'Carolina', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'Camila', 'name' => 'Camila', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'Matilde', 'name' => 'Matilde', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'pt-BR-Wavenet-A', 'name' => 'Ines', 'lang'    	=> 'portuguese-br',),
			array('value'    => 'Cristiano', 'name' => 'Cristiano', 'lang'    	=> 'portuguese',),
			array('value'    => 'Ines', 'name' => 'Ines', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Standard-A', 'name' => 'Leonor', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Standard-B', 'name' => 'Duda', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Standard-C', 'name' => 'Emilio', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Standard-D', 'name' => 'Beatriz', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Wavenet-A', 'name' => 'Ana', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Wavenet-B', 'name' => 'Jordao', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Wavenet-C', 'name' => 'Marco', 'lang'    	=> 'portuguese',),
			array('value'    => 'pt-PT-Wavenet-D', 'name' => 'Mariana', 'lang'    	=> 'portuguese',),
			array('value'    => 'Carmen', 'name' => 'Carmen', 'lang'    	=> 'romanian',),
			array('value'    => 'Maxim', 'name' => 'Maxim', 'lang'    	=> 'russian',),
			array('value'    => 'Tatyana', 'name' => 'Tatyana', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Standard-A', 'name' => 'Galina', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Standard-B', 'name' => 'Abram', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Standard-C', 'name' => 'Katina', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Standard-D', 'name' => 'Borya', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Wavenet-A', 'name' => 'Annika', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Wavenet-B', 'name' => 'Alyosha', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Wavenet-C', 'name' => 'Khristina', 'lang'    	=> 'russian',),
			array('value'    => 'ru-RU-Wavenet-D', 'name' => 'Artyom', 'lang'    	=> 'russian',),
			array('value'    => 'sk-SK-Standard-A', 'name' => 'Natalia', 'lang'    	=> 'slovak',),
			array('value'    => 'sk-SK-Wavenet-A', 'name' => 'Nela', 'lang'    	=> 'slovak',),
			array('value'    => 'sv-SE-Standard-A', 'name' => 'Alice', 'lang'    	=> 'swedish',),
			array('value'    => 'Astrid', 'name' => 'Astrid', 'lang'    	=> 'swedish',),
			array('value'    => 'sv-SE-Wavenet-A', 'name' => 'Agnes', 'lang'    	=> 'swedish',),
			array('value'    => 'tr-TR-Standard-A', 'name' => 'Mira', 'lang'    	=> 'turkish',),
			array('value'    => 'Filiz', 'name' => 'Filiz', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Standard-B', 'name' => 'Ahmet', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Standard-C', 'name' => 'Aysel', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Standard-D', 'name' => 'Aysun', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Standard-E', 'name' => 'Ayaz', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Wavenet-A', 'name' => 'Aylin', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Wavenet-B', 'name' => 'Berat', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Wavenet-C', 'name' => 'Berna', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Wavenet-D', 'name' => 'Basak', 'lang'    	=> 'turkish',),
			array('value'    => 'tr-TR-Wavenet-E', 'name' => 'Omer', 'lang'    	=> 'turkish',),
			array('value'    => 'uk-UA-Standard-A', 'name' => 'Yulia', 'lang'    	=> 'ukrainian',),
			array('value'    => 'uk-UA-Wavenet-A', 'name' => 'Natalia', 'lang'    	=> 'ukrainian',),
			array('value'    => 'vi-VN-Standard-A', 'name' => 'Bich', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Standard-B', 'name' => 'Chi', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Standard-C', 'name' => 'Cam', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Standard-D', 'name' => 'Danh', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Wavenet-A', 'name' => 'Hau', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Wavenet-B', 'name' => 'Dung', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Wavenet-C', 'name' => 'Hoa', 'lang'    	=> 'vietnamese',),
			array('value'    => 'vi-VN-Wavenet-D', 'name' => 'Duong', 'lang'    	=> 'vietnamese',),
			array('value'    => 'zh-CN_LiNaVoice', 'name' => 'Lina', 'lang'    	=> 'chinese',),
			array('value'    => 'zh-CN_WangWeiVoice', 'name' => 'Wang', 'lang'    	=> 'chinese',),
			array('value'    => 'zh-CN_ZhangJingVoice', 'name' => 'Zhang', 'lang'    	=> 'chinese',),
					
		);

		return $voi;
	}
}
