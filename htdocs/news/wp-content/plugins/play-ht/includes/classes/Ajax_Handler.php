<?php

namespace Wordpress\Play;

/**
 * AJAX handler
 *
 * @package Wordpress\Play
 */
class Ajax_Handler extends Component
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	protected function init()
	{
		parent::init();

		if (defined('DOING_AJAX') && DOING_AJAX) {
			$action = filter_var(isset($_REQUEST['action']) ? $_REQUEST['action'] : '', FILTER_SANITIZE_STRING);
			if (method_exists($this, $action)) {
				// hook into action if it's method exists
				add_action('wp_ajax_' . $action, [&$this, $action]);
				//@todo check to add no_priev
			}
		}
	}

	public function get_post_content_data()
	{

		if (!isset($_POST) || empty($_POST)) {
			$this->error(['data' => __('Error, Please provide a valid data', WCDF_DOMAIN)]);
		}

		function remove_divi_shortcodes($content)
		{
			$content = preg_replace('/\[\/?et_pb.*?\]/', '', $content);
			$content = preg_replace('/\[\/?vc(.*?)\]/', '', $content);
			return $content;
		}

		$post_id               = filter_input(INPUT_POST, 'post_id',         FILTER_VALIDATE_INT);
		$article_content       = "<p>" . get_post_field('post_title', $post_id) . "</p>. " .  remove_divi_shortcodes(do_shortcode(get_post_field('post_content', $post_id)));
		$article_content       = str_replace('&nbsp;', ' ', $article_content);
		$article_content       = str_replace("</p>", "\n", $article_content);
		$article_content_array = explode("\n", wp_strip_all_tags($article_content));

		$this->success(['response' => $article_content_array]);

		exit(json_encode($this));
	}
	/**
	 * Save conversion data
	 */

	public function save_post_conversion_data()
	{

		if (!isset($_POST) || empty($_POST)) {
			$this->error(['data' => __('Error, Please provide a valid data', WCDF_DOMAIN)]);
		}

		function remove_divi_shortcodes($content)
		{
			$content = preg_replace('/\[\/?et_pb.*?\]/', '', $content);
			$content = preg_replace('/\[\/?vc(.*?)\]/', '', $content);
			return $content;
		}

		$post_id               = filter_input(INPUT_POST, 'post_id',         FILTER_VALIDATE_INT);
		$ssml                  = json_decode(filter_input(INPUT_POST, 'ssml'));
		$language              = filter_input(INPUT_POST, 'lang',            FILTER_SANITIZE_STRING);
		$voice                 = filter_input(INPUT_POST, 'voice',           FILTER_SANITIZE_STRING);
		$wordsCount            = filter_input(INPUT_POST, 'wordsCount',      FILTER_SANITIZE_STRING);
		$narrationStyle        = filter_input(INPUT_POST, 'narrationStyle',  FILTER_SANITIZE_STRING);
		$readAlongEnabled	   = filter_input(INPUT_POST, 'readAlongEnabled',  FILTER_SANITIZE_STRING);
		$play_article_id       = filter_input(INPUT_POST, 'play_article_id', FILTER_SANITIZE_STRING);
		$article_fallback_url  = filter_input(INPUT_POST, 'article_fallback_url', FILTER_SANITIZE_STRING);
		$post_url              = wp_get_shortlink($post_id) ? wp_get_shortlink($post_id) : $article_fallback_url;
		$article_content       = "<p>" . get_the_title($post_id) . "</p>\n\n" .  remove_divi_shortcodes(do_shortcode(get_post_field('post_content', $post_id)));
		$article_content = str_replace('</p><p>', ' ', $article_content);
		$article_content       = str_replace('&nbsp;', ' ', $article_content);
		$article_content_array = wp_strip_all_tags($article_content);
		$article_content_array = explode("\n\n", $article_content_array);
		$request_params = array(
			'url' 		=> $post_url,
			'voice' 	=> $voice,
			'narrationStyle' 	=> $narrationStyle,
			'enableReadAlong' => $readAlongEnabled,
			'appId'		=> get_option('wppp_blog_appId'),
			'content' => $article_content_array,
			'ssml' => $ssml,
			'wordsCount' => $wordsCount,
		);

		$transcription_response = wp_remote_post(Conversion_URL, array(
			'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
			'body'      => json_encode($request_params),
			'method'    => 'POST',
			'timeout'   => 50, // the default is 5 seconds, and used to fail sometimes!
		));

		if (is_wp_error($transcription_response)) {
			$error_message = $transcription_response->get_error_message();
			$this->error([
				'sent' => false,
				'error' => $error_message
			]);
		} else {
			$podcast_meta_data = get_post_meta($post_id, 'play_podcast_data', true);
			$parsed_body        = json_decode(wp_remote_retrieve_body($transcription_response), true);
			$trans_id           = $parsed_body['transcriptionId'];

			update_post_meta($post_id, 'play_podcast_data', maybe_serialize([
				'url'            => $post_url,
				'lang'           => $language,
				'voice'          => $voice,
				'trans_id'       => $trans_id,
				'audio_status'   => 1, // 0 not-converted, 1 converting, 2 Done
				'listens'        => 0,
				'convertion_time' => date('Y-m-d H:i:s'),
				'play_article_id' => $play_article_id,
			]));

			$this->success(['response' => $transcription_response]);
		}

		exit(json_encode($this));
	}

	/**
	 * On Article Conversion Sucess
	 */
	public function article_converted_success()
	{

		// Get the Post ID
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);

		// Get All Stored Play data
		$podcast_data = maybe_unserialize(get_post_meta($post_id, 'play_podcast_data', true));

		// Update the needed data
		$podcast_data['trans_id']      = filter_input(INPUT_POST, 'trans_id',  FILTER_SANITIZE_STRING);
		$podcast_data['article_audio'] = filter_input(INPUT_POST, 'audio_url', FILTER_SANITIZE_URL);
		$podcast_data['audio_status']  = 2; // converted successfully

		// update post meta
		update_post_meta($post_id, 'play_podcast_data', $podcast_data);
	}


	/**
	 * Save Draft
	 */
	public function save_article_draft()
	{

		// Get the Post ID
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);
		$voice = filter_input(INPUT_POST, 'voice', FILTER_SANITIZE_STRING);
		$play_article_id = filter_input(INPUT_POST, 'play_article_id', FILTER_SANITIZE_STRING);
		update_post_meta($post_id, 'play_podcast_data', maybe_serialize([
			'audio_status'   => 4, // 4 Draft
			'listens'        => 0,
			'voice'          => $voice,
			'play_article_id' => $play_article_id,
		]));
	}

	public function delete_article_draft()
	{
		// Get the Post ID
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);

		update_post_meta($post_id, 'play_podcast_data', maybe_serialize([
			'audio_status'   => 0, // 4 Draft
		]));
	}

	public function delete_article_audio()
	{
		// Get the Post ID
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);

		update_post_meta($post_id, 'deleted_podcast', get_post_meta($post_id, 'play_podcast_data', true));
		delete_post_meta($post_id, 'play_podcast_data');

	}

	/**
	 * On Article Conversion Failure
	 */
	public function article_converted_failed()
	{

		// Get the Post ID
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);

		// Get All Stored Play data
		$podcast_data = maybe_unserialize(get_post_meta($post_id, 'play_podcast_data', true));

		// Update the needed data
		$podcast_data['trans_id']      = '';
		$podcast_data['article_audio'] = '';
		$podcast_data['article_error'] = 'audio conversion failed';
		$podcast_data['audio_status']  = 3; // conversion failed

		// update post meta
		update_post_meta($post_id, 'play_podcast_data', $podcast_data);
	}

	/**
	 * Retrieve User data
	 */
	public function retrieve_user_data()
	{

		$userData = $_POST['userData'];
		$userData['plugin_user_saved'] = 1;

		$appId = $userData['appId'];
		// save Play user data
		update_option('wppp_play_user_data', maybe_serialize($userData));
		update_option('wppp_blog_appid',     maybe_serialize($appId));

		do_action('Play/retrieve_user_data');
	}

	/**
	 * Retrieve post meta data
	 */
	public function retrieve_post_meta_data()
	{
		$post_id = $_POST['post_id'];
		$post_meta = maybe_unserialize(get_post_meta($post_id, 'play_podcast_data', true));
		$this->success(['response' => $post_meta]);

		exit(json_encode($this));
	}

	/**
	 * Save article Listens Number
	 */
	public function get_listens_number()
	{

		//received data
		$listens = filter_input(INPUT_POST, 'listens', FILTER_SANITIZE_URL);
		$post_id = filter_input(INPUT_POST, 'post_id', FILTER_VALIDATE_INT);

		$podcast_data = maybe_unserialize(get_post_meta($post_id, 'play_podcast_data', true));
		$podcast_data['listens'] = $listens;

		// update post meta
		update_post_meta($post_id, 'play_podcast_data',    $podcast_data);
		update_post_meta($post_id, 'play_podcast_listens', $listens);

		do_action('Play/update_listens_number');
	}

	/**
	 * AJAX Debug response
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function debug($data)
	{
		// return dump
		$this->error($data);
	}

	/**
	 * AJAX Debug response ( dump )
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $args
	 *
	 * @return void
	 */
	public function dump($args)
	{
		// return dump
		$this->error(print_r(func_num_args() === 1 ? $args : func_get_args(), true));
	}

	/**
	 * AJAX Error response
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function error($data)
	{
		wp_send_json_error($data);
	}

	/**
	 * AJAX success response
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $data
	 *
	 * @return void
	 */
	public function success($data)
	{
		wp_send_json_success($data);
	}

	/**
	 * AJAX JSON Response
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $response
	 *
	 * @return void
	 */
	public function response($response)
	{
		// send response
		wp_send_json($response);
	}
}
