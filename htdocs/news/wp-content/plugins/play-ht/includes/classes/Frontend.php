<?php

namespace Wordpress\Play;

/**
 * Frontend logic
 *
 * @package Wordpress\Play
 */
class Frontend extends Component
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	protected function init()
	{
		parent::init();
		//append podcast to post
		add_filter('the_content', array($this, 'append_podcast_to_post'));
		add_action('wp_enqueue_scripts', array($this, 'load_play_scripts'));

		add_action('wp_footer', array($this, 'load_play_footer_scripts') );

		// For Accelerated Mobile Pages (AMP)
		add_action('pre_amp_render_post', array($this, 'amp_content_filters'));
	}

	/**
	 * Add the podcast to the post
	 *
	 * @param $content
	 *
	 * @return string
	 */
	public function append_podcast_to_post($content)
	{
		$images_path = WPPP_URI . 'assets/images/';

		global $post;

		$podcast_meta_data = get_post_meta(get_the_ID(), 'play_podcast_data', true);
		if (!$podcast_meta_data) {
			return $content;
		}
		$podcast_data = maybe_unserialize($podcast_meta_data);

		if (is_singular() && !is_home() && !is_front_page()) {
			$post_type_a = get_option('playht_type_switch');
			if(is_array($post_type_a)){
				$flip_array = array_keys($post_type_a);
			}

        	if(($post_type_a == 1 || empty($post_type_a)) && (get_post_type( get_the_ID() ) == 'post' || get_post_type( get_the_ID() ) == 'page') ){
        		// check if status =2 (article successfully converted) and the page player is enabled : show player
				$podcast = '';
				$play_button = '';
				if ($podcast_data['audio_status'] == 2) {
					if (get_option('playHt_articleplayer_switch', "") === "1") {
						// if shortcode is in the page, don't add EP
						if (!(is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'playht_player') )) {
							$podcast = WPPP_view('front-end/podcast_iframe', [
								'article_url'   => $podcast_data['url'],
								'article_voice' => $podcast_data['voice'],
								'trans_id'		=> $podcast_data['trans_id'],
								'blog_app_id'	=> get_option('wppp_blog_appId'),
							], true);
						}
					}

					$play_button = WPPP_view('front-end/podcast_button', [
						'article_url'   => $podcast_data['url'],
						'article_voice' => $podcast_data['voice'],
						'blog_app_id'	=> get_option('wppp_blog_appId'),
						'article_audio'	=> $podcast_data['article_audio'],
						'listen_icon'   => $images_path . "listen-icon.svg",
					], true);
				}
				return $play_button . $podcast . $content;
        	}elseif(in_array(get_post_type( get_the_ID() ), $flip_array)){
        		// check if status =2 (article successfully converted) and the page player is enabled : show player
				$podcast = '';
				$play_button = '';
				if ($podcast_data['audio_status'] == 2) {
					if (get_option('playHt_articleplayer_switch', "") === "1") {
						// if shortcode is in the page, don't add EP
						if (!(is_a($post, 'WP_Post') && has_shortcode($post->post_content, 'playht_player') )) {
							$podcast = WPPP_view('front-end/podcast_iframe', [
								'article_url'   => $podcast_data['url'],
								'article_voice' => $podcast_data['voice'],
								'trans_id'		=> $podcast_data['trans_id'],
								'blog_app_id'	=> get_option('wppp_blog_appId'),
							], true);
						}
					}

					$play_button = WPPP_view('front-end/podcast_button', [
						'article_url'   => $podcast_data['url'],
						'article_voice' => $podcast_data['voice'],
						'blog_app_id'	=> get_option('wppp_blog_appId'),
						'article_audio'	=> $podcast_data['article_audio'],
						'listen_icon'   => $images_path . "listen-icon.svg",
					], true);
				}
				return $play_button . $podcast . $content;
        	}
			
		} else if (is_home() && is_front_page()) {
			// check if status =2 (article successfully converted) : show player
			$play_button = '';
			if ($podcast_data['audio_status'] == 2) {
				$play_button = WPPP_view('front-end/podcast_button', [
					'article_url'   => $podcast_data['url'],
					'article_voice' => $podcast_data['voice'],
					'blog_app_id'	=> get_option('wppp_blog_appId'),
					'article_audio'	=> $podcast_data['article_audio'],
					'listen_icon'   => $images_path . "listen-icon.svg",
				]);
			}
			// return $play_button . $content;
			return $content;
		} else {
			return $content;
		}
	}


	public function load_play_footer_scripts() {
		$load_path = WPPP_URI . (Helpers::is_script_debugging() ? 'assets/src/' : 'assets/dist/');
		$post_type_a = get_option('playht_type_switch');
		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
		}

		$podcast_meta_data = get_post_meta(get_the_ID(), 'play_podcast_data', true);
	
		$podcast_data = maybe_unserialize($podcast_meta_data);

    	if(($post_type_a == 1 || empty($post_type_a)) && (get_post_type( get_the_ID() ) == 'post' || get_post_type( get_the_ID() ) == 'page') && !empty($podcast_data)){
    		wp_enqueue_script('play_footer_scripts', $load_path . 'js/play_footer_scripts.js', [], wppp_version(), true);
    	}elseif(in_array(get_post_type( get_the_ID() ), $flip_array) && !empty($podcast_data) ){
    		wp_enqueue_script('play_footer_scripts', $load_path . 'js/play_footer_scripts.js', [], wppp_version(), true);
    	}
	}

	/**
	 * The podcast load play scripts
	 */
	public function load_play_scripts()
	{
		if (is_singular()) {
			$post_type_a = get_option('playht_type_switch');
			if(is_array($post_type_a)){
				$flip_array = array_keys($post_type_a);
			}

			$podcast_meta_data = get_post_meta(get_the_ID(), 'play_podcast_data', true);
	
			$podcast_data = maybe_unserialize($podcast_meta_data);

        	if((($post_type_a == 1 || empty($post_type_a)) && (get_post_type( get_the_ID() ) == 'post' || get_post_type( get_the_ID() ) == 'page') && !empty($podcast_data)) ||  (in_array(get_post_type( get_the_ID() ), $flip_array) && !empty($podcast_data) )){
        		global $post;
				$current_post = get_post(get_the_ID());
				$podcast_meta_data = get_post_meta(get_the_ID(), 'play_podcast_data', true);
				$podcast_data = maybe_unserialize($podcast_meta_data);
				// if (!empty($podcast_data['audio_status']) && $podcast_data['audio_status'] == 2) {
					// load path for scripts
					$load_path = WPPP_URI . (Helpers::is_script_debugging() ? 'assets/src/' : 'assets/dist/');
					$images_path = WPPP_URI . 'assets/images/';

					wp_enqueue_script('dashboard_wppp_pageplayer', 'https://static.play.ht/playht-pageplayer-plugin-v12.js', array(), wppp_version(), true);
					wp_enqueue_style('dashboard_wppp_pageplayer_styles', 'https://static.play.ht/playht-pageplayer-plugin-v10.css', [], wppp_version());

					wp_enqueue_script('page_player', $load_path . 'js/page_player.js', ['dashboard_wppp_pageplayer'], wppp_version(), true);

					wp_localize_script('page_player', 'wppp_page_player', [
						'current_article_play_data' => $podcast_data,
						'current_article_data' => $current_post
					]);



					wp_localize_script('page_player', 'wppp_user_data', [
						'user_id'  => get_option('wppp_blog_userId'),
						'app_id'  => get_option('wppp_blog_appId'),
						'ajax_url' => admin_url('admin-ajax.php'),

						'playhtButtonSwitch' 		=> !empty($podcast_data['audio_status']) && $podcast_data['audio_status'] == 2 ? get_option('playht_button_switch', "1") : false,
						'playhtListenbuttonSwitch' => get_option('playht_Listenbutton_switch', "1"),
						'playHtbuttonWLabel' 		=> get_option('playht_button_wlabel', ""),
						'playHtcolor_backgrund' 	=> get_option('playHtcolor_backgrund', "#222"),
						'playHttextColor' 			=> get_option('playHttextColor', "#fff"),
						'FielddesktopPositionID' 	=> get_option('FielddesktopPositionID', "right"),
						'FieldmobilePositionID' 	=> get_option('FieldmobilePositionID', "right"),
						'playHtDarkMode' 			=> get_option('playHtDarkMode', ""),
						'playHtPlayerItemsColor' 	=> get_option('playHtPlayerItemsColor', "#fff"),
						'playHtPlayerTextColor' 	=> get_option('playHtPlayerTextColor', "#fff"),
						'playHtPlayerBackgroundColor' => get_option('playHtPlayerBackgroundColor', "#222"),
						'fullScreenMobEnabledID' 	=> get_option('fullScreenMobEnabledID', "1"),
						'playHtListencolor_backgrund' => get_option('playHtListencolor_backgrund','#222'),
						'playHtListenBorderColor' => get_option('playHtListenBorderColor', '#222' ),
						'playHtListenBorderRadius' => get_option('playHtListenBorderRadius', '2' ),
						'playHtListenText' => get_option('playHtListenText', 'Listen' ),
						'playHtListentextColor' => get_option('playHtListentextColor', '#fff' )
					]);

					wp_enqueue_style('page_player', $load_path . 'css/page_player.css', [], wppp_version());
					wp_localize_script('page_player', 'wppp_player_images', [
						'close' 	=> $images_path . "close.png",
						'loader'	=> $images_path . "loader.gif",
						'play_btn'	=> $images_path . "play-btn.png",
						'pause_btn'	=> $images_path . "pause-btn.png",
						'pageplayer_placeholder'	=> $images_path . "pageplayer_placeholder.png",
					]);

					add_action('wp_footer', array($this, 'show_play_btn') );
        	}
			
			// }
		}
	}

	/**
	 * For Accelerated Mobile Pages (AMP)
	 */
	function amp_content_filters()
	{
		// Remove Filter
		remove_filter('the_content', array($this, 'append_podcast_to_post'));
		remove_action('wp_enqueue_scripts', array($this, 'load_play_scripts'));

		add_filter('the_content', array($this, 'palyiframe_shortcodes'));
		add_action('amp_post_template_css', array($this, 'ampforwp_add_custom_css'));
	}
	/**
	 * Function For Accelerated Mobile Pages (AMP)
	 */
	function palyiframe_shortcodes($content)
	{

		global $post;
		$podcast_meta_data = get_post_meta(get_the_ID(), 'play_podcast_data', true);
		$podcast_data = maybe_unserialize($podcast_meta_data);

		if (is_array($podcast_data) && $podcast_data['audio_status'] && $podcast_data['audio_status'] == 2) {
			$podcast = WPPP_view('front-end/podcast_iframe', [
				'article_url'   => $podcast_data['url'],
				'article_voice' => $podcast_data['voice'],
				'blog_app_id'	=> get_option('wppp_blog_appId'),
			], true);
			return $podcast . $content;
		}
		return $content;
	}

	function show_play_btn() {
		echo "<script>document.querySelectorAll('.playHtListenArea').forEach(function(el) {el.style.display = 'block'});</script>";
	}

	/**
	 * Add Custom Css For Accelerated Mobile Pages (AMP)
	 */
	function ampforwp_add_custom_css()
	{ ?>
		amp-iframe.playht-iframe-player{background: inherit;}
		amp-iframe.playht-iframe-player{width: 100%;}
<?php }
}
