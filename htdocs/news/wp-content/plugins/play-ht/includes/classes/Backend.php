<?php

namespace Wordpress\Play;

/**
 * Backend logic
 *
 * @package Wordpress\Play
 */
class Backend extends Component
{
	/**
	 * Constructor
	 *
	 * @return void
	 */
	protected function init()
	{
		parent::init();

		// avoid injecting our content on learnPress plugin pages
		if (isset($_GET['post_type']) && $_GET['post_type'] === 'lp_course') {
			return false;
		}
		/*$post_type_a = get_option('playht_type_switch');
		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
			if (isset($_GET['post_type']) && !in_array($_GET['post_type'], $flip_array)){
				return false;
			}
		}*/
		
		// hook to add plugin admin menu page
		add_action('admin_menu', [&$this, 'add_admin_menu_page']);
		
		// add activation processes needed
		register_activation_hook(WPPP_MAIN_FILE, [&$this, 'on_activation_process']);
		
		// on plugin activation redirect to welcome page
		add_action('activated_plugin', [&$this, 'play_activation_redirect']);
		
		// deactivation process
		register_deactivation_hook(WPPP_MAIN_FILE, [&$this, 'on_deactivation_process']);
		
		// hook to enqueue scripts and styles in dashboard
		add_action('admin_enqueue_scripts', [&$this, 'load_admin_scripts']);
		
		// add custom filters
		add_action('manage_posts_extra_tablenav', [&$this, 'add_play_filters']);
		
		// add custom columns to posts
		add_filter('manage_posts_columns', [&$this, 'play_columns_head']);
		add_filter('manage_pages_columns', [&$this, 'play_columns_head']);
		//add_filter('manage_playht_posts_columns', [&$this, 'play_columns_head']);
		add_filter('manage_pages_custom_column', [&$this, 'play_columns_head']);

		// custom column content
		add_action('manage_posts_custom_column', [&$this, 'play_columns_content'], 10, 2);
		//add_action('manage_playht_posts_custom_column', [&$this, 'play_columns_content'], 10, 2);
		add_action('manage_pages_custom_column', [&$this, 'play_columns_content'], 10, 2);

		// add action_row controllers dor play
		add_filter('post_row_actions', [&$this, 'add_play_action_row'], 10, 2);
		add_filter('page_row_actions', [&$this, 'add_play_action_row'], 10, 2);

		// add podacst metabox to posts page
		add_action('add_meta_boxes', [&$this, 'podcast_meta_box']);
	}

	/**
	 * Add admin menu page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function add_admin_menu_page()
	{
		// adding menu page.
		add_menu_page(__('Play', WPPP_DOMAIN), __('Play', WPPP_DOMAIN), 'manage_options', 'play-welcome-page', [
			&$this,
			'play_main_view',
		], 'dashicons-controls-play', '90.321');
	}

	/**
	 * Processes needs to be done upon activation
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function on_activation_process()
	{

		//save list of languages
		update_option('wppp_language_list', Play_Requests::get_langs_list());

		// save voices list
		update_option('wppp_voices_list', Play_Requests::get_langs_voices_array());

		// add new option
		// option might be 0(New User),1(connected to play.ht/no articles yet),2(subscribed user/ show articles)
		if (false === get_option('wppp_status_flag')) {
			add_option('wppp_status_flag', 0);
		}
	}

	/**
	 * Redirect Plugin after activation
	 *
	 * @param $plugin
	 *
	 * @return void
	 */
	public function play_activation_redirect($plugin)
	{
		if ($plugin == plugin_basename(WPPP_MAIN_FILE)) {
			exit(wp_redirect(admin_url('admin.php?page=play-welcome-page')));
		}
	}

	/**
	 * Load scripts and styles for dashboard.
	 * @since 1.0.0
	 * @return void
	 */
	public function load_admin_scripts()
	{
		// load path for scripts
		$load_path = WPPP_URI . (Helpers::is_script_debugging() ? 'assets/src/' : 'assets/dist/');
		$images_path = WPPP_URI . 'assets/images/';

		wp_localize_script('save_conversion_data', 'wppp_images', [
			'settings' 	=> $images_path . "settings.png",
			'checked'	=> $images_path . "wp-checked.png",
			'cancel'	=> $images_path . "wp-cancel.png",
			'warning'	=> $images_path . "wp-warning.png",
			'audio'		=> $images_path . "audio-grey.png",
			'no_audio'	=> $images_path . "no-audio-grey.png",
			'audio_error'	=> $images_path . "audio_error.png",
			'shortcodeExplainer' => $images_path . "shortcode-explainer.gif",
		]);

		// register sentry
		wp_register_script('sentry_script', 'https://browser.sentry-cdn.com/5.7.1/bundle.min.js', [], wppp_version(), true);

		// initialize sentry
		wp_enqueue_script('initialize_sentry', $load_path . 'js/initialize_sentry.js', [
			'sentry_script',
		], wppp_version(), true);

		// register firebase script.
		wp_register_script('firebase_script', 'https://www.gstatic.com/firebasejs/4.2.0/firebase.js', [], wppp_version(), true);

		// initialize firebase
		wp_enqueue_script('initialize_firebase', $load_path . 'js/initialize_firebase.js', [
			'jquery',
			'firebase_script',
		], wppp_version(), true);
		wp_enqueue_script('helpers', $load_path . 'js/helpers.js', [], wppp_version(), true);

		if (1 == get_option('wppp_status_flag')) {
			wp_enqueue_script('retrieve_user_data', $load_path . 'js/retrieve_user_data.js', [
				'jquery',
				'firebase_script',
				'helpers',
				'initialize_sentry',
			], wppp_version(), true);

			// localizing script.
			wp_localize_script('retrieve_user_data', 'wppp_retrieve', [
				'user_id'  => get_option('wppp_blog_userId'),
				'app_id'  => get_option('wppp_blog_appId'),
				'ajax_url' => admin_url('admin-ajax.php'),
			]);
		}
		wp_enqueue_script('promise_polyfill', $load_path . 'js/promise.polyfill.js', [], wppp_version(), true);

		// google analytics script
		wp_enqueue_script('analytics', $load_path . 'js/analytics.js', [
			'jquery',
		], wppp_version(), true);

		// welcome page voice samples functionality
		wp_enqueue_script('welcome_page_voices', $load_path . 'js/welcome_page_voices.js', [
			'jquery',
		], wppp_version(), true);

		$post_lang    = '0';
		$post_voice   = '0';
		$podcast_data = false;

		if (isset($_GET['post'])) {
			$podcast_data = maybe_unserialize(get_post_meta($_GET['post'], 'play_podcast_data', true));
			$post_lang  = $podcast_data ? $podcast_data['lang'] : '0';
			$post_voice = $podcast_data ? $podcast_data['voice'] : '0';
		}

		// save voices list
		update_option('wppp_voices_list', Play_Requests::get_langs_voices_array());

		// map language with voices
		wp_enqueue_script('map_langs_voices', $load_path . 'js/map_langs_voices.js', [
			'jquery',
		], wppp_version(), true);

		// localizing script langs voices.
		wp_localize_script('map_langs_voices', 'wppp_lv_obj', [
			'url'            => admin_url('admin-ajax.php'),
			'langs'          => get_option('wppp_language_list'),
			'langs_voices'   => get_option('wppp_voices_list'),
			'post_lang'      => $post_lang,
			'post_voice'     => $post_voice,
			'new_post_alert' => __('You Cannot convert unsaved article!', WPPP_DOMAIN),
		]);

		if (get_post_type() == 'post') {
			wp_enqueue_script('sweetalert', $load_path . 'js/sweetalert.min.js', [], wppp_version(), true);
			wp_enqueue_script('tinymce_ep_button', $load_path . 'js/tinymce_ep_button.js', [
				'jquery',
				'sweetalert'
			], wppp_version(), true);

			wp_localize_script('tinymce_ep_button', 'tinymce_ep_obj', [
				'post_data'   => $podcast_data,
				'audio_icon'	=> $images_path . "audio-grey.png",
			]);
			wp_enqueue_style('sweetalert', $load_path . 'css/sweetalert.min.css', [], wppp_version());
		}

		wp_enqueue_style('additional_style', $load_path . 'css/additional_style.css', [], wppp_version());

		// check if in play welcome page.
		if (isset($_GET['page']) && $_GET['page'] === 'play-welcome-page') {
			// enqueue setting page styles.
			wp_enqueue_style('font-awesomescript', '//use.fontawesome.com/eb811892d4.js', [], wppp_version());
			wp_enqueue_style('jquery_ui_styles', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css', [], wppp_version());
			wp_enqueue_style('play-welcome-page-style', $load_path . 'css/welcome-page.css', [], wppp_version());
			wp_enqueue_style('play-jqueryui-style', $load_path . 'css/jqueryui.css', [], wppp_version());
			wp_enqueue_script('dashboard_wppp_Chart', 'https://cdnjs.cloudflare.com/ajax/libs/highcharts/7.1.1/highcharts.js', array(), wppp_version(), true);

			wp_enqueue_script('jquery_dropdown_wpplay', $load_path . 'js/jquery-dropdown.js', array(), wppp_version(), true);
			wp_enqueue_script('jscolor_wpplay', $load_path . 'js/jscolor.js', array(), wppp_version(), true);

			wp_enqueue_script('dashboard_wppp', $load_path . 'js/dashboard-wpp.js', array(
				'jquery-ui-datepicker',
				'dashboard_wppp_Chart',
				'jquery_dropdown_wpplay',
				'jscolor_wpplay',
				'analytics',
				'retrieve_user_data',
				'initialize_sentry'
			), wppp_version(), true);

			// wp_enqueue_style( 'page_player', $load_path . 'css/page_player.css', [ ], wppp_version() );		
			wp_enqueue_style('wp-color-picker');
			wp_enqueue_style('play-switchery-style', $load_path . 'css/switchery.css', [], wppp_version());
			wp_enqueue_script('play_ht_settings', $load_path . 'js/playhtsettings.js', array(
				'wp-color-picker',
			), wppp_version(), true);
			wp_enqueue_script('switchery', $load_path . 'js/switchery.js', array(), wppp_version(), true);
		}

		$post_type_a = get_option('playht_type_switch');
		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
		}

        if($post_type_a == 1 || empty($post_type_a)){

        	if ('post.php' === $GLOBALS['pagenow'] || 'post-new.php' === $GLOBALS['pagenow'] || 'edit.php' === $GLOBALS['pagenow'] && (isset($GLOBALS['typenow']) && '' == $GLOBALS['typenow'] || 'post' == $GLOBALS['typenow']  || 'page' === $GLOBALS['post_type'] )) {
        		
				wp_enqueue_style('additional_style', $load_path . 'css/additional_style.css', [], wppp_version());

				wp_enqueue_script('try_voices_select', $load_path . 'js/try_voices_select.js', array(), wppp_version(), true);

				wp_enqueue_script('popups', $load_path . 'js/popups.js', [
					'jquery',
					'analytics',
					'initialize_sentry'
				], wppp_version(), true);

				wp_localize_script('popups', 'wppp_popups_data', [
					'default_language'         => 'english-us',
				]);
			}
        }else{
        	
			if ('post.php' === $GLOBALS['pagenow'] || 'post-new.php' === $GLOBALS['pagenow'] || 'edit.php' === $GLOBALS['pagenow'] && (isset($GLOBALS['typenow']) && '' == $GLOBALS['typenow'] || 'post' == $GLOBALS['typenow']  || in_array($GLOBALS['post_type'], $flip_array))) {

				wp_enqueue_style('additional_style', $load_path . 'css/additional_style.css', [], wppp_version());

				wp_enqueue_script('try_voices_select', $load_path . 'js/try_voices_select.js', array(), wppp_version(), true);

				wp_enqueue_script('popups', $load_path . 'js/popups.js', [
					'jquery',
					'analytics',
					'initialize_sentry'
				], wppp_version(), true);

				wp_localize_script('popups', 'wppp_popups_data', [
					'default_language'         => 'english-us',
				]);
			}
		}
	}

	/**
	 * load welcome page view
	 *
	 * @since 1.0.0.
	 * @return void
	 */
	public function play_main_view()
	{
		// option might be 0(New User),1(connected to play.ht/no articles yet),2(subscribed user/ show articles)
		$play_flag = get_option('wppp_status_flag');
		$user_data = get_option('wppp_play_user_data');
		$images_path = WPPP_URI . 'assets/images/';
		// load view.
		if ($play_flag == 0) {
			// New User/not connected to play.ht yet
			WPPP_view('back-end/play_main_view', []);
		} else {
			WPPP_view('back-end/main_view_subscribed', [
				'user_data' => (false === $user_data) ? $user_data : maybe_unserialize($user_data),
				'settings' 	=> $images_path . "settings.png",
			]);
		}
	}

	/**
	 * On Plugin deactivation
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function on_deactivation_process()
	{
		// delete the defult category
		//		wp_delete_category( get_cat_ID( 'has_podcast' ) );
	}

	/**
	 * Add metabox to edit post page
	 */
	public function podcast_meta_box()
	{
		// add it directly after the publish box.
		add_meta_box('podcast-meta-box-id', __('Play - Convert this post to audio', WPPP_DOMAIN), [
			&$this,
			'podcast_metabox_content',
		], 'post', 'side', 'high');
	}

	/**
	 * metabox content
	 */
	public function podcast_metabox_content()
	{
		global $pagenow;
		$podcast_data   = maybe_unserialize(get_post_meta(get_the_ID(), 'play_podcast_data', true));
		$selected_lang  = $podcast_data ? $podcast_data['lang'] : '0';
		$selected_voice = $podcast_data ? $podcast_data['voice'] : 0;
		$new_post_check = false;
		if (in_array( $pagenow, array( 'post-new.php' ) )) {
			$new_post_check = true;
		}
		// load view.
		WPPP_view('back-end/metabox_content', [
			// 'langs'          => json_decode( get_option( 'wppp_language_list' ) ),
			'lang'           => $selected_lang,
			'voice'          => $selected_voice,
			'is_new_post' => $new_post_check,
		]);
	}

	/**
	 * Save post meta
	 *
	 * @param $post_id
	 *
	 * @return void
	 */
	public function podcast_meta_box_save($post_id)
	{
		// check user capabilities
		if (!current_user_can('edit_post')) {
			return;
		}
		// if auto save routine
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		// check  nonce
		if (!wp_verify_nonce($_POST['podcasr_metabox_nonce_name'], 'podcast_metabox_nonce_action')) {
			// skip if nonce mismatch
			return;
		}

		//get& sanitize podcast post data
		$post_language 		= filter_input(INPUT_POST, 'wppp_lang_meta', FILTER_SANITIZE_STRING);
		$post_voice    		= filter_input(INPUT_POST, 'wppp_voice_meta', FILTER_SANITIZE_STRING);
		$post_voice_style   = filter_input(INPUT_POST, 'wppp_voice_style_meta', FILTER_SANITIZE_STRING);
		// save post meta
		//		update_post_meta( $post_id, 'play_podcast_language', $post_language );
		//		update_post_meta( $post_id, 'play_podcast_voice', $post_voice );
	}

	/**
	 *  Add custom language/voices filters to post page
	 *
	 * @param string $which
	 *
	 * @return void
	 */
	public function add_play_filters($which)
	{
		// add filters in only all posts page/ top filters
		$post_type_a = get_option('playht_type_switch');

		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
		}

        if($post_type_a == 1 || empty($post_type_a)){

        	if ('edit.php' === $GLOBALS['pagenow'] && (isset($GLOBALS['typenow']) && '' == $GLOBALS['typenow'] || 'post' == $GLOBALS['typenow']) && 'top' === $which || 'page' === $GLOBALS['post_type'] ) {
        		
				WPPP_view('back-end/play_posts_filters');
			}
        }else{
        	
			if ('edit.php' === $GLOBALS['pagenow'] && (isset($GLOBALS['typenow']) && '' == $GLOBALS['typenow'] || 'post' == $GLOBALS['typenow']) && 'top' === $which || in_array($GLOBALS['post_type'], $flip_array)) {
				WPPP_view('back-end/play_posts_filters');
			
			}
		}
		
	}

	/**
	 *  Add custom columns posts table
	 *
	 * @param $defaults
	 *
	 * @return array
	 */
	public function play_columns_head($defaults)
	{
		$new_order = [];

		//print_r($defaults);
		//exit;
		if(is_array($defaults)){
			foreach ($defaults as $key => $title) {
				$new_order[$key] = $title;
				if ($key == 'title') // Put added columns before the comments column
				{
					$new_order['has_audio']   = 'Has Audio';
					// TODO: enable this back when we replace keen with ES and read the number of listens from there.
					// $new_order[ 'no_of_plays' ] = 'No. of plays';
				}
			}
		}
			return $new_order;
	}

	/**
	 *  Play columns content
	 *
	 * @param $column_name
	 * @param $post_ID
	 *
	 */
	public function play_columns_content($column_name, $post_ID)
	{
		$podcast_data = get_post_meta($post_ID, 'play_podcast_data', true);

		if (!$podcast_data) {
			// No
			$has_audio = '<img src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/images%2Fno-audio-grey.png?alt=media&token=d3cf7132-6a05-4e5f-a7b3-1d0e57771512" ><span>No</span>';
			$listens = '0';
		} else {
			$podcast_data = maybe_unserialize($podcast_data);
			$listens = $podcast_data['listens'];

			if ($podcast_data['audio_status'] == 4) {
				// Yes
				$has_audio = '<span><a style="cursor:pointer;text-decoration: underline;" id="play-open-draft-editor" data-postId="'. $post_ID .'" >Edit Draft</a> - <a style="cursor:pointer;text-decoration: underline; color: red;" id="play-delete-draft-link" data-postId="'. $post_ID .'" >Delete Draft</a></span>';
			}
			if ($podcast_data['audio_status'] == 2) {
				// Yes
				$has_audio = '<img src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/images%2Faudio-grey.png?alt=media&amp;token=d4ae8576-f576-4103-ab18-3eb5c1ca51d9" ><span>Yes</span>';
			}
			if ($podcast_data['audio_status'] == 0) {
				// No
				$has_audio = '<img src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/images%2Fno-audio-grey.png?alt=media&token=d3cf7132-6a05-4e5f-a7b3-1d0e57771512" ><span>No</span>';
			}
			if ($podcast_data['audio_status'] == 1) {
				// Converting
				$has_audio = '<img class="loading" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/images%2Fsettings.png?alt=media&token=58e7fc5a-aac9-4b5d-b721-6f7e2c986045" ><span>Converting</span>';
			}
		}

		// echo audio status
		if ($column_name == 'has_audio') {
			echo $has_audio;
		}


		// echo audio listens num
		if ($column_name == 'no_of_plays') {
			echo $listens;
		}
	}


	/**
	 * add action_row controllers dor play
	 *
	 * @param $actions
	 * @param $post
	 *
	 * @return array $actions
	 */
	public function add_play_action_row($actions, $post)
	{
		$post_type_a = get_option('playht_type_switch');
		if(is_array($post_type_a)){
			$flip_array = array_keys($post_type_a);
		}
        //var_dump($post_type_a);
        if($post_type_a == 1 || empty($post_type_a)){
        	if ($post->post_type=='post' || $post->post_type=='page')
	    	{
	    		$podcast_data = get_post_meta($post->ID, 'play_podcast_data', true);
				// var_dump("podcast_data:::", $podcast_data);
				if ($podcast_data === "") {
					return $actions;
				}

				if ($post->post_type !== 'post' && $post->post_type !== 'page' && $post->post_type != 'lp_lesson' && $post->post_type != 'sfwd-lessons' && $post->post_type != 'sfwd-topic' && $post->post_type != 'sfwd-question'&& $post->post_type != 'sfwd-courses'&& $post->post_type != 'sfwd-quiz'&& $post->post_type != 'article'&& $post->post_type != 'news' ) {
					return $actions;
				}
				// $edit_podcast_url = admin_url( '/edit.php/#' );

				$delete_podcast_url = admin_url('/edit.php?post_type=' . $post->post_type . '&play=del_podcast&post=' . $post->ID);


				// $actions = array_merge( $actions, [
				// 	'edit_podcast' => sprintf( '<a href="%1$s" id="edit_' . $post->ID . '" class="editinline">%2$s</a>', esc_url( $edit_podcast_url ), 'Edit Podcast' ),
				// ] );

				$actions = array_merge($actions, [
					//href="%1$s"
					'delete_podcast' => sprintf('<a style="cursor:pointer;" id="js__playht-delete-audio" data-postId="' . $post->ID . '" >Delete Audio</a>'),
				]);		
	    	}
        }else{
        	if (in_array($post->post_type, $flip_array))
	    	{
	    		$podcast_data = get_post_meta($post->ID, 'play_podcast_data', true);
				// var_dump("podcast_data:::", $podcast_data);
				if ($podcast_data === "") {
					return $actions;
				}

				/*if ($post->post_type !== 'post' && $post->post_type !== 'page' ) {
					return $actions;
				}*/
				// $edit_podcast_url = admin_url( '/edit.php/#' );

				$delete_podcast_url = admin_url('/edit.php?post_type=' . $post->post_type . '&play=del_podcast&post=' . $post->ID);


				// $actions = array_merge( $actions, [
				// 	'edit_podcast' => sprintf( '<a href="%1$s" id="edit_' . $post->ID . '" class="editinline">%2$s</a>', esc_url( $edit_podcast_url ), 'Edit Podcast' ),
				// ] );

				$actions = array_merge($actions, [
					//href="%1$s"
					'delete_podcast' => sprintf('<a style="cursor:pointer;" id="js__playht-delete-audio" data-postId="' . $post->ID . '" >Delete Audio</a>'),
				]);	
	    	}
        }
		
		


		return $actions;
	}

	/**
	 * Get Posts that has podcasts
	 */
	public function get_posts_with_podcast()
	{
		$args = array(
			'post_type' => 'post',
			'post_status' => 'any',
			'posts_per_page' => 25,
		);
		$post_list = get_posts($args);
		$results = array();
		foreach ($post_list as $post) {
			$id = $post->ID;
			$post_meta = maybe_unserialize(get_post_meta($id, 'play_podcast_data', true));
			if (is_array($post_meta) && $post_meta['play_article_id']) {
				$results[$id] = $post_meta['play_article_id'];
			}
		}
		return $results;
	}
}
