<?php
/**
 * @package Wordpress\Play
 */

use Wordpress\Play\Plugin;

if ( ! function_exists( 'wp_play' ) ):
	/**
	 * Get plugin instance
	 *
	 * @return Plugin
	 */
	function wp_play()
	{
		return Plugin::get_instance();
	}
endif;

if ( ! function_exists( 'wppp_view' ) ):
	/**
	 * Load view
	 *
	 * @param string  $view_name
	 * @param array   $args
	 * @param boolean $return
	 *
	 * @return void
	 */
	function wppp_view( $view_name, $args = null, $return = false )
	{
		if ( $return ) {
			// start buffer
			ob_start();
		}

		wp_play()->load_view( $view_name, $args );

		if ( $return ) {
			// get buffer flush
			return ob_get_clean();
		}
	}
endif;

if ( ! function_exists( 'wppp_version' ) ):
	/**
	 * Get plugin version
	 *
	 * @return string
	 */
	function wppp_version()
	{
		return wp_play()->version;
	}
endif;

if ( ! function_exists( 'wppp_db_version' ) ):
	/**
	 * Get plugin version
	 *
	 * @return string
	 */
	function wppp_db_version()
	{
		return wp_play()->db_version;
	}
endif;

if ( ! function_exists( 'wppp_conversion_check' ) ):
	/**
	 * Get plugin version
	 *
	 * @return boolean
	 */
	function wppp_conversion_check()
	{
		// if user not registered return false
		if ( false === get_option( 'wppp_blog_appId' ) || false === get_option( 'wppp_blog_userId' ) ) {
			return new WP_Error( 'play', __( "You must login first to be able to use play.ht to add audio to your articles.", WPPP_DOMAIN ) );
		}

		// check quota end date
		$user_data = maybe_unserialize( get_option( 'wppp_play_user_data' ) );
		if (!empty($user_data[ 'package' ])) {
			if ( time() < $user_data[ 'package' ][ 'pro_package_end' ] ) {
				return new WP_Error( 'play', __( "Your subscription is over, you need to renew your subscription.", WPPP_DOMAIN ) );
			}
		}
		//@todo check quota
		if ( false ) {
			return new WP_Error( 'play', __( "You've already used your own quota, please upgrade in order to convert more articles.", WPPP_DOMAIN ) );
		}

		return 1;
	}
endif;

if ( ! function_exists( 'get_playht_player' ) ) :
	function get_playht_player( $atts ) {

		$post_id = get_the_ID();
		$app_id  = get_option( 'wppp_blog_appId' );

		// Player atts
		$atts = wp_parse_args( $section['settings'], array(
			'width'  => '100%',
			'height' => 175,
		));

		$atts = apply_filters( 'Play/player_atts', $atts );

		// Podcast stored data
		$podcast_data = maybe_unserialize( get_post_meta( $post_id, 'play_podcast_data', true ) );
		$podcast_data = apply_filters( 'Play/podcast_data', $podcast_data );

		$article_url   = ! empty( $podcast_data[ 'url' ] )   ? $podcast_data[ 'url' ]   : '';
		$article_voice = ! empty( $podcast_data[ 'voice' ] ) ? $podcast_data[ 'voice' ] : '';

		return '<iframe allowfullscreen="" frameborder="0" scrolling="no" class="playht-iframe-player" data-appId="' . $app_id . '" article-url="' . $article_url . '" data-voice="' . $article_voice . '" src="https://play.ht/embed/?article_url=' . $article_url . '&voice=' . $article_voice . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '"></iframe>';
	}
endif;

add_shortcode('playht_player', 'get_playht_player');

// function audio_articles_url_handler() {
// 	if(strpos($_SERVER['REQUEST_URI'], "/audio-articles/")) {
// 		$userId = get_option( 'wppp_blog_userId' );
// 		echo $userId;
// 		$url      = 'https://play.ht/podcasts/';
// 		$response = wp_remote_get( esc_url_raw( $url ) );
// 		echo wp_remote_retrieve_body( $response );
// 		exit();
// 	}
//  }
// add_action('parse_request', 'audio_articles_url_handler');