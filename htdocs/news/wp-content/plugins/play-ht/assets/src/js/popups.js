(function(w, $, undefined) {
  const narrationStyleVoices = [
    {
      value: "Noah",
      lang: "en-US",
      name: "Noah (en-US)",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogZgW_92qU-g1m9Ggp.mp3?generation=1568410881344476&alt=media"
    },
    {
      value: "Noah",
      lang: "en-US",
      name: "Noah (Newscaster) (en-US)",
      style: "newscaster",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogbA8-4tooZf1-fG-M.mp3?generation=1568411531053358&alt=media"
    },
    {
      value: "Noah",
      lang: "en-US",
      name: "Noah (Conversational) (en-US)",
      style: "conversational",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LzNgBGF6i2SWrPhfsrf.mp3?generation=1579888461924934&alt=media"
    },
    {
      value: "Jessica",
      lang: "en-US",
      name: "Jessica (en-US)",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogXFKxxLFk2j0tRh8w.mp3?generation=1568410241359811&alt=media"
    },
    {
      value: "Jessica",
      lang: "en-US",
      name: "Jessica (Newscaster) (en-US)",
      style: "newscaster",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogbEEgYZqXoPPN9a_7.mp3?generation=1568411548164481&alt=media"
    },
    {
      value: "Jessica",
      lang: "en-US",
      name: "Jessica (Conversational) (en-US)",
      style: "conversational",
      sample:
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LzNgNUzhAYsbIWnHlrd.mp3?generation=1579888512028029&alt=media"
    },
    {
      "value": "Gabriella",
      "lang": "es-US",
      "name": "Gabriella (Newscaster) (es-US)",
      "style": "newscaster",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/speech_20200502112008760.mp3?alt=media&token=6cecedbc-58bb-48ff-a1a7-96bbf02905ff"
    },
  ];

  $(function() {
    var blogName = window.location.hostname;
    var userId = wppp_conv_data ? wppp_conv_data.userId : "";

    try {
      if (wppp_popups_data.default_language) {
        var default_lang = wppp_popups_data.default_language;
        var supported_lang_found = false;
        $("#wppp_lang_meta")
          .find("option")
          .each(function(i, el) {
            var lang = $(el).attr("value");
            var singlePostLangAvailable =
              wppp_conv_data &&
              wppp_conv_data.post &&
              wppp_conv_data.post.lang &&
              wppp_conv_data.post.lang.indexOf(lang) === 0;
            var singlePostLang =
              singlePostLangAvailable && wppp_conv_data.post.lang;

            if (singlePostLangAvailable) {
              $(el)
                .attr("selected", "selected")
                .change();
              $("#wppp_voice_meta")
                .find("option[value=" + wppp_conv_data.post.voice + "]")
                .attr("selected", "selected");
              supported_lang_found = true;
            } else if (default_lang.indexOf(lang) === 0) {
              $(el)
                .attr("selected", "selected")
                .change();
              $("#wppp_voice_meta")
                .find("option")
                .eq(0)
                .attr("selected", "selected");
              supported_lang_found = true;
            }

            if (!supported_lang_found) {
              $("#multiple_conversion").addClass("disabled-input");
            }
          });
      } else {
        $("#multiple_conversion").addClass("disabled-input");
      }

      // handling multiple convert button statuses and tooltip

      $("#multiple_conversion_wrapper").on("mouseover", function() {
        var tooltipMsg = "";

        if (
          $("#wppp_lang_meta").get(0) &&
          $("#wppp_lang_meta").get(0).selectedIndex === 0
        ) {
          tooltipMsg = "Please Select a language and a voice first";
          $(this).attr("data-tooltip", tooltipMsg);
        } else if (
          $(".check-column input[type=checkbox]:checked").length === 0
        ) {
          tooltipMsg = "Please select at least one article";
          $(this).attr("data-tooltip", tooltipMsg);
        }
      });

      // wppp_voice_style_meta
      $("#wppp_voice_style_meta_wrapper").on("mouseover", function() {
        var tooltipMsg = "";

        if (
          $("#wppp_voice_meta").get(0).value === "Noah" ||
          $("#wppp_voice_meta").get(0).value === "Jessica" ||
          $("#wppp_voice_meta").get(0).value === "Gabriella"
        ) {
          tooltipMsg =
            "Press the green Play button to preview the voice with the selected style";
          $(this).attr("data-tooltip", tooltipMsg);
        } else {
          tooltipMsg =
            "The newscaster narration style is applicable only to the English Noah and Jessica voices, Or the Spanish Gabriella voice.";
          $(this).attr("data-tooltip", tooltipMsg);
        }
      });

      $(".check-column input[type=checkbox], #wppp_lang_meta").on(
        "change",
        function() {
          if (
            $(".check-column input[type=checkbox]:checked").length > 0 &&
            $("#wppp_lang_meta").get(0).selectedIndex > 0
          ) {
            $("#multiple_conversion").removeClass("disabled-input");
            $("#multiple_conversion_wrapper").removeAttr("data-tooltip");
          } else {
            $("#multiple_conversion").addClass("disabled-input");
          }
        }
      );

      // handling post edit convert button statuses and tooltip
      $("#wppp_lang_meta").get(0) &&
        $("#wppp_lang_meta").get(0).selectedIndex === 0 &&
        $("#convert_podcast").addClass("disabled-input");
      $("#convert_podcast_wrapper").on("mouseover", function() {
        if ($("#wppp_lang_meta").get(0).selectedIndex === 0) {
          tooltipMsg = "Please Select a language and a voice first";
          $(this).attr("data-tooltip", tooltipMsg);
        }
      });

      $("#wppp_lang_meta").on("change", function(e) {
        if ($("#wppp_lang_meta").get(0).selectedIndex > 0) {
          $("#convert_podcast").removeClass("disabled-input");
          $("#convert_podcast_wrapper").removeAttr("data-tooltip");
        } else {
          $("#convert_podcast").addClass("disabled-input");
        }

        var selectedLang = $(e.target).attr("value");
        window.__playht__analytics &&
          __playht__analytics(
            "playht__analytics.send",
            "event",
            "click",
            "change-language",
            selectedLang
          );
      });

      $("#wppp_voice_style_meta").get(0).disabled = true;
      if (
        $("#wppp_voice_meta").val() === "Noah" ||
        $("#wppp_voice_meta").val() === "Jessica" ||
        $("#wppp_voice_meta").val() === "Gabriella"
      ) {
        $("#wppp_voice_style_meta").get(0).disabled = false;
        $("#wppp_voice_style_meta").html(
          '<option value="standard">Standard</option><option value="newscaster">Newscaster</option><option value="conversational" selected>Conversational</option>'
        );
        $("#wppp_voice_style_meta").get(0).selectedIndex = 2;
      }

      
      const sampleAudioElement = document.getElementById("audio_idforplay")
      $("#wppp_voice_meta").on("change", function(e) {
        const selectedVoice = $(e.target).attr("value");
        window.__playht__analytics &&
          __playht__analytics(
            "playht__analytics.send",
            "event",
            "click",
            "change-voice",
            selectedVoice
          );

        if (selectedVoice === "Noah" || selectedVoice === "Jessica" || selectedVoice === "Gabriella") {
          $("#wppp_voice_style_meta").get(0).disabled = false;
          $("#wppp_voice_style_meta").html(
            '<option value="standard">Standard</option><option value="newscaster">Newscaster</option><option value="conversational" selected>Conversational</option>'
          );
          $("#wppp_voice_style_meta").get(0).selectedIndex = 2;
          narrationStyleVoices.forEach(voice => {
            if(voice.value === selectedVoice && voice.style === 'conversational'){
              $("#wppp_voice_try .voices__list-title").html(
                voice.name
              );
              sampleAudioElement.src = voice.sample;
            }
          });
        } else {
          $("#wppp_voice_style_meta").get(0).selectedIndex = 0;
          $("#wppp_voice_style_meta").get(0).disabled = true;
          $("#wppp_voice_style_meta").html(
            '<option value="standard">Standard</option>'
          );
        }
      });

      narrationStyleVoices.forEach(voice => {
        if(voice.value === 'Noah' && voice.style === 'conversational'){
          $("#wppp_voice_try .voices__list-title").html(
            voice.name
          );
          sampleAudioElement.src = voice.sample;
        }
      });

      $("#wppp_voice_style_meta").on("change", function(e) {
        var selectedStyle = $(e.target).attr("value");
        window.__playht__analytics &&
          __playht__analytics(
            "playht__analytics.send",
            "event",
            "click",
            "change-narration-style",
            selectedStyle
          );
      });
    } catch (error) {
      window.playht__SetSentryContext({
        userId: userId,
        website: window.location.href
      });
      window.Sentry && Sentry.captureException(error);
    }
  });

if(window.location.pathname.includes("post-new.php")){
  let editPost = wp.data ? wp.data.select( 'core/edit-post' ) : null,
  lastIsSaving = false;
  // editPost is undefined in the classic editor
  if(!editPost){
    return;
  }
  wp.data.subscribe( function() {
    try {
      let isSaving = editPost.isSavingMetaBoxes();
      if ( isSaving !== lastIsSaving && !isSaving ) {
        lastIsSaving = isSaving;

        const convertBtnWrapper = $("#new_post_wrapper");
        convertBtnWrapper.removeAttr("id");
        convertBtnWrapper.off();
        convertBtnWrapper.attr("id", "convert_podcast_wrapper");
        convertBtnWrapper.find('input').removeClass('disabled-input');
        convertBtnWrapper.removeAttr('data-tooltip');
        
        convertBtnWrapper.html('<input type="button" id="convert_podcast" value="Convert to Audio" class="button button-primary">');

        // handling post edit convert button statuses and tooltip
        $("#wppp_lang_meta").get(0) &&
          $("#wppp_lang_meta").get(0).selectedIndex === 0 &&
          $("#convert_podcast").addClass("disabled-input");
        $("#convert_podcast_wrapper").on("mouseover", function() {
          if ($("#wppp_lang_meta").get(0).selectedIndex === 0) {
            tooltipMsg = "Please Select a language and a voice first";
            $(this).attr("data-tooltip", tooltipMsg);
          }
        });

        $("#convert_podcast_wrapper").on("mouseover", function() {
          if ($("#wppp_lang_meta").get(0).selectedIndex === 0) {
            tooltipMsg = "Please Select a language and a voice first";
            $(this).attr("data-tooltip", tooltipMsg);
          }
        });

        $("#wppp_lang_meta").on("change", function(e) {
          if ($("#wppp_lang_meta").get(0).selectedIndex > 0) {
            $("#convert_podcast").removeClass("disabled-input");
            $("#convert_podcast_wrapper").removeAttr("data-tooltip");
          } else {
            $("#convert_podcast").addClass("disabled-input");
          }

          var selectedLang = $(e.target).attr("value");
          window.__playht__analytics &&
            __playht__analytics(
              "playht__analytics.send",
              "event",
              "click",
              "change-language",
              selectedLang
            );
        });
      }

      lastIsSaving = isSaving;
      
    } catch (error) {
      console.log("error", error);
    }
  } );

}

})(window, jQuery);

