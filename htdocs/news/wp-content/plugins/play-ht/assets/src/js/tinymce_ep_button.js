(function() {

    jQuery( document ).on( 'tinymce-editor-setup', function( event, editor ) {
        editor.settings.toolbar1 += ',playht_ep_iframe';
    
        editor.addButton( 'playht_ep_iframe', {
            text: ' Add Audio',
            tooltip: 'Add Play.ht Embedded Audio Player\'s Shortcode',
            image: tinymce_ep_obj.audio_icon,
            onclick: function () {
                if(!window.wppp_conv_data || (window.wppp_conv_data && !wppp_conv_data.post) || (window.wppp_conv_data && wppp_conv_data.post && !wppp_conv_data.post.article_audio) ){
                    // no audio added, or still converting
                    playht__swal.fire({
                        imageUrl: wppp_images.shortcodeExplainer,
                        // title: "No audio ",
                        text: "Please select a voice and add audio first before adding the shortcode",
                        confirmButtonClass: "wppp-modal-ok-btn"
                      });
                }else{
                    var voice = "Michael (en-US)";
                    if(window.wppp_lv_obj && wppp_lv_obj.langs_voices && window.wppp_conv_data && window.wppp_conv_data.post && window.wppp_conv_data.post.voice){
                        var voicesArr = wppp_lv_obj.langs_voices;
                        if(voicesArr && voicesArr.length > 0 ){
                            var voiceObj = voicesArr.filter(function (obj) {
                                return obj.value === window.wppp_conv_data.post.voice;
                              })[0];
                            (voiceObj && voiceObj.name) && (voice = voiceObj.name);
                        }
                    }
                    var playht_iframe = '[playht_player width="100%" height="175" voice="' + voice + '"]';
                    editor.insertContent(playht_iframe);
                }
            }
        });
    });
})();
