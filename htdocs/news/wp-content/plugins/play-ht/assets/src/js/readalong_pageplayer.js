(function () {

// configs - this should be removed and be available from global scope while loading the page
window.playht_read_along_config = {
    contextElement: window.wppp_playReadalong ? window.wppp_playReadalong.postElement : null, // the article text element wrapper, without the title
    wordBackgroundColor: window.wppp_playReadalong ? window.wppp_playReadalong.postWordBackgroundColor : '#000', // highlighted word background
    wordColor: window.wppp_playReadalong ? window.wppp_playReadalong.postWordColor : '#fff', // highlighted word color
    transcriptionId: window.wppp_playReadalong ? window.wppp_playReadalong.postTransId : null, // the current article transcription Id
    audioElementId: 'playht-audio-element', // page player audio element id
    iframeElementId: 'playht-iframe-player' // the EP iframe id
}

if(!window.playht_read_along_config || (window.playht_read_along_config && !window.playht_read_along_config.transcriptionId)) return;

// script required variables
const {contextElement, wordBackgroundColor, wordColor, transcriptionId, audioElementId} = window.playht_read_along_config;
const audioElement = document.getElementById(audioElementId);

if(!audioElement) return;

// injecting read along styles
function addStyleString(str) {
    var node = document.createElement('style');
    node.innerHTML = str;
    document.body.appendChild(node);
}

const readAlongCSS= (wordBackgroundColor = '#00bf99', wordColor = '#fff') => `
    .playht_mark{
        background: ${wordBackgroundColor};
        color: ${wordColor};
        border-radius: 2px;
        padding: 0px !important;
        box-shadow: 3px 0 0px 0px ${wordBackgroundColor}, -3px 0 0px 0px ${wordBackgroundColor};
    }

    .playht_invisible_mark{
        background: transparent;
    }
`
addStyleString(readAlongCSS(wordBackgroundColor, wordColor));

let marker;
let invisibleMarker;
if(window.playht_Mark){
    let context = document.querySelector(contextElement) || (document.querySelector('.playHtListenArea') ? document.querySelector('.playHtListenArea').parentElement : document.querySelector('body'));
    marker = new playht_Mark(context);
    invisibleMarker = new playht_Mark(context);
}else{
    return;
}

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

// Play WordPress ReadAlong v.0.1
var state = '';
var currentIndex = 0;
var currentWord = "";
var timings = [];
var ready = false;
var stopMarking = false;
var ignore = {}
var _words = [];
var _wordsCount = {}
var counterHistory = [];
var seeked = false;

// Fetch timings
function fetchTimings(transcriptionId){
    var request = new XMLHttpRequest();
    request.open('GET', 'https://play.ht/api/articleTimings?transcriptionId='+transcriptionId, true);
    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            timings = JSON.parse(request.responseText);
            _buildWordsIndex(timings);
            ready = true;
        }
    };
    request.onerror = function() {
        // fetchTimings()
    };
    request.send();
}

fetchTimings(transcriptionId);

// audio events
audioElement.addEventListener('play', playerReducer.bind(this, 'play'));
audioElement.addEventListener('pause', playerReducer.bind(this, 'pause'));
audioElement.addEventListener('seeking', playerReducer.bind(this, 'seek'));

function filter(elm, term, totalCounter, counter){
    if (term == "" || term == " " || ignore[term]) {return false;}

    var word = term
    
    _wordsCount[word].counter = _wordsCount[word].counter + 1;

    if(_wordsCount[word].occurance.includes(_wordsCount[word].counter)){
        return true
    }else{
        return false
    }
} 

function getOccurance(count){
    var occurance = [1]
    for(var i = 1; i < count; i++){
        occurance.push(occurance[occurance.length - 1] + (count + 1))
    }
    return occurance
}

function clean(word) {
    return word.trim().toLowerCase()
}

function setPlayState(newState) {
    state = newState
}

function highlight() {
    var currentTime = audioElement.currentTime;
    var playbackRate = audioElement.playbackRate;
    var w = _getCurrentWord(currentTime);
    currentWord = w.word;
    var dur = w.end - currentTime;
    dur = dur * (1.0/playbackRate);
    // setTimeout(function(){
    //     _getCurrentTime()
    // }, dur  * 1000)

    if (currentIndex == w.index && currentIndex != 0) {
        // return;
    }

    if (w.index != currentIndex + 1 && !seeked) {
        var diff = w.index - currentIndex - 1
        for(var i=0; i < diff; i++){
            var idx = currentIndex + i + 1
            var g = clean(_words[idx].word)
            ignore[g] = true;
        }
    }

    seeked = false;

    currentIndex = w.index;

    _wordsCount = jQuery.extend(true, {}, counterHistory[currentIndex]);
    
    if (currentIndex == _words.length - 1) {
        stopMarking = true;
    }

    marker.unmark();
    var _word = clean(currentWord)
    marker.mark(_word, {
        "className": "playht_mark",
        "accuracy": {
            "value": "exactly",
            "limiters": []
        },
        "filter": filter
    });

    if(state === 'play' && !stopMarking) {
        var dur = w.end - currentTime;
        dur = dur * (1.0/playbackRate);
        const highlightTimer = setTimeout(function(){
            highlight()
            clearTimeout(highlightTimer);
        }, dur  * 1000);   
    }
}

function playerReducer(action){
    if(!ready) return;
    switch (action) {
        case 'play':
            setPlayState('play');
            highlight();
            break;

        case 'pause':
            setPlayState('pause');
            break;

        case 'stop':
            state = 'stop';
            stopMarking = true;
            break;

        case 'seek':
            highlight();
            break;

        default:
            break;
    }
}


function _buildWordsIndex(timings){
    var offset = 0;
    timings.map(function(block, index){
        block.map(function(item, i){
            var word = item[0];

            var _word = clean(word)

            if (!_wordsCount[_word]) {
                _wordsCount[_word] = {counter: 0, count: 0, occurance: [1]};
            }
            _words.push({"word": word, "begin": item[1], "end": item[2], "index": offset + i})
        })
        offset += block.length
    })


    for(var i=0; i < _words.length; i++){
        var word = clean(_words[i].word)
        if (_wordsCount[word] && _wordsCount[word].count == 0 ) {
            invisibleMarker.mark(word, {
                "className": "playht_invisible_mark",
                "accuracy": {
                    "value": "exactly",
                    "limiters": []
                },
                "filter": function(elm, term){
                    _wordsCount[term].count = _wordsCount[term].count + 1
                    return false;
                }
            });

            invisibleMarker.unmark()
            _wordsCount[word].occurance = getOccurance(_wordsCount[word].count)
        }
    }

    var wc = jQuery.extend(true, {}, _wordsCount);
    for(var i=0; i < _words.length; i++){
        var word = clean(_words[i].word)
        counterHistory[i] = jQuery.extend(true, {}, wc);
        wc[word].counter = wc[word].counter + wc[word].count;
    }
}

function _search(a, t){
    if (typeof t === 'undefined') {return "";}

    if (a.length == 1) {
        return a[0]
    }

    // split the array
    var i = Math.floor(a.length / 2);
    
    // if middle value is current word, return it
    if (a[i].begin <= t && a[i].end > t) {
        return a[i]
    }

    if (a[i].begin > t) {
        if (a.length === 2) {
            return a[0]
        }else{
            return _search(a.splice(0, i), t)			
        }
    }else{
        if (a.length === 2) {
            return a[1]
        }else{
            return _search(a.splice(i+1, a.length), t)
        }
    }
}

function _getCurrentWord(currentTime){
    return _search(_words.slice(0), currentTime)
}

})();