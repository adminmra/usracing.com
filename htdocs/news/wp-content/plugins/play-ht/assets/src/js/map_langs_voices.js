(function(w, $, undefined) {
  $(function() {
    $("#new_post").addClass("disabled-input");
    $("#new_post_wrapper").on("mouseover", function() {
      tooltipMsg =
        "You Cannot Add audio to an unsaved post, Please save first.";
      $(this).attr("data-tooltip", tooltipMsg);
    });

    // var langs  = jQuery.parseJSON ( wppp_lv_obj.langs );
    var voices = jQuery.parseJSON(JSON.stringify(wppp_lv_obj.langs_voices));
    // convert array to object
    function toObject(arr) {
      var obj = {};
      var lang = "";
      var lang_voices = [];

      $.each(arr, function(key, value) {
        if (value.lang != lang) {
          lang_voices = [];
          lang = value.lang;
          lang_voices.push({ name: value.name, val: value.value });
          obj[value.lang] = lang_voices;
        } else {
          lang_voices.push({ name: value.name, val: value.value });
        }
        obj[value.lang] = lang_voices;
      });
      return obj;
    }

    // populate voices dropdown menu upon language dropdown menu
    try {
      var Lang_voices_obj = toObject(voices);
      var $Lang_voices_obj = $("#wppp_voice_meta");
  
      var old_lang = $("#wppp_lang_meta").val();
  
      if (old_lang != "0") {
        var lang = old_lang,
          lcns = Lang_voices_obj[lang] || [];
  
        var html = $.map(lcns, function(lcn) {
          return '<option value="' + lcn.val + '">' + lcn.name + "</option>";
        }).join("");
  
        $Lang_voices_obj.html(html);
  
        $("#wppp_voice_meta option[value=" + wppp_lv_obj.post_voice + "]").prop(
          "selected",
          "selected"
        ); //get option with value 3 and set selected
      }
  
      $("#wppp_lang_meta").change(function() {
        var lang = $(this).val(),
          lcns = Lang_voices_obj[lang] || [];
  
        var html = $.map(lcns, function(lcn) {
          return '<option value="' + lcn.val + '">' + lcn.name + "</option>";
        }).join("");
  
        $Lang_voices_obj.html(html);
      });
      
    } catch (error) {
      console.log("voice error", error)
      
    }
  });
})(window, jQuery);
