jQuery(document).ready( function($) {    
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
      var switchery = new playht__Switchery(html, { 
            size: 'small',
            color: '#24d066',
        });
    });

    $("#BorderRadiusRange").on("input change", function() { 
        var num = document.getElementById("BorderRadiusRange").value
        var myElementStyle = document.getElementById('playListenBtnID');
        myElementStyle.style.borderRadius = num+'px'; // standard
        myElementStyle.style.MozBorderRadius = num+'px'; // Mozilla
        myElementStyle.style.WebkitBorderRadius = num+'px'; // WebKit
    });
    $("#BorderRadiusReadalongWord").on("input change", function() { 
        var num = document.getElementById("BorderRadiusReadalongWord").value
        var myElementStyle = document.getElementById('readalongWord');
        myElementStyle.style.borderRadius = num+'px'; // standard
        myElementStyle.style.MozBorderRadius = num+'px'; // Mozilla
        myElementStyle.style.WebkitBorderRadius = num+'px'; // WebKit
    });
    var play_listenfloatingBtn = {
        change: function(event, ui){
            $(".play_listenfloatingBtn").css("background-color", ui.color.toString());
        }, 
    };
    $('.color_backgrund_field').wpColorPicker(play_listenfloatingBtn);

    var playHttextColor_field = {
        change: function(event, ui){
            $(".playhtIcon").css("color", ui.color.toString());
        }, 
    };
    $('.playHttextColor_field').wpColorPicker(playHttextColor_field);

    // Listencolor
    var Listencolor_backgrund_field = {
        change: function(event, ui){
            $(".playListenBtn").css("background-color", ui.color.toString());
        }, 
    };
    $('.Listencolor_backgrund_field').wpColorPicker(Listencolor_backgrund_field);

    var playHtListentextColor_field = {
        change: function(event, ui){
            $("#playListenBtnID").css("color", ui.color.toString());
        }, 
    };
    $('.playHtListentextColor_field').wpColorPicker(playHtListentextColor_field);

    var playHtListenBorderColor_field = {
        change: function(event, ui){
            $(".playListenBtn").css("border-color", ui.color.toString());
        }, 
    };
    $('.playHtListenBorderColor_field').wpColorPicker(playHtListenBorderColor_field);

    var playHtPlayerItemsColor = {
        change: function(event, ui){
            $(".playht-custom-audio__playBtns-wrapper, .topArr.playht-downarrow-icon, .playht-custom-audio-meta, .playht-icon-play-circled, .playht-icon-cancel").css("color", ui.color.toString());
            $(".line_r, .line_circl").css("background-color", ui.color.toString());
        }, 
    };
    $('.playHtPlayerItemsColor').wpColorPicker(playHtPlayerItemsColor);

    var playHtPlayerTextColor = {
        change: function(event, ui){
            $(".playht-cutom-audio__time-wrapper--mobile, .playhtRange__value, .playht-custom-audio-wrapper, small.byJohn, .playhtIconA").css("color", ui.color.toString());
        }, 
    };
    $('.playHtPlayerTextColor').wpColorPicker(playHtPlayerTextColor);

    var playHtPlayerBackgroundColor = {
        change: function(event, ui){
            $(".playht-custom-audio-wrapper, .playht-custom-audio-wrapper--mobile").css("background-color", ui.color.toString());
        }, 
    };
    $('.playHtPlayerBackgroundColor').wpColorPicker(playHtPlayerBackgroundColor);

    // readalong Word
    var playHtReadalongTextColor = {
        change: function(event, ui){
            $(".readalongWord").css("color", ui.color.toString());
        }, 
    };
    $('.playHtReadalongTextColor').wpColorPicker(playHtReadalongTextColor);
    var playHtReadalongBackgroundColor = {
        change: function(event, ui){
            $(".readalongWord").css("background-color", ui.color.toString());
        }, 
    };
    $('.playHtReadalongBackgroundColor').wpColorPicker(playHtReadalongBackgroundColor);


    $("#FielddesktopPositionID").on('change', function() {
        if ($(this).val() == 'left'){
            $(".play_listenfloatingBtn").css("float", "left");
        } else {
            $(".play_listenfloatingBtn").css("float", "right");
        }
    });
    
    // #playht_Floating _switch
    var Floatingbutton_switch   = document.querySelector('#playht_button_switch'),
    whiteLabel_switch   = document.querySelector('#playht_button_wlabel'),
    Listenbutton_switch         = document.querySelector('#playht_Listenbutton_switch');
    playHtDarkModeID            = document.querySelector('#playHtDarkModeID');
    playHtReadalongSwitch       = document.querySelector('#playHt_readalong_switch');

    whiteLabel_switch && (whiteLabel_switch.onchange = function() {
        if (whiteLabel_switch.checked) {
            
            $(".whitelabel_switch").css("display", 'none');
        } else {
            $(".whitelabel_switch").css("display", 'block');
        }
    });

    Floatingbutton_switch && (Floatingbutton_switch.onchange = function() {
        if (Floatingbutton_switch.checked) {
            $(".play_listenfloatingBtn").css("opacity", 1);
        } else {
            $(".play_listenfloatingBtn").css("opacity", 0.2);
        }
    });

    // #playht_Listenbutton_switch
    Listenbutton_switch && (Listenbutton_switch.onchange = function() {
        if (Listenbutton_switch.checked) {
            $(".playListenBtn").css("opacity", 1);
        } else {
            $(".playListenBtn").css("opacity", 0.2);
        }
    });
    // playHtDarkModeID
    playHtDarkModeID && (playHtDarkModeID.onchange = function() {
        if (playHtDarkModeID.checked) {
            $(".playht-custom-audio-wrapper--mobile, .playht-custom-audio-wrapper").css("color", '#fff');
            $(".playht-custom-audio-wrapper--mobile, .playht-custom-audio-wrapper").css("background-color", 'rgb(47, 47, 47)');
            $(".line_r,.line_circl").css("background-color", '#fff');
            $(".playht-custom-audio__playBtn").css("color", '#fff');
        } else {
            $(".playht-custom-audio-wrapper--mobile, .playht-custom-audio-wrapper").css("color", '#21b67e');
            $(".playht-custom-audio-wrapper--mobile, .playht-custom-audio-wrapper").css("background-color", '#f7f7f7');
            $(".line_r,.line_circl").css("background-color", '#21b67e');
            $(".playht-custom-audio__playBtn").css("color", '#21b67e');
        }
    }); 
    // playHtReadalongSwitch
    playHtReadalongSwitch && (playHtReadalongSwitch.onchange = function() {
        if (playHtReadalongSwitch.checked) {
            $(".readalongWord").css("color", '#fff');
            $(".readalongWord").css("background-color", '#222');
        } else {
            $(".readalongWord").css("color", '#212529');
            $(".readalongWord").css("background-color", 'transparent');
        }
    });

    $(".playHtListenText_field").on('keyup', function() {
        $(".__play-listen-btn__text").html($(this).val());
    });


    $( "#toUsePlayht" ).click(function() {$('.howToUse').show();});
    $( "#closeV" ).click(function() {$('.howToUse').hide();});
});