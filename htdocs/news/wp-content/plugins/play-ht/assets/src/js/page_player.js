
// pageplayer configs
(function() {
	const playht_articleURLElement = document.getElementById("playht-audioplayer-element");
	if(!playht_articleURLElement) return;
	const articleURL = playht_articleURLElement.getAttribute('data-play-article');
	const articleAudioURL = playht_articleURLElement.getAttribute('data-play-audio');
	window.playht.playerSettings({
		appId: window.wppp_user_data && window.wppp_user_data.app_id,
		userId: '',
		voice: 'Noah',
		delayAudioLoading: true,
		useCanonicalURL: false,
		playerType: 'pp',
		enableBuffering: false,
		listenBtnElement: "#playht-audioplayer-element",
		articleURL,
		articleAudioURL,
		mobile: {
			// bottom: 52
		},
		listenBtn: {
			btnSwitch: window.wppp_user_data && !!window.wppp_user_data.playhtListenbuttonSwitch,
			bgColor: window.wppp_user_data && window.wppp_user_data.playHtListencolor_backgrund,
            borderColor: window.wppp_user_data && window.wppp_user_data.playHtListenBorderColor,
            borderRadius: window.wppp_user_data && window.wppp_user_data.playHtListenBorderRadius,
            text: window.wppp_user_data && window.wppp_user_data.playHtListenText,
            textColor: window.wppp_user_data && window.wppp_user_data.playHtListentextColor,
		},
		theme: {
			itemsColor: window.wppp_user_data && window.wppp_user_data.playHtPlayerItemsColor,
			textColor: window.wppp_user_data ? window.wppp_user_data.playHtPlayerTextColor : "#000",
			playerBackgroundColor: window.wppp_user_data ? window.wppp_user_data.playHtPlayerBackgroundColor : "lavender",
			mode: window.wppp_user_data ? window.wppp_user_data.playHtDarkMode : 'dark'
		},
		fullScreenMobileModeEnabled: window.wppp_user_data ? window.wppp_user_data.fullScreenMobEnabledID : true,
		floatingButton: {
			visiblity: window.wppp_user_data ? !!window.wppp_user_data.playhtButtonSwitch : true,
			theme: {
				backgroundColor: window.wppp_user_data ? window.wppp_user_data.playHtcolor_backgrund : "#222",
				iconColor: window.wppp_user_data ? window.wppp_user_data.playHttextColor : "#fff"
			},
			desktopPosition: window.wppp_user_data ? window.wppp_user_data.FielddesktopPositionID : 'right',
			mobilePosition: window.wppp_user_data ? window.wppp_user_data.FieldmobilePositionID : 'left',
			fullScreenMobileModeEnabled: window.wppp_user_data ? window.wppp_user_data.fullScreenMobEnabledID : true,
		},
		whitelabelSwitch: window.wppp_user_data ? !!window.wppp_user_data.playHtbuttonWLabel : false
	});
})();
