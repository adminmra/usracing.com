(function(w, $, undefined) {
  $(function() {
    var userId = window.wppp_conv_data
      ? window.wppp_conv_data.userId
      : window.wppp_retrieve
      ? window.wppp_retrieve.user_id
      : "";

    // init google analytics
    (function(i, s, o, g, r, a, m) {
      i["GoogleAnalyticsObject"] = r;
      (i[r] =
        i[r] ||
        function() {
          (i[r].q = i[r].q || []).push(arguments);
        }),
        (i[r].l = 1 * new Date());
      (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m);
    })(
      window,
      document,
      "script",
      "//www.google-analytics.com/analytics.js",
      "__playht__analytics"
    );
    if (!__playht__analytics) {
      return;
    }
    __playht__analytics(
      "create",
      "UA-83224178-13",
      "auto",
      "playht__analytics"
    );
    __playht__analytics("playht__analytics.send", "pageview");

    // click events
    $(".js__playht-get-started").on("click", function() {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "click",
        "get-started",
        userId
      );
    });

    $(".welcome-page-voices .voices__list-action").on("click", function() {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "click",
        "listen-to-sample",
        userId
      );
    });

    $(".js__playht-connect-button").on("click", function() {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "click",
        "connect",
        userId
      );
    });

    $(".js__playht-chat-with-us").on("click", function() {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "click",
        "chat-with-us",
        userId
      );
    });

    $("body").on("click", ".wppp-modal-ok-btn", function() {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "click",
        "no-credits-modal-upgrade",
        userId
      );
    });

    // delete audio event
    if (
      location &&
      location.href &&
      location.href.indexOf("play=del_podcast") > -1
    ) {
      __playht__analytics(
        "playht__analytics.send",
        "event",
        "delete",
        "delete-audio",
        userId
      );
    }
  });
})(window, jQuery);
