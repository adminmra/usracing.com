
jQuery(function($) {
  var VOICES = [
    {
      "value": "en-US-Wavenet-A",
      "lang": "en-US",
      "name": "James",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L9221lipExv_CkzIPB2.mp3?generation=1522617039958470&alt=media"
    },
    {
      "value": "en-US-Wavenet-B",
      "lang": "en-US",
      "name": "Robert",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922AzTmsp3o6vbEHja.mp3?generation=1522617077599546&alt=media"
    },
    {
      "value": "en-US-Wavenet-C",
      "lang": "en-US",
      "name": "Patricia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922U22r6AkNB5neK_a.mp3?generation=1522617156348034&alt=media"
    },
    {
      "value": "en-US-Wavenet-D",
      "lang": "en-US",
      "name": "Richard",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ZPVzHtQhUQWXoql.mp3?generation=1522617177761625&alt=media"
    },
    {
      "value": "en-US-Wavenet-E",
      "lang": "en-US",
      "name": "Elizabeth",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ccDPTBRkh2a-OPC.mp3?generation=1522617195128886&alt=media"
    },
    {
      "value": "en-US-Wavenet-F",
      "lang": "en-US",
      "name": "Linda",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922hv4G__UzXDtQiaK.mp3?generation=1522617216961872&alt=media"
    },
    {
      "value": "en-US-Standard-B",
      "lang": "en-US",
      "name": "Joseph",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L923OBYVWRXyjidKeza.mp3?generation=1522617393696202&alt=media"
    },
    {
      "value": "en-US-Standard-C",
      "lang": "en-US",
      "name": "Sarah",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L923SPzNmFnpynKrLFD.mp3?generation=1522617411107453&alt=media"
    },
    {
      "value": "en-US-Standard-D",
      "lang": "en-US",
      "name": "Charles",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L923_nGArb_c994Ksfb.mp3?generation=1522617445720251&alt=media"
    },
    {
      "value": "en-US-Standard-E",
      "lang": "en-US",
      "name": "Nancy",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L923dyr-fVj18jKUZF9.mp3?generation=1522617462655974&alt=media"
    },
    {
      "value": "en-AU-Standard-A",
      "lang": "en-AU",
      "name": "Charlotte",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L9210HA-NVGzYcMAZfz.mp3?generation=1522616773273417&alt=media"
    },
    {
      "value": "en-AU-Standard-B",
      "lang": "en-AU",
      "name": "Oliver",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921NM0fEHtpVmjVpom.mp3?generation=1522616866081866&alt=media"
    },
    {
      "value": "en-AU-Standard-C",
      "lang": "en-AU",
      "name": "Olivia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921RrJaARdr3tBkQfP.mp3?generation=1522616884878418&alt=media"
    },
    {
      "value": "en-AU-Standard-D",
      "lang": "en-AU",
      "name": "William",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921XJraXxWptOh8dj9.mp3?generation=1522616907012445&alt=media"
    },
    {
      "value": "en-AU-Wavenet-A",
      "lang": "en-AU",
      "name": "Chloe",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQA_8KBa_gpxoO6UJPE.mp3?generation=1541013809392628&alt=media"
    },
    {
      "value": "en-AU-Wavenet-B",
      "lang": "en-AU",
      "name": "Ethan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQA_Rsg5B0I7qcINRtp.mp3?generation=1541013888282089&alt=media"
    },
    {
      "value": "en-AU-Wavenet-C",
      "lang": "en-AU",
      "name": "Ava",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQA_WzIjA082fQEb74T.mp3?generation=1541013910287852&alt=media"
    },
    {
      "value": "en-AU-Wavenet-D",
      "lang": "en-AU",
      "name": "Jackson",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQA_bVFtl-7c_fHIa4E.mp3?generation=1541013931631971&alt=media"
    },
    {
      "value": "en-GB-Standard-A",
      "lang": "en-GB",
      "name": "Amelia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921f0tUI3vZaN0c1VM.mp3?generation=1522616942374930&alt=media"
    },
    {
      "value": "en-GB-Standard-B",
      "lang": "en-GB",
      "name": "Jack",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921m2Z4NoZStjD_AV6.mp3?generation=1522616971411564&alt=media"
    },
    {
      "value": "en-GB-Standard-C",
      "lang": "en-GB",
      "name": "Mia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921qRIALLO29qrLerX.mp3?generation=1522616989155611&alt=media"
    },
    {
      "value": "en-GB-Standard-D",
      "lang": "en-GB",
      "name": "Thomas",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L921vgvrH9P6qCJgjH4.mp3?generation=1522617011138861&alt=media"
    },
    {
      "value": "en-GB-Wavenet-A",
      "lang": "en-GB",
      "name": "Isla",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAaA0KoRwYqEFL75Bw.mp3?generation=1541014077276567&alt=media"
    },
    {
      "value": "en-GB-Wavenet-B",
      "lang": "en-GB",
      "name": "Harry",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAaNU9-PM3vIRYvYjT.mp3?generation=1541014132385070&alt=media"
    },
    {
      "value": "en-GB-Wavenet-C",
      "lang": "en-GB",
      "name": "Hannah",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAaTNolT67VXJZLZ8P.mp3?generation=1541014156447725&alt=media"
    },
    {
      "value": "en-GB-Wavenet-D",
      "lang": "en-GB",
      "name": "Charlie",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAaYzxSCupOIZ4kTxV.mp3?generation=1541014179637263&alt=media"
    },
    {
      "value": "fr-FR-Standard-A",
      "lang": "fr-FR",
      "name": "Louise",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbNGVHkq-tkibHRum.mp3?generation=1541014394087052&alt=media"
    },
    {
      "value": "fr-FR-Standard-B",
      "lang": "fr-FR",
      "name": "Paul",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbSgg9Bdx98qncoOT.mp3?generation=1541014415985283&alt=media"
    },
    {
      "value": "fr-FR-Standard-C",
      "lang": "fr-FR",
      "name": "Emma",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L923vnmOO3QFSVOFZbU.mp3?generation=1522617536572176&alt=media"
    },
    {
      "value": "fr-FR-Standard-D",
      "lang": "fr-FR",
      "name": "Nathan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L9240JvANVoimsroxfR.mp3?generation=1522617558473289&alt=media"
    },
    {
      "value": "fr-FR-Wavenet-A",
      "lang": "fr-FR",
      "name": "Adele",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbZg8yrp1sGFxmjYn.mp3?generation=1541014444619035&alt=media"
    },
    {
      "value": "fr-FR-Wavenet-B",
      "lang": "fr-FR",
      "name": "Raphael",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbePA-21ck-dg3lvs.mp3?generation=1541014468028993&alt=media"
    },
    {
      "value": "fr-FR-Wavenet-C",
      "lang": "fr-FR",
      "name": "Lina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbiEw_gYRWamoY5QA.mp3?generation=1541014484583866&alt=media"
    },
    {
      "value": "fr-FR-Wavenet-D",
      "lang": "fr-FR",
      "name": "Victor",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAbpGm2-Kf1b6Rp7rp.mp3?generation=1541014517428182&alt=media"
    },
    {
      "value": "fr-CA-Standard-A",
      "lang": "fr-CA",
      "name": "Lola",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L9247C4JRNOf6ER7tbD.mp3?generation=1522617587086683&alt=media"
    },
    {
      "value": "fr-CA-Standard-B",
      "lang": "fr-CA",
      "name": "Gabriel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L924CBjdScK5csvuN_P.mp3?generation=1522617607177710&alt=media"
    },
    {
      "value": "fr-CA-Standard-C",
      "lang": "fr-CA",
      "name": "Camille",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L924H8IeC-fH1LVOPXg.mp3?generation=1522617627817767&alt=media"
    },
    {
      "value": "fr-CA-Standard-D",
      "lang": "fr-CA",
      "name": "Arthur",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L924LYiIIEAaFpOzHZA.mp3?generation=1522617645991034&alt=media"
    },
    {
      "value": "de-DE-Standard-A",
      "lang": "de-DE",
      "name": "Lara",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L924_sACJisZp7qnHHT.mp3?generation=1522617708348511&alt=media"
    },
    {
      "value": "de-DE-Standard-B",
      "lang": "de-DE",
      "name": "Leon",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L924eGnGaKpyv6veC4k.mp3?generation=1522617726144594&alt=media"
    },
    {
      "value": "de-DE-Wavenet-A",
      "lang": "de-DE",
      "name": "Marie",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAcfcr1Lk_zyuhNaGp.mp3?generation=1541014735484604&alt=media"
    },
    {
      "value": "de-DE-Wavenet-B",
      "lang": "de-DE",
      "name": "Otto",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAck2_bRvPkGnQs2p1.mp3?generation=1541014753268737&alt=media"
    },
    {
      "value": "de-DE-Wavenet-C",
      "lang": "de-DE",
      "name": "Mila",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAcrm6hydmyZ6ubvul.mp3?generation=1541014785445956&alt=media"
    },
    {
      "value": "de-DE-Wavenet-D",
      "lang": "de-DE",
      "name": "Kurt",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAcy0Xjb8tVTSBESLN.mp3?generation=1541014810821320&alt=media"
    },
    {
      "value": "nl-NL-Standard-A",
      "lang": "nl-NL",
      "name": "Sophie",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L95MH1xBVUthlPxyX5_.mp3?generation=1522672676340930&alt=media"
    },
    {
      "value": "nl-NL-Wavenet-A",
      "lang": "nl-NL",
      "name": "Julia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAguHtfde_hXpsKHUM.mp3?generation=1541015842527540&alt=media"
    },
    {
      "value": "it-IT-Standard-A",
      "lang": "it-IT",
      "name": "Giulia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAdmPk8V-Y4oqovhUw.mp3?generation=1541015025512262&alt=media"
    },
    {
      "value": "it-IT-Wavenet-A",
      "lang": "it-IT",
      "name": "Martina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAdyhAR4EVlMJGrZTp.mp3?generation=1541015076025194&alt=media"
    },
    {
      "value": "ja-JP-Standard-A",
      "lang": "ja-JP",
      "name": "Hana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L925rNYFrOFk6mzG2xm.mp3?generation=1522618042248373&alt=media"
    },
    {
      "value": "ja-JP-Wavenet-A",
      "lang": "ja-JP",
      "name": "Yui",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAeJOB7y338xxay2E1.mp3?generation=1541015164749315&alt=media"
    },
    {
      "value": "ko-KR-Standard-A",
      "lang": "ko-KR",
      "name": "Seo-yeon",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAgOeG00TgPeXQ_PRU.mp3?generation=1541015708689944&alt=media"
    },
    {
      "value": "ko-KR-Wavenet-A",
      "lang": "ko-KR",
      "name": "Ha-yoon",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LQAgV5tUlbfzqbpLwuE.mp3?generation=1541015735680246&alt=media"
    },
    {
      "value": "pt-BR-Standard-A",
      "lang": "pt-BR",
      "name": "Maria",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L925cxln2xviT1uu06s.mp3?generation=1522617982991887&alt=media"
    },
    {
      "value": "es-ES-Standard-A",
      "lang": "es-ES",
      "name": "Nora",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L925GfVP0BiQDMCo5RX.mp3?generation=1522617887668496&alt=media"
    },
    {
      "value": "sv-SE-Standard-A",
      "lang": "sv-SE",
      "name": "Alice",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L95Lo4SNhicdJsNH9uu.mp3?generation=1522672557818515&alt=media"
    },
    {
      "value": "tr-TR-Standard-A",
      "lang": "tr-TR",
      "name": "Mira",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L95M1GCFhGaZ9u5yBEt.mp3?generation=1522672613867552&alt=media"
    },
    {
      "value": "Matthew",
      "lang": "en-US",
      "name": "Matthew",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/0-sample-mathew-speech_20171218030814983.mp3?alt=media&token=63ec628e-aeed-4f9b-b6ba-0d684f9d033e"
    },
    {
      "value": "Joanna",
      "lang": "en-US",
      "name": "Joanna",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVhgobnKf5ImspJwLW.mp3?generation=1490902613862000&alt=media"
    },
    {
      "value": "Joey",
      "lang": "en-US",
      "name": "Joey",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVIl2_XCjOOW4SCvlr.mp3?generation=1490895832648000&alt=media"
    },
    {
      "value": "Salli",
      "lang": "en-US",
      "name": "Salli",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVgmqm6Cd3so_R1rsw.mp3?generation=1490902327090000&alt=media"
    },
    {
      "value": "Amy",
      "lang": "en-GB",
      "name": "Amy",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMvY3PjkOOHLo3rwH.mp3?generation=1490896906472000&alt=media"
    },
    {
      "value": "Brian",
      "lang": "en-GB",
      "name": "Brian",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVeFVXKvcJV5pTGeXT.mp3?generation=1490901753456656&alt=media"
    },
    {
      "value": "Emma",
      "lang": "en-GB",
      "name": "Emma",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMZR3Na89X_Jxu7-Z.mp3?generation=1490896816185000&alt=media"
    },
    {
      "value": "Geraint",
      "lang": "en-GB-WLS",
      "name": "Geraint",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVLtlohVq67Sz7hw-x.mp3?generation=1490896629761000&alt=media"
    },
    {
      "value": "Gwyneth",
      "lang": "cy-GB",
      "name": "Gwyneth",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FGwyneth.mp3?alt=media&token=ae6f2307-571a-468d-988f-5c18534d8a15"
    },
    {
      "value": "Mads",
      "lang": "da-DK",
      "name": "Mads",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FMads.mp3?alt=media&token=e304661e-7174-4026-8e0c-b30afb7ac9a2"
    },
    {
      "value": "Naja",
      "lang": "da-DK",
      "name": "Naja",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FNaja.mp3?alt=media&token=959b45e8-fb37-4f22-9013-05428fdd3246"
    },
    {
      "value": "Hans",
      "lang": "de-DE",
      "name": "Hans",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a1.mp3?alt=media&token=9eb1e854-969b-4d5b-9c61-f7a5340c7c62"
    },
    {
      "value": "Marlene",
      "lang": "de-DE",
      "name": "Marlene",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a.mp3?alt=media&token=18ba5695-92fb-4605-b0f8-94f70b529a74"
    },
    {
      "value": "Nicole",
      "lang": "en-AU",
      "name": "Nicole",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVNWvbLR0CcfBa6BJN.mp3?generation=1490897260103000&alt=media"
    },
    {
      "value": "Russell",
      "lang": "en-AU",
      "name": "Russell",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVNOsZztJzvF0enGsT.mp3?generation=1490897242061000&alt=media"
    },
    {
      "value": "Raveena",
      "lang": "en-IN",
      "name": "Raveena",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMJ3aErhvcJdro38R.mp3?generation=1490896731578000&alt=media"
    },
    {
      "value": "Conchita",
      "lang": "es-ES",
      "name": "Conchita",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FConchita.mp3?alt=media&token=eddf8f9f-1955-4c64-b9f3-eba108bb44cf"
    },
    {
      "value": "Enrique",
      "lang": "es-ES",
      "name": "Enrique",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FEnrique.mp3?alt=media&token=d63e84ae-0956-4aa1-ac4e-d1a632ed6314"
    },
    {
      "value": "Chantal",
      "lang": "fr-CA",
      "name": "Chantal",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FChantal.mp3?alt=media&token=b99c5a0b-74f9-4a60-abee-e4255b938cee"
    },
    {
      "value": "Celine",
      "lang": "fr-FR",
      "name": "Celine",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/celine.mp3?alt=media&token=c1c2518f-248f-4fad-802e-648debf8fc2c"
    },
    {
      "value": "Mathieu",
      "lang": "fr-FR",
      "name": "Mathieu",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/mathieu.mp3?alt=media&token=b5cace53-24a1-446f-8616-a312319e4cba"
    },
    {
      "value": "Dora",
      "lang": "is-IS",
      "name": "Dora",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FD%C3%B3ra.mp3?alt=media&token=39b9c9c3-82b8-4c59-83eb-3d24940962fb"
    },
    {
      "value": "Karl",
      "lang": "is-IS",
      "name": "Karl",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FKarl.mp3?alt=media&token=37eceb3b-0377-429d-b12e-65dbc44f7805"
    },
    {
      "value": "Carla",
      "lang": "it-IT",
      "name": "Carla",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FCarla.mp3?alt=media&token=bcaef246-a165-4f43-aeea-5e8776295420"
    },
    {
      "value": "Giorgio",
      "lang": "it-IT",
      "name": "Giorgio",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FGiorgio.mp3?alt=media&token=ba66dbaf-63e2-455f-844f-ab63a3df3303"
    },
    {
      "value": "Mizuki",
      "lang": "ja-JP",
      "name": "Mizuki",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FMizuki.mp3?alt=media&token=92639bd0-0467-43c6-b1a9-a005b2894fb9"
    },
    {
      "value": "Liv",
      "lang": "nb-NO",
      "name": "Liv",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FLiv.mp3?alt=media&token=7e156cc7-bd9a-4ded-ac71-c43cb805711a"
    },
    {
      "value": "Lotte",
      "lang": "nl-NL",
      "name": "Lotte",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FLotte.mp3?alt=media&token=2bb8eb47-4c38-4e59-9762-2cdbf3bad46f"
    },
    {
      "value": "Ruben",
      "lang": "nl-NL",
      "name": "Ruben",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FRuben.mp3?alt=media&token=358ea32e-6344-4ee0-b47d-8d681e20e44e"
    },
    {
      "value": "Ewa",
      "lang": "pl-PL",
      "name": "Ewa",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FEwa.mp3?alt=media&token=5d695c6d-3a5d-4267-9cb5-6c4dbac0179d"
    },
    {
      "value": "Jacek",
      "lang": "pl-PL",
      "name": "Jacek",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FJacek.mp3?alt=media&token=720253d6-d729-4d33-83e2-4604630ee166"
    },
    {
      "value": "Jan",
      "lang": "pl-PL",
      "name": "Jan",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FJan.mp3?alt=media&token=d3d5521d-c535-4d90-8364-6146670f264e"
    },
    {
      "value": "Maja",
      "lang": "pl-PL",
      "name": "Maja",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FMaja.mp3?alt=media&token=6206a7c0-e844-4b99-bc8e-6139bb9341e5"
    },
    {
      "value": "Ricardo",
      "lang": "pt-BR",
      "name": "Ricardo",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/ricardo.mp3?alt=media&token=6478fc45-88da-4232-b47c-f49005d1ea4c"
    },
    {
      "value": "Vitoria",
      "lang": "pt-BR",
      "name": "Vitoria",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/vitro.mp3?alt=media&token=01d80348-4a0e-4399-b06a-04e5f86f72d4"
    },
    {
      "value": "Cristiano",
      "lang": "pt-PT",
      "name": "Cristiano",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/cristiano-pr.mp3?alt=media&token=e6bf7009-4369-4f58-aa10-f7b25dbb54d1"
    },
    {
      "value": "Ines",
      "lang": "pt-PT",
      "name": "Ines",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/ines-pr.mp3?alt=media&token=4eae6877-1c51-4427-8083-6caab3469b0f"
    },
    {
      "value": "Carmen",
      "lang": "ro-RO",
      "name": "Carmen",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FCarmen.mp3?alt=media&token=dc6cd29a-5055-4ee8-ab6f-d048186d1594"
    },
    {
      "value": "Maxim",
      "lang": "ru-RU",
      "name": "Maxim",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/maxim.mp3?alt=media&token=a0bb06cc-2e30-49e7-ad38-2b15fde919b1"
    },
    {
      "value": "Tatyana",
      "lang": "ru-RU",
      "name": "Tatyana",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/tatayana.mp3?alt=media&token=e8c3aa94-c9dc-4867-8e28-e0e680ab2883"
    },
    {
      "value": "Astrid",
      "lang": "sv-SE",
      "name": "Astrid",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FAstrid.mp3?alt=media&token=daf72177-fcdc-45bc-82fa-3b76c610b932"
    },
    {
      "value": "Filiz",
      "lang": "tr-TR",
      "name": "Filiz",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FFiliz.mp3?alt=media&token=b949ca8f-fd70-4acf-a55f-2caff0f24ebc"
    },
    {
      "value": "Vicki",
      "lang": "de-DE",
      "name": "Vicki",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/vicki.mp3?alt=media&token=dba4480d-1ad8-44fc-82a4-b006d95fc2f9"
    },
    {
      "value": "Noah",
      "lang": "en-US",
      "name": "Noah",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogZgW_92qU-g1m9Ggp.mp3?generation=1568410881344476&alt=media"
    },
    {
      "value": "Scarlett",
      "lang": "en-US",
      "name": "Scarlett",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogY_-vcgXGYSusc9P8.mp3?generation=1568410588201308&alt=media"
    },
    {
      "value": "Jessica",
      "lang": "en-US",
      "name": "Jessica",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogXFKxxLFk2j0tRh8w.mp3?generation=1568410241359811&alt=media"
    },
    {
      "value": "Lucas",
      "lang": "en-US",
      "name": "Lucas",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-Log_ErN4hyuxgSyWVua.mp3?generation=1568411026629323&alt=media"
    },
    {
      "value": "Karen",
      "lang": "en-US",
      "name": "Karen",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-Log_6suBUyY6tHc4L7_.mp3?generation=1568410993906132&alt=media"
    },
    {
      "value": "Victoria",
      "lang": "en-US",
      "name": "Victoria",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogXjRYDhRnXoWh1IPK.mp3?generation=1568410368931237&alt=media"
    },
    {
      "value": "Ariana",
      "lang": "en-US",
      "name": "Ariana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogZNENVx8MbdgmEHEu.mp3?generation=1568410798307223&alt=media"
    },
    {
      "value": "Jennifer",
      "lang": "en-US",
      "name": "Jennifer",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogWz9FOwQGZOraxF5o.mp3?generation=1568410183377035&alt=media"
    },
    {
      "value": "Emilia",
      "lang": "en-GB",
      "name": "Emilia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-Log_xztPcPrYlK7njiY.mp3?generation=1568411215756565&alt=media"
    },
    {
      "value": "Logan",
      "lang": "en-GB",
      "name": "Logan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogaK7lNTBxsnG4LXXs.mp3?generation=1568411310551159&alt=media"
    },
    {
      "value": "Anna",
      "lang": "en-GB",
      "name": "Anna",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogaFSlFdkd0_de45l9.mp3?generation=1568411291097477&alt=media"
    },
    {
      "value": "pt-BR_IsabelaVoice",
      "lang": "pt-BR",
      "name": "Isabela",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/transcript_3_.mp3?alt=media&token=6d6392ad-59c0-4a54-8393-7f6b8c1b9c42"
    },
    {
      "value": "es-ES_EnriqueVoice",
      "lang": "es-ES",
      "name": "Diego",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fes-ES_EnriqueVoice.mp3?alt=media&token=09c55b24-4a9d-47c3-84da-7e63f548c13b"
    },
    {
      "value": "es-ES_LauraVoice",
      "lang": "es-ES",
      "name": "Laura",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fes-ES_LauraVoice.mp3?alt=media&token=fcb27805-0a7d-4902-bdd3-3300f639d503"
    },
    {
      "value": "fr-FR_ReneeVoice",
      "lang": "fr-FR",
      "name": "Renee",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a4.mp3?alt=media&token=dd7b1f44-7690-4efe-967c-7f96cf73f530"
    },
    {
      "value": "de-DE_BirgitVoice",
      "lang": "de-DE",
      "name": "Birgit",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a2.mp3?alt=media&token=082280a9-be29-413f-bd54-be78d5b36eb2"
    },
    {
      "value": "de-DE_DieterVoice",
      "lang": "de-DE",
      "name": "Dieter",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a3.mp3?alt=media&token=32853e26-742c-4fb5-b43a-699f51b920a6"
    },
    {
      "value": "it-IT_FrancescaVoice",
      "lang": "it-IT",
      "name": "Francesca",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fit-IT_FrancescaVoice.mp3?alt=media&token=3a55e738-3df4-4bfb-846d-09ab8accc0a6"
    },
    {
      "value": "ja-JP_EmiVoice",
      "lang": "ja-JP",
      "name": "Emi",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fja-JP_EmiVoice.mp3?alt=media&token=4b9611ef-df49-4622-aa9b-a48a7980a3d8"
    },
    {
      "value": "en-GB_KateVoice",
      "lang": "en-GB",
      "name": "Kate",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgV_vRa1kkRpHjNaTIp.mp3?generation=1518483415929395&alt=media"
    },
    {
      "value": "en-US_AllisonVoice",
      "lang": "en-US",
      "name": "Samantha",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVb-83aMMBMvrwu3dM.mp3?generation=1518483417835485&alt=media"
    },
    {
      "value": "en-US_LisaVoice",
      "lang": "en-US",
      "name": "Lisa",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVZd_WcSR924eXtVZX.mp3?generation=1518483411674118&alt=media"
    },
    {
      "value": "en-US_MichaelVoice",
      "lang": "en-US",
      "name": "Michael",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KW9TW-SvS8uNwlZX_R-.mp3?generation=1518381457510829&alt=media"
    },
    {
      "value": "pt-BR_IsabelaV3Voice",
      "lang": "pt-BR",
      "name": "Carolina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXkGmqlZKffNV1DVRD.mp3?generation=1572541156847471&alt=media"
    },
    {
      "value": "en-GB_KateV3Voice",
      "lang": "en-GB",
      "name": "Lily",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXkeeH9kX9l39gQWgy.mp3?generation=1572541260499895&alt=media"
    },
    {
      "value": "en-US_AllisonV3Voice",
      "lang": "en-US",
      "name": "Grace",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXjJKmmuZnvoZ803EU.mp3?generation=1572540907760362&alt=media"
    },
    {
      "value": "en-US_LisaV3Voice",
      "lang": "en-US",
      "name": "Camila",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXjlnpiI_xfsLoFOzd.mp3?generation=1572541010905215&alt=media"
    },
    {
      "value": "en-US_MichaelV3Voice",
      "lang": "en-US",
      "name": "Mark",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXk-Lz3bdr08IFxp7Z.mp3?generation=1572541061467173&alt=media"
    },
    {
      "value": "de-DE_BirgitV3Voice",
      "lang": "de-DE",
      "name": "Lina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXl3v_RRYnFA9uTjY4.mp3?generation=1572541368431776&alt=media"
    },
    {
      "value": "de-DE_DieterV3Voice",
      "lang": "de-DE",
      "name": "Elias",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXlTCP500HRWiivQEL.mp3?generation=1572541448831879&alt=media"
    },
    {
      "value": "it-IT_FrancescaV3Voice",
      "lang": "it-IT",
      "name": "Angelina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXlmin5vaVHXjI1jMU.mp3?generation=1572541532109287&alt=media"
    },
    {
      "value": "ja-JP_EmiV3Voice",
      "lang": "ja-JP",
      "name": "Airi",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXm6ux9_DjrM7HPzM7.mp3?generation=1572541622846285&alt=media"
    },
    {
      "value": "es-ES_EnriqueV3Voice",
      "lang": "es-ES",
      "name": "Leonardo",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXmUvBHm8d-2U5SkGy.mp3?generation=1572541715976296&alt=media"
    },
    {
      "value": "es-ES_LauraV3Voice",
      "lang": "es-ES",
      "name": "Lucia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LsXmlhpvxUEy9hTmu6v.mp3?generation=1572541789144215&alt=media"
    },
    {
      "value": "Camila",
      "lang": "pt-BR",
      "name": "Camila",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWi4gqZ7XveysGb8f3.mp3?generation=1577892471776082&alt=media"
    },
    {
      "value": "Lupe",
      "lang": "es-US",
      "name": "Lupe",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWicBn6Rjsvn6Yi7Cn.mp3?generation=1577892613586843&alt=media"
    },
    {
      "value": "Gabriella",
      "lang": "es-US",
      "name": "Gabriella",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/speech_20200502112008760.mp3?alt=media&token=6cecedbc-58bb-48ff-a1a7-96bbf02905ff"
    },
    {
      "value": "Matilde",
      "lang": "pt-BR",
      "name": "Matilde",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWiHN2ThCm2w6HNlbx.mp3?generation=1577892524056406&alt=media"
    },
    {
      "value": "Isabella",
      "lang": "es-US",
      "name": "Isabella",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWipo5_sYZqDHC0Tuq.mp3?generation=1577892670114310&alt=media"
    },
    {
      "value": "Zeina",
      "lang": "ar-AR",
      "name": "Zeina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWgyp5cNhiXhSnGVkp.mp3?generation=1577892182650502&alt=media"
    },
    {
      "value": "Zhiyu",
      "lang": "cmn-CN",
      "name": "Zhiyu",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWhH8fapjDXxvPMMk8.mp3?generation=1577892261557151&alt=media "
    },
    {
      "value": "Aditi",
      "lang": "hi-IN",
      "name": "Aditi",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWkk-QtGFH-bs1LrKQ.mp3?generation=1577893170919694&alt=media "
    },
    {
      "value": "Lea",
      "lang": "fr-FR",
      "name": "Léa",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWkNiI87eh_JYZjHeQ.mp3?generation=1577893074461106&alt=media"
    },
    {
      "value": "Bianca",
      "lang": "it-IT",
      "name": "Bianca",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWkYcTnJR-p5XZO54L.mp3?generation=1577893118823990&alt=media"
    },
    {
      "value": "Miguel",
      "lang": "es-US",
      "name": "Miguel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWjE_Ota4LsQUMKHcY.mp3?generation=1577892774614999&alt=media"
    },
    {
      "value": "Penelope",
      "lang": "es-US",
      "name": "Penelope",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWjQ7rRSkKvM2kL4Mq.mp3?generation=1577892822127244&alt=media"
    },
    {
      "value": "Seoyeon",
      "lang": "ko-KR",
      "name": "Seoyeon",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWl-G6Oge0_BiqT_Ys.mp3?generation=1577893236905756&alt=media"
    },
    {
      "value": "Takumi",
      "lang": "ja-JP",
      "name": "Takumi",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWlEVg48kq3Mr4vIJi.mp3?generation=1577893299166845&alt=media"
    },
    {
      "value": "Lucia",
      "lang": "es-ES",
      "name": "Lucia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWjmfGFqFTI8WWnfin.mp3?generation=1577892919023354&alt=media"
    },
    {
      "value": "Mia",
      "lang": "es-MX",
      "name": "Mia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LxWjwvL1IufJkN2CDfb.mp3?generation=1577892960781199&alt=media"
    },
    {
      "value": "ar-AR_OmarVoice",
      "lang": "ar-AR",
      "name": "Omar",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/Omar.wav?generation=1577745426743578&alt=media"
    },
    {
      "value": "zh-CN_LiNaVoice",
      "lang": "zh-CN",
      "name": "Lina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/LiNa.wav?generation=1577745430381339&alt=media"
    },
    {
      "value": "zh-CN_WangWeiVoice",
      "lang": "zh-CN",
      "name": "Wang",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/WangWei.wav?generation=1577745433885927&alt=media"
    },
    {
      "value": "zh-CN_ZhangJingVoice",
      "lang": "zh-CN",
      "name": "Zhang",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ZhangJing.wav?generation=1577745438229657&alt=media"
    },
    {
      "value": "nl-NL_EmmaVoice",
      "lang": "nl-NL",
      "name": "Emma",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/Emma.wav?generation=1577745445254132&alt=media"
    },
    {
      "value": "nl-NL_LiamVoice",
      "lang": "nl-NL",
      "name": "Liam",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/Liam.wav?generation=1577745448807288&alt=media"
    },
    {
      "value": "fr-FR_ReneeV3Voice",
      "lang": "fr-FR",
      "name": "Alice",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ReneeV3.wav?generation=1577745452334842&alt=media"
    },
    {
      "value": "es-LA_SofiaVoice",
      "lang": "es-LA",
      "name": "Valentina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/Sofia.wav?generation=1577745455676604&alt=media"
    },
    {
      "value": "es-US_SofiaVoice",
      "lang": "es-US",
      "name": "Sofia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/Sofia.wav?generation=1577745455676604&alt=media"
    },
    {
      "value": "es-LA_SofiaV3Voice",
      "lang": "es-LA",
      "name": "Luciana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/SofiaV3.wav?generation=1577745458751639&alt=media"
    },
    {
      "value": "es-US_SofiaV3Voice",
      "lang": "es-US",
      "name": "Valeria",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/SofiaV3.wav?generation=1577745458751639&alt=media"
    },
    {
      "value": "ru-RU-Standard-A",
      "lang": "ru-RU",
      "name": "Galina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Standard-A.mp3?generation=1577739011726330&alt=media"
    },
    {
      "value": "ru-RU-Standard-B",
      "lang": "ru-RU",
      "name": "Abram",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Standard-B.mp3?generation=1577739014423122&alt=media"
    },
    {
      "value": "ru-RU-Standard-C",
      "lang": "ru-RU",
      "name": "Katina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Standard-C.mp3?generation=1577739016564093&alt=media"
    },
    {
      "value": "ru-RU-Standard-D",
      "lang": "ru-RU",
      "name": "Borya",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Standard-D.mp3?generation=1577739019110135&alt=media"
    },
    {
      "value": "cmn-CN-Standard-A",
      "lang": "cmn-CN",
      "name": "Yu Yan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Standard-A.mp3?generation=1577739022757971&alt=media"
    },
    {
      "value": "cmn-CN-Standard-B",
      "lang": "cmn-CN",
      "name": "Zhang Wei",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Standard-B.mp3?generation=1577739024779796&alt=media"
    },
    {
      "value": "cmn-CN-Standard-C",
      "lang": "cmn-CN",
      "name": "Wang Fang",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Standard-C.mp3?generation=1577739027176084&alt=media"
    },
    {
      "value": "ko-KR-Standard-B",
      "lang": "ko-KR",
      "name": "Bo-young",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Standard-B.mp3?generation=1577739032777051&alt=media"
    },
    {
      "value": "ko-KR-Standard-C",
      "lang": "ko-KR",
      "name": "Baul",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Standard-C.mp3?generation=1577739035427096&alt=media"
    },
    {
      "value": "ko-KR-Standard-D",
      "lang": "ko-KR",
      "name": "Chae-koo",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Standard-D.mp3?generation=1577739038153498&alt=media"
    },
    {
      "value": "ja-JP-Standard-B",
      "lang": "ja-JP",
      "name": "Himari",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Standard-B.mp3?generation=1577739040541073&alt=media"
    },
    {
      "value": "ja-JP-Standard-C",
      "lang": "ja-JP",
      "name": "Daiki",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Standard-C.mp3?generation=1577739043091979&alt=media"
    },
    {
      "value": "ja-JP-Standard-D",
      "lang": "ja-JP",
      "name": "Eito",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Standard-D.mp3?generation=1577739046075102&alt=media"
    },
    {
      "value": "vi-VN-Standard-A",
      "lang": "vi-VN",
      "name": "Bich",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Standard-A.mp3?generation=1577739048458246&alt=media"
    },
    {
      "value": "vi-VN-Standard-B",
      "lang": "vi-VN",
      "name": "Chi",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Standard-B.mp3?generation=1577739050975752&alt=media"
    },
    {
      "value": "vi-VN-Standard-C",
      "lang": "vi-VN",
      "name": "Cam",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Standard-C.mp3?generation=1577739053306565&alt=media"
    },
    {
      "value": "vi-VN-Standard-D",
      "lang": "vi-VN",
      "name": "Danh",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Standard-D.mp3?generation=1577739055778029&alt=media"
    },
    {
      "value": "fil-PH-Standard-A",
      "lang": "fil-PH",
      "name": "Dalisay",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fil-PH-Standard-A.mp3?generation=1577739058248553&alt=media"
    },
    {
      "value": "id-ID-Standard-A",
      "lang": "id-ID",
      "name": "Farah",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Standard-A.mp3?generation=1577739060496907&alt=media"
    },
    {
      "value": "id-ID-Standard-B",
      "lang": "id-ID",
      "name": "Angga",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Standard-B.mp3?generation=1577739063177442&alt=media"
    },
    {
      "value": "id-ID-Standard-C",
      "lang": "id-ID",
      "name": "Farel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Standard-C.mp3?generation=1577739065530130&alt=media"
    },
    {
      "value": "nl-NL-Standard-B",
      "lang": "nl-NL",
      "name": "Bram",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Standard-B.mp3?generation=1577739067887387&alt=media"
    },
    {
      "value": "nl-NL-Standard-C",
      "lang": "nl-NL",
      "name": "Lucas",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Standard-C.mp3?generation=1577739070718970&alt=media"
    },
    {
      "value": "nl-NL-Standard-D",
      "lang": "nl-NL",
      "name": "Tess",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Standard-D.mp3?generation=1577739073394658&alt=media"
    },
    {
      "value": "nl-NL-Standard-E",
      "lang": "nl-NL",
      "name": "Fenna",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Standard-E.mp3?generation=1577739076407888&alt=media"
    },
    {
      "value": "cs-CZ-Standard-A",
      "lang": "cs-CZ",
      "name": "Adina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cs-CZ-Standard-A.mp3?generation=1577739078563687&alt=media"
    },
    {
      "value": "el-GR-Standard-A",
      "lang": "el-GR",
      "name": "Gaia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/el-GR-Standard-A.mp3?generation=1577739080795937&alt=media"
    },
    {
      "value": "hu-HU-Standard-A",
      "lang": "hu-HU",
      "name": "Adel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hu-HU-Standard-A.mp3?generation=1577739083089621&alt=media"
    },
    {
      "value": "pl-PL-Standard-E",
      "lang": "pl-PL",
      "name": "Lena",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Standard-E.mp3?generation=1577739086103631&alt=media"
    },
    {
      "value": "pl-PL-Standard-A",
      "lang": "pl-PL",
      "name": "Zofia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Standard-A.mp3?generation=1577739088445536&alt=media"
    },
    {
      "value": "pl-PL-Standard-B",
      "lang": "pl-PL",
      "name": "Kacper",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Standard-B.mp3?generation=1577739090988285&alt=media"
    },
    {
      "value": "pl-PL-Standard-C",
      "lang": "pl-PL",
      "name": "Filip",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Standard-C.mp3?generation=1577739093364355&alt=media"
    },
    {
      "value": "pl-PL-Standard-D",
      "lang": "pl-PL",
      "name": "Aleksandra",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Standard-D.mp3?generation=1577739096517775&alt=media"
    },
    {
      "value": "sk-SK-Standard-A",
      "lang": "sk-SK",
      "name": "Natalia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/sk-SK-Standard-A.mp3?generation=1577739099046243&alt=media"
    },
    {
      "value": "tr-TR-Standard-B",
      "lang": "tr-TR",
      "name": "Ahmet",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Standard-B.mp3?generation=1577739101152837&alt=media"
    },
    {
      "value": "tr-TR-Standard-C",
      "lang": "tr-TR",
      "name": "Aysel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Standard-C.mp3?generation=1577739103408828&alt=media"
    },
    {
      "value": "tr-TR-Standard-D",
      "lang": "tr-TR",
      "name": "Aysun",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Standard-D.mp3?generation=1577739105651089&alt=media"
    },
    {
      "value": "tr-TR-Standard-E",
      "lang": "tr-TR",
      "name": "Ayaz",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Standard-E.mp3?generation=1577739108489383&alt=media"
    },
    {
      "value": "uk-UA-Standard-A",
      "lang": "uk-UA",
      "name": "Yulia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/uk-UA-Standard-A.mp3?generation=1577739110551869&alt=media"
    },
    {
      "value": "en-IN-Standard-A",
      "lang": "en-IN",
      "name": "Alisha",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Standard-A.mp3?generation=1577739112789160&alt=media"
    },
    {
      "value": "en-IN-Standard-B",
      "lang": "en-IN",
      "name": "Sai",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Standard-B.mp3?generation=1577739114824760&alt=media"
    },
    {
      "value": "en-IN-Standard-C",
      "lang": "en-IN",
      "name": "Ayaan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Standard-C.mp3?generation=1577739117076008&alt=media"
    },
    {
      "value": "hi-IN-Standard-A",
      "lang": "hi-IN",
      "name": "Esha",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Standard-A.mp3?generation=1577739119452901&alt=media"
    },
    {
      "value": "hi-IN-Standard-B",
      "lang": "hi-IN",
      "name": "Ansh",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Standard-B.mp3?generation=1577739121684251&alt=media"
    },
    {
      "value": "hi-IN-Standard-C",
      "lang": "hi-IN",
      "name": "Krishna",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Standard-C.mp3?generation=1577739131934950&alt=media"
    },
    {
      "value": "da-DK-Standard-A",
      "lang": "da-DK",
      "name": "Agathe",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/da-DK-Standard-A.mp3?generation=1577739133929183&alt=media"
    },
    {
      "value": "fi-FI-Standard-A",
      "lang": "fi-FI",
      "name": "Anja",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fi-FI-Standard-A.mp3?generation=1577739136364976&alt=media"
    },
    {
      "value": "pt-PT-Standard-A",
      "lang": "pt-PT",
      "name": "Leonor",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Standard-A.mp3?generation=1577739138631598&alt=media"
    },
    {
      "value": "pt-PT-Standard-B",
      "lang": "pt-PT",
      "name": "Duda",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Standard-B.mp3?generation=1577739141336142&alt=media"
    },
    {
      "value": "pt-PT-Standard-C",
      "lang": "pt-PT",
      "name": "Emilio",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Standard-C.mp3?generation=1577739149572372&alt=media"
    },
    {
      "value": "pt-PT-Standard-D",
      "lang": "pt-PT",
      "name": "Beatriz",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Standard-D.mp3?generation=1577739152232305&alt=media"
    },
    {
      "value": "nb-no-Standard-E",
      "lang": "nb-NO",
      "name": "Agot",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-no-Standard-E.mp3?generation=1577739154403791&alt=media"
    },
    {
      "value": "nb-NO-Standard-A",
      "lang": "nb-NO",
      "name": "Anja",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Standard-A.mp3?generation=1577739157027630&alt=media"
    },
    {
      "value": "nb-NO-Standard-B",
      "lang": "nb-NO",
      "name": "Filip",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Standard-B.mp3?generation=1577739159581968&alt=media"
    },
    {
      "value": "nb-NO-Standard-C",
      "lang": "nb-NO",
      "name": "Hella",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Standard-C.mp3?generation=1577739162331056&alt=media"
    },
    {
      "value": "nb-NO-Standard-D",
      "lang": "nb-NO",
      "name": "Jakob",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Standard-D.mp3?generation=1577739164615116&alt=media"
    },
    {
      "value": "it-IT-Standard-B",
      "lang": "it-IT",
      "name": "Amara",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Standard-B.mp3?generation=1577739167986420&alt=media"
    },
    {
      "value": "it-IT-Standard-C",
      "lang": "it-IT",
      "name": "Simone",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Standard-C.mp3?generation=1577739170434918&alt=media"
    },
    {
      "value": "it-IT-Standard-D",
      "lang": "it-IT",
      "name": "Diego",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Standard-D.mp3?generation=1577739178105559&alt=media"
    },
    {
      "value": "ar-XA-Wavenet-A",
      "lang": "ar-XA",
      "name": "Aisha",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Wavenet-A.mp3?generation=1577738276541750&alt=media"
    },
    {
      "value": "ar-XA-Wavenet-B",
      "lang": "ar-XA",
      "name": "Amjad",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Wavenet-B.mp3?generation=1577738278824389&alt=media"
    },
    {
      "value": "ar-XA-Wavenet-C",
      "lang": "ar-XA",
      "name": "Hazem",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Wavenet-C.mp3?generation=1577738281429059&alt=media"
    },
    {
      "value": "cmn-CN-Wavenet-A",
      "lang": "cmn-CN",
      "name": "Zhi Ruo",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Wavenet-A.mp3?generation=1577738283601861&alt=media"
    },
    {
      "value": "cmn-CN-Wavenet-B",
      "lang": "cmn-CN",
      "name": "Li Wei",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Wavenet-B.mp3?generation=1577738285782054&alt=media"
    },
    {
      "value": "cmn-CN-Wavenet-C",
      "lang": "cmn-CN",
      "name": "Li Na",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cmn-CN-Wavenet-C.mp3?generation=1577738288030285&alt=media"
    },
    {
      "value": "cs-CZ-Wavenet-A",
      "lang": "cs-CZ",
      "name": "Adriana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/cs-CZ-Wavenet-A.mp3?generation=1577738290065529&alt=media"
    },
    {
      "value": "da-DK-Wavenet-A",
      "lang": "da-DK",
      "name": "Agnes",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/da-DK-Wavenet-A.mp3?generation=1577738292729608&alt=media"
    },
    {
      "value": "el-GR-Wavenet-A",
      "lang": "el-GR",
      "name": "Hera",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/el-GR-Wavenet-A.mp3?generation=1577738294839977&alt=media"
    },
    {
      "value": "en-IN-Wavenet-A",
      "lang": "en-IN",
      "name": "Amara",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Wavenet-A.mp3?generation=1577738297793278&alt=media"
    },
    {
      "value": "en-IN-Wavenet-B",
      "lang": "en-IN",
      "name": "Vivaan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Wavenet-B.mp3?generation=1577738300354038&alt=media"
    },
    {
      "value": "en-IN-Wavenet-C",
      "lang": "en-IN",
      "name": "Aditya",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/en-IN-Wavenet-C.mp3?generation=1577738302754049&alt=media"
    },
    {
      "value": "fi-FI-Wavenet-A",
      "lang": "fi-FI",
      "name": "Anneli",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fi-FI-Wavenet-A.mp3?generation=1577738304902879&alt=media"
    },
    {
      "value": "fil-PH-Wavenet-A",
      "lang": "fil-PH",
      "name": "Analyn",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fil-PH-Wavenet-A.mp3?generation=1577738307208123&alt=media"
    },
    {
      "value": "fr-CA-Wavenet-A",
      "lang": "fr-CA",
      "name": "Brigitte",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fr-CA-Wavenet-A.mp3?generation=1577738309588769&alt=media"
    },
    {
      "value": "fr-CA-Wavenet-B",
      "lang": "fr-CA",
      "name": "Victor",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fr-CA-Wavenet-B.mp3?generation=1577738312009079&alt=media"
    },
    {
      "value": "fr-CA-Wavenet-C",
      "lang": "fr-CA",
      "name": "Charlotte",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fr-CA-Wavenet-C.mp3?generation=1577738314380445&alt=media"
    },
    {
      "value": "fr-CA-Wavenet-D",
      "lang": "fr-CA",
      "name": "Jules",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/fr-CA-Wavenet-D.mp3?generation=1577738316852341&alt=media"
    },
    {
      "value": "hi-IN-Wavenet-A",
      "lang": "hi-IN",
      "name": "Deepa",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Wavenet-A.mp3?generation=1577738319088814&alt=media"
    },
    {
      "value": "hi-IN-Wavenet-B",
      "lang": "hi-IN",
      "name": "Ishaan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Wavenet-B.mp3?generation=1577738321652301&alt=media"
    },
    {
      "value": "hi-IN-Wavenet-C",
      "lang": "hi-IN",
      "name": "Rudra",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hi-IN-Wavenet-C.mp3?generation=1577738323871516&alt=media"
    },
    {
      "value": "hu-HU-Wavenet-A",
      "lang": "hu-HU",
      "name": "Aliz",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/hu-HU-Wavenet-A.mp3?generation=1577738326155297&alt=media"
    },
    {
      "value": "id-ID-Wavenet-A",
      "lang": "id-ID",
      "name": "Cindy",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Wavenet-A.mp3?generation=1577738328897775&alt=media"
    },
    {
      "value": "id-ID-Wavenet-B",
      "lang": "id-ID",
      "name": "Andy",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Wavenet-B.mp3?generation=1577738331357407&alt=media"
    },
    {
      "value": "id-ID-Wavenet-C",
      "lang": "id-ID",
      "name": "Aditya",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/id-ID-Wavenet-C.mp3?generation=1577738333665083&alt=media"
    },
    {
      "value": "it-IT-Wavenet-B",
      "lang": "it-IT",
      "name": "Editta",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Wavenet-B.mp3?generation=1577738335867160&alt=media"
    },
    {
      "value": "it-IT-Wavenet-C",
      "lang": "it-IT",
      "name": "Lorenzo",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Wavenet-C.mp3?generation=1577738338481471&alt=media"
    },
    {
      "value": "it-IT-Wavenet-D",
      "lang": "it-IT",
      "name": "Mattia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/it-IT-Wavenet-D.mp3?generation=1577738341837645&alt=media"
    },
    {
      "value": "ja-JP-Wavenet-B",
      "lang": "ja-JP",
      "name": "Hina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Wavenet-B.mp3?generation=1577738344156604&alt=media"
    },
    {
      "value": "ja-JP-Wavenet-C",
      "lang": "ja-JP",
      "name": "Akira",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Wavenet-C.mp3?generation=1577738351965290&alt=media"
    },
    {
      "value": "ja-JP-Wavenet-D",
      "lang": "ja-JP",
      "name": "Aito",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ja-JP-Wavenet-D.mp3?generation=1577738354188304&alt=media"
    },
    {
      "value": "ko-KR-Wavenet-B",
      "lang": "ko-KR",
      "name": "Chan-sook",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Wavenet-B.mp3?generation=1577738356888285&alt=media"
    },
    {
      "value": "ko-KR-Wavenet-C",
      "lang": "ko-KR",
      "name": "Cheol",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Wavenet-C.mp3?generation=1577738359324598&alt=media"
    },
    {
      "value": "ko-KR-Wavenet-D",
      "lang": "ko-KR",
      "name": "Chang-min",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ko-KR-Wavenet-D.mp3?generation=1577738362072975&alt=media"
    },
    {
      "value": "nb-no-Wavenet-E",
      "lang": "nb-NO",
      "name": "Karina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-no-Wavenet-E.mp3?generation=1577738364449208&alt=media"
    },
    {
      "value": "nb-NO-Wavenet-A",
      "lang": "nb-NO",
      "name": "Lise",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Wavenet-A.mp3?generation=1577738367012660&alt=media"
    },
    {
      "value": "nb-NO-Wavenet-B",
      "lang": "nb-NO",
      "name": "Aksel",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Wavenet-B.mp3?generation=1577738369136322&alt=media"
    },
    {
      "value": "nb-NO-Wavenet-C",
      "lang": "nb-NO",
      "name": "Margit",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Wavenet-C.mp3?generation=1577738371778721&alt=media"
    },
    {
      "value": "nb-NO-Wavenet-D",
      "lang": "nb-NO",
      "name": "Mathias",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nb-NO-Wavenet-D.mp3?generation=1577738373991797&alt=media"
    },
    {
      "value": "nl-NL-Wavenet-B",
      "lang": "nl-NL",
      "name": "Daan",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Wavenet-B.mp3?generation=1577738376220848&alt=media"
    },
    {
      "value": "nl-NL-Wavenet-C",
      "lang": "nl-NL",
      "name": "Luuk",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Wavenet-C.mp3?generation=1577738378689491&alt=media"
    },
    {
      "value": "nl-NL-Wavenet-D",
      "lang": "nl-NL",
      "name": "Zoe",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Wavenet-D.mp3?generation=1577738381136976&alt=media"
    },
    {
      "value": "nl-NL-Wavenet-E",
      "lang": "nl-NL",
      "name": "Eva",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/nl-NL-Wavenet-E.mp3?generation=1577738383516076&alt=media"
    },
    {
      "value": "pl-PL-Wavenet-A",
      "lang": "pl-PL",
      "name": "Alicja",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Wavenet-A.mp3?generation=1577738386356317&alt=media"
    },
    {
      "value": "pl-PL-Wavenet-B",
      "lang": "pl-PL",
      "name": "Szymon",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Wavenet-B.mp3?generation=1577738394141260&alt=media"
    },
    {
      "value": "pl-PL-Wavenet-C",
      "lang": "pl-PL",
      "name": "Antoni",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Wavenet-C.mp3?generation=1577738398769596&alt=media"
    },
    {
      "value": "pl-PL-Wavenet-D",
      "lang": "pl-PL",
      "name": "Nikola",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Wavenet-D.mp3?generation=1577738401456134&alt=media"
    },
    {
      "value": "pl-PL-Wavenet-E",
      "lang": "pl-PL",
      "name": "Oliwia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pl-PL-Wavenet-E.mp3?generation=1577738408758701&alt=media"
    },
    {
      "value": "pt-BR-Wavenet-A",
      "lang": "pt-BR",
      "name": "Ines",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-BR-Wavenet-A.mp3?generation=1577738411192281&alt=media"
    },
    {
      "value": "pt-PT-Wavenet-A",
      "lang": "pt-PT",
      "name": "Ana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Wavenet-A.mp3?generation=1577738413431936&alt=media"
    },
    {
      "value": "pt-PT-Wavenet-B",
      "lang": "pt-PT",
      "name": "Jordao",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Wavenet-B.mp3?generation=1577738416138251&alt=media"
    },
    {
      "value": "pt-PT-Wavenet-C",
      "lang": "pt-PT",
      "name": "Marco",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Wavenet-C.mp3?generation=1577738418793257&alt=media"
    },
    {
      "value": "pt-PT-Wavenet-D",
      "lang": "pt-PT",
      "name": "Mariana",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/pt-PT-Wavenet-D.mp3?generation=1577738420859360&alt=media"
    },
    {
      "value": "ru-RU-Wavenet-A",
      "lang": "ru-RU",
      "name": "Annika",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Wavenet-A.mp3?generation=1577738424041004&alt=media"
    },
    {
      "value": "ru-RU-Wavenet-B",
      "lang": "ru-RU",
      "name": "Alyosha",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Wavenet-B.mp3?generation=1577738431705078&alt=media"
    },
    {
      "value": "ru-RU-Wavenet-C",
      "lang": "ru-RU",
      "name": "Khristina",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Wavenet-C.mp3?generation=1577738433887601&alt=media"
    },
    {
      "value": "ru-RU-Wavenet-D",
      "lang": "ru-RU",
      "name": "Artyom",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ru-RU-Wavenet-D.mp3?generation=1577738437044750&alt=media"
    },
    {
      "value": "sk-SK-Wavenet-A",
      "lang": "sk-SK",
      "name": "Nela",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/sk-SK-Wavenet-A.mp3?generation=1577738439633502&alt=media"
    },
    {
      "value": "sv-SE-Wavenet-A",
      "lang": "sv-SE",
      "name": "Agnes",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/sv-SE-Wavenet-A.mp3?generation=1577738443028242&alt=media"
    },
    {
      "value": "tr-TR-Wavenet-A",
      "lang": "tr-TR",
      "name": "Aylin",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Wavenet-A.mp3?generation=1577738450225890&alt=media"
    },
    {
      "value": "tr-TR-Wavenet-B",
      "lang": "tr-TR",
      "name": "Berat",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Wavenet-B.mp3?generation=1577738452735133&alt=media"
    },
    {
      "value": "tr-TR-Wavenet-C",
      "lang": "tr-TR",
      "name": "Berna",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Wavenet-C.mp3?generation=1577738454946864&alt=media"
    },
    {
      "value": "tr-TR-Wavenet-D",
      "lang": "tr-TR",
      "name": "Basak",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Wavenet-D.mp3?generation=1577738457027041&alt=media"
    },
    {
      "value": "tr-TR-Wavenet-E",
      "lang": "tr-TR",
      "name": "Omer",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/tr-TR-Wavenet-E.mp3?generation=1577738459913023&alt=media"
    },
    {
      "value": "uk-UA-Wavenet-A",
      "lang": "uk-UA",
      "name": "Natalia",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/uk-UA-Wavenet-A.mp3?generation=1577738462098259&alt=media"
    },
    {
      "value": "vi-VN-Wavenet-A",
      "lang": "vi-VN",
      "name": "Hau",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Wavenet-A.mp3?generation=1577738464428077&alt=media"
    },
    {
      "value": "vi-VN-Wavenet-B",
      "lang": "vi-VN",
      "name": "Dung",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Wavenet-B.mp3?generation=1577738610954777&alt=media"
    },
    {
      "value": "vi-VN-Wavenet-C",
      "lang": "vi-VN",
      "name": "Hoa",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Wavenet-C.mp3?generation=1577738613163180&alt=media"
    },
    {
      "value": "vi-VN-Wavenet-D",
      "lang": "vi-VN",
      "name": "Duong",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/vi-VN-Wavenet-D.mp3?generation=1577738615731465&alt=media"
    },
    {
      "value": "ar-XA-Standard-A",
      "lang": "ar-XA",
      "name": "Alya",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Standard-A.mp3?generation=1577738618139375&alt=media"
    },
    {
      "value": "ar-XA-Standard-B",
      "lang": "ar-XA",
      "name": "Idris",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Standard-B.mp3?generation=1577738641731649&alt=media"
    },
    {
      "value": "ar-XA-Standard-C",
      "lang": "ar-XA",
      "name": "Jalal",
      "sample": "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/ar-XA-Standard-C.mp3?generation=1577738644019839&alt=media"
    },
    {
      "value": "Noah",
      "lang": "en-US",
      "name": "Noah (Newscaster) (en-US)",
      "style": "newscaster",
      "sample":
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogbA8-4tooZf1-fG-M.mp3?generation=1568411531053358&alt=media"
    },
    {
      "value": "Noah",
      "lang": "en-US",
      "name": "Noah (Conversational) (en-US)",
      "style": "conversational",
      "sample":
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LzNgBGF6i2SWrPhfsrf.mp3?generation=1579888461924934&alt=media"
    },
    {
      "value": "Jessica",
      "lang": "en-US",
      "name": "Jessica (Newscaster) (en-US)",
      "style": "newscaster",
      "sample":
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LogbEEgYZqXoPPN9a_7.mp3?generation=1568411548164481&alt=media"
    },
    {
      "value": "Jessica",
      "lang": "en-US",
      "name": "Jessica (Conversational) (en-US)",
      "style": "conversational",
      "sample":
        "https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-LzNgNUzhAYsbIWnHlrd.mp3?generation=1579888512028029&alt=media"
    },
    {
      "value": "Gabriella",
      "lang": "es-US",
      "name": "Gabriella (Newscaster) (es-US)",
      "style": "newscaster",
      "sample": "https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/speech_20200502112008760.mp3?alt=media&token=6cecedbc-58bb-48ff-a1a7-96bbf02905ff"
    },
  ];

  $(document).ready(function() {
    var element = document.getElementById("wppp_voice_try");
    element && (element.style.visibility = "visible");
    var voice_meta = document.getElementById("wppp_voice_meta");
  });

  $("#wppp_voice_meta, #wppp_lang_meta, #wppp_voice_style_meta").change(
    function() {
      var voice_meta = document.getElementById("wppp_voice_meta");
      var audio_form_a = document.getElementById("audio_idforplay");
      var ifelement = document.getElementById("wppp_voice_try");
      var icon_play = document.getElementById("audio_icon_play");
      var selectedStyle = $("#wppp_voice_style_meta").attr("value");

      if (ifelement && ifelement.style.visibility == "hidden") {
        ifelement.style.visibility = "visible";
      }

      icon_play.src = "https://play.ht/app-landing/images/play-btn.png";

      if ($("#wppp_lang_meta").val() != "0") {
          var selectedVoice = voice_meta.options[voice_meta.selectedIndex].value;
          VOICES.forEach(voice => {
            if (selectedVoice == voice.value) {
                if(voice.style  && (voice.style === selectedStyle)){
                  $("#wppp_voice_try .voices__list-title").html(
                    voice.name
                  );
                  audio_form_a.src = voice.sample;
                }
                if(!voice.style){
                  $("#wppp_voice_try .voices__list-title").html(
                    voice.name
                  );
                  audio_form_a.src = voice.sample;
                }
            }
          });
      } else {
        ifelement.style.visibility = "hidden";
      }
    }
  );
});
