if(typeof Sentry !== 'undefined'){
	Sentry.init({ dsn: 'https://f0b7bf8864764e3cb9d296b11ceba12d@sentry.io/301844' });
	window.playht__SetSentryContext = function(context) {
		Sentry.configureScope((scope) => {
			Object.keys(context).forEach(key => scope.setExtra(key, context[key]));
		});
		context.userId && Sentry.setUser({"userId": context.userId});
	}
}
