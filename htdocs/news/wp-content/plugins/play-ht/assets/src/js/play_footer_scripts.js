(function (w, $, undefined) {
  if (typeof $ !== "undefined") {
    $(document).ready(function () {
      if ($(".playht-iframe-player:visible") && $(".playht-iframe-player:visible").width() < 600) {
        $(".playht-iframe-player").attr("height", 210);
      }
    });
  }
})(window, jQuery);
