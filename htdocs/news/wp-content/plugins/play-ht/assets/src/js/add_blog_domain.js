
(function ( w, $, undefined )
{
	$ ( function ()
	{
		var domain_obj = {
			userId: wppp_blog.user_id,
			appId : wppp_blog.app_id,
			origin: wppp_blog.blog
		};
		$.ajax( {
			type   : 'POST',
			url    : wppp_blog.ajax_url,
			contentType: 'application/json; charset=utf-8',
			dataType   : "json",
			data   : JSON.stringify ( domain_obj ),
		})
		;
	} );
}) ( window, jQuery );