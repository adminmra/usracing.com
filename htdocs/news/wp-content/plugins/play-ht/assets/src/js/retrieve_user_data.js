
(function ( w, $, undefined )
{
	$ ( function ()
	{
		$("#welcome_msg__loader").show();
		function updatePageView(userData) {
			var __play_packages_credits = {
				"wp-personal-monthly" : {
					type: "monthly",
					allowance: "10",
				},
				"wp-professional-monthly" : {
					type: "monthly",
					allowance: "35",
				},
				"wp-premium-monthly" : {
					type: "monthly",
					allowance: "100",
				},
				"wp-personal-yearly" : {
					type: "yearly",
					allowance: "120",
				},
				"wp-professional-yearly" : {
					type: "yearly",
					allowance: "420",
				},
				"wp-premium-yearly" : {
					type: "yearly",
					allowance: "1200",
				},
				"writer-yearly": {
					type: "yearly",
					allowance: "120",
				},
				"writer-monthly": {
					type: "monthly",
					allowance: "10",
				},
				"publication-yearly": {
					type: "yearly",
					allowance: "420",
				},
				"revised-starter-monthly": {
					type: "monthly",
					allowance: "10",
				},
				"publication-monthly": {
					type: "monthly",
					allowance: "35",
				}
			}

			if(userData && userData.packages) {
				var package_name = "";
				var package_key = "";
				Object.keys(userData.packages).map(function(key){
					package_name = userData.packages[key].name;
					package_key = key;
				});

				var total_credits = __play_packages_credits[package_key] && (__play_packages_credits[package_key].allowance * 2000);
				$("#__play_package_type, #__play_total_credits").show();
				if($("#__play_package_type").text() && (!$("#__play_package_type").text().indexOf("Play") > -1 && !$("#__play_package_type").text().indexOf("WP ") > -1)){
					!!package_name && $("#__play_package_type").html(package_name);	
				}
				!!total_credits && $("#__play_total_credits").html(total_credits);
			}

			if((userData && !userData.is_pro) || (userData && !userData.packages) ){
				$("#__play_package_type").html("Free Package");
				$("#__play_total_credits").html("0");
			}

			// var __play_userdata_saved = getCookie("__play_userdata_saved");
			if(userData){
				!!userData.imageUrl && $("#__play_profile_pic").attr("src", userData.imageUrl);
				!!userData.name && $(".user-div .welcome_msg #user_id").html(userData.name);
				!!userData.usage && $(".user-div .remaining_credits").html(userData.usage.words_count);
			}
			
			$("#welcome_msg__loader").hide();
		}

		wppp_retrieve && wppp_retrieve.user_id && firebase.database().ref( 'users' ).child( wppp_retrieve.user_id ).once('value', function ( data )
		{
			if ( data.val () !== null ) {

				var userData = data.val();
				updatePageView(userData);
				jQuery.ajax ( {
					type: 'POST',
					url : wppp_retrieve.ajax_url,
					data: {
						'userData': userData,
						'action'  : 'retrieve_user_data'
					},
					success: function() {
						setCookie("__play_userdata_saved", true, 300);
					}
				} );
			} else {
				window.playht__SetSentryContext({
					userId: wppp_retrieve.user_id,
					website: window.location.href,
				});
				window.Sentry && Sentry.captureException(new Error('No user data in dashboard for user id:' + wppp_retrieve.user_id));
			}
		} );

	} );
}) ( window, jQuery );