var PLAYHT__VOICES_NAMES = {
	'Amy':"Amy (en-UK)",
	'Astrid':"Astrid (sv-Swedish)",
	'Brian':"Brian (en-UK)",
	'Carla':"Carla (it-Italian)",
	'Carmen':"Carmen (ro-Romanian)",
	'Celine':"Celine (fr-French)",
	'Chantal':"Chantal (fr-Canadian French)",
	'Conchita':"Conchita (es-Spanish - Castilian)",
	'Cristiano':"Cristiano (pt-European Portuguese)",
	'Dora':"Dora (is-Icelandic)",
	'Emma':"Emma (en-UK)",
	'Enrique':"Enrique (es-Spanish - Castilian)",
	'Ewa':"Ewa (pl-Polish)",
	'Filiz':"Filiz (tr-Turkish)",
	'Geraint':"Geraint (en-Welsh)",
	'Giorgio':"Giorgio (it-Italian)",
	'Gwyneth':"Gwyneth (cy-Welsh)",
	'Hans':"Hans (de-German)",
	'Ines':"Ines (pt-European Portuguese)",
	'Jacek':"Jacek (pl-Polish)",
	'Jan':"Jan (pl-Polish)",
	'Joanna':"Joanna (en-US)",
	'Joey':"Joey (en-US)",
	'Karl':"Karl (is-Icelandic)",
	'Liv':"Karl (nb-Norwegian)",
	'Lotte':"Lotte (nl-Dutch)",
	'Mads':"Mads (da-Danish)",
	'Maja':"Maja (pl-Polish)",
	'Marlene':"Marlene (de-German)",
	'Mathieu':"Mathieu (fr-French)",
	'Matthew':"Matthew (en-US)",
	'Maxim':"Maxim (ru-Russian)",
	'Mizuki':"Mizuki (jp-Japanese)",
	'Naja':"Naja (da-Danish)",
	'Nicole':"Nicole (en-Australian)",
	'Raveena':"Raveena (en-India)",
	'Ricardo':"Ricardo (pt-Brazilian Portuguese)",
	'Ruben':"Ruben (nl-Dutch)",
	'Russell':"Russell (en-Australian)",
	'Salli':"Salli (en-US)",
	'Tatyana':"Tatyana (ru-Russian)",
	'Vicki':"Vicki (de-German)",
	'Vitoria':"Vitoria (pt-Brazilian Portuguese)",
	'de-DE_BirgitVoice':"Birgit (de-German)",
	'de-DE_DieterVoice':"Dieter (de-German)",
	'en-GB_KateVoice':"Kate (en-UK)",
	'en-US_AllisonVoice':"Samantha (en-US)",
	'en-US_LisaVoice':"Lisa (en-US)",
	'en-US_MichaelVoice':"Michael (en-US)",
	'es-ES_EnriqueVoice':"Diego (es-Spanish - Castilian)",
	'es-ES_LauraVoice':"Laura (es-Spanish - Castilian)",
	'fr-FR_ReneeVoice':"Renee (fr-French)",
	'it-IT_FrancescaVoice':"Francesca (it-Italian)",
	'ja-JP_EmiVoice':"Emi (jp-Japanese)",
	'pt-BR_IsabelaVoice':"Isabela (pt-Brazilian Portuguese)"
}

jQuery(function ($) {
	if(!window.wppp_retrieve) return;
		var userid = window.wppp_retrieve.user_id;
    var username = window.location.host;
    var paid_user = false;
    var words = 0;
		window.playht__SetSentryContext({
			website: window.location.href,
			userId: userid,
			username: username,
		})

    $("[data-onclick='tab']").on('click', function(e){
      $("[data-onclick='tab']").removeClass("active")
      e.target.classList.add("active")
      $(".play-dashboard-tab").hide();
      $("#play-dashboard-tab-"+e.target.dataset.tab).show()

			window.__playht__analytics && window.__playht__analytics('playht__analytics.send', 'event', 'click', 'dashboard_tab_'+e.target.dataset.tabname, userid);
    });

var fetchDashboardInfo = function(user){
		var enableDownload = false;
		var enableSubscription = false;
		var whitelabel = false;		
		var color = '19bc9c';
		var title = 'Listen to this article';
		var message = 'Powered by Play.ht';
		var url = '';
		var twitterMessage = "Listening to {article_title} by {author_name} via @play_ht - convert your articles to audio. Visit {article_url}";
		var facebookMessage = "Listening to {article_title} by {author_name} via https://play.ht/ - convert your articles to audio. Visit {article_url}";
		if (user.epconf) {
			if (user.epconf.whitelabel) {whitelabel = user.epconf.whitelabel}
			if (user.epconf.download) {enableDownload = user.epconf.download}
			if (user.epconf.subscription) {enableSubscription = user.epconf.subscription}
			if (user.epconf.color) {color = user.epconf.color}
			if (user.epconf.title) {title = user.epconf.title}
			if (user.epconf.message) {message = user.epconf.message}
			if (user.epconf.url) {url = user.epconf.url}
			if(user.epconf.twitterMessage) {twitterMessage = user.epconf.twitterMessage}
			if(user.epconf.facebookMessage) {facebookMessage = user.epconf.facebookMessage}

		}

		if (typeof user.packages != 'undefined' && user.packages != null && Object.keys(user.packages).length > 0) {
			paid_user = true;
		}

		// to return to the old design, just remove the minimal class below, beside 'player-shadow'
		// and change the marketing massage input value back to 'Create Audio Narrations with Play.ht'
		$(".js-play-dashboard-tab-6")[0].innerHTML = `
		<div class="player-shadow minimal">
			<div class="_tophead alter-color" style="color: #`+color+`;">
				<div class="_l" id="shadow-player-title">`+title+`</div>
				<div class="_r" id="shadow-player-marketing-message" style="`+(whitelabel ? 'display:none;' : '')+`">`+(user.epconf && user.epconf.message ? message : 'Powered by <span>Play.ht</span>')+`</div>
			</div>
			<div class="_head">
				<div class="_content">
					<div class="_img">
					</div>
					<div class="_meta">
						<div class="_t">
						</div>
						<div class="_i">
							<div class="_i1">
							</div>
							<div class="_i2">
							</div>
						</div>
						<div class="_c">
							<div class="_p alter-background-color" style="background: #`+color+`;">
								<img src="https://play.ht/img/play.png">	
							</div>
							<img class="_w" src="https://play.ht/img/waveform.svg">
						</div>
					</div>
				</div>
				<div class="_ctrl">
					<div class="_s" id="shadow-player-subscription" style="`+(enableSubscription && paid_user ? '' : 'display:none;')+`">
						<div class="_input alter-border-color" style="border-color: #`+color+`;">Enter your email to get my next article as audio</div>
						<div class="_btn alter-background-color" style="background: #`+color+`;">Subscribe</div>
					</div>
					<div class="_bts">
						<div class="_b alter-color alter-border-color" style="border-color: #`+color+`; color: #`+color+`; `+(enableDownload ? '' : 'display:none;')+`" id="shadow-player-download-button">Download</div>
						<div class="_b alter-color alter-border-color" style="border-color: #`+color+`; color: #`+color+`;">Speed</div>
						<div class="_b _bs alter-border-color" style="border-color: #`+color+`;">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 612 612" style="enable-background:new 0 0 612 612;" xml:space="preserve" width="16px" height="16px" data-reactid="87"><g data-reactid="88"><g data-reactid="89"><path style="fill: #`+color+`;" class="alter-fill-color" d="M612,116.258c-22.525,9.981-46.694,16.75-72.088,19.772c25.929-15.527,45.777-40.155,55.184-69.411 c-24.322,14.379-51.169,24.82-79.775,30.48c-22.907-24.437-55.49-39.658-91.63-39.658c-69.334,0-125.551,56.217-125.551,125.513 c0,9.828,1.109,19.427,3.251,28.606C197.065,206.32,104.556,156.337,42.641,80.386c-10.823,18.51-16.98,40.078-16.98,63.101 c0,43.559,22.181,81.993,55.835,104.479c-20.575-0.688-39.926-6.348-56.867-15.756v1.568c0,60.806,43.291,111.554,100.693,123.104 c-10.517,2.83-21.607,4.398-33.08,4.398c-8.107,0-15.947-0.803-23.634-2.333c15.985,49.907,62.336,86.199,117.253,87.194 c-42.947,33.654-97.099,53.655-155.916,53.655c-10.134,0-20.116-0.612-29.944-1.721c55.567,35.681,121.536,56.485,192.438,56.485 c230.948,0,357.188-191.291,357.188-357.188l-0.421-16.253C573.872,163.526,595.211,141.422,612,116.258z" data-reactid="90"></path></g></g><g data-reactid="91"></g><g data-reactid="92"></g><g data-reactid="93"></g><g data-reactid="94"></g><g data-reactid="95"></g><g data-reactid="96"></g><g data-reactid="97"></g><g data-reactid="98"></g><g data-reactid="99"></g><g data-reactid="100"></g><g data-reactid="101"></g><g data-reactid="102"></g><g data-reactid="103"></g><g data-reactid="104"></g><g data-reactid="105"></g></svg>
						</div>
						<div class="_b _bs alter-border-color" style="border-color: #`+color+`;">
							<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 96.124 96.123" style="enable-background:new 0 0 96.124 96.123;" xml:space="preserve" data-reactid="68"><g data-reactid="69"><path style="fill: #`+color+`;" class="alter-fill-color" d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z" data-reactid="70"></path></g><g data-reactid="71"></g><g data-reactid="72"></g><g data-reactid="73"></g><g data-reactid="74"></g><g data-reactid="75"></g><g data-reactid="76"></g><g data-reactid="77"></g><g data-reactid="78"></g><g data-reactid="79"></g><g data-reactid="80"></g><g data-reactid="81"></g><g data-reactid="82"></g><g data-reactid="83"></g><g data-reactid="84"></g><g data-reactid="85"></g></svg>
						</div>
					</div>
				</div>
			</div>
			<div class="_footer">
				<div id="shadow-player-marketing-message" class="alter-color" style="color: #`+color+`; `+(whitelabel ? 'display:none;' : '')+`">Create Audio Narrations with <span>Play.ht</span></div>
			</div>
		</div>
		<div class="conf-wrapper">
			<div class="_conf">
				<div class="radio">
					<input id="play-dashboard-conf-download-checkbox" type="checkbox" data-onclick="playht__toggleDownloadButton" name="download" `+ (enableDownload ? ' checked': '') +`>
					<label class="radio-label">Enable Download Button</label>
				</div>
				<div class="_desc">Adds a Download button on the Audio Player so users can download your article as an audio file.</div>
			</div>
			<div class="_conf">
				<div class="radio">
					<input id="play-dashboard-conf-subscription-checkbox" type="checkbox" data-onclick="playht__toggleSubscription" name="subscription" `+ (enableSubscription && paid_user ? ' checked': '')+`>
					<label class="radio-label">Enable Email Subscriptions</label>
				</div>
				<div class="_desc">Allow readers to sign up for future audio articles.</div>
			</div>
			<div class="_conf">
				<div class="radio">
					<input id="play-dashboard-conf-whitelabel-checkbox" type="checkbox" data-onclick="playht__toggleMarketingMessage" name="whitelabel" `+ (whitelabel && paid_user ? ' checked': '')+`>
					<label class="radio-label">Whitelabel Player</label>
				</div>
				<div class="_desc">Removes Play.ht marketing message from the Audio Player.</div>
			</div>
			<div class="_conf">
				<div class="_title">Customize The Title</div>
				<div class="_desc" style="margin-top: 3px;">Title: <input id="dashboard-player-title-input" value="`+title+`"></div>
			</div>
			<div class="_conf">
				<div class="_title">Add Custom Marketing Message</div>
				<div class="_desc" style="margin-top: 3px;">Message: <input id="dashboard-player-marketing-message-input" value="`+message+`"></div>
			</div>
			<div class="_conf">
				<div class="_title">Add Custom Marketing Message URL</div>
				<div class="_desc" style="margin-top: 3px;">URL: <input id="dashboard-player-marketing-message-url-input" value="`+url+`"></div>
			</div>
			<div class="_conf">
				<div class="_title">Share on twitter message</div>
				<div class="_desc" style="margin-top: 3px;">message: <input id="dashboard-player-twitter-message-input" value="`+twitterMessage+`"></div>
			</div>
			<div class="_conf">
				<div class="_title">Share on facebook message</div>
				<div class="_desc" style="margin-top: 3px;">message: <input id="dashboard-player-facebook-message-input" value="`+facebookMessage+`"></div>
			</div>
			<div class="_conf">
				<div class="_title">Change Player Color</div>
				<div class="_desc" style="margin-top: 3px;">Color: <input id="dashboard-color-picker" class="_color-picker" value="`+color+`"></div>
			</div>
		</div>
		<div style="width: 100%;left: 0;background: #fff;padding-top: 20px;-webkit-box-sizing: border-box;-moz-box-sizing: border-box;box-sizing: border-box;border-top: 1px solid #ddd;">
			<button id="play-dashboard-conf-save-btn" class="_save-btn medium-button medium-button--primary medium-button--filled medium-button--withChrome u-accentColor-- buttonNormal is-touched" type="button" data-onclick="playht__saveEPConf">Save Changes</button>
			<span id="play-dashboard-conf-save-msg" style="display: none;">Saved!</span>	
		</div>
		`

		if(!paid_user){
			$("#play-dashboard-conf-save-btn").text("Upgrade to save")
		}

			document.getElementById('dashboard-color-picker').style.cursor = 'pointer';
			document.getElementById('dashboard-color-picker').disabled = false;
			// http://jscolor.com/
			playht__jscolor && (new playht__jscolor(document.getElementById('dashboard-color-picker')));
			document.getElementById('dashboard-color-picker').onchange = function(e){
				var color = e.target.value
				var elms = document.getElementsByClassName('alter-background-color')
				for(var i=0; i < elms.length; i++){
					elms[i].style.background = '#' + color
				}

				elms = document.getElementsByClassName('alter-color')
				for(var i=0; i < elms.length; i++){
					elms[i].style.color = '#' + color
				}

				elms = document.getElementsByClassName('alter-border-color')
				for(var i=0; i < elms.length; i++){
					elms[i].style.borderColor = '#' + color
				}

				elms = document.getElementsByClassName('alter-fill-color')
				for(var i=0; i < elms.length; i++){
					elms[i].style.fill = '#' + color
				}

				$("#play-dashboard-conf-save-btn")[0].disabled = false;
			}

			document.getElementById('dashboard-player-marketing-message-input').onkeyup = function(e){
				$("#shadow-player-marketing-message").text(e.target.value)
				$("#play-dashboard-conf-save-btn")[0].disabled = false;
			}

			document.getElementById('dashboard-player-title-input').onkeyup = function(e){
				$("#shadow-player-title").text(e.target.value)
				$("#play-dashboard-conf-save-btn")[0].disabled = false;
			}

		$("[data-onclick='playht__toggleMarketingMessage']").on('click', function(e){
			var m = $("#shadow-player-marketing-message")
			if(e.target.checked){
				m.hide()
			}else{
				m.show()
			}

			$("#play-dashboard-conf-save-btn")[0].disabled = false;
		})

		$("[data-onclick='playht__toggleDownloadButton']").on('click', function(e){
			var m = $("#shadow-player-download-button")
			if(e.target.checked){
				m.show()
			}else{
				m.hide()
			}

			$("#play-dashboard-conf-save-btn")[0].disabled = false;
		})

		$("[data-onclick='playht__toggleSubscription']").on('click', function(e){
			var m = $("#shadow-player-subscription")
			if(e.target.checked){
				m.show()
			}else{
				m.hide()
			}

			$("#play-dashboard-conf-save-btn")[0].disabled = false;
		})

		$("[data-onclick='playht__saveEPConf']").on('click', function(e){

			if(!paid_user){
				var url = "https://play.ht/wordpress/upgrade/";
				$("<a>").attr("href", url).attr("target", "_blank")[0].click();
				window.__playht__analytics && window.__playht__analytics('playTracker.send', 'event', "click", 'dashboard_customization_upgrade_clicked', username, {'dimension2': `${userid} - ${username}`});
				return;
			}

			window.__playht__analytics && window.__playht__analytics('playTracker.send', 'event', "click", 'dashboard_customization_save_clicked', username, {'dimension2': `${userid} - ${username}`});

			$("#play-dashboard-conf-save-btn")[0].disabled = true;
			$("#play-dashboard-conf-save-btn").text("Saving...");
			$.post("https://play.ht/api/saveEPConf", {
				userId: userid, 
				whitelabel: $("#play-dashboard-conf-whitelabel-checkbox")[0].checked,
				download: $("#play-dashboard-conf-download-checkbox")[0].checked,
				subscription: $("#play-dashboard-conf-subscription-checkbox")[0].checked,
				color: document.getElementById('dashboard-color-picker').value,
				title:  document.getElementById('dashboard-player-title-input').value == title ? '' : document.getElementById('dashboard-player-title-input').value,
				message: document.getElementById('dashboard-player-marketing-message-input').value == message ? '': document.getElementById('dashboard-player-marketing-message-input').value,
				url: document.getElementById('dashboard-player-marketing-message-url-input').value,
				twitterMessage: document.getElementById('dashboard-player-twitter-message-input').value,
				facebookMessage: document.getElementById('dashboard-player-facebook-message-input').value,
			}, function(error){
				if(!error) {
					$("#play-dashboard-conf-save-msg").show().css("color", "#21b67e").text("Settings have been Saved Successfully")
					setTimeout(function(){
						$("#play-dashboard-conf-save-msg").hide()
					}, 3000);
				}else{
					$("#play-dashboard-conf-save-msg").show().css("color", "red").text("Saving UI settings failed, please try again later or contact us to help.");
					setTimeout(function(){
						$("#play-dashboard-conf-save-msg").hide();
					}, 3000);
					window.playht__SetSentryContext({
						userId: userid, 
						url: document.getElementById('dashboard-player-marketing-message-url-input').value
					});
					window.Sentry && Sentry.captureException(error);
				}

				$("#play-dashboard-conf-save-btn")[0].disabled = false;
				$("#play-dashboard-conf-save-btn").text("Save Changes");
			})
		})

		if(typeof user.usage !== 'undefined' && typeof user.usage.words_count !== 'undefined'){
        	words += Number(user.usage.words_count)
		}

		if(typeof user.usage !== 'undefined' && typeof user.usage.appsumo_words !== 'undefined'){
        	words += Number(user.usage.appsumo_words)
		}

		if (paid_user) {
			// paid account
			var content = ""
			var keys = Object.keys(user.packages)
			for(var i = 0; i < keys.length; i++){
				var p = user.packages[keys[i]]
				content = p.name ? content+"<strong class='_package_meta'>Package Name</strong><h3 class='_package_name'>"+p.name+"</h3><br />" : content;
			}

			var h = `
				<div class="_account_type">
					<div class="_title">Account Type</div>
					<div class="_meta">PAID</div>
					<div class="_meta_credits">Remaining Words: `+words+`</div>
				</div>
				<div class="_packages">
					<div id="packages">`+content+`</div>
				</div>
				<div class="_logout">
					<div class="_title">Logged In as</div>
					<div class="_meta">`+(user.email || user.name)+`</div>
					<button class="button button-primary"><a href="https://play.ht/wordpress/login/?logout=1&backurl=`+ window.location.href +`">Login to another account</a></button>
				</div>
			`
			$("#play-dashboard-tab-2").html(h)

		}else{
			$('.playHtsubmit_button_area').html('<button class="_save-btn medium-button medium-button--primary medium-button--filled medium-button--withChrome u-accentColor-- buttonNormal is-touched" type="button" data-onclick="playht__saveEPConf"><a href="https://play.ht/wordpress/upgrade/" target="_blank">Upgrade to save</a></button>')

			var h = `
				<div class="_account_type">
					<div class="_title">Account Type</div>
					<div class="_meta">FREE</div>
					<div class="_meta_credits">Remaining Words: `+words+`</div>
					<div class="_act">
						<a data-onclick="accountUpgrade" target="_blank" class="medium-button medium-button--primary medium-button--withChrome u-accentColor--buttonNormal" style="vertical-align: middle;font-size: 14px;" href="https://play.ht/wordpress/upgrade/">
							Upgrade Account
						</a>
					</div>
				</div>
				<div class="_logout">
					<div class="_title">Logged In as</div>
					<div class="_meta">`+(user.email || user.name)+`</div>
					<button class="button button-primary"><a href="https://play.ht/wordpress/login/?logout=1&backurl=`+ window.location.href +`">Login to another account</a></button>
				</div>
			
				<div class="_account_missing">
					<div class="_title">You are missing out on these great features that are included in the paid package:</div>
					<div class="_meta">
						<div class="_meta_t">Premium voices</div>
						<div class="_meta_m">More natural and human like voices that offer increased engagment to listeners.</div>
						<div class="_meta_t">Stats</div>
						<div class="_meta_m">Analytics that show you the number of people listening to your articles.</div>
						<div class="_meta_t">Audio Player Customizations</div>
						<div class="_meta_m">Change the look and feel of the Audio Player, including <strong>white-labeling</strong> the player.</div>
					</div>
					<div class="_act">
						<a style="font-size: 14px;" target="_blank" data-onclick="accountUpgrade" class="medium-button medium-button--primary medium-button--filled medium-button--withChrome u-accentColor-- buttonNormal is-touched" href="https://play.ht/wordpress/upgrade/">
						Upgrade Account
						</a>
						<div class="_act_m">Pricing starts from $9/month. You can cancel anytime.</div>
					</div>
				</div>
			`

			$("#play-dashboard-tab-2").html(h)

			$("[data-onclick='accountUpgrade']").on('click', function(e){
				window.__playht__analytics && window.__playht__analytics('playTracker.send', 'event', "click", 'dashboard_account_upgrade_clicked', username, {'dimension2': `${userid} - ${username}`});
			})
		}

		var subscribers_start;
		var subscribers_end;
		var fetchSubscribersStats = function(){
			var chartXLables = []
			var subscribersData = []
			var subscribersCount = 0
			// var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
			var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
			
			if (typeof subscribers_start != 'undefined') {
				$("#dashboard-subscribers-stats-loader").show();
			}

			var timeframe = [subscribers_start, subscribers_end]
			if (typeof subscribers_start == 'undefined') {
				var now = (new Date()).toISOString().split('.')[0] + "Z"
				var bck = new Date()
				bck.setDate(bck.getDate() - 13)
				bck = bck.toISOString().split('.')[0] + "Z"
				timeframe = [bck, now]
			}

			var dur = new Date(timeframe[1]) - new Date(timeframe[0])
			var daysDiff = Math.round(dur / (1000 * 60 * 60 * 24))

			chartXLables = []
			var d = new Date(timeframe[0])
			chartXLables.push((d.getDate()).toString() + "-" + months[d.getMonth()]) // days[d.getDay()] + " " +
			for (var i = 1; i <= daysDiff; i++) {
				d.setDate(d.getDate() + 1)
				chartXLables.push((d.getDate()).toString() + "-" + months[d.getMonth()]) // days[d.getDay()] + " " +
			}
		
			subscribersData = []
			subscribersCount = 0

			var from = timeframe[0].split("T")[0]
			var to = timeframe[1].split("T")[0]
			if (subscribers_start) {
				from = new Date(timeframe[0])
				from.setDate(from.getDate() + 1)
				from = from.toISOString().split("T")[0]

				to = new Date(timeframe[1])
				to.setDate(to.getDate() + 1)
				to = to.toISOString().split("T")[0]
			}
			if (!user.appId) {
				return
			}

			var _doneChart = function(){
				if (subscribersData.length == 0) {return;}
				$(".sub-stats-head").show();
				$(".sub-stats-filter").show();
				$("#dashboard-subscribers-stats-loader").hide();
				Highcharts.chart('dashboard-subscribers-stats', {
					chart: {
						type: 'column',
						height: 280,
						width: $(".dashboard-container").width() - ($(".dashboard-container").width()* 0.1)
					},
					title: {
						text: ''
					},
					legend:{
						enabled: false
					},
					xAxis: {
						categories: chartXLables
					},
					yAxis: {
						title: ''
					},
					series: [{
							name: 'Subscribers',
							data: subscribersData
						}
					]
				});
			}

			var url = 'https://a.play.ht/subscribtions/' + (articleid ? "/article" : "") + "?app_id=" + user.appId + "&created_at__gte=" + from + "&created_at__lte=" + to + (articleid ? "&article_id=" + articleid : "") 
			fetch(url)
			.then(function(response){
					response.text().then(function(res){
						if (res.aggregations && res.aggregations.counts && res.aggregations.counts.buckets && res.aggregations.counts.buckets.length > 0) {
							var data = res.aggregations.counts.buckets;
							for (var i = 0; i < data.length; i++) {
								var count = data[i].doc_count
								subscribersData.push(count)
								subscribersCount = subscribersCount + count
							}
							_doneChart()
						} else {
							for (var i = 0; i < chartXLables.length; i++) {
								subscribersData.push(0)
							}
							_doneChart()
						}
					})
			})
			.catch(function(err){
				window.playht__SetSentryContext({
					userId: userid,
					articleId: articleid,
				});
				window.Sentry && Sentry.captureException(err);
				for (var i = 0; i < chartXLables.length; i++) {
					subscribersData.push(0)
				}
				_doneChart()
			})

			var count = 0;
			if(typeof subscribers_start == 'undefined'){
				firebase.database().ref('subscribers').child(user.id).once('value').then(function(snapshot){
					var data = snapshot.val();
		
					const rows = [
						["name", "email", "date", "raw date"]
					];
					let csvContent = "data:text/csv;charset=utf-8,";
					var tb = "<tbody>"
					if (data !== null) {
						var keys = Object.keys(data)
						if (data !== null) {
							for (var i = 0; i < keys.length; i++) {
								var obj = data[keys[i]]
								if (typeof obj != "object") {
									continue;
								}
								count++
								tb = tb + "<tr><td>" + obj.name + "</td>" + "<td>" + obj.email + "</td>" + "<td>" + playht__getDate(obj.created_at) + "</td></tr>"
								rows.push([obj.name, obj.email, playht__getDate(obj.created_at), obj.created_at])
							}
							rows.forEach(function (rowArray) {
								let row = rowArray.join(",");
								csvContent += row + "\r\n";
							});
						}
					}
					tb = tb + "</tbody>"

					if(count == 0){
						$("#dashboard-subscribers-stats-loader").hide();
						$("#zero-sub-modal").show()
					}
		
					var h = `
						<div id="_subscribers">
							<div id="_subscribers_info_head"><span>Name</span><span>Email</span><span>Subscription date</span></div>
							<table id="_subscribers_info">
							` + tb + `</table>
							<a id="_subscribers-download" ` + (count == 0 ? 'style="display:none;"' : "") + ` data-onclick="downloadSubscribers" href="#" class="button button--withChrome u-accentColor--buttonNormal">CSV Export</a>
						</div>
					`
					$("#dashboard-subscribers-info-wrapper").html(h)
					$("#play-dash-sub-count").text(count)
		
					$("[data-onclick='downloadSubscribers']").on('click', function (e) {
						var encodedUri = encodeURI(csvContent);
						var link = document.createElement("a");
						link.setAttribute("href", encodedUri);
						link.setAttribute("download", "subscribers.csv");
						document.body.appendChild(link);
						link.click();
					})
				})
			}


			$("#play-dashboard-sub-stats-date-picker-from").datepicker({
				dateFormat: "DD, dd MM yy",
				autoSize: true,
				onSelect: function (d) {
					var min = new Date(Date.parse(d))
					$("#play-dashboard-sub-stats-date-picker-to").datepicker("destroy");
					$("#play-dashboard-sub-stats-date-picker-to").datepicker({
						minDate: min,
						autoSize: true,
						dateFormat: "DD, dd MM yy",
						onSelect: function (d2) {
							var max = new Date(Date.parse(d2))

							var dayBefore = max;
							dayBefore.setDate(dayBefore.getDate() - 1);
							$("#play-dashboard-sub-stats-date-picker-from").datepicker("option", "maxDate", dayBefore);

							// when converting from js date to ISO-8601 (which required by keen)
							// a date like "Sun Apr 01 2018 00:00:00 GMT+0200 (EET)"
							// becomes "2018-03-31T22:00:00.000Z"
							// so keen return the day after what we want too
							min.setHours(2)

							// to include all the end day
							max.setHours(25)
							max.setMinutes(59)
							max.setSeconds(59)


							subscribers_start = min.toISOString().split('.')[0] + "Z"
							subscribers_end = max.toISOString().split('.')[0] + "Z"

							fetchSubscribersStats()
						}
					});

					var to = $("#play-dashboard-sub-stats-date-picker-to").datepicker("getDate");
					if (to == null) {
						var today = new Date()

						var yesterday = today;
						yesterday.setDate(yesterday.getDate() - 1);
						$("#play-dashboard-sub-stats-date-picker-from").datepicker("option", "maxDate", yesterday);

						today.setHours(25)
						today.setMinutes(59)
						today.setSeconds(59)
						$("#play-dashboard-sub-stats-date-picker-to").datepicker("setDate", today);

						subscribers_start = min.toISOString().split('.')[0] + "Z"
						subscribers_end = today.toISOString().split('.')[0] + "Z"
						fetchSubscribersStats()
					} else {
						min.setHours(2)
						to.setHours(25)
						to.setMinutes(59)
						to.setSeconds(59)

						subscribers_start = min.toISOString().split('.')[0] + "Z"
						subscribers_end = to.toISOString().split('.')[0] + "Z"

						fetchSubscribersStats()
					}

					$("#play-dashboard-sub-stats-date-picker-to").datepicker("option", "disabled", false);
				}
			});
			$("#play-dashboard-sub-stats-date-picker-to").datepicker({
				dateFormat: "DD, dd MM yy",
				autoSize: true
			});
			$("#play-dashboard-sub-stats-date-picker-to").datepicker("option", "disabled", true);
			
		}

		fetchSubscribersStats();

		var start;
		var end;
		var articleid = "";
		var articleVoice = "";
		var articleTitle = "";
		var chartYAxisTickMax = 0;

		var chartXLables = []

		var listensData = []
		var listensCount = 0
		
		var sharesData = []
		var sharesCount = 0
		
		var downloadsData = []
		var downloadsCount = 0
		
		var days = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

		var setXLables = function(events, interval){
			chartXLables = []
			for(var i = 0; i < events.length; i++){
				var d = new Date(events[i].timeframe.start)
				if (interval == 'monthly') {
					chartXLables.push(months[d.getMonth()])
				}else{
					chartXLables.push(days[d.getDay()] + " " + (d.getDate()).toString() + "-" +  months[d.getMonth()])
				}
			}
		}

		var doneChart = function(){
			if (listensData.length == 0 ||
				sharesData.length == 0 ||
				downloadsData.length == 0 
				) {return;}

			firebase.database().ref('articleData').orderByChild('userId').equalTo(user.id).once('value').then(function(snapshot){
				var data = snapshot.val();
				var ul = ""
				var keys = Object.keys(data)
				if (data !== null) {
					ul = "<ul>"
					ul = ul + `
						<li data-onclick="resetStats" data-title="Show stats for a specific article" data-id="">
							<div class="_art" data-onclick="resetStats" data-title="Show stats for a specific article" data-id="">
								<div class="_tt" data-onclick="resetStats" data-title="Show stats for a specific article" data-id="">All articles</div>
								<div class="_vc" data-onclick="resetStats" data-title="Show stats for a specific article" data-id="">Show stats for all articles</div>
							</div>
						</li>
					`
					for(var i = 0; i < keys.length; i++){
						var v = data[keys[i]].voice
						if (typeof v == 'undefined' || v == '' || v == null || typeof PLAYHT__VOICES_NAMES[v] == 'undefined') {
							v = "..."
						}

						var t = data[keys[i]].title


						ul = ul + `
							<li data-onclick="resetStats" data-voice="`+PLAYHT__VOICES_NAMES[v]+`" data-title="`+t+`" data-id="`+data[keys[i]].id+`">
								<div class="_art" data-voice="`+PLAYHT__VOICES_NAMES[v]+`" data-onclick="resetStats" data-title="`+t+`" data-id="`+data[keys[i]].id+`">
									<div class="_tt" data-voice="`+PLAYHT__VOICES_NAMES[v]+`" data-onclick="resetStats" data-title="`+t+`" data-id="`+data[keys[i]].id+`">`+t+`</div>
									<div class="_vc" data-voice="`+PLAYHT__VOICES_NAMES[v]+`" data-onclick="resetStats" data-title="`+t+`" data-id="`+data[keys[i]].id+`">`+PLAYHT__VOICES_NAMES[v]+`</div>
									<div class="_ul" data-voice="`+PLAYHT__VOICES_NAMES[v]+`" data-onclick="resetStats" data-title="`+t+`" data-id="`+data[keys[i]].id+`">`+data[keys[i]].url+`</div>
								</div>
							</li>
						`							
					}
					ul = ul + "</ul>"
				}


				var content = `
					<div class="jq-dropdown-panel" style="padding: 0;max-height: 300px;overflow-y: auto;">
						<div class="play-dropdown-title">Show stats for a specific article</div>
						`+ul+`	
					</div>
				`

				$("#jq-dropdown-stats-article").html(content)


				$("[data-onclick='resetStats']").on('click', function(e){
					if (e.target.dataset.id == articleid) {
						$("._article-selector")[0].click();
						return;
					}

					$("._article-selector")[0].innerText = e.target.dataset.title;

					// hide menu after selecting
					$("._article-selector")[0].click();

					articleid = e.target.dataset.id;

					articleTitle = e.target.dataset.title;

					articleVoice = e.target.dataset.voice;

					fetchStats();

					$("#dashboard-stats-loader").show();
				})
				
				_doneChart();
			})
		}

		var _doneChart = function(){
			$("#dashboard-stats-loader").hide();

			if (listensData.length == 0 ||
				sharesData.length == 0 ||
				downloadsData.length == 0 
				) {return;}

			$("#play-dashboard-tab-3 span").remove()
			$("#date-selectors-wrapper").show();

			Highcharts.chart('dashboard-stats', {
				chart: {
					type: 'column',
					height: 350,
					width: $(".dashboard-container").width() - ($(".dashboard-container").width()* 0.1)
				},
				title: {
					text: ''
				},
				legend:{
					enabled: false
				},
				xAxis: {
					categories: chartXLables
				},
				yAxis: {
					title: ''
				},
				series: [{
						name: 'Listens',
						data: listensData
					}, {
						name: 'Shares',
						data: sharesData
					}, {
						name: 'Downloads',
						data: downloadsData
					}
				]
			});
			
			var div = document.createElement("div")
			div.classList.add("total-counts")
			var t = `<div class="_title">
					Showing analytics for all audio articles
				</div>`
			if (articleid) {
				t = `<div class="_title">
				`+articleTitle+`
				<div class="_voice">Voice: `+articleVoice+`</div>
			</div>`
			}

			div.innerHTML = t + `
				<div class="_count_box_wrapper">
					<div class="_count_box">
						<p class="_count">`+listensCount+`</p>
						<p class="_word">Listens</p>
						<p class="_point _point_listens"></p>
					</div>
					<div class="_count_box">
						<p class="_count">`+sharesCount+`</p>
						<p class="_word">Shares</p>
						<p class="_point _point_listeningtime"></p>
					</div>
					<div class="_count_box">
						<p class="_count">`+downloadsCount+`</p>
						<p class="_word">Downloads</p>
						<p class="_point _point_downloads"></p>
					</div>
				</div>
				<div class="_csv">
					<a data-onclick="csv" href="#" class="medium-button medium-button--withChrome u-accentColor--buttonNormal">CSV Export</a>
				</div>
			`
			$("#play-dashboard-tab-3 .total-counts").remove()
			$("#play-dashboard-tab-3")[0].prepend(div)

			$("[data-onclick='csv']").on('click', function(e){
				const rows = [
					["date", "listens", "shares", "downloads"]
				];
				for (let i = 0; i < chartXLables.length; i++) {
					rows[i + 1] = [chartXLables[i], listensData[i], sharesData[i], downloadsData[i]]
				}
				let csvContent = "data:text/csv;charset=utf-8,";
				rows.forEach(function(rowArray){
					let row = rowArray.join(",");
					csvContent += row + "\r\n";
				}); 
				var encodedUri = encodeURI(csvContent);
				var link = document.createElement("a");
				link.style.display = "none";
				link.setAttribute("href", encodedUri);
				link.setAttribute("download", "output.csv");
				document.body.appendChild(link);
				link.click();
			})
		}

		var fetchStats = function(){
			if (typeof start != 'undefined'){
				$("#dashboard-stats-loader").show();
			}

			var timeframe = [start, end]
			if (typeof start == 'undefined') {
				var now = (new Date()).toISOString().split('.')[0]+"Z"
				var bck = new Date()
				bck.setDate(bck.getDate() - 27)
				bck = bck.toISOString().split('.')[0]+"Z"
				timeframe = [bck, now]
			}

			var dur = new Date(timeframe[1]) - new Date(timeframe[0])
			var daysDiff = Math.round(dur/(1000*60*60*24))

			chartXLables = []
			var d = new Date(timeframe[0])
			chartXLables.push(days[d.getDay()] + " " + (d.getDate()).toString() + "-" +  months[d.getMonth()])
			for(var i = 1; i <= daysDiff; i++){
				d.setDate(d.getDate() + 1)
				chartXLables.push(days[d.getDay()] + " " + (d.getDate()).toString() + "-" +  months[d.getMonth()])
			}


			listensData = []
			listensCount = 0
			chartYAxisTickMax = 0
			
			sharesData = []
			sharesCount = 0
			
			downloadsData = []
			downloadsCount = 0

			var getStats = function(collection){
				var from = timeframe[0].split("T")[0]
				var to = timeframe[1].split("T")[0]
				if(start){
					from = new Date(timeframe[0])
					from.setDate(from.getDate() + 1)
					from = from.toISOString().split("T")[0]

					to = new Date(timeframe[1])
					to.setDate(to.getDate() + 1)
					to = to.toISOString().split("T")[0]
				}
				if(!user.appId){
					return;
				}
				$.get('https://a.play.ht/'+collection+"/" + (articleid ? "article/" : "/") + "?app_id="+user.appId+"&created_at__gte="+from+"&created_at__lte="+to+(articleid ? "&article_id=" + articleid : ""))
				.done(function(res){
					if(res.aggregations && res.aggregations.counts && res.aggregations.counts.buckets && res.aggregations.counts.buckets.length > 0){
						var data = res.aggregations.counts.buckets;
						for(var i=0; i < data.length; i++){
							// var date = data[i].key_as_string
							var count = data[i].doc_count

							if(collection == 'listens'){
								listensData.push(count)
								listensCount = listensCount + count
							}

							if(collection == 'shares'){
								sharesData.push(count)
								sharesCount = sharesCount + count
							}

							if(collection == 'downloads'){
								downloadsData.push(count)
								downloadsCount = downloadsCount + count
							}
						}
						doneChart()
					}else{
						if(collection == 'listens'){
							for(var i = 0; i < chartXLables.length; i++){
								listensData.push(0)
							}
							doneChart()
							return
						}

						if(collection == 'shares'){
							for(var i = 0; i < chartXLables.length; i++){
								sharesData.push(0)
							}
							doneChart()
							return
						}

						if(collection == 'downloads'){
							for(var i = 0; i < chartXLables.length; i++){
								downloadsData.push(0)
							}
							doneChart()
							return
						}
					} 
				}).fail(function(err){
					window.playht__SetSentryContext({
						userId: userid,
						articleId: articleid,
					});
					window.Sentry && Sentry.captureException(err);

					if(collection == 'listens'){
						for(var i = 0; i < chartXLables.length; i++){
							listensData.push(0)
						}
						doneChart()
						return
					}

					if(collection == 'shares'){
						for(var i = 0; i < chartXLables.length; i++){
							sharesData.push(0)
						}
						doneChart()
						return
					}

					if(collection == 'downloads'){
						for(var i = 0; i < chartXLables.length; i++){
							downloadsData.push(0)
						}
						doneChart()
						return
					}
				})
			}

			getStats('listens')
			getStats('shares')
			getStats('downloads')
			
		}


		if(paid_user){
			fetchStats()
		}else{
			var h = `
				<div class="_stats_unavalibale_head">
					<div class="_title">Stats are available to paid members only.</div>
					<div class="_meta">Please upgrade to get access to your stats</div>
					<div class="_act">
						<a data-onclick="statsUpgrade" target="_blank" class="medium-button medium-button--primary medium-button--withChrome u-accentColor--buttonNormal" style="vertical-align: middle;font-size: 14px;padding: 5px;" href="https://play.ht/wordpress/upgrade/">
							Upgrade Account
						</a>
					</div>
				</div>
				<div class="_stats_unavalibale_body">
					<div class="_title">With stats you can see the following information about your articles:</div>
					<div class="_meta">
						<div class="_meta_t">Listens</div>
						<div class="_meta_m">The number of times your articles were listened.</div>
						<div class="_meta_t">Downloads</div>
						<div class="_meta_m">The number of times your articles were downloaded as audio files.</div>
						<div class="_meta_t">Shares</div>
						<div class="_meta_m">The number of times your articles were shared as audio on Twitter or Facebook.</div>
					</div>
					<div class="_act">
						<a target="_blank" data-onclick="statsUpgrade" class="medium-button medium-button--primary medium-button--filled medium-button--withChrome u-accentColor-- buttonNormal is-touched" href="https://play.ht/wordpress/upgrade/">
						Upgrade Account
						</a>
						<div class="_act_m">Pricing starts from $9/month. You can cancel anytime.</div>
					</div>
				</div>
			`
			$("#play-dashboard-tab-3").html(h)

			$("[data-onclick='statsUpgrade']").on('click', function(e){
				window.__playht__analytics && window.__playht__analytics('playTracker.send', 'event', "click", 'dashboard_stats_upgrade_clicked', username, {'dimension2': `${userid} - ${username}`});
			})
		}

		if(paid_user){
			$("#play-dashboard-stats-date-picker-from").datepicker({
				dateFormat: "DD, dd MM yy", 
				autoSize: true,
				onSelect: function(d){
					var min = new Date(Date.parse(d))
					$("#play-dashboard-stats-date-picker-to").datepicker("destroy");
					$("#play-dashboard-stats-date-picker-to").datepicker({
						minDate: min,
						autoSize: true,
						dateFormat: "DD, dd MM yy",
						onSelect: function(d2){
							var max = new Date(Date.parse(d2))
	
							var dayBefore = max;
							dayBefore.setDate(dayBefore.getDate()-1);
							$("#play-dashboard-stats-date-picker-from").datepicker("option", "maxDate", dayBefore);
	
							// when converting from js date to ISO-8601 (which required by keen)
							// a date like "Sun Apr 01 2018 00:00:00 GMT+0200 (EET)"
							// becomes "2018-03-31T22:00:00.000Z"
							// so keen return the day after what we want too
							min.setHours(2)
	
							// to include all the end day
							max.setHours(25)
							max.setMinutes(59)
							max.setSeconds(59)
	
	
							start = min.toISOString().split('.')[0]+"Z"
							end = max.toISOString().split('.')[0]+"Z"
	
							fetchStats()
						}	
					});
	
					var to = $( "#play-dashboard-stats-date-picker-to" ).datepicker("getDate");
					if (to == null) {
						var today = new Date()
						
						var yesterday = today;
						yesterday.setDate(yesterday.getDate()-1);
						$("#play-dashboard-stats-date-picker-from").datepicker("option", "maxDate", yesterday);
						
						today.setHours(25)
						today.setMinutes(59)
						today.setSeconds(59)
						$("#play-dashboard-stats-date-picker-to").datepicker("setDate", today);	
	
						start = min.toISOString().split('.')[0]+"Z"
						end = today.toISOString().split('.')[0]+"Z"
						fetchStats()	
					}else{
						min.setHours(2)
						to.setHours(25)
						to.setMinutes(59)
						to.setSeconds(59)
	
						start = min.toISOString().split('.')[0]+"Z"
						end = to.toISOString().split('.')[0]+"Z"
	
						fetchStats()
					}
	
					$("#play-dashboard-stats-date-picker-to").datepicker("option", "disabled", false);
				}});
			$("#play-dashboard-stats-date-picker-to").datepicker({dateFormat: "DD, dd MM yy", autoSize: true});
			$("#play-dashboard-stats-date-picker-to").datepicker("option", "disabled", true);
		}
	}


		if (userid != "") {
			firebase.database().ref('users/'+ userid).once('value').then(function(snapshot){
				var u = snapshot.val();
				if (u !== null) {
					fetchDashboardInfo(u)

					if(typeof u.usage !== 'undefined' && typeof u.usage.words_count !== 'undefined'){
						words += Number(u.usage.words_count)
					}
			
					if(typeof u.usage !== 'undefined' && typeof u.usage.appsumo_words !== 'undefined'){
						words += Number(u.usage.appsumo_words)
					}

					if(typeof u.usage !== 'undefined' && typeof u.usage.words_count !== 'undefined'){
						$("#play-dashbaord-credits").text(words)
						words = Number(u.usage.words_count)
								}else{
									words = 0;
					}
				}
			})
		}
	})
	
function playht__getDate(t) {
	if (typeof t == 'undefined') {return "DD MM, YY"}
	var a = new Date(t);
	var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	return a.getDate() + ' ' + months[a.getMonth()] + '  ' + a.getFullYear();
}

function playht__getTime(t) { 
	if (typeof t == 'undefined') {return "DD MM, YY"}    
	var a = new Date(t * 1000);
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	return a.getDate() + ' ' + months[a.getMonth()] + ', ' + a.getFullYear()
};