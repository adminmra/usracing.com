
(function ( w, $, undefined )
{
	$ ( function ()
	{
        $(document).ready(function(){
            if(!$(".voices__list-action")) return;
            $(".voices__list-action").on('click', function(e){
                var $this = $(this);
                e.stopPropagation();
                $('audio').each(function(i, el){
                  var voiceIcon = $(el).parent('.voices__list-el').find('.voice-icon');
                  if(voiceIcon.attr('id') !== $this.find('img').attr('id') ){
                    el.pause();
                    voiceIcon.attr('src', 'https://play.ht/app-landing/images/play-btn.png');
                  }
            
                });
            
                var audioEl = $(this).parent('.voices__list-el').find('audio').get(0);
                if(audioEl.duration > 0 && !audioEl.paused){
                  audioEl.pause();
                  $(this).find('img').attr('src', 'https://play.ht/app-landing/images/play-btn.png');
                }else{
                  $('.voices__list').find('audio').trigger('pause');
                  audioEl.play();
                  $(this).find('img').attr('src', 'https://play.ht/app-landing/images/pause-btn.png');
                }
              });

              $("body").on('click', '.js__playht-chat-with-us', function(){
                var $this = $(this);
                $this.text('Loading Chat...');
                if(!!window.$crisp){
                  $crisp.push(['do', 'chat:show']);
                  $crisp.push(['do', 'chat:open']);
                  if(typeof window.wppp_retrieve !== 'undefined') {
                    $crisp.push(["set", "session:data", [[["user-id", wppp_retrieve.user_id]]]])
                    $crisp.push(["set", "session:data", [[["user-app-id", wppp_retrieve.app_id]]]])
                  }
                  $this.text('Contact us');
                }
              })
        });
	} );
}) ( window, jQuery );
