
(function ( w, $, undefined )
{
	$ ( function ()
	{
		var referral_obj = {
			userId  : wppp_theme.user_id,
			referral: wppp_theme.theme
		};
		$.ajax ( {
			type: 'POST',
			url : wppp_theme.ajax_url,
			contentType: 'application/json; charset=utf-8',
			dataType   : "json",
			data: JSON.stringify ( referral_obj ),
		} )
		;
	} );
}) ( window, jQuery );