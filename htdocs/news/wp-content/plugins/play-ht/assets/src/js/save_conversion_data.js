(function (w, $, undefined) {
  $(function () {
    var userId = wppp_conv_data ? wppp_conv_data.userId : "";
    var blogName = window.location.hostname;

    var ids = {}; // ids= {<post-id>: <transcription-id>, ...}
    if (wppp_conv_data.posts) {
      Object.keys(wppp_conv_data.posts).map(function (id) {
        // register events only for articles in "converting" state
        if (
          wppp_conv_data.posts[id] &&
          wppp_conv_data.posts[id].audio_status == "1"
        ) {
          if (
            !wppp_conv_data.posts[id].trans_id ||
            wppp_conv_data.posts[id].trans_id === "ArraybodytranscriptionId"
          ) {
            var $element = $("#post-" + id).find(".has_audio");
            showDoneStatusInColumn($element, "No");
          } else {
            ids[id] = wppp_conv_data.posts[id].trans_id;
          }
        } else if (
          wppp_conv_data.posts[id] &&
          wppp_conv_data.posts[id].audio_status == "3"
        ) {
          var $element = $("#post-" + id).find(".has_audio");
          showErrorStatusInColumn($element);
        }
      });
      if (Object.keys(wppp_conv_data.posts).length > 0) {
        firebaseListenOnIds(ids);
      }
    } else if (
      wppp_conv_data.post_id &&
      wppp_conv_data.post &&
      wppp_conv_data.post.trans_id &&
      wppp_conv_data.post.audio_status == "1"
    ) {
      // single post still converting.
      ids[wppp_conv_data.post_id] = wppp_conv_data.post.trans_id;
      var $element = $("#podcast-meta-box-id")
        .find(".inside")
        .find(".converting-msg-wrapper");
      $element && showConvertingStatusInMetaBox($element);
      firebaseListenOnIds(ids, true);
    } else if (
      wppp_conv_data.post_id &&
      wppp_conv_data.post &&
      wppp_conv_data.post.trans_id &&
      wppp_conv_data.post.audio_status == "2"
    ) {
      // single post done converting.
      var $element = $("#podcast-meta-box-id")
        .find(".inside")
        .find(".converting-msg-wrapper");
      showDoneStatusInMetaBox($element);
    } else if (
      wppp_conv_data.post_id &&
      wppp_conv_data.post &&
      wppp_conv_data.post.trans_id &&
      wppp_conv_data.post.audio_status == "3"
    ) {
      var $element = $("#podcast-meta-box-id")
        .find(".inside")
        .find(".converting-msg-wrapper");
      showErrorStatusInMetaBox($element);
    } else if (
      (wppp_conv_data.post_id &&
        wppp_conv_data.post &&
        !wppp_conv_data.post.trans_id) ||
      (wppp_conv_data.post_id &&
        wppp_conv_data.post &&
        wppp_conv_data.post.trans_id === "ArraybodytranscriptionId")
    ) {
      var $element = $("#podcast-meta-box-id")
        .find(".inside")
        .find(".converting-msg-wrapper");
      showDoneStatusInMetaBox($element, "This post does not have audio.");
    }

    function firebaseListenOnIds(ids, isSinglePost) {
      $.each(ids, function (key, value) {
        if (!value) return;
        var firebaseArticleNode = firebase
          .database()
          .ref("articles")
          .child(value);

        firebaseArticleNode.on("value", function articleListener(snapshot) {
          var article_object = snapshot.val();
          if (article_object && article_object.error) {
            var $element = isSinglePost
              ? $("#podcast-meta-box-id")
                  .find(".inside")
                  .find(".converting-msg-wrapper")
              : $("#post-" + key).find(".has_audio");
            isSinglePost
              ? showErrorStatusInMetaBox($element)
              : showErrorStatusInColumn($element);
            $.ajax({
              type: "POST",
              url: wppp_conv_data.admin_url,
              data: {
                action: "article_converted_failed",
                post_id: key,
              },
            });
            firebaseArticleNode.off("value", articleListener);
            return;
          }

          if (
            article_object &&
            article_object.transcriped &&
            typeof article_object.transcriptionUrl_mp3 !== "undefined"
          ) {
            firebaseArticleNode.off("value", articleListener);
            window.wppp_conv_data &&
              (wppp_conv_data.post = {
                article_audio: article_object.transcriptionUrl_mp3,
                audio_status: 2,
                trans_id: value,
                voice: article_object.voice,
              });

            $.ajax({
              type: "POST",
              url: wppp_conv_data.admin_url,
              data: {
                action: "article_converted_success",
                post_id: key,
                trans_id: value,
                audio_url: article_object.transcriptionUrl_mp3,
              },
              success: function () {
                var $element = isSinglePost
                  ? $("#podcast-meta-box-id")
                      .find(".inside")
                      .find(".converting-msg-wrapper")
                  : $("#post-" + key).find(".has_audio");
                
                if(isSinglePost){
                    showDoneStatusInMetaBox($element); 
                  }else{
                    showDoneStatusInColumn($element);
                    var del_item = $("#post-" + key).find(".row-actions");

                    var del_ele = ' | <span class="delete_podcast"><a style="cursor:pointer;" id="js__playht-delete-audio" data-postid="'+key+'">Delete Audio</a></span>';

                    adddeleteAudioInColumn(del_item, del_ele);
                  }
              },
              error: function (error) {
                window.playht__SetSentryContext({
                  userId: userId,
                  website: window.location.href,
                  post_id: key,
                  trans_id: value,
                  audio_url: article_object.transcriptionUrl_mp3,
                  isSinglePostPage: isSinglePost,
                });
                window.Sentry && Sentry.captureException(error);
              },
            });
          }
        });
      });
    }

    function saveDraftMetaData({ postId, playArticleId, voice }) {
      return $.ajax({
        type: "POST",
        url: wppp_conv_data.admin_url,
        data: {
          action: "save_article_draft",
          post_id: postId,
          play_article_id: playArticleId,
          voice,
        },
      });
    }

    function deleteDraft({ postId }) {
      return $.ajax({
        type: "POST",
        url: wppp_conv_data.admin_url,
        data: {
          action: "delete_article_draft",
          post_id: postId,
        },
      });
    }

    function deleteAudio({ postId }) {
      return $.ajax({
        type: "POST",
        url: wppp_conv_data.admin_url,
        data: {
          action: "delete_article_audio",
          post_id: postId,
        },
      });
    }

    async function getPostDataAndAuthor({ postId }) {
      try {
        if (
          wppp_conv_data.current_post_type === "page" ||
          wppp_conv_data.current_post_type === "post"
        ) {
          const wp = new WPAPI({
            endpoint: window.wppp_conv_data.rest_endpoint,
            nonce: window.wppp_conv_data.nonce,
          });
          const postData =
            wppp_conv_data.current_post_type === "post"
              ? await wp.posts().id(postId)
              : await wp.pages().id(postId);
          const authorData = await wp.users().id(postData.author);
          return { postData, authorData };
        }
        return {};
      } catch (error) {
        console.log("getPostDataAndAuthor -> error", error);
        return {};
      }
    }

    function getPostMetaData(postId) {
      return new Promise(function (resolve, reject) {
        $.ajax({
          type: "POST",
          url: wppp_conv_data.admin_url,
          data: {
            action: "retrieve_post_meta_data",
            post_id: postId,
          },
          success: function (resp) {
            if (resp.success && resp.data.response) {
              resolve(resp.data.response);
            } else {
              resolve(null);
            }
          },
          error: function (error) {
            console.log("getPostMetaData -> error", error);
            resolve(null);
          },
        });
      });
    }

    function parseHTMLText(domElement) {
      var root = domElement;
      var text = [];

      function traverseTree(root) {
        if (!root.childNodes) return null;
        Array.prototype.forEach.call(root.childNodes, function (child) {
          if (child.nodeType === 3) {
            var str = child.nodeValue.trim();
            if (str.length > 0) {
              text.push(str);
            }
          } else {
            traverseTree(child);
          }
        });
      }
      traverseTree(root);
      return text.join(" ");
    }

    function getArticleInfo(
      articleURL,
      voice,
      appId,
      userId,
      author_name,
      author_image,
      published_date,
      title,
      image,
      ssml,
      isDraft
    ) {
      var data = {
        url: articleURL,
        voice: voice,
        appId: appId,
        userId: userId,
        title: title,
        image: image,
        author_name: author_name,
        author_image: author_image,
        published_date: published_date,
        ssml,
        publishStatus: isDraft ? "draft" : "",
      };

      return new Promise(function (resolve, reject) {
        $.ajax({
          type: "POST",
          url: "https://play.ht/api/setArticleInfo",
          data: JSON.stringify(data),
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (resp) {
            if (resp && resp.article) {
              resolve(resp.article.id);
            } else {
              reject("no articleInfo available for article: ", articleURL);
            }
          },
          error: function (error) {
            window.playht__SetSentryContext({
              userId: userId,
              website: window.location.href,
              url: articleURL,
              voice: voice,
              appId: appId,
              author_name: author_name,
            });
            window.Sentry && Sentry.captureException(error);
            reject(error);
          },
        });
      });
    }

    function conversionRequest(
      voice,
      lang,
      narrationStyle,
      postId,
      articleDataId,
      ssml,
      wordsCount
    ) {
      const articleFallbackURL = wppp_conv_data.home_url + postId;

      return new Promise(function (resolve, reject) {
        $.ajax({
          type: "POST",
          url: wppp_conv_data.admin_url,
          data: {
            action: "save_post_conversion_data",
            lang: lang,
            post_id: postId,
            article_fallback_url: articleFallbackURL,
            play_article_id: articleDataId,
            voice: voice,
            ssml: JSON.stringify(ssml),
            narrationStyle: narrationStyle,
            readAlongEnabled: wppp_conv_data.readAlongEnabled === "1",
            wordsCount: wordsCount,
          },
          success: function (resp) {
            try {
              ids[postId] = JSON.parse(resp.data.response.body).transcriptionId;
              resolve(resp);
            } catch (error) {
              window.Sentry && Sentry.captureException(error);
              reject(error);
            }
          },
          error: function (error) {
            window.playht__SetSentryContext({
              userId: userId,
              website: window.location.href,
              lang: lang,
              postId: postId,
              article_id: articleDataId,
              voice: voice,
            });
            window.Sentry && Sentry.captureException(error);
            reject(error);
          },
        });
      });
    }

    function fetchArticleText(postId) {
      return new Promise(function (resolve, reject) {
        $.ajax({
          type: "POST",
          url: wppp_conv_data.admin_url,
          data: {
            action: "get_post_content_data",
            post_id: postId,
          },
          success: function (resp) {
            try {
              resolve(resp.data.response);
            } catch (error) {
              window.Sentry && Sentry.captureException(error);
              reject(error);
            }
          },
          error: function (error) {
            window.playht__SetSentryContext({
              userId: userId,
              website: window.location.href,
              postId: postId,
            });
            window.Sentry && Sentry.captureException(error);
            reject(error);
          },
        });
      });
    }

    function getUserData(userId) {
      return new Promise(async (resolve) => {
        if (userId) {
          firebase
            .database()
            .ref("users")
            .child(userId)
            .once("value", async (data) => {
              if (data.val()) {
                resolve(data.val());
              } else {
                resolve({});
              }
            });
        } else {
          reject();
        }
      });
    }

    function getArticleData(articleDataId) {
      return new Promise(async (resolve) => {
        if (articleDataId) {
          firebase
            .database()
            .ref("articleData")
            .child(articleDataId)
            .once("value", async (data) => {
              if (data.val()) {
                resolve(data.val());
              } else {
                resolve({});
              }
            });
        } else {
          reject();
        }
      });
    }

    function getArticleContent(postId, articleDataId) {
      return new Promise(async (resolve) => {
        let articleTextFormatted;
        if (articleDataId) {
          firebase
            .database()
            .ref("articlesContent")
            .child(articleDataId)
            .child("text")
            .once("value", async (data) => {
              if (data.val()) {
                let articleText = data.val().join("\n \n");
                articleTextFormatted = articleText
                  .split("\n")
                  .filter((el) => el.trim())
                  .filter(Boolean);
              } else {
                let articleText = await fetchArticleText(postId);
                articleTextFormatted = articleText
                  .filter((el) => el.trim())
                  .filter(Boolean);
              }
              resolve(articleTextFormatted);
            });
        } else {
          let articleText = await fetchArticleText(postId);
          articleTextFormatted = articleText
            .filter((el) => el.trim())
            .filter(Boolean);
          resolve(articleTextFormatted);
        }
      });
    }

    function getArticleSSML(articleDataId) {
      return new Promise(async (resolve) => {
        let articleTextFormatted;
        if (articleDataId) {
          firebase
            .database()
            .ref("articlesContent")
            .child(articleDataId)
            .child("ssml")
            .once("value", async (data) => {
              if (data.val()) {
                let articleText = data.val().join("\n \n");
                articleTextFormatted = articleText
                  .split("\n")
                  .filter((el) => el.trim())
                  .filter(Boolean);
              }
              resolve(articleTextFormatted);
            });
        } else {
          resolve(null);
        }
      });
    }

    function showSchedulingModal(current, total, message = "", title = "") {
      playht__swal.fire({
        imageUrl: wppp_images.settings,
        title: title || "Loading ...",
        text: message || "",
        showConfirmButton: false,
        animation: false,
      });
    }

    function showDoneModal(single) {
      playht__swal.fire({
        imageUrl: wppp_images.checked,
        title: "Done",
        html:
          "Your" +
          (single ? " article has" : " articles have") +
          " been scheduled for audio conversion; It takes few minutes to finish then you will see the Embedded audio player in your " +
          (single ? "article" : "articles"),
        confirmButtonClass: "wppp-modal-ok-btn",
      });
    }
    function showDraftDoneModal() {
      playht__swal.fire({
        imageUrl: wppp_images.checked,
        title: "Done",
        html: "Your Draft has been saved!",
        confirmButtonClass: "wppp-modal-ok-btn",
      });
    }

    function showDraftStatusInColumn(el, post_Id) {
      var convertingHTML =
        '<span><a style="cursor:pointer;text-decoration: underline;" id="play-open-draft-editor" data-postId="' +
        post_Id +
        '" >Edit Draft</a> - <a style="cursor:pointer;text-decoration: underline; color: red;" id="play-delete-draft-link" data-postId="' +
        post_Id +
        '" >Delete Draft</a></span>';
      el.html(convertingHTML);
    }

    function showConvertingStatusInColumn(el) {
      var convertingHTML =
        '<img class="loading" src="' +
        wppp_images.settings +
        '" ><span>Converting</span>';
      el.html(convertingHTML);
    }

    function showConvertingStatusInMetaBox(el) {
      var convertingHTML =
        '<img class="loading" src="' +
        wppp_images.settings +
        '"><span class="converting-text">Converting ...</span>';
      el.html(convertingHTML);
    }

    function removedeleteInColumn(el) {
      el.remove();
    }

    function adddeleteAudioInColumn(del_item, del_ele){
        $( del_item ).append( del_ele );
    }

    function showDoneStatusInColumn(el, text = "Yes") {
      var imgSrc = text === "No" ? wppp_images.no_audio : wppp_images.audio;
      var convertingHTML =
        '<img src="' + imgSrc + '" /><span>' + text + "</span>";
      el.html(convertingHTML);
    }

    function showErrorStatusInColumn(el) {
      var errorHTML =
        '<div class="__play-tooltip-wide" data-tooltip="An error has happened, we have been informed and will fix it shortly. Please try converting other articles in the mean time." ><img src="' +
        wppp_images.audio_error +
        '" /><span>Error</span></div>';
      el.html(errorHTML);
    }

    function showErrorStatusInMetaBox(el) {
      var errorHTML =
        '<div class="__play-tooltip-wide" data-tooltip="An error has happened, we have been informed and will fix it shortly. Please try converting other articles in the mean time." ><img src="' +
        wppp_images.audio_error +
        '" style="width: 24px;"><span class="converting-text">Audio Conversion Error.</span></div>';
      el.html(errorHTML);
    }

    function showDoneStatusInMetaBox(el, text = "This post has audio.") {
      var imgSrc =
        text === "This post does not have audio."
          ? wppp_images.no_audio
          : wppp_images.audio;
      var convertingHTML =
        '<img style="width: 24px;" src="' +
        imgSrc +
        '" /><span class="converting-text">' +
        text +
        "</span>";
      el.html(convertingHTML);
    }

    function checkCreditsAllowance(count) {
      var currentWords = wppp_conv_data.current_credits
        ? wppp_conv_data.current_credits.appsumo_words
          ? Number(wppp_conv_data.current_credits.appsumo_words) +
            Number(wppp_conv_data.current_credits.words_count)
          : Number(wppp_conv_data.current_credits.words_count)
        : 0;
      return Number(100) <= currentWords;
    }

    function openArticleEditor({
      articleDataId,
      postId,
      narrationStyle,
      voice,
      title,
      postContent,
    }) {
      return new Promise(async (resolve, reject) => {
        window.playht__SetSentryContext({
          userId: userId,
          website: window.location.href,
          postId: postId,
          article_id: articleDataId,
          voice: voice,
        });
        try {
          let userData = await getUserData(userId);
          let pauses = { title: 1, paragraph: 0.45, period: 0.4, comma: 0.1 };
          if(userData && userData.conf && userData.conf.default_pauses) {
            pauses = userData.conf.default_pauses;
          }

          let articleSSMLContent =
            (await getArticleSSML(articleDataId)) || null;
          let articleData = {};
          try {
            if (articleDataId) {
              articleData = await getArticleData(articleDataId);
            }
          } catch (error) {
            console.log("openArticleEditor -> error", error);
          }
          const content = articleSSMLContent
            ? null
            : postContent || (await getArticleContent(postId, articleDataId));
          const wrapper = document.createElement("div");
          const editor = document.createElement("div");
          const modalCloseWrapper = document.createElement("div");
          const modalCloseIcon = document.createElement("Img");

          wrapper.id = "playht-tts-editor-root";
          editor.id = "playht-tts-editor-item";
          modalCloseWrapper.className = "playht-tts-editor-close";
          modalCloseIcon.src = wppp_images.close;
          modalCloseWrapper.appendChild(modalCloseIcon);
          wrapper.appendChild(modalCloseWrapper);
          wrapper.appendChild(editor);
          document.body.append(wrapper);
          modalCloseWrapper.addEventListener("click", () => {
            wrapper.remove();
            playht__swal.close();
          });
          ReactDOM.render(
            React.createElement(
              PlayhtTTSEditor.PlayhtTTSEditor,
              {
                onConvert: (value) => {
                  if (value) {
                    window.Sentry &&
                      Sentry.captureMessage("onConvert click", "info");
                    articleSSMLContent = value.patches;
                    voice = value.voice;
                    narrationStyle = value.narrationStyle;
                    wordsCount = value.wordsCount;
                    if (value) {
                      resolve({
                        articleSSMLContent,
                        voice,
                        narrationStyle,
                        wordsCount,
                        isDraft: false,
                      });
                    } else {
                      window.Sentry &&
                        Sentry.captureException(
                          new Error(
                            "custom error: No editor onConvert value patches"
                          )
                        );
                      reject();
                    }
                    wrapper.remove();
                    if (value.errors && value.errors.length) {
                      window.Sentry && Sentry.captureException(value.errors);
                    }
                  } else {
                    window.Sentry &&
                      Sentry.captureException(
                        new Error("custom error: No editor onConvert value")
                      );
                    reject();
                  }
                },
                onCancel: () => {
                  wrapper.remove();
                  playht__swal.close();
                },
                onSaveDraft: (value) => {
                  if (value) {
                    window.Sentry &&
                      Sentry.captureMessage("onSaveDraft click", "info");
                    articleSSMLContent = value.patches;
                    voice = value.voice;
                    narrationStyle = value.narrationStyle;
                    wordsCount = value.wordsCount;
                    if (value) {
                      resolve({
                        articleSSMLContent,
                        voice,
                        narrationStyle,
                        wordsCount,
                        isDraft: true,
                      });
                    } else {
                      window.Sentry &&
                        Sentry.captureException(
                          new Error(
                            "custom error: No editor onDraft value patches"
                          )
                        );
                      reject();
                    }
                    wrapper.remove();
                    if (value.errors && value.errors.length) {
                      window.Sentry && Sentry.captureException(value.errors);
                    }
                  } else {
                    window.Sentry &&
                      Sentry.captureException(
                        new Error("custom error: No editor onConvert value")
                      );
                    reject();
                  }
                },
                userId,
                text: content,
                ssml: articleSSMLContent,
                voice,
                context: "wp",
                title,
                skipToEditor: true,
                convertBtnText: "Convert",
                pauses,
                isDraft:
                  !articleDataId || articleData.publishStatus === "draft",
                narrationStyle,
                containerSelector: ".playht-tts-editor-modal-popup",
              },
              null
            ),
            document.getElementById("playht-tts-editor-item")
          );
        } catch (error) {
          window.Sentry && Sentry.captureException(error);
        }
      });
    }

    var can_convert = wppp_conv_data.can_convert;
    // single post
    $("body").on("click", "#convert_podcast", async function (ev) {
      showSchedulingModal(-1, 1, "Starting the Audio Editor");
      const wpCodeEditor = wp.data ? wp.data.select("core/editor") : null;
      const wpCodeCore = wp.data ? wp.data.select("core") : null;
      let postContent = null;
      let blockEditorPostTitle = null;
      let blockEditorPostId = null;
      let postAuthorData = null;
      let blockEditorPostDate = null;
      if (wpCodeEditor) {
        const originalContent = wpCodeEditor.getCurrentPost().content;
        const editedContent = wpCodeEditor.getEditedPostContent();
        const originalContentHTML = $.parseHTML(originalContent);
        blockEditorPostTitle = wpCodeEditor.getCurrentPost().title;
        blockEditorPostId = wpCodeEditor.getCurrentPost().id;
        blockEditorPostDate = wpCodeEditor.getCurrentPost().date;
        postContent = originalContentHTML
          .map((el) => parseHTMLText(el))
          .map((el) => el.trim().replace(/(?:\r\n|\r|\n)/g, " "))
          .map((el) => el.trim())
          .filter(Boolean);

        if (wpCodeCore) {
          try {
            const postAuthorId = wpCodeEditor.getCurrentPost().author;
            const postAuthors = wpCodeCore.getAuthors();
            postAuthorData = postAuthors.filter(
              (o) => o.id === postAuthorId
            )[0];
          } catch (error) {
            console.log("postAuthorData error", error);
          }
        }
      }

      window.__playht__analytics &&
        __playht__analytics(
          "playht__analytics.send",
          "event",
          "click",
          "add-audio",
          wppp_conv_data.article_url
        );

      if (1 == can_convert) {
        if (!checkCreditsAllowance(1)) {
          window.__playht__analytics &&
            __playht__analytics(
              "playht__analytics.send",
              "event",
              "click",
              "no-credits",
              wppp_conv_data.article_url
            );
          // show not enough words modal, buy more to continue
          playht__swal.fire({
            imageUrl: wppp_images.no_credits,
            title: "Sorry, you don't have any words left!",
            html:
              "<div class='__play-sw-modal-content'><div>Sorry, we are unable to add audio to your articles because you don’t have enough words left! Please purchase them from the link below and you will be able to add audio to your articles again :)</div><a href='https://play.ht/wordpress/upgrade' target='_blank' class='wppp-modal-ok-btn'>Get More Words</a></div>",
            animation: false,
            showConfirmButton: false,
          });
          return false;
        }

        $(".wppp-notice").remove();
        var voice = $("#wppp_voice_meta option:selected").val();
        var lang = $("#wppp_lang_meta option:selected").val();
        var narrationStyle = $("#wppp_voice_style_meta option:selected").val();
        var postTitle =
          blockEditorPostTitle ||
          wppp_conv_data.post.title ||
          wppp_conv_data.post_title;
        var author_name = postAuthorData
          ? postAuthorData.name
          : wppp_conv_data.post_author &&
            wppp_conv_data.post_author.author_firstname &&
            wppp_conv_data.post_author.author_lastname
          ? wppp_conv_data.post_author.author_firstname +
            " " +
            wppp_conv_data.post_author.author_lastname
          : wppp_conv_data.post_author.author_name;
        const postId = blockEditorPostId || wppp_conv_data.post_id;
        const articleDataId =
          wppp_conv_data.post && wppp_conv_data.post.play_article_id;
        const {
          articleSSMLContent,
          voice: v,
          narrationStyle: ns,
          wordsCount,
          isDraft,
        } = await openArticleEditor({
          articleDataId,
          postId,
          narrationStyle,
          voice,
          title: postTitle,
          postContent,
        });
        const published_date =
          blockEditorPostDate || wppp_conv_data.post.published_date;

        showSchedulingModal(-1, 1, "", isDraft ? "Saving Draft ..." : "");
        getArticleInfo(
          wppp_conv_data.article_url,
          v,
          wppp_conv_data.appId,
          wppp_conv_data.userId,
          author_name,
          wppp_conv_data.post_author.author_image,
          published_date,
          postTitle,
          wppp_conv_data.post.image,
          articleSSMLContent,
          isDraft
        )
          .then(async function (articleDataId) {
            if (!isDraft) {
              conversionRequest(
                v,
                lang,
                ns,
                postId,
                articleDataId,
                articleSSMLContent,
                wordsCount
              ).then(function (resp) {
                var $element = $("#podcast-meta-box-id")
                  .find(".inside")
                  .find(".converting-msg-wrapper");
                showConvertingStatusInMetaBox($element);

                showDoneModal(true);
                firebaseListenOnIds(ids, true);
              });
            } else {
              await saveDraftMetaData({
                postId,
                playArticleId: articleDataId,
                voice: v,
              });
              var $element = $("#podcast-meta-box-id")
                .find(".inside")
                .find(".converting-msg-wrapper");
              showDraftStatusInColumn($element, postId);
              showDraftDoneModal();
              if (
                !wppp_conv_data.post ||
                (wppp_conv_data.post && !wppp_conv_data.post.play_article_id)
              ) {
                wppp_conv_data.post = {
                  play_article_id: articleDataId,
                  voice: v,
                };
              } else {
                wppp_conv_data.post.play_article_id = articleDataId;
                wppp_conv_data.post.voice = voice;
              }
            }
          })
          .catch(function (error) {
            window.playht__SetSentryContext({
              userId: wppp_conv_data.userId,
              website: window.location.href,
              articleURL: wppp_conv_data.article_url,
              voice: voice,
              appId: wppp_conv_data.appId,
              title: postTitle,
            });
            window.Sentry && Sentry.captureException(error);

            playht__swal.fire({
              imageUrl: wppp_images.warning,
              title: "Uh oh! Something Went Wrong :(",
              html:
                "<div class='__play-sw-modal-content'><div>Please try again! Or <span class='btn btn-primary posts-btn js__playht-chat-with-us' style='border-bottom: 1px solid #ddd; cursor: pointer;'>Click here to Contact us</span></div></div>",
              animation: false,
              confirmButtonText: "Close",
              confirmButtonClass: "wppp-modal--cancel-btn",
            });
          });
      } else {
        $(".wrap").prepend(
          '<div class="notice notice-error wppp-notice is-dismissible"> ' +
            "<p>" +
            can_convert.errors.play[0] +
            "</p>" +
            "</div>"
        );
        // window.scrollTo(0, 0);
        playht__swal.fire({
          imageUrl: wppp_images.cancel,
          title: "Error!",
          text: can_convert.errors.play[0],
          confirmButtonText: "Close",
          confirmButtonClass: "wppp-modal--cancel-btn",
          animation: false,
        });
      }
    });

    function processArticlesConversion(
      $selectedArticles,
      totalSelectedArticles,
      processedArticles,
      ssml,
      voice,
      narrationStyle,
      wordsCount,
      isDraft,
      postId
    ) {
      showSchedulingModal(
        0,
        totalSelectedArticles,
        "",
        isDraft ? "Saving Draft ..." : ""
      );

      $selectedArticles.each(async function () {
        try {
          var self = this;
          var $this = $(this);
          var id = self.value;

          const { postData, authorData } = await getPostDataAndAuthor({
            postId: id,
          });
          var author_name = authorData
            ? authorData.name
            : wppp_conv_data.posts_authors[id] &&
              wppp_conv_data.posts_authors[id].author_firstname &&
              wppp_conv_data.posts_authors[id].author_lastname
            ? wppp_conv_data.posts_authors[id].author_firstname +
              " " +
              wppp_conv_data.posts_authors[id].author_lastname
            : wppp_conv_data.posts_authors[id].author_name;

          const authorImage =
            authorData && authorData.avatar_urls
              ? authorData.avatar_urls[96]
              : wppp_conv_data.posts_authors[id].author_image;
          const publishedDate = postData
            ? postData.date
            : wppp_conv_data.posts[id].published_date;
          const postTitle = postData
            ? postData.title.rendered
            : wppp_conv_data.posts[id].title;
          const postImg =
            wppp_conv_data.posts && wppp_conv_data.posts[id]
              ? wppp_conv_data.posts[id].image
              : "";

          voice = voice || $("#wppp_voice_meta option:selected").val();
          var lang = $("#wppp_lang_meta option:selected").val();
          narrationStyle =
            narrationStyle || $("#wppp_voice_style_meta option:selected").val();

          window.__playht__analytics &&
            __playht__analytics(
              "playht__analytics.send",
              "event",
              "click",
              "add-audio",
              wppp_conv_data.home_url + self.value
            );

          getArticleInfo(
            wppp_conv_data.home_url + self.value,
            voice,
            wppp_conv_data.appId,
            wppp_conv_data.userId,
            author_name,
            authorImage,
            publishedDate,
            postTitle,
            postImg,
            ssml,
            isDraft
          )
            .then(async function (articleDataId) {
              if (!isDraft) {
                conversionRequest(
                  voice,
                  lang,
                  narrationStyle,
                  self.value,
                  articleDataId,
                  ssml,
                  wordsCount
                ).then(function (resp) {
                  processedArticles = processedArticles + 1;

                  var $cellElement = $this
                    .parentsUntil("tr")
                    .parent()
                    .find(".has_audio");
                  showConvertingStatusInColumn($cellElement);

                  processedArticles < totalSelectedArticles
                    ? showSchedulingModal(
                        processedArticles,
                        totalSelectedArticles
                      )
                    : showDoneModal();

                  if (processedArticles === totalSelectedArticles) {
                    firebaseListenOnIds(ids);
                  }
                });
              } else {
                await saveDraftMetaData({
                  postId,
                  playArticleId: articleDataId,
                  voice,
                });
                var $cellElement = $this
                  .parentsUntil("tr")
                  .parent()
                  .find(".has_audio");
                showDraftStatusInColumn($cellElement, postId);
                showDraftDoneModal();
                if (
                  !wppp_conv_data.posts[postId] ||
                  (wppp_conv_data.posts[postId] &&
                    !wppp_conv_data.posts[postId].play_article_id)
                ) {
                  wppp_conv_data.posts[postId] = {
                    play_article_id: articleDataId,
                    voice,
                  };
                } else {
                  wppp_conv_data.posts[postId].play_article_id = articleDataId;
                  wppp_conv_data.posts[postId].voice = voice;
                }
              }
            })
            .catch(function (error) {
              console.log("processArticlesConversion -> error", error);
              window.Sentry && Sentry.captureException(error);

              playht__swal.fire({
                imageUrl: wppp_images.warning,
                title: "Uh oh! Something Went Wrong :(",
                html:
                  "<div class='__play-sw-modal-content'><div>Please try again! Or <span class='btn btn-primary posts-btn js__playht-chat-with-us' style='border-bottom: 1px solid #ddd; cursor: pointer;'>Click here to Contact us</span></div></div>",
                animation: false,
                confirmButtonText: "Close",
                confirmButtonClass: "wppp-modal--cancel-btn",
              });
            });
        } catch (error) {
          console.log("processArticlesConversion -> error", error);
        }
      });
    }

    // multiple posts
    $("#multiple_conversion").on("click", async function (e) {
      if (1 == can_convert) {
        ids = {};
        var totalSelectedArticles = 0;
        var processedArticles = 0;
        $(".wppp-notice").remove();
        let voice = $("#wppp_voice_meta option:selected").val();
        var narrationStyle = $("#wppp_voice_style_meta option:selected").val();
        var $selectedArticles = $(
          'input:checked[type="checkbox"][name="post[]"]'
        );
        var selectedArticlesCount = $selectedArticles.length;

        if (!checkCreditsAllowance(selectedArticlesCount)) {
          window.__playht__analytics &&
            __playht__analytics(
              "playht__analytics.send",
              "event",
              "click",
              "no-credits",
              wppp_conv_data.article_url
            );
          // show not enough credits modal, buy more to continue
          playht__swal.fire({
            imageUrl: wppp_images.no_credits,
            title: "Sorry, you don't have any words left!",
            html:
              "<div class='__play-sw-modal-content'><div>Sorry, we are unable to add audio to your articles because you don’t have enough words left! Please purchase them from the link below and you will be able to add audio to your articles again :)</div><a href='https://play.ht/wordpress/upgrade' target='_blank' class='wppp-modal-ok-btn'>Get More Words</a></div>",
            animation: false,
            showConfirmButton: false,
          });
          return false;
        }

        totalSelectedArticles = $selectedArticles ? selectedArticlesCount : 0;

        if (totalSelectedArticles === 1) {
          showSchedulingModal(-1, 1, "Starting the Audio Editor");
          try {
            var postId = $selectedArticles[0].value;
            let postMetaData = null;
            let postContent = null;
            const { postData } = await getPostDataAndAuthor({ postId });

            if(postData.content.rendered == ''){
              playht__swal.fire({
                imageUrl: wppp_images.warning,
                title: "There is no content to convert!",
                html:
                  "<div class='__play-sw-modal-content'><div>Sorry, we couldn't find any content to convert to audio. Please add some content first.</div></div>",
                animation: false,
                showConfirmButton: true,
                confirmButtonText: "Close",
                confirmButtonClass: "",
              });
            }
            
            postContent = postData
              ? Array.from($.parseHTML(postData.content.rendered)).map((el) =>
                  parseHTMLText(el)
                )
              : null;
            postContent = postContent ? postContent
              .map((el) => el.trim().replace(/(?:\r\n|\r|\n)/g, " "))
              .map((el) => el.trim())
              .filter(Boolean) : null;
            try {
              postMetaData = await getPostMetaData(postId);
            } catch (error) {}
            const articleDataId = postMetaData
              ? postMetaData.play_article_id
              : wppp_conv_data.posts &&
                wppp_conv_data.posts[postId] &&
                wppp_conv_data.posts[postId].play_article_id;
            const title =
              postData && postData.title && postData.title.rendered
                ? $.parseHTML(postData.title.rendered)[0].textContent
                : wppp_conv_data.posts &&
                  wppp_conv_data.posts[postId] &&
                  wppp_conv_data.posts[postId].title;

            const {
              articleSSMLContent,
              voice: v,
              narrationStyle: ns,
              wordsCount,
              isDraft,
            } = await openArticleEditor({
              articleDataId,
              postId,
              narrationStyle,
              voice,
              title,
              postContent,
            });
            processArticlesConversion(
              $selectedArticles,
              totalSelectedArticles,
              processedArticles,
              articleSSMLContent,
              v,
              ns,
              wordsCount,
              isDraft,
              postId
            );
          } catch (error) {
            console.log("error", error);
          }
        } else {
          processArticlesConversion(
            $selectedArticles,
            totalSelectedArticles,
            processedArticles
          );
        }
      } else {
        $(".wrap").prepend(
          '<div class="notice notice-error wppp-notice is-dismissible"> ' +
            "<p>" +
            can_convert.errors.play[0] +
            "</p>" +
            "</div>"
        );
        window.scrollTo(0, 0);
        playht__swal.fire({
          imageUrl: wppp_images.cancel,
          title: "Error!",
          text: can_convert.errors.play[0],
          confirmButtonText: "Close",
          confirmButtonClass: "wppp-modal--cancel-btn",
          animation: false,
        });
      }
    });

    $(document).on("click", "#play-open-draft-editor", async function () {
      if (1 == can_convert) {
        $(".wppp-notice").remove();
        var postId = $(this).data("postid");
        let voice =
          wppp_conv_data.posts &&
          wppp_conv_data.posts[postId] &&
          wppp_conv_data.posts[postId].voice
            ? wppp_conv_data.posts[postId].voice
            : $("#wppp_voice_meta option:selected").val();
        var narrationStyle = $("#wppp_voice_style_meta option:selected").val();
        var $selectedArticles = $(`tr#post-${postId}`).find(
          'input[type="checkbox"]'
        );

        if (!checkCreditsAllowance(1)) {
          window.__playht__analytics &&
            __playht__analytics(
              "playht__analytics.send",
              "event",
              "click",
              "no-credits",
              wppp_conv_data.article_url
            );
          // show not enough credits modal, buy more to continue
          playht__swal.fire({
            imageUrl: wppp_images.no_credits,
            title: "Sorry, you don't have any words left!",
            html:
              "<div class='__play-sw-modal-content'><div>Sorry, we are unable to add audio to your articles because you don’t have enough words left! Please purchase them from the link below and you will be able to add audio to your articles again :)</div><a href='https://play.ht/wordpress/upgrade' target='_blank' class='wppp-modal-ok-btn'>Get More Words</a></div>",
            animation: false,
            showConfirmButton: false,
          });
          return false;
        }

        showSchedulingModal(-1, 1, "Starting the Audio Editor");
        try {
          let postMetaData = null;
          let postContent = null;
          const { postData } = await getPostDataAndAuthor({ postId });
          postContent = postData
            ? Array.from($.parseHTML(postData.content.rendered)).map((el) =>
                parseHTMLText(el)
              )
            : null;
          postContent = postContent ? postContent
            .map((el) => el.trim().replace(/(?:\r\n|\r|\n)/g, " "))
            .map((el) => el.trim())
            .filter(Boolean) : null;
          try {
            postMetaData = await getPostMetaData(postId);
          } catch (error) {}
          const articleDataId = postMetaData
            ? postMetaData.play_article_id
            : wppp_conv_data.posts &&
              wppp_conv_data.posts[postId] &&
              wppp_conv_data.posts[postId].play_article_id;
          const title = postData
            ? postData.title.rendered
            : wppp_conv_data.posts &&
              wppp_conv_data.posts[postId] &&
              wppp_conv_data.posts[postId].title;

          const {
            articleSSMLContent,
            voice: v,
            narrationStyle: ns,
            wordsCount,
            isDraft,
          } = await openArticleEditor({
            articleDataId,
            postId,
            narrationStyle,
            voice,
            title,
            postContent,
          });
          processArticlesConversion(
            $selectedArticles,
            1,
            0,
            articleSSMLContent,
            v,
            ns,
            wordsCount,
            isDraft,
            postId
          );
        } catch (error) {
          console.log("error", error);
        }
      } else {
        $(".wrap").prepend(
          '<div class="notice notice-error wppp-notice is-dismissible"> ' +
            "<p>" +
            can_convert.errors.play[0] +
            "</p>" +
            "</div>"
        );
        window.scrollTo(0, 0);
        playht__swal.fire({
          imageUrl: wppp_images.cancel,
          title: "Error!",
          text: can_convert.errors.play[0],
          confirmButtonText: "Close",
          confirmButtonClass: "wppp-modal--cancel-btn",
          animation: false,
        });
      }
    });



    $(document).on("click", "#js__playht-delete-audio", async function () {
      try {
        var postId = $(this).data("postid");
        const confirmModal = await playht__swal.fire({
          imageUrl: wppp_images.warning,
          title: "Are you sure you want to delete this Audio?",
          text: "This action will remove the audio from this post",
          animation: false,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#d33",
          cancelButtonColor: "#3085d6",
          confirmButtonText: "Yes, delete it!",
        });
        if (!confirmModal.value) {
          playht__swal.closeModal();
          return;
        }
        playht__swal.showLoading();
        let postMetaData = null;
        try {
          postMetaData = await getPostMetaData(postId);
        } catch (error) {}
    
        // delete_article_draft
        await deleteAudio({ postId });

        const $element = $("#post-" + postId).find(".has_audio");
        showDoneStatusInColumn($element, "No");

        $del_item = $("#post-" + postId).find(".delete_podcast");
        removedeleteInColumn($del_item);

        playht__swal.fire(
          "Deleted!",
          "Your Audio has been deleted!",
          "success"
        );
      } catch (error) {
        console.log("TCL: error", error);
        playht__swal.fire(
          "Error!",
          "An Error has happened while deleting your Audio",
          "error"
        );
      }
    });
    
    $(document).on("click", "#play-delete-draft-link", async function () {
      try {
        var postId = $(this).data("postid");
        const confirmModal = await playht__swal.fire({
          imageUrl: wppp_images.warning,
          title: "Are you sure you want to delete this Draft?",
          text: "this action will remove all the audio edits in this Draft.",
          animation: false,
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, delete it!",
        });
        if (!confirmModal.value) {
          playht__swal.closeModal();
          return;
        }
        playht__swal.showLoading();
        let postMetaData = null;
        try {
          postMetaData = await getPostMetaData(postId);
        } catch (error) {}
        const articleId = postMetaData
          ? postMetaData.play_article_id
          : wppp_conv_data.posts &&
            wppp_conv_data.posts[postId] &&
            wppp_conv_data.posts[postId].play_article_id;

        const token = localStorage.getItem("playht_auth_token");
        const userId = wppp_retrieve.user_id;
        await $.ajax({
          type: "POST",
          url: "https://play.ht/api/deleteDraft",
          data: {
            token,
            userId,
            articleId,
          },
        });
        // delete_article_draft
        await deleteDraft({ postId });

        const $element = $("#post-" + postId).find(".has_audio");
        showDoneStatusInColumn($element, "No");

        playht__swal.fire(
          "Deleted!",
          "Your Draft has been deleted!",
          "success"
        );
      } catch (error) {
        console.log("TCL: error", error);
        playht__swal.fire(
          "Error!",
          "An Error has happened while deleting your draft, please try again or use <a target='_blank' href='https://play.ht/app/audio-files'>our dashboard to manage your audio files and drafts!</a>",
          "error"
        );
      }
    });
  });
})(window, jQuery);
