firebase.initializeApp({
  apiKey: "AIzaSyDIYBezlQ0hOhLvY2Wn0nPPhiNQsXdV-Js",
  authDomain: "play-68705.firebaseapp.com",
  databaseURL: "https://play-68705.firebaseio.com",
  storageBucket: "play-68705.appspot.com",
  cloudMessagingSenderId: "387630554879",
});

(function (w, $, undefined) {
  $(function () {
    $("body").append(
      '<iframe src="https://play.ht/getFirebaseUserIdToken_auth_iframe.html/" id="play-auth-iframe" style="width: 0px; height: 0px; display: none;"></iframe>'
    );
    var iframe = document.getElementById("play-auth-iframe");
    var tries = 0;
    var _askForToken = function () {
      if (tries == 10) {
        clearInterval(intrv);
        return;
      }
      tries++;
      iframe.contentWindow.postMessage({ id: "getPLAYHTUser" }, "*");

      var receiveMessage = function (e) {
        if (
          e.data &&
          e.data.id &&
          e.data.id == "PLAYHTUser" &&
          e.data.token &&
          e.data.user
        ) {
			clearInterval(intrv);
          idToken = e.data.token;
		  play_userid = e.data.user.uid;
		  localStorage.setItem('playht_auth_token', idToken);
        }
      };

      window.addEventListener("message", receiveMessage, false);
    };
    var intrv = setInterval(_askForToken, 1000);
  });
})(window, jQuery);
