<?php
do_action('befor_play_podcast');
if (get_option('playht_Listenbutton_switch', "1")) {
	$playHtDisplay = "inline-flex";
} else {
	$playHtDisplay = "none";
}
?>
<div class="playHtListenArea" style="display:none;margin: 0;">
	<div id="playht-audioplayer-element" data-play-article="<?php echo $article_url ?>" data-play-audio="<?php echo $article_audio ?>">
	</div>
</div>

<?php
do_action('after_play_podcast');
?>
