<div class="playht-custom-audio-wrapper" style="display: none !important;">
	<audio class="playht-audio-element" src="#" style="display: none !important;"></audio>
	<span class="playht-custom-audio__closeBtn" id="playht-closeBtn"></span>
	<span class="playht-custom-audio__playBtn">
		<img class="" src="/play-button.png" />
	</span>
	<span class="playht-custom-audio__played-time">00:00</span>
	<span class="playht-custom-audio__total" id="waveform"></span>
	<span class="playht-custom-audio__total-time">00:00</span>
   	<div class="playht-custom-audio__speed-range" id="playht-speed-range">
	    <span class="playht-custom-audio-range__value" id="playht-speed-rangeValue">1x</span>
	    <input class="playht-custom-audio-range__input" type="range" name="speed" min="0.5" max="2.5" step="0.1" value="1">
	</div>
    <div class="playht-custom-audio__brand">
		<a class="playht-custom-audio__brand-link" href="https://play.ht/app-landing/" target="_blank">
			powered by <span>Play<b>.ht</b></span>
		</a>
	</div>
</div>