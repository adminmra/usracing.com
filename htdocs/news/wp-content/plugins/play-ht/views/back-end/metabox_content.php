<!--sigle post PLAY metabox-->
<!--list of languages-->
<?php 
 use Wordpress\Play\Play_Requests; 
 ?>
<p style="text-align: center">
	<select style="width:80%" name="wppp_lang_meta" id="wppp_lang_meta">
		<option value="0"><?php _e( 'Select Language', WPPP_DOMAIN ); ?></option>
		<?php foreach ( Play_Requests::get_langs_list() as $language ) {  ?>
			<option value="<?php echo $language['value']; ?>"><?php echo $language['label']; ?></option>
		<?php } ?>
	</select>
</p>

<!--list of voices-->
<p style="text-align: center">
	<select style="width: 80%" name="wppp_voice_meta" id="wppp_voice_meta">
		<option value="0"><?php _e( 'Select Voice', WPPP_DOMAIN ); ?></option>
	</select>
</p>

<!--list of narrartion styles-->
<p style="text-align: center"  class="t-top t-lg full-width" id="wppp_voice_style_meta_wrapper" >
	<select style="width: 80%" name="wppp_voice_style_meta" id="wppp_voice_style_meta">
		<option value="0"><?php _e( 'Narration Style', WPPP_DOMAIN ); ?></option>
	</select>
</p>

<!--send conversion request-->
<p class="" style="text-align: center">
	<span class="spinner metabox-play-spinner" style="float: left;margin-right: -35px;"></span>
	<span id="<?php echo ( true !== $is_new_post ) ? 'convert_podcast_wrapper' : 'new_post_wrapper' ?>" class="t-bottom t-lg full-width" ><input type="button" id="<?php echo ( true !== $is_new_post ) ? 'convert_podcast' : 'new_post' ?>"
	       value="<?php _e( 'Convert to Audio', WPPP_DOMAIN ); ?>"
		   class="button button-primary"></span>
	<div class="converting-msg-wrapper"></div>
</p>

<p id="wppp_voice_try" style="text-align: center">
	<span class="voices__list-el">
		<audio id="audio_idforplay" preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ZPVzHtQhUQWXoql.mp3?generation=1522617177761625&alt=media" style="display: none;"></audio>
		<span class="voices__list-action">
			<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio_icon_play" width="16">
		</span>
		<span class="voices__list-title"></span>
	</span>
</p>
