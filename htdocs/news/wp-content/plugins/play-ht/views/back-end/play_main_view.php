<section class="playhtwp content">
	<div class="welcome_page_content">
		<h1><?php _e( 'Convert Your Articles To Audio ', WPPP_DOMAIN ); ?></h1>

		<div class="welcome-page-post welcome-text">
			<p>
				<?php _e( 'You will need first to Connect the WordPress Plugin with your Play.ht account, click the below button to get started.', WPPP_DOMAIN ); ?>
			</p>
				<a class="connect-btn js__playht-connect-button"
					href="<?php echo PLAY_URL . 'wordpress/login/?backurl=' . admin_url() . 'admin.php?page=play-welcome-page'; ?>"
					target="_blank"><?php _e( 'Get Started', WPPP_DOMAIN ); ?>
				</a>
			<div class="welcome-page-post__video">
				<script src="https://fast.wistia.com/embed/medias/dvaw1yq2hf.jsonp" async></script><script src="https://fast.wistia.com/assets/external/E-v1.js" async></script><div class="wistia_responsive_padding" style="padding:62.5% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><span class="wistia_embed wistia_async_dvaw1yq2hf popover=true popoverAnimateThumbnail=true videoFoam=true" style="display:inline-block;height:100%;width:100%">&nbsp;</span></div></div>
			</div>

		</div>

		<div class="welcome-page-post call-to-action">
			<h2><?php _e( 'Play adds a beautiful Embeddable audio player to your articles and generates an iTunes compatible auto-updated feed for your users', WPPP_DOMAIN ); ?> </h2>

			<iframe scrolling="no" height="175px" width="730px" class="app-ep-iframe" frameborder="0"
							src="https://play.ht/embed/?article_url=https://medium.com/personal-growth/21-behaviors-that-will-make-you-brilliant-at-creativity-relationships-c5bdb3d58a28"
							article-url="https://medium.com/personal-growth/21-behaviors-that-will-make-you-brilliant-at-creativity-relationships-c5bdb3d58a28"
							allowfullscreen=""></iframe>

		</div>
		<div class="welcome-page-voices">
			<div class="voices-section">
			<h2 class="section-title">Samples Of Our Voices (18 Languages, 60+ accents)</h2>
			<div class="voices-list">

			<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922AzTmsp3o6vbEHja.mp3?generation=1522617077599546&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-B"></span>
			<span class="voices__list-title">Robert (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
		<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L9221lipExv_CkzIPB2.mp3?generation=1522617039958470&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-A"></span>
			<span class="voices__list-title">James (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
		<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ZPVzHtQhUQWXoql.mp3?generation=1522617177761625&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-D"></span>
			<span class="voices__list-title">Richard (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
		<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922U22r6AkNB5neK_a.mp3?generation=1522617156348034&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-C"></span>
			<span class="voices__list-title">Patricia (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
		<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ccDPTBRkh2a-OPC.mp3?generation=1522617195128886&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-E"></span>
			<span class="voices__list-title">Elizabeth (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
		<div class="voices__list-el">
			<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922hv4G__UzXDtQiaK.mp3?generation=1522617216961872&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US-Wavenet-F"></span>
			<span class="voices__list-title">Linda (en-US)<br /><span class='play_premium_voice'>Premium Quality</span></span>
		</div>
					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KW9TW-SvS8uNwlZX_R-.mp3?generation=1518381457510829&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US_MichaelVoice"></span>
						<span class="voices__list-title">Michael (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/0-sample-mathew-speech_20171218030814983.mp3?alt=media&token=63ec628e-aeed-4f9b-b6ba-0d684f9d033e" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Matthew"></span>
						<span class="voices__list-title">Matthew (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVb-83aMMBMvrwu3dM.mp3?generation=1518483417835485&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US_AllisonVoice"></span>
						<span class="voices__list-title">Samantha (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVZd_WcSR924eXtVZX.mp3?generation=1518483411674118&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-US_LisaVoice"></span>
						<span class="voices__list-title">Lisa (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgV_vRa1kkRpHjNaTIp.mp3?generation=1518483415929395&alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-en-GB_KateVoice"></span>
						<span class="voices__list-title">Kate (en-UK)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVIl2_XCjOOW4SCvlr.mp3?generation=1490895832648000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Joey"></span>
						<span class="voices__list-title">Joey (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVhgobnKf5ImspJwLW.mp3?generation=1490902613862000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Joanna"></span>
						<span class="voices__list-title">Joanna (en-US)</span>
					</div>
					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVgmqm6Cd3so_R1rsw.mp3?generation=1490902327090000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Salli"></span>
						<span class="voices__list-title">Salli (en-US)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-KgVeFVXKvcJV5pTGeXT.mp3?generation=1490901753456656&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Brian"></span>
						<span class="voices__list-title">Brian (en-UK)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMZR3Na89X_Jxu7-Z.mp3?generation=1490896816185000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Emma"></span>
						<span class="voices__list-title">Emma (en-UK)</span>
					</div>

					<div class="voices__list-el">
						<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMvY3PjkOOHLo3rwH.mp3?generation=1490896906472000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Amy"></span>
						<span class="voices__list-title">Amy (en-UK)</span>
					</div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVMJ3aErhvcJdro38R.mp3?generation=1490896731578000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Raveena"></span>
					<span class="voices__list-title">Raveena (en-India)</span></div>

					<div class="voices__list-el">

					<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVLtlohVq67Sz7hw-x.mp3?generation=1490896629761000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Geraint"></span>
					<span class="voices__list-title">Geraint (en-Welsh)</span></div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVNWvbLR0CcfBa6BJN.mp3?generation=1490897260103000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Nicole"></span>
					<span class="voices__list-title">Nicole (en-Australian)</span></div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-testing-412e4.appspot.com/o/full_-KgVNOsZztJzvF0enGsT.mp3?generation=1490897242061000&amp;alt=media" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Russell"></span>
					<span class="voices__list-title">Russell (en-Australian)</span></div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a2.mp3?alt=media&amp;token=082280a9-be29-413f-bd54-be78d5b36eb2" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-de-DE_BirgitVoice"></span>
					<span class="voices__list-title">Birgit (de-German)</span>
					</div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a3.mp3?alt=media&amp;token=32853e26-742c-4fb5-b43a-699f51b920a6" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-de-DE_DieterVoice"></span>
					<span class="voices__list-title">Dieter (de-German)</span>
					</div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a1.mp3?alt=media&amp;token=9eb1e854-969b-4d5b-9c61-f7a5340c7c62" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Hans"></span>
					<span class="voices__list-title">Hans (de-German)</span>
					</div>

					<div class="voices__list-el">
					<audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a.mp3?alt=media&amp;token=18ba5695-92fb-4605-b0f8-94f70b529a74" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Marlene"></span>
					<span class="voices__list-title">Marlene (de-German)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/vicki.mp3?alt=media&amp;token=dba4480d-1ad8-44fc-82a4-b006d95fc2f9" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Vicki"></span>
					<span class="voices__list-title">Vicki (de-German)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/a4.mp3?alt=media&amp;token=dd7b1f44-7690-4efe-967c-7f96cf73f530" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-fr-FR_ReneeVoice"></span>
					<span class="voices__list-title">Renee (fr-French)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/celine.mp3?alt=media&amp;token=c1c2518f-248f-4fad-802e-648debf8fc2c" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Celine"></span>
					<span class="voices__list-title">Celine (fr-French)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/mathieu.mp3?alt=media&amp;token=b5cace53-24a1-446f-8616-a312319e4cba" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Mathieu"></span>
					<span class="voices__list-title">Mathieu (fr-French)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/transcript_3_.mp3?alt=media&amp;token=6d6392ad-59c0-4a54-8393-7f6b8c1b9c42" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-pt-BR_IsabelaVoice"></span>
					<span class="voices__list-title">Isabela (pt-Brazilian Portuguese)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/ricardo.mp3?alt=media&amp;token=6478fc45-88da-4232-b47c-f49005d1ea4c" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Ricardo"></span>
					<span class="voices__list-title">Ricardo (pt-Brazilian Portuguese)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/vitro.mp3?alt=media&amp;token=01d80348-4a0e-4399-b06a-04e5f86f72d4" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Vitoria"></span>
					<span class="voices__list-title">Vitoria (pt-Brazilian Portuguese)</span>
					</div>


					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/cristiano-pr.mp3?alt=media&amp;token=e6bf7009-4369-4f58-aa10-f7b25dbb54d1" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Cristiano"></span>
					<span class="voices__list-title">Cristiano (pt-European Portuguese)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/ines-pr.mp3?alt=media&amp;token=4eae6877-1c51-4427-8083-6caab3469b0f" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Ines"></span>
					<span class="voices__list-title">Ines (pt-European Portuguese)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/maxim.mp3?alt=media&amp;token=a0bb06cc-2e30-49e7-ad38-2b15fde919b1" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Maxim"></span>
					<span class="voices__list-title">Maxim (ru-Russian)</span>
					</div>

					<div class="voices__list-el"><audio preload="metadata" src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/tatayana.mp3?alt=media&amp;token=e8c3aa94-c9dc-4867-8e28-e0e680ab2883" style="display: none;"></audio><span class="voices__list-action"><img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Tatyana"></span>
					<span class="voices__list-title">Tatyana (ru-Russian)</span>
					</div>

					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FFiliz.mp3?alt=media&token=b949ca8f-fd70-4acf-a55f-2caff0f24ebc"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Filiz" />
					</span>
					<span class="voices__list-title">Filiz (tr-Turkish)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fja-JP_EmiVoice.mp3?alt=media&token=4b9611ef-df49-4622-aa9b-a48a7980a3d8"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-ja-JP_EmiVoice" />
					</span>
					<span class="voices__list-title">Emi (jp-Japanese)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FMizuki.mp3?alt=media&token=92639bd0-0467-43c6-b1a9-a005b2894fb9"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Mizuki" />
					</span>
					<span class="voices__list-title">Mizuki (jp-Japanese)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fes-ES_EnriqueVoice.mp3?alt=media&token=09c55b24-4a9d-47c3-84da-7e63f548c13b"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-es-ES_EnriqueVoice" />
					</span>
					<span class="voices__list-title">Diego (es-Spanish - Castilian)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2Fes-ES_LauraVoice.mp3?alt=media&token=fcb27805-0a7d-4902-bdd3-3300f639d503"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-es-ES_LauraVoice" />
					</span>
					<span class="voices__list-title">Laura (es-Spanish - Castilian)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FConchita.mp3?alt=media&token=eddf8f9f-1955-4c64-b9f3-eba108bb44cf"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Conchita" />
					</span>
					<span class="voices__list-title">Conchita (es-Spanish - Castilian)</span>
					</div>
					<div class="voices__list-el">
					<audio preload='metadata' src="https://firebasestorage.googleapis.com/v0/b/play-68705.appspot.com/o/samples%2FEnrique.mp3?alt=media&token=d63e84ae-0956-4aa1-ac4e-d1a632ed6314"
						preload="none" style="display: none;"></audio>
					<span class="voices__list-action">
						<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio-Enrique" />
					</span>
					<span class="voices__list-title">Enrique (es-Spanish - Castilian)</span>
					</div>
			</div>
				</div>
		</div>
	</div>

</section>