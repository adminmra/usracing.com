<?php use Wordpress\Play\Play_Requests; ?>

<span id="play_bulk_convert" class="playhtwp">
	<select name="wppp_lang_meta" id="wppp_lang_meta">
		<option value="0"><?php _e( 'Select Language', WPPP_DOMAIN ); ?></option>
		<?php foreach ( Play_Requests::get_langs_list() as $language ) { ?>
			<option value="<?php echo $language['value']; ?>"><?php echo $language['label']; ?></option>
		<?php } ?>
	</select>
	<select name="wppp_voice_meta" id="wppp_voice_meta">
		<option value="0"><?php _e( 'Select Voice', WPPP_DOMAIN ); ?></option>
	</select>
	
	<span id="wppp_voice_style_meta_wrapper" class="t-top t-lg full-width"><select name="wppp_voice_style_meta" id="wppp_voice_style_meta">
		<option value="0"><?php _e( 'Narration Style', WPPP_DOMAIN ); ?></option>
	</select></span>

	<span id="multiple_conversion_wrapper" class="t-top t-lg full-width" >
		<input type="button" id="multiple_conversion" value="<?php _e( 'Convert to Audio', WPPP_DOMAIN ); ?>" class="button button-primary">
	</span>
	<span id="wppp_voice_try">
		<span class="voices__list-el">
			<audio id="audio_idforplay" preload="metadata" src="https://www.googleapis.com/download/storage/v1/b/play-68705.appspot.com/o/full_-L922ZPVzHtQhUQWXoql.mp3?generation=1522617177761625&alt=media" style="display: none;"></audio>
			<span class="voices__list-action">
				<img class="voice-icon" src="https://play.ht/app-landing/images/play-btn.png" id="audio_icon_play" width="16">
			</span>
			<span class="voices__list-title"></span>
		</span>
	</span>
</span>