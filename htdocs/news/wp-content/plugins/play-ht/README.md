=== Play.ht - Convert Articles to Podcasts ===
Contributors: hammadh
Tags: voice over, audio articles, podcasts, text to audio, seo
Author: Play.ht
Requires at least: 3.0.1
Tested up to: 5.4
Stable tag: 3.0.9

Play.ht converts your blog posts into audio podcasts so your users can listen to them. Play.ht also provides tools like embedded audio players and RSS feeds to engage and grow your users.

== Description ==

This plugin converts your articles into high quality audio narrations so your users can listen to your content instead of reading it.

The plugin renders a simple yet beautiful Embedded Player at the top of each article that users can use to 'play' the article. We have found
that a lot of users like to listen while reading, this dramatically increases user engagement of your website. The meta data contained inside the
Embedded Player also accounts for SEO.

Users while listening to an article have the ability to increase or reduce listening speed, giving them more control and convenience on consuming content.
The Embedded Player also increase share-ability of your article with it's built-in sharing options to Twitter, Facebook and Email.

If you would like to offer your users a more convenient way of consuming your content and engage your users, then you should give this plugin a try.

== Installation ==

Installation is easy and just like other plugins. Please follow these steps and you
should have the plugin installed.

1. Install the Play.ht plugin.
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to 'Appearance' -> 'Widgets'
4. Add the Play.ht Embedded Player widget to any section of your theme (sidebar or footer or where ever)

== Frequently Asked Questions ==

= Is Play.ht a free service? =

Play.ht is a paid service. You can sign up and purchase your subscription at https://play.ht/pricing/

= Does it work for all my articles? =

Yes, once you activate the Play.ht embedded audio player widget, it works for all the articles.

= Can I change the design of the embedded player? =

Yes, you can customize the embedded audio player to match the look and feel of your website.

= Can I change the voice of narration? =

Yes, you can change the voice anytime you wish.

= Can I white label the embedded audio player?

Yes, with a paid account you will be able to white label or even brand the audio player as your own.

== Screenshots ==

1. Play embeds an Audio Player in your Articles so users can listen to them as Podcasts.
2. Play allows  users to listen to Articles from the Home Page of your blog too!

== Changelog ==

= 3.0.9 =
* Added Drafts!
* Fixed issues with Thrive themes
* A couple of other fixes and improvements

= 3.0.8 =
* Fixed an issue with Article Player giving error after embedding
* Updated the voices list
* Added a loader for editor

= 3.0.7 =
* Fixing an issue with audio player customization

= 3.0.6 =
* Important fix where in some cases words were not taken into account

= 3.0.5 =
* Made the system Word based!

= 3.0.4 =
* Fixed the 'Uh no, something went wrong' issue
* Added delete audio for pages

= 3.0.3 =
* Fix wanrings being displayed
* Fix pagination on post list page
* More obvious onboarding

= 3.0.2 =
* Page player white label fix

= 3.0.1 =
* Fix Appsumo credits issue

= 3.0.0 =
* Support for Wordpress Pages
* Support for Custom Post Types
* Fixes for Gutenberg

= 2.9.8 =
* Fix audio for 'questions' in Learndash

= 2.9.7 =
* Add support for 'questions' in Learndash

= 2.9.6 =
* Added crisp
* Fixed duplicate events
* Fixed issues with not being able to convert articles to audio

= 2.9.4 =
* Removed 'Liten' buttons from post list

= 2.9.3 =
* Fixing loading of text in the editor

= 2.9.2 =
* Fixing issues with the editor
* Fix Convert button

= 2.9.1 =
* Fixing issues with the editor
* Error reporting using sentry

= 2.9.0 =
* Adding a Text to Voice-Over editor!
* Fix bugs with Stats

= 2.8.1 =
* Fixing minor bug where error would get displayed on the webpage

= 2.8.0 =
* Adding new voices!

= 2.7.0 =
* Fix issues with player icons

= 2.6.9 =
* Fix conflicts with Divi editor

= 2.6.8 =
* Fix audio player not showing by default

= 2.6.7 =
* More fixes for Learnpress + fixes for analytics + email subscribers tab

= 2.6.6 =
* Fix conflict with Learnpress plugin

= 2.6.5 =
* Fixes and improvements

= 2.6.4 =
* Improvements to stats chart

= 2.6.3 =
* Improvements to stats chart

= 2.6.2 =
* Fixed an issue where post content was being deleted at rare occassions

= 2.6.1 =
* Fixes for Gutenberg editor

= 2.6 =
* Added Page Player

= 2.5.3 =
* Update readme.txt

= 2.5.2 =
* Bug fixes

= 2.5.1 =
* Fixed height of the audio player  on some blogs

= 2.5.0 =
* Added dashboard for analytics and audio player customizations

= 2.4.0 =
* Major bug fixes

= 2.0 =
* Big update

= 1.0 =
* This is the first stable release.

== Upgrade Notice ==

= 2.5.3 =
* This update provides improved stability and a lot of bug fixes to the audio players.

= 1.0 =
* Your plugin is up to date.


== Features ==

These are some of the features this plugin provides:

1. Increases user engagement on website.
2. Converts articles into audio for users to listen to your content.
3. Increases the time users spend on your website
4. Increases SEO with meta data provided by Embedded Player
5. Provides easy shareablity of your articles as audio on Twitter & Facebook
