<?php namespace Wordpress\Play;

/**
 * Plugin Name: Play.ht
 * Description: Play.ht for Wordpress is a magical tool for converting your blog posts, pages and courses to audio to increase content accessibility, user engagement and time on page metrics.
 * Version: 3.0.16
 * Author: Play.ht
 * Author URI: https://play.ht/wordpress/?utm_source=wordpress&utm_medium=plugin&utm_campaign=plugin-page
 * Text Domain: play-ht
 * Domain Path: /language
 */

if ( ! defined( 'WPINC' ) ) {
	// Exit if accessed directly
	die();
}

/**
 * Constants
 */

// plugin master file
define( 'WPPP_MAIN_FILE', __FILE__ );

// plugin DIR
define( 'WPPP_DIR', plugin_dir_path( WPPP_MAIN_FILE ) );

// plugin URI
define( 'WPPP_URI', plugin_dir_url( WPPP_MAIN_FILE ) );

// localization text Domain
define( 'WPPP_DOMAIN', 'play-domain' );

define( 'PLAY_URL', "https://play.ht/" );
// define( 'PLAY_URL', "https://localhost:8080/" );

//URLs to be later used in JS audio conversion
define( 'Conversion_URL', PLAY_URL . 'api/transcripe' );
define( 'Check_Audio_URL', PLAY_URL . 'api/getArticleAudioObjectByURL' );
define( 'Add_Origin_URL', PLAY_URL . 'api/addOrigin' );
define( 'REFERRAL_URL', PLAY_URL . 'api/plugin/saveReferralSource' );
// Play contact mail
define( 'Play_contact_mail', 'support@play.ht' );

require_once WPPP_DIR . 'includes/classes/Singular.php';
require_once WPPP_DIR . 'includes/helpers.php';
require_once WPPP_DIR . 'includes/functions.php';
require_once WPPP_DIR . 'includes/settings.php';

/**
 * Plugin main component
 *
 * @package Wordpress\Play
 */
class Plugin extends Singular {
	/**
	 * Plugin version
	 *
	 * @var string
	 */
	var $version = '3.0.16';
	/**
	 * DB version
	 *
	 * @
	 */
	var $db_version = '1.1.0';

	/**
	 * Backend
	 *
	 * @var Backend
	 */
	var $backend;

	/**
	 * Backend
	 *
	 * @var Frontend
	 */
	var $frontend;

	/**
	 * Backend
	 *
	 * @var Ajax_Handler
	 */
	var $ajax;

	/**
	 * Notice
	 *
	 * @var Play_Admin_notice
	 */
	var $play_notice;

	/**
	 * Request
	 *
	 * @var Play_Requests
	 */
	var $play_requests;

	/**
	 * Initialization
	 *
	 * @return void
	 */
	protected function init()
	{
		// load language files
		add_action( 'plugins_loaded', [ &$this, 'load_language' ] );

		// autoloader register
		spl_autoload_register( [ &$this, 'autoloader' ] );

		// modules
		$this->ajax          = Ajax_Handler::get_instance();
		$this->backend       = Backend::get_instance();
		$this->frontend      = Frontend::get_instance();
		$this->play_notice   = Play_Admin_notice::get_instance();
		$this->play_requests = Play_Requests::get_instance();

		// plugin loaded hook
		do_action_ref_array( 'wppp_loaded', [ &$this ] );
	}

	/**
	 * Load view template
	 *
	 * @param string $view_name
	 * @param array  $args ( optional )
	 *
	 * @return void
	 */
	public function load_view( $view_name, $args = null )
	{
		// build view file path
		$__view_name     = $view_name;
		$__template_path = WPPP_DIR . 'views/' . $__view_name . '.php';
		if ( ! file_exists( $__template_path ) ) {
			// file not found!
			wp_die( sprintf( __( 'Template <code>%s</code> File not found, calculated path: <code>%s</code>', WPPP_DOMAIN ), $__view_name, $__template_path ) );
		}

		// clear vars
		unset( $view_name );

		if ( ! empty( $args ) ) {
			// extract passed args into variables
			extract( $args, EXTR_OVERWRITE );
		}

		/**
		 * Before loading template hook
		 *
		 * @param string $__template_path
		 * @param string $__view_name
		 */
		do_action_ref_array( 'wppp_load_template_before', [ &$__template_path, $__view_name, $args ] );

		/**
		 * Loading template file path filter
		 *
		 * @param string $__template_path
		 * @param string $__view_name
		 *
		 * @return string
		 */
		require apply_filters( 'wppp_load_template_path', $__template_path, $__view_name, $args );

		/**
		 * After loading template hook
		 *
		 * @param string $__template_path
		 * @param string $__view_name
		 */
		do_action( 'wppp_load_template_after', $__template_path, $__view_name, $args );
	}

	/**
	 * Language file loading
	 *
	 * @return void
	 */
	public function load_language()
	{
		load_plugin_textdomain( WPPP_DOMAIN, false, dirname( plugin_basename( WPPP_MAIN_FILE ) ) . '/languages' );
	}

	/**
	 * System classes loader
	 *
	 * @param $class_name
	 *
	 * @return void
	 */
	public function autoloader( $class_name )
	{
		if ( strpos( $class_name, __NAMESPACE__ ) === false ) {
			// skip non related classes
			return;
		}

		$class_path = WPPP_DIR . 'includes' . DIRECTORY_SEPARATOR . 'classes' . str_replace( [
				__NAMESPACE__,
				'\\',
			], [ '', DIRECTORY_SEPARATOR ], $class_name ) . '.php';

		if ( file_exists( $class_path ) ) {
			// load class file if found
			require_once $class_path;
		}
	}
}

// boot up the system
wp_play();