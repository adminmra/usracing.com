<?php die( 'These aren\'t the droids you\'re looking for.' );

/**
 * Extract of translatable text strings from the static config array.
 */
_x( '(code) Schema Type Article [schema_type:article]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Audiobook [schema_type:book.audio]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Blog [schema_type:blog]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Book [schema_type:book]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Brand [schema_type:brand]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Claim Review [schema_type:review.claim]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Collection Page [schema_type:webpage.collection]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Course [schema_type:course]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type CreativeWork [schema_type:creative.work]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Event [schema_type:event]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type FAQPage [schema_type:webpage.faq]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Food Establishment [schema_type:food.establishment]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type How-To [schema_type:how.to]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type ItemList [schema_type:item.list]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Job Posting [schema_type:job.posting]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Local Business [schema_type:local.business]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Movie [schema_type:movie]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Organization [schema_type:organization]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Person [schema_type:person]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Place [schema_type:place]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Product [schema_type:product]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Profile Page [schema_type:webpage.profile]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type QAPage [schema_type:webpage.qa]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Question and Answer [schema_type:question]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Recipe [schema_type:recipe]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Review [schema_type:review]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Search Results Page [schema_type:webpage.search.results]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Software Application [schema_type:software.application]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type Thing [schema_type:thing]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type WebPage [schema_type:webpage]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(code) Schema Type WebSite [schema_type:website]', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(plus) Schema Property aggregateRating', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(plus) Schema Property hasPart', 'lib file description', 'wpsso-schema-json-ld' );
_x( '(plus) Schema Property review', 'lib file description', 'wpsso-schema-json-ld' );
_x( 'Schema Markup', 'lib file description', 'wpsso-schema-json-ld' );
_x( 'Schema Shortcode Guide', 'lib file description', 'wpsso-schema-json-ld' );
_x( 'Google Rich Results and Structured Data for Articles, Carousels (aka Item Lists), Claim Reviews, Events, FAQ Pages, How-Tos, Images, Local Business / Local SEO, Organizations, Products, Ratings, Recipes, Restaurants, Reviews, Videos, and More.', 'plugin description', 'wpsso-schema-json-ld' );
