<?php
//error_reporting( E_ALL );
//ini_set( "display_errors" , 1 );
ini_set("error_log", "/home/ah/news.usracing.com/amp-php-error.log");
class GeneratorAMPCache{
	private $wp_load_path;
	private $AMPDIRNAME;

	
	public function __construct() {
		$this->wp_load_path    = "/home/ah/news.usracing.com/wp-load.php";
		$this->AMPDIRNAME = "/home/ah/usracing.com/htdocs/news/amp/";
		$this->DIRNAME         = "/home/ah/usracing.com/htdocs/news/";
	}
	
	
	private function get_content_news( $url_news ){
		$curl = curl_init();
		curl_setopt( $curl , CURLOPT_URL , $url_news );
		curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true );
		curl_setopt( $curl , CURLOPT_HEADER , false );
		$out = curl_exec( $curl );
		$httpCode = curl_getinfo( $curl , CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ( ! $out || $httpCode != 200 ) {
			return false;
		}
		$out = $this->string_replace( $out );
		return $out;
	}
	
	private	function string_replace( $content ){
		$content = str_replace( "http%3A%2F%2Fnews.usracing.com/news" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http://news.usracing.com/news"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http:\\/\\/news.usracing.com/news"   , "https:\\/\\/www.usracing.com\\/news" , $content );

		$content = str_replace( "https%3A%2F%2Fnews.usracing.com" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "https://news.usracing.com"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "https:\\/\\/news.usracing.com"   , "https:\\/\\/www.usracing.com\\/news" , $content );
		
		$content = str_replace( "http%3A%2F%2Fnews.usracing.com" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http://news.usracing.com"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http:\\/\\/news.usracing.com"   , "https:\\/\\/www.usracing.com\\/news" , $content );		

		$content = str_replace( "http%3A%2F%2Fnewsdev.usracing.com/news" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http://newsdev.usracing.com/news"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "http:\\/\\/newsdev.usracing.com/news"   , "https:\\/\\/www.usracing.com\\/news" , $content );

		$content = str_replace( "https%3A%2F%2Fnewsdev.usracing.com" , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "https://newsdev.usracing.com"       , "https://www.usracing.com/news"       , $content );
		$content = str_replace( "https:\\/\\/newsdev.usracing.com"   , "https:\\/\\/www.usracing.com\\/news" , $content );
		
		$content = str_replace( "http://news.usracing.localhost:90" , "https://www.usracing.com/news" , $content );


		$content = str_replace( "http://www.usracing.com/"        , "https://www.usracing.com"            , $content );
		$content = str_replace( "http://1.gravatar"              , "https://1.gravatar" , $content );

				/// delete wp-json api
		$content = preg_replace( "@wp-json.*\"@"   , "\""    , $content );
		$content = preg_replace( "@wp-json.*\'@"   , "\'"    , $content );
		$content = preg_replace( "@/news/\?p=\d+@" , "/news" , $content );
		$content = str_replace( "cing.comnews" , "cing.com/news" , $content );

		return $content;
	}
	
	public function gen_pages_amp($url_news, $DataArticle, $caturl){
		
		$file_tpl_content= $this->get_content_news($url_news);
 		$file_tpl_path="/home/ah/usracing.com/htdocs/news/amp/".$caturl.$DataArticle["post_name"].".php";
		//$file_tpl_path="/home/ah/usracing.com/news/amp/".$caturl.$DataArticle["post_name"].".php";
		$ampurl="https://www.usracing.com/news/amp/".$caturl.$DataArticle["post_name"];
		$canonical="https://www.usracing.com/news/".$caturl.$DataArticle["post_name"];
		
		
		if(strlen($caturl) > 0){
		$path= 	"/home/ah/usracing.com/htdocs/news/amp/".$caturl;
			if (!is_dir( $path ) ) {
			mkdir( $path , 0755 , true );
			}
		}
		
		
		require_once('/home/ah/usracing.com/htdocs/news/simple_html_dom.php');
		//$html= new simple_html_dom();
		$dom_content = str_get_html($file_tpl_content);
		$descschema="";
		//$descschema=$dom_content->find("meta[name=description]", 0)->content;
		//print_r($descschema);
		$metas = $dom_content->find('meta');
	
		$dom_str = '';
		if (!(isset($amp_vars['metas']))) {
			foreach($metas as $meta){$dom_str .= $meta; }
            $dom_str = str_replace('http://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('https://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('//news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('/news.usracing.com', '/www.usracing.com', $dom_str);
			$amp_vars['metas'] = addcslashes($dom_str,"'");
		}

	//print_r($vars['metas']);

	
	$articles = $dom_content->find('article');
			$dom_str = '';
		if (!(isset($amp_vars['article']))) {
			foreach($articles as $article){$dom_str .= $article; }
            $dom_str = str_replace('http://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('https://news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('//news.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('/news.usracing.com', '/www.usracing.com', $dom_str);
            $dom_str = str_replace('http://newsdev.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('https://newsdev.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('//newsdev.usracing.com', '//www.usracing.com', $dom_str);
            $dom_str = str_replace('/newsdev.usracing.com', '/www.usracing.com', $dom_str);			
			$dom_str = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd"> <html><body>', '', $dom_str);
			$dom_str = str_replace('</body></html>', '', $dom_str);
			$dom_str = str_replace('loading="lazy"', '', $dom_str);
			$dom_str = preg_replace('/style=\"[^\"]*\"/', '', $dom_str);
			$dom_str = preg_replace('/<style>(.*?)\<\/style>/', '', $dom_str);
			$dom_str = preg_replace('/<script(.*?)>(.*?)\<\/script>/', '', $dom_str);
			//$dom_str = preg_replace('/sizes=\"[^\"]*\"/', 'layout="responsive"', $dom_str);
			/*$dom_str = preg_replace('/<img(.*?)\/?>/', '<amp-img$1 ></amp-img>',$dom_str);*/
			$dom_str= preg_replace_callback('~<img.*?aria-describedby=["\']+(.*?)["\']+.*?src=["\']+(.*?)["\']+(.*?)\/?>~', array(&$this, 'replaceIMG'),$dom_str);
			$dom_str= preg_replace_callback('/<img(.*?)\/?>/', array(&$this, 'genAMPIMG'),$dom_str);		
			$dom_str = preg_replace('/<img(.*?)\/?>/', '<amp-img$1 layout="responsive" data-amp-auto-lightbox-disable ></amp-img>',$dom_str);
			$dom_str = preg_replace('/<th>(.*?)<i(.*?)>\<\/i>\<\/th>/', '<th>$1</th>',$dom_str);
			$dom_str = preg_replace('/summary=\"[^\"]*\"/', '', $dom_str);
			
			//$dom_str = preg_replace('/<div(.*?)id=\"(.*?)\"(.*?)class=\"wp-caption(.*?)\">(.*?)<\/div>/i', '<div class="row" ><div$1id="$2"$3class="wp-caption$4">$5</div></div>',$dom_str);
			$dom_str = preg_replace('/<table(.*?)>(.*?)<\/table>/i', '<div id="no-more-tables" ><table$1>$2</table></div>',$dom_str);
			$dom_str = str_replace('<div class="saboxplugin-desc">','<amp-truncate-text layout="fixed-height" height="15em" ><div class="saboxplugin-desc">', $dom_str); 
			$dom_str = str_replace("</div><div class='apss-social-share apss-theme-2 clearfix' >", '<a slot="collapsed" class="readlink" >Read More</a> <a slot="expanded" class="readmore">Read Less</a></amp-truncate-text> </div><div class="apss-social-share apss-theme-2 clearfix" ><div  class="apss-social-center" >', $dom_str);
			$dom_str = preg_replace("#<div class='apss-email apss-single-icon'>(.*?)</div>#", "<div class='apss-email apss-single-icon'>$1</div></div>", $dom_str);
				
				
			$article_content = str_get_html($dom_str);
         		$iframes = $article_content->find('iframe');
				foreach($iframes as $iframe)
				   {
					 $Iframesrc=$iframe->src;
					 if($this->isYouTubeIframe($Iframesrc)){
						 $utubevideo=$this->extract_youtube_video_id($Iframesrc);
						 $ampUtube=$this->amp_youtube_html( $utubevideo);
						 //$dom_str = preg_replace('/<iframe(.*?)\>(.*?)\<\/iframe>/', $ampUtube,$dom_str);
						 $dom_str = preg_replace('/<iframe[^>]+>.*?<\/iframe>/', $ampUtube,$dom_str);						 
					 }else{
						 
						 $iframeReplace='<amp-iframe width="200" height="100"
											sandbox="allow-scripts allow-same-origin"
											layout="responsive"
											frameborder="0"
											src="'.$Iframesrc.'">
										</amp-iframe>';
						$dom_str = preg_replace('/<iframe[^>]+>.*?<\/iframe>/', $iframeReplace,$dom_str);						 
					 }
			
				   }
				   
			$amp_vars['article'] = $dom_str;
		}
	

	
	$postDates=explode(" ",$DataArticle["post_date"]);
	$PostDate=explode("-",$postDates[0]);
	$printdate=date("F j, Y", mktime(0,0,0,$PostDate[1], $PostDate[2], $PostDate[0]));
	
	$amp_vars["structure"]='<script type="application/ld+json">
{
      "@context": "http://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": "'.$ampurl.'",
      "headline": "'.htmlentities($DataArticle["post_title"], ENT_QUOTES).'",
      "datePublished": "'.$DataArticle["post_date_gmt"].'",
      "dateModified": "'.$DataArticle["post_modified_gmt"].'",
      "description": "'.$descschema.'",
      "author": {
        "@type": "Person",
        "name": "'.$DataArticle["avtar"].'"
      },
      "publisher": {
        "@type": "Organization",
        "name": "US Racing",
        "logo": {
          "@type": "ImageObject",
          "url": "https://www.usracing.com/img/us-racing.png",
          "width": 508,
          "height": 103
        }
      },
      "image": {
        "@type": "ImageObject",
        "url": "'.$this->string_replace($DataArticle["imgDetail"]).'"
      }
    }
      </script>';
	
	
	$file_tpl_content = "";
		$file_tpl_content .= '<?php header("Content-Type: text/html; charset=utf-8"); ?>'."\n";
		$file_tpl_content .= '<?php $amp_vars["metas"]=\''.$amp_vars["metas"].'\'; ?>'."\n";
		$file_tpl_content .= '<?php $amp_vars["structure"]=\''.$amp_vars["structure"].'\'; ?>'."\n";
		$file_tpl_content .= '<?php $canonical=\''.$canonical.'\'; ?>'."\n";
		$file_tpl_content .= '<?php $pagetitle=\''.htmlentities($DataArticle["post_title"], ENT_QUOTES).'\'; ?>'."\n";				
		$file_tpl_content .= '<?php include("/home/ah/usracing.com/htdocs/news/layout/amp/header.php"); ?>'."\n";
	    $file_tpl_content .= '<div class="headline"><h1>'.$DataArticle["post_title"].'</h1></div>';
		/*$file_tpl_content .= ' <div class="blog-date-author"><span class="author-by" >By '.$DataArticle["avtar"].'</span><br /><time class="entry-date published" datetime="'.str_replace(" ","T",$DataArticle["post_date_gmt"]).'+00:00">'.$printdate.'</time></span></span></div>'; */
		$file_tpl_content .= ' <div class="blog-date-author"><time class="entry-date published" datetime="'.str_replace(" ","T",$DataArticle["post_date_gmt"]).'+00:00">'.$printdate.'</time></span></span></div>'; 	
		$file_tpl_content .= $amp_vars['article'];
				

		$file_tpl_content .= '<?php include("/home/ah/usracing.com/htdocs/news/layout/amp/footer.php"); ?>'."\n";


	file_put_contents( $file_tpl_path , $file_tpl_content ) ;
	
	}
	
	public function replaceIMG($matches){
		$getIMGId=explode("-",$matches[1]);
		//print_r($matches);
		$IMgDetails=wp_get_attachment_image_src($getIMGId[2], "medium_large");
		$IMGsrcReplace=str_replace('http://news.usracing.com/','//usracing.com/news/',$IMgDetails[0]);
		$IMGsrcReplace=str_replace('https://news.usracing.com/','//usracing.com/news/',$IMgDetails[0]);
		$IMGsrcReplace=str_replace('http://newsdev.usracing.com/','//usracing.com/news/',$IMgDetails[0]);
		$IMGsrcReplace=str_replace('https://newsdev.usracing.com/','//usracing.com/news/',$IMgDetails[0]);
		
		$IMGsrcReplace=str_replace('http%3A%2F%2Fnews.usracing.com','//usracing.com/news',$IMgDetails[0]);
		$IMGsrcReplace=str_replace('http://news.usracing.com','//usracing.com/news',$IMGsrcReplace);
		$IMGsrcReplace=str_replace('http:\\/\\/news.usracing.com','//usracing.com/news',$IMGsrcReplace);				
	

		
		return '<img aria-describedby="'.$matches[1].'" src="'.$IMGsrcReplace.'" width="'.$IMgDetails[1].'" height="'.$IMgDetails[2].'"  >';
	}
	
	public function genAMPIMG($matches){
		if(preg_match('~width=["\']+(.*?)["\']+(.*?)height=["\']+(.*?)["\']+~',$matches[1])){
			return '<img  '.$matches[1].' >';
		}else{ return '<img  width="100" height="100" '.$matches[1].' >'; }
	}
		
	protected function isYouTubeIframe($href)
    {
        if (empty($href)) {
            return false;
        }

        if (preg_match('&(*UTF8)(youtube\.com|youtu\.be)&i', $href)) {
            return true;
        }

        return false;
    }
	
	
	public static function extract_youtube_video_id( $url ) {
		$video_id = '';
		if ( preg_match( '#https?://(?:(?:m|www)\.)?youtube\.com/watch\?(.*)#i', $url, $matched ) ) {
			parse_str( $matched[1], $vars );
			if ( ! empty( $vars['v'] ) ) {
				$video_id = $vars['v'];
			}
		} elseif ( preg_match( '#https?://(?:(?:m|www)\.)?youtube\.com/embed/([^\/\?\&]+)#i', $url, $matched ) ) {
			$video_id = $matched[1];
		} elseif ( preg_match( '#https?://(?:(?:m|www)\.)?youtu\.be/([^\/\?\&]+)#i', $url, $matched ) ) {
			$video_id = $matched[1];
		}
		return $video_id;
	}
	
	public function amp_youtube_html( $video_id, $height = 270, $width = 480 ) {
		return sprintf( '<amp-youtube width="%d" height="%d" layout="responsive" data-videoid="%s"></amp-youtube>', $width, $height, $video_id );
	}
	
	public function post_cache_by_id($ID){
		$recent = get_post( $ID, ARRAY_A );
	 	$avtar=get_the_author_meta( 'display_name', $recent["post_author"]);
		$recent["avtar"]=$avtar;
		$recent["ID"]=$ID;
		
		//$postmeta = get_post_meta($recent["ID"]);
		//print_r($postmeta);
		$imgDetail=get_the_post_thumbnail_url($recent["ID"], 'post-thumbnail' ); 
		//print_r($imgDetail);
		$recent["imgDetail"]=esc_url($imgDetail);
		//echo "</pre>";	
		//$title= $recent["post_title"];
		
	    $urllist=explode("/",get_permalink($recent["ID"]));
	    $totalarray=count($urllist);
	    $urlno=($totalarray-2);
	   	if($urllist[$urlno] != 'news'){ $caturl=$urllist[$urlno]."/";}else{$caturl="";}
		$PageSource= get_permalink($recent["ID"]);
				//$PageSource=str_replace('news.usracing.localhost:90', 'news.usracing.com', $PageSource);
		//$d= new GeneratorAMPCache;
 		$this->gen_pages_amp($PageSource, $recent, $caturl); 	
		
	}
	
	public function amp_sidebar_generation(){
	$file_tpl_path="/home/ah/usracing.com/htdocs/news/layout/amp/sidebarWP.php";
	if ( is_active_sidebar('amp-only') ) {	
	 ob_start();
	 dynamic_sidebar('amp-only');	
	  $file_tpl_content = ob_get_clean();
	$file_tpl_content = str_replace('href="https://www.usracing.com/news/', 'href="/news/amp/', $file_tpl_content);
	$file_tpl_content = str_replace('href="http://news.usracing.com/news/', 'href="/news/amp/', $file_tpl_content);
	$file_tpl_content = str_replace('href="http://newsdev.usracing.com/news/', 'href="/news/amp/', $file_tpl_content);
	$file_tpl_content = str_replace('href="https://news.usracing.com/news/', 'href="/news/amp/', $file_tpl_content);	
	$file_tpl_content = str_replace('href="https://newsdev.usracing.com/news/', 'href="/news/amp/', $file_tpl_content);	
	$file_tpl_content = str_replace('src="http://news.usracing.com/', 'src="/news/', $file_tpl_content);
	$file_tpl_content = str_replace('src="http://newsdev.usracing.com/', 'src="/news/', $file_tpl_content);
	$file_tpl_content = str_replace('src="https://news.usracing.com/', 'src="/news/', $file_tpl_content);	
	$file_tpl_content = str_replace('src="https://newsdev.usracing.com/', 'src="/news/', $file_tpl_content);	
	$file_tpl_content = preg_replace('/<img(.*?)\/?>/', '<amp-img width="300" height="225" layout="responsive" $1 ></amp-img>',$file_tpl_content);
	file_put_contents( $file_tpl_path , $file_tpl_content ) ;	
	
	}
	}
	
	
}

?>