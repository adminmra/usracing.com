<?php
/**
 * @package AMP_Newsa
 * @version 1.1
 */
/*
Plugin Name: AMP News
Plugin URI: http://usracing.com
Description: Some tools for AMP news generation.
Author: Arnab Dutta
Version: 1.1
Author URI: http://usracing.com
*/

//https://wordpress.stackexchange.com/questions/134664/what-is-correct-way-to-hook-when-update-post
// if you don't add 3 as as 4th argument, this will not work as expected
add_action( 'save_post', 'amp_save_post_function', 10, 3 );

function amp_save_post_function( $post_ID, $post, $update ) {
	if(!$update){
		return;
	}
	
	if($post->post_status != "publish"){
		return;
	}

	$date = date("Y-m-d H:m");
	$output = "{$date}: {$post_ID}\n";
	//$output .= print_r($post, true) . "\n";
 	file_put_contents("/home/ah/news.usracing.com/amp_logs.txt", $output, FILE_APPEND);

	//require '/home/ah/usracing.com/htdocs/news/GeneratorCache.php';
	require_once 'GeneratorAMPCache.php';
	$c = new GeneratorAMPCache();
	$c->post_cache_by_id($post_ID);
	$c->amp_sidebar_generation();  
	//$c->generate_sidebar_latest();
	//$c->generate_home_featured_post();
	//$c->rsync_resources_for_news();
}

add_action( 'trashed_post', 'amp_delete_post_function');

function amp_delete_post_function( $post_ID ) {
	$post = get_post($post_ID); 

	$archivo = $post->post_name.".php";
	$archivo = str_replace('__trashed.php', '.php', $archivo);
	

	$filePATH='/home/ah/usracing.com/htdocs/news/amp/';
	rename($filePATH.$archivo, $archivo."_trash");

	file_put_contents("/home/ah/news.usracing.com/amp_logs_deleted.txt", $post_ID . " - ". $archivo . "\n", FILE_APPEND);
}

add_action( 'untrashed_post', 'amp_untrashed_post_function');

function amp_untrashed_post_function( $post_ID ) {
	$post = get_post($post_ID); 

	$archivo = $post->post_name.".php";
	$archivo = str_replace('__trashed.php', '.php', $archivo);
	

	$filePATH='/home/ah/usracing.com/htdocs/news/amp/';
	rename($filePATH.$archivo."_trash", $archivo);

	file_put_contents("/home/ah/news.usracing.com/amp_logs_deleted.txt", $post_ID . " U - ". $archivo . "\n", FILE_APPEND);
}



add_action( 'updated_option', 'amp_save_widget_function', 10, 3 );

function amp_save_widget_function(){
	require_once 'GeneratorAMPCache.php';
	$c = new GeneratorAMPCache();
	$c->amp_sidebar_generation();
}

add_action( 'delete_widget', 'amp_delete_widget_function', 10, 3 );

function amp_delete_widget_function(){
	require_once 'GeneratorAMPCache.php';
	$c = new GeneratorAMPCache();
	$c->amp_sidebar_generation();
}


?>
