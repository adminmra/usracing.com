<?php
/*
Plugin Name: Musitronic
Plugin URI: 
Description: Plugin... por definir
Author: Me
Version: 1.0
License: GPLv2
*/

function usrplayer_func ($attributes) {
	ob_start();
	?>
	<style>
	div#mp3_player{ width:50%; height:60px; background:#000; margin:50px auto; }
	div#mp3_player > div > audio{  width:100%; background:#000; float:left;  }
	div#mp3_player > canvas{ width:100%; height:30px; background:#002D3C; float:left; }
	</style>
	<script>
	// Create a new instance of an audio object and adjust some of its properties
	var audio = new Audio();
	audio.src = '<?php echo $attributes['src']; ?>';
	audio.controls = true;
	audio.loop = true;
	audio.autoplay = false;
	// Establish all variables that your Analyser will use
	var canvas, ctx, source, analyser, fbc_array, bars, bar_x, bar_width, bar_height;
	// Initialize the MP3 player after the page loads all of its HTML into the window
	window.addEventListener("load", initMp3Player, false);
	function initMp3Player(){
		document.getElementById('audio_box').appendChild(audio);
		window.AudioContext = new (window.AudioContext || window.webkitAudioContext)(); // AudioContext object instance
		analyser = window.AudioContext.createAnalyser(); // AnalyserNode method
		canvas = document.getElementById('analyser_render');
		ctx = canvas.getContext('2d');
		// Re-route audio playback into the processing graph of the AudioContext
		source = window.AudioContext.createMediaElementSource(audio); 
		source.connect(analyser);
		analyser.connect(window.AudioContext.destination);
		frameLooper();
	}
	// frameLooper() animates any style of graphics you wish to the audio frequency
	// Looping at the default frame rate that the browser provides(approx. 60 FPS)
	function frameLooper(){
		window.requestAnimationFrame(frameLooper);
		fbc_array = new Uint8Array(analyser.frequencyBinCount);
		analyser.getByteFrequencyData(fbc_array);
		ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas
		ctx.fillStyle = '#00CCFF'; // Color of the bars
		//ctx.fillStyle = '#1571ba'; // Color of the bars

		bars = 100;
		for (var i = 0; i < bars; i++) {
			bar_x = i * 3;
			bar_width = 2;
			bar_height = -(fbc_array[i] / 2);
			//  fillRect( x, y, width, height ) // Explanation of the parameters below
			ctx.fillRect(bar_x, canvas.height, bar_width, bar_height);
		}
	}
	</script>
	<div id="mp3_player">
	  <div id="audio_box"></div>
	  <canvas id="analyser_render"></canvas>
	</div>
<?php
	$ans = ob_get_contents();
	ob_end_clean();
	return $ans;
}

add_shortcode( 'usrplayer', 'usrplayer_func' );


