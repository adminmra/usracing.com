<?php

defined('ABSPATH') OR die('This script cannot be accessed directly.');

/**
 * Handle interactions with the http://app.schemaapp,com server
 *
 * @author Mark van Berkel
 */
class SchemaServer {

        private $options;
        private $resource;
        public $transient_id	= '';
        public $transient		= false;
        public $page_url		= '';
        public $data_sources	= array();

        const EDITOR = "https://app.schemaapp.com/editor";

        public function __construct($uri = "") {
			$this->options = get_option( 'schema_option_name' );

			if ( ! empty( $uri ) ) {
				$this->resource = $uri;
			} elseif ( is_singular() ) {
				$this->resource = get_permalink();
			} else {
				$permalink_structure = get_option( 'permalink_structure' );

				// If pretty permalink then remove query string from URL
				if ( $permalink_structure ) {
					$request_uri = parse_url( $_SERVER['REQUEST_URI'], PHP_URL_PATH );
				
					$this->resource = ( is_ssl() ? 'https' : 'http' ) . "://{$_SERVER['HTTP_HOST']}{$request_uri}";
				} else {
					$this->resource = ( is_ssl() ? 'https' : 'http' ) . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
				}
			}
        }

        /**
         * Get Resource JSON_LD content
         * 
         * @param type $uri
         * @return string
         */
        public function getResource($uri = '', $pretty = false) {
			if ( empty( $this->options['graph_uri'] ) ) {
				return '';
			}


			$resource = '';

			if ( ! empty( $uri ) ) {
				$resource = $uri;
			} elseif ( ! empty( $this->resource ) ) {
				$resource = $this->resource;
			} else {
				return '';
			}


			$account_id			= str_replace( array( 'http://schemaapp.com/db/', 'https://schemaapp.com/db/' ), '', $this->options['graph_uri'] );
			$resource			= $this->page_url = urldecode( $resource ); // Decode accent characters
			$resource_hash		= trim( base64_encode( $resource ), '=' );
			$this->data_sources	= array( "https://data.schemaapp.com/{$account_id}/{$resource_hash}" );
			$this->transient_id	= 'HunchSchema-Markup-' . md5( $resource );
			$transient			= get_transient( $this->transient_id );


			$data_sources_additional = ( array ) apply_filters( 'hunch_schema_markup_api_data_source', $account_id, $resource );

			if ( ! empty( $data_sources_additional ) ) {
				foreach ( $data_sources_additional as $data_source ) {
					if ( filter_var( $data_source, FILTER_VALIDATE_URL ) ) {
						$this->data_sources[] = $data_source;
					}
				}
			}


			// Check for missing, empty or 'null' transient
			if ( ! empty( $transient ) && $transient !== 'null' ) {
				$this->transient = true;

				return $transient;
			}


			$schemadata = '';

			foreach ( $this->data_sources as $data_source ) {
				$remote_response = wp_remote_get( $data_source );

				if ( ! is_wp_error( $remote_response ) && wp_remote_retrieve_response_code( $remote_response ) == 200 && ! empty( wp_remote_retrieve_body( $remote_response ) ) && wp_remote_retrieve_body( $remote_response ) !== 'null' ) {
					$schemadata = wp_remote_retrieve_body( $remote_response );

					// First delete then set; set method only updates expiry time if transient already exists
					delete_transient( $this->transient_id );
					set_transient( $this->transient_id, $schemadata, 86400 );

					if ( $pretty && version_compare( phpversion(), '5.4.0', '>=' ) ) {
						$schemadata = json_encode( json_decode( $schemadata ), JSON_PRETTY_PRINT );
					}

					break;
				}
			}

			return $schemadata;
        }


        /**
         * Get the Link to Update a Resource that exists
         * 
         * @param type $uri
         * @return string
         */
        public function updateLink() {
                $link = self::EDITOR . "?resource=" . $this->resource;
                return $link;
        }

        /**
         * Get the link to create a new resource
         * 
         * @param type $uri
         * @return string
         */
        public function createLink() {
                $link = self::EDITOR . "?create=" . $this->resource;
                return $link;
        }

        /**
         * Activate Licenses, sends license key to Hunch Servers to confirm purchase 
         * 
         * @param type $params
         */
        public function activateLicense( $params ) {
			$response = wp_remote_post( 'https://app.schemaapp.com/schemaApi/license/addSite', array(
				'sslverify' => false,
				'headers' => array( 'Content-Type' => 'application/json' ),
				'body' => json_encode( $params ),
			));

			$response_code = wp_remote_retrieve_response_code( $response );
			$response_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( in_array( $response_code, array( 200, 201 ) ) ) {
				return array( true, $response_data->status );
			} else {
				return array( false, $response_data->error );
			}
        }

}
