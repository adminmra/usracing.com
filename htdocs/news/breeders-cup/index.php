<?php header("Content-Type: text/html; charset=utf-8"); ?>
<?php $_usr_vars=array (
  'title' => 'Breeders\' Cup News | Horse Racing News',
  'h1' => 'Breeders\' Cup News',
  'visible_latest_news' => true,
  'visible_cta_handicapping' => true,
  'classes' => 'archive category category-breeders-cup category-805',
  'cta_form' => true,
  'pagination_slug' => 'breeders-cup'
); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/header-bc.php"); ?>
<?php //include("/home/ah/usracing.com/htdocs/news/layout/cta_handicapping_form.php"); ?>
<?php //include('/home/ah/usracing.com/htdocs/news/wp-content/themes/usracing/subscribe-ctc-form.php'); ?>
<div id="main" class="container2 news-content"><div class="row">
<?php include("/home/ah/usracing.com/htdocs/news/layout/body_category.php"); ?>
<?php include("/home/ah/usracing.com/htdocs/news/layout/sidebar.php"); ?>
</div><!-- end/row --></div><!-- end/main -->
<?php include("/home/ah/usracing.com/htdocs/news/layout/footer.php"); ?>
