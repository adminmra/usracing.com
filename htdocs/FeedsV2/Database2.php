<?php

/*
* Mysql database class - only one connection alowed
*/
class DB2 {
	private $_connection;
	private static $_instance; //The single instance
	private $_host     = "localhost";
	private $_username = "otbnet_db";
	private $_password = "4Z3zW,o5s3VKCo}urs";
	private $_database = "otbnet_db";

	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function query( $sql ) {
		if( ! self::$_instance ) // If no instance then make one
			self::$_instance = new self();
		if ( self::$_instance->getConnection() != null )
			return self::$_instance->getConnection()->query( $sql );
		else
			return 'Failed conect to MySQL';

	}
	// Constructor
	private function __construct() {
		$this->_connection = new mysqli(
				$this->_host,
				$this->_username, 
				$this->_password,
				$this->_database
			);
	
		// Error handling
		if( mysqli_connect_error() ) {
			trigger_error("Failed conect to MySQL: " . mysql_connect_error(),
				 E_USER_ERROR);
			$this->_connection = null;
		}

	}
	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }
	// Get mysqli connection
	public function getConnection() {
		return $this->_connection;
	}
}

?>
