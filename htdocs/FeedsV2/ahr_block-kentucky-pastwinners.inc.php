<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>

<div class="block">
<h2 class="title-custom">Past Winners and Results of the Kentucky Derby</h2>
<div style="padding: 0 34px;">
<p>&nbsp;</p>
<br />
<p>
<h1  class="DarkBlue">Kentucky Derby Past Winners</h1>
<br />
<p><strong>When is the Kentucky Derby?</strong><br /> The 136th Kentucky Derby is on Saturday May 1, 2010!</p>
<p><strong>Where is the Kentucky Derby?</strong><br /> The Derby is raced at Churchill Downs Racecourse in Louisville, Kentucky</p>
<p><strong>Where can I watch the Kentucky Derby?</strong><br /> Watch the Kentucky Derby live on TV with NBC at 5:00 p.m. Eastern Time</p>
</p>
<p>&nbsp;</p>

</div>
</div>
<div id="box-shd"></div>





<?php	 		 	
require_once "dbconnect.inc";
$Result=mysql_query_w("SELECT * FROM kentucky_derby_result ORDER BY year DESC");
?>


<div class="block">
<h2 class="title-custom">Kentucky Derby Past Winners</h2>
<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Kentucky Derby Winners" summary="The past winners of the Kentucky Derby" >
      <tbody>
	  <tr>
        <th width="46" >Year</th>
        <th width="105"  >Winner</th>
        <th width="139" >Jockey</th>
        <th width="150" >Trainer</th>
        <th width="52" >Time</th>
        </tr>
       <?php	 	
 $count=0;
 while($BreedersCup2=mysql_fetch_object($Result)){
  if($count%2 ==1)
  {
  echo '<tr class="odd" >';
  }
  else
  {
  echo '<tr>';
  }
  
   $sqlhorse = " SELECT * FROM horse WHERE horse like '%".addslashes($BreedersCup2->winner)."%' ";
  $resulthorse=mysql_query_w($sqlhorse);
  $numhorse=mysql_num_rows($resulthorse);
  
  $sqljockey = " SELECT * FROM jockey WHERE name like '%".$BreedersCup2->jockey."%' ";
  $resultjockey=mysql_query_w($sqljockey);
  $numjockey=mysql_num_rows($resultjockey);
 
  $sqltrainer = " SELECT * FROM trainers WHERE name like '%".$BreedersCup2->trainer."%' ";
  $resulttrainer=mysql_query_w($sqltrainer);
  $numtrainer=mysql_num_rows($resulttrainer);

  ?>
    <td ><?php	 	 echo $BreedersCup2->year;?></td>
    <td><?php	 	 if($BreedersCup2->winner!=""){if($numhorse > 0) echo "<a href=\"/horse?name=".str_replace(" ","_",$BreedersCup2->winner)."\" title=\"".$BreedersCup2->winner."\">$BreedersCup2->winner</a>"; else echo $BreedersCup2->winner; }else{ echo "-"; }?></td>
    <td><?php	 	 if($BreedersCup2->jockey!=""){if($numjockey > 0) echo "<a href=\"/jockey?name=".str_replace(" ","_",$BreedersCup2->jockey)."\" title=\"".$BreedersCup2->jockey."\">$BreedersCup2->jockey</a>"; else echo $BreedersCup2->jockey; }else{ echo "-";}?></td>
    <td><?php	 	 if($BreedersCup2->trainer!=""){if($numtrainer > 0) echo "<a href=\"/trainer?name=".str_replace(" ","_",$BreedersCup2->trainer)."\" title=\"".$BreedersCup2->trainer."\">$BreedersCup2->trainer</a>"; else echo $BreedersCup2->trainer; }else{ echo "-";}?></td>
    <td><?php	 	 echo $BreedersCup2->wintime;?></td>
    </tr>
	<?php	 	
	$count++;
	
	}
	?>
  </tbody>
</table>
 </div>
						  </div>
<div id="box-shd"></div><!-- === SHADOW === -->
						