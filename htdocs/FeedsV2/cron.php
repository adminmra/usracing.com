<?php
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
////ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
//error_reporting(E_ALL);

define( 'APP_PATH' , '/home/ah/usracing.com/htdocs/FeedsV2/' );


require_once APP_PATH . 'AhrBlockGradedScheduleGenerator.php';
//'BetlmScraper'                  => 'horse_racing_schedule/BetlmScraper.php',
require_once APP_PATH . 'AhrHorseRacingScheduleGenerator.php';
//'NtraScraper'                   => 'horse_racing_schedule/NtraScraper.php',// ver en detalle

//'BreedersOddsScraper'           => 'breeders/BreedersOddsScraper.php',
//'BreedersDistaffOddsScraper'    => 'breeders/BreedersDistaffOddsScraper.php',
require_once APP_PATH . 'BreedersChallengeGenerator.php';
//'Odds926Scraper'                => 'kd/Odds926Scraper.php',

//'OddsApScraper'                 => 'misc/OddsApScraper.php',
//'LiveInRunningScraper'          => 'misc/LiveInRunningScraper.php',
require_once APP_PATH . 'AhrBlockTopLeadersGenerator.php';
require_once APP_PATH . 'AhrBlockRacingNewsGenerator.php';
//'GeneraXmlScraper'              => 'newsxml/GeneraXmlScraper.php'
require_once APP_PATH . 'GradedStakesHomeGenerator.php';
require_once APP_PATH . 'AhrRacingResultsGenerator.php';
require_once APP_PATH . 'AhrTripleCrownGenerator.php';
require_once APP_PATH . 'KentuckyDerbyGenerator.php';


/*   DPRECATED FUNCTIONS
//as_ahr_horse_racing_schedule_byname(); //by JI
//as_ahr_racing_schedule_page();

//as_ahr_breeders_challenge_home(); ***
//as_ahr_kdodds2014();

//as_apj_matchups2015();
//as_apj_marginvictory2015();
//as_apj_propsmatchups2015();
//as_apj_racingschedule2015();
/*

as_ahr_block_track_results();
as_ahr_block_graded_stakes_race_calendar();
as_ahr_graded_stakes_schedule();
as_ahr_block_upcoming_races();
as_ahr_upcoming_races();
as_ahr_rotw_banner();
as_ahr_countdown();
as_ahr_dailyracingnewsimage();
as_ahr_breederscuprace();
as_ahr_kentuckypastwinners();
as_ahr_belmontpastwinners();
as_ahr_preaknesspastwinners();
as_ahr_KDcontenders();
as_ahr_belmontcontenders();
as_ahr_preaknesscontenders();
as_ahr_kd_prep();
as_ahr_Top_Horse_Future();
as_ahr_koakscontenders();




as_ahr_kdoddshome();


as_ahr_koaksodds2011();
as_ahr_preaknessodds2011();
as_ahr_belmontodds2011();
as_ahr_logout();
//as_ahr_race-of-the-week();
* /
}else{
 print('Mysql Server is Down therefore no template files will be update.<br /> Please start the server to complete the cron job');
}

function as_ahr_horse_racing_schedule(){
	//correr el scraper aqui
	ob_start();
	require_once('ahr_horse_racing_schedule.inc.php');
	as_file_save_data(ob_get_contents(), 'ahr_racing_schedule_page2.tpl');
	ob_end_clean();
}

function as_ahr_horse_racing_schedule_byname(){
	//correr el scraper aqui
	ob_start();
	require_once('ahr_horse_racing_schedule_byname.inc.php');
	as_file_save_data(ob_get_contents(), 'ahr_racing_schedule_page_byname.tpl');
	ob_end_clean();
}

function as_ahr_dailyracingnewsimage(){
	$url="http://www.dailyracingnews.com/images/homephotos/allhorseracing/1.jpg";
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/Cache/1.jpg", file_get_contents($url));
	$url="http://www.dailyracingnews.com/images/homephotos/allhorseracing/2.jpg";
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/Cache/2.jpg", file_get_contents($url));
	$url="http://www.dailyracingnews.com/images/homephotos/allhorseracing/3.jpg";
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/Cache/3.jpg", file_get_contents($url));
	$url="http://www.dailyracingnews.com/images/homephotos/allhorseracing/4.jpg";
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/Cache/4.jpg", file_get_contents($url));
	$url="http://www.dailyracingnews.com/images/homephotos/allhorseracing/5.jpg";
	file_put_contents($_SERVER['DOCUMENT_ROOT']."/Feeds/TrackResults/Cache/5.jpg", file_get_contents($url));
}
###################################################################
function as_apj_racingschedule2015(){
		ob_start();
        require_once('apj_block_kd_racing_schedule_2015.inc.php');
		$block = ob_get_contents();
		ob_end_clean();
        print(as_file_save_data($block,'apj_block_kd_racing_schedule.tpl'));
}
###################################################################
function as_apj_propsmatchups2015(){
		ob_start();
        require_once('apj_block_kd_props_matchups_2015.inc.php');
		$block = ob_get_contents();
		ob_end_clean();
        print(as_file_save_data($block,'apj_block_kd_props_matchups_2015.tpl'));
}
###################################################################
function as_apj_marginvictory2015(){
		ob_start();
        require_once('apj_block_kd_marginvictory_2015.inc.php');
		$block = ob_get_contents();
		ob_end_clean();
        print(as_file_save_data($block,'apj_kd_marginvictory_2015.tpl'));
}
###################################################################
function as_apj_matchups2015(){
		ob_start();
        require_once('apj_block_kd_matchups_2015.inc.php');
		$block = ob_get_contents();
		ob_end_clean();
        print(as_file_save_data($block,'apj_kd_matchups_2015.tpl'));
}
###################################################################
function as_ahr_block_track_results(){
	ob_start();
	include('ahr_block-track-results.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_track_results.tpl'));
}



function as_ahr_breeders_challenge_home(){
	ob_start();
	include('breeders_challenge_home.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_breeders_challenge_home.tpl'));
}


function as_ahr_block_graded_stakes_race_calendar(){
	ob_start();
	include('ahr_block-graded-stakes-calendar.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_graded_stakes_race_calendar.tpl'));
}

function as_ahr_block_upcoming_races(){
	ob_start();
	include('ahr_block-upcoming_races.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_upcoming_races.tpl'));
}

function as_ahr_graded_stakes_schedule(){
	ob_start();
	include('ahr_block-graded_stakes_schedule.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_graded_stakes_schedule.tpl'));
}

function as_ahr_rotw_banner(){
	ob_start();
	include('ahr_block-rotw_banner.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_rotw_banner.tpl'));
}

function as_ahr_breederscuprace(){
	ob_start();
	include('ahr_block-breederscup-race.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_breederscup-race.tpl'));
}

function as_ahr_kentuckypastwinners(){
	ob_start();
	include('ahr_block-kentucky-pastwinners.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_kentucky-pastwinners.tpl'));
}

function as_ahr_belmontpastwinners(){
	ob_start();
	include('ahr_block-belmontstakes-pastwinners.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_belmont-pastwinners.tpl'));
}

function as_ahr_preaknesspastwinners(){
	ob_start();
	include('ahr_block-preakness-pastwinners.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_preakness-pastwinners.tpl'));
}


function as_ahr_KDcontenders(){
	ob_start();
	include('ahr_block-kd-contenders.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_KD-contenders_2011.tpl'));
}

function as_ahr_belmontcontenders(){
	ob_start();
	include('ahr_block-belmont-contenders.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_belmont-contenders.tpl'));
}


function as_ahr_preaknesscontenders(){
	ob_start();
	include('ahr_block-preakness-contenders.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_preakness-contenders.tpl'));
}

function as_ahr_kd_prep(){
	ob_start();
	include('ahr_block-kd-preprace.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_kd-preprace.tpl'));
}


function as_ahr_Top_Horse_Future(){
	ob_start();
	include('ahr_block-top-horse-future.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_top-horse-future.tpl'));
}

/*function as_ahr_race-of-the-week(){
	ob_start();
	include('ahr_block-race-of-the-week.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block-race-of-the-week.tpl'));
}* /

function as_ahr_countdown(){
	ob_start();
	include('ahr_block-countdown.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_countdown.tpl'));
}

function as_ahr_racing_schedule_page(){
	ob_start();
	require_once('ahr_racing_schedule.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print( as_file_save_data( $block , 'ahr_racing_schedule_page.tpl' ) );
}


function as_ahr_upcoming_races(){
	ob_start();
	require_once('ahr_upcoming_races.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_upcoming_races.tpl'));
}

/**function as_2010_race_schedule(){

        $block = module_invoke('block', 'block', 'view', 113);
        print(as_file_save_data($block['content'],'2010_race_schedule.tpl'));
}
function as_ahr_block_racing_news_mini(){

        $block = module_invoke('multiblock', 'block', 'view', 2);
        print(as_file_save_data($block['content'],'ahr_block_racing_news_mini.tpl'));
}

function as_ahr_racing_news_page(){

        $block = allhorseracing_news_page();
        print(as_file_save_data($block,'ahr_racing_news_page.tpl'));
}
** /


function as_ahr_kdodds2014(){
	ob_start();
	require_once('ahr_block_kd_odds_2014.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_kd_odds_2014.tpl'));
}


function as_ahr_koakscontenders(){
	ob_start();
	require_once('ahr_block-koaks-contenders.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block-koaks-contenders.tpl'));
}

function as_ahr_kdoddshome(){
	ob_start();
	require_once('ahr_block_kdodds_home.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_kdodds_home.tpl'));
}

function as_ahr_koaksodds2011(){
	ob_start();
	require_once('ahr_block_koaks_odds_2011.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_koaks_odds_2011.tpl'));
}

function as_ahr_preaknessodds2011(){
	ob_start();
	require_once('ahr_block_preakness_odds_2011.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_preakness_odds_2011.tpl'));
}

function as_ahr_belmontodds2011(){
	ob_start();
	require_once('ahr_block_belmont_odds_2011.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_belmont_odds_2011.tpl'));
}

function as_ahr_logout(){
	ob_start();
	require_once('ahr_block-logout.inc.php');
	$block = ob_get_contents();
	ob_end_clean();
	print(as_file_save_data($block,'ahr_block_logout.tpl'));
}




function ahr_dailyracingnews_connect() {
	static $client;

	if ( !$client ) {
		require_once( 'soap/lib/nusoap.php' );
		$client = new nusoap_client( 'http://www.dailyracingnews.com/webservices/contentfeed.asmx?wsdl' , TRUE );
	}

	return $client;
}
*/
?>
