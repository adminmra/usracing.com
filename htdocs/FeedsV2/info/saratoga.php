<!-- ======== STYLES NEEDED TO HIDE DRUPAL BLOCK ELEMENTS ================== -->
<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>
<!-- ============================================================== -->
<div class="block">
<h2 class="title-custom"><?php	 		 	 echo $_REQUEST["race"] ?> Past Winners</h2>
<?php	 	
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/clean.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/imageresize.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/sort.php";
include $_SERVER['DOCUMENT_ROOT']."/Feeds/info/class.xmlreader.php";
?>
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Sratoga Winners" summary="The past winners of the Sratoga Stakes" >
      <tbody>
	  <tr>
        <th width="46" >Year</th>
        <th width="105"  >Horse</th>
        <th width="139" >Jockey</th>
        <th width="150" >Trainer</th>
        <th width="52" >Time</th>
		<th width="52" >Weight</th>
		<th width="52" >Value</th>
        </tr>
<?php	 	
$DATEAR=array();
$count=0;
	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/xmlfeed/saratoga_pastwinners.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
	for($i=0;$i<count($xml["allhorse"]["#"]["saratoga_pastwinners"]);$i++)
	{
			//echo $xml["allhorse"]["#"]["race_schedule"][$i]["#"]["racedate"][0]["#"];
			//$date=explode("-",trim($xml["allhorse"]["#"]["nflschedule"][$i]["#"]["weekselect"][0]["#"]));
			if($xml["allhorse"]["#"]["saratoga_pastwinners"][$i]["#"]["race_id"][0]["#"] == $_REQUEST["id"])
			{
//echo $xml["allhorse"]["#"]["nflschedule"][$i]["#"]["weekselect"][0]["#"]."<br>";
			$DATAARR=array();
			foreach($xml["allhorse"]["#"]["saratoga_pastwinners"][$i]["#"] as $key => $value){
				$DATAARR[$key] = str_replace("�","",$value[0]["#"]);
			}
			$datapast= (object)$DATAARR;
			/*echo "<pre>";
			print_r($datadate);
			echo "</pre>";*/
			
			 if($count%2 ==1)
			  {
			  echo '<tr class="odd" >';
			  }
			  else
			  {
			  echo '<tr>';
			  $numhorse=0;
			  $numjockey=0;
			  $numtrainer=0;
			  }
			  ?>
			 <td ><?php	 	 echo $datapast->year;?></td>
    <td><?php	 	 if($datapast->horse!=""){if($numhorse > 0) echo "<a href=\"/horse?name=".str_replace(" ","_",$datapast->horse)."\" title=\"".$datapast->horse."\">$datapast->horse</a>"; else echo $datapast->horse; }else{ echo "-"; }?></td>
    <td><?php	 	 if($datapast->jockey!=""){if($numjockey > 0) echo "<a href=\"/jockey?name=".str_replace(" ","_",$datapast->jockey)."\" title=\"".$datapast->jockey."\">$datapast->jockey</a>"; else echo $datapast->jockey; }else{ echo "-";}?></td>
    <td><?php	 	 if($datapast->trainer!=""){if($numtrainer > 0) echo "<a href=\"/trainer?name=".str_replace(" ","_",$datapast->trainer)."\" title=\"".$datapast->trainer."\">$datapast->trainer</a>"; else echo $datapast->trainer; }else{ echo "-";}?></td>
    <td><?php	 	 echo $datapast->time;?></td>
	<td><?php	 	 if($datapast->wt == 0){echo "-";}else{echo $datapast->wt;}?></td>
	<td><?php	 	 echo $datapast->value;?></td>
    </tr>
	<?php	 	
	$count++;
			}
	}


?>
</tbody>
</table>
</div>