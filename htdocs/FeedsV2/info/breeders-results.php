<?php	 		 	
include_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/clean.php";
include_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/imageresize.php";
include_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/sort.php";
include_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/class.xmlreader.php";

	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/FeedData/breeders_cup_result.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();
//print_r($xml);

?>
   <!-- ======== BOX ================================================= -->                             
<div class="block">
<h2 class="title-custom"><?php	 	 echo $stakes_name; ?> Odds and Results</h2>
<div> 

   <table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries"  title=" <?php	 	 echo $stakes_name; ?> Odds and Results" summary=" <?php	 	 echo $stakes_name; ?> Odds and Results for  2007 Breeders' Cup Betting">
                              
                                <tr >
                                <th width="8%">Result</th>
                                  <th width="8%">Time</th>
                                  <th width="5%">Post</th>
                                  <th width="6%">M/L</th>
                              <th width="25%">Horse</th>
                                   <th width="25%">Trainer</th>
                                  <th width="25%">Jockey</th> 
                                </tr>
                                <?php	 	
										 $seeindex=1;


	for($i=0;$i<count($xml["allhorse"]["#"]["breeders_cup_result"]);$i++)
	{
			//print_r($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["race_id"][0]["#"]);
			if($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["race_id"][0]["#"] == $_REQUEST['id'])
			{
			$DATAARR=array();
			foreach($xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"] as $key => $value){
				$DATAARR[$key] = str_replace("�","",$value[0]["#"]);
			}
			$rows= (object)$DATAARR;
			
$numhorse=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numhorse"][0]["#"];
$numtrainer=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numtrainer"][0]["#"];
$numjockey=$xml["allhorse"]["#"]["breeders_cup_result"][$i]["#"]["numjockey"][0]["#"];

 									if(($seeindex%2)== 1)
											   {
											   echo " <tr >";
											   }
											   else
											   {
											   	echo ' <tr   class="odd">';
											   }

										  ?>
                               
                                <td width="8%"  align="center" ><?php	 	 if($rows->result==0) echo "-"; else echo $rows->result;?></td>
                                  <td width="8%" align="center" ><?php	 	 if($rows->time1!="") echo $rows->time1; else echo "-";?></td>
                                  <td width="5%" align="center" ><?php	 	 if($rows->post==0) echo "-"; else echo $rows->post;?></td>
                                  <td width="6%" align="center" ><?php	 	 if($rows->odds!="") echo $rows->odds; else echo "-";?></td>
                               <td width="21%" ><?php	 	  if($rows->horse!=""){if($numhorse > 0) echo "<a href=\"/horse?name=$rows->horse\">$rows->horse</a>"; else echo $rows->horse; }else{ echo "-"; }?></td>
                               <td width="25%" ><?php	 	 if($rows->trainer!=""){if($numtrainer > 0) echo "<a href=\"/trainer?name=$rows->trainer\">$rows->trainer</a>"; else echo $rows->trainer; }else{ echo "-";}?></td>
                                  <td width="25%" ><?php	 	 if($rows->jockey!=""){if($numjockey > 0) echo "<a href=\"/jockey?name=$rows->jockey\">$rows->jockey</a>"; else echo $rows->jockey; }else{ echo "-";}?></td>
                                </tr>
                                <?php	 	 //}
									  
									  $seeindex++;
							
								

			}
	}

//print_r($rows);
?>
<?php	 	
if($seeindex == 1)
								{
								?>
								<tr>
                                   <td align="center" colspan="7" >No entries available now. Check Back soon for the entries on <?php	 	 echo $stakes_name; ?></td>
								</tr>
								<?php	 	
								}
?>
                              </table>
							  </div>
				 </div>           
                  <div id="box-shd"></div><!-- == Shadow outside of block == -->
<!-- ======== END BOX ============================================== -->   