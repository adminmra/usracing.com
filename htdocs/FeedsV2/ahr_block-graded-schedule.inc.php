<?php	 		 	
$sql_top= "SELECT * FROM graded_schedule ORDER BY racedate ";
$result_top=mysql_query_w($sql_top);
?>
<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
<tbody>
<tr>
<th width="10%"><strong>Date</strong></th>
<th width="12%"><strong>Stakes</strong></th>
<th width="12%"><strong>Track</strong></th>
<th width="10%"><strong>Grade</strong></th>
<th width="10%"><strong>Purses</strong></th>
<th width="10%"><strong>Age/Sex</strong></th>
<th width="10%"><strong>DS</strong></th>
<th width="12%"><strong>Horse</strong></th>
<th width="12%"><strong>Jockey</strong></th>
<th width="12%"><strong>Trainer</strong></th>
</tr>
<?php	 	
$counter=0;
while($data_top=mysql_fetch_object($result_top))
{
$updatedate=explode("-",$data_top->racedate);
$updateas=date("M d",mktime(0,0,0,$updatedate[1],$updatedate[2],$updatedate[0]));
if($counter%2 == 1)
{
	echo '<tr class="odd">';
}
else
{
	echo '<tr>';
}
?>
	<td class="num"><?php	 	 echo $updateas; ?></td>
	<td><?php	 	 echo stripslashes($data_top->racename); ?></td>
	<td><?php	 	 echo $data_top->track; ?></td>
	<td><?php	 	 echo $data_top->grade; ?></td>
	<td><?php	 	 echo $data_top->purse; ?></td>
	<td><?php	 	 echo $data_top->age; ?></td>
	<td><?php	 	 echo $data_top->ds; ?></td>
	<td><?php	 	 echo trim($data_top->horse); ?></td>
	<td><?php	 	 echo trim($data_top->jockey); ?></td>
	<td><?php	 	 echo trim($data_top->trainer); ?></td>
</tr>
<?php	 	
$counter++;
}

?>
</tbody>
</table>