
<?php

if ( !defined('APP_PATH') ) {
	define( 'APP_PATH' , '/home/ah/usracing.com/htdocs/FeedsV2/' );
}

require_once APP_PATH . 'Generator.php';

class AhrBlockRacingNewsGenerator extends Generator{

	function __construct(){
		parent::__construct();
	}

	public function content_tpl(){

		$articles = $this->template_preprocess_ahr_block_racing_news_mini();

		$queryFirst = "select * from newsarchive WHERE newsarchive.featured = '1' and newsarchive.typetosee = 'OTH' order by timecreate desc limit 0,1";
		//$query = "select * from newsarchive where 1 order by ContentId desc".$limit;
		$resultsFirst = DB::query( $queryFirst );
		while ($rowfirst = $resultsFirst->fetch_object()) {
			$url    = "http://allhorse.com/programs/news/images/" . $rowfirst->ContentId . "-otb.jpg";
			$urlLrg = "http://allhorse.com/programs/news/images/" . $rowfirst->ContentId . ".jpg";
		    file_put_contents( APP_PATH . "TrackResults/Cache/" . $rowfirst->ContentId . "-otb.jpg", file_get_contents($url));
			file_put_contents( APP_PATH . "TrackResults/Cache/" . $rowfirst->ContentId . ".jpg", file_get_contents($urlLrg));
			$imagefilepath    = $rowfirst->ContentId . "-otb.jpg";
			$imagefilepathLrg = $rowfirst->ContentId . ".jpg";
			// _ahr_parse_articles_result_item($resultsFirst);
		    $query = "select * from newsarticle where ContentIdART =" . $rowfirst->ContentId;
		    $results = DB::query($query);

		    $article_result = array();
		    while ($row = $results->fetch_assoc()) {
		        $article_result = $row;
		    }

			$item['title']   = $article_result['TitleART'];
			$item['aid']     = $article_result['ContentIdART'];
			$item['created'] = $article_result['DateCreatedART'];
			$item['url']     = $article_result['url'];
			//$item['body']  = check_markup($xml->Text->asXML(), $format, FALSE);
			$item['teaser']  = $this->ahr_abbr_html(strip_tags(htmlspecialchars_decode($article_result['HtmlART'])), 115);

		    $first_article = '<p class="news-title"><a href="/news/' . $item['url'] . '">' . $item['title'] . '</a></p><div class="teaser">' . $item['teaser'] . /*'<a href="/news/' . $item['url'] . '">Read article</a>' . */'<div class="news-date">' . $item['created'] . '</div>';
		}
		//$first_article = array_shift($articles);
		//echo "<pre>";
		//print $articles[0];
		//echo "</pre>";

		ob_start();

		if ( isset( $articles[0] ) && $articles[0] == "dead" ) {
		//print $first_article;
		    ?>

		    <?php
		} else {
		    $template_path = '/themes/';
		    ?>

		<div class="news">
		    <div id="news-content">

		    <?php
		        foreach ($articles as $article) {
		        print $article . "\n";
		    }
		    ?>
		    </div><!-- end/#news-content -->
		</div><!-- end/news -->
		    <?php
		}

		return ob_get_clean();
	}

	public function right_sidebar(){

		$articles = $this->template_preprocess_ahr_block_racing_news_mini();

		//$first_article = array_shift($articles);
		//print_r($articles);
		foreach ($articles as $article){
			if($article == "dead"){$article = "dead";}
		}

		ob_start();
		if($article == "dead") {
		//print $first_article;
		?>

		<?php
		}
		else {
			$template_path = '/themes/';

		?>

		<div id="news" class="news">
			<div class="headline"><h2>Racing News <a target="_blank" href="/news/rss.xml"><i class="fa fa-rss"></i></a></h2></div>
			<div class="content">
		      <?php
		      foreach ($articles as $article) {
		      	print $article ."\n";
		      }
		      ?>
			</div>
			<div class="blockfooter">
				<a href="/news" title="Read More News" class="btn btn-primary" rel="nofollow">Read More News<i class="fa fa-angle-right"></i></a>
			</div>
		</div>
		<?php
		}
		return ob_get_clean();
	}


	public static function run() {
		try {

			$instance   = new self;
			$html_block = $instance->content_tpl();
			//$instance->as_file_save_data( $html_block , 'test_class.tpl' );
			$instance->as_file_save_data( $html_block , 'ahr_block_racing_news.tpl' );

			$right_sidebar = $instance->right_sidebar();
			$instance->as_file_save_data( $right_sidebar , 'ahr_block-racing-news-right-sidebar.tpl' ) ;
		} catch ( Exception $e ) {
			print( $e->getMessage() );
		}
	}

	public function template_preprocess_ahr_block_racing_news_mini() {

		$results     = DB::query( "select * from outguard where id = '6'" );
		$results_msg = DB::query( "select * from outgurd_message where id='1'" );

		$row    = $results->fetch_array();
		$rows[] = $row;
		if ( $row['status'] == 'dead' ) {
			$row_msg = $results_msg->fetch_array();
			//print $row_msg["default_msg"];
			$articles = array();
			$articles[$aid] = $row_msg["default_msg"];
		} else {
			//Get list of articles
			$newsarticles = $this->ahr_get_news_articles(4, 1);
			//kpr($articles);
			$articles = array();

			foreach ( $newsarticles as $aid => $article ) {
				$articles[ $aid ] = $this->ahr_racing_news_article_teaser( $article );
			}
		}//else switch
	    return $articles;
	}

	public function ahr_racing_news_article_teaser($article, $first = FALSE) {
		$class = ($first) ? 'story-lng' : 'story';
		$output='';

		if (strlen(trim($article->url) ) == 0 ) {
			$URLNEW = '/news/' . $article->aid . '?title=' . str_replace('%26%2339%3Bs', '', str_replace('%20', '-', rawurlencode(strtolower($article->title))));
		} else {
			$URLNEW = '/news/' . $article->url;
		}

		if ( isset( $article->pictureThumb ) ){
		    # code...
		    if ($article->pictureThumb == '') {
		        //$articlePic = "http://usracing.com/images/usr-logo.png";
		    } else {
		        $articlePic = '<dt><a href="' . $article->pictureThumb . '" data-rel="fancybox-button" class="thumbnail fancybox-button zoomer"><span class="overlay-zoom"><img class="img-responsive" src="' . $article->pictureThumb . '" /><div class="zoom-icon"></div></span></a></dt>';
		    }
		}

		$output = '<dl class="dl-horizontal ' . $class . '" id="news-story-' . $article->aid . '">';
		if ( isset( $articlePic  ) ) {
		    $output .=  $articlePic;
		}
		$output .= '<dd><p class="news-title"><a href="' . $URLNEW . '">' . $article->title . '</a></p>';
		//$output .= '<div class="teaser">'. $this->ahr_abbr_html($article->teaser,90) .'<a href="'.$URLNEW.'">Read more</a></div>';
		$output .= '    <div class="teaser">' . $this->ahr_abbr_html($article->teaser, 300) . '</div>';
		$output .= '    <div class="news-date">' . $article->created . '</div></dd>';
		$output .= '</dl>';
		return $output;
	}



	function ahr_get_news_articles($items_per_page = 10, $page_number = 1, $nocache = FALSE) {
	    $cid = 'ahr_news_' . $items_per_page . '_' . $page_number . '_articles';
	    $expires_ts = time() + 3600;
	    $articles = FALSE;

	    /*
	      $client = ahr_dailyracingnews_connect();

	      // Fetch list of articles
	      $params = array(
	      'contentTypeId' => 3,
	      'pageSize' => $items_per_page,
	      'pageNumber' => $page_number,
	      'orderByColumn' => 'datecreated',
	      'orderByDirection' => 'desc'
	      );
	      $article_list_result = $client->call("GetArticleList", $params);
	      $article_list_result = $article_list_result['GetArticleListResult']['Article'];
	     */

	    $limit = $this->ahr_get_news_articles_limit($items_per_page, $page_number);
	    //$query = "select * from newsarchive where 1 order by STR_TO_DATE(DateCreated, '%M %d, %Y') desc".$limit;
	    $query = "select * from newsarchive WHERE newsarchive.typetosee = 'OTH' OR newsarchive.typetosee = 'AHR' order by timecreate desc" . $limit;
	    //$query = "select * from newsarchive where 1 order by ContentId desc".$limit;
	    $results = DB::query( $query );

	    $article_list_result = array();
	    while ($row = $results->fetch_array() ) {
	        array_push($article_list_result, $row);
	    }

	    if (isset($article_list_result['ContentId'])) {
	        $articles[$article_list_result['ContentId']] = $article_list_result;
	    } else if (count($article_list_result)) {

	        // Processing each article item returned
	        foreach ($article_list_result as $item) {
	            $articles[$item['ContentId']] = $this->_ahr_parse_articles_result_item($item);
	        }
	    }
	    return $articles;
	}

	function ahr_get_news_articles_limit($items_per_page = 10, $page_number = 1) {
	    $x = $items_per_page * $page_number;
	    $y = $x - $items_per_page;
	    return " limit " . $y . ", " . $x;
	}

	function _ahr_parse_articles_result_item($article) {
	    //$client = ahr_dailyracingnews_connect();

	    $created = $article['DateCreated'];

	    // Mapping initial article fields
	    /* $item = array(
	      'aid' => $article['ContentId'],
	      'title' => $article['Title'],
	      'subtitle' => $article['Teaser'],
	      'created' => $created,
	      ); */

	    // Adding on the full article text
	    //$article_result = $client->call("GetArticle", array('ArticleId' => $article['ContentId']));
	    //$xmlstr = $article_result['GetArticleResult']['Html'];
	    $query = "select * from newsarticle where ContentIdART =" . $article['ContentId'];
	    $results = DB::query($query);

	    $article_result = array();
	    while ($row = $results->fetch_assoc() ) {
	        $article_result = $row;
	    }
	    // print_r($article_result);
	    // $xmlstr =$article_result['HtmlART'];
	    //$xml = simplexml_load_string($xmlstr);
		$item['xml']     = $article_result['HtmlART'];
		//$format        = variable_get('filter_default_format', 1);
		$teaser_size     = 300;
		$item['title']   = $article_result['TitleART'];
		$item['aid']     = $article_result['ContentIdART'];
		$item['created'] = $article_result['DateCreatedART'];
		$item['url']     = $article_result['url'];
		//$item['body']  = check_markup($xml->Text->asXML(), $format, FALSE);
		$item['teaser']  = $this->ahr_abbr_html(strip_tags(htmlspecialchars_decode($article_result['HtmlART'])), 200); //_ahr_article_teaser($item['Teaser'], $format, $teaser_size);

	    $item['body'] = "";
	    // $item['teaser'] = "";

	    return (object) $item;
	}

}

AhrBlockRacingNewsGenerator::run();

?>
