<?php	 		 	
require_once "dbconnect.inc";
?>


<h2>Kentucky Derby Prep Race Schedule</h2>
<p>Here are the prep races for the 138th Kentucky Derby. </p>

<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="0"  class="table table-condensed table-striped table-bordered" title="Kentucky Derby Prep Race Schedule and Horses" summary="The Prep Race Schedule for Kentucky Derby Horses">  <tr>
 <th>Date</th>
 <th>Race</th>
 <th>Racetrack</th>
 <th>Dist</th>
 <th>Purse</th>
 <th>Win</th>
 <th>Place</th>
 <th>Show</th>
 </tr>
 
<?php	 	
$counter=0;
$sql="select * from 2011_kentucky_prep_new order by racedate desc";
$result=mysql_query_w($sql); 
while($data=mysql_fetch_object($result))
{	
		
	//$sqltrack = " SELECT * FROM racetracks WHERE name like '%".stripslashes($data->racetrack)."%' ";
$sqltrack = " select * from page_url where pagename like '%".str_replace(" ","_",trim($data->racetrack))."%' and page = 'track' ";
	$resulttrack=mysql_query_w($sqltrack);
	$numtrack=@mysql_num_rows($resulttrack);
$datatrackurl=mysql_fetch_object($resulttrack);
	
	//$sqlstakes = " SELECT * FROM stakes WHERE name like '%".stripslashes($data->race)."%' ";
$sqlstakes = " select * from page_url where pagename like '%".str_replace(" ","_",trim(stripslashes($data->race)))."%' and page = 'stakes' ";
	$resultstakes=mysql_query_w($sqlstakes);
	$numstakes=@mysql_num_rows($resultstakes);
	$datastakesurl=mysql_fetch_object($resultstakes);
	
	$sqlhorse = " SELECT * FROM horse WHERE horse like '%".stripslashes($data->win)."%' ";
	  $resulthorse=mysql_query_w($sqlhorse);
	  $numwin=@mysql_num_rows($resulthorse);
	  
	  $sqlhorse = " SELECT * FROM horse WHERE horse like '%".stripslashes($data->place)."%' ";
	  $resulthorse=mysql_query_w($sqlhorse);
	  $numplace=@mysql_num_rows($resulthorse);
	  
	  $sqlhorse = " SELECT * FROM horse WHERE horse like '%".stripslashes($data->showwin)."%' ";
	  $resulthorse=mysql_query_w($sqlhorse);
	  $numshow=@mysql_num_rows($resulthorse);
if($counter%2 == 0)
{
?>

 <tr> 
 <?php	 	
 }
 else
 {
 ?>
 
 <tr class="odd">
 <?php	 	
 }
 ?>
 <td ><?php	 	 $RACEDATE=explode("-",$data->racedate); echo $RACEDATE[1]."/".$RACEDATE[2]; ?></td>
 <td ><?php	 	 if($numstakes > 0){echo "<a href='/stakes?name=".$datastakesurl->pagename."' title='".stripslashes($data->race)."'>".stripslashes($data->race)."</a>";}else{echo  stripslashes($data->race);}?></td>
 <td ><?php	 	 if($numtrack > 0){echo "<a href='/racetrack?name=".$datatrackurl->pagename."' title='".stripslashes($data->racetrack)."'>".stripslashes($data->racetrack)."</a>";}else{echo  stripslashes($data->racetrack);}?></td>
<!--  <td ><?php	 	 echo $data->grade; ?></td>
 --> <td ><?php	 	 echo $data->purse; ?></td>
 <td ><?php	 	 echo $data->distance; ?></td>
 <td style="width: 50px;"><?php	 	 if($numwin > 0){echo "<a href='/horse?name=".str_replace(" ","_",stripslashes(trim($data->win)))."' title='".stripslashes($data->win)."'>".stripslashes($data->win)."</a>";}else{echo  stripslashes($data->win);}  ?></td>
 <td ><?php	 	 if($numplace > 0){echo "<a href='/horse?name=".str_replace(" ","_",stripslashes(trim($data->place)))."' title='".stripslashes($data->place)."'>".stripslashes($data->place)."</a>";}else{echo  stripslashes($data->place);}  ?></td>
 <td ><?php	 	 if($numshow > 0){echo "<a href='/horse?name=".str_replace(" ","_",stripslashes(trim($data->showwin)))."' title='".stripslashes($data->showwin)."'>".stripslashes($data->showwin)."</a>";}else{echo  stripslashes($data->showwin);}  ?></td>
 </tr>


 <?php	 	
 $counter++;
 }
 ?>
 </table>
 </div>

						
