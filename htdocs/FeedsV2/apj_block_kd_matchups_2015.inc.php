<?php	 		 	
$sql = "SELECT * FROM cron_matchups ORDER BY id_cron_matchup ASC";
$result = mysql_query_w($sql);

?>
<div class="table-responsive">
<table class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Kentucky Derby Odds" summary="Kentucky Derby Odds">
<!-- <tr>
<th> Derby Contenders</th>
<th class="right">Odds to Win</th>

</tr> -->
<?php	 	
$last_title ="";
while($data=mysql_fetch_object($result))
{
	$title = $data->title_column;

	if(strcasecmp($last_title, $title)!=0)
	{
		$last_title = $title;
	}
	else
	{
		$last_title = "";	
	}

	if(strcasecmp($last_title,"")!=0)
	{?>
		<tr style="font-weight:bold">
			<td colspan="2"><?php echo $last_title ?></td>
		</tr>
	<?php
	}?>
	<tr>
		<td><?php	echo $data->label_column; ?></td>
		<td class="right sortOdds" align="right"><?php	 echo $data->point_column; ?></td>
	</tr>
	<?php
}
?>

</table>
</div>