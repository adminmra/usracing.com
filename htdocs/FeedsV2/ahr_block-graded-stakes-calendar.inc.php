<?php	 		 	
// $Id$

/*
 * @file
 
  */

$races = template_preprocess_ahr_block_graded_stakes_race_calendar();
function template_preprocess_ahr_block_graded_stakes_race_calendar() {
	
  $races = ahr_get_graded_stakes(TRUE,FALSE);
  $gradedrace = array();
  $idx = 0;
  $limited_reached = 0;
  foreach($races as $id => $race) {
    //$date = date_make_date($row['addedon']);
    //$added_on = format_date($race['date'], 'custom', 'M j');
	$added_on = date('M j',$race['date']);

    $output = '<div class="graded-date"><span class="bold">'. $added_on .'</span></div>' ."\n";

    $output .= '<div class="graded-race">' ."\n";
	$idx = 0;
    foreach ($race['locations'] as $loc) {
	if ($idx > 10) {
		//$limited_reached = 1;
		break;
	}
      $output .= '<p><span class="bold">'. $loc['location'] .':</span> '. ahr_abbr_html($loc['details'], 45) .'</p>'."\n";
	$idx++;
    }
    $output .= '</div>'. "\n";
    $gradedrace[$id] = $output;
    if ($limited_reached == 1) {
		break;
    }
  }
	return $gradedrace;
}
  
  function ahr_get_graded_stakes($future_only = FALSE, $nocache = FALSE) {

  //$expires_ts = time() + 3600; // Expire in 1 hour
  $cid = ($future_only)? 'graded_stakes_future' : 'graded_stakes_all';
  //$races = (!$nocache)? cache_get($cid) : FALSE;
   try{
      if ($future_only) {
        $results = mysql_query_w("SELECT * FROM schedule WHERE type='4' AND display='y' and TO_DAYS(addedon)>= TO_DAYS(now()) ORDER BY addedon ASC ");	
			
      }
      else {
        $results = mysql_query_w("SELECT * FROM schedule WHERE type='4' AND display='y' ORDER BY addedon ASC");		
      }
	  
      // important: setting db back to default
      //db_set_active('default');

      $races = array();

      while ($row = mysql_fetch_array($results)) {
        $rows[] = $row;
        //$date = date_make_date($row['addedon']);
		$scdate = explode(" ",$row['addedon']); 
		$fscdate =explode("-",$scdate[0]);
		$date = mktime(0,0,0,$fscdate[1],$fscdate[2],$fscdate[0]);

        $data = explode('*', $row['data']);
        if (trim($data[0]) == '') {
          array_shift($data);
        }

        $locations = array();

        foreach ($data as $el) {
          $el = trim(stripslashes($el));
          $location = substr($el, 0, strpos($el, ':'));
          $details = substr($el, strpos($el, ':')+1);

          $locations[] = array(
            'location' => $location,
            'details' => $details,
          );
        }

        $races[$row['id']] = array(
          'date' => $date,
          'locations' => $locations,
        );
      }
	  return $races;
	  }catch(Exception $er){
	  	//print $er;
	  }
	  
}

$template_path = '/themes/';
$zebra = TRUE; $idx = 0;

?>
<div id="graded">
  <div class="boxheader">
    <div class="boxicon"><img src="<?php	 	 print $template_path .'images/gradedicon.gif' ?>" alt="graded stakes icon"/></div>
    <div><h1>Graded Stakes Races</h1></div>
  </div>

  <div id="graded-content">
      <?php	 	
      foreach ($races as $race) {
        if ($idx > 5) {
        	break;
        }
		/*if ($idx == 0) {
			$idx++;
        	continue;
        }*/
        $class = ($zebra)? 'stakes-blue' : 'stakes-yellow';
        $zebra = !$zebra;

        print '<div class="'. $class .'" id="grade-stake-'. $idx .'">'."\n". $race .'</div>';
        $idx++;
      }
    ?>
  </div>
  <div class="boxfooter"><span class="bold"><a href="/races/graded_stakes_schedule">More Graded Stakes Races</a></span></div>
</div>
