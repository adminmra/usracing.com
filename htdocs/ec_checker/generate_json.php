<?php
	error_reporting(0);
	ini_set('display_errors', 0);
	define("PATH_SMARTY_DYNAMICVAR", "/home/ah/allhorse/public_html/varsmarty/smartydynamicvar.php");
	define("PATH_CONTROL_SWITCH", "/home/ah/allhorse/public_html/control-switch/index.php");
	define('PATH_USR_PAGES', '/home/ah/usracing.com/smarty/templates/content');
	
	include_once(PATH_SMARTY_DYNAMICVAR);
	include_once(PATH_CONTROL_SWITCH);

	$results = array();
	//var_dump($SwitchVariable); die();
	foreach($SwitchVariable['variableName'] as $key => $value) {
		scan_pages(PATH_USR_PAGES, $key, $value, 0);

		//if($key == 1) break;
	}

	function scan_pages($root_path, $variableKey, $variableName, $level) {
		global $conn;
		global $SwitchVariable;
		global $SmartyDynamicVar;
		global $results;

		//echo $variableName; echo '<br>';
		$dirs = loopfolder($root_path);
		$next_level = $level + 1;
		foreach ($dirs as $d) {
			//echo str_repeat("------", $level);
			//echo $d . '<br>';
			$vars = loop_var_xml($d);
			foreach ($vars as $key => $var) {
				//echo str_repeat("------", $level);
				//echo $var['xml']->title; echo '<br>';
				$url = str_replace(PATH_USR_PAGES, 'https://usracing.com', dirname($var['path']));
				$title = $var['xml']->title;
				$switch_variables = $var['xml']->switch_variables->{$variableName};
				
				//var_dump($switch_variables); echo '<br>';
				if($switch_variables != NULL && $switch_variables->count() > 0) {
					//echo '<a href="'.$url.'">'.$url.'</a>'; echo '<br>';
					$file_path = str_replace(PATH_USR_PAGES, '/home/ah/usracing.com/smarty/templates/content', $var['path']);
					//echo $file_path; echo '<br>';
					//echo $variableName . ' - ' . $SwitchVariable['startTIME'][$variableKey] . ' - ' . $SwitchVariable['endTIME'][$variableKey]; echo '<br>';
					//echo $switch_variables->title; echo '<br>';
					$xml = $var['xml']->switch_variables->{$variableName}->asXML();
					//echo '<xmp>' . $xml . '</xmp><br>';
					$xml_binded = $xml;
					foreach ($SmartyDynamicVar as $sdv_key => $sdv_value) {
						$xml_binded = str_replace('{%'.$sdv_key.'%}', $sdv_value, $xml_binded);
					}
					//echo '<xmp>' . $xml_binded . '</xmp>';
					$results[] = array(
						'variable'		=> $variableName,
						'start_time'	=> $SwitchVariable['startTIME'][$variableKey],
						'end_time'		=> $SwitchVariable['endTIME'][$variableKey],
						'file_path'		=> $file_path,
						'xml'			=> $xml,
						'xml_binded'	=> $xml_binded,
						'url'			=> $url
					);
				}
			}
			scan_pages($d, $variableKey, $variableName, $next_level);
		}
	}

	function loop_var_xml($path) {
		$result = array();
		$list = glob($path . "/*.var.xml");
		foreach ($list as $l) {
			try {
				$xml=simplexml_load_file($l);
				$result[] = array(
					'path' => $l,
					'xml' => $xml
				);
			} catch (Exception $e) {
			    //echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
		}
		return $result;
	}

	function loopfolder($path) {
		$dirs = glob($path . '/*', GLOB_ONLYDIR);
		return $dirs;
	}

	$fp = fopen('/home/ah/usracing.com/htdocs/ec_checker/result.json', 'w');
	fwrite($fp, json_encode($results));
	fclose($fp);

	echo "usracing.com JSON updated\n";
	//echo json_encode($results);
?>