<?php

if ($_usr_vars['cta_form']) { ?>
<div id="cta_handicapping_form" class="subscribe-section col-md-12 col-sm-12 col-xs-12">
	<div class="inner-subscribe-section container3">
			<h2>News and Handicapping Reports</h2>
			<p>
                Breaking news, expert analysis and handicapping reports to help you win at the track.   Sign up to get the latest insights delivered straight to your inbox.
            </p>
            <div class="showafterform"><h3>Thanks for subscribing to US Racing!</h3></div>
			<div class="form-subscribe">
				<form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
					<!-- The following code must be included to ensure your sign-up form works properly. -->
					<input data-id="ca:input" type="hidden" name="ca" value="6a796fab-de62-40d3-874e-d0ddf9ba3426">
					<input data-id="list:input" type="hidden" name="list" value="1770667721">
					<input data-id="source:input" type="hidden" name="source" value="EFD">
					<input data-id="required:input" type="hidden" name="required" value="list,email,first_name">
					<input data-id="url:input" type="hidden" name="url" value="">

					<div data-id="First Name:p" class="input-field col-md-4 col-xs-12">
						<label data-id="First Name:label" data-name="first_name" class="ctct-form-required"></label>
						<input type="text" name="first_name" placeholder="Your First Name" data-id="First Name:input" />
					</div>

					<div data-id="Email Address:p"  class="input-field col-md-4 col-xs-12">
						<label data-id="Email Address:label" data-name="email" class="ctct-form-required"></label>
						<input type="email" name="email" placeholder="Your Best Email Address" data-id="Email Address:input"  />
					</div>
					<div class="input-field col-md-4 col-xs-12">
						<label></label>
						<input type="submit" value="Subscribe Now" />
					</div>
				</form>
			</div>
	</div>
</div>
<?php  } else { echo ''; }?>
