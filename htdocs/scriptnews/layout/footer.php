<!--point-->

<!-- === Footer Here ================================================== -->
<div class="footer">
<div class="container">

    <div class="row">

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/off-track-betting">Off Track Betting</a></li>
          <li><a href="/online-horse-wagering" >Online Horse Wagering</a></li>
          <li><a href="/odds">Horse Racing Odds</a></li>
          <li><a href="/bet-on-horses">Bet on Horses</a></li>
          <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li>
          <li><a href="/harness-racing">Harness Racing</a></li>
          <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
          <li><a href="/pegasus-world-cup/odds">Pegasus World Cup</a></li>
          <li><a href="/texas">Texas Horse Betting</a></li>
                    <li><a href="/royal-ascot">Royal Ascot Betting</a></li>

        <li><a href="/dubai-world-cup">Dubai World Cup Betting</a></li> 


<!--<li><a href="/santa-anita" >Santa Anita Horse Racing</a></li>
<li><a href="/keeneland" >Keeneland Horse Racing</a>  </li>
<li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
<li><a href="/delta-downs" >Delta Downs Horse Racing</a></li>
<li><a href="http://www.pickthewinner.com" >Pick the Winner</a>  </li>
<li><a href="http://www.breederscupbetting.com" >Breeders Cup Betting</a>  </li>
    <li><a href="/breeders-cup/betting" >Breeders' Cup Betting</a>
     <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a>-->
    </li>
                 </ul>
      </div> <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
          <li><a href="/kentucky-derby/future-wager">Kentucky Derby Future Wager</a></li>
          <li><a href="/bet-on/kentucky-derby">Bet on Kentucky Derby</a></li>
          <li><a href="/road-to-the-roses">Road to the Roses</a></li>
          <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
          <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
          <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
          <li><a href="/twin-spires">Twin Spires Betting</a></li>



        </ul>
      </div>
        <!--/col-md-3-->
	        <div class="col-md-3 margin-bottom-30">
	        <div class="headline">
	          	<h3>Triple Crown</h3>
	        </div>
		        <ul class="list-unstyled margin-bottom-20">
		        <!--	<li><a href="/kentucky-derby" >Kentucky Derby</a></li>-->
		        	<li><a href="/preakness-stakes" >Preakness Stakes</a></li>
		        	<li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>
		        	<li><a href="/preakness-stakes/betting" >Preakness Stakes Betting</a></li>
		        	<li><a href="/preakness-stakes/odds" >Preakness Stakes Odds</a></li>
					<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>
		          	<li><a href="/preakness-stakes/winners" >Preakness Stakes Winners</a></li>
		           <li><a href="/belmont-stakes" >Belmont Stakes</a></li>
		           <li><a href="/bet-on/belmont-stakes" >Bet on Belmont Stakes </a></li>
		        	<li><a href="/belmont-stakes/betting" >Belmont Stakes Betting</a></li>
		        	<li><a href="/belmont-stakes/odds" >Belmont Stakes Odds</a></li>
					<!--<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
		          	<li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> -->
		          	<li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
		        </ul>
			</div>
      <!--/col-md-3-->



      <!--/col-md-3-->
				   <!--<div class="col-md-3 margin-bottom-30">
				        <div class="headline">
				          <h3>US Horse Racing</h3>
				        </div>
				        <ul class="list-unstyled margin-bottom-20">
				        	<li><a href="/about" >About Us</a></li>-->

				          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
				       <li><a href="#" >Our Guarantee</a></li>
				          	<li><a href="/faqs" >FAQs</a></li>
				            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

				<!--<li><a href="/signup/" >Sign Up Today</a></li>
				        </ul>
						</div> -->
      <!--/col-md-3-->

      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>

        <ul class="social-icons">
          <li><a href="https://www.facebook.com/betusracing" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>
          <li><a href="https://twitter.com/betusracing" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
          <li><a href="https://plus.google.com/+Usracingcom" data-original-title="Google+"  title="Google+" class="social_google" rel="publisher"></a></li>
          <!--
          <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
          -->
        </ul>
        <br>   <br>
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
        	<li><a href="/about" >About Us</a></li>
          <!--	<li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
          	<li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
<li><a href="/signup?ref=news" rel="nofollow" >Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
<!--<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>-->

        </ul>



      </div> <!--/col-md-3-->

</div><!--/row-->



<div class="row friends">
	<center>Proudly featured on:
<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center>


     <!--
   <div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>

        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
-->
        </div>
</div>
<!-- /row/friends -->


</div><!--/container-->
</div><!--/footer-->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand">Copyright {'Y'|date}  <a href="/">US Racing</a>, All Rights Reserved</span>  <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/responsible-gaming" rel="nofollow" >Responsible Gambling</a> | <a href="/preakness-stakes/betting" >Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting" >Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting" >Kentucky Derby Betting</a>    </div>
    </div><!--/row-->

  </div><!--/container-->
</div><!--/copyright-->



<div class="wp-hide">{include file='scripts-footer.tpl'}</div>

            <!--
				<div class="site-info">
					
					<span class="sep"> | </span>
									</div>--><!-- close .site-info -->

<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<!--<script type="text/javascript" src="/assets/js/app.js"></script> -->
<script type="text/javascript">
jQuery(document).ready(function() {
	/*App.init();
	App.initFancybox();
	App.initRoyalSlider();
    Index.initIndex();    */
    jQuery('#ol').sidr({
        name: 'left-menu',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
		  jQuery('#left-menu').perfectScrollbar();
          jQuery("#ol i.fa").hide();
          jQuery("#ol i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu').perfectScrollbar('update');
          jQuery("#ol i.glyphicon").hide();
          jQuery("#ol i.fa").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#ol-left').sidr({
        name: 'left-menu-main',
        side: 'left',
        speed: 300,
        source: '#left-nav',
        renaming: false,
        onOpen: function () {
		  jQuery('#left-menu-main').perfectScrollbar();
          jQuery("#ol-left i.fa, #ol-left span.more").hide();
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").show().css('display', 'block');
          jQuery("#ol-left").addClass("active");
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'left-menu-main');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'left-menu-main');
            }
          });
        },
        onClose: function () {
		  jQuery('#left-menu-main').perfectScrollbar('update');
          jQuery("#ol-left i.glyphicon, #ol-left span.exit").hide();
          jQuery("#ol-left i.fa, #ol-left span.more").show().css('display', 'block');
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery("#ol-left").removeClass("active");
          jQuery('.sidr li').removeClass('active');
        }
      });
      jQuery('#or').sidr({
        name: 'nav-side',
        side: 'left',
        speed: 300,
        source: '.navbar-collapse',
        renaming: false,
        onOpen: function () {
		  jQuery('#nav-side').perfectScrollbar();
          jQuery("#or span").hide();
          jQuery("#or i.glyphicon").show();
          jQuery(".container").on('click touchstart', function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(window).resize(function () {
            jQuery.sidr('close', 'nav-side');
          });
          jQuery(document).keyup(function (e) {
            var key = e.keyCode || e.which;
            if (key === 27) {
              jQuery.sidr('close', 'nav-side');
            }
          });
        },
        onClose: function () {
		  jQuery('#nav-side').perfectScrollbar('update');
          jQuery("#or i.glyphicon").hide();
          jQuery("#or span").show();
          jQuery('.sidr li.active ul').slideUp('normal');
          jQuery('.sidr li').removeClass('active');
        }
      });
      // Sidr Accordian
      jQuery(".sidr a").click(function () {
        var link = jQuery(this);
        var closest_ul = link.closest("ul");
        var parallel_active_links = closest_ul.find(".active")
        var closest_li = link.closest("li");
        var link_status = closest_li.hasClass("active");
        var count = 0;
        closest_ul.find("ul").slideUp(function () {
          if (++count == closest_ul.find("ul").length)
            parallel_active_links.removeClass("active");
        });
        if (!link_status) {
          closest_li.children("ul").slideDown();
          closest_li.addClass("active");
      	  jQuery('.sidr').perfectScrollbar('update');
        }
    });
});
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest -->
<script type="text/javascript" async src="//www.usracing.com/wp-content/themes/usracing/js/pinit.js"></script>
<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'usracing.com');
  ga('send', 'pageview');
</script>
-->

<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
  	App.init();
  	App.initFancybox();
  	App.initRoyalSlider();
    try{
      Index.initIndex();
    }catch(e){ console.log( "assets/js/index.js -> does not imported" ); }
  });
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->
<!-- Google Analytics -->
<script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
ga('require', 'displayfeatures');
ga('send', 'pageview');
ga('require', 'linker');
ga('linker:autoLink', ['betusracing.ag', 'bet.usracing.com'] );
</script>
<script async src='//www.google-analytics.com/analytics.js'></script>
<!-- End Google Analytics -->





<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1710529292584739', {
em: 'insert_email_variable'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1710529292584739&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Bing Ads RP  -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4047476"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


<!--  OptinMonster ---><script>var om571fe1cc5ae65,om571fe1cc5ae65_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){om571fe1cc5ae65_poll(function(){if(window['om_loaded']){if(!om571fe1cc5ae65){om571fe1cc5ae65=new OptinMonsterApp();return om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmstr.com/app/js/api.min.js",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;om571fe1cc5ae65=new OptinMonsterApp();om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster -->

<!-- Lucky Orange-->
<script> window.lo_use_ip_lookups = true; </script>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 43153;
(function() {
var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
})();
</script>
<!-- End Lucky Orange-->






        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=825577710919578&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>	
    <!-- CTA Handicapping -->
<script type='text/javascript'>
   var localizedErrMap = {};
   localizedErrMap['required'] = 		'This field is required.';
   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
   localizedErrMap['list'] = 			'Please select at least one email list.';
   localizedErrMap['generic'] = 		'This field is invalid.';
   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
	localizedErrMap['state_province'] = 'Select a state/province';
   localizedErrMap['selectcountry'] = 	'Select a country';
   var postURL = 'https://visitor2.constantcontact.com/api/signup';
</script>
<script type='text/javascript' src='//www.usracing.com/wp-content/themes/usracing/js/signup-form.js'></script>
<!-- End CTA -->
</body>
</html>
