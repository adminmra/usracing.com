<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>

<!-- ======== BOX WITH TITLE ======================================== -->
<div class="block">
<h2 class="title-custom">Thoroughbred Racetracks</h2>
<div>
<!-- ========ENTER CONTENTS HERE======== -->

<?php	 		 	

//require_once LIBS_DIR."/dbconnect.inc";
include_once($_SERVER['DOCUMENT_ROOT'].'/Feeds/info/functions.php');
 include_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/class.xmlreader.php";
	 $xmlurl = $_SERVER['DOCUMENT_ROOT']."/FeedData/racetracks.xml";
	  	$xmlreader = new xmlreader1($xmlurl);
  	$xml = $xmlreader->parse();

 /* $sql_th = "SELECT * FROM racetracks WHERE live = 'Y' AND hta LIKE '%t%' ORDER BY name ";
												$result_th = mysql_query_w($sql_th);
												$count_th=0;*/
?>
							  			
                              <table width="100%" border="0" id="infoEntries">
							  <tbody>
                                 <tr>
                                    <th width="50%" ><strong>Track Name</strong></th>
                                    <th width="50%" ><strong>Racing Dates </strong></th>
                                 </tr>
                                <?php	 	
				for($i=0;$i<count($xml["allhorse"]["#"]["racetracks"]);$i++)
				{
					if($xml["allhorse"]["#"]["racetracks"][$i]["#"]["live"][0]["#"] == "Y" and strtoupper($xml["allhorse"]["#"]["racetracks"][$i]["#"]["hta"][0]["#"]) == "T")
					{
									if($count_th%2 == 1)
									{ echo "<tr class=\"odd\" >";}
									else
									{echo "<tr >";}
								?>
                                    <td><a href="racetrack?name=<?php	 	 echo urldecode(str_replace(" ","_",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"])); ?>"  title="<?php	 	 echo $xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"]; ?>"><?php	 	 echo $xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"]; ?></a> </td>
                                    <td > 
									 <?php	 	 
					$datetext="";
				if(strlen($xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateopen"][0]["#"]) > 0)
					{
				$datetext= date_formatterNew("M d, y",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateopen"][0]["#"]);
					}
					if(strlen($xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateclosed"][0]["#"]) > 0)
					{
					$datetext=$datetext."-".date_formatterNew("M d, y",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateclosed"][0]["#"]);
					}
					echo $datetext;
					?></td>
                                 </tr>
								<?php	 	
									$count_th++;
								}
							}
								?>
								</tbody>
                              </table>
  <?php	 	
							 /* $sql_h = "SELECT * FROM racetracks WHERE live = 'Y' AND hta LIKE '%h%' ORDER BY name ";
											$result_h = mysql_query_w($sql_h);
											$count_h=0;*/
							  ?>

<!-- ========END CONTENT ================ -->
</div>
</div>
<div id="box-shd"></div>
<!-- ======== END BOX WITH TITLE =================================== -->



<!-- ======== BOX WITH TITLE ======================================== -->
<div class="block">
<h2 class="title-custom">Harness Racing</h2>
<div>
<!-- ========ENTER CONTENTS HERE======== -->

<table width="100%" border="0" id="infoEntries">
<tbody>
                                 <tr>
                                    <th width="50%" ><strong>Track Name</strong></th>
                                    <th width="50%" ><strong> Racing Dates </strong></th>
									</tr>
								  <?php	 	
					for($i=0;$i<count($xml["allhorse"]["#"]["racetracks"]);$i++)
					{
					if($xml["allhorse"]["#"]["racetracks"][$i]["#"]["live"][0]["#"] == "Y" and strtoupper($xml["allhorse"]["#"]["racetracks"][$i]["#"]["hta"][0]["#"]) == "H")
					{
										if($count_h%2 == 1)
										{ echo "<tr class=\"odd\">";}
										else
										{echo "<tr >";}
									?>
                                 
                                    <td> <a href="racetrack?name=<?php	 	 echo urldecode(str_replace(" ","_",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"])); ?>"  title="<?php	 	 echo $xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"]; ?>"><?php	 	 echo $xml["allhorse"]["#"]["racetracks"][$i]["#"]["name"][0]["#"]; ?></a></td>
                                    <td >
									 <?php	 	 
					$datetext="";
				if(strlen($xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateopen"][0]["#"]) > 0)
					{
				$datetext= date_formatterNew("M d, y",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateopen"][0]["#"]);
					}
					if(strlen($xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateclosed"][0]["#"]) > 0)
					{
					$datetext=$datetext."-".date_formatterNew("M d, y",$xml["allhorse"]["#"]["racetracks"][$i]["#"]["dateclosed"][0]["#"]);
					}
					echo $datetext;
					?>
									</td>
                                 </tr>
								 <?php	 	
										$count_h++;
									}
								}
									?>
									</tbody>
                             </table>


<!-- ========END CONTENT ================ -->
</div>
</div>
<div id="box-shd"></div>
<!-- ======== END BOX WITH TITLE =================================== -->


