<style>
#col3 #content-box { padding: 0; }
#col3 #infoEntries { margin: 0; }
</style>

<?php	 		 	
require_once LIBS_DIR.'/dbconnect.inc';
?>
<script type="text/javascript">

function Nflschedule(a)
{
	Http.get({
				url: "/Feeds/info/race-schedule.php?nflweek="+a.value,
				callback: Nflschedulefile,
				cache: Http.Cache.Get
				});
				return false;	
}

function Getschedule(a,b,c)
{
	Http.get({
				url: "/Feeds/info/raceschedule.php?nflweek="+a+"&order="+b+"&type="+c,
				callback: Nflschedulefile,
				cache: Http.Cache.Get
				});
				return false;	
}

function Nflschedulefile(xmlreply)
{
	var request = xmlreply.responseText;
	//alert(request);	
	document.getElementById("gametable").innerHTML=request;
}

</script>
 <div style="width: 100%; float:left; height:auto; padding:0px;">
<span style="float:right; padding:10px 0; height:auto;">
<img style="float:left; margin: 2px 7px; 0 0; " src="/themes/images/nfl-selectdate.gif" />

<select name="weekselect" onChange="return Nflschedule(this);">
<option value="1" <?php	 	 if(date('m')=="1"){echo "selected"; } ?>   >January</option>
<option value="2"  <?php	 	 if(date('m')=="2"){echo "selected"; } ?> >February</option>
<option value="3"  <?php	 	 if(date('m')=="3"){echo "selected"; } ?> >March</option>
<option value="4"  <?php	 	 if(date('m')=="4"){echo "selected"; } ?> >April</option>
<option value="5"  <?php	 	 if(date('m')=="5"){echo "selected"; } ?> >May</option>
<option value="6"  <?php	 	 if(date('m')=="6"){echo "selected"; } ?> >June</option>
<option value="7"  <?php	 	 if(date('m')=="7"){echo "selected"; } ?> >July</option>
<option value="8"  <?php	 	 if(date('m')=="8"){echo "selected"; } ?> >August</option>
<option value="9"  <?php	 	 if(date('m')=="9"){echo "selected"; } ?> >September</option>
<option value="10" <?php	 	 if(date('m')=="10"){echo "selected"; } ?>  >October</option>
<option value="11"  <?php	 	 if(date('m')=="11"){echo "selected"; } ?> >November</option>
<option value="12"  <?php	 	 if(date('m')=="12"){echo "selected"; } ?> >December</option>
</select>
</span>
</div>
<div id="gametable" style="clear:both;">
<?php	 	
require_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/race-schedule.php";
?>
	</div>