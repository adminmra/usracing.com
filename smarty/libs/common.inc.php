<?php	 		 	

/**
 * Returns SOAP connection to Dailyracingnews web service
 */
//ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
//error_reporting(E_ALL);

define('UNICODE_MULTIBYTE', 1);

include(LIBS_DIR . 'dbconnect.inc');

function ahr_dailyracingnews_connect() {
	static $client;

	if (!$client) {
		require_once('soap/lib/nusoap.php');
		$client = new nusoap_client('http://www.dailyracingnews.com/webservices/contentfeed.asmx?wsdl', TRUE);
	}

	return $client;
}

/**
 * Fetch the total # of articles available
 */
function ahr_get_news_total_count() {
	// $client = ahr_dailyracingnews_connect();
	// $total_records_result = $client->call("GetTotalRecords", array('contentTypeId' => 3));
	//return $total_records_result['GetTotalRecordsResult'];
	//$query = "select COUNT(*) from newsarticle";
	$query = "select COUNT(*) from newsarchive where typetosee = 'OTH' ";

	$results = mysql_query_w($query);
	$total_count = 0;
	while ($row = mysql_fetch_array($results)) {
		$total_count = $row['COUNT(*)'];
	}
	return $total_count;
}

function ahr_get_news_total_count_new() {
	// $client = ahr_dailyracingnews_connect();
	// $total_records_result = $client->call("GetTotalRecords", array('contentTypeId' => 3));
	//return $total_records_result['GetTotalRecordsResult'];
	$query = "select COUNT(*) from ahrnews";
	$results = mysql_query_w($query);
	$total_count = 0;
	while ($row = mysql_fetch_array($results)) {
		$total_count = $row['COUNT(*)'];
	}
	return $total_count;
}

function ahr_abbr_html($Html, $Len) {
	if (strlen($Html) > $Len) {
		return substr($Html, 0, $Len) . '...';
	} else {
		return $Html;
	}
}

function format_rss_item($title, $link, $description, $args = array()) {
	$output = "<item>\n";
	$output .= ' <title>' . stripslashes($title) . "</title>\n";
	$output .= ' <link>' . $link . "</link>\n";
	$output .= ' <description>' . stripslashes($description) . "</description>\n";
	$output .= format_xml_elements($args);
	$output .= "</item>\n";

	return $output;
}

function format_xml_elements($array) {
	$output = '';
	foreach ($array as $key => $value) {
		if (is_numeric($key)) {
			if ($value['key']) {
				$output .= ' <' . $value['key'];
				if (isset($value['attributes']) && is_array($value['attributes'])) {
					$output .= drupal_attributes($value['attributes']);
				}

				if (isset($value['value']) && $value['value'] != '') {
					$output .= '>' . (is_array($value['value']) ? format_xml_elements($value['value']) : $value['value']) . '</' . $value['key'] . ">\n";
				} else {
					$output .= " />\n";
				}
			}
		} else {
			$output .= ' <' . $key . '>' . (is_array($value) ? format_xml_elements($value) : $value) . "</$key>\n";
		}
	}
	return $output;
}

function nextdayformatted($date, $dayafter) {
	$datearray = explode("-", $date);
	return date('F j, Y', mktime(0, 0, 0, $datearray[1], $datearray[2] + $dayafter, $datearray[0]));
}

function weekfirstday() {
	$today = date('w');
	return date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - ($today - 1), date('Y')));
}

function nextday($date, $dayafter) {
	$datearray = explode("-", $date);
	return date('Y-m-d', mktime(0, 0, 0, $datearray[1], $datearray[2] + $dayafter, $datearray[0]));
}

function variable_get($name, $default) {
	global $conf;

	return isset($conf[$name]) ? $conf[$name] : $default;
}

function format_rss_channel($title, $link, $description, $items, $langcode = NULL, $args = array()) {
	global $language;
	$langcode = $langcode ? $langcode : $language->language;

	$output = "<channel>\n";
	$output .= ' <title>' . $title . "</title>\n";
	$output .= ' <link>' . $link . "</link>\n";

	// The RSS 2.0 "spec" doesn't indicate HTML can be used in the description.
	// We strip all HTML tags, but need to prevent double encoding from properly
	// escaped source data (such as &amp becoming &amp;amp;).
	$output .= ' <description>' . strip_tags($description) . "</description>\n";
	$output .= ' <language>' . 'en' . "</language>\n";
	$output .= format_xml_elements($args);
	$output .= $items;
	$output .= "</channel>\n";
	return $output;
}

function pager($tags = array(), $limit = 10, $element = 0, $attributes = array(), $quantity = 9) {
	global $pager_page_array, $pager_total;
	$parameters = array();

	// Calculate various markers within this pager piece:
	// Middle is used to "center" pages around the current page.
	$pager_middle = ceil($quantity / 2);
	// current is the page we are currently paged to
	$pager_current = $pager_page_array[$element] + 1;
	// first is the first page listed by this pager piece (re quantity)
	$pager_first = $pager_current - $pager_middle + 1;
	// last is the last page listed by this pager piece (re quantity)
	$pager_last = $pager_current + $quantity - $pager_middle;
	// max is the maximum page number
	$pager_max = $pager_total[$element];
	// End of marker calculations.
	// Prepare for generation loop.
	$i = $pager_first;

	if ($pager_last > $pager_max) {
		// Adjust "center" if at end of query.
		$i = $i + ($pager_max - $pager_last);
		$pager_last = $pager_max;
	}
	if ($i <= 0) {
		// Adjust "center" if at start of query.
		$pager_last = $pager_last + (1 - $i);
		$i = 1;
	}
	// End of generation loop preparation.

	$li_first = pager_first(isset($tags[0]) ? $tags[0] : t('<< <span>First</span>'), $limit, $element, $parameters);
	$li_previous = pager_previous(isset($tags[1]) ? $tags[1] : t('< <span>Prev</span>'), $limit, $element, 1, $parameters);
	$li_next = pager_next(isset($tags[3]) ? $tags[3] : t('<span>Next</span> >'), $limit, $element, 1, $parameters);
	$li_last = pager_last(isset($tags[4]) ? $tags[4] : t('<span>Last</span> >>'), $limit, $element, $parameters);

	if ($pager_total[$element] > 1) {
		if ($li_first) {
			$items[] = array(
				'class' => 'pager-first',
				'data' => $li_first,
			);
		}
		if ($li_previous) {
			$items[] = array(
				'class' => 'pager-previous',
				'data' => $li_previous,
			);
		}

		// When there is more than one page, create the pager list.
		if ($i != $pager_max) {
			if ($i > 1) {
				$items[] = array(
					'class' => 'pager-ellipsis',
					'data' => '...',
				);
			}

			// Now generate the actual pager piece.
			for (; $i <= $pager_last && $i <= $pager_max; $i++) {
				if ($i < $pager_current) {
					$items[] = array(
						'class' => 'pager-item',
						'data' => pager_previous($i, $limit, $element, ($pager_current - $i), $parameters),
					);
				}
				if ($i == $pager_current) {
					$items[] = array(
						'class' => 'pager-current',
						'data' => '<span>' . $i . '</span>',
					);
				}
				if ($i > $pager_current) {
					$items[] = array(
						'class' => 'pager-item',
						'data' => pager_next($i, $limit, $element, ($i - $pager_current), $parameters),
					);
				}
			}
			if ($i < $pager_max) {
				$items[] = array(
					'class' => 'pager-ellipsis',
					'data' => '...',
				);
			}
		}
		// End generation.
		if ($li_next) {
			$items[] = array(
				'class' => 'pager-next',
				'data' => $li_next,
			);
		}
		if ($li_last) {
			$items[] = array(
				'class' => 'pager-last',
				'data' => $li_last,
			);
		}
		return item_list($items, NULL, 'ul', array('class' => 'pager'));
	}
}

function pager_first($text, $limit, $element = 0, $parameters = array()) {
	global $pager_page_array;
	$output = '';

	// If we are anywhere but the first page
	if ($pager_page_array[$element] > 0) {
		$output = pager_link($text, pager_load_array(0, $element, $pager_page_array), $element, $parameters);
	}

	return $output;
}

function pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
	global $pager_page_array;
	$output = '';

	// If we are anywhere but the first page
	if ($pager_page_array[$element] > 0) {
		$page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

		// If the previous page is the first page, mark the link as such.
		if ($page_new[$element] == 0) {
			$output = pager_first($text, $limit, $element, $parameters);
		}
		// The previous page is not the first page.
		else {
			$output = pager_link($text, $page_new, $element, $parameters);
		}
	}

	return $output;
}

function pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
	global $pager_page_array, $pager_total;
	$output = '';

	// If we are anywhere but the last page
	if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
		$page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
		// If the next page is the last page, mark the link as such.
		if ($page_new[$element] == ($pager_total[$element] - 1)) {
			$output = pager_last($text, $limit, $element, $parameters);
		}
		// The next page is not the last page.
		else {
			$output = pager_link($text, $page_new, $element, $parameters);
		}
	}

	return $output;
}

function pager_last($text, $limit, $element = 0, $parameters = array()) {
	global $pager_page_array, $pager_total;
	$output = '';

	// If we are anywhere but the last page
	if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
		$output = pager_link($text, pager_load_array($pager_total[$element] - 1, $element, $pager_page_array), $element, $parameters);
	}

	return $output;
}

function pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
	$page = isset($_GET['page']) ? $_GET['page'] : '';

	if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
		$parameters['page'] = $new_page;
	}

	$query = array();
	if (count($parameters)) {
		$query[] = drupal_query_string_encode($parameters, array());
	}
	$querystring = pager_get_querystring();
	if ($querystring != '') {
		$query[] = $querystring;
	}

	// Set each pager link title
	if (!isset($attributes['title'])) {
		static $titles = NULL;
		if (!isset($titles)) {
			$titles = array(
				t('<< first') => t('Go to first page'),
				t('< previous') => t('Go to previous page'),
				t('next >') => t('Go to next page'),
				t('last >>') => t('Go to last page'),
			);
		}
		if (isset($titles[$text])) {
			$attributes['title'] = $titles[$text];
		} else if (is_numeric($text)) {
			$attributes['title'] = t('Go to page @number', array('@number' => $text));
		}
	}
//print_r($_SERVER);
	return l($text, 'news', array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL));
}

function pager_load_array($value, $element, $old_array) {
	$new_array = $old_array;
	// Look for empty elements.
	for ($i = 0; $i < $element; $i++) {
		if (!$new_array[$i]) {
			// Load found empty element with 0.
			$new_array[$i] = 0;
		}
	}
	// Update the changed element.
	$new_array[$element] = (int) $value;
	return $new_array;
}

function drupal_query_string_encode($query, $exclude = array(), $parent = '') {
	$params = array();

	foreach ($query as $key => $value) {
		$key = rawurlencode($key);
		if ($parent) {
			$key = $parent . '[' . $key . ']';
		}

		if (in_array($key, $exclude)) {
			continue;
		}

		if (is_array($value)) {
			$params[] = drupal_query_string_encode($value, $exclude, $key);
		} else {
			$params[] = $key . '=' . rawurlencode($value);
		}
	}

	return implode('&', $params);
}

function pager_get_querystring() {
	static $string = NULL;
	if (!isset($string)) {
		$string = drupal_query_string_encode($_REQUEST, array_merge(array('q', 'page', 'pass'), array_keys($_COOKIE)));
	}
	return $string;
}

function item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {
	$output = '<div class="item-list">';
	if (isset($title)) {
		$output .= '<h3>' . $title . '</h3>';
	}

	if (!empty($items)) {
		$output .= "<$type" . drupal_attributes($attributes) . '>';
		$num_items = count($items);
		foreach ($items as $i => $item) {
			$attributes = array();
			$children = array();
			if (is_array($item)) {
				foreach ($item as $key => $value) {
					if ($key == 'data') {
						$data = $value;
					} elseif ($key == 'children') {
						$children = $value;
					} else {
						$attributes[$key] = $value;
					}
				}
			} else {
				$data = $item;
			}
			if (count($children) > 0) {
				$data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
			}
			if ($i == 0) {
				$attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] . ' first');
			}
			if ($i == $num_items - 1) {
				$attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] . ' last');
			}
			$output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
		}
		$output .= "</$type>";
	}
	$output .= '</div>';
	return $output;
}

function drupal_attributes($attributes = array()) {
	if (is_array($attributes)) {
		$t = '';
		foreach ($attributes as $key => $value) {
			$t .= " $key=" . '"' . $value . '"';
		}
		return $t;
	}
}

function t($string, $args = array(), $langcode = NULL) {
	global $language;
	static $custom_strings;

	$langcode = 'en';

	// First, check for an array of customized strings. If present, use the array
	// *instead of* database lookups. This is a high performance way to provide a
	// handful of string replacements. See settings.php for examples.
	// Cache the $custom_strings variable to improve performance.

	if (empty($args)) {
		return $string;
	} else {
		// Transform arguments before inserting them.
		foreach ($args as $key => $value) {
			switch ($key[0]) {
				case '@':
					// Escaped only.
					$args[$key] = $value;
					break;

				case '%':
				default:
					// Escaped and placeholder.
					$args[$key] = placeholder($value);
					break;

				case '!':
				// Pass-through.
			}
		}
		return strtr($string, $args);
	}
}

function l($text, $path, $options = array()) {
	// Merge in defaults.
	$options += array(
		'attributes' => array(),
		'html' => FALSE,
	);

	if (isset($options['attributes']['title']) && strpos($options['attributes']['title'], '<') !== FALSE) {
		$options['attributes']['title'] = strip_tags($options['attributes']['title']);
	}
	return '<a href="' . url($path, $options) . '"' . drupal_attributes($options['attributes']) . '>' . ($options['html'] ? $text : $text) . '</a>';
}

function placeholder($text) {
	return '<em>' . $text . '</em>';
}

function url($path = NULL, $options = array()) {
	// Merge in defaults.
	$options += array(
		'fragment' => '',
		'query' => '',
		'absolute' => FALSE,
		'alias' => FALSE,
		'prefix' => ''
	);
	if (!isset($options['external'])) {
		// Return an external link if $path contains an allowed absolute URL.
		// Only call the slow filter_xss_bad_protocol if $path contains a ':' before
		// any / ? or #.
		$colonpos = strpos($path, ':');
		$options['external'] = ($colonpos !== FALSE && !preg_match('![/?#]!', substr($path, 0, $colonpos)) == $path);
	}

	if ($options['fragment']) {
		$options['fragment'] = '#' . $options['fragment'];
	}
	if (is_array($options['query'])) {
		$options['query'] = drupal_query_string_encode($options['query']);
	}

	if ($options['external']) {
		// Split off the fragment.
		if (strpos($path, '#') !== FALSE) {
			list($path, $old_fragment) = explode('#', $path, 2);
			if (isset($old_fragment) && !$options['fragment']) {
				$options['fragment'] = '#' . $old_fragment;
			}
		}
		// Append the query.
		if ($options['query']) {
			// $path .= (strpos($path, '?') !== FALSE ? '&' : '?') . $options['query'];
		}
		// Reassemble.
		return $path . $options['fragment'];
	}
	global $base_url;
	static $script;

	if (!isset($script)) {
		// On some web servers, such as IIS, we can't omit "index.php". So, we
		// generate "index.php?q=foo" instead of "?q=foo" on anything that is not
		// Apache.
		$script = (strpos($_SERVER['SERVER_SOFTWARE'], 'Apache') === FALSE) ? 'index.php' : '';
	}

	if (!isset($options['base_url'])) {
		// The base_url might be rewritten from the language rewrite in domain mode.
		$options['base_url'] = $base_url;
	}

	// Preserve the original path before aliasing.
	$original_path = $path;

	$base = $options['absolute'] ? $options['base_url'] . '/' : base_path();
	$prefix = empty($path) ? rtrim($options['prefix'], '/') : $options['prefix'];
	$path = drupal_urlencode($prefix . $path);

	if (variable_get('clean_url', '0')) {
		// With Clean URLs.
		if ($options['query']) {
			return $base . $path . '?' . $options['query'] . $options['fragment'];
		} else {
			return $base . $path . $options['fragment'];
		}
	} else {
		// Without Clean URLs.
		$variables = array();
		if (!empty($path)) {
			//$variables[] = $path;
		}
		if (!empty($options['query'])) {
			$variables[] = $options['query'];
		}
		if ($query = join('&', $variables)) {
			return $base . '/' . $path . '?' . $query . $options['fragment'];
		} else {
			return $base . '/' . $path . $options['fragment'];
		}
	}
}

function drupal_urlencode($text) {
	if (variable_get('clean_url', '0')) {
		return str_replace(array('%2F', '%26', '%23', '//'), array('/', '%2526', '%2523', '/%252F'), rawurlencode($text));
	} else {
		return str_replace('%2F', '/', rawurlencode($text));
	}
}

function base_path() {
	//return 'http://www.allhorseracing.ag';
	return '';
}

function pager_list($limit, $element = 0, $quantity = 5, $text = '', $attributes = array()) {
	global $pager_page_array, $pager_total;

	$output = '<li class="pager-item">';
	// Calculate various markers within this pager piece:
	// Middle is used to "center" pages around the current page.
	$pager_middle = ceil((int) $quantity / 2);
	// offset adds "offset" second page
	$pager_offset = (int) $pager_page_array[$element] % (int) $limit;

	// current is the page we are currently paged to
	if (($pager_current = (ceil(($pager_page_array[$element] + 1) / $limit))) < 1) {
		$pager_current = 1;
	}
	// first is the first page listed by this pager piece (re quantity)
	$pager_first = (int) $pager_current - (int) $pager_middle + 1;
	// last is the last page listed by this pager piece (re quantity)
	$pager_last = (int) $pager_current + (int) $quantity - (int) $pager_middle;
	// max is the maximum number of pages content can is divided into
	if (!$pager_max = (ceil($pager_total[$element] / $limit))) {
		$pager_max = 1;
	}
	if ((int) $pager_offset) {
		// adjust for offset second page
		$pager_max++;
		$pager_current++;
	}
	// End of marker calculations.
	// Prepare for generation loop.
	$i = (int) $pager_first;
	if ($pager_last > $pager_max) {
		// Adjust "center" if at end of query.
		$i = $i + (int) ($pager_max - $pager_last);
		$pager_last = $pager_max;
	}
	if ($i <= 0) {
		// Adjust "center" if at start of query.
		$pager_last = $pager_last + (1 - $i);
		$i = 1;
	}
	// End of generation loop preparation.
	// When there is more than one page, create the pager list.
	if ($i != $pager_max) {
		$output .= $text;
		if ($i > 1) {
			$output .= '<div class="pager-list-dots-left">...</div>';
		}

		// Now generate the actual pager piece.
		for (; $i <= $pager_last && $i <= $pager_max; $i++) {
			if ($i < $pager_current) {
				$output .=pager_previous($i, $limit, $element, ($pager_current - $i), $attributes) . " ";
			}
			if ($i == $pager_current) {
				$output .= '<strong>' . $i . '</strong> ';
			}
			if ($i > $pager_current) {
				$output .= pager_next($i, $limit, $element, ($i - $pager_current), $attributes) . " ";
			}
		}

		if ($i < $pager_max) {
			$output .= '<li class="pager-ellipsis">...</li>';
		}
	}
	$output .= '</li>';

	return $output;
}

/**
 * News article page view,
 *  @see ahr_news_article_load()
 */
function allhorseracing_news_article_view($article) {

	$results = mysql_query_w("select * from outguard where id = '6'");
	$results_msg = mysql_query_w("select * from outgurd_message where id='1'");

	$row = mysql_fetch_array($results);
	$rows[] = $row;
	if ($row['status'] == 'dead') {
		$row_msg = mysql_fetch_array($results_msg);
		return $row_msg["default_msg"];
		//$vars['articles'] = array();
		//$vars['articles'][$aid] = $row_msg["default_msg"];
	} else {
		//$breadcrumb = drupal_get_breadcrumb();
		//$breadcrumb[] = l(t('Racing News'), 'news');
		//drupal_set_breadcrumb($breadcrumb);
		//drupal_set_title($article->title);

		if (!$article->aid) {
			//return drupal_not_found();
		}

		//return theme('ahr_racing_news_article', $article);
	}//else switch
}

function theme_ahr_racing_news_article($article) {
	$output = '<div id="article-' . $article->aid . '" class="article">';
	/* $output .= '<div style="display: block; height: 16px; width:125px; padding:0;" ><!-- AddThis Button BEGIN -->
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b7342703113cb61"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b7342703113cb61"></script>
<!-- AddThis Button END --></div>'; */
	$output .= '<div class="story">' . $article->body . '</div>';
	$output .= '<div class="news-date">' . $article->created . '</div>';
	$output .= '</div>';

	return $output;
}

/**
 * Formatting an article teaser item for the racing news block
 * @param $article An Article result array from Dailyracingnews web services call
 * /news Page uses this
 */
function ahr_racing_news_article_teaser($article, $first = FALSE) {
	$class = ($first) ? 'story-lng' : 'story';
	$articlePic = "";
	if (strlen(trim($article['pageuri'])) == 0) {
		$URLNEW = '/news/' . $article['ContentId'] . '?title=' . str_replace('%26%2339%3Bs', '', str_replace('%20', '-', rawurlencode(strtolower($article['Title']))));
	} else {
		$URLNEW = '/news/' . $article['pageuri'];
	}
	if ($article['pictureThumb'] == '') {
		//$articlePic = "http://usracing.com/images/usr-logo.png";
	} else {
		$articlePic = '<dt><a href ="' . $article['pictureThumb'] . '" class="thumbnail fancybox-button zoomer" data-rel="fancybox-button"><span class="overlay-zoom"><img class="img-responsive" src="' . $article['pictureThumb'] . '" /><div class="zoom-icon"></div></span></a></dt>';
	}
	$output =  '<dl class="dl-horizontal ' . $class . '" id="news-story-' . $article['ContentId'] . '">';
	$output .=  $articlePic;
	$output .= '  <dd>';
	$output .= '    <h3 class="news-title"><a href="' . $URLNEW . '">' . stripslashes($article['Title']) . '</a></h3>';
	
	//$output .= '<div class="teaser">'. ahr_abbr_html(stripslashes($article['Teaser']),90) .'<a href="'.$URLNEW.'">Read more</a></div>';
	$output .= '    <div class="teaser">' . ahr_abbr_html(stripslashes($article['Teaser']), 350) . '</div>';
	$output .= '    <div class="news-date">' . $article['DateCreated'] . '</div>';
	$output .= '  </dd>';
	$output .= '</dl>';
	/*$output .= '<!-- ';
	$output .= print_r($article, true);
	$output .= ' -->';*/

	return $output;
}

function ahr_racing_news_article_teaser_new($article, $first = FALSE) {
	$class = ($first) ? 'story-lng' : 'story';
	$pdate = explode("-", $article["pdate"]);
	$output = '<dl class="dl-horizontal ' . $class . '" id="news-story-' . $article['id'] . '">';
	$output .= '<dd>';
	$output .= '<h3 class="news-title"><a href="/news/horsebetting/' . $article['url'] . '">' . $article['title'] . '</a></h3>';
	$output .= '<div class="teaser">' . ahr_abbr_html($article['teasure'], 220) . '</div>';
	$output .= '<div class="news-date">' . date('F d, Y', mktime(0, 0, 0, $pdate[1], $pdate[2], $pdate[0])) . '</div>';
	$output .= '</dd>';
	$output .= '</dl>';
	return $output;
}

function template_preprocess_ahr_racing_news_page() {
	global $path;
	$results = mysql_query_w("select * from outguard where id = '6'");
	$results_msg = mysql_query_w("select * from outgurd_message where id='1'");
	$row = mysql_fetch_array($results);
	$rows[] = $row;
	if ($row['status'] == 'dead') {
		$row_msg = mysql_fetch_array($results_msg);
		//return $row_msg["default_msg"];
		$articles = array();
		$articles[] = $row_msg["default_msg"];
	} else if (isset($_GET['title'])) {
		$contentId = arg(2, $path);

		$output = ahr_get_news_articles(0, 0, FALSE, $contentId);
		return $output;
	} else {
		$total = ahr_get_news_total_count();
		$limit = 10;
		$page = (isset($_GET['page'])) ? $_GET['page'] + 1 : 1;

		$articles = ahr_get_news_articles($limit, $page);
		//print_r($articles);
		foreach ($articles as $article) {
			$articlesHTML[] = ahr_racing_news_article_teaser($article);
		}
		$pager = ahr_racing_news_page_pager($articles, $limit, $total);
	}// else switch
	$output = array('articlesHTML' => $articlesHTML, 'pager' => $pager);
	return $output;
}

function ahr_new_racing_news_list() {
	$total = ahr_get_news_total_count_new();
	$limit = 20;
	$page = (isset($_GET['page'])) ? $_GET['page'] + 1 : 1;

	$articles = ahr_get_news_articles_new($limit, $page);
	//print_r($articles);
	foreach ($articles as $article) {
		$articlesHTML[] = ahr_racing_news_article_teaser_new($article);
	}
	$pager = ahr_racing_news_page_pager($articles, $limit, $total);
	$pagerNew = str_replace('/news', '/news/horsebetting', $pager);
	$output = array('articlesHTML' => $articlesHTML, 'pager' => $pagerNew);
	return $output;
}

function ahr_racing_news_page_pager($articles, $limit, $total) {
	global $pager_page_array, $pager_total;
	$page = isset($_GET['page']) ? $_GET['page'] : '';
	$element = 0;

	// Convert comma-separated $page to an array, used by other functions.
	$pager_page_array = explode(',', $page);

	$pager_total[$element] = ceil($total / $limit);
	$pager_page_array[$element] = max(0, min((int) $pager_page_array[$element], ((int) $pager_total[$element]) - 1));

	return pager(array(), $limit);
}

/**
 * Fetching articles from the dailyracingnews web service, and caching them locally within Drupal
 */
function ahr_get_news_articles($items_per_page = 10, $page_number = 1, $nocache = FALSE, $id = NULL) {
	//$cid = 'ahr_news_'. $items_per_page .'_'. $page_number .'_articles';
	//$expires_ts = time() + 3600;
	//$articles = (!$nocache) ? cache_get($cid) : FALSE;


	if (!isset($id)) {
		/*
		  $client = ahr_dailyracingnews_connect();

		  // Fetch list of articles
		  $params = array(
		  'contentTypeId' => 3,
		  'pageSize' => $items_per_page,
		  'pageNumber' => $page_number,
		  'orderByColumn' => 'datecreated',
		  'orderByDirection' => 'desc'
		  );
		  $article_list_result = $client->call("GetArticleList", $params);
		  $article_list_result = $article_list_result['GetArticleListResult']['Article']; */
		$limit = ahr_get_news_articles_limit($items_per_page, $page_number);
		//$query = "select ContentIdART as ContentId, TitleART as Title, HtmlART as Html, DateCreatedART as DateCreated,Teaser from newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt order by STR_TO_DATE('DateCreatedART', '%m/%d/%Y') desc".$limit;
		//$query = "select ContentIdART as ContentId, TitleART as Title, HtmlART as Html, DateCreatedART as DateCreated, newsarchive.timecreate as timecreate, Teaser from newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt order by newsarchive.timecreate desc".$limit;
		$query = "select pictureThumb, ContentIdART as ContentId, TitleART as Title, HtmlART as Html, DateCreatedART as DateCreated, newsarchive.timecreate as timecreate, newsarchive.pageuri, Teaser from newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt where newsarchive.typetosee = 'OTH' or newsarchive.typetosee = 'AHR' order by newsarchive.timecreate desc" . $limit;

		$results = mysql_query_w($query);

		$article_list_result = array();
		while ($row = mysql_fetch_assoc($results)) {
			array_push($article_list_result, $row);
		}
		$articles = $article_list_result;
		return $articles;
	} else {

		// Processing each article item returned
		$articles = _ahr_parse_articles_result_item($id);
		return $articles;
	}
}

function ahr_get_news_articles_new($items_per_page = 10, $page_number = 1, $nocache = FALSE, $id = NULL) {

	$limit = ahr_get_news_articles_limit($items_per_page, $page_number);
	$query = "select * from ahrnews order by pdate desc" . $limit;
	$results = mysql_query_w($query);

	$article_list_result = array();
	while ($row = mysql_fetch_assoc($results)) {
		array_push($article_list_result, $row);
	}
	$articles = $article_list_result;
	return $articles;
}

function ahr_get_news_articles_limit($items_per_page = 10, $page_number = 1) {
	$x = $items_per_page * $page_number;
	$y = $x - $items_per_page;
	return " limit " . $y . ", " . $items_per_page;
}

function _ahr_parse_articles_result_item($article) {
	/*
	  $client = ahr_dailyracingnews_connect();

	  $created = $article['DateCreated'];

	  // Mapping initial article fields
	  $item = array(
	  'aid' => $article['ContentId'],
	  'title' => $article['Title'],
	  'subtitle' => $article['Teaser'],
	  'created' => $created,
	  ); */
	$query = "select TitleART as Title, DateCreatedART as DateCreated,HtmlART as Html from newsarticle where ContentIdART =" . $article;
	$results = mysql_query_w($query);

	$article_result = array();
	while ($row = mysql_fetch_assoc($results)) {
		$article_result = $row;
	}
	// Adding on the full article text
	//$article_result = $client->call("GetArticle", array('ArticleId' => $article));
	//$xmlstr = $article_result['GetArticleResult']['Html'];
	//$xml = simplexml_load_string($xmlstr);
	//$item['xml'] = $article_result['GetArticleResult']['Html'];
	//$format = variable_get('filter_default_format', 1);
	//$teaser_size = 140;
	//$item['body'] = check_markup($xml->Text->asXML(), $format, FALSE);
	//$item['teaser'] = ahr_abbr_html(strip_tags($article_result['GetArticleResult']['Html']), 115); //_ahr_article_teaser($item['Teaser'], $format, $teaser_size);
	//$item['body'] = "";
	// $item['teaser'] = "";
	//print_r($item);
	return $article_result;
}

///new fuction //
function _ahr_parse_articles_result_item_new($article) {
	$query = "select * from ahrnews where url = '" . $article . "' ";
	$results = mysql_query_w($query);

	$article_result = array();
	while ($row = mysql_fetch_assoc($results)) {
		$article_result = $row;
	}

	return $article_result;
}

function _ahr_parse_articles_result_item_main($article) {
	//$query = "select * from newsarticle where url = '".$article."' ";
	$numrow = 0;
	$query_req = " select * from newsarchive where pageuri = '" . $article . "' and typetosee = 'OTH' ";
	$results_req = mysql_query_w($query_req);
	$numrow = mysql_num_rows($results_req);
	$data = mysql_fetch_object($results_req);

	if ($numrow > 0) {
		$query = " select * from newsarticle where ContentIdART = '" . $data->ContentId . "'  ";
		$results = mysql_query_w($query);

		$article_result = array();
		while ($row = mysql_fetch_assoc($results)) {
			$article_result = $row;
			$article_result['id'] = $data->ContentId;
			$article_result['featured'] = $data->featured;
		}
	} else {
		$article_result = array();
	}

	return $article_result;
}

/////

function allhorseracing_news_jack_feed() {
	global $base_url, $language;
	$aids = FALSE;
	$channel = array();
	$limit = 20;

	if ($aids === FALSE) {
		$articles = ahr_get_news_articles_new($limit, 1);
//    $aids = $articles;
	}

	$item_length = 'teaser';
	$namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

	$items = '';
	foreach ($articles as $aid) {
		// Load the specified node:
		$item = $aid;
		// $item->build_mode = NODE_BUILD_RSS;
		$absolute = TRUE;
		$query = NULL;
		$fragment = NULL;
		$item_link = 'http://www.usracing.com/news/horsebetting/' . $item['url'];
		$extra = array('pubDate' => date('r', strtotime($item['pdate'])));
		$item_title = $item['title'];
		// Prepare the item description
		switch ($item_length) {
			/* case 'fulltext':
			  $item_text = $item->body;
			  break; */
			case 'teaser':
				$item_text = $item['teasure'] . "....";
				//  if (!empty($item->readmore)) {
				//  $item_text .= '<p><a href="news/'.$item['ContentId'].'" target="_blank">read more</a></p>';
				//  }
				break;
			/* case 'title':
			  $item_text = '';
			  break; */
		}

		$items .= format_rss_item($item_title, $item_link, $item_text, $extra);
	}

	$channel_defaults = array(
		'version' => '2.0',
		'title' => 'Us Racing',
		'link' => 'http://www.usracing.com',
		'description' => '',
		'language' => 'en'
	);
	$channel = array_merge($channel_defaults, $channel);

	$output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	$output .= '<rss version="' . $channel["version"] . '" xml:base="' . $base_url . '" ' . drupal_attributes($namespaces) . '>\n';
	$output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
	$output .= "</rss>\n";
	header('Content-Type: application/rss+xml; charset=utf-8');
	return $output;
}

//////////////

function allhorseracing_news_feed() {
	global $base_url, $language;
	$aids = FALSE;
	$channel = array();
	$limit = 20;

	if ($aids === FALSE) {
		$articles = ahr_get_news_articles($limit, 1);
//    $aids = $articles;
	}

	$item_length = 'teaser';
	$namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

	$items = '';
	foreach ($articles as $aid) {
		// Load the specified node:
		$item = $aid;
		// $item->build_mode = NODE_BUILD_RSS;
		$absolute = TRUE;
		$query = NULL;
		$fragment = NULL;
		if (strlen(trim($item['pageuri'])) == 0) {
			$item_link = 'http://www.usracing.com/news/' . $item['ContentId'] . '?title=' . str_replace('%26%2339%3Bs', '', str_replace('%20', '-', rawurlencode(strtolower($item['Title']))));
		} else {
			$item_link = 'http://www.usracing.com/news/' . trim($item['pageuri']);
		}
		$extra = array('pubDate' => date('r', strtotime($item['timecreate'])));
		$item_title = $item['Title'];
		// Prepare the item description
		switch ($item_length) {
			/* case 'fulltext':
			  $item_text = $item->body;
			  break; */
			case 'teaser':
				$item_text = $item['Teaser'];
				//  if (!empty($item->readmore)) {
				//  $item_text .= '<p><a href="news/'.$item['ContentId'].'" target="_blank">read more</a></p>';
				//  }
				break;
			/* case 'title':
			  $item_text = '';
			  break; */
		}

		$items .= format_rss_item($item_title, $item_link, $item_text, $extra);
	}

	$channel_defaults = array(
		'version' => '2.0',
		'title' => 'Us Racing',
		'link' => 'http://www.usracing.com',
		'description' => '',
		'language' => 'en'
	);
	$channel = array_merge($channel_defaults, $channel);

	$output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	$output .= '<rss version="' . $channel["version"] . '" xml:base="' . $base_url . '" ' . drupal_attributes($namespaces) . '>\n';
	$output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
	$output .= "</rss>\n";
	header('Content-Type: application/rss+xml; charset=utf-8');
	return $output;
}

function date_t_strings(&$replace, $langcode) {
	$replace[$langcode]['day_name'] = explode('|', trim(t('!day-name Sunday|Monday|Tuesday|Wednesday|Thursday|Friday|Saturday', array('!day-name' => ''), $langcode)));
	$replace[$langcode]['day_abbr'] = explode('|', trim(t('!day-abbreviation Sun|Mon|Tue|Wed|Thu|Fri|Sat', array('!day-abbreviation' => ''), $langcode)));
	$replace[$langcode]['day_abbr1'] = explode('|', trim(t('!day-abbreviation S|M|T|W|T|F|S', array('!day-abbreviation' => ''), $langcode)));
	$replace[$langcode]['day_abbr2'] = explode('|', trim(t('!day-abbreviation SU|MO|TU|WE|TH|FR|SA', array('!day-abbreviation' => ''), $langcode)));
	$replace[$langcode]['ampm'] = explode('|', trim(t('!ampm-abbreviation am|pm|AM|PM', array('!ampm-abbreviation' => ''), $langcode)));
	$replace[$langcode]['datetime'] = explode('|', trim(t('!datetime Year|Month|Day|Week|Hour|Minute|Second|All Day|All day', array('!datetime' => ''), $langcode)));
	$replace[$langcode]['datetime_plural'] = explode('|', trim(t('!datetime_plural Years|Months|Days|Weeks|Hours|Minutes|Seconds', array('!datetime_plural' => ''), $langcode)));
	$replace[$langcode]['date_order'] = explode('|', trim(t('!date_order Every|First|Second|Third|Fourth|Fifth', array('!date_order' => ''), $langcode)));
	$replace[$langcode]['date_order_reverse'] = explode('|', trim(t('!date_order |Last|Next to last|Third from last|Fourth from last|Fifth from last', array('!date_order' => ''), $langcode)));
	$replace[$langcode]['date_nav'] = explode('|', trim(t('!date_nav Prev|Next|Today', array('!date_nav' => ''), $langcode)));

	// These start with a pipe so the January value will be in position 1 instead of position 0.
	$replace[$langcode]['month_name'] = explode('|', trim(t('!month-name |January|February|March|April|May|June|July|August|September|October|November|December', array('!month-name' => ''), $langcode)));
	$replace[$langcode]['month_abbr'] = explode('|', trim(t('!month-abbreviation |Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec', array('!month-abbreviation' => ''), $langcode)));
}

function date_week_days_untranslated($refresh = TRUE) {
	static $weekdays;
	if ($refresh || empty($weekdays)) {
		$weekdays = array(0 => 'Sunday', 1 => 'Monday', 2 => 'Tuesday',
			3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday',
			6 => 'Saturday');
	}
	return $weekdays;
}

function date_month_names_untranslated() {
	static $month_names;
	if (empty($month_names)) {
		$month_names = array(1 => 'January', 2 => 'February', 3 => 'March',
			4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July',
			8 => 'August', 9 => 'September', 10 => 'October',
			11 => 'November', 12 => 'December');
	}
	return $month_names;
}

function date_t($string, $context, $langcode = NULL) {
	//static $replace = array();

	if (empty($replace[$langcode])) {
		// The function to create the date string arrays is kept separate
		// so those arrays can be directly accessed by other functions.
		date_t_strings($replace, $langcode);
	}
	switch ($context) {
		case 'day_name':
		case 'day_abbr':
		case 'day_abbr1':
		case 'day_abbr2':
			$untranslated = array_flip(date_week_days_untranslated());
			break;
		case 'month_name':
		case 'month_abbr':
			$untranslated = array_flip(date_month_names_untranslated());
			break;
		case 'ampm':
			$untranslated = array_flip(array('am', 'pm', 'AM', 'PM'));
			break;
		case 'datetime':
			$untranslated = array_flip(array('Year', 'Month', 'Day', 'Week', 'Hour', 'Minute', 'Second', 'All Day', 'All day'));
			break;
		case 'datetime_plural':
			$untranslated = array_flip(array('Years', 'Months', 'Days', 'Weeks', 'Hours', 'Minutes', 'Seconds'));
			break;
		case 'date_order':
			$untranslated = array_flip(array('Every', 'First', 'Second', 'Third', 'Fourth', 'Fifth'));
			break;
		case 'date_order_reverse':
			$untranslated = array_flip(array('', 'Last', 'Next to last', 'Third from last', 'Fourth from last', 'Fifth from last'));
			break;
		case 'date_nav':
			$untranslated = array_flip(array('Prev', 'Next', 'Today'));
			break;
	}
	$pos = $untranslated[$string];
	return $replace[$langcode][$context][$pos];
}

function format_date($timestamp, $type = 'medium', $format = '', $timezone = NULL) {
	if (!isset($timezone)) {
		global $user;
		if (variable_get('configurable_timezones', 1) && $user->uid && strlen($user->timezone)) {
			$timezone = $user->timezone;
		} else {
			$timezone = variable_get('date_default_timezone', 0);
		}
	}

	$timestamp += $timezone;

	switch ($type) {
		case 'small':
			$format = variable_get('date_format_short', 'm/d/Y - H:i');
			break;
		case 'large':
			$format = variable_get('date_format_long', 'l, F j, Y - H:i');
			break;
		case 'custom':
			// No change to format
			break;
		case 'medium':
		default:
			$format = variable_get('date_format_medium', 'D, m/d/Y - H:i');
	}

	$max = strlen($format);
	$date = '';
	for ($i = 0; $i < $max; $i++) {
		$c = $format[$i];
		if (strpos('AaDFlM', $c) !== FALSE) {
			$date .= t(gmdate($c, $timestamp));
		} else if (strpos('BdgGhHiIjLmnsStTUwWYyz', $c) !== FALSE) {
			$date .= gmdate($c, $timestamp);
		} else if ($c == 'r') {
			$date .= format_date($timestamp - $timezone, 'custom', 'D, d M Y H:i:s O', $timezone);
		} else if ($c == 'O') {
			$date .= sprintf('%s%02d%02d', ($timezone < 0 ? '-' : '+'), abs($timezone / 3600), abs($timezone % 3600) / 60);
		} else if ($c == 'Z') {
			$date .= $timezone;
		} else if ($c == '\\') {
			$date .= $format[++$i];
		} else {
			$date .= $c;
		}
	}

	return $date;
}

function date_format_date($date, $type = 'medium', $format = '', $langcode = NULL) {
	if (empty($date)) {
		return '';
	}
	switch ($type) {
		case 'small':
		case 'short':
			$format = variable_get('date_format_short', 'm/d/Y - H:i');
			break;
		case 'large':
		case 'long':
			$format = variable_get('date_format_long', 'l, F j, Y - H:i');
			break;
		case 'custom':
			$format = $format;
			break;
		case 'medium':
		default:
			$format = variable_get('date_format_medium', 'D, m/d/Y - H:i');
	}
	$max = strlen($format);
	$datestring = '';
	for ($i = 0; $i < $max; $i++) {
		$c = $format[$i];
		switch ($c) {
			// Use date_t() for ambiguous short strings that need translation.
			// We send long day and month names to date_t(), along with context.
			case 'l':
				$datestring .= date_t(date_format($date, 'l'), 'day_name', $langcode);
				break;
			case 'D':
				$datestring .= date_t(date_format($date, 'l'), 'day_abbr', $langcode);
				break;
			case 'F':
				$datestring .= date_t(date_format($date, 'F'), 'month_name', $langcode);
				break;
			case 'M':
				$datestring .= date_t(date_format($date, 'F'), 'month_abbr', $langcode);
				break;
			case 'A':
			case 'a':
				$datestring .= date_t(date_format($date, $c), 'ampm', $langcode);
				break;
			// The timezone name translations can use t().  
			case 'e':
			case 'T':
				$datestring .= t(date_format($date, $c));
				break;
			// Remaining date parts need no translation.
			case 'O':
				$datestring .= sprintf('%s%02d%02d', (date_offset_get($date) < 0 ? '-' : '+'), abs(date_offset_get($date) / 3600), abs(date_offset_get($date) % 3600) / 60);
				break;
			case 'P':
				$datestring .= sprintf('%s%02d:%02d', (date_offset_get($date) < 0 ? '-' : '+'), abs(date_offset_get($date) / 3600), abs(date_offset_get($date) % 3600) / 60);
				break;
			case 'Z':
				$datestring .= date_offset_get($date);
				break;
			case '\\':
				$datestring .= $format[++$i];
				break;
			default:
				if (strpos('BdgGhHiIjLmnsStTUwWYyz', $c) !== FALSE) {
					$datestring .= date_format($date, $c);
				} else if ($c == 'r') {
					$datestring .= format_date($date, 'custom', 'D, d M Y H:i:s O', $langcode);
				} else {
					$datestring .= $c;
				}
		}
	}
	return $datestring;
}

function ahr_news_article_load($aid, $nocache = FALSE) {

	if (!$article) {
		try {
			$client = ahr_dailyracingnews_connect();

			// Adding on the full article text
			$article_result = $client->call("GetArticle", array('ArticleId' => $aid));
			if (empty($article_result)) {
				return FALSE;
			}

			$article = $article_result['GetArticleResult'];

			$created = date_create($article['DateCreated']);

			// Mapping initial article fields
			$item = array(
				'aid' => $article['ContentId'],
				'title' => $article['Title'],
				'created' => (int) date_format($created, 'U'),
			);

			$xmlstr = $article['Html'];
			$xml = simplexml_load_string($xmlstr);
			$format = variable_get('filter_default_format', 1);
			$item['body'] = $xml->Text->asXML();
			$item['teaser'] = _ahr_article_teaser($item['body'], $format);

			$article = (object) $item;

			if (!$nocache) {
				//cache_set($cid, $article, 'cache', $expires_ts);
				// watchdog('AHR', 'Caching news article !aid', array('!aid' => $aid), WATCHDOG_INFO);
			}

			return $article;
		} catch (Exception $err) {
			//watchdog('AHR', 'Error updating racing news article cache: !error', array('!error' => $err->getMessage()), WATCHDOG_ERROR);
		}

		return $article;
	} else {
		return $article->data;
	}
}

function _ahr_article_teaser($body, $format = NULL, $size = NULL) {

	if (!isset($size)) {
		$size = variable_get('teaser_length', 600);
	}

	// Find where the delimiter is in the body
	$delimiter = strpos($body, '<!--break-->');

	// If the size is zero, and there is no delimiter, the entire body is the teaser.
	if ($size == 0 && $delimiter === FALSE) {
		return $body;
	}

	// If a valid delimiter has been specified, use it to chop off the teaser.
	if ($delimiter !== FALSE) {
		return substr($body, 0, $delimiter);
	}

	// We check for the presence of the PHP evaluator filter in the current
	// format. If the body contains PHP code, we do not split it up to prevent
	// parse errors.
	if (isset($format)) {
		$filters = $format;
		if (isset($filters['php/0']) && strpos($body, '<?') !== FALSE) {
			return $body;
		}
	}

	// If we have a short body, the entire body is the teaser.
	if (drupal_strlen($body) <= $size) {
		return $body;
	}
	// If the delimiter has not been specified, try to split at paragraph or
	// sentence boundaries.
	// The teaser may not be longer than maximum length specified. Initial slice.
	$teaser = truncate_utf8($body, $size);

	// Store the actual length of the UTF8 string -- which might not be the same
	// as $size.
	$max_rpos = strlen($teaser);

	// How much to cut off the end of the teaser so that it doesn't end in the
	// middle of a paragraph, sentence, or word.
	// Initialize it to maximum in order to find the minimum.
	$min_rpos = $max_rpos;

	// Store the reverse of the teaser.  We use strpos on the reversed needle and
	// haystack for speed and convenience.
	$reversed = strrev($teaser);

	// Build an array of arrays of break points grouped by preference.
	$break_points = array();

	// A paragraph near the end of sliced teaser is most preferable.
	$break_points[] = array('</p>' => 0);

	// If no complete paragraph then treat line breaks as paragraphs.
	$line_breaks = array('<br />' => 6, '<br>' => 4);
	// Newline only indicates a line break if line break converter
	// filter is present.
	if (isset($filters['filter/1'])) {
		$line_breaks["\n"] = 1;
	}
	$break_points[] = $line_breaks;

	// If the first paragraph is too long, split at the end of a sentence.
	//$break_points[] = array('. ' => 1, '! ' => 1, '? ' => 1, '?' => 0, '? ' => 1);
	// Iterate over the groups of break points until a break point is found.
	foreach ($break_points as $points) {
		// Look for each break point, starting at the end of the teaser.
		foreach ($points as $point => $offset) {
			// The teaser is already reversed, but the break point isn't.
			$rpos = strpos($reversed, strrev($point));
			if ($rpos !== FALSE) {
				$min_rpos = min($rpos + $offset, $min_rpos);
			}
		}

		// If a break point was found in this group, slice and return the teaser.
		if ($min_rpos !== $max_rpos) {
			// Don't slice with length 0.  Length must be <0 to slice from RHS.
			return ($min_rpos === 0) ? $teaser : substr($teaser, 0, 0 - $min_rpos);
		}
	}

	// If a break point was not found, still return a teaser.
	return $teaser;
}

function drupal_substr($text, $start, $length = NULL) {
	global $multibyte;
	if ($multibyte == UNICODE_MULTIBYTE) {
		return $length === NULL ? mb_substr($text, $start) : mb_substr($text, $start, $length);
	} else {
		$strlen = strlen($text);
		// Find the starting byte offset
		$bytes = 0;
		if ($start > 0) {
			// Count all the continuation bytes from the start until we have found
			// $start characters
			$bytes = -1;
			$chars = -1;
			while ($bytes < $strlen && $chars < $start) {
				$bytes++;
				$c = ord($text[$bytes]);
				if ($c < 0x80 || $c >= 0xC0) {
					$chars++;
				}
			}
		} else if ($start < 0) {
			// Count all the continuation bytes from the end until we have found
			// abs($start) characters
			$start = abs($start);
			$bytes = $strlen;
			$chars = 0;
			while ($bytes > 0 && $chars < $start) {
				$bytes--;
				$c = ord($text[$bytes]);
				if ($c < 0x80 || $c >= 0xC0) {
					$chars++;
				}
			}
		}
		$istart = $bytes;

		// Find the ending byte offset
		if ($length === NULL) {
			$bytes = $strlen - 1;
		} else if ($length > 0) {
			// Count all the continuation bytes from the starting index until we have
			// found $length + 1 characters. Then backtrack one byte.
			$bytes = $istart;
			$chars = 0;
			while ($bytes < $strlen && $chars < $length) {
				$bytes++;
				$c = ord($text[$bytes]);
				if ($c < 0x80 || $c >= 0xC0) {
					$chars++;
				}
			}
			$bytes--;
		} else if ($length < 0) {
			// Count all the continuation bytes from the end until we have found
			// abs($length) characters
			$length = abs($length);
			$bytes = $strlen - 1;
			$chars = 0;
			while ($bytes >= 0 && $chars < $length) {
				$c = ord($text[$bytes]);
				if ($c < 0x80 || $c >= 0xC0) {
					$chars++;
				}
				$bytes--;
			}
		}
		$iend = $bytes;

		return substr($text, $istart, max(0, $iend - $istart + 1));
	}
}

function drupal_strlen($text) {
	global $multibyte;
	if ($multibyte == UNICODE_MULTIBYTE) {
		return mb_strlen($text);
	} else {
		// Do not count UTF-8 continuation bytes.
		return strlen(preg_replace("/[\x80-\xBF]/", '', $text));
	}
}

function truncate_utf8($string, $len, $wordsafe = FALSE, $dots = FALSE) {

	if (drupal_strlen($string) <= $len) {
		return $string;
	}

	if ($dots) {
		$len -= 4;
	}

	if ($wordsafe) {
		$string = drupal_substr($string, 0, $len + 1); // leave one more character
		if ($last_space = strrpos($string, ' ')) { // space exists AND is not on position 0
			$string = substr($string, 0, $last_space);
		} else {
			$string = drupal_substr($string, 0, $len);
		}
	} else {
		$string = drupal_substr($string, 0, $len);
	}

	if ($dots) {
		$string .= ' ...';
	}

	return $string;
}

?>
