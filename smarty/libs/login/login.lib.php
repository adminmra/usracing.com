<?php	 		 	

class Login {
	
	var $error = null;
	var $tpl = null;
	
 	function Login() {
		 $this->tpl =& new Display_Smarty;
	}
	
	function mungeFormData(&$formvars){
		
		// trim off excess whitespace
        $formvars['name'] = trim($formvars['name']);
        $formvars['pass'] = trim($formvars['pass']);
	}
   
     function isValidForm($formvars) {

        // reset error message
        $this->error = null;
        
        // test if "Name" is empty
        if(strlen($formvars['name']) == 0) {
            $this->error = 'name_empty';
            return false; 
        }

        // test if "Comment" is empty
        if(strlen($formvars['pass']) == 0) {
            $this->error = 'pass_empty';
            return false; 
        }
        
        // form passed validation
        return true;
    }

	function authenticateLogin(&$formvars){
		$username = $formvars['name'];
		$userpass = $formvars['pass'];
		
	    $this->tpl->assign('action', 'https://ppe.gamingsystem.net/customerauthn/pl/login');
		$this->tpl->assign('service','https://ctr.gamingsystem.net/acctmgt/pl/postlogin.ctr');
		$this->tpl->assign('lp_casinoid','7630');
		$this->tpl->assign('sp_landingPage', 'http://www.allhorseracing.com/welcome/'. $username);
		$this->tpl->assign('login_fail','http://www.allhorseracing.com/support/loginhelp');
		$this->tpl->assign('username',$username);
		$this->tpl->assign('password',$userpass);
		$this->tpl->display('content/login.tpl');
	
	}
	
}



?>