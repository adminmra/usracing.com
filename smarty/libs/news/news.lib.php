<?php

class News {

	var $error = null;
	var $tpl = null;

	function News() {
		//$this->tpl =& new Display_Smarty; //php4
		$this->tpl = new Display_Smarty; //php5
	}

	function generateRssFile($filename) {
		ob_start();
		echo allhorseracing_news_feed();
		$rss_contents = ob_get_contents();
		ob_end_clean();
		return file_put_contents($filename, $rss_contents);
	}

	function fixedRssFile() {
		$filename = 'rsscontents2.txt';

		require_once(LIBS_DIR . 'dbconnect.inc');
		$query = "select COUNT(*) from newsarchive where typetosee = 'OTH' ";
		$results = mysql_query_w($query);
		$total_count = 0;
		while ($row = mysql_fetch_array($results)) {
			$total_count = $row['COUNT(*)'];
		}
		
		header('Content-Type: application/rss+xml; charset=utf-8');

		//if live then
		if($total_count == 0){
			echo file_get_contents($filename);
			exit;
		}
		
		if (file_exists($filename)) {
			$current_date = time();
			$file_last_mod = filemtime($filename);
			$old = 24 * 3600; // 1 day
			//$old = 20;
			if (($current_date - $file_last_mod) >= $old) {
				//echo "updating rss file";
				if ($this->generateRssFile($filename) === FALSE) {
					//error
				}
				header('Content-Type: application/rss+xml; charset=utf-8');
				echo file_get_contents($filename);
			} else {
				//echo "retrieving from file";
				header('Content-Type: application/rss+xml; charset=utf-8');
				echo file_get_contents($filename);
			}
		} else {
			//echo "creating new rss file";
			if ($this->generateRssFile($filename) === FALSE) {
				//error
			}
			header('Content-Type: application/rss+xml; charset=utf-8');
			echo file_get_contents($filename);
		}
		exit;
	}

	function displayNews(&$formvars, $pathName) {
		if ($pathName == '/news/rss.xml') {
			/* echo allhorseracing_news_feed();
			  exit; */
			$this->fixedRssFile();
		}

		$_GET = $formvars;
		$newpathname = $pathName;
		$pathparts = explode("/", $newpathname);
		if (count($pathparts) > 2) {

			$bodyclass = getBodyClasses(arg(1, '/news'));
			$this->tpl->assign('HorseBetting', 'no');
			//print_r($_SERVER);
			$newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
			$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
			$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
			$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
			$newscontentItem = array();
			if (is_numeric(trim($pathparts[2]))) {
				$this->tpl->assign('title', $bodyclass['title']);
				$this->tpl->assign('link', $bodyclass['link']);
				$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));
				$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
				$this->tpl->assign('topcontent', 'topcontent.tpl');

				$newscontent = template_preprocess_ahr_racing_news_page();
			} else {
				if (strlen(trim($pathparts[2])) > 0) {
					$newscontentItem = _ahr_parse_articles_result_item_main($pathparts[2]);
					//print_r($newscontentItem); exit;
					//$date=explode("-", $newscontentItem["pdate"]);
					$newClassMeta['title'] = '<title>' . stripslashes($newscontentItem["TitleART"]) . '</title>';

					$temp_description = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
					$temp_description = strip_tags($temp_description);
					$temp_description = str_replace('"', '', $temp_description);
					$temp_description = substr($temp_description, 0, 160);
					$newClassMeta['meta'][1] = '<meta name="description" content="' . $temp_description . '" />';
					$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting, ' . strip_tags(stripslashes($newscontentItem["TitleART"])) . '" />';


					/* $newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
					  $newClassMeta['meta'][1] = '<meta name="description" content="'.$newscontentItem["metad"].'" />';
					  $newClassMeta['meta'][2] = '<meta name="keywords" content="'.$newscontentItem["metak"].'" />';
					  $newClassMeta['meta'][3] = '<meta name="robots" content="all" />'; */
					$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));

					$newscontent['Title'] = stripslashes($newscontentItem["TitleART"]);
					$newscontent['id'] = stripslashes($newscontentItem["id"]);
					$newscontent['featured'] = stripslashes($newscontentItem["featured"]);
					//$newscontent['DateCreated']=date('F d, Y', mktime(0,0,0,$date[1],$date[2],$date[0]));
					$newscontent['DateCreated'] = $newscontentItem["DateCreatedART"];
					$newscontent['Html'] = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
					$this->tpl->assign('title', $newClassMeta['title']);
					$this->tpl->assign('link', $bodyclass['link']);
					$this->tpl->assign('metatag', implode("\n", $newClassMeta['meta']));
					$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
					$this->tpl->assign('topcontent', 'topcontent.tpl');
				}
				
			}

			$this->tpl->assign('header', 'header.tpl');
			if (count($newscontentItem) > 0) {
				$this->tpl->assign('article', $newscontent);
			} else {
				$this->tpl->assign('article', $newscontent);
			}
			$this->tpl->assign('pager', $newscontent['pager']);
			$this->tpl->assign('body', 'content/news.tpl');
			$this->tpl->assign('footer', 'footer.tpl');
			$this->tpl->display('display.tpl');
			exit;
		}

		$bodyclass = getBodyClasses(arg(1, $pathName));
		$this->tpl->assign('HorseBetting', 'no');
		$this->tpl->assign('header', 'header.tpl');
		$this->tpl->assign('title', $bodyclass['title']);
		$this->tpl->assign('link', $bodyclass['link']);
		$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));
		$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
		$this->tpl->assign('topcontent', 'topcontent.tpl');
		$newscontent = template_preprocess_ahr_racing_news_page();
		if (isset($newscontent['articlesHTML'])) {
			$this->tpl->assign('articles', $newscontent['articlesHTML']);
		} else {
			$this->tpl->assign('article', $newscontent);
		}
		//$this->tpl->assign('article',"wazp"); // this was causing the W title
		$this->tpl->assign('pager', $newscontent['pager']);
		$this->tpl->assign('body', 'content/news.tpl');
		$this->tpl->assign('footer', 'footer.tpl');
		$this->tpl->display('display.tpl');
	}

	/*
	Leopold added this new function for loading new from xml files
	*/
	function displayNews2(&$formvars, $pathName) {
		if ($pathName == '/news/rss.xml') {
			$this->fixedRssFile();
		}

		$_GET = $formvars;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;

		define("XMLSOURCEPATH", "/home/ah/allhorse/public_html/output/newsxml/");
		define("XMLFILEPREFIX", "news");
		
		$newpathname = $pathName;
		$pathparts = explode("/", $newpathname);
		
		// Checking whether the path has a new entry request or not.
		if (count($pathparts) > 2) {
			// The user is requesting a news entry
			$bodyclass = getBodyClasses(arg(1, '/news'));
			$this->tpl->assign('HorseBetting', 'no');
			$newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
			$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
			$newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
			$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
			$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
			$newscontentItem = array();
			$newscontent = array();
			
			if (is_numeric(trim($pathparts[2]))) {
				// the news entry requested is a number
				$this->tpl->assign('title', $bodyclass['title']);
				$this->tpl->assign('link', $bodyclass['link']);
				$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));
				$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
				$this->tpl->assign('topcontent', 'topcontent.tpl');
				//$newscontent = template_preprocess_ahr_racing_news_page();// fix this
			} else if (strlen(trim($pathparts[2])) > 0) {
				// Checking if the news entry's title is not empty
				$totalPages = count(glob(XMLSOURCEPATH.'*.xml'));
				$article = array();
				
				for($it = 1; $it<=$totalPages; $it++){
					$file = XMLSOURCEPATH . XMLFILEPREFIX . $it . ".xml";
					if (file_exists($file)) {
						$xml = simplexml_load_file($file);
						$object = (array) $xml;
						$items = $object['item'];
						
						foreach ($items as $key => $item) {
							if($item->pageuri != $pathparts[2]){
								unset($item);
								continue;
							}
							else{
								$article["id"] = stripslashes($item->ContentId);
								$article["TitleART"] = stripslashes($item->Title);
								$article["DateCreatedART"] = $item->DateCreated;
								$article["Link"] = '/news/'.stripslashes($item->pageuri);
								$article["HtmlART"] = htmlspecialchars_decode($item->Article);
								$article["featured"] = 0;
								unset($item);
								//$this->renderArticle($article);
								break 2;
							}
						}
						
					}
				}
				
				$newscontentItem = $article;
				$newClassMeta['title'] = '<title>' . @stripslashes($newscontentItem["TitleART"]) . '</title>';

				$temp_description = htmlspecialchars_decode(@stripslashes($newscontentItem["HtmlART"]));
				$temp_description = strip_tags($temp_description);
				$temp_description = str_replace('"', '', $temp_description);
				$temp_description = substr($temp_description, 0, 160);
				$newClassMeta['meta'][1] = '<meta name="description" content="' . $temp_description . '" />';
				$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting, ' . strip_tags(stripslashes($newscontentItem["TitleART"])) . '" />';

				$newscontent['Title'] = @stripslashes($newscontentItem["TitleART"]);
				$newscontent['id'] = @stripslashes($newscontentItem["id"]);
				$newscontent['featured'] = @stripslashes($newscontentItem["featured"]);
				$newscontent['DateCreated'] = $newscontentItem["DateCreatedART"];
				$newscontent['Html'] = htmlspecialchars_decode(@stripslashes($newscontentItem["HtmlART"]));
				$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));
				$this->tpl->assign('title', $newClassMeta['title']);
				$this->tpl->assign('link', $bodyclass['link']);
				$this->tpl->assign('metatag', implode("\n", $newClassMeta['meta']));
				$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
				$this->tpl->assign('topcontent', 'topcontent.tpl');
			}
			
			if (count($newscontentItem) > 0) {
				$this->tpl->assign('article', $newscontent);
			} else {
				$this->tpl->assign('article', $newscontent);
			}
			
			$this->tpl->assign('header', 'header.tpl');
			$this->tpl->assign('pager', isset($newscontent['pager'])?$newscontent['pager']:'');
			$this->tpl->assign('body', 'content/news.tpl');
			$this->tpl->assign('footer', 'footer.tpl');
			$this->tpl->display('display.tpl');
			exit;
		}
		else{
			// there is not a news entry request, load  news list page
			
			$totalPages = count(glob(XMLSOURCEPATH.'*.xml'));
			$file = XMLSOURCEPATH . XMLFILEPREFIX . $page . ".xml";
			$output = '';
			
			if (file_exists($file)) {
				$xml = simplexml_load_file($file);
				$object = (array) $xml;
				$items = $object['item'];
				
				foreach ($items as $key => $item) {
					$class = 'story';
					if (strlen(trim($item->pageuri)) == 0) {
						$URLNEW = '/news/' . $item->ContendId . '?title=' . str_replace('%26%2339%3Bs', '', str_replace('%20', '-', rawurlencode(strtolower($item->Title))));
					} else {
						$URLNEW = '/news/' . $item->pageuri;
					}

					$output .= '<dl class="dl-horizontal ' . $class . '" id="news-story-' . $item->ContendId . '">';
					$output .= '  <dd>';
					$output .= '    <h3 class="news-title"><a href="' . $URLNEW . '">' . stripslashes($item->Title) . '</a></h3>';
					$output .= '    <div class="teaser">' . $this->ahr_abbr_html(stripslashes($item->Teaser), 350) . '</div>';
					$output .= '    <div class="news-date">' . $item->DateCreated . '</div>';
					$output .= '  </dd>';
					$output .= '</dl>';
					//$output .= htmlspecialchars_decode($item->Article);
				}
			}
			$bodyclass = getBodyClasses(arg(1, $pathName));
			$this->tpl->assign('HorseBetting', 'no');
			$this->tpl->assign('header', 'header.tpl');
			$this->tpl->assign('title', $bodyclass['title']);
			$this->tpl->assign('link', $bodyclass['link']);
			$this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));
			$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
			$this->tpl->assign('topcontent', 'topcontent.tpl');
			$this->tpl->assign('articles', $output);
			$this->tpl->assign('pager', $this->pager($totalPages, $page));
			$this->tpl->assign('body', 'content/news.tpl');
			$this->tpl->assign('footer', 'footer.tpl');
			$this->tpl->display('display.tpl');
		}
	}
	

	function ahr_abbr_html($Html, $Len) {
		if (strlen($Html) > $Len) {
			return substr($Html, 0, $Len) . '...';
		} else {
			return $Html;
		}
	}
	
	function pager($totalpages, $current, $displayQty=9){		
		if($displayQty>$totalpages){
			$displayQty = $totalpages;
			$first = 1;
			$last = $displayQty;
		}
		else{
			$media = (integer)($displayQty/2);
			if($current-$media<=0){
				$first = 1;
				$last = $displayQty;
			}
			else if($current+$media>=$totalpages){
				$last = $totalpages;
				$first = $last - $displayQty;
			}
			else{
				$first = $current-$media;
				$last = $current+$media;
			}
		}
		
		$pager = '<div class="item-list"><ul class="pager">';
		if($current>1){
			$pager .= '<li class="pager-first first"><a href="/news">&lt;&lt; <span>First</span></a></li>';
			$pàger .= sprintf('<li class="pager-previous"><a href="/news?page=%d">&lt; <span>Prev</span></a></li>', $current-1);
		}
		
		if($first>1){
			$pager .= '<li class="pager-ellipsis">...</li>';
		}
		
		for($i=$first; $i<=$last; $i++){
			if($i==$current)
				$pager .= sprintf('<li class="pager-current"><span>%d</span></li>', $i);
			else
				$pager .= sprintf('<li class="pager-item"><a href="/news?page=%d" title="Go to page %d">%d</a></li>', $i, $i, $i);
		}
		
		if($last<$totalpages){
			$pager .= '<li class="pager-ellipsis">...</li>';
		}
		
		if($current<$totalpages){
			$pager .= sprintf('<li class="pager-next"><a href="/news?page=%d"><span>Next</span> &gt;</a></li>', $current+1);
			$pager .= sprintf('<li class="pager-last last"><a href="/news?page=%d"><span>Last</span> &gt;&gt;</a></li>', $totalpages);
		}
				
		$pager .= '</ul></div>';
		return $pager;
		
	}
	
	function renderArticle($article){
	  $bodyclass = getBodyClasses(arg(1, '/news'));
	  $this->tpl->assign('HorseBetting', 'no');
	  $newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
	  $newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	  $newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
	  $newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
	  $newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
	  $newscontentItem = array();
	  if (strlen(trim($pathparts[2])) > 0) {
		  $newscontentItem = _ahr_parse_articles_result_item_main($pathparts[2]);
		  //print_r($newscontentItem); exit;
		  //$date=explode("-", $newscontentItem["pdate"]);
		  $newClassMeta['title'] = '<title>' . stripslashes($newscontentItem["TitleART"]) . '</title>';

		  $temp_description = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
		  $temp_description = strip_tags($temp_description);
		  $temp_description = str_replace('"', '', $temp_description);
		  $temp_description = substr($temp_description, 0, 160);
		  $newClassMeta['meta'][1] = '<meta name="description" content="' . $temp_description . '" />';
		  $newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting, ' . strip_tags(stripslashes($newscontentItem["TitleART"])) . '" />';

		  $this->tpl->assign('metatag', implode("\n", $bodyclass['meta']));

		  $newscontent['Title'] = stripslashes($newscontentItem["TitleART"]);
		  $newscontent['id'] = stripslashes($newscontentItem["id"]);
		  $newscontent['featured'] = stripslashes($newscontentItem["featured"]);
		  //$newscontent['DateCreated']=date('F d, Y', mktime(0,0,0,$date[1],$date[2],$date[0]));
		  $newscontent['DateCreated'] = $newscontentItem["DateCreatedART"];
		  $newscontent['Html'] = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
		  $this->tpl->assign('title', $newClassMeta['title']);
		  $this->tpl->assign('link', $bodyclass['link']);
		  $this->tpl->assign('metatag', implode("\n", $newClassMeta['meta']));
		  $this->tpl->assign('bodyclass', trim($bodyclass['frontVar'] . ' ' . $bodyclass['sidebars'] . ' ' . $bodyclass['classes']));
		  $this->tpl->assign('topcontent', 'topcontent.tpl');
	  }

	  $this->tpl->assign('header', 'header.tpl');
	  if (count($newscontentItem) > 0) {
		  $this->tpl->assign('article', $newscontent);
	  } else {
		  $this->tpl->assign('article', $newscontent);
	  }
	  $this->tpl->assign('pager', '');
	  $this->tpl->assign('body', 'content/news.tpl');
	  $this->tpl->assign('footer', 'footer.tpl');
	  $this->tpl->display('display.tpl');
	}

	function renderAfterArticle(){
	  $bodyclass = getBodyClasses(arg(1, '/news'));
	  $this->tpl->assign('HorseBetting', 'no');	  
	  $newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
	  $newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
	  $newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
	  $newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
	  $newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
	  $newscontentItem = array();
	  $this->tpl->assign('article', $newscontent);
	  $this->tpl->assign('pager', '');
	  $this->tpl->assign('body', 'content/news.tpl');
	  $this->tpl->assign('footer', 'footer.tpl');
	  $this->tpl->display('display.tpl');
	}
}
?>
