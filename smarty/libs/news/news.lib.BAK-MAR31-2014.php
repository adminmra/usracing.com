<?php	 		 	

class News {
	
	var $error = null;
	var $tpl = null;
	
 	function News() {
		 //$this->tpl =& new Display_Smarty; //php4
		 $this->tpl = new Display_Smarty; //php5
	}
	
	function generateRssFile($filename){
	  ob_start();
	  echo allhorseracing_news_feed();
	  $rss_contents = ob_get_contents();
	  ob_end_clean();
	  return file_put_contents($filename, $rss_contents);
	}
	
	function fixedRssFile(){
	  $filename = 'rsscontents.txt';
	  if (file_exists($filename)) {
	    $current_date = time();
	    $file_last_mod = filemtime($filename);
	    $old = 24 * 3600; // 1 day
	    //$old = 20;
	    if(($current_date-$file_last_mod) >= $old){
	      //echo "updating rss file";
	      if($this->generateRssFile($filename) === FALSE){
		//error
	      }
	      echo file_get_contents($filename);
	    }
	    else{
	      //echo "retrieving from file";
	      header('Content-Type: application/rss+xml; charset=utf-8');
	      echo file_get_contents($filename);
	    }
	  }
	  else{
	    //echo "creating new rss file";
	    if($this->generateRssFile($filename) === FALSE){
	      //error
	    }
	    echo file_get_contents($filename);
	  }
	  exit;
	}
	
	function displayNews(&$formvars,$pathName){
		if($pathName =='/news/rss.xml'){
		 /*echo allhorseracing_news_feed();
		 exit;*/
		 $this->fixedRssFile();
		}

		$_GET = $formvars;
		$newpathname=$pathName;
		$pathparts=explode("/",$newpathname);
		if(count($pathparts) > 2)
			{
						
						$bodyclass = getBodyClasses(arg(1,'/news'));
						$this->tpl->assign('HorseBetting','no');
						//print_r($_SERVER);
						$newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
						$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            			$newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
            			$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
						$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
						$newscontentItem=array();
						if(is_numeric(trim($pathparts[2])))
						{
							$this->tpl->assign('title',$bodyclass['title']);
							$this->tpl->assign('link',$bodyclass['link']);
							$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));
							$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
							$this->tpl->assign('topcontent','topcontent.tpl');

							$newscontent = template_preprocess_ahr_racing_news_page();
						}
						else
						{
							if(strlen(trim($pathparts[2])) > 0)
							{
								$newscontentItem=_ahr_parse_articles_result_item_main($pathparts[2]);
								//print_r($newscontentItem); exit;
							//$date=explode("-", $newscontentItem["pdate"]);
							$newClassMeta['title'] = '<title>'.stripslashes($newscontentItem["TitleART"]).'</title>';
							
							$temp_description = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
							$temp_description = strip_tags($temp_description);
							$temp_description = str_replace('"', '', $temp_description);
							$temp_description = substr($temp_description, 0, 160);
							$newClassMeta['meta'][1] = '<meta name="description" content="'.$temp_description.'" />';
							$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting, '.strip_tags(stripslashes($newscontentItem["TitleART"])).'" />';
							
							
							/*$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
							$newClassMeta['meta'][1] = '<meta name="description" content="'.$newscontentItem["metad"].'" />';
							$newClassMeta['meta'][2] = '<meta name="keywords" content="'.$newscontentItem["metak"].'" />';
							$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';*/
								$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));

							$newscontent['Title']=stripslashes($newscontentItem["TitleART"]);
							$newscontent['id']=stripslashes($newscontentItem["id"]);
							$newscontent['featured']=stripslashes($newscontentItem["featured"]);
							//$newscontent['DateCreated']=date('F d, Y', mktime(0,0,0,$date[1],$date[2],$date[0]));
							$newscontent['DateCreated']=$newscontentItem["DateCreatedART"];
							$newscontent['Html']=htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
						$this->tpl->assign('title',$newClassMeta['title']);
						$this->tpl->assign('link',$bodyclass['link']);
						$this->tpl->assign('metatag',implode("\n",$newClassMeta['meta']));
						$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
						$this->tpl->assign('topcontent','topcontent.tpl');

							}
						}

						$this->tpl->assign('header','header.tpl');
						if(count($newscontentItem) > 0)
							{
								$this->tpl->assign('article',$newscontent);
							}
							else
							{
								$this->tpl->assign('article',$newscontent);
							}
						$this->tpl->assign('pager',$newscontent['pager']);
						$this->tpl->assign('body','content/news.tpl');
						$this->tpl->assign('footer','footer.tpl');
						$this->tpl->display('display.tpl');
						exit;

			}

		$bodyclass = getBodyClasses(arg(1,$pathName));
		$this->tpl->assign('HorseBetting','no');
		$this->tpl->assign('header','header.tpl');
		$this->tpl->assign('title',$bodyclass['title']);
		$this->tpl->assign('link',$bodyclass['link']);
		$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));
		$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
		$this->tpl->assign('topcontent','topcontent.tpl');
		$newscontent = template_preprocess_ahr_racing_news_page();
		if(isset($newscontent['articlesHTML'])){
		$this->tpl->assign('articles',$newscontent['articlesHTML']);
		}else{
		$this->tpl->assign('article',$newscontent);
		}
		//$this->tpl->assign('article',"wazp"); // this was causing the W title
		$this->tpl->assign('pager',$newscontent['pager']);
		$this->tpl->assign('body','content/news.tpl');
		$this->tpl->assign('footer','footer.tpl');
		$this->tpl->display('display.tpl');
	
	}
	
	
	function displayNews2(&$formvars,$pathName){
		if($pathName =='/news/rss.xml'){
		 /*echo allhorseracing_news_feed();
		 exit;*/
		 $this->fixedRssFile();
		}

		$_GET = $formvars;
		$page = (isset($_GET['page'])) ? $_GET['page'] : 1;
						
						$bodyclass = getBodyClasses(arg(1,'/news'));
						$this->tpl->assign('HorseBetting','no');
						//print_r($_SERVER);
						$newClassMeta['title'] = '<title>Horse Racing Betting News | All Horse Racing</title>';
						$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            			$newClassMeta['meta'][1] = '<meta name="description" content="Horse Racing Betting News. Triple Crown through the Breeders Cup. Horse Racing news for OTB horse betting year round." />';
            			$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting" />';
						$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';
						$newscontentItem=array();
						if(is_numeric(trim($pathparts[2])))
						{
							$this->tpl->assign('title',$bodyclass['title']);
							$this->tpl->assign('link',$bodyclass['link']);
							$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));
							$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
							$this->tpl->assign('topcontent','topcontent.tpl');

							$newscontent = template_preprocess_ahr_racing_news_page();
						}
						else
						{
							if(strlen(trim($pathparts[2])) > 0)
							{
								$newscontentItem=_ahr_parse_articles_result_item_main($pathparts[2]);
								//print_r($newscontentItem); exit;
							//$date=explode("-", $newscontentItem["pdate"]);
							$newClassMeta['title'] = '<title>'.stripslashes($newscontentItem["TitleART"]).'</title>';
							
							$temp_description = htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
							$temp_description = strip_tags($temp_description);
							$temp_description = str_replace('"', '', $temp_description);
							$temp_description = substr($temp_description, 0, 160);
							$newClassMeta['meta'][1] = '<meta name="description" content="'.$temp_description.'" />';
							$newClassMeta['meta'][2] = '<meta name="keywords" content="horse racing, bet on horses, kentucky derby betting, breeders cup, belmont stakes betting, horse race, offtrack betting, otb, preakness stakes betting, '.strip_tags(stripslashes($newscontentItem["TitleART"])).'" />';
							
							
							/*$newClassMeta['meta'][0] = '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
							$newClassMeta['meta'][1] = '<meta name="description" content="'.$newscontentItem["metad"].'" />';
							$newClassMeta['meta'][2] = '<meta name="keywords" content="'.$newscontentItem["metak"].'" />';
							$newClassMeta['meta'][3] = '<meta name="robots" content="all" />';*/
								$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));

							$newscontent['Title']=stripslashes($newscontentItem["TitleART"]);
							$newscontent['id']=stripslashes($newscontentItem["id"]);
							$newscontent['featured']=stripslashes($newscontentItem["featured"]);
							//$newscontent['DateCreated']=date('F d, Y', mktime(0,0,0,$date[1],$date[2],$date[0]));
							$newscontent['DateCreated']=$newscontentItem["DateCreatedART"];
							$newscontent['Html']=htmlspecialchars_decode(stripslashes($newscontentItem["HtmlART"]));
						$this->tpl->assign('title',$newClassMeta['title']);
						$this->tpl->assign('link',$bodyclass['link']);
						$this->tpl->assign('metatag',implode("\n",$newClassMeta['meta']));
						$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
						$this->tpl->assign('topcontent','topcontent.tpl');

							}
						}

						$this->tpl->assign('header','header.tpl');
						if(count($newscontentItem) > 0)
							{
								$this->tpl->assign('article',$newscontent);
							}
							else
							{
								$this->tpl->assign('article',$newscontent);
							}
						$this->tpl->assign('pager',$newscontent['pager']);
						$this->tpl->assign('body','content/news.tpl');
						$this->tpl->assign('footer','footer.tpl');
						$this->tpl->display('display.tpl');
						exit;

			}

		$bodyclass = getBodyClasses(arg(1,$pathName));
		$this->tpl->assign('HorseBetting','no');
		$this->tpl->assign('header','header.tpl');
		$this->tpl->assign('title',$bodyclass['title']);
		$this->tpl->assign('link',$bodyclass['link']);
		$this->tpl->assign('metatag',implode("\n",$bodyclass['meta']));
		$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
		$this->tpl->assign('topcontent','topcontent.tpl');
		$newscontent = this->readnewsxml();
		if(isset($newscontent['articlesHTML'])){
		$this->tpl->assign('articles',$newscontent['articlesHTML']);
		}else{
		$this->tpl->assign('article',$newscontent);
		}
		//$this->tpl->assign('article',"wazp"); // this was causing the W title
		$this->tpl->assign('pager',$newscontent['pager']);
		$this->tpl->assign('body','content/news.tpl');
		$this->tpl->assign('footer','footer.tpl');
		$this->tpl->display('display.tpl');
	
	}
}

?>