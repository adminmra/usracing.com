<?php	 		 	

class Casino {
	
	var $error = null;
	var $tpl = null;
	
 	function Casino() {
		 $this->tpl =& new Display_Smarty;
	}
	

	function displayCasino($pathName){
			$bodyclass = getBodyClasses($pathName);
			$this->tpl->assign('header','header.tpl');
			$this->tpl->assign('title',$bodyclass['title']);
			$this->tpl->assign('link',$bodyclass['link']);
			$this->tpl->assign('metatag',$bodyclass['meta'][0]);
			$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
			$this->tpl->assign('topcontent','topcontent.tpl');
			$this->tpl->assign('footer','footer.tpl');
			$this->tpl->assign('body','content/'.$pathName.'.tpl');
			$this->tpl->display('display.tpl');
	   		
	
	}
	
	function displayCasinoSlideFlow(){
			$content = file_get_contents(TEMPLATE_DIR.'libs/casino/slideflow.lib.php');
			$this->tpl->assign('content',$content);
			$this->tpl->display('display-plain.tpl');
	}
	
	function displayCasinoPlay(&$formvars,&$requestvars){
			$_POST = $formvars;
			$_REQUEST = $requestvars;
			include(TEMPLATE_DIR.'libs/casino/play.lib.php');
			
	}
	function displayCasinoBank(){
			header("Location: https://ctr.gamingsystem.net/acctmgt/pl/login.ctr?sp_rp_casinoid=7630&siteID=7630&sp_external_resource=https://teller.gamingsystem.net/teller/depositDisplayOptions.htm&casinoid=7630&sp_siteID=7630&service=https://ctr.gamingsystem.net/acctmgt/pl/myaccount.ctr&sp_casinoid=7630&sp_rp_siteID=7630&sp_ctrPage=");
		exit;
	}
	function displayCasinoNoDemo(){
			$content = file_get_contents(TEMPLATE_DIR.'libs/casino/no-demo-available.lib.php');
			$this->tpl->assign('content',$content);
			$this->tpl->display('display-plain.tpl');
	}

}



?>