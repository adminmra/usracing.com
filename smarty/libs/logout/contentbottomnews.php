<?php	 		 	
$SingleArticle = ahr_get_news_articles(1, 1);
$indiviualNews= ahr_news_article_load($SingleArticle[0]["ContentId"]);

 $BodyPara=explode("</p>",$SingleArticle[0]['Html']);
	
?>
<div class="news">
<dl class="dl-horizontal story">
<dt>
<a data-rel="fancybox-button" class="thumbnail fancybox-button zoomer" href="#"><span class="overlay-zoom"><div class="zoom-icon"></div></span></a>
</dt>

<dd> 
<h3 class="news-title"><?php	 	 echo $SingleArticle[0]["Title"]; ?></h3>
<div class="teaser">
<?php	 	 echo $BodyPara[0]; ?>
<?php	 	 echo $BodyPara[1]; ?> ... &nbsp;&nbsp;<a href="news/<?php	 	 echo $SingleArticle[0]["ContentId"]; ?>?title=<?php	 	 echo str_replace('%26%2339%3Bs', '',str_replace('%20', '-', rawurlencode(strtolower($SingleArticle[0]["Title"])))); ?>">Read Article</a> </div>

<div class="news-date"><?php	 	 print $SingleArticle[0]["DateCreated"]; ?></div>
</dd>

</dl>
</div>