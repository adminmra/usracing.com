<?php	 		 	
//require_once LIBS_DIR.'/dbconnect.inc';
/**
if(user_is_logged_in() && isset($_GET["megamenu"])) {
   header("Location: http://www.allhorseracing.com/sportsbook?defline=NFL");
   exit;
}
**/
?>
<link type="text/css" rel="stylesheet" media="all" href="/modules/allhorseracing/tabs/tools.tabs-general.css" />
<script type="text/javascript" src="/modules/allhorseracing/tabs/tools.tabs-1.0.4.min.js"></script>
<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>

<script type="text/javascript">
if(user_is_logged_in() && gup('megamenu')){
	window.location="http://www.allhorseracing.com/sportsbook?defline=NFL";
}
function Nflschedule(a)
{
	Http.get({
				url: "/Feeds/info/nfl-schedule-current.php?nflweek="+a.value,
				callback: Nflschedulefile,
				cache: Http.Cache.Get
				});
				return false;	
}

function Nflschedulefile(xmlreply)
{
	var request = xmlreply.responseText;
	//alert(request);	
	document.getElementById("gametable").innerHTML=request;
}

</script>
<style>
.not-front #content-box { padding: 0;}
</style>
<p>&nbsp;</p>
<p><strong>Bet on the NFL at US Racing.</strong> It has been an NFL offseason filled with new faces in new places with Pete Carol bolting USC in College Football for the Seattle Seahawks before the NCAA came down on the hard on the program, leaving us with one of the most publicized stories of the offseason.</p>

<p>Big name Free Agents and Players being released were also the talk of the football offseason with WR Brandon Marshall leaving the Denver Broncos to play for the Miami Dolphins after a highly publicized falling out with head coach Josh McDaniels led to his being benched late last season and possibly costing the Broncos a playoff run.</p>
<p>Here is a list of other players making the start this season for a new team:</p>
<br />
<div id="basic-tabs">
		<ul class="tabs"><!-- the tabs -->
		<li class="first"><a style="padding: 4px 26px; width:160px;" href="#">QUARTERBACK</a></li>
		<li class=""><a style="padding: 4px 26px; width:160px;" href="#">RUNNING BACK</a></li>
		<li class=""><a style="padding: 4px 26px; width:160px;" href="#">WIDE RECEIVER</a></li>		
		</ul><!-- end tabs -->
</div>

<div class="panes">

<!-- ======== NEW TAB CONTENTS - COPY CODE FROM HERE ==================== -->

<div class="basic-tabs-content" style="padding:15px; margin:0 0 15px 0;"><!-- Pane -->

<div id="Quarterback" class="RoundBody">
<h1><span class="DarkBlue">Quarterback</span></h1>
<br />
<table title="Quarterback" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;table-layout:fixed;">
<tr>
	<th width="22%">Player</th>
	<th>Position</th>
	<th>Status</th>
	<th>Experience</th>
	<th>2009 Team</th>
	<th>2010 Team</th>
</tr>
<tr>
	<td>Anderson, Derek</td>
	<td>QB</td>
	<td>Released</td>
	<td>5th Season</td>
	<td>CLE</td>
	<td>ARI</td>
</tr>
<tr>
	<td>Bulger, Marc</td>
	<td>QB</td>
	<td>Released</td>
	<td>9th Season</td>
	<td>STL</td>
	<td>BAL</td>
</tr>
<tr>
	<td>Carr, David</td>
	<td>QB</td>
	<td>UFA</td>
	<td>8th Season</td>
	<td>NYG</td>
	<td>SF</td>
</tr>
<tr>
	<td>Delhomme, Jake</td>
	<td>QB</td>
	<td>Released</td>
	<td>11th Season</td>
	<td>CAR</td>
	<td>CLE</td>
</tr>
<tr>
	<td>Russell, JaMarcus</td>
	<td>QB</td>
	<td>Released</td>
	<td>3rd Season</td>
	<td>OAK</td>
	<td>TBD</td>
</tr>
<tr>
	<td>Whitehurst, Charlie</td>
	<td>QB</td>
	<td>RFA</td>
	<td>4th Season</td>
	<td>SD</td>
	<td>SEA</td>
</tr>
</table>
					</div>



</div><!-- End Pane1 -->

<!-- ======== TO HERE ============================================================ -->



<!-- ======== NEW TAB CONTENTS - COPY CODE FROM HERE ============================ -->

<div class="basic-tabs-content" style="padding:15px; margin:0 0 15px 0;"><!-- Pane -->

<div id="Running-Back" class="RoundBody">
<h1><span class="DarkBlue">Running Back</span></h1>
<br />
<table title="Running Back" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;table-layout:fixed; ">
<tr>
	<th width="22%">Player</th>
	<th>Position</th>
	<th>Status</th>
	<th>Experience</th>
	<th>2009 Team</th>
	<th>2010 Team</th>
</tr>
<tr>
	<td>Bell, Mike</td>
	<td>RB</td>
	<td>RFA</td>
	<td>4th Season</td>
	<td>NO</td>
	<td>PHI</td>
</tr>
<tr>
	<td>Johnson, Larry</td>
	<td>RB</td>
	<td>UFA</td>
	<td>7th Season</td>
	<td>CIN</td>
	<td>WAS</td>
</tr>
<tr>
	<td>Jones, Thomas</td>
	<td>RB</td>
	<td>UFA</td>
	<td>10th Season</td>
	<td>NYG</td>
	<td>KC</td>
</tr>
<tr>
	<td>Parker, Willie</td>
	<td>RB</td>
	<td>UFC</td>
	<td>6th Season</td>
	<td>PIT</td>
	<td>WAS</td>
</tr>
<tr>
	<td>Smith, Kolby</td>
	<td>RB</td>
	<td>Released</td>
	<td>3rd Season</td>
	<td>KC</td>
	<td>DEN</td>
</tr>
<tr>
	<td>Taylor, Chester</td>
	<td>RB</td>
	<td>UFA</td>
	<td>8th Season</td>
	<td>MIN</td>
	<td>CHI</td>
</tr>
<tr>
	<td>Tomlinson, LaDainian</td>
	<td>RB</td>
	<td>Released</td>
	<td>9th Season</td>
	<td>SD</td>
	<td>NYJ</td>
</tr>
</table>

</div>
		
</div><!-- End Pane4 -->

<!-- ======== TO HERE ============================================================ -->

<!-- ======== NEW TAB CONTENTS - COPY CODE FROM HERE ============================ -->

<div class="basic-tabs-content" style="padding:15px; margin:0 0 15px 0;"><!-- Pane -->


<div id="Wide-Receiver" class="RoundBody">
<h1><span class="DarkBlue">Wide Receiver</span></h1>
<br />
<table title="Wide Receiver" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;table-layout:fixed;">
<tr>
	<th width="22%">Player</th>
	<th>Position</th>
	<th>Status</th>
	<th>Experience</th>
	<th>2009 Team</th>
	<th>2010 Team</th>
</tr>
<tr>
	<td>Bryant, Antonio</td>
	<td>WR</td>
	<td>UFA</td>
	<td>7th Season</td>
	<td>TB</td>
	<td>CIN</td>
</tr>
<tr>
	<td>Burleson, Nate</td>
	<td>WR</td>
	<td>UFA</td>
	<td>7th Season</td>
	<td>SEA</td>
	<td>DET</td>
</tr>
<tr>
	<td>Galloway, Joey</td>
	<td>WR</td>
	<td>UFA</td>
	<td>15th Season</td>
	<td>PIT</td>
	<td>WAS</td>
</tr>
<tr>
	<td>Holt, Torry</td>
	<td>WR</td>
	<td>Released</td>
	<td>11th Season</td>
	<td>JAC</td>
	<td>NE</td>
</tr>
<tr>
	<td>Marshall, Brandon</td>
	<td>WR</td>
	<td>RFA</td>
	<td>4th Season</td>
	<td>DEN</td>
	<td>DEN</td>
</tr>
<tr>
	<td>Owens, Terrell</td>
	<td>WR</td>
	<td>UFA</td>
	<td>14th Season</td>
	<td>BUF</td>
	<td>TBD</td>
</tr>
<tr>
	<td>Randle El, Antwaan</td>
	<td>WR</td>
	<td>Released</td>
	<td>8th Season</td>
	<td>WAS</td>
	<td>PIT</td>
</tr>
</table>
					</div>


</div><!-- End Pane5 -->		

<!-- ======== TO  HERE ============================ -->



<!-- ======== DO NOT EDIT BELOW HERE ============================================== -->
</div><!-- End Panes -->	

<script type="text/javascript">
<!--//--><![CDATA[//><!--
    $(function() {
      // setup ul.tabs to work as tabs for each div directly under div.panes
      $("#basic-tabs ul.tabs").tabs("div.panes > div");
    });
//--><!]]>
</script>

<p>We look forward to taking care of all your football betting needs this year and be sure to check our promotions page during  the season as we offer you more ways to win in addition to giving you 5% cash back on all sports betting losses along with 5% free cash on all deposits, everyday! Bet on the NFL at US Racing </p>
<br />
<br />
<div style="width: auto;">
<h1 style="float:left;"><span class="DarkBlue">2010 - 2011 Schedule</span></h1>

<span style="float:right;">
<img style="float:left; margin: 2px 7px; 0 0" src="/themes/images/nfl-selectdate.gif" />

<select name="weekselect" onChange="return Nflschedule(this);">
<option value="Hall of Fame Game"   >Hall of Fame Game</option>
<option value="Preseason week 1"    >Preseason week 1</option>
<option value="Preseason week 2"    >Preseason week 2</option>
<option value="Preseason week 3"    >Preseason week 3</option>
<option value="Preseason week 4"   >Preseason week 4</option>
<option value="Week 1"   >Week 1</option>
<option value="Week 2"   >Week 2</option>

<option value="Week 3"   >Week 3</option>
<option value="Week 4"   >Week 4</option>
<option value="Week 5"   >Week 5</option>
<option value="Week 6"   >Week 6</option>
<option value="Week 7"  >Week 7</option>
<option value="Week 8"   >Week 8</option>
<option value="Week 9"   >Week 9</option>
<option value="Week 10"   >Week 10</option>
<option value="Week 11"   >Week 11</option>
<option value="Week 12"   >Week 12</option>
<option value="Week 13"  >Week 13</option>
<option value="Week 14"  >Week 14</option>
<option value="Week 15"  >Week 15</option>
<option value="Week 16"   >Week 16</option>
<option value="Week 17"   >Week 17</option>
<option value="Wild-card"   >Wild-card</option>
<option value="Divisional"   >Divisional</option>
<option value="Conference"   >Conference</option>
<option value="Pro Bowl"   >Pro Bowl</option>
<option value="Super Bowl"  selected >Super Bowl</option>

</select>
</span>
</div>

<div id="gametable" style="clear:both;">
<?php	 	
require_once $_SERVER['DOCUMENT_ROOT']."/Feeds/info/nfl-schedule-current.php";
?>
</div>



