<?php	 		 	
/*require_once LIBS_DIR."dbconnect.inc";
$sql_week=" select * from racepromo where racepromoid ='1' ";
$result_week=mysql_query_w($sql_week);
$data_week=mysql_fetch_object($result_week);
$promo=$data_week->promo;
$updatedate=explode("-",$data_week->lastupdate);
$day=$updatedate[2];
$year=$updatedate[0];
$mon=$updatedate[1];*/
?>

<!-- <a class="logged-out" href="/join"><img style="margin: 30px 0 0 0;" src="/themes/images/bigrace_banner.jpg" align="left" hspace="0" vspace="0" /></a>
<a class="logged-in" href="/racebook"><img style="margin: 30px 0 0 0;" src="/themes/images/bigrace_banner.jpg" align="left" hspace="0" vspace="0" /></a>
 -->
<div style="clear:left;">
<p>&nbsp;</p>
<p><strong>US Racing</strong> is giving you the chance to double your winnings on a horse racing wager every weekend with our hot Race of the Week promotion. Place a ‘To Win’ wager on a horse with 2:1 or greater odds in the highlighted race and we’ll double your winnings if that horse wins.</p>
<!-- <p>Ready to win? Then, visit the <a href="/racebook/" title="Racebook">Racebook</a> on race day to place your bet. </p>
 --></div>

<style>
.not-front #col3 #content-box p+p { padding-top: 6px; }
</style>

<div class="rotw-block-details" style="clear:left;">
<?php	 	
//if(date('w') != 0)
//{
/*	if(date('W') == date('W',mktime(0,0,0,$mon,$day,$year)))
	{
		echo stripslashes($promo); 
	}
	else
	{
		echo "<br /><p><strong>The Race of the Week is posted by Thursday of every week, please check back again for this week's race.</strong></p>"; 
	}
/*}
else
{
	echo "<br /><p><strong>The Race of the Week is posted by Thursday of every week, please check back again for this week's race.</strong></p>"; 
}*/
$MSG = file_get_contents('FeedData/Raceoftheweek.txt');
print $MSG;
?>
</div>

<br><br>

<p><strong>Terms &amp; Conditions</strong></p>
<p>The 100% Win Bonus is on 'Win' bets only placed in the Racebook on the race listed above.</p><br>
<ol>
	<li>To qualify for the 100% Win Bonus, the 'Odds to Win' wager must pay out 2-1 or greater.</li>
		<li>Maximum Win Bonus per single winning wager is $100.00.</li>

	<li>The 100% Win Bonus will be credited within 48 hours.</li>

	<li>Standard rollover terms apply.</li>
	<li><strong>AllHorseRacing.com</strong> reserves the right to cancel or amend this promotion at any time. </li>

	<li><a href="/house-rules" style="color: rgb(78, 80, 75); font-weight: bold; text-decoration: underline;" target="_blank">House Rules</a> apply.</li>

</ol>
<p>&nbsp;</p>

