
<?php	 		 	
	
//initialize nusoap client

require_once(TEMPLATE_DIR.'libs/soap/lib/nusoap.php');


$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
$pageSize = isset($_GET['pagesize']) ? $_GET['pagesize'] : 10;
$curPage = isset($_GET['curpage']) ? $_GET['curpage'] : 1;
$orderBy = isset($_GET['orderby']) ? $_GET['orderby'] : 'datecreated';
$orderDir = isset($_GET['orderdir']) ? $_GET['orderdir'] : 'desc';

$keyword = htmlspecialchars(urldecode($keyword));

$numPagesToShow = 10;
$numPages = 0;
$totalRecords = 0;
$result = array();
//$client	= new nusoap_client('http://www.dailyracingnews.com/webservices/contentfeed.asmx?wsdl', true,'','','','');

//Get list of articles
//$params = array('keyword'=>$keyword,'pageSize'=>$pageSize,'pageNumber'=>$curPage,'orderByColumn'=>$orderBy,'orderByDirection'=>$orderDir);
//$result = $client->call("GetSearchResults",$params);
$limit = ahr_get_news_articles_limit($pageSize,$curPage );
$query = "select ContentIdART as ContentId, TitleART as Title, DateCreatedART as DateCreated,Teaser from newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt where TitleART like '%".$keyword."%' or HtmlART like '%".$keyword."%' order by newsarchive.timecreate desc".$limit;
$results = mysql_query_w($query );
 while($row = mysql_fetch_assoc($results)){
                array_push($result,$row);
    }

//print_r($result);
//Get number of records for search query
//$trParam = array('keyword'=>$keyword);
//$totalRes = $client->call("GetTotalRecordsForSearch",$trParam);
//$totalRecords = $totalRes['GetTotalRecordsForSearchResult'];
$queryTotal = "select COUNT(*)  from newsarticle left join newsarchive on newsarchive.ContentId = newsarticle.ContentIdArt where TitleART like '%".$keyword."%' or HtmlART like '%".$keyword."%'";
 $total_count = mysql_query_w($queryTotal );
 $totalRecords =0;
  while($row = mysql_fetch_assoc($total_count)){
        $totalRecords =  $row['COUNT(*)'];
   }


//cal number of pages
$numPages = floor($totalRecords / $pageSize);
$numPages = (($totalRecords % $pageSize) > 0) ? ($numPages+=1) : $numPages;


if (sizeof($result) > 0)
{
	if (is_array($result))
	{
		$MoreStories = '<div class="page-news">';
		$MoreStories = $MoreStories . '<div class="racing-news">';
		foreach ($result as $art)
		{
			$MoreStories = $MoreStories . '<div class="story">';
			$MoreStories = $MoreStories . '<h3 class="news-title"><a href="/news/' . $art['ContentId'] . '?title=' . $art["Title"] . '" title="' . str_replace("'","",$art["Title"]) . '">' . $art["Title"] . '</a></h3>';	
			$cDate = strtotime($art['DateCreated']);
			$MoreStories = $MoreStories . '<span class="news-date">' . date("F j, Y",$cDate) . "</span><br/>";
			$MoreStories = $MoreStories . '<span class="teaser">' . TRIM($art['Teaser']) . '</span>';
			$MoreStories = $MoreStories . '</div>';
		}
		$MoreStories = $MoreStories . '</div>';
		$MoreStories = $MoreStories . '</div>';
	}
	
}

//function FormatLinkTitle($Value)
//{
	//$Title = str_replace(" ", "_", $Value);
	//$Title = str_replace("&","", $Title);
	//return $Title;
//}
echo $MoreStories;


if ($totalRecords > 0)
{
echo '<div class="item-list">';
echo '<ul class="pager">';
if ($curPage > 1)
{
	
	echo '<li class="pager-first"><a href="/search6?keyword=' . $keyword . '&pagesize='. $pageSize . '&curpage=1&orderby='. $orderBy .'&orderdir=' . $orderDir . '">&lt;&lt; first</a></li>';
	echo '<li class="pager-previous"><a href="/search6?keyword=' . $keyword . '&pagesize='. $pageSize . '&curpage='. ($curPage-1) . '&orderby='. $orderBy .'&orderdir=' . $orderDir . '">&lt; previous</a></li>';
}

//page sections
$section = floor(($curPage-1) / $numPagesToShow);
$start = $section * $numPagesToShow;
$end = (($start + $numPagesToShow) > $numPages) ? $numPages : ($start + $numPagesToShow) ;


for ($i = $start ; $i < $end ; $i++)
{
	if ($curPage == ($i+1))
	{
		echo '<li class="pager-current">';
		echo ($i+1);
	}
	else
	{
		echo '<li class="active">';
		echo '<a href="/search6?keyword=' . $keyword . '&pagesize='. $pageSize . '&curpage='. ($i+1) .'&orderby='. $orderBy .'&orderdir=' . $orderDir . '">'. ($i+1) .'</a>';
	}
	echo '</li>';
}

if($curPage < $numPages)
{
	echo '<li class="paer-next"><a href="/search6?keyword=' . $keyword . '&pagesize='. $pageSize . '&curpage='. ($curPage+1) .'&orderby='. $orderBy .'&orderdir=' . $orderDir . '">next &gt;</a></li>';
	echo '<li class="pager-last"><a href="/search6?keyword=' . $keyword . '&pagesize='. $pageSize . '&curpage='. $numPages . '&orderby='. $orderBy .'&orderdir=' . $orderDir . '">last &gt;&gt;</a></li>';
}
echo '</ul>';
echo '</div>';

}
else
{
	echo '<p>&nbsp;</p><span>Your search returned no results. There you go. </span>';
}
//echo '</div>';
?>
