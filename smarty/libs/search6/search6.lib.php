<?php	 		 	

class Search6 {
	
	var $error = null;
	var $tpl = null;
	
 	function Search6() {
		 //$this->tpl =& new Display_Smarty; //php4
		 $this->tpl = new Display_Smarty; //php5
	}
	
	function displaySearch6(&$formvars,$pathName){
		$_GET = $formvars;
		$bodyclass = getBodyClasses($pathName);
		$this->tpl->assign('header','header.tpl');
		$this->tpl->assign('title',$bodyclass['title']);
		$this->tpl->assign('link',$bodyclass['link']);
		$this->tpl->assign('metatag',$bodyclass['meta'][0]);
		$this->tpl->assign('bodyclass', trim($bodyclass['frontVar'].' '.$bodyclass['sidebars'].' '.$bodyclass['classes']));
		$this->tpl->assign('topcontent','topcontent.tpl');
		$this->tpl->assign('search',TEMPLATE_DIR.'libs/search6/search.lib.php');
		$this->tpl->assign('footer','footer.tpl');
		$this->tpl->display('content/search6.tpl');
	
	}
	
}



?>