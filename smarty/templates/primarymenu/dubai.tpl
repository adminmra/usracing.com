<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Dubai World Cup<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
    <li><a href="/dubai-world-cup/results" >Dubai World Cup Results</a></li> 
    <li><a href="/dubai-world-cup/winners" >Dubai World Cup Winners</a></li>          
  </ul>
</li>