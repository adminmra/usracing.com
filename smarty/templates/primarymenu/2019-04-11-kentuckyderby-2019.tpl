<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Kentucky Derby<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu newnewtwo" id="double">
    <li><a href="/kentucky-derby">Kentucky Derby</a></li>
    <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
    <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
    <li><a href="/road-to-the-roses">Road to the Roses</a></li>
    <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
    <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
    <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
    <li><a href="/kentucky-derby/free-bet">Kentucky Derby Free Bet</a></li>
    <li><a href="/kentucky-derby/jockey-betting">Kentucky Derby Jockey Betting</a></li>
    <li><a href="/kentucky-derby/trainer-betting">Kentucky Derby Trainer Betting</a></li>
    <li><a href="/kentucky-derby/match-races" class="nobroder">Kentucky Derby Match Races</a></li>
    <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Margin of Victory</a></li>
    <li><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li>
    <li><a href="/kentucky-derby/racing-schedule">Kentucky Derby Racing Schedule</a></li>
    <li><a href="/kentucky-derby/video">Kentucky Derby Video</a></li>
    <li><a href="/kentucky-oaks">Kentucky Oaks</a></li>
    <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>
    <li><a href="/kentucky-oaks/betting">Kentucky Oaks Betting</a></li>
    <li class="bc-nemu-last"><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li>
        <!--<li class="bc-nemu-last"><a href="/twin-spires">Twin Spires</a></li>-->
    <li class="bc-nemu-empty"><a>&nbsp;</a></li>    
  </ul>
</li>