{* <!--Horse Racing News ================================================ --> *}
<li class="dropdown">
{* <a href="/news" >Racing News</a> *}
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >{* Latest *} News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
	<li><a href="/news">Latest Racing News </a></li>
	{* <!--<li><a href="#">Kentucky Derby News </a></li>>--> *}
	{* <!--<li><a href="/news/news-notes">news-notes </a></li>--> *}
    {if $BS_Switch_Menu}
      <li><a href="/news/belmont-stakes/">Belmont Stakes News</a></li>
      <li><a href="/news/belmont-stakes-betting/">Belmont Stakes Betting</a></li>    
    {/if}    
    
    
		<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
{if $BC_Switch}
	{*<li><a href="/news/breeders-cup">Breeders' Cup News</a></li>*}
{/if}
{if $PWC_Switch}
<li><a href="/news/tag/pegasus-world-cup/">Pegasus World Cup News</a></li>
{/if}
{if $DWC_Switch} <!-- Please add the options here --> {/if}
{if $KD_Switch_Early}
	<li><a href="/news//tag/kentucky-derby">Kentucky Derby News</a></li>
	{*<li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>
	<li><a href="/news/tag/kentucky-derby-handicapping">Kentucky Derby Handicapping</a></li>
{*	<li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>
	<li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li> *}
{/if}
{if $KD_Switch_Main}
	<li><a href="/news//tag/kentucky-derby">Kentucky Derby News</a></li>
	<li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>
	<li><a href="/news/tag/kentucky-derby-handicapping">Kentucky Derby Handicapping</a></li>
{*	<li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>
	<li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li> *}
{/if}
{if $KD_Switch_Race_Day}
	<li><a href="/news//tag/kentucky-derby">Kentucky Derby News</a></li>
	<li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>
	<li><a href="/news/tag/kentucky-derby-handicapping">Kentucky Derby Handicapping</a></li>
{*	<li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>
	<li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li> *}
{/if}
{if $PS_Switch}
	<li><a href="/news/tag/preakness-stakes-news">Preakness Stakes News </a></li>
{*     <li><a href="/news/tag/preakness-stakes-favorites">Preakness Stakes Favorites </a></li>  *}
    <li><a href="/news/tag/preakness-stakes-contenders">Preakness Stakes Contenders </a></li>
{*     <li><a href="/news/tag/preakness-stakes-workouts">Preakness Stakes Workouts </a></li>  *}
{/if}
{*if $BS_Switch}
      <li><a href="/news/tag/belmont-stakes-news">Belmont Stakes News </a></li>
      <li><a href="/news/tag/belmont-stakes-handicapping">Belmont Stakes Handicapping</a></li>
{/if *}

{if $dateNow >= '2019-08-19 00:00:00' && $dateNow <= '2019-08-27 00:59:59'}<li><a href="/news/tag/travers-stakes/">Travers Stakes</a></li>{/if}
{if $Default}
	{*<li><a href="/news/tag/triple-crown">Triple Crown News </a></li> *}
	{*<li><a href="/news/features">Featured Stories </a></li>
		<li><a href="/news/analysis">Racing Analysis </a></li>
		<li><a href="/news/harness-racing">Harness Racing</a></li>
		<li><a href="/news/recap">Race Recaps</a></li> *}
	<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>
	{*<li><a href="/usr-awards">2019 USR Awards</a></li>*}
	{if $dateNow >= '2020-01-15 00:00:00' && $dateNow <= '2020-01-31 23:59:59'}<li><a href="/usr-awards-winners">2019 USR Awards</a></li>{/if}
{/if}
{* <li><a href=""> </a></li>  *}
 </ul>
</li>