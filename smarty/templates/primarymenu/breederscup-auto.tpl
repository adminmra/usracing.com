{*<!-- Breeders Cup ================================================ --> *}
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >Breeders&#039; Cup <i class="fa fa-angle-down"></i></a>
{if $bcmenu=='Y' and $bcjmenu=='Y' and $bcFmenuM=='Y'}
    <ul class="dropdown-menu newnew" id="double">
  {elseif $bcmenu=='Y' and $bcjmenu=='N' and $bcFmenuM=='Y'}
  <ul class="dropdown-menu newnewtwo" id="double">
    {elseif $bcmenu=='N' and $bcjmenu=='Y' and $bcFmenuM=='Y'}
  <ul class="dropdown-menu newnewtwo" id="double">
    {elseif $bcmenu=='Y' and $bcjmenu=='Y' and $bcFmenuM=='N'}
  <ul class="dropdown-menu newnewtwo" id="double">
 {else}
  <ul class="dropdown-menu" id="double">
 {/if}

{if $bcFmenuM=='Y' }
        <div class="bc-menu-title">Breeders' Cup</div>
        <li><a href="/bet-on/breeders-cup"  >Bet on Breeders' Cup</a></li>
        <li><a href="/breeders-cup" >{include file='/home/ah/allhorse/public_html/breeders/year.php'}  Breeders' Cup</a></li>
        <li><a href="/breeders-cup/betting" >Breeders&#039; Cup Betting</a></li>
        <li><a href="/news/breeders-cup" >Latest Updates</a>
        <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
        <li><a href="/news/tag/breeders-cup-contenders/"  >Breeders&#039; Contenders</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/challenge" >Breeders&#039; Cup Challenge</a></li>
        {if $bcmenu=='Y'}
        <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
                <li class="bc-nemu-empty"><a>&nbsp;</a></li> 
                {/if}
 
{*          <li class="bc-nemu-empty"><a>&nbsp;</a></li>   *}

{/if}
            {if $bcjmenu=='Y'}
    
       

 
        <div class="bc-menu-title">Friday Nov. 2nd</div>
        <li><a href="/breeders-cup/juvenile"  > Juvenile </a></li>
        <li><a href="/breeders-cup/juvenile-turf" >Juvenile Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf"  >Juvenile Fillies Turf</a></li>
        <li><a href="/breeders-cup/juvenile-fillies"  >Juvenile Fillies</a></li>   
		<li class="bc-nemu-last"><a href="/breeders-cup/juvenile-turf-sprint" >Juvenile Turf Sprint</a></li>	
        
        {if $bcmenu=='Y' and $bcFmenuM=='Y'}
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty finalBC"><a>&nbsp;</a></li> 
        {elseif $bcmenu=='Y' and $bcFmenuM=='N'}
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty finalBC"><a>&nbsp;</a></li>              
          {elseif $bcmenuM=='N' and $bcFmenuM=='Y'}
         <li class="bc-nemu-empty"><a>&nbsp;</a></li>  
          <li class="bc-nemu-empty finalBC"><a>&nbsp;</a></li>            
          {/if}

      {/if}


 {if $bcmenu=='Y'}

 
        <div class="bc-menu-title">Saturday Nov. 3rd</div>


		<li><a href="/breeders-cup/classic">Classic <!--<span class=“sub-link-hour”>@ 4PM</span>--></a></li>    {* <span class=“sub-link-hour”>@ 4PM</span> *}
        
	{*<div class="bc-menu-title-lower">@ 4PM</div>	*}												{* Saturday *}
		<li><a class="turfBC" href="/breeders-cup/turf"  >Turf </a></li>
        <li><a href="/breeders-cup/distaff"  > Distaff </a></li>
        <li><a href="/breeders-cup/mile" >Mile</a></li>
        <li><a href="/breeders-cup/sprint"  >Sprint</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >Filly and Mare Turf</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >Dirt Mile</a></li>
        <li><a href="/breeders-cup/turf-sprint"  >Turf Sprint</a></li>
        <li class="bc-nemu-last"><a href="/breeders-cup/filly-mare-sprint"  >Filly and Mare Sprint</a></li>       
	 

{/if}

        

      
      
      
    </ul>
</li>
