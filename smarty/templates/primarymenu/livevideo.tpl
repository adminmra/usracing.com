<!-- Live Video ******************************************************* -->
<li class="dropdown">
<a href="/live-racing-video" title="Live Racing Video" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">LIVE  VIDEO</a>

<ul class="dropdown-menu">
<li><a href="/live-racing-video" title="US Racing on your Mobile Device">Watch the Races from your mobile phone</a></li>
<li><a href="/live-racing-video/iphone" title="Live Racing on iPhone">Live Racing on iPhone</a></li>
<li><a href="/live-racing-video/ipad" title="Live Racing on iPad">Live Racing on iPad</a></li>
<li><a href="/live-racing-video/android" title="Live Racing on Android">Live Racing on Android</a></li>
<li><a href="/live-racing-video/blackberry" title="Live Racing on Blackberry">Live Racing on Blackberry</a></li>
</ul>
</li>
