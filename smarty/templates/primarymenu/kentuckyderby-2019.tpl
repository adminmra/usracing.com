<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Kentucky Derby<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu newnewtwo" id="double">
{*     <li><a href="/kentucky-derby">Kentucky Derby</a></li> *}
    <li><a href="/kentucky-derby/free-bet">Kentucky Derby FREE BET</a></li>
    <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
    <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
    <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
    <li><a href="/kentucky-derby/jockey-betting">Kentucky Derby Betting: Jockeys</a></li>
    <li><a href="/kentucky-derby/trainer-betting">Kentucky Derby Betting: Trainers</a></li>
	<li ><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li>  
{*     <li><a href="/road-to-the-roses">Road to the Roses</a></li> *}

{*
    <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
    <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
*}
    <li><a href="/kentucky-derby/match-races" class="nobroder">Kentucky Derby Match Races</a></li>
    <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Margin of Victory</a></li>
    <li><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li>
    <li><a href="/kentucky-derby/racing-schedule">Kentucky Derby Racing Schedule</a></li>
    {if $KD_KOAKS_Switch}
    <li><a href="/kentucky-oaks">Kentucky Oaks</a></li>
    <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>
    <li class="bc-nemu-last"><a href="/kentucky-oaks/betting">Kentucky Oaks Betting</a></li> 
    {/if} 
 
   {* <li class="bc-nemu-last"><a href="/twin-spires">Twin Spires</a></li> *}
   {*if $KD_KOAKS_Switch}
{*     <li class="bc-nemu-empty"><a>&nbsp;</a></li>  *}
    {*/if *}   
  </ul>
</li>
<!-- class="nobroder"--->


{* 		 <li><a href="/news/kentucky-derby-betting-cryptocurrency-increases-popularity">Gambling with Bitcoin at  Derby</a></li>	 *}    
			    
			  
{* 	    <li><a href="/kentucky-derby/props" >Kentucky Derby Betting: Props</a></li> *}
		
{* 		 <li><a href="/kentucky-oaks/odds" >Kentucky Oaks Odds</a></li> *}
{* 	    <li><a href="/kentucky-derby/odds" >Kentucky Derby Odds</a></li> *}
{* 		<li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li> *}
{* 		<li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li> *}
{*       <li><a href="/kentucky-derby/match-races">Match Races</a></li> *}
{* 		<li><a href="/kentucky-derby/margin-of-victory">Margin of Victory Odds</a></li> *}
{* 		<li><a href="/bet-on/triple-crown">Triple Crown Winner Odds</a></li> *}
{* 		<li><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li> *}
{* 	    <li><a href="/news/kentucky-derby-road-to-the-roses">Kentucky Derby News</a></li> *}
{* 	    <li><a href="/kentucky-derby/contenders">Kentucky Derby Contender Rankings</a></li>  *}
{* 	    <li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>  *}
{* 	    <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-workout-report">Kentucky Derby Workouts</a></li>  *}
{* 	    	    <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>  *}
{* 	    <li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li>  *}
{*
   <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
      <li><a href="/kentucky-derby/winners" >Kentucky Derby Winners</a></li>
		<li><a href="/promos/blackjack-tournament">Blackjack Tournament</a></li>
*}

{*    <li><a href="/mint-julep-recipe"  >Mint Julep Recipes</a></li> *}
   
