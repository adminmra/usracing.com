<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Famous Races<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
   {*<li><a href="/kentucky-derby/betting"  >Kentucky Derby</a></li>*}
    <li><a href="/royal-ascot"  >Royal Ascot</a></li>
    <li><a href="/belmont-stakes/betting"  >Belmont Stakes</a></li>
    <li><a href="/breeders-cup/betting"  >Breeders' Cup</a></li>
    <li><a href="/dubai-world-cup"  >Dubai World Cup</a></li>
    <li><a href="/melbourne-cup"  >Melbourne Cup</a></li>
    <li><a href="/preakness-stakes/betting"  >Preakness Stakes</a></li>
    <li><a href="/hong-kong-racing"  >Hong Kong Racing</a></li>
    <li><a href="https://secure.usracing.com/signup" title="And Even More Available!" rel="nofollow">And Even More Available!</a></li>
    {*
    <li><a href="/kentucky-derby" >{include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby</a></li>
    <li><a href="/kentucky-derby/betting"  >Kentucky Derby Betting</a></li>
    <li><a href="/kentucky-derby/contenders" >Kentucky Derby Contenders</a></li>
    <li><a href="/kentucky-derby/prep-races"  >Kentucky Derby Prep Races</a></li>
    <li><a href="/kentucky-derby/odds"  >Kentucky Derby Odds</a></li>
    <li><a href="/kentucky-derby/future-wager"  >Derby future wager</a></li>
    <li><a href="/kentucky-derby/winners"  >Kentucky Derby Past Winners</a></li>
    <li><a href="/kentucky-derby/graded-earnings" ></a></li>
    <li><a href="/churchill-downs"  >Churchill Downs</a></li>
    <li><a href="/twin-spires"  >Twin Spires</a></li>
    <li><a href="/kentucky-oaks"  >Kentucky Oaks</a></li>
    <li><a href="/kentucky-oaks/betting">Kentucky Oaks Betting</a></li>
    *}
  </ul>
</li>