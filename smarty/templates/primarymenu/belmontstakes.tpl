<li class="dropdown"><a href="/belmont-stakes" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
    data-delay="0" data-close-others="false">Belmont Stakes <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
    {if $BS_Switch_Early}
      <li><a href="/belmont-stakes"> {include file="/home/ah/allhorse/public_html/belmont/year.php"} Belmont Stakes</a></li>
      <li><a href="/belmont-stakes/free-bet">Free Belmont Stakes Bet</a></li>
      <li><a href="/belmont-stakes/betting">Belmont Stakes Betting</a></li>
      <li><a href="/belmont-stakes/odds">Belmont Stakes Odds</a></li>
      <li><a href="/belmont-stakes/contenders">Belmont Stakes Contenders</a></li>
      <li><a href="/belmont-stakes/entries">Belmont Stakes Entries</a></li>
      <li><a href="/belmont-stakes/watch-live">Belmont Stakes Livestream</a></li>
        {*<li><a href="/preakness-stakes/results">Preakness Stakes Results</a></li>
      <li><a href="/preakness-stakes/winners">Preakness Stakes Winners</a></li>
      {* <li><a href="/belmont-park">Belmont Park</a></li> 
      <li><a href="/bet-on/belmont-stakes">Bet on the Belmont Stakes</a></li>
      <li><a href="/news/belmont-stakes/">Belmont Stakes News</a></li> *}
      
      {/if}
      
      {if $BS_Switch_Main}
 <li><a href="/belmont-stakes"> {include file="/home/ah/allhorse/public_html/belmont/year.php"} Belmont Stakes</a></li>
      <li><a href="/belmont-stakes/free-bet">Free Belmont Stakes Bet</a></li>
      <li><a href="/belmont-stakes/betting">Belmont Stakes Betting</a></li>
      <li><a href="/belmont-stakes/odds">Belmont Stakes Odds</a></li>
      <li><a href="/belmont-stakes/contenders">Belmont Stakes Contenders</a></li>
      <li><a href="/belmont-stakes/entries">Belmont Stakes Entries</a></li>
      <li><a href="/belmont-stakes/watch-live">Belmont Stakes Livestream</a></li>
    {/if}

      {if $BS_Switch_Race_Day}
 <li><a href="/belmont-stakes"> {include file="/home/ah/allhorse/public_html/belmont/year.php"} Belmont Stakes</a></li>
      <li><a href="/belmont-stakes/free-bet">Free Belmont Stakes Bet</a></li>
      <li><a href="/belmont-stakes/betting">Belmont Stakes Betting</a></li>
      <li><a href="/belmont-stakes/odds">Belmont Stakes Odds</a></li>
      <li><a href="/belmont-stakes/contenders">Belmont Stakes Contenders</a></li>
      <li><a href="/belmont-stakes/entries">Belmont Stakes Entries</a></li>
      <li><a href="/belmont-stakes/watch-live">Belmont Stakes Livestream</a></li>
    {/if}

    {if $BS_Results}
      <li><a href="/belmont-stakes/results">Belmont Stakes Results</a></li>
      <li><a href="/belmont-stakes/winners">Belmont Stakes Winners</a></li>
    {/if}
    {*
    <li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
    *}
    {* <li><a href="/belmont-stakes/free-bet">FREE BET</a></li> *}
    {if $dateNow <= '2019-06-21 23:59:59' }<li><a href="/promos/blackjack-tournament">Blackjack Tournament</a>
</li>{/if}
{* <li><a href="/belmont-stakes/betting-guide">Free Belmont Stakes Betting Guide</a></li> *}
{if $BS_Switch_Results}<li><a href="/belmont-stakes/results">Belmont Stakes Results</a></li>
<li><a href="/belmont-stakes/winners">Belmont Stakes Winners</a></li>{/if}

{if $PS_Switch_Results} <li><a href="/preakness-stakes/results">Preakness Stakes Results</a></li>
<li><a href="/preakness-stakes/winners">Preakness Stakes Winners</a></li> {/if}

{if $KD_Switch_Results} <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
<li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li> {/if}

</ul>
</li>
