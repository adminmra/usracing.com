{* assign var="dateNow" value=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S' *}{*Inline Expiring setting 'now' time*}
<li id="collapse-bc-desktop-odds" class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">{* Latest *} Odds<i class="fa fa-angle-down"></i></a>
 {*if $BC_FRI_Switch and  $BC_SAT_Switch}
  <ul class="dropdown-menu newnewOdds" id="double">
 {elseif $BC_FRI_Switch or  $BC_SAT_Switch}
    <ul class="dropdown-menu newnewOddstwo" id="double">
 {else}
    <ul class="dropdown-menu" id="double">
 {/if*}
  {if $BC_PREMENU_Switch}
  <ul class="dropdown-menu newnewOdds" id="double">
 {else}
    <ul class="dropdown-menu" id="double">
 {/if}
 {* <ul class="dropdown-menu" id="double"> *}
     {* <div class="bc-menu-title lo">Latest Odds</div> *}
	 {* {if $dateNow <= '2022-01-29 23:59:59'}<li><a href="/pegasus-world-cup/odds">2022 Pegasus World Cup Odds</a></li>{/if}  *}
{if $dateNow <= '2022-05-07 23:59:59'}<li><a href="/kentucky-derby/odds">{include file="/home/ah/allhorse/public_html/kd/year.php"} Kentucky Derby Odds</a></li> {/if}
{if $dateNow <= '2020-09-05 18:50:59'}<li><a href="/kentucky-derby/props" >Kentucky Derby Props</a></li>{/if}
{if $dateNow <= '2020-09-05 18:50:59'}<li><a href="/kentucky-derby/match-races" >Kentucky Derby Match Races</a></li>{/if}
{if $dateNow <= '2020-09-04 17:45:59'}<li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>{/if}
{* <li class="bc-nemu-last"><a href="/bet-on/triple-crown">Triple Crown Odds</a></li> *}
{* <li><a href="/travers-stakes">Travers Stakes</a></li> *}
{* <li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li> *}
<li><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>
{if $dateNow <= '2020-11-15 23:59:59'}<li><a href="/odds/masters">The Masters Odds</a></li>{/if}
{if $dateNow <= '2020-10-03 17:45:59'}<li><a href="/preakness-stakes/odds">Preakness Stakes Odds</a></li>{/if}
{if $dateNow <= '2020-11-07 17:45:59'}<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>{/if}
{*{if $dateNow <= '2021-05-01 18:50:59'}<li><a href="/kentucky-derby/odds"> Kentucky Derby {$KD_YEAR} Odds</a></li> {/if}*}
{if $dateNow <= '2021-02-20 12:40:59'}<li><a href="/saudi-cup/odds">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds</a></li>{/if}

{if $dateNow <= '2020-07-18 20:00:00'}
<li><a href="/haskell-stakes">Haskell Stakes Betting</a></li>
{/if}

{* <li><a href="/saratoga">Saratoga Horse Betting</a></li> *}
{if $dateNow <= '2021-01-23 23:59:59'}<li><a href="/pegasus-world-cup/odds">2021 Pegasus World Cup Odds</a></li>{/if} 
{if $dateNow <= '2021-03-28 23:59:59'}<li><a href="/dubai-world-cup/odds">2021 Dubai World Cup Odds</a></li>{/if} 
<li><a href="/odds"  >Today's Horse Racing Odds</a></li>

{if $dateNow <= '2021-04-10 23:59:59'}<li><a href="/grand-national">Grand National Odds</a></li>{/if}
{if $dateNow <= '2020-09-07 20:00:00'}
<li><a href="/del-mar">Del Mar Horse Betting</a></li>
{/if}

{*if $dateNow <= '2020-09-05 18:00:00'}
<li><a href="/bluegrass-stakes">Blue Grass Stakes Odds</a></li>
{/if*}

{if $dateNow <= '2020-06-21 23:59:59'}
<li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/belmont/yearlast.php'} Belmont Stakes Odds</a></li>
{/if}

{if $BS_Switch_Phase_1}
<li><a href="/belmont-stakes/props">Belmont Stakes Props</a></li>
<li><a href="/belmont-stakes/match-races">Belmont Stakes Match Races</a></li>
{/if}

{if $KD_Switch}
{*Activate KD ODDS when KD Switch is live and odds are made -TF*}
{* <li><a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</a></li> *}
{* <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li> *}
{* <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li> *}
{*  <li><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li> *}
{*  <li><a href="/kentucky-derby/match-races">Kentucky Derby Odds:  Match Races</a></li> *}
{* <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Odds: Margin of Victory </a></li> *}
{/if}
{if $KD_KOAKS_Switch}<li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>{/if}
{*KD old odds runs 5 days after Derby every year TF-May 9,19*}
{if $PS_Switch_Main}
<li><a href="/preakness-stakes/odds"> Preakness Stakes {include file='/home/ah/allhorse/public_html/ps/year.php'} Odds</a></li>
{*<li><a href="/preakness-stakes/props"  >Preakness Stakes Props</a></li>
<li><a href="/preakness-stakes/match-races"  >Preakness Stakes Match Races</a></li>*}
{/if}
{if $BS_Switch}
{* <li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/belmont/year.php'} Belmont Stakes Odds</a></li> *}
{* <li><a href="/belmont-stakes/props"  >Belmont Stakes Props</a></li> *}
{* <li><a href="/belmont-stakes/match-races"  >Belmont Stakes Match Races</a></li> *}
{/if}
{if $dateNow <= '2019-06-18 23:59:59'}<li><a href="/queen-anne-stakes"  >Queen Anne Stakes {* Betting *} Odds</a></li> {/if}
{if $dateNow <= '2019-06-20 23:59:59'} <li><a href="/ascot-gold-cup"  >Royal Ascot {* Horse Racing *} Odds</a></li> {/if}
{if $dateNow <= '2019-08-24 23:59:59'} <li><a href="/travers-stakes"  >Travers Stakes Odds</a></li> {/if}
{*if $dateNow <= '2019-10-06 23:59:59'} <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li> {/if*}
{if $dateNow <= '2019-11-04 23:59:59'}<li><a href="/melbourne-cup"  >Melbourne Cup Odds</a></li>{/if}
{if $dateNow <= '2019-11-03 23:59:59'}<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>{/if}
{*if $dateNow <= '2019-11-04 23:59:59'}<li><a href="/breeders-cup/classic"  >Breeders' Cup Classic Odds</a> </li>
        <li><a href="/breeders-cup/distaff"  >Breeders' Cup Distaff  Odds</a></li>
        <li><a href="/breeders-cup/turf"  >Breeders' Cup Turf  Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-turf"  >Breeders' Cup  Filly and Mare Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile"  >Breeders' Cup  Juvenile Odds</a></li>
        <li><a href="/breeders-cup/dirt-mile"  >Breeders' Cup  Dirt Mile Odds</a></li>
         <li><a href="/breeders-cup/mile" >Breeders' Cup  Mile Odds</a></li>
         <li><a href="/breeders-cup/sprint"  >Breeders' Cup Sprint Odds</a></li>

        {/if *}
{if $dateNow <= '2019-11-03 23:59:59'}<li><a href="/breeders-cup/classic-top3-finishing-odds"  >Breeders' Cup Top 3 Finishing Odds</a> </li>  {/if}
{if $dateNow <= '2020-01-26 17:34:00'}<li><a href="/pegasus-world-cup/odds">Pegasus World Cup Odds</a></li>{/if}
{* if $dateNow <= '2021-02-20 12:40:59'}<li><a href="/saudi-cup/odds">Saudi Cup Odds</a></li>{/if *}
{* {if $dateNow <= '2021-03-27 23:59:59'}<li><a href="/dubai-world-cup/odds">Dubai World Cup Odds</a></li>{/if}  *}
{* {if $dateNow <= '2020-04-04 23:59:59'}<li><a href="/grand-national">Grand National Odds</a></li>{/if}  *}
{*KD Odds, put live when we have odds, deactivate when KD SWITCH is live -TF*}

{* <li><a href="/prix-de-larc-de-triomphe">Prix de l'Arc de Triomphe Odds</a></li> *}
{* <li><a href="/melbourne-cup">Melbourne Cup Odds</a></li> *}
{if $dateNow <= '2020-01-05 21:00:00'}<li><a href="/odds/golden-globes"> Golden Globes Awards</a></li>{/if}
{if $dateNow <= '2020-01-26 23:59:59'}<li><a href="/odds/grammy-awards">Grammy Awards</a></li>{/if}
{if $dateNow <= '2020-02-09 23:59:59'}<li><a href="/odds/academy-awards">Academy Awards "Oscars"</a></li>{/if}
{if $dateNow <= '2020-06-16 23:59:59'}<li><a href="/ascot-gold-cup">Royal Ascot Gold Cup Odds</a></li>{/if}
{*REsults links set to expire 1 week after BS - TF*}
{if $dateNow <= '2020-05-02 18:30:00'}<li class="bc-nemu-last"><a href="/arkansas-derby">Arkansas Derby Odds</a></li>{/if}
{if $dateNow <= '2020-05-02 17:45:00'}<li class="bc-nemu-last"><a href="/virtual-kentucky-derby">Virtual Kentucky Derby Odds</a></li>{/if}
{if $dateNow <= '2019-09-22 19:59:59'}<li class="bc-nemu-last"><a href="/odds/emmy-awards">Emmy Awards Odds</a></li>{/if}
{if $dateNow >= '2019-05-04 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>  {/if}
{if $KD_Switch_Old_Odds}<li><a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Kentucky Derby Odds</a></li>{/if}
{if $dateNow >= '2019-05-18 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>   {/if}
{if $PS_Switch_Old_Odds}<li><a href="/preakness-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Preakness Odds</a></li>{/if}
{if $dateNow >= '2019-06-08 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>   {/if}
{*{if $BS_Switch_Old_Odds}<li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Belmont Stakes Odds</a></li>{/if} *}
{if $BS_Switch_Old_Odds}<li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'} Belmont Stakes Odds</a></li>{/if}
 {*       <div class="bc-menu-title">All Odds</div> *}
{*<li><a href="/odds/ascot">Ascot Horse Racing Odds</a></li>*}
 {* <li><a href="/odds/oscar"  >Oscar Odds</a></li> *}
{* <li><a href="/pegasus-world-cup/odds"  >Pegasus World Cup Odds</a></li> *}
{* {if $dateNow <= '2019-04-14 23:59:59'}<li><a href="/odds/masters">Golf Odds:  The Masters</a></li>{/if} *}
{* <li><a href="/grand-national"  >Grand National Odds</a></li> *}
{*<li><a href="/diamond-jubilee-stakes"  >Diamond Jubilee Stakes Betting</a></li> *}
{* <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li> *}

{* *******************************************TEMPORARY FOR 2022 -TF *}

{if $BC_Switch_Main || $BC_Switch_Race_Day}<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>{/if}
{* <li><a href="/breeders-cup/distaff">Breeders' Cup Distaff</a></li> *}
{*<li><a href="/belmont-stakes/odds"  >Belmont Stakes Odds</a></li>     *}
{* <li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li> *}
    {if $BC_PREMENU_Switch}
        <div class="bc-menu-title fri">Breeders' Cup Nov. 6th</div>
        <li><a href="/breeders-cup/juvenile-turf-sprint">BC Juvenile Turf Sprint Odds</a></li>
        <li><a href="/breeders-cup/juvenile-turf">BC Juvenile Turf Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies">BC Juvenile Fillies Odds</a></li>
        <li><a href="/breeders-cup/juvenile-fillies-turf">BC Juvenile Fillies Turf Odds</a></li>
        <li  class="bc-nemu-last"><a href="/breeders-cup/juvenile">BC Juvenile Odds</a></li>
        <li class="bc-nemu-empty "><a>&nbsp;</a></li>
        <li class="bc-nemu-empty "><a>&nbsp;</a></li>
         <li class="bc-nemu-empty "><a>&nbsp;</a></li>
          <li class="bc-nemu-empty "><a>&nbsp;</a></li>
     {* <li class="bc-nemu-empty "><a>&nbsp;</a></li> *}
  {/if}
    {if $BC_PREMENU_Switch}
        <div class="bc-menu-title ed" style="height: 37.5px;">Breeders' Cup Nov. 7th</div>
        <li><a href="/breeders-cup/filly-mare-sprint">Filly and Mare Sprint Odds</a></li>
        <li><a href="/breeders-cup/turf-sprint">Turf Sprint Odds</a></li>
        <li><a href="/breeders-cup/dirt-mile">Dirt Mile Odds</a></li>
        <li><a href="/breeders-cup/filly-mare-turf">Filly and Mare Turf Odds</a></li>
        <li><a href="/breeders-cup/sprint">Sprint Odds</a></li>
        <li><a href="/breeders-cup/mile">Mile Odds</a></li>
        <li><a href="/breeders-cup/distaff"> Distaff Odds</a></li>
        <li><a class="turfBC" href="/breeders-cup/turf">Turf Odds</a></li>
        <li  class="bc-nemu-last"><a href="/breeders-cup/classic">Breeders' Cup Classic Odds</a> </li>
    {* <li class="bc-nemu-empty "><a>&nbsp;</a></li> *} {* Saturday *}
  {/if}
  </ul>
</li>
    {* <!-- collapse Note: please all this code need the expiration code --> *}
    {if $BC_PREMENU_Switch}
    {literal}
    <style>
      @media (min-width: 768px) {
        #collapse-bc-mobile-odds {
          display: none;
        }
      }
      @media (max-width: 767px) {
        #collapse-bc-desktop-odds {
          display: none;
        }
      }
    </style>
    {/literal}
    <li id="collapse-bc-mobile-odds" class="dropdown">
      <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">{* Latest *} Odds<i class="fa fa-angle-down"></i></a>
      {if $BC_PREMENU_Switch}
        <ul class="dropdown-menu newnewOdds" id="double">
     {else}
        <ul class="dropdown-menu" id="double">
     {/if}
        <div class="accordion" id="bc-accordion-odds">
          <div class="card">
          {*  <div class="card-header" id="headingOneOdds">
              <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                <a class="btn btn-link collapsed bc-collapse-btn" data-toggle="collapse"
                  data-target="#bc-first-collapse-odds" aria-expanded="true" aria-controls="bc-first-collapse-odds">
                  <div class="collapse-btn-title">Latest Odds</div> <i class="fa fa-angle-down"></i>
                </a>
              </h2>
            </div> *}
            <div id="bc-first-collapse-odds" class="collapse" aria-labelledby="headingOneOdds"
              data-parent="#bc-accordion-odds">
              <div class="card-body">
                <li><a href="/odds"  >Today's Horse Racing Odds</a></li>
                {if $KD_Switch}
                {*Activate KD ODDS when KD Switch is live and odds are made -TF*}
                {* <li><a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</a></li> *}
                {* <li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Odds: Jockeys</a></li> *}
                {* <li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Odds: Trainers</a></li> *}
                {*  <li><a href="/kentucky-derby/props" >Kentucky Derby Odds: Props</a></li> *}
                {*  <li><a href="/kentucky-derby/match-races">Kentucky Derby Odds:  Match Races</a></li> *}
                {* <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Odds: Margin of Victory </a></li> *}
                {/if}
                {if $KD_KOAKS_Switch}<li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>{/if}
                {*KD old odds runs 5 days after Derby every year TF-May 9,19*}
                {if $PS_Switch}
                <li><a href="/preakness-stakes/odds">{include file='/home/ah/allhorse/public_html/ps/year.php'} Preakness Stakes Odds</a></li>
                <li><a href="/preakness-stakes/props"  >Preakness Stakes Props</a></li>
                <li><a href="/preakness-stakes/match-races"  >Preakness Stakes Match Races</a></li>
                {/if}
                {if $BS_Switch}
                <li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/belmont/year.php'} Belmont Stakes Odds</a></li>
                {* <li><a href="/belmont-stakes/props"  >Belmont Stakes Props</a></li> *}
                <li><a href="/belmont-stakes/match-races"  >Belmont Stakes Match Races</a></li>
                {/if}
                {if $dateNow <= '2019-06-18 23:59:59'}<li><a href="/queen-anne-stakes"  >Queen Anne Stakes {* Betting *} Odds</a></li> {/if}
                {if $dateNow <= '2019-06-20 23:59:59'} <li><a href="/ascot-gold-cup"  >Royal Ascot {* Horse Racing *} Odds</a></li> {/if}
                {if $dateNow <= '2019-08-24 23:59:59'} <li><a href="/travers-stakes"  >Travers Stakes Odds</a></li> {/if}
                {if $dateNow <= '2019-10-06 23:59:59'} <li><a href="/prix-de-larc-de-triomphe"  >Prix de l'Arc de Triomphe Odds</a></li> {/if}
                {if $dateNow <= '2019-11-04 23:59:59'}<li><a href="/melbourne-cup"  >Melbourne Cup Odds</a></li>{/if}
                {if $dateNow <= '2019-11-03 23:59:59'}<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>{/if}
                {if $dateNow <= '2019-11-03 23:59:59'}<li><a href="/breeders-cup/classic-top3-finishing-odds"  >Breeders' Cup Top 3 Finishing Odds</a> </li>  {/if}
                {*if $dateNow <= '2019-11-04 23:59:59'}<li><a href="/breeders-cup/classic"  >Breeders' Cup Classic Odds</a> </li>
                <li><a href="/breeders-cup/distaff"  >Breeders' Cup Distaff  Odds</a></li>
                <li><a href="/breeders-cup/turf"  >Breeders' Cup Turf  Odds</a></li>
                <li><a href="/breeders-cup/filly-mare-turf"  >Breeders' Cup  Filly and Mare Turf Odds</a></li>
                <li><a href="/breeders-cup/juvenile"  >Breeders' Cup  Juvenile Odds</a></li>
                <li><a href="/breeders-cup/dirt-mile"  >Breeders' Cup  Dirt Mile Odds</a></li>
                <li><a href="/breeders-cup/mile" >Breeders' Cup  Mile Odds</a></li>
                <li><a href="/breeders-cup/sprint"  >Breeders' Cup Sprint Odds</a></li>
                {/if *}
                {* {if $dateNow <= '2021-03-27 23:59:59'}<li><a href="/dubai-world-cup/odds">{include file='/home/ah/allhorse/public_html/dubai/year.php'} Dubai World Cup Odds</a></li>{/if}  *}
                {* {if $dateNow <= '2020-04-04 23:59:59'}<li><a href="/grand-national">2020 Grand National Odds</a></li>{/if}  *}
                {*KD Odds, put live when we have odds, deactivate when KD SWITCH is live -TF*}
				{if $dateNow <= '2022-01-29 23:59:59'}<li><a href="/pegasus-world-cup/odds">2022 Pegasus World Cup Odds</a></li>{/if} 
                {if $dateNow <= '2022-05-07 23:59:59'}<li><a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</a></li> {/if}
                {if $dateNow <= '2021-02-20 12:40:59'}<li><a href="/saudi-cup/odds">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds</a></li>{/if}
                {*REsults links set to expire 1 week after BS - TF*}
                <li><a href="/odds/us-presidential-election">US Presidential Election Odds</a></li>
                <li><a href="/belmont-stakes/odds">Belmont Stakes Odds</a></li>
                {if $dateNow <= '2020-05-02 23:59:59'}<li class="bc-nemu-last"><a href="/arkansas-derby">Arkansas Derby Odds</a></li>{/if}
                {if $dateNow <= '2019-09-22 19:59:59'}<li class="bc-nemu-last"><a href="/odds/emmy-awards">Emmy Awards Odds</a></li>{/if}
                {if $dateNow >= '2019-05-04 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>  {/if}
                {if $KD_Switch_Old_Odds}<li><a href="/kentucky-derby/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Kentucky Derby Odds</a></li>{/if}
                {if $dateNow >= '2019-05-18 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>   {/if}
                {if $PS_Switch_Old_Odds}<li><a href="/preakness-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Preakness Odds</a></li>{/if}
                {if $dateNow >= '2019-06-08 00:00:00' && $dateNow <= '2019-06-15 23:59:59'}<li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>   {/if}
                {*{if $BS_Switch_Old_Odds}<li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/yearlast.php'} Belmont Stakes Odds</a></li>{/if} *}
                {if $BS_Switch_Old_Odds}<li><a href="/belmont-stakes/odds">{include file='/home/ah/allhorse/public_html/kd/year.php'} Belmont Stakes Odds</a></li>{/if}
                {*       <div class="bc-menu-title">All Odds</div> *}
                {*<li><a href="/odds/ascot">Ascot Horse Racing Odds</a></li>*}
                {* <li><a href="/odds/oscar"  >Oscar Odds</a></li> *}
                {* <li><a href="/pegasus-world-cup/odds"  >Pegasus World Cup Odds</a></li> *}
                {* {if $dateNow <= '2019-04-14 23:59:59'}<li><a href="/odds/masters">Golf Odds:  The Masters</a></li>{/if} *}
                {* <li><a href="/grand-national"  >Grand National Odds</a></li> *}
                {*<li><a href="/diamond-jubilee-stakes"  >Diamond Jubilee Stakes Betting</a></li> *}
                {* <li><a href="/dubai-world-cup" >Dubai World Cup Odds</a></li> *}
                {*<li><a href="/breeders-cup/odds">Breeders' Cup Odds</a></li>*}
                {* <li><a href="/breeders-cup/distaff">Breeders' Cup Distaff</a></li> *}
                {*<li><a href="/belmont-stakes/odds"  >Belmont Stakes Odds</a></li>     *}
                {*
                {*
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                *}
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingTwoOdds">
              <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                <a class="btn btn-link collapsed bc-collapse-btn" data-toggle="collapse"
                  data-target="#bc-second-collapse-odds" aria-expanded="true" aria-controls="bc-second-collapse-odds">
                  <div class="collapse-btn-title">Breeders' Cup Nov. 6th</div> <i class="fa fa-angle-down"></i>
                </a>
              </h2>
            </div>
            <div id="bc-second-collapse-odds" class="collapse" aria-labelledby="headingTwoOdds"
              data-parent="#bc-accordion-odds">
              <div class="card-body">
                <li><a href="/breeders-cup/juvenile-turf-sprint">BC Juvenile Turf Sprint Odds</a></li>
                <li><a href="/breeders-cup/juvenile-turf">BC Juvenile Turf Odds</a></li>
                <li><a href="/breeders-cup/juvenile-fillies">BC Juvenile Fillies Odds</a></li>
                <li><a href="/breeders-cup/juvenile-fillies-turf">BC Juvenile Fillies Turf Odds</a></li>
                <li><a href="/breeders-cup/juvenile">BC Juvenile Odds</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                <li class="bc-nemu-empty"><a>&nbsp;</a></li>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header" id="headingThreeOdds">
              <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                <a class="btn btn-link collapsed bc-collapse-btn" data-toggle="collapse"
                  data-target="#bc-third-collapse-odds" aria-expanded="false" aria-controls="bc-third-collapse-odds">
                  <div class="collapse-btn-title">Breeders' Cup Nov. 7th</div> <i class="fa fa-angle-down"></i>
                </a>
              </h2>
            </div>
            <div id="bc-third-collapse-odds" class="collapse" aria-labelledby="headingThreeOdds"
              data-parent="#bc-accordion-odds">
              <div class="card-body">
                <li><a href="/breeders-cup/filly-mare-sprint">Filly and Mare Sprint Odds</a></li>
                <li><a href="/breeders-cup/turf-sprint">Turf Sprint Odds</a></li>
                <li><a href="/breeders-cup/dirt-mile">Dirt Mile Odds</a></li>
                <li><a href="/breeders-cup/filly-mare-turf">Filly and Mare Turf Odds</a></li>
                <li><a href="/breeders-cup/sprint">Sprint Odds</a></li>
                <li><a href="/breeders-cup/mile">Mile Odds</a></li>
                <li><a href="/breeders-cup/distaff"> Distaff Odds</a></li>
                <li><a class="turfBC" href="/breeders-cup/turf">Turf Odds</a></li>
                <li><a href="/breeders-cup/classic">Breeders' Cup Classic Odds</a> </li> {* Saturday *}
              </div>
            </div>
          </div>
        </div>
      </ul>
    </li>
    {* <!-- collapse end --> *}
    {/if}