 <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Help Center <i class="fa fa-angle-down"></i></a>
<ul class="dropdown-menu right">
  {*<li><a href="/support" >Customer Support</a></li>*}
  <li><a href="/how-to/bet-on-horses">How to Place a Bet</a></li>
  <li><a href="/horse-betting/beginner-tips">Beginner's Tips</a></li>
  {*<li><a href="/faqs">FAQs</a></li>
  <li><a href="https://secure.usracing.com/video-faq">Video FAQs</a></li>*}
  <li><a href="/how-to/read-the-daily-racing-form">How to Read the DRF</a></li>
  <li><a href="/how-to/read-past-performances">Past Performances</a></li>
  <li><a href="/horse-betting/money-management">Money Management</a></li>
  <li><a href="/horse-racing-terms">Horse Racing Terms</a></li>
  {*<li><a href="/terms">Terms & Conditions</a></li>
   <li><a href="#">Call Us, It's Toll Free!</a></li>
  <li><a href="/today" title="Today&#039;s Tracks">Today&#039;s Tracks</a></li>
  <li><a href="/live-video" title="Live Video.">Live Video</a></li>
  <li><a href="/mobile-betting" title="Mobile Horse Betting">Mobile Betting</a></li>
  <li><a href="/handicapping" title="Horse Racing Handicapping">Handicapping</a></li>
  *}
</ul>
</li>
