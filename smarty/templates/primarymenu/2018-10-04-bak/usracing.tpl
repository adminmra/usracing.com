<!-- About ================================================ -->
<li class="dropdown">
  <a href="/about-us" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >US RACING <i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu right">
  	  <li><a href="/about" >About Us</a></li>
      <li><a href="/signup"   rel="nofollow">Sign Up Today for Free</a></li>
     
       
      <li><a href="/horse-racing-schedule"  >Horse Racing Schedule</a></li>
   
      <li><a href="/results"  >Horse Racing Results</a></li>
      <li><a href="/graded-stakes-races" >Graded Stakes Races</a></li>
      <li><a href="/news" >Horse Racing News</a></li>
      
      <li><a href="/contact">Contact Us</a></li>
     
        <li><a href="/today" title="Today&#039;s Tracks">Today&#039;s Tracks</a></li>
      <li><a href="/live-video" title="Live Video.">Live Video</a></li>
      <li><a href="/mobile-betting" title="Mobile Horse Betting">Mobile Betting</a></li>
      <li><a href="/handicapping" title="Horse Racing Handicapping">Handicapping</a></li>
      
      
 </ul>
</li>

