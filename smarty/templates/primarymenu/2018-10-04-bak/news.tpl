<!--Horse Racing News ================================================ -->   
<li class="dropdown">{* <a href="/news" >Racing News</a> *}
     
  <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" >{* Latest *} News<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">

<li><a href="/news">Latest Racing News </a></li>


{* <li><a href="/news/kentucky-derby-road-to-the-roses">Kentucky Derby News</a></li> *}
{* <li><a href="/news/tag/kentucky-derby-contenders/">Kentucky Derby Contenders</a></li>  *}
{* <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-workout-report">Kentucky Derby Workouts</a></li>  *}
{* <li><a href="/news/kentucky-derby-road-to-the-roses/kentucky-derby-news-notes">Kentucky Derby Notes</a></li>  *}
{*  <li><a href="/news/winning-speed-figures">Kentucky Derby Speed Ratings</a></li>  *}
{* <li><a href="/news/tag/kentucky-derby-handicapping">Kentucky Derby Handicapping</a></li>  *}


 
	
{* 	<li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li> *}
{*	   <li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>  
   <li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>  
	
	
	<li><a href="/news/tag/preakness-stakes-news">Preakness Stakes News </a></li>  *}
{*
<li><a href="/news/tag/preakness-stakes-favorites">Preakness Stakes Favorites </a></li> 
<li><a href="/news/tag/preakness-stakes-contenders">Preakness Stakes Contenders </a></li> 
*}
{* <li><a href="/news/tag/preakness-stakes-workouts">Preakness Stakes Workouts </a></li>  *}


{* <li><a href="/news/breeders-cup/">Breeders' Cup News </a></li>  
<li><a href="/news/tag/triple-crown">Triple Crown News </a></li> *}


	    


<li><a href="/news/features">Featured Stories </a></li>
<li><a href="/news/analysis">Racing Analysis </a></li>
<li><a href="/news/handicapping-reports">Handicapping & Picks </a></li>
<li><a href="/news/harness-racing">Harness Racing</a></li>
<li><a href="/news/recap">Race Recaps</a></li>
<li><a href="/news/horse-racing-cartoon">Day in the Life - Cartoons</a></li>

{* <li><a href="/news/breeders-cup">Breeders' Cup</a></li> *}
{* <li><a href=""> </a></li>  *}



	    

      
      
      
    
      
      
      
      
     </ul>
</li>

     
     


