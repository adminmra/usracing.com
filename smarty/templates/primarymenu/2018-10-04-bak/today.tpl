<li class="dropdown">
  <a  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"  >Today's Racing<i class="fa fa-angle-down"></i></a>
  <ul class="dropdown-menu">
  {*	<li><a href="/upcoming-horse-races"  >Upcoming Horse Races</a></li>*}
    
   
    <li><a href="/odds" >Today's Entries</a></li>
   {*  <li><a href="/news/features/race-of-the-week" >Race of the Week</a></li> *}
     <li><a href="/results"  >US Racing Results</a></li> 
     <li><a href="/horse-racing-schedule" >Horse Racing Schedule</a></li>
      <li><a href="/harness-racing-schedule" >Harness Racing Schedule</a></li> 
      <li><a href="/graded-stakes-races"  > Stakes Races Schedule</a></li>
       <li><a href="/breeders-cup/challenge" >Breeders' Cup Challenge</a></li>
    
    {*   
     
    
    <li><a href="/breeders-cup/odds" >Breeders&#039; Cup Odds</a></li>
          
           <li><a href="/kentucky-derby/props" >Kentucky Derby Props</a></li>
         <li><a href="/kentucky-oaks/odds" >Kentucky Oaks Odds</a></li>
         <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a></li>
         <li><a href="/bet-on/triple-crown" >Triple Crown Odds</a></li> *}
           {*<li><a href="/casino/breeders-cup-blackjack-tournament"  >BC Blackjack Tournament</a></li>*}
     <li><a href="/racetracks"  >Racetracks</a></li>
     {* <li><a href="/past-performances" >Free Past Performances</a></li> *}
       
     <li><a href="/leaders/horses"  >Top Horses</a></li>
      <li><a href="/leaders/jockeys"  >Top Jockeys</a></li>
       <li><a href="/leaders/trainers"  >Top Trainers</a></li>
      {* <li><a href="https://www.guaranteedtipsheet.com/index.asp?afid=usr" target="_blank" rel="nofollow">Guaranteed Picks</a></li> *}
       
       
 {*       <li><a href="/kentucky-derby/prep-races" >Kentucky Derby Prep Races</a></li> *}
     
{*      <li><a href="/news/tag/pegasus-world-cup/" >Pegasus World Cup</a></li> *}
     
     
    {*
  <li><a href="/news"  >News</a></li>
       <li><a href="/horse-racing-videos"  >Horse Racing Videos</a></li>
*}
        {* <li><a href="/blog/" >US Racing Blog</a></li>
             <li><a href="/signup/?todays-racing=Signup-Today">Sign Up Today!</a></li> *}
    {*      
  
    <li><a href="/racetracks"  >Our Racetracks</a></li>
    <li><a href="/news" >Horse Racing News</a></li>
    <li><a href="/about" >About Us</a></li>
    <li><a href="/graded-stakes-races" >Races to Watch</a></li>
    <li><a href="/" title="Online Horse Betting">Online Horse Betting</a>
    <li><a href="/how-to/place-a-bet" title="How To Place A Bet" rel="nofollow">How To Place A Horse Bet</a></li>
    <li><a href="/rebates" title="Horse Raceing Rebates">Rebates</a></li>
    <li><a href="/racetracks" title="Horse Racetracks">Horse Racetracks</a></li>
    <li><a href="/famous-jockeys" title="Famous Horses, Jockeys &amp; Trainers" rel="nofollow">Famous Jockeys</a></li>
    <li><a href="/advance-deposit-wagering" title="Advance Deposit Wagering">Advance Deposit Wagering</a></li>
    <li><a href="/horse-racing-games" title="Horse Racing Games">Horse Racing Game</a></li>
    <li><a href="/haskell-stakes" title="Stakes Races">Stakes Races</a></li>
    <li><a href="/haskell-stakes" title="Haskell Stakes">Haskell Stakes</a></li>
    <li><a href="/travers-stakes" title="Travers Stakes">Travers Stakes</a></li>
    <li><a href="/santa-anita-derby" title="Santa Anita Derby">Santa Anita Derby</a></li>
    <li><a href="/arkansas-derby" title="Arkansas Derby">Arkansas Derby</a></li>
    <li><a href="/illinois-derby" title="Illinois Derby">Illinois Derby</a></li>
    <li><a href="/florida-derby" title="Florida Derby">Florida Derby</a></li>
    <li><a href="/bluegrass-stakes" title="Blue Grass Stakes">Blue Grass Stakes</a></li>
    <li><a href="/racetracks" title="Handicapping">Popular Racetracks</a></li>
    <li><a href="/belmont-park" title="">Belmont Park</a></li>
    <li><a href="/delmar" title="">Delmar</a></li>
    <li><a href="/churchill-downs" title="">Churchill Downs</a></li>
    <li><a href="/gulfstream-park" title="">Gulfstream Park</a></li>
    <li><a href="/pimlico" title="">Pimlico</a></li>
    <li><a href="/santa-anita-park" title="">Santa Anita Park</a></li>
    <li><a href="/saratoga" title="">Saratoga</a></li>
    *}
  </ul>
</li>
