{*
<!-- Breeders Cup ================================================ --> *}

<li id="collapse-bc-mobile">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0"
    data-close-others="false">Breeders&#039; Cup <i class="fa fa-angle-down"></i></a>
  {*if $BC_FRI_Switch and $BC_Switch}
  <ul class="dropdown-menu newnew" id="double">
    {elseif $BC_FRI_Switch or $BC_Switch}
    <ul class="dropdown-menu newnewtwo" id="double">
      {else}
      <ul class="dropdown-menu newnew" id="double">
        {/if*}
        <ul class="dropdown-menu newnew" id="double">
          <div class="accordion" id="bc-accordion">
            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                  <a class="btn btn-link collapsed bc-collapse-btn" data-toggle="collapse"
                    data-target="#bc-first-collapse" aria-expanded="true" aria-controls="bc-first-collapse">
                    <div class="collapse-btn-title">Bet On Breeders' Cup</div> <i class="fa fa-angle-down"></i>
                  </a>
                </h2>
              </div>

              <div id="bc-first-collapse" class="collapse" aria-labelledby="headingOne" data-parent="#bc-accordion">
                <div class="card-body">
                  <li><a href="/bet-on/breeders-cup">Bet on Breeders' Cup</a></li>
                  <li><a href="/breeders-cup">{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders'
                      Cup</a></li>
                  <li><a href="/breeders-cup/betting">Breeders&#039; Cup Betting</a></li>
                  <li><a href="/news/breeders-cup">Latest Updates</a>
                  <li><a href="/breeders-cup/odds">Breeders&#039; Cup Odds</a></li>
                 {*  <li><a href="/news/tag/breeders-cup-contenders/">Breeders&#039; Contenders</a></li> *}
                {*   <li><a href="/breeders-cup/challenge">Breeders&#039; Cup Challenge</a></li> *}
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingTwo">
                <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                  <a class="btn btn-link collapsed bc-collapse-btn" type="a" data-toggle="collapse"
                    data-target="#bc-second-collapse" aria-expanded="false" aria-controls="bc-second-collapse">
                    <div class="collapse-btn-title">Breeders' Cup Day 1</div> <i class="fa fa-angle-down"></i>
                  </a>
                </h2>
              </div>

              <div id="bc-second-collapse" class="collapse" aria-labelledby="headingTwo" data-parent="#bc-accordion">
                <div class="card-body">
                  {*if $BC_FRI_Switch*}
                  {if $BC_JUVENILE_TURF_SPRINT_Switch}
                  <li><a href="/breeders-cup/juvenile-turf-sprint">Juvenile Turf Sprint</a></li>
                  {else}
                  <li><a href="/breeders-cup/juvenile-turf-sprint/results">Juvenile Turf Sprint Results</a></li>
                  {/if}
                  {if $BC_JUVENILE_TURF_Switch}
                  <li><a href="/breeders-cup/juvenile-turf">Juvenile Turf</a></li>
                  {else}
                  <li><a href="/breeders-cup/juvenile-turf/results">Juvenile Turf Results</a></li>
                  {/if}
                  {if $BC_JUVENILE_FILLIES_Switch}
                  <li><a href="/breeders-cup/juvenile-fillies">Juvenile Fillies</a></li>
                  {else}
                  <li><a href="/breeders-cup/juvenile-fillies/results">Juvenile Fillies Results</a></li>
                  {/if}
                  {if $BC_JUVENILE_FILLIES_TURF_Switch}
                  <li><a href="/breeders-cup/juvenile-fillies-turf">Juvenile Fillies Turf</a></li>
                  {else}
                  <li><a href="/breeders-cup/juvenile-fillies-turf/results">Juvenile Fillies Turf Results</a></li>
                  {/if}
                  {if $BC_JUVENILE_Switch}
                  <li><a href="/breeders-cup/juvenile"> Juvenile </a></li>
                  {else}
                  <li><a href="/breeders-cup/juvenile/results">Juvenile Results</a></li>
                  {/if}
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                  <li class="bc-nemu-empty"><a>&nbsp;</a></li>
                  {*/if*}
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingThree">
                <h2 class="mb-0 collapse-name" style="padding-left: 0px;">
                  <a class="btn btn-link collapsed bc-collapse-btn" type="a" data-toggle="collapse"
                    data-target="#bc-third-collapse" aria-expanded="false" aria-controls="bc-third-collapse">
                    <div class="collapse-btn-title">Breeders' Cup Day 2</div> <i class="fa fa-angle-down"></i>
                  </a>
                </h2>
              </div>
              <div id="bc-third-collapse" class="collapse" aria-labelledby="headingThree" data-parent="#bc-accordion">
                <div class="card-body">
                  {if $BC_FILLY_MARE_SPRINT_Switch}
                  <li><a href="/breeders-cup/filly-mare-sprint">Filly and Mare Sprint</a></li>
                  {else}
                  <li><a href="/breeders-cup/filly-mare-sprint/results">Filly and Mare Sprint Results</a></li>
                  {/if}
                  {if $BC_TURF_SPRINT_Switch}
                  <li><a href="/breeders-cup/turf-sprint">Turf Sprint</a></li>
                  {else}
                  <li><a href="/breeders-cup/turf-sprint/results">Turf Sprint Results</a></li>
                  {/if}
                  {if $BC_DIRT_MILE_Switch}
                  <li><a href="/breeders-cup/dirt-mile">Dirt Mile</a></li>
                  {else}
                  <li><a href="/breeders-cup/dirt-mile/results">Dirt Mile Results</a></li>
                  {/if}
                  {if $BC_FILLY_MARE_TURF_Switch}
                  <li><a href="/breeders-cup/filly-mare-turf">Filly and Mare Turf</a></li>
                  {else}
                  <li><a href="/breeders-cup/filly-mare-turf/results">Filly and Mare Results</a></li>
                  {/if}
                  {if $BC_SPRINT_Switch}
                  <li><a href="/breeders-cup/sprint">Sprint</a></li>
                  {else}
                  <li><a href="/breeders-cup/sprint/results">Sprint Results</a></li>
                  {/if}
                  {if $BC_MILE_Switch}
                  <li><a href="/breeders-cup/mile">Mile</a></li>
                  {else}
                  <li><a href="/breeders-cup/mile/results">Mile Results</a></li>
                  {/if}
                  {if $BC_DISTAFF_Switch}
                  <li><a href="/breeders-cup/distaff"> Distaff </a></li>
                  {else}
                  <li><a href="/breeders-cup/distaff/results">Distaff Results</a></li>
                  {/if}
                  {if $BC_TURF_Switch}
                  <li><a class="turfBC" href="/breeders-cup/turf">Turf </a></li>
                  {else}
                  <li><a href="/breeders-cup/turf/results">Turf Results</a></li>
                  {/if}
                  {if $BC_CLASSIC_Switch}
                  <li><a href="/breeders-cup/classic">Classic</a></li>
                  {else}
                  <li><a href="/breeders-cup/classic/results">Classic Results</a></li>
                  {/if}

                </div>
              </div>
            </div>
          </div>

        </ul>
</li>

<li id="collapse-bc-desktop" class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0"
    data-close-others="false">Breeders&#039; Cup <i class="fa fa-angle-down"></i></a>
  {* if $BC_FRI_Switch and $BC_Switch}
  <ul class="dropdown-menu newnew" id="double">
    {elseif $BC_FRI_Switch or $BC_Switch}
    <ul class="dropdown-menu newnewtwo" id="double">
      {else}
      <ul class="dropdown-menu newnew" id="double">
        {/if *}
        <ul class="dropdown-menu newnew" id="double" style="-moz-column-count:2;-webkit-column-count:2;column-count:2; margin-left:-68%;">
          {* <div class="bc-menu-title bcc">Breeders' Cup</div>
          <li><a href="/bet-on/breeders-cup">Bet on Breeders' Cup</a></li>
          {if $dateNow <= '2019-11-02 20:44:00' } <li><a href="/breeders-cup">{include
              file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup</a></li>
{else}
<li><a href="/breeders-cup">{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup</a></li>
{/if}
<li><a href="/breeders-cup/betting">Breeders&#039; Cup Betting</a></li>
<li><a href="/news/breeders-cup">Latest Updates</a>
<li><a href="/breeders-cup/odds">Breeders&#039; Cup Odds</a></li> *}
{*
<li><a href="/news/tag/breeders-cup-contenders/">Breeders&#039; Contenders</a></li>
<li class="bc-nemu-last"><a href="/breeders-cup/challenge">Breeders&#039; Cup Challenge</a></li>
*}
{* <li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li> *}

{* <li class="bc-nemu-empty"><a>&nbsp;</a></li> *}


<div class="bc-menu-title fri">Friday Nov. 5th</div>
{if $BC_JUVENILE_TURF_SPRINT_Switch}
<li><a href="/breeders-cup/juvenile-turf-sprint">Juvenile Turf Sprint</a></li>
{else}
<li><a href="/breeders-cup/juvenile-turf-sprint/results">Juvenile Turf Sprint Results</a></li>
{/if}
{if $BC_JUVENILE_TURF_Switch}
<li><a href="/breeders-cup/juvenile-turf">Juvenile Turf</a></li>
{else}
<li><a href="/breeders-cup/juvenile-turf/results">Juvenile Turf Results</a></li>
{/if}
{if $BC_JUVENILE_FILLIES_Switch}
<li><a href="/breeders-cup/juvenile-fillies">Juvenile Fillies</a></li>
{else}
<li><a href="/breeders-cup/juvenile-fillies/results">Juvenile Fillies Results</a></li>
{/if}
{if $BC_JUVENILE_FILLIES_TURF_Switch}
<li><a href="/breeders-cup/juvenile-fillies-turf">Juvenile Fillies Turf</a></li>
{else}
<li><a href="/breeders-cup/juvenile-fillies-turf/results">Juvenile Fillies Turf Results</a></li>
{/if}
{if $BC_JUVENILE_Switch}
<li class="bc-nemu-last"><a href="/breeders-cup/juvenile"> Juvenile </a></li>
{else}
<li class="bc-nemu-last"><a href="/breeders-cup/juvenile/results">Juvenile Results</a></li>
{/if}

<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>
<li class="bc-nemu-empty"><a>&nbsp;</a></li>

<div class="bc-menu-title sat" style="height:38.5px;">Saturday Nov. 6th</div>
{if $BC_FILLY_MARE_SPRINT_Switch}
<li><a href="/breeders-cup/filly-mare-sprint">Filly and Mare Sprint</a></li>
{else}
<li><a href="/breeders-cup/filly-mare-sprint/results">Filly and Mare Sprint Results</a></li>
{/if}
{if $BC_TURF_SPRINT_Switch}
<li><a href="/breeders-cup/turf-sprint">Turf Sprint</a></li>
{else}
<li><a href="/breeders-cup/turf-sprint/results">Turf Sprint Results</a></li>
{/if}
{if $BC_DIRT_MILE_Switch}
<li><a href="/breeders-cup/dirt-mile">Dirt Mile</a></li>
{else}
<li><a href="/breeders-cup/dirt-mile/results">Dirt Mile Results</a></li>
{/if}
{if $BC_FILLY_MARE_TURF_Switch}
<li><a href="/breeders-cup/filly-mare-turf">Filly and Mare Turf</a></li>
{else}
<li><a href="/breeders-cup/filly-mare-turf/results">Filly and Mare Results</a></li>
{/if}
{if $BC_SPRINT_Switch}
<li><a href="/breeders-cup/sprint">Sprint</a></li>
{else}
<li><a href="/breeders-cup/sprint/results">Sprint Results</a></li>
{/if}
{if $BC_MILE_Switch}
<li><a href="/breeders-cup/mile">Mile</a></li>
{else}
<li><a href="/breeders-cup/mile/results">Mile Results</a></li>
{/if}
{if $BC_DISTAFF_Switch}
<li><a href="/breeders-cup/distaff"> Distaff </a></li>
{else}
<li><a href="/breeders-cup/distaff/results">Distaff Results</a></li>
{/if}
{if $BC_TURF_Switch}
<li><a class="turfBC" href="/breeders-cup/turf">Turf </a></li>
{else}
<li><a href="/breeders-cup/turf/results">Turf Results</a></li>
{/if}
{if $BC_CLASSIC_Switch}
<li><a href="/breeders-cup/classic">Classic</a></li>
{else}
<li><a href="/breeders-cup/classic/results">Classic Results</a></li>
{/if}
</ul>
</li>