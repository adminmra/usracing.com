<!--Secondary Menu-->

<div class="preLoadimages"><img src="/themes/images/searchbox-bg.gif" alt="Search box background" title="Search box background" width="19" height="20"/><img src="/themes/images/searchbox-button.gif" alt="Search box button" title="Search box button" width="21" height="20"/><img src="/themes/images/searchbox-button-hover.gif" alt="Search box button hover" title="Search box button hover" width="21" height="20"/></div>
{literal} 
<script type="text/javascript">
    $(function () {
        $("#searchkeyword").keypress(function (e) {
            if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                //$("#search_button").click();
                submit_search();
                return false;
            }
            else {
                return true;
            }
        });
    });
</script>{/literal}
<div id="submenubar">
  <div class="subindent"></div>
  <div id="submenu" class="hlist"> 
    <!-- secondary navigation -->
    <ul class="links">
      <li ><a href="/racetracks" title="US Racetracks">Racetracks</a></li>
      <li class="first"><a href="/schedule" title="Horse Racing Schedule">Horse Racing Schedule</a></li>
      <li class="first"><a href="/graded-stakes-races" title="Graded Stakes Schedule" rel="nofollow">Graded Stakes Races</a></li>
      {*
      <li ><a href="/today" title="Today&#039;s Tracks">Today&#039;s Tracks</a></li>
      *}
      <li ><a href="/results" title="Horse Racing Results" rel="nofollow">Horse Racing Results</a></li>
      <li ><a href="/news" title="Horse Racing News" rel="nofollow">Latest Racing News</a></li>
      <li ><a href="/about" title="About US" rel="nofollow">About Us</a></li>
      <li ><a href="/contact" title="Contact US" rel="nofollow">Contact US</a></li>
      {*
      <li ><a href="/live-video" title="Watch live horse racing video.">Live Video</a></li>
      <li ><a href="/mobile-betting" title="Mobile Horse Betting">Mobile Betting</a></li>
      <li ><a href="/handicapping" title="Horse Racing Handicapping">Handicapping</a></li>
      <li ><a href="/about-us" title="About US Racing">About US Racing</a></li>
      <!-- {literal}<script language="javascript">document.write((logged_in()?'':'<li class="menu-592"><a href="/bestracebook" title="About US Racing">Why Us?</a></li>'));</script>{/literal}
<li class="menu-593 last"><a href="/support" title="Customer Support">Support</a></li>
 -->*}
    </ul>
  </div>
  {*
  <div class="social-box">
    <div class="follow-submenu"><img width="56" height="26" border="0" usemap="#MapFollow" src="/themes/images/follow_us.png" alt="Follow us"></div>
    <map id="MapFollow" name="MapFollow">
      <area alt="Add US Racing to my Favorites / Bookmarks" href="javascript:bookmarksite('US Racing', 'http://www.usracing.com')" coords="-9,0,14,16" shape="rect">
      <area target="_blank" title="Follow US Racing on Facebook" alt="Follow us on Facebook" href="http://www.facebook.com/betusracing" coords="0,0,26,26" shape="rect">
      <area target="_blank" title="Follow US Racing on Twitter" alt="Follow us on Twitter" href="http://twitter.com/betusracing" coords="30,0,56,26" shape="rect">
    </map>
    <div class="searchbox">
      <form method="get" id="searchform" name="searchform" action="">
        <fieldset class="search">
          <input type="text" class="box" id="searchkeyword"/>
          <button class="btn" id="search_button" title="Submit Search" onClick="return submit_search();">Search</button>
        </fieldset>
      </form>
    </div>
  </div>
  end:social-box *} </div>
