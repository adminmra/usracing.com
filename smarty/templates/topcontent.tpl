<!-- Top -->
<div id="top" class="top">
  <div class="container">
    <div class="navbar-header pull-left">
 {*
     <!--
     <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
     <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
     <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->
 *}
 {* <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:0px 0px 0px -10px; padding: 10px; padding-bottom: 11px;"></span><i class="glyphicon glyphicon-remove"></i></a> *}
 <a id="or_" class="navbar-toggle collapsed" onclick="openNav()">
   <span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:0px 0px 0px -10px; padding: 10px; padding-bottom: 11px;"></span>
   <i class="glyphicon glyphicon-remove"></i>
 </a>
 </div>
 <a class="logo logo-lrg" href="/" ><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting"></a>
 <a class="logo logo-sm" href="/" ><img src="/img/usracing-sm.png" alt="Online Hose Betting" width="128" height="24"></a>
   <!--{*include file='topright.tpl'*}-->
   {include file='topmessage.tpl'}
   {include file='topmenu.tpl'}
 {*include file='topmenu.orig.mb.tpl'*}
  {*include file='topcountdown.tpl'*}
 </div>
 </div><!--/top-->
 <!-- Nav -->
 <div id="nav">
   <a id="navigation" name="navigation"></a>
   <div class="navbar navbar-default" role="navigation">
   <div class="container">
 {*
   <!-- Toggle NAV 
   <div class="navbar-header">
     <!--
     <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
     <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
     <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
     <a id="or" class="navbar-toggle collapsed"><span>EXPLORE</span><i class="glyphicon glyphicon-remove"></i></a>
   </div>-->
 *}
     <!-- Nav Itms -->
     <div class="collapse navbar-collapse navbar-responsive-collapse">
     <ul class="nav navbar-nav nav-justified">
     <span class="closeNavIcon" onclick="closeNav()" style="display: none">
       <i class="fa fa-times" style="font-size: 24px;"></i>
     </span>
     {if $smarty.server.REQUEST_URI == '/demo-auto'}
     {include file='primarymenu-auto.tpl'}
     {else}
     {include file='primarymenu.tpl'}
     {/if}
   </ul>
   <div class="closeNav" onclick="closeNav();"></div>
    </div><!-- /navbar-collapse -->
   </div>
  </div>
 </div> <!--/#nav-->
 
 
 {literal}
  <script type="rocketlazyloadscript" data-rocket-type="text/javascript">
   function resizeCloseNav() {
       var width = window.screen.width - 260 + "px";
       document.querySelectorAll(".closeNav")[1].style.width = width;
     }
 
     function openNav() {
       document.querySelector("#nav-side").classList.remove("hideNav");
       document.querySelector("#nav-side").classList.add("displayNav");
       document.querySelector("body").style.overflow = "hidden";
       document.querySelectorAll(".closeNavIcon")[1].style.display = "block";
     }
 
     function closeNav() {
       document.querySelector("#nav-side").classList.add("hideNav");
       document.querySelectorAll(".navbar-nav")[1].classList.add("hideBar");
       document.querySelectorAll(".closeNavIcon")[1].style.display = "none";
       document.querySelector("body").style.overflow = "auto";
       setTimeout(() => {
         document.querySelector("#nav-side").classList.remove("displayNav");
         document.querySelectorAll(".navbar-nav")[1].classList.remove("hideBar");
       }, 500);
     }
 </script>
 {/literal}