<div id="menubar">
<div class="indent"></div>
<div id="menu" class="hlist">
<ul class="sf-menu">

<li class="menu-494 depth-0 first">
<a href="/racebook" title="Racebook">RACEBOOK</a>
<ul>
<div class="mega-menu">

<div class="advertisement group-tids-10" id="ad-racebook"><!-- Ad: Racebook -->
<script language="javascript">document.write('<a href="/'+(logged_in()?'sportsbook':'kentuckyderby-odds')+'" title="Kentucky Derby 2011 - Bet Now!"><img src="/themes/images/megamenu-racebook-kd.jpg" alt="Racebook" /></a>');</script>
</div>

<div class="inner">
<div class="menu-506 depth-1 first">
<a href="/racebook" title="Racebook">Racebook</a>
<div class="menu-508 depth-2 first"><a href="/howto/placeabet" title="How To Place A Bet">How To Place A Bet</a></div>
<div class="menu-509 depth-2"><a href="/rebates" title="Rebates">Rebates</a></div>
<div class="menu-510 depth-2"><a href="/racetracks" title="Horse Racetracks">Horse Racetracks</a></div>
<div class="menu-903 depth-2 lastbutone"><a href="/horseracing/bettinglimits" title="Horse Racing Rules">Horse Racing Rules</a></div>
<div class="menu-643 depth-2"><a href="/famousjockeys" title="Famous Horses, Jockeys &amp; Trainers">Connections</a></div>
<div class="menu-341 depth-2 last"><a href="/beton/race-of-the-week" title="Bet on the Race of the Week">Race of the Week</a></div>
</div>

<div class="menu-517 depth-1" style="width:27%; padding-right:0;">
<a href="/triplecrown" title="Triple Crown">Triple Crown</a>
<div class="menu-548 depth-2 first"><a href="/triplecrown/triple-crown-nominations" title="2011 Triple Crown Nominations">Triple Crown Nominations List</a></div>
<div class="menu-518 depth-2"><a href="/kentuckyderby" title="Kentucky Derby">Kentucky Derby</a></div>
<div class="menu-529 depth-2"><script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'kentuckyderby-odds')+'" title="Kentucky Derby 2011 - Odds to Win!">Kentucky Derby Odds</a>');</script></div>
<div class="menu-512 depth-2"><a href="/kentuckyderby-prepraces" title="2011 Kentucky Derby Prep Races">Kentucky Derby Prep Races</a></div>
<div class="menu-540 depth-2"><a href="/kentuckyderby-gradedearnings" title="2011 Kentucky Derby Graded Earnings">Kentucky Derby Graded Earnings</a></div>
<div class="menu-513 depth-2"><a href="/kentuckyderby-futurewager" title="2011 Kentucky Derby Future Wagers">Kentucky Derby Future Wager</a></div>
<div class="menu-516 depth-2"><a href="/triplecrown/freebet" title="Triple Crown FREE Bet Extravaganza!">Kentucky Derby FREE Bet</a></div>
<div class="menu-519 depth-2"><a href="/preakness-stakes" title="Preakness Stakes">Preakness Stakes</a></div>
<div class="menu-520 depth-2 last"><a href="/belmontstakes" title="Belmont Stakes">Belmont Stakes</a></div>
</div>

<div class="menu-521 depth-1">
<a href="/haskellstakes" title="Stakes Races">Stakes Races</a>
<div class="menu-523 depth-2 first"><a href="/haskellstakes" title="Haskell Stakes">Haskell Stakes</a></div>
<div class="menu-524 depth-2"><a href="/traversstakes" title="Travers Stakes">Travers Stakes</a></div>
<div class="menu-644 depth-2"><a href="/santaanitaderby" title="Santa Anita Derby">Santa Anita Derby</a></div>
<div class="menu-645 depth-2"><a href="/arkansasderby" title="Arkansas Derby">Arkansas Derby</a></div>
<div class="menu-646 depth-2"><a href="/illinoisderby" title="Illinois Derby">Illinois Derby</a></div>
<div class="menu-647 depth-2 lastbutone"><a href="/floridaderby" title="Florida Derby">Florida Derby</a></div>
<div class="menu-648 depth-2 last"><a href="/bluegrassstakes" title="Blue Grass Stakes">Blue Grass Stakes</a></div>
</div>

<div class="menu-784 depth-1 lastbutone">
<a href="/royalascot" title="Special Races">SPECIAL RACES</a>
<div class="menu-525 depth-2 first"><a href="/royalascot" title="Ascot">Ascot</a></div>
<div class="menu-649 depth-2"><a href="/breederscup" title="Breeders&#039; Cup">Breeders&#039; Cup</a></div>
<div class="menu-650 depth-2"><a href="/dubaiworldcup" title="Dubai World Cup">Dubai World Cup</a></div>
<div class="menu-651 depth-2"><a href="/melbournecup" title="Melbourne Cup">Melbourne Cup</a></div>
<div class="menu-652 depth-2"><a href="/kentuckyoaks" title="Kentucky Oaks">Kentucky Oaks</a></div>
<div class="menu-653 depth-2 lastbutone"><a href="/hongkongcup" title="Hong Kong Cup">Hong Kong Cup</a></div>
<div class="menu-654 depth-2 last"><a href="/prixdelarcdetriomphe" title="Pris De L&#039;Arc De Triomphe">Pris De L&#039;Arc De Triomphe</a></div>
</div>

<div class="menu-526 depth-1 last">
<a href="/handicapping-horses" title="Handicapping">Handicapping</a>
<div class="menu-527 depth-2 first"><a href="/bethorses/beginnertips" title="Beginner Tips">Beginner Tips</a></div>
<div class="menu-655 depth-2"><a href="/bethorses/moneymanagement" title="Money Management">Money Management</a></div>
<div class="menu-656 depth-2"><a href="/howto/readpastperformances" title="Past Performances">Past Performances</a></div>
<div class="menu-657 depth-2 lastbutone"><a href="/horseracingterms" title="Racing Terms &amp; Glossary">Racing Terms &amp; Glossary</a></div>
<div class="menu-658 depth-2 last"><a href="/howto/readthedailyracingform" title="Daily Racing Form">Daily Racing Form</a></div>
</div>

</div></div></ul></li><!-- end: RACEBOOK -->


<li class="menu-918 depth-0">
<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">HORSE FUTURES</a>');</script>
<ul>
<div class="mega-menu" style="width:952px;">

<div class="advertisement group-tids-13" id="ad-horsefutures">
<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures"><img src="/themes/images/megamenu-horsefutures-kd.jpg" alt="Horse Futures - Bet Now!" /></a>');</script>
</div><!-- Ad: Horse Futures -->

<div class="inner" style="width:790px;">
<div class="menu-959 depth-1 first">
<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Bet on Horse Matchups Now!">BET NOW!</a>');</script>

{include file="includes/ahr_block_top-horse-future.tpl"}
</div>

<div class="menu-998 depth-1" style="width:33%;">
<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">Propositions</a>');</script>
<div class="menu-992 depth-2 first">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}Will a Horse Win The 2011 Triple Crown?</a></div>
<div class="menu-992 depth-2">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}Will Uncle Mo Finish Top Three Positons in Kentucky Derby?</a></div>
<div class="menu-992 depth-2">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}Kentucky Derby - Official Winning Time?</a></div>
<div class="menu-992 depth-2">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}Will Uncle Mo win Kentucky Derby?</a></div>
<div class="menu-992 depth-2">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}Will the Derby winner win the Belmont?</a></div>
<div class="menu-992 depth-2">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">');</script>{/literal}$5 Free Bet - 2011 Kentucky Derby - Finishing Time of Winning Horse</a></div>
<div class="menu-992 depth-2 last">{literal}<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Racing Propositions" style="color:#FFBB00;">');</script>{/literal}More Horse Betting Propositions...</a></div>
</div>

<div class="menu-988 depth-1 last" style="width:20%;">
<script language="javascript">document.write('<a '+(logged_in()?'href="/sportsbook"':'href="/horsefutures"')+' title="Horse Futures">Horse Racing</a>');</script>
<div class="menu-992 depth-2 first"><a href="/kentuckyderby-contenders" title="Derby Contenders">2011 Kentucky Derby Contenders</a></div>
<div class="menu-992 depth-2"><a href="/kentuckyderby-odds" title="Derby Odds">2011 Kentucky Derby Odds To Win</a></div>
<div class="menu-989 depth-2 last"><a href="/promotions/horseracing" title="Horse Racing Promotions">Horse Racing Promotions</a></div>
</div>

</div>
</div>
</ul>
</li><!-- end: Horse Futures -->

<li class="menu-495 depth-0"><a href="/virtualderby" title="Virtual Derby">VIRTUAL DERBY</a></li>

<li class="menu-496 depth-0"><a href="/casino" title="Casino">CASINO</a></li>

<li class="menu-497 depth-0">
<a href="/sportsbook" title="Sportsbook">SPORTSBOOK</a>
<ul>
<div class="mega-menu">
<div class="advertisement group-tids-13" id="ad-sportsbook">
<a href="/sportsbook" title="Bet on sports - Bet Now!"><img src="/themes/images/megamenu-basketball.jpg" alt="Sportsbook - Bet Now!" /></a>
</div><!-- Ad: Sportsbook -->

<div class="inner">
<div class="menu-659 depth-1 first">
<a href="/sportsbook" title="Sports Lines">Sports Lines</a>
<div class="menu-692 depth-2 first"><a href="/beton/football/?megamenu=true" title="NFL Bets">NFL</a></div>
<div class="menu-693 depth-2"><a href="/beton/college-football/?megamenu=true" title="NCAA College Football Bets">College Football</a></div>
<div class="menu-694 depth-2"><a href="/beton/basketball/?megamenu=true" title="NBA Basketball Bets">NBA</a></div>
<div class="menu-695 depth-2"><a href="/beton/collegebasketball/?megamenu=true" title="NCAA Basketball Bets">College Basketball</a></div>
<div class="menu-696 depth-2"><a href="/beton/hockey/?megamenu=true" title="NHL Hockey Bets">Hockey</a></div>
<div class="menu-699 depth-2"><a href="/beton/baseball/?megamenu=true" title="MLB Baseball Bets">Baseball</a></div>
<div class="menu-697 depth-2"><a href="/beton/golf/?megamenu=true" title="PGA Golf Bets">Golf</a></div>
<div class="menu-698 depth-2"><a href="/beton/boxing/?megamenu=true" title="UFC, MMA and Boxing Bets">MMA / Boxing</a></div>
<div class="menu-701 depth-2"><a href="/beton/soccer/?megamenu=true" title="European Football | Soccer Bets">Soccer</a></div>
<div class="menu-700 depth-2 lastbutone"><a href="/beton/oscars/?megamenu=true" title="Misc. Bets">Other</a></div>
<div class="menu-867 depth-2 last"><a href="/sportsbook">More Sports</a></div>
</div>

<div class="menu-702 depth-1 lastbutone">
<a href="/howto/betonsports" title="Sports Betting Rules">Sports Betting Rules</a>
<div class="menu-715 depth-2 first"><a href="/flash/howtobetonsports" title="How to Place a Sports Bet">How To Place A Sports Bet</a></div>
<div class="menu-706 depth-2"><a href="/sportsbetting/datatransmission" title="Data Transmission">Data Transmission</a></div>
<div class="menu-707 depth-2"><a href="/sportsbetting/propsfutures" title="Sports Betting Props and Futures">Props &amp; Futures</a></div>
<div class="menu-708 depth-2"><a href="/sportsbetting/halfpricesales" title="Half Price Betting Sales">Half Price Sales</a></div>
<div class="menu-709 depth-2"><a href="/sportsbetting/oddslimits" title="Sports Betting Odds and Limits">Odds &amp; Limits</a></div>
<div class="menu-710 depth-2"><a href="/sportsbetting/buypoints" title="Buy Points to Better Your Odds">Buy Points</a></div>
<div class="menu-711 depth-2"><a href="/sportsbetting/teasers" title="US Racing Betting Teasers">Betting Teasers | US Racing</a></div>
<div class="menu-712 depth-2"><a href="/sportsbetting/parlayscombos" title="Sports Betting Parlays and Combos">Parlays &amp; Combos</a></div>
<div class="menu-713 depth-2"><a href="/sportsbetting/roundrobins" title="Round Robin Sports Betting">Round Robin Betting | US Racing</a></div>
<div class="menu-714 depth-2 lastbutone"><a href="/sportsbetting/progressiveparlays" title="Progressive Sports Betting Parlays">Progressive Parlays</a></div>
<div class="menu-716 depth-2 last"><a href="/howto/betonsports" title="View US Racing Rules">VIEW ALL RULES</a></div>
</div>

<div class="menu-788 depth-1 last">
<a href="/promotions" title="sportsbook promotions, US Racing promotions,">Sportsbook Promotions</a>
<div class="menu-789 depth-2 first"><a href="/promotions/sportsbook/football" title="NFL and NCAA Football Promotions">NFL &amp; NCAA Football</a></div>
<div class="menu-792 depth-2"><a href="/promotions/sportsbook/mlb" title="MLB Baseball Betting Promotions">MLB</a></div>
<div class="menu-790 depth-2"><a href="/promotions/sportsbook/nba" title="NBA Basketball Betting Promotions">NBA</a></div>
<div class="menu-791 depth-2"><a href="/promotions/sportsbook/nhl" title="NHL Hockey Promotions">NHL</a></div>
<div class="menu-793 depth-2"><a href="/promotions/sportsbook/sports-cashback" title="5% Cash Back on Sports Bets">5% Sports Cash Back</a></div>
<div class="menu-794 depth-2 lastbutone"><a href="/promotions/deposit-specials" title="Deposit Specials ">Deposit Specials</a></div>
<div class="menu-795 depth-2 last"><a href="/promotions/horseracing" title="Horse Racing Promotions">Horse Racing</a></div>
</div>

</div></div></ul></li><!-- end: SPORTSBOOK -->

<li class="menu-498 depth-0"><a href="/poker" title="Poker">POKER</a></li>

<li class="menu-500 depth-0"><a href="/promotions" title="Promotions">PROMOS</a></li>

<li class="menu-499 depth-0"><script language="javascript">document.write('<a href="/clubhouse" title="Clubhouse"'+ (logged_in()?'class="active"':'')+'>CLUBHOUSE</a>');</script></li>
<script language="javascript">(logged_in())?document.write('<li class="menu-942 depth-0 lastbutone"><a href="javascript:void(0);" onclick="open_banking();" title="Deposit">DEPOSIT</a></li>'):document.write('<li class="menu-501 depth-0 last"><a href="/join" title="Sign Up">JOIN</a></li>');</script>

</ul>
</div>
</div>
