<div id="menubar">
    <div class="indent"></div>
    <div id="menu" class="hlist">
        <ul class="sf-menu">
            <li class="menu-494 depth-0 first">
                <a href="/racebook" title="Horse Racing">Horse Racing</a>
                <ul>
                    <div class="mega-menu">
                        <div class="advertisement group-tids-10" id="ad-racebook"><!-- Ad: Racebook -->
                            <script language="javascript">document.write('<a href="/' + (logged_in() ? 'racebook' : 'racebook') + '" title="Horse Racing"><img src="/themes/images/megamenu-horseracing.jpg" alt="Horse Racing" /></a>');</script>
                        </div>
                        <div class="inner">
                            <div class="menu-506 depth-1 first">
                                <a href="/racebook" title="Horse Racing">Horse Racing</a>
                                <!-- <div class="menu-508 depth-2 first"><a href="/howto/placeabet" title="How To Place A Bet">How To Place A Bet</a></div>
                                <div class="menu-509 depth-2"><a href="/rebates" title="Rebates">Rebates</a></div>
                                 -->
                                <div class="menu-510 depth-2 first"><a href="/racetracks" title="Horse Racetracks">Horse Racetracks</a></div>
                                <!-- <div class="menu-903 depth-2 lastbutone"><a href="/horseracing/bettinglimits" title="Horse Racing Rules">Horse Racing Rules</a></div>
                                 -->
                                <div class="menu-643 depth-2"><a href="/famousjockeys" title="Famous Horses, Jockeys &amp; Trainers">Connections</a></div>
                                <!-- <div class="menu-341 depth-2 last"><a href="/beton/race-of-the-week" title="Bet on the Race of the Week">Race of the Week</a></div>
                                 --></div>
                            <div class="menu-517 depth-1" style="width:25%; padding-right:0;">
                                <div class="cat">Kentucky Derby</div>
                                <div class="menu-218 depth-2 first"><a href="/kentuckyderby" title="Kentucky Derby 2012">Kentucky Derby</a></div>
                                <div class="menu-229 depth-2">
                                    <script language="javascript">document.write('<a href="/' + ((logged_in()) ? 'sportsbook' : 'kentuckyderby-odds') + '" title="Kentucky Derby Odds">Kentucky Derby Odds</a>');</script>
                                </div>
                                <div class="menu-212 depth-2"><a href="/kentuckyderby-prepraces" title="Kentucky Derby Prep Races">Kentucky Derby Prep Races</a></div>
                                <div class="menu-212 depth-2"><a href="/kentuckyderby-contenders" title="Kentucky Derby Contenders">Kentucky Derby Contenders</a></div>
                                <div class="menu-240 depth-2"><a href="/kentuckyderby-gradedearnings" title="Kentucky Derby Graded Earnings">Kentucky Derby Graded Earnings</a></div>
                                <div class="menu-213 depth-2 last"><a href="/kentuckyderby-futurewager" title="Kentucky Derby Future Wagers">Kentucky Derby Future Wager</a></div>
                            </div>
                            <div class="menu-521 depth-1" style="width:20%; padding-right:0;">
                                <div class="cat">Preakness Stakes</div>
                                <div class="menu-523 depth-2 first"><a href="/preakness-stakes" title="Preakness Stakes 2012">Preakness Stakes</a></div>
                                <div class="menu-524 depth-2"><a href="/preaknessstakes-betting" title="Preakness Stakes Betting">Preakness Betting</a></div>
                                <div class="menu-644 depth-2"><a href="/preaknessstakes-contenders" title="Preakness Stakes Contenders">Preakness Contenders</a></div>
                                <div class="menu-645 depth-2"><a href="/preaknessstakes-odds" title="Preakness Stakes Odds">Preakness Odds</a></div>
                                <div class="menu-646 depth-2"><a href="/stakes?name=Preakness_Stakes" title="Preakness Stakes Results">Preakness Results</a></div>
                                <div class="menu-647 depth-2 last"><a href="/preaknessstakes-pastwinners" title="Preakness Stakes Past Winners">Preakness Past Winners</a></div>
                            </div>
                            <div class="menu-784 depth-1" style="width:18%; padding-right:0;">
                                <div class="cat">Belmont Stakes</div>
                                <div class="menu-525 depth-2 first"><a href="/belmontstakes" title="2011 Belmont Stakes">Belmont Stakes</a></div>
                                <div class="menu-649 depth-2"><a href="/belmontstakes-betting" title="2011 Belmont Stakes Betting">Belmont Betting</a></div>
                                <div class="menu-650 depth-2"><a href="/belmontstakes-contenders" title="Belmont Stakes Contenders">Belmont Contenders</a></div>
                                <div class="menu-651 depth-2"><a href="/belmontstakes-odds" title="Belmont Stakes Odds">Belmont Odds</a></div>
                                <div class="menu-652 depth-2"><a href="/stakes?name=Belmont_stakes" title="Belmont Stakes Results">Belmont Results</a></div>
                                <div class="menu-653 depth-2"><a href="/belmontstakes-pastwinners" title="Belmont Stakes Winners">Belmont Past Winners</a></div>
                                <div class="menu-248 depth-2 last"><a href="/triplecrown/triple-crown-nominations" title="2011 Triple Crown Nominations">Triple Crown Nominations List</a></div>
                            </div>
                            <div class="menu-521 depth-1 last">
                                <div class="cat">Stakes Races</div>
                                <div class="menu-523 depth-2 first"><a href="/kentuckyoaks" title="Kentucky Oaks Betting">Kentucky Oaks</a></div>
                                <div class="menu-647 depth-2"><a href="/floridaderby" title="Florida Derby">Florida Derby</a></div>
                                <div class="menu-646 depth-2"><a href="/illinoisderby" title="Illinois Derby">Illinois Derby</a></div>
                                <div class="menu-644 depth-2"><a href="/santaanitaderby" title="Santa Anita Derby">Santa Anita Derby</a></div>
                                <div class="menu-644 depth-2"><a href="/stakes?name=Wood_Memorial_Stakes" title="Wood Memorial Stakes">Wood Memorial Stakes</a></div>
                                <div class="menu-644 depth-2"><a href="/stakes?name=Arkansas_Derby" title="Arkansas Derby">Arkansas Derby</a></div>
                                <div class="menu-648 depth-2"><a href="/bluegrassstakes" title="Blue Grass Stakes">Blue Grass Stakes</a></div>
                                <div class="menu-648 depth-2 last"><a href="/stakes?name=Coolmore_Lexington_Stakes" title="Lexington Stakes">Lexington Stakes</a></div>
                            </div>
                        </div>
                    </div>
                </ul>
            </li>
            <!-- end: RACEBOOK -->
        </ul>
    </div>
</div>
