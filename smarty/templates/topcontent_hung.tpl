<!-- Top -->
<div id="top" class="top">
 <div class="container">
	 <div class="navbar-header pull-left">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>-->

    <a id="or" class="navbar-toggle collapsed"><span class="fa fa-bars" aria-hidden="true" style="font-size: 29px; margin:2px 0px 0px -10px; padding: 10px;"></span><i class="glyphicon glyphicon-remove"></i></a>
	
	
	</div>

  <a class="logo logo-lrg" href="/" ><img id="logo-header" src="/img/usracing.png" alt="Online Horse Betting"></a>
 <a class="logo logo-sm " href="/" ><img src="/img/usracing-sm.png" alt="Online Hose Betting"></a>
  <!--{*include file='topright.tpl'*}-->
  {include file='topmessage.tpl'}
  {include file='topmenu.tpl'}
{*include file='topmenu.orig.mb.tpl'*}
  {*include file='topcountdown.tpl'*}


 </div>
</div><!--/top-->

<!-- Nav -->
<div id="nav">
	<a id="navigation" name="navigation"></a>
	<div class="navbar navbar-default" role="navigation">
	<div class="container">


	<!-- Toggle NAV 
	<div class="navbar-header">
		<!--
    <a class="navbar-home" href="/"><i class="fa fa-home"></i></a>
    <a class="btn btn-sm btn-default phoneToogle collapsed" id="phoneBtn" data-toggle="collapse" data-target=".phone-responsive-collapse" rel="nofollow"> <i class="glyphicon glyphicon-earphone"></i> </a>
    <a class="btn btn-sm btn-default" id="helpBtn" href="/support"> <i class="glyphicon glyphicon-question-sign"></i> </a>
    <a id="or" class="navbar-toggle collapsed"><span>EXPLORE</span><i class="glyphicon glyphicon-remove"></i></a>
	</div>-->

    <!-- Nav Itms -->
    <div class="collapse navbar-collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav nav-justified">
    {include file='primarymenu_hung.tpl'}
	</ul>
   </div><!-- /navbar-collapse -->
  </div>
 </div>
</div> <!--/#nav-->


