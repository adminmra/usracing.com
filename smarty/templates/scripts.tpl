<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
<meta name="theme-color" content="#1571ba">
<link rel="manifest" href="/manifest.json">
<link rel='dns-prefetch' href='//www.usracing.com' /> 
<link rel='dns-prefetch' href='//fonts.googleapis.com' /> 
<link rel="preconnect" href="//fonts.gstatic.com/" crossorigin>
<link rel="preconnect" href="//code.jquery.com" crossorigin>
<link rel="preconnect" href="//maxcdn.bootstrapcdn.com" crossorigin>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
{* <link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css"> *}
<link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
{* <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css"> *}
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel='preload' as='style' href='https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap'>
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap" rel="stylesheet">
{php}
 echo '<style type="text/css" >';
//echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/swiper.min.css"); 
//echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/slider.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/rs_v5.min.css"); 

echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/style.v2.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/app.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-ps.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-bs.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/index-becca.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/sprites.min.css"); 
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/css/as-seen-on-new-tpl-min.css"); 
echo '</style>';
{/php}
{*<link rel="stylesheet" type="text/css" href="/assets/css/style.min.css?v2.4.1" > *}
{* <link rel="stylesheet" type="text/css" href="/assets/css/custom_me.css"> *}
{* <link rel="stylesheet" type="text/css" href="/assets/css/app.min.css?v=2.4" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" type="text/css" href="/assets/css/sprites.min.css?v=1.0" media="none" onload="if(media!='all')media='all'">
<link rel="stylesheet" href="/assets/css/as-seen-on-new-tpl-min.css?v=1.3" media="none" onload="if(media!='all')media='all'"> *}
{* <script src="https://cdn.pushassist.com/account/assets/psa-usracing.js" async="async"></script> *}
{*if $page == 'index'*}
{* <link rel="stylesheet" type="text/css" href="/assets/css/home_v4.css?v=1"> *}
{*/if*}
<link rel="preload" href="/assets/css/plugins.min.css?v=2.5" as="style">
<link rel="stylesheet" type="text/css" href="/assets/css/plugins.min.css?v=2.5" media="none" onload="if(media!='all')media='all'">

<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->
{literal}
<!-- <script async type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script> -->
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<!--<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900&v1" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Roboto:400,700&v1' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700&v1' rel='stylesheet' type='text/css'>-->
{/literal}
{*
<!-- hide nav script -->
{literal}
<!-- <script>
var senseSpeed = 35;
var previousScroll = 0;
$(window).scroll(function(event){
   var scroller = $(this).scrollTop();
   if (scroller-senseSpeed > previousScroll){
      $("#nav.is_stuck").filter(':not(:animated)').slideUp();
   } else if (scroller+senseSpeed < previousScroll) {
      $("#nav.is_stuck").filter(':not(:animated)').slideDown();
   }
   previousScroll = scroller;
});
</script>-->
<!--<script type="text/javascript" src="/assets/plugins/jquery-1.10.2.min.js"></script>-->
<!--[if lt IE 9]>
	 <link rel="stylesheet" type="text/css" href="/assets/plugins/iealert/alert.css" />
     <script type='text/javascript' src="/assets/js/html5.js"></script>
     <script type="text/javascript" src="/assets/plugins/iealert/iealert.min.js"></script>
     <script type="text/javascript">$(document).ready(function() { $("body").iealert({  closeBtn: true,  overlayClose: true }); });</script>
<![endif]-->

{/literal}
*}
{literal}
<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>
{/literal}
{php}
echo '<script type="text/javascript">';
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/js/jquery-1.12.0.min.js");
echo file_get_contents("/home/ah/usracing.com/htdocs/assets/js/jquery-migrate-1.2.1.min.js"); 
echo '</script>'; 
{/php}
{* <script type="rocketlazyloadscript" data-rocket-type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js" ></script>
<script type="rocketlazyloadscript" data-rocket-type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js" ></script> *}
{*
<script src="//code.jquery.com/jquery-1.12.0.min.js" ></script>
  <!-- <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->

  <!-- <script src="/assets/js/jquery-1.12.0.custom.js"></script> -->
  <!-- <script src="/assets/js/jquery-1.12.0.custom.min.js"></script> -->

<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js" ></script>
<script defer async src="/assets/js/index-kd-tinysort.js"></script>
*}

{* {if $smarty.server.REQUEST_URI == '/index-ps' or $smarty.server.REQUEST_URI == '/index-bs' }
    <script defer type="text/javascript" src="/assets/plugins/countdown/jbclock.js"></script>
{/if} *}

{if $page == 'index'}
{if !$PWC_Switch}
{* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" media="none" onload="if(media!='all')media='all'"> *}
<script type="rocketlazyloadscript" data-rocket-type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>   
{* <link rel="stylesheet" href="/assets/css/slider.min.css?v=1.5.1" media="none" onload="if(media!='all')media='all'"> *}
{* <link rel="stylesheet" type="text/css" href="/assets/css/rs_v5.min.css?v=1.1" media="screen" onload="if(media!='all')media='all'" /> *}
{/if}
{* <link rel="stylesheet" type="text/css" href="/assets/plugins/rs/css/settings.css" media="screen" /> *}
{* <script defer type="text/javascript" src="assets/plugins/countdown/jbclock.js"></script> *}
{* <script defer type="text/javascript" src="assets/plugins/rs/js/rs.plugins.min.js"></script>
<script defer type="text/javascript" src="assets/plugins/rs/js/rs.revolution.min.js"></script>
<script defer type="text/javascript" src="assets/plugins/rs/js/rs.revolution.init.js"></script> *}
<script type="rocketlazyloadscript" data-rocket-type="text/javascript" type="text/javascript" src="assets/js/index.js"></script>
{/if}

{literal}
<script type="rocketlazyloadscript" data-rocket-type="text/javascript">
$( document ).ready(function() {
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    var is_Edge = navigator.userAgent.indexOf("Edge") > -1;
    var is_explorer= typeof document !== 'undefined' && !!document.documentMode && !is_Edge;

    var img;
    if (!is_safari && !is_Edge && !is_explorer)  {
       img = "/img/index-tv/as-seen-on.webp";         
    } else {
       img = "/img/index-tv/as-seen-on.png";    
    }
    $(".bloombergmarkets").css('background-image', 'url(' + img + ')');
    $(".bloombergtv").css('background-image', 'url(' + img + ')');
    $(".espn").css('background-image', 'url(' + img + ')');
    $(".forbes").css('background-image', 'url(' + img + ')');
    $(".laweekly").css('background-image', 'url(' + img + ')');
    $(".marketwatch").css('background-image', 'url(' + img + ')');
    $(".seekingalpha").css('background-image', 'url(' + img + ')');
    $(".sportsillustrated").css('background-image', 'url(' + img + ')');
    $(".usatoday").css('background-image', 'url(' + img + ')');
    $(".bleacherreport").css('background-image', 'url(' + img + ')');
    $(".sporttechie").css('background-image', 'url(' + img + ')');
});
</script>
{/literal}

