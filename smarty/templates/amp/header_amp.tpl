<head>
{*$smarty.template*}
{$metatag}
{$title}
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
<link rel="canonical" href="https://{$main_domain}{$arg_path}"/>
{$link}
{$css}
{include file='amp/scripts_amp_header.tpl'}
{if $redirect_switch=="1"}
{include file='redirectsripts.tpl'}
{/if}
{$js}
</head>
