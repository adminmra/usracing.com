{literal}
<!--<script async type="text/javascript" src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/plugins/hover-dropdown.min.js"></script>
<script type="text/javascript" src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/plugins/psScrollbar/psScrollw_mw.min.js"></script>
<script type="text/javascript" src="/assets/plugins/sidr/jquery.sidr.min.js"></script>
<script type="text/javascript" src="/assets/js/app.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function() {
  	App.init();
  	App.initFancybox();
  	App.initRoyalSlider();
    try{
      Index.initIndex();
    }catch(e){ console.log( "assets/js/index.js -> does not imported" ); }
  });
</script>
<!--[if lt IE 9]>
      <script type="text/javascript" src="/assets/js/respond.js"></script>
<![endif]-->
<!-- Pin It - Pinterest
<script type="text/javascript" async src="/blog/wp-content/themes/usracing/js/pinit.js"></script>-->

<!--
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'auto', {'allowLinker': true});
  ga('require', 'displayfeatures');
  ga('require', 'linker');
  ga('linker:autoLink', ['betusracing.ag'] );
  ga('send', 'pageview');

</script>
-->
<!-- Google Analytics -->
<amp-analytics type="googleanalytics">
<script type="application/json">
{
 "vars": {
  "account": "UA-742771-29"
 },
 "triggers": {
  "trackPageview": {
   "on": "visible",
   "request": "pageview"
  }    
 }
}
</script>
</amp-analytics>
<!--<script async src='//www.google-analytics.com/analytics.js'></script>-->
<!-- End Google Analytics -->





<!-- Facebook Pixel Code
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '906957672685525');
fbq('track', "PageView");


</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=906957672685525&ev=PageView&noscript=1"
/></noscript> -->
<!-- End Facebook Pixel Code -->


<!-- Bing Ads RP  -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4047476"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img alt="Bing" src="//bat.bing.com/action/0?ti=4047476&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>


<!--  OptinMonster ---><script>var om571fe1cc5ae65,om571fe1cc5ae65_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){om571fe1cc5ae65_poll(function(){if(window['om_loaded']){if(!om571fe1cc5ae65){om571fe1cc5ae65=new OptinMonsterApp();return om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmstr.com/app/js/api.min.js",o.async=true,o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;om571fe1cc5ae65=new OptinMonsterApp();om571fe1cc5ae65.init({"s":"18242.571fe1cc5ae65","staging":0,"dev":0,"beta":0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster -->

<!-- Lucky Orange-->
<script> window.lo_use_ip_lookups = true; </script>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 43153;
(function() {
var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
})();
</script>
<!-- End Lucky Orange-->



{/literal}
