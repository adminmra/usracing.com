{if $plainbody ne ''}
{include file=$plainbody}
{else}
<!doctype html>
<html amp>
{if $header ne ''}
{include file=$header}
{/if}
<body class="{$bodyclass} {if $smarty.server.REQUEST_URI == '/index-kd' or $smarty.server.REQUEST_URI == '/index-ps' or $smarty.server.REQUEST_URI == '/index-bs' or $smarty.server.REQUEST_URI == '/' or $smarty.server.REQUEST_URI == '/indexbecca' }{'index-kd'}{/if}">
<!-- ClickTale Top part -->
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->

{$ahr_mess}
{include file=$topcontent}
{if $body ne ''}
{include file=$body}
{/if}
{if $footer ne ''}
{include file=$footer}
{/if}
</body>
</html>
{/if}
