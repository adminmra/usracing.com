<div class="countdown">
   <span class="headline">
   <a href="/kentucky-derby">Countdown to the Kentucky Derby<i class="fa fa-angle-right"></i></a>
   </span>
   <ul class="timer">
    <li>
    <span class="days"></span>
    <p class="days_ref">Days</p>
    </li>
    <li>
    <span class="hours"></span>
    <p class="hours_ref">Hours</p>
    </li>
    <li>
    <span class="minutes"></span>
    <p class="minutes_ref">Mins</p>
    </li>
    <li>
    <span class="seconds"></span>
    <p class="seconds_ref">Secs</p>
    </li>
   </ul>
</div>