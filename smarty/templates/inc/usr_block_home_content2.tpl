{literal}
<style type="text/css">
.row3 p { margin:12px 0; padding:0 10px; }
</style>
{/literal}
<div class="block row3" style="height:220px; padding:10px;">
<div class="container">
<h2 class="title">Online Betting Pools – Full Track Odds</h2>


<p>All bets at US Racing are securely commingled into host horse track betting pools. This means that you are actually supporting all the individuals and horsemen who partake in the onshore, legal horse racing industry. </p>


<p>US Racing sends a share of account wagering revenues back to racetracks and horsemen in the market where each wager originates. And, unlike offshore online horse betting sites that do not offer full odds, at US Racing all winning bets are paid at full track odds.</p>

<p>This means that if you win a big bet, you will get paid out in full odds; <strong>your winnings will never be capped at a certain dollar amount.</strong> </p>


</div><!-- end:container -->
</div>
