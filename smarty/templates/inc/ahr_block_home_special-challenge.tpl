{literal}
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; width: 100%; padding: 0; }
#special tbody { border:0; padding:0; }
#special table { float:left; height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { vertical-align:top; text-align:left; font-weight:bold; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special #infoEntries td.right { text-align: right; }
#special #infoEntries td.center { text-align: center; } 
#special h1.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
	<div class="container">

<h2 class="title">Breeders' Cup Challenge Schedule</h2>

<div class="post">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="infoEntries" class="data">
<tbody>
  <tr>
    <th width="13%">Date</th>
    <th>Race</th>
    <th>Track</th>
    <!--<th>Distance</th>-->
    <th class="center">Grade</th>
    <!--<th>Age</th>-->
    <!--<th>Winner</th>-->
  </tr>
  <tr class="odd">
    <td>Jun 18</td>
    <td>Clasico Belgrano</td>
    <td>Palermo Race Track</td>
    <!--<td>1 1/2 Miles</td>-->
    <td class="center">II</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr>
    <td>Jun 25</td>
    <td>Stobart Ireland Pretty Polly Stakes</td>
    <td>Curragh Racecourse</td>
    <!--<td>1 1/4 Miles (T)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr class="odd">
    <td>Jun 26</td>
    <td>Grand Prix de Saint-Cloud</td>
    <td>Saint Cloud</td>
    <!--<td>1 1/2 Miles (T)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr>
    <td>Jun 26</td>
    <td>Takarazuka Kinen</td>
    <td>Hanshin Racecourse</td>
    <!--<td>1 3/8 Mile (T)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr class="odd">
    <td>Jul 02</td>
    <td><a href="/stakes?name=Shoemaker_Mile">Shoemaker Mile</a></td>
    <td><a href="/racetrack?name=Hollywood_Park">Hollywood Park</a></td>
    <!--<td>1 Mile (T)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr>
    <td>Jul 02</td>
    <td><a href="/stakes?name=United_Nations_Stakes">United Nations Stakes</a></td>
    <td><a href="/racetrack?name=Monmouth_Park">Monmouth Park</a></td>
    <!--<td>1 3/8 Mile (T)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr class="odd">
    <td>Jul 09</td>
    <td><a href="/stakes?name=Smile_Sprint_Handicap">Smile Sprint Handicap</a></td>
    <td><a href="/racetrack?name=Calder_Race_Course">Calder Race Course</a></td>
    <!--<td>6 Furlongs</td>-->
    <td class="center">II</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr>
    <td>Jul 09</td>
    <td><a href="/stakes?name=Hollywood_Gold_Cup">Hollywood Gold Cup</a></td>
    <td><a href="/racetrack?name=Hollywood_Park">Hollywood Park</a></td>
    <!--<td>1 1/4 Miles (AWT)</td>-->
    <td class="center">I</td>
    <!--<td>3 YO &amp; UP</td>-->
    <!--<td></td>-->
  </tr>
  <tr class="odd">
    <td>Jul 16</td>
    <td><a href="/stakes?name=Delaware_Handicap">Delaware Handicap</a></td>
    <td><a href="/racetrack?name=Delaware_Park">Delaware</a></td>
    <!--<td>1 1/4 Miles</td>-->
    <td class="center">II</td>
    <!--<td>3 YO &amp; UP</td>
    <td></td>-->
  </tr>
  <tr>
    <td>Jul 16</td>
    <td><a href="/stakes?name=A_Gleam_Invitational_Handicap">A Gleam Invitational Handicap</a></td>
    <td><a href="/racetrack?name=Hollywood_Park">Hollywood Park</a></td>
    <!--<td>7 Furlongs (AWT)</td>-->
    <td class="center">II</td>
    <!--<td>3 YO &amp; UP</td>
    <td></td>-->
  </tr>
  
</tbody>
</table>



</div><!-- end:post -->
</div><!-- end:container -->

<div class="boxfooter" style="padding-top:0;"><a title="Breeders Cup Challenge Schedule" href="/breeders-cup/challenge"> {include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup Challenge Schedule</a></div>


</div><!-- end:#special -->
<div class="box-shd"></div>
