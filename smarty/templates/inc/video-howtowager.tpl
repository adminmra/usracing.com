{literal}<script type="text/javascript" src="/assets/plugins/jwplayer/jwplayer.js" /></script>{/literal} 

<div id="vidHowTo" style="text-align:center; min-height:100px;"><img src="/img/spinner-loading.gif" width="100" height="100" style="margin:0 auto;" /></div>

{literal}
<script type="text/javascript">
    jwplayer("vidHowTo").setup({
        file: "/video/howtomakeawager.mp4",
        image: "/video/howtomakeawager.jpg",
		skin: "/assets/plugins/jwplayer/five.xml",
		width:"100%",
		aspectratio: "4:3"
    });
</script>
{/literal}        