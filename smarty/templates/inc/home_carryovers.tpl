{include_php file="/home/ah/allhorse/public_html/secure/carryovers.php"}	

<!-- Modal -->
<div class="modal fade" id="carryoverHelp" tabindex="-1">
 <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"><button class="close" data-dismiss="modal" type="button">X</button>
 <h3 class="modal-title">What are Carryover Pools?</h3>
 </div>
 <div class="modal-body" style="padding-bottom:0;">
<p>A carryover is when no wins occur at the end of a race on a particular wager type - for example, a Pick 4, Pick 5 or Pick 6. All wagers in the carryover pool or 'pool' are then carried over to the next designated race. </p>
<p>The carryover continues from race to race until a lucky winner or winners collect the prize pool, which can reach millions of dollars. As carryover pools increase in value, more betting interest is evidenced and consequently as horse bettors have a chance to win bigger money.</p>
 </div>
 <div class="modal-footer center">
 <button class="btn btn-lrg btn-default center" data-dismiss="modal" type="button">Close</button>
 </div>
 </div>
 </div>
</div>