{literal}<style type="text/css">
#infoEntries td { padding: 7px 10px; }
</style>{/literal}
<table width="100%" height="12" cellpadding="0" cellspacing="0" id="infoEntries" class="data" title="Kentucky Oaks Odds" summary="2011 Kentucky Oaks Odds">
<tr>
<th width="50%">2011 KENTUCKY OAKS</th>
<th align="right" style="text-align:right;">ODDS TO WIN</th>
</tr>
<tr>
<td class="left" nowrap><div>A Z Warrior</div></td>
<td class="sortOdds" align="right" nowrap>25 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Big Tiz</td>
<td class="sortOdds" align="right" nowrap>30 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Bouquet Booth</td>
<td class="sortOdds" align="right" nowrap>35 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Dancinginherdreams</td>
<td class="sortOdds" align="right" nowrap>8 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Delightful Mary</td>
<td class="sortOdds" align="right" nowrap>20 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Harlans Ruby</td>
<td class="sortOdds" align="right" nowrap>50 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Holy Heavens</td>
<td class="sortOdds" align="right" nowrap>50 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Inglorious</td>
<td class="sortOdds" align="right" nowrap>15 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Its Tricky</td>
<td class="sortOdds" align="right" nowrap>8 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Joyful Victory</td>
<td class="sortOdds" align="right" nowrap>7 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Kathmanblu</td>
<td class="sortOdds" align="right" nowrap>7 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Mildly Offensive</td>
<td class="sortOdds" align="right" nowrap>45 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Miss Smarty Pants</td>
<td class="sortOdds" align="right" nowrap>40 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Oh Carole</td>
<td class="sortOdds" align="right" nowrap>45 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Pomeroys Pistol</td>
<td class="sortOdds" align="right" nowrap>50 - 1</td>
</tr>
<tr>
<td class="left" nowrap>R Heat Lightning</td>
<td class="sortOdds" align="right" nowrap>3 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Rigoletta</td>
<td class="sortOdds" align="right" nowrap>45 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Royal Delta</td>
<td class="sortOdds" align="right" nowrap>30 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Salty Strike</td>
<td class="sortOdds" align="right" nowrap>50 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Snow Fall</td>
<td class="sortOdds" align="right" nowrap>45 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Summer Soiree</td>
<td class="sortOdds" align="right" nowrap>45 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Wyomia</td>
<td class="sortOdds" align="right" nowrap>20 - 1</td>
</tr>
<tr>
<td class="left" nowrap>Zazu</td>
<td class="sortOdds" align="right" nowrap>7 - 1</td>
</tr>


<tr style="background:#ffffff;">
<td class="left" nowrap></td>
<td class="sortOdds" align="right" nowrap><em style="color:#808080;">Odds Updated April 7th, 2011</em></td>
</tr>
</table>
<script type="text/javascript">
$("#infoEntries tr:even").addClass("odd");
</script>