{literal}
<style type="text/css">
#graded-content { height:266px;  }
table.odds td {padding:3px 10px; border:0; text-align:right;}
table.odds td:first-child { text-align:left;}
table.odds tr, table tbody { border:0;}
</style>
{/literal}
<div class="block" style="height:220px; margin-top:0; position:relative;">
<h2 class="title">Kentucky Derby Pool Odds</h2>
<div class="container" style="padding:10px 0 0;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="odds">
<tr>
  <td><strong>1.</strong> Almost Famous</td>
  <td>22-1</td>
</tr>
<tr>
  <td><strong>2.</strong> Bobby's Kitten </td>
  <td>21-1</td>
</tr>
<tr>
  <td><strong>3.</strong> Bond Holder</td>
  <td>21-1</td>
</tr>
<tr>
  <td><strong>4.</strong> Cairo Prince</td>
  <td>22-1</td>
</tr>
<tr>
  <td><strong>5.</strong> Cleburne </td>
  <td>99-1</td>
</tr>
<tr>
  <td><strong>6.</strong> Commissioner</td>
  <td>23-1</td>
</tr>
<tr>
  <td><strong>7.</strong> Coup de Grace </td>
  <td>25-1</td>
</tr>
</table>














</div><!-- end:container -->
<div class="boxfooter" style="right:0; position:absolute;"><span class="bold"><a href="https://secure.usracing.com/Register.aspx">Kentucky Derby Pool Odds</a></span></div>
</div>