<!-- UPCOMING RACES SLIDER -->
<div class="upcomingRaces margin-bottom-30">
    <div class="headline">
     <h2>Upcoming Races</h2>
    </div>
    <div class="dateCarousel carousel slide carousel-v1" id="raceCarousel">
     <div class="carousel-inner margin-bottom-20">
      <div class="item active">
       <div class="dateLarge">
        <div class="month"><span>M</span><span>A</span><span>R</span></div>
        <div class="day"><span>1</span><span>8</span></div>
       </div>
       <div class="details">
        <div class="race"><span>RACE:</span> Long Island Handicap</div>
        <div class="track"><span>TRACK:</span>
         <a href="#">Aqueduct</a>
        </div>
        <div class="info"><span>PURSE:</span> $150,000, II, 3up</div>
       </div>
      </div>  <!-- /item -->
      
      <div class="item">
       <div class="dateLarge">
        <div class="month"><span>M</span><span>A</span><span>R</span></div>
        <div class="day"><span>2</span><span>0</span></div>
       </div>
       <div class="details">
        <div class="race"><span>RACE:</span> Preakness Stakes</div>
        <div class="track"><span>TRACK:</span>
         <a href="#">Aqueduct</a>
        </div>
        <div class="info"><span>PURSE:</span> $150,000, II, 3up</div>
       </div>
      </div> <!-- /item -->
      
      <div class="item">
       <div class="dateLarge">
        <div class="month"><span>M</span><span>A</span><span>R</span></div>
        <div class="day"><span>2</span><span>2</span></div>
       </div>
       <div class="details">
        <div class="race"><span>RACE:</span> Kentucky Derby</div>
        <div class="track"><span>TRACK:</span>
         <a href="#">Aqueduct</a>
        </div>
        <div class="info"><span>PURSE:</span> $150,000, II, 3up</div>
       </div>
      </div>  <!-- /item -->
      
     </div><!-- /carousel-inner -->
     
     <div class="carousel-arrow">
      <a data-slide="prev" href="#raceCarousel" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
      <a data-slide="next" href="#raceCarousel" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
     </div>
     <div class="blockfooter">
      <a href="#" title="Upcoming Races" class="btn btn-primary">Upcoming Races<i class="fa fa-angle-right"></i></a>
     </div>
     
    </div> <!-- /carousel -->
   </div> <!-- /upcomingRaces -->