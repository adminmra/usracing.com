re<div class="content">
  <div class="headline"><h2>Shop</h2></div>
  <div class="shopFeed margin-bottom-20">
    <p><strong>Cool gear for cool people.</strong></p>
    <p>Get custom US Racing gear for every need. Everybody's doing it!</p>
    <div class="shopImg"> <a  href="http://www.cafepress.com/betusracing" rel="nofollow"><img class="img-responsive" src="img/usracing-gear.jpg" alt="US Racing Gear" /></a> </div>
  </div>
</div><!--/content-->

<div class="blockfooter"> <a href="http://www.cafepress.com/betusracing" class="btn btn-primary" rel="nofollow" target="_blank">Buy Horse Racing Gear<i class="fa fa-angle-right"></i></a> </div>
