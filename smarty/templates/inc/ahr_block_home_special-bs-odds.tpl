{literal}
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; background:#f2f9ff; width: auto; border:1px solid #aad3ff; padding: 10px; margin-top:10px; }
#special .race { float:left; width:100%; display:block; padding:0 0 5px 0; height:auto; margin: 0 0 0 0; border-bottom:1px solid #e6e6e6;  }
#special .race .date { font-weight: bold; width:auto; float:left; }
#special .race .betlink { font-weight: bold; float:right;}
#special .details { display:block; width: 345px; clear:both;}
#special .track { text-transform:uppercase; }
#special .note { clear:left; padding:10px 0 0 10px; *padding-top:0;  *margin-top:10px; display:block; }
#special tbody { border:0; padding:0; }
#special table { float:left; height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special table .right { text-align: right; } 
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { width:33%; text-align:left; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special h1.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
	<div class="container">

<h1 class="title">Belmont Stakes Odds to Win now available!</h1>

<div class="post">

<div class="race">
	<span class="date">Belmont Stakes - Saturday, June 11th, 2011</strong></span>
	<span class="betlink">&nbsp;</span>
</div>



<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td valign="top">Shackleford<br>
    Animal Kingdom<br>
    Nehro<br>
    Mucho Macho Man<br>
    Awesome Patriot<br>
    Alternation<br>
    Jaycito<br></td>
  <td class="right" valign="top"> 3-1<br>
    3-1<br>
    5-1<br>
    8-1<br>
    8-1<br>
    8-1<br>
    10-1<br></td>
  <td width="30"></td>
  <td valign="top">Uncle Sam<br>
    Prime Cut<br>
    Master Of Hounds<br>
    Harlans Hello<br>
    Santiva<br>
    Stay Thirsty<br>
    Isnt He Perfect<br></td>
  <td class="right" valign="top"> 15-1<br>
    15-1<br>
    15-1<br>
    15-1<br>
    20-1<br>
    25-1<br>
    25-1<br></td>
</tr>
</table>



</div><!-- end:post -->

<div class="note">
	See the complete list of {literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'belmontstakes-odds')+'" title="Belmont Stakes 2011 - Odds to win!">Belmont Stakes Odds to Win here!</a>');</script>{/literal}
</div>

<div class="note">
	The <a href="/belmontstakes" title="Belmont Stakes">143rd Belmont Stakes</a> is on Saturday, June 11th, 2011 at <a href="/belmontpark" title="Belmont Park">Belmont Park</a>.
  <br />
  View the complete <a href="/triplecrown/triple-crown-nominations">2011 Triple Crown Nominations List here</a>! 
</div>

</div><!-- end:container -->

<div class="boxfooter" style="padding-top:0;"><a title="More Belmont Stakes" href="/belmontstakes">More Belmont Stakes</a></div>


</div><!-- end:#special -->
<div class="box-shd"></div>
