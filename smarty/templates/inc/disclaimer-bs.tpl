<div class="container"><div class="disclaimer">
<div class="headline"><h2>Disclaimer</h2></div>

<p>Belmont Stakes is a registered trademarks of the New York Racing Association and/or its affiliates (collectively NYRA). NYRA does not sponsor or endorse, and is not associated or affiliated with US Racing or its products, services or promotions.  US Racing provides free information, facts, odds and commentary about the Belmont Stakes and horse racing and betting, in general.  Third party marks may be referenced in a transformative, editorial, informational, nominative, critical, analytical or comparative context. US Racing may reference marks belonging to third parties pursuant to our right to engage in fair use, fair comment, statutory fair use or trade mark fair use doctrine.  As such, US Racing does not contribute to any dilution of any trade or service marks. US Racing provides this information in an effort to educate and grow the sport of thoroughbred racing in North America with an emphasis on attracting new fans of the sport.</p>
</div>
</div>
