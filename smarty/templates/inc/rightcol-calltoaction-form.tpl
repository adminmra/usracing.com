<span id="object"><i class="animated floating fa fa-map-marker"></i></span>
  <form class="form-signin" id="s_form-signup" method="post" action="" role="form" novalidate="novalidate">
	<div class="form-group account-group"><input type="hidden" name="ref" value="{$ref}" id="s_ref"></div>
	<div class="form-group account-group"><input type="hidden" name="cta" value="{$cta}"></div>
	<div class="form-group account-group"><input type="hidden" name="cta_sub" value="{$cta_sub}"></div>
	<div class="form-group account-group">	
	  <input  class="form-control" placeholder="First Name" name="first_name" required="" type="text" id="s_firstname">
      <small class="error" style="display: none;" id="s_fn_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input  class="form-control" placeholder="Last Name" name="last_name" required="" type="text" id="s_lastname">
      <small class="error" style="display: none;" id="s_ln_error">Please fill out this field</small>
      </div>
	  <div class="form-group account-group">	
	  <input  class="form-control" placeholder="Email address" required="" name="email" type="email" id="s_email">
      <small class="error" style="display: none;" id="s_em_error">Please fill out this field</small>
      </div>
				
{*
	<div class="form-group pin-group">	
	<input class="form-control" placeholder="Password (6 - 10 chars)" maxlength="10" required="" name="password" autofocus="" type="password"></div>
*}
				
	  <div class="form-group login-group">	
	  <input name="step" value="front" type="hidden">
	  <center>
       <button type="submit"  class="btn btn-sm btn-red" id="signupBtn">&nbsp;&nbsp; Sign Up  &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-angle-right"></i></button>
      </center></div>
	</form>
</div>
{literal}
<script type="text/javascript">
$( document ).ready(function() {
$(".calltoaction #object").delay(500).queue(function(next) {
  $(this).addClass("animated bounceInDown").css("visibility", "visible");
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction h2").addClass("animated tada"); 
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction .fa-angle-right").addClass("animated fadeInLeft").css("visibility", "visible");; 
  next();
});
});
</script>
{/literal}
