<!-- <p><span><a title="Bet on the Kentucky Derby" href="/join"><img style="float: right; margin: 0 0 30px 20px; border: 0;" title="Kentucky Derby Betting" src="/themes/images/specialraces/kentucky_derby_300x250.gif" border="0" alt="Kentucky Derby Betting" align="right" /></a></span></p>
 --><br />

<p><strong>When is the Kentucky Derby?
<br />
  </strong> The 138th Kentucky Derby is on Saturday, May 5, 2012!
  <br />
  <br />
  <strong>Where is the Kentucky Derby?
  <br />
  </strong> The Derby is raced at Churchill Downs Racecourse in Louisville, Kentucky
  <br />
  <br />
  <strong>Where can I watch the Kentucky Derby?
  <br />
  </strong> Watch the Kentucky Derby live on TV with NBC at 5:00 p.m. Eastern Time
  </p>

<p>The Kentucky Derby is a stakes race for three-year-old thoroughbred horses, staged yearly in Louisville, Kentucky.</p>
<p>&nbsp;</p>