{* 	--- Leave HTML format unchanged. Keep a single space between <span> for the date ----
	--- Order of <li> is not important. As long as date HTML format is formatted as below, the list will sort -- *}

<div id="event"><ul>
<!----------------------------------------------->
<li>	
	<time>May<br>2nd</time>
    <div>
    <header>The {include_php file='/home/ah/allhorse/public_html/kd/running.php'} Kentucky Derby</header>
    <span class="details"><a href="/churchill-downs">Churchill Downs Racecourse</a></span>
    
    </div>    
</li>
<!----------------------------------------------->
<li>	
	<time>May<br>16th</time>
    <div>
    <header>The {include_php file='/home/ah/allhorse/public_html/ps/running.php'} Preakness Stakes</header>
    <span class="details"><a href="/pimlico">Pimlico Racecourse</a></span>
    
    </div>    
</li>
<!----------------------------------------------->
<li>	
	<time>June<br>6th</time>
    <div>
    <header>The {include_php file='/home/ah/allhorse/public_html/belmont/running.php'} Belmont Stakes</header>
    <span class="details"><a href="/belmont-park">Belmont Park</a></span>    
    </div>    
</li>
<!----------------------------------------------->
<li>	
	<time>Oct<br>30th</time>
    <div>
    <header>Breeders Cup</header>
    <span class="details"><a href="/santa-anita-park">Santa Anita Park</a></span>    
    </div>    
</li>

<!----------------------------------------------->
</ul>
     
</div><!-- end/Events -->
