<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/belmontstakes/belmont-park.jpg"  />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">Belmont Park in Elmont, New York</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/belmontstakes/belmont-stakes-winner.jpg"  />
    <figure class="rsCaption">2013 Belmont Stakes Winner</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/belmontstakes/carnations.jpg" />
    <figure class="rsCaption">Photo by Shannon Truax | Carnations, the official flower</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/belmontstakes/belmont-breeze-drink.jpg" />
    <figure class="rsCaption">Belmont Breeze - Official drink of the Belmont Stakes</figure>
   </div>
   
  </div>
 </div>
