{php}
if(isset($_GET['upcoming']) && $_GET['upcoming']!="")
   echo '<div id="col2_content" class="clearfix">
        <div id="col2_inside" class="floatbox sidebar-col sidebar-col-right">
          <div  class="block">
            <div class="box">
              <div class="box-content">
               <div class="graded">
                  <div class="boxheader"><div class="boxicon"><img src="/themes/images/horse-racing-icon.gif"><h2>Upcoming Races</h2></div></div>
                  <div>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px">
                        <tr><th>Track</th><th>Race</th><th>MTP</th></tr>
                        <tr>
                           <td>GB4 Kempton</td>
                           <td>2</td>
                           <td>0</td>
                        </tr>
                        <tr>
                           <td>GB4 Kempton</td>
                           <td>3</td>
                           <td>8</td>
                        </tr>
                        <tr>
                           <td>Fingre Lakes</td>
                           <td>1</td>
                           <td>13</td>
                        </tr>
                        <tr>
                           <td>Aqueduct</td>
                           <td>1</td>
                           <td>18</td>
                        </tr>
                        <tr>
                           <td>Freehold Raceway</td>
                           <td>1</td>
                           <td>27</td>
                        </tr>
                     </table>
                  </div>
               </div>
              </div>
            </div>
            <div class="box-shd"></div>
          </div>
        </div>
      </div>';
{/php}
