
<div class="calltoaction">
<a href="/signup?ref={$ref}" >
<span id="object"><i class="animated floating fa fa-map-marker"></i></span>
<h2>{$h1}</h2>
<h3>Place your bets now!</h3>
<span class="btn btn-red">Sign Up <i class="fa fa-angle-right"></i></span></a>
</div>


{literal}
<script type="text/javascript"> 
$( document ).ready(function() {
var str = $("#left-col h1").text();
//$(".calltoaction h2").html(str);

$(".calltoaction #object").delay(500).queue(function(next) {
  $(this).addClass("animated bounceInDown").css("visibility", "visible");
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction h2").addClass("animated tada"); 
  next();
}).delay(600).queue(function(next) {
  $(".calltoaction .fa-angle-right").addClass("animated fadeInLeft").css("visibility", "visible");; 
  next();
});
});
</script>
{/literal}
