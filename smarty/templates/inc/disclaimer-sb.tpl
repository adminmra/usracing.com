<div class="disclaimer">
<div class="headline"><h2>Terms & Conditions</h2></div>

<p>The Super Bowl Deposit Bonus promotion is available to all BUSR clients. 
The bonus will be added to your account immediately after making the deposit.
If you make multiple deposits on the same day, you will only receive a a bonus based on your first deposit.
The Super Bowl promo codes are valid during the following times:
SB20 promo code is valid from until 11:59pm EST on Thursday, February 2nd.
SB15 promo code is valid from 12:00am - 11:59pm EST on Friday, February 3rd.
SB10 promo code is valid from 12:00am - 11:59pm EST on Saturday, February 4th.
Reload bonuses and other cash rewards are subject to the following standard rollover requirements:
Sports betting activity equal to 30 times the amount of the bonus.
Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and certain casino games, as specified in the Casino Rules, do not apply towards rollover requirements.
Blackjack activity equal to 200 times the amount of the bonus.
Rollover requirements must be met within 15 days of your deposit.
You can make a deposit and receive a bonus for each day, 
You cannot make a deposit, receive the bonus funds and then withdraw your deposit and bet with the bonus funds.
You cannot open multiple accounts to abuse promotions or bonuses.
Management reserves the right to modify or cancel this promotion at any time. General house rules/terms and conditions apply.
</p>
</div>
