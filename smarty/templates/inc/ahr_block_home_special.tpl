{literal}
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; background:#f2f9ff; width: 100%; border:1px solid #aad3ff; padding: 0; margin-top:10px; }
#special .race { float:left; width:100%; display:block; padding:5px 0; height:auto; margin:0; }
#special .race .message { float:left; width:auto; padding:5px 5px 5px 15px;}
#special .race .betlink { text-align:right; float:right; display:block; margin-right:5px;}
#special .race .betlink a { font-weight: bold; font-size:11px; text-align:right; text-decoration:none; display:block; padding:0; background: #fff;}
#special .note { clear:left; padding:10px 10px 0; *padding-top:0;  *margin-top:10px; display:block; }
#special tbody { border:0; padding:0; }
#special table { height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special #infoEntries { margin-top:7px; padding:0; }
#special #infoEntries td { padding:2px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { text-align:left; }
#special #infoEntries th { padding:2px 5px; text-align: left;  border-bottom:0; }
#special #infoEntries th:first-child { text-align: left; }
#special h2.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
<div class="container">

<h2 class="title">Congratulations to Animal Kingdom!</h2>

<div class="post">         
	<div class="race">
		<span class="message">
		<span style="font-size:13px; line-height:19px;">
    <strong>For Winning the 137th Kentucky Derby!</strong></span>
		<br/>
		Who will win the Preakness Stakes?
		</span>
		<span class="betlink">{literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'preaknessstakes-odds')+'" title="Bet on the Preakness Stakes"><img src="/themes/images/home/preakness-stakes-sml.jpg" border=\"0\" /></a>');</script>{/literal}</span>
</div>
	</div><!-- end:post -->

<div class="note">
<span><strong>The Preakness Stakes Contenders are up!</strong></span>
<table width="100%" summary="2011 Preakness Stakes Contenders" title="Preakness Stakes Odds" id="infoEntries">
<tbody>
  <tr> 

    <th width="33%">Horse</th>
    <th width="33%">Trainer</th>
    <th width="34%">Jockey</th>
    
  </tr>
  <tr>
    <td><a title="Dialed In" href="/horse?name=Dialed In">Dialed In</a></td>
    <td>Nick Zito</td>
    <td><a title="Julien Leparoux" href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
    
  </tr>
  <tr class="odd">
    <td><a title="Soldat" href="/horse?name=Soldat">Soldat</a></td>
    <td><a title="Kiaran McLaughlin" href="/trainer?name=Kiaran McLaughlin">Kiaran McLaughlin</a></td>
    <td><a title="Alan Garcia" href="/jockey?name=Alan Garcia">Alan Garcia</a></td>
    
  </tr>
  <tr>
    <td><a title="Anthonys Cross" href="/horse?name=Anthonys Cross">Anthonys Cross</a></td>
    <td><a title="Eoin G. Harty" href="/trainer?name=Eoin G. Harty">Eoin G. Harty</a></td>
    <td><a title="Joel Rosario" href="/jockey?name=Joel Rosario">Joel Rosario</a></td>
    
  </tr>
  <tr class="odd">
    <td><a title="Silver Medallion" href="/horse?name=Silver Medallion">Silver Medallion</a></td>
    <td><a title="Steven M. Asmussen" href="/trainer?name=Steven M. Asmussen">Steven M. Asmussen</a></td>
    <td><a title="Russell Baze" href="/jockey?name=Russell Baze">Russell Baze</a></td>
    
  </tr>
  <tr>
    <td><a title="Premier Pegasus" href="/horse?name=Premier Pegasus">Premier Pegasus</a></td>
    <td>Cho Myung</td>
    <td>Alonso Quinonez</td>
    
  </tr>
  <tr class="odd">
    <td>R Heat Lightening</td>
    <td><a title="Todd Pletcher" href="/trainer?name=Todd Pletcher">Todd Pletcher</a></td>
    <td>J. R. Velazquez</td>
    
  </tr>
  <tr>
    <td>Turbulent Descent</td>
    <td><a title="Mike Puype" href="/trainer?name=Mike Puype">Mike Puype</a></td>
    <td><a title="David Flores" href="/jockey?name=David Flores">David Flores</a></td>
    
  </tr>
</tbody>
</table>
</div>

<div class="note">See the complete details of {literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'preaknessstakes-contenders':'preaknessstakes-contenders')+'" title="Preakness Stakes 2011 - Contenders!">Preakness Stakes Contenders here!</a>');</script>{/literal}</div>

</div><!-- end:container -->

</div>
<div class="box-shd"></div>
