<!-- <a title="Join Today for Free!" href="/join"><img style="float: right; margin: 0 0 30px 30px;" title="Bet on the 2012 Preakness Stakes" border="0" alt="Bet on the 2012 Preakness Stakes" src="/themes/images/specialraces/preakness_stakes_300x250.gif" /></a>
 --><br />
<p>
<strong>When is the Preakness Stakes?</strong>
<br />
The Preakness Stakes is on Saturday, May 19, 2012!
<br />
<br />

<strong>Where is the Preakness Stakes?</strong>
<br />
The Preakness Stakes is raced at Pimlico Racecourse in Baltimore, Maryland
<br />
<br />

<strong>Where can I watch the Preakness Stakes?</strong><br />
Watch the Preakness Stakes live on TV with NBC at 5:00 p.m. Eastern Time
</p>

<p>The 137th running of the $1,000,000 <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a> (Grade I), is the second jewel of the Triple Crown, set for Saturday, May 19, 2012 at the historic track.</p>