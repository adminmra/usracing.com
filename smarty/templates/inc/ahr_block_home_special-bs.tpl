{literal}
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; background:#f2f9ff; width: 100%; border:1px solid #aad3ff; padding: 0; margin-top:15px; }
#special .race { float:left; width:100%; display:block; padding:5px 0; height:auto; margin:0; }
#special .container .message { width:auto; padding:10px 0 4px; display:block; margin:0; border-bottom:1px solid #DEDEDE; }
#special .race .message { float:left; width:auto; padding:5px 5px 5px 15px; margin:0; border:0;}
#special .race .betlink { text-align:right; float:right; display:block; margin-right:5px;}
#special .race .betlink a { font-weight: bold; font-size:11px; text-align:right; text-decoration:none; display:block; padding:0; background: #fff;}
#special .note { clear:left; padding:10px 10px 0; *padding-top:0;  *margin-top:10px; display:block; }
#special tbody { border:0; padding:0; }
#special table { height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special #infoEntries { margin:3px 0 5px; padding:0; }
#special #infoEntries td { padding:2px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border:0; }
#special #infoEntries td:first-child { text-align:left; }
#special #infoEntries th { padding:2px 5px; text-align: left;  border-bottom:1px solid #ccc; background:#fff; color:#000; border-right:0; }
#special #infoEntries th:first-child { text-align: left; }
#special h2.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
<div class="container">

<h2 class="title">Congratulations to Shackleford!</h2>

	<div class="message">
		<span style="font-size:13px; line-height:19px;"><strong>For Winning the 136th Preakness Stakes!</strong></span>		
	</div>
  
<div class="note">
<span style="display:block; width:100%; color:#fff; background:#105CA9; padding:3px 0;">&nbsp; &nbsp;<strong>2011 Preakness Stakes Result</strong></span>
<table summary="Preakness Stakes 2011 Winner" title="Preakness Stakes 2011 Winner" id="infoEntries">
<tbody>
<tr>
	<th>Winner</th>
	<th>Trainer</th>
	<th>Jockey</th>
	<th>Time</th>
</tr>
<tr>
	<td>Shackleford</td>
	 <td><a href="/trainer?name=Dale Romans">Dale Romans</a></td>
	<td>Jesus Castanon</td>
	<td>1:56.47</td>
</tr>
							
</tbody>
</table>
</div>

<div class="note">
<span style="display:block; width:100%; color:#fff; background:#105CA9; padding:3px 0;">&nbsp; &nbsp;<strong>2011 Black-Eyed Susan S. Result</strong></span>
<table summary="Black-Eyed Susan S. Winner" title="2011 Black-Eyed Susan S. Winner" id="infoEntries">
<tbody>
<tr>
	<th>Winner</th>
	<th>Trainer</th>
	<th>Jockey</th>
	<th>Time</th>
</tr>
<tr>
	<td>Royal Delta</td>
	 <td>Bill Mott</td>
	<td>Jose Lezcano</td>
	<td>1:49.60</td>
</tr>
							
</tbody>
</table>
</div>

<div class="post"> 
	<div class="race">    
		<span class="betlink">{literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'belmontstakes-betting':'belmontstakes-betting')+'" title="Bet on the Belmont Stakes"><img src="/themes/images/home/belmont-stakes-sml.jpg" border=\"0\" /></a>');</script>{/literal}</span>    
    
	<div class="message">			
		<strong>Who will win the 2011 Belmont Stakes?</strong>
		<br />
		Saturday, June 11th at <a href="/belmontpark">Belmont Park</a>.
   </div>
   
	</div>
</div><!-- end:post -->

<!--<div class="note">See the complete details of {literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'belmontstakes-contenders':'belmontstakes-contenders')+'" title="Belmont Stakes 2011 - Contenders!">Belmont Stakes Contenders here!</a>');</script>{/literal}</div>-->

</div><!-- end:container -->
<div class="boxfooter"><span class="h2"><a href="/belmontstakes">Click Here for More Belmont Stakes</a></span></div>
</div>
<div class="box-shd"></div>
