<!-- MAP STARTS HERE -->
{literal}
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrKPug10x9RA-gxnQMNyk1nJMS0yvGMs0&sensor=false"></script>
<script type="text/javascript">
function initialize() { 

//////////////////////    LOCATION    //////////////////////////////////////////////////
var theLocation = new google.maps.LatLng(38.20425, -85.77163);
// You can find Lat & Long at: http://universimmedia.pagesperso-orange.fr/geo/loc.htm
////////////////////////////////////////////////////////////////////////////////////////




var mapElement = document.getElementById('map');
// Options: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
var mapOptions = {
      zoom: 13,
      center: theLocation,
      styles: [	{featureType:"administrative",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:20}]},	{featureType:"road",elementType:"all",stylers:[{visibility:"on"},{saturation:-100},{lightness:40}]},	{featureType:"water",elementType:"all",stylers:[{visibility:"on"},{saturation:-10},{lightness:30}]},	{featureType:"landscape.man_made",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:10}]},	{featureType:"landscape.natural",elementType:"all",stylers:[{visibility:"simplified"},{saturation:-60},{lightness:60}]},	{featureType:"poi",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]},	{featureType:"transit",elementType:"all",stylers:[{visibility:"off"},{saturation:-100},{lightness:60}]}]
};
var map = new google.maps.Map(mapElement, mapOptions);
var marker = new google.maps.Marker({
    map:map,
    animation: google.maps.Animation.DROP,
    position: theLocation
  });				
google.maps.event.addListener(marker, 'click', toggleBounce);

function toggleBounce() {
  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
};
google.maps.event.addDomListener(window, 'load', initialize); // Initialize
</script>
{/literal}
<div class="map panel panel-white">
	<div class="panel-body">    
    <table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
    <i class="fa fa-map-marker"></i>
    <div class="address">    
    700 Central Ave, Louisville, KY, 40214, United States
    </div><!-- end/address -->
    </td></tr></table>
	<div id="map"></div>

	</div><!-- end/panel-body -->
</div><!-- end/panel -->
<!-- END/MAP -->
