{literal}
<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; background:#f2f9ff; width: auto; border:1px solid #aad3ff; padding: 10px; margin-top:10px; }
#special .race { float:left; width:100%; display:block; padding:0 0 5px 0; height:auto; margin: 0 0 0 0; border-bottom:1px solid #e6e6e6;  }
#special .race .date { font-weight: bold; width:auto; float:left; }
#special .race .betlink { font-weight: bold; float:right;}
#special .details { display:block; width: 345px; clear:both;}
#special .track { text-transform:uppercase; }
#special .note { clear:left; padding:10px 0 0 10px; *padding-top:0;  *margin-top:10px; display:block; }
#special tbody { border:0; padding:0; }
#special table { float:left; height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special table .right { text-align: right; } 
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { width:33%; text-align:left; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special h1.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
	<div class="container">

<h1 class="title"> Preakness Stakes Odds to Win now available!</h1>

<div class="post">

<div class="race">
	<span class="date">Preakness Stakes - Saturday, May 21, 2011</strong></span>
	<span class="betlink">{literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'join')+'" title="Odds to Win - Bet Now!"><img src="/themes/images/betnow-default.gif" border="0" width="67" /></a>');</script>{/literal}</span>
</div>

{include file="includes/ahr_block_kdodds_home.tpl"}
</div><!-- end:post -->

<div class="note">
	See the complete list of {literal}<script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'preaknessstakes-odds')+'" title="Preakness Stakes 2011 - Odds to win!">Preakness Stakes Odds to Win here!</a>');</script>{/literal}
</div>

<div class="note">
	The <a href="/preakness-stakes" title="Preakness Stakes">136th Preakness Stakes</a> is on Saturday, May 21, 2011 at <a href="/pimlico" title="Pimlico Race Course">Pimlico</a>.
  <br />
  View the complete <a href="/triplecrown/triple-crown-nominations">2011 Triple Crown Nominations List here</a>! 
</div>

<!-- <div class="note">Don't see your favorite horse? <a href="/support" title="Support">Contact</a> 'VIP' at US Racing and we'll see about adding it for you!</div>
 -->
</div><!-- end:container -->

<div class="boxfooter" style="padding-top:0;"><a title="More Preakness Stakes Details" href="/preakness-stakes">More Preakness Stakes</a></div>


</div><!-- end:#special -->
<div class="box-shd"></div>
