<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/triplecrown/american-pharoah.jpg"  />  
    <figure class="rsCaption">2015 Triple Crown Champion American Pharoah</figure> 
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/triplecrown/affirmed.jpg"  />
    <figure class="rsCaption">Affirmed takes the crown in 1978</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/triplecrown/seattleslew.jpg" />
    <figure class="rsCaption">Seattle Slew, Triple Crown unbeaten in 1977</figure>
   </div>
   	<div  class="rsContent">
    <img class="rsImg" src="/img/triplecrown/whirlaway.jpg" />
    <figure class="rsCaption">Whirlaway, only horse to win the "Superfecta" of the Triple Crown</figure>
   </div>
  
   
  </div>
 </div>
