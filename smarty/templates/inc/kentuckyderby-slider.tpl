<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/kentuckyderby/churchilldowns.jpg" alt="Kentucky Derby Betting"  />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">Churchill Downs, Louisville, Kentucky</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/kentuckyderby/kentucky-derby-trophy.jpg"  />
    <figure class="rsCaption">Winners Trophy</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/kentuckyderby/kentucky-derby-roses.jpg" alt="Road to the Roses" />
    <figure class="rsCaption">Road to the Roses - Official flower of the Kentucky Derby</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/kentuckyderby/kentucky-derby-mint-julep.jpg" alt="Mint Julep" />
    <figure class="rsCaption">Mint Julep - Official drink of the Kentucky Derby</figure>
   </div>
  </div>
 </div>
