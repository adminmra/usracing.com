{literal}
<style type="text/css">
#home-row-3 .right-col .block.row3, #home-row-3 .left-col .block.row3 { height:200px;}
.row3 p { margin:12px 0; padding:0 10px; }
</style>
{/literal}
<div class="block row3" style="padding:10px; margin-top:0;">
<div class="container">
<h1 class="title">Bet On Horse Racing Online!</h1>

<p><strong>Never miss another race!</strong> US Racing offers live video and online betting for all the major horse racing tracks.</p>

<p>Offering off-track betting on horse racing 365 days a year, we have a full menu of wagering options. Everything that's available to you at the track is also available to you online or mobile device.</p>

<p>The horse racing industry recognizes that future growth of the sport lies in modern off track betting online. Online growth for horse racing betting is increasing every year. <a href="https://secure.usracing.com/Register.aspx" rel="nofollow"><strong>Bet with US Racing today!</strong></a></p>

</div><!-- end:container -->
</div>