{literal}
<style type="text/css">
.subtext { width:auto; border-bottom:1px solid #4993C8; margin:0 20px; padding:10px 0;}
.subtext p { margin:8px 0;  }
</style>
{/literal}
<div class="subtext">
<div class="container">

<p>So, you want to bet on horses? And you want to be able to bet in person at the track but it would also be fun to bet online at home on your computer.  Or your iPad.  Or your iPhone. Or your Galaxy. Or... well, you get the idea.  First things first: Can I bet on horses legally over the Internet? Is betting on horses legal?  The answer to both these questions is YES.  Pari-mutuel wagering, i.e., online horse betting or horse racing betting is one of the few legal forms of online gambling available to American citizens.</p>

<p>Okay, so what do you get if you signup for an ADW racing account?  First, you have signup—it's free.  Then, you have to make a deposit with your credit card or via the bank ACH.  Not all online ADWs accept VISA and MasterCard.  But US Racing accepts these cards which, yes, makes it easy to deposit online!</p>

<p>So, you have funded your account.  What do you do next?  Some people say, "How do I bet on horses?" Or, "Where can I bet online for the Kentucky Derby?" You can do both these things right here!  After you deposit, go to the betting section.
</p>

<p>Take a breath—this is so simple:  Select the racetrack that you want to bet on.  Select the race that you want to bet on.  Select the horses or horses, jockey or jockeys and the bet type you want to place.  A Win bet? That means you think your horse (and jockey) will finish in first place.  This is a bet that they won the gold medal.  Got it?  How about a Place bet?  All that means is that you are betting that your horse (jockey) will get a Gold or Silver medal—they will finish in first or second place.  If that happens, you win!</p>


<p>Now, in sports there are statistics and then there are statistics.  MLB? Yes, they have a lot of stats.  Guess what?  Horse racing has more stats and information!  If you study that information (it's called handicapping) you increase your edge and likelihood that you will win your bets.</p>

<p>Want to know the difference between US Racing and all those offshore sites?  Well, for one, you are NOT breaking the law when you bet with us!  Two, your funds are safe in a United States bank.  Offshore accounts that can be shut down by the government.  Three, you get paid out at full track odds.  What does this mean? It means that you get paid YOUR money.  Offshore sites have to cap the amount of money they pay you.  That is why you will see that a website offers 300 – 1 or 600-1 payouts.  At US Racing you get paid what you are owed.  Period.  End of story.</p>

<p>Guess what? If you had placed a Pick Six wager (that is another type of horse bet) like the guy who did in the 2003 Breeders' Cup (an $8 dollar bet)—you would have won $2.6 MILLION.  Want to know how much you would have been paid by US Racing? All of it. $2.6 Million. </p>

<p>How much would you have gotten if you played offshore? $8,000 maybe $25,000 if you were lucky.  Then, with an offshore site, you have to wait and wait and wait for your money to be sent.  Typically it takes 8 weeks or more!  Two months to get paid? </p>

<p>At US Racing you get paid in 3 business days.  What about watching the races?  Offshore sites can't offer it—yes, that old legality issue cropping up again.  At US Racing you can watch the live horse racing on your computer, iPad, iPhone or other mobile device.  Make the right choice to bet on horses with US Racing.</p>


</div><!-- end:container -->
</div>