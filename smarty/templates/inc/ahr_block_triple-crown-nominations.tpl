{literal}<style type="text/css">
#infoEntries td { padding: 7px; }
#infoEntries th { padding:7px; line-height:13px; }
</style>{/literal}
<table id="infoEntries" class="data" cellspacing="1">
    <tbody>
        <tr>
            <th>Horse</th>
            <th>Sire</th>
            <th>Owner</th>
            <th>Trainer</th>
            <th>State Bred</th>
            <th>Best Beyer</th>
        </tr>
        <tr>
            <td>Abjer (FR)</td>
            <td>Singspiel (IRE)</td>
            <td>Mohammed Al Shafar</td>
            <td>Clive E. Brittain</td>
            <td>FR</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Abra</td>
            <td>Elusive Quality</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Achaemenes</td>
            <td>Empire Maker</td>
            <td>Starlight Partners</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>39</td>
        </tr>
        <tr>
            <td>Admiral Rocket</td>
            <td>Successful Appeal</td>
            <td>Frank Fletcher Racing Operations, Inc.</td>
            <td>W. T. Howard</td>
            <td>FL</td>
            <td>70</td>
        </tr>
        <tr>
            <td>Adulare</td>
            <td>Flatter</td>
            <td>Wind River Stables</td>
            <td>George R. Arnold, II</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Afleet's Acclaim</td>
            <td>Afleet Alex</td>
            <td>Whitehall Stable</td>
            <td>Linda Rice</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Albergatti</td>
            <td>Unbridled's Song</td>
            <td>Stonestreet Stables LLC and Natrona Racing Stable LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Alexander Pope (IRE)</td>
            <td>Danehill Dancer (IRE)</td>
            <td>Magnier, Mrs. John, Tabor, Michael and Smith, Derrick</td>
            <td>Aidan P. O'Brien</td>
            <td>IRE</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Alternation</td>
            <td>Distorted Humor</td>
            <td>Pin Oak Stable</td>
            <td>Donnie K. Von Hemel</td>
            <td>KY</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Ameri Weber</td>
            <td>Ameri Valay</td>
            <td>Nancy H. Alberts</td>
            <td>Nancy H. Alberts</td>
            <td>MD</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Animal Kingdom</td>
            <td>Leroidesanimaux (BRZ)</td>
            <td>Team Valor International</td>
            <td>H. Graham Motion</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Annual Update</td>
            <td>Dynaformer</td>
            <td>Twin Creeks Farm</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>62</td>
        </tr>
        <tr>
            <td>Anthony's Cross</td>
            <td>Indian Charlie</td>
            <td>Arianne De Kwiatkowski</td>
            <td>Eoin G. Harty</td>
            <td>PA</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Apply</td>
            <td>First Samurai</td>
            <td>Williford, Ward and Roberta</td>
            <td>James Lloyd</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Arch Traveler</td>
            <td>Sky Mesa</td>
            <td>Centennial Farms</td>
            <td>James Jerkens</td>
            <td>KY</td>
            <td>83</td>
        </tr>
        <tr>
            <td>Archarcharch</td>
            <td>Arch</td>
            <td>Robert Yagos</td>
            <td>William H. Fires</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Ari C</td>
            <td>Stormy Atlantic</td>
            <td>Zayat Stables, LLC</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Artful Run</td>
            <td>Artie Schiller</td>
            <td>Preston Stables LLC</td>
            <td>Dale L. Romans</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Arthur's Tale</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Associate</td>
            <td>Wando</td>
            <td>Whitehall Stable</td>
            <td>Linda Rice</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Astrology</td>
            <td>A.P. Indy</td>
            <td>Stonestreet Stables and Bolton, George</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Atlantic Bull</td>
            <td>Stormy Atlantic</td>
            <td>Just For Fun Stable</td>
            <td>Juan D. Arias</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>August Osage</td>
            <td>Indian Charlie</td>
            <td>Frankel, Jerry and Ronald</td>
            <td>Mark A. Hennig</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Awed</td>
            <td>El Prado (IRE)</td>
            <td>William M. Backer</td>
            <td>Barclay Tagg</td>
            <td>KY</td>
            <td>57</td>
        </tr>
        <tr>
            <td>Awesome Bet</td>
            <td>Awesome Again</td>
            <td>Mike McCarty</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>69</td>
        </tr>
        <tr>
            <td>Awesome Patriot</td>
            <td>Awesome Again</td>
            <td>Lanni Family Trust and Schiappa, Bernard C.</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>B F Bird</td>
            <td>Birdstone</td>
            <td>Robert V. LaPenta</td>
            <td>Nicholas P. Zito</td>
            <td>VA</td>
            <td>48</td>
        </tr>
        <tr>
            <td>Back Room Deal</td>
            <td>Maria's Mon</td>
            <td>Francis N. Sanna</td>
            <td>Ralph E. Nicks</td>
            <td>KY</td>
            <td>55</td>
        </tr>
        <tr>
            <td>Balladry</td>
            <td>Unbridled's Song</td>
            <td>Darley Stable</td>
            <td>Eoin G. Harty</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Bandbox</td>
            <td>Tapit</td>
            <td>Hillwood Stable LLC</td>
            <td>Rodney enkins</td>
            <td>NY</td>
            <td>86</td>
        </tr>
        <tr>
            <td>Bayamo</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Beamer</td>
            <td>Vindication</td>
            <td>Jim Tafel LLC</td>
            <td>Carl A. Nafzger</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Become the Wind</td>
            <td>It's No Joke</td>
            <td>L-Bo Racing and Summer of Fun Stables LLC</td>
            <td>A. C. Avila</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Bellamy's Boss</td>
            <td>Bellamy Road</td>
            <td>Kinsman Stable</td>
            <td>William Phipps</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Bench Points</td>
            <td>Benchmark</td>
            <td>Crevier, Donnie, Mariani, Linda, Martin, Charles and Zuraitis, Mary Jo</td>
            <td>Tim Yakteen</td>
            <td>CA</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Benergy</td>
            <td>Tapit</td>
            <td>Zayat Stables, LLC</td>
            <td>Dale L. Romans</td>
            <td>KY</td>
            <td>63</td>
        </tr>
        <tr>
            <td>Bert B Don</td>
            <td>Harlan's Holiday</td>
            <td>Monarch Stables, Inc.</td>
            <td>David Fawkes</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Big Herman</td>
            <td>Indian Charlie</td>
            <td>Lael Stables</td>
            <td>Barclay Tagg</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Big Mack Daddy</td>
            <td>Johannesburg</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>54</td>
        </tr>
        <tr>
            <td>Billy Smart</td>
            <td>Rockport Harbor</td>
            <td>Prime Equestrian S.A.R.L.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>39</td>
        </tr>
        <tr>
            <td>Birdway</td>
            <td>Street Cry (IRE)</td>
            <td>Marylou Whitney</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Black N Beauty</td>
            <td>Devil His Due</td>
            <td>Zayat Stables, LLC</td>
            <td>Dale L. Romans</td>
            <td>ON</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Blue Laser</td>
            <td>Bluegrass Cat</td>
            <td>WinStar Farm LLC</td>
            <td>Mark E. Casse</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Bluegrass Reward</td>
            <td>Good Reward</td>
            <td>Nicholas B. Alexander</td>
            <td>Steven Miyadi</td>
            <td>CA</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Bomber Boy</td>
            <td>Bernardini</td>
            <td>Robert V. LaPenta</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Bonaroo</td>
            <td>Dynaformer</td>
            <td>Bluegrass Hall LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Boys At Tosconova</td>
            <td>Officer</td>
            <td>Jay Em Ess Stable, Hot Racing Stable LLC, Hale, Jr., R., Cady, T., Kanakaris, D.</td>
            <td>Richard E. Dutrow Jr.</td>
            <td>KY</td>
            <td>102</td>
        </tr>
        <tr>
            <td>Br. Alexander</td>
            <td>Maria's Mon</td>
            <td>Peter J. Callahan</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Break Up the Game</td>
            <td>Bernardini</td>
            <td>Phipps Stable</td>
            <td>Claude R. McGaughey III</td>
            <td>KY</td>
            <td>83</td>
        </tr>
        <tr>
            <td>Brethren</td>
            <td>Distorted Humor</td>
            <td>WinStar Farm LLC</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Brickyard Fast</td>
            <td>Sharp Humor</td>
            <td>Baccari Racing Stable LLC</td>
            <td>Joseph Petalino</td>
            <td>KY</td>
            <td>83</td>
        </tr>
        <tr>
            <td>Brilliant Speed</td>
            <td>Dynaformer</td>
            <td>Live Oak Plantation</td>
            <td>Thomas Albertrani</td>
            <td>FL</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Brock</td>
            <td>Distorted Humor</td>
            <td>Stonestreet Stables LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>33</td>
        </tr>
        <tr>
            <td>Brooklyn Legend</td>
            <td>Congrats</td>
            <td>Fantasy Lane Stable</td>
            <td>Edward J. Coletti, Sr.</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Bryan Daniel</td>
            <td>Holy Bull</td>
            <td>Steve J. Gula</td>
            <td>Jerry Hollendorfer</td>
            <td>KY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Burns</td>
            <td>Unusual Heat</td>
            <td>M. Auerbach LLC and Pais, Alfred</td>
            <td>Barry Abrams</td>
            <td>CA</td>
            <td>74</td>
        </tr>
        <tr>
            <td>C J Russell</td>
            <td>El Corredor</td>
            <td>Michael E. Pegram</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Cal Nation</td>
            <td>Distorted Humor</td>
            <td>WinStar Farm LLC</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>93</td>
        </tr>
        <tr>
            <td>Caleb's Posse</td>
            <td>Posse</td>
            <td>McNeill Stables</td>
            <td>Donnie K. Von Hemel</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Cane Garden Bay</td>
            <td>Rockport Harbor</td>
            <td>Wind River Stables</td>
            <td>George R. Arnold, II</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Cash Michael</td>
            <td>Unbridled Energy</td>
            <td>Brewer Racing Stable, Shustek, Mike, Stanley, Don et al</td>
            <td>Jeff Mullins</td>
            <td>KY</td>
            <td>60</td>
        </tr>
        <tr>
            <td>Casino Host</td>
            <td>Dynaformer</td>
            <td>Gary and Mary West Stables, Inc.</td>
            <td>Chad C. Brown</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Casper's Touch</td>
            <td>Touch Gold</td>
            <td>Magdalena Racing (Susan McPeek, et al)</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Cassini Flight</td>
            <td>Bernardini</td>
            <td>Sanford R. Robertson</td>
            <td>Jeremy Noseda</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Cat Sweep</td>
            <td>Bluegrass Cat</td>
            <td>Curragh Stables, Covello, James and Juster, Gary</td>
            <td>John P. Terranova, II</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Charlies Swell</td>
            <td>Malibu Moon</td>
            <td>My Meadowview Farm</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Chasing Moonlight</td>
            <td>Grand Slam</td>
            <td>Leppala, Ricky and West Point Thoroughbreds</td>
            <td>Ricky Leppala</td>
            <td>KY</td>
            <td>55</td>
        </tr>
        <tr>
            <td>Chico d'Oro</td>
            <td>Medaglia d'Oro</td>
            <td>Joy Ride Racing</td>
            <td>John W. Sadler</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Classic Legacy</td>
            <td>Macho Uno</td>
            <td>Lanni Family Trust, Schiappa, B. and Jones, K.</td>
            <td>Bob Baffert</td>
            <td>FL</td>
            <td>87</td>
        </tr>
        <tr>
            <td>Clear Attempt</td>
            <td>A.P. Indy</td>
            <td>Wygod, Pam and Martin</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>61</td>
        </tr>
        <tr>
            <td>Close Ally</td>
            <td>Giant's Causeway</td>
            <td>Farish, William S. and Skara Glen Stables</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>69</td>
        </tr>
        <tr>
            <td>Cloud Man</td>
            <td>Thunder Gulch</td>
            <td>Moss, Mr. and Mrs. Jerome S.</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Clubhouse Ride</td>
            <td>Candy Ride (ARG)</td>
            <td>Six-S Racing Stable and Petralia, Nikolas</td>
            <td>Craig Anthony Lewis</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Coil</td>
            <td>Point Given</td>
            <td>Watson, Karl, Pegram, Michael E. and Weitman, Paul</td>
            <td>Bob Baffert</td>
            <td>FL</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Coming Through</td>
            <td>Dynaformer</td>
            <td>Larry A. Byer</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Comma to the Top</td>
            <td>Bwana Charlie</td>
            <td>Barber, Gary, Birnbaum, Roger and Tsujihara, Kevin</td>
            <td>Peter Miller</td>
            <td>FL</td>
            <td>95</td>
        </tr>
        <tr>
            <td>Commander</td>
            <td>Broken Vow</td>
            <td>Fox Hill Farms, Inc.</td>
            <td>J. Larry Jones</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Commonwealth Rush</td>
            <td>Afleet Alex</td>
            <td>Bluegrass Legacy Equine</td>
            <td>Eddie Kenneally</td>
            <td>KY</td>
            <td>69</td>
        </tr>
        <tr>
            <td>Complete Dyno</td>
            <td>Dynaformer</td>
            <td>Mignon C. Smith</td>
            <td>Robert B. Hess, Jr.</td>
            <td>VA</td>
            <td>7</td>
        </tr>
        <tr>
            <td>Concealed Identity</td>
            <td>Smarty Jones</td>
            <td>Gaudet, Linda S. and Bailey, Morris</td>
            <td>Edmond D. Gaudet</td>
            <td>MD</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Conway</td>
            <td>El Prado (IRE)</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>21</td>
        </tr>
        <tr>
            <td>Cook Inlet</td>
            <td>Candy Ride (ARG)</td>
            <td>Wygod, Pam and Martin</td>
            <td>John W. Sadler</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Cool Blue Red Hot</td>
            <td>Harlan's Holiday</td>
            <td>Bakke, Jim and Isbister, Gerry</td>
            <td>Angel J. Penna, Jr.</td>
            <td>KY</td>
            <td>96</td>
        </tr>
        <tr>
            <td>Crimson China</td>
            <td>Giant's Causeway</td>
            <td>Team Valor International</td>
            <td>H. Graham Motion</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Crossbow</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>86</td>
        </tr>
        <tr>
            <td>Crossed the Line</td>
            <td>Silver Train</td>
            <td>Sallusto, Nick and Bulger, Joseph</td>
            <td>Justin Sallusto</td>
            <td>KY</td>
            <td>70</td>
        </tr>
        <tr>
            <td>Crushing</td>
            <td>Giacomo</td>
            <td>Joseph Ricelli</td>
            <td>Eddie Kenneally</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Cryin Out Loud</td>
            <td>Street Cry (IRE)</td>
            <td>Roseland Farm Stable (Bowers)</td>
            <td>John J. Tammaro, III</td>
            <td>NJ</td>
            <td>65</td>
        </tr>
        <tr>
            <td>Da Ruler</td>
            <td>Roman Ruler</td>
            <td>Watson and Weitman Performances LLC</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>92</td>
        </tr>
        <tr>
            <td>Dance City</td>
            <td>City Zip</td>
            <td>Estate of Edward P. Evans</td>
            <td>Tod A. Pletcher</td>
            <td>VA</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Data Link</td>
            <td>War Front</td>
            <td>Stuart S. Janney III</td>
            <td>Claude R. McGaughey III</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Death Star</td>
            <td>Quiet American</td>
            <td>St. George Farm Racing LLC (Banwell)</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Decisive Moment</td>
            <td>With Distinction</td>
            <td>Just For Fun Stable</td>
            <td>Juan D. Arias</td>
            <td>FL</td>
            <td>91</td>
        </tr>
        <tr>
            <td>Denny the Great</td>
            <td>Uncle Denny</td>
            <td>Wicall, Buck and Luanne</td>
            <td>Robert B. Hess, Jr.</td>
            <td>CA</td>
            <td>61</td>
        </tr>
        <tr>
            <td>Depeche Chat</td>
            <td>Wildcat Heir</td>
            <td>Wind River Stables</td>
            <td>George R. Arnold, II</td>
            <td>FL</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Derby Kitten</td>
            <td>Kitten's Joy</td>
            <td>Ramsey, Kenneth L. and Sarah K.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Derby Obsession</td>
            <td>Ten Centuries</td>
            <td>Ramsey, Kenneth L. and Sarah K.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>65</td>
        </tr>
        <tr>
            <td>Derivative</td>
            <td>Mr. Greeley</td>
            <td>Baker, Robert C. and Mack, WIlliam L.</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Dialed In</td>
            <td>Mineshaft</td>
            <td>Robert V. LaPenta</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>97</td>
        </tr>
        <tr>
            <td>Disfraz</td>
            <td>Lemon Drop Kid</td>
            <td>Dr. Jose Birrell, Jr.</td>
            <td>Eddie Kenneally</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Distorted Appeal</td>
            <td>Distorted Humor</td>
            <td>Golden Dome Stable</td>
            <td>Mark Shuman</td>
            <td>KY</td>
            <td>63</td>
        </tr>
        <tr>
            <td>Dixon Lane</td>
            <td>Pulpit</td>
            <td>TYB Stable</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>D'marin</td>
            <td>More Than Ready</td>
            <td>Courtlandt Farms</td>
            <td>James E. Baker</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Doc La Pointe</td>
            <td>Even the Score</td>
            <td>Willey, Bill and Connie</td>
            <td>Anthony J. Battaglia</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Dominus</td>
            <td>Smart Strike</td>
            <td>Bolton, George, Stonestreet Stables and Spendthrift Farm</td>
            <td>Steven M. Asmussen</td>
            <td>VA</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Doubledown On Ten</td>
            <td>Rock Hard Ten</td>
            <td>Doubledown Stables, Inc.</td>
            <td>John W. Sadler</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Dream Drop Kid</td>
            <td>Lemon Drop Kid</td>
            <td>WellSpring Stables</td>
            <td>Dominic G. Galluscio</td>
            <td>NY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Dreamy Kid</td>
            <td>Lemon Drop Kid</td>
            <td>Robert S. Evans</td>
            <td>Neil D. Drysdale</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Dubber</td>
            <td>With Distinction</td>
            <td>Donver Stable</td>
            <td>Bob Baffert</td>
            <td>FL</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Duca</td>
            <td>Empire Maker</td>
            <td>Baker, Robert C. and Mack, WIlliam L.</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>63</td>
        </tr>
        <tr>
            <td>Dynamize</td>
            <td>Dynaformer</td>
            <td>Slex Solis Jr.</td>
            <td>Mike Puype</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Eastside Train</td>
            <td>Silver Train</td>
            <td>Kinsman Stable</td>
            <td>Carl J. Domino</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Economic Summit</td>
            <td>Malibu Moon</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>FL</td>
            <td>83</td>
        </tr>
        <tr>
            <td>El Grayling</td>
            <td>El Prado (IRE)</td>
            <td>Conway, F. Thomas and Matt and Hemberger, Steve</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Elite Alex</td>
            <td>Afleet Alex</td>
            <td>Elite Alex LLC</td>
            <td>Timothy F. Ritchey</td>
            <td>ON</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Energized</td>
            <td>Lemon Drop Kid</td>
            <td>Golden Goose Enterprise LLC</td>
            <td>John P. Terranova, II</td>
            <td>KY</td>
            <td>64</td>
        </tr>
        <tr>
            <td>Eurasian</td>
            <td>Bernstein</td>
            <td>Sheehy LLC</td>
            <td>Carl O'Callaghan</td>
            <td>MN</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Extra Fifty</td>
            <td>Afleet Alex</td>
            <td>L-Bo Racing, Pyle, Monte and Summer of Fun Racing, LLC</td>
            <td>A. C. Avila</td>
            <td>KY</td>
            <td>55</td>
        </tr>
        <tr>
            <td>Factum</td>
            <td>Storm Cat</td>
            <td>Joseph Allen</td>
            <td>Neil D. Drysdale</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Fad</td>
            <td>Tiznow</td>
            <td>Lael Stables</td>
            <td>Barclay Tagg</td>
            <td>KY</td>
            <td>59</td>
        </tr>
        <tr>
            <td>Fairview Heights</td>
            <td>Distorted Humor</td>
            <td>Melnyk Racing Stables, Inc.</td>
            <td>Todd A. Pletcher</td>
            <td>FL</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Fantasy of Flight (f)</td>
            <td>Tiznow</td>
            <td>IEAH Stables</td>
            <td>John P. Terranova, II</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Fire With Fire</td>
            <td>Distorted Humor</td>
            <td>WinStar Farm LLC</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Flashpoint</td>
            <td>Pomeroy</td>
            <td>Peachtree Stable</td>
            <td>Richard E. Dutrow Jr.</td>
            <td>FL</td>
            <td>91</td>
        </tr>
        <tr>
            <td>Fly On the Wall</td>
            <td>Artie Schiller</td>
            <td>Ramsey, Kenneth L. and Sarah K.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>23</td>
        </tr>
        <tr>
            <td>Foreboding</td>
            <td>Tiznow</td>
            <td>Darley Stable</td>
            <td>Eoin G. Harty</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Fort Hughes</td>
            <td>Henny Hughes</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>104</td>
        </tr>
        <tr>
            <td>Fort Larned</td>
            <td>E Dubai</td>
            <td>Janis R. Whitham</td>
            <td>Ian R. Wilkes</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Forum</td>
            <td>Roman Ruler</td>
            <td>Dogwood Stable</td>
            <td>George Weaver</td>
            <td>KY</td>
            <td>83</td>
        </tr>
        <tr>
            <td>Free Entry</td>
            <td>Tale of the Cat</td>
            <td>Gary and Mary West Stables, Inc.</td>
            <td>Chad C. Brown</td>
            <td>VA</td>
            <td>83</td>
        </tr>
        <tr>
            <td>Free Pourin</td>
            <td>Roman Ruler</td>
            <td>Michael E. Pegram</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Free Ticket</td>
            <td>Freefourinternet</td>
            <td>Bluegrass Equine Bloodstock LLC</td>
            <td>Robin Parks</td>
            <td>FL</td>
            <td>-</td>
        </tr>
        <tr>
            <td>French Fury</td>
            <td>Mineshaft</td>
            <td>Richard C. Pell</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Fusa Code</td>
            <td>Fusaichi Pegasus</td>
            <td>Zollars, M. Cathy and R.L. Bob</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>G Ten</td>
            <td>Giacomo</td>
            <td>Moss, Mr. and Mrs. Jerome S.</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Gallant Dreams</td>
            <td>Bernardini</td>
            <td>Live Oak Plantation</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>66</td>
        </tr>
        <tr>
            <td>Glint</td>
            <td>Sharp Humor</td>
            <td>TK Stables (Hulse)</td>
            <td>Kellyn Gorder</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Gnarly Dude</td>
            <td>Mr. Greeley</td>
            <td>Kretz, Juliana and Rustin</td>
            <td>Kristin Mulhall</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Golden Triumph</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Gourmet Dinner</td>
            <td>Trippi</td>
            <td>William Terrill</td>
            <td>Steven W. Standridge</td>
            <td>FL</td>
            <td>94</td>
        </tr>
        <tr>
            <td>Grace's Devil</td>
            <td>Devil His Due</td>
            <td>Charles V. Collins</td>
            <td>Vernon D. Coyle</td>
            <td>KY</td>
            <td>45</td>
        </tr>
        <tr>
            <td>Grande Shores</td>
            <td>Black Mambo</td>
            <td>Jacks or Better Farm, Inc.</td>
            <td>Stanley I. Gold</td>
            <td>FL</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Grip Hands</td>
            <td>Giant's Causeway</td>
            <td>West Point Thoroughbreds</td>
            <td>Craig Dollase</td>
            <td>KY</td>
            <td>55</td>
        </tr>
        <tr>
            <td>Guest Star</td>
            <td>Storm Cat</td>
            <td>George Krikorian</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Guy On the Go</td>
            <td>Tapit</td>
            <td>Duignan, Gabriel, McElroy, Ben and McMahon, Mike</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Gypsy Wind</td>
            <td>Millennium Wind</td>
            <td>Ramsey, Kenneth L. and Sarah K.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>62</td>
        </tr>
        <tr>
            <td>Hammersmith</td>
            <td>Johannesburg</td>
            <td>Michael B. Tabor</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Harlan's Hello</td>
            <td>Harlan's Holiday</td>
            <td>Shivmangal Racing Stable</td>
            <td>Doodnauth Shivmangal</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Heat Shield</td>
            <td>Distorted Humor</td>
            <td>Juddmonte Farms, Inc.</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Heron Lake</td>
            <td>Bernardini</td>
            <td>Robert V. LaPenta</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Hoorayforhollywood</td>
            <td>Storm Cat</td>
            <td>George Krikorian</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Hot Faucet</td>
            <td>Tapit</td>
            <td>Zollars, M. Cathy and R.L. Bob</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Houston Harbor</td>
            <td>Rockport Harbor</td>
            <td>Stonestreet Stables LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>86</td>
        </tr>
        <tr>
            <td>Humble and Hungry</td>
            <td>Limehouse</td>
            <td>Sagamore Farm</td>
            <td>Ignacio Correas, IV</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Hy Lime Time</td>
            <td>Limehouse</td>
            <td>Roger. S. Sofer</td>
            <td>Dallas E. Keen</td>
            <td>KY</td>
            <td>58</td>
        </tr>
        <tr>
            <td>I'm Steppin' It Up</td>
            <td>Congrats</td>
            <td>Roman Hill Farm LLC</td>
            <td>Anthony Pecoraro</td>
            <td>FL</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Imhotep</td>
            <td>Giant's Causeway</td>
            <td>Zayat Stables, LLC</td>
            <td>Todd A. Pletcher</td>
            <td>ON</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Indian Knight</td>
            <td>Indian Charlie</td>
            <td>D and E Racing LLC</td>
            <td>Bob Baffert</td>
            <td>VA</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Indian Winter</td>
            <td>Indian Charlie</td>
            <td>Abruzzo, Peter, Carver, John, Hollendorfer, Jerry and Todaro, George</td>
            <td>Jerry Hollendorfer</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Industry Leader</td>
            <td>Cherokee Run</td>
            <td>Dimitri Racing Stable, Inc. and Twilight Racing, LLC</td>
            <td>Kristin Mulhall</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Indy Heir</td>
            <td>A.P. Indy</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>54</td>
        </tr>
        <tr>
            <td>Indy Tune</td>
            <td>Suave</td>
            <td>Blue Streak Stable and Sinatra Stables</td>
            <td>Edward T. Allard</td>
            <td>NY</td>
            <td>50</td>
        </tr>
        <tr>
            <td>Infrattini</td>
            <td>Include</td>
            <td>Z Thoroughbreds, LLC</td>
            <td>Paul J. McGee</td>
            <td>KY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Iscar</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>66</td>
        </tr>
        <tr>
            <td>Isn't He Perfect</td>
            <td>Pleasantly Perfect</td>
            <td>Kharag Stables</td>
            <td>Doodnauth Shivmangal</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>J J's Lucky Train</td>
            <td>Silver Train</td>
            <td>Fresh Start Stable</td>
            <td>William D. Anderson</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>J P's Gusto</td>
            <td>Successful Appeal</td>
            <td>Gem, Inc. (Waken)</td>
            <td>David E. Hofmans</td>
            <td>KY</td>
            <td>92</td>
        </tr>
        <tr>
            <td>Jack London</td>
            <td>Tale of the Cat</td>
            <td>William M. Backer</td>
            <td>Barclay Tagg</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Jakesam</td>
            <td>Smoke Glacken</td>
            <td>Bourque Goldstein Thoroughbreds LLC</td>
            <td>Jerry Hollendorfer</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Jaycito</td>
            <td>Victory Gallop</td>
            <td>Zayat Stables, LLC</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Jeanbaptiste Corot</td>
            <td>Mr. Greeley</td>
            <td>Baum, Michael and Reiko</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Just Meteor</td>
            <td>Forestry</td>
            <td>Wertheimer and Frere</td>
            <td>Richard E. Mandella</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Justin Phillip</td>
            <td>First Samurai</td>
            <td>Zayat Stables, LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>93</td>
        </tr>
        <tr>
            <td>Kanagaro</td>
            <td>Indian Charlie</td>
            <td>West Point Thoroughbreds</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Kid You Not</td>
            <td>Lemon Drop Kid</td>
            <td>Poindexter Thoroughbreds LLC</td>
            <td>Allen Milligan</td>
            <td>KY</td>
            <td>44</td>
        </tr>
        <tr>
            <td>King Alpha</td>
            <td>Tiznow</td>
            <td>William H. Lawrence</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>64</td>
        </tr>
        <tr>
            <td>King Congie</td>
            <td>Badge of Silver</td>
            <td>West Point Thoroughbreds</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Lauburu</td>
            <td>Unbridled's Song</td>
            <td>Tabor, Michael B., Magnier, Mrs. John, Scatuorchio, James T. and Smith, Derrick</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>70</td>
        </tr>
        <tr>
            <td>Le Mans</td>
            <td>War Front</td>
            <td>Padua Stables</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>66</td>
        </tr>
        <tr>
            <td>Leave of Absence</td>
            <td>Harlan's Holiday</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>FL</td>
            <td>94</td>
        </tr>
        <tr>
            <td>Lemansky</td>
            <td>Lemon Drop Kid</td>
            <td>JC Summit Farm</td>
            <td>Allen Crupper</td>
            <td>NY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Lemon Ghost</td>
            <td>Lemon Drop Kid</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>16</td>
        </tr>
        <tr>
            <td>Liondrive</td>
            <td>Lion Tamer</td>
            <td>Amenta, Thomas and Amenta, II, Tom</td>
            <td>Tim A. Ice</td>
            <td>LA</td>
            <td>62</td>
        </tr>
        <tr>
            <td>Litigate</td>
            <td>Closing Argument</td>
            <td>Silverton Hill LLC</td>
            <td>David P. Duggan</td>
            <td>NY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Lou Brissie</td>
            <td>Limehouse</td>
            <td>Dogwood Stable</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Lumberyard Jack</td>
            <td>Bellamy Road</td>
            <td>Joel Meredith</td>
            <td>Michael Stidham</td>
            <td>IL</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Mach Twelve</td>
            <td>Rock Hard Ten</td>
            <td>Mercedes Stables LLC</td>
            <td>Anthony W. Dutrow</td>
            <td>KY</td>
            <td>53</td>
        </tr>
        <tr>
            <td>Machen</td>
            <td>Distorted Humor</td>
            <td>Courtlandt Farms</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>95</td>
        </tr>
        <tr>
            <td>Mac's Surprise</td>
            <td>Stormy Atlantic</td>
            <td>Dickerson, Jack and Fromby, Buddy</td>
            <td>Richard D. Jackson</td>
            <td>VA</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Maestro</td>
            <td>Malibu Moon</td>
            <td>Moss, Mr. and Mrs. Jerome S.</td>
            <td>John A. Shirreffs</td>
            <td>MD</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Magnet Cove</td>
            <td>Aptitude</td>
            <td>Edwin C. Anthony</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>66</td>
        </tr>
        <tr>
            <td>Majestic Harbor</td>
            <td>Rockport Harbor</td>
            <td>Gallant Stable</td>
            <td>Paul J. McGee</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Major Art (GB)</td>
            <td>Compton Place (GB)</td>
            <td>Class Racing Stable</td>
            <td>J. Eric Kruljac</td>
            <td>GB</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Major Duomo</td>
            <td>Bernardini</td>
            <td>Baker, Robert C. and Mack, WIlliam L.</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>37</td>
        </tr>
        <tr>
            <td>Major Gain</td>
            <td>More Than Ready</td>
            <td>Gary and Mary West Stables, Inc.</td>
            <td>Wayne M. Catalano</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Manhattan Man</td>
            <td>A.P. Indy</td>
            <td>Baker, Robert C. and Mack, WIlliam L.</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>43</td>
        </tr>
        <tr>
            <td>Manicero</td>
            <td>Mass Media</td>
            <td>Leo Azpurua Sr.</td>
            <td>Leo Azpurua, Jr.</td>
            <td>FL</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Manresa Road (IRE)</td>
            <td>Giant's Causeway</td>
            <td>Magnier, Mrs. John, Tabor, Michael and Smith, Derrick</td>
            <td>Todd A. Pletcher</td>
            <td>IRE</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Mas Trueno</td>
            <td>Afleet Alex</td>
            <td>Bach Stables LLC</td>
            <td>William Phipps</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Master Dunker</td>
            <td>Imperialism</td>
            <td>Get Away Farm Racing Stable</td>
            <td>David Fawkes</td>
            <td>FL</td>
            <td>85</td>
        </tr>
        <tr>
            <td>Master of Hounds</td>
            <td>Kingmambo</td>
            <td>Mrs. John Magnier</td>
            <td>Aidan P. O'Brien</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Meadow Road</td>
            <td>Dixie Union</td>
            <td>Diamond A Racing Corporation</td>
            <td>Richard E. Mandella</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Meistersinger</td>
            <td>Yes It's True</td>
            <td>Team Valor International and Barber, Gary</td>
            <td>H. Graham Motion</td>
            <td>KY</td>
            <td>57</td>
        </tr>
        <tr>
            <td>Messner</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>63</td>
        </tr>
        <tr>
            <td>Midnight Interlude</td>
            <td>War Chant</td>
            <td>Arnold Zetcher LLC</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>67</td>
        </tr>
        <tr>
            <td>Mister Pippit</td>
            <td>Tapit</td>
            <td>Whitehall Stable</td>
            <td>Seth Benzel</td>
            <td>FL</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Monroe's Music</td>
            <td>Candy Ride (ARG)</td>
            <td>Flavin, Patrick J., Shaw, James and Pincins, Robert J.</td>
            <td>Robert J. Pincins</td>
            <td>KY</td>
            <td>42</td>
        </tr>
        <tr>
            <td>Mont Pelato</td>
            <td>Forest Danger</td>
            <td>Frederic Sauque</td>
            <td>Christophe Ferland</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Monzon</td>
            <td>Thunder Gulch</td>
            <td>Sagamore Farm</td>
            <td>Ignacio Correas, IV</td>
            <td>MD</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Moon On Fire</td>
            <td>Malibu Moon</td>
            <td>Robert V. LaPenta</td>
            <td>Nicholas P. Zito</td>
            <td>MD</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Moonhanger</td>
            <td>Malibu Moon</td>
            <td>Bruce F. Alexander</td>
            <td>Bruce F. Alexander</td>
            <td>KY</td>
            <td>69</td>
        </tr>
        <tr>
            <td>Moreno Star</td>
            <td>Quiet American</td>
            <td>Southern Equine Stable LLC</td>
            <td>Eric J. Guillot</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Mr Artistic M D</td>
            <td>Mr. Greeley</td>
            <td>Mark Dedomenico LLC</td>
            <td>Kathy Walsh</td>
            <td>KY</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Mr. Commons</td>
            <td>Artie Schiller</td>
            <td>St. George Farm Racing LLC (Banwell)</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Mucho Macho Man</td>
            <td>Macho Uno</td>
            <td>Reeves Thoroughbred Racing and Dream Team One Racing Stable</td>
            <td>Katherine Ritvo</td>
            <td>FL</td>
            <td>99</td>
        </tr>
        <tr>
            <td>My Dividend</td>
            <td>Bernardini</td>
            <td>Tarabilla Farms, Inc.</td>
            <td>David E. Hofmans</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Mysticism</td>
            <td>A. P. Warrior</td>
            <td>Darley Stable</td>
            <td>Thomas Albertrani</td>
            <td>KY</td>
            <td>41</td>
        </tr>
        <tr>
            <td>Nacho Business</td>
            <td>Rahy</td>
            <td>Hall, George and Lori</td>
            <td>Kelly J. Breen</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>National</td>
            <td>Awesome Again</td>
            <td>Moss, Mr. and Mrs. Jerome S.</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Nehro</td>
            <td>Mineshaft</td>
            <td>Zayat Stables, LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>65</td>
        </tr>
        <tr>
            <td>New Hyde Park (f)</td>
            <td>Expressionist</td>
            <td>John Michalik</td>
            <td>Daniel Dunham</td>
            <td>CA</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Night Hunt</td>
            <td>Storm Cat</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>12</td>
        </tr>
        <tr>
            <td>Night Party</td>
            <td>Rahy</td>
            <td>Camelia J. Casby</td>
            <td>Patrick Huffman</td>
            <td>KY</td>
            <td>57</td>
        </tr>
        <tr>
            <td>Nolangrant'skitten</td>
            <td>Kitten's Joy</td>
            <td>Ramsey, Kenneth L. and Sarah K.</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Northern Indy</td>
            <td>A.P. Indy</td>
            <td>Highland Yard LLC</td>
            <td>O. J. Jauregui</td>
            <td>KY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Occelli</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>44</td>
        </tr>
        <tr>
            <td>Old Guys Rule</td>
            <td>Malibu Moon</td>
            <td>PHS Racing Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>NY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Old Hickory</td>
            <td>Kela</td>
            <td>Flying Dutchman Thoroughbreds</td>
            <td>Tim A. Ice</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>One Cool Dude (ARG)</td>
            <td>Incurable Optimist</td>
            <td>Behrendt, John T. and Marquis, Charles K.</td>
            <td>Michael R. Matz</td>
            <td>ARG</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Opening Move</td>
            <td>Bernardini</td>
            <td>Darley Stable</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>86</td>
        </tr>
        <tr>
            <td>Ordained</td>
            <td>Pulpit</td>
            <td>Westrock Stables, LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>68</td>
        </tr>
        <tr>
            <td>Orsonian</td>
            <td>Officer</td>
            <td>James C. Spence</td>
            <td>Ralph E. Nicks</td>
            <td>KY</td>
            <td>58</td>
        </tr>
        <tr>
            <td>Our Eli</td>
            <td>Ten Most Wanted</td>
            <td>Aaron Racing Stables</td>
            <td>Bruce R. Brown</td>
            <td>NY</td>
            <td>55</td>
        </tr>
        <tr>
            <td>Pants On Fire</td>
            <td>Jump Start</td>
            <td>Hall, George and Lori</td>
            <td>Kelly J. Breen</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Parent's Honor</td>
            <td>Elusive Quality</td>
            <td>Warren, Jr., Mr. and Mrs. William K.</td>
            <td>Anthony W. Dutrow</td>
            <td>KY</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Perfect Coconut</td>
            <td>Pleasantly Perfect</td>
            <td>Shivmangal Racing Stable</td>
            <td>Doodnauth Shivmangal</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Peter Martins</td>
            <td>Johannesburg</td>
            <td>Earle I. Mack</td>
            <td>Jeremy Noseda</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Pleasant Run</td>
            <td>Pleasant Tap</td>
            <td>Chasing Dreams Racing 2009, LLC</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Point of Entry</td>
            <td>Dynaformer</td>
            <td>Phipps Stable</td>
            <td>Claude R. McGaughey 3rd</td>
            <td>KY</td>
            <td>64</td>
        </tr>
        <tr>
            <td>Populist Politics</td>
            <td>Don't Get Mad</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Thomas M. Amoss</td>
            <td>LA</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Poseidon's Warrior</td>
            <td>Speightstown</td>
            <td>Cash is King LLC</td>
            <td>Robert E. Reid, Jr.</td>
            <td>MD</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Positive Response</td>
            <td>Pomeroy</td>
            <td>Gevertz, Saul, Morey, William E., Newman, Roger and Pagano, Ray</td>
            <td>William E. Morey</td>
            <td>FL</td>
            <td>87</td>
        </tr>
        <tr>
            <td>Post Ranch</td>
            <td>Not For Love</td>
            <td>Robert S. Evans</td>
            <td>Seth Benzel</td>
            <td>MD</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Powhatan County</td>
            <td>Fusaichi Pegasus</td>
            <td>Richlyn Farms</td>
            <td>George Weaver</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Praetereo</td>
            <td>Giant's Causeway</td>
            <td>Starlight Partners</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Praise the Bird</td>
            <td>Pulpit</td>
            <td>Marylou Whitney</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Preachintothedevil</td>
            <td>Pulpit</td>
            <td>Crossed Sabres Farm</td>
            <td>Gary C. Contessa</td>
            <td>NY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Premier Pegasus</td>
            <td>Fusaichi Pegasus</td>
            <td>M.K. Cho Stables LLC</td>
            <td>Myung Kwon Cho</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Pride of Silver</td>
            <td>Badge of Silver</td>
            <td>ITA Thoroughbreds, Schroeder Farms LLC and Sigband, Michael</td>
            <td>Robert B. Hess, Jr.</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Prime Cut</td>
            <td>Bernstein</td>
            <td>Courtlandt Farms</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Prime Objective</td>
            <td>Aragorn (IRE)</td>
            <td>Cash Counters LLC and Ritchey, Timothy F.</td>
            <td>Timothy F. Ritchey</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Printing Press</td>
            <td>Tapit</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Teresa M. Pompay</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Private Prize</td>
            <td>Pure Prize</td>
            <td>Magdalena Racing (Susan McPeek, et al)</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Purely Awesome</td>
            <td>Awesome Again</td>
            <td>Kirkwood, Al and Saundra S.</td>
            <td>Kathy Walsh</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Quail Hill</td>
            <td>Candy Ride (ARG)</td>
            <td>Tarabilla Farms, Inc.</td>
            <td>David E. Hofmans</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Queen'splatekitten</td>
            <td>Kitten's Joy</td>
            <td>Mill House</td>
            <td>Todd A. Pletcher</td>
            <td>ON</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Quiet Assault</td>
            <td>Quiet American</td>
            <td>Bluegrass Hall LLC</td>
            <td>Jerry Hollendorfer</td>
            <td>KY</td>
            <td>61</td>
        </tr>
        <tr>
            <td>Quiet Pride</td>
            <td>Quiet American</td>
            <td>Two Bucks Stable</td>
            <td>Jonathan E. Sheppard</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Racing Aptitude</td>
            <td>Aptitude</td>
            <td>Donald R. Dizney</td>
            <td>Howard M. Tesher</td>
            <td>FL</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Raison d'Etat</td>
            <td>A.P. Indy</td>
            <td>Juddmonte Farms, Inc.</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>76</td>
        </tr>
        <tr>
            <td>Rattlesnake Bridge</td>
            <td>Tapit</td>
            <td>Mill House</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Razmataz</td>
            <td>Forest Wildcat</td>
            <td>Let's Go Stable</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Read the Contract</td>
            <td>Read the Footnotes</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>NY</td>
            <td>98</td>
        </tr>
        <tr>
            <td>Red Ace</td>
            <td>Northern Afleet</td>
            <td>Peachtree Stable</td>
            <td>Steve Margolis</td>
            <td>ON</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Red Maserati</td>
            <td>Songandaprayer</td>
            <td>Peachtree Stable</td>
            <td>Steve Margolis</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Redwood Falls</td>
            <td>Indian Charlie</td>
            <td>Michael B. Tabor</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>64</td>
        </tr>
        <tr>
            <td>Rescind the Trade</td>
            <td>Put It Back</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>FL</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Ribo Bobo</td>
            <td>Louis Quatorze</td>
            <td>Rapputi Stables LLC, Pencheff, Dimitar and Carlota Stable</td>
            <td>Manuel J. Azpurua</td>
            <td>FL</td>
            <td>74</td>
        </tr>
        <tr>
            <td>Riveting Reason</td>
            <td>Fusaichi Pegasus</td>
            <td>M.K. Cho Stables LLC</td>
            <td>Myung Kwon Cho</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Robie the Cat</td>
            <td>Distorted Humor</td>
            <td>Diamond A Racing Corporation</td>
            <td>Richard E. Mandella</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Rock So Hard</td>
            <td>Rock Hard Ten</td>
            <td>BG Stable and Summit Racing LLC</td>
            <td>Bob Baffert</td>
            <td>FL</td>
            <td>61</td>
        </tr>
        <tr>
            <td>Rocking Out</td>
            <td>Include</td>
            <td>J.W. Singer LLC</td>
            <td>Richard E. Dutrow, Jr.</td>
            <td>KY</td>
            <td>87</td>
        </tr>
        <tr>
            <td>Rogue Romance</td>
            <td>Smarty Jones</td>
            <td>Catesby W. Clay</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Roman Classic</td>
            <td>Roman Ruler</td>
            <td>Audley Farm Stable</td>
            <td>Barclay Tagg</td>
            <td>VA</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Ronin Dax</td>
            <td>Tapit</td>
            <td>DeJulio, Bruno, Ho'o, Galen, Rosen, Molly Jo and Troeger, Robert</td>
            <td>Robert Troeger</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Rothko</td>
            <td>Arch</td>
            <td>Padua Stables</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Runflatout</td>
            <td>Flatter</td>
            <td>West Point Thoroughbreds</td>
            <td>John W. Sadler</td>
            <td>KY</td>
            <td>96</td>
        </tr>
        <tr>
            <td>Rustler Hustler</td>
            <td>Ecton Park</td>
            <td>Paul P. Pompa Jr.</td>
            <td>Richard E. Dutrow Jr.</td>
            <td>PA</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Sachem</td>
            <td>Smarty Jones</td>
            <td>Richard C. Pell</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>43</td>
        </tr>
        <tr>
            <td>Sampo</td>
            <td>Smart Strike</td>
            <td>Earle I. Mack</td>
            <td>H. Graham Motion</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>San Pablo</td>
            <td>Jump Start</td>
            <td>Dan Bearden</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>86</td>
        </tr>
        <tr>
            <td>Santiva</td>
            <td>Giant's Causeway</td>
            <td>Tom R. Walters</td>
            <td>Eddie Kenneally</td>
            <td>KY</td>
            <td>78</td>
        </tr>
        <tr>
            <td>Sasueno</td>
            <td>Broken Vow</td>
            <td>Casa Farms I, LLC</td>
            <td>Michelle Nihei</td>
            <td>KY</td>
            <td>65</td>
        </tr>
        <tr>
            <td>Scuff</td>
            <td>Arch</td>
            <td>Agave Racing Stable</td>
            <td>Michelle Lovell</td>
            <td>KY</td>
            <td>66</td>
        </tr>
        <tr>
            <td>Seal Rock</td>
            <td>Tiznow</td>
            <td>Joseph Hudson</td>
            <td>Neil D. Drysdale</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Sensational Slam</td>
            <td>Grand Slam</td>
            <td>Bobby Flay</td>
            <td>Todd A. Pletcher</td>
            <td>ON</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Sequoia Warrior</td>
            <td>Smart Strike</td>
            <td>Donald R. Dizney</td>
            <td>Dale L. Romans</td>
            <td>FL</td>
            <td>70</td>
        </tr>
        <tr>
            <td>Shackleford</td>
            <td>Forestry</td>
            <td>Lauffer, Michael and Cubbedge, W.D.</td>
            <td>Dale L. Romans</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Shadow Warrior</td>
            <td>A.P. Indy</td>
            <td>G. Watts Humphrey Jr.</td>
            <td>George R. Arnold, II</td>
            <td>KY</td>
            <td>84</td>
        </tr>
        <tr>
            <td>Shamino</td>
            <td>Elusive Quality</td>
            <td>Blue Vista, Inc.</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Silver Medallion</td>
            <td>Badge of Silver</td>
            <td>Michael J. Ryan</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Simbamangu</td>
            <td>Tale of the Cat</td>
            <td>Lael Stables</td>
            <td>Barclay Tagg</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Sinai</td>
            <td>Rockport Harbor</td>
            <td>Zayat Stables, LLC</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>90</td>
        </tr>
        <tr>
            <td>Sinorice</td>
            <td>Hold That Tiger</td>
            <td>My Meadowview Farm and Hurricane Boys Racing Stable</td>
            <td>Nicholas P. Zito</td>
            <td>KY</td>
            <td>67</td>
        </tr>
        <tr>
            <td>Sky Music</td>
            <td>Sky Mesa</td>
            <td>Dogwood Stable</td>
            <td>Todd A. Pletcher</td>
            <td>NY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Skylord</td>
            <td>Sky Mesa</td>
            <td>Patrick D. Donahoe</td>
            <td>William H. Fires</td>
            <td>KY</td>
            <td>91</td>
        </tr>
        <tr>
            <td>Slammer Time</td>
            <td>Grand Slam</td>
            <td>Alto Racing, LLC</td>
            <td>Gary Mandella</td>
            <td>CA</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Small Town Talk</td>
            <td>Yes It's True</td>
            <td>Copper Penny Stables</td>
            <td>James L. Lawrence, II</td>
            <td>KY</td>
            <td>60</td>
        </tr>
        <tr>
            <td>Smash</td>
            <td>Smart Strike</td>
            <td>Kaleem Shah</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Smug</td>
            <td>El Prado (IRE)</td>
            <td>Robert S. Evans</td>
            <td>Neil D. Drysdale</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>So Bold</td>
            <td>Royal Academy</td>
            <td>For Pete's Sake</td>
            <td>Joseph F. Orseno</td>
            <td>KY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Sockarooni</td>
            <td>Dixieland Band</td>
            <td>Steven J. Belford</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Soldat</td>
            <td>War Front</td>
            <td>Clarke, Harvey A. and Robertson III, W. Craig</td>
            <td>Kiaran P. McLaughlin</td>
            <td>KY</td>
            <td>103</td>
        </tr>
        <tr>
            <td>Son of a General</td>
            <td>Mineshaft</td>
            <td>Hermitage Farm LLC</td>
            <td>David R. Vance</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Sorgho</td>
            <td>Storm Cat</td>
            <td>Bennett, William Don and Bowling, Tony and Carl</td>
            <td>William Donennett</td>
            <td>KY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Southern Sculptor</td>
            <td>Langfuhr</td>
            <td>Southern Equine Stable LLC</td>
            <td>Eric J. Guillot</td>
            <td>KY</td>
            <td>61</td>
        </tr>
        <tr>
            <td>Sovereign Default</td>
            <td>Northern Afleet</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>FL</td>
            <td>94</td>
        </tr>
        <tr>
            <td>Special Tree</td>
            <td>Special Rate</td>
            <td>Stephen R. Baker</td>
            <td>Ralph R. Irwin</td>
            <td>TX</td>
            <td>64</td>
        </tr>
        <tr>
            <td>Sports Day</td>
            <td>Tiznow</td>
            <td>Courtlandt Farms</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>50</td>
        </tr>
        <tr>
            <td>Star Harbour</td>
            <td>Indian Charlie</td>
            <td>Peter Vegso</td>
            <td>William I. Mott</td>
            <td>FL</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Starliner</td>
            <td>Johar</td>
            <td>Paul Rothfuss</td>
            <td>Josep F. Orseno</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Stay Thirsty</td>
            <td>Bernardini</td>
            <td>Repole Stable</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Sterling Legend</td>
            <td>Bellamy Road</td>
            <td>Tom Wilson</td>
            <td>Robert B. Hess, Jr.</td>
            <td>KY</td>
            <td>47</td>
        </tr>
        <tr>
            <td>Stormberg</td>
            <td>Storm Cat</td>
            <td>William J. Sims</td>
            <td>Seth Benzel</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Strike Oil</td>
            <td>Forest Wildcat</td>
            <td>John C. Oxley</td>
            <td>Mark E. Casse</td>
            <td>ON</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Supreme Ruler</td>
            <td>Don't Get Mad</td>
            <td>Donald Weber</td>
            <td>William H. Fires</td>
            <td>KY</td>
            <td>72</td>
        </tr>
        <tr>
            <td>Surfactant</td>
            <td>Closing Argument</td>
            <td>Kendall E. Hansen</td>
            <td>Michael J. Maker</td>
            <td>NY</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Surrey Star (IRE)</td>
            <td>Dubawi (IRE)</td>
            <td>Class Racing Stable</td>
            <td>James M. Cassidy</td>
            <td>IRE</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Sway Away</td>
            <td>Afleet Alex</td>
            <td>Batman Stable, Hanna, M., Lebherz, P., Olsen, C., Sharp, J. and Wallace, G.</td>
            <td>Jeff Bonde</td>
            <td>KY</td>
            <td>91</td>
        </tr>
        <tr>
            <td>Sweet Ducky</td>
            <td>Pulpit</td>
            <td>Hall, George and Lori</td>
            <td>Kelly J. Breen</td>
            <td>KY</td>
            <td>94</td>
        </tr>
        <tr>
            <td>Tale of the Dance</td>
            <td>Tale of the Cat</td>
            <td>David E. Garner</td>
            <td>Ralph E. Nicks</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Tapaway</td>
            <td>Tapit</td>
            <td>Gainesway Thoroughbreds, Ltd.</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Tapizar</td>
            <td>Tapit</td>
            <td>Winchell Thoroughbreds LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>98</td>
        </tr>
        <tr>
            <td>Taptowne</td>
            <td>Tapit</td>
            <td>Clovertowne Farm (Q. Spivey)</td>
            <td>Wayne D. Mogge</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Tazered</td>
            <td>Mizzen Mast</td>
            <td>Moirano, Team Stallion Racing Corp., Harold Lerner LLC, Cohen, and Contessa</td>
            <td>Gary C. Contessa</td>
            <td>KY</td>
            <td>65</td>
        </tr>
        <tr>
            <td>Tech Fall</td>
            <td>Birdstone</td>
            <td>Dubb, Michael, Bethlehem Stables and The Elkstone Group</td>
            <td>Anthony W. Dutrow</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Ten Devils</td>
            <td>Rock Hard Ten</td>
            <td>Diamond A Racing Corporation</td>
            <td>Richard E. Mandella</td>
            <td>KY</td>
            <td>81</td>
        </tr>
        <tr>
            <td>The Factor</td>
            <td>War Front</td>
            <td>Fog City Stable and Bolton, George</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>The Fed Eased</td>
            <td>Montbrook</td>
            <td>Klaravich Stables, Inc. and Lawrence, W.H.</td>
            <td>Richard A. Violette Jr.</td>
            <td>FL</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Thirtyfirststreet</td>
            <td>Good Journey</td>
            <td>Gorman, Mark, Haymes, Neil and Sterling Stable</td>
            <td>Doug F. O'Neill</td>
            <td>CA</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Tiz Blessed</td>
            <td>Tiznow</td>
            <td>Gary and Mary West Stables, Inc.</td>
            <td>Chad C. Brown</td>
            <td>KY</td>
            <td>85</td>
        </tr>
        <tr>
            <td>To Honor and Serve</td>
            <td>Bernardini</td>
            <td>Live Oak Plantation</td>
            <td>William I. Mott</td>
            <td>KY</td>
            <td>102</td>
        </tr>
        <tr>
            <td>Toby's Corner</td>
            <td>Bellamy Road</td>
            <td>Diane D. Cotter</td>
            <td>H. Graham Motion</td>
            <td>FL</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Tokubetsu</td>
            <td>Congaree</td>
            <td>Peachtree Stable</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>89</td>
        </tr>
        <tr>
            <td>Toreador</td>
            <td>Grand Slam</td>
            <td>Dogwood Stable</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Travelin Man</td>
            <td>Trippi</td>
            <td>E. Paul Robsham Stables LLC</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>106</td>
        </tr>
        <tr>
            <td>Turbulent Descent (f)</td>
            <td>Congrats</td>
            <td>Blinkers On Racing, Aurelio, Dave, Butler, Robert, Coons, Joleen, Lapso, et al</td>
            <td>Mike Puype</td>
            <td>FL</td>
            <td>94</td>
        </tr>
        <tr>
            <td>Twice the Appeal</td>
            <td>Successful Appeal</td>
            <td>Brown, Jr., Edward J., Flores, Victor and Hernandez, Henry</td>
            <td>Jeff Bonde</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Twinspired</td>
            <td>Harlan's Holiday</td>
            <td>Alpha Stables, Skychai Racing LLC and Sand Dollar Stable LLC</td>
            <td>Michael J. Maker</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Tyrus</td>
            <td>First Samurai</td>
            <td>Celebre Investments</td>
            <td>Ian R. ilkes</td>
            <td>KY</td>
            <td>58</td>
        </tr>
        <tr>
            <td>Unbridled Kimanchi</td>
            <td>Unbridled's Song</td>
            <td>Terra Di Sienna Stables</td>
            <td>John A. Shirreffs</td>
            <td>KY</td>
            <td>38</td>
        </tr>
        <tr>
            <td>Unbridled Sheriff</td>
            <td>Posse</td>
            <td>Stephen R. Baker</td>
            <td>Ralph R. Irwin</td>
            <td>KY</td>
            <td>67</td>
        </tr>
        <tr>
            <td>Uncle Mo</td>
            <td>Indian Charlie</td>
            <td>Repole Stable</td>
            <td>Todd A. Pletcher</td>
            <td>KY</td>
            <td>108</td>
        </tr>
        <tr>
            <td>Uncle Sam</td>
            <td>Tapit</td>
            <td>Kaleem Shah</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>81</td>
        </tr>
        <tr>
            <td>Uncle Smokey</td>
            <td>Uncle Camie</td>
            <td>Joseph Marx</td>
            <td>Luis C. Alvarez</td>
            <td>NY</td>
            <td>58</td>
        </tr>
        <tr>
            <td>Unex Dali (GB)</td>
            <td>Dubawi (IRE)</td>
            <td>J. Paul Reddam</td>
            <td>Craig Dollase</td>
            <td>GB</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Valient Tenobob</td>
            <td>Service Stripe</td>
            <td>Jackson Laura and Red Riding Hood Stables LLC</td>
            <td>Laura Jackson</td>
            <td>MI</td>
            <td>70</td>
        </tr>
        <tr>
            <td>Violadellamora (f)</td>
            <td>Luftikus</td>
            <td>Excellence Godsway, Derby Dreamers Club and Racanelli, Mark</td>
            <td>Annette M. Aulbach</td>
            <td>WV</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Warren's Dr. Yang</td>
            <td>Doc Gus</td>
            <td>Warren Thoroughbreds, LP</td>
            <td>Jorge Gutierrez</td>
            <td>CA</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Warren's Knockout</td>
            <td>Stormed</td>
            <td>Benjamin C. Warren</td>
            <td>Jorge Gutierrez</td>
            <td>CA</td>
            <td>73</td>
        </tr>
        <tr>
            <td>Washington's Rules</td>
            <td>Roman Ruler</td>
            <td>Magdalena Racing (Susan McPeek, et al)</td>
            <td>Kenneth G. McPeek</td>
            <td>KY</td>
            <td>88</td>
        </tr>
        <tr>
            <td>Wegner</td>
            <td>Dynaformer</td>
            <td>Alexander, Elizabeth A. and Shustek, Mike</td>
            <td>Bob Baffert</td>
            <td>KY</td>
            <td>75</td>
        </tr>
        <tr>
            <td>Western Aristocrat</td>
            <td>Mr. Greeley</td>
            <td>Thomas Ludt</td>
            <td>Jeremy Noseda</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>What a Rush</td>
            <td>Tribal Rule</td>
            <td>L-Bo Racing, Pyle, Monte and Summer of Fun Racing, LLC</td>
            <td>A. C. Avila</td>
            <td>CA</td>
            <td>48</td>
        </tr>
        <tr>
            <td>Wilburn</td>
            <td>Bernardini</td>
            <td>Stonestreet Stables LLC</td>
            <td>Steven M. Asmussen</td>
            <td>KY</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Wilkinson</td>
            <td>Lemon Drop Kid</td>
            <td>Gaillardia Racing LLC</td>
            <td>Neil J. Howard</td>
            <td>KY</td>
            <td>77</td>
        </tr>
        <tr>
            <td>Willcox Inn</td>
            <td>Harlan's Holiday</td>
            <td>All In Stable</td>
            <td>Michael Stidham</td>
            <td>KY</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Winchill</td>
            <td>Tapit</td>
            <td>Heiligbrodt Racing Stable</td>
            <td>Dale L. Romans</td>
            <td>PA</td>
            <td>80</td>
        </tr>
        <tr>
            <td>Wolfcamp</td>
            <td>El Prado (IRE)</td>
            <td>Bluegrass Hall LLC</td>
            <td>D. Wayne Lukas</td>
            <td>KY</td>
            <td>38</td>
        </tr>
        <tr>
            <td>World Renowned</td>
            <td>A.P. Indy</td>
            <td>Spendthrift Farm LLC</td>
            <td>John W. Sadler</td>
            <td>FL</td>
            <td>67</td>
        </tr>
        <tr>
            <td>Would You</td>
            <td>Lemon Drop Kid</td>
            <td>Poindexter Thoroughbreds LLC</td>
            <td>Allen Milligan</td>
            <td>KY</td>
            <td>82</td>
        </tr>
        <tr>
            <td>Yankee Passion</td>
            <td>Yankee Gentleman</td>
            <td>Brereton C. Jones</td>
            <td>J. Larry Jones</td>
            <td>KY</td>
            <td>79</td>
        </tr>
        <tr>
            <td>Zoebear</td>
            <td>Bellamy Road</td>
            <td>William Stiritz</td>
            <td>Scott Becker</td>
            <td>KY</td>
            <td>71</td>
        </tr>
        <tr>
            <td>Zorro Veloz</td>
            <td>Take Me Out</td>
            <td>El Herraje Stud</td>
            <td>Dante Zanelli, Jr.</td>
            <td>NY</td>
            <td>16</td>
        </tr>
    </tbody>
</table>
{literal}<script type="text/javascript">
$("#infoEntries tr:even").addClass("odd");
</script>{/literal}