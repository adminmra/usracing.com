{literal}<script type="text/javascript">
$(function(){
  $("#infoEntries tr:odd").addClass("odd");
});
</script>{/literal}
{literal}<style type="text/css">
#infoEntries td { padding: 5px 10px; }
</style>{/literal}

<h4><strong>Props & Kentucky Derby Special Wagers Odds</strong></h4>

{include file=inc/ahr_block-horsefutures-props-top3.tpl}
<br />
<br />
<table width="100%" height="12" cellpadding="0" cellspacing="0" id="infoEntries" class="data" title="Graded Stakes Earnings" summary="Graded Stakes Earnings">
<tr>
<th colspan="2">GRADED STAKES EARNINGS (AMOUNT IN US DOLLARS)
<br /><span style="font-weight:normal;">Tie Breaker for Starters On The 2011 Kentucky Derby - All Bets Action - Apr 30, 2011 12:00 PM ET</span></th>
</tr>
<tr>
<td class="left"   width="50%"><div>230999 or Less</div></td>
<td class="sortOdds" align="right">7 - 5</td>
</tr>
<tr>
<td class="left"><div>
  <div>231000 to 249999</div>
</div></td>
<td class="sortOdds" align="right">7 - 2</td>
</tr>
<tr>
<td class="left"><div>
  <div>250000 to 265999</div>
</div></td>
<td class="sortOdds" align="right">4 - 1</td>
</tr>
<tr>
<td class="left"><div>
  <div>266000 to 280999</div>
</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<tr>
<td class="left"><div>
  <div>281000 to 295999</div>
</div></td>
<td class="sortOdds" align="right">7 - 1</td>
</tr>
<tr>
<td class="left"><div>
  <div>296000 to 310999</div>
</div></td>
<td class="sortOdds" align="right">9 - 1</td>
</tr>
<tr>
<td class="left"><div>
  <div>311000 or Higher</div>
</div></td>
<td class="sortOdds" align="right">9 - 1</td>
</tr>
<tr style="background:#ffffff;">
<td class="left"></td>
<td class="sortOdds" align="right"><em style="color:#808080;">Log in and visit Sportsbook for the latest Odds</em></td>
</tr>
</table>

<br />
<br />

<table cellspacing="0" cellpadding="0" width="100%" height="12" summary="2011 Kentucky Derby Propositions" title="Propositions" class="data" id="infoEntries">
  <tr>
   <th style="text-align:left;" colspan="5">PROPOSITIONS</th>
  </tr>

  <tr>
    <td width="40%" style="padding-right:20px;">Will a Horse Win The 2011 Triple Crown?</td>
    <td>YES</td>
    <td align="right">7/1</td>
    <td>NO</td>    
    <td align="right">1/11</td>
  </tr>
<!--   <tr>
    <td style="padding-right:20px;">Will Uncle Mo Finish Top Three Positons in Kentucky Derby</td>
    <td>YES</td>
    <td align="right">1/1</td>
    <td>NO</td>
    <td align="right">5/7</td>
  </tr> -->
  <tr>
    <td style="padding-right:20px;">Winning Saddle Cloth Number of the 2011 Kentucky Derby</td>
    <td>ODD</td>
    <td align="right">10/11</td>
    <td>EVEN</td>
    <td align="right">10/11</td>
    
  </tr>
  <tr>
    <td style="padding-right:20px;">Winning Horses Name Begins with A-M or N-Z for the 2011 Kentucky Derby</td>
    <td>Letters A through M</td>
    <td align="right">5/7</td>
    <td>Letters N through Z</td>
    <td align="right">1/1</td>
  </tr>
    <tr>
    <td style="padding-right:20px;">Will Secretariats 1973 Record Time be Beaten in the 2011 Kentucky Derby</td>
    <td>Yes Under 1:59.40</td>
    <td align="right">15/1</td>
    <td>No Over 1:59.40</td>
    <td align="right">1/25</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Kentucky Derby - Official Winning Time</td>
    <td>Over 2:02.15</td>
    <td align="right">5/11</td>
    <td>Under 2:02.15</td>
    <td align="right">8/5</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Will the Derby winner win the Preakness</td>
    <td>YES</td>
    <td align="right">3/2</td>
    <td>NO</td>
    <td align="right">1/2</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Will the Derby winner win the Belmont</td>
    <td>YES</td>
    <td align="right">7/2</td>
    <td>NO</td>
    <td align="right">2/11</td>
  </tr>

     <tr>
    <td style="padding-right:20px;">Will Any Horse Win Two of the Three Triple Crown Races (Kentucky Derby/Preakness Stakes/Belmont Stakes)</td>
    <td>YES</td>
    <td align="right">1/1</td>
    <td>NO</td>
    <td align="right">5/7</td>
  </tr>
<!--   <tr>
    <td style="padding-right:20px;">Will Uncle Mo win Kentucky Derby</td>
    <td>YES</td>
    <td align="right">4/1</td>
    <td>NO</td>
    <td align="right">1/6</td>
  </tr> -->
  <tr>
    <td style="padding-right:20px;">Will a Horse Win the 137th Kentucky Derby Wire to Wire?</td>
    <td>YES Wire to Wire</td>
    <td align="right">5/1</td>
    <td>NO Wire to Wire</td>
    <td align="right">1/10</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Winning Saddle Cloth Number of the 2011 Kentucky Derby</td>
    <td>Numbers 1 to 10</td>
    <td align="right">10/11</td>
    <td>Numbers 11 to 20</td>
    <td align="right">10/11</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Will the Crowd Attendance Exceed 154000 for Kentucky Derby</td>
    <td>YES Exceeds</td>
    <td align="right">1/2</td>
    <td>NO Does not Exceed</td>
    <td align="right">3/2</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Will there be an Inquiry on the outcome of The Kentucky Derby</td>
    <td>YES Inquiry</td>
    <td align="right">2/1</td>
    <td>NO Inquiry</td>
    <td align="right">1/4</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">Winning Horse Starts From Which Gate (Gate 1 (1-10) Gate 2 (11-20) )</td>
    <td>Gate 1</td>
    <td align="right">20/21</td>
    <td>Gate 2</td>
    <td align="right">20/23</td>
  </tr>
  <tr>
    <td style="padding-right:20px;">$5 Free Bet - 2011 Kentucky Derby - Finishing Time of Winning Horse</td>
    <td>Over 2:02.15 Winning Time</td>
    <td align="right">10/17</td>
    <td>Under 2:02.15 Winning Time</td>
    <td align="right">7/5</td>
  </tr>
  <tr style="background:#ffffff;">
<td class="sortOdds" align="right" colspan="5"><em style="color:#808080;">Log in and visit Sportsbook for the latest Odds</em></td>
</tr>
</table>

<br />
<br />
<table cellspacing="0" cellpadding="0" width="100%" height="12" summary="2011 Kentucky Derby Margin of Victory" title="Margin of Victory" class="data" id="infoEntries">
  <tr>
   <th style="text-align:left;" colspan="5">MARGIN OF VICTORY</th>
  </tr>
  <tr>
    <td>1 Length to 2 3/4 lengths</td>
    <td align="right">2/1</td>
  </tr>
  <tr>
    <td>1/2 Length to 3/4 length</td>
     <td align="right">4/1</td>
  </tr>
  <tr>
    <td>11 Lengths to 14 3/4 lengths</td>
     <td align="right">12/1</td>
  </tr>
  <tr>
    <td>15 Lengths or More</td>
     <td align="right">14/1</td>
  </tr>
  <tr>
    <td>3 Lengths to 5 3/4 lengths</td>
     <td align="right">3/1</td>
  </tr>
  <tr>
    <td>6 Lengths to 7 3/4 lengths</td>
     <td align="right">7/1</td>
  </tr>
  <tr>
    <td>8 Lengths to 10 3/4 lengths</td>
     <td align="right">10/1</td>
  </tr>
  <tr>
    <td>Dead Heat</td>
     <td align="right">30/1</td>
  </tr>
  <tr>
    <td>Head</td>
     <td align="right">8/1</td>
  </tr>
  <tr>
    <td>Neck</td>
    <td align="right">8/1</td>
  </tr>
  <tr>
    <td>Nose</td>
     <td align="right">10/1</td>
  </tr>  
  <tr>
    <td></td>
    <td align="right"><em style="color: #808080;">Log in and visit Sportsbook for the latest Odds</em></td>
  </tr> 
</table>

