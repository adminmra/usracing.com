<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
 
   <div  class="rsContent">
    <img class="rsImg" src="/img/preaknessstakes/oxbow-2013-preakness-stakes.jpg"  />
    <figure class="rsCaption">Photo credit | Mitch Stringer USA TODAY</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/preaknessstakes/138th-preakness-stakes.jpg" />
    <figure class="rsCaption">138th Preakness Stakes</figure>
   </div>
   	<div  class="rsContent">
    <img class="rsImg" src="/img/preaknessstakes/black-Eyed-susans.jpg" />
    <figure class="rsCaption">Field of Black-Eyed Susans, the official flower</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/preaknessstakes/black-Eyed-susan-drink.jpg" />
    <figure class="rsCaption">Black-Eyed Susan - Official drink at the Preakness Stakes</figure>
   </div>
   
  </div>
 </div>
