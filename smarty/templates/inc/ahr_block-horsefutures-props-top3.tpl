<table width="100%" height="12" cellpadding="0" cellspacing="0" id="infoEntries" class="data top3" title="Kentucky Derby Top Three Finish" summary="Kentucky Derby Top Three Finish Odds">
<tr>
<th colspan="2">KENTUCKY DERBY TOP THREE FINISH (CHURCHILL DOWNS)
<br /><span style="font-weight:normal;">(Non Runner Refund on May 6th 2011) - (Horse Must Run For Action) - May 07, 2011 12:00 PM ET</span></th>
</tr>
<tr>
<td class="left"><div>Animal Kingdom</div></td>
<td class="sortOdds" align="right">8- 1</td>
</tr>

<tr>
<td class="left"><div>Archarcharch</div></td>
<td class="sortOdds" align="right">3 - 1</td>
</tr>
<tr>
<td class="left"><div>Brilliant Speed</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<tr>
<td class="left"><div>Comma to the Top</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<tr>
<td class="left"><div>Decisive Moment</div></td>
<td class="sortOdds" align="right">7 - 1</td>
</tr>
<tr>
<td class="left"><div>Derby Kitten</div></td>
<td class="sortOdds" align="right">7 - 1</td>
</tr>
<tr>
<td class="left"><div>Dialed In</div></td>
<td class="sortOdds" align="right">1 - 1</td>
</tr>

<tr>
<td class="left"><div>Master Of Hounds</div></td>
<td class="sortOdds" align="right">7 - 1</td>
</tr>
<tr>
<td class="left">Midnight Interlude</td>
<td class="sortOdds" align="right">3 - 1</td>
</tr>
<tr>
<td class="left">Mucho Macho Man</td>
<td class="sortOdds" align="right">3 - 1</td>
</tr>


<tr>
<td class="left"><div>Nehro</div></td>
<td class="sortOdds" align="right">3 - 2</td>
</tr>


<tr>
<td class="left"><div>Pants On Fire</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<tr>
<td class="left">Santiva</td>
<td class="sortOdds" align="right">9 - 1</td>
</tr>

<tr>
<td class="left">Shackleford</td>
<td class="sortOdds" align="right">3 - 1</td>
</tr>

<tr>
<td class="left">Soldat</td>
<td class="sortOdds" align="right">3 - 1</td>
</tr>

<tr>
<td class="left"><div>Stay Thirsty</div></td>
<td class="sortOdds" align="right">7 - 1</td>
</tr>
<tr>
<td class="left"><div>Twice The Appeal</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<tr>
<td class="left"><div>Twinspired</div></td>
<td class="sortOdds" align="right">5 - 1</td>
</tr>
<!-- <tr>
<td class="left"><div>Uncle Mo</div></td>
<td class="sortOdds" align="right">3 - 2</td>
</tr> -->

<tr>
<td class="left"><div>Watch Me Go</div></td>
<td class="sortOdds" align="right">8 - 1</td>
</tr>


<tr style="background:#ffffff;">
<td class="left"></td>
<td class="sortOdds" align="right"><em style="color:#808080;">Log in and visit Sportsbook for the latest Odds</em></td>
</tr>
</table>