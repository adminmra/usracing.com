<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/breeders/santaanitapark.jpg"  />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">Santa Anita Park, Arcadia, California</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/breeders/breederscup-racing.jpg"  />
    <figure class="rsCaption">Breeders' Cup racing</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/breeders/breederscup-unclemo.jpg" />
    <figure class="rsCaption">Breeders' Cup winner - Uncle Mo</figure>
   </div>
   
   <div  class="rsContent">
    <img class="rsImg" src="/img/breeders/down-the-stretch-drink.jpg" />
    <figure class="rsCaption">"Down the Stretch" - Signature drink of the Breeders' cup</figure>
   </div>
  </div>
 </div>
