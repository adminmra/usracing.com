<div id="sliderPage" class="sliderPage fullWidth clearfix margin-bottom-30">
  <div id="content-slider-1" class="royalSlider rsDefault">
   <div class="rsContent">
    <img class="rsImg" src="/img/royalascot/royal-ascot-racing.jpg"  />
    <figure class="rsCaption" data-move-effect="left" data-speed="200">Horse racing action at the Royal Ascot</figure>
   
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/royalascot/royal-ascot-racecourse.jpg"  />
    <figure class="rsCaption">Around the turn at the Ascot Racecourse</figure>
   </div>
   <div  class="rsContent">
    <img class="rsImg" src="/img/royalascot/royal-ascot-crowds.jpg" />
    <figure class="rsCaption">Large crowds gather to watch the race</figure>
   </div>
 
   
  </div>
 </div>
