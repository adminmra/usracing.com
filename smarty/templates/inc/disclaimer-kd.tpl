<br />
<div class="disclaimer">
<div class="headline"><h2>Disclaimer</h2></div>

<p>Churchill Downs, Kentucky Derby, Kentucky Oaks, the "twin spires design," "Road to the Roses" and Churchill Downs Incorporated-related trademarks are registered trademarks of Churchill Downs Incorporated or its affiliates (collectively CDI). CDI does not sponsor or endorse, and is not associated or affiliated with US Racing or its products, services or promotions.  US Racing provides free information, odds, facts and commentary about the Kentucky Derby and horse racing and betting, in general.  Third party marks may be referenced in a transformative, editorial, informational, nominative, critical, analytical or comparative context. US Racing may reference marks belonging to third parties pursuant to our right to engage in fair use, fair comment, statutory fair use or trade mark fair use doctrine.  As such, US Racing does not contribute to any dilution of any trade or service marks. US Racing provides this information in an effort to educate and grow the sport of thoroughbred racing in North America with an emphasis on attracting new fans of the sport.
</p>
</div>
