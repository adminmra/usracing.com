{literal}
<style>
    #signupBtn {
        background-color: #e2001a;
        background-image: -webkit-linear-gradient(top, #e2001a, #991a1e);
        background-image: linear-gradient(to bottom, #e2001a, #991a1e);
        border-radius: 3px;
        border: 0;
        display: inline-block;
        font-family: 'Lato', sans-serif;
        font-size: 16px;
        font-weight: 900;
        outline: none;
        text-align: center;
        text-decoration: none;
        min-width: 100px;
        -webkit-transition: .2s;
        transition: .2s;
        transition: .2s
    }

    #signupBtn:hover {
        background: #d6242a;
    }

    #signupBtn:focus, #signupBtn:hover {
        color:#fff;font-weight:900;
        text-decoration:none;
    }

    #signupBtn:active {
        -webkit-transform:scale(0.98);transform:scale(0.98);
    }
</style>
{/literal}
<div class="login-header">
    <a class="btn btn-sm btn-red" id="signupBtn" href="/signup?ref={$ref}" rel="nofollow">
        <span>Bet Now</span> <i class="glyphicon glyphicon-pencil"></i>
    </a>
</div>
