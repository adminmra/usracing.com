{if $plainbody ne ''}
{include file=$plainbody}
{else}
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
{if $header ne ''}
{include file=$header}
{/if}
<body class="{$bodyclass} {if $smarty.server.REQUEST_URI == '/index-kd' or $smarty.server.REQUEST_URI == '/index-ps' or $smarty.server.REQUEST_URI == '/index-bs' or $smarty.server.REQUEST_URI == '/' or $smarty.server.REQUEST_URI == '/indexbecca' }{'index-kd'}{/if}">
<!-- ClickTale Top part -->
{literal}
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K74Z827"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
{/literal}
<script type="text/javascript">
var WRInitTime=(new Date()).getTime();
</script>
<!-- ClickTale end of Top part -->

{$ahr_mess}
<!-- test 1 -->
{include file=$topcontent}
<!-- test 2 -->
{if $body ne ''}
{include file=$body}
{/if}
{if $footer ne ''}
{include file=$footer}
{/if}
</body>
</html>
{/if}
