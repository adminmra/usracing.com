<head>
{*
{literal}

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-742771-1', 'auto');
  ga('send', 'pageview');
</script>

{/literal}
*}
{$title}
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
{$link}
{$metatag}
<meta name="google-site-verification" content="mT_jDUaAW4wPFFflBMaN7yyiEB4x6aKMyCM0qFtcM1E" />
{$css}

{if $redirect_switch=="1"}
{include file='redirectsripts.tpl'}
{/if}

  

{include file='scripts.tpl'}
{$js}

{* included this style .. as an experiment *}


{literal}
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126793368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126793368-1');
</script>
{/literal}
{literal}
<style>





@media screen and (max-width:767px) and (min-width:0px) {
.sidr ul li ul li {
    background: #fff;
    line-height: 40px;
    border-bottom: 0px solid #fff;
}

  .bc-nemu-empty{
    border-bottom: 0px solid #fff;
  }
}

</style>
{/literal}


{literal}
<link rel="apple-touch-icon-precomposed" href="icon/usr-logo-apple-touch-iphone.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="icon/usr-logo-apple-touch-ipad.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="icon/usr-logo-apple-touch-iphone4.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="icon/usr-logo-apple-touch-ipad-retina.png" />
{/literal}

{literal}
<!-- Start Open Web Analytics Tracker -->
<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'http://tools.allhorse.com/owa/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', '55de2b043cfcfc97cace6371e6e2d777']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);
owa_cmds.push(['trackDomStream']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script>
<!-- End Open Web Analytics Code -->
{/literal}


</head>
