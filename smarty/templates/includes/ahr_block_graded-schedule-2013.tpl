<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
	<tbody>
		<tr>
			<th width="10%"><strong>Date</strong></th>
			<th width="12%"><strong>Stakes</strong></th>
			<th width="12%"><strong>Track</strong></th>
			<!-- <th width="10%"><strong>Grade</strong></th> -->
			<th width="10%"><strong>Purses</strong></th>
			<!-- <th width="10%"><strong>Age/Sex</strong></th>
			<th width="10%"><strong>DS</strong></th> -->
			<th width="12%"><strong>Horse</strong></th>
			<!-- <th width="12%"><strong>Jockey</strong></th>
			<th width="12%"><strong>Trainer</strong></th> -->
		</tr>
		<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-01">Jan 01</time></td>
		<td itemprop="name">Old Hat S.</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$98,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Kauai Katie</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-05">Jan 05</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/jerome-stakes">Jerome Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.32</td> -->
		<td>Vyjack</td>
		<!-- 	<td>C. Velasquez</td>
		<td>R. Rodriguez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-05">Jan 05</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/san-pasqual-stakes">San Pasqual Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Coil</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-06">Jan 06</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/monrovia-stakes">Monrovia Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>6.5 T</td> -->
		<td>Mizdirection</td>
		<!-- 	<td>M. Smith</td>
		<td>M. Puype</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-12">Jan 12</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/fort-lauderdale-stakes">Fort Lauderdale Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Mucho Mas Macho</td>
		<!-- 	<td>J. Leyva</td>
		<td>H. Collazo</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-12">Jan 12</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/san-fernando-stakes">San Fernando Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Fed Biz</td>
		<!-- 	<td>M. Smith</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-13">Jan 13</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/san-gabriel-stakes">San Gabriel Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Jeranimo</td>
		<!-- 	<td>R. Bejarano</td>
		<td>M. Pender</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-19">Jan 19</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/lecomte-stakes">Lecomte Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.32</td> -->
		<td>Oxbow</td>
		<!-- 	<td>J. Court</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-19">Jan 19</time></td>
		<td itemprop="name">Palos Verdes Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Sahara Sky</td>
		<!-- 	<td>J. Talamo</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-20">Jan 20</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/la-canada-stakes">La Canada Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>More Chocolate</td>
		<!-- 	<td>G. Gomez</td>
		<td>J. Sadler</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-26">Jan 26</time></td>
		<td itemprop="name">Santa Ysabel Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Fiftyshadesofhay</td>
		<!-- 	<td>R. Bejarano</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-26">Jan 26</time></td>
		<td itemprop="name">John B. Connally Turf Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/sam-houston" itemprop="url">Sam Houston</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Swift Warrior</td>
		<!-- 	<td>J. Espinoza</td>
		<td>J. Terranova, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-26">Jan 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/forward-gal-stakes">Forward Gal Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Kauai Katie</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-01-26">Jan 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/holy-bull-stakes">Holy Bull Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Itsmyluckyday</td>
		<!-- 	<td>E. Trujillo</td>
		<td>E. Plesa, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-01-26">Jan 26</time></td>
		<td itemprop="name">Colonel E.R. Bradley Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$125,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Optimizer</td>
		<!-- 	<td>J. Court</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Florida Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Tapicat</td>
		<!-- 	<td>J. Rosario</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Endeavour Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Old Tune</td>
		<!-- 	<td>J. Rosario</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Sam F. Davis Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Falling Sky</td>
		<!-- 	<td>J. Espinoza</td>
		<td>J. Terranova, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Arcadia Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Suggestive Boy</td>
		<!-- 	<td>J. Talamo</td>
		<td>R. McAnally</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/strub-stakes">Strub Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>4yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Guilt Trip</td>
		<!-- 	<td>J. Talamo</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Robert B. Lewis Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Flashback</td>
		<!-- 	<td>J. Leparoux</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Hutcheson Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Honorable Dillon</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>E. Kenneally</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Toboggan Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Head Heart Hoof</td>
		<!-- 	<td>C. Velasquez</td>
		<td>R. Rodriguez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-02">Feb 02</time></td>
		<td itemprop="name">Withers Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Revolutionary</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-03">Feb 03</time></td>
		<td itemprop="name">San Antonio Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Game On Dude</td>
		<!-- 	<td>M. Smith</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-09">Feb 09</time></td>
		<td itemprop="name">San Marcos Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Slim Shadey</td>
		<!-- 	<td>G. Stevens</td>
		<td>S. Callaghan</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-09">Feb 09</time></td>
		<td itemprop="name">Suwannee River Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Channel Lady</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-09">Feb 09</time></td>
		<td itemprop="name">Gulfstream Park Sprint</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Fort Loudon</td>
		<!-- 	<td>J. Lezcano</td>
		<td>N. Zito</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-09">Feb 09</time></td>
		<td itemprop="name">Gulfstream Park Turf Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Point Of Entry</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-09">Feb 09</time></td>
		<td itemprop="name">Donn Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Graydar</td>
		<!-- 	<td>E. Prado</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-10">Feb 10</time></td>
		<td itemprop="name">Hurricane Bertie Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Golden Mystery</td>
		<!-- 	<td>L. Saez</td>
		<td>M. Wolfson</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-16">Feb 16</time></td>
		<td itemprop="name">Tampa Bay Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Swift Warrior</td>
		<!-- 	<td>J. Espinoza</td>
		<td>J. Terranova, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-16">Feb 16</time></td>
		<td itemprop="name">Santa Maria Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Great Hot</td>
		<!-- 	<td>G. Stevens</td>
		<td>A. Avila</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-16">Feb 16</time></td>
		<td itemprop="name">Barbara Fritchie Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/laurel-park" itemprop="url">Laurel Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Funnys Approval</td>
		<!-- 	<td>J. Vargas</td>
		<td>J. Lopez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-16">Feb 16</time></td>
		<td itemprop="name">The Very One Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Starformer</td>
		<!-- 	<td>E. Prado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-16">Feb 16</time></td>
		<td itemprop="name">El Camino Real Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate-fields" itemprop="url">Golden Gate Fields</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Dice Flavor</td>
		<!-- 	<td>J. Valdivia, Jr.</td>
		<td>P. Gallagher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-17">Feb 17</time></td>
		<td itemprop="name">Sabin Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Royal Delta</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-17">Feb 17</time></td>
		<td itemprop="name">San Vicente Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Shakin It Up</td>
		<!-- 	<td>D. Flores</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-18">Feb 18</time></td>
		<td itemprop="name">Buena Vista Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Mizdirection</td>
		<!-- 	<td>M. Smith</td>
		<td>M. Puype</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-18">Feb 18</time></td>
		<td itemprop="name">Southwest Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Super Ninety Nine</td>
		<!-- 	<td>R. Bejarano</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-18">Feb 18</time></td>
		<td itemprop="name">General George Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/laurel-park" itemprop="url">Laurel Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Javerre</td>
		<!-- 	<td>J. Acosta</td>
		<td>C. Lynch</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">San Carlos Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Sahara Sky</td>
		<!-- 	<td>J. Talamo</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">Canadian Turf Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Data Link</td>
		<!-- 	<td>J. Castellano</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">Davona Dale Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Live Lively</td>
		<!-- 	<td>J. Rosario</td>
		<td>M. Hennig</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">Fountain of Youth Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Orb</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">Mineshaft Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Mark Valeski</td>
		<!-- 	<td>R. Napravnik</td>
		<td>J. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name">Fair Grounds Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Optimizer</td>
		<!-- 	<td>J. Court</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/rachel-alexandra-stakes">Rachel Alexandra Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Unlimited Budget</td>
		<!-- 	<td>R. Napravnik</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-02-23">Feb 23</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/risen-star-stakes">Risen Star Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Ive Struck A Nerve</td>
		<!-- 	<td>J. Graham</td>
		<td>J. Desormeaux</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Las Virgenes Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Beholder</td>
		<!-- 	<td>G. Gomez</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Frank E. Kilroe Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Suggestive Boy</td>
		<!-- 	<td>J. Talamo</td>
		<td>R. McAnally</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Santa Anita Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Game On Dude</td>
		<!-- 	<td>M. Smith</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Swale Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Clearly Now</td>
		<!-- 	<td>L. Saez</td>
		<td>B. Lynch</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Top Flight Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Summer Applause</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Tom Fool Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Comma To The Top</td>
		<!-- 	<td>J. Rosario</td>
		<td>P. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-02">Mar 02</time></td>
		<td itemprop="name">Gotham Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Vyjack</td>
		<!-- 	<td>J. Rosario</td>
		<td>R. Rodriguez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-03">Mar 03</time></td>
		<td itemprop="name">Palm Beach Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Rydilluc</td>
		<!-- 	<td>E. Prado</td>
		<td>G. Contessa</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">Hillsborough Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Old Tune</td>
		<!-- 	<td>J. Rosario</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">Tampa Bay Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Verrazano</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">San Felipe Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Hear The Ghost</td>
		<!-- 	<td>C. Nakatani</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">Razorback Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Cyber Secret</td>
		<!-- 	<td>R. Albarado</td>
		<td>L. Whiting</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">Honeybee Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Rose To Gold</td>
		<!-- 	<td>C. Borel</td>
		<td>S. Santoro</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-09">Mar 09</time></td>
		<td itemprop="name">Gulfstream Park Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Discreet Dancer</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-10">Mar 10</time></td>
		<td itemprop="name">Las Flores Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Rumor</td>
		<!-- 	<td>M. Smith</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-16">Mar 16</time></td>
		<td itemprop="name">San Luis Rey Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Bright Thought</td>
		<!-- 	<td>V. Espinoza</td>
		<td>J. Gutierrez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-16">Mar 16</time></td>
		<td itemprop="name">Santa Margarita Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Joyful Victory</td>
		<!-- 	<td>R. Napravnik</td>
		<td>J. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-16">Mar 16</time></td>
		<td itemprop="name">Azeri Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Tiz Miz Sue</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>S. Hobby</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-16">Mar 16</time></td>
		<td itemprop="name">Rebel Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Will Take Charge</td>
		<!-- 	<td>J. Court</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-16">Mar 16</time></td>
		<td itemprop="name">Honey Fox Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Centre Court</td>
		<!-- 	<td>J. Leparoux</td>
		<td>G. Arnold, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-17">Mar 17</time></td>
		<td itemprop="name">Inside Information Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Aubby K</td>
		<!-- 	<td>E. Prado</td>
		<td>R. Nicks</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-23">Mar 23</time></td>
		<td itemprop="name">Fathead Bourbonette Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/turfway-park" itemprop="url">Turfway Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Silsita</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-23">Mar 23</time></td>
		<td itemprop="name">Horseshoe Casino Cincinnati Spiral Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/turfway-park" itemprop="url">Turfway Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$550,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Black Onyx</td>
		<!-- 	<td>J. Bravo</td>
		<td>K. Breen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-23">Mar 23</time></td>
		<td itemprop="name">Tokyo City Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>12.0</td> -->
		<td>Sky Kingdom</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-23">Mar 23</time></td>
		<td itemprop="name">Pan American Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Twilight Eclipse</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Albertrani</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-23">Mar 23</time></td>
		<td itemprop="name">Excelsior Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Last Gunfighter</td>
		<!-- 	<td>R. Maragh</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-24">Mar 24</time></td>
		<td itemprop="name">Santa Ana Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Tiz Flirtatious</td>
		<!-- 	<td>J. Leparoux</td>
		<td>M. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-24">Mar 24</time></td>
		<td itemprop="name">Sunland Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/sunland-park" itemprop="url">Sunland Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$800,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Governor Charlie</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Skip Away Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.5</td> -->
		<td>Cigar Street</td>
		<!-- 	<td>J. Velazquez</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Appleton Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Za Approval</td>
		<!-- 	<td>J. Lezcano</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Rampart Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Ciao Bella</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Orchid Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Regalo Mia</td>
		<!-- 	<td>L. Contreras</td>
		<td>M. Nihei</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Gulfstream Park Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Dreaming Of Julia</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/florida-derby">Florida Derby</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Orb</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">New Orleans Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Graydar</td>
		<!-- 	<td>E. Prado</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Fair Grounds Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Unlimited Budget</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-03-30">Mar 30</time></td>
		<td itemprop="name">Louisiana Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds-race-course" itemprop="url">Fair Grounds</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Revolutionary</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-05">Apr 05</time></td>
		<td itemprop="name">Transylvania Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Jack Milton</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Potrero Grande Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Jimmy Creed</td>
		<!-- 	<td>G. Gomez</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Santa Anita Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Beholder</td>
		<!-- 	<td>G. Gomez</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Central Bank Ashland Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Emollient</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Ruffian Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Withgreatpleasure</td>
		<!-- 	<td>J. Velazquez</td>
		<td>D. Nunn</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Gazelle Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Close Hatches</td>
		<!-- 	<td>J. Rosario</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Carter Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Swagger Jack</td>
		<!-- 	<td>I. Ortiz, Jr.</td>
		<td>M. Wolfson</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-06">Apr 06</time></td>
		<td itemprop="name">Wood Memorial Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Verrazano</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-07">Apr 07</time></td>
		<td itemprop="name">Providencia Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Scarlet Strike</td>
		<!-- 	<td>R. Bejarano</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-10">Apr 10</time></td>
		<td itemprop="name">Fantasy Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Rose To Gold</td>
		<!-- 	<td>C. Borel</td>
		<td>S. Santoro</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-12">Apr 12</time></td>
		<td itemprop="name">Apple Blossom Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>On Fire Baby</td>
		<!-- 	<td>J. Johnson</td>
		<td>G. Hartlage</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Las Cienegas Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>6.5 T</td> -->
		<td>Mizdirection</td>
		<!-- 	<td>D. Flores</td>
		<td>M. Puype</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Count Fleet Sprint Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Justin Phillip</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Oaklawn Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Cyber Secret</td>
		<!-- 	<td>R. Albarado</td>
		<td>L. Whiting</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/arkansas-derby">Arkansas Derby</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Overanalyze</td>
		<!-- 	<td>R. Bejarano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Shakertown Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>5.5 T</td> -->
		<td>Havelock</td>
		<!-- 	<td>G. Gomez</td>
		<td>D. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Madison Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Last Full Measure</td>
		<!-- 	<td>C. Nakatani</td>
		<td>P. Oliver</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Jenny Wiley Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Centre Court</td>
		<!-- 	<td>J. Leparoux</td>
		<td>G. Arnold, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-13">Apr 13</time></td>
		<td itemprop="name">Distaff Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Cluster Of Stars</td>
		<!-- 	<td>J. Alvarado</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-14">Apr 14</time></td>
		<td itemprop="name">Beaumont Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Ciao Bella Luna</td>
		<!-- 	<td>J. Rosario</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-18">Apr 18</time></td>
		<td itemprop="name">Appalachian Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Unbelievable Dream</td>
		<!-- 	<td>J. Rosario</td>
		<td>B. Tagg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-19">Apr 19</time></td>
		<td itemprop="name">Hilliard Lyons Doubledogdare Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Ice Cream Silence</td>
		<!-- 	<td>R. Napravnik</td>
		<td>G. Arnold, II</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name">San Simeon Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.5 T</td> -->
		<td>Chips All In</td>
		<!-- 	<td>T. Baze</td>
		<td>J. Mullins</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name">Santa Barbara Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Lady Of Shamrock</td>
		<!-- 	<td>R. Bejarano</td>
		<td>J. Sadler</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name">Coolmore Lexington Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Winning Cause</td>
		<!-- 	<td>J. Leparoux</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name">Sixty Sails Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne-race-course" itemprop="url">Hawthorne</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Disposablepleasure</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/illinois-derby">Illinois Derby</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne-race-course" itemprop="url">Hawthorne</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Departing</td>
		<!-- 	<td>B. Hernandez, Jr.</td>
		<td>A. Stall, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-20">Apr 20</time></td>
		<td itemprop="name">Charles Town Classic</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/charles-town" itemprop="url">Charles Town</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$1,500,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Game On Dude</td>
		<!-- 	<td>M. Smith</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-21">Apr 21</time></td>
		<td itemprop="name">Ben Ali Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Successful Dan</td>
		<!-- 	<td>J. Leparoux</td>
		<td>C. LoPresti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-21">Apr 21</time></td>
		<td itemprop="name">San Juan Capistrano Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>14.0 T</td> -->
		<td>Interaction</td>
		<!-- 	<td>J. Talamo</td>
		<td>R. McAnally</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-25">Apr 25</time></td>
		<td itemprop="name">Bewitch Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Strathnaver</td>
		<!-- 	<td>J. Rosario</td>
		<td>H. Motion</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-26">Apr 26</time></td>
		<td itemprop="name">Elkhorn Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Dark Cove</td>
		<!-- 	<td>J. Rosario</td>
		<td>M. Maker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-27">Apr 27</time></td>
		<td itemprop="name">Westchester Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Flat Out</td>
		<!-- 	<td>J. Alvarado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-27">Apr 27</time></td>
		<td itemprop="name">Derby Trial Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$175,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Forty Tales</td>
		<!-- 	<td>J. Rosario</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-27">Apr 27</time></td>
		<td itemprop="name">San Francisco Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate-fields" itemprop="url">Golden Gate Fields</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Tigah</td>
		<!-- 	<td>A. Quinonez</td>
		<td>J. Sadler</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-27">Apr 27</time></td>
		<td itemprop="name">Texas Mile Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/lone-star-park" itemprop="url">Lone Star Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Master Rick</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-04-28">Apr 28</time></td>
		<td itemprop="name">Whimsical Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$152,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Acting Naughty</td>
		<!-- 	<td>T. Pizarro</td>
		<td>D. MacRae</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-04-28">Apr 28</time></td>
		<td itemprop="name">Wilshire Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Halo Dolly</td>
		<!-- 	<td>R. Bejarano</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-03">May 03</time></td>
		<td itemprop="name">Eight Belles Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>7.0</td> -->
		<td>So Many Ways</td>
		<!-- 	<td>G. Gomez</td>
		<td>T. Amoss</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-03">May 03</time></td>
		<td itemprop="name">American Turf</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Noble Tune</td>
		<!-- 	<td>J. Castellano</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-03">May 03</time></td>
		<td itemprop="name">La Troienne Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Authenticity</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-03">May 03</time></td>
		<td itemprop="name">Alysheba Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Take Charge Indy</td>
		<!-- 	<td>R. Napravnik</td>
		<td>P. Byrne</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-03">May 03</time></td>
		<td itemprop="name">Kentucky Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3f</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Princess Of Slymar</td>
		<!-- 	<td>M. Smith</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Twin Spires Turf Sprint</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$125,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>5.0 T</td> -->
		<td>Berlino Di Tiger</td>
		<!-- 	<td>L. Goncalves</td>
		<td>E. Caramori</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Humana Distaff</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>4up f/m</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Aubby K</td>
		<!-- 	<td>E. Prado</td>
		<td>R. Nicks</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Churchill Downs Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Delaunay</td>
		<!-- 	<td>R. Napravnik</td>
		<td>T. Amoss</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Woodford Reserve Turf Classic Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Wise Dan</td>
		<!-- 	<td>J. Lezcano</td>
		<td>C. LoPresti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Kentucky Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$2,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Orb</td>
		<!-- 	<td>J. Rosario</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Mervyn LeRoy Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Liaison</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Fort Marcy Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Lubash</td>
		<!-- 	<td>J. Alvarado</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-04">May 04</time></td>
		<td itemprop="name">Beaugay Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Hessonite</td>
		<!-- 	<td>J. Alvarado</td>
		<td>D. Donk</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-11">May 11</time></td>
		<td itemprop="name">Peter Pan Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Freedom Child</td>
		<!-- 	<td>L. Saez</td>
		<td>T. Albertrani</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-11">May 11</time></td>
		<td itemprop="name">Senorita Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Charlie Em</td>
		<!-- 	<td>G. Gomez</td>
		<td>P. Gallagher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-17">May 17</time></td>
		<td itemprop="name">Pimlico Special</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.5</td> -->
		<td>Last Gunfighter</td>
		<!-- 	<td>J. Castellano</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-17">May 17</time></td>
		<td itemprop="name">Black-Eyed Susan Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Fiftyshadesofhay</td>
		<!-- 	<td>J. Rosario</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Maryland Sprint Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Sage Valley</td>
		<!-- 	<td>J. Velazquez</td>
		<td>R. Rodriguez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Gallorette Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Pianist</td>
		<!-- 	<td>M. Smith</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Allaire duPont Distaff Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Summer Applause</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Dixie Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Skyring</td>
		<!-- 	<td>G. Stevens</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Preakness Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.5</td> -->
		<td>Oxbow</td>
		<!-- 	<td>G. Stevens</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Red Bank Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Za Approval</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Marjorie L. Everett Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Open Water</td>
		<!-- 	<td>J. Talamo</td>
		<td>E. Guillot</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-18">May 18</time></td>
		<td itemprop="name">Vagrancy Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Glorious View</td>
		<!-- 	<td>J. Alvarado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">Louisville Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Dark Cove</td>
		<!-- 	<td>R. Napravnik</td>
		<td>M. Maker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">American Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Obviously</td>
		<!-- 	<td>J. Talamo</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">Sheepshead Bay Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Tannery</td>
		<!-- 	<td>L. Saez</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">Hanshin Cup Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Hogy</td>
		<!-- 	<td>C. Emigh</td>
		<td>S. Becker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">Arlington Matron Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Imposing Grace</td>
		<!-- 	<td>C. Hill</td>
		<td>W. Catalano</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-25">May 25</time></td>
		<td itemprop="name">Arlington Classic Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>General Election</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>K. Gorder</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Lone Star Park Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/lone-star-park" itemprop="url">Lone Star Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Master Rick</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">All American Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate-fields" itemprop="url">Golden Gate Fields</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Summer Hit</td>
		<!-- 	<td>R. Baze</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Winning Colors Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Beat The Blues</td>
		<!-- 	<td>M. Mena</td>
		<td>W. Calhoun</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Los Angeles Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Comma To The Top</td>
		<!-- 	<td>E. Maldonado</td>
		<td>P. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Gamely Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Marketing Mix</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Sands Point Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Discreet Marq</td>
		<!-- 	<td>C. Clement</td>
		<td>J. Lezcano</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Acorn Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Midnight Lucky</td>
		<!-- 	<td>R. Napravnik</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Ogden Phipps Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Tiz Miz Sue</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>S. Hobby</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-05-27">May 27</time></td>
		<td itemprop="name">Metropolitan Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Sahara Sky</td>
		<!-- 	<td>J. Rosario</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-01">Jun 01</time></td>
		<td itemprop="name">The Californian</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Clubhouse Ride</td>
		<!-- 	<td>G. Gomez</td>
		<td>C. Lewis</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-01">Jun 01</time></td>
		<td itemprop="name">Aristides</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Scatman</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-01">Jun 01</time></td>
		<td itemprop="name">Hendrie Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Delightful Mary</td>
		<!-- 	<td>L. Contreras</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-02">Jun 02</time></td>
		<td itemprop="name">Vigil Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$156,000</td>
		<!-- <td>4up</td> -->
		<!-- 	<td>7</td> -->
		<td>Laugh Track</td>
		<!-- 	<td>L. Contreras</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-02">Jun 02</time></td>
		<td itemprop="name">Affirmed Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Tiz A Minister</td>
		<!-- 	<td>C. Nakatani</td>
		<td>P. Aguirre</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-07">Jun 07</time></td>
		<td itemprop="name">Brooklyn Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12.0</td> -->
		<td>Calidoscopio</td>
		<!-- 	<td>A. Gryder</td>
		<td>M. Puype</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-07">Jun 07</time></td>
		<td itemprop="name">Jaipur Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>7.0 T</td> -->
		<td>Souper Speedy</td>
		<!-- 	<td>J. Lezcano</td>
		<td>T. Albertrani</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Mint Julep Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Miz Ida</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>S. Margolis</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Honeymoon Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Sarach</td>
		<!-- 	<td>M. Garcia</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Charles Whittingham Memorial Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Tale Of A Champion</td>
		<!-- 	<td>J. Talamo</td>
		<td>K. Mulhall</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Woody Stephens Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Forty Tales</td>
		<!-- 	<td>J. Rosario</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">True North Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Fast Bullet</td>
		<!-- 	<td>J. Rosario</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Manhattan Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Point Of Entry</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-08">Jun 08</time></td>
		<td itemprop="name">Belmont Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>12.0</td> -->
		<td>Palace Malice</td>
		<!-- 	<td>M. Smith</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-09">Jun 09</time></td>
		<td itemprop="name">Monmouth Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Boiserous</td>
		<!-- 	<td>C. McGaughey III</td>
		<td>J. Velazquez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-15">Jun 15</time></td>
		<td itemprop="name">Fleur de Lis Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$175,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Funny Proposition</td>
		<!-- 	<td>J. Rosario</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-15">Jun 15</time></td>
		<td itemprop="name">Stephen Foster Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Fort Larned</td>
		<!-- 	<td>B. Hernandez, Jr.</td>
		<td>I. Wilkes</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-15">Jun 15</time></td>
		<td itemprop="name">Vanity Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Byrama</td>
		<!-- 	<td>G. Stevens</td>
		<td>S. Callaghan</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-15">Jun 15</time></td>
		<td itemprop="name">Hill Prince Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Notacatbutallama</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-16">Jun 16</time></td>
		<td itemprop="name">Pegasus Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Verrazano</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-22">Jun 22</time></td>
		<td itemprop="name">Edward P. Evans All Along Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Colonial Downs</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Channel Lady</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-22">Jun 22</time></td>
		<td itemprop="name">Colonial Turf Cup Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Colonial Downs</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.5 T</td> -->
		<td>London Lane</td>
		<!-- 	<td>H. Karamanos</td>
		<td>L. Murray</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-22">Jun 22</time></td>
		<td itemprop="name">Hollywood Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Doinghardtimeagain</td>
		<!-- 	<td>R. Bejarano</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-22">Jun 22</time></td>
		<td itemprop="name">Mother Goose Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Close Hatches</td>
		<!-- 	<td>J. Rosario</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-23">Jun 23</time></td>
		<td itemprop="name">King Edward Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Riding The River</td>
		<!-- 	<td>T. Kabel</td>
		<td>D. Cotey</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Iowa Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Fiftyshadesofhay</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Iowa Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Looking Cool</td>
		<!-- 	<td>L. Goncalves</td>
		<td>C. Nafzger</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Prairie Meadows Cornhusker Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Prayer For Relief</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Eatontown Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Laughing</td>
		<!-- 	<td>A. Serpa</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Bashford Manor Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Debt Ceiling</td>
		<!-- 	<td>E. Camacho</td>
		<td>J. Robb</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Firecracker Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Wise Dan</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. LoPresti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Triple Bend Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Centralinteligence</td>
		<!-- 	<td>V. Espinoza</td>
		<td>R. Ellis</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Shoemaker Mile Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Obviously</td>
		<!-- 	<td>J. Talamo</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Victory Ride Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Baby J</td>
		<!-- 	<td>J. Rosario</td>
		<td>P. Reynolds</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">New York Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Starformer</td>
		<!-- 	<td>E. Prado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-06-29">Jun 29</time></td>
		<td itemprop="name">Chicago Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Cozze Up Lady</td>
		<!-- 	<td>M. Mena</td>
		<td>W. Calhoun</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-01">Jul 01</time></td>
		<td itemprop="name">Dominion Day Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Delegation</td>
		<!-- 	<td>L. Contreras</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-04">Jul 04</time></td>
		<td itemprop="name">Poker Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>King Kreesa</td>
		<!-- 	<td>I. Ortiz, Jr.</td>
		<td>J. Englehart</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-04">Jul 04</time></td>
		<td itemprop="name">Jersey Shore Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Rainbow Heir</td>
		<!-- 	<td>E. Trujillo</td>
		<td>B. Perkins, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-04">Jul 04</time></td>
		<td itemprop="name">Swaps Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Chief Havoc</td>
		<!-- 	<td>R. Bejarano</td>
		<td>P. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Dwyer Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Moreno</td>
		<!-- 	<td>J. Ortiz</td>
		<td>E. Guillot</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Suburban Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Flat Out</td>
		<!-- 	<td>J. Alvarado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Smile Sprint Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder Race Course</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Bahamian Squall</td>
		<!-- 	<td>L. Saez</td>
		<td>D. Fawkes</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Princess Rooney Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder Race Course</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Starship Truffles</td>
		<!-- 	<td>E. Zayas</td>
		<td>M. Wolfson</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Carry Back Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder Race Course</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Mico Margarita</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Azalea Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder Race Course</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Wildcat Lily</td>
		<!-- 	<td>J. Alvarez</td>
		<td>M. Azpurua</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">United Nations Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Big Blue Kitten</td>
		<!-- 	<td>C. Brown</td>
		<td>J. Bravo</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Salvator Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Raging Daoust</td>
		<!-- 	<td>V. Santiago</td>
		<td>C. Carlesimo, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Royal Heroine Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Schiaparelli</td>
		<!-- 	<td>J. Talamo</td>
		<td>M. Puype</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-06">Jul 06</time></td>
		<td itemprop="name">Hollywood Gold Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Game On Dude</td>
		<!-- 	<td>M. Smith</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Virginia Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Colonial Downs</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>War Dancer</td>
		<!-- 	<td>A. Garcia</td>
		<td>K. McPeek</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Delaware Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Dancing Afleet</td>
		<!-- 	<td>J. Navarro</td>
		<td>T. Ritchey</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Robert G. Dick Memorial Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Treasured Up</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>A. Stall, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Man O&rsquo; War Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Boisterous</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Stars and Stripes Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Dark Cove</td>
		<!-- 	<td>R. Napravnik</td>
		<td>M. Maker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Modesty Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>9.5 T</td> -->
		<td>Ausus</td>
		<!-- 	<td>J. Graham</td>
		<td>D. Peitz</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Arlington Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Rahystrada</td>
		<!-- 	<td>R. Napravnik</td>
		<td>B. Hughes</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">American Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>9.5 T</td> -->
		<td>Infinite Magic</td>
		<!-- 	<td>C. Hill</td>
		<td>R. Mettee</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">Hollywood Juvenile Championship</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2-yo M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Alpine Luck</td>
		<!-- 	<td>G. Stevens</td>
		<td>M. Harrington</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">A Gleam Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Book Review</td>
		<!-- 	<td>R. Bejarano</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-13">Jul 13</time></td>
		<td itemprop="name">American Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Emollient</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-14">Jul 14</time></td>
		<td itemprop="name">Sunset Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Marketing Mix</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-17">Jul 17</time></td>
		<td itemprop="name">Bold Venture Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Essence Hit Man</td>
		<!-- 	<td>J. Campbell</td>
		<td>L. Cappuccitti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-19">Jul 19</time></td>
		<td itemprop="name">Schuylerville Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2-yo F</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Brazen Persuasion</td>
		<!-- 	<td>R. Napravnik</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-19">Jul 19</time></td>
		<td itemprop="name">James Marvin Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Sage Valley</td>
		<!-- 	<td>C. Velasquez</td>
		<td>R. Rodriguez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Arlington Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>9.0</td> -->
		<td>My Option</td>
		<!-- 	<td>E. Perez</td>
		<td>C. Block</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Ontario Matron Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Sisterly Love</td>
		<!-- 	<td>E. Da Silva</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Eddie Read Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Jeranimo</td>
		<!-- 	<td>R. Bejarano</td>
		<td>M. Pender</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Shuvee Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Authenticity</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Coaching Club American Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Princess Of Sylmar</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-20">Jul 20</time></td>
		<td itemprop="name">Delaware Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Royal Delta</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-21">Jul 21</time></td>
		<td itemprop="name">San Clemente Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-yo f</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Wishing Gate</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-21">Jul 21</time></td>
		<td itemprop="name">Sanford Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2-yo M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Wired Bryan</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>M. Dilger</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-21">Jul 21</time></td>
		<td itemprop="name">Nijinsky Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>So Long George</td>
		<!-- 	<td>E. Wilson</td>
		<td>J. Charalambous</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-27">Jul 27</time></td>
		<td itemprop="name">Diana Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Laughing</td>
		<!-- 	<td>J. Lezcano</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-27">Jul 27</time></td>
		<td itemprop="name">Jim Dandy Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Palice Malice</td>
		<!-- 	<td>M. Smith</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-27">Jul 27</time></td>
		<td itemprop="name">Prioress Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Lighthouse Bay</td>
		<!-- 	<td>J. Rocco, Jr</td>
		<td>G. Weaver</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-27">Jul 27</time></td>
		<td itemprop="name">San Diego Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Kettle Corn</td>
		<!-- 	<td>V. Espinoza</td>
		<td>J. Sadler</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Bing Crosby Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Points Offthebench</td>
		<!-- 	<td>M. Smith</td>
		<td>T. Yakteen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Taylor Made Matchmaker Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Starstruck</td>
		<!-- 	<td>R. Napravnik</td>
		<td>J. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Oceanport Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Silver Max</td>
		<!-- 	<td>R. Albarado</td>
		<td>D. Romans</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Monmouth Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Pants On Fire</td>
		<!-- 	<td>P. Lopez</td>
		<td>K. Breen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Molly Pitcher Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Joyful Victory</td>
		<!-- 	<td>R. Napravnik</td>
		<td>J. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Haskell Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Verrazano</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Amsterdam Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Forty Tales</td>
		<!-- 	<td>J. Rosario</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-07-28">Jul 28</time></td>
		<td itemprop="name">Royal North Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up f/m</td> -->
		<!-- 	<td>6.0 T</td> -->
		<td>Nikkis Smartypants</td>
		<!-- 	<td>E. Da Silva</td>
		<td>R. Tiller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-07-29">Jul 29</time></td>
		<td itemprop="name">Honorable Miss Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Dance To Bristol</td>
		<!-- 	<td>X. Perez</td>
		<td>O. Figgins, III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-03">Aug 03</time></td>
		<td itemprop="name">Seagram Cup Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Alpha Bettor</td>
		<!-- 	<td>J. Stein</td>
		<td>D. Vella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-03">Aug 03</time></td>
		<td itemprop="name">Whitney Invitational Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Cross Traffic</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-03">Aug 03</time></td>
		<td itemprop="name">Clement L. Hirsch Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Lady Of Fifty</td>
		<!-- 	<td>C. Nakatani</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-03">Aug 03</time></td>
		<td itemprop="name">West Virginia Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Mountaineer</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Departing</td>
		<!-- 	<td>R. Albarado</td>
		<td>A. Stall, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-04">Aug 04</time></td>
		<td itemprop="name">Alfred G. Vanderbilt Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Justin Phillip</td>
		<!-- 	<td>J. Velazquez</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-09">Aug 09</time></td>
		<td itemprop="name">National Museum of Racing Hall Of Fame</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Notacatbutallama</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-10">Aug 10</time></td>
		<td itemprop="name">La Jolla Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Dice Flavor</td>
		<!-- 	<td>G. Gomez</td>
		<td>P. Gallagher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-10">Aug 10</time></td>
		<td itemprop="name">Gardenia Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/ellis-park" itemprop="url">Ellis Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Devious Intent</td>
		<!-- 	<td>R. Morales</td>
		<td>K. Gorder</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-10">Aug 10</time></td>
		<td itemprop="name">Fourstardave Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Wise Dan</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. LoPresti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-10">Aug 10</time></td>
		<td itemprop="name">Monmouth Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Seaneen Girl</td>
		<!-- 	<td>P. Lopez</td>
		<td>B. Flint</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-11">Aug 11</time></td>
		<td itemprop="name">Saratoga Special Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2-yo M</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Corfu</td>
		<!-- 	<td>J. Velazquez</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-11">Aug 11</time></td>
		<td itemprop="name">Adirondack Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2-yo F</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Designer Legs</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>D. Stewart</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-11">Aug 11</time></td>
		<td itemprop="name">John C. Mabee Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Tiz Flirtatious</td>
		<!-- 	<td>J. Leparoux</td>
		<td>M. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Ontario Colleen Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Leigh Court</td>
		<!-- 	<td>G. Boulanger</td>
		<td>J. Carroll</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Alabama Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Princess Of Sylmar</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Sword Dancer Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>12.0 T</td> -->
		<td>Big Blue Kitten</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Del Mar Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo f</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Discreet Marq</td>
		<!-- 	<td>J. Leparoux</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Arlington Million</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Real Solution</td>
		<!-- 	<td>A. Garcia</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Beverly D. Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>10.5 T</td> -->
		<td>Dank</td>
		<!-- 	<td>R. Moore</td>
		<td>S. Stoute</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-17">Aug 17</time></td>
		<td itemprop="name">Secretariat Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>10.0 T</td> -->
		<td>Admiral Kitten</td>
		<!-- 	<td>R. Napravnik</td>
		<td>M. Maker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-18">Aug 18</time></td>
		<td itemprop="name">Philip H. Iselin Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Last Gunfighter</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-18">Aug 18</time></td>
		<td itemprop="name">Woodford Reserve Lake Placid</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Caroline Thomas</td>
		<!-- 	<td>R. Napravnik</td>
		<td>B. Tagg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-18">Aug 18</time></td>
		<td itemprop="name">Longacres Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/emerald-downs" itemprop="url">Emerald Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.0</td> -->
		<td>Herbie D</td>
		<!-- 	<td>A. Perez</td>
		<td>R. Gilker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-18">Aug 18</time></td>
		<td itemprop="name">Rancho Bernardo Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up f/m</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Reneesgotzip</td>
		<!-- 	<td>G. Gomez</td>
		<td>P. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-23">Aug 23</time></td>
		<td itemprop="name">Ballerina Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-up F</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Dance To Bristol</td>
		<!-- 	<td>X. Perez</td>
		<td>O. Figgins, III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-24">Aug 24</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/travers-stakes">Travers Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3-yo M</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Will Take Charge</td>
		<!-- 	<td>L. Saez</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-24">Aug 24</time></td>
		<td itemprop="name">Test Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-yo F</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Sweet Lulu</td>
		<!-- 	<td>J. Leparoux</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-24">Aug 24</time></td>
		<td itemprop="name">Ballston Spa Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Laughing</td>
		<!-- 	<td>J. Lezcano</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-24">Aug 24</time></td>
		<td itemprop="name">Del Mar Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>11.0 T</td> -->
		<td>Vagabond Shoes</td>
		<!-- 	<td>V. Espinoza</td>
		<td>J. Sadler</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-25">Aug 25</time></td>
		<td itemprop="name">Pacific Classic</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3-up</td> -->
		<!-- 	<td>10.0</td> -->
		<td>Game On Dude</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-25">Aug 25</time></td>
		<td itemprop="name">Del Mar Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up</td> -->
		<!-- 	<td>8.0 T</td> -->
		<td>Obviously</td>
		<!-- 	<td>J. Talamo</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-25">Aug 25</time></td>
		<td itemprop="name">Personal Ensign Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3-up FM</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Royal Delta</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-25">Aug 25</time></td>
		<td itemprop="name">Cliff Hanger Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Summer Front</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-25">Aug 25</time></td>
		<td itemprop="name">Play the King Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3-up</td> -->
		<!-- 	<td>7.0 T</td> -->
		<td>Dimension</td>
		<!-- 	<td>D. Moran</td>
		<td>C. Murphy</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-29">Aug 29</time></td>
		<td itemprop="name">With Anticipation Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2-yo M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Bashart</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-31">Aug 31</time></td>
		<td itemprop="name">Washington Park Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Willcox Inn</td>
		<!-- 	<td>J. Graham</td>
		<td>M. Stidham</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-31">Aug 31</time></td>
		<td itemprop="name">Bernard Baruch Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3-UP M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Silver Max</td>
		<!-- 	<td>R. Albarado</td>
		<td>D. Romans</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-08-31">Aug 31</time></td>
		<td itemprop="name">Forego Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Strapping Groom</td>
		<!-- 	<td>J. Alvarado</td>
		<td>D. Jacobson</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-08-31">Aug 31</time></td>
		<td itemprop="name">Woodward Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3-up M</td> -->
		<!-- 	<td>9.0</td> -->
		<td>Alpha</td>
		<!-- 	<td>J. Velazquez</td>
		<td>K. McLaughlin</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-01">Sep 01</time></td>
		<td itemprop="name">Saranac Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Five Iron</td>
		<!-- 	<td>L. Saez</td>
		<td>B. Lynch</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-01">Sep 01</time></td>
		<td itemprop="name">Spinaway Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>2yo f</td> -->
		<!-- 	<td>7.0</td> -->
		<td>Sweet Reason</td>
		<!-- 	<td>A. Solis</td>
		<td>L. Gyarmati</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-01">Sep 01</time></td>
		<td itemprop="name">Sapling Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/monmouth-park" itemprop="url">Monmouth Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>6.0</td> -->
		<td>Dunkin Bend</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-01">Sep 01</time></td>
		<td itemprop="name">Del Mar Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9.0 T</td> -->
		<td>Gabriel Charles</td>
		<!-- 	<td>M. Smith</td>
		<td>J. Mullins</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Glens Falls Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>n/a</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>11 T</td> -->
		<td>Lady Cohiba</td>
		<!-- 	<td>J. Alvarado</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Three Chimneys Hopeful Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/saratoga" itemprop="url">Saratoga</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>7</td> -->
		<td>Strong Mandate</td>
		<!-- 	<td>J. Ortiz</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Greenwood Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>12</td> -->
		<td>Eldaafer</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>D. Alvarado</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Turf Monster Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>5 T</td> -->
		<td>Stormofthecentury</td>
		<!-- 	<td>S. Elliott</td>
		<td>L. Ruberto, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Smarty Jones Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.32</td> -->
		<td>Edge Of Reality</td>
		<!-- 	<td>S. Elliott</td>
		<td>A. Dutrow</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-02">Sep 02</time></td>
		<td itemprop="name">Yellow Ribbon Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Egg Drop</td>
		<!-- 	<td>M. Garcia</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-04">Sep 04</time></td>
		<td itemprop="name">Del Mar Futurity</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/del-mar" itemprop="url">Del Mar</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>7</td> -->
		<td>Tamarando</td>
		<!-- 	<td>J. Leparoux</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Super Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/louisiana-downs" itemprop="url">Louisiana Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Departing</td>
		<!-- 	<td>R. Albarado</td>
		<td>A. Stall, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Kent Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Are You Kidding Me</td>
		<!-- 	<td>R. Maragh</td>
		<td>R. Attfield</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Ack Ack Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8</td> -->
		<td>Pants On Fire</td>
		<!-- 	<td>P. Lopez</td>
		<td>K. Breen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Pocahontas Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Untapable</td>
		<!-- 	<td>R. Napravnik</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Iroquois Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Cleburne</td>
		<!-- 	<td>C. Lanerie</td>
		<td>D. Romans</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Bowling Green Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10 T</td> -->
		<td>Hyper</td>
		<!-- 	<td>J. Castellano</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-07">Sep 07</time></td>
		<td itemprop="name">Arlington-Washington Futurity</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Arlington</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8</td> -->
		<td>Solitary Ranger</td>
		<!-- 	<td>F. Geroux</td>
		<td>W. Catalano</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-08">Sep 08</time></td>
		<td itemprop="name">British Columbia Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Hastings</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Title Contender</td>
		<!-- 	<td>R. Walcott</td>
		<td>A. Bolton</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-09">Sep 09</time></td>
		<td itemprop="name">Presque Isle Downs Masters Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/presque-isle-downs" itemprop="url">Presque Isle Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Groupie Doll</td>
		<!-- 	<td>R. Maragh</td>
		<td>W. Bradley</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-14">Sep 14</time></td>
		<td itemprop="name">Summer Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8 T</td> -->
		<td>My Conquestadory</td>
		<!-- 	<td>E. Da Silva</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-14">Sep 14</time></td>
		<td itemprop="name">Natalma Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Llanarmon</td>
		<!-- 	<td>E. Wilson</td>
		<td>R. Attfield</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-14">Sep 14</time></td>
		<td itemprop="name">Kentucky Turf Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/kentucky-downs" itemprop="url">Kentucky Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Temeraine</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-14">Sep 14</time></td>
		<td itemprop="name">Noble Damsel</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Peace Preserver</td>
		<!-- 	<td>I. Ortiz, Jr.</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-14">Sep 14</time></td>
		<td itemprop="name">Garden City</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Alterite</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-15">Sep 15</time></td>
		<td itemprop="name">Ontario Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>His Race To Win</td>
		<!-- 	<td>E. Da Silva</td>
		<td>M. Pierce</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-15">Sep 15</time></td>
		<td itemprop="name">Northern Dancer Turf</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Forte Del Marmi</td>
		<!-- 	<td>E. Da Silva</td>
		<td>R. Attfield</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-15">Sep 15</time></td>
		<td itemprop="name">Canadian Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Minakshi</td>
		<!-- 	<td>L. Contreras</td>
		<td>M. Matz</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-15">Sep 15</time></td>
		<td itemprop="name">Ricoh Woodbine Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Wise Dan</td>
		<!-- 	<td>J. Velazquez</td>
		<td>C. LoPresti</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-21">Sep 21</time></td>
		<td itemprop="name">Gallant Bob Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>City Of Weston</td>
		<!-- 	<td>P. Lopez</td>
		<td>A. Sano</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-21">Sep 21</time></td>
		<td itemprop="name">Pennsylvania Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Will Take Charge</td>
		<!-- 	<td>L. Saez</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-21">Sep 21</time></td>
		<td itemprop="name">Cotillion Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Close Hatches</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-21">Sep 21</time></td>
		<td itemprop="name">Dogwood Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>7</td> -->
		<td>Sky Girl</td>
		<!-- 	<td>C. Lanerie</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-21">Sep 21</time></td>
		<td itemprop="name">Gallant Bloom Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Cluster Of Stars</td>
		<!-- 	<td>J. Castellano</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-27">Sep 27</time></td>
		<td itemprop="name">Eddie D Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6.5 T</td> -->
		<td>Chips All In</td>
		<!-- 	<td>J. Leparoux</td>
		<td>J. Mullins</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Zenyatta Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Beholder</td>
		<!-- 	<td>G. Stevens</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Rodeo Drive Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>10 T</td> -->
		<td>Tiz Flirtatious</td>
		<!-- 	<td>J. Leparoux</td>
		<td>M. Jones</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Chandelier Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Secret Compass</td>
		<!-- 	<td>R. Napravnik</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Awesome Again Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Mucho Macho Man</td>
		<!-- 	<td>G. Stevens</td>
		<td>K. Ritvo</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Jefferson Cup Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8 T</td> -->
		<td>General Election</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>K. Gorder</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Kelso Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8</td> -->
		<td>Graydar</td>
		<!-- 	<td>E. Prado</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Beldame Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>9</td> -->
		<td>Princess Of Sylmar</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Joe Hirsch Turf Classic Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Little Mike</td>
		<!-- 	<td>M. Smith</td>
		<td>D. Romans</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Flower Bowl Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$600,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>10 T</td> -->
		<td>Laughing</td>
		<!-- 	<td>J. Lezcano</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-28">Sep 28</time></td>
		<td itemprop="name">Jockey Club Gold Cup Invitational</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10</td> -->
		<td>Ron The Greek</td>
		<!-- 	<td>J. Lezcano</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-29">Sep 29</time></td>
		<td itemprop="name">John Henry Turf Championship</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10</td> -->
		<td>Indy Point</td>
		<!-- 	<td>G. Stevens</td>
		<td>R. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-29">Sep 29</time></td>
		<td itemprop="name">Oklahoma Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/remington-park" itemprop="url">Remington Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Broadway Empire</td>
		<!-- 	<td>R. Walcott</td>
		<td>R. Diodoro</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-09-29">Sep 29</time></td>
		<td itemprop="name">Matron Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>6</td> -->
		<td>Miss Behaviour</td>
		<!-- 	<td>G. Cruise</td>
		<td>P. Schoenthal</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-09-29">Sep 29</time></td>
		<td itemprop="name">Futurity Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>6</td> -->
		<td>In Trouble</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>A. Dutrow</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-04">Oct 04</time></td>
		<td itemprop="name">Darley Alcibiades Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>My Conquestadory</td>
		<!-- 	<td>E. Da Silva</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-04">Oct 04</time></td>
		<td itemprop="name">Stoll Keenon Ogden Phoenix</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Sum Of The Parts</td>
		<!-- 	<td>L. Goncalves</td>
		<td>T. Amoss</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Thoroughbred Club of America Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>6</td> -->
		<td>Judy The Beauty</td>
		<!-- 	<td>J. Velazquez</td>
		<td>W. Ward</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Woodford Stakes Presented by Keeneland Select</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>5.5 T</td> -->
		<td>Havelock</td>
		<!-- 	<td>G. Gomez</td>
		<td>D. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Santa Anita Sprint Championship</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Points Offthebench</td>
		<!-- 	<td>M. Smith</td>
		<td>T. Yakteen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">City of Hope Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8 T</td> -->
		<td>No Jet Lag</td>
		<!-- 	<td>M. Smith</td>
		<td>S. Callaghan</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Mazarine Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Madly Truly</td>
		<!-- 	<td>P. Husbands</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">First Lady Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Better Lucky</td>
		<!-- 	<td>J. Leparoux</td>
		<td>T. Albertrani</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Shadwell Turf Mile</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Silver Max</td>
		<!-- 	<td>R. Albarado</td>
		<td>D. Romans</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Indiana Oaks</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/indiana-downs" itemprop="url">Indiana Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Pure Fun</td>
		<!-- 	<td>V. Lebron</td>
		<td>K. McPeek</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Indiana Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/indiana-downs" itemprop="url">Indiana Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Power Broker</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Hawthorne Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne-race-course" itemprop="url">Hawthorne</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Kid Dreams</td>
		<!-- 	<td>F. Torres</td>
		<td>N. Drysdale</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Jamaica Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Up With The Birds</td>
		<!-- 	<td>C. Velasquez</td>
		<td>M. Pierce</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Frizette Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8</td> -->
		<td>Artemis Agrotera</td>
		<!-- 	<td>J. Lezcano</td>
		<td>M. Hushion</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-05">Oct 05</time></td>
		<td itemprop="name">Foxwoods Champagne Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8</td> -->
		<td>Havana</td>
		<!-- 	<td>I. Ortiz, Jr.</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-06">Oct 06</time></td>
		<td itemprop="name">Durham Cup Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>James Street</td>
		<!-- 	<td>P. Husbands</td>
		<td>J. Carroll</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-06">Oct 06</time></td>
		<td itemprop="name">Bourbon Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Poker Player</td>
		<!-- 	<td>C. Hill</td>
		<td>W. Catalano</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-06">Oct 06</time></td>
		<td itemprop="name">Juddmonte Spinster Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>9</td> -->
		<td>Emollient</td>
		<!-- 	<td>M. Smith</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-06">Oct 06</time></td>
		<td itemprop="name">Miss Grillo Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Testa Rossi</td>
		<!-- 	<td>J. Lezcano</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-09">Oct 09</time></td>
		<td itemprop="name">JPMorgan Chase Jessamine Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Kitten Kaboodle</td>
		<!-- 	<td>A. Garcia</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-12">Oct 12</time></td>
		<td itemprop="name">Knickerbocker Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Za Approval</td>
		<!-- 	<td>J. Rosario</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-12">Oct 12</time></td>
		<td itemprop="name">Athenia</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Pianist</td>
		<!-- 	<td>J. Ortiz</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-14">Oct 14</time></td>
		<td itemprop="name">Ballerina Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Hastings</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>9</td> -->
		<td>Madeira Park</td>
		<!-- 	<td>R. Walcott</td>
		<td>D. Forster</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-17">Oct 17</time></td>
		<td itemprop="name">Sycamore Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12</td> -->
		<td>Najjaar</td>
		<!-- 	<td>J. Graham</td>
		<td>D. Peitz</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-18">Oct 18</time></td>
		<td itemprop="name">Pin Oak Valley View Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Overheard</td>
		<!-- 	<td>E. Da Silva</td>
		<td>M. Pierce</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-19">Oct 19</time></td>
		<td itemprop="name">Lexus Raven Run Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>7</td> -->
		<td>Madame Cactus</td>
		<!-- 	<td>J. Rocco, Jr.</td>
		<td>P. Eurton</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-26">Oct 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/autumn-miss-stakes">Autumn Miss Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Wishing Gate</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-26">Oct 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/hagyard-fayette-stakes">Hagyard Fayette Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Stanwyck</td>
		<!-- 	<td>A. Solis</td>
		<td>J. Shirreffs</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-26">Oct 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/turnback-the-alarm-handicap">Turnback the Alarm Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8.5</td> -->
		<td></td>
		<!-- 	<td></td>
		<td></td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-26">Oct 26</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/bold-ruler-handicap">Bold Ruler Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>7</td> -->
		<td>Clearly Now</td>
		<!-- 	<td>J. Lezcano</td>
		<td>B. Lynch</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-10-27">Oct 27</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/ep-taylor-stakes">E.P. Taylor Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>10 T</td> -->
		<td>Tannery</td>
		<!-- 	<td>J. Rosario</td>
		<td>A. Goldberg</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-10-27">Oct 27</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/pattison-canadian-international">Pattison Canadian International</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Joshua Tree</td>
		<!-- 	<td>R. Moore</td>
		<td>E. Dunlop</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-01">Nov 01</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/twilight-derby">Twilight Derby</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Rookie Sensation</td>
		<!-- 	<td>V. Espinoza</td>
		<td>J. Shirreffs</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-02">Nov 02</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/discovery-handicap">Discovery Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Romansh</td>
		<!-- 	<td>J. Ortiz</td>
		<td>T. Albertrani</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-02">Nov 02</time></td>
		<td itemprop="name">Sen. Ken Maddy Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>6.5</td> -->
		<td>Pontchatrain</td>
		<!-- 	<td>G. Stevens</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-02">Nov 02</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/maple-leaf-stakes">Maple Leaf Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>10</td> -->
		<td>Moonlit Beauty</td>
		<!-- 	<td>G. Boulanger</td>
		<td>J. LeBlanc, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-03">Nov 03</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/goldikova-stakes">Goldikova Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Egg Drop</td>
		<!-- 	<td>M. Garcia</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-03">Nov 03</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/tempted-stakes">Tempted Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8</td> -->
		<td>Stopchargingmaria</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-03">Nov 03</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/nashua-stakes">Nashua Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8</td> -->
		<td>Cairo Prince</td>
		<!-- 	<td>L. Saez</td>
		<td>K. McLaughlin</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-09">Nov 09</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/long-island-handicap">Long Island Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Inimitable Romanee</td>
		<!-- 	<td>C. DeCarlo</td>
		<td>H. Motion</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-15">Nov 15</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/autumn-stakes">Autumn Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Alpha Bettor</td>
		<!-- 	<td>J. Stein</td>
		<td>D. Vella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-16">Nov 16</time></td>
		<td itemprop="name">Mrs. Revere Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>Emotional Kitten</td>
		<!-- 	<td>V. Espinoza</td>
		<td>W. Ward</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-16">Nov 16</time></td>
		<td itemprop="name">Commonwealth Turf Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$120,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8.5 T</td> -->
		<td>River Seven</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>N. Gonzalez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-16">Nov 16</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/red-smith-handicap">Red Smith Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>11 T</td> -->
		<td>Imagining</td>
		<!-- 	<td>J. Rosario</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-23">Nov 23</time></td>
		<td itemprop="name">River City Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$125,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Potomac River</td>
		<!-- 	<td>J. Vargas</td>
		<td>S. Baez</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-23">Nov 23</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/delta-downs-jackpot-stakes">Delta Downs Jackpot Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delta-downs" itemprop="url">Delta Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$1,000,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Rise Up</td>
		<!-- 	<td>G. Melancon</td>
		<td>T. Amoss</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-23">Nov 23</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/delta-downs-princess-stakes">Delta Downs Princess</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delta-downs" itemprop="url">Delta Downs</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>8</td> -->
		<td>Tepin</td>
		<!-- 	<td>M. Mena</td>
		<td>M. Casse</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-28">Nov 28</time></td>
		<td itemprop="name">Vernon O. Underwood Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Majestic Stride</td>
		<!-- 	<td>E. Maldonado</td>
		<td>J. Bonde</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-28">Nov 28</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/fall-highweight-handicap">Fall Highweight Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Palace</td>
		<!-- 	<td>C. Velasquez</td>
		<td>L. Rice</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-28">Nov 28</time></td>
		<td itemprop="name">Falls City Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$165,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Wine Princess</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>S. Margolis</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Kentucky Jockey Club S.</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Tapiture</td>
		<!-- 	<td>R. Santana, Jr.</td>
		<td>S. Asmussen</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Golden Rod S.</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Vexed</td>
		<!-- 	<td>S. Bridgmohan</td>
		<td>A. Stall, Jr.</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Clark Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Will Take Charge</td>
		<!-- 	<td>L. Saez</td>
		<td>D. Lukas</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Berkeley Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate-fields" itemprop="url">Golden Gate Fields</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Summer Hit</td>
		<!-- 	<td>R. Baze</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Citation Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Silentio</td>
		<!-- 	<td>R. Bejarano</td>
		<td>G. Mandella</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-29">Nov 29</time></td>
		<td itemprop="name">Go For Wand Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3up f&amp;m</td> -->
		<!-- 	<td>8</td> -->
		<td>Royal Lahaina</td>
		<!-- 	<td>J. Ortiz</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/remsen-stakes">Remsen Stakes</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>9</td> -->
		<td>Honor Code</td>
		<!-- 	<td>J. Castellano</td>
		<td>C. McGaughey III</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name">Kennedy Road Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Bear No Joke</td>
		<!-- 	<td>E. Wilson</td>
		<td>R. Baker</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name">Miesque Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Full Ransom</td>
		<!-- 	<td>V. Espinoza</td>
		<td>J. Cassidy</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name">Generous Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Global View</td>
		<!-- 	<td>J. Talamo</td>
		<td>T. Proctor</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/hawthorne-gold-cup-handicap">Hawthorne Gold Cup Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne-race-course" itemprop="url">Hawthorne</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$350,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>10</td> -->
		<td>Last Gunfighter</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Brown</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name">Comely Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>9</td> -->
		<td>Wedding Toast</td>
		<!-- 	<td>J. Castellano</td>
		<td>K. McLaughlin</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name">Demoiselle Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$400,000</td>
		<!-- <td>2yo F</td> -->
		<!-- 	<td>9</td> -->
		<td>Stopchargingmaria</td>
		<!-- 	<td>J. Castellano</td>
		<td>T. Pletcher</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-11-30">Nov 30</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/cigar-mile-handicap">Cigar Mile Handicap</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8</td> -->
		<td>Flat Out</td>
		<!-- 	<td>J. Alvarado</td>
		<td>W. Mott</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-01">Dec 01</time></td>
		<td itemprop="name">Bessarabian Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>7</td> -->
		<td>Part The Seas</td>
		<!-- 	<td>J. McAleney</td>
		<td>M. De Paulo</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-01">Dec 01</time></td>
		<td itemprop="name">Hollywood Derby</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>10 T</td> -->
		<td>Seek Again</td>
		<!-- 	<td>C. Nakatani</td>
		<td>J. Gosden</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-01">Dec 01</time></td>
		<td itemprop="name">Matriarch Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Egg Drop</td>
		<!-- 	<td>M. Garcia</td>
		<td>M. Mitchell</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-07">Dec 07</time></td>
		<td itemprop="name">Tropical Turf Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Speaking Of Which</td>
		<!-- 	<td>J. Bravo</td>
		<td>C. Clement</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-07">Dec 07</time></td>
		<td itemprop="name">My Charmer Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up F/M</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Valiant Girl</td>
		<!-- 	<td>M. Rispoli</td>
		<td>H. Motion</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-07">Dec 07</time></td>
		<td itemprop="name">Fred W. Hooper Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/calder-race-course" itemprop="url">Calder</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Csaba</td>
		<!-- 	<td>L. Saez</td>
		<td>P. Gleaves</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-07">Dec 07</time></td>
		<td itemprop="name">Bayakoa Handicap</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up f&amp;m</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Broken Sword</td>
		<!-- 	<td>J. Rosario</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-07">Dec 07</time></td>
		<td itemprop="name">Hollywood Starlet Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$500,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Streaming</td>
		<!-- 	<td>M. Garcia</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-14">Dec 14</time></td>
		<td itemprop="name">Hollywood Turf Cup</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$250,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>12 T</td> -->
		<td>Lucayan</td>
		<!-- 	<td>J. Rosario</td>
		<td>N. Drysdale</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-14">Dec 14</time></td>
		<td itemprop="name">Native Diver Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>9</td> -->
		<td>Blueskiesnrainbows</td>
		<!-- 	<td>M. Pedroza</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-14">Dec 14</time></td>
		<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/cashcall-futurity">CashCall Futurity</a></td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hollywood-park" itemprop="url">Hollywood Park</a></span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$750,000</td>
		<!-- <td>2yo</td> -->
		<!-- 	<td>8.5</td> -->
		<td>Shared Belief</td>
		<!-- 	<td>C. Nakatani</td>
		<td>J. Hollendorfer</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-14">Dec 14</time></td>
		<td itemprop="name">Sugar Swirl Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Heart Stealer</td>
		<!-- 	<td>J. Castellano</td>
		<td>M. Wolfson</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-15">Dec 15</time></td>
		<td itemprop="name">Valedictory Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$150,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>14</td> -->
		<td>Quaesitor</td>
		<!-- 	<td>I. Howard</td>
		<td>G. Olguin</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-26">Dec 26</time></td>
		<td itemprop="name">La Brea</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo F</td> -->
		<!-- 	<td>7</td> -->
		<td>Heir Kitty</td>
		<!-- 	<td>G. Stevens</td>
		<td>P. Miller</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-26">Dec 26</time></td>
		<td itemprop="name">Sir Beaufort Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>II</td> -->
		<td itemprop="description">$200,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>8 T</td> -->
		<td>Gervinho</td>
		<!-- 	<td>R. Bejarano</td>
		<td>C. Gaines</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-26">Dec 26</time></td>
		<td itemprop="name">Malibu Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>I</td> -->
		<td itemprop="description">$300,000</td>
		<!-- <td>3yo</td> -->
		<!-- 	<td>7</td> -->
		<td>Shakin It Up</td>
		<!-- 	<td>D. Flores</td>
		<td>B. Baffert</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event" class="odd">		<td class="num"><time itemprop="startDate" content="2014-12-28">Dec 28</time></td>
		<td itemprop="name">Mr. Prospector Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up</td> -->
		<!-- 	<td>6</td> -->
		<td>Singanothersong</td>
		<!-- 	<td>J. Leyva</td>
		<td>R. Pellegrini</td> -->
	</tr>
	<tr itemscope="" itemtype="http://schema.org/Event">		<td class="num"><time itemprop="startDate" content="2014-12-29">Dec 29</time></td>
		<td itemprop="name">Robert J. Frankel Stakes</td>
		<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span></td>
		<!-- <td>III</td> -->
		<td itemprop="description">$100,000</td>
		<!-- <td>3up f/m</td> -->
		<!-- 	<td>9 T</td> -->
		<td>Customer Base</td>
		<!-- 	<td>T. Proctor</td>
		<td>M. Smith</td> -->
	</tr>
	</tbody>
</table>
