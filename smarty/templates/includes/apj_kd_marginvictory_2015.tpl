<div class="table-responsive">
<table class="table table-condensed table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0" title="Kentucky Derby Odds" summary="Kentucky Derby Odds">
<!-- <tr>
<th> Derby Contenders</th>
<th class="right">Odds to Win</th>

</tr> -->
	<tr>
		<td>1 Length to 2 3/4 lengths</td>
		<td class="right sortOdds" align="right">+200</td>
	</tr>
		<tr>
		<td>3 Lengths to 5 3/4 lengths</td>
		<td class="right sortOdds" align="right">+250</td>
	</tr>
		<tr>
		<td>1/2 Length to 3/4 length</td>
		<td class="right sortOdds" align="right">+400</td>
	</tr>
		<tr>
		<td>6 Lengths to 7 3/4 lengths</td>
		<td class="right sortOdds" align="right">+700</td>
	</tr>
		<tr>
		<td>8 Lengths to 10 3/4 lengths</td>
		<td class="right sortOdds" align="right">+800</td>
	</tr>
		<tr>
		<td>Head</td>
		<td class="right sortOdds" align="right">+800</td>
	</tr>
		<tr>
		<td>Neck</td>
		<td class="right sortOdds" align="right">+800</td>
	</tr>
		<tr>
		<td>11 Lengths to 14 3/4 lengths</td>
		<td class="right sortOdds" align="right">+1000</td>
	</tr>
		<tr>
		<td>Nose</td>
		<td class="right sortOdds" align="right">+1000</td>
	</tr>
		<tr>
		<td>15 Lengths or More</td>
		<td class="right sortOdds" align="right">+1200</td>
	</tr>
		<tr>
		<td>Dead Heat</td>
		<td class="right sortOdds" align="right">+3000</td>
	</tr>
	
</table>
</div>