
<div class="schedule">
					<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Monday, June 22, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Parx Racing</div><div class='tsBody'> <strong>Location:</strong> Bensalem, PA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:25 PM&nbsp;&nbsp;&nbsp;12:52 PM&nbsp;&nbsp;&nbsp;1:19 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:07 PM&nbsp;&nbsp;&nbsp;3:34 PM&nbsp;&nbsp;&nbsp;4:01 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:25 PM</span>
							<span class="race">Parx Racing</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Northlands Park</div><div class='tsBody'> <strong>Location:</strong> Edmonton, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:28 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:24 PM (Cancelled)&nbsp;&nbsp;&nbsp;2:52 PM (Cancelled)&nbsp;&nbsp;&nbsp;3:20 PM (Cancelled)&nbsp;&nbsp;&nbsp;<br>3:48 PM (Cancelled)&nbsp;&nbsp;&nbsp;4:16 PM (Cancelled)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Northlands Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ruidoso Downs</div><div class='tsBody'> <strong>Location:</strong> Ruidoso Downs, NM  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:26 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:18 PM&nbsp;&nbsp;&nbsp;2:44 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:36 PM&nbsp;&nbsp;&nbsp;4:02 PM&nbsp;&nbsp;&nbsp;4:28 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Ruidoso Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Finger Lakes</div><div class='tsBody'> <strong>Location:</strong> Farmington, NY  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 72 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:10 PM&nbsp;&nbsp;&nbsp;1:37 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:31 PM&nbsp;&nbsp;&nbsp;2:58 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:52 PM&nbsp;&nbsp;&nbsp;4:19 PM&nbsp;&nbsp;&nbsp;4:46 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:10 PM</span>
							<span class="race">Finger Lakes</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Delaware Park</div><div class='tsBody'> <strong>Location:</strong> Wilmington, DE  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Delaware Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Suffolk Downs</div><div class='tsBody'> <strong>Location:</strong> East Boston, MA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:44 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:11 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;5:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Suffolk Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Beulah Park</div><div class='tsBody'> <strong>Location:</strong> Grove City, OH  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:14 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:06 PM&nbsp;&nbsp;&nbsp;4:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Beulah Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Presque Isle Downs</div><div class='tsBody'> <strong>Location:</strong> Erie, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>5:25 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;<br>7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:25 PM</span>
							<span class="race">Presque Isle Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Indiana Downs</div><div class='tsBody'> <strong>Location:</strong> Shelbyville, IN  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Indiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Mountaineer Casino Racetrack & Resort</div><div class='tsBody'> <strong>Location:</strong> Chester, WV  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:30 PM&nbsp;&nbsp;&nbsp;9:55 PM&nbsp;&nbsp;&nbsp;10:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Mountaineer Casino Racetrack & Resort</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Tuesday, June 23, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Parx Racing</div><div class='tsBody'> <strong>Location:</strong> Bensalem, PA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:25 PM&nbsp;&nbsp;&nbsp;12:52 PM&nbsp;&nbsp;&nbsp;1:19 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:07 PM&nbsp;&nbsp;&nbsp;3:34 PM&nbsp;&nbsp;&nbsp;4:01 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:25 PM</span>
							<span class="race">Parx Racing</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ajax Downs</div><div class='tsBody'> <strong>Location:</strong> Ajax, ON  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:55 PM&nbsp;&nbsp;&nbsp;1:22 PM&nbsp;&nbsp;&nbsp;1:49 PM&nbsp;&nbsp;&nbsp;2:16 PM&nbsp;&nbsp;&nbsp;2:43 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:37 PM&nbsp;&nbsp;&nbsp;4:04 PM&nbsp;&nbsp;&nbsp;4:31 PM&nbsp;&nbsp;&nbsp;4:58 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:55 PM</span>
							<span class="race">Ajax Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fairmount Park</div><div class='tsBody'> <strong>Location:</strong> Collinsville, IL  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:27 PM&nbsp;&nbsp;&nbsp;1:54 PM&nbsp;&nbsp;&nbsp;2:21 PM&nbsp;&nbsp;&nbsp;2:49 PM&nbsp;&nbsp;&nbsp;3:16 PM&nbsp;&nbsp;&nbsp;<br>3:44 PM&nbsp;&nbsp;&nbsp;4:12 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Fairmount Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Finger Lakes</div><div class='tsBody'> <strong>Location:</strong> Farmington, NY  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 72 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:10 PM&nbsp;&nbsp;&nbsp;1:37 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:31 PM&nbsp;&nbsp;&nbsp;2:58 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:52 PM&nbsp;&nbsp;&nbsp;4:19 PM&nbsp;&nbsp;&nbsp;4:46 PM&nbsp;&nbsp;&nbsp;5:13 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:10 PM</span>
							<span class="race">Finger Lakes</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Beulah Park</div><div class='tsBody'> <strong>Location:</strong> Grove City, OH  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 72 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:14 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:06 PM&nbsp;&nbsp;&nbsp;4:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Beulah Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fort Erie</div><div class='tsBody'> <strong>Location:</strong> Fort Erie, ON  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>4:15 PM&nbsp;&nbsp;&nbsp;4:42 PM&nbsp;&nbsp;&nbsp;5:09 PM&nbsp;&nbsp;&nbsp;5:36 PM&nbsp;&nbsp;&nbsp;6:03 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;<br>6:57 PM&nbsp;&nbsp;&nbsp;7:24 PM&nbsp;&nbsp;&nbsp;7:51 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">4:15 PM</span>
							<span class="race">Fort Erie</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Presque Isle Downs</div><div class='tsBody'> <strong>Location:</strong> Erie, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>5:25 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;<br>7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:25 PM</span>
							<span class="race">Presque Isle Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Indiana Downs</div><div class='tsBody'> <strong>Location:</strong> Shelbyville, IN  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Indiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Mountaineer Casino Racetrack & Resort</div><div class='tsBody'> <strong>Location:</strong> Chester, WV  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:30 PM&nbsp;&nbsp;&nbsp;9:55 PM&nbsp;&nbsp;&nbsp;10:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Mountaineer Casino Racetrack & Resort</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Wednesday, June 24, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Thistledown</div><div class='tsBody'> <strong>Location:</strong> North Randall, OH  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:55 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:25 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:55 PM</span>
							<span class="race">Thistledown</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Delaware Park</div><div class='tsBody'> <strong>Location:</strong> Wilmington, DE  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Delaware Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Suffolk Downs</div><div class='tsBody'> <strong>Location:</strong> East Boston, MA  <br><b>Entry Draw:</b>  48 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:44 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:11 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Suffolk Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Beulah Park</div><div class='tsBody'> <strong>Location:</strong> Grove City, OH  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:14 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:06 PM&nbsp;&nbsp;&nbsp;4:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Beulah Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sacramento</div><div class='tsBody'> <strong>Location:</strong> Sacramento, CA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:45 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:53 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;3:57 PM&nbsp;&nbsp;&nbsp;4:29 PM&nbsp;&nbsp;&nbsp;<br>5:01 PM&nbsp;&nbsp;&nbsp;5:32 PM&nbsp;&nbsp;&nbsp;6:02 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:45 PM</span>
							<span class="race">Sacramento</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Del Mar</div><div class='tsBody'> <strong>Location:</strong> Del Mar, CA  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:05 PM&nbsp;&nbsp;&nbsp;2:37 PM&nbsp;&nbsp;&nbsp;3:09 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;<br>5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;6:10 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:05 PM</span>
							<span class="race">Del Mar</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Presque Isle Downs</div><div class='tsBody'> <strong>Location:</strong> Erie, PA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>5:25 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;<br>7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:25 PM</span>
							<span class="race">Presque Isle Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Les Bois Park</div><div class='tsBody'> <strong>Location:</strong> Boise, ID  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>5:30 PM&nbsp;&nbsp;&nbsp;5:53 PM&nbsp;&nbsp;&nbsp;6:16 PM&nbsp;&nbsp;&nbsp;6:39 PM&nbsp;&nbsp;&nbsp;7:03 PM&nbsp;&nbsp;&nbsp;7:27 PM&nbsp;&nbsp;&nbsp;<br>7:52 PM&nbsp;&nbsp;&nbsp;8:17 PM&nbsp;&nbsp;&nbsp;8:42 PM&nbsp;&nbsp;&nbsp;9:07 PM&nbsp;&nbsp;&nbsp;9:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:30 PM</span>
							<span class="race">Les Bois Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Evangeline Downs</div><div class='tsBody'> <strong>Location:</strong> Opelousas, LA  <br><b>Entry Draw:</b> 168 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>5:40 PM&nbsp;&nbsp;&nbsp;6:05 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;6:55 PM&nbsp;&nbsp;&nbsp;7:20 PM&nbsp;&nbsp;&nbsp;7:45 PM&nbsp;&nbsp;&nbsp;<br>8:10 PM&nbsp;&nbsp;&nbsp;8:35 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;9:25 PM&nbsp;&nbsp;&nbsp;9:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:40 PM</span>
							<span class="race">Evangeline Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fair Meadows</div><div class='tsBody'> <strong>Location:</strong> Tulsa, OK  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;10:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Fair Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Indiana Downs</div><div class='tsBody'> <strong>Location:</strong> Shelbyville, IN  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Indiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Northlands Park</div><div class='tsBody'> <strong>Location:</strong> Edmonton, AB  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:28 PM&nbsp;&nbsp;&nbsp;6:56 PM&nbsp;&nbsp;&nbsp;7:24 PM&nbsp;&nbsp;&nbsp;7:52 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;<br>8:48 PM&nbsp;&nbsp;&nbsp;9:16 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Northlands Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Penn National</div><div class='tsBody'> <strong>Location:</strong> Grantville, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:27 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;7:22 PM&nbsp;&nbsp;&nbsp;7:49 PM&nbsp;&nbsp;&nbsp;8:16 PM&nbsp;&nbsp;&nbsp;<br>8:43 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Penn National</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Woodbine</div><div class='tsBody'> <strong>Location:</strong> Rexdale, ON  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:45 PM&nbsp;&nbsp;&nbsp;7:16 PM&nbsp;&nbsp;&nbsp;7:47 PM&nbsp;&nbsp;&nbsp;8:19 PM&nbsp;&nbsp;&nbsp;8:50 PM&nbsp;&nbsp;&nbsp;9:21 PM&nbsp;&nbsp;&nbsp;<br>9:51 PM&nbsp;&nbsp;&nbsp;10:21 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:45 PM</span>
							<span class="race">Woodbine</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Assiniboia Downs</div><div class='tsBody'> <strong>Location:</strong> Winnipeg, MB  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>7:30 PM&nbsp;&nbsp;&nbsp;7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;8:45 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:35 PM&nbsp;&nbsp;&nbsp;<br>10:00 PM&nbsp;&nbsp;&nbsp;10:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:30 PM</span>
							<span class="race">Assiniboia Downs</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Thursday, June 25, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arlington Park</div><div class='tsBody'> <strong>Location:</strong> Arlington, IL  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:28 PM&nbsp;&nbsp;&nbsp;1:57 PM&nbsp;&nbsp;&nbsp;2:26 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;4:54 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arlington Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Finger Lakes</div><div class='tsBody'> <strong>Location:</strong> Farmington, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:10 PM&nbsp;&nbsp;&nbsp;1:37 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:31 PM&nbsp;&nbsp;&nbsp;2:58 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:52 PM&nbsp;&nbsp;&nbsp;4:19 PM&nbsp;&nbsp;&nbsp;4:46 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:10 PM</span>
							<span class="race">Finger Lakes</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Delaware Park</div><div class='tsBody'> <strong>Location:</strong> Wilmington, DE  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Delaware Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Louisiana Downs</div><div class='tsBody'> <strong>Location:</strong> Bossier City, LA  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;3:38 PM&nbsp;&nbsp;&nbsp;<br>4:08 PM&nbsp;&nbsp;&nbsp;4:35 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:25 PM</span>
							<span class="race">Louisiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sacramento</div><div class='tsBody'> <strong>Location:</strong> Sacramento, CA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;4:15 PM&nbsp;&nbsp;&nbsp;<br>4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:45 PM</span>
							<span class="race">Sacramento</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Del Mar</div><div class='tsBody'> <strong>Location:</strong> Del Mar, CA  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:05 PM&nbsp;&nbsp;&nbsp;2:37 PM&nbsp;&nbsp;&nbsp;3:09 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;<br>5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:05 PM</span>
							<span class="race">Del Mar</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Presque Isle Downs</div><div class='tsBody'> <strong>Location:</strong> Erie, PA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>5:25 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;<br>7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:25 PM</span>
							<span class="race">Presque Isle Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Evangeline Downs</div><div class='tsBody'> <strong>Location:</strong> Opelousas, LA  <br><b>Entry Draw:</b> 168 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>5:40 PM&nbsp;&nbsp;&nbsp;6:05 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;6:55 PM&nbsp;&nbsp;&nbsp;7:20 PM&nbsp;&nbsp;&nbsp;7:45 PM&nbsp;&nbsp;&nbsp;<br>8:10 PM&nbsp;&nbsp;&nbsp;8:35 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;9:25 PM&nbsp;&nbsp;&nbsp;9:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:40 PM</span>
							<span class="race">Evangeline Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fair Meadows</div><div class='tsBody'> <strong>Location:</strong> Tulsa, OK  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;10:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Fair Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Penn National</div><div class='tsBody'> <strong>Location:</strong> Grantville, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:27 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;7:22 PM&nbsp;&nbsp;&nbsp;7:49 PM&nbsp;&nbsp;&nbsp;8:16 PM&nbsp;&nbsp;&nbsp;<br>8:43 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Penn National</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Canterbury Park</div><div class='tsBody'> <strong>Location:</strong> Shakopee, MN  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:30 PM&nbsp;&nbsp;&nbsp;6:58 PM&nbsp;&nbsp;&nbsp;7:33 PM&nbsp;&nbsp;&nbsp;8:02 PM&nbsp;&nbsp;&nbsp;8:31 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;<br>9:29 PM&nbsp;&nbsp;&nbsp;9:58 PM&nbsp;&nbsp;&nbsp;10:27 PM&nbsp;&nbsp;&nbsp;10:56 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:30 PM</span>
							<span class="race">Canterbury Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Prairie Meadows</div><div class='tsBody'> <strong>Location:</strong> Altoona, IA  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:30 PM&nbsp;&nbsp;&nbsp;6:56 PM&nbsp;&nbsp;&nbsp;7:23 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:17 PM&nbsp;&nbsp;&nbsp;8:44 PM&nbsp;&nbsp;&nbsp;<br>9:11 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;10:03 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:30 PM</span>
							<span class="race">Prairie Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hollywood Casino At Charles Town Races</div><div class='tsBody'> <strong>Location:</strong> Charles Town, WV  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:15 PM&nbsp;&nbsp;&nbsp;7:43 PM&nbsp;&nbsp;&nbsp;8:10 PM&nbsp;&nbsp;&nbsp;8:37 PM&nbsp;&nbsp;&nbsp;9:04 PM&nbsp;&nbsp;&nbsp;9:31 PM&nbsp;&nbsp;&nbsp;<br>9:58 PM&nbsp;&nbsp;&nbsp;10:25 PM&nbsp;&nbsp;&nbsp;10:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:15 PM</span>
							<span class="race">Hollywood Casino At Charles Town Races</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Friday, June 26, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Finger Lakes</div><div class='tsBody'> <strong>Location:</strong> Farmington, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:35 PM&nbsp;&nbsp;&nbsp;1:02 PM&nbsp;&nbsp;&nbsp;1:29 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:23 PM&nbsp;&nbsp;&nbsp;2:50 PM&nbsp;&nbsp;&nbsp;<br>3:17 PM&nbsp;&nbsp;&nbsp;3:44 PM&nbsp;&nbsp;&nbsp;4:11 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:35 PM</span>
							<span class="race">Finger Lakes</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Calder Race Course</div><div class='tsBody'> <strong>Location:</strong> Miami, FL  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:17 PM&nbsp;&nbsp;&nbsp;1:44 PM&nbsp;&nbsp;&nbsp;2:11 PM&nbsp;&nbsp;&nbsp;2:38 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:32 PM&nbsp;&nbsp;&nbsp;3:59 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Calder Race Course</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ellis Park</div><div class='tsBody'> <strong>Location:</strong> Henderson, KY  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Ellis Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Monmouth Park</div><div class='tsBody'> <strong>Location:</strong> Oceanport, NJ  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:20 PM&nbsp;&nbsp;&nbsp;1:51 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:53 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:57 PM&nbsp;&nbsp;&nbsp;4:29 PM&nbsp;&nbsp;&nbsp;5:01 PM&nbsp;&nbsp;&nbsp;5:31 PM&nbsp;&nbsp;&nbsp;6:01 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Monmouth Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Woodbine</div><div class='tsBody'> <strong>Location:</strong> Rexdale, ON  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:38 PM&nbsp;&nbsp;&nbsp;4:06 PM&nbsp;&nbsp;&nbsp;4:34 PM&nbsp;&nbsp;&nbsp;5:02 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Woodbine</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Thistledown</div><div class='tsBody'> <strong>Location:</strong> North Randall, OH  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:55 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:25 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:55 PM</span>
							<span class="race">Thistledown</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arapahoe Park</div><div class='tsBody'> <strong>Location:</strong> Aurora, CO  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:23 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:09 PM&nbsp;&nbsp;&nbsp;2:32 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;<br>3:18 PM&nbsp;&nbsp;&nbsp;3:41 PM&nbsp;&nbsp;&nbsp;4:04 PM&nbsp;&nbsp;&nbsp;4:27 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arapahoe Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ruidoso Downs</div><div class='tsBody'> <strong>Location:</strong> Ruidoso Downs, NM  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:26 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:18 PM&nbsp;&nbsp;&nbsp;2:44 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:36 PM&nbsp;&nbsp;&nbsp;4:02 PM&nbsp;&nbsp;&nbsp;4:28 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Ruidoso Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Saratoga</div><div class='tsBody'> <strong>Location:</strong> Saratoga Springs, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:32 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:36 PM&nbsp;&nbsp;&nbsp;3:08 PM&nbsp;&nbsp;&nbsp;3:41 PM&nbsp;&nbsp;&nbsp;<br>4:14 PM&nbsp;&nbsp;&nbsp;4:47 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;5:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Saratoga</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Louisiana Downs</div><div class='tsBody'> <strong>Location:</strong> Bossier City, LA  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:25 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:25 PM</span>
							<span class="race">Louisiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arlington Park</div><div class='tsBody'> <strong>Location:</strong> Arlington, IL  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>3:00 PM&nbsp;&nbsp;&nbsp;3:28 PM&nbsp;&nbsp;&nbsp;3:56 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;4:54 PM&nbsp;&nbsp;&nbsp;5:24 PM&nbsp;&nbsp;&nbsp;<br>5:54 PM&nbsp;&nbsp;&nbsp;6:24 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">3:00 PM</span>
							<span class="race">Arlington Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sacramento</div><div class='tsBody'> <strong>Location:</strong> Sacramento, CA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>3:45 PM&nbsp;&nbsp;&nbsp;4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;<br>6:45 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:45 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:45 PM&nbsp;&nbsp;&nbsp;9:15 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">3:45 PM</span>
							<span class="race">Sacramento</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Del Mar</div><div class='tsBody'> <strong>Location:</strong> Del Mar, CA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>4:00 PM&nbsp;&nbsp;&nbsp;4:30 PM&nbsp;&nbsp;&nbsp;5:00 PM&nbsp;&nbsp;&nbsp;5:30 PM&nbsp;&nbsp;&nbsp;6:00 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;<br>7:00 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">4:00 PM</span>
							<span class="race">Del Mar</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Evangeline Downs</div><div class='tsBody'> <strong>Location:</strong> Opelousas, LA  <br><b>Entry Draw:</b> 168 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>5:40 PM&nbsp;&nbsp;&nbsp;6:05 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;6:55 PM&nbsp;&nbsp;&nbsp;7:20 PM&nbsp;&nbsp;&nbsp;7:45 PM&nbsp;&nbsp;&nbsp;<br>8:10 PM&nbsp;&nbsp;&nbsp;8:35 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;9:25 PM&nbsp;&nbsp;&nbsp;9:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:40 PM</span>
							<span class="race">Evangeline Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fair Meadows</div><div class='tsBody'> <strong>Location:</strong> Tulsa, OK  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;10:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Fair Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Grande Prairie</div><div class='tsBody'> <strong>Location:</strong> Grande Prairie, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:20 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:00 PM&nbsp;&nbsp;&nbsp;7:20 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;<br>8:00 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Grande Prairie</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Indiana Downs</div><div class='tsBody'> <strong>Location:</strong> Shelbyville, IN  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Indiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Northlands Park</div><div class='tsBody'> <strong>Location:</strong> Edmonton, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:27 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;7:21 PM&nbsp;&nbsp;&nbsp;7:48 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;<br>8:42 PM&nbsp;&nbsp;&nbsp;9:09 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Northlands Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Penn National</div><div class='tsBody'> <strong>Location:</strong> Grantville, PA  <br><b>Entry Draw:</b>  48 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:27 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;7:22 PM&nbsp;&nbsp;&nbsp;7:49 PM&nbsp;&nbsp;&nbsp;8:16 PM&nbsp;&nbsp;&nbsp;<br>8:43 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Penn National</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Canterbury Park</div><div class='tsBody'> <strong>Location:</strong> Shakopee, MN  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:30 PM&nbsp;&nbsp;&nbsp;6:55 PM&nbsp;&nbsp;&nbsp;7:35 PM&nbsp;&nbsp;&nbsp;8:03 PM&nbsp;&nbsp;&nbsp;8:31 PM&nbsp;&nbsp;&nbsp;8:59 PM&nbsp;&nbsp;&nbsp;<br>9:29 PM&nbsp;&nbsp;&nbsp;9:58 PM&nbsp;&nbsp;&nbsp;10:27 PM&nbsp;&nbsp;&nbsp;10:56 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:30 PM</span>
							<span class="race">Canterbury Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Prairie Meadows</div><div class='tsBody'> <strong>Location:</strong> Altoona, IA  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:30 PM&nbsp;&nbsp;&nbsp;6:56 PM&nbsp;&nbsp;&nbsp;7:23 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:17 PM&nbsp;&nbsp;&nbsp;8:44 PM&nbsp;&nbsp;&nbsp;<br>9:11 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;10:03 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:30 PM</span>
							<span class="race">Prairie Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Emerald Downs</div><div class='tsBody'> <strong>Location:</strong> Auburn, WA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>6:45 PM&nbsp;&nbsp;&nbsp;7:13 PM&nbsp;&nbsp;&nbsp;7:41 PM&nbsp;&nbsp;&nbsp;8:09 PM&nbsp;&nbsp;&nbsp;8:37 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:45 PM</span>
							<span class="race">Emerald Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Retama Park</div><div class='tsBody'> <strong>Location:</strong> San Antonio, TX  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:45 PM&nbsp;&nbsp;&nbsp;7:12 PM&nbsp;&nbsp;&nbsp;7:39 PM&nbsp;&nbsp;&nbsp;8:06 PM&nbsp;&nbsp;&nbsp;8:33 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;<br>9:27 PM&nbsp;&nbsp;&nbsp;9:54 PM&nbsp;&nbsp;&nbsp;10:21 PM&nbsp;&nbsp;&nbsp;10:48 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:45 PM</span>
							<span class="race">Retama Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hastings Racecourse</div><div class='tsBody'> <strong>Location:</strong> Vancouver, BC  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:28 PM&nbsp;&nbsp;&nbsp;7:56 PM&nbsp;&nbsp;&nbsp;8:24 PM&nbsp;&nbsp;&nbsp;8:52 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;<br>9:48 PM&nbsp;&nbsp;&nbsp;10:16 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Hastings Racecourse</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Los Alamitos</div><div class='tsBody'> <strong>Location:</strong> Los Alamitos, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:24 PM&nbsp;&nbsp;&nbsp;7:48 PM&nbsp;&nbsp;&nbsp;8:12 PM&nbsp;&nbsp;&nbsp;8:36 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;<br>9:24 PM&nbsp;&nbsp;&nbsp;9:48 PM&nbsp;&nbsp;&nbsp;10:12 PM&nbsp;&nbsp;&nbsp;10:36 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Los Alamitos</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Mountaineer Casino Racetrack & Resort</div><div class='tsBody'> <strong>Location:</strong> Chester, WV  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 72 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:30 PM&nbsp;&nbsp;&nbsp;9:55 PM&nbsp;&nbsp;&nbsp;10:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Mountaineer Casino Racetrack & Resort</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Marquis Downs</div><div class='tsBody'> <strong>Location:</strong> Saskatoon, SK  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;8:45 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;<br>9:35 PM&nbsp;&nbsp;&nbsp;10:00 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:05 PM</span>
							<span class="race">Marquis Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hollywood Casino At Charles Town Races</div><div class='tsBody'> <strong>Location:</strong> Charles Town, WV  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:15 PM&nbsp;&nbsp;&nbsp;7:43 PM&nbsp;&nbsp;&nbsp;8:10 PM&nbsp;&nbsp;&nbsp;8:37 PM&nbsp;&nbsp;&nbsp;9:04 PM&nbsp;&nbsp;&nbsp;9:31 PM&nbsp;&nbsp;&nbsp;<br>9:58 PM&nbsp;&nbsp;&nbsp;10:25 PM&nbsp;&nbsp;&nbsp;10:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:15 PM</span>
							<span class="race">Hollywood Casino At Charles Town Races</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Assiniboia Downs</div><div class='tsBody'> <strong>Location:</strong> Winnipeg, MB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>7:30 PM&nbsp;&nbsp;&nbsp;7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;8:45 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:35 PM&nbsp;&nbsp;&nbsp;<br>10:00 PM&nbsp;&nbsp;&nbsp;10:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:30 PM</span>
							<span class="race">Assiniboia Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fairmount Park</div><div class='tsBody'> <strong>Location:</strong> Collinsville, IL  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>7:30 PM&nbsp;&nbsp;&nbsp;7:57 PM&nbsp;&nbsp;&nbsp;8:24 PM&nbsp;&nbsp;&nbsp;8:51 PM&nbsp;&nbsp;&nbsp;9:19 PM&nbsp;&nbsp;&nbsp;9:46 PM&nbsp;&nbsp;&nbsp;<br>10:14 PM&nbsp;&nbsp;&nbsp;10:42 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:30 PM</span>
							<span class="race">Fairmount Park</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Saturday, June 27, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Parx Racing</div><div class='tsBody'> <strong>Location:</strong> Bensalem, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:25 PM&nbsp;&nbsp;&nbsp;12:52 PM&nbsp;&nbsp;&nbsp;1:19 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:07 PM&nbsp;&nbsp;&nbsp;3:34 PM&nbsp;&nbsp;&nbsp;4:01 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:25 PM</span>
							<span class="race">Parx Racing</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Calder Race Course</div><div class='tsBody'> <strong>Location:</strong> Miami, FL  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:17 PM&nbsp;&nbsp;&nbsp;1:44 PM&nbsp;&nbsp;&nbsp;2:11 PM&nbsp;&nbsp;&nbsp;2:38 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:32 PM&nbsp;&nbsp;&nbsp;3:59 PM&nbsp;&nbsp;&nbsp;4:26 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Calder Race Course</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ellis Park</div><div class='tsBody'> <strong>Location:</strong> Henderson, KY  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:24 PM&nbsp;&nbsp;&nbsp;<br>3:54 PM&nbsp;&nbsp;&nbsp;4:24 PM&nbsp;&nbsp;&nbsp;4:54 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Ellis Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Monmouth Park</div><div class='tsBody'> <strong>Location:</strong> Oceanport, NJ  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:20 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:50 PM&nbsp;&nbsp;&nbsp;3:21 PM&nbsp;&nbsp;&nbsp;<br>3:52 PM&nbsp;&nbsp;&nbsp;4:23 PM&nbsp;&nbsp;&nbsp;4:55 PM&nbsp;&nbsp;&nbsp;5:27 PM&nbsp;&nbsp;&nbsp;5:59 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Monmouth Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Woodbine</div><div class='tsBody'> <strong>Location:</strong> Rexdale, ON  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:47 PM&nbsp;&nbsp;&nbsp;2:17 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:19 PM&nbsp;&nbsp;&nbsp;<br>3:50 PM&nbsp;&nbsp;&nbsp;4:22 PM&nbsp;&nbsp;&nbsp;4:54 PM&nbsp;&nbsp;&nbsp;5:26 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Woodbine</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Thistledown</div><div class='tsBody'> <strong>Location:</strong> North Randall, OH  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:55 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:25 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:55 PM</span>
							<span class="race">Thistledown</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arapahoe Park</div><div class='tsBody'> <strong>Location:</strong> Aurora, CO  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:30 PM&nbsp;&nbsp;&nbsp;3:55 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arapahoe Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arlington Park</div><div class='tsBody'> <strong>Location:</strong> Arlington, IL  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:28 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:25 PM&nbsp;&nbsp;&nbsp;2:54 PM&nbsp;&nbsp;&nbsp;3:24 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:26 PM&nbsp;&nbsp;&nbsp;4:56 PM&nbsp;&nbsp;&nbsp;5:25 PM&nbsp;&nbsp;&nbsp;5:54 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arlington Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Gillespie County Fairground</div><div class='tsBody'> <strong>Location:</strong> Fredericksburg, TX  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:30 PM&nbsp;&nbsp;&nbsp;3:55 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:10 PM&nbsp;&nbsp;&nbsp;5:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Gillespie County Fairground</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Northlands Park</div><div class='tsBody'> <strong>Location:</strong> Edmonton, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:27 PM&nbsp;&nbsp;&nbsp;1:54 PM&nbsp;&nbsp;&nbsp;2:21 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;<br>3:42 PM&nbsp;&nbsp;&nbsp;4:09 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Northlands Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ruidoso Downs</div><div class='tsBody'> <strong>Location:</strong> Ruidoso Downs, NM  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:26 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:18 PM&nbsp;&nbsp;&nbsp;2:44 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:36 PM&nbsp;&nbsp;&nbsp;4:02 PM&nbsp;&nbsp;&nbsp;4:28 PM&nbsp;&nbsp;&nbsp;4:54 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;5:46 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Ruidoso Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sandy Downs</div><div class='tsBody'> <strong>Location:</strong> Idaho Falls, ID  <br><b>Entry Draw:</b>  48 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:20 PM&nbsp;&nbsp;&nbsp;1:40 PM&nbsp;&nbsp;&nbsp;2:00 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:00 PM&nbsp;&nbsp;&nbsp;3:20 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Sandy Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Saratoga</div><div class='tsBody'> <strong>Location:</strong> Saratoga Springs, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:30 PM&nbsp;&nbsp;&nbsp;2:00 PM&nbsp;&nbsp;&nbsp;2:31 PM&nbsp;&nbsp;&nbsp;3:03 PM&nbsp;&nbsp;&nbsp;3:35 PM&nbsp;&nbsp;&nbsp;<br>4:07 PM&nbsp;&nbsp;&nbsp;4:39 PM&nbsp;&nbsp;&nbsp;5:12 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:18 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Saratoga</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Finger Lakes</div><div class='tsBody'> <strong>Location:</strong> Farmington, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:10 PM&nbsp;&nbsp;&nbsp;1:37 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:31 PM&nbsp;&nbsp;&nbsp;2:58 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:52 PM&nbsp;&nbsp;&nbsp;4:19 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:10 PM</span>
							<span class="race">Finger Lakes</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Delaware Park</div><div class='tsBody'> <strong>Location:</strong> Wilmington, DE  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Delaware Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Gulfstream Park</div><div class='tsBody'> <strong>Location:</strong> Hallandale, FL  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Gulfstream Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Suffolk Downs</div><div class='tsBody'> <strong>Location:</strong> East Boston, MA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:40 PM&nbsp;&nbsp;&nbsp;2:06 PM&nbsp;&nbsp;&nbsp;2:32 PM&nbsp;&nbsp;&nbsp;2:58 PM&nbsp;&nbsp;&nbsp;3:24 PM&nbsp;&nbsp;&nbsp;<br>3:50 PM&nbsp;&nbsp;&nbsp;4:42 PM&nbsp;&nbsp;&nbsp;5:08 PM&nbsp;&nbsp;&nbsp;5:34 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Suffolk Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Louisiana Downs</div><div class='tsBody'> <strong>Location:</strong> Bossier City, LA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:25 PM&nbsp;&nbsp;&nbsp;1:53 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:53 PM&nbsp;&nbsp;&nbsp;3:24 PM&nbsp;&nbsp;&nbsp;3:55 PM&nbsp;&nbsp;&nbsp;<br>4:25 PM&nbsp;&nbsp;&nbsp;5:00 PM&nbsp;&nbsp;&nbsp;5:27 PM&nbsp;&nbsp;&nbsp;5:54 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:25 PM</span>
							<span class="race">Louisiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Canterbury Park</div><div class='tsBody'> <strong>Location:</strong> Shakopee, MN  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:58 PM&nbsp;&nbsp;&nbsp;2:26 PM&nbsp;&nbsp;&nbsp;2:54 PM&nbsp;&nbsp;&nbsp;3:22 PM&nbsp;&nbsp;&nbsp;3:50 PM&nbsp;&nbsp;&nbsp;<br>4:18 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:12 PM&nbsp;&nbsp;&nbsp;5:35 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Canterbury Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Great Falls</div><div class='tsBody'> <strong>Location:</strong> , MT  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;3:35 PM&nbsp;&nbsp;&nbsp;<br>4:00 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;4:50 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Great Falls</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sacramento</div><div class='tsBody'> <strong>Location:</strong> Sacramento, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;4:15 PM&nbsp;&nbsp;&nbsp;<br>4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:45 PM</span>
							<span class="race">Sacramento</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hastings Racecourse</div><div class='tsBody'> <strong>Location:</strong> Vancouver, BC  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:50 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:50 PM&nbsp;&nbsp;&nbsp;3:20 PM&nbsp;&nbsp;&nbsp;3:50 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;<br>4:50 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:50 PM</span>
							<span class="race">Hastings Racecourse</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Emerald Downs</div><div class='tsBody'> <strong>Location:</strong> Auburn, WA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:00 PM&nbsp;&nbsp;&nbsp;2:33 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;3:36 PM&nbsp;&nbsp;&nbsp;4:07 PM&nbsp;&nbsp;&nbsp;4:38 PM&nbsp;&nbsp;&nbsp;<br>5:09 PM&nbsp;&nbsp;&nbsp;5:39 PM&nbsp;&nbsp;&nbsp;6:09 PM&nbsp;&nbsp;&nbsp;6:39 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:00 PM</span>
							<span class="race">Emerald Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Del Mar</div><div class='tsBody'> <strong>Location:</strong> Del Mar, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:05 PM&nbsp;&nbsp;&nbsp;2:37 PM&nbsp;&nbsp;&nbsp;3:09 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;<br>5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;6:10 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:05 PM</span>
							<span class="race">Del Mar</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Les Bois Park</div><div class='tsBody'> <strong>Location:</strong> Boise, ID  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>5:30 PM&nbsp;&nbsp;&nbsp;5:53 PM&nbsp;&nbsp;&nbsp;6:16 PM&nbsp;&nbsp;&nbsp;6:39 PM&nbsp;&nbsp;&nbsp;7:03 PM&nbsp;&nbsp;&nbsp;7:27 PM&nbsp;&nbsp;&nbsp;<br>7:52 PM&nbsp;&nbsp;&nbsp;8:17 PM&nbsp;&nbsp;&nbsp;8:42 PM&nbsp;&nbsp;&nbsp;9:07 PM&nbsp;&nbsp;&nbsp;9:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:30 PM</span>
							<span class="race">Les Bois Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Evangeline Downs</div><div class='tsBody'> <strong>Location:</strong> Opelousas, LA  <br><b>Entry Draw:</b> 168 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>5:40 PM&nbsp;&nbsp;&nbsp;6:05 PM&nbsp;&nbsp;&nbsp;6:30 PM&nbsp;&nbsp;&nbsp;6:55 PM&nbsp;&nbsp;&nbsp;7:20 PM&nbsp;&nbsp;&nbsp;7:45 PM&nbsp;&nbsp;&nbsp;<br>8:10 PM&nbsp;&nbsp;&nbsp;8:35 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;9:25 PM&nbsp;&nbsp;&nbsp;9:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:40 PM</span>
							<span class="race">Evangeline Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fair Meadows</div><div class='tsBody'> <strong>Location:</strong> Tulsa, OK  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;10:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Fair Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Grande Prairie</div><div class='tsBody'> <strong>Location:</strong> Grande Prairie, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Grande Prairie</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Indiana Downs</div><div class='tsBody'> <strong>Location:</strong> Shelbyville, IN  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:25 PM&nbsp;&nbsp;&nbsp;6:50 PM&nbsp;&nbsp;&nbsp;7:15 PM&nbsp;&nbsp;&nbsp;7:40 PM&nbsp;&nbsp;&nbsp;8:05 PM&nbsp;&nbsp;&nbsp;<br>8:30 PM&nbsp;&nbsp;&nbsp;8:55 PM&nbsp;&nbsp;&nbsp;9:20 PM&nbsp;&nbsp;&nbsp;9:45 PM&nbsp;&nbsp;&nbsp;10:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Indiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Los Alamitos</div><div class='tsBody'> <strong>Location:</strong> Los Alamitos, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:24 PM&nbsp;&nbsp;&nbsp;6:48 PM&nbsp;&nbsp;&nbsp;7:12 PM&nbsp;&nbsp;&nbsp;7:36 PM&nbsp;&nbsp;&nbsp;8:00 PM&nbsp;&nbsp;&nbsp;<br>8:24 PM&nbsp;&nbsp;&nbsp;8:48 PM&nbsp;&nbsp;&nbsp;9:12 PM&nbsp;&nbsp;&nbsp;9:36 PM&nbsp;&nbsp;&nbsp;10:00 PM&nbsp;&nbsp;&nbsp;10:24 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Los Alamitos</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Penn National</div><div class='tsBody'> <strong>Location:</strong> Grantville, PA  <br><b>Entry Draw:</b>  48 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>6:00 PM&nbsp;&nbsp;&nbsp;6:27 PM&nbsp;&nbsp;&nbsp;6:54 PM&nbsp;&nbsp;&nbsp;7:22 PM&nbsp;&nbsp;&nbsp;7:49 PM&nbsp;&nbsp;&nbsp;8:16 PM&nbsp;&nbsp;&nbsp;<br>8:43 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:00 PM</span>
							<span class="race">Penn National</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Prairie Meadows</div><div class='tsBody'> <strong>Location:</strong> Altoona, IA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:30 PM&nbsp;&nbsp;&nbsp;6:56 PM&nbsp;&nbsp;&nbsp;7:23 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:17 PM&nbsp;&nbsp;&nbsp;8:44 PM&nbsp;&nbsp;&nbsp;<br>9:11 PM&nbsp;&nbsp;&nbsp;9:37 PM&nbsp;&nbsp;&nbsp;10:03 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:30 PM</span>
							<span class="race">Prairie Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Retama Park</div><div class='tsBody'> <strong>Location:</strong> San Antonio, TX  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>6:45 PM&nbsp;&nbsp;&nbsp;7:12 PM&nbsp;&nbsp;&nbsp;7:39 PM&nbsp;&nbsp;&nbsp;8:06 PM&nbsp;&nbsp;&nbsp;8:33 PM&nbsp;&nbsp;&nbsp;9:00 PM&nbsp;&nbsp;&nbsp;<br>9:27 PM&nbsp;&nbsp;&nbsp;9:54 PM&nbsp;&nbsp;&nbsp;10:21 PM&nbsp;&nbsp;&nbsp;10:48 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">6:45 PM</span>
							<span class="race">Retama Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Mountaineer Casino Racetrack & Resort</div><div class='tsBody'> <strong>Location:</strong> Chester, WV  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:30 PM&nbsp;&nbsp;&nbsp;9:55 PM&nbsp;&nbsp;&nbsp;10:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Mountaineer Casino Racetrack & Resort</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Marquis Downs</div><div class='tsBody'> <strong>Location:</strong> Saskatoon, SK  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;8:45 PM&nbsp;&nbsp;&nbsp;9:10 PM&nbsp;&nbsp;&nbsp;<br>9:35 PM&nbsp;&nbsp;&nbsp;10:00 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:05 PM</span>
							<span class="race">Marquis Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hollywood Casino At Charles Town Races</div><div class='tsBody'> <strong>Location:</strong> Charles Town, WV  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:15 PM&nbsp;&nbsp;&nbsp;7:43 PM&nbsp;&nbsp;&nbsp;8:10 PM&nbsp;&nbsp;&nbsp;8:37 PM&nbsp;&nbsp;&nbsp;9:04 PM&nbsp;&nbsp;&nbsp;9:31 PM&nbsp;&nbsp;&nbsp;<br>9:58 PM&nbsp;&nbsp;&nbsp;10:25 PM&nbsp;&nbsp;&nbsp;10:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:15 PM</span>
							<span class="race">Hollywood Casino At Charles Town Races</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Assiniboia Downs</div><div class='tsBody'> <strong>Location:</strong> Winnipeg, MB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>7:30 PM&nbsp;&nbsp;&nbsp;7:53 PM&nbsp;&nbsp;&nbsp;8:16 PM&nbsp;&nbsp;&nbsp;8:39 PM&nbsp;&nbsp;&nbsp;9:02 PM&nbsp;&nbsp;&nbsp;9:25 PM&nbsp;&nbsp;&nbsp;<br>9:48 PM&nbsp;&nbsp;&nbsp;10:11 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:30 PM</span>
							<span class="race">Assiniboia Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fairmount Park</div><div class='tsBody'> <strong>Location:</strong> Collinsville, IL  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>7:30 PM&nbsp;&nbsp;&nbsp;7:57 PM&nbsp;&nbsp;&nbsp;8:24 PM&nbsp;&nbsp;&nbsp;8:51 PM&nbsp;&nbsp;&nbsp;9:19 PM&nbsp;&nbsp;&nbsp;9:46 PM&nbsp;&nbsp;&nbsp;<br>10:14 PM&nbsp;&nbsp;&nbsp;10:42 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:30 PM</span>
							<span class="race">Fairmount Park</span>
						</div>
											</div>
				</div>
								<div class="sItem">
					<div class="sDay"><i class="fa fa-calendar"></i> Sunday, June 28, 2015</div>
					<div class="sEvents">
											<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Parx Racing</div><div class='tsBody'> <strong>Location:</strong> Bensalem, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:25 PM&nbsp;&nbsp;&nbsp;12:52 PM&nbsp;&nbsp;&nbsp;1:19 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:13 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:07 PM&nbsp;&nbsp;&nbsp;3:34 PM&nbsp;&nbsp;&nbsp;4:01 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:25 PM</span>
							<span class="race">Parx Racing</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Calder Race Course</div><div class='tsBody'> <strong>Location:</strong> Miami, FL  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:17 PM&nbsp;&nbsp;&nbsp;1:44 PM&nbsp;&nbsp;&nbsp;2:11 PM&nbsp;&nbsp;&nbsp;2:38 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:32 PM&nbsp;&nbsp;&nbsp;3:59 PM&nbsp;&nbsp;&nbsp;4:26 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Calder Race Course</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ellis Park</div><div class='tsBody'> <strong>Location:</strong> Henderson, KY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Ellis Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Monmouth Park</div><div class='tsBody'> <strong>Location:</strong> Oceanport, NJ  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:21 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:23 PM&nbsp;&nbsp;&nbsp;2:54 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:57 PM&nbsp;&nbsp;&nbsp;4:29 PM&nbsp;&nbsp;&nbsp;5:01 PM&nbsp;&nbsp;&nbsp;5:33 PM&nbsp;&nbsp;&nbsp;6:04 PM&nbsp;&nbsp;&nbsp;6:32 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Monmouth Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Woodbine</div><div class='tsBody'> <strong>Location:</strong> Rexdale, ON  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:50 PM&nbsp;&nbsp;&nbsp;1:18 PM&nbsp;&nbsp;&nbsp;1:46 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:42 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:38 PM&nbsp;&nbsp;&nbsp;4:06 PM&nbsp;&nbsp;&nbsp;4:34 PM&nbsp;&nbsp;&nbsp;5:02 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:50 PM</span>
							<span class="race">Woodbine</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Thistledown</div><div class='tsBody'> <strong>Location:</strong> North Randall, OH  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>12:55 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:25 PM&nbsp;&nbsp;&nbsp;2:55 PM&nbsp;&nbsp;&nbsp;3:25 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">12:55 PM</span>
							<span class="race">Thistledown</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arapahoe Park</div><div class='tsBody'> <strong>Location:</strong> Aurora, CO  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:30 PM&nbsp;&nbsp;&nbsp;3:55 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arapahoe Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Arlington Park</div><div class='tsBody'> <strong>Location:</strong> Arlington, IL  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:29 PM&nbsp;&nbsp;&nbsp;1:58 PM&nbsp;&nbsp;&nbsp;2:28 PM&nbsp;&nbsp;&nbsp;2:59 PM&nbsp;&nbsp;&nbsp;3:30 PM&nbsp;&nbsp;&nbsp;<br>4:01 PM&nbsp;&nbsp;&nbsp;4:32 PM&nbsp;&nbsp;&nbsp;5:03 PM&nbsp;&nbsp;&nbsp;5:32 PM&nbsp;&nbsp;&nbsp;6:01 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Arlington Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Gillespie County Fairground</div><div class='tsBody'> <strong>Location:</strong> Fredericksburg, TX  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:30 PM&nbsp;&nbsp;&nbsp;3:55 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:10 PM&nbsp;&nbsp;&nbsp;5:35 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Gillespie County Fairground</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Grande Prairie</div><div class='tsBody'> <strong>Location:</strong> Grande Prairie, AB  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:25 PM&nbsp;&nbsp;&nbsp;1:50 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;<br>3:30 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Grande Prairie</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hollywood Casino At Charles Town Races</div><div class='tsBody'> <strong>Location:</strong> Charles Town, WV  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:27 PM&nbsp;&nbsp;&nbsp;1:54 PM&nbsp;&nbsp;&nbsp;2:21 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;<br>3:42 PM&nbsp;&nbsp;&nbsp;4:09 PM&nbsp;&nbsp;&nbsp;4:36 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Hollywood Casino At Charles Town Races</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Prairie Meadows</div><div class='tsBody'> <strong>Location:</strong> Altoona, IA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:26 PM&nbsp;&nbsp;&nbsp;1:53 PM&nbsp;&nbsp;&nbsp;2:30 PM&nbsp;&nbsp;&nbsp;2:57 PM&nbsp;&nbsp;&nbsp;3:24 PM&nbsp;&nbsp;&nbsp;<br>4:01 PM&nbsp;&nbsp;&nbsp;4:27 PM&nbsp;&nbsp;&nbsp;4:53 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Prairie Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Ruidoso Downs</div><div class='tsBody'> <strong>Location:</strong> Ruidoso Downs, NM  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:26 PM&nbsp;&nbsp;&nbsp;1:52 PM&nbsp;&nbsp;&nbsp;2:18 PM&nbsp;&nbsp;&nbsp;2:44 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;<br>3:36 PM&nbsp;&nbsp;&nbsp;4:02 PM&nbsp;&nbsp;&nbsp;4:28 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Ruidoso Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sandy Downs</div><div class='tsBody'> <strong>Location:</strong> Idaho Falls, ID  <br><b>Entry Draw:</b>  48 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:20 PM&nbsp;&nbsp;&nbsp;1:40 PM&nbsp;&nbsp;&nbsp;2:00 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:40 PM&nbsp;&nbsp;&nbsp;<br>3:00 PM&nbsp;&nbsp;&nbsp;3:20 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Sandy Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Saratoga</div><div class='tsBody'> <strong>Location:</strong> Saratoga Springs, NY  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:00 PM&nbsp;&nbsp;&nbsp;1:32 PM&nbsp;&nbsp;&nbsp;2:04 PM&nbsp;&nbsp;&nbsp;2:36 PM&nbsp;&nbsp;&nbsp;3:08 PM&nbsp;&nbsp;&nbsp;3:41 PM&nbsp;&nbsp;&nbsp;<br>4:14 PM&nbsp;&nbsp;&nbsp;4:47 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;5:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:00 PM</span>
							<span class="race">Saratoga</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fort Erie</div><div class='tsBody'> <strong>Location:</strong> Fort Erie, ON  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:41 PM&nbsp;&nbsp;&nbsp;2:07 PM&nbsp;&nbsp;&nbsp;2:35 PM&nbsp;&nbsp;&nbsp;3:03 PM&nbsp;&nbsp;&nbsp;3:29 PM&nbsp;&nbsp;&nbsp;<br>3:57 PM&nbsp;&nbsp;&nbsp;4:24 PM&nbsp;&nbsp;&nbsp;4:52 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Fort Erie</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Gulfstream Park</div><div class='tsBody'> <strong>Location:</strong> Hallandale, FL  <br><b>Entry Draw:</b> 144 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:15 PM&nbsp;&nbsp;&nbsp;1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;<br>4:15 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:15 PM</span>
							<span class="race">Gulfstream Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Louisiana Downs</div><div class='tsBody'> <strong>Location:</strong> Bossier City, LA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:25 PM&nbsp;&nbsp;&nbsp;1:49 PM&nbsp;&nbsp;&nbsp;2:14 PM&nbsp;&nbsp;&nbsp;2:39 PM&nbsp;&nbsp;&nbsp;3:04 PM&nbsp;&nbsp;&nbsp;3:29 PM&nbsp;&nbsp;&nbsp;<br>3:55 PM&nbsp;&nbsp;&nbsp;4:21 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:25 PM</span>
							<span class="race">Louisiana Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Beulah Park</div><div class='tsBody'> <strong>Location:</strong> Grove City, OH  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch 96 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:56 PM&nbsp;&nbsp;&nbsp;2:22 PM&nbsp;&nbsp;&nbsp;2:48 PM&nbsp;&nbsp;&nbsp;3:14 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;<br>4:06 PM&nbsp;&nbsp;&nbsp;4:32 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Beulah Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Canterbury Park</div><div class='tsBody'> <strong>Location:</strong> Shakopee, MN  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:58 PM&nbsp;&nbsp;&nbsp;2:26 PM&nbsp;&nbsp;&nbsp;2:54 PM&nbsp;&nbsp;&nbsp;3:22 PM&nbsp;&nbsp;&nbsp;3:50 PM&nbsp;&nbsp;&nbsp;<br>4:18 PM&nbsp;&nbsp;&nbsp;4:45 PM&nbsp;&nbsp;&nbsp;5:12 PM&nbsp;&nbsp;&nbsp;5:35 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Canterbury Park</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Great Falls</div><div class='tsBody'> <strong>Location:</strong> , MT  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Mountain Daylight Time <br><b>Race Time:</b><br>1:30 PM&nbsp;&nbsp;&nbsp;1:55 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:10 PM&nbsp;&nbsp;&nbsp;3:35 PM&nbsp;&nbsp;&nbsp;<br>4:00 PM&nbsp;&nbsp;&nbsp;4:25 PM&nbsp;&nbsp;&nbsp;4:50 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:30 PM</span>
							<span class="race">Great Falls</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Portland Meadows</div><div class='tsBody'> <strong>Location:</strong> Portland, OR  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:45 PM&nbsp;&nbsp;&nbsp;2:11 PM&nbsp;&nbsp;&nbsp;2:41 PM&nbsp;&nbsp;&nbsp;3:11 PM&nbsp;&nbsp;&nbsp;3:41 PM&nbsp;&nbsp;&nbsp;4:11 PM&nbsp;&nbsp;&nbsp;<br>4:41 PM&nbsp;&nbsp;&nbsp;5:11 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:45 PM</span>
							<span class="race">Portland Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Sacramento</div><div class='tsBody'> <strong>Location:</strong> Sacramento, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:45 PM&nbsp;&nbsp;&nbsp;2:15 PM&nbsp;&nbsp;&nbsp;2:45 PM&nbsp;&nbsp;&nbsp;3:15 PM&nbsp;&nbsp;&nbsp;3:45 PM&nbsp;&nbsp;&nbsp;4:15 PM&nbsp;&nbsp;&nbsp;<br>4:45 PM&nbsp;&nbsp;&nbsp;5:15 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:45 PM</span>
							<span class="race">Sacramento</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Hastings Racecourse</div><div class='tsBody'> <strong>Location:</strong> Vancouver, BC  <br><b>Entry Draw:</b>  96 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>1:50 PM&nbsp;&nbsp;&nbsp;2:20 PM&nbsp;&nbsp;&nbsp;2:50 PM&nbsp;&nbsp;&nbsp;3:20 PM&nbsp;&nbsp;&nbsp;3:50 PM&nbsp;&nbsp;&nbsp;4:20 PM&nbsp;&nbsp;&nbsp;<br>4:50 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">1:50 PM</span>
							<span class="race">Hastings Racecourse</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Emerald Downs</div><div class='tsBody'> <strong>Location:</strong> Auburn, WA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:00 PM&nbsp;&nbsp;&nbsp;2:33 PM&nbsp;&nbsp;&nbsp;3:05 PM&nbsp;&nbsp;&nbsp;3:36 PM&nbsp;&nbsp;&nbsp;4:07 PM&nbsp;&nbsp;&nbsp;4:38 PM&nbsp;&nbsp;&nbsp;<br>5:09 PM&nbsp;&nbsp;&nbsp;5:39 PM&nbsp;&nbsp;&nbsp;6:09 PM&nbsp;&nbsp;&nbsp;6:39 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:00 PM</span>
							<span class="race">Emerald Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Del Mar</div><div class='tsBody'> <strong>Location:</strong> Del Mar, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 24 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>2:05 PM&nbsp;&nbsp;&nbsp;2:37 PM&nbsp;&nbsp;&nbsp;3:09 PM&nbsp;&nbsp;&nbsp;3:40 PM&nbsp;&nbsp;&nbsp;4:10 PM&nbsp;&nbsp;&nbsp;4:40 PM&nbsp;&nbsp;&nbsp;<br>5:10 PM&nbsp;&nbsp;&nbsp;5:40 PM&nbsp;&nbsp;&nbsp;6:10 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">2:05 PM</span>
							<span class="race">Del Mar</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Fair Meadows</div><div class='tsBody'> <strong>Location:</strong> Tulsa, OK  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Central Daylight Time <br><b>Race Time:</b><br>4:05 PM&nbsp;&nbsp;&nbsp;4:30 PM&nbsp;&nbsp;&nbsp;4:55 PM&nbsp;&nbsp;&nbsp;5:20 PM&nbsp;&nbsp;&nbsp;5:45 PM&nbsp;&nbsp;&nbsp;6:10 PM&nbsp;&nbsp;&nbsp;<br>6:35 PM&nbsp;&nbsp;&nbsp;7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">4:05 PM</span>
							<span class="race">Fair Meadows</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Los Alamitos</div><div class='tsBody'> <strong>Location:</strong> Los Alamitos, CA  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Pacific Daylight Time <br><b>Race Time:</b><br>5:00 PM&nbsp;&nbsp;&nbsp;5:24 PM&nbsp;&nbsp;&nbsp;5:48 PM&nbsp;&nbsp;&nbsp;6:12 PM&nbsp;&nbsp;&nbsp;6:36 PM&nbsp;&nbsp;&nbsp;7:00 PM&nbsp;&nbsp;&nbsp;<br>7:24 PM&nbsp;&nbsp;&nbsp;7:48 PM&nbsp;&nbsp;&nbsp;8:12 PM&nbsp;&nbsp;&nbsp;8:36 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:00 PM</span>
							<span class="race">Los Alamitos</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Presque Isle Downs</div><div class='tsBody'> <strong>Location:</strong> Erie, PA  <br><b>Entry Draw:</b> 120 hours <br><b>Scratch:</b> Scratch off the program <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>5:25 PM&nbsp;&nbsp;&nbsp;5:50 PM&nbsp;&nbsp;&nbsp;6:15 PM&nbsp;&nbsp;&nbsp;6:40 PM&nbsp;&nbsp;&nbsp;7:05 PM&nbsp;&nbsp;&nbsp;7:30 PM&nbsp;&nbsp;&nbsp;<br>7:55 PM&nbsp;&nbsp;&nbsp;8:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">5:25 PM</span>
							<span class="race">Presque Isle Downs</span>
						</div>
												<div class="sRace"  data-toggle="tooltip" title="<div class='tsHeader'>Mountaineer Casino Racetrack & Resort</div><div class='tsBody'> <strong>Location:</strong> Chester, WV  <br><b>Entry Draw:</b>  72 hours <br><b>Scratch:</b> Scratch 48 hours <br><b>Time Zone:</b> Eastern Daylight Time <br><b>Race Time:</b><br>7:00 PM&nbsp;&nbsp;&nbsp;7:25 PM&nbsp;&nbsp;&nbsp;7:50 PM&nbsp;&nbsp;&nbsp;8:15 PM&nbsp;&nbsp;&nbsp;8:40 PM&nbsp;&nbsp;&nbsp;9:05 PM&nbsp;&nbsp;&nbsp;<br>9:30 PM&nbsp;&nbsp;&nbsp;9:55 PM&nbsp;&nbsp;&nbsp;10:20 PM&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br></div>">
							<span class="time">7:00 PM</span>
							<span class="race">Mountaineer Casino Racetrack & Resort</span>
						</div>
											</div>
				</div>
				</div>
{literal}<script type="text/javascript">
$('.sRace').tooltip({
html: true,
placement: "auto"
});
$('.sRace').on('show.bs.tooltip', function () {
 $(this).addClass("in");
}).on('hide.bs.tooltip', function () {
 $(this).removeClass("in");
})

</script>{/literal}