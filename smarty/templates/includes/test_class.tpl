		                       
		<div id="news" class="news">
			<div class="headline"><h2>Racing News <a target="_blank" href="/news/rss.xml"><i class="fa fa-rss"></i></a></h2></div>
			<div class="content">
		      <dl class="dl-horizontal story" id="news-story-13857"><dd><p class="news-title"><a href="/news/american-pharoah-039on-course039-for-1st-race-since-triple-crown">American Pharoah &#039;on course&#039; for 1st race since Triple Crown</a></p>    <div class="teaser">           DEL MAR, Calif. &mdash; Trainer Bob Baffert says American Pharoah is &quot;right on course&quot; for his first race since winning the Triple Crown after a longer-than-expected run in Califo...</div>    <div class="news-date">July 24, 2015</div></dd></dl>
<dl class="dl-horizontal story" id="news-story-13858"><dd><p class="news-title"><a href="/news/3-jockeys-accused-of-fixing-races-at-evangeline-downs">3 jockeys accused of fixing races at Evangeline Downs</a></p>    <div class="teaser">           OPELOUSAS, La.  &mdash; Three jockeys are accused in a race-fixing scheme that involved holding back a horse picked as a favorite to win and using an agent to place bets on the race at Evan...</div>    <div class="news-date">July 24, 2015</div></dd></dl>
<dl class="dl-horizontal story" id="news-story-13859"><dd><p class="news-title"><a href="/news/long-shot-star-act-rallies-to-win-at-del-mar">Long-shot Star Act rallies to win at Del Mar</a></p>    <div class="teaser">           DEL MAR, Calif.  &mdash; Long-shot Star Act rallied to beat Circling by 1 1/2 lengths Thursday in the $72,000 feature race for fillies and mares at Del Mar.Ridden by Hall of Famer Gary Stev...</div>    <div class="news-date">July 24, 2015</div></dd></dl>
<dl class="dl-horizontal story" id="news-story-13854"><dd><p class="news-title"><a href="/news/trainer-california-chrome-to-return-to-racing-in-2016">Trainer: California Chrome to return to racing in 2016</a></p>    <div class="teaser">           DEL MAR, Calif.  &mdash; Trainer Art Sherman says California Chrome, winner of the 2014 Kentucky Derby and Horse of the Year, will return to racing next year.The 4-year-old colt is currentl...</div>    <div class="news-date">July 20, 2015</div></dd></dl>
			</div>  
			<div class="blockfooter">
				<a href="/news" title="Read More News" class="btn btn-primary" rel="nofollow">Read More News<i class="fa fa-angle-right"></i></a>
			</div>
		</div>
		