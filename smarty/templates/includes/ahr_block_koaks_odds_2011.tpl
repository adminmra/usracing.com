{literal}<style type="text/css">
#infoEntries td { padding: 7px 10px; }
</style>{/literal}
<table width="100%" height="12" cellpadding="0" cellspacing="0" id="infoEntries" class="table table-condensed table-striped table-bordered" title="KENTUCKY OAKS Odds" summary="2011 KENTUCKY OAKS Odds">
<tr>
<th>2011 KENTUCKY OAKS</th>
<th class="right">ODDS TO WIN</th>

</tr>
<tr>
<td>A Z Warrior</td>
<td class="sortOdds right">25-1</td>
</tr>
<tr>
<td>Big Tiz</td>
<td class="sortOdds right">30-1</td>
</tr>
<tr>
<td>Bouquet Booth</td>
<td class="sortOdds right">35-1</td>
</tr>
<tr>
<td>Dancinginherdreams</td>
<td class="sortOdds right">8-1</td>
</tr>
<tr>
<td>Delightful Mary</td>
<td class="sortOdds right">20-1</td>
</tr>
<tr>
<td>Harlans Ruby</td>
<td class="sortOdds right">50-1</td>
</tr>
<tr>
<td>Holy Heavens</td>
<td class="sortOdds right">50-1</td>
</tr>
<tr>
<td>Inglorious</td>
<td class="sortOdds right">15-1</td>
</tr>
<tr>
<td>Its Tricky</td>
<td class="sortOdds right">8-1</td>
</tr>
<tr>
<td>Joyful Victory</td>
<td class="sortOdds right">7-1</td>
</tr>
<tr>
<td>Kathmanblu</td>
<td class="sortOdds right">7-1</td>
</tr>
<tr>
<td>Lilacs And Lace</td>
<td class="sortOdds right">8 - 1</td>
</tr>
<tr>
<td>Mildly Offensive</td>
<td class="sortOdds right">45-1</td>
</tr>
<tr>
<td>Miss Smarty Pants</td>
<td class="sortOdds right">40-1</td>
</tr>
<tr>
<td>Oh Carole</td>
<td class="sortOdds right">45-1</td>
</tr>
<tr>
<td>Pomeroys Pistol</td>
<td class="sortOdds right">50-1</td>
</tr>
<tr>
<td>R Heat Lightning</td>
<td class="sortOdds right">3-1</td>
</tr>
<tr>
<td>Rigoletta</td>
<td class="sortOdds right">45-1</td>
</tr>
<tr>
<td>Royal Delta</td>
<td class="sortOdds right">30-1</td>
</tr>
<tr>
<td>Salty Strike</td>
<td class="sortOdds right">50-1</td>
</tr>
<tr>
<td>Snow Fall</td>
<td class="sortOdds right">45-1</td>
</tr>
<tr>
<td>Summer Soiree</td>
<td class="sortOdds right">45-1</td>
</tr>
<tr>
<td>Wyomia</td>
<td class="sortOdds right">20-1</td>
</tr>
<tr>
<td>Zazu</td>
<td class="sortOdds right">7-1</td>
</tr>
<tr>
<td colspan="2" class="dateUpdated center"><em>Odds Updated April 10th, 2011</em></td>
</tr>
</table>
