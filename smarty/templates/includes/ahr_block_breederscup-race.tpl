{literal}<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>{/literal}
										<div class="block">
										<h2 class="title-custom">Classic<a name="Classic"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Dangerous Midge">Dangerous Midge</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=Brian Meehan">Brian Meehan</a></td>
                                            <td >2:29.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Conduit">Conduit</a></td>
                                            <td >Ryan L. Moore</td>
                                            <td ><a href="/trainer?name=Sir Michael Stoute">Sir Michael Stoute</a></td>
                                            <td >2:23.75</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Conduit">Conduit</a></td>
                                            <td >Ryan L. Moore</td>
                                            <td ><a href="/trainer?name=Sir Michael Stoute">Sir Michael Stoute</a></td>
                                            <td >2:23.42</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=English Channel">English Channel</a></td>
                                            <td ><a href="/jockey?name=John R. Velazquez">John R. Velazquez</a></td>
                                            <td ><a href="/trainer?name=Todd A. Pletcher">Todd A. Pletcher</a></td>
                                            <td >2:36.96</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Red Rocks">Red Rocks</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=Brian Meehan">Brian Meehan</a></td>
                                            <td >2:27.32</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  ><a href="/horse?name=Shirocco">Shirocco</a></td>
                                            <td ><a href="/jockey?name=Christophe Soumillon">Christophe Soumillon</a></td>
                                            <td ><a href="/trainer?name=Andre Fabre">Andre Fabre</a></td>
                                            <td >2:29.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Better Talk Now">Better Talk Now</a></td>
                                            <td ><a href="/jockey?name=Ramon Dominguez">Ramon Dominguez</a></td>
                                            <td ><a href="/trainer?name=H. Graham Motion">H. Graham Motion</a></td>
                                            <td >2:29.70</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  >High Chaparral (DH)</td>
                                            <td ><a href="/jockey?name=Michael Kinane">Michael Kinane</a></td>
                                            <td >Aidan O\'Brien</td>
                                            <td >2:24.24</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2003</td>
										    <td  >Johar (DH)</td>
                                            <td ><a href="/jockey?name=Alex Solis">Alex Solis</a></td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >2:24.24</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=High Chaparral">High Chaparral</a></td>
                                            <td ><a href="/jockey?name=Michael Kinane">Michael Kinane</a></td>
                                            <td >Aidan O\'Brien</td>
                                            <td >2:30.14</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2001</td>
										    <td  >Fantastic Light</td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=Saeed bin Suroor">Saeed bin Suroor</a></td>
                                            <td >2:24.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2000</td>
										    <td  >Kalanisi</td>
                                            <td >Johnny Murtagh</td>
                                            <td ><a href="/trainer?name=Sir Michael Stoute">Sir Michael Stoute</a></td>
                                            <td >2:26.96</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1999</td>
										    <td  >Daylami</td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=Saeed bin Suroor">Saeed bin Suroor</a></td>
                                            <td >2:24.73</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1998</td>
										    <td  >Buck\'s Boy</td>
                                            <td >Shane Sellers</td>
                                            <td >P. Noel Hickey</td>
                                            <td >2:28.74</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1997</td>
										    <td  >Chief Bearhart</td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td ><a href="/trainer?name=Mark Frostad">Mark Frostad</a></td>
                                            <td >2:23.92</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1996</td>
										    <td  >Pilsudski</td>
                                            <td >Walter Swinburn</td>
                                            <td ><a href="/trainer?name=Michael Stoute">Michael Stoute</a></td>
                                            <td >2:30.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1995</td>
										    <td  >Northern Spur</td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Ron McAnally</td>
                                            <td >2:42.07</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1994</td>
										    <td  >Tikkanen</td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td >Jonathan Pease</td>
                                            <td >2:26.50</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1993</td>
										    <td  >Kotashaan</td>
                                            <td ><a href="/jockey?name=Kent Desormeaux">Kent Desormeaux</a></td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >2:25.16</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1992</td>
										    <td  >Fraise</td>
                                            <td >Pat Valenzuela</td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >2:24.08</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1991</td>
										    <td  >Miss Alleged</td>
                                            <td >Eric Legrix</td>
                                            <td >Pascal Bary</td>
                                            <td >2:30.95</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1990</td>
										    <td  >In the Wings</td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td ><a href="/trainer?name=Andre Fabre">Andre Fabre</a></td>
                                            <td >2:29.60</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1989</td>
										    <td  >Prized</td>
                                            <td >Ed Delahoussaye</td>
                                            <td ><a href="/trainer?name=Neil Drysdale">Neil Drysdale</a></td>
                                            <td >2:28.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1988</td>
										    <td  >Great Communicator</td>
                                            <td >Ray Sibille</td>
                                            <td >Thad Ackel</td>
                                            <td >2:35.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1987</td>
										    <td  >Theatrical</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >2:24.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1986</td>
										    <td  >Manila</td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td >LeRoy Jolley</td>
                                            <td >2:25.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1985</td>
										    <td  >Pebbles</td>
                                            <td >Pat Eddery</td>
                                            <td >Clive Brittain</td>
                                            <td >2:27.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1984</td>
										    <td  >Lashkari</td>
                                            <td >Yves Saint-Martin</td>
                                            <td >Alain de Royer-Dupre</td>
                                            <td >2:25.20</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Dirt Mile<a name="Dirt Mile"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Dakota Phone">Dakota Phone</a></td>
                                            <td ><a href="/jockey?name=Joel Rosario">Joel Rosario</a></td>
                                            <td ><a href="/trainer?name=Jerry Hollendorfer">Jerry Hollendorfer</a></td>
                                            <td >1:35.29</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Furthest Land">Furthest Land</a></td>
                                            <td ><a href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
                                            <td ><a href="/trainer?name=Michael J. Maker">Michael J. Maker</a></td>
                                            <td >1:35.50</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Albertus Maximus">Albertus Maximus</a></td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td ><a href="/trainer?name=Vladimir Cerin">Vladimir Cerin</a></td>
                                            <td >1:33.41</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Corinthian">Corinthian</a></td>
                                            <td ><a href="/jockey?name=Kent Desormeaux">Kent Desormeaux</a></td>
                                            <td ><a href="/trainer?name=James A. Jerkens">James A. Jerkens</a></td>
                                            <td >1:39.06</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Distaff<a name="Distaff"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Dubai Majesty">Dubai Majesty</a></td>
                                            <td ><a href="/jockey?name=Jamie Theriot">Jamie Theriot</a></td>
                                            <td ><a href="/trainer?name=W. Bret Calhoun">W. Bret Calhoun</a></td>
                                            <td >1:22:33</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Informed Decision">Informed Decision</a></td>
                                            <td ><a href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
                                            <td ><a href="/trainer?name=Jonathan E. Sheppard">Jonathan E. Sheppard</a></td>
                                            <td >1:21.66</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Ventura">Ventura</a></td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td ><a href="/trainer?name=Robert J. Frankel">Robert J. Frankel</a></td>
                                            <td >1:19.90</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Maryfield">Maryfield</a></td>
                                            <td ><a href="/jockey?name=Elvis Trujillo">Elvis Trujillo</a></td>
                                            <td >Doug O'Neill</td>
                                            <td >1:09.85</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Filly & Mare Sprint<a name="Filly & Mare Sprint"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Pluck">Pluck</a></td>
                                            <td ><a href="/jockey?name=Garrett K. Gomez">Garrett K. Gomez</a></td>
                                            <td ><a href="/trainer?name=Todd Pletcher">Todd Pletcher</a></td>
                                            <td >1:36.98</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Pounced">Pounced</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=John Gosden">John Gosden</a></td>
                                            <td >1:35.47</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Donativum">Donativum</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=John Gosden">John Gosden</a></td>
                                            <td >1:34.68</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Nownownow">Nownownow</a></td>
                                            <td ><a href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
                                            <td >Francois Parisel</td>
                                            <td >1:40.48</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Filly & Mare Turf<a name="Filly & Mare Turf"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Blame">Blame</a></td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td >Albert M. Stall, Jr.</td>
                                            <td >2:02.28</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Zenyatta">Zenyatta</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=John Shirreffs">John Shirreffs</a></td>
                                            <td >2:00.32</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  >Raven's Pass</td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=John Gosden">John Gosden</a></td>
                                            <td >1:59.27</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Curlin">Curlin</a></td>
                                            <td ><a href="/jockey?name=Robby Albarado">Robby Albarado</a></td>
                                            <td ><a href="/trainer?name=Steve Asmussen">Steve Asmussen</a></td>
                                            <td >2:00.59</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Invasor">Invasor</a></td>
                                            <td ><a href="/jockey?name=Fernando Jara">Fernando Jara</a></td>
                                            <td ><a href="/trainer?name=Kiaran McLaughlin">Kiaran McLaughlin</a></td>
                                            <td >2:02.18</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  ><a href="/horse?name=Saint Liam">Saint Liam</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=Richard E. Dutrow, Jr.">Richard E. Dutrow, Jr.</a></td>
                                            <td >2:01.49</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Ghostzapper">Ghostzapper</a></td>
                                            <td ><a href="/jockey?name=Javier Castellano">Javier Castellano</a></td>
                                            <td ><a href="/trainer?name=Robert J. Frankel">Robert J. Frankel</a></td>
                                            <td >1:59.02</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  ><a href="/horse?name=Pleasantly Perfect">Pleasantly Perfect</a></td>
                                            <td ><a href="/jockey?name=Alex Solis">Alex Solis</a></td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >1:59.88</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=Volponi">Volponi</a></td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td >Phillip G. Johnson</td>
                                            <td >2:01.39</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2001</td>
										    <td  ><a href="/horse?name=Tiznow">Tiznow</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Jay M. Robbins</td>
                                            <td >2:00.62</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  ><a href="/horse?name=Tiznow">Tiznow</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Jay M. Robbins</td>
                                            <td >2:00.75</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1999</td>
										    <td  ><a href="/horse?name=Cat Thief">Cat Thief</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:59.52</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1998</td>
										    <td  ><a href="/horse?name=Awesome Again">Awesome Again</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td >Patrick B. Byrne</td>
                                            <td >2:02.16</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1997</td>
										    <td  ><a href="/horse?name=Skip Away">Skip Away</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=Sonny Hine">Sonny Hine</a></td>
                                            <td >1:59.16</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1996</td>
										    <td  ><a href="/horse?name=Alphabet Soup">Alphabet Soup</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td ><a href="/trainer?name=David Hofmans">David Hofmans</a></td>
                                            <td >2:01.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1995</td>
										    <td  ><a href="/horse?name=Cigar">Cigar</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >1:59.58</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1994</td>
										    <td  ><a href="/horse?name=Concern">Concern</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >Richard W. Small</td>
                                            <td >2:02.41</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1993</td>
										    <td  ><a href="/horse?name=Arcangues">Arcangues</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=Andre Fabre">Andre Fabre</a></td>
                                            <td >2:00.83</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1992</td>
										    <td  ><a href="/horse?name=A.P. Indy">A.P. Indy</a></td>
                                            <td >Ed Delahoussaye</td>
                                            <td ><a href="/trainer?name=Neil Drysdale">Neil Drysdale</a></td>
                                            <td >2:00.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1991</td>
										    <td  ><a href="/horse?name=Black Tie Affair">Black Tie Affair</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=Ernie T. Poulos">Ernie T. Poulos</a></td>
                                            <td >2:02.95</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1990</td>
										    <td  ><a href="/horse?name=Unbridled">Unbridled</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=Carl Nafzger">Carl Nafzger</a></td>
                                            <td >2:02.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1989</td>
										    <td  ><a href="/horse?name=Sunday Silence">Sunday Silence</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Charlie Whittingham</td>
                                            <td >2:00.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1988</td>
										    <td  ><a href="/horse?name=Alysheba">Alysheba</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td ><a href="/trainer?name=Jack Van Berg">Jack Van Berg</a></td>
                                            <td >2:04.80</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1987</td>
										    <td  ><a href="/horse?name=Ferdinand">Ferdinand</a></td>
                                            <td >Bill Shoemaker</td>
                                            <td >Charlie Whittingham</td>
                                            <td >2:01.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1986</td>
										    <td  ><a href="/horse?name=Skywalker">Skywalker</a></td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td >Michael Whittingham</td>
                                            <td >2:00.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1985</td>
										    <td  ><a href="/horse?name=Proud Truth">Proud Truth</a></td>
                                            <td >Jorge Velasquez</td>
                                            <td ><a href="/trainer?name=John M. Veitch">John M. Veitch</a></td>
                                            <td >2:00.80</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1984</td>
										    <td  ><a href="/horse?name=Wild Again">Wild Again</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td >Vincent Timphony</td>
                                            <td >2:03.40</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Juvenile<a name="Juvenile"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Uncle Mo">Uncle Mo</a></td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td ><a href="/trainer?name=Todd Pletcher">Todd Pletcher</a></td>
                                            <td >1:42.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Vale of York">Vale of York</a></td>
                                            <td >Ahmed Ajtebi</td>
                                            <td ><a href="/trainer?name=Saeed bin Suroor">Saeed bin Suroor</a></td>
                                            <td >1:43.48</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Midshipman">Midshipman</a></td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td ><a href="/trainer?name=Bob Baffert">Bob Baffert</a></td>
                                            <td >1:40.94</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=War Pass">War Pass</a></td>
                                            <td ><a href="/jockey?name=Cornelio Velasquez">Cornelio Velasquez</a></td>
                                            <td >Nick Zito</td>
                                            <td >1:42.76</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Street Sense">Street Sense</a></td>
                                            <td ><a href="/jockey?name=Calvin Borel">Calvin Borel</a></td>
                                            <td ><a href="/trainer?name=Carl Nafzger">Carl Nafzger</a></td>
                                            <td >1:42.59</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  >Stevie Wonderboy</td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td >Doug O'Neill</td>
                                            <td >1:41.64</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Wilko">Wilko</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td ><a href="/trainer?name=Jeremy Noseda">Jeremy Noseda</a></td>
                                            <td >1:42.09</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  ><a href="/horse?name=Action This Day">Action This Day</a></td>
                                            <td >David R. Flores</td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >1:43.62</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=Vindication">Vindication</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=Bob Baffert">Bob Baffert</a></td>
                                            <td >1:49.61</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2001</td>
										    <td  >Johannesburg</td>
                                            <td ><a href="/jockey?name=Michael Kinane">Michael Kinane</a></td>
                                            <td >Aidan O'Brien</td>
                                            <td >1:42.27</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  ><a href="/horse?name=Macho Uno">Macho Uno</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >Joseph Orseno</td>
                                            <td >1:42.05</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1999</td>
										    <td  >Anees</td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td >Alex Hassinger, Jr.</td>
                                            <td >1:42.29</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1998</td>
										    <td  ><a href="/horse?name=Answer Lively">Answer Lively</a></td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >Bobby C. Barnett</td>
                                            <td >1:44.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1997</td>
										    <td  ><a href="/horse?name=Favorite Trick">Favorite Trick</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td >Patrick B. Byrne</td>
                                            <td >1:41.47</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1996</td>
										    <td  >Boston Harbor</td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:43.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1995</td>
										    <td  >Unbridled's Song</td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td >James T. Ryerson</td>
                                            <td >1:41.60</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1994</td>
										    <td  ><a href="/horse?name=Timber Country">Timber Country</a></td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:44.55</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1993</td>
										    <td  ><a href="/horse?name=Brocco">Brocco</a></td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td >Randy Winick</td>
                                            <td >1:42.99</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1992</td>
										    <td  ><a href="/horse?name=Gilded Time">Gilded Time</a></td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Darrell Vienna</td>
                                            <td >1:43.43</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1991</td>
										    <td  ><a href="/horse?name=Arazi">Arazi</a></td>
                                            <td >Pat Valenzuela</td>
                                            <td >Francois Boutin</td>
                                            <td >1:44.78</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1990</td>
										    <td  ><a href="/horse?name=Fly So Free">Fly So Free</a></td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td >Scotty Schulhofer</td>
                                            <td >1:43.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1989</td>
										    <td  ><a href="/horse?name=Rhythm">Rhythm</a></td>
                                            <td >Craig Perret</td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:43.60</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1988</td>
										    <td  ><a href="/horse?name=Is It True">Is It True</a></td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:46.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1987</td>
										    <td  ><a href="/horse?name=Success Express">Success Express</a></td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:35.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1986</td>
										    <td  ><a href="/horse?name=Capote">Capote</a></td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:43.80</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1985</td>
										    <td  >Tasso</td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td ><a href="/trainer?name=Neil Drysdale">Neil Drysdale</a></td>
                                            <td >1:36.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1984</td>
										    <td  >Chief's Crown</td>
                                            <td >Don MacBeth</td>
                                            <td >Roger Laurin</td>
                                            <td >1:36.20</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Juvenile Fillies Turf<a name="Juvenile Fillies Turf"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Eldaafer">Eldaafer</a></td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td >Diane Alvardo</td>
                                            <td >2:59.62</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  >Man of Iron</td>
                                            <td >Johnny Murtagh</td>
                                            <td >Aidan O'Brien</td>
                                            <td >2:54.11</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Muhannak">Muhannak</a></td>
                                            <td >Pat Smullen</td>
                                            <td >Ralph Beckett</td>
                                            <td >2:28.24</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Juvenile Filly<a name="Juvenile Filly"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Unrivaled Belle">Unrivaled Belle</a></td>
                                            <td ><a href="/jockey?name=Kent Desormeaux">Kent Desormeaux</a></td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >1:50.04</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Life Is Sweet">Life Is Sweet</a></td>
                                            <td ><a href="/jockey?name=Garrett K. Gomez">Garrett K. Gomez</a></td>
                                            <td ><a href="/trainer?name=John Shirreffs">John Shirreffs</a></td>
                                            <td >1:48.58</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Zenyatta">Zenyatta</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=John Shirreffs">John Shirreffs</a></td>
                                            <td >1:46.85</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Ginger Punch">Ginger Punch</a></td>
                                            <td ><a href="/jockey?name=Rafael Bejarano">Rafael Bejarano</a></td>
                                            <td ><a href="/trainer?name=Robert J. Frankel">Robert J. Frankel</a></td>
                                            <td >1:50.11</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Round Pond">Round Pond</a></td>
                                            <td ><a href="/jockey?name=Edgar Prado">Edgar Prado</a></td>
                                            <td ><a href="/trainer?name=Michael R. Matz">Michael R. Matz</a></td>
                                            <td >1:50.50</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  >Pleasant Home</td>
                                            <td ><a href="/jockey?name=Cornelio Velasquez">Cornelio Velasquez</a></td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:48.34</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Ashado">Ashado</a></td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td ><a href="/trainer?name=Todd Pletcher">Todd Pletcher</a></td>
                                            <td >1:48.26</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  ><a href="/horse?name=Adoration">Adoration</a></td>
                                            <td >Pat Valenzuela</td>
                                            <td ><a href="/trainer?name=David Hofmans">David Hofmans</a></td>
                                            <td >1:49.17</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=Azeri">Azeri</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td >Laura de Seroux</td>
                                            <td >1:48.64</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2001</td>
										    <td  >Unbridled Elaine</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=Dallas Stewart">Dallas Stewart</a></td>
                                            <td >1:49.21</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  ><a href="/horse?name=Spain">Spain</a></td>
                                            <td ><a href="/jockey?name=Victor Espinoza">Victor Espinoza</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:47.66</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1999</td>
										    <td  >Beautiful Pleasure</td>
                                            <td ><a href="/jockey?name=Jorge F. Chavez">Jorge F. Chavez</a></td>
                                            <td >John T. Ward, Jr.</td>
                                            <td >1:47.56</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1998</td>
										    <td  >Escena</td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >1:49.89</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1997</td>
										    <td  >Ajina</td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=William I. Mott">William I. Mott</a></td>
                                            <td >1:47.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1996</td>
										    <td  >Jewel Princess</td>
                                            <td ><a href="/jockey?name=Corey Nakatani">Corey Nakatani</a></td>
                                            <td >Wallace Dollase</td>
                                            <td >1:48.40</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1995</td>
										    <td  ><a href="/horse?name=Inside Information">Inside Information</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:46.15</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1994</td>
										    <td  >One Dreamer</td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td >Thomas F. Proctor</td>
                                            <td >1:50.79</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1993</td>
										    <td  >Hollywood Wildcat</td>
                                            <td >Ed Delahoussaye</td>
                                            <td ><a href="/trainer?name=Neil Drysdale">Neil Drysdale</a></td>
                                            <td >1:48.35</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1992</td>
										    <td  >Paseana</td>
                                            <td ><a href="/jockey?name=Chris McCarron">Chris McCarron</a></td>
                                            <td >Ron McAnally</td>
                                            <td >1:48.17</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1991</td>
										    <td  >Dance Smartly</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td >James E. Day</td>
                                            <td >1:50.95</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1990</td>
										    <td  >Bayakoa</td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td >Ron McAnally</td>
                                            <td >1:49.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1989</td>
										    <td  >Bayakoa</td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td >Ron McAnally</td>
                                            <td >1:47.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1988</td>
										    <td  >Personal Ensign</td>
                                            <td >Randy Romero</td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:52.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1987</td>
										    <td  >Sacahuista</td>
                                            <td >Randy Romero</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >2:02.80</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1986</td>
										    <td  >Lady's Secret</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >2:01.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1985</td>
										    <td  >Life's Magic</td>
                                            <td >Angel Cordero, Jr.</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >2:02.00</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1984</td>
										    <td  >Princess Rooney</td>
                                            <td >Ed Delahoussaye</td>
                                            <td ><a href="/trainer?name=Neil Drysdale">Neil Drysdale</a></td>
                                            <td >2:02.40</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Juvenile Sprint<a name="Juvenile Sprint"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Juvenile Turf<a name="Juvenile Turf"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Ladies Classic<a name="Ladies Classic"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Chamberlain Bridge">Chamberlain Bridge</a></td>
                                            <td ><a href="/jockey?name=Jamie Theriot">Jamie Theriot</a></td>
                                            <td ><a href="/trainer?name=Bret Calhoun">Bret Calhoun</a></td>
                                            <td >56.53</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=California Flag">California Flag</a></td>
                                            <td ><a href="/jockey?name=Joe Talamo">Joe Talamo</a></td>
                                            <td >Brian Koriner</td>
                                            <td >1:11.28</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Desert Code">Desert Code</a></td>
                                            <td ><a href="/jockey?name=Richard Migliore">Richard Migliore</a></td>
                                            <td ><a href="/trainer?name=David Hofmans">David Hofmans</a></td>
                                            <td >1:11.60</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Marathon<a name="Marathon"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Mile<a name="Mile"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Awesome Feather">Awesome Feather</a></td>
                                            <td ><a href="/jockey?name=Jeffrey Sanchez">Jeffrey Sanchez</a></td>
                                            <td >Stanley I. Gold</td>
                                            <td >1:45.17</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=She Be Wild">She Be Wild</a></td>
                                            <td ><a href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
                                            <td ><a href="/trainer?name=Wayne Catalano">Wayne Catalano</a></td>
                                            <td >1:43.80</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Stardom Bound">Stardom Bound</a></td>
                                            <td ><a href="/jockey?name=Mike E. Smith">Mike E. Smith</a></td>
                                            <td ><a href="/trainer?name=Christopher Paasch">Christopher Paasch</a></td>
                                            <td >1:40.99</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Indian Blessing">Indian Blessing</a></td>
                                            <td ><a href="/jockey?name=Garrett Gomez">Garrett Gomez</a></td>
                                            <td ><a href="/trainer?name=Bob Baffert">Bob Baffert</a></td>
                                            <td >1:44.73</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Dreaming of Anna">Dreaming of Anna</a></td>
                                            <td ><a href="/jockey?name=Rene Douglas">Rene Douglas</a></td>
                                            <td ><a href="/trainer?name=Wayne Catalano">Wayne Catalano</a></td>
                                            <td >1:43.81</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  ><a href="/horse?name=Folklore">Folklore</a></td>
                                            <td ><a href="/jockey?name=Edgar Prado">Edgar Prado</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:43.85</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Sweet Catomine">Sweet Catomine</a></td>
                                            <td ><a href="/jockey?name=Corey Nakatani">Corey Nakatani</a></td>
                                            <td ><a href="/trainer?name=Julio C. Canani">Julio C. Canani</a></td>
                                            <td >1:41.65</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  ><a href="/horse?name=Halfbridled">Halfbridled</a></td>
                                            <td ><a href="/jockey?name=Julie Krone">Julie Krone</a></td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >1:42.75</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=Storm Flag Flying">Storm Flag Flying</a></td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:49.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2001</td>
										    <td  >Tempera</td>
                                            <td ><a href="/jockey?name=David Flores">David Flores</a></td>
                                            <td ><a href="/trainer?name=Eoin G. Harty">Eoin G. Harty</a></td>
                                            <td >1:41.49</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  >Caressing</td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td >David R. Vance</td>
                                            <td >1:42.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1999</td>
										    <td  >Cash Run</td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:43.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1998</td>
										    <td  >Silverbulletday</td>
                                            <td ><a href="/jockey?name=Gary Stevens">Gary Stevens</a></td>
                                            <td ><a href="/trainer?name=Bob Baffert">Bob Baffert</a></td>
                                            <td >1:43.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1997</td>
										    <td  >Countess Diana</td>
                                            <td >Shane Sellers</td>
                                            <td >Patrick B. Byrne</td>
                                            <td >1:42.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1996</td>
										    <td  ><a href="/horse?name=Storm Song">Storm Song</a></td>
                                            <td >Craig Perret</td>
                                            <td >Nick Zito</td>
                                            <td >1:43.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1995</td>
										    <td  >My Flag</td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >C. R. McGaughey III</td>
                                            <td >1:42.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1994</td>
										    <td  >Flanders</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:45.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1993</td>
										    <td  >Phone Chatter</td>
                                            <td >Laffit Pincay, Jr.</td>
                                            <td ><a href="/trainer?name=Richard Mandella">Richard Mandella</a></td>
                                            <td >1:43.00</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1992</td>
										    <td  >Eliza</td>
                                            <td >Pat Valenzuela</td>
                                            <td >Alex L. Hassinger, Jr.</td>
                                            <td >1:42.80</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1991</td>
										    <td  >Pleasant Stage</td>
                                            <td >Ed Delahoussaye</td>
                                            <td >Chris Speckert</td>
                                            <td >1:46.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1990</td>
										    <td  ><a href="/horse?name=Meadow Star">Meadow Star</a></td>
                                            <td ><a href="/jockey?name=Jose Santos">Jose Santos</a></td>
                                            <td >LeRoy Jolley</td>
                                            <td >1:44.00</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1989</td>
										    <td  >Go For Wand</td>
                                            <td >Randy Romero</td>
                                            <td >William Badgett, Jr.</td>
                                            <td >1:44.20</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1988</td>
										    <td  ><a href="/horse?name=Open Mind">Open Mind</a></td>
                                            <td >Angel Cordero, Jr.</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:46.60</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1987</td>
										    <td  >Epitome</td>
                                            <td ><a href="/jockey?name=Pat Day">Pat Day</a></td>
                                            <td >Phil Hauswald</td>
                                            <td >1:36.40</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1986</td>
										    <td  >Brave Raj</td>
                                            <td >Pat Valenzuela</td>
                                            <td >Melvin F. Stute</td>
                                            <td >1:43.20</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1985</td>
										    <td  >Twilight Ridge</td>
                                            <td >Jorge Velasquez</td>
                                            <td ><a href="/trainer?name=D. Wayne Lukas">D. Wayne Lukas</a></td>
                                            <td >1:35.80</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >1984</td>
										    <td  >Outstandingly</td>
                                            <td >Walter Guerra</td>
                                            <td >Pancho Martin</td>
                                            <td >1:37.80</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Sprint<a name="Sprint"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=Shared Account">Shared Account</a></td>
                                            <td ><a href="/jockey?name=Edgar Prado">Edgar Prado</a></td>
                                            <td ><a href="/trainer?name=Graham Motion">Graham Motion</a></td>
                                            <td >2:17.74</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Midday">Midday</a></td>
                                            <td >Tom Queally</td>
                                            <td >Henry Cecil</td>
                                            <td >1:59.14</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Forever Together">Forever Together</a></td>
                                            <td ><a href="/jockey?name=Julien Leparoux">Julien Leparoux</a></td>
                                            <td >Jonathan Sheppard</td>
                                            <td >2:01.58</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2007</td>
										    <td  ><a href="/horse?name=Lahudood">Lahudood</a></td>
                                            <td ><a href="/jockey?name=Alan Garcia">Alan Garcia</a></td>
                                            <td ><a href="/trainer?name=Kiaran McLaughlin">Kiaran McLaughlin</a></td>
                                            <td >2:22.75</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2006</td>
										    <td  ><a href="/horse?name=Ouija Board">Ouija Board</a></td>
                                            <td ><a href="/jockey?name=Frankie Dettori">Frankie Dettori</a></td>
                                            <td >Ed Dunlop</td>
                                            <td >2:14.55</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2005</td>
										    <td  ><a href="/horse?name=Intercontinental">Intercontinental</a></td>
                                            <td ><a href="/jockey?name=Rafael Bejarano">Rafael Bejarano</a></td>
                                            <td ><a href="/trainer?name=Robert J. Frankel">Robert J. Frankel</a></td>
                                            <td >2:02.34</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2004</td>
										    <td  ><a href="/horse?name=Ouija Board">Ouija Board</a></td>
                                            <td >Kieren Fallon</td>
                                            <td >Ed Dunlop</td>
                                            <td >2:18.25</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2003</td>
										    <td  ><a href="/horse?name=Islington">Islington</a></td>
                                            <td >Kieren Fallon</td>
                                            <td ><a href="/trainer?name=Sir Michael Stoute">Sir Michael Stoute</a></td>
                                            <td >1:59.13</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2002</td>
										    <td  ><a href="/horse?name=Starine">Starine</a></td>
                                            <td ><a href="/jockey?name=John Velazquez">John Velazquez</a></td>
                                            <td ><a href="/trainer?name=Robert J. Frankel">Robert J. Frankel</a></td>
                                            <td >2:03.57</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2001</td>
										    <td  >Banks Hill</td>
                                            <td ><a href="/jockey?name=Olivier Peslier">Olivier Peslier</a></td>
                                            <td ><a href="/trainer?name=Andre Fabre">Andre Fabre</a></td>
                                            <td >2:00.36</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2000</td>
										    <td  >Perfect Sting</td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >Joe Orseno</td>
                                            <td >2:13.07</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >1999</td>
										    <td  >Soaring Softly</td>
                                            <td ><a href="/jockey?name=Jerry Bailey">Jerry Bailey</a></td>
                                            <td >James J. Toner</td>
                                            <td >2:13.89</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Turf<a name="Turf"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  <tr>                                        
                                           	 <td  >2010</td>
										    <td  ><a href="/horse?name=More Than Real">More Than Real</a></td>
                                            <td ><a href="/jockey?name=Garrett K. Gomez">Garrett K. Gomez</a></td>
                                            <td ><a href="/trainer?name=Todd Pletcher">Todd Pletcher</a></td>
                                            <td >1:36.61</td>
                                            </tr>
										  
										 <tr class="odd">                                        
                                           	 <td  >2009</td>
										    <td  ><a href="/horse?name=Tapitsfly">Tapitsfly</a></td>
                                            <td ><a href="/jockey?name=Robby Albarado">Robby Albarado</a></td>
                                            <td ><a href="/trainer?name=Dale L. Romans">Dale L. Romans</a></td>
                                            <td >1:34.25</td>
                                            </tr>
										  
										 <tr>                                        
                                           	 <td  >2008</td>
										    <td  ><a href="/horse?name=Maram">Maram</a></td>
                                            <td ><a href="/jockey?name=Jose Lezcano">Jose Lezcano</a></td>
                                            <td ><a href="/trainer?name=Chad C. Brown">Chad C. Brown</a></td>
                                            <td >1:35.15</td>
                                            </tr>
										  
										 										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										<div class="block">
										<h2 class="title-custom">Turf Sprint<a name="Turf Sprint"></a></h2>

<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Breeders Cup Winners" summary="The past winners of the Breeders Cup " >
<tr>										   <tr >
										  	<th width="12%" >Year</th>
                                            <th width="25%" >Winner</th>
                                            <th width="25%">Jockey</th>
                                            <th width="25%">Trainer</th>
                                            <th width="13%">Win Time</th>
                                          </tr>
										  										  </table> </div>
						  </div><div id="box-shd"></div><!-- === SHADOW === -->			 										