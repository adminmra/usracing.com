		<table width="100%" class="table table-condensed table-striped table-bordered" id="infoEntries" >

			<tr >
				<th width="15%" >Date</th>
				<th>Race</th>
				<th>Track</th>
				<!--th >Distance</th>
				<th >Grade</th>
				<th >Age</th>
				-->
				<th >Qualifier</th>
				<th >Winner</th>
			</tr>
			<tr>
				<td  >Jan 7, 2017</td>
				<td >
					L'Ormarins Queen's Plate				</td>
				<td >
					KEN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Legal Eagle (SAF)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jan 7, 2017</td>
				<td >
					Maine Chance Farms Paddock S.				</td>
				<td >
					KEN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Bela-Bela (SAF)</td>
			</tr>

			<tr>
				<td  >Feb 19, 2017</td>
				<td >
					February S.				</td>
				<td >
					TOK				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Gold Dream (JPN)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Apr 1, 2017</td>
				<td >
					Darley TJ Smith Stakes				</td>
				<td >
					RAN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf-sprint'>Turf Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Chautauqua (AUS)</td>
			</tr>

			<tr>
				<td  >Apr 1, 2017</td>
				<td >
					The Star Doncaster Mile				</td>
				<td >
					RAN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Somewhat</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Apr 8, 2017</td>
				<td >
					Coolmore Legacy Queen of the Turf Stakes				</td>
				<td >
					RAN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Foxplay (AUS)</td>
			</tr>

			<tr>
				<td  >May 1, 2017</td>
				<td >
					Gran Premio Criadores				</td>
				<td >
					PAL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td > Kiriaki (ARG)</td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >May 25, 2017</td>
				<td >
					Gran Premio 25 de Mayo				</td>
				<td >
					SIS				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >May 28, 2017</td>
				<td >
					Gran Premio Club Hipico Falabella				</td>
				<td >
					CHS				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 3, 2017</td>
				<td >
					Shoemaker Mile				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jun 4, 2017</td>
				<td >
					Yasuda Kinen				</td>
				<td >
					TOK				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 10, 2017</td>
				<td >
					Metropolitan H.				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/dirt-mile'>Dirt Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jun 10, 2017</td>
				<td >
					Ogden Phipps				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 11, 2017</td>
				<td >
					Grande Premio Brasil				</td>
				<td >
					GVA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jun 17, 2017</td>
				<td >
					Fleur de Lis H.				</td>
				<td >
					CD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 17, 2017</td>
				<td >
					Stephen Foster H.				</td>
				<td >
					CD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jun 25, 2017</td>
				<td >
					Takarazuka Kinen				</td>
				<td >
					HSN				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jun 25, 2017</td>
				<td >
					Gran Premio Pamplona				</td>
				<td >
					MON				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jul 1, 2017</td>
				<td >
					Princess Rooney H.				</td>
				<td >
					GP				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-sprint'>Filly & Mare Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 1, 2017</td>
				<td >
					Smile Sprint Handicap				</td>
				<td >
					GP				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jul 2, 2017</td>
				<td >
					Highlander Stakes				</td>
				<td >
					WO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf-sprint'>Turf Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 8, 2017</td>
				<td >
					Belmont Sprint Championship				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jul 29, 2017</td>
				<td >
					King George VI & Queen Elizabeth Stakes sponsored by QIPCO				</td>
				<td >
					ASC				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 29, 2017</td>
				<td >
					Bing Crosby S.				</td>
				<td >
					DMR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Jul 30, 2017</td>
				<td >
					Clement L. Hirsch Stakes				</td>
				<td >
					DMR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Jul 30, 2017</td>
				<td >
					Betfair.com Haskell Invitational				</td>
				<td >
					MTH				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 2, 2017</td>
				<td >
					Qatar Sussex Stakes				</td>
				<td >
					GOO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 5, 2017</td>
				<td >
					Whitney Handicap				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 12, 2017</td>
				<td >
					Arlington Million				</td>
				<td >
					AP				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 12, 2017</td>
				<td >
					Beverly D. Stakes				</td>
				<td >
					AP				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 19, 2017</td>
				<td >
					Del Mar H. Presented By The Japan Racing Association				</td>
				<td >
					DMR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 19, 2017</td>
				<td >
					Pacific Classic				</td>
				<td >
					DMR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 20, 2017</td>
				<td >
					Prix du Haras de Fresnay-Le Buffard - Jacques Le Marois				</td>
				<td >
					DEA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 23, 2017</td>
				<td >
					Juddmonte International S.				</td>
				<td >
					YOR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 24, 2017</td>
				<td >
					Darley Yorkshire Oaks				</td>
				<td >
					YOR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 25, 2017</td>
				<td >
					Coolmore Nunthorpe Stakes				</td>
				<td >
					YOR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf-sprint'>Turf Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					Pat O'Brien S.				</td>
				<td >
					DMR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/dirt-mile'>Dirt Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 26, 2017</td>
				<td >
					Ballerina Stakes				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-sprint'>Filly & Mare Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					Personal Ensign H.				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Aug 26, 2017</td>
				<td >
					Priority One Jets Forego Stakes				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/dirt-mile'>Dirt Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Aug 26, 2017</td>
				<td >
					Sword Dancer Invitational S.				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 2, 2017</td>
				<td >
					T. Von Zastrow Stutenpreis				</td>
				<td >
					BAD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 2, 2017</td>
				<td >
					Spinaway S.				</td>
				<td >
					SAR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies'>Juvenile Fillies</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 3, 2017</td>
				<td >
					Longines Grosser Preis Von Baden				</td>
				<td >
					BAD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 9, 2017</td>
				<td >
					Coolmore Fastnet Rock Matron S.				</td>
				<td >
					LEO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 9, 2017</td>
				<td >
					QIPCO Irish Champion Stakes				</td>
				<td >
					LEO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 9, 2017</td>
				<td >
					Willis Towers Watson Champions Juvenile Turf				</td>
				<td >
					LEO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-turf'>Juvenile Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 10, 2017</td>
				<td >
					Derrinstown Stud Flying Five Stakes				</td>
				<td >
					CUR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf-sprint'>Turf Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 10, 2017</td>
				<td >
					Moyglare Stud Stakes				</td>
				<td >
					CUR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies-turf'>Juvenile Fillies Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 16, 2017</td>
				<td >
					Iroquois S.				</td>
				<td >
					CD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile'>Juvenile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 16, 2017</td>
				<td >
					Pocahontas S.				</td>
				<td >
					CD				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies'>Juvenile Fillies</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 16, 2017</td>
				<td >
					Ricoh Woodbine Mile				</td>
				<td >
					WO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 17, 2017</td>
				<td >
					Natalma Stakes				</td>
				<td >
					WO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies-turf'>Juvenile Fillies Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 17, 2017</td>
				<td >
					Summer Stakes				</td>
				<td >
					WO				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-turf'>Juvenile Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 29, 2017</td>
				<td >
					Shadwell Rockfel				</td>
				<td >
					NEW				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies-turf'>Juvenile Fillies Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Joe Hirsch Turf Classic Invitational Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf'>Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					Vosburgh Invitational Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Juddmonte Royal Lodge Stakes				</td>
				<td >
					NEW				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-turf'>Juvenile Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					Awesome Again Stakes				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Chandelier Stakes				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies'>Juvenile Fillies</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					FrontRunner Stakes				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile'>Juvenile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Sep 30, 2017</td>
				<td >
					Rodeo Drive Stakes				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Sep 30, 2017</td>
				<td >
					Zenyatta Stakes				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 1, 2017</td>
				<td >
					Longines Prix de L'Opera				</td>
				<td >
					CHY				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 1, 2017</td>
				<td >
					Qatar Prix Jean-Luc Lagardere				</td>
				<td >
					CHY				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-turf'>Juvenile Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 1, 2017</td>
				<td >
					Total Prix Marcel Boussac				</td>
				<td >
					CHY				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies-turf'>Juvenile Fillies Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 1, 2017</td>
				<td >
					Sprinters S.				</td>
				<td >
					NKR				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/turf-sprint'>Turf Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 6, 2017</td>
				<td >
					Darley Alcibiades Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies'>Juvenile Fillies</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 6, 2017</td>
				<td >
					Stoll Keenon Ogden Phoenix Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Frizette Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies'>Juvenile Fillies</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					Jockey Club Gold Cup Invitational Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/classic'>Classic</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Claiborne Breeders' Futurity				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile'>Juvenile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					First Lady S.				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Shadwell Mile Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/mile'>Mile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 7, 2017</td>
				<td >
					Thoroughbred Club of America Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-sprint'>Filly & Mare Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 7, 2017</td>
				<td >
					Santa Anita Sprint Championship				</td>
				<td >
					SA				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/sprint'>Sprint</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 8, 2017</td>
				<td >
					Flower Bowl Invitational Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/filly-mare-turf'>Filly & Mare Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 8, 2017</td>
				<td >
					Foxwoods Champagne Stakes				</td>
				<td >
					BEL				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile'>Juvenile</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 8, 2017</td>
				<td >
					Dixiana Bourbon S.				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-turf'>Juvenile Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr bgcolor='#EBEBEB'>
				<td  >Oct 8, 2017</td>
				<td >
					Juddmonte Spinster Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/distaff'>Distaff</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

			<tr>
				<td  >Oct 11, 2017</td>
				<td >
					JPMorgan Chase Jessamine Stakes				</td>
				<td >
					KEE				</td>
				<td >
					<a href='https://www.usracing.com/breeders-cup/juvenile-fillies-turf'>Juvenile Fillies Turf</a>
				</td>
				<!--td ></td>
				<td ></td>
				<td ></td-->
				<td ></td>
			</tr>

					<tr><td colspan="7" align="left">
				* - tentative date<br>
				** - purse includes Breeders' Cup Stakes funds
			</td></tr>

		</table>
		</td>
		</tr>
		</table>

		