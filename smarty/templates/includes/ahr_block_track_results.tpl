<!-- Start TrackResults -->
<div id="track-results-block" class="listings">
  <ul class="tabs"><!-- the tabs -->
    <li>
      <a href="#"><span class="b1">Todays&#39; Tracks</span>
      <span class="b1-border"></span></a>
    </li>
    <li>
      <a href="#"><span class="b2">Results</span>
      <span class="b2-border"></span></a>
    </li>
  </ul><!-- end tabs -->
  <div class="panes">
    <div class="pane1">
      <div class="items1">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										<tr class="tracks-cell"><td class="width50perc" colspan="4">Please login to your account to view all entries and results. <!--You may experience intermittent delays and/or wagering may become temporarily unavailable during that time.Currently our horse racing results and entries are unavailable, however full wagering is still online. Thank you for your patience as we upgrade the site.--></td></tr> </tbody></table>      </div><!-- End Items1 -->
      <div class="boxfooter"><span class="bold"><a href="tracks">View All Tracks</a></span></div>
    </div><!-- End Pane1 -->
    <div class="pane2">
      <div class="items2">
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
										
												  <thead>
													<tr class="tracks-titlebar">
													  <th class="width50perc">TRACKS</th>
													  <th><span title="Minutes To Post">MTP</span></th>
													  <th>RACE</th>
													  <th class="width67"></th>
													</tr>
										
												  </thead>
												  <tbody>
													<tr class="tracks-spacer">
													  <td colspan="4" ></td>
													</tr>        
										<tr class="tracks-cell"><td class="width50perc" colspan="4">Please login to your account to view all entries and results. <!--You may experience intermittent delays and/or wagering may become temporarily unavailable during that time.Currently our horse racing results and entries are unavailable, however full wagering is still online. Thank you for your patience as we upgrade the site.--></td></tr> </tbody></table>      </div><!-- End Items2 -->
      <div class="boxfooter"><span class="bold"><a href="tracks">View All Tracks</a></span></div>
    </div><!-- End Pane2 -->
  </div><!-- /.panes -->
</div><!-- end listings -->
<!-- End TrackResults -->
