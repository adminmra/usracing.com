{literal}<style type="text/css">
.rotw-details { display: block; padding:8px 0 0 0px; font-family: "Helvetica", Arial; }
.rotw-details a { background: none; display: none; }
.hide-this, .rotw-num, .rotw-grade, .rotw-purse, .rotw-distance, .rotw-age, .rotw-run, .rotw-quote   { display: none;}
.rotw-day-title, .rotw-day-title, .rotw-track-title   { display: none;}
.rotw-race { color: #fff373;  font-size: 13px; font-weight: bold; text-transform: uppercase; margin: 0 0 0 12px; }
.rotw-day { color: #fff8de; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:4px 0 0 12px; }
.rotw-track { color: black; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:1px 0 0 12px; }
.rotw-click { color: #ade07a; font-size: 12px; font-family: Arial, Helvetica, sans-serif; padding:2px 12px 0 0; text-transform: uppercase; }
</style>{/literal}

<a href="/beton/race-of-the-week" title="Big Race Of The Week"><img width="294" src="/themes/images/home/banner-right-rotw-general.jpg" border="0" />
</a>