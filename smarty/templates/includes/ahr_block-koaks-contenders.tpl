


<table width="100%" height="12"  cellpadding="0" cellspacing="0" class="table table-condensed table-striped table-bordered" id="infoEntries" title="Kentucky Oaks Result" summary="2015 Kentucky Oaks  Result">
                                
                                <tr >
								  <th width="5%">Result</th>
								<!--  <th width="5%">Time</th>  -->
								 <th width="5%">Post</th> 
								<!-- <th width="5%">Odds</th>  -->
								 <th width="14%">Horse</th>
                                  <th width="14%">Trainer</th>
                                   <th width="14%">Jockey</th> 
                                  <th width="20%">Owner</th>
                                </tr>
                                <tr>						   <td width="5%" align="center" >1</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >12</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Plum Pretty</td>
									<td width="14%"  ><a href='/trainer?name=Bob Baffert'>Bob Baffert</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Martin Garcia'>Martin Garcia</a></td>
                                   <td width="10%"  >Peachtree Stable</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >2</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >13</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >St. John\'s River</td>
									<td width="14%"  ><a href='/trainer?name=Andrew Leggio Jr.'>Andrew Leggio Jr.</a></td>
                                  <td width="14%"  >Rosie Napravnik</td>
                                   <td width="10%"  >Dede McGehee</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >3</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >6</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Zazu</td>
									<td width="14%"  ><a href='/trainer?name=John Sadler'>John Sadler</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Joel Rosario'>Joel Rosario</a></td>
                                   <td width="10%"  >Jerry & Ann Moss</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >4</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >1</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Joyful Victory</td>
									<td width="14%"  ><a href='/trainer?name=Larry Jones'>Larry Jones</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Mike Smith'>Mike Smith</a></td>
                                   <td width="10%"  >Fox Hill Farms (Rick Porter)</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >5</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >8</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Bouquet Booth</td>
									<td width="14%"  ><a href='/trainer?name=Steve Margolis'>Steve Margolis</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Robby Albarado'>Robby Albarado</a></td>
                                   <td width="10%"  >Right Time Racing Stable</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >6</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >4</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Kathmanblu</td>
									<td width="14%"  ><a href='/trainer?name=Kenny McPeek'>Kenny McPeek</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Julien Leparoux'>Julien Leparoux</a></td>
                                   <td width="10%"  >Five D Thoroughbreds & Wind River Stables</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >7</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >9</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   ><a href='/horse?name=Daisy Devine'>Daisy Devine</a></td>
									<td width="14%"  >Andrew McKeever</td>
                                  <td width="14%"  ><a href='/jockey?name=James Graham'>James Graham</a></td>
                                   <td width="10%"  >James Miller</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >8</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >10</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Street Storm</td>
									<td width="14%"  ><a href='/trainer?name=Steve Margolis'>Steve Margolis</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Shaun Bridgmohan'>Shaun Bridgmohan</a></td>
                                   <td width="10%"  >Right Time Racing LLC</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >9</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >5</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Suave Voir Faire</td>
									<td width="14%"  >Daniel M. Smithwick Jr</td>
                                  <td width="14%"  ><a href='/jockey?name=Miguel Mena'>Miguel Mena</a></td>
                                   <td width="10%"  >Fleur de Lis Stable LLC</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >10</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >3</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Summer Soiree</td>
									<td width="14%"  ><a href='/trainer?name=H. Graham Motion'>H. Graham Motion</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Gabriel Saez'>Gabriel Saez</a></td>
                                   <td width="10%"  >Team Valor International</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >11</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >7</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Her Smile</td>
									<td width="14%"  ><a href='/trainer?name=Todd Pletcher'>Todd Pletcher</a></td>
                                  <td width="14%"  ><a href='/jockey?name=Garrett Gomez'>Garrett Gomez</a></td>
                                   <td width="10%"  >Bobby Flay</td>
                           		</tr>
                                <tr  class="odd">						   <td width="5%" align="center" >12</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >2</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Lilacs and Lace</td>
									<td width="14%"  >John Terranova</td>
                                  <td width="14%"  ><a href='/jockey?name=Javier Castellano'>Javier Castellano</a></td>
                                   <td width="10%"  >James Covello, Judy Hicks and Kathryn Nikkel</td>
                           		</tr>
                                <tr>						   <td width="5%" align="center" >13</td>
								  <!--  <td width="5%"  align="center"></td>         -->                    
                               <td width="5%"  >11</td> 
								<!-- 		   <td width="5%"  ></td>  -->
                               	   <td width="14%"   >Holy Heavens</td>
									<td width="14%"  >Benard Chatters</td>
                                  <td width="14%"  ><a href='/jockey?name=Kent Desormeaux'>Kent Desormeaux</a></td>
                                   <td width="10%"  >Drs. K.K. and Vilasini Javaraman</td>
                           		</tr>
                                											                                
                              </table>
                              

