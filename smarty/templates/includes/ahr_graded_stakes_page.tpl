<div>
  <table class="graded-stakes-races">
    <caption>This Week's Graded Stakes Schedule</caption>
    <thead>
      <tr>
        <th>Date</th>
        <th>Graded Stakes</th>
      </tr>
    </thead>
    <tbody>
      <tr class=""><td class="addedon">Sep 17</td><td class="locations"><p><span class="bold">Belmont Park:</span>  Garden City Breeders&#039; Cup (G1), $250,000, 3yo f, 9f T</p>
<p><span class="bold">Charles Town:</span>  Charles Town Oaks, $400,000, 3yo f, 7f</p>
<p><span class="bold">Charles Town:</span>  Charles Town Juvenile S., $100,000, 2yo, 7f</p>
<p><span class="bold">Charles Town:</span>  Miss Shenandoah S., $100,000, 2yo f, 4.5f</p>
<p><span class="bold">Charles Town:</span>  Pink Ribbon S., $75,000, 3&amp;up, F&amp;M, 7f</p>
<p><span class="bold">Fairplex Park:</span>  Barretts Debutante, $110,000, 2yo f, 6.5f</p>
<p><span class="bold">Monmouth Park:</span>  Charles Hesse H., $100,000, 3&amp;up, 8.5f</p>
<p><span class="bold">Monmouth Park:</span>  Eleven North H., $100,000, 3&amp;up, F&amp;M, 6f</p>
<p><span class="bold">Monmouth Park:</span>  Jersey Girl H., $100,000, 3&amp;up, F&amp;M, 8.5f</p>
<p><span class="bold">Monmouth Park:</span>  New Jersey Breeders&#039; H., $100,000, 3&amp;up, 6f</p>
<p><span class="bold">Parx Racing:</span>  PTHA President&#039;s Cup, $250,000, 3&amp;up, 9f T</p>
<p><span class="bold">Woodbine:</span>  Summer S, (G3), $250,000, 2yo, 8f T</p>
<p><span class="bold">Woodbine:</span>  Natalma S, (G3), $200,000, 2yo f, 8f T</p>
<td></tr><tr class="AlternateRow"><td class="addedon">Sep 18</td><td class="locations"><p><span class="bold">Belmont Park:</span>  Noble Damsel H, (G3), $100,000, 3&amp;up, F&amp;M, 8f T</p>
<p><span class="bold">Fairplex Park:</span>  Barretts Juvenile, $110,000, 2yo, C&amp;G, 6.5f</p>
<p><span class="bold">Woodbine:</span>  Woodbine Mile (G1), $1,000,000, 3&amp;up, 8f T</p>
<p><span class="bold">Woodbine:</span>  Northern Dancer Breeders&#039; Cup Turf (G1), $750,000, 3&amp;up, 12f T</p>
<p><span class="bold">Woodbine:</span>  Canadian S, (G2), $300,000, 3&amp;up, F&amp;M, 9f T</p>
<td></tr><tr class=""><td class="addedon">Sep 21</td><td class="locations"><p><span class="bold">Woodbine:</span>  King Corrie S., $100,000, 3&amp;up, 6f</p>
<td></tr><tr class="AlternateRow"><td class="addedon">Sep 24</td><td class="locations"><p><span class="bold">Belmont Park:</span>  Gallant Bloom H, (G2), $150,000, 3&amp;up, F&amp;M, 6.5f</p>
<p><span class="bold">Belmont Park:</span>  Brave Raj Breeders&#039; Cup S., $85,000, 2yo f, 8.32f</p>
<p><span class="bold">Belmont Park:</span>  Foolish Pleasure Breeders&#039; Cup S., $85,000, 2yo, 8.32f</p>
<p><span class="bold">Delaware Park:</span>  Kent Breeders&#039; Cup S, (G3), $200,000, $50,000 Breeders&#039; Cup Fund, 3yo, 9f T</p>
<p><span class="bold">Hastings Racecourse:</span>  British Columbia Breeders&#039; Cup Oaks, $100,000, 3yo f, 9f</p>
<p><span class="bold">Hastings Racecourse:</span>  Fantasy S., $75,000, 2yo f, 8.5f</p>
<p><span class="bold">Louisiana Downs:</span>  Super Derby (G2), $500,000, 3yo, 9f</p>
<p><span class="bold">Louisiana Downs:</span>  Louisiana Stallion S., $100,000, 2yo fd, 7f</p>
<p><span class="bold">Louisiana Downs:</span>  Louisiana Stallion S., $100,000, 2yo, C&amp;Gd, 7f</p>
<p><span class="bold">Louisiana Downs:</span>  Sunday Silence Breeders&#039; Cup, $100,000, 2yo, 8.5f T</p>
<p><span class="bold">Louisiana Downs:</span>  Happy Ticket Breeders&#039; Cup, $75,000, 2yo f, 8.5f T</p>
<p><span class="bold">Monmouth Park:</span>  Helen Haskell Sampson S, (NSA-I), $100,000, 4&amp;up, 20f T</p>
<p><span class="bold">Parx Racing:</span>  Pennsylvania Derby (G2), $1,000,000, 3yo, 9f</p>
<p><span class="bold">Parx Racing:</span>  Turf Amazon H., $200,000, 3&amp;up, F&amp;M, 5f T</p>
<p><span class="bold">Presque Isle Downs:</span>  Fitz Dixon Jr. Memorial Juvenile S., $100,000, 2yo, 6.5f</p>
<p><span class="bold">Presque Isle Downs:</span>  H.B.P.A. Stakes, $100,000, 3&amp;up, F&amp;M, 8.32f</p>
<p><span class="bold">Presque Isle Downs:</span>  Presque Isle Debutante, $100,000, 2yo f, 6f</p>
<p><span class="bold">Retama Park:</span>  Darby&#039;s Daughter Texas Stallion S., $100,000, 2yo f, 6f</p>
<p><span class="bold">Retama Park:</span>  My Dandy Texas Stallion S., $100,000, 2yo, C&amp;G, 6f</p>
<p><span class="bold">Woodbine:</span>  Ontario Derby, $150,000, 3yo, 9f</p>
<td></tr>    </tbody>
  </table>
</div>
