{literal}<style>
.not-front #col3 #content-box { padding: 0; }
.not-front #col3 #content-wrapper { border: none; padding: 0 }
.not-front #col3 #content-wrapper  h2.title { display: none; }
.not-front #col3 .box-shd { display: none; }
</style>{/literal}

<div class="block">
<h2 class="title-custom">Preakness Stakes Past Winners</h2>
<div>    
  <table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Kentucky Derby Winners" summary="The past winners of the Kentucky Derby" >
      <tbody>
	  <tr>
        <th width="46" >Year</th>
        <th width="105"  >Winner</th>
        <th width="139" >Jockey</th>
        <th width="150" >Trainer</th>
        <th width="52" >Time</th>
        </tr>
       <tr>    <td >2011</td>
    <td><a href="/horse?name=Shackleford" title="Shackleford">Shackleford</a></td>
    <td>Jesus Castanon</td>
    <td><a href="/trainer?name=Dale_Romans" title="Dale Romans">Dale Romans</a></td>
    <td>1:56.47</td>
    </tr>
	<tr class="odd" >    <td >2010</td>
    <td><a href="/horse?name=Lookin_at_Lucky" title="Lookin at Lucky">Lookin at Lucky</a></td>
    <td><a href="/jockey?name=Martin_Garcia" title="Martin Garcia">Martin Garcia</a></td>
    <td><a href="/trainer?name=Bob_Baffert" title="Bob Baffert">Bob Baffert</a></td>
    <td>1:55.47</td>
    </tr>
	<tr>    <td >2009</td>
    <td><a href="/horse?name=Rachel_Alexandra" title="Rachel Alexandra">Rachel Alexandra</a></td>
    <td><a href="/jockey?name=Calvin_Borel" title="Calvin Borel">Calvin Borel</a></td>
    <td><a href="/trainer?name=Steve_Asmussen" title="Steve Asmussen">Steve Asmussen</a></td>
    <td>1:55.08</td>
    </tr>
	<tr class="odd" >    <td >2008</td>
    <td><a href="/horse?name=Big_Brown" title="Big Brown">Big Brown</a></td>
    <td><a href="/jockey?name=Kent_Desormeaux" title="Kent Desormeaux">Kent Desormeaux</a></td>
    <td>Richard Dutrow, Jr.</td>
    <td>1:54.80</td>
    </tr>
	<tr>    <td >2007</td>
    <td><a href="/horse?name=Curlin" title="Curlin">Curlin</a></td>
    <td><a href="/jockey?name=Robby_Albarado" title="Robby Albarado">Robby Albarado</a></td>
    <td><a href="/trainer?name=Steve_Asmussen" title="Steve Asmussen">Steve Asmussen</a></td>
    <td>1:53 2/5</td>
    </tr>
	<tr class="odd" >    <td >2006</td>
    <td>Bernadini</td>
    <td>Tom Albertrani</td>
    <td>Javier Castellano</td>
    <td>1:54.65</td>
    </tr>
	<tr>    <td >2005</td>
    <td><a href="/horse?name=Afleet_Alex" title="Afleet Alex">Afleet Alex</a></td>
    <td><a href="/jockey?name=Jeremy_Rose" title="Jeremy Rose">Jeremy Rose</a></td>
    <td>Tim Ritchey</td>
    <td>1:55.04</td>
    </tr>
	<tr class="odd" >    <td >2004</td>
    <td><a href="/horse?name=Smarty_Jones" title="Smarty Jones">Smarty Jones</a></td>
    <td><a href="/jockey?name=Stewart_Elliott" title="Stewart Elliott">Stewart Elliott</a></td>
    <td><a href="/trainer?name=John_Servis" title="John Servis">John Servis</a></td>
    <td>1:55.59</td>
    </tr>
	<tr>    <td >2003</td>
    <td><a href="/horse?name=Funny_Cide" title="Funny Cide">Funny Cide</a></td>
    <td><a href="/jockey?name=Jose_Santos" title="Jose Santos">Jose Santos</a></td>
    <td><a href="/trainer?name=Barclay_Tagg" title="Barclay Tagg">Barclay Tagg</a></td>
    <td>1:55.61</td>
    </tr>
	<tr class="odd" >    <td >2002</td>
    <td><a href="/horse?name=War_Emblem" title="War Emblem">War Emblem</a></td>
    <td><a href="/jockey?name=Victor_Espinoza" title="Victor Espinoza">Victor Espinoza</a></td>
    <td><a href="/trainer?name=Bob_Baffert" title="Bob Baffert">Bob Baffert</a></td>
    <td>1:56 2/5</td>
    </tr>
	<tr>    <td >2001</td>
    <td><a href="/horse?name=Point_Given" title="Point Given">Point Given</a></td>
    <td><a href="/jockey?name=Gary_Stevens" title="Gary Stevens">Gary Stevens</a></td>
    <td><a href="/trainer?name=Bob_Baffert" title="Bob Baffert">Bob Baffert</a></td>
    <td>1:55 2/5</td>
    </tr>
	<tr class="odd" >    <td >2000</td>
    <td>Red Bullet</td>
    <td><a href="/jockey?name=Jerry_Bailey" title="Jerry Bailey">Jerry Bailey</a></td>
    <td>Joe Orseno</td>
    <td>1:56</td>
    </tr>
	<tr>    <td >1999</td>
    <td><a href="/horse?name=Charismatic" title="Charismatic">Charismatic</a></td>
    <td>Chris Antley</td>
    <td><a href="/trainer?name=D._Wayne_Lukas" title="D. Wayne Lukas">D. Wayne Lukas</a></td>
    <td>1:55 1/5</td>
    </tr>
	<tr class="odd" >    <td >1998</td>
    <td><a href="/horse?name=Real_Quiet" title="Real Quiet">Real Quiet</a></td>
    <td><a href="/jockey?name=Kent_Desormeaux" title="Kent Desormeaux">Kent Desormeaux</a></td>
    <td><a href="/trainer?name=Bob_Baffert" title="Bob Baffert">Bob Baffert</a></td>
    <td>1:54 4/5</td>
    </tr>
	<tr>    <td >1997</td>
    <td><a href="/horse?name=Silver_Charm" title="Silver Charm">Silver Charm</a></td>
    <td><a href="/jockey?name=Gary_Stevens" title="Gary Stevens">Gary Stevens</a></td>
    <td><a href="/trainer?name=Bob_Baffert" title="Bob Baffert">Bob Baffert</a></td>
    <td>1:54 2/5</td>
    </tr>
	<tr class="odd" >    <td >1996</td>
    <td>Louis Quatorze</td>
    <td><a href="/jockey?name=Pat_Day" title="Pat Day">Pat Day</a></td>
    <td>Nick Zito</td>
    <td>1:53 2/5</td>
    </tr>
	<tr>    <td >1995</td>
    <td><a href="/horse?name=Timber_Country" title="Timber Country">Timber Country</a></td>
    <td><a href="/jockey?name=Pat_Day" title="Pat Day">Pat Day</a></td>
    <td><a href="/trainer?name=D._Wayne_Lukas" title="D. Wayne Lukas">D. Wayne Lukas</a></td>
    <td>1:54 2/5</td>
    </tr>
	<tr class="odd" >    <td >1994</td>
    <td><a href="/horse?name=Tabasco_Cat" title="Tabasco Cat">Tabasco Cat</a></td>
    <td><a href="/jockey?name=Pat_Day" title="Pat Day">Pat Day</a></td>
    <td><a href="/trainer?name=D._Wayne_Lukas" title="D. Wayne Lukas">D. Wayne Lukas</a></td>
    <td>1:56 2/5</td>
    </tr>
	<tr>    <td >1993</td>
    <td>Prairie Bayou</td>
    <td><a href="/jockey?name=Mike_Smith" title="Mike Smith">Mike Smith</a></td>
    <td>Tom Bohannan</td>
    <td>1:56 3/5</td>
    </tr>
	<tr class="odd" >    <td >1992</td>
    <td>Pine Bluff</td>
    <td><a href="/jockey?name=Chris_McCarron" title="Chris McCarron">Chris McCarron</a></td>
    <td>Tom Bohannan</td>
    <td>1:55 3/5</td>
    </tr>
	<tr>    <td >1991</td>
    <td>Hansel</td>
    <td><a href="/jockey?name=Jerry_Bailey" title="Jerry Bailey">Jerry Bailey</a></td>
    <td><a href="/trainer?name=Frank_Brothers" title="Frank Brothers">Frank Brothers</a></td>
    <td>1:54</td>
    </tr>
	<tr class="odd" >    <td >1990</td>
    <td>Summer Squall</td>
    <td><a href="/jockey?name=Pat_Day" title="Pat Day">Pat Day</a></td>
    <td>Neil Howard</td>
    <td>1:53 3/5</td>
    </tr>
	<tr>    <td >1989</td>
    <td><a href="/horse?name=Sunday_Silence" title="Sunday Silence">Sunday Silence</a></td>
    <td>Pat Valenzuela</td>
    <td>Charlie Whittingham</td>
    <td>1:53 4/5</td>
    </tr>
	<tr class="odd" >    <td >1988</td>
    <td>Risen Star</td>
    <td><a href="/jockey?name=Eddie_Delahoussaye" title="Eddie Delahoussaye">Eddie Delahoussaye</a></td>
    <td>Louie Roussel III</td>
    <td>1:56 1/5</td>
    </tr>
	<tr>    <td >1987</td>
    <td><a href="/horse?name=Alysheba" title="Alysheba">Alysheba</a></td>
    <td><a href="/jockey?name=Chris_McCarron" title="Chris McCarron">Chris McCarron</a></td>
    <td><a href="/trainer?name=Jack_Van_Berg" title="Jack Van Berg">Jack Van Berg</a></td>
    <td>1:55 4/5</td>
    </tr>
	<tr class="odd" >    <td >1986</td>
    <td>Snow Chief</td>
    <td><a href="/jockey?name=Alex_Solis" title="Alex Solis">Alex Solis</a></td>
    <td>Melvin Stute</td>
    <td>1:54 4/5</td>
    </tr>
	<tr>    <td >1984</td>
    <td>Gate Dancer</td>
    <td>Angel Cordero Jr.</td>
    <td><a href="/trainer?name=Jack_Van_Berg" title="Jack Van Berg">Jack Van Berg</a></td>
    <td>1:53 3/5</td>
    </tr>
	<tr class="odd" >    <td >1983</td>
    <td>Deputed Testamony</td>
    <td>Donald Miller Jr.</td>
    <td>Bill Boniface</td>
    <td>1:55 2/5</td>
    </tr>
	<tr>    <td >1981</td>
    <td><a href="/horse?name=Pleasant_Colony" title="Pleasant Colony">Pleasant Colony</a></td>
    <td>Jorge Velasquez</td>
    <td>John Campo</td>
    <td>1:54 3/5</td>
    </tr>
	<tr class="odd" >    <td >1980</td>
    <td>Codex</td>
    <td>Angel Cordero Jr.</td>
    <td><a href="/trainer?name=D._Wayne_Lukas" title="D. Wayne Lukas">D. Wayne Lukas</a></td>
    <td>1:54 1/5</td>
    </tr>
	<tr>    <td >1979</td>
    <td><a href="/horse?name=Spectacular_Bid" title="Spectacular Bid">Spectacular Bid</a></td>
    <td>Ron Franklin</td>
    <td>Bud Delp</td>
    <td>1:54 1/5</td>
    </tr>
	<tr class="odd" >    <td >1978</td>
    <td><a href="/horse?name=Affirmed" title="Affirmed">Affirmed</a></td>
    <td>Steve Cauthen</td>
    <td>Laz Barrera</td>
    <td>1:54 2/5</td>
    </tr>
	<tr>    <td >1977</td>
    <td><a href="/horse?name=Seattle_Slew" title="Seattle Slew">Seattle Slew</a></td>
    <td>Jean Cruguet</td>
    <td>Billy Turner</td>
    <td>1:54 2/5</td>
    </tr>
	<tr class="odd" >    <td >1976</td>
    <td>Elocutionist</td>
    <td>John Lively</td>
    <td>Paul Adwell</td>
    <td>1:55</td>
    </tr>
	<tr>    <td >1975</td>
    <td>Master Derby</td>
    <td>Darrel McHargue</td>
    <td>Smiley Adams</td>
    <td>1:56 2/5</td>
    </tr>
	<tr class="odd" >    <td >1974</td>
    <td>Little Current</td>
    <td>Miguel Rivera</td>
    <td>Lou Rondinello</td>
    <td>1:54 3/5</td>
    </tr>
	<tr>    <td >1973</td>
    <td><a href="/horse?name=Secretariat" title="Secretariat">Secretariat</a></td>
    <td>Ron Turcotte</td>
    <td>Lucien Laurin</td>
    <td>1:54.40</td>
    </tr>
	<tr class="odd" >    <td >1972</td>
    <td>Bee Bee Bee</td>
    <td>Eldon Nelson</td>
    <td>Red Carroll</td>
    <td>1:55 3/5</td>
    </tr>
	<tr>    <td >1971</td>
    <td><a href="/horse?name=Canonero_II" title="Canonero II">Canonero II</a></td>
    <td>Gustavo Avila</td>
    <td>Juan Arias</td>
    <td>1:54</td>
    </tr>
	<tr class="odd" >    <td >1970</td>
    <td>Personality</td>
    <td>Eddie Belmonte</td>
    <td>John Jacobs</td>
    <td>1:56 1/5</td>
    </tr>
	<tr>    <td >1969</td>
    <td><a href="/horse?name=Majestic_Prince" title="Majestic Prince">Majestic Prince</a></td>
    <td>Bill Hartack</td>
    <td>Johnny Longden</td>
    <td>1:55 3/5</td>
    </tr>
	<tr class="odd" >    <td >1968</td>
    <td><a href="/horse?name=Forward_Pass" title="Forward Pass">Forward Pass</a></td>
    <td>Ismael Valenzuela</td>
    <td>Henry Forrest</td>
    <td>1:56.4/5</td>
    </tr>
	<tr>    <td >1967</td>
    <td><a href="/horse?name=Damascus" title="Damascus">Damascus</a></td>
    <td>Bill Shoemaker</td>
    <td>Frank Whiteley</td>
    <td>1:55 1/5</td>
    </tr>
	<tr class="odd" >    <td >1966</td>
    <td><a href="/horse?name=Kauai_King" title="Kauai King">Kauai King</a></td>
    <td>Don Brumfield</td>
    <td>Henry Forrest</td>
    <td>1:55 2/5</td>
    </tr>
	<tr>    <td >1965</td>
    <td>Tom Rolfe</td>
    <td>Ron Turcotte</td>
    <td>Frank Whiteley</td>
    <td>1:56 1/5</td>
    </tr>
	<tr class="odd" >    <td >1964</td>
    <td>Northern Dancer</td>
    <td>Bill Hartack</td>
    <td>Horatio Luro</td>
    <td>1:56 4/5</td>
    </tr>
	<tr>    <td >1963</td>
    <td>Candy Spots</td>
    <td>Bill Shoemaker</td>
    <td>Mesh Tenney</td>
    <td>1:56 1/5</td>
    </tr>
	<tr class="odd" >    <td >1962</td>
    <td>Greek Money</td>
    <td>John Rotz</td>
    <td>V. W. Raines</td>
    <td>1:56 1/5</td>
    </tr>
	<tr>    <td >1961</td>
    <td><a href="/horse?name=Carry_Back" title="Carry Back">Carry Back</a></td>
    <td>John Sellers</td>
    <td>Jack Price</td>
    <td>1:57 3/5</td>
    </tr>
	<tr class="odd" >    <td >1960</td>
    <td>Bally Ache</td>
    <td>Bobby Ussery</td>
    <td>Jimmy Pitt</td>
    <td>1:57 3/5</td>
    </tr>
	<tr>    <td >1959</td>
    <td>Royal Orbit</td>
    <td>William Harmatz</td>
    <td>R. Cornell</td>
    <td>1:57</td>
    </tr>
	<tr class="odd" >    <td >1958</td>
    <td><a href="/horse?name=Tim_Tam" title="Tim Tam">Tim Tam</a></td>
    <td>Ismael Valenzuela</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>1:57 1/5</td>
    </tr>
	<tr>    <td >1957</td>
    <td>Bold Ruler</td>
    <td>Eddie Arcaro</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>1:56 1/5</td>
    </tr>
	<tr class="odd" >    <td >1956</td>
    <td>Fabius</td>
    <td>Bill Hartack</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>1:58 2/5</td>
    </tr>
	<tr>    <td >1955</td>
    <td>Nashua</td>
    <td>Eddie Arcaro</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>1:54 3/5</td>
    </tr>
	<tr class="odd" >    <td >1954</td>
    <td>Hasty Road</td>
    <td>Johnny Adams</td>
    <td>Harry Trotsek</td>
    <td>1:57.40</td>
    </tr>
	<tr>    <td >1953</td>
    <td><a href="/horse?name=Native_Dancer" title="Native Dancer">Native Dancer</a></td>
    <td>Eric Guerin</td>
    <td>Bill Winfrey</td>
    <td>1:57 4/5</td>
    </tr>
	<tr class="odd" >    <td >1952</td>
    <td>Blue Man</td>
    <td>Conn McCreary</td>
    <td>Woody Stephens</td>
    <td>1:57 2/5</td>
    </tr>
	<tr>    <td >1951</td>
    <td><a href="/horse?name=Bold" title="Bold">Bold</a></td>
    <td>Eddie Arcaro</td>
    <td>Preston Burch</td>
    <td>1:56 2/5</td>
    </tr>
	<tr class="odd" >    <td >1950</td>
    <td>Hill Prince</td>
    <td>Eddie Arcaro</td>
    <td>Casey Hayes</td>
    <td>1:59 1/5</td>
    </tr>
	<tr>    <td >1949</td>
    <td><a href="/horse?name=Capot" title="Capot">Capot</a></td>
    <td><a href="/jockey?name=Ted_Atkinson" title="Ted Atkinson">Ted Atkinson</a></td>
    <td>John M. Gaver</td>
    <td>1:56</td>
    </tr>
	<tr class="odd" >    <td >1948</td>
    <td><a href="/horse?name=Citation" title="Citation">Citation</a></td>
    <td>Eddie Arcaro</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>2:02 2/5</td>
    </tr>
	<tr>    <td >1947</td>
    <td>Faultless</td>
    <td>Doug Dodson</td>
    <td>Horace A. "Jimmy" Jones</td>
    <td>1:59</td>
    </tr>
	<tr class="odd" >    <td >1946</td>
    <td><a href="/horse?name=Assault" title="Assault">Assault</a></td>
    <td>Warren Mehrtens</td>
    <td>Max Hirsch</td>
    <td>2:01 2/5</td>
    </tr>
	<tr>    <td >1945</td>
    <td>Polynesian</td>
    <td>W. D. Wright</td>
    <td>Morris Dixon</td>
    <td>1:58 4/5</td>
    </tr>
	<tr class="odd" >    <td >1944</td>
    <td><a href="/horse?name=Pensive" title="Pensive">Pensive</a></td>
    <td>Conn McCreary</td>
    <td>Ben A. Jones</td>
    <td>1:59 1/5</td>
    </tr>
	<tr>    <td >1943</td>
    <td><a href="/horse?name=Count_Fleet" title="Count Fleet">Count Fleet</a></td>
    <td>Johnny Longden</td>
    <td>Don Cameron</td>
    <td>1:57 2/5</td>
    </tr>
	<tr class="odd" >    <td >1942</td>
    <td>Alsab</td>
    <td>Basil James</td>
    <td>Sarge Swenke</td>
    <td>1:57</td>
    </tr>
	<tr>    <td >1941</td>
    <td><a href="/horse?name=Whirlaway" title="Whirlaway">Whirlaway</a></td>
    <td>Eddie Arcaro</td>
    <td>Ben A. Jones</td>
    <td>1:58 4/5</td>
    </tr>
	<tr class="odd" >    <td >1940</td>
    <td>Bimelech</td>
    <td>F. A. Smith</td>
    <td>Bill Hurley</td>
    <td>1:58 3/5</td>
    </tr>
	<tr>    <td >1939</td>
    <td>Challedon</td>
    <td>George Seabo</td>
    <td>Louis Schaefer</td>
    <td>1:59 4/5</td>
    </tr>
	<tr class="odd" >    <td >1938</td>
    <td>Dauber</td>
    <td>Maurice Peters</td>
    <td>Dick Handlen</td>
    <td>1:59 4/5</td>
    </tr>
	<tr>    <td >1937</td>
    <td><a href="/horse?name=War_Admiral" title="War Admiral">War Admiral</a></td>
    <td>Charley Kurtsinger</td>
    <td>George Conway</td>
    <td>1:58 2/5</td>
    </tr>
	<tr class="odd" >    <td >1936</td>
    <td><a href="/horse?name=Bold_Venture" title="Bold Venture">Bold Venture</a></td>
    <td>George Woolf</td>
    <td>Max Hirsch</td>
    <td>1:59</td>
    </tr>
	<tr>    <td >1935</td>
    <td><a href="/horse?name=Omaha" title="Omaha">Omaha</a></td>
    <td>Willie Saunders</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>1:58 2/5</td>
    </tr>
	<tr class="odd" >    <td >1934</td>
    <td>High Quest</td>
    <td>Robert Jones</td>
    <td>Bob Smith</td>
    <td>1:58 1/5</td>
    </tr>
	<tr>    <td >1933</td>
    <td>Head Play</td>
    <td>Charley Kurtsinger</td>
    <td>Thomas Hayes</td>
    <td>2:02</td>
    </tr>
	<tr class="odd" >    <td >1932</td>
    <td><a href="/horse?name=Burgoo_King" title="Burgoo King">Burgoo King</a></td>
    <td>Eugene James</td>
    <td>Henry J. "Dick" Thompson</td>
    <td>1:59 4/5</td>
    </tr>
	<tr>    <td >1931</td>
    <td><a href="/horse?name=Mate" title="Mate">Mate</a></td>
    <td>George Ellis</td>
    <td>James W. Healy</td>
    <td>1:59</td>
    </tr>
	<tr class="odd" >    <td >1930</td>
    <td><a href="/horse?name=Gallant_Fox" title="Gallant Fox">Gallant Fox</a></td>
    <td>Earl Sande</td>
    <td>Sunny Jim Fitzsimmons</td>
    <td>2:00 3/5</td>
    </tr>
	<tr>    <td >1929</td>
    <td>Dr. Freeland</td>
    <td>Louis Schaefer</td>
    <td>Thomas J. Healey</td>
    <td>2:01 3/5</td>
    </tr>
	<tr class="odd" >    <td >1928</td>
    <td>Victorian</td>
    <td>Sonny Workman</td>
    <td>James Rowe, Jr.</td>
    <td>2:00 1/5</td>
    </tr>
	<tr>    <td >1927</td>
    <td>Bostonian</td>
    <td>Whitey Abel</td>
    <td>Fred Hopkins</td>
    <td>2:01 3/5</td>
    </tr>
	<tr class="odd" >    <td >1926</td>
    <td>Display</td>
    <td>John Maiben</td>
    <td>Thomas J. Healey</td>
    <td>1:59 4/5</td>
    </tr>
	<tr>    <td >1925</td>
    <td>Coventry</td>
    <td>Clarence Kummer</td>
    <td>William Duke</td>
    <td>1:59</td>
    </tr>
	<tr class="odd" >    <td >1924</td>
    <td>Nellie Morse</td>
    <td>John Merimee</td>
    <td>A. B. Gordon</td>
    <td>1:57 1/5</td>
    </tr>
	<tr>    <td >1923</td>
    <td>Vigil</td>
    <td>Benny Marinelli</td>
    <td>Thomas J. Healey</td>
    <td>1:53 3/5</td>
    </tr>
	<tr class="odd" >    <td >1922</td>
    <td>Pillory</td>
    <td>L. Morris</td>
    <td>Thomas J. Healey</td>
    <td>1:51 3/5</td>
    </tr>
	<tr>    <td >1921</td>
    <td>Broomspun</td>
    <td>Frank Coltiletti</td>
    <td>James Rowe, Sr.</td>
    <td>1:54 1/5</td>
    </tr>
	<tr class="odd" >    <td >1919</td>
    <td><a href="/horse?name=Sir_Barton" title="Sir Barton">Sir Barton</a></td>
    <td>Johnny Loftus</td>
    <td>H. Guy Bedwell</td>
    <td>1:53</td>
    </tr>
	<tr>    <td >1918</td>
    <td>Jack Hare Jr.</td>
    <td>Charles Peak</td>
    <td>F. D. Weir</td>
    <td>1:53 2/5</td>
    </tr>
	<tr class="odd" >    <td >1918</td>
    <td>War Cloud</td>
    <td>Johnny Loftus</td>
    <td>W. B. Jennings</td>
    <td>1:53 3/5</td>
    </tr>
	<tr>    <td >1917</td>
    <td>Kalitan</td>
    <td>E. Haynes</td>
    <td>Bill Hurley</td>
    <td>1:54 2/5</td>
    </tr>
	<tr class="odd" >    <td >1916</td>
    <td>Damrosch</td>
    <td>Linus McAtee</td>
    <td>A. G. Weston</td>
    <td>1:54 4/5</td>
    </tr>
	<tr>    <td >1915</td>
    <td>Rhine Maiden</td>
    <td>Douglas Hoffman</td>
    <td>F. Devers</td>
    <td>1:58</td>
    </tr>
	<tr class="odd" >    <td >1914</td>
    <td>Holiday</td>
    <td>Andy Schuttinger</td>
    <td>J. S. Healy</td>
    <td>1:53 4/5</td>
    </tr>
	<tr>    <td >1913</td>
    <td>Buskin</td>
    <td>James Butwell</td>
    <td>John Whalen</td>
    <td>1:53 2/5</td>
    </tr>
	<tr class="odd" >    <td >1912</td>
    <td>Colonel Holloway</td>
    <td>Clarence Turner</td>
    <td>D. Woodford</td>
    <td>1:56 3/5</td>
    </tr>
	<tr>    <td >1911</td>
    <td>Watervale</td>
    <td>Eddie Dugan</td>
    <td>John Whalen</td>
    <td>1:51</td>
    </tr>
	<tr class="odd" >    <td >1910</td>
    <td>Layminster</td>
    <td>Roy Estep</td>
    <td>J. S. Healy</td>
    <td>1:40 3/5</td>
    </tr>
	<tr>    <td >1909</td>
    <td>Effendi</td>
    <td>Willie Doyle</td>
    <td>F. C. Frisbie</td>
    <td>1:39 4/5</td>
    </tr>
	<tr class="odd" >    <td >1908</td>
    <td>Royal Tourist</td>
    <td>Eddie Dugan</td>
    <td>Andrew J. Joyner</td>
    <td>1:46 2/5</td>
    </tr>
	<tr>    <td >1907</td>
    <td>Don Enrique</td>
    <td>G. Mountain</td>
    <td>John Whalen</td>
    <td>1:45 2/5</td>
    </tr>
	<tr class="odd" >    <td >1906</td>
    <td>Whimsical</td>
    <td>Walter Miller</td>
    <td>T. J. Gaynor</td>
    <td>1:45</td>
    </tr>
	<tr>    <td >1905</td>
    <td>Cairngorm</td>
    <td>W. Davis</td>
    <td>Andrew J. Joyner</td>
    <td>1:45 4/5</td>
    </tr>
	<tr class="odd" >    <td >1904</td>
    <td>Bryn Mawr</td>
    <td>E. Hildebrand</td>
    <td>W. F. Presgrave</td>
    <td>1:44 1/5</td>
    </tr>
	<tr>    <td >1903</td>
    <td>Flocarline</td>
    <td>W. Gannon</td>
    <td>H. C. Riddle</td>
    <td>1:44 4/5</td>
    </tr>
	<tr class="odd" >    <td >1902</td>
    <td>Old England</td>
    <td>L. Jackson</td>
    <td>G. B. Morris</td>
    <td>1:45 4/5</td>
    </tr>
	<tr>    <td >1901</td>
    <td>The Parader</td>
    <td>F. Landry</td>
    <td>Thomas J. Healey</td>
    <td>1:47 1/5</td>
    </tr>
	<tr class="odd" >    <td >1900</td>
    <td>Hindus</td>
    <td>H. Spencer</td>
    <td>J. H. Morris</td>
    <td>1:48 2/5</td>
    </tr>
	<tr>    <td >1899</td>
    <td>Half Time</td>
    <td>R. Clawson</td>
    <td>Frank McCabe</td>
    <td>1:47</td>
    </tr>
	<tr class="odd" >    <td >1898</td>
    <td>Sly Fox</td>
    <td>Willie Simms</td>
    <td>Hardy Campbell</td>
    <td>1:49 3/4</td>
    </tr>
	<tr>    <td >1897</td>
    <td>Paul Kauvar</td>
    <td>T. Thorpe</td>
    <td>T. P. Hayes</td>
    <td>1:51 1/4</td>
    </tr>
	<tr class="odd" >    <td >1896</td>
    <td>Margrave</td>
    <td>Henry Griffin</td>
    <td>Byron McClelland</td>
    <td>1:51</td>
    </tr>
	<tr>    <td >1895</td>
    <td>Belmar</td>
    <td>Fred Taral</td>
    <td>E. Feakes</td>
    <td>1:50 1/2</td>
    </tr>
	<tr class="odd" >    <td >1894</td>
    <td>Assignee</td>
    <td>Fred Taral</td>
    <td>W. Lakeland</td>
    <td>1:49 1/4</td>
    </tr>
	<tr>    <td >1890</td>
    <td>Montague</td>
    <td>W. Martin</td>
    <td>E. Feakes</td>
    <td>2:36 3/4</td>
    </tr>
	<tr class="odd" >    <td >1889</td>
    <td>Buddhist</td>
    <td>George B. Anderson</td>
    <td>J. Rogers</td>
    <td>2:17 1/2</td>
    </tr>
	<tr>    <td >1888</td>
    <td><a href="/horse?name=Refund" title="Refund">Refund</a></td>
    <td>Fred Littlefield</td>
    <td>Robert W. Walden</td>
    <td>2:49</td>
    </tr>
	<tr class="odd" >    <td >1887</td>
    <td>Dunboyne (horse)</td>
    <td>William Donohue</td>
    <td>W. Jennings</td>
    <td>2:39 1/2</td>
    </tr>
	<tr>    <td >1886</td>
    <td>The Bard</td>
    <td>S. Fisher</td>
    <td>J. Huggins</td>
    <td>2:45</td>
    </tr>
	<tr class="odd" >    <td >1885</td>
    <td>Tecumseh</td>
    <td>Jim McLaughlin</td>
    <td>Charles Littlefield</td>
    <td>2:49</td>
    </tr>
	<tr>    <td >1884</td>
    <td>Knight of Ellerslie</td>
    <td>S. Fisher</td>
    <td>T. B. Doswell</td>
    <td>2:39 1/2</td>
    </tr>
	<tr class="odd" >    <td >1883</td>
    <td>Jacobus</td>
    <td>George Barbee</td>
    <td>R. Dwyer</td>
    <td>2:42 1/2</td>
    </tr>
	<tr>    <td >1882</td>
    <td>Vanguard</td>
    <td>T. Costello</td>
    <td>Robert W. Walden</td>
    <td>2:44 1/2</td>
    </tr>
	<tr class="odd" >    <td >1881</td>
    <td>Saunterer</td>
    <td>T. Costello</td>
    <td>Robert W. Walden</td>
    <td>2:40 1/2</td>
    </tr>
	<tr>    <td >1880</td>
    <td>Grenada</td>
    <td>Lloyd Hughes</td>
    <td>Robert W. Walden</td>
    <td>2:40 1/2</td>
    </tr>
	<tr class="odd" >    <td >1879</td>
    <td>Harold</td>
    <td>Lloyd Hughes</td>
    <td>Robert W. Walden</td>
    <td>2:40 1/2</td>
    </tr>
	<tr>    <td >1878</td>
    <td>Duke of Magenta</td>
    <td>C. Holloway</td>
    <td>Robert W. Walden</td>
    <td>2:41 3/4</td>
    </tr>
	<tr class="odd" >    <td >1877</td>
    <td>Cloverbrook</td>
    <td>C. Holloway</td>
    <td>J. Walden</td>
    <td>2:45 1/2</td>
    </tr>
	<tr>    <td >1876</td>
    <td>Shirley</td>
    <td>George Barbee</td>
    <td>W. Brown</td>
    <td>2:44.75</td>
    </tr>
	<tr class="odd" >    <td >1875</td>
    <td>Tom Ochiltree</td>
    <td>Lloyd Hughes</td>
    <td>Robert W. Walden</td>
    <td>2:43 1/2</td>
    </tr>
	<tr>    <td >1874</td>
    <td>Culpepper</td>
    <td>William Donohue</td>
    <td>H. Gaffney</td>
    <td>2:56 1/2</td>
    </tr>
	<tr class="odd" >    <td >1873</td>
    <td>Survivor</td>
    <td>George Barbee</td>
    <td>A. D. Pryor</td>
    <td>2:43</td>
    </tr>
	  </tbody>
</table>
 </div>
						  </div>
<div id="box-shd"></div><!-- === SHADOW === -->						