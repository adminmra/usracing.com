<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
<tbody>
<tr>
<th width="10%"><strong>Date</strong></th>
<th width="12%"><strong>Stakes</strong></th>
<th width="12%"><strong>Track</strong></th>
<th width="10%"><strong>Grade</strong></th>
<th width="10%"><strong>Purses</strong></th>
<th width="10%"><strong>Age/Sex</strong></th>
<th width="10%"><strong>DS</strong></th>
<th width="12%"><strong>Horse</strong></th>
<th width="12%"><strong>Jockey</strong></th>
<th width="12%"><strong>Trainer</strong></th>
</tr>
<tr>	<td class="num">Jan 03</td>
	<td>Jerome Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>8.32f </td>
	<td>EL KABEIR</td>
	<td>C. Lopez</td>
	<td>J. Terranova, II</td>
</tr>
<tr class="odd">	<td class="num">Jan 03</td>
	<td>Dania Beach Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>7.5f T</td>
	<td>NIGHT PROWLER</td>
	<td>J. Castellano</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">Jan 03</td>
	<td>San Gabriel Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>FINNEGANS WAKE</td>
	<td>V. Espinoza</td>
	<td>P. Miller</td>
</tr>
<tr class="odd">	<td class="num">Jan 03</td>
	<td>Santa Ynez Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>6.5f </td>
	<td>SEDUIRE</td>
	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td>
</tr>
<tr>	<td class="num">Jan 04</td>
	<td>Monrovia Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>6.5f T</td>
	<td>SHRINKING VIOLET</td>
	<td>K. Desormeaux</td>
	<td>W. Ward</td>
</tr>
<tr class="odd">	<td class="num">Jan 10</td>
	<td>Fort Lauderdale Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>8.5f T</td>
	<td>MSHAWISH</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Jan 10</td>
	<td>San Pasqual Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>8.5f </td>
	<td>HOPPERTUNITY</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Jan 10</td>
	<td>Sham Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>8f </td>
	<td>CALCULATOR</td>
	<td>E. Trujillo</td>
	<td>P. Miller</td>
</tr>
<tr>	<td class="num">Jan 17</td>
	<td>La Ca&ntilde;ada Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4yo f</td>
	<td>8.5f </td>
	<td>THEGIRLINTHATSONG</td>
	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td>
</tr>
<tr class="odd">	<td class="num">Jan 17</td>
	<td>Lecomte Stakes</td>
	<td>Fair Grounds</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>8.32f </td>
	<td>INTERNATIONAL STAR</td>
	<td>M. Mena</td>
	<td>M. Maker</td>
</tr>
<tr>	<td class="num">Jan 17</td>
	<td>Col. E.R. Bradley Handicap</td>
	<td>Fair Grounds</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up</td>
	<td>8.5f T</td>
	<td>STRING KING</td>
	<td>J. Graham</td>
	<td>C. Smith</td>
</tr>
<tr class="odd">	<td class="num">Jan 24</td>
	<td>Forward Gal Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>7f </td>
	<td>BIRDATTHEWIRE</td>
	<td>I. Ortiz, Jr.</td>
	<td>D. Romans</td>
</tr>
<tr>	<td class="num">Jan 24</td>
	<td>Holy Bull Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>UPSTART</td>
	<td>J. Ortiz</td>
	<td>R. Violette, Jr.</td>
</tr>
<tr class="odd">	<td class="num">Jan 24</td>
	<td>Hutcheson Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>BARBADOS</td>
	<td>L. Saez</td>
	<td>M. Tomlinson</td>
</tr>
<tr>	<td class="num">Jan 24</td>
	<td>Sweetest Chant Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>8f T</td>
	<td>CONSUMER CREDIT</td>
	<td>E. Zayas</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Jan 24</td>
	<td>Connally Turf Cup</td>
	<td>Sam Houston</td>
	<td>III</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>COALPORT</td>
	<td>M. Mena</td>
	<td>M. Maker</td>
</tr>
<tr>	<td class="num">Jan 24</td>
	<td>Tampa Bay Stakes</td>
	<td>Tampa Bay Downs</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>8.5f T</td>
	<td>LOCHTE</td>
	<td>P. Lopez</td>
	<td>M. Vitali</td>
</tr>
<tr class="odd">	<td class="num">Jan 31</td>
	<td>Sam F. Davis Stakes</td>
	<td>Tampa Bay Downs</td>
	<td>III</td>
	<td>$250,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>OCEAN KNIGHT</td>
	<td>I. Ortiz, Jr.</td>
	<td>K. McLaughlin</td>
</tr>
<tr>	<td class="num">Jan 31</td>
	<td>Endeavour Stakes</td>
	<td>Tampa Bay Downs</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f T</td>
	<td>TESTA ROSSI</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Jan 31</td>
	<td>Palos Verdes Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>6f </td>
	<td>CONQUEST TWO STEP</td>
	<td>J. Talamo</td>
	<td>M. Casse</td>
</tr>
<tr>	<td class="num">Jan 31</td>
	<td>Las Virgenes Stakes</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>8f </td>
	<td>CALLBACK</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Jan 31</td>
	<td>Arcadia Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>8f T</td>
	<td>AVANZARE</td>
	<td>G. Stevens</td>
	<td>T. Proctor</td>
</tr>
<tr>	<td class="num">Feb 01</td>
	<td>San Vicente Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>LORD NELSON</td>
	<td>R. Bejarano</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Feb 07</td>
	<td>San Marcos Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>10f T</td>
	<td>FINNEGANS WAKE</td>
	<td>V. Espinoza</td>
	<td>P. Miller</td>
</tr>
<tr>	<td class="num">Feb 07</td>
	<td>San Antonio Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$500,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>SHARED BELIEF</td>
	<td>M. Smith</td>
	<td>J. Hollendorfer</td>
</tr>
<tr class="odd">	<td class="num">Feb 07</td>
	<td>Robert B. Lewis Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>DORTMUND</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Feb 07</td>
	<td>Suwannee River Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>9f T</td>
	<td>SANDIVA</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Feb 07</td>
	<td>Gulfstream Park Turf Handicap</td>
	<td>Gulfstream</td>
	<td>I</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>MSHAWISH</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Feb 07</td>
	<td>Fred W. Hooper Handicap</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up</td>
	<td>8f </td>
	<td>VALID</td>
	<td>P. Lopez</td>
	<td>M. Vitali</td>
</tr>
<tr class="odd">	<td class="num">Feb 07</td>
	<td>Donn Handicap</td>
	<td>Gulfstream</td>
	<td>I</td>
	<td>$500,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>CONSTITUTION</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Feb 07</td>
	<td>Withers Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$250,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>FAR FROM OVER</td>
	<td>M. Franco</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Feb 07</td>
	<td>Toboggan Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>6f </td>
	<td>SALUTOS AMIGOS</td>
	<td>I. Ortiz, Jr.</td>
	<td>D. Jacobson</td>
</tr>
<tr>	<td class="num">Feb 14</td>
	<td>El Camino Real Derby</td>
	<td>Golden Gate</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>9f AW</td>
	<td>METABOSS</td>
	<td>A. Solis</td>
	<td>J. Bonde</td>
</tr>
<tr class="odd">	<td class="num">Feb 14</td>
	<td>Hurricane Bertie Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>6.5f </td>
	<td>MERRY MEADOW</td>
	<td>J. Castellano</td>
	<td>M. Hennig</td>
</tr>
<tr>	<td class="num">Feb 14</td>
	<td>Barbara Fritchie Handicap</td>
	<td>Laurel Park</td>
	<td>II</td>
	<td>$300,000</td>
	<td>3&amp;up f/m</td>
	<td>7f </td>
	<td>LADY SABELIA</td>
	<td>H. Karamanos</td>
	<td>R. Graham</td>
</tr>
<tr class="odd">	<td class="num">Feb 15</td>
	<td>Bayakoa Stakes</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>MUFAJAAH</td>
	<td>E. Esquivel</td>
	<td>D. Peitz</td>
</tr>
<tr>	<td class="num">Feb 16</td>
	<td>General George Handicap</td>
	<td>Laurel Park</td>
	<td>III</td>
	<td>$250,000</td>
	<td>3&amp;up</td>
	<td>7f </td>
	<td>MISCONNECT</td>
	<td>K. Carmouche</td>
	<td>B. Levine</td>
</tr>
<tr class="odd">	<td class="num">Feb 16</td>
	<td>Buena Vista Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>8f T</td>
	<td>DIVERSY HARBOR</td>
	<td>G. Stevens</td>
	<td>T. Proctor</td>
</tr>
<tr>	<td class="num">Feb 21</td>
	<td>The Very One Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>11f T</td>
	<td>IRISH MISSION</td>
	<td>J. Velazquez</td>
	<td>C. Clement</td>
</tr>
<tr class="odd">	<td class="num">Feb 21</td>
	<td>Rampart Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up f/m</td>
	<td>8f </td>
	<td>HOUSE RULES</td>
	<td>J. Castellano</td>
	<td>J. Jerkens</td>
</tr>
<tr>	<td class="num">Feb 21</td>
	<td>Mac Diarmida Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>11f T</td>
	<td>MAIN SEQUENCE</td>
	<td>R. Maragh</td>
	<td>H. Motion</td>
</tr>
<tr class="odd">	<td class="num">Feb 21</td>
	<td>Gulfstream Park Sprint Championship</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up</td>
	<td>6.5f </td>
	<td>C ZEE</td>
	<td>E. Zayas</td>
	<td>S. Gold</td>
</tr>
<tr>	<td class="num">Feb 21</td>
	<td>Fountain of Youth Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>ITSAKNOCKOUT</td>
	<td>L. Saez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Feb 21</td>
	<td>Canadian Turf Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>8f T</td>
	<td>LONG ON VALUE</td>
	<td>J. Rosario</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Feb 21</td>
	<td>Risen Star Stakes</td>
	<td>Fair Grounds</td>
	<td>II</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>INTERNATIONAL STAR</td>
	<td>M. Mena</td>
	<td>M. Maker</td>
</tr>
<tr class="odd">	<td class="num">Feb 21</td>
	<td>Mineshaft Handicap</td>
	<td>Fair Grounds</td>
	<td>III</td>
	<td>$125,000</td>
	<td>4&amp;up</td>
	<td>8.5f </td>
	<td>STREET BABE</td>
	<td>K. Clark</td>
	<td>M. Dilger</td>
</tr>
<tr>	<td class="num">Feb 21</td>
	<td>Fair Grounds Handicap</td>
	<td>Fair Grounds</td>
	<td>III</td>
	<td>$125,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>CHOCOLATE RIDE</td>
	<td>F. Geroux</td>
	<td>B. Cox</td>
</tr>
<tr class="odd">	<td class="num">Feb 22</td>
	<td>Southwest Stakes</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>FAR RIGHT</td>
	<td>M. Smith</td>
	<td>R. Moquett</td>
</tr>
<tr>	<td class="num">Feb 28</td>
	<td>Santa Ysabel Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>STELLAR WIND</td>
	<td>V. Espinoza</td>
	<td>J. Sadler</td>
</tr>
<tr class="odd">	<td class="num">Feb 28</td>
	<td>Herecomesthebride Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>8.5f T</td>
	<td>DEVINE AIDA</td>
	<td>J. Rios</td>
	<td>R. Morales</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>Tampa Bay Derby</td>
	<td>Tampa Bay Downs</td>
	<td>II</td>
	<td>$350,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>CARPE DIEM</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>Florida Oaks</td>
	<td>Tampa Bay Downs</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8.5f T</td>
	<td>QUALITY ROCKS</td>
	<td>J. Lezcano</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>Santa Anita Handicap</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>4&amp;up</td>
	<td>10f </td>
	<td>SHARED BELIEF</td>
	<td>M. Smith</td>
	<td>J. Hollendorfer</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>San Felipe Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>DORTMUND;</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>San Carlos Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$250,000</td>
	<td>4&amp;up</td>
	<td>7f </td>
	<td>WILD DUDE</td>
	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>Frank E. Kilroe Mile</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$400,000</td>
	<td>4&amp;up</td>
	<td>8f T</td>
	<td>RING WEEKEND</td>
	<td>D. Van Dyke</td>
	<td>H. Motion</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>Honeybee Stakes</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>SARAH SIS</td>
	<td>J. Felix</td>
	<td>I. Mason</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>Gulfstream Park Handicap</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>8f </td>
	<td>HONOR CODE</td>
	<td>J. Castellano</td>
	<td>C. McGaughey III</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>Swale Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>READY FOR RYE</td>
	<td>L. Saez</td>
	<td>T. Albertrani</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>Palm Beach Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>8.5f T</td>
	<td>EH CUMPARI</td>
	<td>J. Caraballo</td>
	<td>M. Dilger</td>
</tr>
<tr>	<td class="num">Mar 07</td>
	<td>Tom Fool Handicap</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>6f </td>
	<td>SALUTOS AMIGOS</td>
	<td>C. Velasquez</td>
	<td>D. Jacobson</td>
</tr>
<tr class="odd">	<td class="num">Mar 07</td>
	<td>Gotham Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>EL KABEIR</td>
	<td>C. Lopez</td>
	<td>J. Terranova, II</td>
</tr>
<tr>	<td class="num">Mar 14</td>
	<td>Rebel Stakes</td>
	<td>Oaklawn Park</td>
	<td>II</td>
	<td>$750,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>AMERICAN PHAROAH</td>
	<td>V. Espinoza</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Mar 14</td>
	<td>Razorback Handicap</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$250,000</td>
	<td>4&amp;up</td>
	<td>8.5f </td>
	<td>RACE DAY</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Mar 14</td>
	<td>Azeri Stakes</td>
	<td>Oaklawn Park</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>GOLD MEDAL DANCER</td>
	<td>L. Quinonez</td>
	<td>D. Von Hemel</td>
</tr>
<tr class="odd">	<td class="num">Mar 15</td>
	<td>Santa Ana Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>9f T</td>
	<td>HOOP OF COLOUR</td>
	<td>D. Van Dyke</td>
	<td>H. Motion</td>
</tr>
<tr>	<td class="num">Mar 21</td>
	<td>Spiral Stakes</td>
	<td>Turfway Park</td>
	<td>III</td>
	<td>$550,000</td>
	<td>3yo</td>
	<td>9f AW</td>
	<td>DUBAI SKY</td>
	<td>J. Lezcano</td>
	<td>W. Mott</td>
</tr>
<tr class="odd">	<td class="num">Mar 21</td>
	<td>San Luis Rey Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>12f T</td>
	<td>ASHLEYLUVSSUGAR</td>
	<td>G. Stevens</td>
	<td>P. Eurton</td>
</tr>
<tr>	<td class="num">Mar 21</td>
	<td>Royal Delta Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>SHEER DRAMA</td>
	<td>J. Bravo</td>
	<td>D. Fawkes</td>
</tr>
<tr class="odd">	<td class="num">Mar 21</td>
	<td>Inside Information Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>7f </td>
	<td>CLASSIC POINT</td>
	<td>P. Lopez</td>
	<td>J. Jerkens</td>
</tr>
<tr>	<td class="num">Mar 22</td>
	<td>Sunland Derby</td>
	<td>Sunland Park</td>
	<td>III</td>
	<td>$800,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>FIRING LINE</td>
	<td>G. Stevens</td>
	<td>S. Callaghan</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>Tokyo City Cup</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up</td>
	<td>12f </td>
	<td>SKY KINGDOM</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Mar 28</td>
	<td>Skip Away Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>9.5f </td>
	<td>COMMISSIONER</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>Pan American Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>12f T</td>
	<td>IMAGINING</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. McGaughey III</td>
</tr>
<tr>	<td class="num">Mar 28</td>
	<td>Orchid Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>12f T</td>
	<td>BEAUTY PARLOR</td>
	<td>J. Velazquez</td>
	<td>C. Clement</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>Honey Fox Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>8f T</td>
	<td>LADY LARA</td>
	<td>J. Alvarado</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Mar 28</td>
	<td>Gulfstream Park Oaks</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>BIRDATTHEWIRE</td>
	<td>I. Ortiz, Jr.</td>
	<td>D. Romans</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>Florida Derby</td>
	<td>Gulfstream</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>MATERIALITY</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Mar 28</td>
	<td>Appleton Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>8f T</td>
	<td>WAR CORRESPONDENT</td>
	<td>J. Velazquez</td>
	<td>C. Clement</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>New Orleans Handicap</td>
	<td>Fair Grounds</td>
	<td>II</td>
	<td>$400,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>CALL ME GEORGE</td>
	<td>J. Graham</td>
	<td>G. Forster</td>
</tr>
<tr>	<td class="num">Mar 28</td>
	<td>Mervin H. Muniz Jr. Memorial Handicap</td>
	<td>Fair Grounds</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>CHOCOLATE RIDE</td>
	<td>J. Talamo</td>
	<td>B. Cox</td>
</tr>
<tr class="odd">	<td class="num">Mar 28</td>
	<td>Louisiana Derby</td>
	<td>Fair Grounds</td>
	<td>II</td>
	<td>$750,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>INTERNATIONAL STAR</td>
	<td>M. Mena</td>
	<td>M. Maker</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Toyota Blue Grass Stakes</td>
	<td>Keeneland</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>CARPE DIEM</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Fantasy Stakes</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$400,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>INCLUDE BETTY</td>
	<td>R. Homeister, Jr.</td>
	<td>T. Proctor</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Providencia Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>9f T</td>
	<td>SPIRIT OF XIAN</td>
	<td>J. Talamo</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Santa Anita Derby</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>DORTMUND</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Santa Anita Oaks</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$400,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>STELLAR WIND</td>
	<td>V. Espinoza</td>
	<td>J. Sadler</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Shakertown Stakes</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$125,000</td>
	<td>4&amp;up</td>
	<td>5.5f T</td>
	<td>SOMETHING EXTRA</td>
	<td>S. Bridgmohan</td>
	<td>G. Cox</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Madison Stakes</td>
	<td>Keeneland</td>
	<td>I</td>
	<td>$350,000</td>
	<td>4&amp;up f/m</td>
	<td>7f </td>
	<td>PRINCESS VIOLET</td>
	<td>J. Alvarado</td>
	<td>L. Rice</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Central Bank Ashland Stakes</td>
	<td>Keeneland</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>LOVELY MARIA</td>
	<td>K. Clark</td>
	<td>J. Jones</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Wood Memorial Stakes</td>
	<td>Aqueduct</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>FROSTED</td>
	<td>J. Rosario</td>
	<td>K. McLaughlin</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Gazelle Stakes</td>
	<td>Aqueduct</td>
	<td>II</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>9f </td>
	<td>CONDO COMMANDO</td>
	<td>J. Rosario</td>
	<td>R. Rodriguez</td>
</tr>
<tr>	<td class="num">Apr 04</td>
	<td>Carter Handicap</td>
	<td>Aqueduct</td>
	<td>I</td>
	<td>$400,000</td>
	<td>4&amp;up</td>
	<td>7f </td>
	<td>DADS CAPS</td>
	<td>J. Ortiz</td>
	<td>R. Rodriguez</td>
</tr>
<tr class="odd">	<td class="num">Apr 04</td>
	<td>Bay Shore Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>MARCH</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">Apr 09</td>
	<td>Count Fleet Sprint Handicap</td>
	<td>Oaklawn Park</td>
	<td>III</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>6f </td>
	<td>ALSVID</td>
	<td>C. Landeros</td>
	<td>C. Hartman</td>
</tr>
<tr class="odd">	<td class="num">Apr 10</td>
	<td>Apple Blossom Handicap</td>
	<td>Oaklawn Park</td>
	<td>I</td>
	<td>$600,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>UNTAPABLE</td>
	<td>J. Velazquez</td>
	<td>S. Asmussen</td>
</tr>
<tr>	<td class="num">Apr 11</td>
	<td>Las Cienegas Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up f/m</td>
	<td>6.5f T</td>
	<td>HOME JOURNEY</td>
	<td>R. Bejarano</td>
	<td>M. Puype</td>
</tr>
<tr class="odd">	<td class="num">Apr 11</td>
	<td>Kona Gold Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>6.5f </td>
	<td>MASOCHISTIC</td>
	<td>T. Baze</td>
	<td>R. Ellis</td>
</tr>
<tr>	<td class="num">Apr 11</td>
	<td>Oaklawn Handicap</td>
	<td>Oaklawn Park</td>
	<td>II</td>
	<td>$600,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>RACE DAY</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Apr 11</td>
	<td>Arkansas Derby</td>
	<td>Oaklawn Park</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>AMERICAN PHAROAH</td>
	<td>V. Espinoza</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Apr 11</td>
	<td>Jenny Wiley Stakes</td>
	<td>Keeneland</td>
	<td>I</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f T</td>
	<td>BALL DANCING</td>
	<td>J. Castellano</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Apr 11</td>
	<td>Coolmore Lexington Stakes</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$250,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>DIVINING RED</td>
	<td>J. Leparoux</td>
	<td>A. Delacour</td>
</tr>
<tr>	<td class="num">Apr 11</td>
	<td>Ben Ali Stakes</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>PROTONICO</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Apr 11</td>
	<td>Top Flight Handicap</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>9f </td>
	<td>HOUSE RULES</td>
	<td>J. Alvarado</td>
	<td>J. Jerkens</td>
</tr>
<tr>	<td class="num">Apr 12</td>
	<td>Adena Springs Beaumont Stakes</td>
	<td>Keeneland</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3yo f</td>
	<td>7f </td>
	<td>MISS ELLA</td>
	<td>R. Maragh</td>
	<td>H. Motion</td>
</tr>
<tr class="odd">	<td class="num">Apr 12</td>
	<td>Appalachian S Presented by Japan Racing Association</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$125,000</td>
	<td>3yo f</td>
	<td>8f T</td>
	<td>LADY ELI</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">Apr 15</td>
	<td>Transylvania Stakes</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>8.5f T</td>
	<td>NIGHT PROWLER</td>
	<td>J. Castellano</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Apr 17</td>
	<td>Hilliard Lyons Doubledogdare Stakes</td>
	<td>Keeneland</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>DECEPTIVE VISION</td>
	<td>J. Velazquez</td>
	<td>M. Pierce</td>
</tr>
<tr>	<td class="num">Apr 18</td>
	<td>Distaff Handicap</td>
	<td>Aqueduct</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>6f </td>
	<td>LA VERDAD</td>
	<td>J. Ortiz</td>
	<td>L. Rice</td>
</tr>
<tr class="odd">	<td class="num">Apr 18</td>
	<td>Charles Town Classic</td>
	<td>Charles Town</td>
	<td>II</td>
	<td>$1,500,000</td>
	<td>4&amp;up</td>
	<td>9f </td>
	<td>MORENO</td>
	<td>C. Velasquez</td>
	<td>E. Guillot</td>
</tr>
<tr>	<td class="num">Apr 18</td>
	<td>Dixiana Elkhorn Stakes</td>
	<td>Keeneland</td>
	<td>II</td>
	<td>$250,000</td>
	<td>4&amp;up</td>
	<td>12f T</td>
	<td>DRAMEDY</td>
	<td>J. Bravo</td>
	<td>G. Aschinger</td>
</tr>
<tr class="odd">	<td class="num">Apr 18</td>
	<td>Illinois Derby</td>
	<td>Hawthorne</td>
	<td>III</td>
	<td>$400,000</td>
	<td>3yo</td>
	<td>9f</td>
	<td>WHISKEY TICKET</td>
	<td>M. Pedroza</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Apr 18</td>
	<td>Sixty Sails Handicap</td>
	<td>Hawthorne</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>9f</td>
	<td>YAHILWA</td>
	<td>E. Trujillo</td>
	<td>J. Cassidy</td>
</tr>
<tr class="odd">	<td class="num">Apr 18</td>
	<td>Whimsical Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>6f T</td>
	<td>UNSPURNED</td>
	<td>A. Garcia</td>
	<td>R. Attfield</td>
</tr>
<tr>	<td class="num">Apr 18</td>
	<td>Santa Barbara Handicap</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>10f T</td>
	<td>QUEEN OF THE SAND</td>
	<td>D. Van Dyke</td>
	<td>P. Gallagher</td>
</tr>
<tr class="odd">	<td class="num">Apr 19</td>
	<td>San Simeon Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>4&amp;up</td>
	<td>6.5f T</td>
	<td>GET HAPPY MISTER</td>
	<td>T. Baze</td>
	<td>M. Tsagalakis</td>
</tr>
<tr>	<td class="num">Apr 25</td>
	<td>Excelsior Stakes</td>
	<td>Aqueduct</td>
	<td>III</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>10f </td>
	<td>EFFINEX</td>
	<td>A. Arroyo</td>
	<td>J. Jerkens</td>
</tr>
<tr class="odd">	<td class="num">Apr 25</td>
	<td>San Francisco Mile</td>
	<td>Golden Gate</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>G. G. RYDER</td>
	<td>R. Gonzalez</td>
	<td>J. Hollendorfer</td>
</tr>
<tr>	<td class="num">Apr 25</td>
	<td>Miami Mile Handicap</td>
	<td>Gulfstream Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>RERUN</td>
	<td>J. Caraballo</td>
	<td>C. Stewart</td>
</tr>
<tr class="odd">	<td class="num">Apr 25</td>
	<td>Last Tycoon Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>10f T</td>
	<td>SI SAGE</td>
	<td>E. Trujillo</td>
	<td>D. Vienna</td>
</tr>
<tr>	<td class="num">Apr 26</td>
	<td>Wilshire Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>8f T</td>
	<td>BLINGISMYTHING</td>
	<td>T. Baze</td>
	<td>J. Kruljac</td>
</tr>
<tr class="odd">	<td class="num">May 01</td>
	<td>Turf Sprint</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>5f T</td>
	<td>POWER ALERT</td>
	<td>J. Leparoux</td>
	<td>B. Lynch</td>
</tr>
<tr>	<td class="num">May 01</td>
	<td>Kentucky Oaks</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo f</td>
	<td>9f </td>
	<td>LOVELY MARIA</td>
	<td>K. Clark</td>
	<td>J. Jones</td>
</tr>
<tr class="odd">	<td class="num">May 01</td>
	<td>La Troienne Stakes</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>MOLLY MORGAN</td>
	<td>C. Lanerie</td>
	<td>D. Romans</td>
</tr>
<tr>	<td class="num">May 01</td>
	<td>Eight Belles Stakes</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>7f </td>
	<td>PROMISE ME SILVER</td>
	<td>R. Albarado</td>
	<td>W. Calhoun</td>
</tr>
<tr class="odd">	<td class="num">May 01</td>
	<td>Edgewood Stakes</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>8.5f T</td>
	<td>FEATHERED</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">May 01</td>
	<td>Alysheba Stakes</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$400,000</td>
	<td>4&amp;up</td>
	<td>8.5f </td>
	<td>PROTONICO</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">May 01</td>
	<td>Texas Mile Stakes</td>
	<td>Lonestar Park</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8f</td>
	<td>TEXAS AIR</td>
	<td>Q. Hamilton</td>
	<td>A. Milligan</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Precisionist Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8.5f </td>
	<td>CATCH A FLIGHT</td>
	<td>F. Prat</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">May 02</td>
	<td>Woodford Reserve Turf Classic Stakes</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$500,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>FINNEGANS WAKE</td>
	<td>V. Espinoza</td>
	<td>P. Miller</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Pat Day Mile Stakes</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>8f </td>
	<td>COMPETITIVE EDGE</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">May 02</td>
	<td>Kentucky Derby</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$2,000,000</td>
	<td>3yo</td>
	<td>10f </td>
	<td>AMERICAN PHAROAH</td>
	<td>V. Espinoza</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Humana Distaff</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>7f </td>
	<td>DAME DOROTHY</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">May 02</td>
	<td>Churchill Downs</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$500,000</td>
	<td>4&amp;up</td>
	<td>7f </td>
	<td>PRIVATE ZONE</td>
	<td>M. Pedroza</td>
	<td>J. Navarro</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Churchill Distaff Turf Mile</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>8f T</td>
	<td>TEPIN</td>
	<td>J. Leparoux</td>
	<td>M. Casse</td>
</tr>
<tr class="odd">	<td class="num">May 02</td>
	<td>American Turf</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3yo</td>
	<td>8.5f T</td>
	<td>DIVISIDERO</td>
	<td>R. Hernandez</td>
	<td>W. Bradley</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Westchester Stakes</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>8f </td>
	<td>TONALIST</td>
	<td>J. Bravo</td>
	<td>C. Clement</td>
</tr>
<tr class="odd">	<td class="num">May 02</td>
	<td>Sheepshead Bay Stakes</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up f/m</td>
	<td>11f T</td>
	<td>ROSALIND</td>
	<td>J. Bravo</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">May 02</td>
	<td>Fort Marcy Stakes</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>9f T</td>
	<td>BIG BLUE KITTEN</td>
	<td>J. Bravo</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">May 03</td>
	<td>Honeymoon Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>9f T</td>
	<td>SPANISH QUEEN</td>
	<td>B. Blanc</td>
	<td>R. Baltas</td>
</tr>
<tr>	<td class="num">May 09</td>
	<td>Hendrie Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>6.5f AW</td>
	<td>SKYLANDER GIRL</td>
	<td>E. Ramsammy</td>
	<td>A. Patykewich</td>
</tr>
<tr class="odd">	<td class="num">May 09</td>
	<td>Vanity Stakes</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td>MY SWEET ADDICTION</td>
	<td>M. Smith</td>
	<td>M. Jones</td>
</tr>
<tr>	<td class="num">May 09</td>
	<td>American Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>BAL A BALI</td>
	<td>F. Prat</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">May 09</td>
	<td>Ruffian Handicap</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$250,000</td>
	<td>4&amp;up f/m</td>
	<td>8f </td>
	<td>WEDDING TOAST</td>
	<td>J. Lezcano</td>
	<td>K. McLaughlin</td>
</tr>
<tr>	<td class="num">May 09</td>
	<td>Peter Pan Stakes</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td>MADEFROMLUCKY</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">May 09</td>
	<td>Beaugay Stakes</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f T</td>
	<td>DISCREET MARQ</td>
	<td>J. Velazquez</td>
	<td>C. Clement</td>
</tr>
<tr>	<td class="num">May 10</td>
	<td>Lazaro Barrera Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>KENTUCKIAN</td>
	<td>M. Smith</td>
	<td>J. Hollendorfer</td>
</tr>
<tr class="odd">	<td class="num">May 15</td>
	<td>Pimlico Special</td>
	<td>Pimlico</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3&amp;up</td>
	<td>9.5f </td>
	<td>COMMISSIONER</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">May 15</td>
	<td>Black-Eyed Susan Stakes</td>
	<td>Pimlico</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3yo f</td>
	<td>9f </td>
	<td>KEEN PAULINE</td>
	<td>J. Castellano</td>
	<td>D. Romans</td>
</tr>
<tr class="odd">	<td class="num">May 15</td>
	<td>Allaire duPont Distaff Stakes</td>
	<td>Pimlico</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td>STOPCHARGINGMARIA</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">May 15</td>
	<td>Adena Springs Miss Preakness Stakes</td>
	<td>Pimlico</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>6f </td>
	<td>IRISH JASPER</td>
	<td>K. Carmouche</td>
	<td>D. Ryan</td>
</tr>
<tr class="odd">	<td class="num">May 16</td>
	<td>Marine Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>8.5f AW</td>
	<td>SHAMAN GHOST</td>
	<td>R. Hernandez</td>
	<td>B. Lynch</td>
</tr>
<tr>	<td class="num">May 16</td>
	<td>Preakness Stakes</td>
	<td>Pimlico</td>
	<td>I</td>
	<td>$1,500,000</td>
	<td>3yo</td>
	<td>9.5f </td>
	<td>AMERICAN PHAROAH</td>
	<td>V. Espinoza</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">May 16</td>
	<td>Maryland Sprint Handicap</td>
	<td>Pimlico</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td>SANDBAR</td>
	<td>J. Rosario</td>
	<td>J. Sharp</td>
</tr>
<tr>	<td class="num">May 16</td>
	<td>Longines Dixie Stakes</td>
	<td>Pimlico</td>
	<td>II</td>
	<td>$300,000</td>
	<td>3&amp;up</td>
	<td>8.5f T</td>
	<td>IRONICUS</td>
	<td>J. Castellano</td>
	<td>C. McGaughey III</td>
</tr>
<tr class="odd">	<td class="num">May 16</td>
	<td>Gallorette Handicap</td>
	<td>Pimlico</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f T</td>
	<td>WATSDACHANCES</td>
	<td>J. Castellano</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">May 16</td>
	<td>Red Bank Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>WINNING CAUSE</td>
	<td>A. Castellano, Jr.</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">May 16</td>
	<td>Vagrancy Handicap</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up f/m</td>
	<td>6.5f </td>
	<td>LA VERDAD</td>
	<td>J. Ortiz</td>
	<td>L. Rice</td>
</tr>
<tr>	<td class="num">May 17</td>
	<td>Vigil Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>7f AW</td>
	<td>BLACK HORNET</td>
	<td>L. Contreras</td>
	<td>P. Parente</td>
</tr>
<tr class="odd">	<td class="num">May 23</td>
	<td>Louisville Handicap</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>12f T</td>
	<td>XTRA LUCK</td>
	<td>B. Hernandez, Jr.</td>
	<td>N. Howard</td>
</tr>
<tr>	<td class="num">May 23</td>
	<td>Nassau Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>8f T</td>
	<td>SKY TREASURE</td>
	<td>G. Boulanger</td>
	<td>M. Casse</td>
</tr>
<tr class="odd">	<td class="num">May 24</td>
	<td>Charles Whittingham Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>12f T</td>
	<td>ASHLEYLUVSSUGAR</td>
	<td>G. Stevens</td>
	<td>P. Eurton</td>
</tr>
<tr>	<td class="num">May 25</td>
	<td>Los Angeles Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td>DISTINCTIV PASSION</td>
	<td>E. Maldonado</td>
	<td>J. Bonde</td>
</tr>
<tr class="odd">	<td class="num">May 25</td>
	<td>Gamely Stakes</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td>HARD NOT TO LIKE</td>
	<td>V. Espinoza</td>
	<td>C. Clement</td>
</tr>
<tr>	<td class="num">May 25</td>
	<td>Lone Star Park Handicap</td>
	<td>Lone Star</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8.5f </td>
	<td>MAJESTIC CITY</td>
	<td>C. Lopez</td>
	<td>R. Baltas</td>
</tr>
<tr class="odd">	<td class="num">May 25</td>
	<td>All American</td>
	<td>Golden Gate</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f AW</td>
	<td>G.G. RYDER</td>
	<td>R. Gonzalez</td>
	<td>J. Hollendorfer</td>
</tr>
<tr>	<td class="num">May 25</td>
	<td>Winning Colors Stakes</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>6f </td>
	<td>STREET STORY</td>
	<td>F. Geroux</td>
	<td>S. Asmussen</td>
</tr>
<tr class="odd">	<td class="num">May 30</td>
	<td>Aristides</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td>ALSVID</td>
	<td>C. Landeros</td>
	<td>C. Hartman</td>
</tr>
<tr>	<td class="num">May 30</td>
	<td>Penn Mile</td>
	<td>Penn National</td>
	<td>III</td>
	<td>$500,000</td>
	<td>3yo</td>
	<td>8f T</td>
	<td>FORCE THE PASS</td>
	<td>J. Rosario</td>
	<td>A. Goldberg</td>
</tr>
<tr class="odd">	<td class="num">May 30</td>
	<td>American Oaks</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$400,000</td>
	<td>3yo f</td>
	<td>10f T</td>
	<td>SPANISH QUEEN</td>
	<td>B. Blanc</td>
	<td>R. Baltas</td>
</tr>
<tr>	<td class="num">May 30</td>
	<td>Californian Stakes</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>9f </td>
	<td>CATCH A FLIGHT</td>
	<td>G. Stevens</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">May 30</td>
	<td>Connaught Cup Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>7f T</td>
	<td>LOCKOUT</td>
	<td>G. Boulanger</td>
	<td>M. Casse</td>
</tr>
<tr>	<td class="num">Jun 05</td>
	<td>True North Stakes</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$250,000</td>
	<td>4&amp;up</td>
	<td>6f </td>
	<td>ROCK FALL</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Jun 05</td>
	<td>New York Stakes</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$300,000</td>
	<td>4&amp;up f/m</td>
	<td>10f T</td>
	<td>WALTZING MATILDA</td>
	<td>J. Alvarado</td>
	<td>T. Stack</td>
</tr>
<tr>	<td class="num">Jun 06</td>
	<td>Old Forester Mint Julep</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f T</td>
	<td>KISS MOON</td>
	<td>C. Lanerie</td>
	<td>D. Vance</td>
</tr>
<tr class="odd">	<td class="num">Jun 06</td>
	<td>Woody Stephens Stakes</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$500,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>MARCH</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">Jun 06</td>
	<td>Ogden Phipps Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>4&amp;up f/m</td>
	<td>8.5f </td>
	<td>WEDDING TOAST</td>
	<td>J. Lezcano</td>
	<td>K. McLaughlin</td>
</tr>
<tr class="odd">	<td class="num">Jun 06</td>
	<td>Metropolitan Handicap</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,250,000</td>
	<td>3&amp;up</td>
	<td>8f </td>
	<td>HONOR CODE</td>
	<td>J. Castellano</td>
	<td>C. McGaughey III</td>
</tr>
<tr>	<td class="num">Jun 06</td>
	<td>Manhattan Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>4&amp;up</td>
	<td>10f T</td>
	<td>SLUMBER</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Jun 06</td>
	<td>Longines Just a Game Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$700,000</td>
	<td>4&amp;up f/m</td>
	<td>8f T</td>
	<td>TEPIN</td>
	<td>J. Leparoux</td>
	<td>M. Casse</td>
</tr>
<tr>	<td class="num">Jun 06</td>
	<td>Jaipur Invitational</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>6f T</td>
	<td>CHANNEL MARKER</td>
	<td>F. Torres</td>
	<td>P. Bauer</td>
</tr>
<tr class="odd">	<td class="num">Jun 06</td>
	<td>Brooklyn Invitational</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$400,000</td>
	<td>4&amp;up</td>
	<td>12f </td>
	<td>COACH INGE</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Jun 06</td>
	<td>Belmont Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,500,000</td>
	<td>3yo</td>
	<td>12f </td>
	<td>AMERICAN PHAROAH</td>
	<td>V. Espinoza</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Jun 06</td>
	<td>Acorn Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$750,000</td>
	<td>3yo f</td>
	<td>8f </td>
	<td>CURALINA</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Jun 07</td>
	<td>Eclipse Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>4&amp;up</td>
	<td>8.5f AW</td>
	<td>ARE YOU KIDDING ME</td>
	<td>A. Garcia</td>
	<td>R. Attfield</td>
</tr>
<tr class="odd">	<td class="num">Jun 07</td>
	<td>Affirmed Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>GIMME DA LUTE</td>
	<td>M. Garcia</td>
	<td>B. Baffert</td>
</tr>
<tr>	<td class="num">Jun 07</td>
	<td>Monmouth Stakes</td>
	<td>Monmouth</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>9f T</td>
	<td>TRIPLE THREAT</td>
	<td>J. Lezcano</td>
	<td>W. Mott</td>
</tr>
<tr class="odd">	<td class="num">Jun 13</td>
	<td>Shoemaker Mile</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$400,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>TALCO</td>
	<td>R. Bejarano</td>
	<td>J. Sadler</td>
</tr>
<tr>	<td class="num">Jun 13</td>
	<td>Adoration Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f </td>
	<td>BEHOLDER</td>
	<td>G. Stevens</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">Jun 13</td>
	<td>Stephen Foster Handicap</td>
	<td>Churchill Downs</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3&amp;up</td>
	<td>9f </td>
	<td>NOBLE BIRD</td>
	<td>S. Bridgmohan</td>
	<td>M. Casse</td>
</tr>
<tr>	<td class="num">Jun 13</td>
	<td>Matt Winn Stakes</td>
	<td>Churchill Downs</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>ISLAND TOWN</td>
	<td>J. Leparoux</td>
	<td>I. Wilkes</td>
</tr>
<tr class="odd">	<td class="num">Jun 13</td>
	<td>Fleur de Lis Handicap</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td>FRIVOLOUS</td>
	<td>J. Court</td>
	<td>V. Oliver</td>
</tr>
<tr>	<td class="num">Jun 13</td>
	<td>Poker Handicap</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$300,000</td>
	<td>4&amp;up</td>
	<td>8f T</td>
	<td>KING KREESA</td>
	<td>J. Ortiz</td>
	<td>D. Donk</td>
</tr>
<tr class="odd">	<td class="num">Jun 20</td>
	<td>Obeah Stakes</td>
	<td>Delaware Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td>LUNA TIME</td>
	<td>F. Boyce</td>
	<td>H. Motion</td>
</tr>
<tr>	<td class="num">Jun 20</td>
	<td>Summertime Oaks</td>
	<td>Santa Anita</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>STELLAR WIND</td>
	<td>V. Espinoza</td>
	<td>J. Sadler</td>
</tr>
<tr class="odd">	<td class="num">Jun 21</td>
	<td>King Edward Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>TOWER OF TEXAS</td>
	<td>E. Da Silva</td>
	<td>R. Attfield</td>
</tr>
<tr>	<td class="num">Jun 21</td>
	<td>Pegasus Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>MR. JORDAN</td>
	<td>P. Lopez</td>
	<td>E. Plesa, Jr.</td>
</tr>
<tr class="odd">	<td class="num">Jun 27</td>
	<td>Triple Bend Stakes</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3&amp;up</td>
	<td>7f </td>
	<td>MASOCHISTIC</td>
	<td>T. Baze</td>
	<td>R. Ellis</td>
</tr>
<tr>	<td class="num">Jun 27</td>
	<td>The Gold Cup at Santa Anita</td>
	<td>Santa Anita</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3&amp;up</td>
	<td>10f </td>
	<td>HARD ACES</td>
	<td>V. Espinoza</td>
	<td>J. Sadler</td>
</tr>
<tr class="odd">	<td class="num">Jun 27</td>
	<td>Senorita Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>8f T</td>
	<td>PRIZE EXHIBIT</td>
	<td>S. Gonzalez</td>
	<td>J. Cassidy</td>
</tr>
<tr>	<td class="num">Jun 27</td>
	<td>Prairie Meadows Cornhusker Handicap</td>
	<td>Prairie Meadows</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3&amp;up</td>
	<td>9f </td>
	<td>GOLDEN LAD</td>
	<td>J. Castellano</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Jun 27</td>
	<td>Iowa Oaks</td>
	<td>Prairie Meadows</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>SARAH SIS</td>
	<td>J. Felix</td>
	<td>I. Mason</td>
</tr>
<tr>	<td class="num">Jun 27</td>
	<td>Iowa Derby</td>
	<td>Prairie Meadows</td>
	<td>III</td>
	<td>$250,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>BENT ON BOURBON</td>
	<td>J. Castellano</td>
	<td>E. Kenneally</td>
</tr>
<tr class="odd">	<td class="num">Jun 27</td>
	<td>Firecracker Stakes</td>
	<td>Churchill Downs</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td>DEPARTING</td>
	<td>M. Mena</td>
	<td>A. Stall, Jr.</td>
</tr>
<tr>	<td class="num">Jun 27</td>
	<td>Mother Goose Stakes</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>INCLUDE BETTY</td>
	<td>D. Van Dyke</td>
	<td>T. Proctor</td>
</tr>
<tr class="odd">	<td class="num">Jun 28</td>
	<td>San Juan Capistrano Stakes</td>
	<td>Santa Anita</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>14f T</td>
	<td>CRUCERO</td>
	<td>K. Desormeaux</td>
	<td>J. Desormeaux</td>
</tr>
<tr>	<td class="num">Jul 01</td>
	<td>Dominion Day Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>10f AW</td>
	<td>RED RIFLE</td>
	<td>A. Garcia</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Jul 03</td>
	<td>Molly Pitcher Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f </td>
	<td>GOT LUCKY</td>
	<td>P. Lopez</td>
	<td>T. Pletcher</td>
</tr>
<tr>	<td class="num">Jul 04</td>
	<td>Dr. James Penny Memorial Handicap</td>
	<td>Parx Racing</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f T</td>
	<td>COFFEE CLIQUE</td>
	<td>M. Franco</td>
	<td>B. Lynch</td>
</tr>
<tr class="odd">	<td class="num">Jul 04</td>
	<td>Victory Ride Stakes</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>6.5f </td>
	<td>IRISH JASPER</td>
	<td>J. Castellano</td>
	<td>D. Ryan</td>
</tr>
<tr>	<td class="num">Jul 04</td>
	<td>Suburban Handicap</td>
	<td>Belmont Park</td>
	<td>II</td>
	<td>$500,000</td>
	<td>4&amp;up</td>
	<td>10f </td>
	<td>EFFINEX</td>
	<td>J. Alvarado</td>
	<td>J. Jerkens</td>
</tr>
<tr class="odd">	<td class="num">Jul 04</td>
	<td>Dwyer Stakes</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$500,000</td>
	<td>3yo</td>
	<td>8f </td>
	<td>SPEIGHTSTER</td>
	<td>J. Lezcano</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Jul 04</td>
	<td>Belmont Sprint Championship</td>
	<td>Belmont Park</td>
	<td>III</td>
	<td>$400,000</td>
	<td>3&amp;up</td>
	<td>7f </td>
	<td>PRIVATE ZONE</td>
	<td>M. Pedroza</td>
	<td>J. Navarro</td>
</tr>
<tr class="odd">	<td class="num">Jul 04</td>
	<td>Belmont Oaks Invitational</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo f</td>
	<td>10f T</td>
	<td>LADY ELI</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr>	<td class="num">Jul 04</td>
	<td>Belmont Derby Invitational</td>
	<td>Belmont Park</td>
	<td>I</td>
	<td>$1,250,000</td>
	<td>3yo</td>
	<td>10f T</td>
	<td>FORCE THE PASS</td>
	<td>J. Rosario</td>
	<td>A. Goldberg</td>
</tr>
<tr class="odd">	<td class="num">Jul 05</td>
	<td>Singspiel Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>12f T</td>
	<td>ALDOUS SNOW</td>
	<td>E. Da Silva</td>
	<td>M. Pierce</td>
</tr>
<tr>	<td class="num">Jul 05</td>
	<td>Highlander Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>6f T</td>
	<td>GO BLUE OR GO HOME</td>
	<td>A. Garcia</td>
	<td>R. Baker</td>
</tr>
<tr class="odd">	<td class="num">Jul 05</td>
	<td>Dance Smartly Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td>STRUT THE COURSE</td>
	<td>L. Contreras</td>
	<td>B. Minshall</td>
</tr>
<tr>	<td class="num">Jul 05</td>
	<td>United Nations Stakes</td>
	<td>Monmouth</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3&amp;up</td>
	<td>11f T</td>
	<td>BIG BLUE KITTEN</td>
	<td>J. Bravo</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Jul 05</td>
	<td>Salvator Mile Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>8f </td>
	<td>BRADESTER</td>
	<td>C. Lanerie</td>
	<td>E. Kenneally</td>
</tr>
<tr>	<td class="num">Jul 05</td>
	<td>Smile Sprint Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td>FAVORITE TALE</td>
	<td>E. Zayas</td>
	<td>G. Preciado</td>
</tr>
<tr class="odd">	<td class="num">Jul 05</td>
	<td>Princess Rooney Stakes</td>
	<td>Gulfstream</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3&amp;up f/m</td>
	<td>6f </td>
	<td>MERRY MEADOW</td>
	<td>J. Castellano</td>
	<td>M. Hennig</td>
</tr>
<tr>	<td class="num">Jul 05</td>
	<td>Carry Back Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>7f </td>
	<td>GRAND BILI</td>
	<td>J. Castellano</td>
	<td>G. Delgado</td>
</tr>
<tr class="odd">	<td class="num">Jul 05</td>
	<td>Azalea Stakes</td>
	<td>Gulfstream</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>7f </td>
	<td>DOGWOOD TRAIL</td>
	<td>J. Rios</td>
	<td>S. Gold</td>
</tr>
<tr>	<td class="num">Jul 11</td>
	<td>Great Lady M. Stakes</td>
	<td>Los Alamitos</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3up f/m</td>
	<td>6.5f </td>
	<td>FANTASTIC STYLE</td>
	<td>R. Bejarano</td>
	<td>B. Baffert</td>
</tr>
<tr class="odd">	<td class="num">Jul 11</td>
	<td>Parx Dash</td>
	<td>Parx Racing</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>5f T</td>
	<td>TIGHTEND TOUCHDOWN</td>
	<td>F. Pennington</td>
	<td>J. Servis</td>
</tr>
<tr>	<td class="num">Jul 11</td>
	<td>Robert G. Dick Memorial Stakes</td>
	<td>Delaware Park</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>11f T</td>
	<td>CEISTEACH</td>
	<td>C. Hill</td>
	<td>T. Proctor</td>
</tr>
<tr class="odd">	<td class="num">Jul 11</td>
	<td>Delaware Oaks</td>
	<td>Delaware Park</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>CALAMITY KATE</td>
	<td>E. Prado</td>
	<td>K. Breen</td>
</tr>
<tr>	<td class="num">Jul 11</td>
	<td>Stars and Stripes</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>12f T</td>
	<td>THE PIZZA MAN</td>
	<td>F. Geroux</td>
	<td>R. Brueggemann</td>
</tr>
<tr class="odd">	<td class="num">Jul 11</td>
	<td>Modesty Handicap</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>9.5f T</td>
	<td>WALK CLOSE</td>
	<td>J. Graham</td>
	<td>C. Clement</td>
</tr>
<tr>	<td class="num">Jul 11</td>
	<td>Arlington Handicap</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>9.5f T</td>
	<td>QUITE FORCE</td>
	<td>J. Leparoux</td>
	<td>M. Maker</td>
</tr>
<tr class="odd">	<td class="num">Jul 11</td>
	<td>American Derby</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>9f T</td>
	<td>WORLD APPROVAL</td>
	<td>J. Lezcano</td>
	<td>M. Casse</td>
</tr>
<tr>	<td class="num">Jul 18</td>
	<td>Indiana Oaks</td>
	<td>Indiana Downs</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td>HIGH DOLLAR WOMAN</td>
	<td>J. Rocco, Jr.</td>
	<td>S. Hobby</td>
</tr>
<tr class="odd">	<td class="num">Jul 18</td>
	<td>Indiana Derby</td>
	<td>Indiana Downs</td>
	<td>II</td>
	<td>$500,000</td>
	<td>3yo</td>
	<td>8.5f </td>
	<td>TIZ SHEA D</td>
	<td>J. Lezcano</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Jul 18</td>
	<td>Eddie Read Stakes</td>
	<td>Del Mar</td>
	<td>I</td>
	<td>$400,000</td>
	<td>3&amp;up</td>
	<td>9f T</td>
	<td>GABRIEL CHARLES</td>
	<td>M. Smith</td>
	<td>J. Mullins</td>
</tr>
<tr class="odd">	<td class="num">Jul 18</td>
	<td>Kent Stakes</td>
	<td>Delaware Park</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>9f T</td>
	<td>SYNTAX</td>
	<td>J. Alvarado</td>
	<td>W. Mott</td>
</tr>
<tr>	<td class="num">Jul 18</td>
	<td>Delaware Handicap</td>
	<td>Delaware Park</td>
	<td>I</td>
	<td>$750,000</td>
	<td>3&amp;up f/m</td>
	<td>10f </td>
	<td>SHEER DRAMA</td>
	<td>J. Bravo</td>
	<td>D. Fawkes</td>
</tr>
<tr class="odd">	<td class="num">Jul 18</td>
	<td>Hanshin Cup Stakes</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>8f AW</td>
	<td>MIDNIGHT CELLO</td>
	<td>F. Geroux</td>
	<td>M. Tomlinson</td>
</tr>
<tr>	<td class="num">Jul 19</td>
	<td>Nijinsky Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>9f T</td>
	<td>ARE YOU KIDDING ME</td>
	<td>A. Garcia</td>
	<td>R. Attfield</td>
</tr>
<tr class="odd">	<td class="num">Jul 19</td>
	<td>San Clemente Handicap</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8f T</td>
	<td>PRIZE EXHIBIT</td>
	<td>S. Gonzalez</td>
	<td>J. Cassidy</td>
</tr>
<tr>	<td class="num">Jul 24</td>
	<td>Lake George Stakes</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo f</td>
	<td>8.5f T</td>
	<td>MRS MCDOUGAL</td>
	<td>I. Ortiz, Jr.</td>
	<td>C. Brown</td>
</tr>
<tr class="odd">	<td class="num">Jul 24</td>
	<td>Schuylerville Stakes</td>
	<td>Saratoga</td>
	<td>III</td>
	<td>$150,000</td>
	<td>2yo f</td>
	<td>6f </td>
	<td>OFF THE TRACKS</td>
	<td>L. Saez</td>
	<td>R. Rodriguez</td>
</tr>
<tr>	<td class="num">Jul 25</td>
	<td>San Diego Handicap</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8.5f </td>
	<td>CATCH A FLIGHT</td>
	<td>G. Stevens</td>
	<td>R. Mandella</td>
</tr>
<tr class="odd">	<td class="num">Jul 25</td>
	<td>Jersey Shore Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo</td>
	<td>6f </td>
	<td>HEBBRONVILLE</td>
	<td>T. McCarthy</td>
	<td>L. Whiting</td>
</tr>
<tr>	<td class="num">Jul 25</td>
	<td>Sanford Stakes</td>
	<td>Saratoga</td>
	<td>III</td>
	<td>$150,000</td>
	<td>2yo</td>
	<td>6f </td>
	<td>UNCLE VINNY</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Jul 25</td>
	<td>Diana Stakes</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td>HARD NOT TO LIKE</td>
	<td>J. Velazquez</td>
	<td>C. Clement</td>
</tr>
<tr>	<td class="num">Jul 25</td>
	<td>Ontario Matron Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f AW</td>
	<td>UCHENNA</td>
	<td>A. Garcia</td>
	<td>R. Attfield</td>
</tr>
<tr class="odd">	<td class="num">Jul 26</td>
	<td>Royal North Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>6f T</td>
	<td>AGELESS</td>
	<td>J. Leparoux</td>
	<td>A. Delacour</td>
</tr>
<tr>	<td class="num">Jul 26</td>
	<td>Coaching Club American Oaks</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>9f </td>
	<td>CURALINA</td>
	<td>J. Velazquez</td>
	<td>T. Pletcher</td>
</tr>
<tr class="odd">	<td class="num">Jul 26</td>
	<td>Bing Crosby Stakes</td>
	<td>Del Mar</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td>WILD DUDE</td>
	<td>F. Prat</td>
	<td>J. Hollendorfer</td>
</tr>
<tr>	<td class="num">Jul 29</td>
	<td>Honorable Miss Handicap</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>6f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Jul 30</td>
	<td>A.P. Smithwick Memorial Steeplechase Handicap</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$125,000</td>
	<td>4&amp;up</td>
	<td>16.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 01</td>
	<td>Jim Dandy Stakes</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$600,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 01</td>
	<td>Bowling Green Handicap</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$250,000</td>
	<td>4&amp;up</td>
	<td>11f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 01</td>
	<td>Amsterdam Stakes</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>6f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 01</td>
	<td>Alfred G. Vanderbilt Handicap</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$350,000</td>
	<td>3&amp;up</td>
	<td>6f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 01</td>
	<td>West Virginia Derby</td>
	<td>Mountaineer</td>
	<td>II</td>
	<td>$750,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 01</td>
	<td>Clement L. Hirsch Stakes</td>
	<td>Del Mar</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 02</td>
	<td>Seagram Cup Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>8.5f AW</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 02</td>
	<td>Shuvee Handicap</td>
	<td>Saratoga</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 02</td>
	<td>WinStar Matchmaker Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 02</td>
	<td>William Hill Haskell Invitational</td>
	<td>Monmouth</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3yo</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 02</td>
	<td>Monmouth Cup Stakes</td>
	<td>Monmouth</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 02</td>
	<td>Metropolitan Jets Oceanport Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>8.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 07</td>
	<td>National Museum of Racing Hall of Fame</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>8.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 08</td>
	<td>Seaway Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up f/m</td>
	<td>7f AW</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 08</td>
	<td>Whitney</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$1,250,000</td>
	<td>3&amp;up</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 08</td>
	<td>Test Stakes</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3yo f</td>
	<td>7f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 08</td>
	<td>Fasig-Tipton Waya Stakes</td>
	<td>Saratoga</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up f/m</td>
	<td>12f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 08</td>
	<td>John C. Mabee Stakes</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 08</td>
	<td>Best Pal Stakes</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$200,000</td>
	<td>2yo</td>
	<td>6.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 09</td>
	<td>La Jolla Handicap</td>
	<td>Del Mar</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo</td>
	<td>8.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 12</td>
	<td>Sorrento Stakes</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$200,000</td>
	<td>2yo f</td>
	<td>6.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 14</td>
	<td>Knob Creek Lake Placid</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 15</td>
	<td>Fourstardave Handicap</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$500,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 15</td>
	<td>Adirondack Stakes</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>2yo f</td>
	<td>6.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 15</td>
	<td>Canadian Derby</td>
	<td>Northlands Park</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3yo</td>
	<td>11f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 15</td>
	<td>Del Mar Oaks</td>
	<td>Del Mar</td>
	<td>I</td>
	<td>$300,000</td>
	<td>3yo f</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 15</td>
	<td>Secretariat Stakes</td>
	<td>Arlington Park</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3yo</td>
	<td>10f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 15</td>
	<td>Pucker Up Stakes</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 15</td>
	<td>Beverly D.</td>
	<td>Arlington Park</td>
	<td>I</td>
	<td>$750,000</td>
	<td>3&amp;up f/m</td>
	<td>9.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 15</td>
	<td>Arlington Million XXXIII</td>
	<td>Arlington Park</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3&amp;up</td>
	<td>10f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 15</td>
	<td>American St. Leger</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$400,000</td>
	<td>3&amp;up</td>
	<td>13.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 16</td>
	<td>Sky Classic Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>10f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 16</td>
	<td>Toyota Saratoga Special Stakes</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$200,000</td>
	<td>2yo</td>
	<td>6.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 16</td>
	<td>Longacres Mile</td>
	<td>Emerald Downs</td>
	<td>III</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 16</td>
	<td>Rancho Bernardo Handicap</td>
	<td>Del Mar</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>6.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 22</td>
	<td>Ontario Colleen Stakes</td>
	<td>Woodbine</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3yo f</td>
	<td>8f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 22</td>
	<td>Alabama Stakes</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$600,000</td>
	<td>3yo f</td>
	<td>10f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 22</td>
	<td>Monmouth Oaks</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>8.5f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 22</td>
	<td>Pacific Classic</td>
	<td>Del Mar</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3&amp;up</td>
	<td>10f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 22</td>
	<td>Del Mar Handicap</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$250,000</td>
	<td>3&amp;up</td>
	<td>11f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 22</td>
	<td>Arlington-Washington Futurity</td>
	<td>Arlington Park</td>
	<td>III</td>
	<td>$100,000</td>
	<td>2yo</td>
	<td>7f AW</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 23</td>
	<td>Play the King Stakes</td>
	<td>Woodbine</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>7f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 23</td>
	<td>Cliff Hanger Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 23</td>
	<td>Del Mar Mile</td>
	<td>Del Mar</td>
	<td>II</td>
	<td>$200,000</td>
	<td>3&amp;up</td>
	<td>8f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 27</td>
	<td>New York Turf Writers Cup</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$150,000</td>
	<td>4&amp;up</td>
	<td>19f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 29</td>
	<td>Travers Stakes</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$1,250,000</td>
	<td>3yo</td>
	<td>10f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 29</td>
	<td>Sword Dancer</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$1,000,000</td>
	<td>3&amp;up</td>
	<td>12f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 29</td>
	<td>Personal Ensign</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$750,000</td>
	<td>3&amp;up f/m</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 29</td>
	<td>Forego</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$700,000</td>
	<td>3&amp;up</td>
	<td>7f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 29</td>
	<td>Ballston Spa</td>
	<td>Saratoga</td>
	<td>II</td>
	<td>$400,000</td>
	<td>3&amp;up f/m</td>
	<td>8.5f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 29</td>
	<td>Ballerina Stakes</td>
	<td>Saratoga</td>
	<td>I</td>
	<td>$500,000</td>
	<td>3&amp;up f/m</td>
	<td>7f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 29</td>
	<td>Smarty Jones Stakes</td>
	<td>Parx Racing</td>
	<td>III</td>
	<td>$300,000</td>
	<td>3yo</td>
	<td>8.32f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 29</td>
	<td>Violet Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3&amp;up f/m</td>
	<td>9f T</td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr class="odd">	<td class="num">Aug 30</td>
	<td>Torrey Pines Stakes</td>
	<td>Del Mar</td>
	<td>III</td>
	<td>$100,000</td>
	<td>3yo f</td>
	<td>8f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>	<td class="num">Aug 30</td>
	<td>Philip H. Iselin Stakes</td>
	<td>Monmouth</td>
	<td>III</td>
	<td>$150,000</td>
	<td>3&amp;up</td>
	<td>9f </td>
	<td></td>
	<td></td>
	<td></td>
</tr>
</tbody>
</table>