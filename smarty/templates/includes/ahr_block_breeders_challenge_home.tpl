
{literal}<style type="text/css">
#special { height: 321px; display:block; padding:10px; } 
#special .container { height: 305px; display:block; } 
#special .post { display:block; float:left; width: 100%; padding: 0; }
#special tbody { border:0; padding:0; }
#special table { float:left; height:auto; border-collapse:collapse; overflow:hidden;  margin-top: 10px; }
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { vertical-align:top; text-align:left; font-weight:bold; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special #infoEntries td.right { text-align: right; }
#special #infoEntries td.center { text-align: center; } 
#special h2.title { background:transparent url('/themes/images/horse-racing-icon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>{/literal}


      
<div id="special" class="block" style="padding:10px;">
	<div class="container">

<h2 class="title">Breeders' Cup Challenge Schedule</h2>

<div class="post">
<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
  <tr>
    <th width="13%">Date</th>
    <th>Race</th>
    <th>Track</th>
    <th class="center">Grade</th>
  </tr>
                                        <tr>                                        
                                           	 <td  >Oct 10</td>
										    <td >J. P. Morgan Chase Jessamine Stakes</td>
                                            <td >Keeneland</td>                                            
											 <td >III</td>
											
                                          </tr>
										  
										 <tr bgcolor='#EBEBEB'>                                        
                                           	 <td  >Oct 13</td>
										    <td >Nearctic Stakes</td>
                                            <td >Woodbine</td>                                            
											 <td >I</td>
											
                                          </tr>
										  
										 										
                             </tbody>            
                          </table>
</div><!-- end:post -->
</div><!-- end:container -->

<div class="boxfooter" style="padding-top:0;"><a title="Breeders Cup Challenge Schedule" href="/breeders-cup/challenge"> 2013 Breeders' Cup Challenge Schedule</a></div>


</div>
                                          