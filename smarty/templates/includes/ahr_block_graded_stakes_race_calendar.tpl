<div id="graded">
  <div class="boxheader">
    <div class="boxicon"><img src="/themes/images/gradedicon.gif" alt="graded stakes icon"/></div>
    <div><h1>Graded Stakes Races</h1></div>
  </div>

  <div id="graded-content">
        </div>
  <div class="boxfooter"><span class="bold"><a href="/races/graded_stakes_schedule">More Graded Stakes Races</a></span></div>
</div>
