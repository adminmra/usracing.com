{literal}<style>
#col3 #content-box { padding: 0; }
#infoEntries td, #infoEntries th { padding: 7px;}
</style>{/literal}

<div style="padding: 0 34px;">
<p>&nbsp;</p>
<br />
<h1 class="DarkBlue">Derby Prep Race Schedule</h1>
<p>Here are the prep races for the 138th Kentucky Derby. </p>
<br />
</div>

<table class="table table-condensed table-striped table-bordered" id="infoEntries" title="Kentucky Derby 2012 Prep Race Schedule and Horses" summary="The Prep Race Schedule for Kentucky Derby Horses">
 <tr>
 <th>Date</th>
 <th>Race</th>
 <th>Racetrack</th>
<!--  <th style="width: 53px;">Grade</th> -->
 <th>Dist</th>
 <th>Purse</th>
 <th>Win</th>
 <th>Place</th>
 <th>Show</th>
 </tr>
 

 <tr> 
  <td >05/05</td>
 <td >Kentucky Derby 138</td>
 <td ><a href='/racetrack?name=Churchill_Downs_Racetrack' title='Churchill Downs'>Churchill Downs</a></td>
<!--  <td >I</td>
 --> <td >1 1/4 Miles</td>
 <td >2,000,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >04/28</td>
 <td ><a href='/stakes?name=Derby_Trial_Stakes' title='Derby Trial Stakes'>Derby Trial Stakes</a></td>
 <td ><a href='/racetrack?name=Churchill_Downs_Racetrack' title='Churchill Downs'>Churchill Downs</a></td>
<!--  <td >III</td>
 --> <td >1 Mile</td>
 <td >$200,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >04/21</td>
 <td ><a href='/stakes?name=Jerome_Handicap' title='Jerome Handicap'>Jerome Handicap</a></td>
 <td ><a href='/racetrack?name=Aqueduct_Racetrack' title='Aqueduct'>Aqueduct</a></td>
<!--  <td >II</td>
 --> <td >1 Mile</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >04/21</td>
 <td ><a href='/stakes?name=Coolmore_Lexington_Stakes' title='Coolmore Lexington Stakes'>Coolmore Lexington Stakes</a></td>
 <td ><a href='/racetrack?name=Keeneland' title='Keeneland'>Keeneland</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$300,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >04/14</td>
 <td ><a href='/stakes?name=Toyota_Blue_Grass_Stakes' title='Toyota Blue Grass Stakes'>Toyota Blue Grass Stakes</a></td>
 <td ><a href='/racetrack?name=Keeneland' title='Keeneland'>Keeneland</a></td>
<!--  <td >I</td>
 --> <td >1 1/8 Miles</td>
 <td >$750,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >04/14</td>
 <td ><a href='/stakes?name=Arkansas_Derby' title='Arkansas Derby'>Arkansas Derby</a></td>
 <td ><a href='/racetrack?name=Oaklawn_Park' title='Oaklawn Park'>Oaklawn Park</a></td>
<!--  <td >I</td>
 --> <td >1 1/8 Miles</td>
 <td >$1,000,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >04/07</td>
 <td ><a href='/stakes?name=Wood_Memorial_Stakes' title='Wood Memorial Stakes'>Wood Memorial Stakes</a></td>
 <td ><a href='/racetrack?name=Aqueduct_Racetrack' title='Aqueduct'>Aqueduct</a></td>
<!--  <td >I</td>
 --> <td >1 1/8 Miles</td>
 <td >$1,000,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >04/07</td>
 <td ><a href='/stakes?name=Santa_Anita_Derby' title='Santa Anita Derby'>Santa Anita Derby</a></td>
 <td ><a href='/racetrack?name=Oak_Tree_@_Santa_Anita' title='Santa Anita'>Santa Anita</a></td>
<!--  <td >I</td>
 --> <td >1 1/8 Miles</td>
 <td >$1,000,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >04/07</td>
 <td ><a href='/stakes?name=Illinois_Derby' title='Illinois Derby'>Illinois Derby</a></td>
 <td >Hawthorne Park</td>
<!--  <td >III</td>
 --> <td >1 1/8 Mile</td>
 <td >$300,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >04/06</td>
 <td ><a href='/stakes?name=Transylvania_Stakes' title='Transylvania Stakes'>Transylvania Stakes</a></td>
 <td ><a href='/racetrack?name=Keeneland' title='Keeneland'>Keeneland</a></td>
<!--  <td >III</td>
 --> <td >1 1/16 Miles T</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/31</td>
 <td ><a href='/stakes?name=Florida_Derby' title='Florida Derby'>Florida Derby</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >II</td>
 --> <td >1 1/8 Miles</td>
 <td >$750,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/31</td>
 <td ><a href='/stakes?name=Swale_Stakes' title='Swale Stakes'>Swale Stakes</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >II</td>
 --> <td >7 Furlongs</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/25</td>
 <td ><a href='/stakes?name=Sunland_Derby' title='Sunland Derby'>Sunland Derby</a></td>
 <td ><a href='/racetrack?name=Sunland_Park' title='Sunland Park'>Sunland Park</a></td>
<!--  <td >III</td>
 --> <td >1 1/8 Miles</td>
 <td >$800,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/24</td>
 <td >UAE Derby</td>
 <td >Nad El Sheba</td>
<!--  <td >II</td>
 --> <td >1 1/8 Miles</td>
 <td >$2,000,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/24</td>
 <td ><a href='/stakes?name=Louisiana_Derby' title='Louisiana Derby'>Louisiana Derby</a></td>
 <td ><a href='/racetrack?name=Fair_Grounds_Race_Course' title='Fair Grounds'>Fair Grounds</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$750,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/24</td>
 <td ><a href='/stakes?name=Rushaway_Stakes' title='Rushaway Stakes'>Rushaway Stakes</a></td>
 <td ><a href='/racetrack?name=Turfway_Park' title='Turfway Park'>Turfway Park</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/17</td>
 <td ><a href='/stakes?name=Rebel_Stakes' title='Rebel Stakes'>Rebel Stakes</a></td>
 <td ><a href='/racetrack?name=Oaklawn_Park' title='Oaklawn Park'>Oaklawn Park</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$300,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/17</td>
 <td ><a href='/stakes?name=Private_Terms_Stakes' title='Private Terms Stakes'>Private Terms Stakes</a></td>
 <td ><a href='/racetrack?name=Laurel_Park' title='Laurel Park'>Laurel Park</a></td>
<!--  <td ></td>
 --> <td >1 Mile</td>
 <td >$75,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/10</td>
 <td ><a href='/stakes?name=Palm_Beach_Stakes' title='Palm Beach Stakes'>Palm Beach Stakes</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >III</td>
 --> <td >1 1/8 Miles T</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/10</td>
 <td ><a href='/stakes?name=San_Felipe_Stakes' title='San Felipe Stakes'>San Felipe Stakes</a></td>
 <td ><a href='/racetrack?name=Oak_Tree_@_Santa_Anita' title='Santa Anita'>Santa Anita</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 MIles</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/10</td>
 <td ><a href='/stakes?name=Tampa_Bay_Derby' title='Tampa Bay Derby'>Tampa Bay Derby</a></td>
 <td ><a href='/racetrack?name=Tampa_Bay_Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$350,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >03/03</td>
 <td ><a href='/stakes?name=Gotham_Stakes' title='Gotham Stakes'>Gotham Stakes</a></td>
 <td ><a href='/racetrack?name=Aqueduct_Racetrack' title='Aqueduct'>Aqueduct</a></td>
<!--  <td >III</td>
 --> <td >1 1/16 Miles</td>
 <td >$250,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >03/01</td>
 <td >Al Bastakiya</td>
 <td >Meydan</td>
<!--  <td ></td>
 --> <td >1 3/16 Miles</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/25</td>
 <td ><a href='/stakes?name=Hutcheson_Stakes' title='Hutcheson Stakes'>Hutcheson Stakes</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >II</td>
 --> <td >7 Furlongs</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/25</td>
 <td ><a href='/stakes?name=Fountain_of_Youth_Stakes' title='Fountain of Youth Stakes'>Fountain of Youth Stakes</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >II</td>
 --> <td >1 1/8 Mile</td>
 <td >$400,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/25</td>
 <td ><a href='/stakes?name=Borderland_Derby' title='Borderland Derby'>Borderland Derby</a></td>
 <td ><a href='/racetrack?name=Sunland_Park' title='Sunland Park'>Sunland Park</a></td>
<!--  <td >III</td>
 --> <td >1 1/16 Miles</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/19</td>
 <td ><a href='/stakes?name=Southwest_Stakes' title='Southwest Stakes'>Southwest Stakes</a></td>
 <td ><a href='/racetrack?name=Oaklawn_Park' title='Oaklawn Park'>Oaklawn Park</a></td>
<!--  <td >III</td>
 --> <td >1 Mile</td>
 <td >$250,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/18</td>
 <td ><a href='/stakes?name=San_Vincente_Stakes' title='San Vincente Stakes'>San Vincente Stakes</a></td>
 <td ><a href='/racetrack?name=Oak_Tree_@_Santa_Anita' title='Santa Anita'>Santa Anita</a></td>
<!--  <td >II</td>
 --> <td >7 Furlongs</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/17</td>
 <td ><a href='/stakes?name=Risen_Star_Stakes' title='Risen Star Stakes'>Risen Star Stakes</a></td>
 <td ><a href='/racetrack?name=Fair_Grounds_Race_Course' title='Fair Grounds'>Fair Grounds</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$300,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/11</td>
 <td ><a href='/stakes?name=El_Camino_Real_Derby' title='El Camino Real Derby'>El Camino Real Derby</a></td>
 <td ><a href='/racetrack?name=Golden_Gate_Fields' title='Golden Gate Fields'>Golden Gate Fields</a></td>
<!--  <td >III</td>
 --> <td >1 1/8 Mile</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/11</td>
 <td ><a href='/stakes?name=Sam_F._Davis_Stakes' title='Sam F. Davis Stakes'>Sam F. Davis Stakes</a></td>
 <td ><a href='/racetrack?name=Tampa_Bay_Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
<!--  <td >III</td>
 --> <td >1 1/16 Miles</td>
 <td >$225,500</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/11</td>
 <td ><a href='/stakes?name=Robert_B._Lewis_Stakes' title='Robert B. Lewis Stakes'>Robert B. Lewis Stakes</a></td>
 <td ><a href='/racetrack?name=Oak_Tree_@_Santa_Anita' title='Santa Anita'>Santa Anita</a></td>
<!--  <td >II</td>
 --> <td >1 1/16 Miles</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/10</td>
 <td >UAE 2,000 Guineas</td>
 <td >Meydan</td>
<!--  <td ></td>
 --> <td >1 Mile</td>
 <td >$250,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >02/04</td>
 <td ><a href='/stakes?name=Whirlaway_Stakes' title='Whirlaway Stakes'>Whirlaway Stakes</a></td>
 <td ><a href='/racetrack?name=Aqueduct_Racetrack' title='Aqueduct'>Aqueduct</a></td>
<!--  <td ></td>
 --> <td >1 1/16 Miles</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >02/04</td>
 <td ><a href='/stakes?name=Black_Gold_Stakes' title='Black Gold'>Black Gold</a></td>
 <td ><a href='/racetrack?name=Fair_Grounds_Race_Course' title='Fair Grounds'>Fair Grounds</a></td>
<!--  <td ></td>
 --> <td >6 Furlongs</td>
 <td >$60,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >01/28</td>
 <td ><a href='/stakes?name=Holy_Bull_Stakes' title='Holy Bull Stakes'>Holy Bull Stakes</a></td>
 <td ><a href='/racetrack?name=Gulfstream_Park' title='Gulfstream Park'>Gulfstream Park</a></td>
<!--  <td >III</td>
 --> <td >1 Mile</td>
 <td >$400,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >01/21</td>
 <td >Lecompte Stakes</td>
 <td ><a href='/racetrack?name=Fair_Grounds_Race_Course' title='Fair Grounds'>Fair Grounds</a></td>
<!--  <td >III</td>
 --> <td >1 Mile 40 yds</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  
 <tr class="odd">
  <td >01/16</td>
 <td ><a href='/stakes?name=Smarty_Jones_Stakes' title='Smarty Jones Stakes'>Smarty Jones Stakes</a></td>
 <td ><a href='/racetrack?name=Oaklawn_Park' title='Oaklawn Park'>Oaklawn Park</a></td>
<!--  <td ></td>
 --> <td >1 Mile</td>
 <td >$100,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


 
 <tr> 
  <td >01/14</td>
 <td ><a href='/stakes?name=Sham_Stakes' title='Sham Stakes'>Sham Stakes</a></td>
 <td ><a href='/racetrack?name=Oak_Tree_@_Santa_Anita' title='Santa Anita'>Santa Anita</a></td>
<!--  <td >III</td>
 --> <td >1 1/8 Miles</td>
 <td >$150,000</td>
 <td style="width: 50px;"><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 <td ><a href='/horse?name=' title=''></a></td>
 </tr>


  </table>

						
