<!-- Start Slide Banner -->
{literal}
<style type="text/css">
/* ADS Slideshow --------------------------------------------- */
#ad-slideshow { background-color: #fff; width: auto; height: auto; }
#ad-slideshow ul { margin: 0; padding: 0; width:100%; list-style-type: none; height: 1%; /* IE fix */ }
#ad-slideshow ul:after { content: "."; clear: both; display: block; height: 0; visibility: hidden; }

/*  ADS Slideshow > SLIDES --------------------------------------------- */
#ad-slideshow .slides { overflow: hidden; width: auto; }
#ad-slideshow .slides ul { width: 2880px;}
#ad-slideshow .slides li { width: auto; float: left; padding: 0; margin:0;}
#ad-slideshow .slides h2 { margin-top: 0;}

/* ADs Slideshow > NAVIGATION ---------------------------------------------------- */
#ad-slideshow .slides-nav { width: auto; position:relative; bottom:15px; right:0; margin:0; display: block; float: right; margin:0; padding:0 5px 0 0; z-index: 33; }

#ad-slideshow .slides-nav li { float: left; padding:0; margin:0;  }
#ad-slideshow .slides-nav li a { display: block; padding: 2px 2px; margin:0; height: auto; outline: none; text-decoration:none; color: #fff; }
.js #ad-slideshow .slides-nav li.on,
.js #ad-slideshow .slides-nav li.on a {  }
.js #ad-slideshow .slides-nav li.on a { position: relative; color: white; opacity: 1; top: 0;}

#ad-slideshow .slides-nav li a img {opacity: 0.5; filter: alpha(opacity = 50); }
.js #ad-slideshow .slides-nav li.on a img {opacity: 1; filter: alpha(opacity = 100); }
</style>
{/literal}
<div id="ad-container">
<div id="ad-slideshow" style="width: 313px; height: 119px;">
        <div class="slides">
        <ul>        
        <!--<li id="slide-one">
        <div><script language="javascript">document.write('<a href="/'+((logged_in())?'casino':'join')+'" title="10% On All Deposits!"><img width="313" height="119" src="/themes/images/home/cashcall-10percent_on_deposits.jpg" /></a>');</script></div>
        </li>-->
       
        <li id="slide-one" >
        <div class="logged-out"><a href="join" title="10% On all Deposits!"><img width="313" height="119" src="/themes/images/home/cashcall-10percent_on_deposits.jpg" /></a></div>
        <div class="logged-in"><a href="javascript:void(0);" onclick="open_banking();" title="Make a Deposit"><img width="313" height="119" src="/themes/images/home/cashcall-10percent_on_deposits.jpg" /></a></div>        
        </li>
        
        <li id="slide-two">   
		<div><script language="javascript">document.write('<a href="/'+((logged_in())?'casino':'join')+'" title="50% Thursday Casino Cash Back!"><img src="/themes/images/home/casino-50percent-thursdays.jpg" width="313" height="119" /></a>');</script></div>       
        </li>
        
        <!--<li id="slide-three">   
		<div><script language="javascript">document.write('<a href="/'+((logged_in())?'casino':'casino')+'" title="Play Blackjack!"><img src="/themes/images/home/banner-home-blackjack.jpg" width="313" height="119" /></a>');</script></div>       
        </li>-->
        
        <!--<li id="slide-three">
        <div><script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'promotions/horseracing')+'" title="Double Your Winnings!"><img width="313" height="119" src="/themes/images/home/doubleyourwinnings.jpg" /></a>');</script></div>
        </li>-->     
                       
        </ul>
</div><!--  end slides -->
            <ul class="slides-nav">
            <li class="on"><a href="#slide-one"><img width="5" height="5" src="/themes/images/circle-selector.png" /></a></li>
            <li><a href="#slide-two"><img width="5" height="5" src="/themes/images/circle-selector.png" /></a></li>            </ul>
</div>
</div>

{literal}
<script type="text/javascript" src="/smarty-include/js/jquery.cycle.js"></script>
<script type="text/javascript" src="/smarty-include/js/ad-slideshow.js"></script>
{/literal}
<!-- End Slide Banner -->
