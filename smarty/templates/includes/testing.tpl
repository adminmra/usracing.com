


<div class="item" id="grade-stake-0" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>J</span><span>u</span><span>l</span></div><div class="day"><span>1</span><span>8</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2015-07-18">2015-07-18</time>
		<div class="race">
		  <span>RACE:</span>
		  <span style="font-weight:400" itemprop="name">Hanshin Cup Stakes</span>
		</div><div class="track">
		  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Arlington Park</span></span></div><div class="purse">
		  <span>PURSE:</span>$100,000</div><div class="info">
		  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">III / 3&amp;up</span></div>
</div>
</div><div class="item" id="grade-stake-1" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>J</span><span>u</span><span>l</span></div><div class="day"><span>1</span><span>8</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2015-07-18">2015-07-18</time>
		<div class="race">
		  <span>RACE:</span>
		  <span style="font-weight:400" itemprop="name">Eddie Read Stakes</span>
		</div><div class="track">
		  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Del Mar</span></span></div><div class="purse">
		  <span>PURSE:</span>$400,000</div><div class="info">
		  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">I / 3&amp;up</span></div>
</div>
</div><div class="item" id="grade-stake-2" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>J</span><span>u</span><span>l</span></div><div class="day"><span>1</span><span>8</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2015-07-18">2015-07-18</time>
		<div class="race">
		  <span>RACE:</span>
		  <span style="font-weight:400" itemprop="name">Delaware Handicap</span>
		</div><div class="track">
		  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Delaware Park</span></span></div><div class="purse">
		  <span>PURSE:</span>$750,000</div><div class="info">
		  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">I / 3&amp;up f/m</span></div>
</div>
</div><div class="item" id="grade-stake-3" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>J</span><span>u</span><span>l</span></div><div class="day"><span>1</span><span>8</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2015-07-18">2015-07-18</time>
		<div class="race">
		  <span>RACE:</span>
		  <span style="font-weight:400" itemprop="name">Kent Stakes</span>
		</div><div class="track">
		  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Delaware Park</span></span></div><div class="purse">
		  <span>PURSE:</span>$200,000</div><div class="info">
		  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">III / 3yo</span></div>
</div>
</div><div class="item" id="grade-stake-4" itemscope="" itemtype="http://schema.org/Event"><div class="dateLarge"><div class="month"><span>J</span><span>u</span><span>l</span></div><div class="day"><span>1</span><span>8</span></div></div>
<div class="details">
<time style="display:none" itemprop="startDate" content="2015-07-18">2015-07-18</time>
		<div class="race">
		  <span>RACE:</span>
		  <span style="font-weight:400" itemprop="name">Indiana Derby</span>
		</div><div class="track">
		  <span>TRACK:</span><span itemprop="location" itemscope itemtype="http://schema.org/Place"><span style="font-weight:400" itemprop="name">Indiana Downs</span></span></div><div class="purse">
		  <span>PURSE:</span>$500,000</div><div class="info">
		  <span>GRADE / AGE:</span><span style="font-weight:400" itemprop="description">II / 3yo</span></div>
</div>
</div>
