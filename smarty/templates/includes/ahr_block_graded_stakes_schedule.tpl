{literal}<style>
.not-front #content-box { padding: 0;}
</style>{/literal}
<table class="table table-condensed table-striped table-bordered"   id="infoEntries" summary="Horse Racing Graded Stakes Schedule"> 
                     <tbody>
                       <tr >
                         <th valign="top"  >Date</th>
                         <th  >Graded Stakes </th>
                       </tr>
                       <tr class="odd" >                       
                         <td valign="top"  width="10%"><strong>Sep 24</strong></td>
                         <td  >    * Belmont Park: Gallant Bloom Handicap. (G2), $150,000, 3up fm,6.5<br />
    * Delaware Park: Kent Stakes. (G3), $250,000, 3yo,9.0 T<br />
    * PARX Racing: Pennsylvania Derby. (G2), $1000,000, 3yo,9.0<br />
    * Turfway Park: Kentucky Cup Distaff Stakes. (G3), $100,000, 3up fm,8.5 S<br />
    * Turfway Park: Kentucky Cup Sprint Stakes. (G3), $100,000, 3yo,6.0 S<br />
    * Turfway Park: Kentucky Cup Stakes. (G2), $100,000, 3up,8.5 S</td>
                       </tr>
                       <tr>                       
                         <td valign="top"  width="10%"><strong>Sep 30</strong></td>
                         <td  >    * Santa Anita: Sen. Ken Maddy Stakes. (G3), $100,000, 3up fm,6.5 T</td>
                       </tr>
                       <tr class="odd" >                       
                         <td valign="top"  width="10%"><strong>Oct 01</strong></td>
                         <td  >    * Belmont Park: Kelso Handicap. (G2), $200,000, 3up,8.0<br />
    * Belmont Park: Flower Bowl Invitational Stakes. (G1), $500,000, 3up fm,10.0 T<br />
    * Belmont Park: Beldame Invitational Stakes. (G1), $350,000, 3up fm,9.0<br />
    * Belmont Park: Joe Hirsch Turf Classic Invitational Stakes. (G1), $500,000, 3up,12.0 T<br />
    * Belmont Park: Jockey Club Gold Cup. (G1), $750,000, 3up,10.0<br />
    * Belmont Park: Vosburgh Invitational Stakes. (G1), $350,000, 3up,6.0<br />
    * Hoosier Park: Indiana Derby. (G2), $500,000, 3yo,8.5<br />
    * Hoosier Park: Indiana Oaks. (G2), $200,000, 3yo f,8.5<br />
    * Santa Anita: Yellow Ribbon Stakes. (G1), $250,000, 3up fm,10.0 T<br />
    * Santa Anita: Ladys Secret Stakes. (G1), $250,000, 3up fm,8.5 S<br />
    * Santa Anita: Goodwood Stakes. (G1), $250,000, 3up,9.0 S<br />
    * Santa Anita: Norfolk Stakes. (G1), $250,000, 2yo,8.5 S</td>
                       </tr>
                       <tr>                       
                         <td valign="top"  width="10%"><strong>Oct 02</strong></td>
                         <td  >    * Belmont Park: Miss Grillo Stakes. (G3), $100,000, 2yo f,8.5 T<br />
    * Belmont Park: Pilgrim Stakes. (G3), $100,000, 3yo,8.5 T<br />
    * Belmont Park: Tempted Stakes. (G3), $100,000, 2yo f,6.0<br />
    * Belmont Park: Nashua Stakes. (G2), $150,000, 2yo,6.0<br />
    * Santa Anita: Oak Leaf Stakes. (G1), $250,000, 2yo f,8.5 S<br />
    * Santa Anita: Clement L. Hirsch Turf Championship. (G2), $150,000, 3up,10.0 T</td>
                       </tr>
                                             </tbody>
                     </table>
