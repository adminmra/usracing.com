<div>
{literal}<style type="text/css">
.rotw-details { display: block; padding:8px 0 0 0px; font-family: "Helvetica", Arial; }
.rotw-details a { background: none; display: none; }
.hide-this, .rotw-num, .rotw-grade, .rotw-purse, .rotw-distance, .rotw-age, .rotw-run, .rotw-quote   { display: none;}
.rotw-day-title, .rotw-day-title, .rotw-track-title   { display: none;}
.rotw-race { color: #fff373;  font-size: 13px; font-weight: bold; text-transform: uppercase; margin: 0 0 0 12px; }
.rotw-day { color: #fff8de; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:4px 0 0 12px; }
.rotw-track { color: black; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:1px 0 0 12px; }
.rotw-click { color: #ade07a; font-size: 12px; font-family: Arial, Helvetica, sans-serif; padding:2px 12px 0 0; text-transform: uppercase; }
</style>{/literal}

<a href="/beton/race-of-the-week" style="background: url('/themes/images/home/banner-mid-rotw-top.gif') top left no-repeat; width: 313px; height: 37px; display: block; padding:0;"></a>

<a href="/beton/race-of-the-week" style="display: block; background: url('/themes/images/home/banner-mid-rotw-bot.jpg') left top;  background-repeat: none; width: 313px; height: 82px; text-decoration: none; margin:0; line-height: 12px; text-align: left; padding:0;"><!-- NOTES - Please Read: 
1. "hide-this" hides element on the Home page.
2. Do not remove <strong> tags.
3. Comment out entire <p> element if info is missing.
4. Classes have no affect on the ROTW page. They are needed for the Home page.
5. Don't use <a> links. It messes things up for some reason.
-->

<p class="rotw-details">
<br class="hide-this" />

<!-- RACE TITLE - Also added to Home Page -->
<p class="rotw-race"><strong>ONTARIO FASHION STAKES</strong></p>
<p class="hide-this">at Woodbine</p>

<br class="hide-this" />

<!-- RACE DAY - Also added to Home Page -->
<p class="rotw-day"><strong class="rotw-day-title">Day: </strong> Sunday, October 31st
<strong class="hide-this" style="font-weight: normal;"> - Estimated post time is - NA at the moment</strong>
</p>

<!-- RACE TRACK - Also added to Home Page -->
<p class="rotw-track"><strong class="rotw-track-title">RaceTrack:</strong> Woodbine</p>

<p class="rotw-num"><strong class="rotw-num-title">Race Number:</strong>  NA at the moment</p> 

<!--<p class="rotw-grade"><strong class="rotw-grade-title">Grade: </strong>I</p>-->

<p class="rotw-purse"><strong class="rotw-purse-title">Purse:</strong> $150,000</p>

<p class="rotw-distance"><strong class="rotw-distance-title">Distance:</strong> Six Furlongs</p>

<p class="rotw-age"><strong class="rotw-age-title">Age:</strong>Fillies & Mares,  Three-Year-Olds & Upward</p>

<br class="hide-this" />
<p class="rotw-run">ONTARIO FASHION STAKES to be run Sunday, October 31st, 2010 </p>
<br class="hide-this" />

<p class="rotw-quote">"Run annually since 1979 our race of the Week has become a milestone for a filly or mare who wants to be named as Champion Female Sprinter for the Sovereign Awards; this privilege was accomplished by Tribal Belle last year." </p>


</p><!--end rotw-details --></a>
</div>
