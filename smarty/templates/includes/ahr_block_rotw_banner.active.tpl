{literal}<style type="text/css">
.rotw-details { display: block; padding:8px 0 0 0px; font-family: "Helvetica", Arial; }
.rotw-details a { background: none; display: none; }
.hide-this, .rotw-num, .rotw-grade, .rotw-purse, .rotw-distance, .rotw-age, .rotw-run, .rotw-quote   { display: none;}
.rotw-day-title, .rotw-day-title, .rotw-track-title   { display: none;}
.rotw-race { color: #fff373;  font-size: 13px; font-weight: bold; text-transform: uppercase; margin: 0 0 0 12px; }
.rotw-day { color: #fff8de; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:4px 0 0 12px; }
.rotw-track { color: black; font-weight: bold; font-size: 11px; text-transform: uppercase; margin:1px 0 0 12px; }
.rotw-click { color: #ade07a; font-size: 12px; font-family: Arial, Helvetica, sans-serif; padding:2px 12px 0 0; text-transform: uppercase; }
</style>{/literal}

<!--<a href="/beton/race-of-the-week" style="width: 313px; height: 119px; display: block; padding:0;" title="Big Race of the Week"><img src="/themes/images/home-raceoftheweek-general.jpg" /></a>-->

<a href="/beton/race-of-the-week" style="background: url('/themes/images/home/banner-mid-rotw-top.gif') top left no-repeat; width: 313px; height: 37px; display: block; padding:0;"></a>

<a href="/beton/race-of-the-week" style="display: block; background: url('/themes/images/home/banner-mid-rotw-bot.jpg') left top;  background-repeat: none; width: 313px; height: 82px; text-decoration: none; margin:0; line-height: 12px; text-align: left; padding:0;">

<p class="rotw-details">

<!-- RACE TITLE - Also added to Home Page -->
<p class="rotw-race"><strong>LONG ISLAND H.</strong></p>

<!-- RACE DAY - Also added to Home Page -->
<p class="rotw-day"><strong class="rotw-day-title">Day: </strong> Saturday, November 13th</p>

<!-- RACE TRACK - Also added to Home Page -->
<p class="rotw-track"><strong class="rotw-track-title">RaceTrack:</strong>Aqueduct</p>
</p><!-- end:rotw-details -->

</a>
