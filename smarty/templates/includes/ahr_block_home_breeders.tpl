{literal}
<style type="text/css">
#special { height: 321px; display:block; } 
#special .world { display:block; background:#f2f9ff; width: auto; border:1px solid #aad3ff; margin-top:10px; } 
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { width:33%; text-align:left; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special h1.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}
<!-- NOVEMBER 6th -->
<div id="special" class="block" style="padding:10px;">
<h1 class="title">{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup Results</h1>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table table-condensed table-striped table-bordered" id="infoEntries">
  <tr>
    <th>Race</th>
    <th>Win</th>
    <th>Place</th>
    <th>Show</th>
  </tr>
  <tr>    
    <td style="padding-top:11px;"><a href="/breederscup-race?stakes=Breeders%27%20Cup%20Juvenile%20Turf&id=13">Breeders Cup Juvenile Turf</a></td>
    <td style="padding-top:11px;">More than Real</td>
    <td style="padding-top:11px;">Winter Memories</td>
    <td style="padding-top:11px;">Kathmanblu</td>
  </tr>
  <tr>    
    <td><a href="/breederscup-race?stakes=Breeders%27%20Cup%20Dirt%20Mile&id=12">Breeders' Cup Dirt Mile</a></td>
    <td>Dakota Phone</td>
    <td>Morning Line</td>
    <td>Gayego</td>
  </tr>
  <tr>
    <td><a href="/breederscup-race?stakes=Breeders' Cup Turf Sprint&id=15">Breeders Cup Turf Sprint</a></td>
    <td>Chamberlain Bridge</td>
    <td>Central City</td>
    <td>Unzip Me</td>
  </tr>
  <tr>
    <td><a href="/breederscup-race?stakes=Grey Goose Breeders' Cup Juvenile&id=11">Grey Goose Breeders' Cup Juvenile</a></td>
    <td>Uncle Mo</td>
    <td>Boys At Tosconova</td>
    <td>Rogue Romance</td>
  </tr>
  <tr>
    
    <td><a href="/breederscup-race?stakes=Breeders' Cup Mile&id=9">Breeders Cup Mile</a></td>
    <td>Goldikova</td>
    <td>Gio Ponti</td>
    <td>Paco Boy</td>
  </tr>
  <tr>
    
    <td><a href="/breederscup-race?stakes=Sentient Jet Breeders' Cup Sprint&id=10">Sentient Jet Breeders' Cup Sprint</a></td>
    <td>Big Drama</td>
    <td>Hamazing Destiny</td>
    <td>Smiling Tiger</td>
  </tr>
  <tr>    
    <td><a href="/breederscup-race?stakes=Emirates Airline Breeders' Cup Turf&id=8">Emirates Airline Breeders Cup Turf</a></td>
    <td>Dangerous Midge</td>
    <td>Champ Pegasus</td>
    <td>Behkabad</td>
  </tr>
  <tr>    
    <td><a href="/breederscup-race?stakes=Breeders' Cup Classic&id=7"><strong>Breeders Cup Classic</strong></a></td>
    <td>Blame</td>
    <td>Zenyatta</td>
    <td>Fly Down</td>
  </tr>
</table>
</div>
<div class="box-shd"></div>