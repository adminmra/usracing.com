{literal}<style>
.not-front #content-box { padding: 0;}
</style>{/literal}
<table class="table table-condensed table-striped table-bordered"   id="infoEntries" summary="Horse Racing Graded Stakes Schedule"> 
                     <tbody>
                       <tr >
                         <th valign="top"  >Date</th>
                         <th  >Graded Stakes </th>
                       </tr>
                       <tr class="odd" >                       
                         <td valign="top" width="10%" ><strong>Sep 18</strong></td>
                         <td  >    * Belmont Park: Garden City Breeders\' Cup (G1), $250,000, 3yo f, 9f T<br />
    * Charles Town: Charles Town Oaks, $400,000, 3yo f, 7f<br />
    * Charles Town: Charles Town Juvenile S., $100,000, 2yo, 7f<br />
    * Charles Town: Miss Shenandoah S., $100,000, 2yo f, 4.5f<br />
    * Charles Town: Pink Ribbon S., $75,000, 3&up, F&M, 7f<br />
    * Fairplex Park: Barretts Debutante, $110,000, 2yo f, 6.5f<br />
    * Monmouth Park: Charles Hesse H., $100,000, 3&up, 8.5f<br />
    * Monmouth Park: Eleven North H., $100,000, 3&up, F&M, 6f<br />
    * Monmouth Park: Jersey Girl H., $100,000, 3&up, F&M, 8.5f<br />
    * Monmouth Park: New Jersey Breeders\' H., $100,000, 3&up, 6f<br />
    * Parx Racing: PTHA President\'s Cup, $250,000, 3&up, 9f T<br />
    * Woodbine: Summer S, (G3), $250,000, 2yo, 8f T<br />
    * Woodbine: Natalma S, (G3), $200,000, 2yo f, 8f T </td>
                       </tr>
                       <tr>                       
                         <td valign="top" width="10%" ><strong>Sep 19</strong></td>
                         <td  >    * Belmont Park: Noble Damsel H, (G3), $100,000, 3&up, F&M, 8f T<br />
    * Fairplex Park: Barretts Juvenile, $110,000, 2yo, C&G, 6.5f<br />
    * Woodbine: Woodbine Mile (G1), $1,000,000, 3&up, 8f T<br />
    * Woodbine: Northern Dancer Breeders\' Cup Turf (G1), $750,000, 3&up, 12f T<br />
    * Woodbine: Canadian S, (G2), $300,000, 3&up, F&M, 9f T </td>
                       </tr>
                       <tr class="odd" >                       
                         <td valign="top" width="10%" ><strong>Sep 22</strong></td>
                         <td  >    * Woodbine: King Corrie S., $100,000, 3&up, 6f </td>
                       </tr>
                       <tr>                       
                         <td valign="top" width="10%" ><strong>Sep 25</strong></td>
                         <td  >    * Belmont Park: Gallant Bloom H, (G2), $150,000, 3&up, F&M, 6.5f<br />
    * Belmont Park: Brave Raj Breeders\' Cup S., $85,000, 2yo f, 8.32f<br />
    * Belmont Park: Foolish Pleasure Breeders\' Cup S., $85,000, 2yo, 8.32f<br />
    * Delaware Park: Kent Breeders\' Cup S, (G3), $200,000, $50,000 Breeders\' Cup Fund, 3yo, 9f T<br />
    * Hastings Racecourse: British Columbia Breeders\' Cup Oaks, $100,000, 3yo f, 9f<br />
    * Hastings Racecourse: Fantasy S., $75,000, 2yo f, 8.5f<br />
    * Louisiana Downs: Super Derby (G2), $500,000, 3yo, 9f<br />
    * Louisiana Downs: Louisiana Stallion S., $100,000, 2yo fd, 7f<br />
    * Louisiana Downs: Louisiana Stallion S., $100,000, 2yo, C&Gd, 7f<br />
    * Louisiana Downs: Sunday Silence Breeders\' Cup, $100,000, 2yo, 8.5f T<br />
    * Louisiana Downs: Happy Ticket Breeders\' Cup, $75,000, 2yo f, 8.5f T<br />
    * Monmouth Park: Helen Haskell Sampson S, (NSA-I), $100,000, 4&up, 20f T<br />
    * Parx Racing: Pennsylvania Derby (G2), $1,000,000, 3yo, 9f<br />
    * Parx Racing: Turf Amazon H., $200,000, 3&up, F&M, 5f T<br />
    * Presque Isle Downs: Fitz Dixon Jr. Memorial Juvenile S., $100,000, 2yo, 6.5f<br />
    * Presque Isle Downs: H.B.P.A. Stakes, $100,000, 3&up, F&M, 8.32f<br />
    * Presque Isle Downs: Presque Isle Debutante, $100,000, 2yo f, 6f<br />
    * Retama Park: Darby\'s Daughter Texas Stallion S., $100,000, 2yo f, 6f<br />
    * Retama Park: My Dandy Texas Stallion S., $100,000, 2yo, C&G, 6f<br />
    * Woodbine: Ontario Derby, $150,000, 3yo, 9f </td>
                       </tr>
                                             </tbody>
                     </table>