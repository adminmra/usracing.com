		{literal}<style>
			.not-front #col3 #content-box { padding: 0; }
			.not-front #col3 #content-wrapper { border: none; padding: 0 }
			.not-front #col3 #content-wrapper  h2.title { display: none; }
			.not-front #col3 .box-shd { display: none; }
		</style>{/literal}

		<div class="block">
			<p><h2 class="title-custom">Triple Crown Trail</h2></p>
			<div>
			  <table class="table table-condensed table-striped table-bordered" id="infoEntries"  style="width:100%; " title="Triple Crown Trail 2011" summary="Triple Crown Trail 2011">
			 <tr>
			 <th  style="width: 62px;">Date</th>
			 <th style="width: 100px;">Horse</th>
			 <th style="width: 10px;">PF</th>
			 <th style="width: 48px;">Sire</th>
			 <th style="width: 48px;">Mare Sire</th>
			 <th style="width: 50px;">Race</th>
			 <th style="width: 5px;">Gr</th>
			 <th style="width: 40px;">Track</th>
			 <th style="width: 40px;">Surface</th>
			 <th style="width: 10px;">Dp</th>
			 <th style="width: 10px;">Di</th>
			 <th style="width: 10px;">CD</th>
			 <th style="width: 10px;">Points</th>
			 </tr>

			
				 <tr>
				 				<td >01/01</td>
				<td>Monzon</td>
			<td>-41</td>
			<td>Thunder                Gulch</td>
			<td>Belong                To Me</td>
			<td>Count                Fleet S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>8.32</td>
			<td>10-2-10-0-0</td>
			<td>3.40</td>
			<td>1.00</td>
			<td>22</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/01</td>
				<td>Preachintothedevil</td>
			<td>-9</td>
			<td>Pulpit</td>
			<td>Abaginone</td>
			<td>Champagneforashley                S</td>
			<td>R</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>8.32</td>
			<td>7-5-12-0-0</td>
			<td>3.00</td>
			<td>0.79</td>
			<td>24</td>
			 </tr>


			 
				 <tr>
				 				<td >01/01</td>
				<td>King                Congie</td>
			<td>-19</td>
			<td>Badge                Of Silver</td>
			<td>End                Sweep</td>
			<td>Tropical                Park Derby</td>
			<td>L</td>
			<td>CRC</td>
			<td>TURF</td>
			<td>9.00</td>
			<td>2-0-4-0-0</td>
			<td>2.00</td>
			<td>0.67</td>
			<td>6</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/08</td>
				<td>Determinato</td>
			<td>-43</td>
			<td>Closing Argument</td>
			<td>Jeblar</td>
			<td>Spectacular                Bid S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>4-1-7-0-0</td>
			<td>2.43</td>
			<td>0.75</td>
			<td>12</td>
			 </tr>


			 
				 <tr>
				 				<td >01/08</td>
				<td>Inhisglory</td>
			<td>18</td>
			<td>Pure Prize</td>
			<td>Pulpit</td>
			<td>Turfway Preview S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>6.50</td>
			<td>5-4-10-1-0</td>
			<td>2.33</td>
			<td>0.65</td>
			<td>20</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/14</td>
				<td>Decisive Moment</td>
			<td>-17</td>
			<td>With Distinction</td>
			<td>Dehere</td>
			<td>Jean Lafitte S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Delta Downs' title='Delta Downs'>Delta Downs</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>3-2-9-0-0</td>
			<td>2.11</td>
			<td>0.57</td>
			<td>14</td>
			 </tr>


			 
				 <tr>
				 				<td >01/15</td>
				<td>Positive Response</td>
			<td>-26</td>
			<td>Pomeroy</td>
			<td>Farma Way</td>
			<td>California Derby</td>
			<td>L</td>
			<td><a href='/racetrack?name=Golden Gate Fields' title='Golden Gate Fields'>Golden Gate Fields</a></td>
			<td>AWS</td>
			<td>8.50</td>
			<td>4-3-5-0-0</td>
			<td>3.80</td>
			<td>0.92</td>
			<td>12</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/15</td>
				<td>Tapizar</td>
			<td>-68</td>
			<td>Tapit</td>
			<td>Deputy Minister</td>
			<td>Sham S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>6-5-8-1-0</td>
			<td>3.00</td>
			<td>0.80</td>
			<td>20</td>
			 </tr>


			 
				 <tr>
				 				<td >01/15</td>
				<td>Manicero</td>
			<td>-25</td>
			<td>Mass Media</td>
			<td>Kris S.</td>
			<td>Pasco S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Tampa Bay Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>1-1-9-1-0</td>
			<td>1.18</td>
			<td>0.17</td>
			<td>12</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/16</td>
				<td>Adirondack Summer</td>
			<td>-55</td>
			<td>Thunder Gulch</td>
			<td>Caerleon</td>
			<td>Dania Beach S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>TURF</td>
			<td>8.00</td>
			<td>7-0-7-4-0</td>
			<td>1.40</td>
			<td>0.56</td>
			<td>18</td>
			 </tr>


			 
				 <tr>
				 				<td >01/17</td>
				<td>Fort Hughes</td>
			<td>-83</td>
			<td>Henny Hughes</td>
			<td>Roar</td>
			<td>Jimmy Winkfield S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>3-0-3-0-0</td>
			<td>3.00</td>
			<td>1.00</td>
			<td>6</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/17</td>
				<td>Caleb's Posse</td>
			<td>-13</td>
			<td>Posse</td>
			<td>Slewacide</td>
			<td>Smarty Jones S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>4-0-8-0-0  	</td>
			<td>2.00</td>
			<td>0.67</td>
			<td>12</td>
			 </tr>


			 
				 <tr>
				 				<td >01/17</td>
				<td>Indian Winter</td>
			<td>-7</td>
			<td>Indian Charlie</td>
			<td>Dixieland Band</td>
			<td>San Pedro S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>6.50</td>
			<td>2-2-4-0-0</td>
			<td>3.00</td>
			<td>0.75</td>
			<td>8</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/22</td>
				<td>Wilkinson</td>
			<td>-48</td>
			<td>Lemon Drop Kid</td>
			<td>Afternoon Deelites</td>
			<td>Lecomte S</td>
			<td>3</td>
			<td>FG</td>
			<td>DIRT</td>
			<td>8.32</td>
			<td>6-0-14-4-0</td>
			<td>1.18</td>
			<td>0.33</td>
			<td>24</td>
			 </tr>


			 
				 <tr>
				 				<td >01/22</td>
				<td>Aces N Kings</td>
			<td>-12</td>
			<td>Jet Phone</td>
			<td>Wayne's Crane</td>
			<td>Groovy S</td>
			<td>R</td>
			<td>HOU</td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>3-0-7-0-0</td>
			<td>1-86</td>
			<td>0.60</td>
			<td>10</td>
			 </tr>


			 
					<tr class="odd">
									<td >01/22</td>
				<td>Beau Wizer</td>
			<td>12</td>
			<td>Premeditation</td>
			<td>Beau Genius</td>
			<td>Pepsi Cola H</td>
			<td>R</td>
			<td>SUN</td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>1-3-4-0-0</td>
			<td>3.00</td>
			<td>0.63</td>
			<td>8</td>
			 </tr>


			 
				 <tr>
				 				<td >01/30</td>
				<td>Dialed In</td>
			<td>-50</td>
			<td>Mineshaft</td>
			<td>Miss Doolittle</td>
			<td>Holy Bull S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>9-8-13-0-0</td>
			<td>3.62</td>
			<td>0.87</td>
			<td>30</td>
			 </tr>


			 
					<tr class="odd">
									<td >02/05</td>
				<td>Toby's Corner</td>
			<td>-7</td>
			<td>Bellamy Road</td>
			<td>Mister Frisky</td>
			<td>Whirlaway                S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>1-4-3-2-0</td>
			<td>1.86</td>
			<td>0.40  	</td>
			<td>10</td>
			 </tr>


			 
				 <tr>
				 				<td >02/05</td>
				<td>Su Casa G Casa</td>
			<td>-8</td>
			<td>During</td>
			<td>Known Fact</td>
			<td>Louisiana                Premier Night Prince S</td>
			<td>R</td>
			<td><a href='/racetrack?name=Delta Downs' title='Delta Downs'>Delta Downs</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>5-2-3-0-0</td>
			<td>5.67</td>
			<td>1.20</td>
			<td>10</td>
			 </tr>


			 
					<tr class="odd">
									<td >02/05</td>
				<td>Trubs</td>
			<td>-34</td>
			<td>First Samurai</td>
			<td>Seeking The Gold</td>
			<td>Black                Gold S</td>
			<td>L</td>
			<td>FG</td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>7-1-17-1-0</td>
			<td>1.74</td>
			<td>0.54</td>
			<td>26</td>
			 </tr>


			 
				 <tr>
				 				<td >02/05</td>
				<td>Twinspired</td>
			<td>-30</td>
			<td>Harlan's Holiday</td>
			<td>El Prado</td>
			<td>Webn                S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>8.00</td>
			<td>2-1-9-2-0</td>
			<td>1.15</td>
			<td>0.21</td>
			<td>14</td>
			 </tr>


			 
					<tr class="odd">
									<td >02/06</td>
				<td>Master Dunker (by Dq)</td>
			<td>-39</td>
			<td>Imperialism</td>
			<td>Buckfinder</td>
			<td>Hallandale                Beach S</td>
			<td>L</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>TURF</td>
			<td>8.50</td>
			<td>3-4-11-2-2</td>
			<td>1.32</td>
			<td>0.18</td>
			<td>22</td>
			 </tr>


			 
				 <tr>
				 				<td >02/12</td>
				<td>Silver Medallion</td>
			<td>-48</td>
			<td>Badge Of Silver</td>
			<td>Stalwart</td>
			<td>El                Camino Real Derby</td>
			<td>3</td>
			<td><a href='/racetrack?name=Golden Gate Fields' title='Golden Gate Fields'>Golden Gate Fields</a></td>
			<td>AWS</td>
			<td>9.00</td>
			<td>3-2-4-0-1</td>
			<td>2.33</td>
			<td>0.60</td>
			<td>10</td>
			 </tr>


			 
					<tr class="odd">
									<td >02/12</td>
				<td>Anthony's Cross</td>
			<td>-49</td>
			<td>Indian Charlie</td>
			<td>Unbridled</td>
			<td>Robert                B. Lewis S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>6-9-7-0-2</td>
			<td>3.36</td>
			<td>0.71</td>
			<td>24</td>
			 </tr>


			 
				 <tr>
				 				<td >02/12</td>
				<td>Brethren</td>
			<td>-21</td>
			<td>Distorted Humor</td>
			<td>A.p. Indy</td>
			<td>Sam                F. Davis S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Tampa Bay Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>12-7-16 -0-1</td>
			<td>3.00</td>
			<td>0.81</td>
			<td>36</td>
			 </tr>


			 
					<tr class="odd">
									<td >02/19</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Risen                Star S</td>
			<td>2</td>
			<td>FG</td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >02/20</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>San                Vicente S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >02/21</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Southwest                S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Gentilly                S</td>
			<td>R</td>
			<td>FG</td>
			<td>TURF</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Fountain                Of Youth S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Hutcheson                S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Rainbow                S</td>
			<td>R</td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Borderland                Derby</td>
			<td></td>
			<td>SUN</td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Turf                Paradise Derby</td>
			<td></td>
			<td>TUP</td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >02/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Miracle                Wood S</td>
			<td></td>
			<td>LRL</td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/05</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Gotham                S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/05</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Fred                "cappy" Capossela S</td>
			<td></td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/05</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Grindstone                S</td>
			<td></td>
			<td>FG</td>
			<td>TURF</td>
			<td>7.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/05</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>John                Battaglia Memorial S</td>
			<td></td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/05</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Mountain                Valley S</td>
			<td></td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/12</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Palm                Beach S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>TURF</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/12</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>San                Felipe S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/12</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Tampa                Bay Derby</td>
			<td>2</td>
			<td><a href='/racetrack?name=Tampa Bay Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/14</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Obs                Championship S (c/g)</td>
			<td>R</td>
			<td>OTC</td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/14</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Obs                Sprint S (c/g)</td>
			<td>R</td>
			<td>OTC</td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/19</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Rebel                S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/19</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Private                Terms S</td>
			<td></td>
			<td>LRL</td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/25</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Cresent                City Derby</td>
			<td>R</td>
			<td>FG</td>
			<td>DIRT</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Louisiana                Derby</td>
			<td>2</td>
			<td>FG</td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Uae                Derby</td>
			<td>UAE-2</td>
			<td><a href='/racetrack?name=Meadowlands Racetrack' title='Meadowlands Racetrack'>Meadowlands Racetrack</a></td>
			<td>AWS</td>
			<td>9.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Vinery                Racing Spiral S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Rushaway                S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >03/26</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Hansel                S</td>
			<td></td>
			<td><a href='/racetrack?name=Stampede Park' title='Stampede Park'>Stampede Park</a></td>
			<td>AWS</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >03/27</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Sunland                Derby</td>
			<td>3</td>
			<td>SUN</td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/01</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Transylvania                S</td>
			<td>3</td>
			<td>KEE</td>
			<td>TURF</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/03</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Florida                Derby</td>
			<td>1</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/03</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Swale                S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Gulfstream Park' title='Gulfstream Park'>Gulfstream Park</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Wood                Memorial S</td>
			<td>1</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Bay                Shore S</td>
			<td>3</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Santa                Anita Derby</td>
			<td>1</td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Toyota                Blue Grass S</td>
			<td></td>
			<td>KEE</td>
			<td>AWS</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Illinois                Derby</td>
			<td>3</td>
			<td><a href='/racetrack?name=Hawthorne Racecourse' title='Hawthorne Racecourse'>Hawthorne Racecourse</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Dayton                Andrews Dodge Sophomore Turf</td>
			<td></td>
			<td><a href='/racetrack?name=Tampa Bay Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
			<td>TURF</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/09</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Obs                Sophomore S</td>
			<td></td>
			<td><a href='/racetrack?name=Tampa Bay Downs' title='Tampa Bay Downs'>Tampa Bay Downs</a></td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/10</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Lafayette                S</td>
			<td></td>
			<td>KEE</td>
			<td>AWS</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/10</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>La                Puente S</td>
			<td></td>
			<td><a href='/racetrack?name=Sandown Park' title='Sandown Park'>Sandown Park</a></td>
			<td>TURF</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/16</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Blue                And Gold S</td>
			<td></td>
			<td>CT</td>
			<td>DIRT</td>
			<td>7.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/16</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Arkansas                Derby</td>
			<td>1</td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/16</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Northern                Spur S</td>
			<td></td>
			<td><a href='/racetrack?name=Oaklawn Park' title='Oaklawn Park'>Oaklawn Park</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/16</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Golden                Poppy S</td>
			<td></td>
			<td><a href='/racetrack?name=Golden Gate Fields' title='Golden Gate Fields'>Golden Gate Fields</a></td>
			<td>TURF</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/19</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Khey                Y-96 Sprint S</td>
			<td></td>
			<td>SUN</td>
			<td>DIRT</td>
			<td>6.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/23</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Jerome                H</td>
			<td>2</td>
			<td><a href='/racetrack?name=Aqueduct Racetrack' title='Aqueduct Racetrack'>Aqueduct Racetrack</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >04/23</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Coolmore                Lexington S</td>
			<td>3</td>
			<td>KEE</td>
			<td>AWS</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >04/30</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Derby                Trial</td>
			<td>3</td>
			<td>CD</td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >05/07</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Kentucky                Derby</td>
			<td>1</td>
			<td>CD</td>
			<td>DIRT</td>
			<td>10.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >05/14</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Peter                Pan S</td>
			<td>2</td>
			<td><a href='/racetrack?name=Belmont Park' title='Belmont Park'>Belmont Park</a></td>
			<td>DIRT</td>
			<td>9.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >05/14</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Alcatraz                S</td>
			<td></td>
			<td><a href='/racetrack?name=Golden Gate Fields' title='Golden Gate Fields'>Golden Gate Fields</a></td>
			<td>TURF</td>
			<td>8.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >05/21</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Preakness                S</td>
			<td>1</td>
			<td><a href='/racetrack?name=Pimlico Race Course' title='Pimlico Race Course'>Pimlico Race Course</a></td>
			<td>DIRT</td>
			<td>9.50</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
				 <tr>
				 				<td >05/21</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Sunray                Park & Casino H</td>
			<td></td>
			<td><a href='/racetrack?name=SunRay Park' title='SunRay Park'>SunRay Park</a></td>
			<td>DIRT</td>
			<td>8.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 
					<tr class="odd">
									<td >06/11</td>
				<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td>Belmont                S</td>
			<td>1</td>
			<td><a href='/racetrack?name=Belmont Park' title='Belmont Park'>Belmont Park</a></td>
			<td>DIRT</td>
			<td>12.00</td>
			<td>----</td>
			<td></td>
			<td></td>
			<td></td>
			 </tr>


			 			 </table>

			</div>
			</div>
		<div id="box-shd"></div><!-- === SHADOW === -->
		