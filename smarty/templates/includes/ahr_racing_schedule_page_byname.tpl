<div class="schedule">
	<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Arapahoe    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Arlington    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Arlington Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Ascot    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Australia    *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Australia *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Ayr Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Balmoral Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Bath    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Belmont Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Belterra Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Belterrra Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Bluffs Run -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Bluffs Run -    Greyhound *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Brighton    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Buffalo    Raceway		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Canterbury    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Carlisle    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Catterick    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Charles Town		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Chepstow    Raceourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Chester    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Cork    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Delaware Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Delta Downs    (QH)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Derby Lane -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Doncaster    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Dundalk    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Ellis Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Emerald Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Epsom Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Evangeline    Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Fairmount Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Fairview    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Finger Lakes		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Fort Erie		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Gulfstream    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Hamilton Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Happy Valley    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Harrah&rsquo;s    Philadelphia		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Harrington    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Harrington    Raceway		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Hastings		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Hoosier Park    Harness		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Indiana Downs    (T)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Kempton Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Laurel Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Leopardstown    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Lingfield Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Lone Star    Park  (QH)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Lone Star Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Lone Star Park    (QH)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Los Alamitos    (QH)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Los Alamitos    (T)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Louisiana    Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Maywood Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Meadowlands    Harness		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Mohawk		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Monmouth Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Monticello    Raceway		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Mountaineer		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Naas    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Navan    Raceourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Newbury    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Newmarket    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Northfield    Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Northfield    Park 		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Orange Park -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Orange Park -    Greyhound 		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Palm Beach -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Parx Racing at    Philadelphia Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Penn National		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Perth    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Pocono Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Pontefract    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Prairie    Meadows		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Presque Isle    Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Retama Park    (QH)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Ripon    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Roscommon    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Ruidoso Downs		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Sacramento		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Salisbury    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Saratoga    Harness		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Scottsville		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Sha Tin    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Southland -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Southland -    Greyhound *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Southwell    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Stratford    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Sunray Park		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			The Meadows		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Thistledown		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Tipperary    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Tri-State -    Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Tri-State -    Greyhound *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Tri-State - Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Turffontein		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Uttoxeter    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Vaal    Racecourse 		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Wheeling Downs    - Greyhound		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Wheeling Downs    - Greyhound *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Wheeling Downs    - Greyhound 		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Wheeling Downs - Greyhound *		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Windsor    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Wolverhampton		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Woodbine (T)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Wednesday, July 08, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Woodbine  (T)		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Sunday, July 12, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Worcester    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Yonkers    Raceway		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Monday, July 06, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Tuesday, July 07, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Thursday, July 09, 2015</span>
			</div>
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			Yonkers Raceway		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			York    Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Saturday, July 11, 2015</span>
			</div>
			</div>
	</div>
		<div class="sItem">
		<div class="sName">		
			<i class="fa fa-map-marker"></i>
			York Racecourse		</div>
		<div class="sDay">
				<div class="sRace">
				<span class="race">Friday, July 10, 2015</span>
			</div>
			</div>
	</div>
	</div>