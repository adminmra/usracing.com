<table id="infoEntries" class="table table-condensed table-striped table-bordered" border="0" cellspacing="3" cellpadding="3" width="100%">
<tbody>
<tr>
<th width="10%"><strong>Date</strong></th>
<th width="12%"><strong>Stakes</strong></th>
<th width="12%"><strong>Track</strong></th>
<!-- <th width="10%"><strong>Grade</strong></th> -->
<th width="10%"><strong>Purses</strong></th>
<!-- <th width="10%"><strong>Age/Sex</strong></th>
<th width="10%"><strong>DS</strong></th> -->
<th width="12%"><strong>Horse</strong></th>
<!-- <th width="12%"><strong>Jockey</strong></th>
<th width="12%"><strong>Trainer</strong></th> -->
</tr>
<tr>	<td class="num">Jan 01</td><!--hi mr anders-->
	<td>Old Hat S.</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$98,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>6.0</td> -->
	<td>KAUAI KATIE</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 05</td>
	<td>Jerome Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.32</td> -->
	<td>VYJACK</td>
<!-- 	<td>C. Velasquez</td>
	<td>R. Rodriguez</td> -->
</tr>
<tr>	<td class="num">Jan 05</td>
	<td>San Pasqual Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5</td> -->
	<td>COIL</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 06</td>
	<td>Monrovia Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>6.5 T</td> -->
	<td>MIZDIRECTION</td>
<!-- 	<td>M. Smith</td>
	<td>M. Puype</td> -->
</tr>
<tr>	<td class="num">Jan 12</td>
	<td>Fort Lauderdale Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>MUCHO MAS MACHO</td>
<!-- 	<td>J. Leyva</td>
	<td>H. Collazo</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 12</td>
	<td>San Fernando Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>FED BIZ</td>
<!-- 	<td>M. Smith</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Jan 13</td>
	<td>San Gabriel Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>JERANIMO</td>
<!-- 	<td>R. Bejarano</td>
	<td>M. Pender</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 19</td>
	<td>Lecomte Stakes</td>
	<td>Fair Grounds</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.32</td> -->
	<td>OXBOW</td>
<!-- 	<td>J. Court</td>
	<td>D. Lukas</td> -->
</tr>
<tr>	<td class="num">Jan 19</td>
	<td>Palos Verdes Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.0</td> -->
	<td>SAHARA SKY</td>
<!-- 	<td>J. Talamo</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 20</td>
	<td>La Canada Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>MORE CHOCOLATE</td>
<!-- 	<td>G. Gomez</td>
	<td>J. Sadler</td> -->
</tr>
<tr>	<td class="num">Jan 26</td>
	<td>Santa Ysabel Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>FIFTYSHADESOFHAY</td>
<!-- 	<td>R. Bejarano</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 26</td>
	<td>John B. Connally Turf Cup</td>
	<td>Sam Houston</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>SWIFT WARRIOR</td>
<!-- 	<td>J. Espinoza</td>
	<td>J. Terranova, II</td> -->
</tr>
<tr>	<td class="num">Jan 26</td>
	<td>Forward Gal Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>7.0</td> -->
	<td>KAUAI KATIE</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jan 26</td>
	<td>Holy Bull Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>ITSMYLUCKYDAY</td>
<!-- 	<td>E. Trujillo</td>
	<td>E. Plesa, Jr.</td> -->
</tr>
<tr>	<td class="num">Jan 26</td>
	<td>Colonel E.R. Bradley Handicap</td>
	<td>Fair Grounds</td>
	<!-- <td>III</td> -->
	<td>$125,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>OPTIMIZER</td>
<!-- 	<td>J. Court</td>
	<td>D. Lukas</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 02</td>
	<td>Florida Oaks</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>TAPICAT</td>
<!-- 	<td>J. Rosario</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Feb 02</td>
	<td>Endeavour Stakes</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>OLD TUNE</td>
<!-- 	<td>J. Rosario</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 02</td>
	<td>Sam F. Davis Stakes</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>FALLING SKY</td>
<!-- 	<td>J. Espinoza</td>
	<td>J. Terranova, II</td> -->
</tr>
<tr>	<td class="num">Feb 02</td>
	<td>Arcadia Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>SUGGESTIVE BOY</td>
<!-- 	<td>J. Talamo</td>
	<td>R. McAnally</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 02</td>
	<td>Strub Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>4yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>GUILT TRIP</td>
<!-- 	<td>J. Talamo</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Feb 02</td>
	<td>Robert B. Lewis Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>FLASHBACK</td>
<!-- 	<td>J. Leparoux</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 02</td>
	<td>Hutcheson Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>7.0</td> -->
	<td>HONORABLE DILLON</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>E. Kenneally</td> -->
</tr>
<tr>	<td class="num">Feb 02</td>
	<td>Toboggan Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>HEAD HEART HOOF</td>
<!-- 	<td>C. Velasquez</td>
	<td>R. Rodriguez</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 02</td>
	<td>Withers Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>REVOLUTIONARY</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Feb 03</td>
	<td>San Antonio Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>GAME ON DUDE</td>
<!-- 	<td>M. Smith</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 09</td>
	<td>San Marcos Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>SLIM SHADEY</td>
<!-- 	<td>G. Stevens</td>
	<td>S. Callaghan</td> -->
</tr>
<tr>	<td class="num">Feb 09</td>
	<td>Suwannee River Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>CHANNEL LADY</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 09</td>
	<td>Gulfstream Park Sprint</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>7.0</td> -->
	<td>FORT LOUDON</td>
<!-- 	<td>J. Lezcano</td>
	<td>N. Zito</td> -->
</tr>
<tr>	<td class="num">Feb 09</td>
	<td>Gulfstream Park Turf Handicap</td>
	<td>Gulfstream Park</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>POINT OF ENTRY</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 09</td>
	<td>Donn Handicap</td>
	<td>Gulfstream Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>GRAYDAR</td>
<!-- 	<td>E. Prado</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Feb 10</td>
	<td>Hurricane Bertie Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>6.5</td> -->
	<td>GOLDEN MYSTERY</td>
<!-- 	<td>L. Saez</td>
	<td>M. Wolfson</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 16</td>
	<td>Tampa Bay Stakes</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>SWIFT WARRIOR</td>
<!-- 	<td>J. Espinoza</td>
	<td>J. Terranova, II</td> -->
</tr>
<tr>	<td class="num">Feb 16</td>
	<td>Santa Maria Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>GREAT HOT</td>
<!-- 	<td>G. Stevens</td>
	<td>A. Avila</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 16</td>
	<td>Barbara Fritchie Handicap</td>
	<td>Laurel Park</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>7.0</td> -->
	<td>FUNNYS APPROVAL</td>
<!-- 	<td>J. Vargas</td>
	<td>J. Lopez</td> -->
</tr>
<tr>	<td class="num">Feb 16</td>
	<td>The Very One Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>STARFORMER</td>
<!-- 	<td>E. Prado</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 16</td>
	<td>El Camino Real Derby</td>
	<td>Golden Gate Fields</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>DICE FLAVOR</td>
<!-- 	<td>J. Valdivia, Jr.</td>
	<td>P. Gallagher</td> -->
</tr>
<tr>	<td class="num">Feb 17</td>
	<td>Sabin Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>ROYAL DELTA</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 17</td>
	<td>San Vicente Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>7.0</td> -->
	<td>SHAKIN IT UP</td>
<!-- 	<td>D. Flores</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Feb 18</td>
	<td>Buena Vista Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>MIZDIRECTION</td>
<!-- 	<td>M. Smith</td>
	<td>M. Puype</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 18</td>
	<td>Southwest Stakes</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>SUPER NINETY NINE</td>
<!-- 	<td>R. Bejarano</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Feb 18</td>
	<td>General George Handicap</td>
	<td>Laurel Park</td>
	<!-- <td>III</td> -->
	<td>$250,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>7.0</td> -->
	<td>JAVERRE</td>
<!-- 	<td>J. Acosta</td>
	<td>C. Lynch</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 23</td>
	<td>San Carlos Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>7.0</td> -->
	<td>SAHARA SKY</td>
<!-- 	<td>J. Talamo</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr>	<td class="num">Feb 23</td>
	<td>Canadian Turf Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>DATA LINK</td>
<!-- 	<td>J. Castellano</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 23</td>
	<td>Davona Dale Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>LIVE LIVELY</td>
<!-- 	<td>J. Rosario</td>
	<td>M. Hennig</td> -->
</tr>
<tr>	<td class="num">Feb 23</td>
	<td>Fountain of Youth Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>ORB</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 23</td>
	<td>Mineshaft Handicap</td>
	<td>Fair Grounds</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5</td> -->
	<td>MARK VALESKI</td>
<!-- 	<td>R. Napravnik</td>
	<td>J. Jones</td> -->
</tr>
<tr>	<td class="num">Feb 23</td>
	<td>Fair Grounds Handicap</td>
	<td>Fair Grounds</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>OPTIMIZER</td>
<!-- 	<td>J. Court</td>
	<td>D. Lukas</td> -->
</tr>
<tr class="odd">	<td class="num">Feb 23</td>
	<td>Rachel Alexandra Stakes</td>
	<td>Fair Grounds</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>UNLIMITED BUDGET</td>
<!-- 	<td>R. Napravnik</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Feb 23</td>
	<td>Risen Star Stakes</td>
	<td>Fair Grounds</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>IVE STRUCK A NERVE</td>
<!-- 	<td>J. Graham</td>
	<td>J. Desormeaux</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 02</td>
	<td>Las Virgenes Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0</td> -->
	<td>BEHOLDER</td>
<!-- 	<td>G. Gomez</td>
	<td>R. Mandella</td> -->
</tr>
<tr>	<td class="num">Mar 02</td>
	<td>Frank E. Kilroe Mile</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>SUGGESTIVE BOY</td>
<!-- 	<td>J. Talamo</td>
	<td>R. McAnally</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 02</td>
	<td>Santa Anita Handicap</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>10.0</td> -->
	<td>GAME ON DUDE</td>
<!-- 	<td>M. Smith</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Mar 02</td>
	<td>Swale Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>7.0</td> -->
	<td>CLEARLY NOW</td>
<!-- 	<td>L. Saez</td>
	<td>B. Lynch</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 02</td>
	<td>Top Flight Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>SUMMER APPLAUSE</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. Brown</td> -->
</tr>
<tr>	<td class="num">Mar 02</td>
	<td>Tom Fool Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>COMMA TO THE TOP</td>
<!-- 	<td>J. Rosario</td>
	<td>P. Miller</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 02</td>
	<td>Gotham Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>VYJACK</td>
<!-- 	<td>J. Rosario</td>
	<td>R. Rodriguez</td> -->
</tr>
<tr>	<td class="num">Mar 03</td>
	<td>Palm Beach Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>RYDILLUC</td>
<!-- 	<td>E. Prado</td>
	<td>G. Contessa</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 09</td>
	<td>Hillsborough Stakes</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>OLD TUNE</td>
<!-- 	<td>J. Rosario</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Mar 09</td>
	<td>Tampa Bay Derby</td>
	<td>Tampa Bay Downs</td>
	<!-- <td>II</td> -->
	<td>$350,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>VERRAZANO</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 09</td>
	<td>San Felipe Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>HEAR THE GHOST</td>
<!-- 	<td>C. Nakatani</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr>	<td class="num">Mar 09</td>
	<td>Razorback Handicap</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5</td> -->
	<td>CYBER SECRET</td>
<!-- 	<td>R. Albarado</td>
	<td>L. Whiting</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 09</td>
	<td>Honeybee Stakes</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>ROSE TO GOLD</td>
<!-- 	<td>C. Borel</td>
	<td>S. Santoro</td> -->
</tr>
<tr>	<td class="num">Mar 09</td>
	<td>Gulfstream Park Handicap</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.0</td> -->
	<td>DISCREET DANCER</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 10</td>
	<td>Las Flores Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>6.5</td> -->
	<td>RUMOR</td>
<!-- 	<td>M. Smith</td>
	<td>R. Mandella</td> -->
</tr>
<tr>	<td class="num">Mar 16</td>
	<td>San Luis Rey Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>BRIGHT THOUGHT</td>
<!-- 	<td>V. Espinoza</td>
	<td>J. Gutierrez</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 16</td>
	<td>Santa Margarita Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>JOYFUL VICTORY</td>
<!-- 	<td>R. Napravnik</td>
	<td>J. Jones</td> -->
</tr>
<tr>	<td class="num">Mar 16</td>
	<td>Azeri Stakes</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>TIZ MIZ SUE</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>S. Hobby</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 16</td>
	<td>Rebel Stakes</td>
	<td>Oaklawn Park</td>
	<!-- <td>II</td> -->
	<td>$600,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>WILL TAKE CHARGE</td>
<!-- 	<td>J. Court</td>
	<td>D. Lukas</td> -->
</tr>
<tr>	<td class="num">Mar 16</td>
	<td>Honey Fox Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>CENTRE COURT</td>
<!-- 	<td>J. Leparoux</td>
	<td>G. Arnold, II</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 17</td>
	<td>Inside Information Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>7.0</td> -->
	<td>AUBBY K</td>
<!-- 	<td>E. Prado</td>
	<td>R. Nicks</td> -->
</tr>
<tr>	<td class="num">Mar 23</td>
	<td>Fathead Bourbonette Oaks</td>
	<td>Turfway Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0</td> -->
	<td>SILSITA</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 23</td>
	<td>Horseshoe Casino Cincinnati Spiral Stakes</td>
	<td>Turfway Park</td>
	<!-- <td>III</td> -->
	<td>$550,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>BLACK ONYX</td>
<!-- 	<td>J. Bravo</td>
	<td>K. Breen</td> -->
</tr>
<tr>	<td class="num">Mar 23</td>
	<td>Tokyo City Cup</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>12.0</td> -->
	<td>SKY KINGDOM</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 23</td>
	<td>Pan American Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>TWILIGHT ECLIPSE</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Albertrani</td> -->
</tr>
<tr>	<td class="num">Mar 23</td>
	<td>Excelsior Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0</td> -->
	<td>LAST GUNFIGHTER</td>
<!-- 	<td>R. Maragh</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 24</td>
	<td>Santa Ana Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>TIZ FLIRTATIOUS</td>
<!-- 	<td>J. Leparoux</td>
	<td>M. Jones</td> -->
</tr>
<tr>	<td class="num">Mar 24</td>
	<td>Sunland Derby</td>
	<td>Sunland Park</td>
	<!-- <td>III</td> -->
	<td>$800,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>GOVERNOR CHARLIE</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 30</td>
	<td>Skip Away Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.5</td> -->
	<td>CIGAR STREET</td>
<!-- 	<td>J. Velazquez</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Mar 30</td>
	<td>Appleton Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>ZA APPROVAL</td>
<!-- 	<td>J. Lezcano</td>
	<td>C. Clement</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 30</td>
	<td>Rampart Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>CIAO BELLA</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Mar 30</td>
	<td>Orchid Stakes</td>
	<td>Gulfstream Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>REGALO MIA</td>
<!-- 	<td>L. Contreras</td>
	<td>M. Nihei</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 30</td>
	<td>Gulfstream Park Oaks</td>
	<td>Gulfstream Park</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0</td> -->
	<td>DREAMING OF JULIA</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Mar 30</td>
	<td>Florida Derby</td>
	<td>Gulfstream Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>ORB</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 30</td>
	<td>New Orleans Handicap</td>
	<td>Fair Grounds</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>GRAYDAR</td>
<!-- 	<td>E. Prado</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Mar 30</td>
	<td>Fair Grounds Oaks</td>
	<td>Fair Grounds</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>UNLIMITED BUDGET</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Mar 30</td>
	<td>Louisiana Derby</td>
	<td>Fair Grounds</td>
	<!-- <td>II</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>REVOLUTIONARY</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Apr 05</td>
	<td>Transylvania Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>JACK MILTON</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 06</td>
	<td>Potrero Grande Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.5</td> -->
	<td>JIMMY CREED</td>
<!-- 	<td>G. Gomez</td>
	<td>R. Mandella</td> -->
</tr>
<tr>	<td class="num">Apr 06</td>
	<td>Santa Anita Oaks</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>BEHOLDER</td>
<!-- 	<td>G. Gomez</td>
	<td>R. Mandella</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 06</td>
	<td>Central Bank Ashland Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>EMOLLIENT</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Apr 06</td>
	<td>Ruffian Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.0</td> -->
	<td>WITHGREATPLEASURE</td>
<!-- 	<td>J. Velazquez</td>
	<td>D. Nunn</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 06</td>
	<td>Gazelle Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0</td> -->
	<td>CLOSE HATCHES</td>
<!-- 	<td>J. Rosario</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Apr 06</td>
	<td>Carter Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>7.0</td> -->
	<td>SWAGGER JACK</td>
<!-- 	<td>I. Ortiz, Jr.</td>
	<td>M. Wolfson</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 06</td>
	<td>Wood Memorial Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>VERRAZANO</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Apr 07</td>
	<td>Providencia Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>SCARLET STRIKE</td>
<!-- 	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 10</td>
	<td>Fantasy Stakes</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$400,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>ROSE TO GOLD</td>
<!-- 	<td>C. Borel</td>
	<td>S. Santoro</td> -->
</tr>
<tr>	<td class="num">Apr 12</td>
	<td>Apple Blossom Handicap</td>
	<td>Oaklawn Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>ON FIRE BABY</td>
<!-- 	<td>J. Johnson</td>
	<td>G. Hartlage</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 13</td>
	<td>Las Cienegas Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>6.5 T</td> -->
	<td>MIZDIRECTION</td>
<!-- 	<td>D. Flores</td>
	<td>M. Puype</td> -->
</tr>
<tr>	<td class="num">Apr 13</td>
	<td>Count Fleet Sprint Handicap</td>
	<td>Oaklawn Park</td>
	<!-- <td>III</td> -->
	<td>$250,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.0</td> -->
	<td>JUSTIN PHILLIP</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 13</td>
	<td>Oaklawn Handicap</td>
	<td>Oaklawn Park</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>CYBER SECRET</td>
<!-- 	<td>R. Albarado</td>
	<td>L. Whiting</td> -->
</tr>
<tr>	<td class="num">Apr 13</td>
	<td>Arkansas Derby</td>
	<td>Oaklawn Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>OVERANALYZE</td>
<!-- 	<td>R. Bejarano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 13</td>
	<td>Shakertown Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>5.5 T</td> -->
	<td>HAVELOCK</td>
<!-- 	<td>G. Gomez</td>
	<td>D. Miller</td> -->
</tr>
<tr>	<td class="num">Apr 13</td>
	<td>Madison Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>7.0</td> -->
	<td>LAST FULL MEASURE</td>
<!-- 	<td>C. Nakatani</td>
	<td>P. Oliver</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 13</td>
	<td>Jenny Wiley Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>CENTRE COURT</td>
<!-- 	<td>J. Leparoux</td>
	<td>G. Arnold, II</td> -->
</tr>
<tr>	<td class="num">Apr 13</td>
	<td>Distaff Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>6.0</td> -->
	<td>CLUSTER OF STARS</td>
<!-- 	<td>J. Alvarado</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 14</td>
	<td>Beaumont Stakes</td>
	<td>Keeneland</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>7.0</td> -->
	<td>CIAO BELLA LUNA</td>
<!-- 	<td>J. Rosario</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr>	<td class="num">Apr 18</td>
	<td>Appalachian Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>UNBELIEVABLE DREAM</td>
<!-- 	<td>J. Rosario</td>
	<td>B. Tagg</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 19</td>
	<td>Hilliard Lyons Doubledogdare Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>ICE CREAM SILENCE</td>
<!-- 	<td>R. Napravnik</td>
	<td>G. Arnold, II</td> -->
</tr>
<tr>	<td class="num">Apr 20</td>
	<td>San Simeon Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.5 T</td> -->
	<td>CHIPS ALL IN</td>
<!-- 	<td>T. Baze</td>
	<td>J. Mullins</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 20</td>
	<td>Santa Barbara Handicap</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>LADY OF SHAMROCK</td>
<!-- 	<td>R. Bejarano</td>
	<td>J. Sadler</td> -->
</tr>
<tr>	<td class="num">Apr 20</td>
	<td>Coolmore Lexington Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>WINNING CAUSE</td>
<!-- 	<td>J. Leparoux</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 20</td>
	<td>Sixty Sails Handicap</td>
	<td>Hawthorne</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>DISPOSABLEPLEASURE</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Apr 20</td>
	<td>Illinois Derby</td>
	<td>Hawthorne</td>
	<!-- <td>III</td> -->
	<td>$750,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>DEPARTING</td>
<!-- 	<td>B. Hernandez, Jr.</td>
	<td>A. Stall, Jr.</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 20</td>
	<td>Charles Town Classic</td>
	<td>Charles Town</td>
	<!-- <td>II</td> -->
	<td>$1,500,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>GAME ON DUDE</td>
<!-- 	<td>M. Smith</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Apr 21</td>
	<td>Ben Ali Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>SUCCESSFUL DAN</td>
<!-- 	<td>J. Leparoux</td>
	<td>C. LoPresti</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 21</td>
	<td>San Juan Capistrano Handicap</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>14.0 T</td> -->
	<td>INTERACTION</td>
<!-- 	<td>J. Talamo</td>
	<td>R. McAnally</td> -->
</tr>
<tr>	<td class="num">Apr 25</td>
	<td>Bewitch Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>STRATHNAVER</td>
<!-- 	<td>J. Rosario</td>
	<td>H. Motion</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 26</td>
	<td>Elkhorn Stakes</td>
	<td>Keeneland</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>DARK COVE</td>
<!-- 	<td>J. Rosario</td>
	<td>M. Maker</td> -->
</tr>
<tr>	<td class="num">Apr 27</td>
	<td>Westchester Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0</td> -->
	<td>FLAT OUT</td>
<!-- 	<td>J. Alvarado</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 27</td>
	<td>Derby Trial Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$175,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.0</td> -->
	<td>FORTY TALES</td>
<!-- 	<td>J. Rosario</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Apr 27</td>
	<td>San Francisco Mile</td>
	<td>Golden Gate Fields</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>TIGAH</td>
<!-- 	<td>A. Quinonez</td>
	<td>J. Sadler</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 27</td>
	<td>Texas Mile Stakes</td>
	<td>Lone Star Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0</td> -->
	<td>MASTER RICK</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr>	<td class="num">Apr 28</td>
	<td>Wilshire Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>HALO DOLLY</td>
<!-- 	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Apr 28</td>
	<td>Whimsical Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$152,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.0</td> -->
	<td>ACTING NAUGHTY</td>
<!-- 	<td>T. Pizarro</td>
	<td>D. MacRae</td> -->
</tr>
<tr>	<td class="num">May 03</td>
	<td>Eight Belles Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>7.0</td> -->
	<td>SO MANY WAYS</td>
<!-- 	<td>G. Gomez</td>
	<td>T. Amoss</td> -->
</tr>
<tr class="odd">	<td class="num">May 03</td>
	<td>American Turf</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>NOBLE TUNE</td>
<!-- 	<td>J. Castellano</td>
	<td>C. Brown</td> -->
</tr>
<tr>	<td class="num">May 03</td>
	<td>La Troienne Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>AUTHENTICITY</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">May 03</td>
	<td>Alysheba Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>8.5</td> -->
	<td>TAKE CHARGE INDY</td>
<!-- 	<td>R. Napravnik</td>
	<td>P. Byrne</td> -->
</tr>
<tr>	<td class="num">May 03</td>
	<td>Kentucky Oaks</td>
	<td>Churchill Downs</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3f</td> -->
<!-- 	<td>9.0</td> -->
	<td>PRINCESS OF SLYMAR</td>
<!-- 	<td>M. Smith</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">May 04</td>
	<td>Twin Spires Turf Sprint</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$125,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>5.0 T</td> -->
	<td>BERLINO DI TIGER</td>
<!-- 	<td>L. Goncalves</td>
	<td>E. Caramori</td> -->
</tr>
<tr>	<td class="num">May 04</td>
	<td>Humana Distaff</td>
	<td>Churchill Downs</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>4up f/m</td> -->
<!-- 	<td>7.0</td> -->
	<td>AUBBY K</td>
<!-- 	<td>E. Prado</td>
	<td>R. Nicks</td> -->
</tr>
<tr class="odd">	<td class="num">May 04</td>
	<td>Churchill Downs Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>7.0</td> -->
	<td>DELAUNAY</td>
<!-- 	<td>R. Napravnik</td>
	<td>T. Amoss</td> -->
</tr>
<tr>	<td class="num">May 04</td>
	<td>Woodford Reserve Turf Classic Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>9.0</td> -->
	<td>WISE DAN</td>
<!-- 	<td>J. Lezcano</td>
	<td>C. LoPresti</td> -->
</tr>
<tr class="odd">	<td class="num">May 04</td>
	<td>Kentucky Derby</td>
	<td>Churchill Downs</td>
	<!-- <td>I</td> -->
	<td>$2,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>10.0</td> -->
	<td>ORB</td>
<!-- 	<td>J. Rosario</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr>	<td class="num">May 04</td>
	<td>Mervyn LeRoy Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td>LIAISON</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">May 04</td>
	<td>Fort Marcy Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>LUBASH</td>
<!-- 	<td>J. Alvarado</td>
	<td>C. Clement</td> -->
</tr>
<tr>	<td class="num">May 04</td>
	<td>Beaugay Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>HESSONITE</td>
<!-- 	<td>J. Alvarado</td>
	<td>D. Donk</td> -->
</tr>
<tr class="odd">	<td class="num">May 11</td>
	<td>Peter Pan Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0</td> -->
	<td>FREEDOM CHILD</td>
<!-- 	<td>L. Saez</td>
	<td>T. Albertrani</td> -->
</tr>
<tr>	<td class="num">May 11</td>
	<td>Senorita Stakes</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>CHARLIE EM</td>
<!-- 	<td>G. Gomez</td>
	<td>P. Gallagher</td> -->
</tr>
<tr class="odd">	<td class="num">May 17</td>
	<td>Pimlico Special</td>
	<td>Pimlico</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.5</td> -->
	<td>LAST GUNFIGHTER</td>
<!-- 	<td>J. Castellano</td>
	<td>C. Brown</td> -->
</tr>
<tr>	<td class="num">May 17</td>
	<td>Black-Eyed Susan Stakes</td>
	<td>Pimlico</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0</td> -->
	<td>FIFTYSHADESOFHAY</td>
<!-- 	<td>J. Rosario</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">May 18</td>
	<td>Maryland Sprint Handicap</td>
	<td>Pimlico</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>SAGE VALLEY</td>
<!-- 	<td>J. Velazquez</td>
	<td>R. Rodriguez</td> -->
</tr>
<tr>	<td class="num">May 18</td>
	<td>Gallorette Handicap</td>
	<td>Pimlico</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>PIANIST</td>
<!-- 	<td>M. Smith</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">May 18</td>
	<td>Allaire duPont Distaff Stakes</td>
	<td>Pimlico</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>SUMMER APPLAUSE</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. Brown</td> -->
</tr>
<tr>	<td class="num">May 18</td>
	<td>Dixie Stakes</td>
	<td>Pimlico</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>SKYRING</td>
<!-- 	<td>G. Stevens</td>
	<td>D. Lukas</td> -->
</tr>
<tr class="odd">	<td class="num">May 18</td>
	<td>Preakness Stakes</td>
	<td>Pimlico</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.5</td> -->
	<td>OXBOW</td>
<!-- 	<td>G. Stevens</td>
	<td>D. Lukas</td> -->
</tr>
<tr>	<td class="num">May 18</td>
	<td>Red Bank Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>ZA APPROVAL</td>
<!-- 	<td>J. Bravo</td>
	<td>C. Clement</td> -->
</tr>
<tr class="odd">	<td class="num">May 18</td>
	<td>Marjorie L. Everett Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>OPEN WATER</td>
<!-- 	<td>J. Talamo</td>
	<td>E. Guillot</td> -->
</tr>
<tr>	<td class="num">May 18</td>
	<td>Vagrancy Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>6.5</td> -->
	<td>GLORIOUS VIEW</td>
<!-- 	<td>J. Alvarado</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">May 25</td>
	<td>Louisville Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>DARK COVE</td>
<!-- 	<td>R. Napravnik</td>
	<td>M. Maker</td> -->
</tr>
<tr>	<td class="num">May 25</td>
	<td>American Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>OBVIOUSLY</td>
<!-- 	<td>J. Talamo</td>
	<td>M. Mitchell</td> -->
</tr>
<tr class="odd">	<td class="num">May 25</td>
	<td>Sheepshead Bay Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>TANNERY</td>
<!-- 	<td>L. Saez</td>
	<td>A. Goldberg</td> -->
</tr>
<tr>	<td class="num">May 25</td>
	<td>Hanshin Cup Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0</td> -->
	<td>HOGY</td>
<!-- 	<td>C. Emigh</td>
	<td>S. Becker</td> -->
</tr>
<tr class="odd">	<td class="num">May 25</td>
	<td>Arlington Matron Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>IMPOSING GRACE</td>
<!-- 	<td>C. Hill</td>
	<td>W. Catalano</td> -->
</tr>
<tr>	<td class="num">May 25</td>
	<td>Arlington Classic Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>GENERAL ELECTION</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>K. Gorder</td> -->
</tr>
<tr class="odd">	<td class="num">May 27</td>
	<td>Lone Star Park Handicap</td>
	<td>Lone Star Park</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td>MASTER RICK</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr>	<td class="num">May 27</td>
	<td>All American Stakes</td>
	<td>Golden Gate Fields</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td>SUMMER HIT</td>
<!-- 	<td>R. Baze</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">May 27</td>
	<td>Winning Colors Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>6.0</td> -->
	<td>BEAT THE BLUES</td>
<!-- 	<td>M. Mena</td>
	<td>W. Calhoun</td> -->
</tr>
<tr>	<td class="num">May 27</td>
	<td>Los Angeles Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>COMMA TO THE TOP</td>
<!-- 	<td>E. Maldonado</td>
	<td>P. Miller</td> -->
</tr>
<tr class="odd">	<td class="num">May 27</td>
	<td>Gamely Stakes</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>MARKETING MIX</td>
<!-- 	<td>G. Stevens</td>
	<td>T. Proctor</td> -->
</tr>
<tr>	<td class="num">May 27</td>
	<td>Sands Point Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>DISCREET MARQ</td>
<!-- 	<td>C. Clement</td>
	<td>J. Lezcano</td> -->
</tr>
<tr class="odd">	<td class="num">May 27</td>
	<td>Acorn Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0</td> -->
	<td>MIDNIGHT LUCKY</td>
<!-- 	<td>R. Napravnik</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">May 27</td>
	<td>Ogden Phipps Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>TIZ MIZ SUE</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>S. Hobby</td> -->
</tr>
<tr class="odd">	<td class="num">May 27</td>
	<td>Metropolitan Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0</td> -->
	<td>SAHARA SKY</td>
<!-- 	<td>J. Rosario</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr>	<td class="num">Jun 01</td>
	<td>Hendrie Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>6.5</td> -->
	<td>DELIGHTFUL MARY</td>
<!-- 	<td>L. Contreras</td>
	<td>M. Casse</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 01</td>
	<td>Aristides</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>SCATMAN</td>
<!-- 	<td>S. Bridgmohan</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Jun 01</td>
	<td>The Californian</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0</td> -->
	<td>CLUBHOUSE RIDE</td>
<!-- 	<td>G. Gomez</td>
	<td>C. Lewis</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 02</td>
	<td>Affirmed Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>TIZ A MINISTER</td>
<!-- 	<td>C. Nakatani</td>
	<td>P. Aguirre</td> -->
</tr>
<tr>	<td class="num">Jun 02</td>
	<td>Vigil Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$156,000</td>
	<!-- <td>4up</td> -->
<!-- 	<td>7</td> -->
	<td>LAUGH TRACK</td>
<!-- 	<td>L. Contreras</td>
	<td>M. Casse</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 07</td>
	<td>Jaipur Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>7.0 T</td> -->
	<td>SOUPER SPEEDY</td>
<!-- 	<td>J. Lezcano</td>
	<td>T. Albertrani</td> -->
</tr>
<tr>	<td class="num">Jun 07</td>
	<td>Brooklyn Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12.0</td> -->
	<td>CALIDOSCOPIO</td>
<!-- 	<td>A. Gryder</td>
	<td>M. Puype</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 08</td>
	<td>Mint Julep Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>MIZ IDA</td>
<!-- 	<td>S. Bridgmohan</td>
	<td>S. Margolis</td> -->
</tr>
<tr>	<td class="num">Jun 08</td>
	<td>Honeymoon Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>SARACH</td>
<!-- 	<td>M. Garcia</td>
	<td>R. Mandella</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 08</td>
	<td>Charles Whittingham Memorial H</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>TALE OF A CHAMPION</td>
<!-- 	<td>J. Talamo</td>
	<td>K. Mulhall</td> -->
</tr>
<tr>	<td class="num">Jun 08</td>
	<td>Woody Stephens Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>7.0</td> -->
	<td>FORTY TALES</td>
<!-- 	<td>J. Rosario</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 08</td>
	<td>True North Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>FAST BULLET</td>
<!-- 	<td>J. Rosario</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Jun 08</td>
	<td>Manhattan Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>POINT OF ENTRY</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 08</td>
	<td>Belmont Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>12.0</td> -->
	<td>PALACE MALICE</td>
<!-- 	<td>M. Smith</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Jun 09</td>
	<td>Monmouth Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>BOISEROUS</td>
<!-- 	<td>C. McGaughey III</td>
	<td>J. Velazquez</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 15</td>
	<td>Fleur de Lis Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$175,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>FUNNY PROPOSITION</td>
<!-- 	<td>J. Rosario</td>
	<td>M. Casse</td> -->
</tr>
<tr>	<td class="num">Jun 15</td>
	<td>Stephen Foster Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0</td> -->
	<td>FORT LARNED</td>
<!-- 	<td>B. Hernandez, Jr.</td>
	<td>I. Wilkes</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 15</td>
	<td>Vanity Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0</td> -->
	<td>BYRAMA</td>
<!-- 	<td>G. Stevens</td>
	<td>S. Callaghan</td> -->
</tr>
<tr>	<td class="num">Jun 15</td>
	<td>Hill Prince Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>NOTACATBUTALLAMA</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 16</td>
	<td>Pegasus Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>VERRAZANO</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Jun 22</td>
	<td>Edward P. Evans All Along S</td>
	<td>Colonial Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>CHANNEL LADY</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 22</td>
	<td>Colonial Turf Cup Stakes</td>
	<td>Colonial Downs</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.5 T</td> -->
	<td>LONDON LANE</td>
<!-- 	<td>H. Karamanos</td>
	<td>L. Murray</td> -->
</tr>
<tr>	<td class="num">Jun 22</td>
	<td>Hollywood Oaks</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>DOINGHARDTIMEAGAIN</td>
<!-- 	<td>R. Bejarano</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 22</td>
	<td>Mother Goose Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>CLOSE HATCHES</td>
<!-- 	<td>J. Rosario</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Jun 23</td>
	<td>King Edward Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0</td> -->
	<td>RIDING THE RIVER</td>
<!-- 	<td>T. Kabel</td>
	<td>D. Cotey</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Iowa Oaks</td>
	<td>Prairie Meadows</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.5</td> -->
	<td>FIFTYSHADESOFHAY</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Jun 29</td>
	<td>Iowa Derby</td>
	<td>Prairie Meadows</td>
	<!-- <td>III</td> -->
	<td>$250,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>LOOKING COOL</td>
<!-- 	<td>L. Goncalves</td>
	<td>C. Nafzger</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Prairie Meadows Cornhusker H</td>
	<td>Prairie Meadows</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0</td> -->
	<td>PRAYER FOR RELIEF</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr>	<td class="num">Jun 29</td>
	<td>Eatontown Handicap</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>LAUGHING</td>
<!-- 	<td>A. Serpa</td>
	<td>A. Goldberg</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Bashford Manor Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>6.0</td> -->
	<td>DEBT CEILING</td>
<!-- 	<td>E. Camacho</td>
	<td>J. Robb</td> -->
</tr>
<tr>	<td class="num">Jun 29</td>
	<td>Firecracker Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>WISE DAN</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. LoPresti</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Triple Bend Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>7.0</td> -->
	<td>CENTRALINTELIGENCE</td>
<!-- 	<td>V. Espinoza</td>
	<td>R. Ellis</td> -->
</tr>
<tr>	<td class="num">Jun 29</td>
	<td>Shoemaker Mile Stakes</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>OBVIOUSLY</td>
<!-- 	<td>J. Talamo</td>
	<td>M. Mitchell</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Victory Ride Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>6.0</td> -->
	<td>BABY J</td>
<!-- 	<td>J. Rosario</td>
	<td>P. Reynolds</td> -->
</tr>
<tr>	<td class="num">Jun 29</td>
	<td>New York Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>STARFORMER</td>
<!-- 	<td>E. Prado</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Jun 29</td>
	<td>Chicago Handicap</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>7.0</td> -->
	<td>COZZE UP LADY</td>
<!-- 	<td>M. Mena</td>
	<td>W. Calhoun</td> -->
</tr>
<tr>	<td class="num">Jul 01</td>
	<td>Dominion Day Handicap</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>DELEGATION</td>
<!-- 	<td>L. Contreras</td>
	<td>M. Casse</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 04</td>
	<td>Poker Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>KING KREESA</td>
<!-- 	<td>I. Ortiz, Jr.</td>
	<td>J. Englehart</td> -->
</tr>
<tr>	<td class="num">Jul 04</td>
	<td>Jersey Shore Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>6.0</td> -->
	<td>RAINBOW HEIR</td>
<!-- 	<td>E. Trujillo</td>
	<td>B. Perkins, Jr.</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 04</td>
	<td>Swaps Stakes</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>9.0</td> -->
	<td>CHIEF HAVOC</td>
<!-- 	<td>R. Bejarano</td>
	<td>P. Miller</td> -->
</tr>
<tr>	<td class="num">Jul 06</td>
	<td>Dwyer Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>8.5</td> -->
	<td>MORENO</td>
<!-- 	<td>J. Ortiz</td>
	<td>E. Guillot</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 06</td>
	<td>Suburban Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$350,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>FLAT OUT</td>
<!-- 	<td>J. Alvarado</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Jul 06</td>
	<td>Smile Sprint Handicap</td>
	<td>Calder Race Course</td>
	<!-- <td>II</td> -->
	<td>$350,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>6.0</td> -->
	<td>BAHAMIAN SQUALL</td>
<!-- 	<td>L. Saez</td>
	<td>D. Fawkes</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 06</td>
	<td>Princess Rooney Handicap</td>
	<td>Calder Race Course</td>
	<!-- <td>I</td> -->
	<td>$350,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>6.0</td> -->
	<td>STARSHIP TRUFFLES</td>
<!-- 	<td>E. Zayas</td>
	<td>M. Wolfson</td> -->
</tr>
<tr>	<td class="num">Jul 06</td>
	<td>Carry Back Stakes</td>
	<td>Calder Race Course</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>6.0</td> -->
	<td>MICO MARGARITA</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 06</td>
	<td>Azalea Stakes</td>
	<td>Calder Race Course</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>6.0</td> -->
	<td>WILDCAT LILY</td>
<!-- 	<td>J. Alvarez</td>
	<td>M. Azpurua</td> -->
</tr>
<tr>	<td class="num">Jul 06</td>
	<td>United Nations Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>BIG BLUE KITTEN</td>
<!-- 	<td>C. Brown</td>
	<td>J. Bravo</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 06</td>
	<td>Salvator Mile</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.0</td> -->
	<td>RAGING DAOUST</td>
<!-- 	<td>V. Santiago</td>
	<td>C. Carlesimo, Jr.</td> -->
</tr>
<tr>	<td class="num">Jul 06</td>
	<td>Royal Heroine Mile</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>SCHIAPARELLI</td>
<!-- 	<td>J. Talamo</td>
	<td>M. Puype</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 06</td>
	<td>Hollywood Gold Cup</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>10.0</td> -->
	<td>GAME ON DUDE</td>
<!-- 	<td>M. Smith</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>Virginia Derby</td>
	<td>Colonial Downs</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>WAR DANCER</td>
<!-- 	<td>A. Garcia</td>
	<td>K. McPeek</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 13</td>
	<td>Delaware Oaks</td>
	<td>Delaware Park</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>DANCING AFLEET</td>
<!-- 	<td>J. Navarro</td>
	<td>T. Ritchey</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>Robert G. Dick Memorial Stakes</td>
	<td>Delaware Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>TREASURED UP</td>
<!-- 	<td>S. Bridgmohan</td>
	<td>A. Stall, Jr.</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 13</td>
	<td>Man O&rsquo; War Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>BOISTEROUS</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. McGaughey III</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>Stars and Stripes Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>DARK COVE</td>
<!-- 	<td>R. Napravnik</td>
	<td>M. Maker</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 13</td>
	<td>Modesty Handicap</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>9.5 T</td> -->
	<td>AUSUS</td>
<!-- 	<td>J. Graham</td>
	<td>D. Peitz</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>Arlington Handicap</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>RAHYSTRADA</td>
<!-- 	<td>R. Napravnik</td>
	<td>B. Hughes</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 13</td>
	<td>American Derby</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>9.5 T</td> -->
	<td>INFINITE MAGIC</td>
<!-- 	<td>C. Hill</td>
	<td>R. Mettee</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>Hollywood Juvenile Championship</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2-yo M</td> -->
<!-- 	<td>6.0</td> -->
	<td>ALPINE LUCK</td>
<!-- 	<td>G. Stevens</td>
	<td>M. Harrington</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 13</td>
	<td>A Gleam Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>7.0</td> -->
	<td>BOOK REVIEW</td>
<!-- 	<td>R. Bejarano</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Jul 13</td>
	<td>American Oaks</td>
	<td>Hollywood Park</td>
	<!-- <td>I</td> -->
	<td>$350,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>EMOLLIENT</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 14</td>
	<td>Sunset Handicap</td>
	<td>Hollywood Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>MARKETING MIX</td>
<!-- 	<td>G. Stevens</td>
	<td>T. Proctor</td> -->
</tr>
<tr>	<td class="num">Jul 17</td>
	<td>Bold Venture Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up</td> -->
<!-- 	<td>6.5</td> -->
	<td>ESSENCE HIT MAN</td>
<!-- 	<td>J. Campbell</td>
	<td>L. Cappuccitti</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 19</td>
	<td>James Marvin Stakes</td>
	<td>Saratoga</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>7.0</td> -->
	<td>SAGE VALLEY</td>
<!-- 	<td>C. Velasquez</td>
	<td>R. Rodriguez</td> -->
</tr>
<tr>	<td class="num">Jul 19</td>
	<td>Schuylerville Stakes</td>
	<td>Saratoga</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2-yo F</td> -->
<!-- 	<td>6.0</td> -->
	<td>BRAZEN PERSUASION</td>
<!-- 	<td>R. Napravnik</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 20</td>
	<td>Delaware Handicap</td>
	<td>Delaware Park</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>10.0</td> -->
	<td>ROYAL DELTA</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Jul 20</td>
	<td>Coaching Club American Oaks</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>9.0</td> -->
	<td>PRINCESS OF SYLMAR</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 20</td>
	<td>Shuvee Handicap</td>
	<td>Saratoga</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>9.0</td> -->
	<td>AUTHENTICITY</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Jul 20</td>
	<td>Eddie Read Stakes</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>JERANIMO</td>
<!-- 	<td>R. Bejarano</td>
	<td>M. Pender</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 20</td>
	<td>Ontario Matron Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>SISTERLY LOVE</td>
<!-- 	<td>E. Da Silva</td>
	<td>M. Casse</td> -->
</tr>
<tr>	<td class="num">Jul 20</td>
	<td>Arlington Oaks</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>9.0</td> -->
	<td>MY OPTION</td>
<!-- 	<td>E. Perez</td>
	<td>C. Block</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 21</td>
	<td>Nijinsky Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-up</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>SO LONG GEORGE</td>
<!-- 	<td>E. Wilson</td>
	<td>J. Charalambous</td> -->
</tr>
<tr>	<td class="num">Jul 21</td>
	<td>Sanford Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2-yo M</td> -->
<!-- 	<td>6.0</td> -->
	<td>WIRED BRYAN</td>
<!-- 	<td>S. Bridgmohan</td>
	<td>M. Dilger</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 21</td>
	<td>San Clemente Handicap</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3-yo f</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>WISHING GATE</td>
<!-- 	<td>G. Stevens</td>
	<td>T. Proctor</td> -->
</tr>
<tr>	<td class="num">Jul 27</td>
	<td>San Diego Handicap</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td>KETTLE CORN</td>
<!-- 	<td>V. Espinoza</td>
	<td>J. Sadler</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 27</td>
	<td>Prioress Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>6.0</td> -->
	<td>LIGHTHOUSE BAY</td>
<!-- 	<td>J. Rocco, Jr</td>
	<td>G. Weaver</td> -->
</tr>
<tr>	<td class="num">Jul 27</td>
	<td>Jim Dandy Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$600,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>9.0</td> -->
	<td>PALICE MALICE</td>
<!-- 	<td>M. Smith</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 27</td>
	<td>Diana Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>LAUGHING</td>
<!-- 	<td>J. Lezcano</td>
	<td>A. Goldberg</td> -->
</tr>
<tr>	<td class="num">Jul 28</td>
	<td>Bing Crosby Stakes</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.0</td> -->
	<td>POINTS OFFTHEBENCH</td>
<!-- 	<td>M. Smith</td>
	<td>T. Yakteen</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 28</td>
	<td>Taylor Made Matchmaker Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>STARSTRUCK</td>
<!-- 	<td>R. Napravnik</td>
	<td>J. Jones</td> -->
</tr>
<tr>	<td class="num">Jul 28</td>
	<td>Oceanport Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>SILVER MAX</td>
<!-- 	<td>R. Albarado</td>
	<td>D. Romans</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 28</td>
	<td>Monmouth Cup</td>
	<td>Monmouth Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.5</td> -->
	<td>PANTS ON FIRE</td>
<!-- 	<td>P. Lopez</td>
	<td>K. Breen</td> -->
</tr>
<tr>	<td class="num">Jul 28</td>
	<td>Molly Pitcher Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>I</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>8.5</td> -->
	<td>JOYFUL VICTORY</td>
<!-- 	<td>R. Napravnik</td>
	<td>J. Jones</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 28</td>
	<td>Haskell Invitational</td>
	<td>Monmouth Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>9.0</td> -->
	<td>VERRAZANO</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Jul 28</td>
	<td>Amsterdam Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>6.5</td> -->
	<td>FORTY TALES</td>
<!-- 	<td>J. Rosario</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Jul 28</td>
	<td>Royal North Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up f/m</td> -->
<!-- 	<td>6.0 T</td> -->
	<td>NIKKIS SMARTYPANTS</td>
<!-- 	<td>E. Da Silva</td>
	<td>R. Tiller</td> -->
</tr>
<tr>	<td class="num">Jul 29</td>
	<td>Honorable Miss Handicap</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>6.0</td> -->
	<td>DANCE TO BRISTOL</td>
<!-- 	<td>X. Perez</td>
	<td>O. Figgins, III</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 03</td>
	<td>Seagram Cup Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td>ALPHA BETTOR</td>
<!-- 	<td>J. Stein</td>
	<td>D. Vella</td> -->
</tr>
<tr>	<td class="num">Aug 03</td>
	<td>Whitney Invitational Handicap</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>CROSS TRAFFIC</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 03</td>
	<td>Clement L. Hirsch Stakes</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.5</td> -->
	<td>LADY OF FIFTY</td>
<!-- 	<td>C. Nakatani</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr>	<td class="num">Aug 03</td>
	<td>West Virginia Derby</td>
	<td>Mountaineer</td>
	<!-- <td>II</td> -->
	<td>$750,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>9.0</td> -->
	<td>DEPARTING</td>
<!-- 	<td>R. Albarado</td>
	<td>A. Stall, Jr.</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 04</td>
	<td>Alfred G. Vanderbilt Handicap</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>6.0</td> -->
	<td>JUSTIN PHILLIP</td>
<!-- 	<td>J. Velazquez</td>
	<td>S. Asmussen</td> -->
</tr>
<tr>	<td class="num">Aug 09</td>
	<td>National Museum of Racing Hall Of Fame</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>NOTACATBUTALLAMA</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 10</td>
	<td>La Jolla Handicap</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>DICE FLAVOR</td>
<!-- 	<td>G. Gomez</td>
	<td>P. Gallagher</td> -->
</tr>
<tr>	<td class="num">Aug 10</td>
	<td>Gardenia Stakes</td>
	<td>Ellis Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>8.0</td> -->
	<td>DEVIOUS INTENT</td>
<!-- 	<td>R. Morales</td>
	<td>K. Gorder</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 10</td>
	<td>Fourstardave Handicap</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>WISE DAN</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. LoPresti</td> -->
</tr>
<tr>	<td class="num">Aug 10</td>
	<td>Monmouth Oaks</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>SEANEEN GIRL</td>
<!-- 	<td>P. Lopez</td>
	<td>B. Flint</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 11</td>
	<td>Saratoga Special Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2-yo M</td> -->
<!-- 	<td>6.5</td> -->
	<td>CORFU</td>
<!-- 	<td>J. Velazquez</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Aug 11</td>
	<td>Adirondack Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2-yo F</td> -->
<!-- 	<td>6.5</td> -->
	<td>DESIGNER LEGS</td>
<!-- 	<td>S. Bridgmohan</td>
	<td>D. Stewart</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 11</td>
	<td>John C. Mabee Stakes</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3up f/m</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>TIZ FLIRTATIOUS</td>
<!-- 	<td>J. Leparoux</td>
	<td>M. Jones</td> -->
</tr>
<tr>	<td class="num">Aug 17</td>
	<td>Ontario Colleen Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>LEIGH COURT</td>
<!-- 	<td>G. Boulanger</td>
	<td>J. Carroll</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 17</td>
	<td>Alabama Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>10.0</td> -->
	<td>PRINCESS OF SYLMAR</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Aug 17</td>
	<td>Sword Dancer Invitational</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>12.0 T</td> -->
	<td>BIG BLUE KITTEN</td>
<!-- 	<td>J. Bravo</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 17</td>
	<td>Del Mar Oaks</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3yo f</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>DISCREET MARQ</td>
<!-- 	<td>J. Leparoux</td>
	<td>C. Clement</td> -->
</tr>
<tr>	<td class="num">Aug 17</td>
	<td>Arlington Million</td>
	<td>Arlington Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>REAL SOLUTION</td>
<!-- 	<td>A. Garcia</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 17</td>
	<td>Beverly D. Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>10.5 T</td> -->
	<td>DANK</td>
<!-- 	<td>R. Moore</td>
	<td>S. Stoute</td> -->
</tr>
<tr>	<td class="num">Aug 17</td>
	<td>Secretariat Stakes</td>
	<td>Arlington Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>10.0 T</td> -->
	<td>ADMIRAL KITTEN</td>
<!-- 	<td>R. Napravnik</td>
	<td>M. Maker</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 18</td>
	<td>Rancho Bernardo Handicap</td>
	<td>Del Mar</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up f/m</td> -->
<!-- 	<td>6.5</td> -->
	<td>RENEESGOTZIP</td>
<!-- 	<td>G. Gomez</td>
	<td>P. Miller</td> -->
</tr>
<tr>	<td class="num">Aug 18</td>
	<td>Longacres Mile</td>
	<td>Emerald Downs</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.0</td> -->
	<td>HERBIE D</td>
<!-- 	<td>A. Perez</td>
	<td>R. Gilker</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 18</td>
	<td>Woodford Reserve Lake Placid</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>CAROLINE THOMAS</td>
<!-- 	<td>R. Napravnik</td>
	<td>B. Tagg</td> -->
</tr>
<tr>	<td class="num">Aug 18</td>
	<td>Philip H. Iselin Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>LAST GUNFIGHTER</td>
<!-- 	<td>J. Bravo</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 23</td>
	<td>Ballerina Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-up F</td> -->
<!-- 	<td>7.0</td> -->
	<td>DANCE TO BRISTOL</td>
<!-- 	<td>X. Perez</td>
	<td>O. Figgins, III</td> -->
</tr>
<tr>	<td class="num">Aug 24</td>
	<td>Del Mar Handicap</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>11.0 T</td> -->
	<td>VAGABOND SHOES</td>
<!-- 	<td>V. Espinoza</td>
	<td>J. Sadler</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 24</td>
	<td>Ballston Spa Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>LAUGHING</td>
<!-- 	<td>J. Lezcano</td>
	<td>A. Goldberg</td> -->
</tr>
<tr>	<td class="num">Aug 24</td>
	<td>Test Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-yo F</td> -->
<!-- 	<td>7.0</td> -->
	<td>SWEET LULU</td>
<!-- 	<td>J. Leparoux</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 24</td>
	<td>Travers Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3-yo M</td> -->
<!-- 	<td>10.0</td> -->
	<td>WILL TAKE CHARGE</td>
<!-- 	<td>L. Saez</td>
	<td>D. Lukas</td> -->
</tr>
<tr>	<td class="num">Aug 25</td>
	<td>Play the King Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3-up</td> -->
<!-- 	<td>7.0 T</td> -->
	<td>DIMENSION</td>
<!-- 	<td>D. Moran</td>
	<td>C. Murphy</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 25</td>
	<td>Cliff Hanger Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>SUMMER FRONT</td>
<!-- 	<td>J. Bravo</td>
	<td>C. Clement</td> -->
</tr>
<tr>	<td class="num">Aug 25</td>
	<td>Personal Ensign Invitational</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3-up FM</td> -->
<!-- 	<td>9.0</td> -->
	<td>ROYAL DELTA</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 25</td>
	<td>Del Mar Mile</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3-up</td> -->
<!-- 	<td>8.0 T</td> -->
	<td>OBVIOUSLY</td>
<!-- 	<td>J. Talamo</td>
	<td>M. Mitchell</td> -->
</tr>
<tr>	<td class="num">Aug 25</td>
	<td>Pacific Classic</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3-up</td> -->
<!-- 	<td>10.0</td> -->
	<td>GAME ON DUDE</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 29</td>
	<td>With Anticipation Stakes</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2-yo M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>BASHART</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Aug 31</td>
	<td>Washington Park Handicap</td>
	<td>Arlington Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>WILLCOX INN</td>
<!-- 	<td>J. Graham</td>
	<td>M. Stidham</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 31</td>
	<td>Bernard Baruch Handicap</td>
	<td>Saratoga</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3-UP M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>SILVER MAX</td>
<!-- 	<td>R. Albarado</td>
	<td>D. Romans</td> -->
</tr>
<tr>	<td class="num">Aug 31</td>
	<td>Forego Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>7.0</td> -->
	<td>STRAPPING GROOM</td>
<!-- 	<td>J. Alvarado</td>
	<td>D. Jacobson</td> -->
</tr>
<tr class="odd">	<td class="num">Aug 31</td>
	<td>Woodward Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3-up M</td> -->
<!-- 	<td>9.0</td> -->
	<td>ALPHA</td>
<!-- 	<td>J. Velazquez</td>
	<td>K. McLaughlin</td> -->
</tr>
<tr>	<td class="num">Sep 01</td>
	<td>Saranac Stakes</td>
	<td>Saratoga</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>FIVE IRON</td>
<!-- 	<td>L. Saez</td>
	<td>B. Lynch</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 01</td>
	<td>Spinaway Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>2yo f</td> -->
<!-- 	<td>7.0</td> -->
	<td>SWEET REASON</td>
<!-- 	<td>A. Solis</td>
	<td>L. Gyarmati</td> -->
</tr>
<tr>	<td class="num">Sep 01</td>
	<td>Sapling Stakes</td>
	<td>Monmouth Park</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>6.0</td> -->
	<td>DUNKIN BEND</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 01</td>
	<td>Del Mar Derby</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9.0 T</td> -->
	<td>GABRIEL CHARLES</td>
<!-- 	<td>M. Smith</td>
	<td>J. Mullins</td> -->
</tr>
<tr>	<td class="num">Sep 02</td>
	<td>Glens Falls Handicap</td>
	<td>Saratoga</td>
	<!-- <td>n/a</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>11 T</td> -->
	<td>LADY COHIBA</td>
<!-- 	<td>J. Alvarado</td>
	<td>C. Clement</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 02</td>
	<td>Three Chimneys Hopeful Stakes</td>
	<td>Saratoga</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>7</td> -->
	<td>STRONG MANDATE</td>
<!-- 	<td>J. Ortiz</td>
	<td>D. Lukas</td> -->
</tr>
<tr>	<td class="num">Sep 02</td>
	<td>Greenwood Cup</td>
	<td>Parx Racing</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>12</td> -->
	<td>ELDAAFER</td>
<!-- 	<td>R. Santana, Jr.</td>
	<td>D. Alvarado</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 02</td>
	<td>Turf Monster Handicap</td>
	<td>Parx Racing</td>
	<!-- <td>III</td> -->
	<td>$350,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>5 T</td> -->
	<td>STORMOFTHECENTURY</td>
<!-- 	<td>S. Elliott</td>
	<td>L. Ruberto, Jr.</td> -->
</tr>
<tr>	<td class="num">Sep 02</td>
	<td>Smarty Jones Stakes</td>
	<td>Parx Racing</td>
	<!-- <td>III</td> -->
	<td>$350,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.32</td> -->
	<td>EDGE OF REALITY</td>
<!-- 	<td>S. Elliott</td>
	<td>A. Dutrow</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 02</td>
	<td>Yellow Ribbon Handicap</td>
	<td>Del Mar</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>EGG DROP</td>
<!-- 	<td>M. Garcia</td>
	<td>M. Mitchell</td> -->
</tr>
<tr>	<td class="num">Sep 04</td>
	<td>Del Mar Futurity</td>
	<td>Del Mar</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>7</td> -->
	<td>TAMARANDO</td>
<!-- 	<td>J. Leparoux</td>
	<td>J. Hollendorfer</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 07</td>
	<td>Super Derby</td>
	<td>Louisiana Downs</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>DEPARTING</td>
<!-- 	<td>R. Albarado</td>
	<td>A. Stall, Jr.</td> -->
</tr>
<tr>	<td class="num">Sep 07</td>
	<td>Kent Stakes</td>
	<td>Delaware Park</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9 T</td> -->
	<td>ARE YOU KIDDING ME</td>
<!-- 	<td>R. Maragh</td>
	<td>R. Attfield</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 07</td>
	<td>Ack Ack Handicap</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8</td> -->
	<td>PANTS ON FIRE</td>
<!-- 	<td>P. Lopez</td>
	<td>K. Breen</td> -->
</tr>
<tr>	<td class="num">Sep 07</td>
	<td>Pocahontas Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>UNTAPABLE</td>
<!-- 	<td>R. Napravnik</td>
	<td>S. Asmussen</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 07</td>
	<td>Iroquois Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>CLEBURNE</td>
<!-- 	<td>C. Lanerie</td>
	<td>D. Romans</td> -->
</tr>
<tr>	<td class="num">Sep 07</td>
	<td>Bowling Green Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10 T</td> -->
	<td>HYPER</td>
<!-- 	<td>J. Castellano</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 07</td>
	<td>Arlington-Washington Futurity</td>
	<td>Arlington</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8</td> -->
	<td>SOLITARY RANGER</td>
<!-- 	<td>F. Geroux</td>
	<td>W. Catalano</td> -->
</tr>
<tr>	<td class="num">Sep 08</td>
	<td>British Columbia Derby</td>
	<td>Hastings</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>TITLE CONTENDER</td>
<!-- 	<td>R. Walcott</td>
	<td>A. Bolton</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 09</td>
	<td>Presque Isle Downs Masters Stakes</td>
	<td>Presque Isle Downs</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>6.5</td> -->
	<td>GROUPIE DOLL</td>
<!-- 	<td>R. Maragh</td>
	<td>W. Bradley</td> -->
</tr>
<tr>	<td class="num">Sep 14</td>
	<td>Summer Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8 T</td> -->
	<td>MY CONQUESTADORY</td>
<!-- 	<td>E. Da Silva</td>
	<td>M. Casse</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 14</td>
	<td>Natalma Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8 T</td> -->
	<td>LLANARMON</td>
<!-- 	<td>E. Wilson</td>
	<td>R. Attfield</td> -->
</tr>
<tr>	<td class="num">Sep 14</td>
	<td>Kentucky Turf Cup</td>
	<td>Kentucky Downs</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12 T</td> -->
	<td>TEMERAINE</td>
<!-- 	<td>G. Stevens</td>
	<td>T. Proctor</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 14</td>
	<td>Noble Damsel</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8 T</td> -->
	<td>PEACE PRESERVER</td>
<!-- 	<td>I. Ortiz, Jr.</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Sep 14</td>
	<td>Garden City</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$350,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>9 T</td> -->
	<td>ALTERITE</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 15</td>
	<td>Ricoh Woodbine Mile</td>
	<td>Woodbine</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8 T</td> -->
	<td>WISE DAN</td>
<!-- 	<td>J. Velazquez</td>
	<td>C. LoPresti</td> -->
</tr>
<tr>	<td class="num">Sep 15</td>
	<td>Canadian Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$300,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>9 T</td> -->
	<td>MINAKSHI</td>
<!-- 	<td>L. Contreras</td>
	<td>M. Matz</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 15</td>
	<td>Northern Dancer Turf</td>
	<td>Woodbine</td>
	<!-- <td>I</td> -->
	<td>$300,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12 T</td> -->
	<td>FORTE DEL MARMI</td>
<!-- 	<td>E. Da Silva</td>
	<td>R. Attfield</td> -->
</tr>
<tr>	<td class="num">Sep 15</td>
	<td>Ontario Derby</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>HIS RACE TO WIN</td>
<!-- 	<td>E. Da Silva</td>
	<td>M. Pierce</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 21</td>
	<td>Gallant Bob Stakes</td>
	<td>Parx Racing</td>
	<!-- <td>III</td> -->
	<td>$300,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>CITY OF WESTON</td>
<!-- 	<td>P. Lopez</td>
	<td>A. Sano</td> -->
</tr>
<tr>	<td class="num">Sep 21</td>
	<td>Pennsylvania Derby</td>
	<td>Parx Racing</td>
	<!-- <td>II</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>WILL TAKE CHARGE</td>
<!-- 	<td>L. Saez</td>
	<td>D. Lukas</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 21</td>
	<td>Cotillion Stakes</td>
	<td>Parx Racing</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>CLOSE HATCHES</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr>	<td class="num">Sep 21</td>
	<td>Dogwood Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>7</td> -->
	<td>SKY GIRL</td>
<!-- 	<td>C. Lanerie</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 21</td>
	<td>Gallant Bloom Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>6.5</td> -->
	<td>CLUSTER OF STARS</td>
<!-- 	<td>J. Castellano</td>
	<td>S. Asmussen</td> -->
</tr>
<tr>	<td class="num">Sep 27</td>
	<td>Eddie D Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6.5 T</td> -->
	<td>CHIPS ALL IN</td>
<!-- 	<td>J. Leparoux</td>
	<td>J. Mullins</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 28</td>
	<td>Zenyatta Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8.5</td> -->
	<td>BEHOLDER</td>
<!-- 	<td>G. Stevens</td>
	<td>R. Mandella</td> -->
</tr>
<tr>	<td class="num">Sep 28</td>
	<td>Rodeo Drive Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>10 T</td> -->
	<td>TIZ FLIRTATIOUS</td>
<!-- 	<td>J. Leparoux</td>
	<td>M. Jones</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 28</td>
	<td>Chandelier Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>SECRET COMPASS</td>
<!-- 	<td>R. Napravnik</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Sep 28</td>
	<td>Awesome Again Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9</td> -->
	<td>MUCHO MACHO MAN</td>
<!-- 	<td>G. Stevens</td>
	<td>K. Ritvo</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 28</td>
	<td>Jefferson Cup Stakes</td>
	<td>Churchill Downs</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8 T</td> -->
	<td>GENERAL ELECTION</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>K. Gorder</td> -->
</tr>
<tr>	<td class="num">Sep 28</td>
	<td>Kelso Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$400,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8</td> -->
	<td>GRAYDAR</td>
<!-- 	<td>E. Prado</td>
	<td>T. Pletcher</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 28</td>
	<td>Beldame Invitational</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>9</td> -->
	<td>PRINCESS OF SYLMAR</td>
<!-- 	<td>J. Castellano</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Sep 28</td>
	<td>Joe Hirsch Turf Classic Invitational</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12 T</td> -->
	<td>LITTLE MIKE</td>
<!-- 	<td>M. Smith</td>
	<td>D. Romans</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 28</td>
	<td>Flower Bowl Invitational</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$600,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>10 T</td> -->
	<td>LAUGHING</td>
<!-- 	<td>J. Lezcano</td>
	<td>A. Goldberg</td> -->
</tr>
<tr>	<td class="num">Sep 28</td>
	<td>Jockey Club Gold Cup Invitational</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10</td> -->
	<td>RON THE GREEK</td>
<!-- 	<td>J. Lezcano</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 29</td>
	<td>John Henry Turf Championship</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10</td> -->
	<td>INDY POINT</td>
<!-- 	<td>G. Stevens</td>
	<td>R. Mandella</td> -->
</tr>
<tr>	<td class="num">Sep 29</td>
	<td>Oklahoma Derby</td>
	<td>Remington Park</td>
	<!-- <td>III</td> -->
	<td>$400,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>BROADWAY EMPIRE</td>
<!-- 	<td>R. Walcott</td>
	<td>R. Diodoro</td> -->
</tr>
<tr class="odd">	<td class="num">Sep 29</td>
	<td>Matron Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>6</td> -->
	<td>MISS BEHAVIOUR</td>
<!-- 	<td>G. Cruise</td>
	<td>P. Schoenthal</td> -->
</tr>
<tr>	<td class="num">Sep 29</td>
	<td>Futurity Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>6</td> -->
	<td>IN TROUBLE</td>
<!-- 	<td>J. Rocco, Jr.</td>
	<td>A. Dutrow</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 04</td>
	<td>Darley Alcibiades Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>MY CONQUESTADORY</td>
<!-- 	<td>E. Da Silva</td>
	<td>M. Casse</td> -->
</tr>
<tr>	<td class="num">Oct 04</td>
	<td>Stoll Keenon Ogden Phoenix</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6</td> -->
	<td>SUM OF THE PARTS</td>
<!-- 	<td>L. Goncalves</td>
	<td>T. Amoss</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Thoroughbred Club of America Stakes</td>
	<td>Keeneland</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>6</td> -->
	<td>JUDY THE BEAUTY</td>
<!-- 	<td>J. Velazquez</td>
	<td>W. Ward</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>Woodford Stakes Presented by Keeneland Select</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>5.5 T</td> -->
	<td>HAVELOCK</td>
<!-- 	<td>G. Gomez</td>
	<td>D. Miller</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Santa Anita Sprint Championship</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$250,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6</td> -->
	<td>POINTS OFFTHEBENCH</td>
<!-- 	<td>M. Smith</td>
	<td>T. Yakteen</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>City of Hope Mile</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8 T</td> -->
	<td>NO JET LAG</td>
<!-- 	<td>M. Smith</td>
	<td>S. Callaghan</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Mazarine Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>MADLY TRULY</td>
<!-- 	<td>P. Husbands</td>
	<td>M. Casse</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>First Lady Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8 T</td> -->
	<td>BETTER LUCKY</td>
<!-- 	<td>J. Leparoux</td>
	<td>T. Albertrani</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Shadwell Turf Mile</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$750,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8 T</td> -->
	<td>SILVER MAX</td>
<!-- 	<td>R. Albarado</td>
	<td>D. Romans</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>Indiana Oaks</td>
	<td>Indiana Downs</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>8.5</td> -->
	<td>PURE FUN</td>
<!-- 	<td>V. Lebron</td>
	<td>K. McPeek</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Indiana Derby</td>
	<td>Indiana Downs</td>
	<!-- <td>II</td> -->
	<td>$500,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>8.5</td> -->
	<td>POWER BROKER</td>
<!-- 	<td>M. Garcia</td>
	<td>B. Baffert</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>Hawthorne Derby</td>
	<td>Hawthorne</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>KID DREAMS</td>
<!-- 	<td>F. Torres</td>
	<td>N. Drysdale</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Jamaica Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td>UP WITH THE BIRDS</td>
<!-- 	<td>C. Velasquez</td>
	<td>M. Pierce</td> -->
</tr>
<tr>	<td class="num">Oct 05</td>
	<td>Frizette Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8</td> -->
	<td>ARTEMIS AGROTERA</td>
<!-- 	<td>J. Lezcano</td>
	<td>M. Hushion</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 05</td>
	<td>Foxwoods Champagne Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8</td> -->
	<td>HAVANA</td>
<!-- 	<td>I. Ortiz, Jr.</td>
	<td>T. Pletcher</td> -->
</tr>
<tr>	<td class="num">Oct 06</td>
	<td>Durham Cup Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9</td> -->
	<td>JAMES STREET</td>
<!-- 	<td>P. Husbands</td>
	<td>J. Carroll</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 06</td>
	<td>Bourbon Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>POKER PLAYER</td>
<!-- 	<td>C. Hill</td>
	<td>W. Catalano</td> -->
</tr>
<tr>	<td class="num">Oct 06</td>
	<td>Juddmonte Spinster Stakes</td>
	<td>Keeneland</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>9</td> -->
	<td>EMOLLIENT</td>
<!-- 	<td>M. Smith</td>
	<td>W. Mott</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 06</td>
	<td>Miss Grillo Stakes</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>TESTA ROSSI</td>
<!-- 	<td>J. Lezcano</td>
	<td>C. Brown</td> -->
</tr>
<tr>	<td class="num">Oct 09</td>
	<td>JPMorgan Chase Jessamine Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>KITTEN KABOODLE</td>
<!-- 	<td>A. Garcia</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 12</td>
	<td>Knickerbocker Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9 T</td> -->
	<td>ZA APPROVAL</td>
<!-- 	<td>J. Rosario</td>
	<td>C. Clement</td> -->
</tr>
<tr>	<td class="num">Oct 12</td>
	<td>Athenia</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8.5 T</td> -->
	<td>PIANIST</td>
<!-- 	<td>J. Ortiz</td>
	<td>C. Brown</td> -->
</tr>
<tr class="odd">	<td class="num">Oct 14</td>
	<td>Ballerina Stakes</td>
	<td>Hastings</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>9</td> -->
	<td>MADEIRA PARK</td>
<!-- 	<td>R. Walcott</td>
	<td>D. Forster</td> -->
</tr>
<tr>	<td class="num">Oct 17</td>
	<td>Sycamore Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Oct 18</td>
	<td>Pin Oak Valley View Stakes</td>
	<td>Keeneland</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>8.5 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Oct 19</td>
	<td>Lexus Raven Run Stakes</td>
	<td>Keeneland</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>7</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Oct 26</td>
	<td>Autumn Miss Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>8 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Oct 26</td>
	<td>Hagyard Fayette Stakes</td>
	<td>Keeneland</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>9</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Oct 26</td>
	<td>Turnback the Alarm Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8.5</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Oct 26</td>
	<td>Bold Ruler Handicap</td>
	<td>Belmont Park</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>7</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Oct 27</td>
	<td>E.P. Taylor Stakes</td>
	<td>Woodbine</td>
	<!-- <td>I</td> -->
	<td>$500,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>10 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Oct 27</td>
	<td>Pattison Canadian International</td>
	<td>Woodbine</td>
	<!-- <td>I</td> -->
	<td>$1,000,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>12 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 01</td>
	<td>Twilight Derby</td>
	<td>Santa Anita</td>
	<!-- <td>I</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 02</td>
	<td>Discovery Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3yo</td> -->
<!-- 	<td>9</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 02</td>
	<td>Sen. Ken Maddy Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>III</td> -->
	<td>$100,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>6.5</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 02</td>
	<td>Maple Leaf Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>10</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 03</td>
	<td>Ontario Fashion Stakes</td>
	<td>Woodbine</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>6</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 03</td>
	<td>Goldikova Stakes</td>
	<td>Santa Anita</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>8 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 03</td>
	<td>Tempted Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 03</td>
	<td>Nashua Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$200,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 09</td>
	<td>Long Island Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up F/M</td> -->
<!-- 	<td>12 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 10</td>
	<td>Autumn Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8.5</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 16</td>
	<td>Red Smith Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>11 T</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 23</td>
	<td>Delta Downs Jackpot Stakes</td>
	<td>Delta Downs</td>
	<!-- <td>III</td> -->
	<td>$1,000,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>8.5</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 23</td>
	<td>Delta Downs Princess</td>
	<td>Delta Downs</td>
	<!-- <td>III</td> -->
	<td>$500,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>8</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 28</td>
	<td>Fall Highweight Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 30</td>
	<td>Hawthorne Gold Cup Handicap</td>
	<td>Hawthorne</td>
	<!-- <td>II</td> -->
	<td>$350,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>10</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 30</td>
	<td>Comely Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>III</td> -->
	<td>$200,000</td>
	<!-- <td>3yo F</td> -->
<!-- 	<td>9</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 30</td>
	<td>Remsen Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>2yo</td> -->
<!-- 	<td>9</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 30</td>
	<td>Demoiselle Stakes</td>
	<td>Aqueduct</td>
	<!-- <td>II</td> -->
	<td>$250,000</td>
	<!-- <td>2yo F</td> -->
<!-- 	<td>9</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr class="odd">	<td class="num">Nov 30</td>
	<td>Cigar Mile Handicap</td>
	<td>Aqueduct</td>
	<!-- <td>I</td> -->
	<td>$400,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>8</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
<tr>	<td class="num">Nov 30</td>
	<td>Kennedy Road Stakes</td>
	<td>Woodbine</td>
	<!-- <td>II</td> -->
	<td>$150,000</td>
	<!-- <td>3up</td> -->
<!-- 	<td>6</td> -->
	<td></td>
<!-- 	<td></td>
	<td></td> -->
</tr>
</tbody>
</table>
