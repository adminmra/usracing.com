{literal}<script type="text/javascript">

function Nflschedule(a)
{
	Http.get({
				url: "/Feeds/info/raceschedule.php?nflweek="+a.value,
				callback: Nflschedulefile,
				cache: Http.Cache.Get
				});
				return false;	
}

function Getschedule(a,b,c)
{
	Http.get({
				url: "/Feeds/info/raceschedule.php?nflweek="+a+"&order="+b+"&type="+c,
				callback: Nflschedulefile,
				cache: Http.Cache.Get
				});
				return false;	
}

function Nflschedulefile(xmlreply)
{
	var request = xmlreply.responseText;
	//alert(request);	
	document.getElementById("gametable").innerHTML=request;
}

</script>{/literal}

<div id="gametable" style="clear:both;">
        <table class="table table-condensed table-striped table-bordered" id="infoEntries">
	
	<tbody>
	<tr>
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','racedate','ASC');" style="color:#FFFFFF; ">Date</a></th>		
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','race','ASC');" style="color:#FFFFFF; ">Race</a></th>	
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','grade','ASC');" style="color:#FFFFFF; ">Grade</a></th>
		<th>Distance</th>
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','age','ASC');" style="color:#FFFFFF; ">Age</a></th>
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','track','ASC');" style="color:#FFFFFF; ">Track</a></th>	
		<th>Time(ET)</th>
		<th><a href="javascript: void(0);" onClick="return Getschedule('09','purse','ASC');" style="color:#FFFFFF; ">Purse</a></th>
	</tr>	
	<tr>		<td>Sep , 18</td>
		<td><a href="/stakes?name=Summer S." title="Summer S.">Summer S.</a></td>
		<td>|||</td>
		<td>1 Mile</td>
		<td>2</td>
		<td><a href="/racetrack?name=Woodbine" title="Woodbine">Woodbine</a></td>
		<td>4:00 PM</td>
		<td>$250,000</td>
	</tr>
	<tr class="odd">		<td>Sep , 18</td>
		<td>Garden City S.</td>
		<td>|</td>
		<td>9 Furlongs (T)</td>
		<td>3</td>
		<td><a href="/racetrack?name=Belmont Park" title="Belmont Park">Belmont Park</a></td>
		<td>4:00 PM</td>
		<td>$250,000</td>
	</tr>
	<tr>		<td>Sep , 18</td>
		<td><a href="/stakes?name=Natalma S." title="Natalma S.">Natalma S.</a></td>
		<td>|||</td>
		<td>1 Mile</td>
		<td>2</td>
		<td><a href="/racetrack?name=Woodbine" title="Woodbine">Woodbine</a></td>
		<td>4:00 PM</td>
		<td>$200,000</td>
	</tr>
	<tr class="odd">		<td>Sep , 19</td>
		<td>Northern Dancer Turf S. presented by Vtech</td>
		<td>|</td>
		<td>1 1/2 Miles</td>
		<td>3+</td>
		<td><a href="/racetrack?name=Woodbine" title="Woodbine">Woodbine</a></td>
		<td>4:00 PM</td>
		<td>$750,000</td>
	</tr>
	<tr>		<td>Sep , 19</td>
		<td>Woodbine Mile S.</td>
		<td>|</td>
		<td>1 Mile</td>
		<td>3+</td>
		<td><a href="/racetrack?name=Woodbine" title="Woodbine">Woodbine</a></td>
		<td>4:00 PM</td>
		<td>$1,000,000</td>
	</tr>
	<tr class="odd">		<td>Sep , 19</td>
		<td>Canadian S.</td>
		<td>||</td>
		<td>1 1/8 Miles</td>
		<td>3+</td>
		<td><a href="/racetrack?name=Woodbine" title="Woodbine">Woodbine</a></td>
		<td>4:00 PM</td>
		<td>$300,000</td>
	</tr>
	<tr>		<td>Sep , 19</td>
		<td>Nobel Damsel S.</td>
		<td>|||</td>
		<td>1 Mile</td>
		<td>3+</td>
		<td><a href="/racetrack?name=Arlington Park" title="Arlington Park">Arlington Park</a></td>
		<td>5:00 PM</td>
		<td>$100,000</td>
	</tr>
	<tr class="odd">		<td colspan="8" ><a href="/race-schedule">View More 2010 Race Schedule..</a></td>
	</tr>
	</tbody>
	</table>
	</div>