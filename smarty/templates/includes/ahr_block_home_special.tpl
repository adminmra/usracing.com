{literal}
<style type="text/css">
#special p { margin-top:10px; }
#special p:first-child { margin-top:0; }
#special { display:block; padding:10px; } 
#special .container { height: auto; display:block; } 
#special .post { display:block; background:#f2f9ff; width: auto; border:1px solid #aad3ff; padding: 10px; margin:10px 0; }
#special .race { width:100%; display:block; padding:0 0 5px 0; margin: 0 0 10px 0; border-bottom:1px solid #e6e6e6;  }
#special .race .date { font-weight: bold; width:auto; float:left; }
#special .race .betlink { font-weight: bold; float:right;}
#special .details { display:block; width: 345px; clear:both;}
#special .track { text-transform:uppercase; }
#special .note { padding-top:0;}
#special #infoEntries { margin-top:10px; padding:0; }
#special #infoEntries td { padding:3px 5px; text-align: left; vertical-align:top; line-height:15px; text-align:left; border-bottom:1px solid #e6e6e6; }
#special #infoEntries td:first-child { width:33%; text-align:left; }
#special #infoEntries th { padding:3px 5px; text-align: left; }
#special #infoEntries th:first-child { text-align: left; }
#special h1.title { background:transparent url('/themes/images/genericicon.gif') no-repeat scroll left 1px; border-bottom:1px solid #AAD3FF; line-height:17px; padding:1px 0 4px 34px; } 
</style>
{/literal}

<div id="special" class="block" style="padding:10px;">
<div class="container">

<h2 class="title">Breeders' Cup Races</h2>
<div class="post">               

<!--<div class="race">
<span class="date"><strong>November 28th:</strong></span>
<span class="betlink"><a href="/racebook" title="Hollywood Derby - Bet Now!"><img src="/themes/images/betnow-default.gif" border="0" width="67" /></a></span>
<div class="details">
<span class="track">Hollywood Derby - </span>
<a href="/racetrack?name=Hollywood_Park">Hollywood Park</a> - <strong>Race:</strong> #8, <strong>Grade:</strong> I, <strong>Purse:</strong> $250,000
</div>
</div>-->

<div class="note">
<p><strong>Friday, November 1, 2013</strong><br />
$500,000 Breeders' Cup Marathon (G2)<br />
$1 million Breeders' Cup Juvenile Fillies Turf (G1)<br />
$2 million Breeders' Cup Juvenile Fillies (G1)<br />
$2 million Breeders' Cup Filly & Mare Turf (G1)<br />
$2 million Breeders' Cup Distaff (formerly known as Ladies' Classic) (G1)
</p>

<p><strong>Saturday, November 2, 2013</strong><br />
$1 million Breeders' Cup Juvenile Turf (G1)<br />
$1 million Breeders' Cup Filly & Mare Sprint (G1)<br />
$1 million Breeders' Cup Dirt Mile (G1)<br />
$1 million Breeders' Cup Turf Sprint (G1)<br />
$2 million Breeders' Cup Juvenile (G1)<br />
$3 million Breeders' Cup Turf (G1)<br />
$1.5 million Breeders' Cup Sprint (G1)<br />
$2 million Breeders' Cup Mile (G1)<br />
$5 million Breeders' Cup Classic (G1)<br />
</p>
</div>

</div><!-- end:post -->

<div class="boxfooter"><span class="bold"><a href="/breeders-cup/races">Breeders' Cup Races</a></span></div>

<!--<div style="height: 45px; padding:10px 10px 0 10px;">
<strong>When is the Kentucky Derby?</strong><br />
The 137th Kentucky Derby is on Saturday May 7, 2011!

<strong>Where is the Kentucky Derby?</strong><br />
The Derby is raced at Churchill Downs Racecourse in Louisville, Kentucky
</div>-->

</div><!-- end:container -->
<!--<div class="boxfooter" style="padding-top:0;"><a title="More Kentucky Derby Details" href="/kentuckyderby">More Kentucky Derby</a></div>-->


</div>
