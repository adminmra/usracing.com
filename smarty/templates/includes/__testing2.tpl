
Notice: Constant SMARTYPATH already defined in /home/ah/usracing.com/htdocs/Feeds/ahr_block-graded-schedule-old.inc.php on line 2

Notice: Constant SITEABSOLUTEURL already defined in /home/ah/usracing.com/htdocs/Feeds/ahr_block-graded-schedule-old.inc.php on line 3

<div class="table-responsive">
			<table class="table table-condensed table-striped table-bordered" border="0" cellspacing="0" cellpadding="0" width="100%">
			<tbody>
				<tr>
					<th><strong>Date</strong></th>
					<th><strong>Stakes</strong></th>
					<th><strong>Track</strong></th>
					<!-- <th width="10%"><strong>Grade</strong></th> -->
					<th><strong>Purses</strong></th>
					<!-- <th width="10%"><strong>Age/Sex</strong></th>
					<th width="10%"><strong>DS</strong></th> -->
					<th><strong>Horse</strong></th>					<!-- <th width="12%"><strong>Jockey</strong></th>
					<th width="12%"><strong>Trainer</strong></th> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Modesty Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span><!-- Arlington Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9.5f T</td> -->
					<td>Walk Close</td>					<!-- 	<td>J. Graham</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Great Lady M. Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/los-alamitos" itemprop="url">Los Alamitos</a></span><!-- Los Alamitos --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3up f/m</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>Fantastic Style</td>					<!-- 	<td>R. Bejarano</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">American Derby</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span><!-- Arlington Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f T</td> -->
					<td>World Approval</td>					<!-- 	<td>J. Lezcano</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Arlington Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span><!-- Arlington Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f T</td> -->
					<td>Quite Force</td>					<!-- 	<td>J. Leparoux</td>
					<td>M. Maker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Stars and Stripes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/arlington-park" itemprop="url">Arlington Park</a></span><!-- Arlington Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>The Pizza Man</td>					<!-- 	<td>F. Geroux</td>
					<td>R. Brueggemann</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Delaware Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span><!-- Delaware Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Calamity Kate</td>					<!-- 	<td>E. Prado</td>
					<td>K. Breen</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Robert G. Dick Memorial Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span><!-- Delaware Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>11f T</td> -->
					<td>Ceisteach</td>					<!-- 	<td>C. Hill</td>
					<td>T. Proctor</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-11">Jul 11</time></td>
					<td itemprop="name">Parx Dash</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span><!-- Parx Racing --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>5f T</td> -->
					<td>Tightend Touchdown</td>					<!-- 	<td>F. Pennington</td>
					<td>J. Servis</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Azalea Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>Dogwood Trail</td>					<!-- 	<td>J. Rios</td>
					<td>S. Gold</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Carry Back Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f </td> -->
					<td>Grand Bili</td>					<!-- 	<td>J. Castellano</td>
					<td>G. Delgado</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Smile Sprint Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Favorite Tale</td>					<!-- 	<td>E. Zayas</td>
					<td>G. Preciado</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Salvator Mile Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f </td> -->
					<td>Bradester</td>					<!-- 	<td>C. Lanerie</td>
					<td>E. Kenneally</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">United Nations Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>11f T</td> -->
					<td>Big Blue Kitten</td>					<!-- 	<td>J. Bravo</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Dance Smartly Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Strut The Course</td>					<!-- 	<td>L. Contreras</td>
					<td>B. Minshall</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Highlander Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f T</td> -->
					<td>Go Blue Or Go Home</td>					<!-- 	<td>A. Garcia</td>
					<td>R. Baker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Singspiel Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Aldous Snow</td>					<!-- 	<td>E. Da Silva</td>
					<td>M. Pierce</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-05">Jul 05</time></td>
					<td itemprop="name">Princess Rooney Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f </td> -->
					<td>Merry Meadow</td>					<!-- 	<td>J. Castellano</td>
					<td>M. Hennig</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Belmont Derby Invitational</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Force The Pass</td>					<!-- 	<td>J. Rosario</td>
					<td>A. Goldberg</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Belmont Oaks Invitational</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Lady Eli</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Belmont Sprint Championship</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Private Zone</td>					<!-- 	<td>M. Pedroza</td>
					<td>J. Navarro</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Dwyer Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8f </td> -->
					<td>Speightster</td>					<!-- 	<td>J. Lezcano</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Suburban Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f </td> -->
					<td>Effinex</td>					<!-- 	<td>J. Alvarado</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Victory Ride Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>Irish Jasper</td>					<!-- 	<td>J. Castellano</td>
					<td>D. Ryan</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-04">Jul 04</time></td>
					<td itemprop="name">Dr. James Penny Memorial Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/parx-racing" itemprop="url">Parx Racing</a></span><!-- Parx Racing --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Coffee Clique</td>					<!-- 	<td>M. Franco</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-07-03">Jul 03</time></td>
					<td itemprop="name">Molly Pitcher Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Got Lucky</td>					<!-- 	<td>P. Lopez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-07-01">Jul 01</time></td>
					<td itemprop="name">Dominion Day Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f AW</td> -->
					<td>Red Rifle</td>					<!-- 	<td>A. Garcia</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-28">Jun 28</time></td>
					<td itemprop="name">San Juan Capistrano Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>14f T</td> -->
					<td>Crucero</td>					<!-- 	<td>K. Desormeaux</td>
					<td>J. Desormeaux</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">The Gold Cup at Santa Anita</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f </td> -->
					<td>Hard Aces</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Mother Goose Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Include Betty</td>					<!-- 	<td>D. Van Dyke</td>
					<td>T. Proctor</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Firecracker Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Departing</td>					<!-- 	<td>M. Mena</td>
					<td>A. Stall, Jr.</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Iowa Derby</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span><!-- Prairie Meadows --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Bent On Bourbon</td>					<!-- 	<td>J. Castellano</td>
					<td>E. Kenneally</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Iowa Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span><!-- Prairie Meadows --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Sarah Sis</td>					<!-- 	<td>J. Felix</td>
					<td>I. Mason</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Prairie Meadows Cornhusker Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/prairie-meadows" itemprop="url">Prairie Meadows</a></span><!-- Prairie Meadows --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Golden Lad</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Senorita Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Prize Exhibit</td>					<!-- 	<td>S. Gonzalez</td>
					<td>J. Cassidy</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-27">Jun 27</time></td>
					<td itemprop="name">Triple Bend Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Masochistic</td>					<!-- 	<td>T. Baze</td>
					<td>R. Ellis</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-21">Jun 21</time></td>
					<td itemprop="name">King Edward Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Tower Of Texas</td>					<!-- 	<td>E. Da Silva</td>
					<td>R. Attfield</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-21">Jun 21</time></td>
					<td itemprop="name">Pegasus Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Mr. Jordan</td>					<!-- 	<td>P. Lopez</td>
					<td>E. Plesa, Jr.</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-20">Jun 20</time></td>
					<td itemprop="name">Obeah Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/delaware-park" itemprop="url">Delaware Park</a></span><!-- Delaware Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f </td> -->
					<td>Luna Time</td>					<!-- 	<td>F. Boyce</td>
					<td>H. Motion</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-20">Jun 20</time></td>
					<td itemprop="name">Summertime Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Stellar Wind</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Poker Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>King Kreesa</td>					<!-- 	<td>J. Ortiz</td>
					<td>D. Donk</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Fleur de Lis Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f </td> -->
					<td>Frivolous</td>					<!-- 	<td>J. Court</td>
					<td>V. Oliver</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Matt Winn Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Island Town</td>					<!-- 	<td>J. Leparoux</td>
					<td>I. Wilkes</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Stephen Foster Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Noble Bird</td>					<!-- 	<td>S. Bridgmohan</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Adoration Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Beholder</td>					<!-- 	<td>G. Stevens</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-13">Jun 13</time></td>
					<td itemprop="name">Shoemaker Mile</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Talco</td>					<!-- 	<td>R. Bejarano</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-07">Jun 07</time></td>
					<td itemprop="name">Monmouth Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Triple Threat</td>					<!-- 	<td>J. Lezcano</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-07">Jun 07</time></td>
					<td itemprop="name">Affirmed Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Gimme Da Lute</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-07">Jun 07</time></td>
					<td itemprop="name">Eclipse Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td>Are You Kidding Me</td>					<!-- 	<td>A. Garcia</td>
					<td>R. Attfield</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Acorn Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$750,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f </td> -->
					<td>Curalina</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Belmont Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,500,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>12f </td> -->
					<td>American Pharoah</td>					<!-- 	<td>V. Espinoza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Brooklyn Invitational</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f </td> -->
					<td>Coach Inge</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Jaipur Invitational</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f T</td> -->
					<td>Channel Marker</td>					<!-- 	<td>F. Torres</td>
					<td>P. Bauer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Longines Just a Game Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$700,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Tepin</td>					<!-- 	<td>J. Leparoux</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Manhattan Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Slumber</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Metropolitan Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,250,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f </td> -->
					<td>Honor Code</td>					<!-- 	<td>J. Castellano</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Ogden Phipps Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Wedding Toast</td>					<!-- 	<td>J. Lezcano</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Woody Stephens Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>March</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-06">Jun 06</time></td>
					<td itemprop="name">Old Forester Mint Julep</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Kiss Moon</td>					<!-- 	<td>C. Lanerie</td>
					<td>D. Vance</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-06-05">Jun 05</time></td>
					<td itemprop="name">New York Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Waltzing Matilda</td>					<!-- 	<td>J. Alvarado</td>
					<td>T. Stack</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-06-05">Jun 05</time></td>
					<td itemprop="name">True North Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Rock Fall</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-30">May 30</time></td>
					<td itemprop="name">Connaught Cup Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f T</td> -->
					<td>Lockout</td>					<!-- 	<td>G. Boulanger</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-30">May 30</time></td>
					<td itemprop="name">Californian Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Catch A Flight</td>					<!-- 	<td>G. Stevens</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-30">May 30</time></td>
					<td itemprop="name">American Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Spanish Queen</td>					<!-- 	<td>B. Blanc</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-30">May 30</time></td>
					<td itemprop="name">Penn Mile</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/penn-national" itemprop="url">Penn National</a></span><!-- Penn National --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Force The Pass</td>					<!-- 	<td>J. Rosario</td>
					<td>A. Goldberg</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-30">May 30</time></td>
					<td itemprop="name">Aristides</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Alsvid</td>					<!-- 	<td>C. Landeros</td>
					<td>C. Hartman</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-25">May 25</time></td>
					<td itemprop="name">Winning Colors Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>6f </td> -->
					<td>Street Story</td>					<!-- 	<td>F. Geroux</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-25">May 25</time></td>
					<td itemprop="name">All American</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate" itemprop="url">Golden Gate</a></span><!-- Golden Gate --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f AW</td> -->
					<td>G.g. Ryder</td>					<!-- 	<td>R. Gonzalez</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-25">May 25</time></td>
					<td itemprop="name">Lone Star Park Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Lone Star</span><!-- Lone Star --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Majestic City</td>					<!-- 	<td>C. Lopez</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-25">May 25</time></td>
					<td itemprop="name">Gamely Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Hard Not To Like</td>					<!-- 	<td>V. Espinoza</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-25">May 25</time></td>
					<td itemprop="name">Los Angeles Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Distinctiv Passion</td>					<!-- 	<td>E. Maldonado</td>
					<td>J. Bonde</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-24">May 24</time></td>
					<td itemprop="name">Charles Whittingham Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Ashleyluvssugar</td>					<!-- 	<td>G. Stevens</td>
					<td>P. Eurton</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-23">May 23</time></td>
					<td itemprop="name">Nassau Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Sky Treasure</td>					<!-- 	<td>G. Boulanger</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-23">May 23</time></td>
					<td itemprop="name">Louisville Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Xtra Luck</td>					<!-- 	<td>B. Hernandez, Jr.</td>
					<td>N. Howard</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-17">May 17</time></td>
					<td itemprop="name">Vigil Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f AW</td> -->
					<td>Black Hornet</td>					<!-- 	<td>L. Contreras</td>
					<td>P. Parente</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Vagrancy Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>La Verdad</td>					<!-- 	<td>J. Ortiz</td>
					<td>L. Rice</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Red Bank Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Monmouth</span><!-- Monmouth --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Winning Cause</td>					<!-- 	<td>A. Castellano, Jr.</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Gallorette Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Watsdachances</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Longines Dixie Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Ironicus</td>					<!-- 	<td>J. Castellano</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Maryland Sprint Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Sandbar</td>					<!-- 	<td>J. Rosario</td>
					<td>J. Sharp</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Preakness Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,500,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9.5f </td> -->
					<td>American Pharoah</td>					<!-- 	<td>V. Espinoza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-16">May 16</time></td>
					<td itemprop="name">Marine Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f AW</td> -->
					<td>Shaman Ghost</td>					<!-- 	<td>R. Hernandez</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-15">May 15</time></td>
					<td itemprop="name">Adena Springs Miss Preakness Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>6f </td> -->
					<td>Irish Jasper</td>					<!-- 	<td>K. Carmouche</td>
					<td>D. Ryan</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-15">May 15</time></td>
					<td itemprop="name">Allaire duPont Distaff Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f </td> -->
					<td>Stopchargingmaria</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-15">May 15</time></td>
					<td itemprop="name">Black-Eyed Susan Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f </td> -->
					<td>Keen Pauline</td>					<!-- 	<td>J. Castellano</td>
					<td>D. Romans</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-15">May 15</time></td>
					<td itemprop="name">Pimlico Special</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/pimlico" itemprop="url">Pimlico</a></span><!-- Pimlico --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>9.5f </td> -->
					<td>Commissioner</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-10">May 10</time></td>
					<td itemprop="name">Lazaro Barrera Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>Kentuckian</td>					<!-- 	<td>M. Smith</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">Beaugay Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Discreet Marq</td>					<!-- 	<td>J. Velazquez</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">Peter Pan Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Madefromlucky</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">Ruffian Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f </td> -->
					<td>Wedding Toast</td>					<!-- 	<td>J. Lezcano</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">American Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Bal A Bali</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">Hendrie Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f AW</td> -->
					<td>Skylander Girl</td>					<!-- 	<td>E. Ramsammy</td>
					<td>A. Patykewich</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-09">May 09</time></td>
					<td itemprop="name">Vanity Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f </td> -->
					<td>My Sweet Addiction</td>					<!-- 	<td>M. Smith</td>
					<td>M. Jones</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-03">May 03</time></td>
					<td itemprop="name">Honeymoon Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Spanish Queen</td>					<!-- 	<td>B. Blanc</td>
					<td>R. Baltas</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Fort Marcy Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Big Blue Kitten</td>					<!-- 	<td>J. Bravo</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Sheepshead Bay Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f T</td> -->
					<td>Rosalind</td>					<!-- 	<td>J. Bravo</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Westchester Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/belmont-park" itemprop="url">Belmont Park</a></span><!-- Belmont Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f </td> -->
					<td>Tonalist</td>					<!-- 	<td>J. Bravo</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">American Turf</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Divisidero</td>					<!-- 	<td>R. Hernandez</td>
					<td>W. Bradley</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Churchill Distaff Turf Mile</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Tepin</td>					<!-- 	<td>J. Leparoux</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Churchill Downs</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Private Zone</td>					<!-- 	<td>M. Pedroza</td>
					<td>J. Navarro</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Humana Distaff</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f </td> -->
					<td>Dame Dorothy</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Kentucky Derby</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$2,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>10f </td> -->
					<td>American Pharoah</td>					<!-- 	<td>V. Espinoza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Pat Day Mile Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8f </td> -->
					<td>Competitive Edge</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Woodford Reserve Turf Classic Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Finnegans Wake</td>					<!-- 	<td>V. Espinoza</td>
					<td>P. Miller</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-02">May 02</time></td>
					<td itemprop="name">Precisionist Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Catch A Flight</td>					<!-- 	<td>F. Prat</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Texas Mile Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/lonestar-park" itemprop="url">Lonestar Park</a></span><!-- Lonestar Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f</td> -->
					<td>Texas Air</td>					<!-- 	<td>Q. Hamilton</td>
					<td>A. Milligan</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Alysheba Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Protonico</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Edgewood Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Feathered</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Eight Belles Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f </td> -->
					<td>Promise Me Silver</td>					<!-- 	<td>R. Albarado</td>
					<td>W. Calhoun</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">La Troienne Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Molly Morgan</td>					<!-- 	<td>C. Lanerie</td>
					<td>D. Romans</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Kentucky Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f </td> -->
					<td>Lovely Maria</td>					<!-- 	<td>K. Clark</td>
					<td>J. Jones</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-05-01">May 01</time></td>
					<td itemprop="name">Turf Sprint</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/churchill-downs" itemprop="url">Churchill Downs</a></span><!-- Churchill Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>5f T</td> -->
					<td>Power Alert</td>					<!-- 	<td>J. Leparoux</td>
					<td>B. Lynch</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-26">Apr 26</time></td>
					<td itemprop="name">Wilshire Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Blingismything</td>					<!-- 	<td>T. Baze</td>
					<td>J. Kruljac</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-25">Apr 25</time></td>
					<td itemprop="name">Last Tycoon Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Si Sage</td>					<!-- 	<td>E. Trujillo</td>
					<td>D. Vienna</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-25">Apr 25</time></td>
					<td itemprop="name">Miami Mile Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/gulfstream-park" itemprop="url">Gulfstream Park</a></span><!-- Gulfstream Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Rerun</td>					<!-- 	<td>J. Caraballo</td>
					<td>C. Stewart</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-25">Apr 25</time></td>
					<td itemprop="name">San Francisco Mile</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate" itemprop="url">Golden Gate</a></span><!-- Golden Gate --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>G. G. Ryder</td>					<!-- 	<td>R. Gonzalez</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-25">Apr 25</time></td>
					<td itemprop="name">Excelsior Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f </td> -->
					<td>Effinex</td>					<!-- 	<td>A. Arroyo</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-19">Apr 19</time></td>
					<td itemprop="name">San Simeon Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f T</td> -->
					<td>Get Happy Mister</td>					<!-- 	<td>T. Baze</td>
					<td>M. Tsagalakis</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Santa Barbara Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Queen Of The Sand</td>					<!-- 	<td>D. Van Dyke</td>
					<td>P. Gallagher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Whimsical Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/woodbine" itemprop="url">Woodbine</a></span><!-- Woodbine --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f T</td> -->
					<td>Unspurned</td>					<!-- 	<td>A. Garcia</td>
					<td>R. Attfield</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Sixty Sails Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne" itemprop="url">Hawthorne</a></span><!-- Hawthorne --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>9f</td> -->
					<td>Yahilwa</td>					<!-- 	<td>E. Trujillo</td>
					<td>J. Cassidy</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/illinois-derby">Illinois Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/hawthorne" itemprop="url">Hawthorne</a></span><!-- Hawthorne --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f</td> -->
					<td>Whiskey Ticket</td>					<!-- 	<td>M. Pedroza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Dixiana Elkhorn Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Dramedy</td>					<!-- 	<td>J. Bravo</td>
					<td>G. Aschinger</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Charles Town Classic</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/charles-town" itemprop="url">Charles Town</a></span><!-- Charles Town --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$1,500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Moreno</td>					<!-- 	<td>C. Velasquez</td>
					<td>E. Guillot</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-18">Apr 18</time></td>
					<td itemprop="name">Distaff Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6f </td> -->
					<td>La Verdad</td>					<!-- 	<td>J. Ortiz</td>
					<td>L. Rice</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-17">Apr 17</time></td>
					<td itemprop="name">Hilliard Lyons Doubledogdare Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Deceptive Vision</td>					<!-- 	<td>J. Velazquez</td>
					<td>M. Pierce</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-15">Apr 15</time></td>
					<td itemprop="name">Transylvania Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Night Prowler</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-12">Apr 12</time></td>
					<td itemprop="name">Appalachian Stakes Presented by Japan Racing Association</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$125,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Lady Eli</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-12">Apr 12</time></td>
					<td itemprop="name">Adena Springs Beaumont Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f </td> -->
					<td>Miss Ella</td>					<!-- 	<td>R. Maragh</td>
					<td>H. Motion</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Top Flight Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f </td> -->
					<td>House Rules</td>					<!-- 	<td>J. Alvarado</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Ben Ali Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Protonico</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Coolmore Lexington Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Divining Red</td>					<!-- 	<td>J. Leparoux</td>
					<td>A. Delacour</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Jenny Wiley Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Ball Dancing</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/arkansas-derby">Arkansas Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>American Pharoah</td>					<!-- 	<td>V. Espinoza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Oaklawn Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$600,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Race Day</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Kona Gold Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>Masochistic</td>					<!-- 	<td>T. Baze</td>
					<td>R. Ellis</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-11">Apr 11</time></td>
					<td itemprop="name">Las Cienegas Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f T</td> -->
					<td>Home Journey</td>					<!-- 	<td>R. Bejarano</td>
					<td>M. Puype</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-10">Apr 10</time></td>
					<td itemprop="name">Apple Blossom Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$600,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Untapable</td>					<!-- 	<td>J. Velazquez</td>
					<td>S. Asmussen</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-09">Apr 09</time></td>
					<td itemprop="name">Count Fleet Sprint Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Alsvid</td>					<!-- 	<td>C. Landeros</td>
					<td>C. Hartman</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Madison Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$350,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f </td> -->
					<td>Princess Violet</td>					<!-- 	<td>J. Alvarado</td>
					<td>L. Rice</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Wood Memorial Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Frosted</td>					<!-- 	<td>J. Rosario</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Gazelle Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f </td> -->
					<td>Condo Commando</td>					<!-- 	<td>J. Rosario</td>
					<td>R. Rodriguez</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Carter Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Dads Caps</td>					<!-- 	<td>J. Ortiz</td>
					<td>R. Rodriguez</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Bay Shore Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>March</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Central Bank Ashland Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Lovely Maria</td>					<!-- 	<td>K. Clark</td>
					<td>J. Jones</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Shakertown Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$125,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>5.5f T</td> -->
					<td>Something Extra</td>					<!-- 	<td>S. Bridgmohan</td>
					<td>G. Cox</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Santa Anita Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Stellar Wind</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/santa-anita-derby">Santa Anita Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Dortmund</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Providencia Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Spirit Of Xian</td>					<!-- 	<td>J. Talamo</td>
					<td>R. Mandella</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Fantasy Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Include Betty</td>					<!-- 	<td>R. Homeister, Jr.</td>
					<td>T. Proctor</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-04-04">Apr 04</time></td>
					<td itemprop="name">Toyota Blue Grass Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/keeneland" itemprop="url">Keeneland</a></span><!-- Keeneland --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Carpe Diem</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/louisiana-derby">Louisiana Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$750,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>International Star</td>					<!-- 	<td>M. Mena</td>
					<td>M. Maker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Mervin H. Muniz Jr. Memorial Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Chocolate Ride</td>					<!-- 	<td>J. Talamo</td>
					<td>B. Cox</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">New Orleans Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Call Me George</td>					<!-- 	<td>J. Graham</td>
					<td>G. Forster</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Appleton Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>War Correspondent</td>					<!-- 	<td>J. Velazquez</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/florida-derby">Florida Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Materiality</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Gulfstream Park Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Birdatthewire</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>D. Romans</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Honey Fox Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Lady Lara</td>					<!-- 	<td>J. Alvarado</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Orchid Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Beauty Parlor</td>					<!-- 	<td>J. Velazquez</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Pan American Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Imagining</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Skip Away Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9.5f </td> -->
					<td>Commissioner</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-28">Mar 28</time></td>
					<td itemprop="name">Tokyo City Cup</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f </td> -->
					<td>Sky Kingdom</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-22">Mar 22</time></td>
					<td itemprop="name">Sunland Derby</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/sunland-park" itemprop="url">Sunland Park</a></span><!-- Sunland Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$800,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f </td> -->
					<td>Firing Line</td>					<!-- 	<td>G. Stevens</td>
					<td>S. Callaghan</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-21">Mar 21</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/spiral-stakes">Spiral Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/turfway-park" itemprop="url">Turfway Park</a></span><!-- Turfway Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$550,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f AW</td> -->
					<td>Dubai Sky</td>					<!-- 	<td>J. Lezcano</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-21">Mar 21</time></td>
					<td itemprop="name">San Luis Rey Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>12f T</td> -->
					<td>Ashleyluvssugar</td>					<!-- 	<td>G. Stevens</td>
					<td>P. Eurton</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-21">Mar 21</time></td>
					<td itemprop="name">Royal Delta Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Sheer Drama</td>					<!-- 	<td>J. Bravo</td>
					<td>D. Fawkes</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-21">Mar 21</time></td>
					<td itemprop="name">Inside Information Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>7f </td> -->
					<td>Classic Point</td>					<!-- 	<td>P. Lopez</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-15">Mar 15</time></td>
					<td itemprop="name">Santa Ana Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Hoop Of Colour</td>					<!-- 	<td>D. Van Dyke</td>
					<td>H. Motion</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-14">Mar 14</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/rebel-stakes">Rebel Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$750,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>American Pharoah</td>					<!-- 	<td>V. Espinoza</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-14">Mar 14</time></td>
					<td itemprop="name">Razorback Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Race Day</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-14">Mar 14</time></td>
					<td itemprop="name">Azeri Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Gold Medal Dancer</td>					<!-- 	<td>L. Quinonez</td>
					<td>D. Von Hemel</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">San Felipe Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Dortmund;</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/gotham-stakes">Gotham Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>El Kabeir</td>					<!-- 	<td>C. Lopez</td>
					<td>J. Terranova, II</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Tom Fool Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Salutos Amigos</td>					<!-- 	<td>C. Velasquez</td>
					<td>D. Jacobson</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Palm Beach Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Eh Cumpari</td>					<!-- 	<td>J. Caraballo</td>
					<td>M. Dilger</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Swale Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>Ready For Rye</td>					<!-- 	<td>L. Saez</td>
					<td>T. Albertrani</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Gulfstream Park Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f </td> -->
					<td>Honor Code</td>					<!-- 	<td>J. Castellano</td>
					<td>C. McGaughey III</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Honeybee Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Sarah Sis</td>					<!-- 	<td>J. Felix</td>
					<td>I. Mason</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Frank E. Kilroe Mile</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Ring Weekend</td>					<!-- 	<td>D. Van Dyke</td>
					<td>H. Motion</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">San Carlos Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Wild Dude</td>					<!-- 	<td>R. Bejarano</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/tampa-bay-derby">Tampa Bay Derby</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span><!-- Tampa Bay Downs --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$350,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Carpe Diem</td>					<!-- 	<td>J. Velazquez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Santa Anita Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$1,000,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f </td> -->
					<td>Shared Belief</td>					<!-- 	<td>M. Smith</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-03-07">Mar 07</time></td>
					<td itemprop="name">Florida Oaks</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span><!-- Tampa Bay Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Quality Rocks</td>					<!-- 	<td>J. Lezcano</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-28">Feb 28</time></td>
					<td itemprop="name">Herecomesthebride Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Devine Aida</td>					<!-- 	<td>J. Rios</td>
					<td>R. Morales</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-28">Feb 28</time></td>
					<td itemprop="name">Santa Ysabel Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Stellar Wind</td>					<!-- 	<td>V. Espinoza</td>
					<td>J. Sadler</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-22">Feb 22</time></td>
					<td itemprop="name">Southwest Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Far Right</td>					<!-- 	<td>M. Smith</td>
					<td>R. Moquett</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Fair Grounds Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$125,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Chocolate Ride</td>					<!-- 	<td>F. Geroux</td>
					<td>B. Cox</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Mineshaft Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$125,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Street Babe</td>					<!-- 	<td>K. Clark</td>
					<td>M. Dilger</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/risen-star-stakes">Risen Star Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>International Star</td>					<!-- 	<td>M. Mena</td>
					<td>M. Maker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Canadian Turf Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Long On Value</td>					<!-- 	<td>J. Rosario</td>
					<td>W. Mott</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Fountain of Youth Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Itsaknockout</td>					<!-- 	<td>L. Saez</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Gulfstream Park Sprint Championship</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>C Zee</td>					<!-- 	<td>E. Zayas</td>
					<td>S. Gold</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Mac Diarmida Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>11f T</td> -->
					<td>Main Sequence</td>					<!-- 	<td>R. Maragh</td>
					<td>H. Motion</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">Rampart Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f </td> -->
					<td>House Rules</td>					<!-- 	<td>J. Castellano</td>
					<td>J. Jerkens</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-21">Feb 21</time></td>
					<td itemprop="name">The Very One Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>11f T</td> -->
					<td>Irish Mission</td>					<!-- 	<td>J. Velazquez</td>
					<td>C. Clement</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-16">Feb 16</time></td>
					<td itemprop="name">Buena Vista Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Diversy Harbor</td>					<!-- 	<td>G. Stevens</td>
					<td>T. Proctor</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-16">Feb 16</time></td>
					<td itemprop="name">General George Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/laurel-park" itemprop="url">Laurel Park</a></span><!-- Laurel Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3&amp;up</td> -->
					<!-- 	<td>7f </td> -->
					<td>Misconnect</td>					<!-- 	<td>K. Carmouche</td>
					<td>B. Levine</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-15">Feb 15</time></td>
					<td itemprop="name">Bayakoa Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/oaklawn-park" itemprop="url">Oaklawn Park</a></span><!-- Oaklawn Park --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Mufajaah</td>					<!-- 	<td>E. Esquivel</td>
					<td>D. Peitz</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-14">Feb 14</time></td>
					<td itemprop="name">El Camino Real Derby</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/golden-gate" itemprop="url">Golden Gate</a></span><!-- Golden Gate --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>9f AW</td> -->
					<td>Metaboss</td>					<!-- 	<td>A. Solis</td>
					<td>J. Bonde</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-14">Feb 14</time></td>
					<td itemprop="name">Hurricane Bertie Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>Merry Meadow</td>					<!-- 	<td>J. Castellano</td>
					<td>M. Hennig</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-14">Feb 14</time></td>
					<td itemprop="name">Barbara Fritchie Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/laurel-park" itemprop="url">Laurel Park</a></span><!-- Laurel Park --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3&amp;up f/m</td> -->
					<!-- 	<td>7f </td> -->
					<td>Lady Sabelia</td>					<!-- 	<td>H. Karamanos</td>
					<td>R. Graham</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Toboggan Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Salutos Amigos</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>D. Jacobson</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Withers Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Far From Over</td>					<!-- 	<td>M. Franco</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Donn Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Constitution</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Suwannee River Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Sandiva</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Fred W. Hooper Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f </td> -->
					<td>Valid</td>					<!-- 	<td>P. Lopez</td>
					<td>M. Vitali</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Gulfstream Park Turf Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Mshawish</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">Robert B. Lewis Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Dortmund</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">San Antonio Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$500,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f </td> -->
					<td>Shared Belief</td>					<!-- 	<td>M. Smith</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-02-07">Feb 07</time></td>
					<td itemprop="name">San Marcos Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>10f T</td> -->
					<td>Finnegans Wake</td>					<!-- 	<td>V. Espinoza</td>
					<td>P. Miller</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-02-01">Feb 01</time></td>
					<td itemprop="name">San Vicente Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>Lord Nelson</td>					<!-- 	<td>R. Bejarano</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-31">Jan 31</time></td>
					<td itemprop="name">Arcadia Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Avanzare</td>					<!-- 	<td>G. Stevens</td>
					<td>T. Proctor</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-31">Jan 31</time></td>
					<td itemprop="name">Las Virgenes Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>I</td> -->
					<td itemprop="description">$300,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f </td> -->
					<td>Callback</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-31">Jan 31</time></td>
					<td itemprop="name">Palos Verdes Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>6f </td> -->
					<td>Conquest Two Step</td>					<!-- 	<td>J. Talamo</td>
					<td>M. Casse</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-31">Jan 31</time></td>
					<td itemprop="name">Endeavour Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span><!-- Tampa Bay Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Testa Rossi</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-31">Jan 31</time></td>
					<td itemprop="name">Sam F. Davis Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span><!-- Tampa Bay Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$250,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Ocean Knight</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>K. McLaughlin</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name">Sweetest Chant Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>8f T</td> -->
					<td>Consumer Credit</td>					<!-- 	<td>E. Zayas</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/forward-gal-stakes">Forward Gal Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>7f </td> -->
					<td>Birdatthewire</td>					<!-- 	<td>I. Ortiz, Jr.</td>
					<td>D. Romans</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/holy-bull-stakes">Holy Bull Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$400,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Upstart</td>					<!-- 	<td>J. Ortiz</td>
					<td>R. Violette, Jr.</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name">Hutcheson Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7f </td> -->
					<td>Barbados</td>					<!-- 	<td>L. Saez</td>
					<td>M. Tomlinson</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name">Tampa Bay Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/tampa-bay-downs" itemprop="url">Tampa Bay Downs</a></span><!-- Tampa Bay Downs --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$150,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Lochte</td>					<!-- 	<td>P. Lopez</td>
					<td>M. Vitali</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-24">Jan 24</time></td>
					<td itemprop="name">Connally Turf Cup</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/sam-houston" itemprop="url">Sam Houston</a></span><!-- Sam Houston --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Coalport</td>					<!-- 	<td>M. Mena</td>
					<td>M. Maker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-17">Jan 17</time></td>
					<td itemprop="name">La Ca&ntilde;ada Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4yo f</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Thegirlinthatsong</td>					<!-- 	<td>R. Bejarano</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-17">Jan 17</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/lecomte-stakes">Lecomte Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.32f </td> -->
					<td>International Star</td>					<!-- 	<td>M. Mena</td>
					<td>M. Maker</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-17">Jan 17</time></td>
					<td itemprop="name">Col. E.R. Bradley Handicap</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/fair-grounds" itemprop="url">Fair Grounds</a></span><!-- Fair Grounds --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>String King</td>					<!-- 	<td>J. Graham</td>
					<td>C. Smith</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-10">Jan 10</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/fort-lauderdale-stakes">Fort Lauderdale Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f T</td> -->
					<td>Mshawish</td>					<!-- 	<td>J. Castellano</td>
					<td>T. Pletcher</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-10">Jan 10</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/san-pasqual-stakes">San Pasqual Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>8.5f </td> -->
					<td>Hoppertunity</td>					<!-- 	<td>M. Garcia</td>
					<td>B. Baffert</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-10">Jan 10</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/sham-stakes">Sham Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8f </td> -->
					<td>Calculator</td>					<!-- 	<td>E. Trujillo</td>
					<td>P. Miller</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-04">Jan 04</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/monrovia-stakes">Monrovia Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up f/m</td> -->
					<!-- 	<td>6.5f T</td> -->
					<td>Shrinking Violet</td>					<!-- 	<td>K. Desormeaux</td>
					<td>W. Ward</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-03">Jan 03</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/santa-ynez-stakes">Santa Ynez Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo f</td> -->
					<!-- 	<td>6.5f </td> -->
					<td>Seduire</td>					<!-- 	<td>R. Bejarano</td>
					<td>J. Hollendorfer</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-03">Jan 03</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/san-gabriel-stakes">San Gabriel Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Santa Anita</span><!-- Santa Anita --></td>
					<!-- <td>II</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>4&amp;up</td> -->
					<!-- 	<td>9f T</td> -->
					<td>Finnegans Wake</td>					<!-- 	<td>V. Espinoza</td>
					<td>P. Miller</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event">					<td><time itemprop="startDate" content="2015-01-03">Jan 03</time></td>
					<td itemprop="name">Dania Beach Stakes</td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name">Gulfstream</span><!-- Gulfstream --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$100,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>7.5f T</td> -->
					<td>Night Prowler</td>					<!-- 	<td>J. Castellano</td>
					<td>C. Brown</td> -->
				</tr>
				<tr itemscope="" itemtype="http://schema.org/Event" class="odd">					<td><time itemprop="startDate" content="2015-01-03">Jan 03</time></td>
					<td itemprop="name"><a itemprop="url" href="http://www.usracing.com/jerome-stakes">Jerome Stakes</a></td>
					<td itemprop="location" itemscope itemtype="http://schema.org/Place"><span itemprop="name"><a itemprop="location" itemscope itemtype="http://schema.org/Place" href="http://www.usracing.com/aqueduct" itemprop="url">Aqueduct</a></span><!-- Aqueduct --></td>
					<!-- <td>III</td> -->
					<td itemprop="description">$200,000</td>
					<!-- <td>3yo</td> -->
					<!-- 	<td>8.32f </td> -->
					<td>El Kabeir</td>					<!-- 	<td>C. Lopez</td>
					<td>J. Terranova, II</td> -->
				</tr>
							</tbody>
		</table>
		</div>
