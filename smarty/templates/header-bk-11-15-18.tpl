<head>
{*
{literal}

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-742771-1', 'auto');
  ga('send', 'pageview');
</script>

{/literal}
*}
{$title}
<link rel="image_src" href="/themes/images/thumb.jpg" title="US Racing" />
{$link}
{$metatag}
{$css}

{if $redirect_switch=="1"}
{include file='redirectsripts.tpl'}
{/if}

  

{include file='scripts.tpl'}
{$js}

{* included this style .. as an experiment *}


{literal}
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126793368-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-126793368-1');
</script>
{/literal}
{literal}
<style>





@media screen and (max-width:767px) and (min-width:0px) {
.sidr ul li ul li {
    background: #fff;
    line-height: 40px;
    border-bottom: 0px solid #fff;
}

  .bc-nemu-empty{
    border-bottom: 0px solid #fff;
  }
}

</style>
{/literal}
</head>
