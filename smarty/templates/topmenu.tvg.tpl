<div class="login-header">
    <a class="btn btn-sm btn-default loginToggle collapsed" data-toggle="collapse" data-target=".login-responsive-collapse" rel="nofollow"><span>Login</span><i class="fa fa-angle-down"></i></a>
    <a class="btn btn-sm btn-red" id="signupBtn" href="https://secure.usracing.com/signup"  rel="nofollow"><span>Sign Up</span><i class="glyphicon glyphicon-pencil"></i></a>
</div>
 <div class="login collapse login-responsive-collapse">   
   <form class="form-inline" id="loginForm" name="loginForm" method="post" action="https://secure.usracing.com/aws/login" role="form">
   			<div class="form-group account-group">
                <label for="account" class="sr-only account">Account:</label>
                <input id="account"  type="text" name="account" maxlength="20" placeholder="Account #" class="form-control" />
                <a href="/forgot" id="forgotAccount" class="btn btn-sm btn-info forgot" rel="nofollow">Forgot</a>
            </div>
            <div class="form-group pin-group">
            <label for="PIN" class="sr-only pin">PIN</label>
            <input id="pin" type="password" name="pin" maxlength="6" placeholder="PIN" class="form-control" />
            <a href="/forgot" id="forgotPIN" class="btn btn-sm btn-info forgot" rel="nofollow">Forgot</a>
            </div>
            <div class="form-group select-group">
            	<select id="state" name="state" class="form-control">
                    <option value="0">State</option>
		            <option value="AL">Alabama</option>
		            <option value="AK">Alaska</option>
		            <option value="AZ">Arizona</option>
		            <option value="AR">Arkansas</option>
		            <option value="CA">California</option>
		            <option value="CO">Colorado</option>
		            <option value="CT">Connecticut</option>
		            <option value="DE">Delaware</option>
		            <option value="DC">District of Columbia</option>
		            <option value="FL">Florida</option>
		            <option value="GA">Georgia</option>
		            <option value="HI">Hawaii</option>
		            <option value="ID">Idaho</option>
		            <option value="IL">Illinois</option>
		            <option value="IN">Indiana</option>
		            <option value="IA">Iowa</option>
		            <option value="KS">Kansas</option>
		            <option value="KY">Kentucky</option>
		            <option value="LA">Louisiana</option>
		            <option value="ME">Maine</option>
		            <option value="MD">Maryland</option>
		            <option value="MA">Massachusetts</option>
		            <option value="MI">Michigan</option>
		            <option value="MN">Minnesota</option>
		            <option value="MS">Mississippi</option>
		            <option value="MO">Missouri</option>
		            <option value="MT">Montana</option>
		            <option value="NE">Nebraska</option>
		            <option value="NV">Nevada</option>
		            <option value="NH">New Hampshire</option>
		            <option value="NJ">New Jersey</option>
		            <option value="NM">New Mexico</option>
		            <option value="NY">New York</option>
		            <option value="NC">North Carolina</option>
		            <option value="ND">North Dakota</option>
		            <option value="OH">Ohio</option>
		            <option value="OK">Oklahoma</option>
		            <option value="OR">Oregon</option>
		            <option value="PA">Pennsylvania</option>
		            <option value="RI">Rhode Island</option>
		            <option value="SC">South Carolina</option>
		            <option value="SD">South Dakota</option>
		            <option value="TN">Tennessee</option>
		            <option value="TX">Texas</option>
		            <option value="UT">Utah</option>
		            <option value="VT">Vermont</option>
		            <option value="VA">Virginia</option>
		            <option value="WA">Washington</option>
		            <option value="WV">West Virginia</option>
		            <option value="WI">Wisconsin</option>
		            <option value="WY">Wyoming</option>
                </select>
           </div>
           <div class="form-group login-group">
                <input id="timezone" type="hidden"  name="timezone" value="0" />
                <div class="loginGroup">
                <input id="loginButton" type="button" class="btn btn-sm btn-default" value="Login" />
                </div>
            </div>
            
        </form>
</div> <!-- end/login -->
