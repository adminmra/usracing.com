{literal}
      <style>
      .infoBlocks .info {
            overflow: initial;
      }
      @media (min-width: 1201px) and (max-width: 1920px) {
            .infoBlocks .col-md-4 .section {
                  height: 250px;
            }
      }
      @media (min-width: 1025px) and (max-width: 1200px) {
            .infoBlocks .col-md-4 .section {
                  height: 320px;
            }
      }
      @media (min-width: 992px) and (max-width: 1024px) {
            .infoBlocks .col-md-4 .section {
                  height: 280px;
            }
      }
      </style>
{/literal}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}
<div class="justify-left">
    <img  class="img-responsive" width="100%" src="/img/kentuckyderby/kentucky-derby-mint-julep.jpg" alt="Mint Julep Recipe" />
    


				<h2>Mint Juleps and Mint Julep Recipes!</h2>

<p ><strong>How do you make a mint julep?</strong><br />  Mint Julep recipes are below!<br />
<strong>What is in a mint julep?</strong><br />  The ingredients in a mint julep vary depending on the hand that serves them.</p>
<p>&nbsp;</p>
<h2 class>The <i>Maker's Mark.</i> Mint Julep</h2> 
<p>2 cups sugar<br>2 cups water<br>Sprigs of fresh mint<br>Crushed ice<br>Maker's Mark. Kentucky Whisky<br>Silver Julep Cups</p>

<p>Make a simple syrup by boiling sugar and water together for five minutes. Cool and place in a covered container with six or eight sprigs of fresh mint, then refrigerate overnight. Make one julep at a time by filling a julep cup with crushed ice, adding one tablespoon mint syrup and two ounces of <i>Maker's Mark. </i>Kentucky Whisky. Stir rapidly with a spoon to frost the outside of the cup. Garnish with a sprig of fresh mint.</p>

<h2>The Mint Julep by Robert Hess, Mixologist</h2>
<iframe class="video" src="//www.youtube.com/embed/wiWOzS_A4r0" frameborder="0" allowfullscreen></iframe>
<p>Geez... I could use one right now...</p>
<h2>The First Saturday in May renews a Tradition. . .</h2>
</p><p>The mint julep is a tradition as old as the Kentucky Derby itself, as much a part of Derby tradition as bugles and roses. Rarely seen the rest of the year, this potent concoction is the national drink for a few hours every first weekend in May. 
</p><p>The julep is the official toast to the winning horse, but fans at Derby parties tend to start long before the finish. Most Southerners will admit that it's an acquired taste, at best, this mixture of bourbon, sugar, mint, and ice. 
</p><p>"I like the taste. I grew up with them," native Kentuckian Norma Taylor says with a smile. "You have to like bourbon...and mint." 
</p><p>Like another Southern delicacy, Coca-Cola, the julep was concocted to mask the taste of medicine. It caught on among the healthy.
</p><p>
Legendary U.S. Sen. Henry Clay served juleps on his Kentucky plantation, and introduced Northerners to the beverage when he went to Washington. In the 1850s, Clay brought his recipe to Washington's Willard Hotel.
</p><p>
Willard bartender Jim Hewes still makes juleps based on Clay's recipe: "A teaspoon of sugar, six or eight red-stem mint leaves, and a small measure of bourbon," Hews says.</p>
<p>He churns that mixture, then adds a lot of ice, more bourbon, a splash of water, a sprig of mint and a sprinkling of sugar on top.</p>
<p>
Controversy rages over the minutiae of a proper julep -- chipped or shaved ice, crystalline or boiled sugar -- but julep purists agree that a real mint julep must be served in a frosted silver julep glass. And, of course, made with the finest Kentucky bourbon. Moonlight and magnolias are optional.</p>

<h2> "The Quintessence of Gentlemanly Beverages. . . " </h2>
<strong> --Lt. Gen. S.B. Buckner, Jr.*</strong>



<p>Major General Wm. D. Connor<br />
West Point, N.Y. 
<p>My dear General Connor: </p>
<p align="justify">Your letter requesting my formula for mixing mint juleps leaves me in the same position in which Capt. Barber found himself when asked how he was able to carve the image of an elephant from a block of wood. He replied that it was a simple process consisting merely of whittling off the part that didn't look like an elephant.</p>
<p align="justify">The preparation of the quintessence of gentlemanly beverages can only be described in like terms. A mint julep is not the product of a formula. It is a ceremony and must be performed by a gentleman possessing a true sense of the artistic, a deep reverence for the ingredients and a proper appreciation of the occasion. It is a rite that must not be entrusted to a novice, a statistician nor a Yankee. It is a heritage of the old South, an emblem of hospitality and a vehicle in which noble minds can travel together upon the flower-strewn paths of a happy and congenial thought.</p>
<p align="justify">
So far as the mere mechanics of the operation are concerned, the procedure, stripped of its ceremonial embellishments, can be described as follows:</p>
<p align="justify">Go to a spring where cool, crystal-clear water bubbles from under a bank of dew-washed ferns. In a consecrated vessel, dip up a little water at the source. Follow the stream through its banks of green moss and wildflowers until it broadens and trickles through beds of a mint growing in aromatic profusion and waving softly in the summer breeze.</p>
<p align="justify">Gather the sweetest and tenderest shoots and gently carry them home.</p>
<p align="justify">Go to the sideboard and select a decanter of Kentucky Bourbon, distilled by a master hand, mellowed with age yet still vigorous and inspiring. An ancestral sugar bowl, a row of silver goblets, some spoons and some ice and you are ready to start. In a canvas bag, pound twice as much ice 
as you think you will need. Make it fine as snow, keep it dry and do not allow to degenerate into slush.</p>
<p align="justify">In each goblet, put a slightly heaping teaspoonful of granulated sugar, barely cover this with spring water and slightly bruise one mint leaf into this, leaving the spoon in the goblet. Then pour elixir from decanter until the goblets are about one-fourth full. Fill the goblets with snowy ice,
sprinkling in a small amount of sugar as you fill. Wipe the outside of the goblets dry and embellish copiously with mint.</p>
<p align="justify">
Then comes the important and delicate operation of frosting. By proper manipulation of the spoon, the ingredients are circulated and blended until Nature, wishing to take a further hand and add another of its beautiful phenomena, encrusts the whole in a glistening coat of white frost.

Thus harmoniously blended by the deft touches of a skilled hand, you have a beverage eminently appropriate for honorable men and beautiful women.</p>
<p align="justify">When all is ready, assemble your guests on the porch or in the garden where the aroma of the juleps will rise Heavenward and make the birds sing. Propose a worthy toast, raise the goblet to your lips, bury your nose in the mint, inhale a deep breath of its fragrance and sip the nectar of
the gods.
<p align="justify">Being overcome by thirst, I can write no further.</p>
<p>
Sincerely,<br /><br /><br />
Lt. Gen. S.B. Buckner, Jr.<br />
V.M.I. Class of 1906<br /><br /> 
*Killed in Okinawa, 1945<br />
Promoted Posthumously to full General, July 1954</p>

     
        
</div>
<p class="cta-center">  <img src="/images/mint-juleps-served.jpg" alt="Amount of Mint Juleps Served at the Kentucky Derby"  class="img-responsive" style="display: block; margin-left: auto; margin-right: auto; width: 50%;" /></p><br>
            
            

<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Kentucky Derby</a></p>
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}