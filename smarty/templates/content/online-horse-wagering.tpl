{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/horsebetting.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Online Horse Wagering</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>You can bet from your home computer, mobile or tablet at BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>
<ul class="nav nav-tabs">				
<li class="active"><a href="#Win" data-toggle="tab"><span>WIN</span></a></li>
<li><a href="#Place" data-toggle="tab"><span>PLACE</span></a></li>
<li><a href="#Show" data-toggle="tab"><span>SHOW</span></a></li>
<li><a href="#Exacta" data-toggle="tab"><span>EXACTA</span></a></li>
<li><a href="#Trifecta" data-toggle="tab"><span>TRIFECTA</span></a></li>
<li><a href="#Superfecta" data-toggle="tab"><span>SUPERFECTA</span></a></li>
<li><a href="#Daily-Double" data-toggle="tab"><span>DAILY DOUBLE</span></a></li>
<li><a href="#Quinella" data-toggle="tab"><span>QUINELLA</span></a></li>
<li><a href="#Pick-3" data-toggle="tab"><span>PICK 3</span></a></li>
<li><a href="#Other-Wagers" data-toggle="tab"><span>OTHER WAGERS</span></a></li>
</ul>
			
<div class="tab-content">

<div class="tab-pane active" id="Win"><!-- Pane -->
<h2>Straight Bets</h2>
<p><strong>WIN</strong>
A "WIN" bet is just what it sounds like: betting that the horse you pick will win the race. You win if the horse wins.</p><p><strong>ACROSS THE BOARD</strong> <br /><br />
This is a quick way to say that I want to bet a horse to finish in any of the first three positions etc. ($2 Across The Board = $2 to win, $2 to place, and $2 to show). If the horse wins you collect all three bets. If the horse finishes 2nd then you collect the place and show bets. If the horse finishes 3rd then you will only collect the show bet. For<br /><br /><strong>A Group Try The SHOW PARLAY</strong> <br />
This is always fun to do when you are at the track with a group of friends. Each participant contributes to the pool of betting money (i.e. $5). You elect a captain to make the wager. Everyone participating gets to vote for their choice of the horse to show. You place the bet on the horse that gets the most votes. If the horse finishes in the top three positions you add the winnings to the pool of betting money and select another race to do the same thing.<br /><br />
Remember that it is not a good idea for this parlay bet or any other bet to attempt to play every race on the card. Some races are much harder to determine the winners. These races should be passed and save your money for better opportunities. As with most everything in life it is more fun to win than lose.<br /></p>
</div>
                    
                    
					
<div class="tab-pane" id="Place"><!-- Pane -->
	<h2><strong>Place</strong></h2>
<p><strong>PLACE</strong><br />
For a horse to "PLACE" it must finish the race either first or second. This bet generally pays less than the win bet but it gives you two chances to win instead of just one.</p><p><strong>ACROSS THE BOARD</strong> <br />
This is a quick way to say that I want to bet a horse to finish in any of the first three positions etc. ($2 Across The Board = $2 to win, $2 to place, and $2 to show). If the horse wins you collect all three bets. If the horse finishes 2nd then you collect the place and show bets. If the horse finishes 3rd then you will only collect the show bet. For<br /><br /><strong>A Group Try The SHOW PARLAY</strong> <br />
This is always fun to do when you are at the track with a group of friends. Each participant contributes to the pool of betting money (i.e. $5). You elect a captain to make the wager. Everyone participating gets to vote for their choice of the horse to show. You place the bet on the horse that gets the most votes. If the horse finishes in the top three positions you add the winnings to the pool of betting money and select another race to do the same thing.<br /><br />
Remember that it is not a good idea for this parlay bet or any other bet to attempt to play every race on the card. Some races are much harder to determine the winners. These races should be passed and save your money for better opportunities. As with most everything in life it is more fun to win than lose.</p>
					</div>
                    
					
                    <div class="tab-pane" id="Show"><!-- Pane -->
					
						<h2>Straight Bets</h2>
<p><strong>SHOW</strong><br />
For a horse to "SHOW" it must finish the race either first, second or third. This bet generally pays less than either the win or place bet, but it gives you three chances to get a return for your investment.<br /><br /><strong>ACROSS THE BOARD</strong> <br />
This is a quick way to say that I want to bet a horse to finish in any of the first three positions etc. ($2 Across The Board = $2 to win, $2 to place, and $2 to show). If the horse wins you collect all three bets. If the horse finishes 2nd then you collect the place and show bets. If the horse finishes 3rd then you will only collect the show bet. For<br /><br /><strong>A Group Try The SHOW PARLAY</strong> <br />
This is always fun to do when you are at the track with a group of friends. Each participant contributes to the pool of betting money (i.e. $5). You elect a captain to make the wager. Everyone participating gets to vote for their choice of the horse to show. You place the bet on the horse that gets the most votes. If the horse finishes in the top three positions you add the winnings to the pool of betting money and select another race to do the same thing.<br /><br />
Remember that it is not a good idea for this parlay bet or any other bet to attempt to play every race on the card. Some races are much harder to determine the winners. These races should be passed and save your money for better opportunities. As with most everything in life it is more fun to win than lose.</p>
					</div>
                    <!-- End Pane1 -->
                    
					
                    <div class="tab-pane" id="Exacta"><!-- Pane -->
					
						<h2>Exacta</h2>
<p><strong>Exacta</strong><br />
With this bet you must pick the first two finishing horses in the order of their finish. In other words, you must pick the horse that wins and the horse that finishes second.<br /><br /><strong>Exacta Box</strong><br />
A "box" on two or more horses in a race means taking all the possible combinations of those horses in each place of finish. If you have determined that two horses are the best in the race but you are not sure which one will win and which one will finish second, then the safest bet is to do the exacta box. You can box more than two horses, but it is very important to remember that with each additional horse you add to the box that the cost of the wager goes up substantially. The formula for calculating the cost of an exacta box is (horses in box x (horses in box minus 1) x dollar amount of bet). A $2 box of two horses would be (2 x 1 x 2) = $4. A $2 box of three horses would be (3 x 2 x 2) = $12. A $2 box of four horses would be (4 x 3 x 2) = $24. As you can see the cost of the exacta box increases greatly with each additional horse. Also remember that you can wager a $1 exacta box. This reduces the amount of your wager by 50% but also reduces your earnings by 50%.<br /><br /><strong>Exacta Wheels</strong><br />
If you are confident in a horse winning a race, but may have several choices as to the second place finisher, you may elect to do an exacta wheel. If for example, you think the #4 horse is going to win and either the #2, #5, #7, or #10 horse will finish second, you could place the following wager: ($2 exacta wheel the 4 WITH the 2, 5, 7, 10). Following the same formula for calculating the cost of the exacta wager as above we have (1 x 4 x 2) = $8. So it will cost you $8 to do an exacta wheel with one horse to win and any one of four horses to finish second. Another point to note is that some horses do not like to win. They allow other horses to pass them without making an extra effort to win the race. This can easily be determined by simply looking at their past performances. If for example, their racing record indicates that they have won one race and have finished second six times, you may want to place multiple horses in the win column and this "seconditis" horse in the place column. The previous wager might be ($2 Exacta Wheel the 2, 5, 7, 10 WITH the 4).<br /></a></p>
					</div>
                    <!-- End Pane1 -->
                    
					
                    <div class="tab-pane" id="Trifecta"><!-- Pane -->
					
						<h2>Trifecta</h2>
<p>With this bet you must pick the first three finishing horses in the order of their finish. In other words, you must pick the horse that wins, the horse that finishes second and the horse that finishes third.<br /><br />
The simplest form of this wager is the Straight Trifecta, which is a single combination of the first three finishers. The minimum bet for a Straight Trifecta is $2. To purchase, simply tell the clerk: "$2 Trifecta on 1-2-3." A Trifecta is not offered in all races at all tracks (refer to program).<br /><br />
NOTE: This wager is also called the "Triple" at some tracks.<br /><br /><b>Trifecta Box</b><br /><br />
The trifecta box has a $1 minimum bet per combination -- $6 minimum total cost. You can box three or more horses on a single ticket and wager $1 (or more) on each combination.<br /><br /><i>Example:</i> To purchase a $1 Trifecta Box using four horses, tell the clerk: "$1 Trifecta Box 11-2-3-4."<br /><br />
The following examples are for a $1 Trifecta Box:</p><p align="center"><br />
3 horse box ( 6 combinations) Cost $ 6<br />
4 horse box ( 24 combinations) Cost $ 24<br />
5 horse box ( 60 combinations) Cost $ 60<br />
6 horse box (120 combinations) Cost $120<br />
7 horse box (210 combinations) Cost $210</p><p><br /><b>Trifecta Key</b><br /><br />
The Key Wager requires the Key horse to finish FIRST with any combination of two or more horses finishing second and third. For example, if your key horse is #5 and your other horses are #'s 2, 4 and 6, you will win if #5 finishes first and two of your other three horses finish second and third.<br /><br /><i>Example:</i> To make the bet, tell the clerk: "$1 Trifecta Key #5 on top of 1, 2 and 3," and the ticket will cost $6. If four horses were coupled with the Key horse, the cost would have been $12, etc.</p><p><br /><b>Trifecta Full Wheel</b><br /><br />
You may select one or two horses to finish in a given position and combine your selection with all possible combinations. The number of combinations will vary according to the total number of horses in the race.<br /><br /><i>Example # 1:</i> If you pick #6 to finish first in a 12-horse field and want to have $1 per combination, tell the clerk: "$1 Trifecta 6-All-All." The total cost of the ticket is $110.<br /><br />
 </p><table class="bodyregular" cellspacing="10" cellpadding="0" width="80%" border="0"><tbody><tr><td width="214"><b>Number of horses in race:</b></td><td width="39"><b>8</b> </td><td width="32"><b>9</b></td><td width="29"><b>10</b></td><td width="39"><b>11</b></td><td width="35"><b>12</b></td></tr><tr><td width="214" height="2"><b>Cost in this Example:</b> </td><td width="39" height="2"><b>$42</b></td><td width="32" height="2"><b>$56</b></td><td width="29" height="2"><b>$72</b></td><td width="39" height="2"><b>$90</b></td><td width="35" height="2"><b>$110</b></td></tr></tbody></table><p><br /><b>Trifecta Part Wheel</b><br /><br />
The trifecta part wheel is offered at a $1 minimum bet per combination -- $2 minimum total cost. It is now possible to make a Trifecta wager coupling one or more horses to finish 1st, 2nd and 3rd.<br /><br /><i>Example:</i> You want #4 and #7 in the win position, #1, #9, and #11 in place position, and #2 and #5 in the show position to bet $1 on each combination, tell the clerk: "$1 Trifecta Part Wheel 4 and 7 with 1, 9 and 11 with 2 and 5." There are 12 possible combinations in the bet for a cost of $12. To win, the 4 or 7 must finish first, the 1, 9 or 11 must finish second and the 2 or 5 must finish third.</p>
					</div>
                    <!-- End Pane1 -->
                    
					
                    <div class="tab-pane" id="Superfecta"><!-- Pane -->
					
						<h2>Superfecta</h2>
                        
<p>With this bet you must pick the first four finishing horses in the order of their finish. In other words, you must pick the horse that wins, the horse that finishes second, the horse that finishes third and the horse that finishes fourth. Since it is very difficult to determine the top four finishing horses without playing a lot of combinations which can be very costly, the superfecta is not a very popular bet among seasoned handicappers. If by chance, the top four finishers are a standout and easily determined, then the return on the wager is generally very low and can easily be smaller than the amount of money required to hit the winning combination. Nevertheless, I will give you the details if you choose to be daring.<br /><br />
The simplest form of this wager is the Straight Superfecta, which is a single combination of the first four finishers. The minimum bet for a Straight Superfecta is $2. To purchase, simply tell the clerk: "$2 Superfecta on 1-2-3-4." A Superfecta is not offered in all races at all tracks (refer to program). </p><p><br /><b>Superfecta Box</b><br /><br />
The Superfecta Box has a $1 minimum bet per combination -- $24 minimum total cost. You can box four or more horses on a single ticket and wager $1 (or more) on each combination.<br /><br /><i>Example:</i> To purchase a $1 Superfecta Box using four horses, tell the clerk: "$1 Superfecta Box 11-2-3-4."<br /><br />
The following examples are for a $1 Superfecta Box:</p><p align="center"><br />
4 horse box ( 24 combinations) Cost $ 24<br />
5 horse box (120 combinations) Cost $120<br />
6 horse box (460 combinations) Cost $460<br />
7 horse box (840 combinations) Cost $840</p><p><br /><b>Superfecta Key</b><br /><br />
The Key Wager requires the key horse to finish FIRST with any combination of three or more horses finishing second, third and fourth. For example, if your key horse is #5 and your other horses are number's 2, 4, 6 and 8, you will win if #5 finishes FIRST and three of your other four horses finish second, third and fourth.<br /><br /><i>Example:</i> To make the bet, tell the clerk: "$1 Superfecta Key No. 5 on top of 2, 4, 6 and 8," and the ticket will cost $24. If five horses were coupled with the Key horse, the cost would have been $60, etc.</p><p><br /><b>Superfecta Full Wheel</b><br /><br />
You may select one or more horses to finish in any given position and combine these selections with all possible entries in the other positions. The number of combinations will vary according to the total number of horses in the race.<br /><br />
Example: If you pick horse #6 to finish FIRST in a 12 horse field and want to have $1 Full Wheel, tell the clerk: "$1 Superfecta 6-All-All-All." The total cost of the ticket is $990. This is another situation where even if you win, it is likely that you will still lose money.<br />
 </p><table class="bodyregular" cellspacing="10" cellpadding="0" width="414" border="0"><tbody><tr><td height="12">Number of horses in race:</td><td height="12">8</td><td height="12">9</td><td height="12">10</td><td height="12">11</td><td height="12">12</td></tr><tr><td>Cost in this Example:</td><td>$210</td><td>$336</td><td>$504</td><td>$720</td><td>$990</td></tr></tbody></table><p><br /><b>Superfecta Part Wheel</b><br /><br />
The Superfecta Part Wheel is offered at a $1 minimum bet per combination -- $2 minimum total cost. It is now possible to make a Superfecta wager coupling one or more horses to finish 1st, 2nd, 3rd and 4th.<br /><br /><i>Example:</i> You want the (#4 and #7 in the win position), the ( #1, #4, and #7 in place position), the (#1, #2, #4, #7 and #9 in the show position) and the (#1, #2 #4, #7, #9 and #11 in the fourth position). To bet a $1 on Superfecta on the above example, tell the clerk: "$1 Superfecta Part Wheel (4 7) with (1, 4, 7) with (1, 2, 7, 9) with (1, 2, 7, 9, 11)." There are 36 possible combinations in the bet for a cost of $36. To win, the (4 or 7) must finish FIRST, the (1, 4 or 7) must finish second, the (1, 2, 4, or 7) must finish third, and the (1, 2, 4, 9, or 11) must finish fourth.<br /><br />
As you can quickly see, the cost of the part wheel can be much less than the cost of the full wheel, but it requires you to be more selective and do a better job handicapping.</p>
					</div>
                    
                    
					
                    <div class="tab-pane" id="Daily-Double"><!-- Pane -->
					
						<h2>Daily Double</h2>
<p><strong>Daily Double</strong><br />
The daily double involves picking the winning horse in two consecutive races. All tracks offer the early daily double which involves races #1 and #2. Most tracks also offer a late daily double which is the last two races of the day. Some tracks also offer a rolling daily double which is on any two consecutive races. To win a daily double you must pick the winning horse in each of the two races. Combination bets can also be made on daily doubles.<br /><br /><strong>Double Wheel</strong><br />
When you are confident of a horse winning either of these two races but are uncertain of the outcome of the other race, especially if the horse in which you are confident has good to excellent odds, you might want to consider the daily double wheel. In this bet you bet your key horse with all the horses in the other race. Your key horse might be in either of the two races. Tell the clerk EX. ($2 Daily Double wheel the 5 with ALL) if you are confident in the #5 horse winning the first of the two races. But if your key horse is the #3 in the second of the two races tell the clerk i.e. ($2 Daily Double wheel ALL with 3). The formula for calculating the cost of the daily double wheel is simply the number of horses in the race other than the race containing your key horse, times the dollar value of your bet.<br /><br />
EXAMPLE: The race other than the race containing your key horse has 10 horses and you wish to make a $2 bet. (2 x 10 = $20)<br /><strong><br />
Daily Double Part-Wheel</strong><br />
The daily double part-wheel bet is a further refinement of the daily double wheel bet. This allows you to exercise your handicapping skills to eliminate all horses that you think will not win the race. This will reduce the size of your investment.<br /><br /><b>EXAMPLE</b>: As in the example in the daily double wheel above you have a key horse in one race and 10 horses in the other race. But you have decided that only five of the 10 horses have a chance of winning their race. So, by doing a daily double part wheel, you have one horse in combination with five horses and if you wish to bet a $2 bet your cost would be (2 x 5 = $10). You just saved yourself $10 over the full wheel bet and hopefully you did not throw out the winning horse. If for example, you decided that the #5 horse will win the first race of the double and that either horses #2, #4, #5, #8 or #9 will win the second race of the double you would tell the clerk i.e. ($2 Daily Double Part Wheel 5 with 2, 4, 5, 8, 9). NOTE: A $10 daily double part wheel (5 with 2,4,5,8,9) would cost (10 x 5 = $50).</p>
					</div>
                    
                    
					
                    <div class="tab-pane" id="Quinella"><!-- Pane -->
					
						<h2>Quinella</h2>
<p>In the Quinella, you must pick the horses that finish first and second. Either one can be first and the other one second. This bet functions exactly the same as the "Exacta Box". Although the Quinella has the same function as the "Exacta Box" the payoff can vary substantially. You must remember that there is no such wager of an exacta box, it is really two separate bets on one ticket. The Quinella wagers are entered into their own pool separate from the exacta pool. Not all tracks offer the Quinella wager, but to the best of my knowledge all tracks accept the term exacta box where ever the exacta wager is offered. If both the Quinella and Exacta wagers are offered it is wise to check the tote board before making a Quinella or Exact Box wager to see which one offers the greater payoff.</p>
					</div>
                    
                    
						
                    <div class="tab-pane" id="Pick-3"><!-- Pane -->
					
						<h2>PICK 3</h2>
<p>In order to win a Pick 3 bet you are required to select the winning horse in three consecutive races. Most tracks only offer one or two pick 3 opportunities per day. Some tracks offer a rolling pick 3 in which every race is part of a pick 3 bet, which means that some races are included in three pick 3 bets. The third race for example, would be the third race of the first pick 3, the second race of the second pick 3, and the first race of the third pick 3. I don't mean to make this seem complicated, it is really very simple. The simplest pick 3 bet is the $2 straight pick 3. If for example, you picked the #2 horse to win the first race, the #4 horse to win the second race, and the #6 horse to win the third race, you would tell the clerk ($2 pick 3 - 2, 4, 6).<br /><strong><br />
Pick 3 Part Wheel</strong><br />
The pick 3 part wheel bet is one of my favorite plays at the track. It allows you to single your favorite horses in some races and combining them with several good paying horses in other races in an attempt to hit a good paying win ticket. There is generally good value in the pick 3 bet, and frequently they will pay considerably more than the comparable amount of money bet in a 3 race parlay.<br /><br />
To increase the probability of winning a pick 3 you may want to play multiple combinations by selecting more than one horse in one or more of the three races. The easiest way to communicate this bet to the clerk is to use the pick 3 part wheel bet. If you think the #3 horse will win the first race of the pick 3 and either the (#1, #5 or #7) horse will win the second race, and the (#2, #5, #8, #10, or #12) horse will win the third race, you would tell the clerk ($2 Pick 3 Part Wheel 3 with 1, 5, 7 with 2, 5, 8, 10, 12). The cost of this bet would be (1 x 3 x 5 x $2) = $30 for your $2 bet. You can bet any amount from $1 on up. Remember, that if you bet a $1 bet, you only collect 1/2 of the payoff amount since most bets are quoted for a $2 bet.</p>
					</div>
                    
                    
					
                    <div class="tab-pane" id="Other-Wagers"><!-- Pane -->
					
						<h2>Other Horse Racing Betting Types</h2>
                        
<p>Some practical examples of betting and variations</p><p><b>Straight bet or Single or Win bet</b>: This is the simplest and most common bet. You bet on a winner at given odds. You collect only if your chosen horse is the first across the finish line.<br /><br /><b>Place</b>: A wager for place means you collect if your selected horse finishes either first or second.<br /><br /><b>Show</b>: The third horse across the finish line. A wager to show means you collect if your selected horse finishes either first, second or third.<br /><br /><b>Combination Bet</b>: Combinations cover from two to four horses to win in chosen order.<br /><br /><b>Pick 3</b>: This wager requires the player to pick the winners of three consecutive races. Some race tracks have a rolling pick 3 which is when the player must pick three races in a row and it continues for the next three races.<br /><br /><b>Pick 6</b>: This wager requires the player to select the winner of six consecutive races prior to the first race of the pick six. Some tracks place the pick six as the first six races, the middle six races, or the last six races. Many tracks have carry over pools for the pick six that can grow to as high as a million dollars.</p><p><b>The Daily Double</b>: You win if you pick the winner of the first and second race. To bet you say "$2 daily double on 3 (your pick to win in the first race) and 5 (your pick to win in the second race)". Your bet must be placed before the start of the first race.</p><p><b>Quiniella</b>: (Reverse Forecast, UK) You win if you pick two horses that finish first and second, in either order, in any single race. To bet you say "$2 quiniella, numbers 5 and 6". If the results of the first two horses are either 5-6 or 6-5, you win.</p><p><b>Quiniella Box</b>: Pick three or more horses. You win if any two of your selections finish first and second. To bet you say "$2 quiniela box on 4, 5 and 6". You are making three separate bets so your minimum bet is $6. If they finish 4-5, 4-6, 6-4, 6-5, 5-4 or 5-6 you win. You can box as few as three or as many as the field.</p><p><b>Quiniella Double</b>: Quiniella Double is a combination bet of two winning quiniellas in the last two races. Select two horses to win and place, in either order, in each of the last two races. You must win both quiniellas to win your Quiniella Double.</p><p><b>Quiniella Wheel</b>: You can also 'wheel' a favorite horse, so that it is combined with all the other horses. Based on the $2 value, the total cost of a wheel with say, 6 other horses is $12. For example; if you wheel #2 with 6 other horses in the same race and #2 comes in first or second, you win because you have all the 6 combinations covered.</p><p><b>Perfecta or Exacta</b>: (Straight Forecast, UK) The Perfecta is similar to the Quiniella, except the two horses must finish in the exact order. To bet you say "$3 Perfecta, 5-6". Only if the horses finish 5-6 you win.</p><p><b>Perfecta Box</b>: As with the quiniella box, except the two horses must finish in the exact order. To bet you say "$2 perfecta box on 4,5 and 6". You are making six separate bets so your minimum bet is $12. If they finish 4-5, 4-6, 6-4, 6-5, 5-4 or 5-6 you win. With Perfecta Box, for each extra horse you add to the box the possible combinations get compounded and your number of bets increases accordingly. A "$2 perfecta box on 4 horses" will cost you $24 (12 bets), a "$2 perfecta box on 5 horses" $40 (20 bets), and a "$2 perfecta box on 6 horses " $60 (30 bets).</p><p><b>Straight Trifecta</b>: (Tricast or Treble Forecast, UK) Pick the first three horses to cross the finish line in exact order. To bet you say "$2 trifecta numbers 7,4 and 5". Only if they finish 7-4-5 you win.</p><p><b>$1 Trifecta Box</b>: Pick three horses to finish first, second and third, in any order. To bet you say "$1 trifecta box 2,3 and 5". You are actually making six $1 bets, so your total bet is $6. If they finish 2-3-5, 2-5-3, 3-2-5, 3-5-2, 5-2-3 or 5-3-2; you win.</p><p><b>$1 Trifecta Key</b>: Pick your favorite horse to win, then two or more others to place and show in any order. To bet you say "$1 trifecta key on 1 with 2 and 3". You are actually making two $1 bets so your total bet is $2. To win your Key horse must win and the other two must finish either 1-2-3 or 1-3-2.</p><p><b>Superfecta</b>: The straight superfecta is played by picking the first four horses to finish in exact order. To bet you say "$2 straight superfecta 1-3-2-8". You would collect if the race finishes exactly 1-3-2-8.</p><p>You can Box, Key or Wheel most of the above bets.</p><p><b>Note: Different countries and different race tracks may have different rules, use different terms for how to bet and also for the types of bets. If unsure, ask the <a href="/how-to/place-a-bet">US Racing Customer Staff</a> when placing a bet.</b></p>
					</div>
                    
                    
                    
</div><!-- End Panes -->	
<!-- ======== DO NOT EDIT BELOW HERE ============================================== -->


{literal}<script type="text/javascript">
<!--//--><![CDATA[//><!--
$('.nav-tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

//--><!]]>
</script>{/literal}        
        

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    