{literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "SportsEvent",
  "name": "Dubai World Cup 2019",
  "startDate": "2019-03-30T15:30",
  "endDate": "2019-03-30T16:00",
  "location": {
    "@type": "EventVenue",
    "name": "Mayden Racecourse, Dubai",
    "telephone": "000-000-1234",
    "openingHours": "Mo,Tu,We,Th,Fr 09:00-17:00",
	"address": "Al Meydan Road, Nad Al Sheba - Dubai - United Arab Emirates",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "40.75",
    "longitude": "73.98"
    }
  	}, 
    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "description": "US Racing presents this week's horse racing schedule for US, Canadian and international race tracks."
  }
</script>
{/literal}

<link rel="stylesheet" href="/assets/css/tooltip.css"/>
<script src="/assets/js/boxover.js"></script> 
<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
{*  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
	
<div class="headline"><h1>Horse Racing Schedule Setting</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

{* <div class="tag-box tag-box-v4"> *}
<p>Please choose the date and upload content to content box </p>
<p>go to url : https://www.equibase.com/liveday.cfm?trk_date=[[date]] where [[date]] is date you want to download format yyyy-mm-dd </p>
<p> open development tool</p>
<p> select Element tab </p>
<p> right click on html tab select edit as html</p>
<p> copy that html content</p>
<p> paste it to content box in this page</p>
<div >
Data Date : <input type="date" name="ddate" id="horse-racing-schedule-date">
Data Content: <input type="text" name="content" id="horse-racing-schedule-content">
<button onclick="submitraceData(this);">Submit</button>
</div>


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
{literal}
<script type="text/javascript">
function submitraceData(element){
  var btnElement = $(element);
  btnElement.prop("disabled", true);
  var __date = $("#horse-racing-schedule-date").val();
  var content = $("#horse-racing-schedule-content").val();
  var arr = content.split("</section>");
  var url="horse-racing-schedules-setting.php";
  var method="POST";
  var inputData={'content':arr['3'],'date':__date};
  $.ajax({
    url : url,
    method : method,
    timeout: 200000,
    data : inputData, 
    dataType : 'text',
    xhrFields: { 
        withCredentials: true 
    },
    success : function(data) {
      alert("Done");
      btnElement.prop("disabled", false);
      $('#horse-racing-schedule-content').attr("value", "");
    }
  });
  //console.log(arr['3']);
}
  </script>
  {/literal}
{*
{literal}
<script type="text/javascript">
var dom = $(".sRace");                    
				dom.find('.race:contains("Churchill Downs")').parent().remove();		
				dom.find('.race:contains("Fair Grounds")').parent().remove();	
				dom.find('.race:contains("Calder")').parent().remove();
				dom.find('.race:contains("Hoosier")').parent().remove();	
				dom.find('.race:contains("Indiana Downs")').parent().remove();	
				dom.find('.race:contains("Miami Valley")').parent().remove();	
				dom.find('.race:contains("Oaklawn Park")').parent().remove();
				dom.find('.race:contains("The Meadows")').parent().remove();
				dom.find('.race:contains("Arlington Park")').parent().remove();
				dom.find('.race:contains("Finger Lakes")').parent().remove();
				dom.find('.race:contains("Canterbury Park")').parent().remove();
</script>
{/literal}
*}
