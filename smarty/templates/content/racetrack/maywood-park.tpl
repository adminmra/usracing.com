{assign  var="racetrackname" value="Maywood Park"} 
{assign  var="racetrackpath" value="maywood-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                   
                    <p>Maywood Park is located in Melrose Park, just ten miles west of downtown Chicago and conveniently close to O&rsquo;Hare, one of the nation&rsquo;s busiest airports.  Melrose Park is a village in Cook County, Illinois; as of the 2010 census it had a population of 25,411.</p>
                    <p>The Maywood race track was opened in 1946 on the original site of the Cook County Fairgrounds. Maywood Park is the only racetrack in the Chicago area devoted exclusively to standardbred horse racing.</p>
                    <p>Located along busy North Avenue between First and Fifth Avenues, less than 10 miles west of Chicago's bustling downtown area and only minutes away from O'Hare airport, Maywood Park can trace its harness racing roots back to a time when the racetrack was surrounded only by forest preserves and farm land.</p>
                  <h2>Maywood Park Racetrack Schedule</strong></h2>
                    <p> <strong>Live Racing: </strong>January - December<br>
                       <strong>Live Racing Calendar: </strong>Thursday and Friday<br>
                       <strong>Race type: </strong>Standardbred<br>
                       <strong>Notable Races:</strong> Windy City Pace, Abe Lincoln, Maywood Filly Pace, Galt, Cinderella<br>
                       <strong>Main track:</strong> 1/2 mile, oval.<br>
                       <strong>Distance from last turn to finish line:</strong>&nbsp; 594 feet
                                                </p>
                 
                    <h2>Stakes Races at Maywood Park</h2>
                    <blockquote>
                      <p>Windy City<br />
                        Cinderella<br />
                        Filly Maywood Pace<br />
                        Maywood Pace<br />
                        Galt Trot<br />
                        Abe Lincoln</p>
                    </blockquote>
                   <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
