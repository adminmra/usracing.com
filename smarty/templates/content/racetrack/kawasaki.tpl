{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/kawasaki.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Kawasaki Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-track-primary.tpl'} </p>
        {*end*}
        <p>Get your Kawasaki Horse Betting action Today! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
        {*Second Copy of the page*}

		   {*Secondary Copy*}
		  <h2>Kawasaki Racecourse Major Races</h2>


		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Kawasaki Racecourse Major Races"
        summary="Major races for Kawasaki Racecourse. Only available at BUSR.">
        <caption>Kawasaki Racecourse Notable Races</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Kawasaki Kinen </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2100m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
            <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Zen-Nippon Nisai Yushun </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo</td>
            </tr>
			  <tr>
                <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;">Empress Hai (Kiyofuji Kinen) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2100m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + f</td>
            </tr>
				  <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Kanto Oaks </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2100m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Sparking Lady Cup (Hokuto Vega Memorial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f</td>
             </tr>
        </tbody>
    </table>
            &nbsp;
        {*end*}
        <br>
        </li>
        </ul>

		    <h2>Kawasaki Off Track Betting</h2>
		  <p>Built in 1906, this race course is located in Kawasaki, Northern Honshu, Japan, and it includes both dirt and turf courses. Kawasaki Racecourse features three race meetings per year, with the biggest events being the Kawasaki Himba Stakes (G3), Tanabata Sho (G3), the Radio Nikkei Sho (G3), and the Kawasaki Kinen (G3).</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>

        {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}