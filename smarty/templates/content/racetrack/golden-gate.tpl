{assign  var="racetrackname" value="Golden Gate Fields"} 
{assign  var="racetrackpath" value="golden-gate-fields"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                    {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
            
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p>Located along the San Francisco Bay in Berkeley, <strong>Golden Gate Fields</strong> is Northern California&rsquo;s premier horse racing destination. It has been offering thoroughbred horse racing to the Bay Area since 1941 and with the closing of the Bay Meadows racetrack in 2008, it became the only major racetrack in Northern California.<p>
                    </p>
                      Winter/Spring Meet at Golden Gate Fields runs from December to June. Golden Gate Fields is home of several graded stakes and black type listed stakes, including the Grade 3 El Camino Real Derby, Grade 3 San Francisco Mile Stakes, Berkeley Handicap and Silky Sullivan Handicap.</p>
                     <h2>Golden Gate Fields Information</h2>
                    <strong> Live Racing:&nbsp;</strong> Thursday to Sunday.<br>
                    <strong>Racing Calendar:</strong>&nbsp; January to June and August to December<strong><br>
                     <strong>Course type:&nbsp;</strong> Thoroughbred flat racing<br>
                    <strong>Notable races:</strong> San Francisco Mile Stakes, Golden Gate Derby, El Camino Real derby, Berkeley Handicap, All American Stakes<br>
                    <strong>Main track:</strong> 1 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong> 1000 feet</p>
             
                    <h2>Stakes Races at <strong>Golden Gate Fields</strong></h2>
                    <blockquote>
                      <p>California Derby<br />
                        California Oaks<br />
                        El Camino Real Derby - Grade III<br />
                        Albany Stakes<br />
                        Camilla Urso<br />
                        Golden Poppy<br />
                        San Francisco Mile - Grade III<br />
                        All American - Grade III<br />
                        Campanile<br />
                        Silky Sullivan<br />
                        Lost in the Fog</p>
                    </blockquote>
  
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
