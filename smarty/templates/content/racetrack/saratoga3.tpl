{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">


<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/tracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Saratoga Racecourse </h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div style="margin-top: 20px; width: 302px; height: 250px; float: right;">
<div><img style="border: 1px solid #1B579F; margin: 0px;" src="/themes/images/racetracks/saratoga_entrance.jpg" alt="Saratoga Racecourse" width="300" height="198" /></div>
<div><a href="/signup/"><img style="margin: 10px auto; width: 150px;" title="Want to bet on Horses Online? Join Today!" src="/themes/images/signup_button.gif" border="0" alt="Want to bet on Horses Online? Join Today!" /></a></div>
</div>
<p>&nbsp;</p>
<h2 class="DarkBlue" style="margin-bottom: 10px;"><strong>Saratoga</strong></h2></div>
<p>Union Avenue<br />Saratoga Springs, New York 12866<br />(718) 641-4700<br /><a title="http://www.nyra.com/saratoga" href="http://www.nyra.com/saratoga" target="_self">http://www.nyra.com/saratoga</a></p>
<p><strong>Saratoga Race Course</strong> is a famous horseracing track located in Saratoga Springs, NY. It opened on August 3, 1863, and is the oldest organized sporting venue of any kind in the United States.<strong> </strong></p>
<p>&nbsp;</p>
<h4><strong>Saratoga 2010 Race Course Meet: </strong></h4>
<p><strong>40 days, July 23-September 6 <br /> Dark Tuesdays<br /> Post times: </strong>1 p.m. daily except:<br />2:30 p.m. on Friday, July 30 and Friday September, 3<br /> 11:35 a.m. on Travers Day, Saturday, August 28</p>
<p> 2010 is the 142nd season at Saratoga Race Course and, to celebrate, the New York Racing Association (NYRA) will offer free admission to the grandstand on Opening Day. &nbsp;NYRA President and CEO Charles Hayward, said the free admission on Friday, July 23 is being offered, “As a thank you to our terrific customers. This will be a great way to start the Saratoga meet, which promises to be one of the best yet.”</p>
<p> As the gates open at Saratoga Race Course for the 142nd time, 2010’s Opening Weekend promises to be an exciting one with the Grade III, $100,000 Schuylerville for 2-year-old fillies on Friday, the Grade I, $250,000 American Oaks for 3-year-old fillies on Saturday, the Grade II, $150,000 Sanford for 2-year-olds on Sunday and the $75,000 Evan Shipman for New York-breeds on Monday.</p>
<p> <strong style="font-size: 12px;">&nbsp;</strong></p>
<h4><strong>Saratoga Racetrack information:</strong></h4>
<p> <strong>Address:</strong> Saratoga Race Course, 267 Union Avenue, Saratoga Springs, NY, 12866<br /> <strong>Main Course:</strong> 1 1/8 Mile <br /> <strong>Turf Course:</strong> 1 Mile <br /> <strong>Steeplechase/Inner Turf Course:</strong> 7/8 Mile <br /> <strong>Attendance Capacity:</strong> 50,000 <br /> <strong>Trackside Dining:</strong> 2,200 <br /> <strong>Total Seating Capacity:</strong> 18,000, including picnic tables and benches.</p>
<p>&nbsp;</p>
              
            
            
            

      <h2 class="title">Saratoga Schedule {'Y'|date} </h2>
  



        

        <table id="infoEntries">
	
	<tbody>
		<tr>
		<th>Date</th>
		<th></th>
		<th>Grade</th>
		<th>Event</th>
	</tr>
	<tr>		<td>23-Jul</td>		
		<td>Opening Day</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>23-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=2&race=The Schuylerville">The Schuylerville</a></td>
		<td>GIII</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>23 to 25-Jul</td>		
		<td>Hats Off to Saratoga Festival</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>24-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=4&race=The Coaching Club American Oaks">The Coaching Club American Oaks</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>25-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=5&race=The Sanford">The Sanford</a></td>
		<td>GII</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>26-Jul</td>		
		<td>The Evan Shipman (NYB)</td>
		<td></td>
		<td>ungraded</td>
	</tr>
	<tr>		<td>29-Jul</td>		
		<td>Mid-summer St. Patrick's Day</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>29-Jul</td>		
		<td>The Quick Call</td>
		<td></td>
		<td></td>
	</tr>
	<tr>		<td>30-Jul</td>		
		<td>Party at the Spa (2:30 post time)</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>30-Jul</td>		
		<td>College Day - win a $1000 scholarship!</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>31-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=11&race=The Diana ">The Diana </a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>31-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=12&race=The Lake George">The Lake George</a></td>
		<td>GII</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>31-Jul</td>		
		<td><a href="/saratoga/pastwinners?id=13&race=The Jim Dandy">The Jim Dandy</a></td>
		<td>GII</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>31 Jul to 1 Aug</td>		
		<td>Fasig-Tipton Festival of Racing </td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>6-Aug</td>		
		<td>Galloping Grapes</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>13-Aug</td>		
		<td>Halloween in the Summer</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>20-Aug</td>		
		<td>Fabulous Fillies Day</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>25-Aug</td>		
		<td>Ice Cream Eating Contest</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>20 to 21-Aug</td>		
		<td>Travers Festival</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>28-Aug</td>		
		<td><a href="/saratoga/pastwinners?id=20&race=Travers Stakes">Travers Stakes</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>28-Aug</td>		
		<td><a href="/saratoga/pastwinners?id=34&race=The King's Bishop">The King's Bishop</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>28-Aug</td>		
		<td><a href="/saratoga/pastwinners?id=21&race=The Ballerina">The Ballerina</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>29-Aug</td>		
		<td><a href="/saratoga/pastwinners?id=22&race=The Personal Ensign">The Personal Ensign</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>2-Sep</td>		
		<td>Proud to be an American Day</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>3-Sep</td>		
		<td>Party at the Park</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>4-Sep</td>		
		<td><a href="/saratoga/pastwinners?id=25&race=The Forego Handicap">The Forego Handicap</a></td>
		<td>GI</td>
		<td>Event</td>
	</tr>
	<tr>		<td>4-Sep</td>		
		<td><a href="/saratoga/pastwinners?id=26&race=The Woodward">The Woodward</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>5-Sep</td>		
		<td><a href="/saratoga/pastwinners?id=27&race=The Spinaway">The Spinaway</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>5-Sep</td>		
		<td><a href="/saratoga/pastwinners?id=28&race=Three Chimneys Hopeful Stakes">Three Chimneys Hopeful Stakes</a></td>
		<td>GI</td>
		<td>Stakes Race</td>
	</tr>
	<tr class="odd">		<td>6-Sep</td>		
		<td><a href="/saratoga/pastwinners?id=29&race=The Glen Falls Handicap">The Glen Falls Handicap</a></td>
		<td></td>
		<td>Stakes Race</td>
	</tr>
	<tr>		<td>4 to 6-Sep</td>		
		<td>Final Stretch Weekend</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr class="odd">		<td>5 to 6 Sep</td>		
		<td>Family Fun Fest</td>
		<td></td>
		<td>Event</td>
	</tr>
	<tr>		<td>6-Sep</td>		
		<td>Closing Day - Labour Day BBQ</td>
		<td></td>
		<td>Event</td>
	</tr>
		
	</tbody>
	</table>  

  

</div><!-- /block-inner, /block -->

  
                                      
          
<div class="headline"><h1>Saratoga marks 150 years of thoroughbred racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="news-date">By CHRIS CAROLA (Associated Press) | The Associated Press – Fri, May 24, 2013 9:27 AM EDT</div> 
                    
                    
                    <p>SARATOGA SPRINGS, N.Y. (AP) -- Mineral springs, gambling and horses.</p>
<p >In this world-famous thoroughbred racing hotbed, the four-legged stars who have drawn people here since the bloodiest days of the Civil War actually took third billing coming out of the 19th-century tourism starting gate, trailing the resort spas and the casinos that offered visitors a diversion in between the taking of the waters.</p>

<p>''The waters brought them first, the casinos second and the horse racing third,'' said Allan Carter, historian at the National Museum of Racing and Hall of Fame in Saratoga Springs, located across Union Avenue from historic Saratoga Race Course.</p>

<p>It may have started out trailing the field, but ''the Sport of Kings'' soon took the lead and still reigns supreme in Saratoga 150 years after the first thoroughbred races were held Aug. 3, 1863. That was just a month after the three-day Battle of Gettysburg ended. A year later, the war was still raging when the second thoroughbred season was held at the newly built race course, located across the road from the site of the first races.</p>

<p>Over the next century and a half, Saratoga's racing season became intertwined with America's sporting and cultural fabric, attracting generations of robber barons and blue bloods, gangsters and celebrities, professional gamblers and $2 bettors. It's America's most successful racetrack and its oldest sports venue, a summertime destination that draws visitors from around the world who catch the daily racing card in between soaking up the city's Victorian charms as well as its upscale 21st century amenities - including a vibrant nightlife, upscale shops and trendy restaurants.</p>

<p>''From New York City you drive north for about 175 miles, turn left on Union Avenue and go back 100 years,'' the late sports columnist Red Smith famously wrote when offering directions to his favorite racetrack.</p>

<p>Over the next few months, Saratoga is celebrating the 150th anniversary of the start of thoroughbred racing with a series of events, including concerts, exhibits and festivals centered on the racing season, which begins July 19 and runs through Labor Day, Sept. 2.</p>
<p>Americans first began trekking to Saratoga in the decades before the Civil War, when the many mineral springs dotting the area drew visitors looking for relief from various ailments. Southern plantation owners brought their families north to escape the southern heat, while wealthy New Yorkers journeyed upstate to enjoy the country air.</p>
<p>A thriving hotel business sprang up along Broadway, Saratoga's main drag, as the summer crowds at ''the Spa'' grew through the 1840s and '50s, arriving aboard trains that deposited them just a short stroll or carriage ride from their accommodations.</p>

<p>Entrepreneurs soon began offering other diversions for the men bored with the daily rounds of mineral water tastings, cotillions and concerts. Gambling joints sprang up, including casinos owned by John Morrissey, an Irish-born Tammany Hall enforcer-turned-prizefighter-turned politician. But the gambling dens didn't open until the evening, leaving the afternoon wide open for men with money to wager.</p>

<p>Morrissey filled that void. With the backing of several wealthy businessmen, he held the first thoroughbred races in Saratoga on a track located on Saratoga's eastern outskirts. The next summer, they moved the races across Union Avenue to the newly built track, where the races have been held ever since.</p>

<p>Saratoga has hosted many of racing's greatest equine stars, including Man o' War and Secretariat. Many have won here, and nearly as many - included the aforementioned legends - have lost here, earning the track the nickname ''graveyard of favorites.''</p>

<p>The highlight of Saratoga's 40-day meet (the track is closed on Tuesdays), is the Travers Stakes, being run for the 144th time on Aug. 24. Older than the Kentucky Derby by 11 years, the Travers typically attracts some of racing's top horses. Saratoga hosts other storied races, such as the Alabama and the Whitney, and it's known as the place where the sport's future stars first make their mark racing as 2-year-olds.</p>

<p>The racetrack grounds, with their wooden, open-air grandstand and clubhouse dating back to the 1890s and late 1920s, respectively, give the place its old-time feel, even with such modern additions as New York City-based Shake Shack among the varied concession offerings in the tree-shaded paddock area known as Saratoga's ''backyard.'' Picnic tables are available and trackgoers are allowed to cart in their own food and beverages into this popular section.</p>

<p >Outside the track, Saratoga Springs, a city of 28,000, is known for its Victorian architecture, with examples of Second Empire, Queen Anne and other styles from the 19th century and early 20th century along Broadway and surrounding tree-lined neighborhoods.</p>
<p >Among the most notable buildings is the casino Morrissey built in 1870 in what is now downtown's Congress Park. Later renamed Canfield Casino for the professional gambler who purchased the property in the 1890s, the three-story brick Italianate hasn't been operated as a casino since an anti-gambling crusade shut it down in 1907.</p>

<p>But the city is home to one of the state's nine ''racinos,'' Saratoga Casino and Raceway, located just down the street from the ''flat track.'' The racino has more than 1,700 electronic slot machines along with electronic roulette and craps, with harness racing most evenings and weekends.</p>
If Saratoga is a haven for horses, it's also a city of museums. The city-owned Canfield Casino, a venue for galas, weddings and other events, is also home to the Saratoga Springs History Museum. Exhibits tell Saratoga's story from its days as a frontier settlement and European-style resort spa to a horse racing mecca and gambling playground that drew mobsters like Meyer Lansky and Dutch Schultz along with upper-crust Whitneys and Vanderbilts.</p>
<p>]
In addition to racing and history museums, there's the National Museum of Dance, New York State Military Museum, Children's Museum, Saratoga Automobile Museum, and Tang Teaching Museum on the Skidmore College campus, among others.</p>

<p>Saratoga made significant strides in marketing itself into a year-round destination to avoid the boom-and-bust swings businesses typically experienced during the decades-long period when the racetrack was only open for four weeks in August. Those efforts, combined with the track's continued popularity, have spurred a hotel construction boom in recent years, with more on the way. Still, the biggest demand for lodging occurs in the summer, especially during racing season, when rates can double, another Saratoga tradition.</p>
<p >At Saratoga Arms, a 31-room concierge-style hotel on Broadway, and the Gideon Putnam, a 75-year-old resort in Saratoga Spa State Park, rates for the best rooms top $600 a night during August. Other lodging options range from popular chains such as Holiday Inn and Marriott to charming bed-and-breakfast accommodations and locally owned motels.</p>

<p >Dining options include Thai, Italian, Irish pub fare, bakeries, tapas and steakhouses. The downtown district that includes Broadway and Caroline, Phila and Henry streets is the center of Saratoga's nightlife, with bars, jazz clubs and nightclubs interspersed with shops, from local clothing boutiques to such national retailers as Banana Republic and Chico's.</p>
                    
                    <p>&nbsp;</p>
        
        

  </div>

  
          
        
       <!-- end: #col3 -->


      
    </div></div>