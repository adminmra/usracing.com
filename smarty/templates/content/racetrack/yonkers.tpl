{assign  var="racetrackname" value="Yonkers Raceway"} 
{assign  var="racetrackpath" value="yonkers"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Yonkers Raceway </strong>located at the corner of Central Park Ave and Yonkers near the border of New York City, opened in 1899. It is a one-half-mile standard bred harness racing dirt track and one of the few that offer harness racing all year.
                    </p>
                    <p>Originally called the Empire City Race Track, Yonkers is now the home of three major harness events: the Yonkers Trot, Art Rooney Pace and Messenger Stakes. The later two are legs of the Trotting and Pacing Triple Crowns. In 2006, when Yonkers Raceway hosted  both the Yonkers Trot and the Messenger Stakes, it became the first harness track in America to host two Triple Crown races on the same day.</p>
                   
<h2>Yonkers Racetrack Schedule</h2>
                    <p><strong>Live Racing: </strong>Monday, Tuesday, Thursday, Friday and Saturday with first post at 7:10 PM ET on all nights<br>
                    <strong>Live Racing Calendar:</strong> January to December<br>
                    <strong>Course type:</strong> Standardbred harness racing dirt track<br>
                    <strong>Notable Races:</strong> Yonkers Trot, Art Rooney Pace and Messenger Stake<br>
                    <strong>Main track: </strong>&nbsp;1/2 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong> 660 feet<br>
                    <strong>Turf course:</strong></strong> 6 furlongs</p>
                    <h2>Stakes Races at Yonkers</h2>
                    <blockquote>
                      <p>Petticoat Pacing Series <br />
                        ADA Trotting Series<br />
                        Sagamore Hill Pacing Series<br />
                        Blue Chip Matchmaker Series <br />
                        Art Rooney Pace<br />
                        George Morton Levy Pacing Series<br />
                        Lismore Pace <br />
                        Hugh Grant Pace <br />
                        Clyde Hirt Pace<br />
                        Dick McGuire Trot<br />
                        Armand Palatucci Pace<br />
                        Mike Cipriani Trot<br />
                        Yonkers Trot </p>
                    </blockquote>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
