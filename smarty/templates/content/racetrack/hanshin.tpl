{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/hanshin.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
    <style>
        @media (min-width: 1024px) {
            .usrHero {
                background-image: url({/literal} {$hero_lg} {literal});
            }
        }

        @media (min-width: 481px) and (max-width: 1023px) {
            .usrHero {
                background-image: url({/literal} {$hero_md} {literal});
            }
        }

        @media (min-width: 320px) and (max-width: 480px) {
            .usrHero {
                background-image: url({/literal} {$hero_sm} {literal});
            }
        }

        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }

        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }

        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/hanshin/hanshin-racetrack-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }

        .kd {
            background: none;
        }
    </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h2 class="kd_subheading">Best Hanshin Off Track Betting Deals & Promotions</h2>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-track-primary.tpl'} </p>
        {*end*}
        <p>Get your Hanshin Horse Betting action with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
        {*Second Copy of the page*}            
          
		   {*Secondary Copy*} 
		  <h2>Hanshin Racecourse Major Races</h2>

		  
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Hanshin Racecourse Major Races"
        summary="Major races for Hanshin Racecourse. Only available at BUSR.">
        <caption>Hanshin Racecourse Major races - Grade I</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Osaka Hai </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Oka Sho (Japanese 1,000 Guineas) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
			  <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Takarazuka Kinen </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2200m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Hanshin Juvenile Fillies </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m out </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Asahi Hai Futurity Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m out </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Hanshin Racecourse Major Races"
        summary="Major races for Hanshin Racecourse. Only available at BUSR.">
			<caption>Hanshin Racecourse Major races - Grade II</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fillies' Revue (Oka Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1400m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
            <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Hanshin Daishoten (Tenno Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 3000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
			  <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> 	Hanshin Himba Stakes (Victoria Mile Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1400m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;">4yo + f</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Centaur Stakes (Sprinters Stakes Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1200m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Rose Stakes (Shuka Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m out </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Kobe Shimbun Hai (Kikuka Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2400m out </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo c/f</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Hanshin Cup </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1400m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
        </tbody>
    </table>
		  <br>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Hanshin Racecourse Major Races"
        summary="Major races for Hanshin Racecourse. Only available at BUSR.">
			<caption>Hanshin Racecourse Major races - Grade III</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Feb/Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Arlington Cup </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m out</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
            <tr>
                <td data-title="Month">Feb/Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> 	Hankyu Hai (Takamatsunomiya Kinen Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1400m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
			  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;">Tulip Sho (Oka Sho Trial)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m out</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;">3yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Mainichi Hai </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m out</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo</td>
            </tr>
				  <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Antares Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1800m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Naruo Kinen </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;">Mermaid Stakes (Handicap)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep/Oct</td>
                <td data-title="Race" style="word-break: keep-all !important;">Sirius Stakes (Handicap)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;">Challenge Cup</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turd 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
        </tbody>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p> 
		  
		    <h2>Hanshin Off Track Betting</h2>
		  <p>Built in 1949, Hanshin Racecourse is located in Takarazuka, Hyogo, Western Japan, and it has a capacity of 139,000. Hanshin features two turf courses, a dirt one, and a jump course. And, this racetrack hosted the Japan Cup Dirt from 2008 until 2015, when this event moved to Chukyo with a new name, the Champions Cup.</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>   
		  
        {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}