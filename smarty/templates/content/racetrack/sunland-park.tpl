{assign  var="racetrackname" value="Sunland Park"} 
{assign  var="racetrackpath" value="sunland-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

   <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p><strong>Sunland Park Racetrack &amp; Casino</strong> opened in 1959. Its located in Sunland Park, New Mexico, a suburb of El Paso, Texas.</p><p>The race track features both thoroughbreds and quarter horses. The signature event at Sunland of the same name is held in March each year and is a prep race for the <a href="/kentucky-derby">Kentucky Derby</a>.</p>                      
                      
           <h2>Sunland Park Track Schedule</h2>
                    <p>
                    <strong>Live Racing:</strong> Tuesday, Friday through Sunday<br />
                    <strong>Live Racing Calendar:</strong> December through April<br>
                    <strong>Course Type:</strong></strong> Thoroughbred, quarter horse<br />
                    <strong>Notable Races</strong>: Sunland Derby, WinStar Derby, Borderland Derby,&nbsp; New Mexican Spring Futurity<br />
                    <strong>Main Track:</strong> One mile dirt course<br />
                     <strong>Distance from last turn to finish line:</strong> 990 Feet<br />
                    <strong>Turf Course:</strong> 6 &frac12; Furlongs </p>
                    
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
