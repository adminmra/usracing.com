{assign  var="racetrackname" value="Suffolk Downs"} 
{assign  var="racetrackpath" value="suffolk-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

<p><strong>Suffolk Downs</strong> is a thoroughbred race track in East Boston, Massachusetts. It was founded in 1935 and  is New England&rsquo;s only remaining active Thoroughbred racetrack. Famous horses that have raced at this track include Seabiscuit, Whirlaway and Cigar and it has been the site of performances by world renowned entertainers like the Beatles and Aerosmith.
                        </p>
                    <p>Suffolk Downs is owned  by Caesars Entertainment Corporation. On September 16, 2013, they announced that it is seeking to become the World's second Casino to achieve LEED Gold Certification by embracing the site's natural ecology which will also provide a dramatic arrival and visitor experience<br />
               
                  
 <h2>Suffolk Downs Race Course Schedule</h2>
                    <p>
                    <strong>Live Racing:</strong> Saturday to Wednesday<br />
                     <strong>Live Racing Calendar:</strong> June through October<br>
                     <strong>Course Type:</strong> Flat<br />
                     <strong>Notable Races</strong>: Massachusetts Handicap G3, James B. Moseley Sprint Handicap, Drumtop Stakes, Robert M. O&rsquo;Malley Memorial, Isadorable Stakes, My Fair Lady Stakes, Old Ironsides Stakes, Waquoit Stakes<br />
                    <strong>Main Track:</strong> One mile, Oval<br />
				   <strong>Distance from last turn to finish line:</strong> 1,030 feet<br />
                   <strong>Turf Course:</strong> 7 Furlongs </p>
                  
                    <h2>Stakes Races at Suffolk Downs</h2>
                    <blockquote>
                      <p>Rise Jim Stakes<br />
                        Isadorable Stakes<br />
                        Last Dance Stakes<br />
                        First Episode Stakes<br />
                        Louise Kimball Stakes<br />
                        African Prince Stakes<br />
                        John Kirby Stakes<br />
                        Norman Hall Stakes</p>
                    </blockquote>

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
