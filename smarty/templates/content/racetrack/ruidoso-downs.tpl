{assign  var="racetrackname" value="Ruidoso Downs"} 
{assign  var="racetrackpath" value="ruidoso-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Situated in southern New Mexico, Ruidoso Downs is a growing city with all the ambiance of a small town. The <strong>Ruidoso Downs Race Track</strong>, Billy the Kid Casino and the Hubbard Museum of the American West are located in the city.</p>
                    <p>                      In 1947, the first pari-mutuel races were held at what was then called Hollywood Park. The name of the track was changed to Ruidoso Park in 1953.</p>
                    <p>Since 1959, Ruidoso Downs Race Track has hosted the All American Futurity - The World's Richest Quarter Horse Race, run over 440 yards every Labor Day. The All American Futurity has the largest purse of any race for 2-year-old quarter horses in North America with a $2.6 million purse.</p>
        
                    <h2>Ruidoso Downs Track Schedule</h2>
                    <p><strong>Live Racing Dates:</strong> May to September<br>
                    <strong>Live Racing Schedule:</strong> Thursday to Monday</p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
