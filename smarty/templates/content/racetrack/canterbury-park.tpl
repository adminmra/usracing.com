{assign  var="racetrackname" value="Canterbury Park"} 
{assign  var="racetrackpath" value="canturbury-park"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address: </strong>1100 Canterbury Rd, Shakopee, MN 55379, United States<br />
                    </p>
                    <p>&nbsp;</p>
                    <p>Built in 1984, Canterbury Park is a horse racing track located in Shakopee, Minnesota, offering live horse racing in the Twin Cities.</p>
                    <p>Live horse racing is held from early May to Labor Day, Thursday through Sunday. From time to time, racing dates will be added on holidays throughout the meet. Canterbury Park has hosted the Claiming Crown of horse racing for all but four years since its inception in 1999. The inaugural Mystic Lake Derby, offering the largest purse at the track since 1991, was run on July 28, 2012. <br />
                    </p>
               
                    <h2><strong>Canterbury Park Racetrack  Information</strong><br />
                        <br />
                    </h2>
                    <p><strong>Live Racing:</strong> Thursday through Sunday<br />
                    </p>
                    <p><strong>Live Racing Calendar:</strong> May to September<strong><br />
                    </strong></p>
                    <p><strong>Course type:</strong> Thoroughbred<strong><br />
                      </strong></p>
                    <p><strong>Main track :</strong> 1 mile with 3 1/2 furlong, 6 1/2 furlong &amp; 1 1/4 mile chutes<br />
                    </p>
                    <p><strong>Turf course:</strong> 7/8 of a mile, oval</p>
                    <p>&nbsp;</p>
                    <h2>Thoroughbred Stakes Races at Canterbury Park</h2>
                    <p>Lady Slipper Stakes  <br />
                      10,000 Lakes Stakes<br />
                      Honor The Hero Stakes<br />
                      Dark Star Cup<br />
                      Brooks Fields Stakes<br />
                      Princess Elaine Stakes<br />
                      Blairs Cove Stakes<br />
                      Frances Genter Stakes<br />
                      Victor Myers Stakes<br />
                      Lady Canterbury Stakes<br />
                      Mystic Lake Mile<br />
                      MTA Stallion Auction Stakes Fillies<br />
                      MTA Stallion Auction Stakes C/G<br />
                      Shakopee Juvenile Stakes<br />
                      Northbound Pride Oaks<br />
                      Mystic&nbsp;Lake Derby<br />
                      Minnesota Oaks<br />
                      Minnesota Derby<br />
                      HBPA&nbsp;Distaff<br />
                      Hoist Her Flag Stakes<br />
                      Northern Lights Debutante<br />
                      Northern Lights Futurity<br />
                      Bella Notte MN Distaff Sprint<br />
                      Crocrock MN Sprint<br />
                      Glitter Star MN Distaff Classic<br />
                      Wally's Choice MN&nbsp;Classic</p>
               <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->