{assign  var="racetrackname" value="Sam Houston"} 
{assign  var="racetrackpath" value="sam-houston"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Sam Houston Race Park</strong> opened on April 24, 1994. It is Houston&rsquo;s premier racing and entertainment facility, located just 15 minutes from downtown Houston offering Thoroughbred and Quarter horse racing and high-dollar stakes races every weekend. </p>
                   
                    <h2>Sam Houston Track Schedule</h2>
                    <p><strong>Post times for live Thoroughbred racing:</strong> Friday (7 p.m.), Saturday (6 p.m.), Sunday (1 p.m.) and Monday (1 p.m.) <br>
                    <strong>Post times for live Quarter Horse racing: </strong>Thursday (7 p.m.), Friday (7 p.m.), and Saturday (6 p.m.)<br />
                       <strong> Racing Calendar:</strong> Thoroughbred - January to March and Quarter Horse - March  to May<br>
                       <strong>Course Type:</strong>Thoroughbred, Quarter horse<br />
                         <strong>Main Track:</strong>1-mile track<br />
                         <strong>Turf Course:</strong>7-furlong turf course </p>
                    
                    <h2>Stakes Races at Sam Houston Race Park</h2>
                    <strong>Thoroughbred stakes schedule  </strong><br />
                     Star of Texas Stakes <br />
                      Richard King Stakes<br />
                      Spirit of Texas Stakes<br />
                      Yellow Rose Stakes<br />
                      Houston Ladies Classic<br />
                      John B. Connally Turf Cup<br />
                      Champions Energy Services Stakes<br />
                      Allen&rsquo;s Landing Stakes<br />
                      Bucharest Stakes<br />
                      Tomball Stakes<br />
                      Jim&rsquo;s Orbit Stakes <br />
                      Two Altazano Stakes<br />
                      Texas Six Shooter Stakes <br />
                      Texas Yellow Rose Stakes<br />
                      Jersey Village Stakes<br />
                      MAXXAM Gold Cup<br />
                      Jersey Lilly Stakes<br />
                      Sam Houston Sprint Cup<br />
                      Spring Dodge Stakes<br />
                        </p>
                    
                    <p><strong>American Quarter Horse Meet </strong><br />
                      Bank of America Challenge Championships <br />
                      Sam Houston Futurity</p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
