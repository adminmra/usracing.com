{assign  var="racetrackname" value="Los Alamitos"} 
{assign  var="racetrackpath" value="los-alamitos"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                    {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Los Alamitos Race Course has been offering Quarter Horse racing since 1947. The track hosted its first pari-mutual meet in 1951. What started out as informal match races on the Vessels Ranch in the late '40s, has grown into year-round Quarter Horse racing in California. Quarterhorse racing makes up to 60% of the horse-racing revenue at Los Alamitos.<br>
                            <br>
                      Currently the track is owned and operated by Ed Allred, who has set the standard for Arabian, thoroughbred, paint, Appaloosa and Quarterhorse races. </p>
                    <p>The track has the distinction of holding four quarter horse stakes races with purses over $1 million, more than any other track in the United States. It's the home of the Los Alamitos Super Derby, which annually offers the richest purse of any race for 3-year-olds in California.</p>
                    <h2>Los Alamitos Racetrack Schedule</h2>
                    <p><
	                    <strong>Live Racing: </strong>All Year Around (No racing on Super Bowl Sunday)<br>
                        <strong>Racing Calendar: </strong>Friday through Sunday<br>
                         <strong>Course type: </strong>Dirt<br>
                         <strong>Notable Races:</strong> Champion of Champions, Los Alamitos $2 Million Futurity, Ed Burke Memorial Futurity, Kindergarten Futurity, Golden State Million Futurity, Mildred Vessels Memorial, Los Alamitos Super Derby.<br>
                         <strong>Main track:</strong> 5/8 mile, oval
                        <br><strong>Distance from last turn to finish line:</strong>&nbsp; 558 feet
                    </p>
                                        <h2>Stakes Races at Los Alamitos</h2>
                    <blockquote>
                      <p>Ed Burke Million Futurity<br />
                        Governor's Cup Derby<br />
                        Governor's Cup Futurity<br />
                        Late Golden State Million<br />
                        Late los Alamitos &quot;2&quot; Million<br />
                        Los Alamitos Super Derby<br />
                        PCQHRA Breeders Derby<br />
                        PCQHRA Breeders Futurity<br />
                        Wild West Futurity<br />
                        Golden State Million<br />
                      </p>
                   <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
