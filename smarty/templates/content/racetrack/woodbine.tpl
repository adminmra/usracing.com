{assign  var="racetrackname" value="Woodbine"} 
{assign  var="racetrackpath" value="woodbine"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Woodbine Racetrack</strong> is Canada's premier horse racing track, located just north of the  core of downtown Toronto. It is owned by Woodbine Entertainment Group, formerly known as the Ontario Jockey Club. The track was opened in 1956. It has been extensively remodelled since 1993, and since 1994 has three racecourses. Woodbine  is the only horse racing track in North America that  is capable of staging, thoroughbred and standardbred horse racing programs on the same day.<br>
                          <br>
                      The track hosts to two of the three Canadian Triple Crown events: the Queen's Plate and the Breeders' Stakes. In addition to some significant Canadian horse racing stakes schedule, Woodbine has also hosted some popular horse races that are typically run in the United States, the <a href="/breeders-cup">Breeders' Cup</a> was held at Woodbine in 1996 and the Arlington Million was held there in 1988.</p>
                    
                    <h2>Woodbine Racetrack Schedule</h2>
                    <strong>Live Racing:</strong> Standard bred Racing: Thursday, Friday and Saturday nights, Thoroughbred Racing: Wednesday - Sunday<br>
                    <strong>Live Racing Calendar:</strong> Thoroughbred Racing: April - December, Standard bred Racing: January- December<br>
                    <strong>Course Type:</strong></strong> Flat Thoroughbred/Harness<br>
                    <strong>Notable Races</strong>: Canadian International Stakes (GI), Queen's Plate (GI), Woodbine Mile (GI), E.P. Taylor Stakes (GI)<br>
                    <strong>Main Track:</strong> 1.5 miles<br>
                    <strong>Distance from last turn to finish line:</strong> 975 feet<br>
                    <strong>Turf Course:</strong> 7 Furlongs</p>
                
                    <h2>Race Tracks at Woodbine</h2>
                    <p>The outermost  turf course for thoroughbreds, called the E. P. Taylor track, is 1.5 miles long with a chute allowing races of 1.125 miles to be run around one turn. The Taylor turf course and the main dirt course at Belmont Park on New York's Long Island are the only mile-and-a-half layouts in North American thoroughbred racing.</p>
                    <p>Inside the E. P. Taylor course is the main Polytrack course for Thoroughbreds. A left-handed one-mile oval with chutes facilitating races at seven furlongs   and at 1.25 miles.</p>
                    <p>Inside the Polytrack course is an oval Standardbred racecourse seven-eighths of a mile  in circumference, made of crushed limestone.</p>
                    <h2>Post Times</h2>
                    <p><strong>Thoroughbred Racing Post Times</strong><br />
                      Wednesday nights: 6:45 pm (June to December)<br />
                      Friday, Saturday and Sunday afternoons: 1:00 pm (April to June)<br />
                      Friday, Saturday and Sunday afternoons: 12:50pm (starting June to December)</p>
                    <p><strong>Standardbred Racing Post Times </strong><br />
                      Monday, Thursday, Friday and Saturday nights: 7:25pm (January to May, October to December)</p>
                    
                    <h2>Graded Stakes Races at Woodbine</h2>
                    <blockquote>
                      <p><strong>Grade I</strong></p>
                      <p> Canadian International Stakes<br />
                        E. P. Taylor Stakes<br />
                        Nearctic Stakes<br />
                        Northern Dancer Turf Stakes<br />
                        Woodbine Mile</p>
                      <p><strong>Grade II</strong></p>
                      <p> Autumn Stakes<br />
                        Canadian Stakes<br />
                        Connaught Cup Stakes<br />
                        Dance Smartly Stakes<br />
                        Grey Breeders' Cup Stakes<br />
                        Highlander Stakes<br />
                        King Edward Breeders' Cup Stakes<br />
                        Nassau Stakes<br />
                        Nijinsky Stakes<br />
                        Play The King Stakes<br />
                        Sky Classic Stakes</p>
                      <p><strong>Grade III</strong></p>
                      <p> Dominion Day Stakes<br />
                        Durham Cup Stakes<br />
                        Eclipse Stakes<br />
                        Hendrie Stakes<br />
                        Kennedy Road Stakes<br />
                        Marine Stakes<br />
                        Natalma Stakes<br />
                        Ontario Derby<br />
                        Royal North Stakes<br />
                        Seagram Cup Stakes<br />
                        Seaway Stakes<br />
                        Selene Stakes<br />
                        Singspiel Stakes<br />
                        Summer Stakes<br />
                        Vigil Stakes<br />
                        Whimsical Stakes<br />
                      </p>
                    </blockquote>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
