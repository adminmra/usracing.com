{assign  var="racetrackname" value="Santa Anita Park"} 
{assign  var="racetrackpath" value="santa-anita"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/tracks.tpl'} 
     <!-- ---------------------- end left menu contents ------------------- -->         
</div> 
  
  
  <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
        
       <p><strong>Major Races:</strong> $500,000 Strub Stakes, February 6; $1-million Santa Anita Handicap, March 6; $250,000 Santa Anita Oaks, March 14; $750,000 Santa Anita Derby, April 3; $400,000 San Juan Capistrano Invitational Handicap, April 18</p>
       <p>Santa Anita conducted its inaugural card on Christmas Day, 1934. . . on February 4, 1938, a $2,000 claimer named Playmay won the first race on the card and returned $673.40 for a two-dollar win mutuel. . . at age five, Citation finished second in four consecutive stakes at Santa Anita. . . turf course, built in 1953, has a hillside leg that crosses the main track to connect with the infield oval. . . Affirmed commenced his 1978 Triple Crown campaign with three consecutive Santa Anita victories, including an eight-length score in the Santa Anita Derby. . . in winning the 1980 Charles H. Strub Stakes, Spectacular Bid set a world record for 1 1/4 miles on the dirt of 1:57 4/5. . . Hall of Fame jockey Bill Shoemaker won the Santa Anita Handicap 11 times; fellow Hall of Famer Laffit Pincay Jr. has scored five "Big 'Cap" victories. . . on March 14, 1987, Pincay won a record seven races on the Santa Anita card. . . San Gabriel Mountains provide a stunning backdrop for Santa Anita's races.</p>
       <p><span class="headercolor"><a title="Santa Anita Derby" href="/santa-anita-derby">Santa Anita Derby Stake Race</a>&nbsp;</span></p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
