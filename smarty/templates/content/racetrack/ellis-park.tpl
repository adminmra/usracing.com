{assign  var="racetrackname" value="Ellis Park"} 
{assign  var="racetrackpath" value="ellis-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p>Address: 3300 U.S. 41, Henderson, Kentucky, United States</p>
                    <p>&nbsp;</p>
                    <p>Built in Henderson, Kentucky, Ellis Park held its first thoroughbred meet in 1922.<br>
                            <br>
                      The track has undergone many changes since the 1920's because of ownership changes, barn fires, floods, and a tornado that almost destroyed the facility in 2005. </p>
                    <p><strong>Here's a history of events at Ellis Park Race course:</strong></p>
                    <ul>
                      <li>Originally called Dade Park, it was built in 1922 by the Green River Jockey Club</li>
                      <li>October 19, 1922 its opening race was a Grand Circuit harness meet</li>
                      <li>On November 18, 1922 the gates opened for the first Thoroughbred meet</li>
                      <li>In January of 1937, a huge Ohio river flood sweept through the Tri-State area with the  flood waters reaching the track&rsquo;s mezzanine in the main grandstand</li>
                      <li>In 1954, the track was renamed to honor the Dade Parks' longtime owner, James C. Ellis</li>
                      <li>In November of 2005, a tornado caused significant damage to Ellis Park</li>
                      <li>In September 2006, Ron Geary, a local Kentucky businessman, purchased Ellis Park from Churchill Downs, Inc.</li>
                      <li>In 1988, Ellis Park, along with the Kentucky Horse Center, was sold to Churchill Downs, Inc. </li>
                      <li>In September 2006, Ron Geary, bought Ellis Park from Churchill Downs, Inc. </li>
                      <li>The 9th edition of the Claiming Crown to Kentucky was held at Ellis Park on August 4, 2007 and brought largest handle ever for any Claiming Crown at $4.9 million. </li>
                    </ul>
                                    <h2>                            <strong>Ellis Park</strong><strong> Race </strong><strong>Course <strong>Information:</strong></strong></h2>
                    <p><br>
                    <strong>Live Racing:</strong> Friday to Sunday.<br>
                            </p>
                    <p><strong>Racing Calendar:</strong> 29 days starting on July 4, and ending on Labor Day<br>
                            </p>
                    <p><strong>Course Type:</strong> Thoroughbred<br>
                            </p>
                    <p><strong>Notable Races</strong>: Gardenia Stakes (Grade III), Ellis Park Breeders' Cup Turf, Governor's Handicap, Tri-State Handicap and Audubon Oaks.<br>
                            </p>
                    <p><strong>Main Track:</strong> 1 1/8 miles, oval with 7 furlongs and 1 mile chutes<br>
                            </p>
                    <p><strong>Distance from last turn to finish line:</strong> 1,175 feet<br>
                            </p>
                    <p><strong>Turf Course:</strong> 7 Furlongs</p>
                       
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
