{assign  var="racetrackname" value="Assiniboia Downs"} 
{assign  var="racetrackpath" value="assininboia-downs"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
   {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address:</strong> 3975 Portage Avenue, Winnipeg, Manitoba  Canada</p>
                    <p><strong>Racing Dates: </strong>May to September<br />
                      <strong>Date opened:</strong> 	June 1958<br />
                      <strong>Course type:</strong> 	Flat racing for Thoroughbreds</p>
                    <p>The track is located on the western edge of the city, near the intersection of the Perimeter Highway and Portage Avenue. It was opened in June 1958, replacing the former Polo Park Racetrack. The main track is a six-and-one-half furlong oval.</p>
                    <p>Assiniboia Downs is a Canadian horse race track located in Winnipeg, Manitoba. It is operated by the Manitoba Jockey Club and is the site of the annual Manitoba Derby.</p>

                              
          
<div class="headline"><h1>Manitoba Pride Soars as Goldencents Triumphs in World Championships</h1></div>



<div class="content">



<p>Chalk up an historic &quot;golden&quot; moment for Manitoba on the world stage of horse racing.<br />
                        <br />
                      Goldencents, the 3-year-old colt born to an Assiniboia Downs mare, has now earned $2 million--and is a leading candidate for champion 3-year-old horse of the year--after crushing his rivals in the Dirt Mile in the Breeders' Cup World Championships at Santa Anita on November 1, 2013.<br />
                        <br />
                      Never has a horse with Manitoba breeding reached such dizzying heights. &quot;To achieve recognition of that level on that stage is unbelievable for Manitoba racing and breeding,&quot; said Assiniboia Downs CEO Darren Dunn. &quot;Every Manitoban should feel proud of what this colt has accomplished.&quot;<br />
                        <br />
                      Goldencents, facing 10 seasoned competitors in the $1 million race, including older more-experienced horses, charged to the lead and never looked back, winning by almost three lengths, awarding his backers $9.60 for every $2 bet.  That was his fifth lifetime victory in 12 starts. <br />
                        <br />
                      The Breeders' Cup victory vaulted him into the lead for the vaunted Eclipse Award for 3-year-old champion male which is voted on in the winter by racing officials and sports writers.<br />
                        <br />
                      Also of interest to Manitobans in the same race was the third-place finish of Brazilian-bred Brujo de Olleros who is partly owned by Assiniboia Downs horseman and Manitoba Jockey Club director, Barry Arnason.<br />
                        <br />
                      Goldencents' mother, Golden Works, was bred in Manitoba by &quot;as seen on TV&quot; product innovator Phil Kives.  Kives raced the mare at Assiniboia Downs in the mid 1990's, then took her down to Kentucky where he sold her for $7,000.  She was subsequently bred to the stallion, Into Mischief, and the rest is history.  One of Goldencents' owners is Rick Pitino, the prominent coach of the University of Louisville men's basketball team.<br />
                        <br />
                      Goldencents' sparkling career had one bump in the road--when he didn't take to the sloppy track at Churchill Downs for the Kentucky Derby and faded to 17th--but he rebounded to become the toast of California racing.<br />
                        <br />
                      &quot;And the exciting thing is, there is still lots of upside to this gorgeous colt,&quot; said Dunn.  &quot;The ease in which he won the world championship portends a future that is unlimited. We could be hearing about this horse for years to come.&quot;<br />
                        </p>
                    <h2><br />
                            <br />
                      Stakes Races at Assiniboia Downs for Two Year Olds</h2>
                    <p>                      Graduation Stakes<br />
                      Sales Stake<br />
                      Alws Osiris Stakes<br />
                      Alws Debutante (Fillies) <br />
                      Winnipeg Futurity <br />
                      Buffalo Stakes</p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Assiniboia Downs for Three Year Old Fillies</h2>
                    <p>Hazel Wright Sires Stake<br />
                      Chantilly Stakes<br />
                      R.C. Anderson Stakes<br />
                      Gold Strike Mile<br />
                      Assiniboia Oaks<br />
                      Jack Hardy Stakes</p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Assiniboia Downs for Three Year Olds</h2>
                    <p>Golden Boy Stakes<br />
                      Frank Arnason Sire Stakes<br />
                      Harry Jeffrey Stakes<br />
                      Gold Strike Mile<br />
                      Manitoba Derby<br />
                      J.W. Sifton Stakes</p>
            
        

  
            
            <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                       
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->
</div> <!-- end/ content -->


<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
