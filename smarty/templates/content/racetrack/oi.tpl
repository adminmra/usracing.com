{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/oi.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Oi Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-track-primary.tpl'} </p>
        {*end*}
        <p>Get your Oi Horse Betting action Today! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
        {*Second Copy of the page*}

		  {*Secondary Copy*}
		  <h2>Oi Racecourse Major Races</h2>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Oi Racecourse Major Races"
        summary="Major races for Oi Racecourse. Only available at BUSR.">
        <caption>Oi Racecourse Notable Races</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Tokyo Daishōten </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2000m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o </td>
            </tr>
            <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Tokyo Derby </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2000m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
            </tr>
			  <tr>
                <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;">Teio Sho </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2000m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 4-y-o</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Tokyo Kinen </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2400m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
            </tr>
				  <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Japan Dirt Derby </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2000m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
             </tr>
              <tr>
                <td data-title="Month">May</td>
                <td data-title="Race" style="word-break: keep-all !important;">Haneda Hai </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1800m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
             </tr>
              <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Tokyo Princess Sho</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1800m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
             </tr>
              <tr>
                <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Kinpai </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 2600m </td>
                <td data-title="Age" style="word-break: keep-all !important;"> 3-y-o</td>
             </tr>
        </tbody>
    </table>
            &nbsp;
        {*end*}
        <br>
        </li>
        </ul>
		    <h2>Oi Off Track Betting</h2>
		  <p>Built in 1950, this racecourse, also known as Tokyo City Keiba is situated in Shinagawa, Tokyo, Japan. Oi Racecourse currently holds a partnership with California's Santa Anita Park, and as a result, TCK holds "Santa Anita Week" every summer, featuring the "Santa Anita Trophy" one-mile race.</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
        {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}