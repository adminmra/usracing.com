{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/gulfstream-park.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
          .kd {
            background: transparent url(/img/racetracks/gulfstream_park/gulfstream-park-florida-horse-betting.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/gulfstream_park/gulfstream-park-florida-horse-betting-pegasus.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Gulfstream Park Horse Betting</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Gulfstream Park Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <p>Get your Gulfstream Park Horse Betting action with <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Gulfstream Park Major Races"
        summary="Major races for Gulfstream Park. Only available at BUSR.">
        <h2>Gulfstream Park Information</h2>
  <div class="racetrack-text">
  <p><strong>Live Racing:</strong>  Thursday - Saturday<br>
  <strong>Live Racing Calendar:</strong>  December to April<br>
  <strong>Course type:</strong> Flat Notable<br>
 <strong>Main track:</strong> 1 1/18 mile, oval<br>
  <strong>Distance from last turn to finish line:</strong> 898 feet<br>
  <strong>Turf course:</strong>  Seven furlongs</p></div>		  
			  <h2> Gulfstream Park - Major Races</h2>	
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Gulfstream Park Major Races"
        summary="Major races for Gulfstream Park. Only available at BUSR.">
        <caption>Gulfstream Park Major races - Grade 1</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pegasus World Cup </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
			  <tr>  
               <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pegasus World Cup Turf</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				    <tr>
                <td data-title="Month">Mar-Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Florida Derby </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo  </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Gulfstream Park Major Races"
        summary="Major races for Gulfstream Park. Only available at BUSR.">
        <caption>Gulfstream Park Major races - Grade 2</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Holy Bull Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">	1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			<tr>
                <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Davona Dale Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 mile (8 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
			<tr>
                <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fountain of Youth Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄16 Miles (8 1/2 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			 <tr>  
               <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Mac Diarmida Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 3⁄8 miles (11 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				  <tr>  
               <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Inside Information Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
			  <tr>  
               <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Gulfstream Park Mile Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 mile (8 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				    <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pan American Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄2 miles (12 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
			    <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Gulfstream Oaks </td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			    <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Princess Rooney Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">7 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f&m </td>
            </tr>
			    <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fort Lauderdale Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Gulfstream Park Off Track Betting</h2>
		  <p>Built in 1939, this race course is located in Hallandale Beach, Florida, and it´s one of the most important venues for horse racing in the USA. Gulfstream Park features two major racing events, The Florida Derby and The Pegasus World Cup</p>
		  <br>
		 {************ End PAGE SPECIFIC COPY*****************************************}		 
 {include file="/home/ah/allhorse/public_html/usracing/racetracks/end-of-page-blocks.tpl"}		