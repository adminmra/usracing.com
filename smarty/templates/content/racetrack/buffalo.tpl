{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Buffalo Raceway</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address:</strong> 5600 McKinley Pkwy, Hamburg, NY 14075, United States</p>
                    <p>&nbsp;</p>
                    <p>Buffalo Raceway opened in June of 1942, shortly after New York State passed a bill allowing pari-mutuel harness racing. The track is located on the Erie County Fairgrounds. With it's proximity to Canada, and an insufficient supply of local horsemen and horses, many Canadian stables arrive in Hamburg every year. Since it opened, an estimated thirty thousand horses have competed here along with over four thousand trainers and drivers. The racing season runs from January to June.</p>
                    <p>Buffalo Raceway became a harness racing fixture with the start of the 1960's and was considered one of the top ten tracks in America at the time.</p>
                    <p>The all time attendance record of 12,779 was established July 15, 1966 when the top horse of the era, Bret Hanover, was victorious in a stake race.</p>
                    <p>The 2010 racing season saw over $250,000 spent on improvements to the facilities. The 1/2 mile track was resurfaced with over 600 tons of material and the Clubhouse had extensive renovations.</p>
                    <p>&nbsp;</p>
                    <h2>Bet on Horses at Buffalo Raceway</h2>
                    <p>Place your bet at Buffalo Raceway online anywhere, anytime at US Racing. Betting on stakes races at Buffalo Raceway has never been easier. View live results, up to the minute schedules, betting odds and a daily rebate. To experience all that the Buffalo Raceway has to offer, along with   200 of the world&rsquo;s best horse racing destinations, <a href="/signup/">join</a> US Racing today!</p>
                    <p>&nbsp;</p>
                    <h2><strong>Buffalo Raceway Track Information</strong></h2>
                    <p><br />
                      <strong>Live Racing: </strong>Wednesday, Thursday, Friday, Saturday and Sunday<strong><br />
                      </strong></p>
                    <p><strong>Racing Dates:</strong> January to July</p>
                    <p><strong>Course type:</strong> Harness<strong><br />
                    </strong></p>
                    <p><strong>Main track:</strong> 1/2 mile<br />
                    </p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-tracks.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
