{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
  <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

                    <p>Freehold Raceway was has been hosting races since 1853 and is the oldest 1/2 mile track in the United States. It's located in central New Jersey, in the in the historic town of Freehold. Freehold features live Standardbred harness racing for trotters and pacers. In 2005, Freehold was also the temporary home of the Yonkers Trot, part of the Triple Crown of Harness Racing for Trotters, while renovations took place at Yonkers Raceway.</p>
                    <h2>Freehold Racetrack Information:</h2>
                    <p>
                    <strong>Live Racing: </strong>January-May and September-December<br>
                              
	                <strong>Racing Calendar: </strong>Thursday, Friday and Saturday<br>
                    <strong>Course type: </strong>Trotters and Pacers<br>
                    <strong>Notable Races:</strong> Cane Pace<strong><br>
                    <strong>Main track:</strong> 1/2 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong>&nbsp; 480 feet</p>
                    
                    <h2>Stakes Races at Freehold Raceway</h2>
                    <blockquote>
                      <p>Dexter Cup <br />
                        Lady Suffolk<br />
                        Green Acres <br />
                        Charles &amp; Helen Smith<br />
                        Lou Babic<br />
                        Harold &amp; Marion Dancer<br />
                        NJ Futurity </p>
                    </blockquote>
                   
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
