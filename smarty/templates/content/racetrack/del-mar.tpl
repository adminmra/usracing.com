{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/saratoga.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
  margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       @media (max-width: 800px) {
            .hide-sm {
                display: none;
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref1}&signup-modal=open" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref1}&signup-modal=open" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
        <h3 class="kd_subheading">{$h2}</h3>

        <p>Enjoy horse betting with a <a href="/promos/cash-bonus-20">$500 bonus on your first deposit</a>, and qualify for an extra  <a href="/promos/cash-bonus-150">$150 bonus!</a></p>
        <p>Why is BUSR the best? Because, all new members can get up to a <a href="cash-bonus-20">$500 welcome bonus!</a> You'll also get <b>free bets</b> and great bonuses found nowhere else online!</p>
        <p>Get your Del Mar Horse Betting action with <a href="https://busr.ag?ref={$ref2}&amp;signup-modal=open" rel="nofollow">BUSR</a> now! </p>
        <p><a href="https://busr.ag?ref={$ref2}&amp;signup-modal=open" rel="nofollow" class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>



        <h3>Del Mar Information</h3>
        <h4>Where The Turf Meets The Surf</h4>
        <div class="racetrack-text">
          <p><strong>Live Racing Calendar:</strong> July to September<br>
          <strong>Live Racing Days:</strong> Friday, Saturday and Sunday<br>
          <strong>Main track:</strong> 1 mile, oval<br>
          <strong>Distance:</strong> Seven furlongs<br>
          <strong>Surface:</strong>  Turf </p>
        </div> 
        <br>

        <p><a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>

        <h2>Del Mar 2020 Stakes Schedule</h2>
        <div id="no-more-tables">
        <table class="table table-condensed table-striped table-bordered ordenableResult data" width="100%" cellpadding="0" cellspacing="0" border="0" title="Del Mar 2020 Stakes Schedule" summary="Del Mar 2020 Stakes Schedule">
        <thead>
            <tr>
                <th> Date </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Purse </th>
            </tr>
        </thead>
        <tbody>
            <caption class="hide-sm">Del Mar August Stakes Calendar</caption>
            <tr>
                <td data-title="Date">Saturday, August 1st</td>
                <td data-title="Race" style="word-break: keep-all !important;">Bing Crosby Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">6 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$250,000</td>
            </tr>
            <tr>
                <td data-title="Date">Sunday, August 2nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Clement L. Hirsch Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄16 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$250,000</td>
            </tr>
            <tr>
                <td data-title="Date">Friday, August 7th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Sorrento Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">6 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 8th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Yellow Ribbon Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄16 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 8th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Best Pal Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">6.5 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
            </tr>
            <tr>
                <td data-title="Date">Sunday, August 9th</td>
                <td data-title="Race" style="word-break: keep-all !important;">La Jolla Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;">​1 1⁄16 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$125,000</td>
            </tr>
            <tr>
                <td data-title="Date">Friday, August 21st</td>
                <td data-title="Race" style="word-break: keep-all !important;">Rancho Bernardo Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;">6.5 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 22nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">TVG Pacific Classic</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄4 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$500,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 22nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Torrey Pines Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 Mile</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 22nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Green Flash Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;">5 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 22nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Del Mar Oaks</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$250,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, August 22nd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Del Mar Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;">11 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$200,000</td>
            </tr>
            <tr>
                <td data-title="Date">Sunday, August 23rd</td>
                <td data-title="Race" style="word-break: keep-all !important;">Del Mar Mile</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 Mile</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
            </tr>
            <tr>
                <td data-title="Date">Saturday, September 5th</td>
                <td data-title="Race" style="word-break: keep-all !important;">John C. Mabee Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
            </tr>
            <tr>
                <td data-title="Date">Sunday, September 6th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Del Mar Derby</td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 Miles</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$200,000</td>
            </tr>
            <tr>
                <td data-title="Date">Sunday, September 6th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Del Mar Debutante Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;">7 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$250,000</td>
            </tr>
            <tr>
                <td data-title="Date">Monday, September 7th</td>
                <td data-title="Race" style="word-break: keep-all !important;">Runhappy Del Mar Futurity</td>
                <td data-title="Distance" style="word-break: keep-all !important;">7 Furlongs</td>
                <td data-title="Purse" style="word-break: keep-all !important;">$250,000</td>
            </tr>
        </tbody>
        <br>
        </table>
        </div>

        <br><br>
        <p><a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>

        <h2>Del Mar Racing</h2>

        <p>"Where the Turf Meets the Surf<strong>"</strong></p>
        <p>Via De La Valle &amp; Jimmy Durante Blvd.<br />
        Del Mar, California 92014<br />
        (858) 755-1141</p>

        <p><strong>Track Facts:</strong></p>
        <p><strong>Main Track:</strong> One Mile, oval.<br />
        <strong>Turf Course:</strong> Seven eighth's Mile.<br />
        <strong>Distance from last turn to finish line:</strong> 919 feet.</p>

        <p><strong>About:</strong></p>
        <p>The annual beach party known as the Del Mar Thoroughbred racing season parades to the post beginning Wednesday, July 21, 2010. The 43 days of racing extends through Wednesday, September 8, with a six-day-a-week schedule, Tuesday being each week's day of rest.</p>
        <p>Del Mar is unique in that its facilities are owned by the State of California and controlled by the 22nd District Agricultural Association, operators of the Southern California Exposition, which annually attracts more than a million visitors from mid-June through the 4th of July holiday to what is commonly called the "Del Mar Fair."</p>
        <p>The track's Spanish-style architecture and laid-back attitude make Del Mar the annual setting for one of America's foremost salutes to the sport, where horses, trainers, jockeys and owners come together for racing beside the sea. The track sits a couple of furlongs from the shimmering blue Pacific ("where the turf meets the surf," get it?), about 20 miles north of San Diego and 100 miles south of Los Angeles.</p>
        <p>Del Mar has been a national leader in terms of daily average attendance<strong> </strong>for the past two decades. In 2009 the on-track attendance was 17,181 per day, totaling 635,679 fans during the 37-day summer season.</p>
        <p>So what makes the racing season at Del Mar different from say, Hollywood Park or Santa Anita up north LA way? Well, for one, it's not in LA (have you ever driven to Hollywood Park? Scary area, that Inglewood is). But besides the typical different stakes activity and trainers, stables and jockeys it attracts, Del Mar is just plain cool.</p>
        <p>And it all begins with the track's beginning back in 1937 when crooner Bing Crosby and some of his Hollywood buddies such as Pat O'Brien and Jimmy Durante got together and found a spot in a sleepy town called Del Mar to start a race track. Now, you gotta either love horses or love gambling to do something like this. Most would say it's foolish, but the genius lies in selecting Del Mar as the track's site. You see, a seaside track is just too cool...especially in the middle of summer!</p>
        <p>Then, in 1938, the great legendary horse, Seabiscuit, came to town in an epic race against Ligaroti, where Seabiscuit, with George "Iceman" Woolf riding, won by a nose. How cool is that? Given that Seabiscuit was immortalized in the acclaimed book by Laura Hillenbrand and the much-anticipated movie based on the book is being released this summer, well, it's very cool to know Del Mar had a chapter in the legendary horse's life.</p>
        <p>Through the years, the track ebbed and flowed with the fortunes of the sport. Some of the greats names in horse racing have graced its turf: Pincay, Shoemaker, Wittingham, Longden, Delahoussaye, Lukas, Baffert, McCarron, Cigar, Mabee, Best Pal. The list goes on...</p>
        {*    <p>In 1993, a major renovation and expansion made Del Mar an even better place to bet on the ponies. Whether it's rubbing elbows with the well-heeled in the Turf Club, or with the sun lovers in their beach chairs down trackside, Del Mar brings a laid-back, fun-loving attitude to horse racing. You might say, San Diego has put it's stamp on horse racing...and losing money was never so much fun!</p> *}


        <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc_signup_modal.tpl"} </p>  

      </div>
    </div>
  </section>
  {* Block for the testimonial  and path *}
  {include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
  {* End *}
  {* Block for the signup bonus and path*}
  {include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
  {* End*}
  {* Block for the signup bonus and path*}
  {include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
  {* End*}
  {* Block for the signup bonus and path*}
  {include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
  {* End*}
  {* Block for the block exciting  and path *}
  {*include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"*}
  <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
    <div class="container">
      <div class="card_wrap">
        <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
          <h2 class="card_heading card_heading--no-cap">Get your New Member Bonus Up To $500 Cash!<br>at BUSR</h2>
          <a href="https://busr.ag?ref={$ref}&signup-modal=open" rel="nofollow" class="card_btn card_btn--default">Sign Up Now</a>
        </div>
      </div>
    </div>
  </section>
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}