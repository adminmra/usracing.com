{assign  var="racetrackname" value="Delaware Park"} 
{assign  var="racetrackpath" value="delaware-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>






<p>Delaware Park Racetrack is the only thoroughbred horse racing track in the state of Delaware.<br>
                        <br>
                      Almost Triple Crown winner Afleet Alex and Kentucky Derby winner Barbaro, both won their career debuts at Delaware Park Racetrack.<br>
  <br>
                      Races at <strong>Delaware Park</strong> are held from April to November, and include Grade II events such as Delaware Oaks, as well as Grade III races such as the annual Endine Stakes, Kent Stakes and Obeah Stakes.</p>
     


<h2>Delaware Park Race Track Schedule 
</h2>
<p>  <strong>Live Racing:</strong> Every Saturday, Monday, Wednesday &amp; Thursday<br>
  <strong>Racing Dates:</strong> April through November<strong><br>
    <strong>Course Type:</strong></strong> Thoroughbred - Flat racing<br>
    <strong>Notable Races:</strong>&nbsp;Grade II: Delaware Oaks, Delaware Handicap. Grade III: Barbaro Stakes, Endine Stakes, Kent Stakes, Obeah Stakes, Robert G. Dick Memorial Handicap.<strong><br>
      <strong>Main Track:</strong></strong> One mile oval with six furlong and 1 1/4 mile chutes<br>
    <strong>Distance from last turn to finish line:</strong> 990 feet<br>
    <strong>Turf Course:</strong> 7 furlongs</p>

            
        

  
            
            
                      
        
       <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->