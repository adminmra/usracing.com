{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/preaknessstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Pimlico Race Course</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Where is Pimlico Race Course?</strong></p>
<br />
<table class="show_design_border" bordercolor="#111111" cellspacing="0" cellpadding="10" border="0"><tbody>
<tr>
<td><strong>Pimlico Race Course<br /></strong>5201 Park Heights Ave.<br />

Baltimore, MD 21215</td><td>1-410-542-9400 </td></tr><tr><td><strong>Racing Office<br />
for Horsemen:</strong> </td><td>1-800-638-1859 </td></tr><tr><td><strong>Free Race Results Line</strong>: </td>
<td>1-410-792-0278 </td></tr><tr><td><strong>E-mail</strong></td><td><a href="mailto:info@marylandracing.com">info@marylandracing.com</a> </td></tr><tr><td><strong>Specific Stakes Info</strong>: </td><td><a href="mailto:racing@marylandracing.com">racing@marylandracing.com</a></td></tr></tbody></table><span class="headercolor"><br /><br /></span><table class="show_design_border" cellspacing="10" cellpadding="0" width="100%" border="0"><tbody><tr><td colspan="2" height="23"><div class="storytitle" align="center"><strong>HISTORY OF PIMLICO RACE COURSE</strong><p>&nbsp;</p></div></td></tr><tr valign="top">

<td  style="padding: 10px;" width="50%">Historic Pimlico Race Course, home of the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>, first opened its doors on October 25, 1870<span style="color: black">, making it the second</span> oldest racetrack in the nation behind <span style="COLOR: black">Saratoga, which debuted in</span> 1864 in upstate <span style="COLOR: black">New York.<br /><br />

Engineered by General John Ellicott,</span> Pimlico has played host to racing icons for over a century, where Baltimoreans have seen the likes of legendary horses such as Man o' War, Sir Barton, Seabiscuit, War Admiral, Citation, Secretariat and Cigar thunder down the stretch in thrilling and memorable competition.   <br /><br />
Constructed on 70 acres west of the <span style="COLOR: black">Jones</span> <span style="COLOR: black">Falls, the Maryland</span> Jockey Club purchased the land for $23,500, and built the racetrack  for $25,000 after <span style="COLOR: black">Maryland's Governor at the time, Oden Bowie,</span>  suggested the interesting proposition during a dinner party in  <span style="COLOR: black">Saratoga, New York in 1868. Bowie and his friends, prominent</span> racing figures, had agreed to run a race in two years commemorating the evening, for horses that were yearlings at the time.</td>

<td style="padding: 10px;" width="50%"><p>The winner would have to host the losers for dinner. Both Saratoga and the American Jockey Club made bids for the event, but Governor Bowie pledged he would build a model racetrack in his home state if the race were to be run in Baltimore. Thus, Pimlico was built.<br /><br />
'Pimlico' was the name given the area by English settlers in Colonial times, although the 'Pemblicoe' spelling appeared on the original settlement charter granted to a group of Englishmen in 1669. The colonists hailed from an area near London, and harbored memories of a famous landmark 'Olde Ben Pimlico's Tavern.<br /><br />
On a typical race day in the 1800's, Baltimoreans in all sorts of horse-drawn carriages paraded out through Druid Hill Park, then by Green Spring Road to the Course. Afterwards, in the early days, a spur was built from the Western Maryland Railroad at Arlington direct to the grandstand, for convenience.</p></td></tr><tr><td colspan="2"><div align="center"><img title="Pimlico Race Course" alt="Pimlico Race Course" src="/media/images/content/allhorseracingcom/Pages/Racetracks/pimlico1.jpg" border="0" /></div></td></tr></tbody></table>


     

<p>The racetrack soon became affectionately known as 'Old Hilltop,' after a small rise in the infield that became a favorite gathering place for trainers and race enthusiasts to view the contestants close-up, and vigorously cheer on their favorites. The infield was always a fashionable rendezvous, where in days gone by the four-in-hands, 'spikes,' tandems, pairs and singles were parked and lively guests congregated between the races for a champagne lunch. This custom continues today in the <span style="COLOR: black">Corporate</span> <span style="COLOR: black">Village at Preakness, where over 5,000 people representing many major corporations in the Mid- Atlantic region gather in a 21st century version of yesteryear's 'garden party.' Over 60,000 revelers crowd additional areas of the infield to celebrate Preakness Day.<br /><br /></span>Regrettably, though the famous moniker remains today, the notable infield 'hill' was <span style="COLOR: black">removed in April 1938, ostensibly for obscuring track-level vision of the racetrack backstretch, which appeared to pose a problem for movie and television cameras in the infant days of</span> filming races.<br /><br />
Despite a brief hiatus from flat racing between 1889 and 1904 - when the Preakness <span style="COLOR: black">and Dixie were run at other tracks, and 'outlaw' race meets sprung up around Maryland Pimlico has conducted racing each year since its revival in 1904. During this interim period, steeplechase enthusiasts kept racing alive, and even became Maryland Jockey Club members upon Pimlico's re-emergence. In 1904, racing at Pimlico ignited unprecedented recognition and interest from the public and newspapers alike. Race charts appeared, quite similar to modern-day style, and for the first time Baltimore readers found the news accounts more than mere social reports.</span></p><p></p><p>Pimlico today welcomes race goers arriving by car, limousine, and even helicopter, as graciously as those who visited when 'Old Hilltop' was reached primarily by horse-drawn vehicle, over 130 years ago. During its rich history, the racetrack has enjoyed being the only track in the United States to be honored by the adjournment of the U.S. House of Representatives for the first and only time in history in 1877 to watch a race between Parole, Ten Broeck and Tom Ochiltree. The race became known as 'The Great Race,' and a reproduction of its finish is immortalized as a Pimlico trademark, adorning the clubhouse as a signal to all entering that Pimlico is a place where legends will endure forever.<br /><br />
En route to becoming a true national treasure, Pimlico has earned its patina of age, weathering small and major wars, recessions, depressions - including the Great Depression of the 1930's - fires, storms . . . and the simple passage of time. Its vitality has spanned many an era, representing a time and a society now involving three centuries. More than 50 years ago, the youthful president of the Maryland Jockey Club, Alfred G. Vanderbilt, made a pertinent observation that remains today, as Pimlico moves into another century: 'Pimlico is more than a dirt track bounded by four streets. It is an accepted American institution, devoted to the best interests of a great sport, graced by time, respected for its honorable past.'<br /><br />
Racing in Pimlico even survived a 1910 anti-gambling movement that swept the country, prohibiting the sport everywhere, except in Maryland and Kentucky. Colonel Matt Winn of Churchill Downs is alleged to have credited Pimlico's Billy Riggs as the savior of eastern racing at this time. It was Riggs' use of the less sinful 'French Pools,' or pari-mutuel machines, in 1913 as opposed to the controversial bookmakers and their blackboards, that preserved racing at Pimlico during this turbulent time in racing. A new era was born at Pimlico, which later became the first racetrack in the country to utilize an electric starting gate.  </p>
<p>&nbsp;</p>
<p>&nbsp;</p>

        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-tracks.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    