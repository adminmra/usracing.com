{assign  var="racetrackname" value="Fairplex Park"} 
{assign  var="racetrackpath" value="fairplex-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>

<h2>{$racetrackname} Racing</h2>

                    <p>Located in Pomona, California, <strong>Fairplex Park Racetrack </strong>features 13 days of live thoroughbred racing every September during the L.A. County Fair in addition to several stakes events running each year.</p>
                    <p>Los Angeles County Fair Association reached an agreement in February of 2013 for naming rights, and the race meet is now referred to as the &quot;<strong>Barretts Race Meet at Fairplex</strong>.&quot; The racing season at Fairplex Park opens with the Beverly J. Lewis Stakes.</p>
       <h2>
            Fairplex Park Track Information:</h2>
                    <strong> Live Racing:</strong> Thursday to Sunday
                    <strong>Live Racing Calendar:</strong> September (during the County Fair)<br>
                    <strong>Course type:</strong> Flat/Thoroughbred<br>
                    <strong>Notable Races:&nbsp;</strong> Derby Trial Stakes, Beau Brummel Stakes, Governor's Cup, Pio Pico Stakes, Barretts Debutante, Barretts Juvenille, Las Madrinas Handicap, Pomona Derby<br>
                    <strong>Main track</strong> (sandy loam surface): 5/8 mile oval<br>
                     <strong>Distance from last turn to finish line:&nbsp;</strong> 757 feet</p>
                   
                    <h2>Stakes Races at Fairplex Park</h2>
                    <blockquote>
                      <p>Beverly J. Lewis<br />
                        E.B. Johnston<br />
                        Jim Kostoff<br />
                        C.B. Afflerbaugh<br />
                        Phil D. Shepherd<br />
                        Barretts Debutante<br />
                        Barretts Juvenile<br />
                        Governor's Cup<br />
                        Bangles and Beads<br />
                        Las Madrinas<br />
                        Pomona Derby<br />
                        Ralph M. Hinds Pomona Handicap<br>
                      </p>
                    </blockquote>
            
        
     
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
