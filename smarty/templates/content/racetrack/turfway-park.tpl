{assign  var="racetrackname" value="Turfway park"} 
{assign  var="racetrackpath" value="turfway-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p><strong>Turfway Park</strong> is located just outside the city limits to the north of Florence, Kentucky. Turfway offers live Thoroughbred racing during the fall, winter, and early spring. It is open all year for simulcast racing from top tracks around the country. It hosts the Turfway Park Fall Championship (G3) which was named a Breeders' Cup Challenge &quot;Win and You're In&quot; race, with its winner guaranteed a spot in the <a href="/breeders-cup/marathon">Breeders' Cup Marathon</a>.
                    </p>
                    
<h2>Turfway Park Track Schedule</h2>
                    <p><strong>Live Racing: </strong>Thursday, Friday, Saturday and Sunday<br />
                    <strong>Winter/Spring Meet:</strong> January to March <br />
                    <strong>Holiday Meet:</strong> December<br />
					<strong>Holiday Meet Post Times:</strong> Thursday-Friday: 5:30pm; Saturday-Sunday: 1:10pm<br />
                    <strong>Course Type:</strong> Thoroughbred<br />
                    <strong>Main Track:</strong> 1 mile Polytrack oval with quarter-mile and 6 1/2 furlong chutes<br />
                    <strong>Distance from last turn to finish line:</strong> 970 feet</p>
                    <h2>Stakes Races at Turfway Park</h2>
                    <blockquote>
                      <p>                        Holiday Inaugural Stakes<br />
                        Prairie Bayou Stakes<br />
                        Holiday Cheer Stakes<br />
                        Turfway Prevue Stakes<br />
                        Likely Exchange Stakes<br />
                        Cincinnati Trophy Stakes<br />
                        96ROCK Stakes<br />
                        John Battaglia Memorial Stakes <br />
                        Horseshoe Casino Cincinnati Spiral Stakes (GIII)<br />
                        Fathead Bourbonette Oaks (GIII)<br />
                        Team Valor Rushaway Stakes</p>
                    </blockquote>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
