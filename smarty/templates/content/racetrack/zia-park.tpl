{assign  var="racetrackname" value="Zia Park"} 
{assign  var="racetrackpath" value="zia-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Zia Park</strong> is a thoroughbred horse racing track owned  by Penn National Gaming, Inc.and located in the city of Hobbs, NM. It offers live horse racing from September to December.</p>
                                       <h2>Stakes Races at Zia Park</h2>
                    <blockquote>
                      <p>Premiere Cup Handicap<br />
                        New Mexico Classic Derby (RG II)<br />
                        New Mexico Classic Quarterhorse Champ. (RG I)<br />
                        New Mexico Classic Futurity (RG I)<br />
                        Chaves County Stakes<br />
                        Hobbs American Derby (G II)<br />
                        Hobbs American Futurity (G II)<br />
                        Herman Jefferson Stakes (G III)<br />
                        Lubbock Stakes (G III)<br />
                        James Issac Hobbs (G II)<br />
                        New Mexico Classic Cup Sprint<br />
                        New Mexico Classic Juvenile<br />
                        Lovington Stakes (G II)<br />
                        ZIA Park Championship<br />
                        ZIA Park Distaff<br />
                        ZIA Park Oaks<br />
                        ZIA Park Derby<br />
                        ZIA Park Quarterhorse Championship (G I)<br />
                        Southwest Juvenile Champ. (G I)</p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                    </blockquote>
            
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
