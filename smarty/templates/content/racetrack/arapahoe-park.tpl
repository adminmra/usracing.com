{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address</strong>: 7800 S Adams St, Centennial, CO 80122, United States</p>
                   
                    <p>Arapahoe Park Racetrack  hosts Thoroughbred, Quarter Horse, Paint Horse and Arabian horse racing.</p>
                    <p>The track is open starting with Memorial Day weekend and goes through August. Arapahoe Park races approximately 40 days out of the year, Friday&ndash;Sunday.</p>
                    <p>Arapahoe Park also offers exactas, trifectas and quinellas on most races, as well as superfectas and daily doubles on selected races. The minimum bet is $2.00.</p>
                    <p>In case you aren't familiar with the term, quarter horse, let's compare it to thoroughbreds. Thoroughbreds run races measured in furlongs around an oval track, and purses are generally higher. Quarter Horses are a smaller breed than thoroughbreds, and race a quarter of a mile, or 400 yards. The shorter distance means most Quarter Horses run straight without any turns around the track. <br />
                    </p>
                     <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                       
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->