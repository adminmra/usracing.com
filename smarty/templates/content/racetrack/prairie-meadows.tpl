{assign  var="racetrackname" value="Prairie Meadows"} 
{assign  var="racetrackpath" value="prairie-meadows"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Located in the Altoona, about five miles from Des Moines, Iowa, <strong>Prairie Meadows Raceway</strong> has a one mile dirt track with a chute for quarter horses.<br>
                        <br>
                      Prairie Meadows held its first race in the spring of 1989, almost six years after the state of Iowa legalized parimutuel betting. The track hit some hard times in those first few years but got back on track by 1993 and even expanded in 2006.</p>
                    <h2>Prairie Meadows Track Schedule</h2>
                    <p>
                    <strong>Live Racing: </strong>Friday - Sunday<br>
                    <strong>Racing Calendar:&nbsp; </strong>Thoroughbred racing: April through August. Quarter Horse racing: August through October. Standardbred racing: October<br>
                    <strong>Course type: </strong>Thoroughbred and Quarter Horse<br>
                    <strong>Notable Races: </strong>Cornhusker Handicap (Grade 2), Iowa Oaks (Grade 3), Iowa Derby (Grade 3), Bank of America Challenge Championships<br>
                    <strong>Main track:</strong> 1 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong> 1,033 feet</p>
                
                    <h2>Stakes Races at Prairie Meadows</h2>
                    <blockquote>
                      <p>Cornhusker Handicap<br />
                        Iowa Oaks<br />
                        Iowa Derby</p>
                    </blockquote>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->