{assign  var="racetrackname" value="Mountaineer Park"} 
{assign  var="racetrackpath" value="mountaineer-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p><strong>Mountaineer Racetrack</strong> is a thoroughbred racetrack located in Chester, West Virginia. The facility hosts over 200 race dates a year and runs 5 nights per week.</span></span> The only day race offered is the yearly West Virginia Derby.</p>
                    <p>Opening day was held in May of 1951 under the name Waterford Park. In 1987, the track was sold to new owners who renamed it as Mountaineer Park.</p>
                    <p>Home of the West Virginia Derby, a Grade II race held the first Saturday in August every year. Prior to 1998, the West Virginia Derby was timed to one-fifth of a second. Since   then, the race has been timed to one-hundredth of a second.</p>
                 <h2>Mountaineer Park Track Schedule  </h2>
                    <p> <strong> Live Racing:</strong> Five nights a week<br>
                    <strong>Racing Calendar:</strong> March to December<br>
                    <strong>Course type:</strong> Flat/Thoroughbred<br>
                    <strong>Notable Races:&nbsp;</strong>West Virginia Derby (Grade II), Harvey Arneault Memorial Breeders' Cup Stakes, West Virginia Senate President's Breeders' Cup, West Virginia House of Delegates Speaker's Cup, West Virginia Legislature Chairman's Cup, Panhandle Handicap, Hancock County Handicap, Waterford Park Handicap, Mountaineer Mile Handicap, Labor Day Handicap, Ohio Valley Handicap<br>
                     <strong>Main track (dirt):</strong> 1 mile oval<br>
                      <strong>Distance from last turn to finish line:&nbsp;</strong> 905 feet<br>
                      <strong>Turf course:</strong> 7 furlongs</p>
                    <h2>Stakes Races at Mountaineer Park</h2>
                    <blockquote>
                      <p>Independence Day Stakes<br />
                        Firecracker Stakes<br />
                        West Virginia Derby <br />
                        West Virginia Governor&rsquo;s Stakes<br />
                        Senator Robert C. Byrd Memorial Stakes<br />
                        West Virginia Senate President&rsquo;s Cup<br />
                        West Virginia House of Delegates Speaker&rsquo;s Cup<br />
                        West Virginia Legislature Chairman&rsquo;s Cup<br />
                        TheWest Virginia Secretary of State<br />
                        Mountaineer Juvenile Stakes<br />
                        Mountaineer Juvenile Fillies Stakes<br />
                        Mountaineer Mile Stakes<br />
                        Sophomore Sprint Championship Stakes</p>
                    </blockquote>
           
        <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
