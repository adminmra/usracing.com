{assign  var="racetrackname" value="Laurel Park"} 
{assign  var="racetrackpath" value="laurel-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
      
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
 <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    
                  
<p>Located between Washington D.C. and Baltimore, <strong>Laurel Park</strong> race track has hosted 
thoroughbred horse racing since 1911. It is one of the regular hosts of the Maryland Million, as well as the Frank J. De Francis Memorial Dash, Barbara Fritchie Handicap and General George Handicap.<br>
<br />
                      Triple Crown winners Sir Barton, War Admiral, Whirlaway, Secretariat and Affirmed won races at Laurel Park during their Hall of Fame careers.</p>
        
                    <h2>Laurel Park Schedule</h2>
                    <p><strong>Live Racing: </strong>&nbsp;Tuesday to Saturday<br>
                    <strong>Racing Calendar:&nbsp; </strong>January - March<br>
                    <strong>Course type: </strong>Flat<br>
                    <strong>Notable races: </strong>De Francis Memorial Dash, General George Handicap, Barbara Fritchie Handicap, Maryland Million Classic, Preakness Stakes, Laurel Dash, the Laurel Futurity, the Endless Surprise Stakes, the Conniver Stakes, the Horatius Stakes and the Private Terms Stakes.<br>
                    <strong>Main track: </strong>1 1/18 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong> 1344 feet<br>
                    <strong>Turf course:</strong> 7 Furlong</p>
                  
                    <h2>Stakes Races at Laurel Park</h2>
                    <blockquote>
                      <p><strong>Grade 1</strong> <br />
                        Frank J. DeFrancis Memorial Dash Stakes </p>
                      <p><strong>Grade 2</strong><br />
                        Barbara Fritchie Handicap <br />
                        General George Handicap </p>
                      <p><strong>Grade 3</strong><br />
                        Safely Kept Stakes </p>
                      <p><strong>State-bred Showcase Stakes Races:</strong><br />
                        Maryland Million Classic<br />
                        Maryland Million Ladies <br />
                        Maryland Million Turf <br />
                        Maryland Million Sprint Handicap <br />
                        Maryland Million Distaff Handicap <br />
                        Maryland Million Oaks <br />
                        Maryland Million Nursery <br />
                        Maryland Million Lassie <br />
                        Maryland Million Turf Sprint Handicap </p>
                    </blockquote>
                   
       <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
