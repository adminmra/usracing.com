{assign  var="racetrackname" value="Indiana Downs"} 
{assign  var="racetrackpath" value="indiana-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
          
            
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>


                    <p> <strong>Indiana Downs</strong> Race Track is located in Shelbyville, Indiana, which is roughly 15 minutes from Indianapolis. The Indiana Downs Race Track is a $35 million dollar state-of-the-art facility that hosts both Thoroughbred and Harness races. </p>
                    <p>Indiana Downs Race Track is owned by Oliver Racing, LLC. 
                      
                      Horse racing fans of all ages and skill levels can take advantage of three levels of racing, dining, and entertainment at the Indiana Downs Racetrack. </p>
                   <h2>Indiana Downs Race Track Information:</h2>
                    <p><strong>Live Racing:</strong> Monday, Tuesday, Wednesday, Friday &amp; Saturday<br />
                    <strong>Racing Calendar:</strong> August through October <br>
                   <strong>Course Type:</strong> Thoroughbred &amp; Quarter Horse Racing<br />
                    <strong>Notable Races:</strong>Indiana Derby - Grade II, Indiana Oaks - Grade II</p>
                    
                    <h2>Stakes Races at Indiana Downs</h2>
                    <blockquote>
                      <p>Merrillville Stakes<br />
                        Don K Memorial Starter Handicap Series<br />
                        Brickyard Stakes<br />
                        Crown Ambassador Stakes<br />
                        Indiana Stallion Stakes<br />
                        Michael G. Schaefer Memorial Stakes<br />
                        Indiana Derby - Grade II<br />
                        Mari Hulman George Stakes<br />
                        Indiana Oaks - Grade II<br />
                        Hillsdale Stakes<br />
                        Indiana Futurity<br />
                        Miss Indiana Stakes<br />
                        Richmond Stakes<br />
                        Gus Grissom Stakes<br />
                        Hoosier Breeders Sophomore Stakes</p>
                    </blockquote>
     
            
        
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
