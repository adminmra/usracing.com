{assign  var="racetrackname" value="Portland Meadows"} 
{assign  var="racetrackpath" value="portland-meadows"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Portland Meadows opened on September 14, 1946. It's located in Portland, Oregon, the 28th most populated city in the United States. It began its first summer meet  in July of 2012. The track hosts both American Quarter Horse racing and Thoroughbred horse races. </p>
                   
                    <h2>Portland Meadows Track Schedule</h2>
                    <p><strong>Live Racing Dates:</strong> July through January<br>
                    <strong>Post Times</strong>: July to Septmeber 1:45 p.m.; October to January 12:00 p.m.<br>
                    <strong>Track Description: </strong>One mile oval; distance from last turn to the finish line is 990 feet. Track has a 6 furlong chute and a 1 1/4 mile chute used for quarter horse racing.</p>
                 
                    <h2>Stakes Races at Portland Meadows</h2>
                    <blockquote>
                      <p><strong>Thoroughbred</strong><br>
                      Oregon H.B.P.A. Sprint <br />
                        Catalina Harbor Stakes<br />
                        Jimbos Fire Ant Stakes<br />
                        Tom Two Stakes <br />
                        Silver Patrona Stakes<br />
                        Portland Mile <br />
                        Stallion Stakes<br />
                        Bill Wineberg Stake<br />
                        Janet Wineberg Stakes<br />
                        Oregon Hers Stakes <br />
                        Oregon His Stakes<br />
                        Lethal Grande Oregon Sprint Championship<br />
                        Oregon Derby <br />
                      </p>
                      <p><strong>Quarter Horse</strong><br>
                      Directors Handicap<br />
                        Labor Day Handicap<br />
                        Far West Futrurity Final<br />
                        Autumn Handicap<br />
                        Baxter Andruss Futurity Final<br />
                        The Memorial<br />
                        New Years Handicap</p>
 
                    </blockquote>
                    
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->