{assign  var="racetrackname" value="Colonial Downs"} 
{assign  var="racetrackpath" value="colonial-downs"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/racetracks.tpl'}


<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">
                                       
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

<p><strong>Colonial Downs</strong>, located on New Kent County, Virginia. It is a racetrack for  thoroughbred racing, the most famous race being the <strong>Virginia Derby</strong></a> held annually. It officially opened in 1997. </p>

          <h2><strong>Colonial Downs Course Information:</strong>
        </h2>
<p>
          <strong>Live Racing Calendar:</strong> Wednesday through Sunday &amp; live racing every weekend from August 15 to Sepetember 7.<br>
          <strong>Course type:</strong> Flat<br>
          <strong>Notable Races:</strong> Virginia Derby<br>
</p>
          
          
          
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->