{assign  var="racetrackname" value="Finger Lakes"} 
{assign  var="racetrackpath" value="finger-lakes"}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p>Located in  Western New York, Finger Lakes Race Course is 20 miles SE of Rochester, NY. Racing began at FingerLakes on May 23,1962 and has approximately 160 race days each season which includes the notable New York Derby.<br>
                            <br>
                      The New York Derby is the second leg of the Big Apple Triple for New York-bred 3-year-olds, which starts with the Mike Lee Handicap at <a href="/belmont-park">Belmont Park</a> in late June, and closes with the Albany Handicap at <a href="/saratoga">Saratoga</a> in mid-August.</p>
                
                                      <h2>
         Finger Lakes Race Course Information:
                                                                </h2>
                    <p><strong>Live Racing:</strong> Sunday to Thursday<br>
                    <strong>Racing Calendar:</strong> April to December<br>
                    <strong>Course Type:</strong> Thoroughbred<br>
                    <strong>Notable Races:</strong>&nbsp; Mt. Rainier Breeders' Cup, Washington Breeders' Cup Oaks, Emerald Breeders' Cup Derby and the Gottstein Futurity<br>
                    <strong>Main Track:</strong>1 mile oval; sandy loam; 6 furlong and 1 1/4 mile chutes<br>
                    <strong>Distance from last turn to finish line:</strong> 960 feet</p>
         
                    <h2>Stakes Races at Finger Lakes</h2>
                    <blockquote>
                      <p>George W. Barker Handicap - NY Bred 3-year olds &amp; up<br />
                        Susan B. Anthony Handicap - NY Bred 3-year olds &amp; up<br />
                        Ontario County Stakes - NY Bred 3-year olds<br />
                        Niagara Stakes - NY Bred 3-year old fillies <br />
                        New York Derby - NY Bred 3-year olds<br />
                        Arctic Queen Handicap - NY Bred 3-year olds<br />
                        Genesee Valley Breeders' Handicap - NY Bred 3-year olds<br />
                        Leon Reed Memorial Handicap - NY Bred 3-year olds<br />
                        New York Oaks - NY Bred 3-year old fillies<br />
                        Lady Finger Stakes - NY Bred 2-year old fillies<br />
                        Aspirant Stakes - NY Bred 2-year old colts<br />
                        Jack Betta Be Rite Handicap - NY Bred 3-year olds<br />
                        New York Breeders' Futurity - NY Bred 2-year olds<br />
                        Tin Cup Chalice - NY Bred 2-year old colts<br />
                        Shesastonecoldfox - NY Bred 2-year old<br>
                      </p>
                    </blockquote>
     
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
