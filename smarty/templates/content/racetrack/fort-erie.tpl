{assign  var="racetrackname" value="Fort Erie"} 
{assign  var="racetrackpath" value="fort-erie"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

                    <p>Fort Erie Race Track is home to the Prince of Whales Stakes, the second jewel in the Canadian Triple Crown. Every year, crowds come to the &quot;Fort&quot;  to watch Canada's best horses take the stage and compete for one of the country's most sought after prizes in horse racing.</p>
                    <p>                      Fort Erie got its beginnings in 1897 and now has approximately 80 days of live Thoroughbred and Quarter Horse racing. With its proximity to the USA, it's also referred to as the &quot;border oval.&quot;</p>

                    <h2>Cup and Stakes Races at Fort Erie Race Track</h2>
                    <blockquote>
                      <p>                      The Rainbow Connection Stakes<br />
                        The Prince of Wales Stakes<br />
                        The Andy Passero Memorial Cup<br />
                        The Molson Cup<br />
                        The Bob Summers Memorial cup<br />
                        The Puss 'N Boots Cup</p>
                    </blockquote>
                <h2>Fort Erie Race Course Information:</h2>
                    <blockquote>
                      <p><strong>Racing Dates:</strong>
                        May through October.  
                        Sundays, Mondays and Tuesdays and selected Wednesdays and Saturdays in October<br>
                      <strong>Main Track:</strong>             One mile, sandy loam soil<br />
                        <strong>Distance from last turn to finish: </strong> 1,060 feet<br />
                        <strong>Width of track: 75 feet</strong><br>
                      <strong>Turf Course:</strong> Seven furlongs, inside main course<br />
                       <strong> Distance from last turn to finish:</strong> 930 feet</p>
                    </blockquote>
                    <p><br />
                    </p>
            
        
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
