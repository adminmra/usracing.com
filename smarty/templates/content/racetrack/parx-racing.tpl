{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/parx-racing.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
        .kd {
            background: transparent url(/img/racetracks/parx-racing/parx-racing-horse-betting-racetrack.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/parx-racing/parx-racing-racetrack-horse-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Parx Racing Horse Betting</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Parx Racing Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <p>Get your Parx Racing Horse Betting action with <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Parx Racing Major Races"
        summary="Major races for Parx Racing. Only available at BUSR.">
  <h2>Parx Racing Horse Betting: Track Bio</h2>
 <div class="racetrack-text"> <p><strong>Live Racing:</strong> Saturday to Tuesday<br>
  <strong>Live Racing Calendar:</strong> January to December<br>
 <strong>Course type:</strong> Flat/Thoroughbred<br>
  <strong>Main track:</strong> (sand, clay and loam): 1 mile oval<br>
  <strong>Distance from last turn to finish line:</strong> 974 feet<br>
<strong>Turf course:</strong>7 furlongs</p></div>

			  <h2> Parx Racing Live - Major Races</h2>
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Parx Racing Major Races"
        summary="Major races for Parx Racing. Only available at BUSR.">
        <caption>Parx Racing Picks - Major races - Grade 1</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pennsylvania Derby </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1.125 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			  <tr>
               <td data-title="Month">Set-Oct</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Cotillion Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		    <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Parx Racing Major Races"
        summary="Major races for Parx Racing. Only available at BUSR.">
        <caption>Parx Racing Picks - Major races - Grade 2</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Gallant Bob Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 6 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Parx Racing Off Track Betting</h2>
		  <p>Built in 1974, once known as Keystone Racetrack and located in Bensalem, Pennsylvania. Parx Racing it´s one of the most important venues for horse racing in the state. It features two major racing events, The Cotillion Stakes and The Pennsylvania Derby.</p>
		  <br>
		    <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		          {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}