{assign  var="racetrackname" value="Meydan Racecourse"} 

{assign  var="racetrackpath" value="meydan-racecourse"} 

{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">



<!-- ---------------------- left menu contents ---------------------- -->



        

          

          

                  {include file='menus/racetracks.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         

</div>



<!--------- NEW HERO  ---------------------------------------->



	<div class="newHero" style="background-image: url(/img/dubai-world-cup/Lg_hero_image_Dubai_meydan_racecourse_USR.jpg);" alt="Meydan Racecourse Betting">

	  <div class="text text-xl">Meydan Racecourse</div>

	  <div class="text text-xl" style="margin-top:0px;"></div>

	  <div class="text text-md">Home of the Dubai World Cup!<br>

	    <a href="/signup?ref=Meydan-Racecourse">

	    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>Sign Up Now</div>

	    </a> </div>

	</div>

</div>  

<!------------------------------------------------------------------>    

      

      

      

      <div id="main" class="container">

<div class="row">





<div id="left-col" class="col-md-9">



<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>







<div class="content">

<!-- --------------------- content starts here ---------------------- -->



<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p>
The Meydan Racecourse is located in Dubai, United Arab Emirates, and hosts the largest international horse race of the year, the Dubai World Cup. Held the last Saturday of March each year, the price purse is one of the richest in the world. Starting back in 1996, some of the famous horses have come to the Meydan Racecourse and left victorious such as Cigar, California Chrome and Arrogate. Who do you think will come out on top this year?<br>

  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    
  {*<br>   



<h2>{$racetrackname} Racing</h2>

                    <p><strong>Hialeah Park</strong> Race Track has had quite a history of events. It originally opened in 1921 but it wasn't until January 25, 1925 that The Miami Jockey Club launched Hialeah's race track at Hialeah Park. It was closed down after a hurricane hit it and officially opened on January 14, 1932 after the reconstruction. On March 5, 1979, it was added to the U.S. National Register of Historic Places. The Hialeah Park Race Track covers 40 square blocks of central-east side Hialeah from Palm Avenue east to East 4th Avenue, and from East 22nd Street on the south to East 32nd Street on the north.</p>

                    <p>In 2001, Hialeah Park stopped hosting racing after a change in the state law kept it from having exclusive dates in its competition with <a href="/gulfstream-park">Gulfstream Park</a> and <a href="/calder-race-course">Calder</a> Race Course. Consequently, owner John Brunetti closed Hialeah Park to the public.</p>

                    <p>In 2004, the Florida Division of Pari-Mutuel Wagering revoked Hialeah's thoroughbred permit because it did not hold races for the previous two years. The historic racetrack re-opened on November 28, 2009 but only for quarter horse races.</p>

                    <h2>Hialeah Park Race Track Information:</h2>

                    <p>

	                <strong>Live Racing Calendar:&nbsp;</strong>&nbsp;Friday, Saturday and Sunday<br>

                    <strong> Live Racing: </strong>November to February<br>

                    <strong>Course type:</strong> &nbsp;Quarter Horse races

                    </p>

                    <h2>Stakes Races at Hialeah Park</h2>

                    <blockquote>

                      <p>Orange Blossom Stakes<br />

                        Mockingbird Stakes<br />

                        Bienvenido de Nuevo Stakes<br />

                        Crystal River Stakes<br />

                        Signature Stakes<br />

                        Hialeah Sophomore Invitational<br />

                        Hialeah Derby<br />

                        Moonstone Stakes<br />

                        City of Hialeah Stakes<br />

                        Biscayne Stakes<br />

                        Key West Stakes<br />

                        Sunshine State Derby Trials<br />

                        Sawgrass Stakes<br />

                        Beautiful Prairie Stakes<br />

                        Sparkling Tip Stakes<br />

                        South Florida Quarter Horse Invitational<br />

                        Sunshine State Derby<br />

                        Miami-Dade County Stakes<br />

                        Sailfish Stakes<br />

                        South Florida Quarter Horse Derby

                        <br>
                        *}
                      </p>

                    </blockquote>



        



       

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         



<!-- ------------------------ content ends -------------------------- -->

</div> <!-- end/ content -->

</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.meydan.tpl'}

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 



 

</div><!-- end/row -->

</div><!-- /#container -->

