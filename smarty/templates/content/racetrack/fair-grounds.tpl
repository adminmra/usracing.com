{assign  var="racetrackname" value="Fair Grounds"} 
{assign  var="racetrackpath" value="fair-grounds"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   


<h2>{$racetrackname} Racing</h2>
<p><strong>Address: </strong>1751 Gentilly Blvd, New Orleans, LA 70119, United States</p>
                    <p>&nbsp;</p>
                    <p>The <strong>Fair Grounds</strong> is located in historic Mid-City and has been a New Orleans staple since 1842. It is often referred to as New Orleans Fair Grounds and is ranked #12, behind <a href="/evangeline-downs">Evangeline Downs</a> in Opelousas, Louisiana, which was ranked #6. The racetrack is operated by Churchill Downs Louisiana Horseracing Company, LLC.</p>
                    <p>Fair Grounds is the home of the Louisiana Derby, a Grade II stakes prep for the Kentucky Derby. The Louisiana Derby carries a $1 million purse which was the first seven-figure race to ever run in New Orleans. Jockey, Rosie Napravnik, become the first woman to win the leading jockey title at Fair Grounds with 110 wins, including the Louisiana Derby aboard Pants On Fire. <br />
                    </p>
                    <p>Fair Grounds is the 3rd oldest thoroughbred racetrack in the United States, behind <a href="/saratoga">Saratoga</a> and <a href="pimlico">Pimlico</a>. The track was initially opened as the &quot;Union Race Course&quot; in 1852 and only 7 years later, in 1859, the track was renamed the &quot;Creole Race Course.&quot; In 1863, the name was changed back again to the &quot;Fair Grounds&quot; and racing continued even during the Civil War. In 1981 a turf course was installed.</p>
                                        <h2>
                       Fair Grounds Race Course Information:</h2>
                    <p>
                        <strong>Live Racing:</strong> Wednesday through Sunday<br />
                   <strong>Race Dates:</strong> November to April<br />
                    <strong>Notable Races:</strong>Louisiana Derby (G2), New Orleans Handicap (G2), Mervin H. Muniz Jr. Mml Handicap (G2), Fair Grounds Oaks (G2)<br />
                 <strong>Main Track:</strong> 1 mile <br />
                    <strong>Turf Course:</strong> 7 Furlongs, oval</p>
                   
                    <h2>Stakes Races at Fair Grounds Race Course</h2>
                    <blockquote>
                      <p><strong>Grade II:</strong><br>
	                      Mervin H. Muniz Jr. Memorial Handicap<br />
                        New Orleans Handicap<br />
                        Fair Grounds Oaks<br />
                        Louisiana Derby</p>
                      <p>
                        <strong>Grade III:</strong><br>
                      Colonel E.R. Bradley Handicap<br />
                        Lecomte Stakes<br />
                        Fair Grounds Handicap<br />
                        Mineshaft Handicap<br />
                        Rachel Alexandra Stakes<br />
                        Risen Star Stakes</p>
                    </blockquote>
                    
     
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
