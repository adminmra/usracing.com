{assign  var="racetrackname" value="Belmont Park"} 
{assign  var="racetrackpath" value="belmont-park"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
 <p>{include file='/home/ah/allhorse/public_html/racetracks/belmont_park/slider.tpl'} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>


<p>Belmont Park is a major thoroughbred horse-racing facility located in Elmont, New York, on Long Island. The track opened its doors on May 4, 1905. It all started in 1902, when a syndicate headed by August Belmont II and the former Secretary of the Navy, William C. Whitney, sought land on Long Island to build the most elaborate track in America, one modeled after the great race courses of Europe. In its first 15 or so years, Belmont Park featured clockwise racing which followed the &quot;English fashion&quot;. This allowed the upper-class members of the racing association and their guests to have the races finish in front of the clubhouse, just to the west of the grandstand.</p>
                    <p>Today, Belmont Park is home to one of horse racing's most celebrated races, the <a href="/belmont-stakes">Belmont Stakes</a>, the final jewel of racing&rsquo;s <a href="/triple-crown">Triple Crown</a>. The track attendance record was made in 2004, when 120,139 people attended on June 5. The biggest one-day handle was $14,658,559 on Breeders&rsquo; Cup Day on October 29, 2005.</p>
                    <p>Belmont was been home to the  <a href="/breeders-cup/betting">Breeders' Cup</a> championship in 1990, 1995, 2001 and 2005 when it broke the daily betting record.<br>
                      <br>
                      Belmont is known as the &quot;Test of the Champions&quot; because almost every champion in racing history has competed on its racecourse, including every one of the 12 Triple Crown winners.</p>
               
                <h2>Belmont Park Racing Schedule</h2><p>
                     <strong>Live Racing:</strong> Tuesday, Friday, Saturday and Sunday</p>
                    <p><strong>Racing Dates:</strong> Spring/Summer Meet -&nbsp; April to July &amp; Fall Championship Meet - September to October</p>
                    <p><strong>Course type:</strong> Flat/Thoroughbred                        </p>
                    <p><strong>Notable Races:</strong>&nbsp;&nbsp; <a href="/belmont-stakes">Belmont Stakes</a>, Jockey Club Gold Cup, Manhattan Handicap, Metropolitan Handicap, Champagne Stakes, Suburban Handicap, Vosburgh Stakes, Beldame Stakes, Acorn Stakes, Mother Goose Stakes, Coaching Club American Oaks, Man O' War Stakes, Joe Hirsch Turf Classic Invitational, Ruffian Handicap, Frizette Stakes<strong><br>
                      </strong></p>
                    <p><strong>Main track:</strong> 1 1/2 miles, oval<br>
                          </p>
                    <p><strong>Distance from last turn to finish line:</strong> 1,097 feet<br>
                          </p>
                    <p><strong>Turf course:</strong> 12 furlongs</p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->