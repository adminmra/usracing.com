{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                   {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Also known as Bet Fair <strong>Hollywood Park</strong>, the Race Track is located in Inglewood and is about 3 miles from LAX. It has hosted racing events since it opened in 1938, including notable races such as the annual Hollywood Gold Cup, American Oaks Invitational and the Hollywood Derby. The track&nbsp; was also the location for the Breeders' Cup in 1984, 1987 and 1997.<br>
                            <br>
                          The legendary Seabiscuit won the inaugural running of the Hollywood Gold Cup, the track's signature race. This same event was won 11 times by other "Horse of the Year" winners.</p>
                    <p>Starting in 2014, racing will be moved to  <a href="/santa-anita-park">Santa Anita Park</a> and <a href="/delmar">Del Mar</a> Racetrack. In May of 2013, it was announced that Hollywood Park would be closing at the end of their fall racing season in 2013.  Hollywood Park president, F. Jack Liebau stated that the 260 acres on which the track sits &quot;now simply has a higher and better use,&quot; and that &quot;in the absence of a favorable change in racing's business model, the ultimate development of the Hollywood property was inevitable.&quot;</p>
                    
	                <h2>Hollywood Park Track Information:</h2>
                    <p>(Through December 2013)<br>
                    <strong>Live Racing:&nbsp;</strong>Thursday to Sunday<br>
                    <strong>Racing Calendar:&nbsp; </strong>Spring/Summer Meet: April  through July &amp;  Autumn Meet: November  through December<br>
                    <strong>Course type: </strong>&nbsp;Flat/Thoroughbred<br>
                    <strong>Notable races:&nbsp; </strong> Hollywood Gold Cup, American Oaks Invitational, Hollywood Derby, Matriarch Stakes, Citation Handicap, Vanity Handicap<strong>Main track: </strong>1 1/8 mile, oval<br>
                    <strong>Distance from last turn to finish line:</strong> 991 feet<br>
	                <strong>Turf course:</strong> 1 mile, 145 feet</p>
                   
                    <h2>Stakes Races at Hollywood Park</h2>
                    <blockquote>
                      <p><strong>Grade 1 :</strong><br />
                        American Oaks<br />
                        CashCall Futurity<br />
                        Citation Handicap<br />
                        Gamely Stakes<br />
                        Hollywood Derby<br />
                        Hollywood Gold Cup<br />
                        Hollywood Starlet Stakes<br />
                        Hollywood Turf Cup Stakes<br />
                        Matriarch Stakes<br />
                        Shoemaker Mile Stakes<br />
                        Triple Bend Invitational Handicap<br />
                        Vanity Handicap</p>
                      <p><strong>Grade 2 :</strong><br />
                        A Gleam Invitational Handicap<br />
                        American Handicap<br />
                        Bayakoa Handicap<br />
                        Beverly Hills Handicap<br />
                        Californian Stakes<br />
                        CashCall Mile Invitational Stakes<br />
                        Charles Whittingham Memorial Handicap<br />
                        Dahlia Handicap<br />
                        Hollywood Breeders' Cup Oaks<br />
                        Honeymoon Breeders' Cup Handicap<br />
                        Lazaro Barrera Memorial Stakes<br />
                        Marjorie L. Everett Handicap<br />
                        Mervyn Leroy Handicap<br />
                        Milady Breeders' Cup Handicap<br />
                        Winter Stakes<br />
                        Sunset Handicap<br />
                        Swaps Stakes</p>
                      <p><strong>Grade 3 :</strong><br />
                        Affirmed Handicap<br />
                        Ack Ack Handicap<br />
                        Cinema Handicap<br />
                        Generous Stakes<br />
                        Hawthorne Handicap<br />
                        Hollywood Juvenile Championship Stakes<br />
                        Hollywood Prevue Stakes<br />
                        Hollywood Turf Express Handicap<br />
                        Inglewood Handicap<br />
                        Jim Murray Memorial Handicap<br />
                        Los Angeles Handicap<br />
                        Miesque Stakes<br />
                        Native Diver Handicap<br />
                        Railbird Stakes<br />
                        Senorita Stakes<br />
                        Vernon O. Underwood Stakes<br />
                        Will Rogers Stakes<br />
                        Wilshire Handicap<br />
                      </p>
                    </blockquote>
                 
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
