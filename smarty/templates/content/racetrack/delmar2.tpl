{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/tracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Del Mar Racetrack</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div style="margin-top: 20px; width: 302px; height: 250px; float: right;">
<div><img style="border: 1px solid #1B579F; margin: 0px;" src="/themes/images/racetracks/delmar_grandstand_back.jpg" alt="Del Mar Racetrack" width="300" height="201" /></div>
</div>
<p>&nbsp;</p>

<p>"Where the Turf Meets the Surf<strong>"</strong></p>
<p>Via De La Valle &amp; Jimmy Durante Blvd.<br /> Del Mar, California 92014<br /> (858) 755-1141<br /> <a title="http://www.dmtc.com " href="http://www.dmtc.com " target="_self" rel="nofollow">http://www.dmtc.com</a></p>
<p>&nbsp;</p>

<p><strong> </strong></p>
<p><strong>Track Facts:</strong></p>
<p><strong>Main Track:</strong> One Mile, oval.<br /> <strong>Turf Course:</strong> Seven eighth's Mile.<br /> <strong>Distance from last turn to finish line:</strong> 919 feet.</p>
<p>&nbsp;</p>
<p><strong>About:</strong></p>
<p>The annual beach party known as the Del Mar Thoroughbred racing season parades to the post beginning Wednesday, July 21, 2010. The 43 days of racing extends through Wednesday, September 8, with a six-day-a-week schedule, Tuesday being each week's day of rest.</p>
<p>Del Mar is unique in that its facilities are owned by the State of California and controlled by the 22nd District Agricultural Association, operators of the Southern California Exposition, which annually attracts more than a million visitors from mid-June through the 4th of July holiday to what is commonly called the “Del Mar Fair.”</p>
<p>The track's Spanish-style architecture and laid-back attitude make Del Mar the annual setting for one of America's foremost salutes to the sport, where horses, trainers, jockeys and owners come together for racing beside the sea. The track sits a couple of furlongs from the shimmering blue Pacific ("where the turf meets the surf," get it?), about 20 miles north of San Diego and 100 miles south of Los Angeles.</p>
<p>Del Mar has been a national leader in terms of daily average attendance<strong> </strong>for the past two decades. In 2009 the on-track attendance was 17,181 per day, totaling 635,679 fans during the 37-day summer season.</p>
<p>So what makes the racing season at Del Mar different from say, Hollywood Park or Santa Anita up north LA way? Well, for one, it's not in LA (have you ever driven to Hollywood Park? Scary area, that Inglewood is). But besides the typical different stakes activity and trainers, stables and jockeys it attracts, Del Mar is just plain cool.</p>
<p>And it all begins with the track's beginning back in 1937 when crooner Bing Crosby and some of his Hollywood buddies such as Pat O'Brien and Jimmy Durante got together and found a spot in a sleepy town called Del Mar to start a race track. Now, you gotta either love horses or love gambling to do something like this. Most would say it's foolish, but the genius lies in selecting Del Mar as the track's site. You see, a seaside track is just too cool...especially in the middle of summer!</p>
<p>Then, in 1938, the great legendary horse, Seabiscuit, came to town in an epic race against Ligaroti, where Seabiscuit, with George "Iceman" Woolf riding, won by a nose. How cool is that? Given that Seabiscuit was immortalized in the acclaimed book by Laura Hillenbrand and the much-anticipated movie based on the book is being released this summer, well, it's very cool to know Del Mar had a chapter in the legendary horse's life.</p>
<p>Through the years, the track ebbed and flowed with the fortunes of the sport. Some of the greats names in horse racing have graced its turf: Pincay, Shoemaker, Wittingham, Longden, Delahoussaye, Lukas, Baffert, McCarron, Cigar, Mabee, Best Pal. The list goes on...</p>
<p>In 1993, a major renovation and expansion made Del Mar an even better place to bet on the ponies. Whether it's rubbing elbows with the well-heeled in the Turf Club, or with the sun lovers in their beach chairs down trackside, Del Mar brings a laid-back, fun-loving attitude to horse racing. You might say, San Diego has put it's stamp on horse racing...and losing money was never so much fun!</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Del Mar Concert Series</strong></p>
<p>In 1994 Del Mar brought the funk to Fridays with free concerts. The Free Friday Concert Series has seen the likes of Cake, Billy Idol, Devo, Gnarls Barkley, Ziggy Marley, Violent Femmes and Jason Mraz. One hundred and seventeen concerts later, brings us to <strong>Del Mar’s  Summer Concert Series</strong>.&nbsp;Four o'clock Friday Concerts are held at the Seaside Stage next to the Seaside Terrace Cabana.</p>
<p>&nbsp;</p>

        
        

  </div>
              
            
            
            

  

</div><!-- /block-inner, /block -->



          
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-tracks.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
