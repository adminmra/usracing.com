{assign  var="racetrackname" value="Pleasanton"} 
{assign  var="racetrackpath" value="pleasanton"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Built in 1858, Pleasanton is located at the Alameda County Fairgrounds in California. Pleasanton Fairgrounds Racetrack is the oldest horse racing track of its kind in the United States. The one-mile horse track features thoroughbred and quarter horse racing between June and July.</p>
                   {*
 <p></p>
                    <p><strong>California State Fair Circuit</strong><br />
                    </p>
                    <ul>
                      <li><a href="/pleasanton">Pleasanton</a><br />
                        </li>
                      <li><a href="/sacramento">Sacramento</a><br />
                        </li>
                      <li><a href="/ferndale">Ferndale</a><br />
                        </li>
                      <li><a href="/stockton">Stockton</a><br />
                        </li>
                      <li><a href="/fresno">Fresno</a><br />
                      </li>
                    </ul>
*}
                  <h2> Pleasanton Racetrack Schedule</h2>
                    <p><br>
                    <strong>Live Racing: </strong>Selected days between June and July<strong><br>
                          </strong></p>
                    <p><strong>Racing Calendar:&nbsp; </strong>June  - July<strong><br>
                          </strong></p>
                    <p><strong>Course type: </strong>Thoroughbred, Quarter horse and Arabian<br>
                            </p>
                    <p><strong>Notable Races: </strong>California Wine Stakes, Livermore Valley Wine Stakes, Alameda County Fillies &amp; Mares, Alamedan Handicap, Sam J. Whiting Memorial Handicap, Juan Gonzalez Memorial Stakes, Everett Nevin Alameda County Overnight Stakes<strong><br>
                              </strong></p>
                    <p><strong>Main track:</strong>&nbsp; One &nbsp;mile, oval<br>
                            </p>
                    <p><strong>Distance from last turn to finish line:</strong> 1,085 feet<br>
                            </p>
                    <p><strong>Turf course:</strong> 6 furlongs </p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Pleasanton</h2>
                    <blockquote>
                      <p>California Wine Stakes<br />
                        Livermore Valley Wine Stakes<br />
                        County of Alameda Handicap<br />
                        Casual Lies Handicap<br />
                        Juan Gonzalez Memorial Overnight Stakes<br />
                        Everett Nevin Stakes</p>
                    </blockquote>
         <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
