{assign  var="racetrackname" value="Harrahs Philadelphia"} 
{assign  var="racetrackpath" value="harrahs-philadelphia"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

 <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    
                    <p><strong>Harrah's Philadelphia</strong> held its first harness racing season opened on September 10, 2006. It's located in Chester, Pennsylvania, just south of the Philadelphia International Airport and situated in the tri-state area of Pennsylvania, New Jersey and Delaware. It is a very short ride from nearby Maryland as well. </p>
                    <p>The track is the  first-of-its-kind in North America, boasting a &quot;miracle turn&quot; that actually extends over the Delaware River due to a specially constructed bridge. The casino claims to have the largest collection of table games in the Pennsylvania/Delaware area, including blackjack, craps, roulette and baccarat.</p>
                                       <p>&nbsp;</p>
                    <h2>Harrah's  Racetrack Information</h2>
                    <blockquote>
                      <strong> Live Racing: </strong>Sunday, Wednesday, Thursday &amp; some Fridays<br>
                      <strong>Racing Calendar:</strong> March to December with a 12:40 pm post time for most race cards<br />
                      <strong>Course type: </strong>Harness Racing<br />
                      <strong>Main track: </strong>5/8 of a mile (limestone-based surface)
                      </p>
                      </blockquote>
                    
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
