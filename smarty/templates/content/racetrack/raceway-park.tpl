{assign  var="racetrackname" value="Raceway Park"} 
{assign  var="racetrackpath" value="raceway-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                      When <strong>Raceway Park</strong> opened in 1959, it originally hosted car racing and thoroughbred racing.  The first harness race was run in 1962. In 2005, the track was purchased by Penn National Gaming, Inc.</p>
                    <p>Raceway Park Racetrack offers weekend live harness racing  and hosts multiple legs of the Ohio Sires Stakes and the Champion of Champions event, which showcases the best of the Northwest Ohio Fair Circuits.</p>
                                       <h2>Raceway Park Track Schedule</h2>
                    <p><strong>Racing Dates:</strong> April to September<br />
                     <strong>Post Time</strong>: 6:00 pm<br />
                      <strong>Live Racing Schedule:</strong> Saturday and Sunday<br>
                      
                    <strong>Notable Races:</strong> Ohio Sire Stakes and Buckeye-Wolverine Pace</p>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->