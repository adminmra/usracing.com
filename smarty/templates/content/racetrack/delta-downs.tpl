{assign  var="racetrackname" value="Delta Downs"} 
{assign  var="racetrackpath" value="delta-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p><a href="/signup"><img class="img-responsive" src="/img/racetracks/delta-downs/delta-downs-horse-racing.jpg" alt="Delta Towns Horse Racing">  </a></p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   




<h2>{$racetrackname} Racing</h2>
<p><strong>Address:</strong> 2717 Delta Downs Dr, Vinton, LA 70668, United States</p>
                    <p>
                    </p>
                    <p>Delta Downs is an American thoroughbred and quarter horse racetrack in Calcasieu Parish, near Vinton, Louisiana in the southwest portion of the state. The track opened in September, 1973.</p>
                    <p>The track usually holds races from November to mid-July, with the Thoroughbred meet beginning in November and the Quarter Horse meet commencing in April.</p>
                
          
                  
                    <h2><strong>Delta Downs Racetrack Information:</strong></h2>
                    <p><strong>Main track dimensions: </strong>6 furlong oval with two chutes--a 550-yard (QH) 1 1/16-mile (TB) chute and a 5-furlong chute
                     <br><strong>Track Surface:</strong>  a mixture of sand (from the Sabine river) and clay with a limestone base
                     <br><strong>Length of stretch:</strong> 660 feet
                      <br><strong>Width of stretch:</strong> 80 feet
                      <br><strong>Width of backstretch and turns:</strong> 70 feet
                     <br><strong>Track type</strong>: banked with a 5% rise down the straight-a-way and 10% incline on turns
                      </p>
                    <p><strong>Maximum field size</strong>: 10</p>
                
                    <h2>Stakes Races at Delta Downs</h2>
                    <p>Delta Jackpot Stakes for 2-year-olds (Grade III)<br>
                      Delta Princess Stakes for 2-year-old fillies (Grade III)</p>
            
             
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
