<?php
date_default_timezone_set('America/New_York');
$cdate = date("Y-m-d H:i:s");
?>
<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Table",
        "name": "Gulfstream Park",
        "keywords": "Gulfstream Park betting, Gulfstream Park betting odds, Gulfstream Park betting lines, Gulfstream Park races, Gulfstream Park 2020, Gulfstream Park race course, Gulfstream Park results, Gulfstream Park odds"
    }
</script>
<style>
    @media (max-width: 800px) {
        caption {
            display: none;
        }
        .none{
            display:none !important;
        }
    }
</style>
<div id="no-more-tables">
          <table class="oddstable"  width="100%" cellpadding="0" cellspacing="0" border="0"
            summary="Only available at BUSR.">
			  
        <table id="o0t" width="100%" cellpadding="0" cellspacing="0"
                    class="data table table-condensed table-striped table-bordered">
                    <tr class="none">
                <caption>Gulfstream Park Racing Schedule</caption>
                <th> <center>Race</center> </th>
                <th> <center>Grade</center> </th>
                <th> <center> Date</center> </th>
            </tr>
        <tbody>
            <?php if($cdate < '2020-03-14 23:59:59') { ?>
            <tr>
                <td data-title="Race">Hurricane Bertie S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-III </td>
				<td data-title="Date"> March 14, 2020 </td>
			</tr>
            <?php } ?>
			<?php if($cdate < '2020-03-21 23:59:59') { ?>
			<tr>
                <td data-title="Race">Melody of Colors</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 21, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-21 23:59:00') { ?>
			<tr>
                <td data-title="Race">Texas Glitter S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 21, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-21 23:59:00') { ?>
			<tr>
                <td data-title="Race">Appleton S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-IIIT </td>
				<td data-title="Date"> March 21, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-27 23:59:00') { ?>
			<tr>
                <td data-title="Race">Hal's Hope S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-III </td>
				<td data-title="Date"> March 27, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-27 23:59:00') { ?>
			<tr>
                <td data-title="Race">Sir Shackleton S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 27, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-27 23:59:00') { ?>
			<tr>
                <td data-title="Race">Curlin Florida Derby</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-1 </td>
				<td data-title="Date"> March 27, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Cutler Bay S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 28, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Gulfstream Park Oaks</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-2 </td>
				<td data-title="Date"> March 28, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Kitten's Joy Pan American S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-IIT </td>
				<td data-title="Date"> March 28, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Orchid S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> G-IIIT </td>
				<td data-title="Date"> March 28, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Sand Springs S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 28, 2020 </td>
			</tr>
			<?php } ?>
			<?php if($cdate < '2020-03-28 23:59:00') { ?>
			<tr>
                <td data-title="Race">Sanibel Island S</td>
                <td data-title="Grade" style="word-break: keep-all !important;"> Stakes </td>
				<td data-title="Date"> March 28, 2020 </td>
            </tr>
            <?php } ?>
        </tbody>
        </table>
    </table>
</div>
<style type="text/css">
    table.ordenableResults th {
        cursor: pointer
    }
</style>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<script src="/assets/js/sorttable_pwc_turf_contenders.js"></script>