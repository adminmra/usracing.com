{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/will-rogers-downs.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       .kd {
            background: transparent url(/img/racetracks/will-rogers-downs/will-rogers-downs-racetrack-horse-betting.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/will-rogers-downs/will-rogers-downs-racetrack-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
<section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Will Rogers Downs Off Track Betting Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <p>Bet at Will Rogers Downs Today!</p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
       		  {*Secondary Copy*}
<h2>Will Rogers Downs Horse Racing Schedule</h2>
<div class="racetrack-text">
                   <p><strong>Schedule: </strong>Spring: Monday, Tuesday, Wednesday &amp; Saturday;  Fall: Friday through Sunday<br>
                   <strong>Post Time:</strong> 12:00 p.m.<br>
                   <strong>Track: </strong>One-mile racetrack<br>
                   <strong>Track Events: </strong>Sequoyah Stakes</p></div>           
                                   <h2>Will Rogers Downs Betting Odds</h2>
<div class="racetrack-text"><p>Check our <a href="/odds">Today's Horse Racing Odds</a>section for a list of the most popular upcoming races, along with Will Rogers Downs Horse Racing Odds.</p></div>
<div><p>Bet at Will Rogers Downs Today!</p></div>
{* <h2>Will Rogers Downs Horse Betting Results</h2>
<div class="racetrack-text">
<p>USRacing has all of Will Rogers Downs results once they become official. Bet the races, and verify all the <a href="/results">live results.</a></p></div> *}
{* <h3>Will Rogers Downs Top Trainers</h3>
<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" summary="Will Rogers Downs Top Trainers">
           <thead>
            <tr>
                <th> Trainer</th>
                <th> R-W-P</th>
                <th> Earnings</th>
               </tr>
        </thead>
        <tbody>
<tr><td>Willis, Eddie D</td> <td>157-28-26</td> <td>$442k</td>
</tr><tr><td>Robinson, RL Rick</td><td>45-18-7</td> <td>$186k</td> 
</tr><tr><td>Young, Scott E</td><td>79-17-16</td><td>$213k</td> 
</tr><tr><td>Whitekiller, Matt</td> <td>45-16-8</td> <td>$157k</td> 
</tr><tr><td>Keener, Dee</td> <td>40-12-5</td> <td>$79k</td> </tr>
<tr><td>Hurdle, Michelle</td> <td>35-12-2</td> <td>$138k</td> </tr>
<tr><td>Valdivia, Guillermo</td> <td>56-11-6</td> <td>$116k</td> </tr>
<tr><td>Dixon, Tim</td> <td>58-10-4</td> <td>$90k</td> </tr><tr>
<td>Broberg, Karl</td> <td>48-10-10</td> <td>$131k</td> </tr><tr>
<td>Nolen, Kenneth</td> <td>34-10-5</td> <td>$156k</td> </tr></tbody></table>
<br>
<p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
<h3>Will Rogers Downs Top Jockeys</h3>
<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0"
        summary="Will Rogers Downs Top Jockeys">
        <thead>
            <tr>
                <th> Jockey</th>
                <th> R-W-P</th>
                <th> Earnings</th>
               </tr>
        </thead>
        <tbody>
<td>Klaiber, Justine</td> <td>149-26-25</td> <td>$294k</td> </tr>
<tr><td>Wethey, Jr Floyd</td> <td>109-24-21</td> <td>$340k</td> </tr>
<tr><td>Kimes, Curtis</td> <td>162-23-29</td> <td>$367k</td> </tr>
<tr><td>Delgado, Mario</td> <td>122-22-17</td> <td>$284k</td> </tr><tr>
<td>Landeros, Benny C</td> <td>77-22-8</td> <td>$229k</td> </tr>
<tr><td>Smith, Cody Rodger</td> <td>110-20-14</td> <td>$345k</td> </tr>
<tr><td>Raudales, Rolando</td> <td>109-19-16</td> <td>$188k</td> </tr>
<tr><td>Silva, Agustin</td> <td>102-19-16</td> <td>$269k</td> </tr>
<tr><td>McNeil, Bryan</td> <td>86-19-9</td> <td>$283k</td> </tr><tr>
<td>Samaniego, Andrew R</td> <td>122-18-16</td> <td>$205k</td> </tr></tbody></table>
		  <br>
		    <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
        end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}