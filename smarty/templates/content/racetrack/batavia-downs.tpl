{assign  var="racetrackname" value="Batavia Downs"} 
{assign  var="racetrackpath" value="batavia-downs"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address:</strong> 8315 Park Rd, Batavia, NY 14020, United States</p>
                    <p>&nbsp;</p>
                    <p>Batavia Downs is a harness racing track in Batavia, New York which is located in Genesee County between Buffalo and Rochester, just off of the New York State Thruway. The track first opened on September 20, 1940.</p>
         
                 
                    <h2>Batavia Downs  Racetrack Information</h2>
                    <p><br />
                        <strong>Live Racing: </strong>July to December<br />
                    </p>
                    <p><strong>Live Racing Calendar: </strong>Saturday, Sunday, Monday, Tuesday, Wednesday<br />
                    </p>
                    <p><strong>Course type: </strong>Harness<strong><br />
                      </strong></p>
                    <p><strong>Main track:</strong> 1/2 mile<br />
                    </p>
                    <p><strong>Turf course:</strong> No Turf Course </p>
                    <p>&nbsp;</p>
                    <h2>Batavia Downs Track Records</h2>
                    <p><strong>Pacers</strong><br />
                      All Age Track Record: Aracache Hanover 1:51.1 2011 <br />
                      Aged Mares: Up front Kellie Jo 1:53.4 2012 <br />
                      Two-Year Old Colt: Doctor Butch 1:54.2 2012 <br />
                      Two-Year Old Filly: Paula&rsquo;s Best 1:55.0 2006 <br />
                      Three-Year Old Colt: Joey The Czar 1:52.1 2010 <br />
                      Three-Year Old Filly: New Album 1:52.4 2011 </p>
                    <p> <strong><br />
                      Trotters</strong><br />
                      All Age Track Record: Archangel 1:54.3 2012 <br />
                      Aged Mares: Cowgirl Hall 1:57 2013 <br />
                      Two-Year Old Colt: Dejarmbro 1:58.4 2010 <br />
                      Two-Year Old Filly: Dreamy Dawn 1:59 2010 <br />
                      Three-Year Old Colt: Archangel 1:54.3 2012 <br />
                      Three-Year Old Filly: Cowgirl Hall 1:57.2 2012 </p>
                   
        

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->