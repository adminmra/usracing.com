{assign  var="racetrackname" value="Tampa Bay Downs"} 
{assign  var="racetrackpath" value="tampa-bay-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
 <div class="container-fluid">
	
		<a href="/signup?ref=tampa-bay-downs"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/tampa-bay-downs/2017-Tamapa-Bay-Downs-betting.jpg" alt="Tampa Bay Downs Betting"></a>
		
		<a href="/signup?ref=tampa-bay-downs"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/tampa-bay-downs/2017-Tamapa-Bay-Downs-betting-usracing.jpg" alt="Tampa Bay Downs Betting">  </p> </a>
	
</div>             
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    
                    <p><strong>Tampa Bay Downs </strong>first opened its doors in 1926 and is the only thoroughbred race track on the West Coast of Florida. It hosts two <a href="/kentucky-derby/prep-races">Kentucky Derby prep races</a>, The Sam F. Davis Stakes and The Tampa Bay Derby every year. The first four (4) finishers in these races get qualifying point for entry into the <a href="/kentucky-derby">Kentucky Derby</a>.<br>
                    </p>
                                     <h2>Tampa Bay Track Schedule</h2>
						<strong> Live Racing:</strong> Friday to Sunday</br>                  
                    	 	<strong>Live Racing Calendar:</strong> December &nbsp;to May</br>
                   			 <strong>Course type:</strong> Flat/Thoroughbred<br>
                      
                    <strong>Notable Races:&nbsp;</strong> Tampa Bay Derby (GII), Endeavour Breeders Cup Stakes (GIII), Hillsborough Stakes (GIII3), Sam F. Davis Stakes (GIII)<br>
                     
                    <strong>Main track (dirt):</strong> 1 mile oval<br>
                        
                    <strong>Distance from last turn to finish line:&nbsp;</strong> 1050 feet<br>
                       
                    <strong>Turf course:</strong> 7 furlongs, 1/4 Mile Chute 
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Tampa Bay Downs</h2>
                    <blockquote>
                      <p>The Inaugural Stakes<br />
                        The Sandpiper Stakes<br />
                        The Pelican Stakes<br />
                        The Minaret Stakes<br />
                        The Turf Dash <br />
                        The Pasco Stakes<br />
                        The Gasparilla Stakes<br />
                        The Lightning City Stakes <br />
                        The Sam F. Davis Stakes - Grade III (Kentucky Derby prep race)<br />
                        The Endeavour Stakes - Grade III<br />
                        The Florida Oaks - Grade III<br />
                        The Manatee Stakes<br />
                        The Tampa Bay Stakes - Grade III <br />
                        The Super Stakes<br />
                        The Challenger Stakes<br />
                        The Suncoast Stakes<br />
                        The Tampa Bay Derby - Grade II (Kentucky Derby prep race)<br />
                        The Hillsborough Stakes - Grade III</p>
                    </blockquote>
                  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
