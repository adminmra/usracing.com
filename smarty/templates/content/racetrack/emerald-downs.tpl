{assign  var="racetrackname" value="Emerald Downs"} 
{assign  var="racetrackpath" value="emerald-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p>
                      Emerald Downs is a thoroughbred racetrack in Auburn, Washington, located a half mile east of Highway 167. It is named after Seattle, the Emerald City.</p>
                    <p><strong>Emerald Downs</strong> made its official debut as a replacement of Long Acres Racetrack, which closed in September 1992.<br />
                      <br />
The 167-acre facility, located midway between Seattle and Tacoma, has established as the premier thoroughbred racing venue in the Pacific Northwest. The facility hosts several large ungraded stakes and many overnight handicaps and stakes. There are an average of 8 races on week nights, and 10 races per race weekend.</p>
                    <p>                      Emerald Downs biggest race is the Longacres Mile Handicap, a Grade III event. Wasserman is the track's all-time leading earner, with $560,858 in purses won entering the 2013 racing season. West Seattle Boy is the horse with the most wins in track history with 21 career trips to the winner's circle</p>
                                       <h2>  <strong>Emerald Downs </strong><strong>Race Course <strong>Information:</strong></strong></h2>
                    <p><strong>Live Racing:</strong> Friday through Sunday<br>
                      </p>
                    <p><strong>Racing Calendar:</strong> April through September<strong><br>
                      </strong></p>
                    <p><strong><strong>Course Type:</strong></strong> Thoroughbred<br>
                      </p>
                    <p><strong>Notable Races:</strong>&nbsp; Mt. Rainier Breeders' Cup, Washington Breeders' Cup Oaks, Emerald Breeders Cup Derby and the Gottstein Futurity<strong><br>
                        </strong></p>
                    <p><strong><strong>Main Track:</strong></strong> One mile oval<br>
                      </p>
                    <p><strong>Distance from last turn to finish line:</strong> 990 feet<br>
                      </p>
                    <p><strong>Turf Course:</strong> 7 furlongs
                      
                      &nbsp;</p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Emerald Downs</h2>
                    <blockquote>
                      <p>Hastings Handicap<br />
                        Governor's Handicap<br />
                        Seattle Handicap<br />
                        Auburn Handicap<br />
                        WA State Legislators Stakes<br />
                        Budweiser Handicap<br />
                        Irish Day Handicap<br />
                        Coca-Cola Handicap<br />
                        Boeing Handicap<br />
                        Kent Handicap<br />
                        Seattle Slew Handicap<br />
                        Mt. Rainier Handicap<br />
                        Emerald Express<br />
                        Angie C Stakes<br />
                        Washington Oaks<br />
                        Emerald Downs Derby<br />
                        WTBOA Lads Stakes<br />
                        Emerald Distaff<br />
                        LONGACRES MILE GR III<br />
                        Barbara Shinpoch Stakes<br />
                        Bank of America EmD Championship Chlng<br />
                        Northwest Farm Stakes<br />
                        Daily Racing Form Dennis Dodge Stakes<br />
                        Comcast Sports Net Stakes<br />
                        Jim Beam Stakes<br />
                        Pegasus Training Center Stakes<br />
                        Muckleshoot Tribal Classic<br />
                        Gottstein Futurity<br />
                        NWSS Cahill Road Stakes</p>
                    </blockquote>
      
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
