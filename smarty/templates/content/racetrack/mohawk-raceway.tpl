{assign  var="racetrackname" value="Mohawk Raceway"} 
{assign  var="racetrackpath" value="mohawk-raceway"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                   
                    <p><strong>Mohawk Racetrack</strong> is a harness racing track that opened in 1963, in Campbellville, Ontario. It is owned by the Woodbine Entertainment Group which also owns <a href="/woodbine">Woodbine</a> Racetrack, located in Toronto.</p>
<p>The track hosts harness racing five nights per week in the Spring and Fall. Spring events include the Youthful Pacing Series, the Mohawk Pacing Series, and the Princess Pacing Series. Most of the major harness racing stakes events have been moved from Woodbine to Mohawk as of 2007. <br>
    <br>
  Mohawk  hosted the Breeders Crown several times in the past with the most recent edition being the 2008 older division events. The Breeders Crown is an annual series of Harness races covering each of the sport's twelve traditional categories of age, gait and gender. </p>

  <h2>Mohawk Raceway Schedule</h2>
<p>
<strong>Live Racing: </strong>Monday to Sunday<br>
<strong>Live Racing Calendar:&nbsp; </strong>Spring and Fall<br>
<strong>Course type: </strong>Standardbred harness racing<br>
<strong>Main track:</strong>&nbsp; 7/8 mile, oval<br>
<strong>Distance from last turn to finish line:</strong> 1,095 feet
</p>

<h2>Stakes Races at Mohawk Raceway</h2>
<blockquote>
  <p>The North America Cup<br />
    Metro Pace<br />
    Canadian Trotting Classic<br />
    he's A Great Lady Stakes<br />
    Maple Leaf Trot<br />
    Canadian Pacing Derby<br />
    Fan Hanover Stakes<br />
    Elegantimage Stakes<br />
    Oakville Stakes<br />
    William Wellwood Memorial</p>
</blockquote>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
