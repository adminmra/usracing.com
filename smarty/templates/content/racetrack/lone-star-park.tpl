{assign  var="racetrackname" value="Lone Star Park"} 
{assign  var="racetrackpath" value="lone-star-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Lone Star Park</strong> is located in Grand Prairie, Texas. The track opened in 1997 and offers both Thoroughbred racing and Quarter Horse racing. The track hosted the <a href="/breeders-cup">Breeders' Cup</a> in November of 2004.<br />
                    </p>
                    <h2>Lone Star Race Course Schedule</h2>
                    <p><strong>Live Racing: </strong>&nbsp;Thursday to Sunday<br>
	                    <strong>Racing Calendar:&nbsp; </strong>Thoroughbreds: April - July &amp; Quarter Horses: September - November<br>
		                    <strong>Course type: </strong>Thoroughbred racing and Quarter Horse racing<br>
		                    <strong>Main track: </strong>One mile, oval<br>
			                <strong>Distance from last turn to finish line:</strong> 930 feet<br>
			                <strong>Turf course:</strong> 7/8 mile oval with 11/8 inside chute</p>
                     <h2>Stakes Races at Lone Star Park</h2>
                    <p><strong>Grade III:</strong><br />
                      Lone Star Park Handicap<br />
                      Texas Mile Stakes<br />
                      Lone Star Derby<br />
                      Ouija Board Handicap<br />
                      Dallas Turf Cup<br />
                    </p>
                    <p><strong>Ungraded stakes:</strong><br />
                      Lone Star Oaks <br />
                      TTA Sales Futurity <br />
                      Carter McGregor Jr. Memorial Stakes <br />
                      Nevill/Kyocera Stakes <br />
                      Lone Star Park Turf Sprint Stakes <br />
                      Bob Johnson Memorial Stakes <br />
                      Silver Spur Stakes <br />
                      Middleground Stakes <br />
                      Richland Hills Stakes <br />
                      Grand Prairie Turf Challenge Stakes <br />
                      Irving Distaff Stakes <br />
                      JEH Stallion Station Stakes <br />
                      Premiere Stakes </p>
                    <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
