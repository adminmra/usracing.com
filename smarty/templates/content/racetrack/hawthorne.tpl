{assign  var="racetrackname" value="Hawthorne Race Course"} 
{assign  var="racetrackpath" value="hawthorne"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Hawthorne Race Course</strong> is the oldest continually-run, family-owned, racetrack in North America and has celebrated live thoroughbred racing in Chicago for over 120 years.</p>
                    <p>In 1890, Edward Corrigan, a Chicago businessman who owned the 1890 Kentucky   Derby winner, Riley, bought  land in Cicero and started constructing   a grandstand for a new racecourse. His track opened in 1891 with a five-race   card including the Chicago Derby. In 1970, harness racing was held at Hawthorne in an effort to offer a product to   lovers of standardbred racing.</p>
                    <p>                      As one of Illinois' most prominent horse racing tracks, Hawthorne hosts a big list of relevant racing events, including the annual Hawthorne Gold Cup Handicap and the <a href="/illinois-derby">Illinois Derby</a>.  Hawthorne is now second tier to Arlington Park.</p>
                                      <h2>Hawthorne Race Course Information:</h2>
                    <p>
	                    
	                <strong>Live Racing Calendar:&nbsp;&nbsp;</strong>Hawthorne race track hosts thoroughbred racing in the Spring and Fall and harness racing in the Summer.<br>
                            <strong>Course type:</strong> &nbsp;Flat/Thoroughbred<br>
                            <strong>Notable races: </strong>Hawthorne Gold Cup Handicap - Grade II, Illinois Derby - Grade II, Hawthorne Derby - Grade III, National Jockey Club Handicap - Grade III, Robert F. Carey, Memorial Handicap - Grade III, Sixty Sails Handicap - Grade III<br>
                            <strong>Main track: </strong>1 mile, oval<br>
                            <strong>Distance from last turn to finish line:</strong> 1,320 feet<br>
                            <strong>Turf course:</strong> 7&nbsp; furlongs</p>
               
  
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
