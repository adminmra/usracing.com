{assign  var="racetrackname" value="Fanduel Racing"} 
{assign  var="racetrackpath" value="fanduel-racing"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
      <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                                                     <p><strong>FanDuel Racing</strong> opened in 1925 with its inaugural meet on   September 26 and is part of the St. Louis metropolitan area. The track hosts Thoroughbred racing and did host harness racing up to 1999. It is one of five horse racing venues  in Illinois and the only one outside the metropolitan Chicago area.</p>
                            <p>North America's   champion sprinter Smile was also the 1985 Fairmount Derby winner. The victory at FanDuel Racing was    with Hall of Famer jockey Jacinto Vasquez aboard. The track record for 1 3/16 miles is  1:58 1/5 which was set by a 7-year-old gelding named Edward Gray on September 3, 1927. The   track record for 1 1/2 miles is 2:3 which was set by a 5-year-old gelding named Firth   of Tay on September 21 of that same year. </p>
                 {*
           <p><strong>Illinois Racetracks:</strong><br />
</p>
                            <ul>
                              <li><a href="/arlington-park">Arlington Park</a>
                              </li>
                              <li><a href="/balmoral-park">Balmoral Park</a>
                              </li>
                              <li><a href="/fairmount-park">FanDuel Racing</a>
                              </li>
                              <li><a href="/hawthorne">Hawthorne Race Course</a>
                              </li>
                              <li><a href="/maywood-park">Maywood Park</a></li>
                            </ul>
*}
                         
                            <h2>FanDuel Racing Race Course Information:</h2>
                            <strong>Racing Dates:</strong> April to September<br>
                            <strong>Race Days:</strong> Tuesday, Friday, Saturday<br>
                            <strong>Course Type:</strong> Thoroughbred, flat racing<br>
                            <strong>Main Track</strong>: 1 mile dirt oval with straight chutes for six furlong and 1 1/4 mile races.</p>
                  
       
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
