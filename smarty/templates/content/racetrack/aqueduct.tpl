{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/aqueduct.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
               .kd {
            background: transparent url(/img/racetracks/aqueduct/aqueduct-horse-betting-racetrack.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/aqueduct/aqueduct-racetrack-horse-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Aqueduct Horse Betting</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Aqueduct Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <h2 class="kd_subheading">Get your Aqueduct Horse Racing odds and betting action with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> now! </h2>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Aqueduct Park Major Races"
        summary="Major races for Aqueduct Racecourse. Only available at BUSR.">
	<h2>Aqueduct Live Racing Information</h2>
 <div class="racetrack-text">
<p><strong>Live Racing:</strong> Wednesday - Sunday<br>
 <strong>Live Racing Calendar:</strong> January-April / November-December<br>
<strong>Course type:</strong> Flat/Thoroughbred<br>
 <strong>Main track:</strong> 1 1/18 mile, oval<br>
 <strong>Distance from last turn to finish line:</strong> 1,346 feet<br>
  <strong>Turf course:</strong>  7/8 mile	</p></div>	  
			  <h2> Aqueduct Betting - Major Races</h2>	
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Aqueduct Major Races"
        summary="Major races for Gulfstream Park. Only available at BUSR.">
        <caption>Aqueduct Major Races - Grade 1</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Carter Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
			  <tr>  
               <td data-title="Month">Nov</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Carter Mile Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 miles (8 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Gulfstream Park Major Races"
        summary="Major races for Gulfstream Park. Only available at BUSR.">
        <caption>Aqueduct Major Races - Grade 2</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
				 <tr>  
               <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Wood Memorial Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			<tr>
                <td data-title="Month">Jun-Sep</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Gazelle Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
			<tr>
                <td data-title="Month">Nov</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Remsen Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo </td>
            </tr>
			      <tr>
                <td data-title="Month">Nov</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Demoiselle Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">	1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo  f</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Aqueduct Off Track Betting</h2>
		  <p>The racetrack is owned by State of New York & Operated by New York Racing Association. Aqueduct closed in 1956 and re-opened in September 14, 1959 after a $33 million renovation designed by noted racetrack architect Arthur Froehlich of the firm Arthur Froehlich and Associates of Beverly Hills, California.</p>
<p>Aqueduct commenced its inaugural meet on September 14, 1959. It is actually the second New York track to bear the name with the first, located on almost exactly the same plot of ground, was demolished in the mid-1950s.</p>
<p>Aqueduct was the first New York track to host a <a href="/breeders-cup" rel="nofollow">Breeders' Cup</a> race on November 2, 1985. <a href="/cigar-mile-handicap" rel="nofollow">The Cigar Mile</a> is named for one of the greatest horses, Cigar, who won the race in 1994.</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		          {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}