{assign  var="racetrackname" value="Turf Paradise"} 
{assign  var="racetrackpath" value="turf-paradise"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

 {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

              <p><strong>Turf Paradise</strong> opened in 1956 and is located 18 miles north of Phoenix Sky Harbor International Airport; 14 miles northwest of downtown Phoenix. It's the 3rd largest sports attraction in this State and holds court to one of the longest thoroughbred racing seasons in the country.</p>
              
              <h2>Turf Paradise Track Schedule</h2>
                      <strong>Live Racing:</strong> Friday to Tuesday<br>
                      <strong>Live Racing Calendar:</strong> October through May<br>
                      <strong>Course Type:</strong> Thoroughbred - Flat racing<br>
                      <strong>Main Track:</strong> One mile oval<br>
                      <strong>Distance from last turn to finish line:</strong> 990 feet<br>
                      <strong>Turf Course:</strong> 7 furlongs</p>
                      
                    <h2>Stakes Races at Turf Paradise</h2>


                      <p> MBNA America West/Southwest Airlines Championship Challenge Quarter horses (QH-GII)<br />
                        AQRA Turf Paradise Open Quarter Horse Derby (QH-GII)<br />
                        AQRA Turf Paradise Open Quarter Horse Futurity (QH-GIII)<br />
                        The Luke Kruytbosch Stakes<br />
                        City of Phoenix Stakes<br />
                        Caballos Del Sol Stakes<br />
                        Turf Paradise 870 Challenge<br />
                        ATBA Fall Sales Stake - Colts and Gelding Division<br />
                        ABTA Fall Sales Stake - Fillies Division<br />
                        Ford West/Southwest Juvenile Challenge Quarter Horse<br />
                        AHW Atukee Express Stakes<br />
                        Saguaro Stakes<br />
                        Walter R. Cluer Memorial Stakes (Turf)<br />
                        Chandler Stakes (Turf)<br />
                        AQRA Futurity Quarter Horse<br />
                        Paradise Valley Stakes (Turf)<br />
                        Queen of the Green Handicap (Turf)<br />
                        The &quot;Corona Cartel&quot; Stakes Quarter Horses<br />
                        Arizona Breeders' Futurity - Colts and Geldings Division<br />
                        Arizona Breeders' Futurity - Fillies Division<br />
                        Black Mountain Handicap<br />
                        Turf Paradise Quarter Horse Championship<br />
                        Mesa Handicap<br />
                        Arizona Juvenile Fillies Stakes<br />
                        Arizona Juvenile Stakes<br />
                        Cactus Wren Handicap<br />
                        Cotton Fitzsimmons Mile (Turf)<br />
                        Swift Stakes<br />
                        Glendale Handicap (Turf)<br />
                        Sun Devil Stakes<br />
                        Rattlesnake Stakes<br />
                        Kachina Handicap<br />
                        G Malleah Handicap<br />
                        Princess of Palms Handicap<br />
                        Turf Paradise Derby<br />
                        Arizona Oaks<br />
                        Phoenix Gold Cup<br />
                        Turf Paradise Handicap (Turf)<br />
                        Sun City Handicap (Turf)<br />
                        Palo Verde Stakes<br />
                        Cactus Handicap<br />
                        Coyote Handicap<br />
                        Tempe Handicap (Turf)<br />
                        Scottsdale Handicap<br />
                        Arizona Stallion Stakes (Turf)<br />
                        Wildcat Handicap (turf)<br />
                        Cactus Flower Handicap<br />
                        Sandra Hall Grand Canyon Handicap<br />
                        Arizona Breeders' Derby<br />
                        Joanne Dye Stakes<br />
                        Ann Owens Distaff Handicap<br />
                        Dwight D. Patterson Handicap (Turf)<br />
                        Desert Sky Handicap (Turf)<br />
                        ATBA Spring Sales Stake<br />
                        Hasta La Vista Handicap<br />
                      </p>

  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
