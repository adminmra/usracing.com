{assign  var="racetrackname" value="Louisiana Downs"} 
{assign  var="racetrackpath" value="louisiana_downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                   
<p>Harrah's <strong>Louisiana Downs</strong> horse racing track and racino opened in 1974 with 15,000 fans in attendance for the first day of its inaugural meet. Its located near Shreveport in Bossier City, northwest Louisiana. It was purchased by Caesars Entertainment in December, 2002. </p>
                          <p>Louisiana Downs is considered to be a showcase for horses from Louisiana, Arkansas, and Texas. Thoroughbred meet takes place from early May through October. A quarter horse meet is held from January through March.</p>
                          <p>Louisiana Downs is home of the Super Derby, a 1-1/4 mile race, which is one of the premiere races for 3 year olds in North America and has been since its first running in 1980. </p>
                          <p>In its 24 runnings, the Super Derby has attracted four <a href="/kentucky-derby/winners">Kentucky Derby winners</a>, four <a href="/preakness-stakes/winners">Preakness Stakes winners</a>, five <a href="/belmont-stakes/winners">Belmont Stakes winners</a> and seven <a href="/travers-stakes">Travers Stakes</a> winners. Six previous Super Derby entrants have won the prestigious <a href="/breeders-cup/classic">Breeders' Cup Classic</a> and three Super Derby winners, Tiznow, Sunday Silence and Alysheba have been voted the Eclipse Award as Horse of the Year, the industry's highest award.</p>
                        <p>{include file='/home/ah/allhorse/public_html/racetracks/louisiana_downs/video.tpl'} </p> 
<h2>Louisiana Downs Race Track Schedule</h2>
                          <p><strong>Live Racing</strong>:&nbsp; </strong>Thursday to Sunday<br>
	                          <strong>Live Racing Calendar:&nbsp; </strong>Thoroughbred racing: May to October, Quarter Horse racing: January to March<br>
	                          <strong>Course type: </strong>&nbsp;Flat/Thoroughbred<br>
	                          <strong>Notable races: </strong>Harrah's Louisiana Downs Breeders' Cup Handicap, Independence Handicap, Island Whirl Handicap, LA Breeders' Derby, LA Bred Quarter Horse Stake, Fleur de Lis, Dixie Miss, Folklore Handicap, Lady Luck<br>
	                          <strong>Main track: </strong>1 mile, oval</span><br>
	                          <strong>Distance from last turn to finish line:</strong> 1,010 feet
                            <br><strong>Turf course:</strong>&nbsp; 7/8 of a mile and 50 feet</p>
       
                    <h2>Stakes Races at Louisiana Downs</h2>
                    <blockquote>
                      <p>John Franks Memorial<br />
                        Honeymoon <br />
                        Sunny&rsquo;s Halo <br />
                        Barksdale<br />
                        Super Derby Prelude <br />
                        Louisiana Cup Juvenile<br />
                        Louisiana Cup Juvenile Fillies<br />
                        Louisiana Cup Derby<br />
                        Louisiana Cup Filly and Mare Sprint <br />
                        Louisiana Cup Oaks<br />
                        Louisiana Cup Sprint <br />
                        Louisiana Cup Turf Classic<br />
                        Louisiana Cup Distaf</p>
                    </blockquote>
            <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
