{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/fonner-park.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       .kd {
            background: transparent url(/img/racetracks/fonner-park/fonner-park-race-odds.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/fonner-park/fonner-park-live-horse-racing.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
<section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Fonner Park Off Track Betting Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <p>Bet at Fonner Park Today!</p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
       		  {*Secondary Copy*}
<h2>Fonner Park Horse Betting Odds</h2>
<p>Check out current <strong>Fonner Park Race Odds</strong></>, and bet on all <strong>Fonner Park Live Racing</strong> events!</p>
<p>Get the <strong>best horse racing bonus</strong> for new members, up to <a href="/promos/cash-bonus-20">$500 cash!</a></p>
<p>Also <strong>bet US Racing</strong> events, and get daily racebook rebates up to 8%!</p>
<p>Questions? Call us now at <strong>1-844-BET-HORSES</strong>
<h2>Fonner Park Live Racing Information:</h2>
<div class="racetrack-text">
<p><strong>Live Racing</strong>: Monday, Tuesday and Wednesday<br>
<strong>Thoroughbred Race Dates</strong>: February to May<br>
<strong>Course Type</strong>: Thoroughbred flat racing<br>
<strong>Main Track</strong>: 5 furlongs</p></div>
		  <br>
		    <p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
       {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}