{assign  var="racetrackname" value="The Meadows"} 
{assign  var="racetrackpath" value="the-meadows"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p>The Meadows Racetrack was the first parimutuel horse racing track in Western Pennsylvaniais and the oldest track overall in Pennsylvania.It opened in November of 1962.<br>
                        <br>
                      The Meadows is  known for introducing major technological advancements. In 1983, the track implemented Call-A-Bet and the Meadows Racing Network (MRN), that allowed bettors to create individual wagering accounts and phone in wagers for races.<br>
  <br>
                      With a racing schedule that goes on year-round, the racing calendar is highlighted by the $1,000,000 Coors Delvin Miller Adios Week, a major harness stakes races culminating with the $500,000 Adios Pace for the Orchids, Pennsylvania's richest stakes race.</p>
                    <p><strong>ADIOS 45 &mdash; ALSACE HANOVER &mdash; 2011</strong></p>
                    <p>In his elimination, Alsace Hanover set a world record for 3-year-old geldings &mdash; and he wasn't done yet. He blazed to victory in the final in 1:48.3, fastest ever paced by a sophomore gelding on a five-eighths-mile track. In doing so, he defeated Custard The Dragon, who established a world record for 3-year-old colts in his Adios elim, and gave Ron Pierce his third blanker of orchids. Pierce triumphed previously with Timesareachanging (2004) and Village Jolt (2005).<br />
                    </p>
                    <p><strong>ADIOS 46 &mdash; Bolt The Duer &mdash; 2012</strong></p>
                    <p>Entering the Adios, Bolt The Duer was suffering from &ldquo;seconditis,&rdquo; with place finishes in the Art Rooney, the Hempt final and his Adios elimination. But in the final of the &ldquo;Pace for the Orchids,&rdquo; he sat the pocket behind the unprecedented fractions thrown down by A Rocknroll Dance - three-quarters in 1:19.2 - shot the Lightning Lane and triumphed at 11-1 in 1:47.4, fastest mile ever on a five-eighths-mile track. Mark MacDonald drove the son of Ponder-Wonderbolt for trainer Peter Foley and owner All Star Racing Inc. Amazingly, A Rocknroll Dance saved place.<br />
                    </p>
<h2>>The Meadows Racetrack Schedule</h2>
                    <p>
                    <strong>Live Racing: </strong>All year long, with over 200 days of the year featuring live racing<br>
                    <strong>Racing Calendar: </strong>6 Days a week/Dark Sundays.Evening cards are on most Wednesdays and Fridays with a post time of 6:55pm local time<br>
                    <strong>Course type: </strong>Flat<br>
                    <strong>Notable Races: </strong>Sire Stakes Championships, F&amp;M Pace, Delvin Miller Adios and Pennsylvania Stallion Series.<strong><br>
                    <strong>Main track:</strong> 5/8 of a mile<br>
                    <strong>The Stretch:</strong> 566 feet in length, and 80 feet wide</p>                                
                      
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
