{assign  var="racetrackname" value="Cal Expo"} 
{assign  var="racetrackpath" value="cal-expo"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                    {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

<p><strong>Address</strong>: 1600 Exposition Blvd., Sacramento, California 95815</p>
                    <p>&nbsp;</p>
                    <p>Cal Expo is home to the only live Harness Racing track on the West Coast. For a short number of action-packed days every fall, live harness racing thunders through the Cal Expo racetrack with 14 days of those racing dates during the California State Fair. It opens the 1st Saturday in October for Saturdays only in October then moves to 2 day per week schedule (Friday and Saturday) in November, December.</p>
                    <p>The mile-long racetrack encircles a lake with fountains surrounded by a lush green lawn. The track is overseen by a multi-purpose grandstand.</p>
                    <p>There is a Clubhouse located above the Sports &amp; Wagering Center which  is only open outside of live harness racing as a party venue during the four big race days of the year:</p>
                    <ul>
                      <li><a href="/breeders-cup">Breeders&rsquo; Cup</a></li>
                      <li><a href="/kentucky-derby">Kentucky Derby</a></li>
                      <li><a href="/belmont-stakes">Belmont Stakes</a></li>
                      <li><a href="/preakness-stakes">Preakness Stakes</a></li>
                    </ul>
                    
                    <h2>
                        <strong>Cal Expo Racetrack Information</strong></h2>
                    <p>
                        <strong>Live Racing:</strong> Friday, Saturday <strong><br />
                      </strong></p>
                    <p><strong>Racing Dates:</strong> October to December</p>
                    <p><strong>Course type:</strong> Harness<strong><br />
                    </strong></p>
                    <p><strong>Main track:</strong> 1 mile
                    </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->