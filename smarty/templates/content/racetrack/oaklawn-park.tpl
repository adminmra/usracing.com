{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/oaklawn-park.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
   .kd {
            background: transparent url(/img/racetracks/oaklawn-park/oaklawn-park-horse-betting-racetrack.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/oaklawn-park/oaklawn-park-racetrack-horse-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Oaklawn Park Horse Betting</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Oaklawn Park Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{*include file='/home/ah/allhorse/public_html/stakes/salesblurb-stakes-primary.tpl' *} </p>
        {*end*}
        <p>Get your Oaklawn Park Horse Betting action with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}
		   

		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Oaklawn Park Major Races"
        summary="Major races for Oaklawn Park. Only available at BUSR.">
			  	  <h2>Oaklawn Park Information</h2>
<div class="racetrack-text">
<p><strong>Live Racing:</strong>  Wednesday - Sunday<br>
<strong>Live Racing Calendar:</strong>  January to April<br>
<strong>Course type:</strong> Thoroughbred<br>
<strong>Main track:</strong> 1 mile, oval<br>
<strong>Distance from last turn to finish line:</strong> 1,089 feet<br>
 <strong>Turf course:</strong>  Seven furlongs</p></div>
			  
			  <h2> Oaklawn Park - Major Races</h2>
	
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Oaklawn Park Major Races"
        summary="Major races for Oaklawn Park. Only available at BUSR.">
        <caption>Oaklawn Park Major races - Grade 1</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Apple Blossom Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
               <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> <a href="/arkansas-derby">Arkansas Derby</a></td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Oaklawn Park Major Races"
        summary="Major races for Oaklawn Park. Only available at BUSR.">
        <caption>Oaklawn Park Major races - Grade 2</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Azeri Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">	1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + f&m </td>
            </tr>
             <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Oaklawn Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
             <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Rebel Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Oaklawn Park anywhere handicapping</h2>
		  <p>Oaklawn Park is the only thoroughbred horse racing track in Arkansas. It officially opened on February 24, 1905 and in 2009, Oaklawn Park was ranked #9 race track in North America by the Horseplayers Association.  Oaklawn has evolved into one of the premier race tracks in the United States and hosts the Arkansas Derby, a <a href="/kentucky-derby/prep-races">Kentucky Derby prep race</a>, every year.</p>
                    <p>The first Arkansas Derby was run in 1936 for a purse of $5,000. By 1965, the <a href="/arkansas-derby">Arkansas Derby</a> was up to a $50,000 stakes and and started attracting  top <a href="/kentucky-derby/contenders">Kentucky Derby contenders</a>. The Arkansas Derby became a $100,000 stakes in 1972 and the following the year the Fantasy Stakes, a prep race to the <a href="/kentucky-oaks">Kentucky Oaks</a>, was first run. In 1983, the purse for the Arkansas Derby was increased to $250,000 and just one year later,  the purse was again lifted to $500,000, in 1984.<br> 
                          <br>
                      <strong>Oaklawn Park has has  hosted some of the biggest names in horse raceing, including:</strong></p>
                    <div class="racetrack-text">
<p><strong>Old Rosebud</strong> - 1917 Horse of the Year<br>
<strong>Black Tie Affair</strong> - 1991 Champion Older Male and Horse of the Year<br>
<strong> Paseana</strong> - 1992 and 1993 Champion Older Female<br>
<a href="/famous-horses/cigar"><strong>Cigar</strong></a> - 1995 Champion Older Male and Horse of the Year<br>
<strong>Favorite Trick</strong> - 1997 Champion Two-Year-Old and Horse of the Year<br>
<strong>Banshee Breeze</strong> - 1998 Champion Three-Year-Old Filly<br>
<strong>Smarty Jones</strong> - 2004 Champion Three-Year-Old<br> 
<strong>Saint Liam</strong> - 2004 Champion Older Male and Horse of the Year<br> 
<strong>Afleet Alex</strong> - 2005 Champion Three-Year-Old<br>
<strong>Proud Spell</strong> - 2008 Champion Three-Year-Old Filly<br> 
<strong>Curlin 2007</strong> - Champion Three-Year-Old and Horse of the Year<br>
<strong>Lookin At Lucky</strong> - 2009 Champion Two-Year-Old and 2010 Champion Three-Year-Old<br>
<strong>Rachel Alexandra</strong> - 2009 Champion Three-Year-Old Filly and Horse of the Year<br> 
<strong>Zenyatta</strong> - 2008, 2009 and 2010 Champion Older Female 2010 Horse of the Year<br>
<strong>Blind Luck</strong> - 2010 Champion Three-Year-Old Filly</p></div>                    
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		          {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}