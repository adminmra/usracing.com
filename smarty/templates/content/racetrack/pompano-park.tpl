{* include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/Pompano-park.tpl" *} {* to delete afer generic track schema made.  see top of page blocks*}
{include file="/home/ah/allhorse/public_html/usracing/racetracks/top-of-page-blocks.tpl"}
      
 {************PAGE SPECIFIC COPY*****************************************}       
        
<p><strong>Pompano Park</strong> is known as, &quot;The Winter Home of Harness Racing" has been the home of world-class standardbred racing since 1964. The track is located in Pompano Beach, Florida. The 5/8th's mile track has some of harness racing's best and bright stars competing on a  regular basis. </p>
 <h2>Pompano Park Track Schedule</h2>
{*  <div class="racetrack-text"> *}
<p>
    <strong>Live Racing: </strong>Monday, Tuesday, Wednesday and Saturday.<br>
    <strong> Racing Calendar:&nbsp; </strong>September - July<br>
    <strong>Course type: </strong>Standardbred harness<br>
     <strong>Main track:</strong>&nbsp; 5/8 mile, oval<br>
     <strong>Distance from last turn to finish line:</strong> 608 feet
</p>
{* </div> *}
<p><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>

{* <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                          *} 
{* 		 <p><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">I Want To Bet Now</a></p> *}

		 <h2>Pompano Park Off Track Betting</h2>
		 <p>Built in 1939, this race course is located in Hallandale Beach, Florida, and it´s one of the most important venues for horse racing in the USA. Pompano Park features two major racing events, The Florida Derby and The Pegasus World Cup</p>
		  
{************ End PAGE SPECIFIC COPY*****************************************}		 
 {include file="/home/ah/allhorse/public_html/usracing/racetracks/end-of-page-blocks.tpl"}		  
		  
		    
		   