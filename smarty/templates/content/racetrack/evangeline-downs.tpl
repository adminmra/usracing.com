{assign  var="racetrackname" value="Evangeline Downs"} 
{assign  var="racetrackpath" value="evangeline-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p>                    Evangeline Downs race track is one of the first horse racing tracks in Louisiana. The track features thoroughbred and quarter horse racing in a season that typically spans from April to October. </p>
                    <p>Evangeline Downs is known for having more horses per race than any other track in the country. The more horses in a race, the better the odds of winning and the larger the payouts.<br>
                            <br>
                      The track's annual program normally features a total of ten events worth $100,000 or more, including the Evangeline Mile Handicap, the largest and richest race hosted at Emerald Downs.</p>
     
                    <h2>
                            Evangeline Downs Race Course Information:</h2>
                    <p>
                      <strong>Live Racing:</strong> Wednesday through Sunday<br>
                           <strong>Thoroughbred Race Dates:</strong> April- September<br>
	                    <strong>Quarter Horse Race Dates:</strong> October - December<br>
                   
                    <strong>Course Type:</strong>Thoroughbred flat racing and American Quarter Horse racing<br>
                         <strong>Notable Races:</strong> Evangeline Mile Handicap<br>
                            <strong>Main Track:</strong> 7/8-mile<br>
                        
                    <strong>Distance from last turn to finish line:</strong> 400 yards<br>
         
                    <strong>Turf Course:</strong> 7 Furlongs </p>
           
                    <h2>Thoroughbred Stakes Races at Evangeline Downs</h2>
                    <blockquote>
                      <p>D.S. &quot;Shine&quot; Young Memorial Futurity - LA Bred<br />
                        Evangeline Mile<br />
                        D.S. &quot;Shine&quot; Young Memorial Futurity - LA Bred<br />
                        Matron Stakes<br />
                        John Franks Memorial Sales Stakes (Filly Div.)<br />
                        The Louisiana Legends Turf - LA Bred<br />
                        Tellike Stakes<br />
                        Hallowed Dreams Stakes<br />
                        Need for Speed Stakes<br />
                        The Louisiana Legends Distaff - LA Bred<br />
                        The Louisiana Legends Mile - LA Bred Fillies<br />
                        Opelousas Stakes<br />
                        John Henry Stakes<br />
                        Inaugural Stakes<br />
                        The Acadiana Stakes - LA Bred<br />
                        The Ragin Cajun Stakes - LA Bred<br />
                        The Louisiana Legends Ladies Sprint - LA Bred<br />
                        The Louisiana Legends Mile - LA Bred C&amp;G<br />
                        The Louisiana Legends Classic - LA Bred<br />
                        The Louisiana Legends Starter Stakes - LA Bred<br />
                      </p>
                    </blockquote>
      
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
