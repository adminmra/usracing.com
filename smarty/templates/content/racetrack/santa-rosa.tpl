{assign  var="racetrackname" value="Santa Rosa"} 
{assign  var="racetrackpath" value="santa-rosa"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                   {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                    <p>Santa Rosa is part of the California State Fair horse racing circuit. The Sonoma County Fair happens every year in July / August.</p>
      
                    <h2>Santa Rosa Track Schedule</h2>
                    <p>
                      Dark on Mondays &amp; Tuesdays<br />
                      Daily Post Time - 1:15 p.m.<br />
                      Friday Post Time - 3:15 p.m.</p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
