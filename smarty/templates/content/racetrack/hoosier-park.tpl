{assign  var="racetrackname" value="Hoosier Park"} 
{assign  var="racetrackpath" value="hoosier-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Hoosier Park</strong> is located in Anderson, Indiana and was the first racetrack outside Kentucky owned by Churchill Downs since 1939. In 2001, Indiana Downs became the second horse racing track in the state. It is rated #4 in US Thoroughbred horse tracks.</p>
                    <p> The race course offers Harness, Thoroughbred, and Quarter horse racing, as well as high-profile Stakes events every year including the Plymouth Stakes; the Muskegon Classic; the Frontier Handicap; the Michigan Sire Stakes; and the Michigan Futurity.</p>
                   
         <h2>Hoosier Park Race Course <strong>Information:</strong></strong><br>
                    </h2>
                    <p>
                      <strong>Live Racing:</strong> Tuesday through Saturday<br>
                    <strong>Racing Calendar:</strong> Live Standardbred Racing: March  to July . Live Thoroughbred &amp; Quarter Horse Racing August to October <br>
                    <strong><strong>Course Type:</strong></strong> Thoroughbred and standardbred<br>
                        <strong>Notable Races:</strong> City Of Anderson Stakes, Richmond Stakes, Indiana Breeders' Cup Oaks (Grade II), Indiana Derby (Grade II), Hillsdale Stakes, Gus Grissom Stakes, Hoosier Silver Cup Stakes - Filly Division, Hoosier Silver Cup Stakes, The Merrillville Stakes, Brickyard Stakes, Miss Indiana Stakes, Indiana Futurity<br>
                      <strong>Main Track:</strong> 7/8 mile<br>
                      <strong>Distance from last turn to finish line:</strong> 1,255 feet
                        </p>
             
                    <h2>Graded Stakes at Hoosier</h2>
                    <blockquote>
                      <p>The City Of Anderson Stakes<br />
                        The Richmond Stakes<br />
                        The Indiana Breeders' Cup Oaks (Grade II)<br />
                        The Indiana Derby (Grade II)<br />
                        The Hillsdale Stakes<br />
                        The Gus Grissom Stakes<br />
                        Hoosier Silver Cup Stakes <br />
                        Hoosier Silver Cup Stakes<br />
                        The Merrillville Stakes<br />
                        The Brickyard Stakes<br />
                        The Miss Indiana Stakes<br />
                        The Indiana Futurity<br />
                        The Frances Slocum Stakes<br />
                        The Michael G. Schaefer Mile Stakes<br />
                        The Too Much Coffee Stakes<br />
                        The Indiana Stallion Stakes <br />
                        The Indiana Stallion Stakes<br />
                      </p>
                    </blockquote>
            
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
