{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/saratoga.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       @media (max-width: 800px) {
            .hide-sm {
                display: none;
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref1}&signup-modal=open" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="https://busr.ag?ref={$ref1}&signup-modal=open" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading"> Saratoga Horse Betting</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Saratoga Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-racetracks-primary.tpl'} </p>
        {*end*}
        <p>Get your Saratoga Horse Betting action with <a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}

		  <!--<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Saratoga Major Races"
        summary="Major races for Saratoga. Only available at BUSR.">-->
		<h2>Saratoga Information</h2>
 <div class="racetrack-text">
<p><strong>Live Racing:</strong>  Saturday to Tuesday<br>
 <strong>Live Racing Calendar:</strong>  June to September<br>
 <strong>Course type:</strong> Flat / Thoroughbred<br>
 <strong>Main track:</strong> 1 1/18 mile, oval<br>
 <strong>Distance from last turn to finish line:</strong> 1144 feet<br>
 <strong>Turf course:</strong>  Seven furlongs, 304 feet</p></div>		  
<!--<h2> Saratoga - Major Races</h2>	
 <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Saratoga Major Races"
        summary="Major races for Saratoga">
        <caption>Saratoga Major Races - Grade 1</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age - Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Alabama Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1​1⁄4 miles (10 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
			  <tr>  
               <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Alfred G. Vanderbilt Handicap</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 6 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				    <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Ballerina Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m  </td>
            </tr>
               <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Coaching Club American Oaks </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f  </td>
            </tr>
                <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Diana Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m </td>
            </tr>
              <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Forego Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
             <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fourstardave Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 8 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
               <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> H. Allen Jerkens Memorial Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
            <tr>
                <td data-title="Month">Set</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Hopeful Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo </td>
            </tr>
             <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Personal Ensign Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m </td>
            </tr>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Spinaway Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f </td>
            </tr>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Sword Dancer Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄2 miles (12 furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo+ </td>
            </tr>
              <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Test Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 7 furlongs</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
              <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Travers Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> ​1 1⁄4 miles (10 Furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
               <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Whitney Stakes (NYRA) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> ​ ​1 1⁄8 miles (9 furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
                 <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Woodward Stakes (NYRA) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> ​ ​1 1⁄8 miles (9 furlongs)</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
			  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Saratoga Major Races"
        summary="Major races for Saratoga. Only available at BUSR.">
        <caption>Saratoga Major Races - Grade 2</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Adirondack Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">	 ​6 1⁄2 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f</td>
            </tr>
	           <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Amsterdam Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">  6 1⁄2 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Ballston Spa Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">  1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m</td>
            </tr>
              <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Bernard Baruch Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;">  1 and 1/16 mile (8 1/2 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
            </tr>
             <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Bowling Green Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">  1 3⁄8 miles (11 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
            </tr>
             <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Honorable Miss Handicap </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 6 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f</td>
            </tr>
            <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Jim Dandy Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 9 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
                <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Lake George Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄16 miles (8.5 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
             <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Lake Placid Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> National Museum of Racing Hall of Fame Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄8 miles (9 furlongs) </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
             <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Prioress Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> 6 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
            </tr>
            <tr>
                <td data-title="Month">Aug</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Saratoga Special Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;">   ​6 1⁄2 furlongs </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo </td>
            </tr>
		    	<br>
			     </tbody>
			  <br>
    </table>-->
    <br>
    <h2>Saratoga Racetrack Odds</h2>
    <div id="no-more-tables">
    <table class="table table-condensed table-striped table-bordered ordenableResult data" width="100%" cellpadding="0" cellspacing="0" border="0" title="Saratoga Racetrack July Schedule" summary="Saratoga Racetrack July Schedule">
    <thead>
        <tr>
            <th> Date </th>
            <th> Race </th>
            <th> Distance </th>
<!--            <th> Surface </th>
            <th> Purse </th>
-->            
        </tr>
    </thead>
    <tbody>
        <caption class="hide-sm">Saratoga Racetrack August Schedule</caption>
        <tr>
            <td data-title="Date">Saturday, August 8</td>

<!-- ***** NOTE: Add the following two lines below each race after we know the real data to addSort *****

            <td data-title="Distance" style="word-break: keep-all !important;">9 furlongs</td>
            <td data-title="Surface" style="word-break: keep-all !important;">Dirt</td>

 -->


            <td data-title="Race" style="word-break: keep-all !important;">Waya Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
        </tr>
        <tr>
            <td data-title="Date">Saturday, August 8</td>
            <td data-title="Race" style="word-break: keep-all !important;">Troy </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$200,000</td>
        </tr>
        <tr>
            <td data-title="Date">Saturday, August 8</td>
            <td data-title="Race" style="word-break: keep-all !important;">Runhappy Travers Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$1,000,000</td>
        </tr>
        <tr>
            <td data-title="Date">Saturday, August 8</td>
            <td data-title="Race" style="word-break: keep-all !important;">Longines Test Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$300,000</td>
        </tr>
        <tr>
            <td data-title="Date">Wednesday, August 12</td>
            <td data-title="Race" style="word-break: keep-all !important;">Adirondack Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
        </tr>   
        <tr>
            <td data-title="Date">Saturday, August 15</td>
            <td data-title="Race" style="word-break: keep-all !important;">Alabama Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$500,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Thursday, August 20</td>
            <td data-title="Race" style="word-break: keep-all !important;">New York Turf Writers Cup </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Saturday, August 22</td>
            <td data-title="Race" style="word-break: keep-all !important;">Fourstardave Handicap </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$400,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Sunday, August 23</td>
            <td data-title="Race" style="word-break: keep-all !important;">Diana Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$500,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Friday, August 28</td>
            <td data-title="Race" style="word-break: keep-all !important;">Lake George Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
        </tr>
        <tr>
            <td data-title="Date">Saturday, August 29</td>
            <td data-title="Race" style="word-break: keep-all !important;">Sword Dancer </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$500,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Saturday, August 29</td>
            <td data-title="Race" style="word-break: keep-all !important;">Saranac Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$100,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Saturday, August 29</td>
            <td data-title="Race" style="word-break: keep-all !important;">Amsterdam Stakes </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$150,000</td>
        </tr> 
        <tr>
            <td data-title="Date">Sunday, August 30</td>
            <td data-title="Race" style="word-break: keep-all !important;">Shuvee </td>
            <td data-title="Purse" style="word-break: keep-all !important;">$125,000</td>
        </tr> 
    </tbody>
    <br>
    </table>
    </div>

		  <br><br>
		    <p><a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Saratoga Off Track Betting</h2>
		  <p>Summer horse racing at its finest! With large crowds, live bands and exciting horse racing action, Saratoga has everything you need to get your adrenaline pumping! Don’t miss the opening day on Thursday, July 16th, 2020.</p>

<p>Opened in 1863, Saratoga Race Course is the oldest organized sporting venue in the United States. It was named one of the world’s greatest sporting venues by Sports Illustrated. This track has a lot of history, in fact, it held its first thoroughbred meet just a month after the Battle of Gettysburg. These days, the population of Saratoga Springs triples to 75,000 when the thoroughbreds return for the summer meet each year. Saratoga’s motto is “Health, history, and horses.”</p>
<p>The  race course is known as the "Graveyard of Champions" as many an upset has happened here. Like <a href="/bet-on/triple-crown">Triple Crown</a> winner, <a href="/famous-horses/secretariat">Secretariat</a> lost and Gallant Fox was bested by 100-1 longshot Jim Dandy in the 1930 <a href="/travers-stakes">Travers Stakes</a>.</p>
<p>The track record for one-day attendance is 72,745 on August 11, 2007 and the record for one-day handle is $10,614,786 all on August 23, 2003 (Travers Day.) </p>
		  <br>
		    <p><a href="https://busr.ag?ref={$ref2}&signup-modal=open" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		          {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{*include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"*}
    <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
            <h2 class="card_heading card_heading--no-cap">Get your New Member Bonus Up To $500 Cash!<br>at BUSR</h2>
            <a href="https://busr.ag?ref={$ref}&signup-modal=open" rel="nofollow" class="card_btn card_btn--default">Sign Up Now</a>
          </div>
        </div>
      </div>
    </section>
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}