{assign  var="racetrackname" value="Western Fair Raceway"} 
{assign  var="racetrackpath" value="western-fair-raceway"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>The <strong>Western Fair Raceway </strong>hosts harness racing on a half-mile track during a Spring meet from January to May. Live racing is also held at the annual Fair in September / October. The Western Fair is a fair held annually in London, Ontario, Canada in early September. </p>
<h2>Stakes Races at Western Fair Raceway</h2>
                      <p><strong>Molson Pace</strong><br />
                        This is the track's signature event, held on the last Friday night in May ending the live spring season</p>
                      <p><strong>City of London Series</strong><br />
                        Held in the spring for 4-year old and younger Ontario sired pacers and trotters who are non-winners of $10,000 lifetime.<br />
                      </p>
                 
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
