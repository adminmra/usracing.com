{assign  var="racetrackname" value="Scioto Downs"} 
{assign  var="racetrackpath" value="scioto_downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

<p><strong>Scioto Downs</strong> has offered  harness racing since 1959. The track is located a half-mile south of Columbus, Ohio. It offers the State Fair Stakes in August of every year.</p>
                 
                                      
  
                    <h2><strong>Scioto Downs Track Schedule</strong></h2>
                    <p>
                    <strong>Live Racing Dates: </strong>May to September<br>
                   
                   <strong>Live Racing Schedule: </strong>Wednesday through Saturday - Dark Sundays with a few exceptions<br>
                       
                    <strong>Course type: </strong>Flat<br>
                     
                    <strong>Notable Races: </strong>Charlie Hill Pace, Jay Kaltenbach Trot, Dick Buxton Trot, Steve Sugg Pace, Herb Coven Jr. Pace, Capital City Pace, Tish Arledge Trot, Ervin Samples Trot, Heart of Ohio Trot<br>
                    <strong>Main track:</strong> 5/8 mile</p>



<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
