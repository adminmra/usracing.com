{assign  var="racetrackname" value="Hastings Park"} 
{assign  var="racetrackpath" value="hastings-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Hastings Park Racecourse</strong> is a jewel of a park located just 4 kilometers from downtown Vancouver with a spectacular view of the North Shore mountains and Burrard Inlet. </p>
                    <p>In 1889, Vancouver&rsquo;s earliest horse racing events took place along Howe Street in the heart of the city. The response was overwhelming and due to its popularity, a race track was built in what was once known as 'East Park.'</p>
                   <h2>Hastings Park Racing Calendar</h2>
                    <p>Hasting Park is open from April to October. Hastings live racing is conducted on Saturdays and Sundays from April until November and Friday nights from June to September with live racing being offered at the following schedule:</p>
                    <ul>
                      <li>9am to 9pm: Monday &amp; Tuesday 
                        </li>
                      <li>9am to 10pm: Wednesday &amp; Thursday 
                        </li>
                      <li>9am to Last Simulcast Race:  Friday &amp; Saturday &amp; Sunday 
                        </li>
                      </ul>
                    <p> The first live race of the day starts at 1:40 p.m. and ends between 5:00 p.m. and 6:00 p.m. 
                    </p>
                 
                    <h2>Stakes Races At Hasting Park</h2>
                    <ul>
                      <li>Brighouse Belles
                        </li>
                      <li>George Royal
                        </li>
                      <li>Ross McLeod
                        </li>
                      <li>Jim Coleman Province
                        </li>
                      <li>John Longden 6000
                        </li>
                      <li>Strawberry Morn
                        </li>
                      <li>Sir Winston Churchill
                        </li>
                      <li>Emerald Downs
                        </li>
                      <li>Vancouver Sun
                    </ul>
                  
                    <h2>BC Cup</h2>
                    <ul>
                      <li>BC Cup Debutante
                        </li>
                      <li>BC Cup Nursery
                        </li>
                      <li>BC Cup Dogwood
                        </li>
                      <li>BC Cup Stellar&rsquo;s Jay
                        </li>
                      <li>BC Cup Distaff
                        </li>
                      <li>Redekop BC Cup Classic
                      </li>
                    </ul>
           
                     
                    <ul>
                      <li><strong>BC DERBY / BC OAKS DAY</strong><br />
                        British Columbia Derby GIII<br />
                        BC Oaks<br />
                        SW Randall Plate<br />
                        Delta Colleen<br />
                        Sadie Diamond<br />
                        Jack Diamond</li>
                    </ul>
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
