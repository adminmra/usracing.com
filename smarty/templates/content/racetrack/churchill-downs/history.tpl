
      
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>History of Churchill Downs Kentucky Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />				
<p>Read about the origins of America's most legendary track, <a title="Churchill Downs" href="/churchil-downs">Churchill Downs</a>, and follow the history of that track's most prestigious race from its inception in 1875 up through modern times.</p>
<br />
<p>This segment of the <a title="US Racing" href="/">US Racing</a> history lesson introduces you to the figures most responsible for the <a title="Derby" href="/kentucky-derby">Derby</a> being what it is today, the most exciting two minutes in sports and the greatest social event in all of Kentucky if not the United States or even the entire world.</p>

<p>There are three chapters, <a title="A History of Horse Racing" href="/horse-racing-history">A History of Horse Racing</a>, <a title="Horse Racing in Kentucky" href="/kentucky">Horse Racing in Kentucky</a> and <a title="A History of Churchill Downs and the Kentucky Derby" href="/churchill-downs/history">A History of Churchill Downs and the Kentucky Derby</a>. The material is meant to be read straight through, but feel free to roam and jump around as you please. The main reference for the US Racing history section is the fantastic website Call to the Derby Post which takes its source from the book "Jockeys, Belles and Bluegrass Kings, The Official Guide to Kentucky Racing" by Lynn S. Renau <cite>(Herr House Press, Louisville, Kentucky: 1995).</cite></p>
<br />
          
          
<div class="headline"><h1><span class="DarkBlue">Churchill's Colonel </span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />

<p>Meriwether Lewis Clark, Jr. was the grandson of former Missouri governor and Lewis and Clark Expedition co-leader General William Clark. He was the great nephew of Louisville founder George Rogers Clark. Most important to our story here is that his father married Abigail Prather Churchill and linked him to one of Kentucky's first families. The Churchills had moved to Louisville in 1785 and purchased 300 acres of land, part of which today includes Churchill Downs. When his mother dies, Lutie, as M. Lewis Clark, Jr. was nicknamed, was sent to live with his aunt and her sons John and Henry, who inherited most of the property. Lutie developed tastes for custom-made suits, good food, champagne and horse racing--he even sat besides his uncles at the Woodlawn Race Course (see Part II). Through two trips to Europe and through convenient marriages and deaths, Lutie gained both a sophisticated appreciation for horse racing and as well strong connections to several Southern racetracks.</p><p>Late in 1873, Clark came home from abroad with ideas about how to build a racetrack--and to eliminate bookmaking by using French pari-mutuel wagering machines. (The "pari" in pari-mutuel is actually a shortened reference to Paris.) The Churchill family, obviously, financially backed Lutie's racetrack venture. With a need to showcase their racing stock, especially in light of the Cincinnati-Lexington railroad connection (also see Part II), a new racetrack was decided upon and was to be built on Churchill land. The Churchill brothers were the entrepreneurs who arranged the new Louisville Jockey Club and Driving Park Association while Lutie acted as president an on-site manager. Half the Louisville Jockey Club members were local bankers, hotel men and streetcar company owners; the other half were those with large farms and whiskey interests. By selling 320 shares of stock at $100 a share the Louisville Jockey Club came up with $32,000 to build a racetrack. Plans for the track were announced in June 1874: in spring and fall the facility would be devoted to racing, at other times people were free to use the grounds for carraige driving (hence the Driving Park Association as part of the Jockey Club's name). Noted architect John Andrewartha was retained to design the grandstand and Jockey Club headquarters. Located on the first turn--as far away from the stable area as possible--the rustic two-story retreat featured a kitchen and the only indoor toilet within miles.</p>

<p>          
          
<div class="headline"><h1><span class="DarkBlue">Derby Day, 1875</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</p>
<br />
<p>The track opened amid great hoopla on May 17, 1875. Believe it or not, the Kentucky Derby was not planned as the main attraction of the inaugural meet, but when H.P. McGrath's Aristides set a new world's record for the mile-and-a-half distance, "the crowd went wild." Still, racing three-year olds was a relatively new venture, and there were two other races that day which were bigger than the Derby: the Louisville Cup, discontinued after 1887, and the Gentlemen's Cup Race, in which a member of a recognized jockey club rode his own horse. After only one year, Lutie Clark and the track were considered a success.</p><p>At this point it is to remember the family history between the Clark's and the Churchill's, and how the relationship ties into the origins of the track. "Colonel" Lutie Clark became more and more obsessed with his racetrack endeavors, and eventually split from his wife. By 1886 Mary Clark and her three children were living with widower John Churchill. In November 1890, John Churchill, then 71 and a widower for 30 years, remarried. His wife was 36-year old Tina Nicholas, who was from a Kentucky family as distinguished as the Churchills. Their son, John Rowan Pope Churchill, Jr. was born 10 months later. As early as 1884 the Churchills began writing wills. Henry Churchill's first will left his wife Julia the family house, $60,000 and $800 yearly rent from the track. In 1889, two years before he died, Henry Churchill rewrote his will, leaving his entire estate to his wife. By mutual agreement, the Louisville Jockey Club land became John's property. Had John not remarried in 1890, Lutie Clark might have inherited the land the track was on, but the birth of a health Churchill heir changed that.</p>

<p>Lutie Clark was quite a figure, both literally and figuratively. At one point he could barely climb the stair to the top of the grandstand to monitor races. He was often seen as bullyish, and was quick to judgment and to express opinion. He was also one easily led into a quarrel, often leading to his own embarrassment:</p>

<p><ul>
<li><p>In 1879 Clark refused prominent breeder T.G. Moore permission to race at the Louisville Jockey Club, claiming Moore's entry fees were past due. Moore took the announcement as a personal insult and demanded an apology; the Colonel refused and ordered Moore out of his Galt House office. When Moore told Clark he would bear the consequences of his decision not to apologize, Clark knocked Moore to the ground, held a gun on him and ordered him off the premises. Moore left the room, got a gun and shot Clark through the door--the bullet hit Clark in the chest, lodging under his right arm. Moore turned himself in at the police station, but no charges were brought. He was subsequently ruled off the track due to the dispute over the fees, yet Clark reversed the decision a year later.</p><br /></li>

<li><p>Several years later, when Clark was working as a steward at a Chicago track, a bartender at Clark's hotel took offense at Clark's calling Chicagoans "thieves and liars," and told Clark so. Clark took off, then returned with a gun, rested the muzzle on the bartender's chest and forced the man to apologize. There were plenty of witnesses, because the story was retold in both Chicago and Louisville newspapers. The Churchill brothers were not pleased with the negative publicity.</p></li>
</ul></p>
<br />

<p>In 1891 John Churchill wrote his will. His wife Tina was named administrator; she and their son got everything minus 46 acres allotted to Clark and his three children. Lutie Clark was to choose the best land from property adjoining the Louisville Jockey Club grounds, but John made it clear that Clark was to have no part of the track itself. By the time John died in 1897 Clark was merely serving as a steward at Churchill Downs, the track he helped to originate. His attitude, personality, disregard of track and city expenses and other unappealing personality traits placed him in ill-repute with almost everyone including his family. But his contributions to the track and to the Derby cannot be overlooked; without him the Derby never would have been born and Churchill Downs might never have gotten off the ground. "He brought the first pari-mutuel wagering machines into Kentucky and tried, although without success, to get the public to use them (instead of relying on bookmakers). He presided over the first American Turf Congress, held at Louisville's Galt House Hotel, and wrote racing rules that are still in force today. He worked for a uniform system of weights and pioneered the stakes system, creating the Great American Stallion Stake--on which the present-day Breeders' Cup is modeled. Clark spoke out against betting by officials and reporters, which certainly did not endear him to the press. His only Derby wager, he bragged, was the price of a new hat."</p><p>Yet during the 1880s his reputation as an arrogant, quick-tempered man grew. He was not well liked by locals, who took to calling the track "Churchill's downs," a reference to English racing that poked fun at the "highfalutin" president as well as reminding him who really controlled the Louisville Jockey Club's purse strings. In 1883 the local press picked up on the nickname, which has become the track's incorporated, trademarked name.</p>

<p>No doubt Clark, who did his gambling on the stock market, lost heavily in 1893 when the economy exploded so badly that the New York Stock Exchange closed for ten days. He traveled from city to city, working as a steward at regional tracks. In 1899, fearing a life of poverty and senility, Clark committed suicide. The Louisville Jockey Club had gone through virtual bankruptcy and a total change of command. Shortly after the takeover, a new double-steepled grandstand took shape after the track was purchased by bookmakers who styled themselves the New Louisville Jockey Club. They ran the Downs for the next eight years, discontinuing the money-losing Fall Meets after 1895. The new owners took over the Churchill land lease and built a twin-spired wooden grandstand on the west side of the track. There was no clubhouse for members and no separate seating for the ladies, but there was a brand new 60-foot wide, 200-foot long, brick-floored betting enclosure only a stone's throw from the new saddling paddock. The myth of racing as sport was replaced by the reality of a view-and-bet gambling operation. Churchill Downs and the Kentucky Derby had survived its birth, and would now need to endure a few more growing pains before finally becoming entrenched in racing, sports, Kentucky and American culture.</p>

<p align="center"> </p>
<p align="center"><a title="A History of Horse Racing" href="/horse-racing-history">A History of Horse Racing</a> </p>

<p align="center"><a title="Horse Racing in Kentucky" href="/kentucky">Horse Racing in Kentucky</a></p>

<p align="center"><a title="A History of Churchill Downs and the Kentucky Derby" href="/churchill-downs/history">A History of Churchill Downs and the Kentucky Derby</a></p>
			        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    