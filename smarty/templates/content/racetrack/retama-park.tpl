{assign  var="racetrackname" value="Retama Park"} 
{assign  var="racetrackpath" value="retama-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>On April 7th, 1995,  <strong>Retama Park</strong> opened for its first season of racing.  The track is open for simulcasting every day except Christmas Day. Retama Park race track is located in Selma, Texas which  is part of the San Antonio Metropolitan Area.</p>
                   
                    <h2>Retama Park Track Schedule</h2>
                    <p><strong>Live Racing Dates:</strong> Thoroughbred Meet - October  to December <br />
                   
                      <strong>Live Racing Schedule:</strong> Friday and Saturday<br>
                    <strong> Post time:</strong> 7:15 P.M. Central</p>
                   
                    <h2>Stakes Races at Retama Park</h2>
                    <p><strong>Thoroughbred Stakes Schedule</strong><br />
                      El Joven Stakes<br />
                      The Fiesta Mile <br />
                      Texas Horse Racing Hall of Fame Stakes <br />
                      Darby's Daughter Texas Stallion Stakes <br />
                      My Dandy Texas Stallion Stakes </p>
                    <p><strong>Arabian Stakes Schedule</strong><br />
                      Texas Arabian Oaks<br />
                      Texas Arabian Derby (Grade III) <br />
                      Lone Star Arabian Fillies Futurity <br />
                      Lone Star Arabian Colts &amp; Geldings Futurity <br />
                    </p>
                 
            
        <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
