
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

                 
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Ajax Downs has a deep rooted history of horse racing in Canada. For 40 years, it has been exclusively devoted to the exciting sport of Quarter Horse racing. Many of Canada's top jockeys have started their careers on the old &quot;J-track&quot;. Formerly called &quot;Picov Downs&quot; the name was changed in 2006 with the establishment of a new Slots facility and construction of a full 6 furlong oval racetrack which was completed in 2009. </p>
                    <p>The track is the only quarter horse racetrack in the area and currently runs a total of 35 dates with purses in excess of $5 million. Key stakes include the Alex Picov Memorial Futurity, the Alex Picov Memorial Championship (G3), and the Ontario Jackpot Futurity. The Ajax Downs Race Track also offers simulcasting of more than 40 of the most popular tracks from around the world</p>
                    <p>Alex Picov, founded the Ajax Downs Racetrack more than 30 years ago. In 1974, after nearly a decade of importing quarter horses from the U.S., the Picov family opened a horse complex in Ajax. The facility, which included an arena and tack shop, hosted horse shows early on. Years later, Picov Downs became Ajax Downs Racetrack and remains a must-see landmark of the Canadian frontier. </p>      
           
<h2>Stakes Races at Ajax Downs Racetrack for 2 year olds &amp; up</h2>
                    <p>Ontario Sire Stakes Futurity<br />
                      All Canadian Futurity<br />
                      Maple Leaf Futurity<br />
                      Alex Picov Memorial Futurity<br />
                      Ontario Jackpot Futurity<br />
                      John Deere Ajax Downs Juvenile Challenge</p>
                  
                    <h2>Stakes Races at Ajax Downs Racetrack for 3 year olds </h2>
                    <p>Maple Leaf Derby<br />
                      Ontario Sire Stakes Derby<br />
                      All Canadian Derby<br />
                      Picov Derby<br />
                      Ontario Bred Derby<br />
                      Ontario Jackpot Derby<br />
                      Adequan Ajax Downs Derby Challenge</p>
          
                    <h2>Stakes Races at Ajax Downs Racetrack for 3 year olds &amp; up</h2>
                    <p>Ontario Sires Stakes Maturity<br />
                      All Canadian Classic <br />
                      Ontario Bred Maturity<br />
                      Open Maturity<br />
                      Picov Maturity<br />
                      Alex Picov Memorial Championship<br />
                      Ontario Jackpot Maturity<br />
                      Ajax Downs Classic <br />
                      Maiden Stakes<br />
                      Merial Distaff Challenge<br />
                      Bank of America Champion Challenge<br />
                      Challenge Maiden</p>
            
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
