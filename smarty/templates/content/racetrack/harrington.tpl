{assign  var="racetrackname" value="Harrington Raceway"} 
{assign  var="racetrackpath" value="harrington-raceway"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Harrington Raceway</strong> is one of the nation's oldest racetracks and has run a race meet every year since 1946. A renonvation in 2003 created  wider turns, and ultimately a safer, faster racetrack. An interesting fact: approximately 10% of the machine win in the casino goes towards purses which has resulted in Harrington Raceway being able to distribute approximately $135,000 in purses nightly and therefore established itself as a top track on the national circuit.</p>
                    <p>Live racing at Harrington is from August to October with races on Sunday through Thursday and a 5:30 p.m. post time.</p>
           
                    <h2>Stakes Races at Harrington Raceway </h2>
                    <ul>
                      <li>Governor's Cup Pace 
                      </li>
                      <li>Legislator's Cup Pace
                      </li>
                      <li>Charles D. Murphy Memorial Trot 
                      </li>
                      <li>President's Cup Pace 
                      </li>
                      <li>Bobby Quillen Memorial </li>
                    </ul>
            
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
