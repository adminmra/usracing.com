{assign  var="racetrackname" value="Monmouth Park"} 
{assign  var="racetrackpath" value="monmouth-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

                   
                    <p><strong>Monmouth Park </strong>Racetrack opened in July of 1870, it's located three miles from Long Branch. The current racing structure dates back to the 1940s when the track re-opened after a 50-year closure due to legislative issues. It was dubbed, &quot;The Shore's Greatest Stretch.&quot;<br>
                          <br>
                          The track's main event is the <a href="/haskell-stakes">Haskell Stakes</a> which was first run in 1968 as a handicap, but was made into an Invitational Handicap in 1981. It is now a 1-1/8-mile test for three-year-olds run in early August. Monmouth Park also hosted the <a href="/breeders-cup">Breeders' Cup</a> World Championships, in 2007.</p>
                    <h2>Monmouth Park Track Schedule</strong></h2>
                    <p>
                    <strong> Live Racing:</strong> Wednesday to Wednesday<br>
                     <strong>Racing Calendar:</strong>: May  to September <br>
                     <strong>Course type:</strong> Flat<br>
                     <strong>Notable Races:&nbsp;</strong> Haskell Invitational Handicap G1, United Nations Stakes G1, Molly Pitcher Stakes G2.<br>
                     <strong>Main track (dirt):</strong> 1 mile oval<br>
                     <strong>Distance from last turn to finish line:&nbsp;</strong> 990 feet<br>
                     <strong>Turf course:</strong> 7 furlongs</p>
                    
                     <h2>Graded Stakes Races at Monmouth Park</h2>
                    <blockquote>
                      <p>The Monmouth Stakes<br />
                        The Red Bank Stakes<br />
                        The Pegasus Stakes<br />
                        The Eatontown Stakes<br />
                        The Jersey Shore Stakes<br />
                        The Salvator Mile<br />
                        The United Nations Stakes<br />
                        The Haskell Invitational<br />
                        The Molly Pitcher Stakes<br />
                        The Monmouth Cup<br />
                        The Oceanport Stakes<br />
                        The Matchmaker Stakes<br />
                        The Monmouth Oaks<br />
                        The Philip H. Iselin Stakes<br />
                        The Cliff Hanger Stakes<br />
                        The Sapling Stake</p>
                    </blockquote>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
