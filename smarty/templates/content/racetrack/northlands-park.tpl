{assign  var="racetrackname" value="Northlands Park"} 
{assign  var="racetrackpath" value="northlands-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks_canada.tpl'}
{*include file='menus/racetracks_international.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>                    
<p><strong>Northlands Park</strong>, located in the city of Edmonton, is considered the &quot;Alberta A circuit&quot;. In its 100 plus years of operation, it has been one of Western Canada&rsquo;s top racetracks and has been the official home of the Canadian Derby since 1957.</p>
                  <h2>Northlands Park Racing Schedule</h2>
                  <p><strong>Harness (Standardbred) meet:</strong> February - March &amp; September to December<br>
	                    <strong>Thoroughbred meet:</strong> May/June to September/October </p>
               
                    <h2>Stakes Races at Northlands Park</h2>
                    <blockquote>
                      <p>The Wild Rose<br />
                        The Journal<br />
                        Western Canada<br />
                        The Chariot Chaser<br />
                        The John Patrick<br />
                        The Spangled Jimmy<br />
                        R. K. (Red) Smith Stakes<br />
                        Ky Alta<br />
                        Princess Margaret<br />
                        Edmonton Juvenile<br />
                        Northlands Oaks<br />
                        Count Lathum<br />
                        The Madamoiselle<br />
                        Fred Jones Stake<br />
                        Sales Stakes Fillies<br />
                        Sales Stakes<br />
                        Sonoma Stakes<br />
                        Canadian Derby<br />
                        Northlands Marathon <br />
                        Edmonton Distaff<br />
                        The Westerner<br />
                        The Bird of Pay<br />
                        The Birdcatcher<br />
                        Speed to Spare<br />
                        Sun Sprint<br />
                        Sturgeon River <br />
                        Premier's Futurity <br />
                        Alberta Oaks <br />
                        Beaufort Stakes <br />
                        Red Diamond Stakes <br />
                        Fall Classic Distaff <br />
                        Breeders' Handicap <br />
                      </p>
                    </blockquote>
            
                <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
