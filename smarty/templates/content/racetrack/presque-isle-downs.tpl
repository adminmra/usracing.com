{assign  var="racetrackname" value="Presque Isle Downs"} 
{assign  var="racetrackpath" value="presque-isle-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p>Presque Isle Downs is located just south of Erie and within 100 miles of Buffalo, New York and Cleveland, Ohio. It opened on February 28, 2007 and is the first synthetic horse racetrack that is longer than 1 mile  in the Northeast and the first racetrack paved with Tapeta (synthetic, all-weather turf for horses) in the United States.</p>
                   
                    <h2>Presque Isle Downs Track Schedule</h2>
                    <p><strong>Live racing: </strong>May  until September <br />
                    <strong>Racing Days:</strong> Sunday to Thursday </p>
                    <h2>Stakes Races at Presque Isle Downs</h2>
                    <blockquote>
                      <p>The Inaugural Stakes<br />
                        The Tom Ridge Stakes<br />
                        The Satin And Lace Stakes<br />
                        The Karl Boyes Memorial<br />
                        Northwestern Pa Stakes<br />
                        The Windward Stakes<br />
                        The Leematt Stakes<br />
                        The Northern Fling Stakes<br />
                        The Malvern Rose Stakes<br />
                        The Mark McDermott Stakes<br />
                        The Presque Isle Mile<br />
                        The Presque Isle Downs Masters Stakes<br />
                        The Fitz Dixon, Jr. Memorial Juvenile Stakes<br />
                        The Presque Isle Debutante<br />
                        
                      </p>
                    </blockquote>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->