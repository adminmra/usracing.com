{assign  var="racetrackname" value="Northfield park"} 
{assign  var="racetrackpath" value="northfield-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2> 
                    <p>Northfield Park was constructed in 1934 by Al Capone for midget car racing. At the time, it was called Sportsman Park which was demolished in 1956 to make way for what would eventually become Northfield Park racetrack, the only remaining commercial harness track in Northeast Ohio. Northfield is 15 miles southeast of Downtown Cleveland.<br>
                        <br>
                        The track's nickname is "The Home of the Flying Turns" and its motto "Every nineteen minutes the place goes crazy."</p>
<h2>Northfield Racetrack Schedule</h2>
                    <p><strong>Live Racing: </strong>Monday, Tuesday, Friday and Saturday<br>
                    <strong>Racing Calendar:&nbsp; </strong>All Year - includes a Summit County Fair date in July<br>
	                 <strong>Course type: </strong>Standardbred harness racing<br>
	                 <strong>Notable Races: </strong>The Courageous Lady, Battle of Lake Erie, Summit County Fair Stakes, Cleveland Classic, Miller Lite Cleveland Classic.<br>
                     <strong>Main track:</strong>&nbsp; 1/2 mile, oval<br>
					 <strong>Distance from last turn to finish line:</strong> 440 feet</p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
