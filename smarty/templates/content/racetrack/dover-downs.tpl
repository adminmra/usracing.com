{assign  var="racetrackname" value="Dover Downs"} 
{assign  var="racetrackpath" value="dover-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address:</strong> 1131 N Dupont Hwy, Dover, DE 19901, United States</p>
                    <p>&nbsp;</p>
                    <p>Located in Dover, Delaware, Dover Downs Racetrack has hosted harness racing events since 1969. It features premier live harness racing from late-October through mid-April.<br>
                        <br>
                      Dover Downs has held major races, including the Dover Downs Progress Pace introduced in 1996. Other stakes events regularly held at Dover Downs include the Matron Series, the Classic Series and the Delaware Standard bred Breeders Fund.</p>
                  
                    <h2>
                      <strong>Dover Downs Racetrack Information:</strong><br>
                    </h2>
                    <p><strong>Live Racing: </strong>November to April<br>
                            <strong>Racing Calendar: </strong>Sunday, Wednesday, Thursday &amp; Saturday <br>
                            <strong>Course type: </strong>Thoroughbred - Flat racing<br>
                            <strong>Notable Races:</strong> Dover Downs Progress Pace, Matron Series<strong><br>
                                                </strong><strong>Main track:</strong> 5/8 mile, bowl-shaped<br>
                            <strong>Distance from last turn to finish line:</strong>&nbsp; 500 feet</p>
        
                    <h2>Dover Downs Stakes Schedule</h2>
                    <blockquote>
                      <p><strong>Progress Pace (Race &amp; Purse)</strong></p>
                      <p>3-Year-Old Pace Elim $35,000<br />
                        3-Year-Old Pace Final $300,000<br />
                        3-Year-Old Pace Consolation $25,000<br />
                                          </p>
                      <p><strong>Delaware Standardbred Breeders Fund (DSBF) (Race &amp; Purse)</strong></p>
                      <p>2-Year-Old Filly Trot Elim 1 $20,000<br />
                        2-Year-Old Colt Trot Elim 1 $20,000<br />
                        2-Year-Old Filly Pace Elim 1 $20,000<br />
                        2-Year-Old Colt Pace Elim 1 $20,000<br />
                        2-Year-Old Filly Trot Elim 2 $20,000<br />
                        2-Year-Old Filly Pace Elim 2 $20,000<br />
                        2-Year-Old Colt Pace Elim 2 $20,000<br />
                        2-Year-Old Filly Trot Final $100,000<br />
                        2-Year-Old Colt Trot Final $100,000<br />
                        2-Year-Old Filly Pace Final $100,000<br />
                        2-Year-Old Colt Pace Final $100,000</p>
                    </blockquote>
                          
     
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
