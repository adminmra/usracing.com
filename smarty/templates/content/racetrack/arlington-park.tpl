{assign  var="racetrackname" value="Arlington Park"} 
{assign  var="racetrackpath" value="arlington-park"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file='/home/ah/allhorse/public_html/racetracks/arlington_park/slider.tpl'} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  


<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p>Arlington Race Park was founded by Harry D. &quot;Curly&quot; Brown and is located in the Chicago suburb of Arlington Heights in Illinois. The track is a major hub for thoroughbred horse racing in the United States and is described as &quot;the most beautiful track in America.&quot;</p>
                    <p>The history of Arlington Park spans more than 80 years. Arlington began   Thoroughbred racing on October 13, 1927, when more than 20,000 fans braved the   cold weather to celebrate the event in high style. In 1981, it was the site of the first thoroughbred race with a million-dollar purse.<br>
                          <br>
                      In addition to hosting some of the most important horse racing events in the North West, Arlington Park is known for introducing many innovations to the horse racing industry. The was the first track that installed a public-address system, in, 1933, which greatly reduced the time between races. In 1936 it added a photo finish camera. Arlington also introduced the first electric starting gate in 1940, the largest closed circuit TV system in all of sports in 1967, the trifecta wagering in 1971.</p>
                      
                      

                    <h2>Arlington Park Racetrack</h2>  
                    <p>{include file='/home/ah/allhorse/public_html/racetracks/arlington_park/map.tpl'} </p>    
                      
                                   <h2>Arlington Park Track Schedule</h2>
                    <p>
                      <strong>Live Racing:</strong> Tuesday, Friday, Saturday and Sunday
                    </p>
                    <p><strong>Live Racing Calendar:</strong> 90 days live racing from May 4th to September 30th</p>
                    <p><strong>Course type:</strong> Flat<br>
                      </p>
                    <p><strong>Notable Races:&nbsp;</strong> Arlington Million, Beverly D. Stakes,&nbsp; Secretariat Stakes,&nbsp; American Derby</p>
                    <p><strong>Main track:</strong> 1 mile and one-eighth dirt oval and a 1 mile turf oval                    </p>
                    <p><strong>Distance from last turn to finish line:</strong> 1,028 feet
                        </p>
                    <p><strong>Turf course:</strong> 7 1/2 furlongs, oval </p>
             
                    <h2>Stakes Races at Arlington Park</h2>
                    <p>The following graded stakes are run at Arlington Park:</p>
                    <p>&nbsp;</p>
                    <blockquote>
                      <p><strong>Grade I</strong><br />
                        Arlington Million<br />
                        Beverly D. Stakes<br />
                        Secretariat Stakes<br />
                      </p>
                      <p><strong>Grade II    </strong><br />
                        American Derby<br />
                      </p>
                      <p><strong>Grade III</strong><br />
                        Arlington Classic Stakes<br />
                        Arlington Handicap<br />
                        Arlington Matron Handicap<br />
                        Arlington Oaks<br />
                        Arlington-Washington Futurity Stakes<br />
                        Arlington-Washington Lassie Stakes<br />
                        Chicago Handicap<br />
                        Hanshin Cup Handicap<br />
                        Modesty Handicap<br />
                        Pucker Up Stakes<br />
                        Sea o'Erin Stakes<br />
                        Stars and Stripes Turf Handicap<br />
                        Washington Park Handicap<br />
                      </p>
                      <p><strong>Listed                      </strong><br />
                        Arlington Sprint Handicap<br />
                        Isaac Murphy Handicap<br />
                        Lincoln Heritage Handicap<br />
                        Round Table Stakes (on hiatus)<br />
                        American 1000 Guineas Stakes</p>
                  
                      </blockquote>
                  
            
        
       
{*
                    <p><strong>Illinois Racetracks:</strong><br />
</p>
                    <ul>
                      <li><a href="/arlington-park">Arlington Park</a>
                      </li>
                      <li><a href="/balmoral-park">Balmoral Park</a>
                      </li>
                      <li><a href="/fairmount-park">Fairmount Park</a> 
                      </li>
                      <li><a href="/hawthorne-race-course">Hawthorne Race Course</a> 
                      </li>
                      <li><a href="/maywood-park">Maywood Park</a></li>
                    </ul>
                 

  
*}
            
            
                      
        
   <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                       
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
