{assign  var="racetrackname" value="Charles Town"} 
{assign  var="racetrackpath" value="charles-town"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
<p><strong>Address</strong>: 750 Hollywood Dr, Charles Town, WV 25414, United States </p>
                    <p>Charles Town Race track and casino is located just outside the eastern city limits of Charles Town, West Virginia. The track opened its doors on December 3, 1933.</p>
                    <p>The facility is home of the West Virginia Breeders' Classic and is one of the busiest thoroughbred tracks in the country with over 240 racing dates scheduled. It&rsquo;s open all year long and runs a 5 days a week schedule (four days per week in July &amp; August and some major holidays.)</p>
                    <p>The first West Virginia Breeders&rsquo; Classics took place in 1987 and has happened annually ever since. A local favorite called Onion Juice won the inaugural running and the state&rsquo;s first $100,000 purse.</p>
                    <p>Charles Town Race track is a renowned entertainment destination for locals and out-of-towners alike. It features a first-class hotel, 5,000 slot machines, 85 table games including Blackjack, Craps, Roulette, live poker, Mini-Baccarat, and Pai Gow Poker, live shows, various special events, and live and simulcast horse racing.</p>
               
                    <h2>Stakes Races at Charles Town Race Track</h2>
                    <blockquote>
                      <p>Coin Collector S. (WV)<br />
                        Robert G. Leavitt S. (WV)<br />
                        Sadie Hawkins S. (WV)<br />
                        Frank Gall Memorial (WV)<br />
                        Sylvia Bishop Mem. (WV)<br />
                        Charles Town Oaks<br />
                        The Researcher<br />
                        Wild &amp; Wonderful<br />
                        Pink Ribbon S.<br />
                        Rachel's Turn S. (WV)<br />
                        Henry Mercer Memorial (WV)<br />
                        Its Only Money S. (WV)<br />
                        WV Breeders' Classics XXVII<br />
                        Tri-State Futurity (R)<br />
                        My Sister Pearl S.(WV)<br />
                        A Huevo Stakes (WV)<br />
                        West Virginia Futurity (WV)<br />
                        Eleanor Casey Mem. (WV)</p>
                      </blockquote>
                  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->