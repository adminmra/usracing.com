{assign  var="racetrackname" value="Rosecroft Raceway"} 
{assign  var="racetrackpath" value="rosecroft-raceway"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{*
 <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Rosecroft Raceway</strong> is located in Washington, Maryland.Rosecroft was nicknamed the &quot;Raceway by the Beltway&quot; for being close to Interstate 495. It is a harness racing track in Fort Washington, Maryland that first opened in 1949. After multi ownership changes, a fire and other events, Rosecroft Raceway closed down in 2010. The next year, Penn National Gaming purchased the racetrack and Rosecroft re-opened in 2011.</p>
               
                    <h2>Rosecroft Raceway Track Schedule</h2>
                    <p><strong>Racing Calendar:</strong> March to June &amp; September to December<br>
                    <strong>Live Racing Schedule:</strong> Tuesday and Saturdays<br>
                    <strong>Post Time:</strong> 7:25 p.m. nightly 
                    </p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->

</div><!-- end/row -->
</div><!-- /#container -->
