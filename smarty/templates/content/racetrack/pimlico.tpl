{assign  var="racetrackname" value="Pimlico"} 
{assign  var="racetrackpath" value="pimlico"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

 {include file='menus/racetracks.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
<div id="main" class="container">
<div class="row">

<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

<p><strong>Pimlico Race Course </strong>is located in Baltimore, Maryland and offers Thoroughbred horse racing from spring to fall each year. It is home of the <a href="/preakness-stakes">Preakness Stakes</a>, the second leg of the <a href="/triple-crown">Triple Crown</a>, after the <a href="/kentucky-derby">Kentucky Derby</a>. The Preakness is held on the third Sunday of May every year.<br />
</p>
<p> Pimlico opened on October 25, 1870. It is  the second oldest racetrack in the nation, next to <a href="/saratoga">Saratoga</a> in New York.
  
  Pimlico has hosted many famous race horses over the years, such as <a href="/famous-horses/manowar">Man o' War</a>, Sir Barton, Citation, California Chrome,<a href="/famous-horses/secretariat">Secretariat</a> and <a href="/famous-horses/seabiscuit">Seabiscuit</a>.</p>
<h2>Pimlico Race Course Schedule</h2>
<p>
  <strong>Live Racing:</strong> Wednesday to Sunday <br>

<strong>Racing Calendar:</strong> May 6th - May 31st<br>
<strong>Course Type:</strong> Flat<br>
<strong>Notable Races:</strong> <strong>Grade 1 Stakes Races</strong>: Preakness Stakes, Pimlico Special. <strong>Grade 2 Stakes Races:</strong> Dixie Stakes, Black-Eyed Susan Stakes, Allaire duPont Distaff Stakes. <strong>Grade 3 Stakes Races:</strong> Gallorette Handicap, Chick Lang Stakes, Miss Preakness Stakes, Maryland Breeders' Cup Sprint Handicap, William Donald Schaefer Handicap.<br>
<strong>Main Track:</strong> One mile oval<br>
<strong>Distance from last turn to finish line:</strong> 1,152 feet<br>
<strong>Turf Course:</strong> 7 furlongs</p>
<h2>Stakes Races at Pimlico</h2>
<blockquote> Twixt Stakes<br />
  Dahlia Stakes<br />
  Mister Diz STakes<br />
  Primonetta Stakes<br />
  Stormy Blue Stakes<br />
  Henry S. Clark Stakes<br />
  Federico Tesio Stakes<br />
  Shine Again Stakes<br />
  Kattegat's Pride Starter Handicap<br />
  Skipat Stakes<br />
  Miss Preakness Stakes<br />
  Black-Eyed Susan Stakes<br />
  Pimlico Special<br />
  Jim McKay Turf Sprint<br />
  The Very One Stakes<br />
  Hilltop Stakes<br />
  Maryland Sprint Handicap<br />
  Deputed Testamony Starter Handicap<br />
  Rollicking Stakes<br />
  The Chick Lang Stakes<br />
  Allaire Dupont Distaff Stakes<br />
  Preakness Stakes<br />
  Gallorette Handicap<br />
  James W. Murphy Stakes<br />
  The Dixie Stakes<br />
  The Canonero II Stakes </blockquote>
            
        <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
