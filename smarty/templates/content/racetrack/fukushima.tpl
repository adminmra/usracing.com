{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/fukushima.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Fukushima Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/stakes/salesblurb-stakes-primary.tpl'} </p>
        {*end*}
        <p>Get your Fukushima Horse Betting action with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
		   {*Secondary Copy*}
		  <h2>Fukushima Racecourse Major Races</h2>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Fukushima Racecourse Major Races"
        summary="Major races for Fukushima Racecourse. Only available at BUSR.">
        <caption>Fukushima Racecourse Major races - Grade I</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fukushima Himba Stakes (Victria Mile Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + f </td>
            </tr>
            <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Radio Nikkei Sho (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
			  <tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Tanabata Sho (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2200m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Nov</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fukushima Kinen (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		  		    <h2>Fukushima Off Track Betting</h2>
		  <p>Built in 1918, this race course is located in Fukushima, Northern Honshu, Japan, and it includes both dirt and turf courses. Fukushima Racecourse features three race meetings per year, with the biggest events being the Fukushima Himba Stakes (G3), Tanabata Sho (G3), the Radio Nikkei Sho (G3), and the Fukushima Kinen (G3).</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
		          {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}