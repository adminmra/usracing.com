{assign  var="racetrackname" value="Penn National"} 
{assign  var="racetrackpath" value="penn-national"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>                    <p><strong>Penn National Race Course</strong> is located in Grantville, Pennsylvania, 17 miles east of Harrisburg, PA. The track opened its doors for racing on August 30 1972.<br>
                          <br>
                      Penn National is owned and operated by Penn National Gaming and is one of the few horse racing facilities in the United States that offers year round live horse racing. In 2013 the inaugural Penn Mile was run on turf with the winning horse being 3-year-old Rydilluc in a time of 1:33.99.</p>
                    <h2>Penn National Race Course Schedule</h2>
                    <p>
                    <strong>Live Racing Schedule:</strong> Wednesday to Sunday<br>
                    <strong>Racing Calendar:</strong> all year<br>
                    <strong>Course Type:</strong> Thoroughbred<br>
                    <strong>Notable Races:</strong> Pennsylvania Governor's Cup, Missy Good Stakes, John J. Shumaker Handicap, Jenny Wade Handicap, Capital City Handicap, Blue Mountain Juvenile Stakes<br>
                    <strong>Main Track:</strong> One mile dirt course<br>
                     <strong>Distance from last turn to finish line:</strong> 990 Feet<br>
                     <strong>Turf Course:</strong> 7 Furlongs, 8 feet</p>
                   
                    <h2>Stakes Races at Penn National</h2>
                    <blockquote>
                      <p>Penn Mile <br />
                        Mountainview Handicap <br />
                        Pennsylvania Governor's Cup <br />
                        Penn Dash <br />
                        E Dubai <br />
                        Silver Train <br />
                        Smarty Jones <br />
                        Wiseman's Ferry <br />
                        Fabulous Strike Handicap <br />
                        Lady in Waiting <br />
                        Swatara <br />
                        Blue Mountain 
                                          </p>
                    </blockquote>
                           <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
