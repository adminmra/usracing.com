{include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/nakayama.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h2 class="kd_subheading">Best Nakayama Off Track Betting Deals & Promotions</h2>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-track-primary.tpl'} </p>
        {*end*}
        <p>Get your Nakayama Horse Betting action with <a href="/signup?ref={$ref2}" rel="nofollow">BUSR</a> now! </p>
        {*The path for the year of the event*}

                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
        {*Second Copy of the page*}

           <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>

           <h2> Nakayama Racecourse Major Races</h2>


		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Nakayama Racecourse Major Races"
        summary="Major races for Nakayama Racecourse. Only available at BUSR.">
     <caption>Nakayama Racecourse Major races - Grade I</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">March/Apr.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fukuryu Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1800m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo c&f </td>
            </tr>
            <tr>
                <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Satsuki Sho (Japanese 2,000 Guineas) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo c&f</td>
            </tr>
			  <tr>
                <td data-title="Month">Sep. / Oct.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Sprinters Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1200m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Arima Kinen (Grand Prix) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2500m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Hopeful Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo c&f</td>
            </tr>
			<br>
			     </tbody>
			  <br>
    </table>
		  <br>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Nakayama Racecourse Major Races"
        summary="Major races for Nakayama Racecourse. Only available at BUSR.">
			<caption>Nakayama Racecourse Major races - Grade II</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> American Jockey Club Cup </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2200m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo f </td>
            </tr>
            <tr>
                <td data-title="Month">Feb. / Mar.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Nakayama Kinen</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
			  <tr>
                <td data-title="Month">Mar.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> 	Yayoi Sho (Satsuki Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;">3yo + f</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Spring Stakes (Satsuki Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo c&f</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Nikkei Sho (Tenno Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2500m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Apr.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> New Zealand Trophy (NHK Mile Cup Trial)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo c&f</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> St. Lite Kinen (Kikuka Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2200m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
            </tr>
             <tr>
                <td data-title="Month">Sep.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> All Comers (Tenno Sho Trial) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2200m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
            <tr>
                <td data-title="Month">Dec.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Stayers Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 3600m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
        </tbody>
    </table>
		  <br>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Nakayama Racecourse Major Races"
        summary="Major races for Nakayama Racecourse. Only available at BUSR.">
			<caption>Nakayama Racecourse Major races - Grade III</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Distance </th>
                <th> Age/Sex </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Nakayama Kimpai (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
            <tr>
                <td data-title="Month">Jan.</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fairy Stakes </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
			  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;">Ocean Stakes (Takamatsunomiya Kinen Trial)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1200m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;">4yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Flower Cup </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Nakayama Himba Stakes (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1800m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo+ f</td>
            </tr>
				  <tr>
                <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> March Stakes (Handicap) </td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1800m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Apr.</td>
                <td data-title="Race" style="word-break: keep-all !important;">Lord Derby Challenge Trophy (Handicap)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo +</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep.</td>
                <td data-title="Race" style="word-break: keep-all !important;">Shion Stakes (Shuka Sho Trial)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 2000m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
            </tr>
				  <tr>
                <td data-title="Month">Sep.</td>
                <td data-title="Race" style="word-break: keep-all !important;">	Keisei Hai Autumn Handicap (Handicap)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m</td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
            <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;">Turquoise Stakes (Handicap)</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Turf 1600m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo+ f</td>
            </tr>
            <tr>
                <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;">Capella Stakes</td>
                <td data-title="Distance" style="word-break: keep-all !important;"> Dirt 1200m </td>
                <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo +</td>
            </tr>
        </tbody>
    </table>
            &nbsp;
        {*end*}
        <br>
        </li>
        </ul>
            <h2>Nakayama Off Track Betting</h2>
           <div class="racetrack-text">
         <p>Built in 1920, Nakayama Racecourse is located in Funabashi, Chiba, Japan, and it features a capacity for 165,676 people. It features two grass courses, a dirt one, and a jump course. The jump course on this track is quite unique and flexible as it can be fixed to accomodate different configurations. </p>
            </div>
   <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
      </div>
    </div>
    </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}