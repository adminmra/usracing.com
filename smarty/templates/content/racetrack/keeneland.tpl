{* include file="/home/ah/allhorse/public_html/usracing/schema/racetrack/Pompano-park.tpl" *} {* to delete afer generic track schema made.  see top of page blocks*}
{include file="/home/ah/allhorse/public_html/usracing/racetracks/top-of-page-blocks.tpl"}
      
{************PAGE SPECIFIC COPY*****************************************}        

<h1 class="kd_heading"></h1>{*This h1 is changed in the xml with the tag h1*}
<div class="racetrack-text">
    <p>
        <strong>Live Racing:</strong> Thursday to Sunday<br>
        <strong>Live Racing Calendar:</strong> April through October<br>
        <strong>Course type:</strong> Flat/Thoroughbred<br>
        <strong>Main track:</strong>1 1/6 mile, oval<br>
        <strong>Distance from last turn to finish line:</strong> 1,147 feet<br>
        <strong>Turf course:</strong>7 1/2 furlongs
    </p>
</div>
<h2> Keeneland - Major Races</h2>  
<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Keeneland Major Races" summary="Major races for Keeneland">
    <caption>Keeneland Major races - Grade 1</caption>
    <thead>
        <tr>
            <th> Month </th>
            <th> Race </th>
            <th> Distance </th>
            <th> Age/Sex </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Alcibiades Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> 1 1⁄16 miles (8.5 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f</td>
        </tr>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Ashland Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 1⁄16 miles (8.5 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Futurity Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ ​1 1⁄16 miles (8.5 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> First Lady Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ ​1 mile (8 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m </td>
        </tr>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Jenny Wiley Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 1⁄16 miles (8.5 furlongs)</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + f&m </td>
        </tr>
    <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Madison Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​7 furlongs</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + f&m </td>
        </tr>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Maker's 46 Mile Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 mile (8 furlongs)</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 4yo + </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Queen Elizabeth II Challenge Cup Stakes </td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 1⁄8 miles (9.0 furlongs)</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Shadwell Turf Mile Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 1⁄8 miles (9.0 furlongs)</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Spinster Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ ​1 1⁄8 miles (9 Furlongs)</td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m</td>
        </tr>
    </tbody>
</table>
<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0" cellspacing="0" border="0" title="Keeneland Major Races" summary="Major races for Keeneland. Only available at BUSR.">
    <caption>Keeneland Major races - Grade ll</caption>
    <thead>
        <tr>
            <th> Month </th>
            <th> Race </th>
            <th> Distance </th>
            <th> Age/Sex </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Appalachian Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;">  1 mile (8 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f</td>
        </tr>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Blue Grass Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;">  ​1 1⁄8 miles (9 Furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Fayette Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;">  ​ ​1 1⁄8 miles (9 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Jessamine Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;">  ​ ​​1 1⁄16 miles (8.5 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 2yo f </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Marathon Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​1 3⁄4 miles (14 furlongs) </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Phoenix Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ 6 furlong sprint </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Raven Run Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ 7 furlongs </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo f </td>
        </tr>
        <tr>
            <td data-title="Month">Apr</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Shakertown Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ 5.5 furlongs </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
            <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Thoroughbred Club of America Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​ 6 furlongs </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + f&m </td>
        </tr>
        <tr>
            <td data-title="Month">Oct</td>
            <td data-title="Race" style="word-break: keep-all !important;"> Woodford Stakes</td>
            <td data-title="Distance" style="word-break: keep-all !important;"> ​   ​5 1⁄2 furlongs </td>
            <td data-title="Age/Sex" style="word-break: keep-all !important;"> 3yo + </td>
        </tr>
    </tbody>
</table>

<h3>Keeneland Horse Racing History</h3>
<p>Keeneland is a top-notch horse racing track located in Lexington, Kentucky which is also known for its reference library on the sport. The racetrack hosted its first day of live racing on October 15, 1936. Keeneland was designated a National Historic Landmark in 1986.</p>
<p>The track has received many great honors over the years, including being named by the Horseplayers Association of North America (HANA) as the continent's top racetrack, for the 3rd consecutive year, in 2011. That same year, Keeneland earned its fourth International Simulcast Award, which honors horse racing's best simulcast production. Keeneland also won the award in 2003, 2008 and 2009.</p>
<p>In 2008, Keeneland became the first racetrack in North America to provide live race coverage and limited simulcasts in high-definition format. Keeneland was used to shoot most of the racing scenes of the 2003 movie Seabiscuit</p>
  
		  
{************ End PAGE SPECIFIC COPY*****************************************}		 
 {include file="/home/ah/allhorse/public_html/usracing/racetracks/end-of-page-blocks.tpl"}