{assign  var="racetrackname" value="Remington Park"} 
{assign  var="racetrackpath" value="remington-park"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                    <p><strong>Remington Park</strong> is located in Oklahoma City, Oklahoma. Built in 1988, it offers betting on Quarter Horse and Thoroughbred horse racing. </p>
                    <p>The main dirt track at Remington Park is one mile in circumference with the inner turf course measuring seven furlongs. </p>
                  
                  
                    <h2>Remington Park Track Schedule</h2>
                    <p><strong>Live Racing Dates:</strong> August 16-December 15<br>
	                    <strong>Live Racing Schedule:</strong> Wednesday through Saturday<br>
	                    <strong>Post Time:</strong>  7pm in August and 6:30 from September to December </p>
              
                    <h2>Stakes Races at Remington Park</h2>
                    <blockquote>
                      <p>Governor's Cup<br />
                        Red Earth Stakes<br />
                        Edward J. Debartolo Memorial Handicap<br />
                        Ricks Memorial Stakes<br />
                        Remington Park Turf Sprint Stakes<br />
                        Tishomingo Stakes<br />
                        Te Ata Stakes<br />
                        Ladies on the Lawn<br />
                        Remington Park Oaks<br />
                        Kip Deville Stakes<br />
                        Remington Park Sprint Cup<br />
                        Oklahoma Derby<br />
                        Flashy Lady Stakes<br />
                        Oklahoma Classics Distaff<br />
                        Oklahoma Classics Turf<br />
                        Oklahoma Classics Distaff Turf<br />
                        Oklahoma Classics Sprint<br />
                        Oklahoma Classics Distaff Sprint<br />
                        Oklahoma Classics Juvenile <br />
                        Oklahoma Classics Lassie<br />
                        Oklahoma Classics Cup<br />
                        E L Gaylord Memorial Stakes<br />
                        Clever Trevor Stakes<br />
                        Silver Goblin Stakes<br />
                        Oklahoma Stallion Stakes<br />
                        Useeit Stakes<br />
                        Jim Thorpe Stakes<br />
                        The Trapeze<br />
                        Remington Springboard Mile</p>
                    </blockquote>
            <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
