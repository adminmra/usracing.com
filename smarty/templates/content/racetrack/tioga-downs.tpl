{assign  var="racetrackname" value="Tiogo Downs"} 
{assign  var="racetrackpath" value="tioga-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
             
                    <p><strong>Tioga Downs</strong> was originally a quarterhorse track known as &quot;Tioga Park&quot; that opened in 1976 but closed down due to financial reasons after  its third season. It re-opened on June 9, 2006 offering harness racing, simulcast betting, and over 800 Video Lottery Terminals. Some popular races at the track include, the Grand Circuit Tompkins Geers, The $300,000 Cane Pace (1st leg of Pacing's Triple Crown) and it's lady counterpart The Shady Daisy. </p>

<h2>Tioga Downs Track Schedule</h2>
                    <p><strong>Live Racing Dates</strong>: May to September<br />
                    <strong>Live Racing Schedule:</strong> Friday and Saturday evenings (6:50 PM), and 1:30 PM on Sundays.<br />
                    <strong>Track: </strong>7/8 mile harness racetrack<br />
                    <h2>Stakes Races at Tioga Downs</h2>
                    <blockquote>
                      <p>Kindergarten Trotting Classic<br />
                        Empire Breeders Classic<br />
                        Miss Versatility</p>
                    </blockquote>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
