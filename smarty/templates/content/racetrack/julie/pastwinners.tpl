{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Special Races</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/royalascot" title="">ASCOT</a></li>
<li><a href="/breederscup" title="">BREEDERS&#039; CUP</a></li>
<li><a href="/dubaiworldcup" title="">DUBAI WORLD CUP</a></li>
<li><a href="/hongkongcup" title="">HONG KONG CUP</a></li>
<li><a href="/kentuckyoaks" title="">KENTUCKY OAKS</a></li>
<li><a href="/melbournecup" title="">MELBOURNE CUP</a></li>
<li><a href="/prixdelarcdetriomphe" title="">PRIX DE L&#039;ARC DE TRIOMPHE</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Saratoga Pastwinners</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include_php file='../smarty/libs/saratoga/pastwinners.php'}  
    
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    