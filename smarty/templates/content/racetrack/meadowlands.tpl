{assign  var="racetrackname" value="Meadowlands"} 
{assign  var="racetrackpath" value="meadowlands"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>

              
                    <p><strong>Meadowlands Racetrack</strong> is located at the MetLife Sports Complex in East Rutherford, New Jersey which consists of:</p>
                    <ul>
                      <li>American Dream Meadowlands, a proposed retail center and entertainment complex<
                        </li>
                      <li>Izod Center
                        </li>
                      <li>Meadowlands (NJT station)
                        </li>
                      <li>Meadowlands Racetrack
                        </li>
                      <li>MetLife Stadium - site of the first outdoor, cold weather Superbowl with Super Bowl XLVIII in 2014.</li>
                      </ul>
                    <p>The racetrack is often referred to as &quot;The Big M&quot; and held its first harness race in1976 and thoroughbred racing started the following year. The track is operated by the New Jersey Sports and Exposition Authority, also the operator of <a href="/monmouth-park">Monmouth Park</a>.</p>
                    <p>The Meadowlands Racetrack has hosted the second leg of the Trotting Triple Crown, the Hambletonian, since 1981. The track is equipped for night races. The world record for a mile by a standard bred set by Holborn Hanover (1:46), was established at the track on August 5, 2006 in a division of the U.S. Pacing Championship.</p>
                                        </ul>
                    <h2>Meadowlands Live Racing Schedule</h2>
                    <p>
                    <strong> Live Racing:</strong> Tuesday through Saturday<br>
                    <strong>Racing Calendar:</strong> Live racing usually run from October to December <br>
                    <strong>Post Time   Schedule:</strong> 1 - 7:15 pm, 2 - 7:34 pm, 3 - 7:54 pm, 4 - 8:13 pm, 5 - 8:32 pm, 6 - 8:51 pm, 7 - 9:11 pm, 8 - 9:30 pm, 9 - 9:49 pm, 10 - 10:08 pm, 11 - 10:27 pm, 12 - 10:46 pm, 13 - 11:05 pm<br>
                    <strong>Course type:</strong> Flat / Thoroughbred<br>
                    <strong>Notable Races:&nbsp;</strong>  Meadowlands Cup (G2), Cliff Hanger Stakes (G3), Pegasus Stakes (G3), Violet Handicap (G3)<br>
                   <strong>Main track (dirt):</strong> 1 mile oval<br>
                    <strong>Distance from last turn to finish line:&nbsp;</strong> 990 feet<br>
                      <strong>Turf course:</strong> 7 furlongs</p>
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
