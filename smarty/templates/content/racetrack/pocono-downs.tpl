{assign  var="racetrackname" value="Pocono Downs"} 
{assign  var="racetrackpath" value="pocono-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/racetracks/$racetrackpath/slider.tpl"} </p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>


                    <p><strong>Pocono Downs</strong> is located on 400 acres on the hillside of Plains, Pennsylvania. It is within   easy access of New York, Philadelphia, New Jersey and Delaware. Pocono Downs is a Harness racing track and is a member of the U.S. Trotting Association.</p>
                           
                    <h2>Pocono Downs Track Schedule</h2>
                    <p><strong>Live Racing: </strong>March  through November<br />
                   <strong>Post Time:</strong> of 6:30pm<br />
						<strong>Racing Days:</strong> Tuesday, Wednesday, Friday &amp; Saturday in August-November
                    </p>

                    <h2>Stakes Races at Pocono Downs</h2>
                    <blockquote>
                      <p>Bobby Weiss Series<br />
                        Hudson River Series<br />
                        Pennsylvania Sire Stakes<br />
                        Pennsylvania Stallion Series<br />
                        Max Hempt Memorial<br />
                        James Lynch Memorial<br />
                        Earl Beal Jr Memorial<br />
                        Ben Franklin<br />
                        Pennsylvania All-Stars<br />
                        Battle Of The Brandywine<br />
                        Stallion Series<br />
                        Breeders Crown</p>
                    </blockquote>
 <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->