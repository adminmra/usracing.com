
{assign  var="racetrackname" value="Fresno"} 
{assign  var="racetrackpath" value="fresno"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                   {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
<div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

                    <p>Live horse racing is held once a year in the Brian I. Tatarian Grandstand at The Big Fresno Fair. The meet happens in October annually. the Big Fresno Fair was established in 1882 and in 1941, Fresno offered its first pari-mutuel horse-racing meeting. Racing is held daily except 2-3 days (usually Monday and Wednesday) during the meet.                      It's traditionally the final stop on the Northern California Fair Circuit.
                    </p>
                  {*
  <p><strong>California Racing Fairs </strong></p>
                    <ul>
                      <li><a href="/pleasanton">Pleasanton</a><br />
                      </li>
                      <li><a href="/sacramento">Sacramento </a><br />
                      </li>
                      <li><a href="/ferndale">Ferndale</a> <br />
                      </li>
                      <li><a href="/stockton">Stockton </a><br />
                      </li>
                      <li><a href="/fresno">Fresno </a></li>
                    </ul>
*}
       
                    <h2>Stake Races at Fresno</h2>
                    <blockquote>
                      <p><br />
                        Bulldog Handicap<br />
                        Charlie Palmer Futurity <br />
                            <br />
                      </p>
                    </blockquote>
            
 
       
<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
