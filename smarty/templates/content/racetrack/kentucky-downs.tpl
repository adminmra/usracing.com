{assign  var="racetrackname" value="Kentucky Downs"} 
{assign  var="racetrackpath" value="kentucky-downs"} 
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

 <div class="headline"><h1>{$racetrackname} Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/calltoaction_text.tpl"} </p><br>
  <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/odds_iframe.tpl"} </p>    <br>   

<h2>{$racetrackname} Racing</h2>
                  
                    <p>Opened in 1990, <strong>Kentucky Downs</strong> racetrack is located in Franklin, Kentucky. The track is a European-style course which means the surface is all turf (grass) not dirt, and it is not oval in shape. When it first opened, it was called Dueling Grounds Race Course. It wasn't until 1997 that it was named Kentucky Downs when it was purchased at auction by Turfway Park, Churchill Downs and other investors. </p>
                    <p>An interesting fact about Kentucky Downs is that its one of two tracks that uses a right turn, the other is <a href="/santa-anita-park">Santa Anita Park</a> in California.<br>
                            <br>
                      In 2009, Kentucky Downs was ranked #2 out of 65 thoroughbred racetracks in North America by the Horseplayers Association of North America.</p>
             
                    <h2><strong>Kentucky Downs Racetrack Information</h2>
                    <p>
	                <strong>Live Racing: </strong>September<br>
                     strong>Racing Calendar: </strong>Saturdays, Mondays and Wednesdays<br>
                    <strong>Course type: </strong>Thoroughbred<br>
                    <strong>Notable Races:</strong> Kentucky Cup Turf Dash Stakes, Kentucky Cup Ladies Turf Stakes, Kentucky Cup Turf Stakes (Grade III), Franklin-Simpson Stakes, Belle Meade Plantation Stakes<br>
                    <strong>Main track:</strong> 1 &amp; 3/8 mile European Style<br>
                    <strong>Distance from last turn to finish line:</strong>&nbsp; 1,125 feet<br>
                    <strong>Turf course:</strong> Six Furlongs</p>
                   <p>{include file="/home/ah/allhorse/public_html/usracing/racetracks/marketing_inc.tpl"} </p>                         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/racetracks/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
