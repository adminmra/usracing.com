{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li class="leaf active-trail"><a href="/flash/howtobetonsports" title="" class="active">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>LEARN HOW TO PLACE A SPORTS BET</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{literal}<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>{/literal}



<div style="padding: 20px 0 0 90px;" id="flashmovie1">bettingTutorials Flash Movie not loaded</div>

{literal}<script type="text/javascript">		
var so = new SWFObject("http://www.usracing.com/flash/bettingTutorial.swf", "bettingTutorial", "520", "300", "8", "#FFFFFF"); 
  so.addParam("allowscriptaccess","always"); so.useExpressInstall("http://www.usracing.com/flash/expressinstall.swf"); 
  so.addParam("wmode", "transparent");
  so.write("flashmovie1");	
</script>{/literal}

<div class="tabcontent">
<p>Sports Betting is something that millions of people enjoy daily.  Whether it is a friendly wager on if your team will win the Super Bowl or a punt on your choice for the Best Actor Oscar<sup><small>TM</small></sup>  at the Academy Awards, US Racing offers its members thousands of different wagers every day. Our Sports Betting Tutorial will help you understand how to bet on sports.</p>

<p><span class="mediumtext">Tours require a Flash Player plug in. Having problems viewing the tutorials?</span></p>

<p>Download the latest Flash Player plug-in.</p>

<p><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank"><img title="Flash Player" border="0" alt="Flash Player" src="/themes/images/get_flash_player.gif" /></a></p>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    