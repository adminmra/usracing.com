<!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>

 </div>

<!--------- NEW HERO END ---------------------------------------->   
     

<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          



<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->
<div class="headline"><h1>Simon Says $25 New Member Bonus</h1></div>



<p>
	Our Sponsor BUSR has a great  new member bonus for you.   When you sign up  and deposit at least $50, you'll be given another $25 absolutely free instantly!   </p>
	
	<p>With BUSR not only do you get a exceptional welcome bonus, you can:
		
		<ul>
			<li>bet on fixed horse racing odds,</li>
			<li>bet on early odds for the Kentucky Derby,</li>
			<li>bet on football or your favorite sport or event,</li>
			<li>play blackjack or your favorite casino games,</li>
			<li>and best of all, get up to an 8% rebate on all racebook wagers, win or lose!</li>
		</ul>
			
			
	
	
	<h3>Please note:</h3>
	
	
<p> Claim your $25 Cash is for new members only. A minimum deposit of $50 is required. A 7X rollover applies to the bonus cash. Cannot be combined with any other signup bonuses. Please use the PROMO CODE found on the card in the cashier or call BUSR at 1-844-BET-HORSES and provide the PROMO CODE over the telephone.  </p>

 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   


<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
<div class="calltoaction">


<h2>Signup for my  Account</h2>
<h3>And Get $25 Free</h3>
{include file='inc/rightcol-calltoaction-form.tpl'} 


{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
