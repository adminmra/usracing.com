{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

<link rel="stylesheet" type="text/css" href="/assets/css/rs_v5.css" media="screen">
<!--------- NEW HERO - MAY 2017 ---------------------------------------->
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
{*         <span>{$h1_line_2}</span> *}
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
    {*     <span>{$h2_line_2}</span> *}
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
{*         <span>{$h2_line_2_sm}</span> *}
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">
<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->
 <!--<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  -->
{include file='/home/ah/allhorse/public_html/shared/promos/20-cash-bonus-usr.php'} 
<br><br>

 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   


<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->
<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
