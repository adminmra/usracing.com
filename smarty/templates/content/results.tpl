{literal}
	<style>
		.equibase {
			cursor: pointer;
		}

		#equibase {
			/* float: left;
			margin-right: 10px;
			margin-top: 10px; */
			margin-bottom: 5px;
		}
		#equibase img {
			width: 170px;
		}
	</style>
{/literal}
<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   
<div id="main" class="container">
<div class="row">

<div id="left-col" class="col-md-9">


<div class="headline"><h1>US Horse Racing Results</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>For the official horse race results of racetracks in North America, you'll be able to find them with the button below:</p>


{*
<div id="equibase" class="equibase">
	<img src="/img/equibase-logo.jpg" alt="equibase-logo">
</div>
*}
<p style="margin-top: 50px; margin-bottom: 50px;" align="center"><a href="https://www.google.com/search?q=horse+racing+results" class="btn-xlrg " target="_blank">Horse Racing Results</a></p>

{* <a href="http://nullrefer.com/?https://www.equibase.com/static/chart/summary/index.html?SAP=TN" target="_blank"> <img src="/img/equibase-logo.jpg" alt="equibase-logo"></a>  *}

{*
<p>Founded in 1990, Equibase is a partnership of The Jockey Club and various racing groups in the United States and Canada.  There are several free services and products available at Equibase for the casual and professional horse player.  If you love The Game, we encourage you to visit Equibase.</p>
<p>To check out today’s horse racing results, click the Equibase logo above.</p>
<p>As always, good luck with your bets!</p>
*}
<div class="headline"><h3>To Bet on Horses</h3></div>
{* <div style="width:100%; height: 1px; background-color: #e8e8e8; margin-bottom: 1.7em;"></div> *}
{assign var="cta_append" value=" at any of these races"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

{*include file='includes/ahr_racing_results_page.tpl'*}
<!-- <p>To get back to the the results home <a href="/results">Click Here</a></p>-->

{* <!--<IFRAME src="https://www.drf.com/race-results?bets=true"rel="nofollow"  and="" scrolling="yes" seemless="" frameborder="0" height="699" width="533">--> *}


 <p align="center" style="margin-top: 50px; margin-bottom: 50px;"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

{* <h2 align="center">Race Results Page, Maintenance in Progress</h2> *}
{* <IFRAME src="https://www.betusracing.ag/iframe/results/" scrolling="yes" seemless="" frameborder="0" height="699" width="533" style="width: 100%;height: 100rem;"></IFRAME> *}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}



</div><!-- end: #right-col -->


</div><!-- end/row -->
</div><!-- /#container -->


{literal}
	<script>
		document.querySelectorAll('.equibase').forEach(function(elem) {
			elem.onclick = function() {
				window.open("http://nullrefer.com/?https://www.equibase.com/static/chart/summary/index.html?SAP=TN","cnn","height=768,width=767,toolbar=no,scrollbars=no,resizable=no,menubar=no,screenX=1140,screenY=0");
			}
		});
	</script>
{/literal}
