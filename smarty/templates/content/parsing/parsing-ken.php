<?php
if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/scrappers/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';
class parsing extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $data;
    private $mainTitle;
    private $tracklist=array("ajx"=>"Ajax Downs",
                            "alb"=>"Albuquerque",
                            "arp"=>"Arapahoe Park",
                            "ap"=>"Arlington",
                            "asd"=>"Assiniboia Downs",
                            "btp"=>"Belterra Park",
                            "cmr"=>"Camarero Race Track",
                            "cby"=>"Canterbury Park",
                            "ct"=>"Charles Town",
                            "dmr"=>"Del Mar",
                            "del"=>"Delaware Park",
                            "elp"=>"Ellis Park",
                            "emd"=>"Emerald Downs",
                            "evd"=>"Evangeline Downs",
                            "fmt"=>"Fair Meadows Tulsa",
                            "fp"=>"Fairmount Park",
                            "fl"=>"Finger Lakes",
                            "fe"=>"Fort Erie",
                            "gpr"=>"Grand Prairie",
                            "gf"=>"Great Falls",
                            "gp"=>"Gulfstream Park",
                            "hst"=>"Hastings Park",
                            "ind"=>"Indiana Downs",
                            "lrl"=>"Laurel",
                            "la"=>"Los Alamitos (Quarter Horse)",
                            "lad"=>"Louisiana Downs",
                            "md"=>"Marquis Downs",
                            "mth"=>"Monmouth Park",
                            "mnr"=>"Mountaineer Park",
                            "far"=>"North Dakota Horse Park",
                            "np"=>"Northlands Park",
                            "prx"=>"Parx Racing",
                            "pen"=>"Penn National",
                            "prm"=>"Prairie Meadows",
                            "pid"=>"Presque Isle Downs",
                            "ret"=>"Retama Park",
                            "rui"=>"Ruidoso Downs",
                            "sac"=>"Sacramento",
                            "sar"=>"Saratoga",
                            "tdn"=>"Thistledown",
                            "wo"=>"Woodbine",
                            "wyo"=>"Wyoming Downs"
    );
    private $storage_folder="/home/ah/pastperformance/pps_script/storage/xml/";
    private $trackArray;
    function __construct(){
        parent::__construct();
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "/home/ah/usracing.com/htdocs/assets/js/ppsdata.js"; //the absolute path of the file to be written
        $this->data           = array();
        $this->mainTitle      = "";
        $this->maxDate        = 3;
        $this->trackArray     = array();
    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){}
    public function getDateString($str,$input,$output){
        $myDateTime = DateTime::createFromFormat($input, $str);
        return $myDateTime->format($output);
    }
    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) {
            print( $e->getMessage() );
        }
    }
    public function process(){
            $this->getCurrentData();
            $this->feedData();
    }
    public function getCurrentData(){
        $i = 0;
        for ( $count = 0; $count < $this->maxDate ;) { 
            $__date = date("Ymd",time() - 60 * 60 * 24 * $i);
            foreach ( $this->tracklist as $k=>$v ) { 
                $file = $this->storage_folder.$k.$__date."ppsXML.xml";
                if ( file_exists ( $file ) ) {
                    $xml = file_get_contents($file); 
                    if ( !isset ( $this->trackArray[$__date] ) ) $this->trackArray[$__date] = array();
                    $this->trackArray[$__date][] = $this->process_track_content($xml,$v);
                }
            }
            $count = count ( $this->trackArray);
            $i++;
        }
    }
    private function process_track_content($xml,$trackName){
        $track = array();
        $track['trackname'] = $trackName;
        $track['trackrace'] = $this->process_track($xml);
        return $track;
    }
    private function process_track($xml_string){
        $data = array();
        $xml = simplexml_load_string($xml_string);
        if ( count ( $xml)> 0 ) {
            $races = json_decode(json_encode($xml[0]), TRUE)['racedata'];
            foreach ( $races as $race ) {  
                $data[] = $this->process_race_data ( $race ) ;   
            }
        }
        return $data;
    }
    private function process_race_data ( $race ) {
        $output = array();
        $output['horsedata'] = $this->process_horse_data ( $race['horsedata'] );
        $output['distance'] = $race['distance'];
        $output['purse'] = $race['purse'];
        $output['claimamt'] = $race['claimamt'];
        $date = DateTime::createFromFormat('Ymd',$race['race_date'] );
        $output['race_date'] = $date->format('d-m-Y');
        $output['dist_unit'] = $race['dist_unit'];
        $output['bet_opt'] = $race['bet_opt'];
        $output['race'] = $race['race'];
        return $output;
    }
    private function process_horse_data ($horses){
        $output = array();
        foreach ( $horses as $horse ) { 
            unset ( $horse['stats_data'] ) ;
            unset ( $horse['workoutdata'] ) ;
            unset ( $horse['ppdata'] );
            $output[] = $horse;
        
        }
        return $output;
    }
    private function feedData(){
        $output = "";
        ob_start();
        ?>
            var trackList = <?php echo preg_replace ( '/#/','' ,preg_replace ( '/@/' , '',json_encode ( $this->trackArray ,1)));?>
        <?php
        $output = ob_get_clean();
        return $this->writeOutputXP($output);

    }
     private function writeOutputXP($contents){
        if($contents == ""){
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH)){
                $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
            }
            else{
                $contents = file_get_contents($this->OUTPUTFILEPATH);
                if($contents !== NULL){
                    $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
                }

                return false;
            }
        }
        else{
            $this->as_file_save_data( $contents , $this->OUTPUTFILEPATH );
        }

    }

}
parsing::run();
?>
