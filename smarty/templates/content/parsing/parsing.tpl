 <div class="table-responsive" race="1" race_date="20180317" claimamt="32000" dist_disp="6F" todays_cls="98" purse="50000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #1</span> - 
<span> Claiming $32000</span> - 
<span> Distance 6F</span> - 
<span> Todays CLS 98</span> -
<span> Purse $50000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Pick 3 Races (1-3), Pick 5 Races (1-5) Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="2" race="1" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> FULL SALUTE</td>
<td> Carmouche Kendrick</td>
<td> Gargan Danny</td>
<td> Lakland Farm</td>
<td> Morris Bailey</td>
<td> Speightstown</td>
<td> Mazucambera</td>
<td> 5/2</td></tr><tr class="horse_2_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_2_1" style="display:none;"><td COLSPAN=9>
 Tr: St 9 W 22% P 0% S 0% ITM 22% ROI -33</td></tr><tr  class="horse_2_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/25/2018 - 7,    AQU,    FT,    52,    F,    52,    (T)-1,    CLM,    40000,    103,    85,    76,    2,    1,    2,    1,    150.00,    2,    7,    5,    16,    6,    Carmouche Kendrick,    L,    119,    6.50,    Daddy D T,    Shalako,    J S Bach,    RANK,GOT OUT BIT,2W1/2,    7,    </div></br> 
<div style="color:red" > 12/23/2017 - 5,    AQU,    SY,    55,    F,    55,    (T)-1,    AOC,    62500,    106,    100,    94,    3,    2,    6,    3,    550.00,    5,    7,    5,    11,    5,    Figueroa Nicky,    L,    115,    24.50,    Harlan Punch,    Gift Box,    Pioneer Spirit,    3w 1/2,5w upper,tired,    7,    </div></br> 
<div style="color:blue" > 12/22/2017 - 8,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    111,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 10/08/2017 - 8,    LRL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    99,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 09/21/2017 - 6,    BEL,    FT,    54,    F,    54,    (T)-1,    CLM,    50000,    104,    71,    97,    5,    3,    3,    2,    260.00,    3,    3,    4,    4,    4,    Lezcano Jose,    L,    120,    14.20,    Very Very Stella,    Cerro,    Bolita Boyz,    3w,chase duel,empty,    7,    </div></br> 
<div style="color:red" > 09/16/2017 - 5,    LRL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    116,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 08/16/2017 - 6,    SAR,    FT,    34,    F,    34,    (T)-1,    CLM,    32000,    101,    67,    92,    1,    3,    1,    -1,    -50.00,    1,    -4,    1,    -1,    1,    Ortiz Jose L,    L,    120,    3.65,    Full Salute,    Helooksthepart,    Paden,    duel inside, held late,    8,    </div></br> 
<div style="color:red" > 07/31/2017 - 5,    SAR,    FM,    118,    F,    118,    (T)12,    CLM,    40000,    106,    92,    88,    6,    5,    5,    3,    410.00,    6,    6,    7,    7,    8,    Saez Luis,    L,    120,    9.50,    Shadow Rock,    Aktabantay,    Padilla,    near inside,no bids,    9,    </div></br> 
<div style="color:blue" > 06/18/2017 - 10,    BEL,    MY,    45,    F,    45,    (T)-1,    CLM,    25000,    98,    137,    101,    1,    5,    4,    2,    160.00,    4,    -1,    1,    -2,    1,    Ortiz Jose L,    L,    120,    3.45,    Full Salute,    Saratoga Wildcat,    Paid Admission,    ins,2p5/16,bid 5w upr,    8,    </div></br> 
<div style="color:red" > 05/12/2017 - 1,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    88,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 03/17/2017 - 7,    GP,    FT,    20,    F,    20,    (T)-1,    CLM,    30000,    96,    59,    88,    6,    5,    4,    3,    110.00,    3,    -1,    1,    2,    3,    Ortiz Jose L,    L,    121,    8.90,    Picadilly Roadster,    Grand Nene,    Full Salute,    wide,no closing kick,    7,    </div></br> 
<div style="color:red" > 03/02/2017 - 9,    GP,    FM,    261,    F,    261,    (T)96,    CLM,    50000,    100,    60,    79,    7,    3,    5,    2,    280.00,    6,    6,    6,    10,    6,    Lopez Paco,    L,    121,    5.00,    Stockyard,    Fire On Fire,    Derby Champagne,    2nd flight btwn,faded,    7,    </div></br> 
</td> </tr>
<tr  class="horse_2_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 101  ( date: 2017 ) 22   7   3   1   $333245.00    </br> 
  2017 : 8   2   0   1   $70510.00    </br> 
  2018 : 1   0   0   0   $550.00    </br> 
  GP: 2   0   0   0   $2620.00    </br> 
  Turf: 5   1   1   0   $46910.00    </br> 
  Dist: 10   4   0   1   $194305.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="1" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> WILL DID IT</td>
<td> McCarthy Trevor</td>
<td> Toscano Jr John T</td>
<td> Aaron Jones & Marie Jones</td>
<td> Drawing Away Stable</td>
<td> Medaglia d'Oro</td>
<td> Bonnie Blue Flag</td>
<td> 9/2</td></tr><tr class="horse_3_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_3_1" style="display:none;"><td COLSPAN=9>
 Tr: St 24 W 8% P 21% S 21% ITM 50% ROI -41</td></tr><tr  class="horse_3_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/10/2018 - 7,    AQU,    MY,    30,    F,    30,    (T)-1,    STR,    20000,    104,    45,    93,    1,    2,    5,    5,    410.00,    4,    4,    4,    5,    4,    Mccarthy Trevor,    L,    118,    3.95,    Very Very Stella,    Holland Road,    Major League,    INS,3W ANGLE 5/16-3/16,    5,    </div></br> 
<div style="color:red" > 01/21/2018 - 6,    AQU,    FT,    40,    F,    40,    (T)-1,    CLM,    32000,    102,    8,    81,    7,    7,    7,    13,    710.00,    7,    6,    7,    11,    6,    Garcia Martin,    L,    119,    5.30,    Old Upstart,    Rockford,    Helooksthepart,    STUMBLE BRK,3W1/2,7P,    7,    </div></br> 
<div style="color:blue" > 12/08/2017 - 6,    AQU,    FT,    44,    F,    44,    (T)-1,    CLM,    25000,    101,    69,    96,    7,    5,    2,    1,    50.00,    2,    1,    2,    2,    2,    Garcia Martin,    L,    120,    3.25,    Rockford,    Will Did It,    Conquest Twister,    prompted 3-2w,repelled,    7,    </div></br> 
<div style="color:red" > 11/18/2017 - 7,    AQU,    FT,    45,    F,    45,    (T)-1,    CLM,    16000,    102,    89,    101,    7,    3,    4,    3,    200.00,    3,    0,    2,    -1,    1,    Arroyo Angel S,    L,    122,    2.25,    Will Did It,    Morning Buzz,    Bar None,    4w upper, inched away,    9,    </div></br> 
<div style="color:blue" > 10/08/2017 - 3,    BEL,    GD,    64,    F,    64,    (T)-1,    CLM,    16000,    100,    68,    95,    6,    4,    4,    5,    250.00,    4,    -3,    1,    -4,    1,    Diaz Jr Hector R,    L,    115,    3.05,    Will Did It,    War Eagle's Return,    Sir Bond,    2w,boldly split 1/4pl,    9,    </div></br> 
<div style="color:red" > 08/12/2017 - 1,    SAR,    FT,    40,    F,    40,    (T)-1,    CLM,    20000,    103,    72,    102,    4,    6,    4,    2,    110.00,    4,    1,    3,    1,    4,    Arroyo Angel S,    L,    120,    4.90,    Set the Trappe,    Sir Bond,    Scarly Charly,    4-3w uppr, fought betw,    7,    </div></br> 
<div style="color:blue" > 07/28/2017 - 4,    SAR,    FT,    47,    F,    47,    (T)-1,    CLM,    12500,    98,    54,    86,    5,    5,    6,    6,    370.00,    6,    4,    4,    5,    3,    Santana Jr Ricardo,    L,    120,    2.75,    Driving Me Crazy,    Draxhall Woods,    Will Did It,    LOST WHIP 1/8 & CAME I,    8,    </div></br> 
<div style="color:red" > 06/23/2017 - 3,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    104,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 06/04/2017 - 2,    MTH,    FT,    40,    F,    40,    (T)-1,    AOC,    30000,    101,    72,    90,    4,    5,    5,    6,    210.00,    4,    2,    3,    5,    3,    Bravo Joe,    L,    120,    2.80,    Defer Heaven,    Griff,    Will Did It,    rail trip,outfinish 2d,    5,    </div></br> 
<div style="color:red" > 04/29/2017 - 4,    PEN,    FT,    0,    F,    0,    (T)0,    SCR,    0,    101,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:blue" > 02/21/2017 - 8,    PRX,    FT,    40,    F,    40,    (T)-1,    AOC,    25000,    105,    91,    86,    3,    6,    7,    4,    420.00,    6,    7,    6,    10,    6,    Ocasio Luis M,    L,    114,    2.40,    Bust Another,    Final Forum,    Gin Makes Ya Sin,    between, no factor,    7,    </div></br> 
<div style="color:red" > 01/21/2017 - 8,    AQU,    FT,    24,    F,    24,    (T)-1,    AOC,    62500,    108,    90,    98,    1,    1,    6,    4,    160.00,    4,    3,    5,    5,    7,    Velasquez Cornelio H,    L,    120,    6.00,    Spartiatis,    Bond Vigilante,    Still Krz,    saved ground, no rally,    9,    </div></br> 
</td> </tr>
<tr  class="horse_3_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2017 ) 35   5   6   6   $253179.00    </br> 
  2017 : 8   2   1   2   $56195.00    </br> 
  2018 : 2   0   0   0   $2750.00    </br> 
  GP: 1   0   0   0   $2250.00    </br> 
  Turf: 1   0   0   0   $296.00    </br> 
  Dist: 21   4   4   3   $193529.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="1" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> KING OF NEW YORK</td>
<td> Davis Dylan</td>
<td> Donk David G</td>
<td> Mike Abraham</td>
<td> Funky Munky Stable LLC</td>
<td> Street Boss</td>
<td> Princess Consort</td>
<td> 8/1</td></tr><tr class="horse_4_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_4_1" style="display:none;"><td COLSPAN=9>
 Tr: St 19 W 32% P 11% S 5% ITM 48% ROI 32</td></tr><tr  class="horse_4_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 10/13/2017 - 7,    BEL,    FM,    144,    F,    144,    (T)9,    CLM,    32000,    104,    73,    89,    1,    7,    8,    17,    1450.00,    8,    11,    7,    10,    6,    Cancel Eric,    L,    120,    25.75,    Souperfast,    Lewis Vale,    For Greater Glory,    5w upper, no impact,    8,    </div></br> 
<div style="color:red" > 09/27/2017 - 7,    BEL,    FM,    162,    F,    162,    (T)27,    CLM,    40000,    104,    72,    92,    4,    6,    6,    10,    950.00,    6,    10,    6,    9,    6,    Rosario Joel,    L,    120,    8.60,    Abiding Star,    Slim Shadey,    Souperfast,    SLUGGISH,INS,4W UPPER,    6,    </div></br> 
<div style="color:blue" > 08/30/2017 - 2,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    108,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 08/19/2017 - 1,    SAR,    FT,    78,    F,    78,    (T)-1,    CLM,    40000,    102,    58,    97,    2,    5,    5,    12,    1250.00,    5,    7,    4,    7,    3,    Rosario Joel,    L,    120,    10.90,    Southside Warrior,    Mills,    King of New York,    2w,5w upper,belatedly,    5,    </div></br> 
<div style="color:blue" > 07/30/2017 - 7,    SAR,    FM,    117,    F,    117,    (T)12,    CLM,    32000,    102,    94,    92,    1,    1,    5,    4,    410.00,    7,    6,    8,    7,    9,    Arroyo Angel S,    L,    120,    5.60,    Docs Legacy,    Indebted,    Neoclassic,    3w upper, weakened,    10,    </div></br> 
<div style="color:red" > 07/08/2017 - 5,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    108,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 07/07/2017 - 7,    BEL,    FT,    46,    F,    46,    (T)-1,    AOC,    80000,    106,    90,    82,    4,    6,    4,    3,    460.00,    4,    9,    6,    13,    6,    Arroyo Angel S,    L,    120,    6.80,    Gypsum Johnny,    El Kabeir,    Poshsky,    stalked, empty,    6,    </div></br> 
<div style="color:red" > 06/30/2017 - 8,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    107,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 06/01/2017 - 4,    BEL,    FM,    153,    F,    153,    (T)27,    CLM,    40000,    99,    82,    97,    5,    5,    5,    4,    210.00,    5,    0,    2,    0,    2,    Ortiz Jr Irad,    L,    120,    4.00,    Mills,    King of New York,    Bow Tie Affair,    tracked 2p, caught lte,    8,    </div></br> 
<div style="color:red" > 05/03/2017 - 2,    BEL,    FM,    128,    F,    128,    (T)18,    CLM,    40000,    101,    65,    88,    5,    1,    2,    1,    50.00,    2,    1,    2,    1,    4,    Ortiz Jr Irad,    L,    120,    5.40,    Indebted,    Birchwood Road,    Dream Man,    3w uppr, loomed, wknd,    7,    </div></br> 
<div style="color:blue" > 03/19/2017 - 7,    OP,    FT,    60,    F,    60,    (T)-1,    AOC,    40000,    105,    80,    66,    2,    7,    7,    4,    820.00,    8,    14,    8,    21,    8,    Franco Geovanni,    L,    116,    3.40,    Taketothestreets,    Goats Town,    Racer,    TOOK UP SHARPLY 15/16,    8,    </div></br> 
<div style="color:red" > 02/25/2017 - 7,    HOU,    FT,    40,    F,    40,    (T)-1,    STK,    0,    109,    96,    94,    4,    9,    10,    6,    270.00,    7,    5,    7,    10,    8,    Doyle Sophie,    L,    120,    6.10,    Iron Fist,    Fear the Cowboy,    Money Flows,    unhurried, no threat,    10,    </div></br> 
</td> </tr>
<tr  class="horse_4_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 97  ( date: 2017 ) 33   4   6   7   $315957.00    </br> 
  2016 : 10   0   1   2   $32661.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 12   1   3   2   $99388.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="1" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> ALRIGHT ALRIGHT</td>
<td> Luzzi Michael J</td>
<td> Toscano Jr John T</td>
<td> Dreamfields Inc & Don Brady</td>
<td> Zimmerman, Scott</td>
<td> Munnings</td>
<td> Afare</td>
<td> 7/2</td></tr><tr class="horse_5_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 7 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_5_1" style="display:none;"><td COLSPAN=9>
 Tr: St 24 W 8% P 21% S 21% ITM 50% ROI -41</td></tr><tr  class="horse_5_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 06/25/2017 - 3,    BEL,    FT,    59,    F,    59,    (T)-1,    CLM,    32000,    106,    89,    89,    3,    5,    5,    4,    260.00,    5,    2,    3,    0,    2,    Diaz Jr Hector R,    L,    117,    0.70,    Rockford,    Alright Alright,    Too Fast to Pass,    2P 1/2,FLOATED 6W UPPR,    5,    </div></br> 
<div style="color:red" > 05/03/2017 - 4,    BEL,    FT,    72,    F,    72,    (T)-1,    CLM,    50000,    108,    127,    110,    8,    5,    2,    1,    -100.00,    1,    -2,    1,    4,    2,    Davis Dylan,    L,    124,    4.00,    Pulling G's,    Alright Alright,    Los Borrachos,    prompted 2-3w, caught,    9,    </div></br> 
<div style="color:blue" > 04/21/2017 - 6,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    110,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 03/31/2017 - 7,    LRL,    SY,    60,    F,    60,    (T)-1,    AOC,    25000,    108,    75,    97,    3,    5,    1,    -1,    -100.00,    1,    -1,    1,    1,    2,    Toledo Jevian,    L,    124,    1.60,    Vorticity,    Alright Alright,    Negrito,    rushed early, gamely,    5,    </div></br> 
<div style="color:blue" > 03/04/2017 - 7,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    113,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 02/25/2017 - 5,    AQU,    FT,    24,    F,    24,    (T)-1,    ALW,    0,    106,    94,    106,    1,    6,    1,    -1,    -150.00,    1,    -3,    1,    -2,    1,    Diaz Jr Hector R,    L,    115,    0.80,    Alright Alright,    Do Share,    Divine Interventio,    inside,ask near 3/16,    7,    </div></br> 
<div style="color:blue" > 02/19/2017 - 7,    AQU,    FT,    26,    F,    26,    (T)-1,    STR,    50000,    102,    126,    112,    2,    4,    2,    1,    -250.00,    1,    -5,    1,    -5,    1,    Davis Dylan,    L,    123,    1.25,    Alright Alright,    Shuffle Up,    Cause for Surprise,    pressed 3-2w, drew clr,    5,    </div></br> 
<div style="color:red" > 02/12/2017 - 1,    AQU,    MY,    24,    F,    24,    (T)-1,    CLM,    50000,    102,    93,    102,    3,    2,    1,    -2,    -550.00,    1,    -9,    1,    -10,    1,    Davis Dylan,    L,    118,    0.85,    Alright Alright,    Buckwellspent,    Dr. Shane,    2p,firm hld,ridden out,    4,    </div></br> 
<div style="color:blue" > 01/28/2017 - 7,    PRX,    FT,    40,    F,    40,    (T)-1,    STR,    25000,    102,    71,    93,    1,    7,    5,    3,    550.00,    5,    7,    5,    4,    4,    Pennington Frankie,    L,    121,    1.60,    High I. Q. Pete,    Sir Rockport,    Thomas Knight,    mild rail bid,    7,    </div></br> 
<div style="color:red" > 01/15/2017 - 7,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 01/14/2017 - 8,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    102,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 12/01/2016 - 6,    DMR,    FM,    30,    F,    30,    (T)24,    AOC,    40000,    105,    128,    102,    4,    7,    1,    -0,    -10.00,    1,    1,    2,    3,    4,    Theriot Jamie,    L,    121,    9.00,    Annie's Candy,    Gutsy Ruler,    Indavidualist,    inside duel,edged 3rd,    9,    </div></br> 
</td> </tr>
<tr  class="horse_5_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 112  ( date: 2017 ) 14   5   4   0   $208585.00    </br> 
  2016 : 7   3   3   0   $136970.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 1   0   0   0   $3180.00    </br> 
  Dist: 12   5   3   0   $185405.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1" race="1" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> HEAVEN'S RUNWAY</td>
<td> Franco Manuel</td>
<td> Rodriguez Rudy R</td>
<td> Dr. K. K. Jayaraman & Dr. V. DeviJa</td>
<td> Dubb, Michael and Imperio, Michael</td>
<td> Run Away and Hide</td>
<td> Heavens Passport</td>
<td> 6/5</td></tr><tr class="horse_1_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_1_1" style="display:none;"><td COLSPAN=9>
 Tr: St 48 W 21% P 19% S 10% ITM 50% ROI -13</td></tr><tr  class="horse_1_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/18/2018 - 5,    AQU,    SY,    0,    F,    0,    (T)0,    SCR,    0,    101,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 02/18/2017 - 10,    LRL,    FT,    60,    F,    60,    (T)-1,    STK,    0,    114,    81,    98,    8,    1,    8,    5,    760.00,    9,    12,    7,    10,    7,    Carmouche Kendrick,    L,    122,    7.90,    Imperial Hint,    Stallwalkin' Dude,    Never Gone South,    rail 5/16,angl,empty,    9,    </div></br> 
<div style="color:blue" > 12/31/2016 - 8,    LRL,    FT,    60,    F,    60,    (T)-1,    STK,    0,    111,    71,    108,    6,    1,    3,    2,    100.00,    2,    3,    2,    -1,    1,    Velasquez Cornelio H,    L,    122,    2.50,    Heaven's Runway,    Chief Lion,    Sir Rockport,    4-3W,DRIFTING OUT,DRVG,    6,    </div></br> 
<div style="color:red" > 11/24/2016 - 8,    AQU,    FT,    40,    F,    40,    (T)-1,    STK,    0,    111,    66,    114,    1,    5,    7,    6,    600.00,    5,    3,    5,    -0,    1,    Alvarado Junior,    L,    123,    32.25,    Heaven's Runway,    Stallwalkin' Dude,    Ready for Rye,    4w upper, just up,    8,    </div></br> 
<div style="color:blue" > 10/14/2016 - 8,    BEL,    GD,    40,    F,    40,    (T)9,    AOC,    80000,    108,    93,    108,    8,    3,    8,    6,    560.00,    8,    6,    5,    3,    4,    Ortiz Jr Irad,    L,    120,    6.00,    Vision Perfect,    Summation Time,    Solemn Tribute,    2p,3w upr,kick in 1/8,    8,    </div></br> 
<div style="color:red" > 09/04/2016 - 9,    SAR,    FT,    0,    F,    0,    (T)0,    SCR,    0,    112,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 08/28/2016 - 5,    SAR,    FM,    80,    F,    80,    (T)0,    AOC,    62500,    109,    63,    100,    2,    4,    6,    5,    510.00,    5,    4,    4,    2,    2,    Ortiz Jr Irad,    L,    120,    4.50,    Big Rock,    Heaven's Runway,    Summer Breezing,    ins,hedge rally place,    7,    </div></br> 
<div style="color:red" > 07/30/2016 - 2,    SAR,    FT,    22,    F,    22,    (T)-1,    CLM,    50000,    107,    80,    92,    7,    4,    3,    2,    250.00,    4,    2,    4,    4,    4,    Alvarado Junior,    L,    120,    5.00,    Old Upstart,    Cerro,    Sam Sparkle,    4w pursuit, no rally,    7,    </div></br> 
<div style="color:blue" > 07/04/2016 - 6,    BEL,    FM,    70,    F,    70,    (T)0,    AOC,    80000,    110,    69,    94,    2,    6,    7,    6,    430.00,    7,    4,    7,    3,    4,    Alvarado Junior,    L,    120,    26.00,    Spring to the Sky,    Mosler,    Sandy'z Slew,    ins half,to 6p 3/16pl,    7,    </div></br> 
<div style="color:red" > 06/22/2016 - 8,    DEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    113,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 06/05/2016 - 3,    BEL,    FM,    72,    F,    72,    (T)27,    AOC,    80000,    109,    126,    105,    3,    7,    2,    1,    50.00,    2,    4,    4,    5,    4,    Alvarado Junior,    L,    120,    14.40,    Full Mast,    Almasty,    Dowse's Beach,    pressed 2-3w, wknd lte,    9,    </div></br> 
<div style="color:red" > 04/13/2016 - 5,    AQU,    FT,    40,    F,    40,    (T)-1,    AOC,    100000,    108,    93,    93,    6,    1,    5,    4,    360.00,    5,    4,    4,    4,    4,    Velasquez Cornelio H,    L,    118,    12.00,    Joking,    Fabulous Kid,    Nubin Ridge,    BRUSH BRK,2P,5W UPR,    6,    </div></br> 
</td> </tr>
<tr  class="horse_1_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 114  ( date: 2016 ) 43   7   4   7   $597925.00    </br> 
  2017 : 1   0   0   0   $0.00    </br> 
  2018 : 0   0   0   0   $0.00    </br> 
  GP: 4   2   0   0   $184500.00    </br> 
  Turf: 8   1   1   0   $98750.00    </br> 
  Dist: 22   6   3   4   $397485.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1A" race="1" title="Show More..." > 1A</br> <span class="past-performance-plus-b" > </span> </td>
<td> SUMMER BOURBON</td>
<td> Reyes Luis R</td>
<td> Rodriguez Rudy R</td>
<td> William Parsons Jr. & David Howe</td>
<td> Michael Dubb</td>
<td> Ghostzapper</td>
<td> Celestial Sweep</td>
<td> 6/5</td></tr><tr class="horse_1A_1" style="display:none;"><td COLSPAN=9>
  Jocks: St 23 W 9% P 9% S 0% ITM 18% ROI -36</td></tr><tr  class="horse_1A_1" style="display:none;"><td COLSPAN=9>
 Tr: St 48 W 21% P 19% S 10% ITM 50% ROI -13</td></tr><tr  class="horse_1A_1" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 4,    AQU,    GD,    42,    F,    42,    (T)-1,    CLM,    25000,    93,    101,    87,    3,    2,    2,    1,    110.00,    3,    3,    3,    5,    4,    Arroyo Angel S,    L,    123,    2.50,    For Pops,    Love That Jazz,    Eddie's Gift,    4w upper, wknd late,    5,    </div></br> 
<div style="color:red" > 12/29/2017 - 7,    AQU,    FT,    53,    F,    53,    (T)-1,    ALW,    0,    91,    93,    98,    2,    6,    2,    1,    50.00,    2,    1,    2,    -0,    1,    Arroyo Angel S,    L,    124,    12.00,    Summer Bourbon,    Sicilia Mike,    Ethan Hunt,    2w,4w upper,determined,    9,    </div></br> 
<div style="color:blue" > 11/28/2017 - 7,    FL,    FT,    56,    F,    56,    (T)-1,    ALW,    0,    90,    46,    76,    1,    7,    5,    2,    400.00,    4,    3,    3,    3,    2,    Davila Jr John R,    L,    124,    1.15,    For Pops,    Summer Bourbon,    Hard Promises,    SLGSH ST,CKD7/16,    7,    </div></br> 
<div style="color:red" > 11/15/2017 - 8,    FL,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    88,    54,    84,    7,    5,    4,    1,    100.00,    2,    -0,    1,    0,    2,    Dediego Emanuel,    L,    124,    3.20,    Freud's Affair,    Summer Bourbon,    Calculated Risker,    btw2p trn, gamely,    9,    </div></br> 
<div style="color:blue" > 11/08/2017 - 1,    FL,    FT,    56,    F,    56,    (T)-1,    ALW,    0,    88,    66,    88,    3,    5,    4,    4,    460.00,    3,    3,    3,    -1,    1,    Davila Jr John R,    L,    120,    4.40,    Summer Bourbon,    Rory Mor,    Freud's Friend,    slgsh st,wrppd up late,    6,    </div></br> 
<div style="color:red" > 10/23/2017 - 7,    FL,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    90,    46,    81,    3,    6,    3,    3,    400.00,    4,    3,    4,    3,    6,    Davila Jr John R,    L,    120,    4.60,    Freud's Affair,    Freud's Friend,    One Summer Nite,    BROKE SLOW,RAIL TRN,    6,    </div></br> 
<div style="color:blue" > 07/10/2017 - 7,    FL,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    87,    29,    72,    8,    8,    8,    8,    530.00,    8,    3,    7,    4,    4,    Davila Jr John R,    L,    124,    1.15,    Hurricane Andy,    Zoo Yorker,    West Sedona,    widest,edged for share,    8,    </div></br> 
<div style="color:red" > 06/13/2017 - 6,    FL,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    89,    65,    87,    2,    5,    4,    3,    250.00,    4,    -0,    1,    -4,    1,    Davila Jr John R,    L,    120,    0.70,    Summer Bourbon,    Just Watch Me,    Preferred Outcome,    BRK SLW,4ACRS1/8,DRW O,    5,    </div></br> 
<div style="color:blue" > 05/30/2017 - 5,    FL,    SY,    56,    F,    56,    (T)-1,    ALW,    0,    91,    50,    80,    4,    5,    4,    4,    500.00,    4,    2,    4,    2,    2,    Davila Jr John R,    L,    120,    1.35,    Eddie's Gift,    Summer Bourbon,    Mr Curiosity,    BMPD ST, RAIL BID,    5,    </div></br> 
<div style="color:red" > 05/16/2017 - 2,    FL,    FT,    54,    F,    54,    (T)-1,    ALW,    0,    90,    45,    85,    5,    5,    4,    2,    210.00,    5,    2,    4,    1,    3,    Davila Jr John R,    L,    120,    1.30,    Mini Sala,    Preferred Outcome,    Summer Bourbon,    drft out,mild str gain,    5,    </div></br> 
<div style="color:blue" > 05/04/2017 - 2,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    92,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 04/02/2017 - 7,    AQU,    FT,    44,    F,    44,    (T)-1,    CLM,    50000,    100,    62,    86,    5,    7,    4,    2,    410.00,    5,    6,    4,    7,    5,    Arroyo Angel S,    L,    120,    8.70,    Manipulated,    Dr. Shane,    Timber,    3W 1/2,4W & BUMP UPPER,    7,    </div></br> 
</td> </tr>
<tr  class="horse_1A_1" style="display:none;"><td COLSPAN=9>
LifeTime Record 98  ( date: 2017 ) 18   5   4   1   $118814.00    </br> 
  2017 : 12   4   3   1   $87494.00    </br> 
  2018 : 1   0   0   0   $2350.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 15   5   4   0   $115334.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="2" race_date="20180317" claimamt="80000" dist_disp="1M" todays_cls="86" purse="67000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #2</span> - 
<span> Claiming $80000</span> - 
<span> Distance 1M</span> - 
<span> Todays CLS 86</span> -
<span> Purse $67000</span> 
    <br><span>Exacta, Quinella, Trifecta (.50), Super (.10), Pick 3 Races (2-4) Pick 4 (.50) Races (2-5), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="2" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> GLENNWOOD</td>
<td> Rocco Jr Joseph</td>
<td> Wilkinson Erin</td>
<td> Zayat Stables, LLC</td>
<td> Moore, Kevin and Myers, Allen R.</td>
<td> Paynter</td>
<td> Enchanted Woods</td>
<td> 8/1</td></tr><tr class="horse_1_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 6% P 14% S 6% ITM 26% ROI -28</td></tr><tr  class="horse_1_2" style="display:none;"><td COLSPAN=9>
 Tr: St 0 W 0% P 0% S 0% ITM 0% ROI Array</td></tr><tr  class="horse_1_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/10/2018 - 8,    AQU,    MY,    52,    F,    52,    (T)-1,    STK,    0,    91,    68,    84,    1,    4,    7,    5,    560.00,    7,    5,    4,    3,    3,    Davis Dylan,    L,    116,    40.00,    A Different Style,    Global Citizen,    Glennwood,    6w upper, late foot,    7,    </div></br> 
<div style="color:red" > 02/08/2018 - 6,    AQU,    GD,    0,    F,    0,    (T)0,    SCR,    0,    86,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 01/13/2018 - 8,    AQU,    MY,    52,    F,    52,    (T)-1,    STK,    0,    93,    53,    68,    6,    6,    3,    2,    1120.00,    6,    17,    6,    18,    6,    Rocco Jr Joseph,    L,    118,    55.50,    Firenze Fire,    Seven Trumpets,    Coltandmississippi,    5w upper, tired,    6,    </div></br> 
<div style="color:red" > 11/25/2017 - 1,    CD,    FT,    34,    F,    34,    (T)-1,    AOC,    75000,    96,    43,    76,    8,    9,    9,    8,    1210.00,    9,    11,    9,    8,    7,    Morales Edgar,    L,    111,    77.40,    Seven Trumpets,    Battle Station,    Nuclear Option,    passed tiring rivals,    10,    </div></br> 
<div style="color:blue" > 09/28/2017 - 3,    CD,    FT,    180,    F,    180,    (T)-1,    MCL,    20000,    68,    66,    70,    5,    3,    2,    1,    20.00,    3,    -2,    1,    -1,    1,    Lanerie Corey J,    L,    120,    0.60,    Glennwood,    Nasty Critter,    Rocky Roma,    bid between, dug in,    7,    </div></br> 
<div style="color:red" > 08/25/2017 - 3,    ELP,    FT,    34,    F,    34,    (T)-1,    MCL,    30000,    71,    69,    71,    4,    3,    3,    1,    100.00,    2,    0,    2,    2,    3,    Lanerie Corey J,    L,    120,    0.70,    Trappezoid,    Lngtermrelationshp,    Glennwood,    drew even 3w,flattened,    6,    </div></br> 
</td> </tr>
<tr  class="horse_1_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 84  ( date: 2018 ) 5   1   0   2   $31407.00    </br> 
  2017 : 3   1   0   1   $13407.00    </br> 
  2018 : 2   0   0   1   $18000.00    </br> 
  GP: 2   0   0   1   $18000.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 1   0   0   0   $3000.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="2" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> FACTOR THIS</td>
<td> Davis Dylan</td>
<td> Barker Edward R</td>
<td> Maccabee Farm</td>
<td> Campbell Road Stables and Chen, Danny J.</td>
<td> The Factor</td>
<td> Capricious Miss (GB)</td>
<td> 12/1</td></tr><tr class="horse_2_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_2_2" style="display:none;"><td COLSPAN=9>
 Tr: St 12 W 25% P 17% S 17% ITM 59% ROI 65</td></tr><tr  class="horse_2_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 6,    AQU,    GD,    50,    F,    50,    (T)-1,    STR,    50000,    86,    66,    83,    4,    4,    4,    2,    100.00,    2,    -1,    1,    -1,    1,    Davis Dylan,    L,    122,    2.00,    Factor This,    Infield Is In,    Davka,    3w upper, inched away,    6,    </div></br> 
<div style="color:red" > 01/13/2018 - 8,    AQU,    MY,    52,    F,    52,    (T)-1,    STK,    0,    93,    50,    81,    4,    5,    5,    3,    160.00,    3,    6,    4,    11,    5,    Davis Dylan,    L,    116,    12.60,    Firenze Fire,    Seven Trumpets,    Coltandmississippi,    3w upper, tired,    6,    </div></br> 
<div style="color:blue" > 12/17/2017 - 1,    AQU,    FT,    54,    F,    54,    (T)-1,    MSW,    0,    93,    96,    88,    3,    4,    3,    2,    -50.00,    1,    -2,    1,    -2,    1,    Davis Dylan,    L,    120,    5.40,    Factor This,    Power Boss,    American Lincoln,    3w upper, edged away,    6,    </div></br> 
<div style="color:red" > 11/30/2017 - 6,    AQU,    FM,    73,    F,    73,    (T)18,    MSW,    0,    92,    42,    86,    2,    2,    5,    3,    200.00,    3,    2,    3,    2,    2,    Davis Dylan,    L,    120,    22.00,    Power of Attorney,    Factor This,    Collective Effort,    pocket to 5/16,3w upr,    10,    </div></br> 
<div style="color:blue" > 11/24/2017 - 10,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    89,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Also-Eligible,    0,    </div></br> 
<div style="color:red" > 11/18/2017 - 6,    AQU,    FM,    120,    F,    120,    (T)18,    MSW,    0,    93,    22,    -97,    7,    4,    12,    15,    620.00,    12,    9,    12,    100,    12,    Davis Dylan,    L,    120,    20.20,    Time On Target,    Unleveraged,    Mathematician,    SLIPPED SADDLE, EASED,    12,    </div></br> 
<div style="color:blue" > 10/27/2017 - 9,    BEL,    FM,    172,    F,    172,    (T)27,    MCL,    75000,    90,    95,    85,    4,    4,    3,    2,    150.00,    3,    1,    2,    1,    2,    Davis Dylan,    L,    120,    13.30,    Wake Me Up At Noon,    Factor This,    Call the Cat,    INS 6F,ANGLE,WILLING,    12,    </div></br> 
<div style="color:red" > 10/01/2017 - 9,    BEL,    FM,    101,    F,    101,    (T)0,    MSW,    0,    95,    56,    67,    4,    5,    6,    6,    600.00,    6,    8,    6,    9,    6,    Davis Dylan,    L,    119,    50.50,    Devine Entry,    Solid,    Spirit Fly,    LITE BMP ST,BRSH 1/2,    7,    </div></br> 
<div style="color:blue" > 09/09/2017 - 2,    BEL,    FM,    173,    F,    173,    (T)9,    MSW,    0,    87,    61,    83,    6,    7,    5,    4,    400.00,    6,    4,    6,    4,    4,    Arroyo Angel S,    L,    119,    41.25,    Bourbon Currency,    Devine Entry,    Solid,    came in break,ins turn,    9,    </div></br> 
<div style="color:red" > 07/31/2017 - 4,    SAR,    FT,    52,    F,    52,    (T)-1,    MCL,    50000,    76,    60,    47,    1,    8,    6,    8,    900.00,    6,    12,    6,    13,    6,    Arroyo Angel S,    L,    119,    9.90,    Joopster,    The Berber,    Shane's Jewel,    BMP AFTER ST,NO IMPACT,    8,    </div></br> 
</td> </tr>
<tr  class="horse_2_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 88  ( date: 2017 ) 9   2   2   0   $100902.00    </br> 
  2017 : 7   1   2   0   $63402.00    </br> 
  2018 : 2   1   0   0   $37500.00    </br> 
  GP: 9   2   2   0   $100902.00    </br> 
  Turf: 5   0   2   0   $27068.00    </br> 
  Dist: 3   2   0   0   $73500.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="2" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> CALIFORNIA NIGHT</td>
<td> Carmouche Kendrick</td>
<td> Maker Michael J</td>
<td> James H. Tolliver</td>
<td> Three Diamonds Farm</td>
<td> Midnight Lute</td>
<td> Moon Over Malibu</td>
<td> 5/1</td></tr><tr class="horse_3_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_3_2" style="display:none;"><td COLSPAN=9>
 Tr: St 85 W 13% P 8% S 11% ITM 32% ROI -51</td></tr><tr  class="horse_3_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/10/2018 - 8,    AQU,    MY,    52,    F,    52,    (T)-1,    STK,    0,    91,    92,    76,    6,    7,    2,    1,    300.00,    3,    7,    6,    7,    5,    Hernandez Rafael M,    L,    116,    3.75,    A Different Style,    Global Citizen,    Glennwood,    coaxed st, weakened,    7,    </div></br> 
<div style="color:red" > 02/03/2018 - 8,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    93,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 12/10/2017 - 3,    AQU,    GD,    20,    F,    20,    (T)-1,    MSW,    0,    88,    94,    82,    1,    1,    1,    -1,    -100.00,    1,    -2,    1,    -3,    1,    Carmouche Kendrick,    L,    120,    3.65,    California Night,    Holland Park,    Fixed Point,    4w upper, edged clear,    4,    </div></br> 
</td> </tr>
<tr  class="horse_3_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 82  ( date: 2017 ) 2   1   0   0   $40500.00    </br> 
  2017 : 1   1   0   0   $36000.00    </br> 
  2018 : 1   0   0   0   $4500.00    </br> 
  GP: 2   1   0   0   $40500.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="2" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> GO GET THE MUNNY</td>
<td> Reyes Luis R</td>
<td> Morley Thomas</td>
<td> T. Gladwell, T. Burns & O. Rameriz</td>
<td> West Point Thoroughbreds and Masiello, Robert</td>
<td> Munnings</td>
<td> Bliss</td>
<td> 6/5</td></tr><tr class="horse_4_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 23 W 9% P 9% S 0% ITM 18% ROI -36</td></tr><tr  class="horse_4_2" style="display:none;"><td COLSPAN=9>
 Tr: St 13 W 23% P 8% S 8% ITM 39% ROI -7</td></tr><tr  class="horse_4_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/01/2018 - 5,    AQU,    GD,    56,    F,    56,    (T)-1,    MSW,    0,    90,    82,    91,    6,    1,    4,    5,    200.00,    3,    -6,    1,    -7,    1,    Reyes Luis R,    L,    120,    4.50,    Go Get the Munny,    Racing Raven,    Split Verdict,    4w trn,brief drive1/16,    7,    </div></br> 
</td> </tr>
<tr  class="horse_4_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 91  ( date: 2018 ) 1   1   0   0   $36000.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  2018 : 1   1   0   0   $36000.00    </br> 
  GP: 1   1   0   0   $36000.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="2" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> GLEASON</td>
<td> Franco Manuel</td>
<td> Pletcher Todd A</td>
<td> Claiborne Farm</td>
<td> Paul P. Pompa, Jr.</td>
<td> Algorithms</td>
<td> Trip</td>
<td> 7/5</td></tr><tr class="horse_5_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_5_2" style="display:none;"><td COLSPAN=9>
 Tr: St 85 W 28% P 20% S 20% ITM 68% ROI -24</td></tr><tr  class="horse_5_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/19/2018 - 8,    LRL,    MY,    0,    F,    0,    (T)0,    SCR,    0,    87,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 01/20/2018 - 7,    TAM,    FT,    30,    F,    30,    (T)-1,    MSW,    0,    83,    108,    86,    8,    5,    2,    2,    50.00,    2,    -3,    1,    -6,    1,    Franco Manuel,    L,    120,    1.00,    Gleason,    Getyourmindright,    Pucon,    stalked 2w,pulled away,    11,    </div></br> 
</td> </tr>
<tr  class="horse_5_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 86  ( date: 2018 ) 1   1   0   0   $11400.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  2018 : 1   1   0   0   $11400.00    </br> 
  GP: 1   1   0   0   $11400.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="2" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> INFIELD IS IN</td>
<td> Lezcano Abel</td>
<td> Metivier Richard</td>
<td> Colts Neck Stables LLC</td>
<td> Richard Metivier</td>
<td> First Samurai</td>
<td> Profit</td>
<td> 20/1</td></tr><tr class="horse_6_2" style="display:none;"><td COLSPAN=9>
  Jocks: St 33 W 3% P 6% S 9% ITM 18% ROI -78</td></tr><tr  class="horse_6_2" style="display:none;"><td COLSPAN=9>
 Tr: St 14 W 0% P 7% S 21% ITM 28% ROI -100</td></tr><tr  class="horse_6_2" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/09/2018 - 7,    AQU,    FT,    50,    F,    50,    (T)-1,    STR,    50000,    81,    79,    70,    2,    6,    6,    6,    500.00,    6,    7,    5,    8,    4,    Lezcano Abel,    L,    119,    6.30,    Big Thicket,    Felix in Fabula,    Lutheran Rags,    ins,clip beh 5/16pl,    7,    </div></br> 
<div style="color:red" > 02/08/2018 - 6,    AQU,    GD,    50,    F,    50,    (T)-1,    STR,    50000,    86,    57,    81,    3,    5,    6,    4,    110.00,    3,    2,    3,    1,    2,    Lezcano Abel,    L,    120,    15.40,    Factor This,    Infield Is In,    Davka,    4w upper, mild bid,    6,    </div></br> 
<div style="color:blue" > 01/15/2018 - 4,    AQU,    FT,    32,    F,    32,    (T)-1,    MCL,    40000,    85,    21,    68,    4,    8,    8,    8,    310.00,    5,    -1,    1,    -3,    1,    Lezcano Abel,    L,    120,    3.60,    Infield Is In,    Warrior Rose,    A Lotta Wine,    4w upper, edged away,    9,    </div></br> 
<div style="color:red" > 11/29/2017 - 3,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    74,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 11/18/2017 - 6,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    93,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/27/2017 - 9,    BEL,    FM,    172,    F,    172,    (T)27,    MCL,    75000,    90,    93,    80,    11,    3,    5,    3,    250.00,    4,    3,    4,    4,    6,    Reyes Luis R,    L,    113,    28.50,    Wake Me Up At Noon,    Factor This,    Call the Cat,    chased 3w, no kick,    12,    </div></br> 
<div style="color:blue" > 10/07/2017 - 4,    BEL,    FM,    55,    F,    55,    (T)0,    MSW,    0,    94,    92,    80,    8,    7,    7,    6,    550.00,    8,    8,    8,    9,    8,    Reyes Luis R,    L,    112,    38.75,    Get the Facts,    Witch Doctor,    Ambassador Jim,    3w both turns,no bids,    8,    </div></br> 
<div style="color:red" > 09/23/2017 - 7,    BEL,    FM,    138,    F,    138,    (T)18,    MSW,    0,    75,    87,    73,    8,    6,    6,    4,    360.00,    6,    4,    6,    6,    5,    Cancel Eric,    L,    119,    40.50,    War Chest,    Congruity,    Thunder Mesa,    5w upper, outkicked,    12,    </div></br> 
<div style="color:blue" > 09/03/2017 - 6,    SAR,    SY,    35,    F,    35,    (T)-1,    MSW,    0,    83,    54,    65,    2,    7,    8,    9,    830.00,    8,    9,    3,    12,    3,    Cancel Eric,    L,    119,    33.25,    Lone Sailor,    Campaign,    Infield Is In,    2p turn,4p into lane,    9,    </div></br> 
<div style="color:red" > 08/12/2017 - 7,    SAR,    FT,    32,    F,    32,    (T)-1,    MSW,    0,    88,    61,    46,    6,    5,    7,    5,    510.00,    7,    12,    7,    14,    8,    DeCarlo Christopher P,    L,    119,    42.50,    Coltandmississippi,    Roaming Union,    Wyatt's Town,    CARRIED OUT 11W 1/4P,    9,    </div></br> 
<div style="color:blue" > 07/22/2017 - 3,    SAR,    FM,    134,    F,    134,    (T)12,    MSW,    0,    87,    62,    72,    2,    2,    2,    1,    100.00,    2,    2,    4,    6,    4,    Lezcano Jose,    L,    119,    28.75,    Untamed Domain,    Another,    Linburgh's Kitten,    2w,stalk winner,tired,    7,    </div></br> 
<div style="color:red" > 07/09/2017 - 2,    BEL,    FM,    102,    F,    102,    (T)0,    MSW,    0,    86,    53,    54,    5,    5,    7,    4,    560.00,    7,    10,    8,    12,    8,    Franco Manuel,    L,    119,    8.80,    Hemp Hemp Hurray,    Ambassador Jim,    Majestic Dunhill,    bit unsettle 1/2,3w,4w,    10,    </div></br> 
</td> </tr>
<tr  class="horse_6_2" style="display:none;"><td COLSPAN=9>
LifeTime Record 81  ( date: 2018 ) 11   1   1   1   $54664.00    </br> 
  2017 : 8   0   0   1   $16564.00    </br> 
  2018 : 3   1   1   0   $38100.00    </br> 
  GP: 9   1   1   1   $53864.00    </br> 
  Turf: 5   0   0   0   $7349.00    </br> 
  Dist: 2   0   1   0   $13500.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="3" race_date="20180317" claimamt="0" dist_disp="1M" todays_cls="80" purse="60000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #3</span> - 
<span> Claiming $0</span> - 
<span> Distance 1M</span> - 
<span> Todays CLS 80</span> -
<span> Purse $60000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Pick 3 Races (3-5), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="3" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> MAD DOG MATTERS</td>
<td> Rose Jeremy</td>
<td> Martin Carlos F</td>
<td> Glencrest Farm LLC</td>
<td> Collinsworth Thoroughbred Racing LLC</td>
<td> Mineshaft</td>
<td> Songfest</td>
<td> 12/1</td></tr><tr class="horse_1_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 21 W 0% P 19% S 14% ITM 33% ROI -100</td></tr><tr  class="horse_1_3" style="display:none;"><td COLSPAN=9>
 Tr: St 16 W 13% P 13% S 13% ITM 39% ROI 117</td></tr><tr  class="horse_1_3" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/18/2018 - 1,    AQU,    SY,    50,    F,    50,    (T)-1,    MSW,    0,    80,    74,    72,    1,    3,    1,    -1,    -50.00,    1,    1,    2,    5,    3,    Rose Jeremy,    L,    120,    7.70,    Mo Flash,    String Section,    Mad Dog Matters,    in hand 2p, wknd late,    4,    </div></br> 
<div style="color:red" > 01/26/2018 - 1,    AQU,    FT,    50,    F,    50,    (T)-1,    MSW,    0,    87,    68,    63,    2,    5,    1,    -1,    -50.00,    1,    3,    2,    8,    2,    Rose Jeremy,    L,    120,    26.50,    Fools Gold,    Mad Dog Matters,    Mo Flash,    in hand 3-2w, kept on,    6,    </div></br> 
<div style="color:blue" > 12/23/2017 - 3,    AQU,    SY,    46,    F,    46,    (T)-1,    MSW,    0,    85,    65,    57,    1,    2,    6,    6,    810.00,    6,    10,    5,    14,    5,    Rose Jeremy,    L,    120,    26.75,    My Miss Lilly,    Lady Suebee,    Twofer,    ins,long drive,no bids,    6,    </div></br> 
<div style="color:red" > 09/17/2017 - 4,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    81,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 08/20/2017 - 5,    SAR,    FM,    62,    F,    62,    (T)18,    MSW,    0,    82,    68,    63,    4,    4,    6,    4,    510.00,    8,    8,    9,    13,    9,    Arroyo Angel S,    L,    119,    10.00,    Layla Noor,    Lemoona,    Fools Gold,    BMPD BRK,2-3W TURNS,    9,    </div></br> 
<div style="color:red" > 08/03/2017 - 5,    SAR,    FT,    62,    F,    62,    (T)-1,    MCL,    50000,    71,    66,    64,    2,    3,    5,    2,    450.00,    4,    8,    4,    6,    2,    Ortiz Jr Irad,    L,    119,    2.35,    Tarawa,    Mad Dog Matters,    In the Mood,    3-4w turn,up for 2nd,    9,    </div></br> 
</td> </tr>
<tr  class="horse_1_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 72  ( date: 2018 ) 5   0   2   1   $30215.00    </br> 
  2017 : 3   0   1   0   $12215.00    </br> 
  2018 : 2   0   1   1   $18000.00    </br> 
  GP: 4   0   1   1   $20215.00    </br> 
  Turf: 1   0   0   0   $415.00    </br> 
  Dist: 2   0   1   1   $18000.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="3" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> OOZLE</td>
<td> Franco Manuel</td>
<td> Brown Chad C</td>
<td> Alan S. Kline Revocable Trust</td>
<td> Coleman, T., Doheny, M., Head of Plains Partn</td>
<td> Dialed In</td>
<td> Zazzle</td>
<td> 5/2</td></tr><tr class="horse_2_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_2_3" style="display:none;"><td COLSPAN=9>
 Tr: St 37 W 22% P 16% S 14% ITM 52% ROI -19</td></tr><tr  class="horse_2_3" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/04/2018 - 1,    AQU,    FT,    32,    F,    32,    (T)-1,    MSW,    0,    82,    65,    57,    4,    6,    3,    2,    100.00,    3,    4,    3,    12,    3,    Lopez Paco,    L,    120,    0.25,    Sara Street,    Lady Suebee,    Oozle,    vied w/ pair turn, wkn,    6,    </div></br> 
</td> </tr>
<tr  class="horse_2_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 57  ( date: 2018 ) 1   0   0   1   $6000.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  2018 : 1   0   0   1   $6000.00    </br> 
  GP: 1   0   0   1   $6000.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="3" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> FINGERPAINT</td>
<td> Davis Dylan</td>
<td> Terranova II John P</td>
<td> Crossed Sabres Farm</td>
<td> Curragh Stables, Gatsas Stables and Geren, Ke</td>
<td> Union Rags</td>
<td> Autumn Ash</td>
<td> 8/1</td></tr><tr class="horse_3_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_3_3" style="display:none;"><td COLSPAN=9>
 Tr: St 9 W 22% P 33% S 11% ITM 66% ROI 68</td></tr><tr  class="horse_3_3" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 11/12/2017 - 4,    AQU,    FM,    27,    F,    27,    (T)12,    MSW,    0,    88,    78,    62,    1,    6,    6,    3,    520.00,    8,    12,    9,    14,    9,    Davis Dylan,    L,    120,    47.75,    Filfila,    Kasbach,    Closing Statement,    3w upper, tired,    9,    </div></br> 
<div style="color:red" > 10/26/2017 - 2,    BEL,    FT,    47,    F,    47,    (T)-1,    MSW,    0,    82,    47,    66,    2,    4,    3,    3,    260.00,    3,    7,    3,    11,    4,    Davis Dylan,    L,    119,    20.10,    Indy Union,    Pink Sands,    Delta House,    2w,4w anlge from 5/16,    4,    </div></br> 
<div style="color:blue" > 10/22/2017 - 9,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/01/2017 - 6,    BEL,    FT,    48,    F,    48,    (T)-1,    MSW,    0,    84,    61,    65,    5,    9,    9,    6,    370.00,    7,    9,    7,    15,    7,    Saez Luis,    L,    119,    38.25,    Navajo,    Pink Sands,    Aerial Assets,    BRUSHED GATE, TIRED,    9,    </div></br> 
<div style="color:blue" > 09/27/2017 - 4,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    83,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 09/03/2017 - 7,    SAR,    SY,    33,    F,    33,    (T)-1,    MSW,    0,    81,    0,    40,    4,    9,    9,    19,    2010.00,    9,    20,    8,    21,    7,    Geroux Florent,    L,    119,    47.25,    Caledonia Road,    Mockery,    Indy Union,    LUNGE,HIT GATE,2W TURN,    9,    </div></br> 
<div style="color:blue" > 08/27/2017 - 10,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    81,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
</td> </tr>
<tr  class="horse_3_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 66  ( date: 2017 ) 4   0   0   0   $4840.00    </br> 
  2016 : 4   0   0   0   $4840.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 4   0   0   0   $4840.00    </br> 
  Turf: 1   0   0   0   $300.00    </br> 
  Dist: 2   0   0   0   $4125.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="3" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> MISS MI MI</td>
<td> Carmouche Kendrick</td>
<td> Clement Christophe</td>
<td> Ledgelands, LLC & Andrew Ritter</td>
<td> Ingram, Lee Ann and Ingram II, Orrin H.</td>
<td> Midshipman</td>
<td> Lots of Mine</td>
<td> 2/1</td></tr><tr class="horse_4_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_4_3" style="display:none;"><td COLSPAN=9>
 Tr: St 19 W 11% P 11% S 21% ITM 43% ROI -41</td></tr><tr  class="horse_4_3" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/10/2018 - 8,    TAM,    FT,    30,    F,    30,    (T)-1,    MSW,    0,    84,    79,    90,    12,    8,    7,    5,    400.00,    4,    9,    4,    6,    4,    Ortiz Jose L,    L,    120,    14.50,    Cilantro,    Contrarian,    Dominance,    moved up nicley wide,    12,    </div></br> 
<div style="color:red" > 01/13/2018 - 6,    TAM,    FT,    35,    F,    35,    (T)-1,    MSW,    0,    73,    0,    61,    11,    10,    10,    13,    1250.00,    8,    14,    5,    8,    4,    Pedroza Brian,    L,    120,    4.20,    History Supreme,    Exclusive Express,    Sweetly Maid,    BUMPED START, STEADIED,    12,    </div></br> 
</td> </tr>
<tr  class="horse_4_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 90  ( date: 2018 ) 2   0   0   0   $2000.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  2018 : 2   0   0   0   $2000.00    </br> 
  GP: 2   0   0   0   $2000.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="3" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> D J'S FAVORITE</td>
<td> Alvarado Junior</td>
<td> Rice Linda</td>
<td> Stonestreet Thoroughbred Holdings L</td>
<td> Leonard C. Green</td>
<td> Union Rags</td>
<td> I'm Mom's Favorite</td>
<td> 7/2</td></tr><tr class="horse_5_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 61 W 28% P 13% S 25% ITM 66% ROI 2</td></tr><tr  class="horse_5_3" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_5_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="3" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> CREATIVE PRINCESS</td>
<td> McCarthy Trevor</td>
<td> Pletcher Todd A</td>
<td> Kildare Stud</td>
<td> Repole Stable</td>
<td> Uncle Mo</td>
<td> Pleasant Point</td>
<td> 3/1</td></tr><tr class="horse_6_3" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_6_3" style="display:none;"><td COLSPAN=9>
 Tr: St 85 W 28% P 20% S 20% ITM 68% ROI -24</td></tr><tr  class="horse_6_3" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="4" race_date="20180317" claimamt="14000" dist_disp="1M" todays_cls="88" purse="27000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #4</span> - 
<span> Claiming $14000</span> - 
<span> Distance 1M</span> - 
<span> Todays CLS 88</span> -
<span> Purse $27000</span> 
    <br><span>Exacta, Quinella, Trifecta (.50), Super (.10), Pick 3 Races (4-6) Pick 6 Races (4-9), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="4" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> KENYAN</td>
<td> Davis Dylan</td>
<td> Sciacca Gary</td>
<td> Twin Creeks Farm</td>
<td> Flanken Farms</td>
<td> Curlin</td>
<td> Our Perfect Ten</td>
<td> 8/1</td></tr><tr class="horse_1_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_1_4" style="display:none;"><td COLSPAN=9>
 Tr: St 24 W 8% P 4% S 17% ITM 29% ROI 135</td></tr><tr  class="horse_1_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/24/2018 - 5,    AQU,    GD,    27,    F,    27,    (T)-1,    CLM,    14000,    88,    31,    74,    1,    3,    6,    9,    750.00,    6,    7,    4,    6,    3,    Hernandez Rafael M,    L,    119,    13.00,    Valyrian,    Delta Outlaw,    Kenyan,    5w upper, mild kick,    6,    </div></br> 
<div style="color:red" > 02/04/2018 - 4,    AQU,    FT,    40,    F,    40,    (T)-1,    CLM,    14000,    89,    40,    77,    3,    7,    5,    4,    360.00,    5,    5,    4,    9,    4,    Reyes Luis R,    L,    119,    25.50,    Toohottoevenspeak,    Peculiar Sensation,    Delta Outlaw,    ins,4w 1/8,outkicked,    7,    </div></br> 
<div style="color:blue" > 01/15/2018 - 9,    AQU,    FT,    32,    F,    32,    (T)-1,    CLM,    14000,    88,    58,    76,    9,    7,    6,    4,    280.00,    7,    5,    7,    8,    4,    Reyes Luis R,    L,    114,    71.75,    Rossie Val,    Toohottoevenspeak,    Sir Alfred,    2w trn,3w upr,alt to4p,    10,    </div></br> 
<div style="color:red" > 12/30/2017 - 2,    AQU,    FT,    46,    F,    46,    (T)-1,    CLM,    14000,    90,    70,    70,    4,    2,    3,    1,    700.00,    4,    9,    4,    10,    3,    Reyes Luis R,    L,    115,    15.30,    Finnegan,    Competitiveness,    Kenyan,    3w turn,6p 1/8,no bids,    4,    </div></br> 
<div style="color:blue" > 12/16/2017 - 3,    PRX,    FT,    40,    F,    40,    (T)-1,    CLM,    12500,    84,    42,    56,    1,    8,    8,    6,    960.00,    8,    8,    8,    13,    8,    Vargas Jr Jorge A,    L,    121,    8.00,    Bitumen,    Shipthegrip,    Bubba's Will,    no factor,    8,    </div></br> 
<div style="color:red" > 11/25/2017 - 7,    PRX,    FT,    0,    F,    0,    (T)0,    SCR,    0,    80,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:blue" > 10/20/2017 - 9,    BEL,    FT,    61,    F,    61,    (T)-1,    CLM,    14000,    86,    23,    72,    5,    6,    5,    8,    450.00,    5,    8,    6,    9,    6,    Arroyo Angel S,    L,    120,    16.90,    Here and There,    Southern Union,    Delta Outlaw,    3w 1/2,6p 1/8,no bids,    7,    </div></br> 
<div style="color:red" > 03/10/2017 - 5,    AQU,    GD,    56,    F,    56,    (T)-1,    CLM,    14000,    88,    96,    68,    6,    3,    1,    -1,    10.00,    2,    10,    5,    20,    6,    Ayala Addiel J,    L,    113,    13.20,    Shadow Rider,    Investigator,    Mister Monolo,    BUMP AFTER BRK,SPAR 2P,    9,    </div></br> 
<div style="color:blue" > 02/02/2017 - 4,    AQU,    FT,    24,    F,    24,    (T)-1,    CLM,    25000,    94,    18,    72,    3,    7,    8,    12,    1160.00,    8,    13,    8,    13,    8,    Davis Dylan,    L,    120,    7.40,    Tasunke Witco,    Proletariat,    Chomsky,    trailed throughout,    8,    </div></br> 
<div style="color:red" > 01/22/2017 - 6,    AQU,    FT,    24,    F,    24,    (T)-1,    CLM,    25000,    99,    21,    77,    5,    9,    10,    9,    650.00,    9,    5,    8,    3,    6,    Davis Dylan,    L,    118,    20.30,    Sol the Freud,    Pegasus Red,    H Man,    cut corner, outkicked,    10,    </div></br> 
<div style="color:blue" > 01/01/2017 - 5,    AQU,    FT,    24,    F,    24,    (T)-1,    CLM,    14000,    88,    42,    82,    9,    8,    7,    10,    1250.00,    6,    14,    6,    11,    3,    Cancel Eric,    L,    120,    1.05,    Bustin It,    Police Escort,    Kenyan,    6w upper, belatedly,    9,    </div></br> 
<div style="color:red" > 12/17/2016 - 7,    AQU,    GD,    24,    F,    24,    (T)-1,    STR,    50000,    96,    77,    89,    1,    6,    7,    5,    560.00,    7,    8,    7,    8,    6,    Franco Manuel,    L,    118,    19.60,    Tashreeh,    Data Driven,    Do Share,    BRUSH GATE, NO IMPACT,    7,    </div></br> 
</td> </tr>
<tr  class="horse_1_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 89  ( date: 2016 ) 18   2   1   4   $67345.00    </br> 
  2017 : 7   0   0   2   $6465.00    </br> 
  2018 : 3   0   0   1   $5400.00    </br> 
  GP: 9   0   0   2   $9165.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 3   0   0   1   $4885.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="4" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> LOST IRON</td>
<td> McCarthy Trevor</td>
<td> Rice Linda</td>
<td> Dorothy A. Matz</td>
<td> The Estate of Anthony Miuccio</td>
<td> Flatter</td>
<td> Vanquished</td>
<td> 2/1</td></tr><tr class="horse_2_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_2_4" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_2_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 4,    AQU,    MY,    0,    F,    0,    (T)0,    SCR,    0,    88,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 02/10/2018 - 2,    AQU,    GD,    50,    F,    50,    (T)-1,    CLM,    25000,    90,    71,    79,    4,    1,    2,    0,    -10.00,    1,    1,    2,    1,    2,    Mccarthy Trevor,    L,    120,    2.65,    The Great Samurai,    Lost Iron,    Acoustic,    mostly 3w, kept on,    6,    </div></br> 
<div style="color:blue" > 12/30/2017 - 2,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    90,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 12/10/2017 - 5,    AQU,    GD,    55,    F,    55,    (T)-1,    CLM,    25000,    92,    104,    72,    5,    4,    3,    2,    -100.00,    1,    -6,    1,    -9,    1,    Ortiz Jr Irad,    L,    121,    1.55,    Lost Iron,    Mydadfloyd,    City Traveler,    4w upper, drew clear,    6,    </div></br> 
<div style="color:blue" > 11/11/2017 - 2,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    95,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/20/2017 - 4,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    94,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 11/30/2016 - 5,    AQU,    MY,    38,    F,    38,    (T)-1,    CLM,    40000,    91,    56,    80,    2,    9,    9,    8,    800.00,    7,    7,    4,    8,    2,    Ortiz Jose L,    L,    119,    2.40,    Going Strong,    Lost Iron,    Cordero,    5w upper, no match,    10,    </div></br> 
<div style="color:red" > 11/09/2016 - 4,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    96,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 09/23/2016 - 7,    BEL,    FM,    130,    F,    130,    (T)27,    CLM,    50000,    101,    76,    86,    8,    8,    8,    11,    1060.00,    8,    11,    7,    12,    7,    Alvarado Junior,    L,    118,    8.50,    Abiding Star,    Bibbo,    Nominal Dollars,    SLUGGISH ST,INS,6P 1/8,    8,    </div></br> 
<div style="color:red" > 06/25/2016 - 8,    BEL,    FT,    24,    F,    24,    (T)-1,    ALW,    0,    100,    79,    61,    1,    7,    7,    10,    1000.00,    7,    14,    7,    21,    7,    Alvarado Junior,    L,    118,    6.50,    Ready Dancer,    Conviction,    Cadeyrn,    ins,5w upr, outrun,    7,    </div></br> 
<div style="color:blue" > 06/18/2016 - 8,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    105,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 05/14/2016 - 9,    BEL,    FT,    20,    F,    20,    (T)-1,    STK,    0,    110,    82,    84,    8,    7,    8,    12,    1210.00,    8,    15,    7,    18,    7,    Alvarado Junior,    L,    116,    41.50,    Unified,    Governor Malibu,    Wild About Deb,    5-6w upper, no impact,    8,    </div></br> 
</td> </tr>
<tr  class="horse_2_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 86  ( date: 2016 ) 9   2   2   0   $83738.00    </br> 
  2017 : 1   1   0   0   $20400.00    </br> 
  2018 : 1   0   1   0   $8800.00    </br> 
  GP: 3   1   2   0   $37200.00    </br> 
  Turf: 1   0   0   0   $434.00    </br> 
  Dist: 3   1   2   0   $37200.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="4" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> WILSHIRE STAR</td>
<td> DeCarlo Christopher P</td>
<td> Levine Bruce N</td>
<td> Donald R. Dizney, LLC</td>
<td> Duffield, Steven, T.</td>
<td> Eskendereya</td>
<td> Wilshire Boulevard</td>
<td> 3/1</td></tr><tr class="horse_3_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 8 W 13% P 13% S 0% ITM 26% ROI -53</td></tr><tr  class="horse_3_4" style="display:none;"><td COLSPAN=9>
 Tr: St 15 W 20% P 20% S 7% ITM 47% ROI 107</td></tr><tr  class="horse_3_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 4,    AQU,    MY,    48,    F,    48,    (T)-1,    CLM,    14000,    88,    91,    88,    6,    1,    1,    -1,    -50.00,    1,    2,    2,    4,    2,    DeCarlo Christopher P,    L,    120,    9.80,    Fielding Gold,    Wilshire Star,    Mr. Euro,    in hand 2p, 2nd best,    8,    </div></br> 
<div style="color:red" > 01/26/2018 - 5,    AQU,    FT,    50,    F,    50,    (T)-1,    CLM,    14000,    86,    71,    56,    1,    6,    3,    2,    410.00,    4,    11,    5,    21,    7,    Carmouche Kendrick,    L,    121,    3.90,    Guick,    Fielding Gold,    Thirsty Donnerstag,    BROKE OUT ST, TIRED,    7,    </div></br> 
<div style="color:blue" > 12/15/2017 - 8,    AQU,    FT,    55,    F,    55,    (T)-1,    CLM,    14000,    88,    91,    75,    5,    4,    1,    -2,    100.00,    2,    3,    2,    8,    3,    Carmouche Kendrick,    L,    119,    4.90,    Deep Sea,    Fielding Gold,    Wilshire Star,    4w upper, wknd late,    9,    </div></br> 
<div style="color:red" > 12/03/2017 - 7,    WO,    FT,    40,    F,    40,    (T)-1,    CLM,    20000,    88,    80,    86,    3,    4,    1,    -2,    -50.00,    1,    -1,    1,    3,    2,    Campbell Jesse M,    L,    118,    6.40,    Dexter Road,    Wilshire Star,    Seize the Bay,    BMP START,PASSED MIDST,    8,    </div></br> 
<div style="color:blue" > 11/26/2017 - 1,    WO,    FT,    40,    F,    40,    (T)-1,    CLM,    10000,    87,    54,    75,    3,    3,    1,    -1,    -150.00,    1,    -1,    1,    1,    2,    Campbell Jesse M,    L,    118,    5.05,    Martini Kid,    Wilshire Star,    Gizmo Jones,    led,gave way grdgngly,    8,    </div></br> 
<div style="color:red" > 11/03/2017 - 5,    WO,    FT,    40,    F,    40,    (T)-1,    CLM,    6250,    79,    76,    80,    3,    6,    1,    -1,    -100.00,    1,    -2,    1,    -4,    1,    Campbell Jesse M,    L,    118,    1.45,    Wilshire Star,    Augusto B,    Fine Grit,    PINCHED BACK,RDN OUT,    10,    </div></br> 
<div style="color:blue" > 10/15/2017 - 12,    WO,    FT,    40,    F,    40,    (T)-1,    CLM,    20000,    85,    113,    72,    4,    4,    1,    -1,    -50.00,    1,    5,    9,    11,    12,    Wilson Emma-Jayne,    L,    119,    6.65,    Remembering Mickey,    Conquest Swagman,    Splash With Me,    spd,tired top stretch,    12,    </div></br> 
<div style="color:red" > 09/30/2017 - 10,    WO,    GD,    40,    F,    40,    (T)60,    CLM,    20000,    86,    111,    82,    7,    8,    1,    -2,    -150.00,    1,    2,    3,    6,    5,    Garcia Alan,    L,    117,    13.75,    Martini Kid,    Unbridled Piper,    Leopard Cat,    gave way upper stretch,    11,    </div></br> 
<div style="color:blue" > 09/17/2017 - 10,    WO,    FM,    0,    F,    0,    (T)0,    SCR,    0,    90,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 09/01/2017 - 7,    WO,    FM,    40,    F,    40,    (T)15,    OCL,    40000,    93,    40,    68,    4,    6,    7,    11,    800.00,    7,    6,    7,    10,    7,    Lermyte Jerome,    L,    118,    10.00,    Ratface Macdougall,    Colton's Corner,    Signature Data,    rail-3w,did not menace,    8,    </div></br> 
<div style="color:blue" > 08/20/2017 - 9,    WO,    FT,    40,    F,    40,    (T)-1,    OCL,    40000,    89,    84,    84,    4,    4,    2,    2,    -100.00,    1,    1,    3,    5,    4,    Lermyte Jerome,    L,    117,    12.40,    Drumcliff,    Synthesize,    Daily Brew,    led into str,headed,    7,    </div></br> 
<div style="color:red" > 08/19/2017 - 10,    WO,    FM,    0,    F,    0,    (T)0,    SCR,    0,    94,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
</td> </tr>
<tr  class="horse_3_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 88  ( date: 2018 ) 21   2   8   4   $72219.00    </br> 
  2017 : 16   2   5   4   $57579.00    </br> 
  2018 : 2   0   1   0   $5670.00    </br> 
  GP: 3   0   1   1   $8370.00    </br> 
  Turf: 9   0   5   2   $28429.00    </br> 
  Dist: 6   1   2   1   $21564.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="4" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> VILMA</td>
<td> Franco Manuel</td>
<td> Rice Linda</td>
<td> John C. Oxley</td>
<td> Rice, Linda and Sacandaga Stable</td>
<td> Broken Vow</td>
<td> Vivid Sunset</td>
<td> 7/2</td></tr><tr class="horse_4_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_4_4" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_4_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/16/2018 - 6,    AQU,    SY,    50,    F,    50,    (T)-1,    CLM,    50000,    101,    82,    71,    6,    6,    1,    -0,    550.00,    5,    10,    7,    16,    7,    Franco Manuel,    L,    118,    2.50,    Halloween Horror,    Hembree,    Micozzi,    ins,coaxed along,tired,    7,    </div></br> 
<div style="color:red" > 01/20/2018 - 4,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    99,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 01/12/2018 - 1,    AQU,    MY,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 06/23/2017 - 2,    BEL,    FT,    48,    F,    48,    (T)-1,    STR,    50000,    97,    71,    80,    7,    2,    2,    1,    -50.00,    1,    4,    4,    12,    4,    Ortiz Jose L,    L,    124,    2.95,    Phat Man,    Shadow Rider,    Planet Trailblazer,    dueled outside, wknd,    7,    </div></br> 
<div style="color:blue" > 06/16/2017 - 7,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 05/26/2017 - 7,    BEL,    MY,    63,    F,    63,    (T)-1,    STR,    50000,    97,    -99,    -97,    2,    6,    6,    100,    9999.00,    6,    100,    6,    100,    6,    Velasquez Cornelio H,    L,    124,    5.40,    Dogtown,    Phat Man,    Shadow Rider,    STUMBLED ST,LOST RIDER,    6,    </div></br> 
<div style="color:blue" > 05/18/2017 - 4,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 05/05/2017 - 9,    BEL,    SY,    40,    F,    40,    (T)-1,    CLM,    40000,    93,    105,    97,    5,    3,    1,    -1,    -100.00,    1,    -2,    1,    -5,    1,    Velasquez Cornelio H,    L,    124,    2.00,    Vilma,    Noble and True,    Snowfly,    coax in 2p, drew clear,    5,    </div></br> 
<div style="color:blue" > 04/22/2017 - 2,    LRL,    MY,    20,    F,    20,    (T)-1,    SOC,    25000,    88,    83,    85,    4,    7,    2,    2,    300.00,    2,    5,    3,    5,    3,    Velasquez Cornelio H,    L,    120,    2.30,    Iredell,    All Out of Aces,    Vilma,    bit close st,4wd,even,    8,    </div></br> 
<div style="color:red" > 03/25/2017 - 7,    AQU,    FT,    80,    F,    80,    (T)-1,    STR,    50000,    101,    61,    73,    5,    4,    3,    3,    310.00,    4,    9,    5,    13,    6,    Velasquez Cornelio H,    L,    118,    3.70,    Bene,    Dogtown,    Planet Trailblazer,    BUMP START,INSIDE,TIRD,    7,    </div></br> 
<div style="color:blue" > 01/06/2017 - 1,    AQU,    FT,    80,    F,    80,    (T)-1,    MCL,    30000,    94,    82,    107,    1,    5,    1,    -3,    -400.00,    1,    -10,    1,    -12,    1,    Ortiz Jose L,    L,    121,    0.60,    Vilma,    Fielding Gold,    Pontastic,    in hand 2p, went clear,    6,    </div></br> 
<div style="color:red" > 12/03/2016 - 2,    AQU,    FT,    38,    F,    38,    (T)-1,    MSW,    0,    95,    69,    68,    2,    9,    7,    5,    1050.00,    8,    14,    8,    18,    8,    Montanez Rosario,    L,    121,    7.80,    Jess I Am,    Karma Delight,    Our Cousin Dick,    BUMP ST, SHUFFLED 1/2P,    10,    </div></br> 
</td> </tr>
<tr  class="horse_4_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 107  ( date: 2017 ) 20   2   3   3   $88070.00    </br> 
  2017 : 6   2   0   1   $51410.00    </br> 
  2018 : 1   0   0   0   $440.00    </br> 
  GP: 10   2   2   1   $67690.00    </br> 
  Turf: 1   0   0   0   $174.00    </br> 
  Dist: 14   2   2   2   $73964.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="4" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> STARRY MESSENGER</td>
<td> Rocco Jr Joseph</td>
<td> Lawrence II James L</td>
<td> Pam & Martin Wygod</td>
<td> Hickory Made Stables</td>
<td> Courageous Cat</td>
<td> Spokeswoman</td>
<td> 20/1</td></tr><tr class="horse_5_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 6% P 14% S 6% ITM 26% ROI -28</td></tr><tr  class="horse_5_4" style="display:none;"><td COLSPAN=9>
 Tr: St 14 W 21% P 29% S 7% ITM 57% ROI -17</td></tr><tr  class="horse_5_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/08/2018 - 4,    PEN,    FT,    40,    F,    40,    (T)-1,    CLM,    10000,    84,    99,    80,    2,    1,    2,    0,    -100.00,    1,    0,    2,    1,    2,    Rivera Edwin,    L,    121,    6.30,    High Five Cotton,    Starry Messenger,    A Smile for Ellie,    VIED INS, RANK, DUG IN,    6,    </div></br> 
<div style="color:red" > 02/24/2018 - 7,    LRL,    SY,    0,    F,    0,    (T)0,    SCR,    0,    93,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:blue" > 02/10/2018 - 2,    LRL,    SY,    110,    F,    110,    (T)-1,    CLM,    5000,    74,    90,    68,    3,    2,    1,    -4,    -450.00,    1,    -3,    1,    -3,    1,    Vargas Jr Jorge A,    L,    124,    1.90,    Starry Messenger,    A True Gentleman,    Wilko's Goldeneye,    3w,ask 1/4,strng drve,    6,    </div></br> 
<div style="color:red" > 01/26/2018 - 4,    LRL,    FT,    55,    F,    55,    (T)-1,    CLM,    10000,    82,    46,    67,    2,    7,    7,    8,    1160.00,    7,    12,    6,    12,    5,    Hamilton Weston,    L,    113,    7.30,    Tam Tam Attack,    Smart Royal T,    All Spun Up,    rate,ins foe 1/2,empty,    8,    </div></br> 
<div style="color:blue" > 01/15/2018 - 9,    PRX,    FT,    48,    F,    48,    (T)-1,    CLM,    7500,    71,    67,    58,    9,    4,    2,    0,    10.00,    2,    2,    2,    2,    3,    Rivera Edwin,    L,    121,    5.00,    December Red,    Tree Top Lover,    Starry Messenger,    dueled, held on well,    11,    </div></br> 
<div style="color:red" > 01/08/2018 - 4,    LRL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    81,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 12/23/2017 - 2,    CT,    SY,    10,    F,    10,    (T)-1,    CLM,    5000,    76,    51,    67,    3,    5,    4,    3,    0.00,    0,    3,    3,    1,    3,    Montano Jose,    L,    122,    3.10,    High Mileage,    Sr. Borincano,    Starry Messenger,    7P TURN, DRFTD IN/OUT,    6,    </div></br> 
<div style="color:red" > 11/20/2017 - 1,    LRL,    GD,    0,    F,    0,    (T)0,    SCR,    0,    83,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Also-Eligible,    0,    </div></br> 
<div style="color:blue" > 10/28/2017 - 8,    MED,    FM,    24,    F,    24,    (T)0,    CLM,    7500,    83,    92,    84,    5,    2,    1,    -0,    -50.00,    1,    -2,    1,    0,    2,    Juarez Nik,    L,    124,    1.60,    Real Creel,    Starry Messenger,    Helluva Choice,    duel insd, dug, sniped,    9,    </div></br> 
<div style="color:red" > 10/07/2017 - 2,    MED,    FM,    24,    F,    24,    (T)0,    MCL,    10000,    69,    70,    76,    5,    1,    1,    -1,    -50.00,    1,    -3,    1,    -4,    1,    Juarez Nik,    L,    122,    1.10,    Starry Messenger,    Southampton Pride,    Fairly Bold,    set press pace, clear,    11,    </div></br> 
<div style="color:blue" > 09/27/2017 - 2,    BEL,    FT,    56,    F,    56,    (T)-1,    MCL,    25000,    80,    92,    49,    7,    2,    1,    -1,    50.00,    2,    7,    5,    17,    5,    Juarez Nik,    L,    124,    9.10,    Nut Nut,    I Miss My Father,    Giant Rocks,    duel, vied betw, tired,    7,    </div></br> 
<div style="color:red" > 02/16/2017 - 8,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    79,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
</td> </tr>
<tr  class="horse_5_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 84  ( date: 2017 ) 13   2   2   3   $31374.00    </br> 
  2017 : 5   1   1   1   $12395.00    </br> 
  2018 : 4   1   1   1   $14260.00    </br> 
  GP: 5   1   1   2   $15260.00    </br> 
  Turf: 3   1   1   0   $11460.00    </br> 
  Dist: 3   1   1   1   $14730.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="4" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> BUNYAAN</td>
<td> Lezcano Abel</td>
<td> Persaud Randi</td>
<td> Shadwell Farm, LLC</td>
<td> Guyana Rocky LLC</td>
<td> Invasor (ARG)</td>
<td> Basaata</td>
<td> 20/1</td></tr><tr class="horse_6_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 33 W 3% P 6% S 9% ITM 18% ROI -78</td></tr><tr  class="horse_6_4" style="display:none;"><td COLSPAN=9>
 Tr: St 10 W 0% P 10% S 10% ITM 20% ROI -100</td></tr><tr  class="horse_6_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 4,    AQU,    MY,    48,    F,    48,    (T)-1,    CLM,    14000,    88,    80,    46,    5,    8,    5,    3,    950.00,    7,    17,    8,    29,    8,    Simpson Trevor,    L,    120,    20.30,    Fielding Gold,    Wilshire Star,    Mr. Euro,    AWKWARD ST, OVERLAND,    8,    </div></br> 
<div style="color:red" > 02/10/2018 - 2,    AQU,    GD,    50,    F,    50,    (T)-1,    CLM,    25000,    90,    62,    75,    2,    4,    5,    3,    210.00,    4,    5,    4,    4,    5,    Simpson Trevor,    L,    120,    41.75,    The Great Samurai,    Lost Iron,    Acoustic,    STEADY 6 1/2, 7W UPPER,    6,    </div></br> 
<div style="color:blue" > 01/26/2018 - 5,    AQU,    FT,    50,    F,    50,    (T)-1,    CLM,    14000,    86,    66,    63,    4,    7,    5,    4,    510.00,    6,    11,    6,    17,    6,    Simpson Trevor,    L,    121,    12.80,    Guick,    Fielding Gold,    Thirsty Donnerstag,    chased 4-5w, tired,    7,    </div></br> 
<div style="color:red" > 01/19/2018 - 1,    AQU,    FT,    50,    F,    50,    (T)-1,    CLM,    25000,    96,    54,    81,    5,    6,    7,    6,    910.00,    6,    12,    4,    13,    4,    Simpson Trevor,    L,    121,    19.20,    California Swing,    Conquest Expresso,    Baratti,    chased 3-2w, improved,    7,    </div></br> 
<div style="color:blue" > 12/29/2017 - 6,    AQU,    FT,    53,    F,    53,    (T)-1,    CLM,    25000,    87,    82,    64,    2,    1,    3,    3,    150.00,    4,    2,    3,    -3,    1,    Simpson Trevor,    L,    124,    20.50,    Bunyaan,    Truly a Moon Shot,    Tale of Mist,    BMP ST,2W,ALTER 5P1/8,    5,    </div></br> 
<div style="color:red" > 12/22/2017 - 7,    AQU,    FT,    55,    F,    55,    (T)-1,    CLM,    16000,    84,    52,    69,    2,    8,    5,    2,    850.00,    5,    10,    4,    12,    3,    Simpson Trevor,    L,    124,    20.90,    The Great Samurai,    Daddy's Home,    Bunyaan,    5w upper, weakened,    8,    </div></br> 
<div style="color:blue" > 12/04/2017 - 5,    PRX,    FT,    0,    F,    0,    (T)0,    SCR,    0,    77,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 11/30/2017 - 4,    AQU,    FT,    55,    F,    55,    (T)-1,    CLM,    16000,    84,    63,    62,    1,    5,    4,    2,    700.00,    5,    7,    5,    7,    5,    Simpson Trevor,    L,    124,    18.60,    Spring Emperor,    City Traveler,    Gun for Hire,    ins 6f,ask turn,5w upr,    7,    </div></br> 
<div style="color:blue" > 11/17/2017 - 6,    AQU,    FT,    44,    F,    44,    (T)-1,    CLM,    16000,    84,    65,    51,    5,    8,    8,    4,    470.00,    8,    15,    8,    19,    8,    Simpson Trevor,    L,    124,    24.50,    Conquest Expresso,    Ten Twenty Nine,    Mr. Euro,    chased 5w, no impact,    9,    </div></br> 
<div style="color:red" > 11/11/2017 - 2,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    95,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 10/20/2017 - 6,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    95,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/12/2017 - 3,    BEL,    FT,    45,    F,    45,    (T)-1,    CLM,    16000,    84,    70,    60,    1,    5,    4,    4,    710.00,    8,    11,    8,    15,    7,    Ortiz Jose L,    L,    121,    1.20,    Honorable Jonas,    Bar None,    Mr. McFrosty,    ins to 5/16,6w upper,    9,    </div></br> 
</td> </tr>
<tr  class="horse_6_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 81  ( date: 2018 ) 19   2   2   1   $78845.00    </br> 
  2017 : 12   2   2   1   $69895.00    </br> 
  2018 : 4   0   0   0   $3970.00    </br> 
  GP: 8   1   0   1   $28015.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 15   2   1   1   $70300.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="4" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> KARMA DELIGHT</td>
<td> Simpson Trevor</td>
<td> Baker Charlton</td>
<td> SF Bloodstock LLC</td>
<td> J.W. Singer LLC</td>
<td> Medaglia d'Oro</td>
<td> Speed Succeeds</td>
<td> 9/2</td></tr><tr class="horse_7_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 9 W 11% P 11% S 11% ITM 33% ROI 41</td></tr><tr  class="horse_7_4" style="display:none;"><td COLSPAN=9>
 Tr: St 18 W 11% P 17% S 11% ITM 39% ROI -10</td></tr><tr  class="horse_7_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/20/2018 - 4,    AQU,    FT,    48,    F,    48,    (T)-1,    CLM,    50000,    99,    72,    60,    9,    3,    3,    1,    560.00,    7,    14,    9,    22,    9,    Hernandez Rafael M,    L,    121,    6.60,    Devine Dental,    E J's Legacy,    Starship Zeus,    speed for 1/2,gave way,    9,    </div></br> 
<div style="color:red" > 12/10/2017 - 6,    AQU,    GD,    55,    F,    55,    (T)-1,    ALW,    0,    100,    82,    81,    1,    5,    1,    -0,    100.00,    3,    9,    6,    17,    7,    Rocco Jr Joseph,    L,    121,    11.60,    T R Crew,    Hammerin Aamer,    Starship Zeus,    vied 3-2w, tired,    7,    </div></br> 
<div style="color:blue" > 10/27/2017 - 7,    BEL,    FT,    44,    F,    44,    (T)-1,    STR,    50000,    96,    64,    97,    4,    1,    2,    0,    10.00,    2,    -0,    1,    -0,    1,    Rosario Joel,    L,    122,    3.55,    Karma Delight,    Scarf It Down,    Shadow Rider,    spar 2-3w,ask upr,game,    6,    </div></br> 
<div style="color:red" > 10/04/2017 - 3,    BEL,    FT,    40,    F,    40,    (T)-1,    STR,    50000,    98,    66,    92,    5,    1,    1,    -1,    60.00,    3,    3,    3,    5,    3,    Rosario Joel,    L,    122,    3.90,    Carlino,    Shadow Rider,    Karma Delight,    2w vs duo to upper,    5,    </div></br> 
<div style="color:blue" > 09/29/2017 - 7,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    94,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 08/27/2017 - 3,    SAR,    FT,    34,    F,    34,    (T)-1,    CLM,    40000,    98,    55,    78,    2,    1,    1,    -1,    -100.00,    1,    6,    3,    13,    3,    Saez Luis,    L,    124,    4.40,    Portfolio Manager,    Harlan Punch,    Karma Delight,    in hand 2p, weakened,    5,    </div></br> 
<div style="color:blue" > 08/14/2017 - 9,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 07/16/2017 - 3,    BEL,    FT,    46,    F,    46,    (T)-1,    MCL,    30000,    80,    100,    86,    1,    2,    3,    2,    50.00,    2,    -2,    1,    -6,    1,    Saez Luis,    L,    124,    3.30,    Karma Delight,    Refinance,    Spring Emperor,    rated ins, drew clear,    7,    </div></br> 
<div style="color:blue" > 06/15/2017 - 9,    BEL,    FM,    84,    F,    84,    (T)9,    MCL,    75000,    90,    105,    75,    2,    8,    4,    2,    210.00,    5,    8,    9,    12,    9,    Velazquez John R,    L,    124,    15.70,    Somesville,    Herecomesyourman,    Big Exchange,    tracked inside, tired,    11,    </div></br> 
<div style="color:red" > 05/12/2017 - 7,    BEL,    FM,    103,    F,    103,    (T)9,    MSW,    0,    95,    84,    90,    12,    5,    6,    4,    450.00,    5,    5,    6,    4,    7,    Rosario Joel,    L,    124,    10.90,    Carbon Data,    Into the Breach,    Rocketry,    4w 1st,4-3w 2nd,no bid,    12,    </div></br> 
<div style="color:blue" > 03/15/2017 - 7,    GP,    FT,    5,    F,    5,    (T)-1,    MSW,    0,    91,    72,    80,    6,    1,    6,    4,    360.00,    5,    5,    4,    6,    3,    Rosario Joel,    L,    123,    4.50,    Street Jersey,    Thebigfundamental,    Karma Delight,    SANDWICHED ST,SWUNG 3W,    8,    </div></br> 
<div style="color:red" > 12/29/2016 - 7,    AQU,    MY,    76,    F,    76,    (T)-1,    MSW,    0,    95,    95,    16,    3,    7,    6,    5,    1160.00,    7,    21,    7,    41,    7,    Franco Manuel,    L,    121,    2.95,    Admiral Blue,    Strong Side,    Spring On Curlin,    chased 2p, no factor,    7,    </div></br> 
</td> </tr>
<tr  class="horse_7_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 97  ( date: 2017 ) 11   2   1   3   $81922.00    </br> 
  2017 : 8   2   0   3   $69102.00    </br> 
  2018 : 1   0   0   0   $220.00    </br> 
  GP: 4   1   0   1   $39390.00    </br> 
  Turf: 2   0   0   0   $432.00    </br> 
  Dist: 8   2   1   2   $76790.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="8" race="4" title="Show More..." > 8</br> <span class="past-performance-plus-b" > </span> </td>
<td> SPRING EMPEROR</td>
<td> Carmouche Kendrick</td>
<td> Ryerson James T</td>
<td> WinStar Farm, LLC</td>
<td> Ryerson, James, T.</td>
<td> Spring At Last</td>
<td> Empress of Gold</td>
<td> 10/1</td></tr><tr class="horse_8_4" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_8_4" style="display:none;"><td COLSPAN=9>
 Tr: St 12 W 0% P 17% S 0% ITM 17% ROI -100</td></tr><tr  class="horse_8_4" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 4,    AQU,    MY,    48,    F,    48,    (T)-1,    CLM,    14000,    88,    79,    69,    4,    5,    6,    4,    600.00,    4,    10,    4,    15,    4,    Diaz Jr Hector R,    L,    117,    8.80,    Fielding Gold,    Wilshire Star,    Mr. Euro,    chased 4-5w, weakened,    8,    </div></br> 
<div style="color:red" > 01/19/2018 - 1,    AQU,    FT,    50,    F,    50,    (T)-1,    CLM,    25000,    96,    56,    75,    7,    5,    6,    5,    860.00,    5,    14,    5,    16,    5,    Carmouche Kendrick,    L,    121,    4.90,    California Swing,    Conquest Expresso,    Baratti,    chased 3-4w, tired,    7,    </div></br> 
<div style="color:blue" > 11/30/2017 - 4,    AQU,    FT,    55,    F,    55,    (T)-1,    CLM,    16000,    84,    63,    74,    7,    3,    3,    2,    100.00,    2,    0,    2,    -2,    1,    Carmouche Kendrick,    L,    121,    2.85,    Spring Emperor,    City Traveler,    Gun for Hire,    vied 4w turn,won duel,    7,    </div></br> 
<div style="color:red" > 10/14/2017 - 10,    BEL,    FM,    72,    F,    72,    (T)9,    CLM,    25000,    95,    82,    83,    1,    8,    3,    3,    260.00,    4,    5,    6,    7,    7,    Carmouche Kendrick,    L,    120,    13.50,    Warm Springs,    New Jersey John,    Croce d'Oro,    tracked ins, weakened,    9,    </div></br> 
<div style="color:blue" > 09/23/2017 - 8,    BEL,    FM,    82,    F,    82,    (T)18,    AOC,    25000,    104,    83,    94,    1,    9,    9,    10,    860.00,    8,    9,    9,    9,    8,    DeCarlo Christopher P,    L,    118,    97.00,    Aquaphobia,    Memories of Peter,    King of Spades,    save ground,no factor,    11,    </div></br> 
<div style="color:red" > 09/04/2017 - 11,    PRX,    FT,    40,    F,    40,    (T)-1,    MCL,    30000,    70,    44,    75,    3,    6,    4,    2,    60.00,    3,    -2,    1,    -5,    1,    Carmouche Kendrick,    L,    120,    3.40,    Spring Emperor,    British Bulldog,    Unpredictable,    rail early, edged away,    8,    </div></br> 
<div style="color:blue" > 08/10/2017 - 2,    SAR,    FT,    82,    F,    82,    (T)-1,    MCL,    30000,    84,    53,    62,    2,    6,    7,    7,    560.00,    6,    9,    5,    15,    6,    Alvarado Junior,    L,    118,    10.70,    Hy Brasil,    Battle Ready,    Moon Over Montana,    4w upper, tired,    7,    </div></br> 
<div style="color:red" > 07/16/2017 - 3,    BEL,    FT,    46,    F,    46,    (T)-1,    MCL,    30000,    80,    86,    73,    4,    5,    6,    6,    350.00,    4,    5,    4,    8,    3,    Alvarado Junior,    L,    117,    17.00,    Karma Delight,    Refinance,    Spring Emperor,    4w upper, improved,    7,    </div></br> 
<div style="color:blue" > 06/24/2017 - 4,    BEL,    FT,    58,    F,    58,    (T)-1,    MCL,    30000,    65,    51,    53,    7,    6,    4,    3,    400.00,    4,    7,    4,    12,    4,    Lezcano Jose,    L,    118,    8.00,    Basic Hero,    Shoot the Gap,    Won't Burn,    inside turn,drve by1/4,    8,    </div></br> 
</td> </tr>
<tr  class="horse_8_4" style="display:none;"><td COLSPAN=9>
LifeTime Record 94  ( date: 2017 ) 9   2   0   1   $41790.00    </br> 
  2017 : 7   2   0   1   $39120.00    </br> 
  2018 : 2   0   0   0   $2670.00    </br> 
  GP: 9   2   0   1   $41790.00    </br> 
  Turf: 2   0   0   0   $470.00    </br> 
  Dist: 5   2   0   1   $39170.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="5" race_date="20180317" claimamt="40000" dist_disp="1M" todays_cls="94" purse="59000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #5</span> - 
<span> Claiming $40000</span> - 
<span> Distance 1M</span> - 
<span> Todays CLS 94</span> -
<span> Purse $59000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Pick 3 Races (5-7) Grand Slam Races (5-8), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="2" race="5" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> BARRIER TO ENTRY</td>
<td> Davis Dylan</td>
<td> Rice Linda</td>
<td> Fox Ridge Farm</td>
<td> Race View Stable</td>
<td> City Zip</td>
<td> Stormy Winter</td>
<td> 8/1</td></tr><tr class="horse_2_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_2_5" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_2_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/16/2018 - 7,    AQU,    SY,    28,    F,    28,    (T)-1,    AOC,    40000,    95,    103,    79,    5,    3,    2,    1,    110.00,    3,    4,    2,    9,    2,    Davis Dylan,    L,    119,    16.50,    Palladian Bridge,    Barrier to Entry,    Andesine,    BROKE IN ST, KEPT ON,    7,    </div></br> 
<div style="color:red" > 12/07/2017 - 7,    AQU,    FM,    96,    F,    96,    (T)24,    AOC,    40000,    99,    82,    89,    1,    6,    4,    6,    760.00,    6,    5,    6,    5,    4,    Ortiz Jr Irad,    L,    120,    3.65,    Munchkin Money,    First Appeal,    Vicki's Dancer,    cut corner, kept on,    6,    </div></br> 
<div style="color:blue" > 11/23/2017 - 6,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    97,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Off-Turf,    0,    </div></br> 
<div style="color:red" > 10/27/2017 - 6,    BEL,    FM,    101,    F,    101,    (T)27,    AOC,    40000,    95,    96,    100,    1,    2,    3,    2,    260.00,    4,    3,    5,    0,    5,    Ortiz Jr Irad,    L,    120,    3.70,    Driven by Speed,    First Appeal,    Munchkin Money,    5w upper, missed,    9,    </div></br> 
<div style="color:blue" > 10/04/2017 - 8,    BEL,    FM,    75,    F,    75,    (T)0,    AOC,    40000,    95,    125,    87,    4,    2,    2,    1,    100.00,    2,    -1,    1,    2,    3,    Ortiz Jr Irad,    L,    120,    6.00,    Broken Border,    Eloweasel,    Barrier to Entry,    rated ins, led, caught,    8,    </div></br> 
<div style="color:red" > 09/08/2017 - 7,    BEL,    FM,    175,    F,    175,    (T)9,    AOC,    40000,    94,    100,    95,    6,    2,    2,    2,    50.00,    2,    -1,    1,    1,    2,    Ortiz Jr Irad,    L,    120,    4.50,    Sister Sophia,    Barrier to Entry,    Vicki's Dancer,    LITE BMPS ST,2W,ASK1/8,    9,    </div></br> 
<div style="color:blue" > 09/04/2017 - 8,    SAR,    YL,    0,    F,    0,    (T)0,    SCR,    0,    95,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 07/15/2017 - 4,    BEL,    FM,    128,    F,    128,    (T)0,    AOC,    40000,    91,    97,    94,    4,    2,    2,    1,    160.00,    4,    3,    4,    1,    2,    Ortiz Jr Irad,    L,    120,    3.50,    Lakeside Sunset,    Barrier to Entry,    Startwithsilver,    3-4w uppr, rebuffed,    7,    </div></br> 
<div style="color:blue" > 06/11/2017 - 1,    BEL,    FM,    127,    F,    127,    (T)0,    CLM,    50000,    97,    118,    91,    4,    4,    2,    1,    50.00,    2,    1,    2,    1,    3,    Lezcano Jose,    L,    120,    1.95,    Five Hearts,    She's Stunning,    Barrier to Entry,    prompted 3p, repelled,    6,    </div></br> 
<div style="color:red" > 05/21/2017 - 7,    BEL,    FM,    139,    F,    139,    (T)18,    AOC,    40000,    88,    98,    87,    1,    1,    3,    3,    160.00,    4,    1,    2,    3,    5,    Velasquez Cornelio H,    L,    120,    4.80,    Epping Forest,    Startwithsilver,    Paz the Bourbon,    ins,keen,beh foe1/4-up,    8,    </div></br> 
<div style="color:blue" > 10/27/2016 - 5,    BEL,    GD,    78,    F,    78,    (T)0,    AOC,    40000,    95,    113,    102,    1,    5,    3,    2,    300.00,    3,    3,    2,    0,    2,    Rosario Joel,    L,    121,    13.70,    True Charm,    Barrier to Entry,    Uncle Southern,    2w 1/4p, just missed,    8,    </div></br> 
<div style="color:red" > 10/20/2016 - 1,    BEL,    FM,    124,    F,    124,    (T)18,    CLM,    25000,    85,    105,    90,    2,    4,    1,    -1,    -50.00,    1,    -1,    1,    0,    2,    Lezcano Jose,    L,    122,    0.85,    Reckless Humor,    Barrier to Entry,    Lady Coventry,    in hand 2p, nailed,    7,    </div></br> 
</td> </tr>
<tr  class="horse_2_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2016 ) 42   2   13   4   $228527.00    </br> 
  2017 : 7   0   2   2   $44610.00    </br> 
  2018 : 1   0   1   0   $11800.00    </br> 
  GP: 12   1   5   2   $115044.00    </br> 
  Turf: 27   2   8   2   $176398.00    </br> 
  Dist: 2   0   0   0   $774.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1" race="5" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> FROSTIE ANNE</td>
<td> Alvarado Junior</td>
<td> Rodriguez Rudy R</td>
<td> Mr. & Mrs. Grant L. Whitmer</td>
<td> Rodriguez, Rudy R. and Imperio, Michael</td>
<td> Frost Giant</td>
<td> Lake Toccet</td>
<td> 4/5</td></tr><tr class="horse_1_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 61 W 28% P 13% S 25% ITM 66% ROI 2</td></tr><tr  class="horse_1_5" style="display:none;"><td COLSPAN=9>
 Tr: St 48 W 21% P 19% S 10% ITM 50% ROI -13</td></tr><tr  class="horse_1_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/18/2018 - 3,    AQU,    GD,    82,    F,    82,    (T)-1,    STK,    0,    95,    98,    91,    2,    2,    1,    -2,    -200.00,    1,    2,    3,    3,    3,    Alvarado Junior,    L,    115,    2.85,    Frost Wise,    Riot Worthy,    Frostie Anne,    rail,vs duo 5/16-1/8pl,    4,    </div></br> 
<div style="color:red" > 12/14/2017 - 1,    AQU,    FT,    20,    F,    20,    (T)-1,    CLM,    25000,    88,    101,    94,    5,    5,    2,    0,    50.00,    2,    -2,    1,    -3,    1,    Lopez Paco,    L,    124,    1.10,    Frostie Anne,    Cats Halo,    Special Dividend,    4-3w upper, edged clr,    5,    </div></br> 
<div style="color:blue" > 11/08/2017 - 2,    AQU,    GD,    12,    F,    12,    (T)-1,    CLM,    25000,    85,    64,    84,    2,    1,    2,    1,    -100.00,    1,    2,    2,    -1,    1,    Ortiz Jr Irad,    L,    122,    2.55,    Frostie Anne,    Special Dividend,    Palladian Bridge,    5w 1/4, came again, up,    6,    </div></br> 
<div style="color:red" > 10/06/2017 - 2,    BEL,    FT,    48,    F,    48,    (T)-1,    AOC,    25000,    86,    79,    65,    4,    3,    1,    -3,    50.00,    2,    5,    2,    11,    5,    Diaz Jr Hector R,    L,    116,    1.90,    No Hayne No Gayne,    Three Eighty Eight,    Archumybaby,    in hand 2p, tired,    6,    </div></br> 
<div style="color:blue" > 09/10/2017 - 1,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    87,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 08/23/2017 - 4,    SAR,    FT,    34,    F,    34,    (T)-1,    CLM,    16000,    96,    66,    91,    4,    1,    1,    -2,    -200.00,    1,    -3,    1,    -3,    1,    Lopez Paco,    L,    124,    3.45,    Frostie Anne,    Arewehavingfunyet,    Pretty Enuff,    HARD TO LOAD,2W,4W UPR,    6,    </div></br> 
<div style="color:blue" > 07/16/2017 - 2,    BEL,    FM,    84,    F,    84,    (T)27,    CLM,    25000,    97,    60,    78,    5,    3,    5,    2,    410.00,    7,    10,    11,    8,    11,    Castellano Javier,    L,    122,    14.10,    Peru,    Rockin Alli,    Baronet,    in range 2w, gave way,    12,    </div></br> 
<div style="color:red" > 06/24/2017 - 5,    BEL,    FT,    67,    F,    67,    (T)-1,    ALW,    0,    85,    69,    80,    3,    1,    1,    -1,    -100.00,    1,    -1,    1,    -3,    1,    Arroyo Angel S,    L,    121,    3.00,    Frostie Anne,    Treatherlikestar,    Electrified,    in hand ins, edged clr,    5,    </div></br> 
<div style="color:blue" > 05/20/2017 - 9,    BEL,    FM,    128,    F,    128,    (T)9,    ALW,    0,    87,    58,    71,    2,    6,    5,    2,    330.00,    6,    5,    7,    5,    6,    Lopez Paco,    L,    121,    18.20,    Overnegotiate,    Discreet Image,    Puparee,    3-4w uppr, no response,    9,    </div></br> 
<div style="color:red" > 05/06/2017 - 5,    BEL,    MY,    0,    F,    0,    (T)0,    SCR,    0,    86,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:blue" > 04/19/2017 - 6,    AQU,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    82,    74,    59,    1,    2,    1,    -1,    100.00,    2,    6,    5,    15,    6,    Diaz Jr Hector R,    L,    115,    2.00,    Abounding Spirit,    Five Star Rampage,    Three Eighty Eight,    in hand 3-2w, faltered,    6,    </div></br> 
<div style="color:red" > 04/02/2017 - 1,    AQU,    GD,    40,    F,    40,    (T)-1,    CLM,    14000,    81,    61,    93,    3,    6,    1,    -2,    -350.00,    1,    -6,    1,    -9,    1,    Diaz Jr Hector R,    L,    113,    1.85,    Frostie Anne,    Morethanjusthello,    Megan's Muse,    2p,rouse 3/16,drew off,    6,    </div></br> 
</td> </tr>
<tr  class="horse_1_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 94  ( date: 2017 ) 24   7   2   2   $218508.00    </br> 
  2017 : 12   5   0   0   $134978.00    </br> 
  2018 : 1   0   0   1   $10000.00    </br> 
  GP: 1   0   0   1   $10000.00    </br> 
  Turf: 5   0   0   1   $8073.00    </br> 
  Dist: 3   1   0   0   $19200.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="5" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> PEACHES AND SPICE</td>
<td> Worrie Andre S</td>
<td> Quiles John N</td>
<td> Dormellito Stud</td>
<td> John N. Quiles</td>
<td> Simmard</td>
<td> Especially Me</td>
<td> 30/1</td></tr><tr class="horse_3_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 10 W 0% P 20% S 0% ITM 20% ROI -100</td></tr><tr  class="horse_3_5" style="display:none;"><td COLSPAN=9>
 Tr: St 1 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_3_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 8,    AQU,    GD,    50,    F,    50,    (T)-1,    CLM,    25000,    82,    45,    67,    4,    5,    5,    3,    400.00,    4,    8,    4,    9,    3,    Worrie Andre S,    L,    119,    30.50,    For Honor,    B Three,    Peaches and Spice,    3-4w upper, improved,    5,    </div></br> 
<div style="color:red" > 12/21/2017 - 3,    AQU,    FT,    47,    F,    47,    (T)-1,    AOC,    40000,    89,    60,    38,    5,    5,    5,    12,    1750.00,    5,    22,    5,    34,    5,    Garcia Martin,    L,    117,    38.75,    Land Mine,    Literata,    No Hayne No Gayne,    USED CHUTE,SLUGGISH,4W,    5,    </div></br> 
<div style="color:blue" > 11/30/2017 - 5,    AQU,    FT,    55,    F,    55,    (T)-1,    ALW,    0,    89,    31,    48,    4,    7,    7,    13,    1400.00,    6,    17,    5,    28,    5,    Diaz Jr Hector R,    L,    115,    70.25,    Frost Wise,    Might Be,    Unbridledadventure,    6w upper, no impact,    7,    </div></br> 
<div style="color:red" > 11/15/2017 - 1,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    89,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 08/09/2017 - 7,    SAR,    FT,    77,    F,    77,    (T)-1,    ALW,    0,    87,    0,    28,    5,    6,    6,    13,    1650.00,    6,    25,    6,    44,    6,    Diaz Jr Hector R,    L,    112,    32.50,    Verdant Pastures,    Cotton Candy Cutie,    Cool as You Like,    ins1st,3w2nd,outrun,    6,    </div></br> 
<div style="color:red" > 07/29/2017 - 7,    SAR,    FM,    139,    F,    139,    (T)12,    ALW,    0,    94,    38,    60,    8,    11,    10,    6,    530.00,    10,    8,    11,    12,    11,    Cancel Eric,    L,    115,    101.25,    Ack Naughty,    Mom's On Strike,    Affileo,    SLUGGISH BRK,INS TURNS,    11,    </div></br> 
<div style="color:blue" > 07/14/2017 - 8,    BEL,    FM,    54,    F,    54,    (T)0,    ALW,    0,    95,    53,    71,    6,    8,    10,    7,    810.00,    10,    9,    9,    8,    8,    Panaijo Jorge,    L,    118,    149.50,    Lido,    Initiate,    Tamit,    ins,drive 5/16,8w lane,    10,    </div></br> 
<div style="color:red" > 06/30/2017 - 6,    BEL,    FM,    58,    F,    58,    (T)0,    ALW,    0,    87,    67,    62,    10,    12,    12,    14,    1710.00,    12,    17,    12,    19,    11,    Montanez Rosario,    L,    114,    125.25,    Belle of the Spa,    Lem Me Dance,    Pinchbeck,    no factor,    12,    </div></br> 
<div style="color:blue" > 06/02/2017 - 2,    BEL,    FT,    42,    F,    42,    (T)-1,    ALW,    0,    82,    44,    64,    1,    5,    6,    13,    1350.00,    6,    17,    4,    21,    4,    Lezcano Jose,    L,    116,    31.75,    Sunset Ridge,    Shimmering Moon,    Three Eighty Eight,    2w 1/2,urged 4w into,    6,    </div></br> 
<div style="color:red" > 05/12/2017 - 5,    BEL,    FT,    63,    F,    63,    (T)-1,    ALW,    0,    81,    71,    43,    6,    6,    6,    4,    650.00,    6,    15,    5,    25,    5,    Lezcano Jose,    L,    115,    13.50,    Five Star Rampage,    Three Eighty Eight,    Zealous Scholar,    2p turn,5w upr,no bids,    6,    </div></br> 
<div style="color:blue" > 04/19/2017 - 6,    AQU,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    82,    40,    62,    2,    6,    6,    10,    1010.00,    6,    10,    6,    13,    5,    Alvarado Junior,    L,    116,    10.00,    Abounding Spirit,    Five Star Rampage,    Three Eighty Eight,    BUMPED ST, NO IMPACT,    6,    </div></br> 
<div style="color:red" > 03/31/2017 - 7,    AQU,    SY,    40,    F,    40,    (T)-1,    SOC,    50000,    76,    67,    68,    7,    6,    5,    4,    350.00,    5,    3,    3,    -0,    1,    Alvarado Junior,    L,    121,    7.30,    Peaches and Spice,    Curiousncuriouser,    Short Kakes,    overland turn,late due,    7,    </div></br> 
</td> </tr>
<tr  class="horse_3_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 71  ( date: 2017 ) 20   2   2   2   $96879.00    </br> 
  2017 : 14   2   2   1   $89481.00    </br> 
  2018 : 1   0   0   1   $4400.00    </br> 
  GP: 20   2   2   2   $96879.00    </br> 
  Turf: 6   0   0   0   $2895.00    </br> 
  Dist: 12   2   2   2   $92210.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="5" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> NO HAYNE NO GAYNE</td>
<td> Arroyo Angel S</td>
<td> Asmussen Steven M</td>
<td> ZBS Thoroughbreds</td>
<td> Sheets, Toby, ZBS Thoroughbreds and Paradise</td>
<td> Haynesfield</td>
<td> Star Orchid</td>
<td> 10/1</td></tr><tr class="horse_4_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 52 W 13% P 12% S 15% ITM 40% ROI -34</td></tr><tr  class="horse_4_5" style="display:none;"><td COLSPAN=9>
 Tr: St 178 W 21% P 26% S 12% ITM 59% ROI -31</td></tr><tr  class="horse_4_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 7,    AQU,    GD,    50,    F,    50,    (T)-1,    STK,    0,    94,    92,    84,    2,    6,    1,    -0,    100.00,    2,    4,    3,    8,    3,    Garcia Martin,    L,    117,    23.40,    Holiday Disguise,    Frost Wise,    No Hayne No Gayne,    3w,brief bid upr,tired,    6,    </div></br> 
<div style="color:red" > 01/25/2018 - 8,    AQU,    FT,    45,    F,    45,    (T)-1,    AOC,    40000,    94,    64,    91,    3,    7,    6,    7,    410.00,    5,    3,    3,    6,    3,    Cancel Eric,    L,    121,    18.90,    Startwithsilver,    Andesine,    No Hayne No Gayne,    BROKE OUT ST, KEPT ON,    7,    </div></br> 
<div style="color:blue" > 12/21/2017 - 3,    AQU,    FT,    47,    F,    47,    (T)-1,    AOC,    40000,    89,    97,    69,    3,    3,    2,    1,    200.00,    2,    8,    3,    15,    3,    Ortiz Jr Irad,    L,    121,    3.50,    Land Mine,    Literata,    No Hayne No Gayne,    3w vs duo to 1/2,tired,    5,    </div></br> 
<div style="color:red" > 10/21/2017 - 10,    BEL,    FT,    52,    F,    52,    (T)-1,    STK,    0,    95,    98,    51,    5,    4,    3,    3,    400.00,    3,    16,    9,    33,    9,    Lezcano Jose,    L,    116,    17.10,    Verdant Pastures,    Might Be,    Bonita Bianca,    3w on turn, gave way,    9,    </div></br> 
<div style="color:blue" > 10/06/2017 - 2,    BEL,    FT,    48,    F,    48,    (T)-1,    AOC,    25000,    86,    71,    84,    6,    1,    2,    3,    -50.00,    1,    -5,    1,    -6,    1,    Ortiz Jr Irad,    L,    117,    8.90,    No Hayne No Gayne,    Three Eighty Eight,    Archumybaby,    3w upper, drew clear,    6,    </div></br> 
<div style="color:red" > 05/12/2017 - 5,    BEL,    FT,    63,    F,    63,    (T)-1,    ALW,    0,    81,    83,    20,    1,    3,    2,    1,    400.00,    5,    22,    6,    40,    6,    Ortiz Jr Irad,    L,    118,    3.20,    Five Star Rampage,    Three Eighty Eight,    Zealous Scholar,    2w, through after 6f,    6,    </div></br> 
<div style="color:blue" > 04/02/2017 - 5,    AQU,    GD,    40,    F,    40,    (T)-1,    AOC,    75000,    87,    64,    54,    4,    2,    2,    0,    150.00,    3,    7,    4,    17,    5,    Carmouche Kendrick,    L,    117,    1.95,    Time Squared,    Three Eighty Eight,    His Girl Friday,    2w & btwn foes to 1/4,    6,    </div></br> 
<div style="color:red" > 02/19/2017 - 4,    AQU,    FT,    56,    F,    56,    (T)-1,    MSW,    0,    82,    86,    87,    6,    1,    3,    1,    100.00,    2,    1,    2,    -0,    1,    Carmouche Kendrick,    L,    120,    4.30,    No Hayne No Gayne,    Decorator Jenn,    Easy Way Out,    fought bit 1st,3w 2nd,    6,    </div></br> 
<div style="color:blue" > 01/27/2017 - 4,    AQU,    FT,    72,    F,    72,    (T)-1,    MSW,    0,    77,    110,    72,    9,    2,    2,    1,    200.00,    2,    4,    2,    8,    2,    Ortiz Jose L,    L,    120,    1.35,    Verdant Pastures,    No Hayne No Gayne,    Scarlett Jo Hansen,    rate 2p,stalked winner,    10,    </div></br> 
<div style="color:red" > 01/14/2017 - 2,    AQU,    FT,    24,    F,    24,    (T)-1,    MSW,    0,    80,    72,    72,    8,    7,    6,    5,    350.00,    4,    4,    4,    6,    4,    Velasquez Cornelio H,    L,    120,    5.20,    Bluegrass Flag,    Easy Way Out,    My Girl Annie,    chased 4w, wknd late,    9,    </div></br> 
<div style="color:blue" > 12/04/2016 - 6,    AQU,    FT,    40,    F,    40,    (T)-1,    MSW,    0,    82,    89,    74,    8,    6,    6,    3,    310.00,    4,    7,    3,    8,    3,    Ortiz Jose L,    L,    120,    2.45,    Royal Inheritance,    Dublin Girl,    No Hayne No Gayne,    3w upper, kept on,    10,    </div></br> 
</td> </tr>
<tr  class="horse_4_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 91  ( date: 2018 ) 11   2   1   4   $115490.00    </br> 
  2017 : 8   2   1   1   $94090.00    </br> 
  2018 : 2   0   0   2   $15900.00    </br> 
  GP: 11   2   1   4   $115490.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 8   2   1   2   $101340.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="5" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> ANDESINE</td>
<td> Carmouche Kendrick</td>
<td> Englehart Chris J</td>
<td> WinStar Farm, LLC</td>
<td> Island Wind Racing</td>
<td> Bluegrass Cat</td>
<td> Holy Moment</td>
<td> 4/1</td></tr><tr class="horse_5_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_5_5" style="display:none;"><td COLSPAN=9>
 Tr: St 30 W 10% P 17% S 23% ITM 50% ROI -59</td></tr><tr  class="horse_5_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/16/2018 - 7,    AQU,    SY,    28,    F,    28,    (T)-1,    AOC,    40000,    95,    89,    72,    2,    5,    6,    3,    320.00,    6,    7,    3,    12,    3,    Carmouche Kendrick,    L,    119,    2.35,    Palladian Bridge,    Barrier to Entry,    Andesine,    7w upper, improved,    7,    </div></br> 
<div style="color:red" > 01/25/2018 - 8,    AQU,    FT,    45,    F,    45,    (T)-1,    AOC,    40000,    94,    94,    98,    7,    6,    4,    2,    260.00,    4,    1,    2,    3,    2,    Carmouche Kendrick,    L,    119,    5.30,    Startwithsilver,    Andesine,    No Hayne No Gayne,    2w 1/4,led, overhauled,    7,    </div></br> 
<div style="color:blue" > 01/14/2018 - 1,    AQU,    FT,    48,    F,    48,    (T)-1,    CLM,    35000,    86,    49,    89,    6,    2,    2,    1,    -150.00,    1,    -7,    1,    -9,    1,    Mccarthy Trevor,    L,    119,    1.95,    Andesine,    Loose,    Road to Perfection,    4-3w upper, drew clear,    6,    </div></br> 
<div style="color:red" > 12/10/2017 - 2,    AQU,    GD,    55,    F,    55,    (T)-1,    CLM,    35000,    87,    75,    74,    2,    5,    5,    6,    150.00,    2,    2,    2,    6,    2,    Ortiz Jr Irad,    L,    120,    1.40,    Christmas Sky,    Andesine,    Winner's Dream,    4w upper, 2nd best,    6,    </div></br> 
<div style="color:blue" > 11/23/2017 - 6,    AQU,    FT,    46,    F,    46,    (T)-1,    AOC,    40000,    97,    62,    87,    3,    5,    5,    3,    250.00,    4,    2,    4,    3,    3,    Ortiz Jose L,    L,    122,    10.20,    Shimmering Moon,    Court Dancer,    Andesine,    chased 5-4w, kept on,    6,    </div></br> 
<div style="color:red" > 11/15/2017 - 1,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    89,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 10/27/2017 - 6,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    95,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 09/27/2017 - 3,    BEL,    FT,    45,    F,    45,    (T)-1,    CLM,    35000,    86,    46,    86,    5,    1,    5,    6,    260.00,    5,    4,    3,    4,    2,    Ortiz Jr Irad,    L,    120,    3.85,    Isotope,    Andesine,    Decorator Jenn,    ins to 5/16,fan 6p 1/8,    6,    </div></br> 
<div style="color:blue" > 09/15/2017 - 8,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    92,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 09/08/2017 - 7,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    94,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 07/28/2017 - 6,    SAR,    FM,    128,    F,    128,    (T)12,    CLM,    35000,    95,    103,    79,    6,    4,    4,    2,    300.00,    5,    6,    8,    9,    11,    Franco Manuel,    L,    120,    15.50,    Flattermefabulous,    Magician's Vanity,    Rimanisempreforte,    LITE BMP ST,2W,ASK 1/4,    12,    </div></br> 
<div style="color:red" > 06/16/2017 - 4,    BEL,    FM,    117,    F,    117,    (T)9,    AOC,    40000,    91,    39,    74,    2,    6,    7,    5,    460.00,    7,    6,    7,    7,    7,    Velazquez John R,    L,    122,    22.60,    Overnegotiate,    Little Bear Cat,    Eloweasel,    saved grd, no headway,    9,    </div></br> 
</td> </tr>
<tr  class="horse_5_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 98  ( date: 2018 ) 15   3   5   2   $166054.00    </br> 
  2017 : 9   1   3   1   $71904.00    </br> 
  2018 : 3   1   1   1   $42900.00    </br> 
  GP: 2   0   1   1   $17700.00    </br> 
  Turf: 2   0   0   0   $464.00    </br> 
  Dist: 6   1   3   0   $60440.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="5" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> SILLY SISTER</td>
<td> Diaz Jr Hector Rafael</td>
<td> Rice Linda</td>
<td> Empire Equines, LLC</td>
<td> Lady Sheila Stable and Edition Farm</td>
<td> Broken Vow</td>
<td> Giggly</td>
<td> 7/2</td></tr><tr class="horse_6_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 41 W 5% P 17% S 12% ITM 34% ROI -81</td></tr><tr  class="horse_6_5" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_6_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/16/2018 - 7,    AQU,    SY,    28,    F,    28,    (T)-1,    AOC,    40000,    95,    97,    69,    7,    4,    3,    2,    270.00,    5,    8,    4,    13,    4,    Alvarado Junior,    L,    119,    3.00,    Palladian Bridge,    Barrier to Entry,    Andesine,    6w upper, weakened,    7,    </div></br> 
<div style="color:red" > 08/31/2017 - 4,    SAR,    FT,    70,    F,    70,    (T)-1,    AOC,    40000,    95,    70,    86,    4,    4,    7,    7,    710.00,    6,    4,    3,    2,    2,    Ortiz Jr Irad,    L,    120,    3.90,    Cozzy Spring,    Silly Sister,    Shimmering Moon,    INS,5P1/8,SHIED WHIP,    7,    </div></br> 
<div style="color:blue" > 08/09/2017 - 2,    SAR,    FT,    36,    F,    36,    (T)-1,    AOC,    40000,    94,    95,    58,    1,    3,    1,    -1,    100.00,    2,    9,    3,    20,    3,    Ortiz Jr Irad,    L,    120,    3.85,    Sunset Ridge,    Moondance Joy,    Silly Sister,    LITE BMP ST,INS,TIRED,    5,    </div></br> 
<div style="color:red" > 08/06/2017 - 5,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    92,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 06/23/2017 - 6,    BEL,    FT,    54,    F,    54,    (T)-1,    ALW,    0,    88,    76,    93,    3,    4,    4,    3,    200.00,    3,    -1,    1,    -3,    1,    Ortiz Jr Irad,    L,    119,    4.90,    Silly Sister,    Mizzen Max,    Zealous Scholar,    4w upper, edged away,    7,    </div></br> 
<div style="color:red" > 05/19/2017 - 4,    BEL,    FT,    67,    F,    67,    (T)-1,    MSW,    0,    84,    94,    65,    3,    5,    2,    1,    50.00,    2,    1,    2,    -1,    1,    Franco Manuel,    L,    117,    2.60,    Silly Sister,    Frost Wise,    Splashy Rise,    BOBBLED ST, GAMELY,    5,    </div></br> 
<div style="color:blue" > 04/28/2017 - 9,    BEL,    GD,    116,    F,    116,    (T)18,    MSW,    0,    83,    8,    55,    10,    12,    12,    13,    900.00,    12,    9,    10,    12,    9,    Ortiz Jose L,    L,    117,    6.10,    Dream a Little,    Milky Whey Gal,    Spring Folly,    SQUEEZE,CHECK BRK,2P,    12,    </div></br> 
</td> </tr>
<tr  class="horse_6_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 93  ( date: 2017 ) 6   2   1   1   $102128.00    </br> 
  2017 : 5   2   1   1   $99178.00    </br> 
  2018 : 1   0   0   0   $2950.00    </br> 
  GP: 6   2   1   1   $102128.00    </br> 
  Turf: 1   0   0   0   $178.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="5" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> LITERATA</td>
<td> Franco Manuel</td>
<td> Asmussen Steven M</td>
<td> Meadow View Thoroughbreds, LLC.</td>
<td> Paradise Farms Corp.</td>
<td> Read the Footnotes</td>
<td> Personal Wealth</td>
<td> 12/1</td></tr><tr class="horse_7_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_7_5" style="display:none;"><td COLSPAN=9>
 Tr: St 178 W 21% P 26% S 12% ITM 59% ROI -31</td></tr><tr  class="horse_7_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 7,    AQU,    GD,    50,    F,    50,    (T)-1,    STK,    0,    94,    76,    74,    6,    4,    5,    5,    450.00,    4,    8,    4,    14,    4,    Franco Manuel,    L,    121,    8.20,    Holiday Disguise,    Frost Wise,    No Hayne No Gayne,    2w turn, 5w down lane,    6,    </div></br> 
<div style="color:red" > 12/21/2017 - 3,    AQU,    FT,    47,    F,    47,    (T)-1,    AOC,    40000,    89,    85,    75,    4,    2,    4,    4,    650.00,    4,    7,    2,    11,    2,    Arroyo Angel S,    L,    124,    2.65,    Land Mine,    Literata,    No Hayne No Gayne,    3w,ask turn,no match,    5,    </div></br> 
<div style="color:blue" > 11/09/2017 - 8,    AQU,    FT,    56,    F,    56,    (T)-1,    AOC,    62500,    95,    63,    80,    5,    7,    8,    8,    810.00,    8,    11,    6,    14,    6,    Franco Manuel,    L,    124,    27.00,    Jamyson 'n Ginger,    Divine Miss Grey,    Parade,    3w1/2,ask 5/16,5p3/16,    9,    </div></br> 
<div style="color:red" > 10/21/2017 - 10,    BEL,    FT,    52,    F,    52,    (T)-1,    STK,    0,    95,    96,    72,    8,    5,    5,    4,    550.00,    5,    12,    5,    20,    7,    Castellano Javier,    L,    121,    5.60,    Verdant Pastures,    Might Be,    Bonita Bianca,    2w,driven into lane,    9,    </div></br> 
<div style="color:blue" > 09/28/2017 - 8,    BEL,    FT,    42,    F,    42,    (T)-1,    AOC,    62500,    97,    80,    77,    6,    4,    2,    1,    300.00,    2,    6,    3,    7,    3,    Franco Manuel,    L,    124,    11.80,    Tejana,    Going for Broke,    Literata,    3w,drive 5/16,no match,    7,    </div></br> 
<div style="color:red" > 09/24/2017 - 8,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    106,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 09/02/2017 - 8,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    105,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 08/25/2017 - 7,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    109,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 08/14/2017 - 8,    SAR,    FT,    75,    F,    75,    (T)-1,    STK,    0,    93,    73,    89,    4,    2,    2,    1,    -150.00,    1,    -0,    1,    -1,    1,    Ortiz Jr Irad,    L,    120,    3.15,    Literata,    Jcs American Dream,    Jc's Shooting Star,    2-3p trns,ask1/4,game,    6,    </div></br> 
<div style="color:red" > 07/22/2017 - 2,    FL,    FT,    54,    F,    54,    (T)-1,    ALW,    0,    82,    67,    89,    2,    4,    4,    2,    210.00,    4,    -1,    1,    -4,    1,    Navarro Joshua,    L,    120,    0.95,    Literata,    Pachamama,    Five Star Rampage,    rail, drew off,    5,    </div></br> 
<div style="color:blue" > 05/29/2017 - 6,    BEL,    SY,    44,    F,    44,    (T)-1,    STK,    0,    99,    93,    81,    4,    10,    9,    5,    450.00,    2,    8,    2,    8,    2,    Castellano Javier,    L,    116,    24.75,    Kathryn the Wise,    Literata,    Bee Noteworthy,    chased 5-6w, 2nd best,    11,    </div></br> 
<div style="color:red" > 05/21/2017 - 2,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
</td> </tr>
<tr  class="horse_7_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 89  ( date: 2017 ) 38   8   10   5   $502293.00    </br> 
  2017 : 8   2   2   1   $136455.00    </br> 
  2018 : 1   0   0   0   $5000.00    </br> 
  GP: 1   0   0   0   $5000.00    </br> 
  Turf: 2   0   0   0   $448.00    </br> 
  Dist: 26   4   8   4   $342255.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1A" race="5" title="Show More..." > 1A</br> <span class="past-performance-plus-b" > </span> </td>
<td> COTTON CANDY CUTIE</td>
<td> McCarthy Trevor</td>
<td> Rodriguez Rudy R</td>
<td> Twin Creeks Farm</td>
<td> Dubb, Michael and Imperio, Michael</td>
<td> Sidney's Candy</td>
<td> Sassy Redhead</td>
<td> 4/5</td></tr><tr class="horse_1A_5" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_1A_5" style="display:none;"><td COLSPAN=9>
 Tr: St 48 W 21% P 19% S 10% ITM 50% ROI -13</td></tr><tr  class="horse_1A_5" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/27/2018 - 8,    AQU,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    87,    52,    82,    1,    1,    5,    2,    160.00,    3,    -1,    1,    -6,    1,    Mccarthy Trevor,    L,    123,    0.45,    Cotton Candy Cutie,    Playinwiththeboys,    Beyond Discreet,    4w upper, drew clear,    9,    </div></br> 
<div style="color:red" > 12/03/2017 - 7,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    89,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 11/08/2017 - 5,    AQU,    GD,    58,    F,    58,    (T)-1,    ALW,    0,    85,    37,    82,    5,    1,    2,    1,    -50.00,    1,    1,    2,    3,    2,    Ortiz Jr Irad,    L,    124,    2.85,    Land Mine,    Cotton Candy Cutie,    Frost Wise,    prompted 3w, held well,    6,    </div></br> 
<div style="color:red" > 10/18/2017 - 7,    BEL,    FM,    74,    F,    74,    (T)18,    ALW,    0,    88,    89,    76,    6,    1,    4,    3,    250.00,    4,    4,    6,    9,    8,    Franco Manuel,    L,    124,    1.25,    La Moneda,    Frosty Lady,    Pinchbeck,    3w 2d,5p3/16,empty,    9,    </div></br> 
<div style="color:blue" > 09/15/2017 - 7,    FL,    FT,    52,    F,    52,    (T)-1,    ALW,    0,    84,    81,    76,    4,    4,    3,    1,    -50.00,    1,    -2,    1,    -2,    1,    Navarro Joshua,    L,    120,    0.30,    Cotton Candy Cutie,    Lady Marlena,    Miss Rickles,    led 2nd trn,drew off,,    5,    </div></br> 
<div style="color:red" > 09/01/2017 - 9,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    90,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 08/09/2017 - 7,    SAR,    FT,    77,    F,    77,    (T)-1,    ALW,    0,    87,    31,    71,    2,    2,    2,    1,    100.00,    2,    7,    2,    15,    2,    Ortiz Jr Irad,    L,    124,    5.70,    Verdant Pastures,    Cotton Candy Cutie,    Cool as You Like,    STDIED 1M,2-3P TURNS,    6,    </div></br> 
<div style="color:red" > 07/05/2017 - 7,    FL,    FT,    54,    F,    54,    (T)-1,    ALW,    0,    85,    58,    85,    1,    2,    2,    1,    -50.00,    1,    -2,    1,    1,    2,    Davila Jr John R,    L,    122,    0.60,    Satin Frost,    Cotton Candy Cutie,    Tinasutopianvision,    2p5/16, willingly,    5,    </div></br> 
<div style="color:blue" > 06/06/2017 - 7,    FL,    FT,    54,    F,    54,    (T)-1,    ALW,    0,    80,    76,    87,    2,    3,    1,    -1,    -400.00,    1,    -10,    1,    -3,    1,    Davila Jr John R,    L,    120,    0.40,    Cotton Candy Cutie,    Satin Frost,    Crazy Love,    rail, ridden out,    5,    </div></br> 
<div style="color:red" > 05/07/2017 - 7,    BEL,    FT,    65,    F,    65,    (T)-1,    ALW,    0,    87,    79,    54,    5,    4,    4,    4,    550.00,    5,    12,    7,    20,    6,    Ortiz Jr Irad,    L,    124,    5.10,    Holiday Disguise,    Royal Inheritance,    Decorator Jenn,    hite gate,2p turn,5w,    8,    </div></br> 
<div style="color:blue" > 04/21/2017 - 1,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    89,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 03/03/2017 - 6,    AQU,    FT,    76,    F,    76,    (T)-1,    STR,    50000,    87,    98,    91,    2,    2,    1,    -1,    -100.00,    1,    -1,    1,    1,    2,    Ortiz Jr Irad,    L,    118,    1.35,    Time Squared,    Cotton Candy Cutie,    Say Cin Cin,    2w 1/4p, bested,    8,    </div></br> 
</td> </tr>
<tr  class="horse_1A_5" style="display:none;"><td COLSPAN=9>
LifeTime Record 91  ( date: 2017 ) 21   5   6   2   $199032.00    </br> 
  2017 : 9   3   4   0   $82057.00    </br> 
  2018 : 1   1   0   0   $34200.00    </br> 
  GP: 9   3   4   0   $100057.00    </br> 
  Turf: 7   0   2   1   $33119.00    </br> 
  Dist: 7   4   3   0   $123920.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="6" race_date="20180317" claimamt="0" dist_disp="6F" todays_cls="90" purse="55000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #6</span> - 
<span> Claiming $0</span> - 
<span> Distance 6F</span> - 
<span> Todays CLS 90</span> -
<span> Purse $55000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Pick 3 Races (6-8) Pick 4 (.50) Races (6-9), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="6" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> RETONOVA</td>
<td> McCarthy Trevor</td>
<td> Englehart Chris J</td>
<td> Barry R. Ostrager</td>
<td> My Purple Haze Stables</td>
<td> Boys At Tosconova</td>
<td> Jb's Golden Regret</td>
<td> 9/2</td></tr><tr class="horse_1_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_1_6" style="display:none;"><td COLSPAN=9>
 Tr: St 30 W 10% P 17% S 23% ITM 50% ROI -59</td></tr><tr  class="horse_1_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="6" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> READY TO ESCAPE</td>
<td> Franco Manuel</td>
<td> Baker Charlton</td>
<td> Albert Fried, Jr.</td>
<td> Albert Fried, Jr.</td>
<td> More Than Ready</td>
<td> Towering Escape</td>
<td> 7/2</td></tr><tr class="horse_2_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_2_6" style="display:none;"><td COLSPAN=9>
 Tr: St 18 W 11% P 17% S 11% ITM 39% ROI -10</td></tr><tr  class="horse_2_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="6" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> PHANTOMOFTHEFROST</td>
<td> Carmouche Kendrick</td>
<td> Morley Thomas</td>
<td> Sunrise Stables LLC</td>
<td> Sunrise Stables</td>
<td> Frost Giant</td>
<td> Athene</td>
<td> 3/1</td></tr><tr class="horse_3_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_3_6" style="display:none;"><td COLSPAN=9>
 Tr: St 13 W 23% P 8% S 8% ITM 39% ROI -7</td></tr><tr  class="horse_3_6" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/09/2018 - 4,    AQU,    GD,    27,    F,    27,    (T)-1,    MSW,    0,    85,    94,    65,    4,    2,    2,    0,    50.00,    2,    3,    3,    8,    3,    Arroyo Angel S,    L,    120,    2.50,    City Never Sleeps,    It's Hot Out,    Phantomofthefrost,    2-3w,btwn foes 1/4pl,    6,    </div></br> 
</td> </tr>
<tr  class="horse_3_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 65  ( date: 2018 ) 1   0   0   1   $5500.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  2018 : 1   0   0   1   $5500.00    </br> 
  GP: 1   0   0   1   $5500.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 1   0   0   1   $5500.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="6" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> P J ADVANTAGE</td>
<td> Rose Jeremy</td>
<td> Barrow Paul W</td>
<td> Andiamo Farm</td>
<td> Barrow, Paul, W.</td>
<td> Posse</td>
<td> Weefc</td>
<td> 15/1</td></tr><tr class="horse_4_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 21 W 0% P 19% S 14% ITM 33% ROI -100</td></tr><tr  class="horse_4_6" style="display:none;"><td COLSPAN=9>
 Tr: St 4 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_4_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="6" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> PANIC ATTACK</td>
<td> Fragoso Pablo</td>
<td> Sciacca Gary</td>
<td> SF Racing, LLC</td>
<td> Laderer, Amanda and Morongello, Paul</td>
<td> Gemologist</td>
<td> Plethora</td>
<td> 10/1</td></tr><tr class="horse_5_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 3 W 33% P 0% S 0% ITM 33% ROI 20</td></tr><tr  class="horse_5_6" style="display:none;"><td COLSPAN=9>
 Tr: St 24 W 8% P 4% S 17% ITM 29% ROI 135</td></tr><tr  class="horse_5_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="6" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> GUY AMERICAN DREAM</td>
<td> Simpson Trevor</td>
<td> Persaud Randi</td>
<td> First Class Thoroughbreds LLC & Jam</td>
<td> Guyana Rocky LLC</td>
<td> Freud</td>
<td> Taffy Pull</td>
<td> 20/1</td></tr><tr class="horse_6_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 9 W 11% P 11% S 11% ITM 33% ROI 41</td></tr><tr  class="horse_6_6" style="display:none;"><td COLSPAN=9>
 Tr: St 10 W 0% P 10% S 10% ITM 20% ROI -100</td></tr><tr  class="horse_6_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="6" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> IRISH CREED</td>
<td> Davis Dylan</td>
<td> Pletcher Todd A</td>
<td> Chevaux Racing</td>
<td> Chevaux Racing and Nine Fifteen Stables</td>
<td> Jimmy Creed</td>
<td> Arguably</td>
<td> 5/2</td></tr><tr class="horse_7_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_7_6" style="display:none;"><td COLSPAN=9>
 Tr: St 85 W 28% P 20% S 20% ITM 68% ROI -24</td></tr><tr  class="horse_7_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 0  ( date:  ) 0   0   0   0   $0.00    </br> 
  -1 : 0   0   0   0   $0.00    </br> 
  0 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="8" race="6" title="Show More..." > 8</br> <span class="past-performance-plus-b" > </span> </td>
<td> RAGING FIRE</td>
<td> Alvarado Junior</td>
<td> Kimmel John C</td>
<td> Gentry Stable</td>
<td> Morton, Tobey, L.</td>
<td> Tapizar</td>
<td> Lemon Splendor</td>
<td> 8/1</td></tr><tr class="horse_8_6" style="display:none;"><td COLSPAN=9>
  Jocks: St 61 W 28% P 13% S 25% ITM 66% ROI 2</td></tr><tr  class="horse_8_6" style="display:none;"><td COLSPAN=9>
 Tr: St 15 W 7% P 20% S 20% ITM 47% ROI -25</td></tr><tr  class="horse_8_6" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 08/07/2017 - 6,    SAR,    FT,    44,    F,    44,    (T)-1,    MSW,    0,    87,    58,    45,    6,    7,    8,    6,    1000.00,    8,    15,    8,    22,    8,    Arroyo Angel S,    L,    119,    10.70,    Barely Impazible,    Ultimateenticement,    Collective Effort,    2w turn,4p1/8,no bids,    10,    </div></br> 
<div style="color:red" > 07/14/2017 - 7,    BEL,    GD,    68,    F,    68,    (T)-1,    MSW,    0,    88,    80,    66,    9,    4,    3,    3,    500.00,    3,    6,    4,    7,    3,    Arroyo Angel S,    L,    119,    14.00,    Inalienable Rights,    Howard Beach,    Raging Fire,    3w pursuit, wknd late,    10,    </div></br> 
</td> </tr>
<tr  class="horse_8_6" style="display:none;"><td COLSPAN=9>
LifeTime Record 66  ( date: 2017 ) 2   0   0   1   $6492.00    </br> 
  2016 : 2   0   0   1   $6492.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 2   0   0   1   $6492.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 2   0   0   1   $6492.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="7" race_date="20180317" claimamt="0" dist_disp="1M" todays_cls="103" purse="67000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #7</span> - 
<span> Claiming $0</span> - 
<span> Distance 1M</span> - 
<span> Todays CLS 103</span> -
<span> Purse $67000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Pick 3 Races (7-9), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="2" race="7" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> CUMBERLAND RIVER</td>
<td> McCarthy Trevor</td>
<td> McLaughlin Kiaran P</td>
<td> Darley</td>
<td> Godolphin Racing, LLC</td>
<td> Distorted Humor</td>
<td> Mari's Sheba</td>
<td> 15/1</td></tr><tr class="horse_2_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_2_7" style="display:none;"><td COLSPAN=9>
 Tr: St 26 W 15% P 19% S 8% ITM 42% ROI -60</td></tr><tr  class="horse_2_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/20/2018 - 7,    AQU,    FT,    48,    F,    48,    (T)-1,    ALW,    0,    103,    91,    60,    5,    9,    3,    1,    400.00,    4,    11,    6,    15,    9,    Garcia Martin,    L,    121,    3.00,    Spieth,    Mr. Buff,    Hammerin Aamer,    3w turn, tired,    9,    </div></br> 
<div style="color:red" > 03/17/2017 - 3,    AQU,    GD,    55,    F,    55,    (T)-1,    MSW,    0,    91,    97,    97,    1,    4,    1,    -2,    -150.00,    1,    -3,    1,    -2,    1,    Ortiz Jr Irad,    L,    121,    1.30,    Cumberland River,    Seat of Honor,    Cookie Crisp,    inside,rated to 3/16pl,    6,    </div></br> 
<div style="color:blue" > 02/16/2017 - 4,    AQU,    FT,    56,    F,    56,    (T)-1,    MSW,    0,    94,    75,    87,    1,    6,    4,    1,    210.00,    4,    7,    4,    9,    3,    Ortiz Jr Irad,    L,    121,    2.80,    Virtual Machine,    Strong Side,    Cumberland River,    CHECKED 7 1/2, RAN ON,    8,    </div></br> 
<div style="color:red" > 12/29/2016 - 7,    AQU,    MY,    76,    F,    76,    (T)-1,    MSW,    0,    95,    97,    50,    1,    6,    5,    5,    410.00,    5,    9,    5,    21,    5,    Ortiz Jr Irad,    L,    121,    1.35,    Admiral Blue,    Strong Side,    Spring On Curlin,    3w 3/8, no response,    7,    </div></br> 
<div style="color:blue" > 12/08/2016 - 3,    GP,    FT,    5,    F,    5,    (T)-1,    MSW,    0,    88,    93,    95,    4,    2,    3,    2,    150.00,    3,    1,    2,    0,    2,    Rosario Joel,    L,    122,    3.10,    Fast Friar,    Cumberland River,    Illustrious Son,    trackd,duel,just missd,    6,    </div></br> 
<div style="color:red" > 02/06/2016 - 11,    GP,    FT,    80,    F,    80,    (T)-1,    MSW,    0,    100,    97,    68,    1,    2,    9,    4,    420.00,    9,    9,    9,    15,    8,    Ortiz Jr Irad,    L,    120,    5.20,    Kelly Tough,    Happy's Causeway,    Majesto,    3w2ndturn, no rally,    11,    </div></br> 
<div style="color:blue" > 10/29/2015 - 6,    BEL,    GD,    44,    F,    44,    (T)-1,    MSW,    0,    105,    73,    85,    4,    6,    3,    2,    350.00,    3,    6,    3,    10,    6,    Desousa Silvestre A,    L,    120,    21.80,    Matt King Coal,    Portfolio Manager,    Shoot From the Hip,    tracked 2p, weakened,    12,    </div></br> 
<div style="color:red" > 10/03/2015 - 11,    BEL,    SY,    50,    F,    50,    (T)-1,    MSW,    0,    87,    95,    74,    5,    7,    4,    4,    450.00,    4,    9,    3,    18,    3,    Bravo Joe,    120,    3.75,    Gift Box,    Matt King Coal,    Cumberland River,    chased 3-4w, tired,    8,    </div></br> 
<div style="color:blue" > 08/30/2015 - 7,    SAR,    FM,    120,    F,    120,    (T)0,    MSW,    0,    87,    74,    74,    7,    7,    8,    7,    600.00,    7,    10,    8,    11,    5,    Lezcano Jose,    119,    5.30,    Eidmilaad,    Its All Relevant,    Dissident,    yanked to 2p,3w 2nd,    10,    </div></br> 
</td> </tr>
<tr  class="horse_2_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 97  ( date: 2017 ) 9   1   1   2   $63660.00    </br> 
  2017 : 2   1   0   1   $42000.00    </br> 
  2018 : 1   0   0   0   $335.00    </br> 
  GP: 9   1   1   2   $63660.00    </br> 
  Turf: 1   0   0   0   $2490.00    </br> 
  Dist: 7   1   1   2   $60770.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1" race="7" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> NOBODY MOVE</td>
<td> Rose Jeremy</td>
<td> Rodriguez Rudy R</td>
<td> Paul Pompa Jr.</td>
<td> Michael Dubb</td>
<td> D' Funnybone</td>
<td> Proud Lisa</td>
<td> 6/1</td></tr><tr class="horse_1_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 21 W 0% P 19% S 14% ITM 33% ROI -100</td></tr><tr  class="horse_1_7" style="display:none;"><td COLSPAN=9>
 Tr: St 48 W 21% P 19% S 10% ITM 50% ROI -13</td></tr><tr  class="horse_1_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/28/2018 - 7,    AQU,    SY,    50,    F,    50,    (T)-1,    AOC,    40000,    100,    58,    94,    1,    1,    3,    2,    300.00,    3,    5,    2,    5,    2,    Carmouche Kendrick,    L,    119,    6.30,    Uncle Sigh,    Nobody Move,    Broken Engagement,    tracked 2-3w, mild bid,    6,    </div></br> 
<div style="color:red" > 11/29/2017 - 8,    AQU,    FT,    48,    F,    48,    (T)-1,    STK,    0,    98,    57,    71,    4,    3,    6,    5,    610.00,    6,    13,    7,    13,    6,    Lebron Victor,    L,    118,    20.00,    Gold for the King,    Sudden Surprise,    Fleet Irish,    5w upper, tired,    7,    </div></br> 
<div style="color:blue" > 11/11/2017 - 3,    AQU,    FT,    48,    F,    48,    (T)-1,    AOC,    40000,    101,    56,    86,    7,    5,    9,    8,    600.00,    7,    5,    5,    -0,    1,    Lebron Victor,    L,    122,    9.60,    Nobody Move,    Saratoga Giro,    Giantinthemoonlite,    7w upper, just up,    9,    </div></br> 
<div style="color:red" > 11/05/2017 - 7,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    103,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 10/15/2017 - 8,    BEL,    FT,    57,    F,    57,    (T)-1,    AOC,    40000,    101,    68,    96,    2,    3,    4,    3,    150.00,    2,    3,    3,    3,    3,    Lebron Victor,    L,    120,    5.60,    Mr. Buff,    Giantinthemoonlite,    Nobody Move,    2w & btwn to 1/4,3w,    7,    </div></br> 
<div style="color:red" > 08/25/2017 - 2,    SAR,    FT,    32,    F,    32,    (T)-1,    AOC,    40000,    104,    78,    91,    4,    6,    6,    4,    400.00,    6,    4,    5,    6,    6,    Castellano Javier,    L,    122,    10.40,    Build to Suit,    Samadi Sky,    Eye Luv Lulu,    2w,chase fight,empty,    9,    </div></br> 
<div style="color:blue" > 08/05/2017 - 1,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    104,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 07/30/2017 - 8,    SAR,    FT,    40,    F,    40,    (T)-1,    AOC,    40000,    105,    51,    99,    2,    7,    7,    8,    450.00,    4,    4,    3,    4,    3,    Ortiz Jr Irad,    L,    122,    4.00,    Rectify,    Runaway Lute,    Nobody Move,    inside, rallied show,    8,    </div></br> 
<div style="color:blue" > 07/14/2017 - 6,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    100,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 07/02/2017 - 6,    BEL,    FT,    48,    F,    48,    (T)-1,    AOC,    40000,    107,    77,    104,    5,    6,    5,    5,    560.00,    5,    6,    2,    3,    2,    Rosario Joel,    L,    122,    4.20,    Bustin It,    Nobody Move,    Spooked Out,    LITE BMP ST,3-4W,6W1/8,    6,    </div></br> 
<div style="color:blue" > 05/26/2017 - 6,    BEL,    MY,    72,    F,    72,    (T)-1,    AOC,    40000,    106,    58,    99,    3,    4,    5,    4,    400.00,    4,    5,    2,    5,    2,    Velasquez Cornelio H,    L,    122,    3.00,    Pop the Hood,    Nobody Move,    West Hills Giant,    out-hustled,2p,ins1/4,    5,    </div></br> 
<div style="color:red" > 05/13/2017 - 9,    BEL,    SY,    68,    F,    68,    (T)-1,    AOC,    40000,    107,    94,    101,    6,    8,    8,    5,    410.00,    7,    2,    5,    -0,    1,    Velasquez Cornelio H,    L,    120,    4.20,    Nobody Move,    Bond Vigilante,    Riff Raff,    4w uppr, up final 70yd,    9,    </div></br> 
</td> </tr>
<tr  class="horse_1_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 104  ( date: 2017 ) 27   4   6   8   $266449.00    </br> 
  2017 : 10   2   2   2   $119075.00    </br> 
  2018 : 1   0   1   0   $11800.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 1   0   0   0   $414.00    </br> 
  Dist: 8   0   3   5   $68800.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="7" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> BACKYARD HEAVEN</td>
<td> Franco Manuel</td>
<td> Brown Chad C</td>
<td> Waymore LLC</td>
<td> Ramsey, Kenneth L. and Sarah K.</td>
<td> Tizway</td>
<td> Cappagh Strand</td>
<td> 6/5</td></tr><tr class="horse_3_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_3_7" style="display:none;"><td COLSPAN=9>
 Tr: St 37 W 22% P 16% S 14% ITM 52% ROI -19</td></tr><tr  class="horse_3_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 12/10/2017 - 4,    AQU,    GD,    55,    F,    55,    (T)-1,    MSW,    0,    97,    96,    108,    4,    3,    5,    2,    150.00,    3,    -1,    1,    -5,    1,    Ortiz Jr Irad,    L,    121,    0.20,    Backyard Heaven,    Ekhtibaar,    Unforeseeable,    6w upper, edged clear,    7,    </div></br> 
<div style="color:red" > 09/10/2017 - 6,    BEL,    FT,    48,    F,    48,    (T)-1,    MSW,    0,    91,    74,    88,    3,    2,    5,    3,    350.00,    3,    5,    3,    4,    2,    Rosario Joel,    L,    120,    8.10,    Copper Town,    Backyard Heaven,    Stan the Man,    4w upper, mild kick,    7,    </div></br> 
</td> </tr>
<tr  class="horse_3_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 108  ( date: 2017 ) 2   1   1   0   $51000.00    </br> 
  2016 : 2   1   1   0   $51000.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 2   1   1   0   $51000.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 2   1   1   0   $51000.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="7" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> EASTPORT</td>
<td> Carmouche Kendrick</td>
<td> Clement Christophe</td>
<td> R. S. Evans</td>
<td> Robert S. Evans</td>
<td> Malibu Moon</td>
<td> Resort</td>
<td> 5/1</td></tr><tr class="horse_4_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_4_7" style="display:none;"><td COLSPAN=9>
 Tr: St 19 W 11% P 11% S 21% ITM 43% ROI -41</td></tr><tr  class="horse_4_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/28/2018 - 3,    TAM,    FM,    0,    F,    0,    (T)0,    SCR,    0,    97,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 04/15/2017 - 5,    KEE,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    95,    78,    83,    2,    2,    4,    2,    150.00,    3,    2,    4,    4,    3,    Ortiz Jose L,    L,    123,    0.90,    Society Beau,    Upper Room,    Eastport,    got through, flattened,    10,    </div></br> 
<div style="color:blue" > 03/04/2017 - 3,    GP,    FT,    60,    F,    60,    (T)-1,    MSW,    0,    92,    69,    87,    8,    3,    6,    5,    450.00,    4,    -0,    1,    -1,    1,    Ortiz Jose L,    L,    120,    2.10,    Eastport,    Donwell,    Blind Ambition,    4-3w 1st, rail bid str,    9,    </div></br> 
<div style="color:red" > 01/28/2017 - 8,    GP,    FT,    30,    F,    30,    (T)-1,    MSW,    0,    97,    47,    85,    5,    6,    13,    8,    830.00,    10,    8,    3,    4,    3,    Ortiz Jose L,    L,    120,    7.40,    Lookin for Eight,    Borsa Vento,    Eastport,    cue1/4p,outside,gainin,    14,    </div></br> 
</td> </tr>
<tr  class="horse_4_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 87  ( date: 2017 ) 3   1   0   2   $33840.00    </br> 
  2017 : 3   1   0   2   $33840.00    </br> 
  2018 : 0   0   0   0   $0.00    </br> 
  GP: 0   0   0   0   $0.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 1   1   0   0   $22800.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="7" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> STARSHIP ZEUS</td>
<td> Davis Dylan</td>
<td> Baker Charlton</td>
<td> Mill Ridge Farm et al</td>
<td> Baker, Charlton and Foster, Michael S.</td>
<td> Bluegrass Cat</td>
<td> Willathewest</td>
<td> 12/1</td></tr><tr class="horse_5_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_5_7" style="display:none;"><td COLSPAN=9>
 Tr: St 18 W 11% P 17% S 11% ITM 39% ROI -10</td></tr><tr  class="horse_5_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/03/2018 - 5,    AQU,    FT,    82,    F,    82,    (T)-1,    ALW,    0,    102,    94,    94,    6,    6,    4,    2,    100.00,    3,    1,    3,    5,    5,    Rose Jeremy,    L,    123,    12.60,    Wild About Deb,    Storm Prophet,    Halloween Horror,    4w vs duo 3/8-1/8,wknd,    6,    </div></br> 
<div style="color:red" > 02/17/2018 - 8,    AQU,    MY,    0,    F,    0,    (T)0,    SCR,    0,    105,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 01/20/2018 - 4,    AQU,    FT,    48,    F,    48,    (T)-1,    CLM,    50000,    99,    55,    87,    4,    6,    7,    6,    500.00,    5,    6,    3,    5,    3,    Davis Dylan,    L,    119,    2.45,    Devine Dental,    E J's Legacy,    Starship Zeus,    6w upper, mild kick,    9,    </div></br> 
<div style="color:red" > 12/10/2017 - 6,    AQU,    GD,    55,    F,    55,    (T)-1,    ALW,    0,    100,    69,    99,    4,    3,    7,    4,    400.00,    5,    5,    4,    6,    3,    Davis Dylan,    L,    121,    20.90,    T R Crew,    Hammerin Aamer,    Starship Zeus,    5w upper, improved,    7,    </div></br> 
<div style="color:blue" > 11/04/2017 - 7,    AQU,    FT,    64,    F,    64,    (T)-1,    ALW,    0,    99,    52,    81,    3,    4,    4,    4,    200.00,    4,    5,    4,    10,    4,    Lopez Paco,    L,    121,    12.10,    Patternrecognition,    Spieth,    Hammerin Aamer,    chased 2p, weakened,    6,    </div></br> 
<div style="color:red" > 10/07/2017 - 2,    BEL,    FT,    56,    F,    56,    (T)-1,    ALW,    0,    99,    60,    86,    2,    5,    5,    11,    1250.00,    5,    13,    4,    11,    3,    Rosario Joel,    L,    121,    11.80,    Copper Town,    Patternrecognition,    Starship Zeus,    inside journey,got shw,    5,    </div></br> 
<div style="color:blue" > 08/19/2017 - 6,    SAR,    FT,    77,    F,    77,    (T)-1,    ALW,    0,    105,    78,    99,    9,    7,    9,    6,    590.00,    9,    3,    6,    2,    4,    Rosario Joel,    L,    121,    19.90,    Race Me Home,    Meantime,    Hookup,    OFF BIT SLW,SWUNG6W1/4,    9,    </div></br> 
<div style="color:red" > 07/28/2017 - 8,    SAR,    FM,    0,    F,    0,    (T)0,    SCR,    0,    99,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 06/29/2017 - 2,    CD,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    97,    74,    96,    2,    4,    5,    7,    460.00,    5,    4,    5,    3,    4,    Lanerie Corey J,    L,    122,    4.20,    Shut the Box,    Curlins Vow,    Gray Sky,    BLOCKED, STEADIED 1/8,    7,    </div></br> 
<div style="color:red" > 06/08/2017 - 7,    CD,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    103,    84,    91,    1,    6,    5,    2,    310.00,    5,    5,    4,    10,    3,    Bridgmohan Shaun,    L,    122,    2.80,    Ntestinalfortitude,    Curlins Vow,    Starship Zeus,    5path 1/4, moved up,    6,    </div></br> 
<div style="color:blue" > 05/12/2017 - 8,    CD,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    104,    43,    94,    3,    7,    6,    4,    400.00,    4,    3,    3,    3,    3,    Leparoux Julien R,    L,    123,    7.80,    Dan the Go to Man,    Gray Sky,    Starship Zeus,    7path turn, flattened,    7,    </div></br> 
<div style="color:red" > 04/14/2017 - 8,    KEE,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    101,    78,    90,    2,    8,    8,    11,    1100.00,    8,    10,    7,    5,    4,    Rosario Joel,    L,    118,    7.20,    Lex Vegas,    Big Red Rocket,    Star Hill,    7path turn, belatedly,    8,    </div></br> 
</td> </tr>
<tr  class="horse_5_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 99  ( date: 2017 ) 38   2   8   10   $186871.00    </br> 
  2017 : 10   0   1   4   $52475.00    </br> 
  2018 : 2   0   0   1   $6410.00    </br> 
  GP: 1   0   0   0   $2010.00    </br> 
  Turf: 10   0   2   1   $23716.00    </br> 
  Dist: 23   2   6   8   $148935.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="7" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> TERRY O GERI</td>
<td> Lezcano Abel</td>
<td> Persaud Randi</td>
<td> Sequel Thoroughbreds & JMJ RacingSt</td>
<td> Goberdhan, Tirbhawan</td>
<td> Freud</td>
<td> Throbbin' Heart</td>
<td> 20/1</td></tr><tr class="horse_6_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 33 W 3% P 6% S 9% ITM 18% ROI -78</td></tr><tr  class="horse_6_7" style="display:none;"><td COLSPAN=9>
 Tr: St 10 W 0% P 10% S 10% ITM 20% ROI -100</td></tr><tr  class="horse_6_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/10/2018 - 2,    AQU,    FT,    50,    F,    50,    (T)-1,    CLM,    35000,    92,    93,    75,    10,    3,    4,    2,    150.00,    2,    1,    2,    6,    2,    Hernandez Rafael M,    L,    119,    25.50,    Sir Alfred,    Terry O Geri,    Micozzi,    4w,5w upper,held place,    10,    </div></br> 
<div style="color:red" > 02/18/2018 - 7,    AQU,    SY,    28,    F,    28,    (T)-1,    CLM,    25000,    92,    101,    65,    8,    8,    4,    1,    50.00,    2,    6,    7,    15,    7,    Navarro Joshua,    L,    118,    5.90,    Gorelli,    Indian Soldier,    Competitiveness,    prompted outside,tired,    8,    </div></br> 
<div style="color:blue" > 12/17/2017 - 7,    AQU,    FT,    48,    F,    48,    (T)-1,    ALW,    0,    92,    65,    80,    4,    2,    4,    1,    250.00,    4,    4,    3,    7,    5,    Davis Dylan,    L,    122,    1.25,    For Pops,    Here Comes Tommy,    Dublinthepleasure,    3w upper, weakened,    8,    </div></br> 
<div style="color:red" > 12/01/2017 - 3,    AQU,    FT,    55,    F,    55,    (T)-1,    ALW,    0,    96,    88,    89,    4,    1,    3,    1,    -100.00,    1,    -1,    1,    1,    2,    Davis Dylan,    L,    119,    2.65,    Storm Prophet,    Terry O Geri,    Get Game,    3w,duel winner fin 1/8,    6,    </div></br> 
<div style="color:blue" > 11/12/2017 - 5,    AQU,    FT,    14,    F,    14,    (T)-1,    ALW,    0,    95,    36,    87,    3,    6,    9,    9,    850.00,    8,    7,    5,    8,    3,    Ortiz Jose L,    L,    122,    3.70,    My Boy Tate,    Benevolence,    Terry O Geri,    rail,cut corner drvng,    10,    </div></br> 
<div style="color:red" > 10/18/2017 - 8,    BEL,    FT,    64,    F,    64,    (T)-1,    ALW,    0,    95,    57,    92,    5,    2,    7,    5,    50.00,    2,    0,    2,    2,    3,    Ortiz Jr Irad,    L,    119,    5.70,    Fleet Irish,    Rock Doc,    Terry O Geri,    STUMBLED, BUMPED ST,    8,    </div></br> 
<div style="color:blue" > 08/24/2017 - 4,    SAR,    FT,    34,    F,    34,    (T)-1,    ALW,    0,    98,    73,    82,    9,    5,    2,    1,    150.00,    4,    5,    3,    7,    5,    Ortiz Jose L,    L,    120,    4.40,    Jewel Can Disco,    Heavy Meddle,    Bourbon Empire,    3w in aim, weakened,    9,    </div></br> 
<div style="color:red" > 08/11/2017 - 6,    SAR,    FT,    44,    F,    44,    (T)-1,    ALW,    0,    92,    76,    95,    1,    6,    5,    3,    160.00,    4,    1,    3,    2,    3,    Carmouche Kendrick,    L,    120,    4.30,    D'funnything,    Gaming,    Terry O Geri,    2-3w turn,out-kicked,    6,    </div></br> 
<div style="color:blue" > 07/22/2017 - 6,    SAR,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    98,    77,    78,    5,    4,    8,    5,    630.00,    11,    4,    4,    7,    4,    Alvarado Junior,    L,    120,    9.00,    Build to Suit,    Heavy Meddle,    Fleet Irish,    8w uppr, bid, wknd lte,    13,    </div></br> 
<div style="color:red" > 06/25/2017 - 9,    BEL,    GD,    65,    F,    65,    (T)18,    STK,    0,    99,    84,    81,    7,    8,    7,    7,    610.00,    7,    5,    5,    8,    5,    Franco Manuel,    L,    118,    14.30,    Crawdaddy,    Heldatgunpoint,    T Loves a Fight,    HIT GATE,2P,5W UPPER,    9,    </div></br> 
<div style="color:blue" > 05/29/2017 - 7,    BEL,    SY,    63,    F,    63,    (T)-1,    STK,    0,    101,    99,    70,    3,    5,    5,    3,    350.00,    6,    7,    6,    13,    6,    Franco Manuel,    L,    118,    9.10,    T Loves a Fight,    Syndergaard,    Pat On the Back,    BUMP BRK,4W & LONG DRV,    6,    </div></br> 
<div style="color:red" > 04/19/2017 - 3,    AQU,    FT,    56,    F,    56,    (T)-1,    AOC,    75000,    103,    74,    98,    2,    3,    3,    3,    300.00,    3,    -1,    1,    -5,    1,    Alvarado Junior,    L,    120,    7.20,    Terry O Geri,    Horoscope,    Mollica,    2w,chase duel,3w upper,    5,    </div></br> 
</td> </tr>
<tr  class="horse_6_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 98  ( date: 2017 ) 15   2   2   3   $114642.00    </br> 
  2017 : 12   2   1   3   $105710.00    </br> 
  2018 : 2   0   1   0   $8640.00    </br> 
  GP: 1   0   1   0   $8400.00    </br> 
  Turf: 2   0   0   0   $3292.00    </br> 
  Dist: 3   0   2   0   $22700.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="1A" race="7" title="Show More..." > 1A</br> <span class="past-performance-plus-b" > </span> </td>
<td> DONJI</td>
<td> Arroyo Angel S</td>
<td> Diodoro Robertino</td>
<td> Wayne Widmer & IEAH</td>
<td> Michael Dubb</td>
<td> Big Brown</td>
<td> Skip Queen</td>
<td> 6/1</td></tr><tr class="horse_1A_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 52 W 13% P 12% S 15% ITM 40% ROI -34</td></tr><tr  class="horse_1A_7" style="display:none;"><td COLSPAN=9>
 Tr: St 95 W 23% P 12% S 16% ITM 51% ROI -33</td></tr><tr  class="horse_1A_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/20/2018 - 7,    AQU,    FT,    48,    F,    48,    (T)-1,    ALW,    0,    103,    84,    63,    3,    6,    7,    3,    800.00,    8,    12,    8,    14,    8,    Arroyo Angel S,    L,    121,    11.50,    Spieth,    Mr. Buff,    Hammerin Aamer,    chased 2w, tired,    9,    </div></br> 
<div style="color:red" > 10/27/2017 - 7,    SA,    FT,    102,    F,    102,    (T)-1,    SHP,    16000,    108,    80,    103,    2,    5,    4,    3,    220.00,    4,    4,    2,    4,    2,    Desormeaux Kent J,    L,    124,    1.30,    Image of Joplin,    Donji,    City Steel,    came out str,held 2nd,    7,    </div></br> 
<div style="color:blue" > 09/22/2017 - 3,    LRC,    FT,    104,    F,    104,    (T)-1,    STR,    16000,    107,    83,    108,    1,    5,    3,    2,    350.00,    3,    3,    2,    1,    2,    Desormeaux Kent J,    L,    124,    0.90,    Tribal Roar,    Donji,    Lazzam,    inside rally, edged,    6,    </div></br> 
<div style="color:red" > 08/27/2017 - 4,    DMR,    FT,    200,    F,    200,    (T)-1,    CLM,    32000,    104,    100,    102,    2,    3,    2,    0,    -10.00,    1,    -1,    1,    1,    2,    Desormeaux Kent J,    L,    120,    2.90,    Shackleford Banks,    Donji,    Blanket of Ice,    dueled btwn,led,game,    7,    </div></br> 
<div style="color:blue" > 08/04/2017 - 7,    DMR,    FM,    130,    F,    130,    (T)12,    ALW,    0,    113,    96,    103,    7,    1,    4,    2,    230.00,    5,    5,    6,    4,    4,    Bejarano Rafael,    L,    122,    5.20,    Eckersley,    Burnaroundtheedges,    Klondike Creek,    BOBBLED EARLY,3WD STR,    7,    </div></br> 
<div style="color:red" > 07/09/2017 - 8,    OTP,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    102,    101,    103,    5,    2,    5,    4,    260.00,    3,    1,    2,    1,    2,    Gomez Alejandro,    L,    125,    2.70,    Saxon Lord,    Donji,    Percy Fawcett,    circled 4w, 2d best,    8,    </div></br> 
<div style="color:blue" > 04/28/2017 - 8,    GG,    FM,    60,    F,    60,    (T)20,    AOC,    32000,    107,    90,    97,    6,    3,    2,    2,    100.00,    2,    2,    3,    2,    4,    Gomez Alejandro,    L,    122,    3.80,    Taelyns Prince,    Lovenseek,    Mr Kerry Hara,    bid 3w,lost 3d late,    9,    </div></br> 
<div style="color:red" > 04/08/2017 - 8,    GG,    FT,    60,    F,    60,    (T)-1,    ALW,    0,    107,    79,    100,    3,    8,    8,    6,    560.00,    7,    3,    5,    2,    4,    Gomez Alejandro,    L,    122,    2.40,    Squidward,    Taelyns Prince,    Super Hawk,    circled 5w, late rally,    9,    </div></br> 
<div style="color:blue" > 02/25/2017 - 8,    GG,    FT,    60,    F,    60,    (T)-1,    AOC,    32000,    109,    87,    106,    8,    7,    7,    7,    710.00,    7,    3,    7,    1,    3,    Gomez Alejandro,    L,    122,    4.90,    Pepper Crown,    Eagle Screams,    Donji,    3w,swung out,clsd fst,    8,    </div></br> 
<div style="color:red" > 02/04/2017 - 8,    GG,    FT,    85,    F,    85,    (T)-1,    AOC,    32000,    109,    102,    107,    3,    2,    1,    -0,    10.00,    2,    1,    4,    1,    4,    Gomez Alejandro,    L,    122,    4.00,    Broken Up,    Eagle Screams,    Taelyns Prince,    pace 2w,outkckd,kpt on,    8,    </div></br> 
<div style="color:blue" > 12/26/2016 - 7,    GG,    YL,    40,    F,    40,    (T)0,    ALW,    0,    108,    103,    109,    9,    9,    8,    8,    770.00,    8,    7,    6,    1,    2,    Gomez Alejandro,    L,    122,    10.10,    He's a Pepper,    Donji,    Dano's Dream,    FANNED 4W, CLOSED FAST,    10,    </div></br> 
<div style="color:red" > 11/26/2016 - 8,    GG,    FT,    60,    F,    60,    (T)-1,    ALW,    0,    108,    74,    96,    1,    2,    4,    3,    300.00,    3,    4,    5,    3,    3,    Hernandez Juan J,    L,    122,    2.70,    Many Roses,    He's a Pepper,    Donji,    chased rail, no rally,    10,    </div></br> 
</td> </tr>
<tr  class="horse_1A_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 109  ( date: 2016 ) 41   4   11   6   $129190.00    </br> 
  2017 : 9   0   4   1   $34020.00    </br> 
  2018 : 1   0   0   0   $335.00    </br> 
  GP: 1   0   0   0   $335.00    </br> 
  Turf: 13   2   1   1   $40745.00    </br> 
  Dist: 18   1   7   4   $66610.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="7" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> KING KOA</td>
<td> Rocco Jr Joseph</td>
<td> Caiazzo Corby</td>
<td> Colts Neck Stables LLC</td>
<td> Sinatra Thoroughbred Racing</td>
<td> Spring At Last</td>
<td> Shade Maker</td>
<td> 20/1</td></tr><tr class="horse_7_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 6% P 14% S 6% ITM 26% ROI -28</td></tr><tr  class="horse_7_7" style="display:none;"><td COLSPAN=9>
 Tr: St 3 W 0% P 0% S 33% ITM 33% ROI -100</td></tr><tr  class="horse_7_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/24/2018 - 7,    LRL,    SY,    0,    F,    0,    (T)0,    SCR,    0,    93,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 02/01/2018 - 4,    AQU,    GD,    82,    F,    82,    (T)-1,    ALW,    0,    98,    89,    83,    1,    4,    4,    3,    160.00,    4,    6,    4,    8,    4,    Davis Dylan,    121,    2.75,    Admiral Blue,    Storm Prophet,    Jet Black,    tracked ins, no rally,    5,    </div></br> 
<div style="color:blue" > 12/15/2017 - 8,    LRL,    FT,    110,    F,    110,    (T)-1,    ALW,    0,    93,    71,    88,    7,    7,    6,    5,    460.00,    5,    4,    3,    3,    3,    Hamilton Steve D,    123,    2.50,    Glennrichment,    Grecian Prince,    King Koa,    circled 5wd,flattn out,    7,    </div></br> 
<div style="color:red" > 11/22/2017 - 5,    AQU,    SY,    54,    F,    54,    (T)-1,    STR,    50000,    97,    73,    93,    2,    2,    2,    1,    100.00,    2,    1,    2,    -0,    1,    Lezcano Jose,    121,    7.50,    King Koa,    Shadow Rider,    Scarf It Down,    3w,up ins 1/16,dug in,    5,    </div></br> 
<div style="color:blue" > 11/18/2017 - 6,    LRL,    GD,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/30/2017 - 7,    LRL,    FT,    110,    F,    110,    (T)-1,    ALW,    0,    95,    62,    90,    1,    2,    2,    0,    -10.00,    1,    0,    3,    2,    2,    Hamilton Steve D,    117,    21.70,    Rare Candy,    King Koa,    Social Stranger,    rail, dueled, sharp,    9,    </div></br> 
<div style="color:blue" > 09/30/2017 - 10,    PEN,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    91,    83,    90,    3,    5,    3,    2,    50.00,    2,    -1,    1,    0,    2,    Whitney Dana G,    120,    6.00,    Grey Gator,    King Koa,    Against the Odds,    2-3w,all out,just miss,    7,    </div></br> 
<div style="color:red" > 09/17/2017 - 10,    LRL,    FM,    50,    F,    50,    (T)0,    SOC,    25000,    86,    57,    75,    8,    8,    9,    7,    650.00,    9,    9,    9,    6,    8,    Davis Katie,    120,    8.30,    Tennessee Wildcat,    Kid Jeter,    Howyaformoney,    failed to menace,    10,    </div></br> 
<div style="color:blue" > 09/02/2017 - 3,    DEL,    GD,    56,    F,    56,    (T)-1,    MCL,    40000,    72,    77,    93,    6,    3,    4,    5,    150.00,    2,    1,    2,    -1,    1,    Davis Katie,    120,    4.60,    King Koa,    Notjudginjustsayin,    Invigorating,    CARRIED WIDE, DRIVING,    6,    </div></br> 
<div style="color:red" > 08/11/2017 - 7,    LRL,    FT,    60,    F,    60,    (T)-1,    WMC,    25000,    72,    15,    56,    4,    7,    11,    11,    960.00,    9,    10,    7,    10,    5,    Davis Katie,    119,    42.40,    Swing Step,    Punch Nephew,    Utter Magic,    by-faders,left lead,    11,    </div></br> 
</td> </tr>
<tr  class="horse_7_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 93  ( date: 2017 ) 8   2   2   1   $73624.00    </br> 
  2017 : 7   2   2   1   $70274.00    </br> 
  2018 : 1   0   0   0   $3350.00    </br> 
  GP: 8   2   2   1   $73624.00    </br> 
  Turf: 1   0   0   0   $0.00    </br> 
  Dist: 5   2   2   1   $69464.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="8" race="7" title="Show More..." > 8</br> <span class="past-performance-plus-b" > </span> </td>
<td> MR. BUFF</td>
<td> Alvarado Junior</td>
<td> Kimmel John C</td>
<td> Chester Broman & Mary R. Broman</td>
<td> Broman, Sr., Chester and Mary</td>
<td> Friend Or Foe</td>
<td> Speightful Affair</td>
<td> 5/2</td></tr><tr class="horse_8_7" style="display:none;"><td COLSPAN=9>
  Jocks: St 61 W 28% P 13% S 25% ITM 66% ROI 2</td></tr><tr  class="horse_8_7" style="display:none;"><td COLSPAN=9>
 Tr: St 15 W 7% P 20% S 20% ITM 47% ROI -25</td></tr><tr  class="horse_8_7" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 8,    AQU,    MY,    48,    F,    48,    (T)-1,    ALW,    0,    105,    131,    102,    1,    3,    1,    -0,    -200.00,    1,    -2,    1,    4,    2,    Lezcano Abel,    L,    121,    3.85,    Preservationist,    Mr. Buff,    Hammerin Aamer,    hustled st, 2nd best,    8,    </div></br> 
<div style="color:red" > 01/20/2018 - 7,    AQU,    FT,    48,    F,    48,    (T)-1,    ALW,    0,    103,    94,    83,    1,    3,    1,    -1,    -50.00,    1,    2,    2,    2,    2,    Lezcano Abel,    L,    121,    4.20,    Spieth,    Mr. Buff,    Hammerin Aamer,    ins,test by winner 1/4,    9,    </div></br> 
<div style="color:blue" > 12/01/2017 - 8,    AQU,    FT,    57,    F,    57,    (T)-1,    ALW,    0,    104,    96,    73,    6,    3,    2,    1,    100.00,    2,    6,    5,    16,    5,    Luzzi Michael J,    L,    122,    3.30,    Flash Trading,    Westwood,    Forest Blue,    4w upper, tired,    7,    </div></br> 
<div style="color:red" > 10/15/2017 - 8,    BEL,    FT,    57,    F,    57,    (T)-1,    AOC,    40000,    101,    85,    103,    3,    1,    1,    -2,    -150.00,    1,    -2,    1,    -1,    1,    Luzzi Michael J,    L,    117,    1.45,    Mr. Buff,    Giantinthemoonlite,    Nobody Move,    ins vs duo,ask3/16,gme,    7,    </div></br> 
<div style="color:blue" > 09/22/2017 - 7,    BEL,    FT,    51,    F,    51,    (T)-1,    AOC,    40000,    100,    101,    98,    2,    9,    3,    1,    -10.00,    1,    -1,    1,    0,    2,    Luzzi Michael J,    L,    117,    12.60,    Sudden Surprise,    Mr. Buff,    West Hills Giant,    BMP ST,INS,VIE 3X,GAME,    10,    </div></br> 
<div style="color:red" > 04/02/2017 - 6,    AQU,    GD,    40,    F,    40,    (T)-1,    SOC,    50000,    91,    95,    98,    5,    4,    1,    -1,    -10.00,    1,    -1,    1,    -2,    1,    Luzzi Michael J,    L,    119,    1.35,    Mr. Buff,    Toga Challenger,    Dynamax Prime,    BRUSH GATE, INCHED CLR,    5,    </div></br> 
<div style="color:blue" > 02/24/2017 - 6,    AQU,    FT,    60,    F,    60,    (T)-1,    AOC,    75000,    92,    76,    96,    9,    3,    1,    -1,    -100.00,    1,    -0,    1,    -0,    1,    Luzzi Michael J,    L,    120,    3.90,    Mr. Buff,    Stretch's Stone,    Dynamax Prime,    TRADED BUMPS, GAME,    10,    </div></br> 
<div style="color:red" > 01/13/2017 - 6,    AQU,    FT,    24,    F,    24,    (T)-1,    AOC,    75000,    90,    68,    88,    4,    7,    6,    7,    550.00,    4,    7,    3,    3,    3,    Camacho Jr Samuel,    L,    120,    8.10,    Tribecca,    Remstin,    Mr. Buff,    2W,FAN 4W 1/8,BLED,    7,    </div></br> 
<div style="color:blue" > 12/17/2016 - 1,    AQU,    GD,    56,    F,    56,    (T)-1,    AOC,    75000,    93,    58,    44,    8,    8,    4,    2,    360.00,    5,    15,    6,    17,    5,    Carmouche Kendrick,    L,    120,    0.85,    Carradine,    Fleet Irish,    The Caretaker,    4w 1st turn, tired,    8,    </div></br> 
<div style="color:red" > 11/25/2016 - 5,    AQU,    GD,    66,    F,    66,    (T)18,    AOC,    75000,    91,    61,    70,    1,    3,    5,    4,    360.00,    6,    4,    8,    7,    8,    Carmouche Kendrick,    L,    120,    4.40,    Mo Maverick,    Manifest Destiny,    D'yer Mak'er,    slip 2p back,4w upper,    11,    </div></br> 
<div style="color:blue" > 11/19/2016 - 5,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    92,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 10/22/2016 - 7,    BEL,    SY,    44,    F,    44,    (T)-1,    STK,    0,    96,    86,    71,    8,    5,    3,    1,    260.00,    3,    4,    3,    5,    4,    Carmouche Kendrick,    L,    120,    13.90,    Pat On the Back,    Tellmeafookystory,    Gold for the King,    3w off duel, wknd late,    9,    </div></br> 
</td> </tr>
<tr  class="horse_8_7" style="display:none;"><td COLSPAN=9>
LifeTime Record 103  ( date: 2017 ) 13   3   4   1   $185300.00    </br> 
  2017 : 6   2   2   1   $105910.00    </br> 
  2018 : 2   0   2   0   $26800.00    </br> 
  GP: 13   3   4   1   $185300.00    </br> 
  Turf: 1   0   0   0   $190.00    </br> 
  Dist: 6   1   3   0   $87210.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="8" race_date="20180317" claimamt="0" dist_disp="6F" todays_cls="104" purse="100000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #8</span> - 
<span> Claiming $0</span> - 
<span> Distance 6F</span> - 
<span> Todays CLS 104</span> -
<span> Purse $100000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10), Double Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="8" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> NISHA</td>
<td> Rose Jeremy</td>
<td> Englehart Jeremiah C</td>
<td> Lesley Campion</td>
<td> James A. Riccio</td>
<td> First Samurai</td>
<td> Lily's Joy</td>
<td> 8/1</td></tr><tr class="horse_1_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 21 W 0% P 19% S 14% ITM 33% ROI -100</td></tr><tr  class="horse_1_8" style="display:none;"><td COLSPAN=9>
 Tr: St 31 W 23% P 23% S 6% ITM 52% ROI 29</td></tr><tr  class="horse_1_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/08/2018 - 3,    AQU,    GD,    42,    F,    42,    (T)-1,    AOC,    62500,    96,    119,    102,    3,    1,    1,    -1,    -100.00,    1,    -2,    1,    -0,    1,    Lopez Paco,    L,    121,    1.90,    Nisha,    Sounds Delicious,    Littlefirefighter,    ins half,2-3w upper,    5,    </div></br> 
<div style="color:red" > 01/13/2018 - 7,    AQU,    MY,    20,    F,    20,    (T)-1,    ALW,    0,    93,    114,    90,    3,    1,    1,    -0,    -50.00,    1,    -2,    1,    -1,    1,    Lopez Paco,    L,    123,    2.45,    Nisha,    Ladies Day,    China Rider,    BRKE THRU GATE,2W,DRFT,    8,    </div></br> 
<div style="color:blue" > 12/01/2017 - 6,    AQU,    FM,    68,    F,    68,    (T)0,    ALW,    0,    93,    -99,    -97,    14,    14,    14,    100,    9999.00,    14,    100,    14,    100,    14,    Ortiz Jose L,    L,    122,    3.50,    Bigkat and Camille,    Avery Maeve,    Sister Sophia,    UNPREPARED, LOST RIDER,    14,    </div></br> 
<div style="color:red" > 10/19/2017 - 5,    BEL,    FM,    202,    F,    202,    (T)18,    CLM,    25000,    97,    104,    92,    1,    1,    1,    -1,    -50.00,    1,    -2,    1,    -1,    1,    Maragh Rajiv,    L,    119,    7.10,    Nisha,    Lamontagne,    Vicki's Dancer,    in hand ins, held safe,    7,    </div></br> 
<div style="color:blue" > 09/28/2017 - 5,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    97,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 09/09/2017 - 1,    BEL,    FT,    54,    F,    54,    (T)-1,    CLM,    25000,    87,    81,    88,    7,    3,    2,    1,    50.00,    2,    -3,    1,    -3,    1,    Saez Luis,    L,    118,    2.45,    Nisha,    Gobi,    Isotope,    prompt 3-2w, edged clr,    7,    </div></br> 
<div style="color:blue" > 08/09/2017 - 6,    SAR,    FM,    110,    F,    110,    (T)18,    CLM,    40000,    90,    106,    77,    1,    1,    2,    2,    200.00,    3,    5,    7,    10,    8,    Carmouche Kendrick,    L,    118,    5.80,    Vasilika,    Culpa Mia,    Fahan Mura,    BRUSHED GATE, TIRED,    10,    </div></br> 
<div style="color:red" > 07/23/2017 - 7,    SAR,    FT,    45,    F,    45,    (T)-1,    CLM,    25000,    88,    105,    73,    6,    9,    3,    1,    50.00,    2,    1,    3,    6,    3,    Ortiz Jr Irad,    L,    120,    2.50,    Tainted Angel,    Honor Way,    Nisha,    STUMBLED ST, WKND LATE,    12,    </div></br> 
<div style="color:blue" > 06/25/2017 - 8,    MNR,    FT,    50,    F,    50,    (T)-1,    ALW,    0,    70,    65,    90,    2,    1,    1,    -2,    -300.00,    1,    -10,    1,    -12,    1,    Vigil Noel,    L,    118,    0.60,    Nisha,    She's Jiggy,    K's Jordan,    drew off, ridden out,,    6,    </div></br> 
<div style="color:red" > 05/12/2017 - 6,    AP,    FT,    36,    F,    36,    (T)-1,    MSW,    0,    85,    105,    83,    1,    2,    1,    -0,    -10.00,    1,    -1,    1,    -1,    1,    Sanjur Santo,    L,    118,    8.00,    Nisha,    Ruby Dusk,    Flash N Go,    dueled inside, dug in,    10,    </div></br> 
</td> </tr>
<tr  class="horse_1_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2018 ) 9   6   0   1   $169444.00    </br> 
  2017 : 7   4   0   1   $87844.00    </br> 
  2018 : 2   2   0   0   $81600.00    </br> 
  GP: 3   2   0   0   $81600.00    </br> 
  Turf: 3   1   0   0   $28448.00    </br> 
  Dist: 5   4   0   1   $124196.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="8" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> ANYDAYISMYDAY</td>
<td> DeCarlo Christopher P</td>
<td> Pringle Edmund</td>
<td> Stonestreet Thoroughbred Holdings L</td>
<td> Joseph A. Lowe</td>
<td> Afleet Alex</td>
<td> Noelle Rose</td>
<td> 20/1</td></tr><tr class="horse_2_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 8 W 13% P 13% S 0% ITM 26% ROI -53</td></tr><tr  class="horse_2_8" style="display:none;"><td COLSPAN=9>
 Tr: St 6 W 0% P 17% S 0% ITM 17% ROI -100</td></tr><tr  class="horse_2_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/23/2018 - 2,    AQU,    MY,    30,    F,    30,    (T)-1,    AOC,    80000,    97,    44,    80,    2,    2,    3,    3,    200.00,    3,    6,    4,    6,    5,    Hernandez Rafael M,    L,    119,    5.50,    Alpine Sky,    Summer Reading,    Friend of Liberty,    chased ins, weakened,    5,    </div></br> 
<div style="color:red" > 05/05/2017 - 7,    BEL,    SY,    63,    F,    63,    (T)-1,    STK,    0,    107,    102,    93,    1,    5,    3,    3,    700.00,    3,    8,    6,    8,    6,    DeCarlo Christopher P,    L,    114,    25.25,    By the Moon,    Pretty N Cool,    Quezon,    rode rail, cut corner,    8,    </div></br> 
<div style="color:blue" > 03/17/2017 - 8,    AQU,    GD,    26,    F,    26,    (T)-1,    AOC,    62500,    94,    72,    90,    3,    4,    3,    2,    150.00,    3,    1,    2,    -1,    1,    Maragh Rajiv,    L,    121,    0.95,    Anydayismyday,    Da Wildcat Girl,    Myfourchix,    3w upper, kept busy,    7,    </div></br> 
<div style="color:red" > 02/10/2017 - 7,    AQU,    FT,    24,    F,    24,    (T)-1,    ALW,    0,    94,    96,    95,    4,    1,    3,    2,    -10.00,    1,    -3,    1,    -3,    1,    Maragh Rajiv,    L,    119,    12.10,    Anydayismyday,    Ring Knocker,    My Fair Lily,    clear inside,ask 3/16,    9,    </div></br> 
<div style="color:blue" > 05/11/2016 - 3,    BEL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    87,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:red" > 03/26/2016 - 3,    AQU,    FT,    24,    F,    24,    (T)-1,    STK,    0,    92,    70,    93,    2,    3,    4,    3,    310.00,    4,    3,    4,    1,    3,    Silvera Ruben,    L,    116,    26.25,    Lost Raven,    Takrees,    Anydayismyday,    2p,drive by 5/16pl,    4,    </div></br> 
<div style="color:blue" > 02/28/2016 - 5,    AQU,    FT,    20,    F,    20,    (T)-1,    MSW,    0,    82,    93,    88,    4,    3,    4,    2,    400.00,    3,    3,    2,    -0,    1,    Alvarado Junior,    L,    120,    2.65,    Anydayismyday,    Battle Tux,    Shoppingforsilver,    chased 2-3w, up late,    6,    </div></br> 
<div style="color:red" > 02/07/2016 - 1,    AQU,    FT,    20,    F,    20,    (T)-1,    MSW,    0,    83,    75,    80,    4,    4,    4,    3,    260.00,    4,    2,    4,    4,    3,    Silvera Ruben,    L,    120,    3.95,    Takrees,    Liana Star,    Anydayismyday,    ins,sharply 4w into,    6,    </div></br> 
<div style="color:blue" > 01/17/2016 - 4,    AQU,    FT,    18,    F,    18,    (T)-1,    MSW,    0,    81,    66,    78,    7,    1,    3,    2,    150.00,    2,    2,    2,    3,    3,    Silvera Ruben,    L,    120,    5.10,    Appealing Maggie,    Mahabodhi Tree,    Anydayismyday,    disputed 2p, weakend,    8,    </div></br> 
<div style="color:red" > 01/01/2016 - 6,    AQU,    FT,    64,    F,    64,    (T)-1,    MSW,    0,    86,    84,    53,    11,    8,    3,    2,    100.00,    3,    9,    5,    14,    6,    Silvera Ruben,    L,    120,    7.10,    Scatoosh,    Thundering Sky,    She's So Fine,    3w pursuit, tired,    11,    </div></br> 
<div style="color:blue" > 12/06/2015 - 4,    AQU,    FT,    12,    F,    12,    (T)-1,    MSW,    0,    80,    92,    71,    2,    5,    3,    2,    260.00,    4,    5,    3,    6,    2,    Silvera Ruben,    L,    120,    8.20,    Browse,    Anydayismyday,    Presumptuous,    5w upper, ran on,    10,    </div></br> 
<div style="color:red" > 11/15/2015 - 1,    AQU,    FT,    40,    F,    40,    (T)-1,    MSW,    0,    80,    76,    67,    6,    2,    3,    2,    200.00,    3,    1,    2,    1,    2,    Silvera Ruben,    L,    120,    13.00,    Mo d'Amour,    Anydayismyday,    Carella,    3W,LUG IN UPR,ERRATIC,    7,    </div></br> 
</td> </tr>
<tr  class="horse_2_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 95  ( date: 2017 ) 11   3   2   3   $169794.00    </br> 
  2017 : 3   2   0   0   $82934.00    </br> 
  2018 : 1   0   0   0   $2160.00    </br> 
  GP: 11   3   2   3   $169794.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 10   3   2   3   $169594.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="8" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> SOUNDS DELICIOUS</td>
<td> Arroyo Angel S</td>
<td> Rice Linda</td>
<td> Eric A. DelValle</td>
<td> Stud El Aguila</td>
<td> Yes It's True</td>
<td> Dulce Realidad</td>
<td> 4/1</td></tr><tr class="horse_3_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 52 W 13% P 12% S 15% ITM 40% ROI -34</td></tr><tr  class="horse_3_8" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_3_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/04/2018 - 8,    AQU,    FT,    28,    F,    28,    (T)-1,    AOC,    62500,    101,    74,    96,    3,    2,    1,    -2,    -50.00,    1,    -7,    1,    -6,    1,    Alvarado Junior,    L,    118,    0.45,    Sounds Delicious,    Kelsocait,    Short Kakes,    drive3/16-1/16,handily,    5,    </div></br> 
<div style="color:red" > 02/08/2018 - 3,    AQU,    GD,    42,    F,    42,    (T)-1,    AOC,    62500,    96,    106,    101,    1,    2,    3,    2,    150.00,    3,    2,    2,    0,    2,    Alvarado Junior,    L,    119,    1.70,    Nisha,    Sounds Delicious,    Littlefirefighter,    ins,2w upr,alter 1/16p,    5,    </div></br> 
<div style="color:blue" > 01/26/2018 - 8,    LRL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    88,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 03/25/2017 - 3,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    86,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 03/05/2017 - 8,    LRL,    FT,    60,    F,    60,    (T)-1,    AOC,    50000,    79,    74,    102,    7,    1,    1,    -1,    -400.00,    1,    -7,    1,    -11,    1,    Karamanos Horacio,    L,    122,    0.70,    Sounds Delicious,    Phantom Shot,    Heaven's Door,    rate 3w,widen,rddn out,    7,    </div></br> 
<div style="color:red" > 01/26/2017 - 4,    AQU,    FT,    24,    F,    24,    (T)-1,    SOC,    50000,    73,    107,    81,    7,    4,    2,    2,    150.00,    2,    -3,    1,    -9,    1,    Velasquez Cornelio H,    L,    122,    20.20,    Sounds Delicious,    Clairvoyant Lady,    Woundwithhereyes,    3w upper, went clear,    7,    </div></br> 
<div style="color:blue" > 12/10/2016 - 1,    LRL,    FT,    60,    F,    60,    (T)-1,    MCL,    40000,    62,    61,    64,    6,    6,    2,    1,    100.00,    2,    -1,    1,    -1,    1,    Karamanos Horacio,    L,    122,    1.80,    Sounds Delicious,    Jumpin' Nancy,    Beautiful Nite Sky,    3wd turn, driving,    7,    </div></br> 
</td> </tr>
<tr  class="horse_3_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2017 ) 5   4   1   0   $132750.00    </br> 
  2017 : 2   2   0   0   $58740.00    </br> 
  2018 : 2   1   1   0   $55200.00    </br> 
  GP: 5   4   1   0   $132750.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 5   4   1   0   $132750.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="8" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> SPICE LADY</td>
<td> McCarthy Trevor</td>
<td> Pletcher Todd A</td>
<td> Piacentino Farms, Town and CountryF</td>
<td> Repole Stable</td>
<td> Speightstown</td>
<td> My American Girl</td>
<td> 6/1</td></tr><tr class="horse_4_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 94 W 15% P 18% S 16% ITM 49% ROI -10</td></tr><tr  class="horse_4_8" style="display:none;"><td COLSPAN=9>
 Tr: St 85 W 28% P 20% S 20% ITM 68% ROI -24</td></tr><tr  class="horse_4_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/27/2018 - 9,    GP,    FT,    20,    F,    20,    (T)-1,    STK,    0,    105,    116,    86,    8,    4,    2,    1,    -50.00,    1,    1,    3,    7,    7,    Velazquez John R,    L,    117,    7.40,    Jordan's Henny,    Curlin's Approval,    Rich Mommy,    pressed, led 3w, tired,    12,    </div></br> 
<div style="color:red" > 01/19/2018 - 9,    GP,    FT,    0,    F,    0,    (T)0,    SCR,    0,    92,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 12/15/2017 - 9,    GP,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    95,    85,    102,    2,    1,    1,    -1,    -50.00,    1,    -1,    1,    -6,    1,    Velazquez John R,    L,    120,    0.80,    Spice Lady,    No Sweat,    Last Kiss,    pace,vied1/4p,3w,clear,    7,    </div></br> 
<div style="color:red" > 05/29/2017 - 9,    GP,    FM,    0,    F,    0,    (T)0,    SCR,    0,    97,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 03/31/2017 - 9,    GP,    FT,    5,    F,    5,    (T)-1,    AOC,    75000,    92,    105,    91,    5,    3,    4,    2,    250.00,    3,    3,    2,    5,    4,    Velazquez John R,    L,    118,    2.20,    Florida Fabulous,    Teresa Z,    Silver Threads,    4w, aim into str, wknd,    5,    </div></br> 
<div style="color:red" > 02/26/2017 - 2,    GP,    FT,    20,    F,    20,    (T)-1,    MSW,    0,    89,    67,    86,    1,    4,    4,    2,    70.00,    4,    -1,    1,    -1,    1,    Velazquez John R,    L,    120,    1.30,    Spice Lady,    Afleet Tizzy,    Raging Town,    BRK IN, 4W, LONG DRIVE,    6,    </div></br> 
</td> </tr>
<tr  class="horse_4_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2017 ) 4   2   0   0   $51850.00    </br> 
  2017 : 3   2   0   0   $50600.00    </br> 
  2018 : 1   0   0   0   $1250.00    </br> 
  GP: 4   2   0   0   $51850.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 1   1   0   0   $22800.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="8" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> QUEZON</td>
<td> Rocco Jr Joseph</td>
<td> Ribaudo Robert</td>
<td> Apache Farm LLC</td>
<td> Marc Keller</td>
<td> Tiz Wonderful</td>
<td> Kalookan Dancer</td>
<td> 7/2</td></tr><tr class="horse_5_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 6% P 14% S 6% ITM 26% ROI -28</td></tr><tr  class="horse_5_8" style="display:none;"><td COLSPAN=9>
 Tr: St 3 W 0% P 33% S 0% ITM 33% ROI -100</td></tr><tr  class="horse_5_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 10,    LRL,    MY,    0,    F,    0,    (T)0,    SCR,    0,    99,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 01/19/2018 - 8,    AQU,    FT,    54,    F,    54,    (T)-1,    STK,    0,    102,    54,    100,    2,    3,    5,    4,    260.00,    4,    -3,    1,    -4,    1,    Rocco Jr Joseph,    L,    121,    0.70,    Quezon,    Palladian Bridge,    Wonderment,    to 3p 3/8,bid3/16,drve,    6,    </div></br> 
<div style="color:blue" > 12/09/2017 - 8,    AQU,    GD,    46,    F,    46,    (T)-1,    STK,    0,    103,    62,    98,    1,    5,    5,    2,    260.00,    5,    2,    3,    -1,    1,    Rocco Jr Joseph,    L,    119,    1.45,    Quezon,    Picco Uno,    Absatootly,    3w uppr, up last jumps,    5,    </div></br> 
<div style="color:red" > 10/21/2017 - 3,    BEL,    FT,    55,    F,    55,    (T)-1,    STK,    0,    100,    84,    102,    2,    2,    3,    3,    100.00,    2,    -1,    1,    1,    2,    Rosario Joel,    L,    118,    1.35,    Absatootly,    Quezon,    Cozzy Spring,    overconfident, bested,    5,    </div></br> 
<div style="color:blue" > 09/24/2017 - 5,    BEL,    FT,    55,    F,    55,    (T)-1,    STK,    0,    105,    102,    106,    4,    2,    2,    2,    150.00,    3,    1,    3,    1,    3,    Rosario Joel,    L,    118,    9.30,    Highway Star,    Carina Mia,    Quezon,    rail,ins duo final 1/8,    6,    </div></br> 
<div style="color:red" > 08/17/2017 - 8,    SAR,    FT,    70,    F,    70,    (T)-1,    STK,    0,    103,    84,    97,    1,    5,    6,    4,    380.00,    7,    4,    5,    3,    2,    Castellano Javier,    L,    122,    1.50,    Picco Uno,    Quezon,    Absatootly,    ins,pull left half,8w,    9,    </div></br> 
<div style="color:blue" > 07/03/2017 - 4,    BEL,    FT,    70,    F,    70,    (T)-1,    AOC,    80000,    100,    56,    99,    2,    4,    3,    2,    250.00,    3,    2,    3,    1,    2,    Castellano Javier,    L,    120,    0.65,    Momameamaria,    Quezon,    Summer House,    4w upper, repelled,    7,    </div></br> 
<div style="color:red" > 06/09/2017 - 11,    BEL,    FT,    63,    F,    63,    (T)-1,    STK,    0,    109,    50,    90,    4,    3,    4,    2,    150.00,    3,    5,    5,    6,    6,    Franco Manuel,    L,    119,    7.80,    By the Moon,    Mia Torri,    Lightstream,    LOST FOOTING ST, WKND,    9,    </div></br> 
<div style="color:blue" > 05/05/2017 - 7,    BEL,    SY,    63,    F,    63,    (T)-1,    STK,    0,    107,    88,    100,    2,    3,    5,    5,    820.00,    6,    7,    3,    5,    3,    Cancel Eric,    L,    115,    2.55,    By the Moon,    Pretty N Cool,    Quezon,    rail turn,urge 2w into,    8,    </div></br> 
<div style="color:red" > 10/22/2016 - 3,    BEL,    SY,    56,    F,    56,    (T)-1,    STK,    0,    104,    96,    110,    2,    2,    3,    2,    200.00,    2,    -3,    1,    -5,    1,    Castellano Javier,    L,    119,    0.25,    Quezon,    Wonderment,    Court Dancer,    6-7w upper, drew clear,    5,    </div></br> 
<div style="color:blue" > 10/01/2016 - 7,    BEL,    MY,    54,    F,    54,    (T)-1,    STK,    0,    108,    78,    107,    2,    6,    4,    4,    410.00,    4,    1,    3,    1,    2,    Castellano Javier,    L,    114,    4.20,    Paulassilverlining,    Quezon,    Wavell Avenue,    chased 2-3w, willingly,    6,    </div></br> 
<div style="color:red" > 08/18/2016 - 9,    SAR,    FT,    70,    F,    70,    (T)-1,    STK,    0,    104,    60,    102,    1,    4,    4,    3,    200.00,    2,    2,    2,    2,    2,    Franco Manuel,    L,    120,    2.40,    Hot City Girl,    Quezon,    Dr. Fager's Gal,    chck st,inside to 5/16,    6,    </div></br> 
</td> </tr>
<tr  class="horse_5_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 110  ( date: 2016 ) 18   7   6   2   $754200.00    </br> 
  2017 : 7   1   3   2   $182000.00    </br> 
  2018 : 1   1   0   0   $60000.00    </br> 
  GP: 18   7   6   2   $754200.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 11   3   6   2   $410200.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="8" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> STARTWITHSILVER</td>
<td> Alvarado Junior</td>
<td> Rice Linda</td>
<td> Burning Sands Stable, LLC</td>
<td> Iris Smith Stable, Lady Sheila Stable, and Ri</td>
<td> Jump Start</td>
<td> Office Miss</td>
<td> 5/2</td></tr><tr class="horse_6_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 61 W 28% P 13% S 25% ITM 66% ROI 2</td></tr><tr  class="horse_6_8" style="display:none;"><td COLSPAN=9>
 Tr: St 43 W 30% P 14% S 21% ITM 65% ROI -11</td></tr><tr  class="horse_6_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/18/2018 - 8,    AQU,    SY,    45,    F,    45,    (T)-1,    STK,    0,    96,    70,    109,    6,    6,    6,    7,    460.00,    6,    -1,    1,    -6,    1,    Alvarado Junior,    L,    117,    2.40,    Startwithsilver,    Bee Noteworthy,    Picco Uno,    2w,7p1/8,lite hand urg,    6,    </div></br> 
<div style="color:red" > 01/25/2018 - 8,    AQU,    FT,    45,    F,    45,    (T)-1,    AOC,    40000,    94,    34,    104,    5,    5,    7,    11,    920.00,    7,    4,    4,    -3,    1,    Alvarado Junior,    L,    119,    4.30,    Startwithsilver,    Andesine,    No Hayne No Gayne,    4-5w uppr, going away,    7,    </div></br> 
<div style="color:blue" > 12/07/2017 - 7,    AQU,    FM,    0,    F,    0,    (T)0,    SCR,    0,    99,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:red" > 10/19/2017 - 5,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    97,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 10/06/2017 - 6,    BEL,    FM,    102,    F,    102,    (T)35,    CLM,    35000,    93,    66,    85,    1,    5,    3,    3,    200.00,    3,    1,    3,    -1,    1,    Ortiz Jr Irad,    L,    120,    1.00,    Startwithsilver,    Bigkat and Camille,    A Dixie Twister,    6w upper, inched away,    6,    </div></br> 
<div style="color:red" > 09/08/2017 - 7,    BEL,    FM,    175,    F,    175,    (T)9,    AOC,    40000,    94,    90,    87,    9,    4,    4,    3,    250.00,    4,    4,    4,    5,    7,    Ortiz Jose L,    L,    120,    3.65,    Sister Sophia,    Barrier to Entry,    Vicki's Dancer,    3w on turn, tired,    9,    </div></br> 
<div style="color:blue" > 08/06/2017 - 5,    SAR,    FM,    21,    F,    21,    (T)18,    AOC,    40000,    92,    100,    92,    2,    3,    4,    4,    360.00,    4,    3,    2,    2,    2,    Ortiz Jr Irad,    L,    120,    2.85,    Fire Key,    Startwithsilver,    Sister Sophia,    INS,WAIT 1/4-UPR,ALT4P,    7,    </div></br> 
<div style="color:red" > 07/15/2017 - 4,    BEL,    FM,    128,    F,    128,    (T)0,    AOC,    40000,    91,    100,    92,    7,    1,    1,    -1,    -10.00,    1,    2,    3,    2,    3,    Ortiz Jose L,    L,    120,    2.25,    Lakeside Sunset,    Barrier to Entry,    Startwithsilver,    3w upper, ran on,    7,    </div></br> 
<div style="color:blue" > 06/16/2017 - 4,    BEL,    FM,    117,    F,    117,    (T)9,    AOC,    40000,    91,    62,    78,    8,    4,    2,    2,    50.00,    2,    1,    3,    5,    4,    Ortiz Jose L,    L,    120,    2.25,    Overnegotiate,    Little Bear Cat,    Eloweasel,    4w upper, wknd late,    9,    </div></br> 
<div style="color:red" > 05/21/2017 - 7,    BEL,    FM,    139,    F,    139,    (T)18,    AOC,    40000,    88,    105,    91,    2,    2,    2,    2,    -100.00,    1,    -1,    1,    1,    2,    Ortiz Jr Irad,    L,    120,    4.70,    Epping Forest,    Startwithsilver,    Paz the Bourbon,    2p turn,led 1/4-deep,    8,    </div></br> 
<div style="color:blue" > 09/10/2016 - 8,    BEL,    FM,    128,    F,    128,    (T)18,    AOC,    40000,    95,    82,    90,    6,    5,    5,    5,    510.00,    7,    5,    7,    5,    7,    Castellano Javier,    L,    116,    2.10,    Louisville First,    Uncle Southern,    First Charmer,    3w turn,5p 1/8,wknd,    8,    </div></br> 
<div style="color:red" > 08/19/2016 - 8,    SAR,    FM,    46,    F,    46,    (T)27,    AOC,    40000,    94,    72,    94,    9,    7,    5,    6,    750.00,    7,    6,    7,    2,    3,    Ortiz Jose L,    L,    119,    2.80,    Full Tap,    First Charmer,    Startwithsilver,    3w,ask 5/16,5p upper,    10,    </div></br> 
</td> </tr>
<tr  class="horse_6_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 109  ( date: 2018 ) 15   5   4   2   $273559.00    </br> 
  2017 : 6   1   2   1   $64575.00    </br> 
  2018 : 2   2   0   0   $95400.00    </br> 
  GP: 15   5   4   2   $273559.00    </br> 
  Turf: 11   2   4   2   $140209.00    </br> 
  Dist: 4   3   0   0   $133350.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="8" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> MY MISS CHIFF</td>
<td> Davis Dylan</td>
<td> Stall Jr Albert M</td>
<td> Steve Holliday</td>
<td> Town and Country Racing, LLC</td>
<td> Into Mischief</td>
<td> Carl's Frosty Girl</td>
<td> 10/1</td></tr><tr class="horse_7_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_7_8" style="display:none;"><td COLSPAN=9>
 Tr: St 33 W 15% P 6% S 15% ITM 36% ROI 5</td></tr><tr  class="horse_7_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/03/2018 - 8,    OP,    FT,    0,    F,    0,    (T)0,    SCR,    0,    102,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:red" > 02/10/2018 - 10,    DED,    SY,    40,    F,    40,    (T)-1,    STK,    0,    98,    111,    99,    5,    4,    2,    0,    10.00,    2,    -0,    1,    1,    2,    Murrill Mitchell,    L,    123,    0.90,    Ours to Run,    My Miss Chiff,    Shamrock Star,    vied inside,outkicked,    8,    </div></br> 
<div style="color:blue" > 12/09/2017 - 12,    FG,    GD,    70,    F,    70,    (T)-1,    STK,    0,    100,    93,    96,    4,    3,    3,    4,    560.00,    3,    2,    2,    -1,    1,    Murrill Mitchell,    L,    116,    1.20,    My Miss Chiff,    Look Into My Eyes,    Wheatfield,    3w turn,lead1/8,held,    10,    </div></br> 
<div style="color:red" > 11/18/2017 - 4,    FG,    FT,    70,    F,    70,    (T)-1,    STK,    0,    100,    81,    98,    7,    4,    4,    2,    100.00,    2,    -2,    1,    -1,    1,    Murrill Mitchell,    L,    116,    3.60,    My Miss Chiff,    Wheatfield,    Look Into My Eyes,    BRUSHED7/16,3-4W TURN,    9,    </div></br> 
<div style="color:blue" > 05/19/2017 - 9,    PIM,    FT,    5,    F,    5,    (T)-1,    STK,    0,    100,    125,    94,    2,    6,    1,    -0,    -10.00,    1,    2,    2,    4,    3,    Rosario Joel,    L,    116,    11.40,    Vertical Oak,    Our Majesty,    My Miss Chiff,    rail,dueled,faded,    13,    </div></br> 
<div style="color:red" > 04/09/2017 - 8,    KEE,    FT,    34,    F,    34,    (T)-1,    STK,    0,    90,    101,    73,    2,    7,    3,    2,    160.00,    5,    3,    4,    9,    5,    Murrill Mitchell,    L,    118,    3.30,    Sweet Loretta,    Sine Wave,    Laney,    came in st, faded late,    7,    </div></br> 
<div style="color:blue" > 03/05/2017 - 5,    FG,    FT,    85,    F,    85,    (T)-1,    AOC,    30000,    76,    98,    87,    7,    2,    2,    2,    250.00,    2,    -5,    1,    -8,    1,    Murrill Mitchell,    L,    119,    0.50,    My Miss Chiff,    Red La Rosa,    Nola Fashion,    2-3w turn,kept to task,    7,    </div></br> 
<div style="color:red" > 01/26/2017 - 5,    FG,    FT,    85,    F,    85,    (T)-1,    MSW,    0,    78,    58,    88,    4,    5,    3,    2,    150.00,    3,    -2,    1,    -6,    1,    Murrill Mitchell,    L,    120,    10.10,    My Miss Chiff,    Efforting,    House Cat,    3w turn,4w1/4,drw away,    9,    </div></br> 
</td> </tr>
<tr  class="horse_7_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 99  ( date: 2018 ) 7   4   1   1   $183660.00    </br> 
  2017 : 6   4   0   1   $159660.00    </br> 
  2018 : 1   0   1   0   $24000.00    </br> 
  GP: 7   4   1   1   $183660.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 5   4   0   1   $155160.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="8" race="8" title="Show More..." > 8</br> <span class="past-performance-plus-b" > </span> </td>
<td> SUMMER READING</td>
<td> Franco Manuel</td>
<td> Jerkens James A</td>
<td> Pollock Farms</td>
<td> Joseph V. Shields, Jr.</td>
<td> Hard Spun</td>
<td> Paradise Playgirl</td>
<td> 12/1</td></tr><tr class="horse_8_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_8_8" style="display:none;"><td COLSPAN=9>
 Tr: St 18 W 22% P 28% S 6% ITM 56% ROI 21</td></tr><tr  class="horse_8_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/23/2018 - 2,    AQU,    MY,    30,    F,    30,    (T)-1,    AOC,    80000,    97,    61,    92,    4,    1,    1,    -2,    -50.00,    1,    -0,    1,    0,    2,    Alvarado Junior,    L,    119,    1.40,    Alpine Sky,    Summer Reading,    Friend of Liberty,    dueled stretch, nailed,    5,    </div></br> 
<div style="color:red" > 09/09/2017 - 11,    LRL,    FT,    60,    F,    60,    (T)-1,    STK,    0,    102,    81,    87,    8,    2,    4,    5,    350.00,    4,    3,    2,    3,    5,    Russell Sheldon,    L,    118,    1.50,    Line of Best Fit,    Summer House,    Lake Ponchatrain,    4wd turn, weakened,    8,    </div></br> 
<div style="color:blue" > 07/24/2017 - 8,    SAR,    GD,    52,    F,    52,    (T)12,    STK,    0,    116,    112,    97,    3,    4,    6,    5,    420.00,    8,    5,    9,    8,    8,    Castellano Javier,    L,    119,    9.30,    Miss Ella,    Fair Point,    Carolina Shag,    6w upper, weakened,    9,    </div></br> 
<div style="color:red" > 06/18/2017 - 8,    MTH,    FT,    40,    F,    40,    (T)-1,    AOC,    50000,    96,    89,    100,    1,    4,    1,    -1,    -150.00,    1,    -3,    1,    -5,    1,    Castellano Javier,    L,    120,    0.60,    Summer Reading,    Fusaichi Red,    Sapphire Seas,    inside, ridden out,    6,    </div></br> 
<div style="color:blue" > 05/19/2017 - 13,    PIM,    SY,    5,    F,    5,    (T)-1,    STK,    0,    106,    81,    102,    2,    3,    5,    7,    300.00,    3,    2,    3,    2,    2,    Castellano Javier,    L,    118,    4.60,    Clipthecouponannie,    Summer Reading,    Sweet On Smokey,    RAIL,ALTERED 3WD 1/8,    7,    </div></br> 
<div style="color:red" > 04/30/2017 - 8,    BEL,    FM,    80,    F,    80,    (T)18,    STK,    0,    104,    101,    94,    2,    1,    2,    1,    100.00,    2,    2,    3,    2,    3,    Castellano Javier,    L,    119,    6.50,    Portmagee,    Miss Ella,    Summer Reading,    3w upper, kept on,    6,    </div></br> 
<div style="color:blue" > 03/18/2017 - 11,    GP,    FT,    30,    F,    30,    (T)-1,    STK,    0,    107,    107,    91,    2,    8,    4,    1,    10.00,    2,    4,    2,    5,    6,    Gaffalione Tyler,    L,    117,    18.80,    Distinta,    Wheatfield,    Dearest,    pace btwn 2w, wknd 1/8,    8,    </div></br> 
<div style="color:red" > 02/05/2017 - 8,    GP,    FT,    20,    F,    20,    (T)-1,    AOC,    62500,    99,    101,    97,    5,    1,    2,    0,    -10.00,    1,    -2,    1,    -1,    1,    Saez Luis,    L,    121,    3.60,    Summer Reading,    Savingtime,    Hola Charlotte,    vied,led,held,drvng,    9,    </div></br> 
<div style="color:blue" > 08/20/2016 - 8,    SAR,    FM,    46,    F,    46,    (T)12,    AOC,    62500,    103,    72,    81,    6,    8,    6,    4,    400.00,    6,    6,    8,    7,    8,    Velazquez John R,    L,    118,    5.00,    Ava's Kitten,    Sunnysammi,    Rumble Doll,    6w upper, no response,    8,    </div></br> 
<div style="color:red" > 07/10/2016 - 7,    BEL,    FT,    56,    F,    56,    (T)-1,    AOC,    62500,    103,    97,    84,    5,    3,    4,    3,    650.00,    7,    8,    5,    9,    3,    Saez Luis,    L,    118,    3.00,    Fusaichi Red,    Know It All Anna,    Summer Reading,    2p,btwn foe trn,6p 1/8,    7,    </div></br> 
<div style="color:blue" > 06/19/2016 - 8,    BEL,    FM,    92,    F,    92,    (T)9,    STK,    0,    100,    92,    90,    1,    6,    1,    -1,    -10.00,    1,    1,    2,    2,    6,    Alvarado Junior,    L,    118,    8.50,    Ancient Secret,    Thundering Sky,    Welcoming,    in hand ins, wknd late,    10,    </div></br> 
<div style="color:red" > 05/21/2016 - 10,    BEL,    FM,    104,    F,    104,    (T)9,    STK,    0,    103,    56,    94,    6,    4,    5,    3,    50.00,    2,    -2,    1,    1,    2,    Alvarado Junior,    L,    118,    5.80,    Lightstream,    Summer Reading,    Welcoming,    3w vs duo, resolute,    7,    </div></br> 
</td> </tr>
<tr  class="horse_8_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 102  ( date: 2017 ) 19   4   5   2   $237550.00    </br> 
  2017 : 7   2   1   1   $90050.00    </br> 
  2018 : 1   0   1   0   $14400.00    </br> 
  GP: 19   4   5   2   $237550.00    </br> 
  Turf: 8   1   2   1   $97380.00    </br> 
  Dist: 7   3   2   1   $122500.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="9" race="8" title="Show More..." > 9</br> <span class="past-performance-plus-b" > </span> </td>
<td> BLUEGRASS FLAG</td>
<td> Carmouche Kendrick</td>
<td> Morley Thomas</td>
<td> Susanne Hooper, James Hooper & Thom</td>
<td> Albrecht, Thomas C. and Fusaro, Vincent J.</td>
<td> Bluegrass Cat</td>
<td> No More Flags</td>
<td> 12/1</td></tr><tr class="horse_9_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_9_8" style="display:none;"><td COLSPAN=9>
 Tr: St 13 W 23% P 8% S 8% ITM 39% ROI -7</td></tr><tr  class="horse_9_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 05/29/2017 - 10,    BEL,    SY,    63,    F,    63,    (T)-1,    STK,    0,    94,    109,    72,    3,    6,    2,    1,    100.00,    2,    6,    5,    11,    6,    Carmouche Kendrick,    L,    122,    2.70,    Holiday Disguise,    Noble Freud,    Tiznow's Smile,    BUMPED ST, TIRED,    10,    </div></br> 
<div style="color:red" > 04/22/2017 - 8,    AQU,    FT,    16,    F,    16,    (T)-1,    STK,    0,    85,    88,    92,    6,    1,    1,    -2,    -100.00,    1,    -3,    1,    -6,    1,    Carmouche Kendrick,    L,    122,    3.85,    Bluegrass Flag,    Noble Freud,    Frosty Gal,    3w on turn,ask 3/16pl,    7,    </div></br> 
<div style="color:blue" > 03/25/2017 - 3,    AQU,    FT,    20,    F,    20,    (T)-1,    STK,    0,    86,    83,    94,    3,    1,    1,    -2,    -100.00,    1,    -3,    1,    -6,    1,    Carmouche Kendrick,    L,    117,    0.30,    Bluegrass Flag,    Heavenly Score,    Tiz Rae Anna,    3w half,rouse1/8-1/16p,    3,    </div></br> 
<div style="color:red" > 02/19/2017 - 9,    AQU,    FT,    26,    F,    26,    (T)-1,    STK,    0,    78,    124,    79,    2,    1,    1,    -1,    -100.00,    1,    1,    2,    2,    2,    Carmouche Kendrick,    L,    117,    3.95,    Wilburnmoney,    Bluegrass Flag,    Luna Rising,    cut corner, fought on,    10,    </div></br> 
<div style="color:blue" > 01/14/2017 - 2,    AQU,    FT,    24,    F,    24,    (T)-1,    MSW,    0,    80,    103,    85,    3,    6,    1,    -1,    -200.00,    1,    -1,    1,    -3,    1,    Carmouche Kendrick,    L,    120,    18.40,    Bluegrass Flag,    Easy Way Out,    My Girl Annie,    BUMPED ST, HELD WELL,    9,    </div></br> 
<div style="color:red" > 12/04/2016 - 6,    AQU,    FT,    40,    F,    40,    (T)-1,    MSW,    0,    82,    72,    71,    10,    9,    8,    5,    610.00,    8,    9,    6,    9,    7,    Carmouche Kendrick,    L,    120,    31.00,    Royal Inheritance,    Dublin Girl,    No Hayne No Gayne,    6w uppr, no response,    10,    </div></br> 
</td> </tr>
<tr  class="horse_9_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 94  ( date: 2017 ) 6   3   1   0   $173720.00    </br> 
  2016 : 5   3   1   0   $173500.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 6   3   1   0   $173720.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 5   3   1   0   $173220.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="10" race="8" title="Show More..." > 10</br> <span class="past-performance-plus-b" > </span> </td>
<td> ROWD E ALLIE</td>
<td> Davis Jacqueline A</td>
<td> Rideout Ronald</td>
<td> Two-Turn Farm LLC</td>
<td> Shindel, Peter</td>
<td> E Dubai</td>
<td> Pining</td>
<td> 50/1</td></tr><tr class="horse_10_8" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 14% P 14% S 14% ITM 42% ROI -33</td></tr><tr  class="horse_10_8" style="display:none;"><td COLSPAN=9>
 Tr: St 1 W 100% P 0% S 0% ITM 100% ROI 80</td></tr><tr  class="horse_10_8" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/20/2018 - 8,    LRL,    FT,    0,    F,    0,    (T)0,    SCR,    0,    98,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 07/16/2017 - 9,    MTH,    FT,    40,    F,    40,    (T)-1,    STK,    0,    103,    81,    84,    2,    5,    5,    7,    660.00,    5,    7,    5,    8,    4,    Pennington Frankie,    L,    117,    25.00,    Disco Chick,    Court Dancer,    Bustin Out,    failed to threaten,    5,    </div></br> 
<div style="color:blue" > 05/28/2017 - 8,    MTH,    FM,    0,    F,    0,    (T)0,    STK,    0,    104,    27,    70,    1,    8,    11,    11,    1130.00,    11,    8,    11,    11,    11,    Juarez Nik,    L,    121,    37.10,    Blue Bahia,    Spectacular Me,    Daylight Ahead,    outrun,    11,    </div></br> 
<div style="color:red" > 04/29/2017 - 7,    PRX,    FT,    24,    F,    24,    (T)-1,    STK,    0,    94,    58,    80,    6,    1,    2,    0,    150.00,    2,    3,    4,    7,    4,    Ocasio Luis M,    L,    119,    6.70,    Discreet Senorita,    Heatherly,    Disco Chick,    chased, weakened,    8,    </div></br> 
<div style="color:blue" > 03/26/2017 - 2,    LRL,    FT,    55,    F,    55,    (T)-1,    AOC,    32000,    94,    87,    101,    4,    2,    2,    1,    100.00,    2,    1,    2,    -2,    1,    Lynch Feargal,    L,    120,    2.40,    Rowd E Allie,    Giddy Up,    My Magician,    4wd outs foe,drv clr,    5,    </div></br> 
<div style="color:red" > 02/18/2017 - 9,    LRL,    FT,    60,    F,    60,    (T)-1,    STK,    0,    109,    105,    74,    2,    3,    2,    1,    -50.00,    1,    6,    5,    14,    8,    Lynch Feargal,    L,    118,    85.50,    High Ridge Road,    By the Moon,    Clothes Fall Off,    RAIL, EASED LATE,    8,    </div></br> 
<div style="color:blue" > 12/11/2016 - 8,    PRX,    FT,    40,    F,    40,    (T)-1,    AOC,    25000,    95,    108,    94,    1,    5,    1,    -1,    -100.00,    1,    -5,    1,    -3,    1,    Ocasio Luis M,    L,    119,    9.90,    Rowd E Allie,    Flatter Me Zanja,    Let's Parlay,    kept to her task,    10,    </div></br> 
<div style="color:red" > 11/15/2016 - 8,    PRX,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    87,    97,    83,    11,    1,    1,    -1,    -100.00,    1,    -2,    1,    -1,    1,    Pennington Frankie,    L,    121,    5.80,    Rowd E Allie,    Polite Pearl,    Like a Lion,    4 p, strong handling,    11,    </div></br> 
<div style="color:blue" > 10/09/2016 - 8,    PRX,    MY,    40,    F,    40,    (T)-1,    STR,    25000,    90,    80,    86,    8,    1,    1,    -2,    -350.00,    1,    -3,    1,    -0,    1,    Perez Irvin O,    L,    116,    10.80,    Rowd E Allie,    R Bling Shines,    Brazilian Symphony,    DRIFT OUT, HELD ON,    8,    </div></br> 
<div style="color:red" > 09/19/2016 - 7,    PRX,    SY,    40,    F,    40,    (T)-1,    STR,    25000,    88,    105,    76,    2,    3,    1,    -1,    -300.00,    1,    -1,    1,    4,    4,    Perez Irvin O,    L,    114,    12.00,    Polite Pearl,    Addibel Lightning,    Hows My Gold,    clear pace, weakened,    7,    </div></br> 
<div style="color:blue" > 07/21/2016 - 6,    PEN,    FT,    56,    F,    56,    (T)-1,    ALW,    0,    87,    91,    58,    6,    4,    3,    1,    100.00,    2,    7,    5,    13,    6,    Pennington Frankie,    L,    119,    4.00,    Headed for Heaven,    Without Thought,    Our Carly,    TAKEN UP 1/4, FLATTNED,    7,    </div></br> 
<div style="color:red" > 06/14/2016 - 9,    PRX,    FT,    40,    F,    40,    (T)-1,    ALW,    0,    89,    55,    33,    6,    6,    4,    3,    900.00,    8,    25,    8,    27,    8,    Pennington Frankie,    L,    121,    1.90,    Your Pace Or Mine,    A Wing and a Song,    Fancy Malka,    close up, stopped,    8,    </div></br> 
</td> </tr>
<tr  class="horse_10_8" style="display:none;"><td COLSPAN=9>
LifeTime Record 101  ( date: 2017 ) 23   7   2   1   $287603.00    </br> 
  2017 : 5   1   0   0   $36990.00    </br> 
  2018 : 0   0   0   0   $0.00    </br> 
  GP: 1   0   0   0   $3000.00    </br> 
  Turf: 1   0   0   0   $1200.00    </br> 
  Dist: 20   7   2   1   $280403.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
     <div class="table-responsive" race="9" race_date="20180317" claimamt="25000" dist_disp="6F" todays_cls="69" purse="31000"> </br>            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <span > 03/17/2018 </span > * 
<span>Delta Downs</span> * 
<span> USA</span>
    <hr><br><span> Race #9</span> - 
<span> Claiming $25000</span> - 
<span> Distance 6F</span> - 
<span> Todays CLS 69</span> -
<span> Purse $31000</span> 
    <br><span>Exacta, Trifecta (.50), Super (.10) Wagers</span>
 </br> <span class="race-tables past-performance-plus" title="Click here for more information"> </span>                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <tr><td ROWSPAN="1" class="horse_data" program="1" race="9" title="Show More..." > 1</br> <span class="past-performance-plus-b" > </span> </td>
<td> BAREFOOT ANGEL</td>
<td> Davis Dylan</td>
<td> Baker Charlton</td>
<td> Saul Kupferberg & Max Kupferberg</td>
<td> Saul J. Kupferberg</td>
<td> Pomeroy</td>
<td> Shoeless Angel</td>
<td> 7/2</td></tr><tr class="horse_1_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 24% P 17% S 19% ITM 60% ROI 7</td></tr><tr  class="horse_1_9" style="display:none;"><td COLSPAN=9>
 Tr: St 18 W 11% P 17% S 11% ITM 39% ROI -10</td></tr><tr  class="horse_1_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 1,    AQU,    MY,    40,    F,    40,    (T)-1,    MCL,    25000,    68,    91,    71,    6,    3,    2,    1,    200.00,    2,    3,    2,    1,    2,    Navarro Joshua,    L,    121,    3.00,    Daring Destiny,    Barefoot Angel,    Our American Star,    2w,4w1/8,sum late gain,    6,    </div></br> 
<div style="color:red" > 02/03/2018 - 9,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    25000,    72,    89,    61,    8,    6,    4,    2,    210.00,    5,    5,    4,    3,    3,    Bravo Joe,    L,    121,    6.20,    Musicality,    Daring Destiny,    Barefoot Angel,    3-4W,FLOATED 8W 1/4PL,    10,    </div></br> 
<div style="color:blue" > 01/27/2018 - 4,    AQU,    FT,    58,    F,    58,    (T)-1,    MCL,    25000,    67,    83,    51,    6,    1,    2,    0,    10.00,    2,    1,    2,    5,    4,    Reyes Luis R,    L,    116,    1.15,    Shipsandgoods,    Port Arch,    Our American Star,    dueled 3-2w, wknd late,    6,    </div></br> 
<div style="color:red" > 01/13/2018 - 4,    AQU,    MY,    36,    F,    36,    (T)-1,    MCL,    25000,    67,    104,    68,    4,    5,    4,    2,    150.00,    3,    -1,    1,    2,    3,    Hernandez Rafael M,    L,    121,    1.20,    Heat Check,    Tammany Giant,    Barefoot Angel,    4w uppr,led, fought on,    8,    </div></br> 
<div style="color:blue" > 12/23/2017 - 2,    AQU,    SY,    22,    F,    22,    (T)-1,    MCL,    25000,    83,    98,    57,    10,    5,    1,    -1,    -50.00,    1,    2,    3,    9,    6,    Franco Manuel,    L,    122,    3.35,    Frozen Angel,    Pence,    Shipsandgoods,    3w upper, wknd late,    10,    </div></br> 
<div style="color:red" > 12/03/2017 - 4,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    79,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 10/19/2017 - 9,    BEL,    FT,    54,    F,    54,    (T)-1,    MCL,    25000,    71,    106,    67,    3,    3,    1,    -1,    50.00,    2,    1,    2,    2,    3,    Franco Manuel,    121,    3.05,    O Captainmycaptain,    Giant Rocks,    Barefoot Angel,    dueled ins, wknd late,    9,    </div></br> 
<div style="color:red" > 10/12/2017 - 6,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    82,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 11/29/2016 - 2,    FL,    GD,    56,    F,    56,    (T)-1,    MSW,    0,    62,    84,    62,    1,    4,    3,    1,    160.00,    3,    -1,    1,    1,    3,    Hernandez Harry,    120,    5.50,    Greenway Stroll,    Meetme At d'Street,    Barefoot Angel,    willingly btwn wire,    9,    </div></br> 
</td> </tr>
<tr  class="horse_1_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 71  ( date: 2018 ) 7   0   2   3   $22274.00    </br> 
  2017 : 2   0   0   1   $3324.00    </br> 
  2018 : 4   0   2   1   $17050.00    </br> 
  GP: 6   0   2   2   $20374.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 6   0   2   3   $20724.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="2" race="9" title="Show More..." > 2</br> <span class="past-performance-plus-b" > </span> </td>
<td> PORT ARCH</td>
<td> Franco Manuel</td>
<td> Hennig Mark A</td>
<td> Stonegate Stables LLC & John P. Hic</td>
<td> Alan Brodsky</td>
<td> Archarcharch</td>
<td> Portera</td>
<td> 2/1</td></tr><tr class="horse_2_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 86 W 27% P 22% S 13% ITM 62% ROI -8</td></tr><tr  class="horse_2_9" style="display:none;"><td COLSPAN=9>
 Tr: St 12 W 0% P 8% S 25% ITM 33% ROI -100</td></tr><tr  class="horse_2_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 01/27/2018 - 4,    AQU,    FT,    58,    F,    58,    (T)-1,    MCL,    25000,    67,    33,    60,    2,    3,    3,    8,    610.00,    3,    3,    3,    1,    2,    Davis Dylan,    L,    121,    1.40,    Shipsandgoods,    Port Arch,    Our American Star,    PAUSED, ALTERED 1/8P,    6,    </div></br> 
<div style="color:red" > 12/29/2017 - 8,    AQU,    FT,    12,    F,    12,    (T)-1,    MCL,    25000,    79,    45,    65,    8,    6,    5,    4,    610.00,    6,    4,    5,    2,    2,    Cancel Eric,    L,    122,    7.10,    Alexander David,    Port Arch,    Big Auk,    STMBLE BRK,4W,6P 1/8PL,    9,    </div></br> 
<div style="color:blue" > 12/23/2017 - 2,    AQU,    SY,    0,    F,    0,    (T)0,    SCR,    0,    83,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Also-Eligible,    0,    </div></br> 
<div style="color:red" > 12/06/2017 - 2,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Off-Turf,    0,    </div></br> 
<div style="color:blue" > 11/10/2017 - 9,    AQU,    FM,    110,    F,    110,    (T)9,    MCL,    40000,    78,    86,    66,    2,    2,    3,    2,    160.00,    3,    7,    6,    7,    6,    Cancel Eric,    L,    121,    26.75,    Wondermeister,    Mr. Massena,    Run for Boston,    rail vs duo, took back,    10,    </div></br> 
</td> </tr>
<tr  class="horse_2_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 66  ( date: 2017 ) 3   0   2   0   $12564.00    </br> 
  2017 : 2   0   1   0   $6364.00    </br> 
  2018 : 1   0   1   0   $6200.00    </br> 
  GP: 3   0   2   0   $12564.00    </br> 
  Turf: 1   0   0   0   $164.00    </br> 
  Dist: 1   0   1   0   $6200.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="3" race="9" title="Show More..." > 3</br> <span class="past-performance-plus-b" > </span> </td>
<td> ED'S QUICK CAT</td>
<td> Simpson Trevor</td>
<td> Pringle Edmund</td>
<td> Edward Cohn</td>
<td> Madeline Cohn</td>
<td> Relato Del Gato</td>
<td> Magiclake</td>
<td> 15/1</td></tr><tr class="horse_3_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 9 W 11% P 11% S 11% ITM 33% ROI 41</td></tr><tr  class="horse_3_9" style="display:none;"><td COLSPAN=9>
 Tr: St 6 W 0% P 17% S 0% ITM 17% ROI -100</td></tr><tr  class="horse_3_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/04/2018 - 9,    AQU,    FT,    54,    F,    54,    (T)-1,    MCL,    20000,    77,    99,    51,    5,    1,    1,    -2,    -50.00,    1,    4,    6,    11,    6,    Kay Winston R,    L,    115,    11.70,    Love Your Buttons,    Dublin Leprechaun,    Arthur Avenue,    BROKE IN ST, FALTERED,    9,    </div></br> 
<div style="color:red" > 01/27/2018 - 4,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    67,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 01/13/2018 - 4,    AQU,    MY,    36,    F,    36,    (T)-1,    MCL,    25000,    67,    117,    57,    1,    6,    1,    -1,    -50.00,    1,    1,    3,    7,    6,    Kay Winston R,    L,    114,    9.60,    Heat Check,    Tammany Giant,    Barefoot Angel,    BRUSH GATE, WEAKENED,    8,    </div></br> 
<div style="color:red" > 12/23/2017 - 2,    AQU,    SY,    0,    F,    0,    (T)0,    SCR,    0,    83,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 03/09/2017 - 3,    FG,    FM,    30,    F,    30,    (T)20,    MCL,    25000,    82,    131,    59,    1,    1,    1,    -1,    50.00,    2,    6,    6,    10,    7,    Pedroza Marcelino,    L,    123,    7.80,    Tough Equation,    Red Fern,    One Kind of Crazy,    BORE OUT 7W TURN,4W1/4,    11,    </div></br> 
<div style="color:red" > 01/05/2017 - 4,    FG,    GD,    35,    F,    35,    (T)10,    MSW,    0,    83,    96,    58,    4,    6,    1,    -0,    160.00,    3,    5,    4,    10,    5,    Kay Winston R,    L,    118,    7.70,    Pontifical,    One Kind of Crazy,    Fine Cat,    LUG OUT 3/8,4-5P,WKND,    6,    </div></br> 
</td> </tr>
<tr  class="horse_3_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 59  ( date: 2017 ) 4   0   0   0   $1547.00    </br> 
  2017 : 2   0   0   0   $1140.00    </br> 
  2018 : 2   0   0   0   $407.00    </br> 
  GP: 2   0   0   0   $407.00    </br> 
  Turf: 2   0   0   0   $1140.00    </br> 
  Dist: 1   0   0   0   $207.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="4" race="9" title="Show More..." > 4</br> <span class="past-performance-plus-b" > </span> </td>
<td> JOLLY</td>
<td> Richards Gary</td>
<td> Contessa Gary C</td>
<td> Windhorse Thoroughbreds, LLC</td>
<td> Windhorse Thoroughbreds LLC</td>
<td> Courageous Cat</td>
<td> Hollybygolly</td>
<td> 30/1</td></tr><tr class="horse_4_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 1 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_4_9" style="display:none;"><td COLSPAN=9>
 Tr: St 28 W 0% P 25% S 29% ITM 54% ROI -100</td></tr><tr  class="horse_4_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/25/2018 - 5,    AQU,    SY,    50,    F,    50,    (T)-1,    MCL,    40000,    85,    67,    1,    3,    5,    4,    3,    1600.00,    7,    28,    8,    41,    8,    Navarro Joshua,    L,    121,    33.75,    Dooley,    Our American Star,    Arthur Avenue,    vie to 5/8p, folded,    8,    </div></br> 
<div style="color:red" > 09/01/2017 - 11,    SAR,    FM,    90,    F,    90,    (T)9,    MCL,    40000,    82,    81,    9,    6,    9,    6,    5,    550.00,    6,    34,    12,    61,    12,    Santana Jr Ricardo,    L,    118,    68.50,    Follow the Signs,    Artic Storm Cat,    Morning Daddy,    FALTERED,EASD,WLKD OFF,    12,    </div></br> 
</td> </tr>
<tr  class="horse_4_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 9  ( date: 2017 ) 2   0   0   0   $398.00    </br> 
  2017 : 1   0   0   0   $126.00    </br> 
  2018 : 1   0   0   0   $272.00    </br> 
  GP: 1   0   0   0   $272.00    </br> 
  Turf: 1   0   0   0   $126.00    </br> 
  Dist: 0   0   0   0   $0.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="5" race="9" title="Show More..." > 5</br> <span class="past-performance-plus-b" > </span> </td>
<td> LUZINSKI</td>
<td> Mendez Kevin</td>
<td> Matties Gregg M</td>
<td> Matties Racing LLC</td>
<td> Matties Racing Stable LLC</td>
<td> Flashy Bull</td>
<td> Lady Louise</td>
<td> 10/1</td></tr><tr class="horse_5_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 9 W 0% P 11% S 0% ITM 11% ROI -100</td></tr><tr  class="horse_5_9" style="display:none;"><td COLSPAN=9>
 Tr: St 1 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_5_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 12/23/2017 - 2,    AQU,    SY,    22,    F,    22,    (T)-1,    MCL,    25000,    83,    41,    52,    7,    8,    8,    9,    1260.00,    9,    11,    8,    11,    8,    Rocco Jr Joseph,    L,    122,    19.20,    Frozen Angel,    Pence,    Shipsandgoods,    4w upper, no impact,    10,    </div></br> 
<div style="color:red" > 12/03/2017 - 4,    AQU,    FT,    44,    F,    44,    (T)-1,    MCL,    25000,    79,    40,    53,    1,    6,    6,    3,    350.00,    6,    4,    6,    5,    5,    Davis Dylan,    L,    122,    7.40,    Cavallotto,    Catapult Jack,    Pence,    coaxed st, outkicked,    9,    </div></br> 
<div style="color:blue" > 11/22/2017 - 9,    AQU,    SY,    54,    F,    54,    (T)-1,    MCL,    40000,    84,    50,    54,    3,    1,    6,    5,    550.00,    5,    8,    4,    13,    5,    Davis Dylan,    L,    121,    13.90,    Quai Voltaire,    Givetheman a Cigar,    Run for Boston,    2w top turn, no bids,    8,    </div></br> 
<div style="color:red" > 04/20/2017 - 5,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    25000,    70,    97,    63,    1,    6,    1,    -0,    610.00,    3,    14,    4,    19,    5,    Davis Dylan,    L,    117,    8.60,    Cournoyer,    Mo Focused,    Nut Nut,    dueled inside, tired,    8,    </div></br> 
<div style="color:blue" > 03/31/2017 - 9,    AQU,    SY,    12,    F,    12,    (T)-1,    MCL,    25000,    68,    82,    58,    7,    10,    7,    4,    150.00,    4,    7,    4,    11,    3,    Davis Dylan,    L,    120,    4.60,    Cespedes,    Mission Trip,    Luzinski,    parked 5w turn,outkick,    10,    </div></br> 
<div style="color:red" > 03/11/2017 - 6,    AQU,    FT,    24,    F,    24,    (T)-1,    MSW,    0,    93,    62,    70,    3,    5,    4,    5,    600.00,    6,    10,    6,    12,    8,    Davis Jacqueline A,    L,    120,    50.75,    Wrong Ben,    Broken Engagement,    Sicilia Mike,    rode rail, roused 5/16,    11,    </div></br> 
<div style="color:blue" > 02/18/2017 - 4,    AQU,    FT,    24,    F,    24,    (T)-1,    MSW,    0,    89,    11,    64,    8,    6,    9,    12,    1500.00,    9,    19,    8,    17,    8,    Davis Jacqueline A,    L,    120,    69.25,    Long Haul Bay,    Horoscope,    Exclusive Zip,    6w upper, no impact,    10,    </div></br> 
</td> </tr>
<tr  class="horse_5_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 70  ( date: 2017 ) 7   0   0   1   $6718.00    </br> 
  2016 : 7   0   0   1   $6718.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 7   0   0   1   $6718.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 5   0   0   1   $4558.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="6" race="9" title="Show More..." > 6</br> <span class="past-performance-plus-b" > </span> </td>
<td> PIMM'S CUP</td>
<td> Diaz Jr Hector Rafael</td>
<td> Cabrera Joseph A</td>
<td> Dr. Jerry Bilinski</td>
<td> Cabrera, Joseph, A.</td>
<td> Old Fashioned</td>
<td> Lake Princess</td>
<td> 20/1</td></tr><tr class="horse_6_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 41 W 5% P 17% S 12% ITM 34% ROI -81</td></tr><tr  class="horse_6_9" style="display:none;"><td COLSPAN=9>
 Tr: St 5 W 0% P 0% S 0% ITM 0% ROI -100</td></tr><tr  class="horse_6_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/25/2018 - 5,    AQU,    SY,    50,    F,    50,    (T)-1,    MCL,    40000,    85,    72,    16,    1,    1,    2,    1,    700.00,    5,    18,    6,    31,    6,    Diaz Jr Hector R,    L,    116,    51.25,    Dooley,    Our American Star,    Arthur Avenue,    vie 2w,pull right,tird,    8,    </div></br> 
<div style="color:red" > 02/17/2018 - 1,    AQU,    MY,    40,    F,    40,    (T)-1,    MCL,    25000,    68,    84,    46,    3,    1,    3,    2,    300.00,    3,    7,    3,    13,    5,    Reyes Luis R,    L,    121,    34.00,    Daring Destiny,    Barefoot Angel,    Our American Star,    3w 3/8,alter rail 1/8,    6,    </div></br> 
<div style="color:blue" > 02/03/2018 - 9,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    25000,    72,    76,    53,    3,    5,    6,    4,    160.00,    4,    4,    3,    7,    4,    Davis Dylan,    L,    121,    26.25,    Musicality,    Daring Destiny,    Barefoot Angel,    ins half,alt upr & 1/8,    10,    </div></br> 
<div style="color:red" > 01/27/2018 - 4,    AQU,    FT,    58,    F,    58,    (T)-1,    MCL,    25000,    67,    84,    40,    5,    2,    1,    -0,    -10.00,    1,    4,    5,    11,    6,    Gomez Oscar,    L,    121,    36.00,    Shipsandgoods,    Port Arch,    Our American Star,    gap dueled 2p, falter,    6,    </div></br> 
<div style="color:blue" > 01/13/2018 - 4,    AQU,    MY,    36,    F,    36,    (T)-1,    MCL,    25000,    67,    114,    54,    3,    1,    2,    1,    50.00,    2,    2,    4,    9,    8,    Reyes Luis R,    L,    116,    12.00,    Heat Check,    Tammany Giant,    Barefoot Angel,    pressed 3-2w, weakened,    8,    </div></br> 
<div style="color:red" > 12/03/2017 - 4,    AQU,    FT,    44,    F,    44,    (T)-1,    MCL,    25000,    79,    60,    51,    2,    4,    1,    -1,    50.00,    2,    1,    3,    6,    6,    Reyes Luis R,    L,    115,    27.25,    Cavallotto,    Catapult Jack,    Pence,    cut pace 2p, wknd late,    9,    </div></br> 
<div style="color:blue" > 11/11/2017 - 9,    AQU,    FT,    16,    F,    16,    (T)-1,    MCL,    25000,    77,    75,    15,    11,    6,    6,    4,    920.00,    10,    18,    12,    32,    12,    Ortiz Jose L,    L,    122,    10.40,    Mydadfloyd,    Catapult Jack,    The I Man,    chased 5-4w, faltered,    12,    </div></br> 
<div style="color:red" > 06/14/2017 - 4,    BEL,    GD,    74,    F,    74,    (T)-1,    MCL,    25000,    71,    84,    38,    3,    3,    4,    2,    250.00,    2,    7,    2,    15,    5,    Davis Dylan,    L,    118,    15.40,    Shadow Surprise,    Nut Nut,    Skeet Shot,    chased inside, tired,    9,    </div></br> 
<div style="color:blue" > 06/04/2017 - 9,    BEL,    FT,    58,    F,    58,    (T)-1,    MCL,    25000,    73,    93,    60,    5,    4,    1,    -1,    -50.00,    1,    4,    3,    9,    3,    Davis Dylan,    L,    118,    10.70,    Rossie Val,    Martino,    Pimm's Cup,    SHIED IN ST, WKND LATE,    10,    </div></br> 
<div style="color:red" > 05/20/2017 - 1,    BEL,    FT,    67,    F,    67,    (T)-1,    MCL,    25000,    67,    90,    49,    2,    7,    6,    3,    450.00,    4,    8,    7,    10,    8,    Davis Dylan,    L,    117,    5.20,    Til We Meet Again,    Nut Nut,    Shadow Surprise,    BUMPED ST, TIRED,    10,    </div></br> 
<div style="color:blue" > 04/30/2017 - 2,    BEL,    FT,    70,    F,    70,    (T)-1,    MCL,    25000,    70,    103,    56,    3,    3,    1,    -1,    -50.00,    1,    1,    2,    4,    2,    Davis Dylan,    L,    117,    3.90,    Mission Trip,    Pimm's Cup,    Martino,    under pressure, ran on,    7,    </div></br> 
<div style="color:red" > 08/18/2016 - 6,    SAR,    FT,    40,    F,    40,    (T)-1,    MSW,    0,    88,    55,    41,    2,    8,    7,    3,    660.00,    6,    14,    7,    22,    9,    Davis Dylan,    119,    71.00,    Mirai,    Ethan Hunt,    Dab,    DUCK IN SHARPLY BRK,2P,    10,    </div></br> 
</td> </tr>
<tr  class="horse_6_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 60  ( date: 2017 ) 12   0   1   1   $14801.00    </br> 
  2017 : 6   0   1   1   $10929.00    </br> 
  2018 : 5   0   0   0   $3580.00    </br> 
  GP: 4   0   0   0   $3374.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 10   0   1   1   $13907.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="7" race="9" title="Show More..." > 7</br> <span class="past-performance-plus-b" > </span> </td>
<td> TAMMANY GIANT</td>
<td> Carmouche Kendrick</td>
<td> Toscano Jr John T</td>
<td> Sunset Stables, LLC & Charles E. Fe</td>
<td> Toscano, Jr., John, T. and Festa, Jr., Charle</td>
<td> Frost Giant</td>
<td> Seattle Ash</td>
<td> 8/1</td></tr><tr class="horse_7_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 75 W 15% P 15% S 13% ITM 43% ROI -33</td></tr><tr  class="horse_7_9" style="display:none;"><td COLSPAN=9>
 Tr: St 24 W 8% P 21% S 21% ITM 50% ROI -41</td></tr><tr  class="horse_7_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/03/2018 - 9,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    25000,    72,    72,    38,    9,    7,    7,    5,    360.00,    6,    9,    8,    14,    8,    Carmouche Kendrick,    121,    7.20,    Musicality,    Daring Destiny,    Barefoot Angel,    2w turn, 5p 1/8,empty,    10,    </div></br> 
<div style="color:red" > 01/26/2018 - 4,    AQU,    FT,    50,    F,    50,    (T)-1,    MCL,    40000,    85,    81,    48,    7,    4,    3,    2,    810.00,    6,    14,    6,    23,    7,    Carmouche Kendrick,    121,    2.25,    St. Patrick Frost,    Dooley,    Catch a Cab,    4w on turn, tired,    7,    </div></br> 
<div style="color:blue" > 01/13/2018 - 4,    AQU,    MY,    36,    F,    36,    (T)-1,    MCL,    25000,    67,    97,    69,    5,    2,    5,    3,    210.00,    5,    6,    7,    2,    2,    Carmouche Kendrick,    121,    2.70,    Heat Check,    Tammany Giant,    Barefoot Angel,    chased 4-5w, ran on,    8,    </div></br> 
<div style="color:red" > 12/08/2017 - 2,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    40000,    76,    54,    74,    3,    2,    4,    5,    250.00,    4,    2,    4,    2,    3,    Montanez Rosario,    122,    29.00,    I Miss My Father,    Croatia,    Tammany Giant,    5w upper, hung,    5,    </div></br> 
<div style="color:blue" > 11/08/2017 - 1,    AQU,    GD,    12,    F,    12,    (T)-1,    MCL,    40000,    82,    16,    57,    3,    4,    6,    5,    900.00,    7,    9,    7,    11,    7,    Montanez Rosario,    122,    45.75,    Light the Vow,    Givetheman a Cigar,    Quai Voltaire,    KEEN,STDIED 2X,2W,    7,    </div></br> 
<div style="color:red" > 03/24/2017 - 3,    AQU,    FT,    22,    F,    22,    (T)-1,    MSW,    0,    90,    31,    53,    3,    5,    6,    6,    1050.00,    6,    17,    6,    23,    6,    Montanez Rosario,    117,    45.25,    Horoscope,    Altesino,    Sicilia Mike,    inside journey,trailed,    6,    </div></br> 
</td> </tr>
<tr  class="horse_7_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 74  ( date: 2017 ) 6   0   1   1   $12344.00    </br> 
  2017 : 3   0   0   1   $5610.00    </br> 
  2018 : 3   0   1   0   $6734.00    </br> 
  GP: 6   0   1   1   $12344.00    </br> 
  Turf: 0   0   0   0   $0.00    </br> 
  Dist: 5   0   1   1   $11934.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="8" race="9" title="Show More..." > 8</br> <span class="past-performance-plus-b" > </span> </td>
<td> CROATIA</td>
<td> Rocco Jr Joseph</td>
<td> Ryerson James T</td>
<td> Wellspring Stables, LLC</td>
<td> WellSpring Stables</td>
<td> Congrats</td>
<td> Treasure Always</td>
<td> 4/1</td></tr><tr class="horse_8_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 35 W 6% P 14% S 6% ITM 26% ROI -28</td></tr><tr  class="horse_8_9" style="display:none;"><td COLSPAN=9>
 Tr: St 12 W 0% P 17% S 0% ITM 17% ROI -100</td></tr><tr  class="horse_8_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 12/08/2017 - 2,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    40000,    76,    84,    77,    4,    3,    2,    1,    100.00,    2,    0,    2,    0,    2,    Lopez Paco,    L,    122,    5.80,    I Miss My Father,    Croatia,    Tammany Giant,    3w upper, repelled,    5,    </div></br> 
<div style="color:red" > 11/09/2017 - 4,    AQU,    GD,    79,    F,    79,    (T)12,    MSW,    0,    89,    38,    69,    5,    8,    9,    8,    960.00,    9,    10,    8,    11,    8,    Carmouche Kendrick,    L,    122,    47.75,    Louisiana Lady,    Ferrad's Party,    Valiant Man,    2w & btwn turn,6p 1/8,    9,    </div></br> 
<div style="color:blue" > 10/06/2017 - 7,    BEL,    FT,    54,    F,    54,    (T)-1,    MSW,    0,    87,    57,    45,    3,    4,    3,    2,    510.00,    4,    11,    4,    16,    4,    Carmouche Kendrick,    L,    121,    13.20,    Side Tracks,    Frozen Angel,    Dwizard,    3w,chase duel, empty,    5,    </div></br> 
</td> </tr>
<tr  class="horse_8_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 77  ( date: 2017 ) 3   0   1   0   $11475.00    </br> 
  2016 : 3   0   1   0   $11475.00    </br> 
  2017 : 0   0   0   0   $0.00    </br> 
  GP: 3   0   1   0   $11475.00    </br> 
  Turf: 1   0   0   0   $275.00    </br> 
  Dist: 2   0   1   0   $11200.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="9" race="9" title="Show More..." > 9</br> <span class="past-performance-plus-b" > </span> </td>
<td> BIG AUK</td>
<td> Reyes Luis R</td>
<td> Barker Edward R</td>
<td> Stonegate Stables</td>
<td> Chen, Danny J. and Viverito, Thomas</td>
<td> Gemologist</td>
<td> Uamh Binn</td>
<td> 12/1</td></tr><tr class="horse_9_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 23 W 9% P 9% S 0% ITM 18% ROI -36</td></tr><tr  class="horse_9_9" style="display:none;"><td COLSPAN=9>
 Tr: St 12 W 25% P 17% S 17% ITM 59% ROI 65</td></tr><tr  class="horse_9_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 02/17/2018 - 1,    AQU,    MY,    40,    F,    40,    (T)-1,    MCL,    25000,    68,    71,    50,    5,    4,    4,    4,    600.00,    5,    9,    5,    11,    4,    DeCarlo Christopher P,    L,    121,    10.20,    Daring Destiny,    Barefoot Angel,    Our American Star,    3-4w turn, no impact,    6,    </div></br> 
<div style="color:red" > 02/03/2018 - 9,    AQU,    FT,    40,    F,    40,    (T)-1,    MCL,    25000,    72,    46,    52,    2,    3,    9,    9,    460.00,    8,    6,    5,    8,    5,    DeCarlo Christopher P,    L,    121,    10.00,    Musicality,    Daring Destiny,    Barefoot Angel,    ins half,to 4p 1/4-1/8,    10,    </div></br> 
<div style="color:blue" > 01/26/2018 - 4,    AQU,    FT,    50,    F,    50,    (T)-1,    MCL,    40000,    85,    80,    67,    5,    2,    4,    2,    560.00,    4,    9,    4,    12,    4,    Navarro Joshua,    L,    121,    7.90,    St. Patrick Frost,    Dooley,    Catch a Cab,    in range inside,tired,    7,    </div></br> 
<div style="color:red" > 01/15/2018 - 2,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 12/29/2017 - 8,    AQU,    FT,    12,    F,    12,    (T)-1,    MCL,    25000,    79,    59,    64,    2,    1,    3,    2,    -50.00,    1,    1,    2,    2,    3,    Navarro Joshua,    L,    122,    10.30,    Alexander David,    Port Arch,    Big Auk,    near ins half,weakened,    9,    </div></br> 
<div style="color:red" > 12/23/2017 - 2,    AQU,    SY,    0,    F,    0,    (T)0,    SCR,    0,    83,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Trainer,    0,    </div></br> 
<div style="color:blue" > 12/15/2017 - 2,    AQU,    FT,    20,    F,    20,    (T)-1,    MCL,    20000,    72,    60,    70,    2,    3,    8,    6,    360.00,    6,    3,    3,    4,    2,    Navarro Joshua,    L,    121,    74.50,    Missle Bomb,    Big Auk,    Shoot the Gap,    2w 1/4p, ran on,    9,    </div></br> 
<div style="color:red" > 12/07/2017 - 3,    AQU,    FT,    56,    F,    56,    (T)-1,    MCL,    20000,    84,    42,    30,    6,    5,    6,    7,    1710.00,    6,    25,    6,    36,    6,    Lezcano Abel,    L,    121,    33.50,    Polar City,    Alpha Team,    Champagne Papi,    CLIPPED ST, NO IMPACT,    7,    </div></br> 
<div style="color:blue" > 12/06/2017 - 2,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Stewards,    0,    </div></br> 
<div style="color:red" > 11/11/2017 - 9,    AQU,    FT,    16,    F,    16,    (T)-1,    MCL,    25000,    77,    38,    59,    4,    8,    10,    9,    770.00,    9,    9,    6,    11,    5,    Lezcano Abel,    L,    122,    53.75,    Mydadfloyd,    Catapult Jack,    The I Man,    6w upper, improved,    12,    </div></br> 
<div style="color:blue" > 10/19/2017 - 9,    BEL,    FT,    54,    F,    54,    (T)-1,    MCL,    25000,    71,    52,    64,    8,    1,    8,    8,    650.00,    7,    6,    7,    3,    5,    Lezcano Abel,    L,    121,    49.25,    O Captainmycaptain,    Giant Rocks,    Barefoot Angel,    6w upper, belatedly,    9,    </div></br> 
<div style="color:red" > 10/05/2017 - 2,    BEL,    FM,    102,    F,    102,    (T)35,    MCL,    40000,    83,    92,    66,    2,    4,    4,    2,    150.00,    3,    6,    6,    9,    6,    Diaz Jr Hector R,    L,    116,    25.25,    Hopper Dropper,    Frost Proof,    Pence,    POCKET,ANGLE 3W UPPER,    7,    </div></br> 
</td> </tr>
<tr  class="horse_9_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 70  ( date: 2017 ) 15   0   1   1   $17822.00    </br> 
  2017 : 12   0   1   1   $13292.00    </br> 
  2018 : 3   0   0   0   $4530.00    </br> 
  GP: 15   0   1   1   $17822.00    </br> 
  Turf: 4   0   0   0   $834.00    </br> 
  Dist: 8   0   1   1   $14214.00    </br> 
 </td> </tr>
<tr><td ROWSPAN="1" class="horse_data" program="10" race="9" title="Show More..." > 10</br> <span class="past-performance-plus-b" > </span> </td>
<td> CLONEDSIMMARD</td>
<td> Arroyo Angel S</td>
<td> Gyarmati Leah</td>
<td> Dormellito Stud</td>
<td> Zamarota Racing Stables</td>
<td> Simmard</td>
<td> Annaclone</td>
<td> 12/1</td></tr><tr class="horse_10_9" style="display:none;"><td COLSPAN=9>
  Jocks: St 52 W 13% P 12% S 15% ITM 40% ROI -34</td></tr><tr  class="horse_10_9" style="display:none;"><td COLSPAN=9>
 Tr: St 5 W 20% P 0% S 20% ITM 40% ROI 2</td></tr><tr  class="horse_10_9" style="display:none;"><td COLSPAN=9>
<div style="color:blue" > 03/04/2018 - 9,    AQU,    FT,    54,    F,    54,    (T)-1,    MCL,    20000,    77,    45,    64,    7,    6,    6,    8,    300.00,    4,    3,    5,    5,    5,    Arroyo Angel S,    L,    121,    29.25,    Love Your Buttons,    Dublin Leprechaun,    Arthur Avenue,    5w upper, wknd late,    9,    </div></br> 
<div style="color:red" > 02/08/2018 - 2,    AQU,    GD,    50,    F,    50,    (T)-1,    MCL,    20000,    82,    49,    53,    3,    7,    4,    2,    310.00,    5,    3,    5,    4,    5,    Arroyo Angel S,    L,    121,    59.75,    Da Meister,    Champagne Papi,    Shoot the Gap,    5w upper, wknd late,    7,    </div></br> 
<div style="color:blue" > 01/27/2018 - 4,    AQU,    FT,    58,    F,    58,    (T)-1,    MCL,    25000,    67,    3,    51,    1,    6,    6,    12,    960.00,    6,    4,    6,    5,    5,    Garcia Martin,    L,    121,    19.20,    Shipsandgoods,    Port Arch,    Our American Star,    chased 5-6w, no impact,    6,    </div></br> 
<div style="color:red" > 01/15/2018 - 2,    AQU,    FT,    0,    F,    0,    (T)0,    SCR,    0,    85,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Veterinarian,    0,    </div></br> 
<div style="color:blue" > 12/29/2017 - 4,    AQU,    FT,    53,    F,    53,    (T)-1,    MCL,    20000,    87,    59,    52,    7,    1,    3,    6,    1350.00,    3,    24,    4,    30,    4,    Navarro Joshua,    L,    121,    54.25,    Cookie Crisp,    Alpha Team,    Champagne Papi,    3w top turn, tired,    7,    </div></br> 
<div style="color:red" > 11/11/2017 - 9,    AQU,    FT,    16,    F,    16,    (T)-1,    MCL,    25000,    77,    42,    44,    1,    1,    9,    9,    670.00,    8,    10,    9,    18,    10,    Cancel Eric,    L,    122,    33.50,    Mydadfloyd,    Catapult Jack,    The I Man,    saved ground no avail,    12,    </div></br> 
<div style="color:blue" > 10/19/2017 - 9,    BEL,    FT,    54,    F,    54,    (T)-1,    MCL,    25000,    71,    62,    63,    7,    8,    6,    7,    550.00,    6,    5,    5,    3,    6,    Cancel Eric,    L,    121,    16.50,    O Captainmycaptain,    Giant Rocks,    Barefoot Angel,    chased 2-3w, outkicked,    9,    </div></br> 
<div style="color:red" > 09/15/2017 - 10,    BEL,    FM,    52,    F,    52,    (T)9,    MCL,    40000,    77,    70,    62,    9,    6,    10,    14,    470.00,    8,    8,    9,    11,    9,    Luzzi Michael J,    L,    120,    132.00,    Risky Sour,    Run for Boston,    Brooklyn Speights,    CHECKED OFF HEELS ST,    12,    </div></br> 
<div style="color:blue" > 08/17/2017 - 2,    SAR,    FM,    38,    F,    38,    (T)18,    MCL,    40000,    78,    73,    58,    8,    9,    5,    6,    650.00,    6,    9,    8,    13,    8,    Luzzi Michael J,    L,    118,    62.50,    You Like That,    Saratoga Colonel,    Fleet Admiral,    3w 1st,2w & ask 2nd,    10,    </div></br> 
<div style="color:red" > 07/15/2017 - 7,    BEL,    FM,    0,    F,    0,    (T)0,    SCR,    0,    82,    0,    0,    0,    0,    0,    0,    0.00,    0,    0,    0,    0,    0,    0,    0.00,    Main-Track-Only,    0,    </div></br> 
<div style="color:blue" > 07/04/2017 - 9,    BEL,    FM,    104,    F,    104,    (T)35,    MCL,    40000,    81,    39,    60,    10,    11,    11,    10,    1070.00,    11,    11,    11,    11,    9,    Luzzi Michael J,    L,    118,    80.50,    Tiffanys Freud,    The Pooch,    Daring Destiny,    OFF BEAT SLW AWKWARDLY,    11,    </div></br> 
<div style="color:red" > 03/10/2017 - 9,    AQU,    GD,    26,    F,    26,    (T)-1,    MCL,    40000,    80,    21,    46,    5,    10,    10,    10,    910.00,    9,    12,    8,    13,    8,    Montanez Rosario,    L,    117,    21.70,    The Hero Within,    Guick,    Quai Voltaire,    LEAPT,HIT GATE,3W TURN,    10,    </div></br> 
</td> </tr>
<tr  class="horse_10_9" style="display:none;"><td COLSPAN=9>
LifeTime Record 64  ( date: 2018 ) 11   0   0   1   $9718.00    </br> 
  2017 : 8   0   0   1   $7048.00    </br> 
  2018 : 3   0   0   0   $2670.00    </br> 
  GP: 9   0   0   0   $4754.00    </br> 
  Turf: 3   0   0   0   $435.00    </br> 
  Dist: 4   0   0   1   $5213.00    </br> 
 </td> </tr>
                </tbody>
             </table>
         </div>
    