<link rel="stylesheet" type="text/css" href="/assets/css/pps.css">

<div id="main" class="container">
  <div class="row">
    <div id="left-col" class="col-md-11">
      <div class="headline">
        <h1>Free Past Performances Demo</h1>
      </div>
      <div class="pps-content" id="pps-tracks" style="display:none;"> 
        
        <!--<div id="pp-sorter" style="float: right;"> Sort A-Z</div>-->
        
        <div id="pps-track-date" >
          <div class="pps-track-date-header">Select Date</div>
          <div class="pps-track-date-body" id="pps-track-date-body"> </div>
        </div>
        <div id="pps-track-menu" >
          <div class="pps-track-menu-header">Select Track</div>
          <div  id="pps-track-menu-body" class="pps-track-menu-body"> </div>
        </div>
      </div>
      <!-- end/ content -->
      
      <div class="pps-content" id="pps-races" style="display:none;">
        <div id="pps-race-header" class="pps-race-header">
          <div id="pps-race-back"  class="pps-race-back" > Back to Tracks </div>
          <div id="pps-race-track-header"  class="pps-race-track-header-title"> </div>
        </div>
        <div id="pps-race-content" > </div>
      </div>
      <div class="pps-content" id="pps-horses" style="display:none;">
        <div id="pps-horse-header" class="pps-horse-header">
          <div id="pps-horse-back"  class="pps-horse-back" > Back to Races </div>
          <div id="pps-horse-track-header"  class="pps-race-track-header"> </div>
          <div class = "pps-race-numb">
            <div id="pps-horse-track-header-carousel" class="pps-header-carousel"> <span class="carousel-left"></span>
              <div id="carousel-body" class="carousel-body"></div>
              <span class="carousel-right"></span> </div>
          </div>
          <div class = "pps-horse-claiming"></div>
          <div class = "pps-horse-bet-opt"></div>
          <div class="btn_top"> <a href="/signup?ref={$race_url}" class="btn-xlrg">{$button_cta}</a> </div>
        </div>
        <div id="pps-horse-content"> </div>
        <div class="btn_down"> <a href="/signup?ref={$race_url}" class="btn-xlrg">{$button_cta}</a> </div>
      </div>
    </div>
    <!-- end/ #left-col --> 
    
  </div>
</div>
<div>
</div>
<script type="text/javascript" >
    var racedate={$racedate};
    var racetrack={$racetrack};
    var raceid={$raceid};
    var pathName="{$arg_path}".replace(/^\//,'');
    var claiming={if ! isset($claiming)} -1 {else} {$claiming} {/if};
    var purse={if ! isset($purse)} -1 {else} {$purse}{/if};
    var trackList={$raceobj};
</script>
{literal} 
<script type="text/javascript" src="/assets/js/pps.js"></script> 
{/literal} 
