<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/assets/css/pps.css">
   <div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-10">
            <div class="headline"><h1>Free Past Performances Demo</h1></div>
            <div id="pp-sorter" style="float: right;"> Sort A-Z
         </div>
            <div class="content">
                <div id="pps-track-date" >
                    <div class="pps-track-date-header">Select Date</div>
                    <div class="pps-track-date-body">
                        <div id="pps-track-date-box-22072018" class="pps-track-date-box" viewDate="22072018">
                            <div class="pps-track-date-text" >Before Yesterday
                            </div>
                        </div>
                        <div id="pps-track-date-box-23072018" class="pps-track-date-box" viewDate="23072018">
                            <div class="pps-track-date-text" >Yesterday</div>
                         </div>
                        <div id="pps-track-date-box-24072018" class="pps-track-date-box" viewDate="24072018">
                            <div class="pps-track-date-text" >Today</div>
                        </div>
                    </div>
                </div>
                <div id="pps-track-menu" >
                        <div class="pps-track-menu-header">Select Track</div>
                        <div  id="pps-track-menu-body" class="pps-track-menu-body">
                            <div tracktype="THB" id="153" class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                              </div>
                            <div tracktype="THB" id="153" class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                                </div>
                            <div tracktype="THB" id="153" class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                                </div>
                            <div tracktype="THB" id="153" class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                                </div>
                            <div tracktype="THB" id="153" class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                                </div>
                            <div  class="pps-track-menu-box">
                                <div class="pps-track-menu-text">Arlington Park</div>
                                </div>
                        </div>   
                 </div>
                  
            
            </div> <!-- end/ content -->
        </div> <!-- end/ #left-col -->
    </div>
</div>
<script type="text/javascript">
{literal}
    var viewDate = '24072018';
    var today = '24072018';
    var trackList={};
    var sortby={'field':'trackname','order':'asc'};
     trackList['24072018'] =[
                { 'trackname':'Arlington Park' },
                { 'trackname':'Albuquerque' },
                { 'trackname':'Arapahoe Park' },
                { 'trackname':'Assiniboia Downs' }
           ];
   trackList['23072018']=[
                { 'trackname':'Belterra Park' },
                { 'trackname':'Camarero Race Track' },
                { 'trackname':'Canterbury Park' },
                { 'trackname':'Charles Town' }
           ];
   trackList['22072018']=[
                { 'trackname':'Del Mar' },
                { 'trackname':'Delaware Park' },
                { 'trackname':'Ellis Park' },
                { 'trackname':'Emerald Downs' }
           ];
    function process_template(){
        var pps_body = document.getElementById("pps-track-menu-body");
        $(".pps-track-date-box").removeClass("date-box-selected");
        pps_body.innerHTML='';
        var tracks =  sort_array(trackList[viewDate],sortby);
        $("#pps-track-date-box-" + viewDate ) .addClass("date-box-selected");
        for ( var i in tracks ) {
            var track = tracks[i];
            var div = document.createElement("div");
            div.setAttribute("class","pps-track-menu-box");
            var child = document.createElement("div");
            child.setAttribute("class","pps-track-menu-text");
            child.innerHTML=track['trackname'];
            div.append(child);
            pps_body.append(div);

        }
        $(".pps-track-date-box").off("click");
        $(".pps-track-date-box").on("click",function(e){
            var __date = $(this).attr('viewDate');
            if ( $(this).hasClass("date-box-selected") && today !=__date){
                viewDate=today;
                process_template();
            } else {
                viewDate=__date;
                process_template();
            }
        });
        $(".pps-track-menu-box").off("click");
        $(".pps-track-menu-box").on("click",function(e){
            $(this).toggleClass("track-box-selected");
        });
        $("#pp-sorter").off("click");
        $("#pp-sorter").on("click",function(e){
            sortby['order'] = ( sortby['order'] =='asc') ? 'desc' : 'asc';
            var text = ( sortby['order'] =='asc') ? " Sort A-Z " : " Sort Z-A ";
            $(this).html( text) ;
            process_template();
        });
    }
    function sort_array(arr,sortby){
        if( typeof( sortby ) != "undefined" )
	    {
		    arr = arr.sort( function( a, b )
		    {
		    	if( a[sortby.field] < b[sortby.field] ) return( sortby.order == 'asc' ) ? -1 : 1;
		    	else if( a[sortby.field] > b[sortby.field] ) return( sortby.order == 'asc' ) ? 1 : -1;
		    	else return 0;
		    });
	    }   
	    return arr;
    }
    $(function(){
        process_template();
        
    });



     {/literal}
</script>
