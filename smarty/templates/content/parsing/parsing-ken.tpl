<link rel="stylesheet" type="text/css" href="/assets/css/pps.css">
   <div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-10">
            <div class="headline"><h1>Free Past Performances Demo</h1></div>
            <div class="content" id="pps-tracks">
                <div id="pp-sorter" style="float: right;"> Sort A-Z</div>
                <div id="pps-track-date" >
                    <div class="pps-track-date-header">Select Date</div>
                    <div class="pps-track-date-body" id="pps-track-date-body">
                    </div>
                </div>
                <div id="pps-track-menu" >
                        <div class="pps-track-menu-header">Select Track</div>
                        <div  id="pps-track-menu-body" class="pps-track-menu-body">
                        </div>   
                 </div>
                  
            
            </div> <!-- end/ content -->
            <div class="content" id="pps-races" style="display:none;">
                <div id="pps-race-header" class="pps-race-header"> 
                    <div id="pps-race-track-header"  class="pps-race-track-header"> </div>
                    <div id="pps-race-back"  class="pps-race-back" > Back </div>
                </div>
                <div id="pps-race-content" >
                </div>
            </div>
            <div class="content" id="pps-horses" style="display:none;">
                <div id="pps-horse-header" class="pps-horse-header">
                    <div id="pps-horse-track-header"  class="pps-horse-track-header"> </div>
                    <div id="pps-horse-back"  class="pps-horse-back" > Back </div>
                </div>
                <div id="pps-horse-content" >
                </div>
            </div>
        </div> <!-- end/ #left-col -->
    </div>
</div>
{literal}
<script type="text/javascript" src="/assets/js/ppsdata.js"></script>
<script type="text/javascript" src="/assets/js/pps.js"></script>
     {/literal}
