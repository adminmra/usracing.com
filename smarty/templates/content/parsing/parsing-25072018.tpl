<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/assets/css/pps-25-07-2018.css">
   <div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-10">
            <div class="headline"><h1>Free Past Performances Demo</h1></div>
            <div class="content" id="pps-tracks">
                <div id="pp-sorter" style="float: right;"> Sort A-Z</div>
                <div id="pps-track-date" >
                    <div class="pps-track-date-header">Select Date</div>
                    <div class="pps-track-date-body">
                        <div id="pps-track-date-box-22072018" class="pps-track-date-box" viewDate="22072018">
                            <div class="pps-track-date-text" >Before Yesterday
                            </div>
                        </div>
                        <div id="pps-track-date-box-23072018" class="pps-track-date-box" viewDate="23072018">
                            <div class="pps-track-date-text" >Yesterday</div>
                         </div>
                        <div id="pps-track-date-box-24072018" class="pps-track-date-box" viewDate="24072018">
                            <div class="pps-track-date-text" >Today</div>
                        </div>
                    </div>
                </div>
                <div id="pps-track-menu" >
                        <div class="pps-track-menu-header">Select Track</div>
                        <div  id="pps-track-menu-body" class="pps-track-menu-body">
                        </div>   
                 </div>
                  
            
            </div> <!-- end/ content -->
            <div class="content" id="pps-races">
                <div id="pps-race-header" class="pps-race-header"> 
                    <div id="pps-race-track-header"  class="pps-race-track-header"> </div>
                    <div id="pps-race-back"  class="pps-race-back" > Back </div>
                </div>
                <div id="pps-race-content" >
                </div>
            </div>
        </div> <!-- end/ #left-col -->
    </div>
</div>
{literal}
<script type="text/javascript" src="/assets/js/pps-25-07-2018.js"></script>
     {/literal}
