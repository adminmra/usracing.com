<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="home/ah/usracing.com/htdocs/assets/css/">
<?php
if ( !defined( 'APP_PATH' ) ) {
    define( 'APP_PATH' , '/home/ah/allhorse/public_html/programsV2/' );
}

require_once APP_PATH . 'Scraper.php';
class parsing extends Scraper{

    private $ALWAYSWRITE;
    private $OUTPUTFILEPATH;
    private $data;
    private $mainTitle;

    function __construct(){
        parent::__construct();
        $this->source         = "parsing.xml";
        $this->ALWAYSWRITE    = FALSE; //always write eveng with the message "please check back shortly"
        $this->OUTPUTFILEPATH = "parsing.tpl"; //the absolute path of the file to be written
        $this->data           = array();
        $this->mainTitle      = "";
    }

    public function scraping_to_database(){}

    public function scraping_to_xml(){}

    public function scraping_to_tpl(){}
    public function getDateString($str,$input,$output){
        $myDateTime = DateTime::createFromFormat($input, $str);
        return $myDateTime->format($output);
        
    }
    public static function run() {
        try {
            $instance = new self;
            $instance->process();

        } catch ( Exception $e ) {
            print( $e->getMessage() );
        }
    }

    public function process(){
        $this->parseXmlStructureXP();
        $this->generateOutputXP();
    }
    
    private function process_ppdata($ppdata){
        $str = '';
        $output = array();
        $lt_speed =0;
        $lt_racedate ='';
        $ty_date=0;
        $wanted_data= array("racenumber","trackcode","trackcondi","runup_dist","disttype","runup_dist","rail_dist","racetype","claimprice","classratin"
        ,"speedfigur","postpositi","gatebreak","position1","lenback1","lenback2","position2","lenbackstr","positionst","lenbackfin","positionfi","jockdisp","medication"
        ,"weightcarr","posttimeod","complineho","complineh2","complineh3","shortcomme","fieldsize");
        $text_color=array("blue","red");
        foreach ( $ppdata as $k=>$pp ) {
                $i = $k % (count ( $text_color ) );
                $str .= "<div style=\"color:" . $text_color[$i] . "\" > ";
                if ( isset ( $pp["racedate"] ) ) $str .= $this->getDateString($pp["racedate"],'Ymd','m/d/Y') . " - ";

                foreach ( $wanted_data as $w ) {
                    if ( isset ( $pp[$w] ) && !is_array ( $pp[$w] ) && $pp[$w] != "" ) {
                            $__str= $pp[$w] ;
                            switch ( $w ) {
                                case "lenback1":
                                case "lenbackstr":
                                case "lenbackfin":
                                    $__str = round ( intval ($pp[$w]) / 100 );
                                break;
                                case "rail_dist":
                                    $__str = "(T)" .$pp[$w];
                                break;
                            }
                            $str .= $__str . ",    ";
                    }
                }
              $str .= "</div></br> \n" ;
                if ( isset ( $pp["speedfigur"] ) && intval($pp["speedfigur"]) > $lt_speed ) {
                      $lt_racedate = $this->getDateString($pp["racedate"],'Ymd','Y');
                      $lt_speed = intval($pp["speedfigur"]);
                }
                if ( isset ( $pp["racedate"] ) && intval ( isset ( $pp["racedate"] ) ) > $ty_date ) {
                      $ty_date =  $pp["racedate"];
                }
         }
        $output['lt_speed'] = $lt_speed;
        $output['lt_racedate'] = $lt_racedate;
        $output['ty_date'] = $ty_date;
        $output['str'] = $str;
        return $output;
    }
    private function hash_stat($stat ) {
        $out = array();
        foreach ( $stat as $s ) {
                $out[$s['@attributes']['type']] = $s;
        }
        return $out;
    }
    private function process_record($stat,$lt_speed,$lt_racedate,$ly_date,$ty_date){
                    $str='';
                    $wanted_data = array('starts','wins','places','shows','earnings');
                    $wanted_type = array ( 'LIFETIME'=>"LifeTime Record $lt_speed  (  $lt_racedate  ) ",
                                           'LAST_YEAR'=>" $ly_date : ",
                                           'THIS_YEAR'=>" $ty_date : ",
                                           'TRAN_HORSE'=>" GP: ",
                                           'TURF'=>" Turf: ",
                                           'SOFT'=>" Soft: ",
                                           'DIST_SURF'=>" Dist: ");
                    $hash = $this->hash_stat($stat);
                    foreach ( $wanted_type as $k=>$st ) {
                        if ( isset ( $hash[$k] ) ) {
                            $d= $hash[$k];
                            $str .= $st;
                             foreach ( $wanted_data as $w){
                                        $str .= ( $w == 'earnings' ) ? "$" : "";
                                        $str .= $d[$w]. "   ";
                            }
                            $str .= " </br> \n " ;
                        }

                    }
           return $str;
    }
    private function parseXmlStructureXP(){

        $ret = false;
        $this->mainTitle = "";
        $this->data = array();
        $xml_string = file_get_contents( $this->source );
        $xml = simplexml_load_string($xml_string);
        if(count($xml)<=0) return $ret;
        $this->data = json_decode(json_encode($xml[0]), TRUE)['racedata']; 
return true;
    }
    private function writeOutputXP($contents){
        if($contents == ""){
            $contents = "The odds are currently being updated, please check back shortly. <em id='updateemp'> $update. </em>";
            if($this->ALWAYSWRITE || !file_exists($this->OUTPUTFILEPATH)){
                if(file_put_contents($this->OUTPUTFILEPATH, $contents)){
                    echo "File " , $this->OUTPUTFILEPATH . " was written with no data.\n";
                    return true;
                }
                else{
                    echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data. Please check permissions.\n";
                    return false;
                }
            }
            else{
                if($contents !== NULL){
                    if(file_put_contents($this->OUTPUTFILEPATH, $contents)){
                        echo "Empty output, update record updated.\n";
                        return true;
                    }
                    else{
                        echo "Error while trying to write file " . $this->OUTPUTFILEPATH . " with no data but updating update record. Please check permissions.\n";
                        return false;
                    }
                }

                return false;
            }
        }
        else{
            if( file_put_contents($this->OUTPUTFILEPATH, $contents) ){
                echo sprintf( $this->console_color->getColoredString( ">>> " , "cyan" ) . "%'.-110s%-s\n" , $this->console_color->getColoredString( $this->OUTPUTFILEPATH , "red" ) , 'GENERATED');
                return true;
            }
            else{
                echo "Error while trying to write file " . $this->OUTPUTFILEPATH . ". Please check permissions.\n";
                return false;
            }
        }

    }

    private function generateOutputXP(){
        
        $output = "";
        if(count($this->data)<=0) return $this->writeOutputXP($output);
        $firstHeader = false;
        ob_start();
        foreach ($this->data as $d) {
            echo " <div class=\"table-responsive\" race=\"" .$d["race"] . "\" race_date=\"".$d["race_date"] . "\" claimamt=\"". $d["claimamt"]
                    . "\" dist_disp=\"". $d["dist_disp"]  ."\" todays_cls=\"". $d["todays_cls"] ."\" purse=\"". $d["purse"] ."\"> </br>"
            ?>
            <table border="1" class="data table table-condensed table-striped table-bordered " width="100%" cellpadding="0" cellspacing="0" title="Delta Downs Past Performances" summary="The  Past Performances for Delta Downs ">
                <caption>
                    <?php
                    if($d["race_date"] != "") {
                            echo "<span > " . $this->getDateString($d["race_date"],'Ymd','m/d/Y') . " </span > * \n"; 
                    }
                    echo "<span>Delta Downs</span> * \n";
                    if ( $d["country"] != "" ) echo "<span> " .$d["country"] . "</span>\n";
                    echo "    <hr><br>";
                    if($d["race"] != "" ) echo "<span> Race #" .$d["race"]. "</span> - \n";
                    if($d["claimamt"] != "" ) echo "<span> Claiming $" .$d["claimamt"]  . "</span> - \n";
                    if($d["dist_disp"] != "" ) echo "<span> Distance " .$d["dist_disp"]. "</span> - \n";
                    if($d["todays_cls"] != "" ) echo "<span> Todays CLS " .$d["todays_cls"]. "</span> -\n";
                    if($d["purse"] != "" ) echo "<span> Purse $" . $d["purse"]. "</span> \n";
                    echo "    <br>";
                    if ( $d["bet_opt"] != "" ) echo  "<span>" .$d["bet_opt"] ."</span>\n";
                    echo " </br> <span class=\"race-tables past-performance-plus\" title=\"Click here for more information\"> </span>";
                    ?>
                </caption>

               
               
               
               
               
                <tbody style="display:none;">
                    <tr>
                        <th>Program</th>
                        <th>Horse</th>
                        <th>Jockey</th>
                        <th>Trainer</th>
                        <th data-breakpoints="xs sm md">Breeder</th>
                        <th data-breakpoints="xs sm md">Owner</th>
                        <th data-breakpoints="xs sm md">Sire</th>
                        <th data-breakpoints="xs sm md">Dam</th>
                        <th style="white-space: nowrap;">Morn Odds</th>
                    </tr>
                  
                    
                    <!--LA COLUMNA #1 VA DE AQUI-->
    <?php
            if(count($d["horsedata"])>0){
                
                foreach($d["horsedata"] as $t=>$g){
                    echo "<tr>";
                    if ($g["program"] != "" ) echo "<td ROWSPAN=\"1\" class=\"horse_data\" program=\"".$g["program"]."\" race=\"".$d["race"] ."\" title=\"Show More...\" > " . $g["program"] 
                                . "</br> <span class=\"past-performance-plus-b\" > </span> </td>\n" ;
                    if ($g["horse_name"] != "" ) echo "<td> " .$g["horse_name"]  . "</td>\n" ;
                    if ( isset ( $g["jockey"] ) && isset ( $g["jockey"]["jock_disp"] ) && $g["jockey"]["jock_disp"] != "" ) {
                         echo "<td> " . $g["jockey"]["jock_disp"]. "</td>\n" ;
                    }
					
					
					//<!-- LA COLUMNA #1 TERMINA AQUI-->
					//<!-- LA COLUMNA #2 COMIENZA AQUI-->
					
                    if ( isset ( $g["trainer"] ) && isset ( $g["trainer"]["tran_disp"] ) && $g["trainer"]["tran_disp"] != "" ) {
                        echo "<td> " .$g["trainer"]["tran_disp"]. "</td>\n" ;
                    }
                    if ($g["breeder"] != "" ) echo "<td> " .$g["breeder"]   . "</td>\n" ;
                    if ($g["owner_name"] != "" ) echo "<td> " .$g["owner_name"]   . "</td>\n" ;
					
					
					//<!-- LA COLUMNA #2 TERMINA AQUI-->
					//<!-- LA COLUMNA #3 COMIENZA AQUI-->
					
                    if ( isset ( $g["sire"]) && isset ( $g["sire"]["sirename"] ) && $g["sire"]["sirename"] != "") {
                            echo "<td> " .$g["sire"]["sirename"]  . "</td>\n" ;
                    }
                    if ( isset ( $g["dam"]) && isset ( $g["dam"]["damname"] ) && $g["dam"]["damname"] != "") {
                            echo "<td> " .$g["dam"]["damname"]  . "</td>\n" ;
                    }
                    if ($g["morn_odds"] != "" )  echo "<td> " .$g["morn_odds"]   . "</td>" ;
                    echo "</tr>";
                    if ( isset ( $g["jockey"] ) ) { 
                        echo "<tr class=\"horse_". $g["program"]."_" . $d["race"] ."\" style=\"display:none;\"><td COLSPAN=9>\n";
                        if ( isset ( $g["jockey"]['stats_data'] ) && count ( $g["jockey"]['stats_data']['stat'] ) > 0 ) {
                                $j = $g["jockey"]['stats_data']['stat'];
                                $starts = intval ( $j['starts'] ) ;
                                $__w = round ( intval ( $j['wins'] ) * 100 / $starts );
                                $__p = round ( intval ( $j['places'] ) * 100 / $starts );
                                $__s = round ( intval ( $j['shows'] ) * 100 / $starts );
                                $__itm = $__w + $__p + $__s;
                                
                                echo "  Jocks: St $starts W $__w% P $__p% S $__s% ITM $__itm% ROI " . $j['roi'] . "";
                        }
                        echo "</td></tr>";
                    }
                    if ( isset ( $g["trainer"] ) ) {
                        echo "<tr  class=\"horse_". $g["program"]."_" . $d["race"] ."\" style=\"display:none;\"><td COLSPAN=9>\n";
                        if ( isset ( $g["trainer"]['stats_data'] ) && count ( $g["trainer"]['stats_data']['stat'] ) > 0) {
                                $t =  $g["trainer"]['stats_data']['stat'] ;
                                $starts = intval ( $t['starts'] ) ;
                                $__w = round ( intval ( $t['wins'] ) * 100 / $starts ) ;
                                $__p = round ( intval ( $t['places'] ) * 100 / $starts );
                                $__s = round ( intval ( $t['shows'] ) * 100 / $starts );
                                $__itm = $__w + $__p + $__s;
                                echo " Tr: St $starts W $__w% P $__p% S $__s% ITM $__itm% ROI " . $t['roi'] . "";
                        }
                        echo "</td></tr>";
                    }
                    $lt_speed = 0;
                    $lt_racedate = '';
                    $ty_date = 0;
                    if ( isset ( $g['ppdata'] ) ) {
                            echo "<tr  class=\"horse_". $g["program"]."_" . $d["race"] ."\" style=\"display:none;\"><td COLSPAN=9>\n";
                            if ( !isset ( $g['ppdata'][0] ) ) $__ppdata= $this->process_ppdata(array($g['ppdata']));
                            else $__ppdata= $this->process_ppdata($g['ppdata']);
                           $lt_speed = $__ppdata['lt_speed'];
                            $lt_racedate = $__ppdata['lt_racedate'];
                            $ty_date = $__ppdata['ty_date'];
                            echo $__ppdata['str'];
                            echo "</td> </tr>\n";
                    }
                    if ( $ty_date ) $ty_date = $this->getDateString($ty_date,'Ymd','Y');
                    $ly_date = intval ( $ty_date )  - 1;
                    if ( isset ( $g['stats_data'] ) && isset ( $g['stats_data']['stat'] ) ) {
                        echo "<tr  class=\"horse_". $g["program"]."_" . $d["race"] ."\" style=\"display:none;\"><td COLSPAN=9>\n";
                        echo $this->process_record($g['stats_data']['stat'],$lt_speed,$lt_racedate,$ly_date,$ty_date);
                        echo "</td> </tr>\n";
}




                }
                
            }
            
?>
                </tbody>
             </table>
         </div>
    <?php
        }
        $output = ob_get_clean();
        return $this->writeOutputXP($output);
    }

}
parsing::run();

?>

<script src=../js/footable.min.js"></script>
<script>
	
	jQuery(function($){
	$('.table').footable();
});
</script>
