<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




                  <div class="homeIntro" style="height: 196px;">
                    <div class="quote"><span style="float: left;">"<strong>If you wager online, your first stop should be US Racing</strong>"</span> <img style="float: right; display: block; margin-top: 3px;" src="/themes/images/maxim-logo.gif" alt="Maxim" width="51" /></div>
                    
                    	<div class="copy first" style="clear: right;">
                      <p>Are you ready to <a href="/kentuckyderby-betting" title="Bet on the Derby"><b>bet on the Kentucky Derby</b></a>?</p>
                      <p>US Racing is the leader in online horse racing.  Call it <a href="/otb" title="Off Track Betting" >off track betting or OTB</a> but know that you can bet safely and securely with us. <a href="/join" title="Sign Up Today for FREE!">Free Membership</a> allows you to bet on your <a href="/leaders/jockeys" title="Jockey Leaders">favorite jockeys</a>, <a href="/leaders/horses" title="Horse Racing Leaders">horses</a> and <a href="/racetracks">racetracks</a>.  Get a huge <a href="/promotions/horseracing" title="Horse Racing Promotions">8% Rebate</a> on your horse wagers, too.</p>
                      <p><a href="/kentuckyderby-odds" title="Kentucky Derby Odds to Win">Kentucky Derby odds</a> are already available - advance wagering!</p>
                    </div>
                    
                    <div class="copy">
                      <p><a title="Weekly Horse Racing" href="/beton/race-of-the-week">Races of the week</a> allow you to double your winnings.  Do you like <a href="/virtualderby">Virtual Horse Racing</a>?  Along with jackpots and fun casino games, US Racing is the smart choice for horse betting &ndash; don't take our word, our company is featured regularly in <a href="/readreviews">magazines and newspapers</a>. </p>
                      <p>So, what are you waiting for? It's free to join and you get a <a title="Bet on the Kentucky Derby for FREE!" href="/triplecrown/freebet">free Derby Bet</a>! Make the choice today &ndash; Bet With Confidence.</p>
                     </div>
                     
                  </div>
                     

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->

</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



    <div id="bottom_content">
      <div id="block-block-6" class="block block-block region-odd odd region-count-1 count-5">
        
      </div>
      <!-- /block-inner, /block --> 
      
    </div>
  

<!-- /#main-inner -->

<!-- /#main --> 

