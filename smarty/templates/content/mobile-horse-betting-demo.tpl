{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/horsebetting.tpl'} 
<!-- ---------------------- end left menu contents ------------------- -->         
</div> 
      
<!--------- NEW HERO  ---------------------------------------->
<div class="newHeroM" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   
     <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      


   <section class="kd bcs usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <h1 class="kd_heading">{$h1}</h1>
<p>Bet on horse racing with your iPhone, mobile phone. Bet on horses with 200+ racetracks, $150 Bonus and daily rebates. Mobile phone betting, bet where and when you want. Online Horse Betting. <a href="/signup" rel="nofollow">Join </a> Today!</p>
<h2>Bet on Horses with your Phone or Tablet</h2>


<p>Mobile Horse Betting offers the convenience of betting on horses with your iPhone, Android, iPad and other mobile tablets and devices. Many websites offer this service like BUSR, TVG, TwinSpires, Bet America and Xpressbet.</p>
                          <p> Incredibly easy, fast and secure mobile horse betting wherever wireless service is provided you can bet and watch the races live!</p>
                          <!-- <iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/danRaPtaV_Y?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> -->
                            
                            {include_php file='iframe/video/how_to_bet_video-mobile-horse-betting.php'}
                            <h2>Mobile Horse Racing Betting</h2>
                            <ul  class="spUL">
                           <li>Bet on horses with your phone</li>
                           <li>Live Horse Racing Video on your laptop, tablet and phone!*</li>
                            <li>Real time racing information</li>
                            <li>Program Changes</li>
                            <li>Track all your bets</li>
                            <li>Daily & Weekly Horse Bonuses</li>
                            <li>Race Results</li>
                            <li>No Download Needed</li>
                            <li>Safe, Secure & Fun!</li>
                            <li>America's OTB</li>
                            <li>$150 Sign Up Bonus</li>
                           
                            </ul>

<br />
<p>Mobile Horse Betting gives members access to mobile account wagering for thoroughbred racing for over 200 racetracks, live horse racing odds, up-to-the-minute program changes, & real-time race results, all with a quick flick of your phone. </p>
<br />
<p>* Video provided by third parties. US Racing makes no guarantee as to the availability of races on video provided by reviewed ADWs and racebooks.</p>



      </div>
      </div>

    
 </section> 
            
      <!------------------------------------------------------------------>  
<!------------------------------------------------------------------>  
    
      <section class="bc-racetracks">
      <div class="container">
        <h2 class="bc-racetracks_title">
           
          Choose from <strong>211 </strong>different racetracks
        </h2>
        <ul class="bc-racetracks_list">
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-thoroughbred-racing.png" alt="Thoroughbred Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Thoroughbred</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-harness-racing.png" alt="Harness Racing Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Harness</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-quarter-horse-racing.png" alt="Quarter Horse Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Quarter Horses</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-international-horse-racing.png" alt="International  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">International</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-greyhound-racing.png" alt="Greyhound  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Greyhound</h4>
              </div></a></li>
        </ul>
      </div>
    </section>
    
  <!------------------------------------------------------------------>   
    
    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/mobile-horse-betting.png" alt="Mobile Horse Betting" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-mobile-horse-betting.png" alt="Mobile Horsed Betting Icon" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the go!</h2>
          <p>Take all the races with you! All tracks are available on phone or tablet at the track, at home, everywhere.</p> <p>Are you out at the bar with your friends? Bet on the game any time, anywhere.  Or, while waiting for your friends to arrive,  how about playing a few hands of blackjack or roulette! <p> BUSR - super easy and super fun.</p><a href="/mobile-horse-betting?ref={$ref}" class="bc-card_btn-red">See Your Mobile Betting Options</a>
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------> 
    
  <!------------------------------------------------------------------>      
   {**}
   {*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-3.tpl"*}
  {*include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"*}

{* {include file="/home/ah/allhorse/public_html/belmont/block-testimonial-other.tpl"} *}
 {include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 

 {include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 

 {*include file="/home/ah/allhorse/public_html/usracing/block-50-states.tpl"*} 
  {*include file="/home/ah/allhorse/public_html/breeders/block-countdown.tpl"*}
    {include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"}  

  


