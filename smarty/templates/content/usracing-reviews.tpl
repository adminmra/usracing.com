<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>USRacing.com Racebook Reviews</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

<p><a href="/signup/" rel="nofollow"><img class="img-responsive" src="/img/best-horse-racing-betting-site.jpg" alt="US Racing Review">  </a></p>
<p><blockquote>Compare racebooks the other ADWs.  US Racing attempts to resolve any customer disputes with whatever racebook or ADW you use.</blockquote></p>

<p>There are several reviews about racebooks available at online horse racing and sports forums like SBR as well as websites that invite reviews of companies, like SiteJabber.  Although US Racing makes every attempt to vet the racebooks and ADWs in our reviews, there are certainly times that our recommended parties make mistakes.  However, we will always do our best to correct any customer issue. </p>
 {*

<p>Security, reputation, ease of use and customer service are among the most important aspects of a solid horse betting site.</p>
*}




 <!-- PRICING TABLE STYLE 1-->
        <div class="container center-block row">    
			<div class="row">
				<div class="col-xs-3"> </div>
				<div class="col-xs-3"><img alt="logo-otherc" class="logo-otherc" src="/img/compare/busr-logo.png"></div>
				<div class="col-xs-2"> </div>
				<div class="col-xs-2"> </div>
				<div class="col-xs-2"> </div>
			</div>
			<div class="row">
				<div class="col-xs-3"> </div>
				<div class="col-xs-3 no-padding"><span ><img alt="arrow-up" class="arrow-up" src="/img/compare/top-arrow.png"></span></div>
				<div class="col-xs-2"><img alt="twinspires" class="logo-otherc" src="/img/compare/twinspires-logo.jpg"></div>
				<div class="col-xs-2"><img alt="tvg" class="logo-otherc" src="/img/compare/tvg-logo.jpg"></div>
				<div class="col-xs-2"><img alt="xpressbet" class="logo-otherc" src="/img/compare/xpressbet-logo.jpg"></div>
			</div>
			<div class="row comparative_table_row top-radius">
				<div class="col-xs-3 feature-text">Free Membership</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2">
				<div class="col-xs-3 feature-text">Free Bets</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row">
				<div class="col-xs-3 feature-text">Rebates</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2">
				<div class="col-xs-3 feature-text">Accepts all US Residents</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO<span class='asterisk'>*</span></div>
				<div class="col-xs-2 text-center feature-no">NO<span class='asterisk'>*</span></div>
				<div class="col-xs-2 text-center feature-no">NO<span class='asterisk'>*</span></div>
			</div>
			<div class="row comparative_table_row">
				<div class="col-xs-3 feature-text">Accepts Canadian/UK Residents</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2">
				<div class="col-xs-3 feature-text">Horse Racing</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-yes">YES</div>
				<div class="col-xs-2 text-center feature-yes">YES</div>
				<div class="col-xs-2 text-center feature-yes">YES</div>
			</div>
			<div class="row comparative_table_row">
				<div class="col-xs-3 feature-text">Greyhound Racing</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2">
				<div class="col-xs-3 feature-text">Horse Racing Future Wagers</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row">
				<div class="col-xs-3 feature-text ">Match Races</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2">
				<div class="col-xs-3 feature-text ">Sports Betting</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row">
				<div class="col-xs-3 feature-text ">Virtual Derby</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<div class="row comparative_table_row-2 bottom-radius">
				<div class="col-xs-3 feature-text">Casino Games</div>
				<div class="col-xs-3 text-center"><span class="usracing_table_row betus-row">YES</span></div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
				<div class="col-xs-2 text-center feature-no">NO</div>
			</div>
			<p><blockquote><i>* Twinspires, TVG and Xpressbet do not accept residents from:  Texas, New Jersey, Alaska, Arizona, Colorado, Connecticut, District of Columbia, Georgia, Guam, Hawaii, Idaho, Indiana, Maine, Michigan, Minnesota, Mississippi, Missouri, Nebraska, Nevada, New Mexico, North Carolina, Pennsylvania, South Carolina, Tennessee, Utah, Vermont</i></blockquote></p>
        </div>
        <!-- PRICING TABLE STYLE 1-->




<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='inc/rightcol-calltoaction.tpl'}


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
