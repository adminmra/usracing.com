{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/connections.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Famous Horse Trainers</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>While the jockey is usually the person that the racing fans most identify with, the single most important player in a racehorse's life is the trainer. Trainers generally earn their money in two ways: through a "day rate" they charge the owner(s) for day-to-day training; and "stakes" they earn when their owner's horse wins a race, usually 10% of the owners winnings. The trainer is comparable to a team's coach - continually trying to produce winners with a constantly changing barnful of talent.&nbsp;</p>
<p>For every horse in the barn, the trainer teaches them how to race, hones their speed, builds their endurance, sees to their care, and calls in help to heal their injuries.&nbsp;</p>
<p>All horses possess a certain amount of class; ultimately telling in the level of race at which they will be successful. As a trainer begins to work with a horse, they assess that class and train accordingly. For the fans, training is like practicing. Horses are trained almost every morning, when they are taken to the racetrack to walk, jog, gallop or workout, depending on their schedule. Workouts are the most noteworthy part of the training regimen. Shorter workouts (those a half mile or under) are designed to increase speed, while longer ones build stamina.&nbsp;</p>
<p>As with any athlete, horses experience physical problems. This is where the keen eye of an experienced trainer can significantly affect a horse's career. Racehorses are by nature delicate, and by closely watching for and treating injuries before they become serious, the trainer will save his horse, owner and himself a lot of problems. For medical problems, trainers call in their veterinarian. At all racetracks, there are a group of vets who, usually working out of their vehicle instead of an office, travel through the barn area taking care of their clients' horses day to day needs.&nbsp;</p>
<p>After all of this, the trainer still has to select the races in which the horses will compete. Trainers use the condition book, created by the Racing Secretary, to select the type of race for each horse in the stable. For example, if the horse has yet to win, it would be entered in a "maiden" race against other non-winners. An old horsemen's adage is to, "keep myself in the best company and my horses in the worst". Easier said than done, but some trainers have the knack.&nbsp;</p>
<p>For the bettor, it is essential to watch the trainers and see which ones are the most successful. The track program has a stats page for trainers, with all the information needed evaluate when they are most successful. Knowing the trainers and being able to pick up on their hot and cold spells is a top priority in making winning selections at the track.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    