{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Aaron Gryder</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<div><img style="float: left; margin:0 20px 10px 0;" src="/themes/images/jockeys/aaron-gryder.gif" alt="Aaron Gryder" class="img-responsive" /></div>
<div>
<p>For years one of racing's most nomadic jockeys, Aaron Gryder has settled his tack in New York in recent years and has become a respected member of the year-round jockey's colony there. A 30-year-old native of West Covina, California, Gryder earned his first victory in Tijuana, Mexico, in early 1987 and wasted little time establishing himself on the Southern California circuit, winning a riding title at Hollywood Park and the 1987 Matriarch Invitational Stakes (G1) and `88 Beverly Hills Handicap (G1).</p>
<p>But Gryder left Southern California in the late 1980s and spent most of the next decade drifting from one circuit to another, always enjoying success but never settling down on a permanent basis. In the late 1990s, Gryder decided to settle in New York and was an immediate hit, winning the 1998-`99 Aqueduct inner dirt meet title with 53 victories.</p>
<p>&nbsp;Through January 2001, Gryder has more than 2,300 victories and $63-million in career earnings.</p>
</div>

             

            

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





      
    