{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>David Flores</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<div><img style="float: left; margin:0 20px 10px 0;" src="/themes/images/jockeys/david-flores.gif" alt="David Flores" class="img-responsive" /></div>
<div>
<p>David Flores has made a steady rise into the upper echelon of North American riders, and last year finished sixth on the earnings list after his mounts won 208 races and earned $11,938,125. Flores began riding as a 16-year-old in his native Mexico back in 1984, and came to the United States five years later to ride at Del Mar. Flores won the 1997 Santa Anita Handicap with Siphon (Brz) and has a pair of Hollywood Gold Cups triumphs. His most notable accomplishments have also included an amazing string of successes on the California fair circuit, particularly at Fairplex, where he scored a record 48 victories in 1991.&nbsp;</p>
<p>In 1997 he was in the saddle when Isitingood clocked a then-world record mile of 1:32.05 at Santa Anita, and Flores also finished third in the Kentucky Derby that season, aboard Free House.</p>
</div>

             

            

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





      
    