{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Laffit Pincay Jr.</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<div><img  src="/themes/images/jockeys/laffit-pincay.gif" alt="Laffit Pincay Jr." width="130" height="180"  vspace="10" hspace="10" align="left"/>
<p>Laffit Alejandro Pincay, Jr. (born December 29, 1946 in Panama City, Panama) was flat racing's all-time winningest jockey, still holding second place many years after his retirement.</p>

<p>Laffit Pincay, Jr. learned to ride by watching his father who was a jockey at many tracks in Panama and Venezuela. He began his riding career in his native Panama and in 1966 prominent horseman Fred W. Hooper and agent Camilo Marin sponsored him to come to the United States and ride under contract. He started his American career at Arlington Park in Chicago and won eight of his first eleven races. During his career, Pincay Jr. was voted the prestigious George Woolf Memorial Jockey Award in 1970 that honors a rider whose career and personal conduct exemplifies the very best example of participants in the sport of thoroughbred racing. In 1996, he was voted the Mike Venezia Memorial Award for "extraordinary sportsmanship and citizenship". He has won the Eclipse Award for Outstanding Jockey on five occasions and was the United States' leading jockey seven times.</p>
<p>
In 1973, Pincay rode Sham, and together they won that year's Santa Anita Derby and placed second in the Wood Memorial behind Angle Light but ahead of their main rival, Secretariat. Sham was considered the best horse in the west, and they were second choice in the Kentucky Derby, once again behind Secretariat. Secretariat won the race, but Sham finished second, just 2/5 of a second behind. In the Preakness Stakes at Pimlico, Sham was in striking distance in the stretch before losing to Secretariat by two lengths. In the Belmont, Pincay was instructed to keep Sham close to Secretariat. They traveled down the backstretch together, but Sham tired and fell back to finish last of five while Secretariat pulled away from the field for a 31-length victory.</p><br>
<iframe width="100%" class="video" src="//www.youtube.com/embed/S1LxBvKwBsI" frameborder="0" allowfullscreen></iframe>
</div>

             

            

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





      
    