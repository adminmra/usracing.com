{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Bug Boy</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<div><img style="float: left; margin:0 20px 10px 0;" src="/themes/images/jockeys/generic-jockey.gif" alt="Jockey" class="img-responsive" /></div>
<div>
<p>Jockeys usually start out when they are young, riding work in the morning for trainers, and entering the riding profession as an apprentice jockey. It is normally necessary for an apprentice jockey to ride a minimum of about 20 barrier trials successfully before being permitted to commence riding in races. An apprentice jockey is known as a "bug boy" because the asterisk that follows the name in the program looks like a bug.&nbsp;</p>
<p>All jockeys must be licensed and usually are not permitted to bet on a race. An apprentice jockey has a master, who is a horse trainer, and also is allowed to "claim" weight off the horse's back (if a horse were to carry 58 kg, and the apprentice was able to claim 3 kg, the horse would only have to carry 55 kg on its back) in some races. This allowance is adjusted according to the number of winners that the apprentice has ridden. After a 4 year indentured apprenticeship, the apprentice becomes a senior jockey[2] and would usually develop relationships with trainers and individual horses. Sometimes senior jockeys are paid a retainer by an owner which gives the owner the right to insist the jockey rides their horses in races.</p>
</div>

             

            

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





      
    