<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>Hong Kong Horse Racing and Betting</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->
<p>{include file='/home/ah/allhorse/public_html/racetracks/hong_kong_racing/slider.tpl'} </p>


<p>You can bet from your home computer, mobile or tablet at BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>


<h2>Bet on Hong Kong Horse Racing</h2>
<p>Horse racing and betting in Hong Kong is, in a word, amazing.</p> 

<p>First, the Happy Valley Race course, home of the venerable Hong Kong Jockey Club (HKJC) is a fantastic oasis in the hustle and bustle of Hong Kong.  Once you enter the doors, you are in a world class venue with the amazing skyline of Hong Kong city surrounding you.  Second, less than 30 minutes away from the city, another fantastic race track is available in the New Territories, namely the Sha Tin Race Course.</p>

<p>And the betting on horse racing in Hong Kong?  We're talking BIG bucks.  There is so much money wagered on horses in Hong Kong that the HKJC is able to give $1 Billion HK dollars to charity each year and contributes HK$11 billion to Hong Kong's tax revenue.</p>

<p>In 2015, over HK$107.9 BILLION was wagered!  That is almost $14 Billion USD.  With only two tracks (Happy Valley and Sha Tin), Hong Kong Racing enjoyed a 5.8% increase in betting from 2014.  Take all the tracks in North America and the total wagered in 2013 was $10.88 Billion.  Maybe America could learn a thing or two from the Hong Kong Jockey Club about racing?</p>

<p>With about 83 races in the year, there were over two million people who attended the races in person!</p>

<p>The pools are enormous at Sha Tin and Happy Valley, so as a horse bettor, you get access to the types of pools that correspond to higher payouts.</p>

<p>The Hong Kong Derby for 4 year olds is raced at Sha Tin while the Happy Valley race course is home to many other Grade I races.</p>

<p>US Racing is proud to be an ADW website that offers betting on Hong Kong racing tracks.</p>

<p>Sha Tin Racecourse is home of the world's richest day of turf racing, the world's first retractable roof over a parade ring and the world's longest Diamond Vision Television screen. Major racing events include the Longines Hong Kong International Races, the Audemars Piguet QEII Cup, Champions Mile and the BMW Hong Kong Derby.
The Happy Valley Race Course is in the Wan Chai District of Hong Kong and typically holds races on Wednesday nights.  The British developed the race track in 1841 and the HKJC was organized in 1843.</p>
<p>If you want to bet on Hong Kong racing, then you can place your bets at US Racing.  Bet the races at Happy Valley and Sha Tin as if you were there in person. </p>
<p>Want to know the current odds for Hong Kong racing?  Just login to your account and check them out.</p> 
{*
<h2>Watch Hong Kong Horse Racing</h2>
<p>At US Racing, you can place your bet on Hong Kong racing and then watch races in Hong Kong live with our video feeds.</p>
<p>Let the games begin!</p>
*}


<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='/home/ah/allhorse/public_html/racetracks/hong_kong_racing/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
