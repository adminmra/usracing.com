<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>ADWs - Advance Deposit Wagering</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Advance Deposit Wagering - ADWs</strong></p>
<p>Advance Deposit Wagering, also known as &quot;ADWs&quot; and &quot;Account Wagering&quot; is a form of gambling on horse races where the bettor funds his account in &ldquo;advance&rdquo; of wagering on the race. Advance deposits are usually made online at US Racing, for example. The advance deposit wagering platform is the new OTB parlor.<br />
</p>
<p>US Racing is not  a &ldquo;credit shop.&rdquo; Wagers without funding in advance are not permitted and it does not extend credit to its members. All accounts at US Racing must be funded before a member can begin betting on live horse racing.</p>
<p><br />
</p>
<p><strong>NYRA refutes account wagering &lsquo;credit&rsquo; allegations</strong></p>
<p><span class="story">By Paulick Report Staff | </span>10:57 a.m., January 13, 2012</p>
<p>The New York Racing Association issued a response Friday refuting allegations that prompted the New York State Racing and Wagering Board (NYSRWB) to open an investigation into NYRA&rsquo;s account wagering practices.  As the Albany Times-Union reported, the probe was initiated immediately following a letter sent by Robert Megna, chairman of the state&rsquo;s Franchise Oversight Board, to NYSRWB Chairman John Sabini requesting a probe into whether NYRA has allowed Advance Deposit Wagering customers to bet on credit in violation of law.</p>
<p>  NYRA&rsquo;s response appears below.</p>
<p>The funding policies for NYRA&rsquo;s advanced deposit wagering program (NYRA Rewards) are in compliance with the law and pursuant to the procedures approved by the New York State Racing and Wagering Board. We refute any allegations that suggest otherwise, and stand by our advanced deposit wagering account funding practices. NYRA welcomes a review of our advanced deposit wagering account funding practices by the NYSRWB.</p>
<p>NYRA Rewards allows customers to make immediate deposits into their accounts of up to $1,000 per week via an electronic funds transfer, referred to as Express Funding. In order to be eligible for Express Funding, a customer must pass a negative check screening process conducted in accordance with financial industry standards using the latest technology available in the marketplace. NYRA screens all of its existing customers and all new customers for eligibility on each request transaction. This process was approved by the NYSRWB on February 26, 2008, and has been in place and followed by NYRA since.</p>
<p>Mr. Megna&rsquo;s letter asks the NYSRWB to determine whether NYRA has extended credit for any wagering by customers in violation of the Racing, Pari-Mutuel Wagering, and Breeding law. If a customer makes an Express Funding deposit to NYRA with insufficient funds to cover the deposit, NYRA has not extended credit. Rather, the person making the deposit has committed a crime and NYRA has been the victim of fraud. If this occurs, NYRA rigorously pursues recompense from the customer and bank.</p>
<p>Furthermore, NYRA Rewards Express Funding is consistent with industry practices for advanced deposit wagering funding policies. Virtually all industry advanced deposit wagering programs, both within the State of New York , and outside of the State, allow customers to make immediate deposits with similar policies in place to those of NYRA<br />
</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    