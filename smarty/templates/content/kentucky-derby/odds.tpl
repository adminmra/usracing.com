{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

        {* <p>{include file='/home/ah/allhorse/public_html/kd/2019_odds_kd.php'}</p> *}
       {* <p>{include_php file='/home/ah/allhorse/public_html/kd/odds_kd.php'}</p> *}
        <p>{*include_php file='/home/ah/allhorse/public_html/kd/odds_kd_new.php'*}</p>
         {*<p>{include_php file='/home/ah/allhorse/public_html/generated/odds/kd/odds_kd.php'}</p> *}

{*<p>
  <iframe main class="video mobile_video_frame"  height="100%" width="100%" src="https://www.youtube.com/embed/oAqH8sarlTc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen ></iframe>
  <iframe main class="video window_video_frame"  height="100%" width="100%" src="https://www.youtube.com/embed/oAqH8sarlTc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen ></iframe>
  <br/>
  <a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a>
</p>*}

        {* <p>{include_php file="/home/ah/allhorse/public_html/kd/odds_kd_hardcoded.php"}</p> *}
		<p>{include_php file="/home/ah/allhorse/public_html/generated/odds/kd/odds_kd.php"}</p>

        {* <p class="cta-center"><button id="loadMore" class="btn btn-xlrg" style="background: #1571BA !important;">Show
            More</button></p>
			<p>Think you know what trainer will win the Kentucky Derby? You can bet on trainers like <a
            href="/kentucky-derby/trainer-betting">Todd Pletcher</a> and Trainer Odds will be coming soon!</p><br> *}

    <p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on the Kentucky Derby</a></p>
            
        <h2>The {$KD_RUNNING} Kentucky Derby runs on {$KD_DATE}.</h2>{*
 Want some helpful trivia? The Kentucky Derby always takes
          place on the first Saturday in May. This year will certainly feature a large field of horses. Up to 20
          three-year-old colts, fillies and geldings can run is this grand event.
*}

{if $KD_Switch_Early || $KD_Switch_Main || $KD_Switch_Race_Day || $KD_Results}
<p>The Kentucky Derby is a Grade I stakes race for three year-old Thoroughbreds at Churchill Downs {* usually *} run on the first Saturday in May.  {* The race is also known as "The run for the Carnations", "the Test of the Champion " and the "Final Jewel of the Triple Crown". *}  This year, the race {* has been postponed a week and is *} will be held on {$KD_DATE}.  You can watch the Kentucky Derby at 5 p.m. EST on TV on NBC or live stream on NBCSports.com. </p>
{else}
        <p>US Racing provides the earliest Kentucky Derby Futures odds of any website. If you see a horse you would like
          to be added, let us know and we might be able to get it added for you!</p>

        <p>Between now and the Kentucky Derby, the odds will be changing when we add new horses and removing others.
          Remember, when you a place future wager the odds are fixed and all wagers have action.</p>
        
        <p>Good luck and see you on {$KD_DATE} for the Run for the Roses!</p>
{/if}
 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}
