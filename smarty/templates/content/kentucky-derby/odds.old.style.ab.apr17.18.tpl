
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
  <div class="container-fluid">
	
		<a href="/signup?ref=kentucky-derby-odds" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/2017-Kentucky-Derby-odds.jpg" alt="Kentucky Derby Odds"></a>
		
		<a href="/kentucky-derby/odds"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/2017-Kentucky-Derby.jpg" alt="Kentucky Derby Odds">  </p> </a>
	
</div>        
<div id="main" class="container">

<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          
<div class="headline"><h1>Kentucky Derby Odds</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'} </p>

{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}

<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</h2>

<p>The 144th Kentucky Derby will be run on May 5, 2018.  Helpful trivia:  The Derby always takes place on the first Saturday in May!  This year will undoubtedly feature a large field or horses,  up to 20 three-year-old colts, fillies and geldings can run.</p>

<p>US Racing provides the earliest Kentucky Derby Futures odds of any website.  If you see a horse you would like to be added, let us know and we might be able to get it added for you!</p>

<p>Between now and the Kentucky Derby, the odds will be updated by adding new horses and removing others.  Remember, when you place future wagers the odds are all action and fixed. Good luck and see you in May for the Run for the Roses!</p>

<p>{*include file='/home/ah/allhorse/public_html/kd/odds_to_win_998_xml.php'*}</p>
<p>{include file='/home/ah/allhorse/public_html/kd/odds_kd.php'}</p>

<p>Think you know what trainer will win the Kentucky Derby?  You can bet on trainers like <a href="/kentucky-derby/trainer-betting">Todd Pletcher</a> and Trainer Odds will be coming soon!</p><br>

<p align="center"><a href="/signup?ref=kentucky-derby-odds" rel="nofollow" class="btn-xlrg ">Bet Now on the Kentucky Derby</a></p>  <br>
{*
<h2> Kentucky Derby Props</h2>
<p>Great unique bets for extra fun at the Derby.</p>
<p>{include file='/home/ah/allhorse/public_html/kd/odds_926_xml.php'}</p>
<p align="center"><a href="/signup?ref=kentucky-derby-props" class="btn-xlrg ">Bet Now on Kentucky Derby Props</a></p>  
*}


 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row --><p>{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
