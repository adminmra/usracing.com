{assign var="ref" value="kentucky-derby-prep-races"}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav"> 
	<!-- ---------------------- left menu contents ---------------------- --> 
	{include file='menus/kentuckyderby.tpl'} 
	<!-- ---------------------- end left menu contents ------------------- --> 
</div>


<div class="container-fluid">
	
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-prep-races-hero.jpg" alt="Kentucky Derby Betting"></a>
		
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-prep-races-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
	
</div>    



<div id="main" class="container">
	<div class="row">
	
		<div id="left-col" class="col-md-9">
			
			<div class="content"> 
				<!-- --------------------- content starts here ---------------------- -->
				
			{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}
				<div class="headline"><h1>Kentucky Derby Prep Races</h1></div>
			
			<p>	{include_php file='/home/ah/allhorse/public_html/kd/dev-prepraces.php'} </p>
	
       <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on Derby Prep Races Now</a></p> 
    
    <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	 <!-- ------------------------ CTA -------------------------- -->

<div class="calltoaction">
	
	<h2>Bet the<BR>Kentucky Derby<br>Prep Races!</h2>
	<h3>Plus exclusive Derby odds and more.</h3>
	
	{include file="/home/ah/usracing.com/smarty/templates/inc/rightcol-calltoaction-form.tpl"}

	 <!-- ------------------------ CTA ends -------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
