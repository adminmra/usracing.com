{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

<div class="headline"><h1>How to get your Free Bet on the  Kentucky Derby</h1></div>

{* <p>{include file='/home/ah/allhorse/public_html/shared/promos/free-derby-bet.php'} </p> 

<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">I Want My Free Bet</a></p>  
*}
 <div class="justify-left">	 
<p>When you sign up and make your first deposit at BUSR you will get a $10 Free Bet for the Kentucky Derby. This Free Bet is on top of your first deposit bonus of up to {$BONUS_WELCOME}.</p>
<p><strong>  Here is how it works:</strong></p>
<p>
1. Sign up to BUSR and make your first deposit{*  using promocode WELCOME20< *}.<br>
2. Your account will be credited with a {$BONUS_WELCOME_PERCENTAGE} Cash Bonus up to {$BONUS_WELCOME} PLUS a $10 Free Bet.<br>
3. Use your Free Bet for the Kentucky Derby or any other event!</p>
<hr>		

<div class="list-text">	 
<p><strong>Terms and Conditions</strong></p>
 <div class="list">
	<p class="list-elem">The Free Bet is available to New BUSR clients Only</p>
	<p class="list-elem">The $10.00 Free Bet will be available to use in the Sports section.</p>
	<p class="list-elem">The Free Bet will be credited to your racing account with your first deposit of $100 or more.</p>
	<p class="list-elem">Management reserves the right to modify or cancel this promotion at any time.</p>
	<p class="list-elem">General house rules/terms and conditions apply.</p>
</div>

</div>
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}  </p>      
    
  {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}