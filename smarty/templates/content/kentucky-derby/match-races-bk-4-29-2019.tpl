{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <style>
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 80%;
        } 
      }
    </style>
{/literal}
{include file='inc/left-nav-btn.tpl'}



<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/kentuckyderby.tpl'}

</div>
{*

<!-- ---------------------- end left menu contents ------------------- -->   

	

<!--	
<div class="container-fluid">
<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/2017-Kentucky-Derby-matchraces.jpg" alt="Kentucky Derby Betting"></a>



	<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-derby-match-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
</div>-->

	

		<!--------- NEW HERO  ---------------------------------------->
*}

  <div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});" alt="{$hero_alt}">{*Mobile is hidden for now - TF-apr16,19*}
	  <div class="text text-xl">{$h1_hero}<br />{$h1_hero_second}</div>
	  <div class="text text-xl" style="margin-top:0px;"></div>
	  <div class="text text-md">{$h2_hero}<br>
	    <a href="/signup?ref={$ref}">
	    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i> Bet Now</div>
	    </a> </div>
	</div>
    
{*include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"*}
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
           <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Oaks Betting" class="kd_logo img-responsive"></a>
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>

<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} </p>

{* <h2>{include file='/home/ah/allhorse/public_html/kd/year.php'}   Kentucky Derby Props</h2> *}

<p> What are match races and why would you want to bet them at the Kentucky Derby?</p><p>Match races are simple wagers where you bet whether one particular horse will beat another horse.&nbsp; Sometimes this is called head-to-head betting. At the Kentucky Derby, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.</p>


<p>{include file='/home/ah/allhorse/public_html/kd/2018_odds_1042_xml.php'}</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
<p>Once the field for the Kentucky Derby is finalized and post positions drawn, you will be able to see the odds of "match races."  Let's review: what are match races in horse racing?  Answer: Match races are bets that punters can place on individual horses in a particular race.  Instead of betting on whether a horse wins, places or shows, you are instead betting whether one horse will beat another horse!  </p>

{*

<p>Good luck with your bets and when we get closer to the start of the <a href="/kentucky-derby/betting">Kentucky Derby</a>, you will find match race odds in our sportsbook&nbsp;section.</p>

<p>&nbsp;</p>

*}

        </div>
      </div>
    </section>

    <!------------------------------------------------------------------>    

{include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-exciting.tpl"}

<!------------------------------------------------------------------>      

{*
<div id="main" class="container">

<div class="row" >
<div class="col-md-12">
{include file='inc/disclaimer-kd.tpl'}
</div>
</div>

</div><!-- /#container --> 
*}