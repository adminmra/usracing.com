{include file='inc/left-nav-btn.tpl'}
<div id="left-nav"> 
<!-- ---------------------- left menu contents ---------------------- --> 

{include file='menus/kentuckyderby.tpl'} 

<!-- ---------------------- end left menu contents ------------------- --> 
</div>

<div id="main" class="container">
  <div class="row">
    <div id="left-col" class="col-md-9">
      <div class="headline"><h1>Kentucky Derby $10,000 FREE Handicapping Contest</h1></div>
<div class="content"> 
<!-- --------------------- content starts here ---------------------- --> 
        
        
<p>Kentucky Derby $10,000 FREE Handicapping Contest is here! and promises to be more exciting than ever. Only Allhorseracing.com brings to you all of the action of the Kentucky Derby for you to become a winner!  All you have to do is wager in the racebook on any of the following races and you will be automatically entered to win part of our $10,000 prize package with no entry required.</p>
                   
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody style="border: 0;">
<tr>
<td class="content-header-bar">&nbsp;&nbsp;<strong>Friday, May 6th, 2011, KENTUCKY OAKS DAY Race Schedule</strong></td>
</tr>
<tr>
<td>
<table class="data" border="0" cellspacing="0" cellpadding="0" summary="2011 Kentuckey Derby Race Schedule">
<tbody style="border: 0;">
<tr>
<td colspan="8">
<hr class="storytitle" size="1" /></td>
</tr>
<tr class="odd">
<td>Edgewood Stakes</td>
<td>$150,000, 1 1/16 mile (turf), 3-year-old fillies</td>
</tr>
<tr>
<td>Grade III Alysheba Stakes</td>
<td>$100,000, 1 1/16 mile, 3-year-olds and up</td>
</tr>
<tr class="odd">
<td>Grade III Aegon Turf Sprint</td>
<td>$150,000, five furlongs, 3-year-olds and up </td>
</tr>
<tr>
<td>Grade II Louisville Breeders' Cup</td>
<td>$300,000, 1 1/16 mile, fillies and mares 3-year-olds and up </td>
</tr>
<tr class="odd">
<td>Grade III Crown Royal American Turf</td>
<td>$150,000,1 1/16 mile, 3-year-olds</td>
</tr>
<tr>
<td>Grade I Kentucky Oaks</td>
<td>500,000-added, 1 1/8 mile, 3-year-old fillies</td>
</tr>
<tr>
<td colspan="8">
<hr size="1" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<br />

<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody style="border: 0;">
<tr>
<td class="content-header-bar">&nbsp;&nbsp;<strong>Saturday, May 7, 2011, KENTUCKY DERBY DAY Race Schedule</strong></td>
</tr>
<tr>
<td>
<table class="data" border="0" cellspacing="0" cellpadding="0" summary="2011 Kentuckey Derby Race Schedule">
<tbody style="border: 0;">
<tr>
<td colspan="8">
<hr class="storytitle" size="1" /></td>
</tr>
<tr class="odd">
<td>Grade II Churchill Downs</td>
<td>$250,000-added, seven furlongs, 4-year-olds and up</td>
</tr>
<tr>
<td>Grade III La Troienne</td>
<td>$150,000-added, seven furlongs, 3-year-old fillies</td>
</tr>
<tr class="odd">
<td>Grade III Distaff Turf Mile</td>
<td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>
</tr>
<tr>
<td>Grade I Distaff</td>
<td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>
</tr>
<tr class="odd">
<td>Grade I Turf Classic</td>
<td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>
</tr>
<tr>
<td>
<p>Grade I Kentucky Derby</p>
</td>
<td>$2,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>
</tr>
<tr>
<td colspan="8">
<hr size="1" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<br />
{literal}<style type="text/css">
table td { padding:5px 7px; vertical-align:top; width:auto;}
</style>{/literal}
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0; vertical-align:top;" >
<tbody>
<tr>
  <td style="width:13%;"><b>1st Prize</b></td>
  <td>$3000 for the player with the most amount of wagers and highest winning percentage.</td>
</tr>
<tr>
  <td><b>2nd Prize</b></td>
  <td>$1000 for the player with the highest winning percentage with a minimum of 37 wagers</td>
</tr>
<tr>
  <td><b>3rd Prize</b></td>
  <td>$500 for the player with the 2nd highest winning percentage win a minimum of 37 wagers.</td>
</tr>
<tr>
  <td><b>4th Prize</b></td>
  <td>The remainder of the $10,000 Handicapping contest will be a $5,000 pot to be divided evenly amongst the remainder of all players who place a minimum of 37 wagers.</td>
</tr>
<tr>
  <td><b>NOTE:</b></td>
  <td>In the event the 4th place prize is greater than the 1st-3rd place prize payouts, 1st-3rd place will be included in the 4th place prize pot payout in addition to their guaranteed winnings.</td>
</tr>
</tbody>
</table>

<br />
<b>Terms and Conditions:</b><br />
<br />
<ul>
<li>This promotion is open to all active players with no entry required.</li>
<!-- <li>To qualify for this contest, you must place a deposit before April 30th 2011.</li> -->
<li>Only wagers placed in the racebook on May 6th & 7th will be counted towards the 37 wager total.</li>
<li>Prize will be awarded as follows 1st Prize - $3000 for the player with the most amount of wagers and highest winning percentage. 2nd Prize - $1000 for the player with the highest winning percentage with a minimum of 37 wagers. 3rd Prize - $500 for the player with the 2nd highest winning percentage win a minimum of 37 wagers. 4th Prize - The remainder of the $10,000 Handicapping contest will be a $5,000 pot to be divided evenly amongst the remainder of all players who place a minimum of 37 wagers.</li>
<li>In the event the 4th place prize is greater than the 1st-3rd place prize payouts, 1st-3rd place will be included in the 4th place prize pot payout in addition to their guaranteed winnings.</li>
<li>Exotics account for 1 wager regardless of how many parts are involved.</li>
<li>All applicable customers will be credited the morning of May 8th, 2011.</li>
<li>Only one entry per household/account is permitted.</li>
<li>US Racing reserves the right to alter or amend the terms and conditions of this promotion without notice.</li>
<li>Standard rollover requirements apply.</li>
<li>General <a href="/house-rules">house rules</a> apply</li>
</ul>


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-kd.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 