{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

<p>Ready for the spectacle that is the Kentucky Derby? How about placing a wager on your favorite Trainer. From Bob Baffert to Mark Casse, you have all the top trainers available for wagering. Why place a bet on the horse to win the Kentucky Derby when you can do it on the Trainer. </p>
<h2>  Kentucky Derby Trainer Odds</h2>
 <p>{include_php file='/home/ah/allhorse/public_html/kd/odds_trainers_betting.php'}</p>
 {* <p>The odds are currently being updated, please check back shortly.</p> *}
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
{* <P>Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher?  Why Not!</P> *}

<div class="justify-left"><P>How about <a  href="/kentucky-derby/jockey-betting">betting on the jockey who will win the Kentucky Derby</a>? Yes, you can do that too! BUSR offers more wagers on the Kentucky Derby than any other horse betting site.  If you have an idea to bet on the derby, send us an email and we will see if it can be added!  </P>
{* <P>There are over 256 Kentucky Derby and <a title="Triple Crown Betting Odds" href="/bet-on/triple-crown">Triple Crown future wagers </a>available at BUSR.</P> *}
<h2>Handicapping the Trainers of the Kentucky Derby</h2>
<P>Want to start making some future bets on the Field Marshals of Racing who send out their troops (Jockey and Horse) to win the big races?</P></div>
{*At this point, everybody knows Hollywood Bob Baffert:  the perfectly coiffed white hair, the perpetual Cheshire cat grin and those slick, black shades. Baffert has pulled that look off for years and seeing that he is the first trainer to win the Triple Crown in 37 years with Victor Espinoza (<a title="Kentucky Derby Jockey Odds" href="/kentucky-derby/jockey-betting">check out Victor Espinoza’s odds to win the Kentucky Derby</a>!) and the magnificent American Pharoah, the <a title="2015 Sports Illustrated Sportsperson of the Year" href="/news/analysis/the-8th-sign-american-pharoah-snubbed-by-sports-illustrated">snubbed Sports Illustrated “Sportsperson of the Year.” </a></P>
<P>Now, how can you bet on what trainer will win the Kentucky Derby?  Well, if you know the horses he or she is training and you have a feel for what horse will win, then you can more than double your cash by also betting on the Trainer!*}{*Some horse trainers like Todd Pletcher have entered several horses in a race one year only to find no Kentucky Derby win.  (Todd did win in 2010 with Super Saver and Calvin Borel).  Is this the year for Todd Pletcher to win the Kentucky Derby or can Bob Baffert win the Kentucky Derby again?  Is Kiran McLaughlin set with Mohaymen or is there a dark horse that is under the radar?  </P>

<P>Lock in the best odds today and good luck in May!</P> 
*}{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}