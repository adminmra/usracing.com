{literal}
  <style>
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 80%;
        } 
      }
    </style>
{/literal}

{literal}
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "name": "Kentucky Derby Betting",
        "description": "Bet on the Kentucky Derby, odds for Kentucky Derby are online right now! The most Kentucky Derby Bets Anywere.",
        "publisher": {
            "@type": "Organization",
            "name": "US Racing"
        }
    }
    </script>


 <script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Kentucky Derby",
    "item": "https://www.usracing.com/kentucky-derby"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "Kentucky Derby Betting",
    "item": "https://www.usracing.com/kentucky-derby/betting"
  }]
}
</script>   
{/literal}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/kentuckyderby.tpl'}

</div>

<!-- ---------------------- end left menu contents ------------------- -->   
{*
		<!-- <div class="container-fluid"> <a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-derby-betting-hero.jpg" alt="Kentucky Derby Betting"></a>

				<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-derby-betting-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
        </div>  
        -->
*}
<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top:0px;"></div>
  <div class="text text-md">{$h2}<br>
    <a href="/signup?ref={$ref}">
    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i> {$signup_cta}</div> </a> </div>
</div>	
{*include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"*}

<!------------------------------------------------------------------>    
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
           <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Betting" class="kd_logo img-responsive"></a>
          <h1 class="kd_heading">Kentucky Derby Betting</h1>
          <h3 class="kd_subheading">With the Most Derby Bet Options Anywhere</h3>
<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} </p>
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>
 <h2>How to Place your Kentucky Derby Bet at BUSR</h2>
<p>{include file='/home/ah/allhorse/public_html/kd/video-how-to.php'} </p>
<p><a href="/kentucky-derby/betting">Betting on the Kentucky Derby</a> rivals the excitement of the Super Bowl that it generates each year.  Although the race is only 2 minutes long, it consistently draws the attention of horse racing fans from around the world as well as people whose only knowledge of horse racing comes from "The Kentucky Derby." From small bets between friends at Kentucky Derby parties, to the big money horse bets at <a title="Off Track Horse Betting" href="/off-track-betting">offtrack horse betting</a> parlours, casinos in Las Vegas and online horse betting sites. Placing a wager on the Derby is a staple of everyone's bucket list. </p>
<p>Known as “The Most Exciting Two Minutes in Sports” for its approximate time length, the Kentucky Derby is the first leg of the Triple Crown in the United States. Every year, fans and celebrities descend on Louisville to take part of the spectacle at Churchill Downs. The Kentucky Derby is one of the crown jewels of the mystical (and maddening) <a title="Triple Crown" href="/triple-crown">Triple Crown</a> which includes the the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a> which is located at Pimlico Race Track in Baltimore, Maryland and the <a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a> located at Belmont Park in Elmont, New York.</p>
<p><a title="Kentucky Derby" href="/kentucky-derby">The Kentucky Derby</a> is the most famous graded stake horse race for three-year-old thoroughbred horses and it takes place in Louisville, Kentucky on the first Saturday in May each year. Capping the two-week-long Kentucky Derby Festival, the Race for the Roses currently covers one and one-quarter miles (2.012 km) at <a title="Churchill Downs" href="/churchill-downs">Churchill Downs</a>.</p>

<p><a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive" src="/img/kentuckyderby/kentucky-derby-betting.jpg" alt="Kentucky Derby Betting">  </a></p>
{*

<h2>Kentucky Derby Facts</h2>


<p><a href="/">US Racing</a>, an online horse racing service, compiled the following Kentucky Derby facts for horse racing fans.

</p>



<o><b>The Crowd</b></o>



<p>In 2014, there was a crowd of 164,905 which almost matched the record of 165,307 in 2012.  If we have sunny weather, we will break 165,000 with people cheering American Pharaoh and Dortmund to victory!</p>



<p><b>The Booze</b></p>



<p>122,000 Mint Juleps

475,000 beers sold in Grand Stands and Infield</p>



<p><b>The Bling</b></p>



<p>90 Woodford Reserve $1,000 Mint Julep Cups

10 Legendary Woodford Reserve Cups – starting bid $2,000

Ingredients: Ice made from pure mineral water sourced from glacial ice of Nova Scotia. Gold dusted mint from Woodford County, Kentucky and Woodford Reserve Distiller’s Select Bourbon.  Hmm, for that much money you could get a group of your friends feeling right and <a title="make a great mint julep from scratch!" href="/mint-julep-recipe">make great a mint julep from scratch</a></p>



<p><b>The Benjamins</b></p>



<p>$345 Million in revenue for Kentucky

$65 bucks to get into Churchill Downs

$5,000 to get in like a Playha</p>





<p><b>The View</b></p>

<p>15,224 square foot HD Video Board installed at Churchill Downs- $12 Million bucks for a TV!  That’s larger than 3 NBA basketball courts—combined!</p>







<h2>My First Kentucky Derby Bet</h2>

<p><strong>By the US Racing Team</strong></p>

<p>I remember placing my first bet at the Kentucky Derby online. </p>

<p>It was in 2005, and I decided to bet on a horse called Giacomo being ridden by Hall of Fame jockey, Mike Smith.  Giacomo was certainly not a favorite. </p>

<p>What were the odds?  50-1.  Yes, the same odds as Mine that Bird who won in 2009! </p>

<p>The only horse with worse odds who won the derby was Donerail who started at 91-1!  So, odds don’t tell the full story with the horse and jockey who end up winning the garland of red roses in Lousiville.</p>

<p>Anyway, Giacomo was named after the singer Sting’s son. Jerry Moss was the owner - Jerry Moss and Herb Albert (yes, of the Whipped Cream and Other Delights!) were the founders of A&amp;M Records. Sting performed and recorded under that label as a solo act and also when he was in the rock group The Police.</p>

<p>Giacomo is Italian for James.  Ironically, Giacomo has a half-brother named Tiago which in Portugese means James!</p>

<p>But, I digress. . .</p>

<p>So, why did I bet on Giacomo at that year’s Derby? </p>

<p>Let’s put it this way: I’m a sucker for the underdog.  If there was a horse called Rocky, I would have bet on him based on principle. </p>

<p>I didn’t bet on Charismatic at the 1999 Kentucky Derby (I would have if I was there- he was a long shot, too—31-1 odds) but I bet on Lemon Drop Kid at the 1999 Belmont Stakes. </p>

<p>It’s not that I wasn’t on the Charismatic bandwagon—I  wasn’t a Triple Crown hater. </p>

<p>I bet Lemon Drop Kid because my sister loved lemon drops (I think it was the Cosmopolitan drink of ’99 before Sex and the City ushered in Cosmos).  I wasn’t following racing back then—I didn’t know Chris Antley’s story and Charismatic’s, either. </p>

<p>So, now the Derby is coming close.   The first Saturday in May, it’s easy to remember.  And it always takes place at Churchill Downs.</p>

<p>I hope that if you are betting on the Kentucky Derby for the first time you will consider a long shot.  Bet for the underdog.  Sometimes, he may not have fleas.  Sometimes, he might just rise to the occasion.  And sometimes. . .  just  sometimes, a 50-1 long shot will put a bankroll in your pocket!</p>

<p>Good luck with your bets and everybody at US Racing hopes you pick a winner!  (If you happen to lose, we wish you have an extra mint julep at arm’s length.).<br />

</p>

<p></p>

<p>Check out the <a title="Kentucky Derby Prep Race Schedule" href="/kentucky-derby/prep-races">Kentucky Derby Prep Race Schedule</a></p>


<h2>Kentucky Derby Prep Race Schedule</h2>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">

                                  <tr>

                                    <td>Grade II Churchill Downs</td>

                                    <td>$250,000-added, seven furlongs, 4-year-olds and up</td>

                                  </tr>

                                  <tr>

                                    <td>Grade III La Troienne</td>

                                    <td>$150,000-added, seven furlongs, 3-year-old fillies</td>

                                  </tr>

                                  <tr>

                                    <td>Grade III Distaff Turf Mile</td>

                                    <td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>

                                  </tr>

                                  <tr>

                                    <td>Grade I Distaff</td>

                                    <td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>

                                  </tr>

                                  <tr>

                                    <td>Grade I Turf Classic</td>

                                    <td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>

                                  </tr>

                                  <tr>

                                    <td>Grade I Kentucky Derby</td>

                                    <td>$3,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>

                                  </tr>

                                  <tr>

                                    <td colspan="2" class="center">&nbsp;</td>

                                  </tr>                                   

                                </table>





<h2>   {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby TV Schedule</h2>



<p><table  width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Kentucky Derby TV Schedule" title="Kentucky Derby Betting - TV Schedule">

<tbody><tr><th>Date</th>



  <th>Races</th>

    <th width="97">Network</th>

  </tr><!-- <tr>

    <td valign="top">March 26</td>



    <td>* Louisiana Derby, TBA</td>

  <td>USA Network</td>



  </tr> -->

  <!-- <tr class="odd">

    <td valign="top">April 9 </td>

    <td>* Santa Anita Derby, TBA</td>



    <td>NBC</td>

  </tr> -->

 

  <tr  >

    <td  >{include file='/home/ah/allhorse/public_html/kd/racedate.php'}</td>



    <td>* {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby, TBA<br></td>

    <td>NBC</td>

  </tr>

  </tbody></table></p>

<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>  

*}
{*<h2 class="title">Kentucky Derby Contenders</h2>*}
{*include file="includes/ahr_block_KD-contenders_2011.tpl" *}
        </div>
</div>
</section>
<!------------------------------------------------------------------>    

 
{include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-exciting.tpl"}

<!------------------------------------------------------------------>      

{*
<div id="main" class="container">

<div class="row" >
<div class="col-md-12">
{include file='inc/disclaimer-kd.tpl'}
</div>
</div>


</div><!-- /#container --> 
*}

