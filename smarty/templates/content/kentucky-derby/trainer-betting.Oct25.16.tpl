
<!-- --------------------- content starts here ---------------------- -->


{* {include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}  *}

{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}
<h3>Bet on your Favorite Trainer to win the Kentucky Derby at US Racing!</h3>

<P>US Racing is proud to announce Kentucky Derby Trainer Betting!</P>

<P>Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher?  Why Not!  </P>

<P>How about betting on the jockey who will win the Kentucky Derby? Yes, you can do that too! US Racing offers more wagers on the Kentucky Derby than any other horse betting site.  If you have an idea to bet on the derby, send us an email and we will see if it can be added!  </P>

<P>There are over 285 Kentucky Derby future wagers, jockey future wagers as well as over 285 different bets on the Kentucky Derby at US Racing.</P>

<h2>  Kentucky Derby Odds</h2>
        
                {include file='/home/ah/allhorse/public_html/kd/odds_winning_trainers_1012_xml.php'}
                
                Handicapping the Trainers of the Kentucky Derby

<P>First, it is [X php inc] days till the first Saturday in May and the Run for the Roses but don’t let that stop you from making some future bets on the Field Marshals of Racing who send out their troops (Jockey and Horse) to win the big races.</P>

<P>At this point, everybody knows Hollywood Bob Baffert:  the perfectly coiffed white hair, the perpetual Cheshire cat grin and those slick, black shades. Baffert has pulled that look off for years and seeing that he is the first trainer to win the Triple Crown in 37 years (link) with Victor Espinoza (check out Victor Espinoza’s odds to win the Kentucky Derby!) and the magnificent American Pharoah, the snubbed Sports Illustrated “Sportsperson of the Year.” (link to Derek blog)</P>

<P>No, how can you bet on what trainer will win the Kentucky Derby?  Well, if you know the horses he or she is training and you have a feel for what horse will win, then you can more than double your cash by also betting on the Trainer!</P>

<P>Some horse trainers like Todd Pletcher have entered several horses in a race one year only to find no Kentucky Derby win.  (Todd did win in 2010 with Super Saver and Calvin Borel).  Is this the year for Todd Pletcher to win the Kentucky Derby or can Bob Baffert win the Kentucky Derby again?  Is Kiran McLaughlin set with Mohaymen or is there a dark horse that is under the radar?  </P>

<P>Lock in the best odds today and good luck in May!</P>




 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-kd.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 