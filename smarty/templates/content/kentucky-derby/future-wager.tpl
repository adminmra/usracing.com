{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}
{*<div class="tag-box tag-box-v4">
<p>The Kentucky Derby and Kentucky Oaks Future Wagers provide fans an opportunity to wager on potential competitors!  Odds can sometimes be considerably more attractive than those available on the day of the Kentucky Derby itself! So go for a dip, and get in on the action!</p></div>
*}
<div class="justify-left"> <div class="headline"><h2>Future Wager Pools for the {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby</h2></div>
<p>Every year <a href="/churchill-downs">Churchill Downs</a> gives people the  opportunity to bet early on the potential Kentucky Derby horse they think will earn a spot in the starting gate and go on to win the Kentucky Derby race. These future wagers  offer larger payouts than betting the same horse on race day because it is not yet known which horses will  qualify to run in the race. This future wager offers 24 potential contenders to bet on. 23 are individually named horses and the 24th option is a bet on “all others”. A bet on “all others” will win if any horse other than the 23 individually named in this pool wins the Kentucky Derby.</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">
            <tr>
              <td><strong>Kentucky Derby Pool 1</strong></td>
              <td>November 22 - 25</td>
            </tr>
            <tr>
              <td><strong>Kentucky Derby Pool 2</strong></td>
              <td>February 8 - 10</td>
            </tr>
            <tr>
              <td><strong>Kentucky Derby Pool 3</strong></td>
              <td>March 9 - 11</td>
            </tr>
            <tr>
              <td><strong>Kentucky Derby Pool 4</strong></td>
              <td>April 6 - 8</td>
            </tr> </table>
        <p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>  
        <p><strong>What is a Kentucky Derby Future Wager?</strong> </p>
        
<p>By definition a "future" wager is generally something that happens in the future, you make the bet long before the sporting event sometimes weeks or months in advance.   In the case of the Kentucky Derby Future Wager, it provides fans with the opportunity to wager on contenders at odds that could be more attractive than those available on the day of the Kentucky Derby.</p>
 <p>As in past years, wagering on each of the Kentucky Derby Future Wager pools will open at noon (all times Eastern) on the first day of the pool's Friday-Sunday run. Betting will conclude in the four Derby pools will conclude at 6 p.m. on Sunday. The Kentucky Derby  Future Wagers are minimum $2 wagers and win bets only. The bets offer the fan the opportunity to wager on contenders for the Kentucky Derby at odds that could be greater than those they would receive on the their respective race days. There are no refunds on any of these Kentucky Derby Future Wagers.</p>
<p>The {include_php file='/home/ah/allhorse/public_html/kd/running.php'}  running of the Kentucky Derby – a 1 ¼-mile test for 3-year-olds and America's greatest race – is scheduled for {include_php file='/home/ah/allhorse/public_html/kd/date.php'} at Churchill Downs. The $500,000-added Kentucky Oaks (GI), a 1 1/8-mile race restricted to 3-year-old fillies, will be run over the track on {include_php file='/home/ah/allhorse/public_html/kd/racedate_ko.php'}.</p>
 <p>A total of $1,056,520 was wagered on the three Kentucky Derby Future Wager pools in 2008 as total betting on the wager topped the $1 million mark for the ninth consecutive time in its 10-year history. Wagering on the three Kentucky Oaks pools in 2008 totaled $174,838, the second highest three-pool total in the history of the bet.</p>
<p>"A wide open and very competitive Kentucky Derby picture is developing and Churchill Downs is excited to again offer three pools with plenty of betting value in the Kentucky Derby Future Wager, which has become part of the Derby tradition over the past decade," said Churchill Downs General Manager Jim Gates. "After careful consideration we decided to return the Kentucky Oaks Future Wager to a single-pool format and believe the March pool offers the best opportunity to present a strong roster of serious Kentucky Oaks wagering interests. We think the single-pool format will provide the Kentucky Oaks Future Wager with its best opportunity to grow and present racing fans a chance to have a lot of fun while searching for the winner of what has become the nation's most prestigious event for 3-year-old fillies."</p>
<h2>Kentucky Derby Future Wager Statistics</h2>
<p>{include file='/home/ah/allhorse/public_html/kd/futurewagerstatistics.php'} </p>
<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>  
<h2>Kentucky Derby Future Wager Mutuel Payouts</h2>
<p>{include file='/home/ah/allhorse/public_html/kd/futurewagerpayouts.php'} </p>
<h2>Kentucky Oaks Future Wager Statistics</h2>
<p>{include file='/home/ah/allhorse/public_html/kd/futurewagerstatistics_ko.php'} </p>
<h2>Kentucky Oaks Future Wager Mutuel Payouts</h2> 
<p>{include file='/home/ah/allhorse/public_html/kd/futurewagerpayouts_ko.php'} </p>
<p><small>*Favorite **KOFW single pool wagering record ***KOFW win payout record ****KOFW total wagering (three pools) record (f) – Mutuel field, was the betting favorite on Derby Day, although he had not been favored in any of the three Kentucky Derby Future Wager pools. The mutuel field, or all others, was favored in Pools 1 and 2, while Curlin, the then-unbeaten winner of the Arkansas Derby (GII) and the eventual third-place finisher in the Kentucky Derby, was the favorite in the third and final pool. Kentucky Oaks winner Rags to Riches was a strong betting favorite on Kentucky Oaks Day, and was favored in two of 2007's three Kentucky Oaks Future Wager pools. The mutuel field was a narrow 3-1 choice over Rags to Riches (7-2) in last year's Pool 1.</small></p></div>
<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>  

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}
