

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<div class="container-fluid">
	
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-trainer-betting-hero.jpg" alt="Kentucky Derby Odds: Trainers"></a>
		
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-trainer-betting-small-hero.jpg" alt="Kentucky Derby Odds: Trainers">  </p> </a>
	
</div>

      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          

<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'} 

{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}
<div class="headline"><h1>Kentucky Derby Odds: Trainers</h1></div>
<p><strong>Bet on your Favorite Trainer to win the Kentucky Derby at BUSR!</strong></p>

{* <P>US Racing is proud to announce Kentucky Derby Trainer Betting!</P> *}

<P>Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher?  Why Not!  </P>

<P>How about <a title="Kentucky Derby Jockey Odds" href="/kentucky-derby/jockey-betting">betting on the jockey who will win the Kentucky Derby</a>? Yes, you can do that too! BUSR offers more wagers on the Kentucky Derby than any other horse betting site.  If you have an idea to bet on the derby, send us an email and we will see if it can be added!  </P>

<P>There are over 256 Kentucky Derby and <a title="Triple Crown Betting Odds" href="/bet-on/triple-crown">Triple Crown future wagers </a>available at BUSR.</P>

<h2>  Kentucky Derby Trainer Odds</h2>
        
                <p>{include file='/home/ah/allhorse/public_html/kd/odds_winning_trainers_1110_xml.php'}</p>
                <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Derby Trainers Now</a></p>
<h2>Handicapping the Trainers of the Kentucky Derby</h2>

<P>Want to start making some future bets on the Field Marshals of Racing who send out their troops (Jockey and Horse) to win the big races?</P>

<P>At this point, everybody knows Hollywood Bob Baffert:  the perfectly coiffed white hair, the perpetual Cheshire cat grin and those slick, black shades. Baffert has pulled that look off for years and seeing that he is the first trainer to win the Triple Crown in 37 years with Victor Espinoza (<a title="Kentucky Derby Jockey Odds" href="/kentucky-derby/jockey-betting">check out Victor Espinoza’s odds to win the Kentucky Derby</a>!) and the magnificent American Pharoah, the <a title="2015 Sports Illustrated Sportsperson of the Year" href="/blog/the-8th-sign-american-pharoah-snubbed-by-sports-illustrated">snubbed Sports Illustrated “Sportsperson of the Year.” </a></P>

<P>Now, how can you bet on what trainer will win the Kentucky Derby?  Well, if you know the horses he or she is training and you have a feel for what horse will win, then you can more than double your cash by also betting on the Trainer!</P>

<P>Some horse trainers like Todd Pletcher have entered several horses in a race one year only to find no Kentucky Derby win.  (Todd did win in 2010 with Super Saver and Calvin Borel).  Is this the year for Todd Pletcher to win the Kentucky Derby or can Bob Baffert win the Kentucky Derby again?  Is Kiran McLaughlin set with Mohaymen or is there a dark horse that is under the radar?  </P>

<P>Lock in the best odds today and good luck in May!</P>



 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->


<div id="right-col" class="col-md-3">
	
	
	 <!-- ------------------------ CTA -------------------------- -->

<div class="calltoaction">
	
	<h2>Bet the<BR>Kentucky Derby<br>Trainers!</h2>
	<h3>Plus exclusive Derby odds and more.</h3>
	
	{include file="/home/ah/usracing.com/smarty/templates/inc/rightcol-calltoaction-form.tpl"}

	 <!-- ------------------------ CTA ends -------------------------- -->

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
