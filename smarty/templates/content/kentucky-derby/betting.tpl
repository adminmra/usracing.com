{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}
<p>{include_php file='/home/ah/allhorse/public_html/generated/odds/kd/odds_kd.php'}</p>
{*<p>{include_php file='/home/ah/allhorse/public_html/kd/odds_kd.php'}</p>*} 
<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on the Kentucky Derby</a></p>

<div class="justify-left">
<p><strong>Where can I  bet on the {$KD_YEAR} Kentucky Derby? </strong> <a href="/signup?ref={$ref}">BUSR</a> offers Futures Wagers, Match Races and all track betting options for the Kentucky Derby. This year the race will be  held on {$KD_DATE}.</p> 


<p><strong>How can I bet on the Kentucky Derby {$KD_YEAR}? </strong> There are many different ways to place your wager on the Derby. {* At BUSR, they offer their "EZ Bet App" for newbies.  *} You just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR Sportsbook, you can bet on Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. You will also find all kinds of bets including  <a href="/kentucky-derby/future-wager"> Future Wagers</a> where you can bet the Kentucky Derby weeks, if not months, in advance.</p>

<p><strong>Where can I Watch the {$KD_YEAR} Kentucky Derby? </strong> You can watch the live coverage starting at 12:30 p.m. EST on TV on NBC or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Kentucky Derby today? </strong> Visit <a href="/kentucky-derby/odds"> Kentucky Derby Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>
</div>
<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>

{*
<p><a href="/kentucky-derby/betting">Betting on the Kentucky Derby</a> rivals the excitement of the Super Bowl that it generates each year.  Although the race is only 2 minutes long, it consistently draws the attention of horse racing fans from around the world as well as people whose only knowledge of horse racing comes from "The Kentucky Derby." From small bets between friends at Kentucky Derby parties, to the big money horse bets at <a title="Off Track Horse Betting" href="/off-track-betting">offtrack horse betting</a> parlors, casinos in Las Vegas and online horse betting sites. Placing a wager on the Derby is a staple of everyone's bucket list. </p>

<p>Known as “The Most Exciting Two Minutes in Sports” for its approximate time length, the Kentucky Derby is the first leg of the Triple Crown in the United States. Every year, fans and celebrities descend on Louisville to take part of the spectacle at Churchill Downs. The Kentucky Derby is one of the crown jewels of the mystical (and maddening) <a title="Triple Crown" href="/triple-crown">Triple Crown</a> which includes the the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a> which is located at Pimlico Race Track in Baltimore, Maryland and the <a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a> located at Belmont Park in Elmont, New York.</p>

<p><a title="Kentucky Derby" href="/kentucky-derby">The Kentucky Derby</a> is the most famous graded stake horse race for three-year-old thoroughbred horses and it takes place in Louisville, Kentucky on the first Saturday in May each year. Capping the two-week-long Kentucky Derby Festival, the Race for the Roses currently covers one and one-quarter miles (2.012 km) at <a title="Churchill Downs" href="/churchill-downs">Churchill Downs</a>.</p>
*}


{*<h2>Kentucky Derby Facts</h2>
<p><a href="/">US Racing</a>, an online horse racing service, compiled the following Kentucky Derby facts for horse racing fans.</p><o><b>The Crowd</b></o><p>In 2014, there was a crowd of 164,905 which almost matched the record of 165,307 in 2012.  If we have sunny weather, we will break 165,000 with people cheering American Pharaoh and Dortmund to victory!</p>

<p><b>The Booze</b></p>

<p>122,000 Mint Juleps475,000 beers sold in Grand Stands and Infield</p>

<p><b>The Bling</b></p>

<p>90 Woodford Reserve $1,000 Mint Julep Cups10 Legendary Woodford Reserve Cups – starting bid $2,000Ingredients: Ice made from pure mineral water sourced from glacial ice of Nova Scotia. Gold dusted mint from Woodford County, Kentucky and Woodford Reserve Distiller’s Select Bourbon.  Hmm, for that much money you could get a group of your friends feeling right and <a title="make a great mint julep from scratch!" href="/mint-julep-recipe">make great a mint julep from scratch</a></p>

<p><b>The Benjamins</b></p>

<p>$345 Million in revenue for Kentucky$65 bucks to get into Churchill Downs$5,000 to get in like a Playha</p>

<p><b>The View</b></p>

<p>15,224 square foot HD Video Board installed at Churchill Downs- $12 Million bucks for a TV!  That’s larger than 3 NBA basketball courts—combined!</p><h2>My First Kentucky Derby Bet</h2><p><strong>By the US Racing Team</strong></p>

<p>I remember placing my first bet at the Kentucky Derby online. </p>

<p>It was in 2005, and I decided to bet on a horse called Giacomo being ridden by Hall of Fame jockey, Mike Smith.  Giacomo was certainly not a favorite. </p>

<p>What were the odds?  50-1.  Yes, the same odds as Mine that Bird who won in 2009! </p>

<p>The only horse with worse odds who won the derby was Donerail who started at 91-1!  So, odds don’t tell the full story with the horse and jockey who end up winning the garland of red roses in Lousiville.</p>

<p>Anyway, Giacomo was named after the singer Sting’s son. Jerry Moss was the owner - Jerry Moss and Herb Albert (yes, of the Whipped Cream and Other Delights!) were the founders of A&amp;M Records. Sting performed and recorded under that label as a solo act and also when he was in the rock group The Police.</p>

<p>Giacomo is Italian for James.  Ironically, Giacomo has a half-brother named Tiago which in Portugese means James!</p>

<p>But, I digress. . .</p>

<p>So, why did I bet on Giacomo at that year’s Derby? </p>

<p>Let’s put it this way: I’m a sucker for the underdog.  If there was a horse called Rocky, I would have bet on him based on principle. </p>

<p>I didn’t bet on Charismatic at the 1999 Kentucky Derby (I would have if I was there- he was a long shot, too—31-1 odds) but I bet on Lemon Drop Kid at the 1999 Belmont Stakes. </p>

<p>It’s not that I wasn’t on the Charismatic bandwagon—I  wasn’t a Triple Crown hater. </p>

<p>I bet Lemon Drop Kid because my sister loved lemon drops (I think it was the Cosmopolitan drink of ’99 before Sex and the City ushered in Cosmos).  I wasn’t following racing back then—I didn’t know Chris Antley’s story and Charismatic’s, either. </p>

<p>So, now the Derby is coming close.   The first Saturday in May, it’s easy to remember.  And it always takes place at Churchill Downs.</p>

<p>I hope that if you are betting on the Kentucky Derby for the first time you will consider a long shot.  Bet for the underdog.  Sometimes, he may not have fleas.  Sometimes, he might just rise to the occasion.  And sometimes. . .  just  sometimes, a 50-1 long shot will put a bankroll in your pocket!</p>

<p>Good luck with your bets and everybody at US Racing hopes you pick a winner!  (If you happen to lose, we wish you have an extra mint julep at arm’s length.).<br /></p>

<p></p>

<p>Check out the <a title="Kentucky Derby Prep Race Schedule" href="/kentucky-derby/prep-races">Kentucky Derby Prep Race Schedule</a></p>
<h2>Kentucky Derby Prep Race Schedule</h2><table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered">                                  <tr>                                    <td>Grade II Churchill Downs</td>                                    <td>$250,000-added, seven furlongs, 4-year-olds and up</td>                                  </tr>                                  <tr>                                    <td>Grade III La Troienne</td>                                    <td>$150,000-added, seven furlongs, 3-year-old fillies</td>                                  </tr>                                  <tr>                                    <td>Grade III Distaff Turf Mile</td>                                    <td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>                                  </tr>                                  <tr>                                    <td>Grade I Distaff</td>                                    <td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>                                  </tr>                                  <tr>                                    <td>Grade I Turf Classic</td>                                    <td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>                                  </tr>                                  <tr>                                    <td>Grade I Kentucky Derby</td>                                    <td>$3,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>                                  </tr>                                  <tr>                                    <td colspan="2" class="center">&nbsp;</td>                                  </tr>                                                                   </table><h2>   {$KD_DATE}  Kentucky Derby TV Schedule</h2><p><table  width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Kentucky Derby TV Schedule" title="Kentucky Derby Betting - TV Schedule"><tbody><tr><th>Date</th>  <th>Races</th>    <th width="97">Network</th>  </tr><!-- <tr>    <td valign="top">March 26</td>    <td>* Louisiana Derby, TBA</td>  <td>USA Network</td>  </tr> -->  <!-- <tr class="odd">    <td valign="top">April 9 </td>    <td>* Santa Anita Derby, TBA</td>    <td>NBC</td>  </tr> -->   <tr  >    <td  >{$KD_DATE}</td>    <td>* {$KD_DATE} Kentucky Derby, TBA<br></td>    <td>NBC</td>  </tr>  </tbody></table></p><p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Derby</a></p>  *}
{*<h2 class="title">Kentucky Derby Contenders</h2>*}
{*include file="includes/ahr_block_KD-contenders_2011.tpl" *}


{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}
