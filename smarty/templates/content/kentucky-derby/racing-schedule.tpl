{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

        <p>The Kentucky Derby day is full of action and excitement. From tradition of sipping on a few Mint Julep's to
          singing of the National Anthem, to the final draping of the Garland of Roses on the winning horse, there is
          something for everyone. Although the main attraction is the Kentucky Derby race itself, the track hosts a
          total of 14 races during the day.</p>


        <h2>{$KD_DATE} Racing Schedule</h2>



        <p>Below is a list of all the races for this year's Kentucky Derby. Looking to place a wager on the big race?
          Check out the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include
          file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show,
          trifecta and more!</p>
        <div>
          <table class="data table table-condensed table-striped table-bordered"
            summary=" Kentuckey Derby Race Schedule" border="1" cellpadding="0" cellspacing="0">
            <caption>
              {$KD_DATE} KENTUCKY DERBY DAY Race Schedule
            </caption>
            <tr>
              <td width="160">Grade II Churchill Downs</td>
              <td width="447">$250,000 - added, seven furlongs, 4-year-olds and up</td>
            </tr>
            <tr>
              <td>Grade III La Troienne</td>
              <td>$150,000 - added, seven furlongs, 3-year-old fillies</td>
            </tr>
            <tr>
              <td>Grade III Distaff Turf Mile</td>
              <td>$150,000 - added, one mile, fillies and mares, 3-year-olds and up</td>
            </tr>
            <tr>
              <td>Grade I Distaff</td>
              <td>$300,000 - added, seven furlongs, fillies and mares, 4-year-olds and up</td>
            </tr>
            <tr>
              <td>Grade I Turf Classic</td>
              <td>$1,000,000 - added, 1 1/8 mile, 3-year-olds and up</td>
            </tr>
            <tr>
              <td>
                <p>Grade I Kentucky Derby</p>
              </td>
              <td>$3,000,000 - guaranteed, 1 1/4 mile, 3-year-olds</td>
            </tr>
            </tbody>

          </table>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>{include file='/home/ah/usracing.com/smarty/templates/includes/apj_block_kd_racing_schedule.tpl'} </p>
       {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}