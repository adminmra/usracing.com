{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}


{* <h2>{include file='/home/ah/allhorse/public_html/kd/year.php'}   Kentucky Derby Props</h2> *}
<h2>{$KD_YEAR}  Kentucky Derby Match Races</h2>
<div class="justify-left"><p><strong>Make head-to-head bets on horses at the Derby! </strong></p>
<p> What are match races and why would you want to bet them at the Kentucky Derby?</p><p>Match races are simple wagers where you bet whether one particular horse will beat another horse.&nbsp; Sometimes this is called head-to-head betting. At the Kentucky Derby, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.</p>
</div>

{*<p>{include file='/home/ah/allhorse/public_html/kd/2018_odds_1042_xml.php'}</p>*}
<p>{include_php file='/home/ah/allhorse/public_html/kd/odds_match_races.php'}</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
<div class="justify-left">
<p>Once the field for the Kentucky Derby is finalized and post positions drawn, you will be able to see the odds of "match races."  Let's review: what are match races in horse racing?  Answer: Match races are bets that punters can place on individual horses in a particular race.  Instead of betting on whether a horse wins, places or shows, you are instead betting whether one horse will beat another horse!  </p>
<p>Good luck with your bets and when we get closer to the start of the <a href="/kentucky-derby/betting">Kentucky Derby</a>, you will find match more race odds posted online.</p></div>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

{*

<p>Good luck with your bets and when we get closer to the start of the <a href="/kentucky-derby/betting">Kentucky Derby</a>, you will find match race odds in our sportsbook&nbsp;section.</p>

<p>&nbsp;</p>

*}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}