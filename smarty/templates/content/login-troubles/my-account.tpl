<div id="main" class="container">
<div class="row">
<div id="full-col" class="col-md-12">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 

<h1>US Racing Membership Update</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->








<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->







<p>Dear US Racing Member,</p><P>If you opened an account with US Racing before April 1, 2015 you will need to reregister in order to login and wager on horses. Although attempts were made to contact members via email, there is a chance that our emails were filtered to your spam folder.  We apologize for this inconvenience. But you can be betting online again in a matter of minutes!</p>
<p>If you had a zero balance in your old US Racing account, simply sign up today and we will get your ready for the races!  It will take less than 5 minutes to open and fund your account with Visa or Mastercard.</p>     
<p>In order to maintain the integrity of your account, our software provider requires this step which should take no longer than 5 minutes of your time. 
	
	
	 Simply click <a href="/signup/">HERE</a> or the <a href="/signup/">SIGN UP</a> button that you will see at the top of every page and enter your information.
		 US Racing members are entitled to a $150 cash bonus on your first deposit!</p> 


<h2>FAQS</h2>

<p>What if I had a balance in my account?</p>

			<p>Your funds are safe!  To access your funds from your previous US Racing  Account, please go to this address:


</p>
	<blockquote><p>www.tvg.com</p>
    	<p>In the login field, enter your previous US Racing Account number and PIN.  When you login, you will see any balance you had in your account.  To withdraw your funds, simply click on the MY ACCOUNT link in the upper righthand corner of the website and select WITHDRAW FUNDS.  Once you make a withdraw, you will receive a check in approximately 7 business days.</p></blockquote>


            			<p><strong>What do I do if I want to bet at US Racing?
            			</strong></p>

		<blockquote>	<p>To start betting again, you will need to re-register.  It?s easy, fast and secure! Please <a href="http://www.usracing.com/signup/">register here.</a> </p></blockquote>
            			<p><strong>Why do I have to re-register?</strong>
</p>

			<blockquote><p>In order to maintain the security of your personal information, we are required by our processor to have you sign up a new account.  It's easy, fast and secure and will take only 2 minutes of your time!  Please visit www.usracing.com and click on the Sign Up button.
</p></blockquote>

</p>

	
            			<p><strong>Why did you switch software providers?</strong></p>

		<blockquote>	<p>To provide our members with access to more tracks and betting options, the decision was made to switch software and gaming providers.  We also had members who wanted to be able to make deposits with other credit cards ? now you can!  Besides great racing from around the world, members will also be able to bet on greyhound races as well as special events during the course of the year.</p></blockquote>
				<p><strong>Do you offer mobile betting?</strong>
</p>

			<p><blockquote>Yes.  Members are able to wager with their mobile phone or tablet devices.</blockquote>
</p>		<p><strong>Can I use MasterCard and VISA to deposit online?</strong></p>
</p>

			<blockquote><p>Yes you can!  There are several methods to make deposits online or over the telephone.
</p>	</blockquote><p><strong>Still Have Questions or Need Help?</strong>
</p>

		<blockquote>	<p>We are here for you!  <br><br>
Please email us at: <a href="mailto:usracing@myracingaccount.com">usracing@myracingaccount.com</a> or call us at 1-866-567-2697. </p></blockquote>
              
        


<p>If you had a zero balance in your account, there is no need to submit your email.  You can open a new account right now.  Just click on the <a href="/signup/">SIGN UP</a> or <a href="/signup/">JOIN</a> buttons today!</p>
	
{*
<iframe width="400" height="32" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="http://www.allhorse.com/usracing_migration/form_submit.php"></iframe>
*}

<p>We are looking forward to welcoming you back and wish you success with all your bets!</p>
      Thank you.<br>US Racing Team</p>		


<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



 
</div><!-- end/row -->
</div><!-- end/container -->
