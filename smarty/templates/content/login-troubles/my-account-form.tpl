<div id="main" class="container">
<div class="row">
<div id="full-col" class="col-md-12">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 

<h1>US Racing Membership Update</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->








<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->







<p>Dear US Racing Member,</p><P>If you opened an account with US Racing before April 1, 2015 you will need to reregister in order to login and wager on horses. Although attempts were made to contact members via email, there is a chance that our emails were filtered to your spam folder.  We apologize for this inconvenience. But you can be betting online again in a matter of minutes!</p>
<p>In order to maintain the integrity of your account, our software provider requires this step which should take no longer than 5 minutes of your time. 
	
	
	 Simply click <a href="/signup/">HERE</a> or the <a href="/signup/">SIGN UP</a> button that you will see at the top of every page and enter your information.
		 Please remember that the address you enter must match your billing address.  All US Racing members are entitled to a 25% cash bonus on your first deposit-- NO LIMITS!</p> 
<p>If you have any questions or concerns about your previous account (Pre April 1, 2015), please enter your email into the form below for further information, we will be happy to answer any questions you have.You will receive an email within minutes, so if you don't see it, please check your junk email box.</p>






{literal}

{/literal}

	
{* <p>{include_php file='/home/ah/allhorse/public_html/usracing_migration/form_submit.php'}</p> *}

<p>We are looking forward to welcoming you back and wish you success with all your bets!</p>



<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



 
</div><!-- end/row -->
</div><!-- end/container -->
