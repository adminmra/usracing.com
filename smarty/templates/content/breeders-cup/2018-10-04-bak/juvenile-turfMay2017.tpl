{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/video.php'}

      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          

                    
          
<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Juvenile Turf
</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/purse.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/description.php'}</p>

<!-- 

<h2>Breeders Cup'  Juvenile Turf Odds</h2>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  -->


<hr><!-- --------------------- HR ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}</p>  


<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Turf Results
</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/results.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  


<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Turf Payouts
</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/payouts.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<h2> Breeders Cup'  Juvenile Turf Winners
</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/winners.php'} </p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  


      
				            
                  
{include file='inc/disclaimer-bc.tpl'} 
       
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/call_to_action.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 