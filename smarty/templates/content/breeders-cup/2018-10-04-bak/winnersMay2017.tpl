{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

                    
          
               
          
<div class="headline"><h1>Breeders' Cup  Winners</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


 
 
 
 <p><a href="/bet-on/breeders-cup"><img class="img-responsive" src="/img/breeders/breeders-cup-winner.jpg" alt="Bet on the Breeders' Cup"  />  </a></p>

 
  {include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}
  <h2>Classic<a name="Distaff"></a></h2>
 {include file='/home/ah/allhorse/public_html/breeders/classic/winners.php'} 
 <h2>Mile<a name="Filly & Mare"></a></h2>
 {include file='/home/ah/allhorse/public_html/breeders/mile/winners.php'} 
						 		 
<h2>Sprint<a name="Juvenile"></a></h2>
 {include file='/home/ah/allhorse/public_html/breeders/sprint/winners.php'} 
						 		 
<h2>Turf<a name="Juvenile Filly"></a></h2>
 {include file='/home/ah/allhorse/public_html/breeders/turf/winners.php'}
  
 <h2>Dirt Mile<a name="Mile"></a></h2>

{include file='/home/ah/allhorse/public_html/breeders/dirt-mile/winners.php'} 
						 		 
<h2>Juvenile<a name="Sprint"></a></h2>

   
  {include file='/home/ah/allhorse/public_html/breeders/juvenile/winners.php'} 
						 		 
<h2>Filly & Mare Sprint<a name="Turf"></a></h2>

  
  {include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/winners.php'}
						 	
<h2>Juvenile Turf<a name="Sprint"></a></h2>

    
  {include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/winners.php'} 
						 		
						  
<h2>Juvenile Sprint<a name="Sprint"></a></h2>

   
  {include file='/home/ah/allhorse/public_html/breeders/juvenile-sprint/winners.php'}
						 		
						  
<h2>Marathon<a name="Sprint"></a></h2>

  
  {include file='/home/ah/allhorse/public_html/breeders/marathon/winners.php'} 
						 		
						  
<h2>Juvenile Fillies Turf<a name="Sprint"></a></h2>


  {include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/winners.php'} 
						 		
						  
<h2>Filly & Mare Turf<a name="Sprint"></a></h2>

   
  {include file='/home/ah/allhorse/public_html/breeders/filly-mare-turf/winners.php'} 
						 		
						  
<h2>Juvenile Fillies<a name="Sprint"></a></h2>

   
  {include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/winners.php'}
						 		
						  
<h2>Distaff<a name="Sprint"></a></h2>

  
  {include file='/home/ah/allhorse/public_html/breeders/distaff/winners.php'} 
						 		
						  
<h2>Turf Sprint<a name="Sprint"></a></h2>

   
  {include file='/home/ah/allhorse/public_html/breeders/turf-sprint/winners.php'} 

   {include file='inc/disclaimer-bc.tpl'} 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
    