{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/video.php'}      
      
      <div id="main" class="container">
<div class="row">

              
          
<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Juvenile Fillies 
</h1></div>

<div id="left-col" class="col-md-9">

 

<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/purse.php'}</p>
{* <p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/description.php'}</p>  *}


<!-- -->

<h2>Breeders' Cup Juvenile Fillies Odds</h2>
{* <p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p> *}
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_juvenile_fillies_xml.php'} </p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>    
	
<hr><!-- --------------------- HR ---------------------- -->


<P>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}</p> 

<!-- --> 

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Fillies  Results</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/results.php'}</p> 
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p> 


<!-- -->

     
<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Fillies  Payouts</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/payouts.php'}</p> 
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p> 

 
<!-- -->
    
				            
<h2>Breeders' Cup  Juvenile Fillies  Winners</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/winners.php'}</p> 
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p> 


<!-- -->

{include file='inc/disclaimer-bc.tpl'} 


   
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/call_to_action.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 