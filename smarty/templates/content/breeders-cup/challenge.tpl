{assign var="showclock" value="1"} 
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  


			<p>Win a Breeders’ Cup Challenge race, and you’re in the Breeders’ Cup. With 67 qualifying races spanning 10 countries, each of the 14 Breeders’ Cup divisions have Challenge races. And a win gets you more than just an invitation. The Breeders’ Cup pays entry fees for the connections of the Challenge winners if they are nominated to the Breeders’ Cup program by October 21st. </p><p>All Championships starters that are not based in California will receive travel awards of $10,000 (US) if traveling within North America or $40,000 (US) if traveling from outside of North America. Last year, 45 winners of Breeders’ Cup Challenge races participated in the World Championships.</p>

<h2>Breeders' Cup Future Odds:</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>

			<h2>Breeders' Cup  Challenge Schedule</h2>
			{*include file='/home/ah/allhorse/public_html/breeders/challenge_scraped.php'*}
            {include_php file='/home/ah/allhorse/public_html/breeders/challenge.php'}
			{*include file="includes/ahr_block_breeders_challenge.tpl"*}




{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/end-of-page-blocks.tpl"}