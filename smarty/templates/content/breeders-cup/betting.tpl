{assign var="showclock" value="1"} 
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  


     <p>Bet on the fastest horses from all over the world as they meet in    {include file='/home/ah/allhorse/public_html/breeders/location.php'}  this year to compete in the Breeders' Cup Races. With {$BC_PURSE_TOTAL} in prize money, it's the richest, most exciting event in sports making most other sporting events look quaint.</p>

 <h2>Bet on the Breeders' Cup</h2>
{*    <p> {include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}  </p> *}
{*  There was a problem with the odds.  Check either include below *}      
<p>{*include file='/home/ah/allhorse/public_html/breeders/odds_classic_xml.php'*}</p>
{* <p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic.php'} </p> *}
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic_manually.php'} </p>
<!--<a name="classic"></a><h2>Breeders' Cup Classic Odds</h2>
<blockquote >Quick links - <a href="/breeders-cup/odds#classic"> Classic </a> - <a href="/breeders-cup/odds#distaff">Distaff </a> - <a href="/breeders-cup/odds#mile">Mile </a> - <a href="/breeders-cup/odds#turf">Turf</a> - <a href="/breeders-cup/odds#sprint">Sprint </a> - <a href="/breeders-cup/odds#juvenile">Juvenile</a> - <a href="/breeders-cup/odds#juvenile-fillies">Juvenile Fillies </a></blockquote>
<p>{*include file='/home/ah/allhorse/public_html/breeders/odds_classic_static.php'*} </p>-->
<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
 
 
 <div class="justify-left">
<p><strong>Where can I  bet on the Breeders' Cup? </strong> <a href="/signup?ref={$ref}">BUSR</a> offers Futures Wagers, Match Races and all track betting options for the Breeders' Cup. This year the race will be  held on {$BC_DAY}.</p> 


<p><strong>How can I bet? </strong> There are many different ways to place your wager on the Breeders' Cup. {* At BUSR, they offer their "EZ Bet App" for newbies.  *} You just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR Sportsbook, you can bet on Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. You will also find all kinds of bets including  <a href="/kentucky-derby/future-wager"> Future Wagers</a> where you can bet the Breeders' Cup weeks, if not months, in advance.</p>

<p><strong>Where can I Watch the Breeders' Cup? </strong> You can watch the race at 2:00 p.m. ET on TV on NBC or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Breeders' Cup today? </strong> Visit <a href="/breeders-cup/odds"> Breeders' Cup Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>	 
	 
{*
 <p>The end of year event attracts a lot of interest from Europe as well as the US. Horses go head to head in various grade divisions and distances for some of the biggest purses in horse racing which also makes it one of the best betting opportunities for horseplayers all year.  All race tracks that have hosted the <a href="/breeders-cup">Breeders' Cup</a> have been in the United States, except in 1996, when the Breeders' Cup was held at the <a href="/woodbine">Woodbine Racetrack</a> in Canada.</p>

<h2>How to Bet on the Breeders' Cup</h2>
<p><a href="/signup?ref={$ref}" rel="nofollow">Open an account</a> online at BUSR, America's online betting choice for horse racing. Fund your account then select the race you want to wager on. Pick your bet type, horse and you're off to the races. </p>
<p>When it comes to betting on the Breeders&rsquo; Cup, there are 5 races to choose from on Friday and 9 races on Saturday. The biggest event of the 2-day Championships is the last race on Saturday for a prize of {$BC_PURSE}. While the <a href="/breeders-cup/classic">Classic</a> has the biggest purse, the Mile is usually the most contentious race. </p>



<h2>Breeders' Cup Betting History</h2>
<p>Breeders' Cup reported that approximately $144.3 million was wagered on the World Championships in 2012 at <a href="/santa-anita-park">Santa Anita Park</a> which was down slightly from the previous year's figure of $155.5 million when the event was hosted by <a href="/churchill-downs">Churchill Downs</a>, in Louisville, Kentucky. Breeders' Cup officials indicated the handle totals were in line with expectations based on the interruption of service experienced by many east coast pari-mutuel outlets which were affected by Hurricane Sandy.</p>
*}

 </div>



{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/end-of-page-blocks.tpl"}