{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{include file="/home/ah/allhorse/public_html/usracing/schema/breeders-event.tpl"}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<!--------- NEW HERO  ---------------------------------------->

 <div class="newHero" style="background-image: url({$hero}); background-position: 0 0;">
 <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>

 
<!--------- NEW HERO END ---------------------------------------->   
     <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      


   <section class="kd bc-sa bcs usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bc/bc.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h1 class="kd_heading">{$h1b}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/breeders/salesblurb.tpl'} </p>

<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>

 <hr><!-- --------------------- HR ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/breeders/classic/purse.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/description.php'}</p>

<!-- -->
 <hr><!-- --------------------- HR ---------------------- -->

<h2>Breeders' Cup Classic Top 3 Finishing Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic_top2_top3.php'} </p>
<p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p>



    </div>
	</div>
  

    
 </section> 
<!--  <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> -->

{*List of blocks comon to BC Pages.  Check the include for date specific (expiry) content-ab *}
{* include file="/home/ah/allhorse/public_html/breeders/blocks-common.tpl" *}

 {include file="/home/ah/allhorse/public_html/usracing/block_breeders/end-of-page-blocks.tpl"}  

{*
<div id="main" class="container">
  {include file='inc/disclaimer-bc.tpl'} 
</div><!-- /#container --> 
*}