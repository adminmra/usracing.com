 {include_php file="/home/ah/usracing.com/smarty/libs/libcontrol_page.php"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<!--------- NEW HERO  ---------------------------------------->

 <div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>

 
<!--------- NEW HERO END ---------------------------------------->   
     <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      


   <section class="kd bcs usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bc/bc.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}  {$h1b}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/breeders/salesblurb.tpl'} </p>

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

<hr><!-- --------------------- HR ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/purse.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/description.php'}</p>



 {*Expires Nov 3*****************************************************************}
 
 {if $bcSwitch=='Y'}
<h2>Breeders Cup'  Juvenile Turf Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_breeders_juvenile_turf.php'}</p>
<p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p>


 <hr><!-- --------------------- HR ---------------------- -->
 
 <h2>More Breeders' Cup Odds</h2>
 <p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>
 
 {else}
 
  {* RESUMES AFTER NOV 3 ****************************************************************}
	 
<hr><!-- --------------------- HR ---------------------- -->	 
	 
<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Turf Results
</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/results.php'}</p>
 <p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p>  


<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Turf Payouts
</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/payouts.php'}</p>
 <p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p>

<h2> Breeders Cup'  Juvenile Turf Winners
</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/winners.php'} </p>
 <p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p> 
 
  {* RESUMES AFTER NOV 3 **************************************************************** end *}


 
 
 {/if}
{*End Expiry*}


{* <p>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}</p>   *}


      </div>
	</div>
  

    
 </section> 
<!--  <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> -->

{*List of blocks comon to BC Pages.  Check the include for date specific (expiry) content-ab *}
{include file="/home/ah/allhorse/public_html/breeders/blocks-common.tpl"}

  


<div id="main" class="container">
  {include file='inc/disclaimer-bc.tpl'} 
</div><!-- /#container --> 
