{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <style>
  @media (max-width: 1440px) and (min-width: 1024px){
        .newHero {
        background-position-x: -252px;
      }
}
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 0%;
        } 
      }

      @media (max-width: 991px) {
        #right-col {
          display: flex;
          justify-content: center;
        }
      }

      #right-col {
        position: sticky;
        top: 116px;
      }


      @media (min-width: 1593px) {
        .grid-item{
          margin-right:5%;
        }
      }

      #left-nav-btn{
        display:none !important;
      }


    </style>
{/literal}

{literal}
<script src="/assets/js/iframeResizer.min.js"></script>

<script>
    $(document).ready(function () {
        $("#iframeResize").iFrameResize({
            log: false,                   // Enable console logging
            enablePublicMethods: false,  // Enable methods within iframe hosted page
            checkOrigin: false,
            bodyMargin: 0

        });
    })
</script>
{/literal}

{include file='inc/left-nav-btn.tpl'}

{literal}
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Horse Racing  polls | Online Horse Betting",
  "description": "US Racing provides  horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit.",
  "image": "https://www.usracing.com/img/best-horse-racing-betting-site.jpg",
  "startDate": "2018-11-27 07:19", 
  "endDate": "2020-11-30 09:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
    "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
      "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],
  "location": {
    "@type": "Place",
    "name": "US Racing | Online Horse Betting",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "123 street ,place park",
      "addressLocality": "miami",
      "addressRegion": "Florida",
      "postalCode": "33184",
      "addressCountry": "US"
    }
  }
}

</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Today's Horse Racing polls",
    "item": "https://www.usracing.com/polls"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "{include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'} Kentucky Derby polls",
    "item": "https://www.usracing.com/kentucky-derby/polls"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "Triple Crown polls",
    "item": "https://www.usracing.com/bet-on/triple-crown"
  },
  {
    "@type": "ListItem",
    "position": 4,
    "name": "Pegasus World Cup polls",
    "item": "https://www.usracing.com/pegasus-world-cup/polls"
  },
  {
    "@type": "ListItem",
    "position": 5,
    "name": "Pegasus World Cup polls",
    "item": "https://www.usracing.com/pegasus-world-cup/polls"
  },
  {
    "@type": "ListItem",
    "position": 6,
    "name": "Grand National polls",
    "item": "https://www.usracing.com/grand-national"
  },
  {
    "@type": "ListItem",
    "position": 7,
    "name": "Dubai World Cup polls",
    "item": "https://www.usracing.com/dubai-world-cup"
  },
  {
    "@type": "ListItem",
    "position": 8,
    "name": "US Presidential Election polls",
    "item": "https://www.usracing.com/polls/us-presidential-election"
  }
  ]
}
</script>
{/literal}
<div id="left-nav"> 
  
  <!-- ---------------------- left menu contents ---------------------- --> 
  
  <!-- ---------------------- end left menu contents ------------------- --> 
</div>

<!--------- NEW HERO class="newHero"  ---------------------------------------->

<div class="usrHero" style="background-image: url({$hero});" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-xl">{$h1b}</div>
  <div class="text text-md" style="text-align:right">{$h2} <br>
    <a href="#polls">
    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a> </div>
</div>

<!--------- NEW HERO END ---------------------------------------->

<div id="main" class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="headline">
        <h1>USR Best of {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'} Awards</h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div id="left-col" class="col-md-9">
      <div class="content" id="polls">
        <p style="margin-bottom: 2px;">The USR Best of {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'} awards nominations have arrived, it's time to voice your choice!</p>
        <p> <a href="https://www.usracing.com">USRacing.com</a> invites you to vote for the best representatives of the horse racing industry. This year,
        you can help us decide the best of the 7 categories our experts have selected.
        Check out the list below, and select your favorites. We'll announce the winners on January 20th.</p>
        <p>{* The top OTTB vote-getter will receive a $500 prize. *} The top OTTB organization vote-getter will receive a $1,000 donation. Additionally the top vote-getters in the remaining categories will each receive $100 to be donated in their name to their choice of one of the OTTB organizations in our contest.</p>
      {literal}
     <script type="text/javascript">
       (function(d,s,id,u){
         if (d.getElementById(id)) return;
         var js, sjs = d.getElementsByTagName(s)[0],
             t = Math.floor(new Date().getTime() / 1000000);
         js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
         sjs.parentNode.insertBefore(js, sjs);
       }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
       </script><div class="os_poll" data-of="usracing" data-path="/polls/2710794" id="os-widget-787287"></div>
       <script type="text/javascript">
         (function(d,s,id,u){
           if (d.getElementById(id)) return;
           var js, sjs = d.getElementsByTagName(s)[0],
               t = Math.floor(new Date().getTime() / 1000000);
           js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
           sjs.parentNode.insertBefore(js, sjs);
         }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
         </script><div class="os_poll" data-of="usracing" data-path="/polls/2711104" id="os-widget-787755"></div>
         <script type="text/javascript">
           (function(d,s,id,u){
             if (d.getElementById(id)) return;
             var js, sjs = d.getElementsByTagName(s)[0],
                 t = Math.floor(new Date().getTime() / 1000000);
             js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
             sjs.parentNode.insertBefore(js, sjs);
           }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
           </script><div class="os_poll" data-of="usracing" data-path="/polls/2711108" id="os-widget-787759"></div>
           <script type="text/javascript">
             (function(d,s,id,u){
               if (d.getElementById(id)) return;
               var js, sjs = d.getElementsByTagName(s)[0],
                   t = Math.floor(new Date().getTime() / 1000000);
               js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
               sjs.parentNode.insertBefore(js, sjs);
             }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
             </script><div class="os_poll" data-of="usracing" data-path="/polls/2711112" id="os-widget-787765"></div>
             <script type="text/javascript">
               (function(d,s,id,u){
                 if (d.getElementById(id)) return;
                 var js, sjs = d.getElementsByTagName(s)[0],
                     t = Math.floor(new Date().getTime() / 1000000);
                 js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
                 sjs.parentNode.insertBefore(js, sjs);
               }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
               </script><div class="os_poll" data-of="usracing" data-path="/polls/2711914" id="os-widget-788944"></div>
               <script type="text/javascript">
                 (function(d,s,id,u){
                   if (d.getElementById(id)) return;
                   var js, sjs = d.getElementsByTagName(s)[0],
                       t = Math.floor(new Date().getTime() / 1000000);
                   js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
                   sjs.parentNode.insertBefore(js, sjs);
                 }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
                 </script><div class="os_poll" data-of="usracing" data-path="/polls/2711106" id="os-widget-787757"></div>
                 <script type="text/javascript">
                   (function(d,s,id,u){
                     if (d.getElementById(id)) return;
                     var js, sjs = d.getElementsByTagName(s)[0],
                         t = Math.floor(new Date().getTime() / 1000000);
                     js=d.createElement(s); js.id=id; js.async=1; js.src=u+'?'+t;
                     sjs.parentNode.insertBefore(js, sjs);
                   }(document, 'script', 'os-widget-jssdk', 'https://www.opinionstage.com/assets/loader.js'));
                   </script><div class="os_poll" data-of="usracing" data-path="/polls/2711919" id="os-widget-788950"></div>
                   
       
      <div id="polls"></div>
        <div class="polls">
          <div class="grid-item">
        </div>
                  <br/>
                  <b><center><p style="font-size:26px;margin-bottom: 0px;">Thank you for your vote</p></center></b>
                  <center><p style="font-size:20px;margin-bottom: 0px;">Re-vote for your favorites again tomorrow. We will announce the winners on January 20th.</p></center>
        </div>
      {/literal} 
      
      </div>
    </div>
    <div id="right-col" class="col-md-3"> 
    {*include_php file='/home/ah/usracing.com/htdocs/news/layout/sidebar/ads.php' *}
        <div class='fixed'>
          {include file='inc/rightcol-calltoaction-usr-awards.tpl'}
        </div>
    </div>
    <!-- end: #right-col -->   
  </div> 
  <!-- end/row --> 
  <br/>
  </div></div></section>
  {* {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"} *}
  {include file="/home/ah/allhorse/public_html/templates/block-testimonial-pace-advantage.tpl"}
  {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-free-bet.tpl"}
  {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-trainers.tpl"}
  {if $KD_Switch_Full_Site}
  {else}
  {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-jockeys.tpl"}
  {/if}
  {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-final-cta.tpl"}
<!-- /#container --> 
</div>
</div>
