<?php include 'header.php'; ?>

<?php 
    include_once("inc/Mobile_Detect.php"); 
    // Detect if is mobile
    $detect = new Mobile_Detect;

?>

    <div class="w100">
        <!--<article class="roundedCorners upComing">
            <h2><img src="images/icons/upcoming.png" alt="Chrono" title="Chrono"/>Upcoming Races</h2>
            <iframe id="iframeUpcoming" scrolling="no" src="https://rbws.mybookie.ag/BOSSWagering/Racebook/RedirectUpcomingRaces?siteid=usracing" frameborder="0" width="100%"></iframe> 
        </article> -->
        <article class="lobbyHolder">
            <ul class="navLobby">
          		<li>
                    <a href="#" class="roundedCorners" onclick="window.parent.location.href='https://engine.betusracing.ag/cashier';" >
                        <div class="navImg">
                            <img src="images/icons/deposit.png" alt="Deposit" title="Deposit" />
                        </div>
                        <span>Deposit Now</span>
                    </a>
                </li>
		<li>
                    <a href="#" onclick="window.parent.location.href='/ch/bm-bet';" class="roundedCorners green">
                        <div class="navImg">
                            <img src="images/icons/belmont.png" alt="Bet on Belmont" title="Bet on Belmont" />
                        </div>
                        <span>Bet on Belmont</span>
                    </a>
                </li>		
		<li>
                    <a target="_self" href="support" class="roundedCorners">
                        <div class="navImg">
                            <img src="images/icons/support.png" alt="Support" title="Support" />
                        </div>
                        <span>Support</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="roundedCorners" onclick="window.parent.location.href='https://engine.betusracing.ag/to/racebook';">
                        <div class="navImg">
                            <img src="images/icons/racing.png" alt="Racing" title="Racing" />
                        </div>
                        <span>Bet Racing</span>
                    </a>
                </li>
                <li>
                     <a href="#" class="roundedCorners" onclick="window.parent.location.href='/ch/basketball';"><?php // see htaccess - tf apr.29.18 ?>
						<div class="navImg">
                            <img src="images/icons/nba.png" alt="NBA" title="NBA" />
                        </div>
                        <span>Bet Sports</span>
                    </a>
				</li>
				<li>
                     <!--   <a href="#" class="roundedCorners" onclick="window.parent.location.href='https://engine.betusracing.ag/casino/betsoft_casino_game/195';" target="_blank"> -->
                     <a href="https://engine.betusracing.ag/casino/betsoft_casino_game/195" target="_blank" class="roundedCorners" >
                            <div class="navImg">
                                <img src="images/icons/blackjack_icon.png" alt="Blackjack" title="Blackjack" />  
                            </div>
                            <span>Play Blackjack</span>
                        </a>
				</li>
                <li>
                    <a  href="#" class="roundedCorners" onclick="window.parent.location.href='https://engine.betusracing.ag/casino';">
                        <div class="navImg">
                            <img src="images/icons/casino.png" alt="Casino" title="Casino" />
                        </div>
                        <span>&nbsp; Casino   &nbsp;</span>
                    </a>
                </li>
                <li>
                    <a target="_self" href="racing-results" class="roundedCorners">
                        <div class="navImg">
                            <img src="images/icons/results.png" alt="Results" title="Results" />
                        </div>
                        <span>&nbsp;  Results &nbsp;</span>
                    </a>
                </li>
				<li>
					<a  href="#" class="roundedCorners" onclick="window.parent.location.href='https://engine.betusracing.ag/to/live_betting';">
                        <div class="navImg">
                            <img src="images/icons/in_game.png" alt="In-Game Betting" title="In-Game Betting" />
                        </div>
                        <span>Live Betting</span>
                    </a>
                </li>
                <li>
                    <a target="_self" href="promo" class="roundedCorners">
                        <div class="navImg">
                            <img src="images/icons/promos.png" alt="Promos" title="Promos" />
                        </div>
                        <span>Promos</span>
                    </a>
                </li>
				<li>
					&nbsp;
                </li>
                <li>
                    <a href="#" class="roundedCorners" onclick="window.parent.location.href='https://betcenter.betusracing.ag/leaderboards/board/TCBlackjack';">
                        <div class="navImg">
                            <img src="images/icons/blackjack_icon.png" alt="Blackjack" title="Blackjack" />  
                        </div>
                        <span>BJ Leaders</span>
                    </a>
                </li>
                <!-- <li>
                    <a href="kd-help-section" class="roundedCorners green">
                        <div class="navImg">
                            <img src="images/icons/kentucky_derby_green.png" alt="Kentucky Derby" title="Kentucky Derby" />
                        </div>
                        <span>Kentucky Derby</span>
                    </a>
                </li> -->
              
               <!-- <li>
                    <a target="_self" href="past-performances" class="roundedCorners">
                        <div class="navImg">
                            <img src="images/icons/performances.png" alt="Performances" title="Performances" />
                        </div>
                        <span>Performances</span>
                    </a>
                </li> -->
                <!-- <li>
                    <a target="_self" href="echeck-deposit" class="roundedCorners">
                        <div class="navImg">
                            <img src="images/icons/e_check.png" alt="eCheck" title="eCheck" />
                        </div>
                        <span>eCheck</span>
                    </a>
                </li> -->
              
                
                <li>
					&nbsp;
                </li>
                <?php 
                    if(!$detect->isMobile() ){ ?> 
                <li>
					&nbsp;
                </li>
              <?php } ?>
            </ul>
        </article>
    </div>
<?php include 'footer-no-widget.php'; ?>
