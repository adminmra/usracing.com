<?xml version="1.0" encoding="ISO-8859-1"?>
<pagevariables>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>A History of Horse Racing</title>
<meta name="description" content="Bet on the Kentucky Derby at US Racing.  OTB with 150 racetracks, 8% rebates, news, live entries and results" />
<meta name="keywords" content="history of horse racing, horse racing history" />
<link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />
<sidebars>
right-sidebars
</sidebars>
<classes>
page page-horseracing-history section-horseracing-history
</classes>
</pagevariables>