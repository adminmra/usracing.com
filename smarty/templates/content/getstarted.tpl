<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



<div class="areaBox">
<!-- =============================== edit Content below here =============================== --> 
 
 
 <p class="title.lrg"></p>
 <p class="title">Welcome to US Racing!</p>
 <p class="title">We invite you to signup and explore the easiest platform for horse betting.</p>
 
 <p class="subTitle">Email <a href="mailto:cs@usracing.com?subject=US Racing Merchandise">
cs@usracing.com</a> after you register to claim your FREE OFFICIAL US Racing Merchandise!</p>
  <p class="subTitle">Please include your full name, address and measurements.</p>

 
 <hr /><!-- line -->
 
 <p class="title reg">US Racing Beta, allows you to deposit and wager on horses:</p>
 
 <ul>
  <li><span><strong>Straightforward Betting Interface</strong></span> <div class="small">Sometimes simple is better</div></li>
  <li><span><strong>Safety Confirmation for your bets</strong></span><div class="small">Change your mind about a bet? Cancel it easily.</div></li>
  <li><span><strong>Instant Results and Free Handicapping Information</strong></span><div class="small">Check out the previous races of your horse and jockey</div></li>
 </ul>
 
 <div style="display:block; text-align:center; margin-top:20px;"><a href="/signup/"> <img alt="joinnow" src="/themes/images/btn-joinnow.png" style="display:block; width:auto; margin:0 auto;" /></a></div>


 </div>

  

<!-- =============================== end editable Content  =============================== --> 



&nbsp;

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


 
    
  

