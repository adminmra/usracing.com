{*literal}
<script type="application/ld+json">
  {
  "@context": "http://schema.org",
  "@type": "SportsEvent",
  "name": "Harness Racing Schedule",
  "startDate": "2019-06-29T15:30",
  "endDate": "2019-06-29T16:00",
  "location": {
    "@type": "EventVenue",
    "name": "Tompkins Geers",
    "telephone": "000-000-1234",
    "openingHours": "Mo,Tu,We,Th,Fr 09:00-17:00",
	"address": "3YOCP $68,000 @ Tioga",
    "geo": {
    "@type": "GeoCoordinates",
    "latitude": "40.75",
    "longitude": "73.98"
    }
  	}, 
    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "description": "3YOCP $68,000 @ Tioga."
  }
</script>
{/literal*}
<div id="main" class="container">
	<div class="row">

	<div id="left-col" class="col-md-9">

		
			<div class="content">
			<!-- --------------------- content starts here ---------------------- -->
            <p><a href="/login"><img class="img-responsive" src="/img/harness-racing.jpg" alt="Harness Racing Schedule">  </a></p>
			{* <div class="tag-box tag-box-v4"> *}
			<p>
				US Racing presents this week's harness racing schedule for US, Canadian and international race tracks.  </p>


{assign var="cta_append" value=" at any of these harness races"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
			{* 	</div> *}
            <div class="headline"><h1>Harness Racing Schedule</h1></div>
			{include file='/home/ah/allhorse/public_html/generated/racing_schedules/harness_schedules_new_manual.php'}


			<!-- ------------------------ content ends -------------------------- -->
		</div> <!-- end/ content -->
	</div> <!-- end/ #left-col -->

	<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction.tpl'}

	</div><!-- end: #right-col -->


	</div><!-- end/row -->
</div><!-- /#container -->

{*
{literal}
<script type="text/javascript">
var dom = $(".sRace");
				dom.find('.race:contains("Churchill Downs")').parent().remove();
				dom.find('.race:contains("Fair Grounds")').parent().remove();
				dom.find('.race:contains("Calder")').parent().remove();
				dom.find('.race:contains("Hoosier")').parent().remove();
				dom.find('.race:contains("Indiana Downs")').parent().remove();
				dom.find('.race:contains("Miami Valley")').parent().remove();
				dom.find('.race:contains("Oaklawn Park")').parent().remove();
				dom.find('.race:contains("The Meadows")').parent().remove();
				dom.find('.race:contains("Arlington Park")').parent().remove();
				dom.find('.race:contains("Finger Lakes")').parent().remove();
				dom.find('.race:contains("Canterbury Park")').parent().remove();
</script>
{/literal}
*}
