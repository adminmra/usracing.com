<div id="main">
  

<div id="col1" class="col-md-3">
{include file='menus/kentuckyderby.tpl'} 
</div><!-- end: #col1 --> 

<div id="col2" class="col-md-3">
</div><!-- end: #col2 --> 

<div id="col3" class="col-md-6">
<h1>The Kentucky Derby</h1>
<div class="content">

{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}

<p>The Kentucky Derby is a stakes race for three-year-old thoroughbred horses, staged yearly in Louisville, Kentucky on the first Saturday in May, capping the two-week-long Kentucky Derby Festival. The race currently covers one and one-quarter miles (2.012 km) at&nbsp;<a title="Churchill Downs" href="/churchill-downs">Churchill Downs</a>; colts and geldings carry 126 pounds (57 kg), fillies 121 pounds (55 kg).</p>

<p>The race, known as "The Most Exciting Two Minutes in Sports" for its approximate time length, is the first leg of the Triple Crown of Thoroughbred Racing in the United States. It typically draws around 155,000 fans.The Kentucky Derby is one of the crown jewels of the elusive&nbsp;<a title="Triple Crown" href="/triple-crown">Triple Crown</a>&nbsp;which includes the&nbsp;<a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a>&nbsp;and the&nbsp;<a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>.</p>


<p><a title="Horse racing  Kentucky" href="/kentucky">Horse racing in Kentucky</a>&nbsp;is rich in history, dating back to 1789 when the first race course was laid out in Lexington. However, it was almost 100 years later, in 1875, that Churchill Downs officially opened and began its tradition as "Home of the Kentucky Derby."</p>

<p>In 1787, The Commons, a park-like block near Lexington's Race Street was used by horsemen for racing. By 1789, complaints by "safety minded" citizens led to the formal development of a race meet at The Commons. The men who organized this race meet, including Kentucky Statesman Henry Clay, also formed the Commonwealth's first Jockey Club. The organization later was named the Kentucky Jockey Club in 1809.</p>

<p>Racing in Louisville dates back to 1783 when local sources reported that races were held on Market Street in the downtown area. To alleviate the problems associated with racing on the busy city thoroughfare, a course was developed at the now abandoned Shippingport Island in 1805. Racing was conducted on the island in the Ohio River at what was called the Elm Tree Gardens.</p>

<p>By 1827, a new track, known as the Hope Distillery Course, was laid out on what is presently Main and 16th Streets. Racing was also held on a number of private tracks located on farms throughout the local area. One of the more prominent of these was Peter Funk's Beargrass Track which was located in an area now bordered by Hurstbourne Lane and Taylorsville Road.</p>

<p>The Oakland Race Course was opened in the fall of 1833 and brought racing back to a formal site with the track, complete with clubhouse, located at what is now Seventh and Magnolia Streets in "Old Louisville". This was followed in 1858 by the opening of the Woodlawn Course on the Louisville and Lexington railroad lines just outside of today's St. Matthews, east of Louisville. The site closed in 1870, but the Woodlawn Vase, the track's premier trophy, has been used in the presentation to the winner of the Preakness Stakes at Pimlico since 1917.</p>

<p>Harness racing was also a significant part of Louisville's early racing history with a number of tracks in existence. One of the most prominent was Greeneland, a racecourse for trotters was built just east of Churchill Downs in 1868.</p>
             
<table border="0" cellspacing="0" cellpadding="0">
  <tbody>
   <tr>
    <td class="content-header-bar">&nbsp;&nbsp;<strong>Kentucky Derby Purse Structure</strong></td>
   </tr>
   <tr>
    <td><table class="data" border="0" cellspacing="0" cellpadding="0" summary="2011 Kentucky Derby Purse">
     <tbody style="border: 0;">
      <tr>
       <th width="250">Result</th>
       <th>Purse of $2,000,000</th>
      </tr>
      <tr>
       <td colspan="8"><hr class="storytitle" size="1" /></td>
      </tr>
      <tr>
       <td>Winner</td>
       <td>$1,240,000 (62%)</td>
      </tr>
      <tr class="odd">
       <td>Second</td>
       <td>$400,000 (20%)</td>
      </tr>
      <tr>
       <td>Third</td>
       <td>$200,000 (10%)</td>
      </tr>
      <tr class="odd">
       <td>Fourth</td>
       <td>$100,000 (5%)</td>
      </tr>
      <tr>
       <td>Fifth</td>
       <td>$60,000 (3%)</td>
      </tr>
      <tr>
       <td colspan="8"><hr class="storytitle" size="1" /></td>
      </tr>
     </tbody>
     </table>
     <p> {*
     <table class="data" border="0" cellspacing="0" cellpadding="0">
     <tbody style="border: 0;">
      <tr>
       <td class="content-header-bar">&nbsp;&nbsp;<strong>Kentucky Derby TV Schedule</strong></td>
      </tr>
      <tr>
       <td><table class="data" cellspacing="0" cellpadding="0" summary="Kentucky Derby TV Schedule">
        <tbody style="border: 0;">
         <tr>
          <th>Date</th>
          <th>Time (ET)</th>
          <th>Races</th>
          <th>Race Track</th>
          <th>Network</th>
         </tr>
         <tr>
          <td colspan="8"><hr class="storytitle" size="1" /></td>
         </tr>
         <tr>
          <!-- <td>March 27</td>
<td>5:00-6:00pm</td>
<td><strong>Road to the Kentucky Derby</strong> - T.V. Series, Part 1</td>
<td>NA</td>
<td>NBC</td>
</tr>
<tr class="odd">
<td>April 3</td>
<td>5:00-6:00pm</td>
<td>Road to the Kentucky Derby - TV Series, Part 2</td>
<td>NA</td>
<td>NBC</td>
</tr>
<tr class="even">
<td>April 10</td>
<td>5:00-6:00pm</td>
<td>Road to the Kentucky Derby - TV Series, Part 3</td>
<td>NA</td>
<td>NBC</td>
</tr>
<tr class="odd">
<td>April 11</td>
<td>6:00-7:00pm</td>
<td>Arkansas Derby / Blue Grass Stakes</td>
<td>Oaklawn / Keenland</td>
<td>ESPN</td>
</tr>
<tr>
<td>April 19</td>
<td>5-6:00pm</td>
<td>Coolmore Lexington Stakes</td>
<td>Keenland</td>
<td>ESPN</td>
</tr> -->
         <tr>
          <td>May 2 </td>
          <td>4:00-5:00pm</td>
          <td>Derby Classics</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr class="odd">
          <td>&nbsp;</td>
          <td>5:00-6:00pm</td>
          <td>Derby Post Position Draw</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr>
          <td>May 3 </td>
          <td>4:00-5:00pm</td>
          <td>Derby Classics</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr class="odd">
          <td>&nbsp;</td>
          <td>5:00-6:00pm</td>
          <td>Derby Access</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr>
          <td>May 4 </td>
          <td>4:00-5:00pm</td>
          <td>Derby Classics</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr class="odd">
          <td>&nbsp;</td>
          <td>5:00-6:00pm</td>
          <td>Kentucky Oaks 138</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr>
          <td>May 5 </td>
          <td>11:00am-4:00pm</td>
          <td>ESPN Coverage of the Kentucky Derby Undercard</td>
          <td>Churchill Downs</td>
          <td>Versus</td>
         </tr>
         <tr  class="odd">
          <td>&nbsp;</td>
          <td>4:00-5:00pm</td>
          <td>Access at the Kentucky Derby / Kentucky Derby Red Carpet Special</td>
          <td>Churchill Downs</td>
          <td>NBC</td>
         </tr>
         <tr>
          <td>&nbsp;</td>
          <td>5:00-7:00pm</td>
          <td><strong>Kentucky Derby 138</strong></td>
          <td>Churchill Downs</td>
          <td>NBC</td>
         </tr>
         <tr  class="odd">
          <td>May 19 </td>
          <td>4:30-5:00pm</td>
          <td>Thoroughbred Classics: Preakness Stakes</td>
          <td>Pimlico Race Course</td>
          <td>ESPN</td>
         </tr>
         <tr>
          <td>&nbsp;</td>
          <td>5:00pm-6:30pm</td>
          <td><strong>Preakness Stakes 137</strong></td>
          <td>Pimlico Race Course</td>
          <td>NBC</td>
         </tr>
         <tr  class="odd">
          <td>June 9 </td>
          <td>5:00-6:30pm</td>
          <td><strong>Belmont Stakes 144</strong></td>
          <td>Belmont Park</td>
          <td>NBC</td>
         </tr>
         <tr>
          <td colspan="8">&nbsp;</td>
         </tr>
        </tbody>
        </table>
        <p>*The post position draw for the Kentucky Derby will be held from 5 to 6 p.m. ET on Wednesday before the derby. </p></td>
      </tr>
     </tbody>
     </table>
     *}
     </p>
     <p>
     <table border="0" cellspacing="0" cellpadding="0" width="100%">
     <tbody style="border: 0;">
      <tr>
       <td class="content-header-bar">&nbsp;&nbsp;<strong>{include file='/home/ah/allhorse/public_html/kd/racedate.php'}  KENTUCKY DERBY DAY Race Schedule</strong></td>
      </tr>
      <tr>
       <td><table class="data" border="0" cellspacing="0" cellpadding="0" summary="2011 Kentuckey Derby Race Schedule">
        <tbody style="border: 0;">
         <tr>
          <td colspan="8"><hr class="storytitle" size="1" /></td>
         </tr>
         <tr class="odd">
          <td>Grade II Churchill Downs</td>
          <td>$250,000-added, seven furlongs, 4-year-olds and up</td>
         </tr>
         <tr>
          <td>Grade III La Troienne</td>
          <td>$150,000-added, seven furlongs, 3-year-old fillies</td>
         </tr>
         <tr class="odd">
          <td>Grade III Distaff Turf Mile</td>
          <td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>
         </tr>
         <tr>
          <td>Grade I Distaff</td>
          <td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>
         </tr>
         <tr class="odd">
          <td>Grade I Turf Classic</td>
          <td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>
         </tr>
         <tr>
          <td><p>Grade I Kentucky Derby</p></td>
          <td>$2,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>
         </tr>
         <tr>
          <td colspan="8"><hr size="1" /></td>
         </tr>
        </tbody>
        </table>
        <p>
        <div>*$40 general admission</div>
        </p></td>
      </tr>
     </tbody>
     </table>
     </p>
     <p>&nbsp;</p></td>
   </tr>
  </tbody>
  </table>
             <p>&nbsp;</p>
             <p><strong>The Founding of Churchill Downs</strong></p>
             <p>While traveling in England and France in 1872-1873, 26-year-old Col. M. Lewis Clark, devised the idea of a Louisville Jockey Club for conducting race meets. Clark toured and visited with a number of prominent racing leaders, including England's Admiral Rous and France's Vicompte Darn, vice president of the French Jockey Club.<br />
              <strong><br />
              </strong>Upon his return from Europe, Clark began development of his racetrack which would serve to showcase the Kentucky breeding industry. The track would eventually become known as "Churchill Downs." The first reference of the name Churchill Downs came in an 1883 Kentucky Derby article reported by the former Louisville Commercial.</p>
             <p>"The crowd in the grand stand sent out a volume of voice, and the crowd in the field took it up and carried it from boundary to boundary of Churchill Downs."</p>
             <p>The track was incorporated as Churchill Downs in 1937.</p>
             <p>The first public notice of establishment of the track was reported in the May 27, 1874 edition of the Courier-Journal. The notice was met with some objections because another track had already been proposed by the Falls City Racing Association for a site near the river just east of downtown Louisville. Clark and a group of prominent Louisville gentlemen met at the Galt House on June 18, 1874 to prepare articles of incorporation with the actual filing for the Louisville Jockey Club and Driving Park Association taking place on June 20.</p>
             <p>To fund the construction of the track, Clark raised $32,000 by selling 320 membership subscriptions to the track at $100 each. Eighty acres of land, approximately three miles south of downtown were leased from Clark's uncles, John and Henry Churchill. A clubhouse, grandstand, porter's lodge and six stables were all eventually constructed on the site for the opening of the track.</p>
             <p>For his inaugural race meet, Clark designed his three major stakes races, the Kentucky Derby, Kentucky Oaks and Clark Handicap, after the three premier races in England, the Epsom Derby, Epsom Oaks and St. Leger Stakes, respectively. These events have each been held continuously at Churchill since their debut in 1875. However, in 1953, the Clark was moved from the spring to the fall meet. The Falls City was also offered during the inaugural meet and after four interruptions, the race continues to be held.</p>
             <p>The track formally opened May 17, 1875 with four races scheduled. The winner of the first race was Bonaventure, however the winner of the day's featured race, the Kentucky Derby, was a three-year-old chestnut colt, Aristides. Owned by H.P. McGrath, Aristides was trained by and ridden by two African-Americans, Ansel Williamson and Oliver Lewis, respectively.</p>
             <p>Despite the success of the first Kentucky Derby, the track was not financially successful and on November 24, 1894 the New Louisville Jockey Club was incorporated. William F. Schulte was appointed president and Clark was retained as presiding judge for the track.</p>
             <p>Under Schulte, a new grandstand was constructed during fall 1894 - spring 1895 on the opposite side of the track for a reported cost of $100,000. The grandstand was complemented by two spires constructed atop the roof. The twin spires, a simple architectural element, would become the symbol of Churchill Downs and the Kentucky Derby.</p>
             <p>1874 -- Col. M. Lewis Clark begins rescue of Kentucky's declining stock farm. Develops Louisville Jockey Club on land secured from his uncles John and Henry Churchill.</p>
             <p>At the turn of the century, financial problems plagued the racetrack. On October 1, 1902 a group headed by former Louisville Mayor Charles Grainger, Charlie Price and Matt J. Winn agreed to takeover the operation. The takeover was done by amending the track's articles of incorporation with no transfer in the form of a deed. Grainger was named president, Price racing secretary and Winn vice president.</p>
             <p>Under this administration, the track finally showed its first profit in 1903, 28 years after its founding. As the Kentucky Derby grew in popularity so did the racetrack. In 1907 the owners of Churchill Downs, who were officials of the New Louisville Jockey Club, joined with nearby Douglas Park to form the Louisville Racing Association. The purpose of the new Association was to establish race dates and policies for racing in the City. This relationship led to the formation of the Kentucky Jockey Club in February 1919 as a holding company for Churchill and three other tracks in the State: Latonia in the north, Lexington in the bluegrass region, and Douglas Park and Churchill Downs in Louisville.</p>
             <p>Under the powerful Kentucky Jockey Club, the track's domain grew between 1919-1929. During this time the company acquired possession of the newly built Fairmount Park in East St. Louis, IL in 1925 and also constructed Lincoln Fields in Crete, IL in 1926. With five tracks under its control, the Kentucky Jockey Club began the process of dissolving the organization in December 1927 for the purpose of reorganizing as a separate holding corporation under the laws of the State of Delaware. According to a Louisville Times article dated December 29, 1927 . . ."incorporated under the laws of Delaware, provides for a capitalization of $6,000,000, an increase of $2,600,000 over the present capitalization."</p>
             <p>The process was finalized on January 16, 1928 with the American Turf Association serving as the new holding company for Churchill Downs, Douglas Park, Lexington, and Latonia in Kentucky and Fairmount Park, Lincoln Fields and Washington Park in Illinois. Washington Park was purchased by the association during this period.</p>
             <p>The Fairmount Park track was sold in 1929, and in 1935, the association began to dramatically trim its holdings with the sale of Washington Park, the closing and eventual sale of Lexington, and the end of racing at Douglas Park. The reduction left the association with three tracks: Churchill Downs, Latonia and Lincoln Fields. Due to economic reasons Churchill Downs and Latonia formed a separate operating corporation titled, Churchill Downs-Latonia Incorporated on January 28, 1937. Lincoln Fields was operated by Lincoln Fields Jockey Club, Inc., but all three tracks were still owned by the parent corporation, American Turf Association.</p>
             <p>On January 13, 1942, officials of Churchill Downs-Latonia Inc. sold the Latonia track and abandoned racing at the site. Later that year on April 24, the Churchill Downs-Latonia Incorporated's name was officially changed to Churchill Downs Incorporated. The American Turf Association continued its affiliation with Churchill, but sold its last out-of-state holding, Lincoln Fields, in March 1947.</p>
             <p>The corporate direction of Churchill Downs became a key topic in November 1948. Backed by track President Matt Winn (1938-49) and other board members, a committee was created to study the feasibility of the creation of a foundation to purchase Churchill and operate the track as a nonprofit entity with its earnings donated to the University of Louisville School of Medicine.</p>
             <p>The proposal was founded upon the experience of the Churchill Downs Foundation, a charitable organization led by J. Graham Brown. Each fall, several days of racing were held for charitable purposes. During a 10-year period 1940-50s, the foundation donated approximately $1.5 million to charity.</p>
             <p>The proposal was considered up until the death of Winn on October 6, 1949. Following the naming of William Veeneman as chairman and chief executive officer of both Churchill Downs and the American Turf Association on Oct. 10, and the selection of Bill Corum as track president, the proposal was permanently shelved December 30, 1949.</p>
             <p>The end of the once mighty American Turf Association came April 3, 1950 as stockholders voted to dissolve the association. Shareholders of the association exchanged their shares on a one for one basis for Churchill Downs Incorporated stock.</p>
             <p>Under the direction of Bill Corum, a former New York Times and New York Journal-American sports columnist, Churchill Downs and the famed Kentucky Derby continued to grow and modernize: the first national telecast of the Kentucky Derby aired May 3, 1952 the first barns constructed of concrete firewalls were built in 1952 more seating boxes were added to the second floor of the grandstand and clubhouse in 1953 with 400 additional third-floor boxes in the clubhouse film patrol was installed in 1954 to provide replays to the racing officials in 1955 a $300,000 automatic sprinkler system was installed in the entire grandstand and clubhouse.</p>
             <p>Following Corum's death in December 1958, Wathen Knebelkamp was selected as his successor on March 3, 1959. Under his direction an aggressive building and renovation program was initiated. During Knebelkamp's tenure improvements rose from $128,000 in 1959 to $1,016,000 in 1966. Renovations ranged from the installation of 1,000 seats on the north end of the grandstand (and construction of a museum in 1960) to the addition of the fourth and fifth floors of the Skye Terrace "Millionaire's Row" in 1965-66.</p>
             <p>The success of the track continued under Knebelkamp, but Churchill's eighth president was faced with speculation that the track was a prime target for a hostile takeover. In January,1960 a proposal was made to have the City of Louisville issue revenue bonds to purchase Churchill Downs. However, the proposal, which was made in an effort to secure the Derby and prevent outside ownership of the track, was turned down by aldermen. In December 1963, the Kentucky Racing Commission set forth a proposal to establish a new organization to purchase Churchill and Lexington's Keeneland Race Course and to modernize both tracks through revenue bonds.</p>
             <p>Finally, in March 1969, as a counter to a stock takeover attempt by National Industries, a group of Churchill board members, headed by John Galbreath, Warner L. Jones, Jr. and Arthur "Bull" Hancock formed what was called the "Derby Protection Group." They successfully outbid National Industries for control of the Company, moving the stock from $22 a share to $35.</p>
             <p>Lynn Stone became Churchill Downs ninth president as he replaced the retiring Knebelkamp in December 1969. Stone had come to Churchill in 1961 as resident manager and was appointed vice president and general manager in 1966.</p>
             <p>Under Stone's leadership: the Derby celebrated its 100th running in 1974, with a record 163,628 on hand; added the Skye Terrace's sixth floor in 1977 for $1.8 million; computerized the pari-mutuel system in 1982; and began development of a $7 million Kentucky Derby Museum.</p>
             <p>As president, Lynn Stone headed the efforts that ended two separate takeover attempts by Brownell Combs II of Spendthrift Farm and Irwin L. Jacobs, respectively in 1984. In August 1984, Stone resigned, following huge financial losses that resulted from two years of failed summer racing. Stone was replaced by acting President Thomas H. Meeker, a former general counsel to Churchill Downs while with the law firm of Wyatt, Tarrant and Combs. In September 1984, Meeker was named permanently to the position.</p>
             <p>At 40, Meeker became the youngest president since Meriwether Lewis Clark organized the track at age 29. Meeker immediately began a five-year, $25 million renovation renaissance, headed by: (dollar figures in millions) $2.5 core renovations (1984) $3.2 Matt Winn Turf Course (1985) $2.5 paddock construction (1986) $5 clubhouse improvements (1987) $3.7 Skye Terrace updating (1988) $1.2 barn area improvements (1989).</p>
             <p>These renovations led to a resurgence of the track and helped attract the Breeders' Cup Championship. Churchill Downs has responded by successfully hosting four of the top five attendance totals for the event: a record 80,452 in 1998; 71,671 in 1994, second; 71,237 in 1988, third; and 66,204 in 1991, fifth.</p>
             <p>Under Meeker's leadership, and through the direction of former Chairman Warner L. Jones, Jr., 1984 to 1992, and current Chairman William S. Farish, the track has experienced impressive growth in all areas.</p>
             <p>Combined Kentucky Derby Day wagering, on-track and national, has increased from $26,805,205 in 1985 to $88,941,006 in 1998. Churchill Downs has become a leader in simulcast wagering as both a host site and receiver. As a receiver, the $15 million Sports Spectrum, a state-of-the-art wagering center located seven miles from the track and constructed in 1992, has proved a national leader. The success in these areas has helped fuel Churchill Downs horsemen's purses, which have risen from a daily average of $187,363 during the 1990 Spring Meet to a record $469,643 during the 1999 season. Fall Meet purses have also grown to a daily average of $378,058.</p>
             <p>A key to the future success of Churchill Downs Incorporated rests upon the Company's potential for development and expansion. Headed by its most aggressive development effort since the days of the American Turf Association in the 1920s-1930s, the Churchill Downs Management Company, a wholly owned subsidiary of Churchill Downs Incorporated, opened Hoosier Park at Anderson in September 1994. The dual Standardbred and Thoroughbred track, located in Anderson, Ind., approximately 40 miles northeast of Indianapolis, serves as Indiana's first pari-mutuel racetrack and Churchill's first out-of-state racing site since 1937. Under the Hoosier Park license, the Company also operates off-track betting facilities in Merrillville, Fort Wayne and Indianapolis, Ind.</p>
             <p>In December 1997, Churchill Downs Incorporated formed the wholly owned subsidiary Churchill Downs Investment Company (CDIC), which oversees the Company's industry-related investments. In recent years, the Company has continued its aggressive growth cycle. In April 1998, the Company finalized the purchase of Ellis Park in Henderson, Ky., and Kentucky Horse Center in Lexington, Ky., for $22 million. In January 1999, the Company purchased a majority interest in Charlson Broadcast Technologies, LLC. The venture was developed as a means to provide simulcast graphic software and video services to racetracks and off-track betting facilities.</p>
             <p>In April 1999, the Company completed a $86 million purchase of Calder Race Course in Miami. The acquisition of Hollywood Park followed in September 1999. In 2000, Churchill Downs Incorporated completed the acquisition of Arlington International outside of Chicago, adding another entity to the Churchill Downs Incorporated network of racetracks.</p>
             <p>Churchill Downs Incorporated's success has been achieved through a corporate strategy based on strengthening its racing program and the Kentucky Derby, increasing the track's share of the national simulcast market, and the geographic expansion of its racing operations. This commitment to quality racing has made the Company one of the premier racing centers in North America.</p>
             <p>&nbsp;</p>
             {include file='/home/ah/allhorse/public_html/kd/links.tpl'}
             
</div> <!-- end: #col3 --> 
</div><!-- /#main --> 
