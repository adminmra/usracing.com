{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
  {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Famous Jockeys</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Jockeys generally get their mounts in races when the horses are training each morning. A jockey employs an agent, who, working for a percentage of their rider's winnings, tries to secure the best horse for them in each race. The tricky part for the agent comes when several trainers want their rider for the same race. Now the jockey's agent is like any bettor - they are handicapping the horses and putting their rider on the most likely winner.&nbsp;</p>


<p>Riding atop a Thoroughbred at speeds up to 40 miles per hour for a mile or more requires tremendous athletic ability and concentration. The best jockeys are skillful strategists and superior gamesmen. They are experts at bringing out the best qualities of their mounts. Top riders are also familiar with the characteristics of many other horses in the race. In addition to the athletic demands of racing, a jockey must maintain a certain weight for riding, normally between 100 and 115 pounds. Strict dieting and conditioning programs are a constant concern for most riders.&nbsp;</p>


<p>Handicapping jockeys is easier than handicapping horses. Here are the basic rules: Some jockeys are better than others. The better riders get to ride the better horses. Because they get the best horses to ride, the best jockeys win the most races. It's a circle. As it is with breeding and training, success creates success. Next to the horse, in Thoroughbred racing, the public is most familiar with the jockeys. It's important for bettors to know the top riders at a track. The best will win the most races. However, there are trends that bear watching. Like the trainers, the program has a stats page that lets you know what the trends are for that meet. But watch out, just like trainers, jockeys get hot and cold. Recognizing these trends can bring profits at the betting window.</p>


        


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    