{*include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"*}


{* old schema *}
{* {include file="/home/ah/allhorse/public_html/usracing/schema/belmont-stakes.tpl"} *}{*Is this content up-to-date? -TF*}
{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{include file="/home/ah/allhorse/public_html/belmont/schema.tpl"} {*Is this content up-to-date? -TF*}
{include file='inc/left-nav-btn.tpl'}<div id="left-nav">{include file='menus/belmontstakes.tpl'}</div>
{*
{literal}
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress"
            }
        },
        "name": "Belmont Stakes",
        "startDate": "2020-06-22",
        "endDate": "2020-06-22",
        "image": "/img/belmontstakes/bet-belmont-stakes-odds.jpg",
        "description": "Bet Belmont Stakes",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-20",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/belmont-stakes/betting"
    }
</script>
{/literal}
*}{* Old Hero Style - May 27, 20 - TF
	
	  <!--------- NEW HERO - MAY 2017 ---------------------------------------->
	
{literal}
    <style>
        .background{
        background-image: url("/img/index-bs/bg-belmont.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 110%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 104%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 98%;
  }
}

@media (min-width: 1024px) and (max-width: 1366px) {
  .background{
    background-position: 0% 100% !important;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 89%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 89%;
  }
}
    </style>
  {/literal}
  

<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">

 <div class="text text-xl centerText">{$h1}</div>

 <div class="text text-xl centerText" style="margin-top:0px;">{$h1b}</div>

 	<div class="text text-md centerText">{if $BS_Switch}Signup Today and Get Free Bets!{else}{$h2}{/if}

	 	<br>

	 		{* <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a> *}
		{* <a href="/signup?ref={$ref}" class="btn-xlrg ">{$signup_cta}</a>

	</div>

 </div> *}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-hero.tpl"}    
{*include file="/home/ah/allhorse/public_html/belmont/block-countdown-new.tpl"*}

<section class="countdown-section sm">
  <div class="countdown-top-block">
    <div class="countdown-icon icon-left">
      <img src="">
    </div>

    <div class="countdown-text">
      <span>Countdown to the {include_php file='/home/ah/allhorse/public_html/belmont/running.php'} Belmont Stakes</span>
    </div>

    <div class="countdown-icon icon-right">
      <img src="" style="transform: scaleX(-1);">
    </div>
  </div>

  <div class="countdown-bottom-block">
    <div class="countdown-icon icon-left">
      <img src="">
    </div>

    <div class="usr-countdown">
      <div class="timer">
        <span id="cd-days" class="quantity"></span>
        <span class="text">Days</span>
      </div>
      <div class="timer">
        <span id="cd-hours" class="quantity"></span>
        <span class="text">Hours</span>
      </div>
      <div class="timer">
        <span id="cd-minutes" class="quantity"></span>
        <span class="text">Minutes</span>
      </div>
      <div class="timer last">
        <span id="cd-seconds" class="quantity"></span>
        <span class="text">Seconds</span>
      </div>
    </div>

    <div class="countdown-icon icon-right">
      <img src="" style="transform: scaleX(-1);">
    </div>
  </div>

</section>
{literal}
    <script>
        var countDownDate = new Date("Jun 2, 2021 17:47:00").getTime();
    </script>
    <script src="/assets/plugins/countdown/countdown.js"></script>    
{/literal}


   <section class="bs-kd bs-kd--default usr-section bs">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bs/bs.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">
	          {if $BS_Switch_Early}{$h2_early} 
	          {elseif $BS_Switch_Main}{$h2_main} 
	          {elseif $BS_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}
	      </h3>

<p>{include file='/home/ah/allhorse/public_html/usracing/block_belmont/sales-copy.tpl'} </p>
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>





{*Unique copy goes here**************************************}
<p>
{literal}
<style>
	@media (max-width: 800px) {
		.hide-sm {
			display: none;
		}
	}
	@media (min-width: 801px) {
		.hide-lg {
			display: none;
		}
	}
</style>

<script type="application/ld+json">
	{
		"@context": "http://schema.org",
		"@type": "Table",
		"about": "2020 Belmont Stakes Odds "
	}
</script>
{/literal}

<!--<h2 class="hide-lg">2020 Belmont Stakes Odds</h2>-->
<h3 class="text-center text-uppercase">2020 Belmont Stakes Odds</h3>

<div id="no-more-tables">
	<table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult data" cellpadding="0"
		cellspacing="0" border="0" title="Belmont Stakes Odds and Post Positions"
		summary="The Latest odds for Belmont Stakes at Belmont Park">

		<thead>
			<tr>
				<th style="width: 8%;">PP</th>
	            <th style="width: 8%;">Horse</th>
	            <th style="width: 8%;">Odds</th>
	            <th style="width: 8%;">Jockey</th>
	            <th style="width: 13%;">Trainer</th>
			</tr>

		</thead>
	    <tbody>
	    	<caption class="hide-sm">Belmont Stakes Odds</caption>
	        <tr>
	            <td data-title="PP_test">1</td>
	            <td data-title="Horse"><strong>Tap It To Win</strong></td>
	            <td data-title="Odds">6-1</td>
	            <td data-title="Jockey">John R. Velazquez</td>
	            <td data-title="Trainer">Mark E. Casse</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">2</td>
	            <td data-title="Horse"><strong>Sole Volante</strong></td>
	            <td data-title="Odds">9-2</td>
	            <td data-title="Jockey">Luca Panici</td>
	            <td data-title="Trainer">Patrick L. Biancone</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">3</td>
	            <td data-title="Horse"><strong>Max Player</strong></td>
	            <td data-title="Odds">15-1</td>
	            <td data-title="Jockey">Joel Rosario</td>
	            <td data-title="Trainer">Linda Rice</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">4</td>
	            <td data-title="Horse"><strong>Modernist</strong></td>
	            <td data-title="Odds">15-1</td>
	            <td data-title="Jockey">Junior Alvarado</td>
	            <td data-title="Trainer">William I. Mott</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">5</td>
	            <td data-title="Horse"><strong>Farmington Road</strong></td>
	            <td data-title="Odds">15-1</td>
	            <td data-title="Jockey">Javier Castellano</td>
	            <td data-title="Trainer">Todd A. Pletcher</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">6</td>
	            <td data-title="Horse"><strong>Fore Left</strong></td>
	            <td data-title="Odds">30-1</td>
	            <td data-title="Jockey">Jose Ortiz</td>
	            <td data-title="Trainer">Doug O’Neill</td>
	        </tr>
	        <tr>
	            <td data-title="PP">.7</td>
	            <td data-title="Horse"><strong>Jungle Runner</strong></td>
	            <td data-title="Odds">50-1</td>
	            <td data-title="Jockey">Reylu Gutierrez</td>
	            <td data-title="Trainer">Steven M. Asmussen</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">8</td>
	            <td data-title="Horse"><strong>Tiz The Law</strong></td>
	            <td data-title="Odds">6-5</td>
	            <td data-title="Jockey">Manny Franco</td>
	            <td data-title="Trainer">Barclay Tagg</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">9</td>
	            <td data-title="Horse"><strong>Dr Post</strong></td>
	            <td data-title="Odds">5-1</td>
	            <td data-title="Jockey">Irad Ortiz, Jr.</td>
	            <td data-title="Trainer">Todd A. Pletcher</td>
	        </tr>
	        <tr>
	            <td data-title="PP_test">10</td>
	            <td data-title="Horse"><strong>Pneumatic</strong></td>
	            <td data-title="Odds">8-1</td>
	            <td data-title="Jockey">Ricardo Santana, Jr.</td>
	            <td data-title="Trainer">Steven M. Asmussen</td>
	        </tr>
	    </tbody>
	</table>


</div> 

<p class="dateUpdated center"><em id="updateemp">Updated Sep 29, 2021 09:00 EST .</em></p>


	
{literal}
<style>
	table.ordenable tbody th {
		cursor: pointer;
	}
</style>

<script src="/assets/js/jquery.sortElements.js"></script>

<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<script>
/*jQuery(document).ready(function(){var a=jQuery("table.ordenableResult tbody");jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function(){var d=jQuery(this),c=d.index(),b=false;d.click(function(){a.find("td").filter(function(){return jQuery(this).index()===c}).sortElements(function(f,e){t_a=jQuery.text([f]);t_b=jQuery.text([e]);if(t_a.indexOf("/")>0&&t_b.indexOf("/")>0){arr_a=t_a.split("/");arr_b=t_b.split("/");itemA_num=parseInt(arr_a[0]);itemB_num=parseInt(arr_b[0]);itemA_den=parseInt(arr_a[1]);itemB_den=parseInt(arr_b[1]);if(itemA_num>=itemA_den){americanA="+"+(itemA_num/itemA_den)*100}else{americanA="-"+(itemA_num/itemA_den)*100}if(itemB_num>=itemB_den){americanB="+"+(itemB_num/itemB_den)*100}else{americanB="-"+(itemB_num/itemB_den)*100}itemA=parseInt(americanA);itemB=parseInt(americanB);return itemA>itemB?b?-1:1:b?1:-1}if((t_a[0]=="-"||t_a[0]=="+")&&(t_b[0]=="-"||t_b[0]=="+")){itemA=parseInt(t_a);itemB=parseInt(t_b);console.log(itemA+" > "+itemB);return itemA>itemB?b?-1:1:b?1:-1}else{return jQuery.text([f])>jQuery.text([e])?b?-1:1:b?1:-1}},function(){return this.parentNode});b=!b})});jQuery("table.ordenableResult tbody th:first").trigger("click")});*/
function is_date(str){

return !isNaN(Date.parse(str));
}
jQuery(document).ready(function() {
    var a = jQuery("table.ordenableResult tbody");
    jQuery("table.ordenableResult thead th").append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function() {
        var d = jQuery(this),
            c = d.index(),
            b = true;
        d.click(function() {
            a.find("td").filter(function() {
                return jQuery(this).index() === c
            }).sortElements(function(f, e) {
                t_a = jQuery.text([f]);
                t_b = jQuery.text([e]);
                if(isNaN(t_a) || isNaN(t_b)){
			return jQuery.text([e]) > jQuery.text([f]) ? b ? -1 : 1 : b ? 1 : -1;
		}
		else return parseInt(jQuery.text([e])) > parseInt(jQuery.text([f])) ? b ? -1 : 1 : b ? 1 : -1
            }, function() {
                return this.parentNode
            });
            b = !b
        })
    });
    jQuery("table.ordenableResult thead th:first").trigger("click")
});
</script>

<script src="//www.usracing.com/assets/js/aligncolumn.js"></script>

<script>
	$(document).ready(function () {

		alignColumn([2], 'left');

	});
</script>
{/literal}
</p>

{* <p><i>bet  	&#9733; usraicng</i><!--hung-test--> has  the odds for the {$BELMONT_YEAR}  Belmont Stakes.  The Belmont Stakes is the third and final jewel of the Triple Crown. Belmont Stakes odds typically available shortly after the results of the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>.&nbsp; Here are the latest <a title="Belmont Stakes Odds" href="/belmont-stakes/odds">odds for the Belmont Stakes</a>.</p> *}
<p>The Belmont Stakes is a Grade I stakes race for three year-old Thoroughbreds at Belmont Park {* usually *} run on the second Saturday in June.  {* The race is also known as "The run for the Carnations", "the Test of the Champion " and the "Final Jewel of the Triple Crown". *}  This year, the race {* has been postponed a week and is *} will be held on {$BELMONT_DATE}.  You can watch the Belmont Stakes at 5 p.m. EST on TV on NBC or live stream on NBCSports.com. </p>
<p>The  odds are updated daily and are available for wagering. <br>Bet early and bet often!</p>
{*include file='/home/ah/allhorse/public_html/belmont/odds.php'*} 

 {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

 <!--hung-test-->
<a href="/famous-horses/secretarait">.</a>
<!--end-hung-test-->

 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}