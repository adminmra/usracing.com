<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

 
                                        
          
<div class="headline"><h1>US Racing Disclaimer</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>US Racing (www.usracing.com) is not a racebook, ADW, gambling or betting website. US Racing neither solicits nor accepts wagers. The racebook and ADW reviews, news, information, opinion, and recommendations on this website are for education and entertainment purposes only. This information is not intended to violate any local, state, or federal laws, and all persons using this website are solely responsible for complying with the laws of the jurisdictions in which they reside.</p>
<p>US Racing is not responsible for the accuracy of any handicapping information displayed on this website, and makes no claim to be a reliable source of information or advice for wagering or for any other purpose. US Racing does not intend to encourage or condone gambling in jurisdictions where it is prohibited or by persons who are under 18 years of age.</p>
<p>Persons who place wagers on the basis of the information found on this website do so at their own risk and with full knowledge of the fact that handicapping information presented on this website can turn out to be wrong. US Racing does not profit from your winnings, and is not liable for any losses incurred as a result of the information or recommendations found on this website.  US Racing is not responsible for any losses you may incur at any racebook or ADW that is reviewed or mentioned at this website.</p>
<p>Your continued use of the US Racing website signifies your consent to this disclaimer and the terms and conditions and privacy policy of this website.</p>
					
					
					
				
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 
 

 
</div><!-- end/row -->
</div><!-- /#container --> 


{literal} 
<script type="text/javascript">
$(document).ready(function() {
  $('.faqs').css("display","none");
});
function slideIt(thechosenone) {
     $('div[class|="faqs"]').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).slideDown(200);
          }
          else {
               $(this).slideUp(200);
          }
     });
}
</script> 
{/literal} 