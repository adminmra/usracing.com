
<div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-12">
            <div class="headline">
                <h1 id="password_reset-title">Just About There...</h1>
            </div>

            <div class="content">
                <div class="col-md-4 col-md-offset-4 calltoaction">
                    <div id="password_reset-helps">
                        <p style="font-size:20px;margin-bottom:5px;font-weight:bold;"><strong>Choose Your Password</strong></p>
                        <p style="color:#444;font-size:14px;">Pick a new password
                        {php}
                        if(isset($_GET["email"]) && $_GET["email"]!=""){
                            echo "for <strong>{$_GET['email']}</strong>";http://www.betusracing.ag/login?ctl00%24MainContent%24Account=USR10661&ctl00%24MainContent%24ctlLogin%24BtnSubmit=Login
                        }
                        {/php}.</p>
                    </div>


                    <form id="reset-pass" role="form">
                        <input class="" placeholder="token" maxlength="50" name="token" value="{php} echo $_GET['t']; {/php}" type="hidden">
                        <input class="form-control form-control-me form-custom-me" placeholder="password" maxlength="50" name="password" type="password" >
                        <input class="form-control form-control-me form-custom-me" placeholder="passwordconfirm" maxlength="50" name="passwordconfirm" type="password">
                        <!--<button type="submit" class="btn btn-sm btn-default" >Reset PW for reals</button>-->
                        <input class="btn btn-sm btn-red btn-red-form" type="submit" value="Update Password and Login">
                        <a style="color:#1B5F95;font-size:14px;display:block;margin-top:15px;" href="//www.betusracing.ag/login">Log In if you know your password</a>
                    </form>

                    <div style="color:#999;font-size:16px;" id="reset-pass-log"></div>
                </div>
            </div> <!-- end/ content -->
        </div> <!-- end/ #left-col -->
    </div><!-- end/row -->
</div><!-- /#container -->

{literal}
<script type="text/javascript">
  $(document).ready(function() {

    $('form#reset-pass').submit(function(event) {
        event.preventDefault();


        password        = $('form#reset-pass input[name=password]').val();
        passwordconfirm = $('form#reset-pass input[name=passwordconfirm]').val();

        if( password == "" ||  password != passwordconfirm ){
            $( "#reset-pass-log" ).text( "Your passwords do not match" );
        }
        else{
            var formData = {
                'token' : $('form#reset-pass input[name=token]').val(),
                'password' : $('form#reset-pass input[name=password]').val(),
                'passwordconfirm' : $('form#reset-pass input[name=passwordconfirm]').val()
            };

            $.ajax({
                type        : 'POST',
                url         : '//ww1.betusracing.ag/loginservicejson.php?a=setpasswordwithtoken',
                data        : formData,
                dataType    : 'json',
                encode      : true ,
                beforeSend: function( xhr ) {
                    $('form#reset-pass input[type=submit]')[0].disabled = true;
                    //$('#reset-pass-log').html("");
                }
            })
            .done( function( data ) {
                window.test = data;
                if ( data.STATUS == 1 ) {
                    $('form#reset-pass').hide();
                    $('div#password_reset-helps').hide();
                    //$('reset-pass-log').html("<h1>" + data.MSG + "</h1>");
                    $('#reset-pass-log').html("Thank you, your password has been updated successfully.");
                    window.location.assign("https://www.betusracing.ag/login?ctl00%24MainContent%24Account={/literal}{php}echo isset($_GET['email'])?$_GET['email']:'';{/php}{literal}&ctl00%24MainContent%24ctlLogin%24BtnSubmit=Login");
                }
                else if ( data.STATUS == 0 ) {
                    $('#reset-pass-log').html("<span style='color:red'>" + data.MSG + "</span>");
                    $('form#reset-pass input[type=submit]')[0].disabled = false;
                }
            })
            .fail( function( data ){
                $('form#reset-pass input[type=submit]')[0].disabled = false;
            });
        }




        // stop the form from submitting the normal way and refreshing the page

    });

});
</script>
{/literal}
