{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{include file="/home/ah/allhorse/public_html/usracing/schema/generated/dubai_world_cup.tpl"}

{literal}
<!-- <link rel="stylesheet" href="/assets/css/sbstyle.css"> -->
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}



{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
        }

        .firstNav > li {
          color: #444;
          font-family: 'Lato',sans-serif;
          font-size: 20px;
          font-weight: 300;
          margin: 0 0 40px;
          text-align: center;
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

    {include file="/home/ah/allhorse/public_html/dubai/block-countdown.tpl"}
    <section class=" dubai kd usr-section">
      <div class="container">
        <div class="kd_content">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h3}</h3>
          <p>{include file='/home/ah/allhorse/public_html/dubai/salesblurb.tpl'} </p>
          <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
          <br><br>

          {* <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Dubai World Cup Odds and Contenders</h2> *}
          <p>{include_php file='/home/ah/allhorse/public_html/dubai/odds_dubai_1005_xml_new.php'}</p>
          <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>

          <h2>Get your Picks Ready for {include_php file='/home/ah/allhorse/public_html/dubai/date.php'}.</h2>
          <p>The Group I <b>Dubai World Cup</b>, sponsored by Emirates, stands alone at the summit of international horse racing. As horse racing entered a new era with the changeover of centuries, it was the <b>Dubai World Cup</b>, inaugurated in 1996, that paved the way forward.</p>
          <p>With the United Arab Emirates and the Middle East, in general, hosting what is recognized as the thoroughbred "World Championship." Every thoroughbred in the world today descends from the three Arabian stallions exported from this part of the world - the Darley Arabian, the Byerley Turk and the Godolphin Arabian.</p>

          <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
        </div>
      </div>
    </section>
    {include file="/home/ah/allhorse/public_html/dubai/block-signup-bonus.tpl"}
    <section class="kd usr-section" style="background: none; padding-bottom: 0px;">
      <div class="container">
        <div class="kd_content">
          <h2>{*include file='/home/ah/allhorse/public_html/dubai/year_last.php'*} Dubai World Cup Race </h2>
          {*include file='/home/ah/allhorse/public_html/dubai/video_winner.php'*}
            <p>If anyone knows how much it costs to stage Saturday's lavish <b>Dubai World Cup</b> horse race, they're not saying.</p>
            <p>Once known as the world's richest race, the <b>Dubai World Cup</b> hands out $15.25 million in prize money alone, and costs millions more to organize, including flying in horses from Japan, the United States, South Africa and Europe.</p>
            <p>But the Nad Al Sheba racetrack -- and the United Arab Emirates -- doesn't allow betting, so there is little income to offset the millions laid out to hold the one-day, once-a-year spectacle.</p>
            <p>Even entry and parking are free, allowing poor immigrant families to mingle among the world's wealthy racing aficionados who've jetted in for the glamorous event.</p>
            <p>Sponsors' fees and broadcast rights recoup a portion of the costs, but most are simply paid for from the pockets of Dubai's royal family, the Maktoums. The family, incidentally, owns the powerful Dubai-based Godolphin stable, with several horses running on the day's seven-race card.</p>
            <p>"It's basically a tool to promote Dubai," said Matt Howard, spokesman for the Dubai Racing Club.</p>
            <p>With the race expected to reach one billion households, the Maktoum family stands to reap its dividends by pitching this beachfront sheikdom as one of the world's hottest luxury destinations.</p>
            <p>They might also use the race to showcase Dubai as one of the earth's most cosmopolitan cities.</p>
            <p><b>Here’s a typical scenario of the race:</b></p>
            <p>Crowds milling in Nad Al Sheba's grandstands, clubhouses and on the lawns covering the gamut of nationalities.</p>
            <p>Emirati men in long white dishdasha robes strolling with women in black head-to-toe chadors and copper-colored facial masks. Pakistani men in skullcaps decorated in glittering cut glass consulting their racing forms, alongside Indian women in bright, billowing saris.</p>
            <p>Westerners in sharp suits and dresses, with audacious hats, thronged at a food pavilion, quaffing champagne and watching fashion shows.</p>
            <p>At the other end of the grandstand, Somali men in skullcaps and white robes knelt in prayer, foreheads pressed to the grass.</p>
            <p>Beyond the track, with a backdrop of Dubai's glimmering modern skyscrapers, camel trainers walk their prize beasts amid the sand dunes. Camels also race at Nad Al Sheba, in another age-old tradition among Arabs.</p>
            <p align="center"><a href="https://www.usracing.com/signup?ref={$ref}" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
        </div>
      </div>
    </section>

    {include file="/home/ah/allhorse/public_html/dubai/block-testimonial.tpl"}

    <section class="kd usr-section" style="background: none; padding-bottom: 0px;">
      <div class="container">
        <div class="kd_content">
          <h2>{*include file='/home/ah/allhorse/public_html/dubai/year_last.php'*} Dubai World Cup Race Quick Facts: </h2>
          <p>
            <table class="show_design_border" border="0" cellspacing="0" cellpadding="0" width="100%">
              <tbody>
                <tr>
                  <td class="bodyregular">
                    <nav class="firstNav">
                        <li> The American champion Cigar's epic victory in 1996 began an honor roll of champions and set in motion an event that is now unrivalled in the world of international horse racing. 
                        <li>Since Cigar there has been Singspiel, Silver Charm, Almutawakel and Dubai Millennium.</li>
                        <li>In such a short space of time, the Dubai World Cup has produced a growth curve in quality unmatched anywhere else in the world. At US $6million, the Dubai World Cup is one of the jewels in the crown of worldwide horse racing. </li>
                        <li>The 2001 Dubai World Cup meeting set the benchmark with 70 international runners arriving in the United Arab Emirates to compete on the evening, the very best facilities awaiting both horses and their connections, highlighting the standards of excellence of which the UAE and Dubai are world renowned.</li>
                        <li> The Dubai World Cup meeting is the center of massive international exposure for Dubai, capturing an audience in the hundreds of millions. The meeting is attended by an unrivaled number of media and features regularly in prominent magazines and newspapers around the world.</li>
                        <li>The increasing quality of the Dubai World Cup itself is recorded by the post-race average ratings for all starters in the event.</li>
                    </nav>
                  </td>
                </tr>
              </tbody>
            </table>
          </p>
          <p align="center"><a href="https://www.usracing.com/signup?ref={$ref}" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
          
        </div>
      </div>
    </section>


  <!------------------------------------------------------------------>

{include file="/home/ah/allhorse/public_html/dubai/block-exciting.tpl"}


  <!------------------------------------------------------------------>

