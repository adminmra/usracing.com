<div id="main" class="container">
<div class="row">

<div id="left-col" class="col-md-9">

<div class="headline"><h1>Racetracks Available</i></h1></div>

<div class="content"> 
<!-- --------------------- content starts here ---------------------- -->
<p>Here is a complete list of thoroughbred, harness and greyhound racetracks sorted by state and country.</p>


{assign var="cta_append" value=" at any of these racetracks"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>


<h2><i class="flag US"></i>US Horse Racing Racetracks</h2>

<ul>
	<dt><a href="/alabama" >Alabama</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/alaska" >Alaska</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
<dt> <a href="/arizona" >Arizona</a></dt>
<li><a href="/turf-paradise" >Turf Paradise</a></li>
</ul>

<ul>
	<dt><a href="/arkansas" >Arkansas</a></dt>
	<li><a href="/oaklawn-park" >Oaklawn Park</a> </li>
</ul>
     
<ul>
	<dt><a href="/california" >California</a></dt>
	<li><a href="/cal-expo" >Cal Expo Harness</a> </li>
	<li><a href="/delmar" >Delmar</a> </li>
	<li><a href="/fairplex-park" >Fairplex Park</a> </li>
	<li><a href="/ferndale" >Ferndale</a> {* August 19, 2015 -	August 30, 2015<br> *}</li>
	<li><a href="/fresno" >Fresno</a> {* October 1, 2015 -	October 12, 2015 *}</li>
	<li><a href="/golden-gate-fields" >Golden Gate Fields</a> {* January 1, 2015 	- June 15, 2015 <br>	August 21, 2015 -	September 13, 2015 	<br> October 5, 2015 -	December 20, 2015 *}</li>
	<li><a href="/los-alamitos" >Los Alamitos</a> {* July 2, 2015 -	July 12, 2015 <br>	December 3, 2015 -	December 20, 2015 *}</li>
	<li><a href="/los-alamitos" >Los Alamitos Quarter Horse</a> {* January 1, 2015 -	December 20, 2015*}</li>
	<li><a href="/pleasanton" >Oak Tree at Pleasanton</a> {* June 17, 2015 -	July 6, 2015 <br>*}</li>
	<li><a href="/pleasanton" >Pleasanton</a> {* June 17, 2015 -	July 6, 2015  *}	</li>
	<li><a href="/sacramento" >Sacramento</a> {* July 30, 2015 -	August 16, 2015 *}</li>
	<li><a href="/santa-anita-park" >Santa Anita Park</a> {* January 1, 2015  - 	July 5, 2015 <br> 	September 26, 2015 	- October 25, 2015 *}</li>
	<li><a href="/santa-rosa" >Santa Rosa</a> {* July 30, 2015 -	August 16, 2015 *}</li>
	<li><a href="/stockton" >Stockton</a>  {* 	September 18, 2015 -	September 27, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/colorado" >Colorado</a></dt>
	<li><a href="/arapahoe-park" >Arapahoe Park</a> {* May 23, 2015 -	August 16, 2015 *}</li>
</ul>

<ul>
	<dt><a href="/conneticut" >Conneticut</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

     
<ul>
	<dt><a href="/delaware" >Delaware</a></dt>
	<li><a href="/delaware-park" >Delaware Park</a></li>
	<li><a href="/dover-downs" >Dover Downs Harness</a>{*  January 1, 2015 - April 9, 2015 <br>	November 1, 2015 -	December 30, 2015 	 *}</li>
	<li><a href="/harrington" >Harrington Raceway Harness</a> {* April 20, 2015 -	July 9, 2015 <br>	August 24, 2015 	- October 22, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/florida" >Florida</a></dt>
	<li><a href="/gulfstream-park" >Gulfstream Park</a> {* January 1, 2015 -	March 28, 2015 <br>	December 6, 2015 -	December 27, 2015 	 *}</li>
	<li><a href="/gulfstream-park" >Gulfstream Park West</a> {* January 2, 2015 -	June 29, 2015< *}</li>
	<li><a href="/hialeah-park" >Hialeah Park Quarter Horse</a> {* January 2, 2015 -	March 2, 2015 <br>	December 25, 2015 -	December 28, 2015<br> *}</li>
	<li><a href="/pompano-park" >Pompano Park Harness</a>{*  January 3, 2015 -	June 27, 2015 	<br> October 3, 2015 -	December 28, 2015 *}</li>
	<li><a href="/tampa-bay-downs" >Tampa Bay Downs</a>{*  January 2, 2015 -	May 3, 2015 <br>	November 28, 2015 -	December 30, 2015 *}</li>
</ul>
 
 <ul>
	<dt><a href="/georgia" >Georgia</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

 <ul>
	<dt><a href="/hawaii" >Hawaii</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>    
<ul>
	<dt><a href="/idaho" >Idaho</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>     
     
     
<ul>
	<dt><a href="/illinois" >Illinois</a></dt>
	<li><a href="/arlington-park" >Arlington Park</a> {* May 1, 2015 -	September 27, 2015 *}</li>
	<li><a href="/balmoral-park" >Balmoral Park Harness</a>{*  February 4, 2015 -	December 30, 2015 *}</li>
	<li><a href="/fanduel-racing" >FanDuel Racing</a>{*  March 24, 2014 	- September 7, 2015 *}</li>
	<li><a href="/hawthorne-race-course" >Hawthorne Race Course</a>{*  January 1, 2015 -	February 20, 2015 <br>	October 1, 2015 	- December 31, 2015 *}</li>
	<li><a href="/maywood-park" >Maywood Park Harness</a>{*  January 1, 2015 -	December 31, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/indiana" >Indiana</a></dt>
	<li><a href="/hoosier-park" >Hoosier Park Harness</a>{*  March 27, 2015 -	November 14, 2015 *}</li>
	<li><a href="/indiana-downs" >Indiana Downs</a> {* April 21, 2015 	- October 31, 2015*}</li>
	<li><a href="/indiana-downs" >Indiana Downs Harness</a> {* March 27, 2015 -	November 14, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/iowa" >Iowa</a></dt>
	<li><a href="/prairie-meadows" >Prairie Meadows</a>{*  April 17, 2015 -	August 8, 2015 *}</li>
	<li><a href="/prairie-meadows" >Prairie Meadows Quarter Horse</a> {* August 15, 2015 	October 17, 2015 *}</li>
</ul>

<ul>
	<dt><a href="/kansas" >Kansas</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>     
     
          
<ul>
	<dt><a href="/kentucky" >Kentucky</a></dt>
	<li><a href="/churchill-downs" >Churchill Downs</a>{*  April 25, 2015 -	June 27, 2015 <br>	September 11, 2015 -	September 27, 2015 <br>	November 1, 2015 -	November 29, 2015 *}</li>
	<li><a href="/ellis-park" >Ellis Park</a>{*  July 3, 2015 -	September 7, 2015 *}</li>
	<li><a href="/keeneland" >Keeneland</a> {* April 3, 2015- 	April 24, 2015 	<br> October 9, 2015 -	October 31, 2015 *}</li>
	<li><a href="/kentucky-downs" >Kentucky Downs</a>{*  September 10, 2015 -	September 19, 2015 *}</li>
	<li><a href="/turfway-park" >Turfway Park</a>{*  January 1, 2015 -	March 29, 2015 <br>	December 3, 2015 -	December 31, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/louisiana" >Louisiana</a></dt>
	<li><a href="/delta-downs" >Delta Downs</a> {* January 1, 2015 -	March 14, 2015 <br>	October 14, 2015 -	December 31, 2015 *}</li>
	<li><a href="/delta-downs" >Delta Downs Quarter Horse</a> {* April 24, 2015 -	July 11, 2015 *}</li>
	<li><a href="/evangeline-downs" >Evangeline Downs</a>{* April 8, 2015 -	August 29, 2015 *}</li>
	
	<li><a href="/evangeline-downs" >Evangeline Downs Quarter Horse </a>{* October 1, 2015 -	September 30, 2015 *}</li>
	<li><a href="/fair-grounds-race-course" >Fair Grounds Race Course</a>{* January 1, 2015 -	March 29, 2015 <br>	November 20, 2015 -	December 30, 2015 *}</li>
	<li><a href="/fair-grounds-race-course" >Fair Grounds Quarter Horse</a>{* August 14, 2015 -	September 4, 2015 *}</li>
	<li><a href="/louisiana-downs" >Louisiana Downs</a> {* May 1, 2015 	- September 19, 2015 *}</li>
	<li><a href="/louisiana-downs" >Louisiana Downs Quarter Horse</a> {* January 10, 2015 -	March 25, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/maine" >Maine</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/maryland" >Maryland</a></dt>
	<li><a href="/laurel-park" >Laurel Park</a> {* January 1, 2015 -	March 28, 2015 <br>	September 10, 2015 -	December 31, 2015 *}</li>
	<li><a href="/pimlico" >Pimlico</a> {* April 2, 2015 -	June 6, 2015 *}</li>
	<li><a href="#" >Timonium Racetrack</a>{* August 28, 2015 -	September 7, 2015 *}</li>

</ul>
     
<ul>
	<dt><a href="/massachusetts" >Massachusetts</a></dt>
	<li>{*<a href="/suffolk-downs" >Suffolk Downs</a>*} {* No Racing dates available *} <a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/michigan" >Michigan</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/minnesota" >Minnesota</a></dt>
	<li><a href="/canterbury-park" >Canterbury Park</a>{*  May 15, 2015 -	September 12, 2015 *}</li>
</ul>

<ul>
	<dt><a href="/mississippi" >Mississippi</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>   

<ul>
	<dt><a href="/missouri" >Missouri</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>   

<ul>
	<dt><a href="/montana" >Montana</a></dt>
	<li><a href="#">No Tracks</a></li>
</ul>     
     
<ul>
	<dt><a href="/nebraska" >Nebraska</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/nevada" >Nevada</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/new-hampshire" >Hew Hampshire</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/new-jersey" >New Jersey</a></dt>
	<li><a href="/atlantic-city-race-course" >Atlantic City Race Course</a> {* April 23, 2015 	-April 29, 2015<br> *}</li>
	<li><a href="/freehold-raceway" >Freehold Raceway Harness</a> {* January 2, 2015 -	May 16, 2015 <br>	September 4, 2015 -	December 12, 2015 *}</li>
	<li><a href="/meadowlands" >Meadowlands</a> {* September 11, 2015 -	October 31, 2015 *}</li>
	<li><a href="/meadowlands" >Meadowlands Harness</a> {* January 2, 2015  -	August 8, 2015 <br>	November 27, 2015 -	December 26, 2015 *}</li>
	<li><a href="/monmouth-park" >Monmouth Park</a> {* May 9, 2015 -	September 7, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/new-mexico" >New Mexico</a></dt>
	<li><a href="#" >Albuquerque St Fair</a> {* September 11, 2015 -	September 27, 2015 *}</li>
	<li><a href="#" >The Downs at Albuquerque</a> {* April 17, 2015 -	June 21, 2015  *}	</li>
	<li><a href="/ruidoso-downs" >Ruidoso Downs</a> {* May 22, 2015 -	September 7, 2015 *}</li>
	<li><a href="#" >SunRay Park</a> {* July 3, 2015 -	September 1, 2015<br> *}</li>
	<li><a href="/sunland-park" >Sunland Park</a> {* January 2, 2015 	- April 13, 2015 	<br>December 4, 2015 -	December 28, 2015 	 *}</li>
	<li><a href="/zia-park" >Zia Park</a> {* September 12, 2015 -	December 15, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/new-york" >New York</a></dt>
	<li><a href="/aqueduct" >Aqueduct</a> {* January 1, 2015 -	March 22, 2015 <br>	April 1, 2015 -	April 26, 2015 <br>	November 4, 2015 -	December 31, 2015 *}</li>
	<li><a href="/belmont-park" >Belmont Park</a> {* April 29, 2015 	July 19, 2015 	September 11, 2015 	November 1, 2015 *}</li>
	<li><a href="/buffalo-raceway" >Buffalo Raceway Harness</a> {* January 14, 2015 	- July 26, 2015<br> *}</li>
	<li><a href="/finger-lakes" >Finger Lakes </a> {* April 18, 2015 	- December 5, 2015< *}</li>
	<li><a href="/saratoga" >Saratoga </a> {* July 24, 2015 -	September 7, 2015 *}</li>
	<li><a href="/saratoga" >Saratoga Harness</a> {* July 17, 2015 	September 1, 2015 *}</li>
	<li><a href="#" >Monticello Raceway</a> {* January 2, 2015 -	December 30, 2015 *}</li>
	<li><a href="/yonkers-raceway" >Yonkers Raceway</a> {* January 9, 2015 -	December 13, 2015 *}</li>
</ul>

<ul>
	<dt><a href="/north-dakota" >North Dakota</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/north-carolina" >North Carolina</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/ohio" >Ohio</a></dt>
	<li><a href="#" >Belterra Park</a> {* April 27, 2015 -	September 27, 2015<br *}</li>
	<li><a href="#" >Belterra Park Quarter Horse</a>{*  August 9, 2015 -	September 13, 2015<br> *}</li>
	<li> <a href="#" >Mahoning Valley </a></li>
	<li><a href="/northfield-park" >Northfield Park Harness</a>{*  January 1, 2015 -	December 30, 2015 *}</li>
	<li><a href="#" >ThistleDown</a> {* April 27, 2015 -	October 24, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/oklahoma" >Oklahoma</a></dt>
	<li><a href="/remington-park" >Remington Park</a> {* March 6, 2015 - 	May 30, 2015 <br>	August 14, 2015 -	December 13, 2015 *}</li>
	<li><a href="/will-rogers-downs" >Will Rogers Downs</a> {* March 16, 2015 -	May 26, 2015 	 *}</li>
	<li><a href="/will-rogers-downs" >Will Rogers Downs Quarter Horse</a> {* September 12, 2015 -	November 14, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/oregon" >Oregon</a></dt>
	<li><a href="/portland-meadows" >Portland Meadows</a> {* January 1, 2015 - 	February 10, 2015 	October 14, 2015 	December 29, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/pennsylvania" >Pennsylvania</a></dt>
	<li><a href="/harrahs-philadelphia" >Harrah's Philadelphia Harness</a> {* March 27, 2015 -	December 13, 2015 *}</li>
	<li><a href="/parx-racing" >Parx Racing</a> {* January 3, 2015 -	December 29, 2015<br> *}</li>
	<li><a href="/penn-national" >Penn National</a>{*  January 2, 2015 -	December 27, 2015 *}</li>
	<li><a href="/pocono-downs" >Pocono Downs</a> {* March 22, 2015 -	November 22, 2015 	 *}</li>
	<li><a href="/presque-isle-downs" >Presque Isle Downs</a> {* May 10, 2015 -	September 24, 2015 *}</li>
	<li><a href="/the-meadows-racetrack" >The Meadows Harness</a> {* January 5, 2015 -	December 23, 2015 *}</li>
</ul>




<ul>
	<dt><a href="/rhode-island" >Rhode Island</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/south-carolina" >South Carolina</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/south-dakota" >South Dakota</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>
     
<ul>
	<dt><a href="/texas" >Texas</a></dt>
	<li><a href="/lone-star-park" >Lone Star Park</a> {* April 9, 2015 -	July 19, 2015 *}</li>
	<li><a href="/lone-star-park" >Lone Star Park Quarter Horses</a>{*  September 18, 2015 	November 14, 2015 *}</li>
	<li><a href="/retama-park" >Retama Park</a>{*  September 4, 2015  -	November 28, 2015 *}</li>
	<li><a href="/retama-park" >Retama Park Quarter Horse</a> {* June 5, 2015   - 	August 8, 2015 *}</li>
	
	<li><a href="/sam-houston-race-park" >Sam Houston Race Park</a> {* January 16, 2015 -	March 10, 2015 *}</li>
	<li><a href="/sam-houston-race-park" >Sam Houston Quarter Horse</a> {* March 27, 2015 -	May 16, 2015 *}</li>
	</ul>

<ul>
	<dt><a href="/tennessee" >Tennessee</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/utah" >Utah</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/vermont" >Vermont</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>

<ul>
	<dt><a href="/virginia" >Virginia</a></dt>
	<li><a href="/colonial-downs" >Colonial Downs</a></li>
</ul>
     
<ul>
	<dt><a href="/washington" >Washington</a></dt>
	<li><a href="/emerald-downs" >Emerald Downs</a>{*  April 18, 2015 -	September 27, 2015 *}</li>
</ul>
     
<ul>
	<dt><a href="/west-virginia" >West Virginia</a></dt>
	<li><a href="/charles-town" >Charles Town</a> {* January 2, 2015 -	December 30, 2015 *}</li>
	<li><a href="/mountaineer-park" >Mountaineer Park</a> {* March 1, 2015 -	December 21, 2015 *}</li>
	<li> <a href="#" >Tri-State Evening </a></li>
	<li> <a href="#" >Tri-State Matinee </a></li>
</ul>
     
<ul>
	<dt><a href="/wyoming" >Wyoming</a></dt>
	<li><a href="#" >No Tracks</a></li>
</ul>     
        
 {*Old Code Include_Php File='../Smarty/Libs/Racetracks/Racetracks.Php'*}
 
 <br />      
<hr />      <!-- --------------------- *****************  ---------------------- -->

<h2><i class="flag CA"></i> Canadian Horse Racing Racetracks</h2>
      
<ul>
	<dt><span>All Provinces</span></dt>	
    <li><a href="/fort-erie" >Fort Erie</a>{*  May 26, 2015 -	September 29, 2015 *}</li>
   
    <li><a href="/hastings-park" >Hastings Park</a> {* April 25, 2015 -	October 12, 2015 *}</li>
    <li><a href="/mohawk-raceway" >Mohawk Raceway</a> {* April 9, 2015 -	September 29, 2015< *}</li>
 
    <li><a href="/western-fair-raceway" >Western Fair Raceway</a>{* January 2, 2015 	May 29, 2015 	October 2, 2015 	December 31, 2015 	<br> *}</li>
    <li><a href="/woodbine" >Woodbine</a> {* April 11, 2015 -	December 6, 2015 *}</li>
    <li><a href="/woodbine" >Woodbine Harness</a> {* January 1, 2015 -	April 6, 2015 <br>	October 1, 2015 -	December 31, 2015 *}</li>
</ul>
     
  
<hr>  <!-- --------------------- *****************  ---------------------- -->	   

     
     
     
<h2>International Horse Racing Racetracks</h2>
         
<ul>
	<dt><a href="#" >Australia </a></dt>
    <li> <a href="#" >Australia A </a></li>
    <li> <a href="#" >Australia B  </a></li>
    <li> <a href="#" >Australia C  </a><br></li>
</ul>    

<hr>  <!-- --------------------- *****************  ---------------------- -->	  
	
<ul>
	<dt><a href="#" >Dubai </a></dt>
    <li> <a href="#" >Jebel Ali Racecourse </a><br></li>
	<li> <a href="/meydan-racecourse" >Dubai Meydan Race Course</a>{* March 28, 2015 *}</li>
	</ul>
	
<hr>  <!-- --------------------- *****************  ---------------------- -->	  
	 
<ul>
	<dt><a href="#" >France </a></dt>
	
	<li> <a href="#" >Auteuil </a><br></li>
	<li> <a href="#" >Chantilly </a><br></li>
	<li> <a href="#" >Deauville </a><br></li>
    <li> <a href="#" >Longchamp </a><br></li>
    <li> <a href="#" >Maisons Laffitte </a><br></li>
   <li> <a href="#" >Saint Cloud </a><br></li>
    
</ul>



<hr>  <!-- --------------------- *****************  ---------------------- -->	  

<ul>
	<dt><a href="#" >Hong Kong </a></dt>
    <li> <a href="#" >Happy Valley Racecourse </a><br></li>
    <li> <a href="#" >Sha Tin Racecourse </a><br></li>
</ul>  

<hr>  <!-- --------------------- *****************  ---------------------- -->	  
	
<ul>
	<dt><a href="#" >Ireland </a></dt>
	<li> <a href="#" >Ballinrobe</a></li>
	<li> <a href="#" >Bellewstown</a></li>
    <li> <a href="#" >Clonmel Racecourse </a><br></li>
    <li> <a href="#" >Cork Racecourse </a><br></li>
    <li> <a href="#" >Curragh Race Course </a><br></li>
    <li> <a href="#" >Dundalk Racecourse</a><br></li>
    <li> <a href="#" >Fairyhouse Racecourse </a><br></li>
    <li> <a href="#" >Galway</a></li>
    <li> <a href="#" >Gowran Park </a><br></li>
    <li> <a href="#" >Kilbeggan</a></li>
    <li> <a href="#" >Killarney</a></li>
    <li> <a href="#" >Laytown </a></li>
	<li> <a href="#" >Leopardstown Racecourse </a><br></li>
	<li> <a href="#" >Limerick </a></li>
	<li> <a href="#" >Listowel </a></li>
   
    <li> <a href="#" >Naas Racecourse </a><br></li>
    <li> <a href="#" >Navan Racecourse </a><br></li>
    <li> <a href="#" >Punchestown RaceCourse  </a><br></li>
    <li> <a href="#" >Roscommon </a></li>
    <li> <a href="#" >Sligo </a></li>
	<li> <a href="#" >Thurles Racecourse </a><br></li>
  	<li> <a href="#" >Tipperary </a></li>
  
    <li> <a href="#" >Tramore Racecourse </a><br></li>
    <li> <a href="#" >Wexford </a></li>
</ul>    

<hr>  <!-- --------------------- *****************  ---------------------- -->	  

<ul>
	<dt><a href="#" >Japan </a></dt>
    <li> <a href="hanshin">Hanshin Racecourse </a><br></li>
	<li> <a href="fukushima">Fukushima Racecourse </a><br></li>
	<li> <a href="nakayama">Nakayama Racecourse </a><br></li>
	<li> <a href="kawasaki">Kawasaki Racecourse </a><br></li>
	<li> <a href="oi">Oi Racecourse</a></li>
</ul>    
  
<hr>  <!-- --------------------- *****************  ---------------------- -->	  

  <ul>
	<dt><a href="#" >Northern Ireland </a></dt>
	<li> <a href="#" >Down Royal</a></li>
	<li> <a href="#" >Downpatrick</a></li>
</ul>  

<hr>  <!-- --------------------- *****************  ---------------------- -->	  

<ul>
	<dt><a href="#" >South Africa </a></dt>
	<li> <a href="#" >Durbanville</a></li>
	<li> <a href="#" >Flamingo</a></li>

	<li> <a href="#" >Greyville</a></li>
	<li> <a href="#" >Kenilworth</a></li>

    <li> <a href="#" >Scottsville </a><br></li>
    <li> <a href="#" >Turffontein </a><br></li>
    <li> <a href="#" >Vaal </a></li>
</ul> 

<hr>  <!-- --------------------- *****************  ---------------------- -->	  
											
<ul>
	<dt><a href="#" >United Kingdom </a></dt>
    <li> <a href="#" >Aintree Racecourse </a><br></li>
    <li> <a href="#" >Ascot Racecourse </a><br></li>
    <li> <a href="#" >Ayr Race Course </a><br></li>
    <li> <a href="#" >Bangor-on-Dee Racecourse </a><br></li>
    <li> <a href="#" >Bath Racecourse </a><br></li>
    <li> <a href="#" >Beverley Race Course </a><br></li>
    <li> <a href="#" >Brighton Racecourse </a><br></li>
    <li> <a href="#" >Carlisle Racecourse </a><br></li>
    <li> <a href="#" >Cartmel Racecourse </a><br></li>
    <li> <a href="#" >Catterick Racecourse </a><br></li>
    <li> <a href="#" >Cheltenham </a><br></li>
    <li> <a href="#" >Chepstow Racecourse </a><br></li>
    <li> <a href="#" >Chester Racecourse </a><br></li>
    <li> <a href="#" >Doncaster Racecourse </a><br></li>
    <li> <a href="#" >Epsom Downs </a><br></li>
    <li> <a href="#" >Exeter Racecourse </a><br></li>
    <li> <a href="#" >Fakenham Racecourse </a><br></li>
    <li> <a href="#" >Ffos Las Racecourse </a><br></li>
    <li> <a href="#" >Fontwell Park </a><br></li>
    <li> <a href="#" >Goodwood RaceCourse </a><br></li>
    <li> <a href="#" >Hamilton Park </a><br></li>
    <li> <a href="#" >Haydock </a><br></li>
    <li> <a href="#" >Hereford</a></li>

    <li> <a href="#" >Hexham Racecourse </a><br></li>
    <li> <a href="#" >Huntingdon Racecourse </a><br></li>
    <li> <a href="#" >Kelso Racecourse </a><br></li>
    <li> <a href="#" >Kempton Park </a><br></li>
    <li> <a href="#" >Leicester Racecourse </a><br></li>
    <li> <a href="#" >Lingfield Park </a><br></li>
    <li> <a href="#" >Ludlow Racecourse </a><br></li>
    <li> <a href="#" >Market Rasen Racecourse </a><br></li>
    <li> <a href="#" >Musselburgh Racecourse </a><br></li>
    <li> <a href="#" >Newbury Race Course  </a><br></li>
    <li> <a href="#" >Newcastle Racecourse </a><br></li>
    <li> <a href="#" >Newmarket Racecourse </a><br></li>
    <li> <a href="#" >Newton Abbot Racecourse </a><br></li>
    <li> <a href="#" >Nottingham Racecourse </a><br></li>
    <li> <a href="#" >Perth Racecourse </a><br></li>
    <li> <a href="#" >Plumpton Racecourse  </a><br></li>
    <li> <a href="#" >Pontefract Racecourse </a><br></li>
    <li> <a href="#" >Redcar Race Course </a><br></li>
    <li> <a href="#" >Ripon Racecourse </a><br></li>
    <li> <a href="#" >Salisbury Racecourse </a><br></li>
    <li> <a href="#" >Sandown Park </a><br></li>
    <li> <a href="#" >Sedgefield Racecourse </a><br></li>
    <li> <a href="#" >Southwell Racecourse </a><br></li>
    <li> <a href="#" >Stratford Racecourse  </a><br></li>
    <li> <a href="#" >Taunton Racecourse </a><br></li>
    <li> <a href="#" >Thirsk Racecourse </a><br></li>
    <li> <a href="#" >Towcester Racecourse  </a><br></li>
    <li> <a href="#" >Uttoxeter Racecourse </a><br></li>
    <li> <a href="#" >Warwick Racecourse </a><br></li>
    <li> <a href="#" >Wetherby Racecourse </a><br></li>
    <li> <a href="#" >Wincanton Racecourse </a><br></li>
    <li> <a href="#" >Windsor Race Course </a><br></li>
    <li> <a href="#" >Wolverhampton </a><br></li>
    <li> <a href="#" >Worcester Racecourse </a><br></li>
    <li> <a href="#" >Yarmouth Racecourse </a><br></li>
    <li> <a href="#" >York Race Course </a><br></li>
</ul>   										
		
		
<hr>		
														
		<h2>Greyhound Racetracks</h2>						 	  		
	
<ul>	
<dt><a href="/iowa" >Iowa</a></dt>						
<li><a href="#" >Bluffs Run</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 			
</ul> 	



<ul>
<dt><a href="/florida" >Florida</a></dt>
<li><a href="#" >Derby Lane Evening</a> 	{* January 1, 2015 -	December 31, 2015 *}</li> 				
<li><a href="#" >Derby Lane Matinee</a> 	{* January 1, 2015 -	December 31, 2015 *}</li> 				
<li><a href="#" >Orange Park Evening</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 				
<li><a href="#" >Orange Park Matinee</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 				
<li><a href="#" >Palm Beach Evening</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 				
<li><a href="#" >Palm Beach Matinee</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 				
</ul> 
<ul>
<dt><a href="/arkansas" >Arkansas</a></dt>
<li><a href="#" >Southland Evening</a> 	{* January 1, 2015 -	December 31, 2015 *}</li> 				
<li><a href="#" >Southland Matinee</a> {* 	January 1, 2015 -	December 31, 2015 *}</li>
</ul> 
<ul>
<dt><a href="/west-virginia" >West Virginia</a></dt>			
<li><a href="#" >Wheeling Downs Evening</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li> 				
<li><a href="#" >Wheeling Downs Matinee</a> 	{* January 1, 2015 -	December 31, 2015<br> *}</li>     
</ul>     
     
     				

      
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
