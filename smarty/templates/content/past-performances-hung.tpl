<div id="main" class="container">
<div class="row">

<div id="left-col" class="col-md-9">

<div class="headline"><h1>Free Past Performances (PPs)</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>Need an edge to take your handicapping to the next level? Learning to be a better horse bettor means research and practice.  Just like the best poker players, the more you study and practice-- the better your chances to increase your odds of winning.  Past performances can help you improve your handicapping skills.  They are an important part of any horse betting system.</p>
<p>Sharpen your day at the track or when your are wagering on horses from your mobile phone and remember to take a little time to research the past performances of the horses, jockeys and trainers. Good luck with your horse picks!</p> 



<p><a href="/signup"><img class="img-responsive" src="/img/handicapping.jpg" alt="Free Past Performances">  </a></p>
<p>Ready to bet on horses online?  You can bet from your home computer, mobile or tablet with BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members.  <a href="/signup/">Join </a> Today!</p>
<br>
<!--<iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/P3pLgjTPXCQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>-->
<p>Learn <a href="#How-to-Read-Past-Performances">How to Read Past Performances</a></p>
{include file='/home/ah/allhorse/public_html/schedules/past_performances.php'}
<a name="How-to-Read-Past-Performances"></a>
<h2> How to Read Past Performances</h2>


<p>Angles appear in past performances when the trainer has an return of investment (“ROI”) of more than 2.00 </p>

<p>ROI is measured based on a $2 wager </p>

<blockquote><strong>Example:</strong>

An ROI of 3.62 means that if you were to wager $2 on an a given trainer angle, you would receive back $3.62 for every $2 wagered. </blockquote>

<p>In this Past Performances Chart, racetracks are ordered alphabetically with data:</p>

<blockquote>3 starts and a winning percent of 100% or <br>
4-6 starts and a winning percent of at least 50% or <br>
7-15 starts and a winning percent of at least 40% or <br>
16-30 starts and a winning percent of at least 30% or <br>
31 or more starts and a winning percent of at least 25% <br></blockquote>

<p>Remember:   Past performances and ROI do not guarantee future performance and ROI at the racetrack.</p>

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}

</div><!-- end: #right-col -->


</div><!-- end/row -->
</div><!-- /#container -->

