{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
    {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Famous Horses</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>Breeders of Thoroughbred racehorses have a motto: "Breed the best to the best, and hope for the best." Farms from California to New York, Florida to Maryland, are all trying to emulate what breeders in the state of Kentucky have done for centuries -- produce champions.</p>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    