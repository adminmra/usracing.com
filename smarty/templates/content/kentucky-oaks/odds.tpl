{assign var="salescopy" value="oaks"}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks Odds</h2>
<p>{*{include_php file='/home/ah/allhorse/public_html/generated/odds/kd/odds_kentucky_oaks.php'}</p>
 {*include file="/home/ah/allhorse/public_html/oddsfeed/odds/1006.php"*}
 {include_php file="/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_hardcoded.php"}</p>
<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">{$button_cta}</a></p>
<p>Run on the the day before the Kentucky Derby, the Kentucky Oaks is a Grade I race for three-year-old fillies with a $1 million purse. Won in recent years by such superstars as Rachel Alexandra (2009), Untapable (2014) and Cathryn Sophia (2016), this year’s event will feature some incredible horses.  Who do you think is going to with this year's Kentucky Oaks?</p>
<p>{*In her last start, Farrell proved she can rate by coming from just off the pace to score a decisive victory in the Grade II Fair Grounds Oaks at <a href="/fair-grounds-race-course">Fair Grounds Racecourse.</a></p>
<p>While Farrell has been tearing it up in the Midwest, Miss Sky Warrior has dominated in the East. Following a fourth-place finish in her debut at Belmont Park, the daughter of First Samurai has raced on the pace and off the pace in winning four consecutive graded stakes, including the Grade II Gazelle by 13 widening lengths. *}</p>

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}