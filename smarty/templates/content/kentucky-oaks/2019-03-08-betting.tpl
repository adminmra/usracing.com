{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/kentuckyderby.tpl'}
</div>
<!-- ---------------------- end left menu contents ------------------- -->   
<!--
<div class="container-fluid">
	
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-oaks-odds-hero.jpg" alt="Kentucky Derby Oaks"></a>
		
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-oaks-odds-smal-hero.jpg" alt="Kentucky Derby Oaks">  </p> </a>
	
</div>       
-->

<!-- ----------------------main menu contents ------------------- -->
<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl-left">{$h1}</div>
        <div class="text text-md-left">{$h2}
                <br>
                        <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
        </div>
 </div>
<!-- ----------------------end main menu contents ------------------- -->
<!------------------------------------------------------------------>
    <section class="friends block-center usr-section" style="margin-top:20px;">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-kd/friends.png" alt="" class="img-responsive friends_img"></div>
      </div>
    </section>

<!------------------------------------------------------------------>
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
         
           <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Oaks Betting" class="kd_logo img-responsive"></a>
          
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">With the Most Oaks Bet Options Anywhere</h3>
       
<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb-oaks.tpl'} </p>




<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Oaks Odds</h2><p><br>
{include file='/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_1006.php'}</p>
 {*include file="/home/ah/allhorse/public_html/oddsfeed/odds/1006.php"*}

 <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Kentucky Oaks</a></p>





<p>The Kentucky Oaks is America's richest race for three-year-old fillies (female horses) and takes place each year on the Friday before the Kentucky Derby. Like the Derby, it was founded by Colonel Meriwether Lewis Clark - grandson of William Clark (of Lewis and Clark fame) - and is modeled after the Epsom Oaks. Since it achieved Grade I status in 1978, the Kentucky Oaks winner has gone on to be named champion three-year-old filly 12 times.

Who do you think is going to with this year's Kentucky Oaks? <p>{*This year, Breeders' Cup Juvenile Fillies victress Songbird is a prohibitive favorite to be draped in a garland of lilies on the first Friday in May. She's undefeated (as of March 24) and has been geared down by regular rider Mike Smith in each of her last two starts. In the Oaks, Songbird is expected to be challenged by another undefeated filly, Cathryn Sophia, as well as last year's BC Juvenile Fillies runner-up Rachel's Valentina (daughter of Horse of the Year Rachel Alexandra, who won the Oaks in 2009).  Sadly, Songbird was pulled from the Oaks due to a high fever.  This, of course, was the best decision made by her connections.  We hope she gets healthy and look to her racing at the Preakness Stakes or possibly the Belmont Stakes. *}</p>


        </div>
      </div>
    </section>

    <!------------------------------------------------------------------>    
    

{include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-exciting.tpl"}
<!------------------------------------------------------------------>      
<div id="main" class="container">
<p>{include file='inc/disclaimer-kd.tpl'} </p>
</div><!-- /#container --> 
