
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>

<div class="container-fluid">
	
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-oaks-odds-hero.jpg" alt="Kentucky Derby Oaks"></a>
		
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-oaks-odds-smal-hero.jpg" alt="Kentucky Derby Oaks">  </p> </a>
	
</div>       
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          
<div class="headline"><h1>Kentucky Oaks Odds</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch_ko.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'}</p>
{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}
<p>What are the Kentucky Oaks Odds for  {include file='/home/ah/allhorse/public_html/kd/year.php'}?</p>
<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Oaks Odds</h2><p>{include file='/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_1006_xml.php'}</p>
<p>Run on the first Friday in May, the Kentucky Oaks is a Grade I race for three-year-old fillies with a $1 million purse. Won in recent years by such superstars as Rachel Alexandra (2009), Untapable (2014) and Cathryn Sophia (2016), this year’s event features an intriguing cast led by Farrell, who is undefeated this year and 5-for-7 lifetime.</p>
<p>In her last start, Farrell proved she can rate by coming from just off the pace to score a decisive victory in the Grade II Fair Grounds Oaks at <a href="/fair-grounds-race-course">Fair Grounds Racecourse.</a></p>
<p>While Farrell has been tearing it up in the Midwest, Miss Sky Warrior has dominated in the East. Following a fourth-place finish in her debut at Belmont Park, the daughter of First Samurai has raced on the pace and off the pace in winning four consecutive graded stakes, including the Grade II Gazelle by 13 widening lengths.</p>



 <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Kentucky Oaks</a></p>


<p>Keep an eye on Paradise Woods.  Paradise Woods stamped herself as a major player with a resounding 11 ¾-length win in the Grade I Santa Anita Oaks — the same race that catapulted Winning Colors to the <a href="/kentucky-derby/odds">Kentucky Derby</a> winner’s circle in 1988.
</p>
 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	 <!-- ------------------------ CTA -------------------------- -->

<div class="calltoaction">
	
	<h2>Bet the<BR>Kentucky Oaks!</h2>
<h3>Get exclusive Derby odds and more.</h3>
	
	{include file="/home/ah/usracing.com/smarty/templates/inc/rightcol-calltoaction-form.tpl"}

	 <!-- ------------------------ CTA ends -------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
