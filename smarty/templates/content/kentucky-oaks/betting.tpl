{assign var="salescopy" value="oaks"}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks Odds</h2>
<p>{include_php file='/home/ah/allhorse/public_html/generated/odds/kd/odds_kentucky_oaks.php'}</p>
 {*include file="/home/ah/allhorse/public_html/oddsfeed/odds/1006.php"*}

 <p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Kentucky Oaks</a></p>


<p><strong>Where can I  bet on the {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks? </strong> <a href="/signup?ref={$ref}">BUSR</a> offers Futures Wagers on  the Kentucky Oaks. This year the race will be  held on {$KO_DATE}.</p> 


<p><strong>How can I bet on the {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Oaks? </strong> There are many different ways to place your wager on the Oaks. At BUSR, You just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR Sportsbook, you can bet on Kentucky Derby Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. You will also find   all kinds of bets including Future Wagers where you can bet the Kentucky Derby weeks, if not months, in advance.</p>

<p><strong>Where can I Watch the Kentucky Oaks {include file='/home/ah/allhorse/public_html/kd/year.php'}? </strong> You can watch the race coverage at 12:30 p.m. EST on TV on NBCSN or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Kentucky Oaks today? </strong> Visit <a href="/kentucky-oaks/odds"> Kentucky Oaks Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>

<p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on Kentucky Oaks</a></p>


{*
<p>The Kentucky Oaks is America's richest race for three-year-old fillies (female horses) and takes place each year on the Friday before the Kentucky Derby. Like the Derby, it was founded by Colonel Meriwether Lewis Clark - grandson of William Clark (of Lewis and Clark fame) - and is modeled after the Epsom Oaks. Since it achieved Grade I status in 1978, the Kentucky Oaks winner has gone on to be named champion three-year-old filly 12 times.

Who do you think is going to with this year's Kentucky Oaks? <p>
*}
{*This year, Breeders' Cup Juvenile Fillies victress Songbird is a prohibitive favorite to be draped in a garland of lilies on the first Friday in May. She's undefeated (as of March 24) and has been geared down by regular rider Mike Smith in each of her last two starts. In the Oaks, Songbird is expected to be challenged by another undefeated filly, Cathryn Sophia, as well as last year's BC Juvenile Fillies runner-up Rachel's Valentina (daughter of Horse of the Year Rachel Alexandra, who won the Oaks in 2009).  Sadly, Songbird was pulled from the Oaks due to a high fever.  This, of course, was the best decision made by her connections.  We hope she gets healthy and look to her racing at the Preakness Stakes or possibly the Belmont Stakes. *}</p>

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}