<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


<div class="headline"><h1><a href="{$smarty.server.REQUEST_URI}" title="{if isset($article)} {$article.Title}{else}Horse Racing News{/if}">

{if isset($article)} {$article.Title} {else}Horse Racing News{/if}</a></h1></div>     
       

<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{if isset($article)}
<div class="news">
<div id="article" class="article">
 
	{if $article.featured == '1'}
	<img class="img-responsive" src="/Feeds/TrackResults/Cache/{$article.id}.jpg" vspace="0" hspace="0" align="left" style="margin: 0px 0 10px; " />
	{/if}
	{$article.Html}

</div><!-- end/article -->
<div class="news-date">{$article.DateCreated}</div>
</div><!-- end/news -->
{/if}

{if isset($articles)}
                            

<div class="news">
  <div class="results">

    {foreach from=$articles item=article}
    	{$article}
    {/foreach}
    
  </div><!-- end/results -->
  {$pager}
</div><!-- end/news -->
{/if}

            
            
                      
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
