{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/horsebetting.tpl'} 
<!-- ---------------------- end left menu contents ------------------- -->         
</div> 
    


<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Contact Us</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<img alt="usracing-support" class="img-responsive pull-right rgt-img-margin" src="/img/usracing-support.jpg" />                          
                          
<p><strong>Welcome to US Racing!</strong></p>

<p>If have questions or comments, please send us an email and a US Racing Team Member will get back to you.</p>

<p><strong>Email:</strong> cs@usracing.com</p>
<p><strong>Customer Support: </strong> {include file='inc/phone-all.tpl'}</p>

<br /><br />
<div class="tag-box tag-box-v6">
<h2>Here is some information that you may find helpful:</h2>
<ul>
<li><strong><a href="/faqs">FAQS</a></strong></li>
<li><strong><a href="/horse-racing-terms">Racing Terms</a></strong></li>
<li><strong><a href="/how-to/bet-on-horses">How to Wager</a></strong></li>
</ul>
</div>



<p>If you still have questions, please don't hesitate to contact us!</p>




             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
