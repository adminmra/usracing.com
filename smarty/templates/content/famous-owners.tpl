{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/connections.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Famous Owners</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Nothing is more thrilling than standing at the finish line holding a ticket on the horse that is about to win the race. Wait a minute, nothing is more thrilling than OWNING the horse that is about to win the race. The ecstasy can be found on the faces of the connections in any winner's circle photograph, whether it's a maiden race (the horse's first win) or the Kentucky Derby.&nbsp;</p>
<p>Nowadays, folks with reasonable bankrolls who buy affordable horses own the majority of horses. However, racing is also full of partnerships in which several people pool their cash to purchase horses. People put their money together as a way of cutting down expenses and the risk if something does go wrong, but they still get all the thrills and excitement when the horse they "own" comes home a winner.</p>
<p>Maybe the ultimate partnership success story, Seattle Slew, winner of the 1977 Triple Crown and one of the greatest sires of the latter 20th century, was purchased by a group of four people for $17,500, while earning the quartet more than $100 million in his racing and breeding careers. While this does not happen to everybody investing in the game, anyone can be a successful owner with careful management and sound planning.</p>

<p><strong>Owners' Silks</strong></p>
<p>For the casual fan, finding your horse during a race can be difficult if the field is full of a bunch of "brown" horses that all look similar. Fans find their horse by the numbered, colored saddle towels, or the colored SILKS that the jockeys wear during the race. Every set of silks represents the owner; a design they created themselves.</p>
<p>First incorporated in the early days of racing, the owners could pick out their horse and rider from the viewing stand by spotting their silks across the meadow from the racers. Back then, silks were simple - red, blue, gold, gray or black. Even today, race callers rely on silks to find each horse.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    