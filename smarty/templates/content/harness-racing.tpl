{literal}<style type="text/css"></style>
{/literal}

  {include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

      
         
          
 {include file='menus/horsebetting.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div> 
    
    
    <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

         
                                        
          
<div class="headline"><h1> Harness Racing Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>You can bet on harness races from your home computer, mobile or tablet.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>

<p>Harness racing is a form of horse racing whereby the horses race at a specific trot or pace also known as a gait. They usually pull a two-wheeled cart called a sulky. </p>

<h2>Trotting vs Pacing</h2>


<strong>Trotting</strong>
  <img src="/images/trotter.jpg" 
                     alt="Trotting Harness Racing"  class="img-responsive"  align="left" hspace="10" width="400" > 
 <p> A trotter moves its legs forward in diagonal pairs (right front and left hind, then left front and right hind striking the ground simultaneously), whereas a pacer moves its legs laterally (right front and right hind together, then left front and left hind).</P>      <p>&nbsp;</p>     <p>&nbsp;</p>                 
                     
<strong>Pacing</strong>
 <img src="/images/pacer.jpg" 
                     alt="Pacing Harness Racing"  class="img-responsive"   align="left" hspace="10"width="400"  > 

<p>Pacing races constitute about 85% of the harness races conducted in North America. Pacing horses are faster and  less likely to break stride. A horse which starts to gallop must be slowed down and taken to the outside until it resumes trotting or pacing.</p><p>&nbsp;</p>     <p>&nbsp;</p>        

<p><i>Images provided by <a href="http://visual.merriam-webster.com/">Merriam-Webster</a></i></p>
<p>Pacers are less likely to break stride as they often wear hobbles (a hobble is a strap connecting the legs on each of the horse's sides).  The pace is a natural gait for many horses, and hobbles are an aid in supporting the gait at top speed; trotting hobbles (a different design, due to the difference in the gait) are becoming increasingly popular for the same reason. </p>

<h2>The Races</h2>
<p>The majority of  harness races start from behind a motorized starting gate. The horses line up behind a slow-moving, hinged gate that leads them to the starting line. At the line, the wings of the gate are folded up and the vehicle accelerates away from the horses.</p> <p>

An alternative starting method is the standing start, where there are tapes or imaginary lines across the track behind which the horses either stand stationary or trot in circles in pairs in a specific pattern to hit the starting line as a group. This enables handicaps to be placed on horses (according to class) with several tapes, usually with 10 or 20 meters between tapes. Many European – and some Australian and New Zealand – races use a standing start.</p>

<p>The sulky (informally known as a "bike") is a light, two-wheeled cart equipped with bicycle wheels. The driver, not a jockey,  carries a light whip chiefly used to signal the horse by tapping and to make noise by striking the sulky shaft.</p>

<h2>US Racing offers harness betting on the following tracks.</h2>



<table class="infoEntries">
                          <tr valign="top">
                            <td width="246"><dl>
                                <dt> <a href="/arizona" >ARIZONA</a></dt>
                                <li><a href="/turf-paradise" >TURF PARADISE</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/arkansas" >ARKANSAS</a></dt>
                                <li><a href="/oaklawn-park" >OAKLAWN PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/california" >CALIFORNIA</a></dt>
                                <li><a href="/hollywood-park" >HOLLYWOOD PARK</a></li>
                                <li><a href="/cal-expo" >CAL EXPO</a></li>
                                <li><a href="/delmar" >DELMAR</a></li>
                                <li><a href="/fairplex-park" >FAIRPLEX PARK</a></li>
                                <li><a href="/ferndale" >FERNDALE</a></li>
                                <li><a href="/fresno" >FRESNO</a></li>
                                <li><a href="/golden-gate-fields" >GOLDEN GATE FIELDS</a></li>
                                <li><a href="/los-alamitos" >LOS ALAMITOS</a></li>
                                <li><a href="/pleasanton" >PLEASANTON</a></li>
                                <li><a href="/sacramento" >SACRAMENTO</a></li>
                                <li><a href="santa-anita-park" >SANTA ANITA PARK</a></li>
                                <li><a href="/santa-rosa" >SANTA ROSA</a></li>
                                <li><a href="/stockton" >STOCKTON</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/colorado" >COLORADO</a></dt>
                                <li><a href="/arapahoe-park" >ARAPAHOE PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/delaware" >DELAWARE</a></dt>
                                <li><a href="/delaware-park" >DELAWARE PARK</a></li>
                                <li><a href="/dover-downs" >DOVER DOWNS</a></li>
                                <li><a href="/harrington" >HARRINGTON RACEWAY</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/florida" >FLORIDA</a></dt>
                                <li><a href="/calder-race-course" >CALDER</a></li>
                                <li><a href="/gulfstream-park" >GULFSTREAM PARK</a></li>
                                <li><a href="/hialeah-park" >HIALEAH PARK</a></li>
                                <li><a href="/pompano-park" >POMPANO PARK</a></li>
                                <li><a href="/tampa-bay-downs" >TAMPA BAY DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/illinois" >ILLINOIS</a></dt>
                                <li><a href="/arlington-park" >ARLINGTON PARK</a></li>
                                <li><a href="/balmoral-park" >BALMORAL PARK</a></li>
                                <li><a href="/fairmount-park" >FAIRMOUNT PARK</a></li>
                                <li><a href="/hawthorne-race-course" >HAWTHORNE RACE COURSE</a></li>
                                <li><a href="/maywood-park" >MAYWOOD PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/indiana" >INDIANA</a></dt>
                                <li><a href="/hoosier-park" >HOOSIER PARK</a></li>
                                <li><a href="/indiana-downs" >INDIANA DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/iowa" >IOWA</a></dt>
                                <li><a href="/prairie-meadows" >PRAIRIE MEADOWS</a></li>
                              </dl></td>
                            <td width="278"><dl>
                                <dt><a href="/kentucky" >KENTUCKY</a></dt>
                                <li><a href="/churchill-downs" >CHURCHILL DOWNS</a></li>
                                <li><a href="/ellis-park" >ELLIS PARK</a></li>
                                <li><a href="/keeneland" >KEENELAND</a></li>
                                <li><a href="/kentucky-downs" >KENTUCKY DOWNS</a></li>
                                <li><a href="/turfway-park" >TURFWAY PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/louisiana" >LOUISIANA</a></dt>
                                <li><a href="/delta-downs" >DELTA DOWNS</a></li>
                                <li><a href="/evangeline-downs" >EVANGELINE DOWNS</a></li>
                                <li><a href="/fair-grounds-race-course" >FAIR GROUNDS RACE COURSE</a></li>
                                <li><a href="/louisiana-downs" >LOUISIANA DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/maine" >MAINE</a></dt>
                                <li><a href="/hollywood-slots-raceway" >HOLLYWOOD SLOTS RACEWAY</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/maryland" >MARYLAND</a></dt>
                                <li><a href="/laurel-park" >LAUREL PARK</a></li>
                                <li><a href="/pimlico" >PIMLICO</a></li>
                                <li><a href="/rosecroft-raceway" >ROSECROFT RACEWAY</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/massachusetts" >MASSACHUSETTS</a></dt>
                                <li><a href="/suffolk-downs" >SUFFOLK DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/minnesota" >MINNESOTA</a></dt>
                                <li><a href="/canterbury-park" >CANTERBURY PARK</a></li>
                                <li><a href="/running-aces" >RUNNING ACES</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/nebraska" >NEBRASKA</a></dt>
                                <li><a href="/fonner-park" >FONNER PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/new-jersey" >NEW JERSEY</a></dt>
                                <li><a href="/atlantic-city-race-course" >ATLANTIC CITY RACE COURSE</a></li>
                                <li><a href="/freehold-raceway" >FREEHOLD RACEWAY</a></li>
                                <li><a href="/meadowlands" >MEADOWLANDS</a></li>
                                <li><a href="/monmouth-park" >MONMOUTH PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/new-mexico" >NEW MEXICO</a></dt>
                                <li><a href="/ruidoso-downs" >RUIDOSO DOWNS</a></li>
                                <li><a href="/sunland-park" >SUNLAND PARK</a></li>
                                <li><a href="/zia-park" >ZIA PARK</a></li>
                              </dl></td>
                            <td width="235"><dl>
                                <dt><a href="/new-york" >NEW YORK</a></dt>
                                <li><a href="/aqueduct" >AQUEDUCT</a></li>
                                <li><a href="/belmont-park" >BELMONT PARK</a></li>
                                <li><a href="/buffalo-raceway" >BUFFALO RACEWAY</a></li>
                                <li><a href="/finger-lakes" >FINGER LAKES </a></li>
                                <li><a href="/saratoga" >SARATOGA</a></li>
                                <li><a href="/tioga-downs" >TIOGA DOWNS</a></li>
                                <li><a href="/vernon-downs" >VERNON DOWNS</a></li>
                                <li><a href="/yonkers-raceway" >YONKERS RACEWAY</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/ohio" >OHIO</a></dt>
                                <li><a href="/northfield-park" >NORTHFIELD PARK</a></li>
                                <li><a href="/raceway-park" >RACEWAY PARK</a></li>
                                <li><a href="/scioto-downs" >SCIOTO DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/oklahoma" >OKLAHOMA</a></dt>
                                <li><a href="/fair-meadows" >FAIR MEADOWS</a></li>
                                <li><a href="/remington-park" >REMINGTON PARK</a></li>
                                <li><a href="/will-rogers-downs" >WILL ROGERS DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/oregon" >OREGON</a></dt>
                                <li><a href="/portland-meadows" >PORTLAND MEADOWS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/pennsylvania" >PENNSYLVANIA</a></dt>
                                <li><a href="/harrahs-philadelphia" >HARRAHS PHILADELPHIA</a></li>
                                <li><a href="/parx-racing" >PARX RACING</a></li>
                                <li><a href="/penn-national" >PENN NATIONAL</a></li>
                                <li><a href="/pocono-downs" >POCONO DOWNS</a></li>
                                <li><a href="/presque-isle-downs" >PRESQUE ISLE DOWNS</a></li>
                                <li><a href="/the-meadows-racetrack" >THE MEADOWS RACETRACK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/texas" >TEXAS</a></dt>
                                <li><a href="/lone-star-park" >LONE STAR PARK</a></li>
                                <li><a href="/retama-park" >RETAMA PARK</a></li>
                                <li><a href="/sam-houston-race-park" >SAM HOUSTON RACE PARK</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/washington" >WASHINGTON</a></dt>
                                <li><a href="/emerald-downs" >EMERALD DOWNS</a></li>
                              </dl>
                              <dl>
                                <dt><a href="/west-virginia" >WEST VIRGINIA</a></dt>
                                <li><a href="/charles-town" >CHARLES TOWN</a></li>
                                <li><a href="/mountaineer-park" >MOUNTAINEER PARK</a></li>
                              </dl></td>
                          </tr>
                          </table>
                          <table class="infoEntries">
                          <tr valign="top">
                            <td><dl>
                              <dt> </td>
                          </tr>
                          </table>
                          <table class="infoEntries">
                          <tr valign="top">
                            <td><dl>
                              <dt> </td>
                          </tr>
                          </table>
                          {*OLD CODE include_php file='../smarty/libs/racetracks/racetracks.php'*} 
                      
                    
                  </div> 
            
          
          
        
         
                                        
          
<div class="headline"><h2>Canadian Harness Racing</h2></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table class="infoEntries">
                          <tr valign="top">
                            <td><li><a href="/ajax-downs" >AJAX DOWNS</a></li>
                              <li><a href="/assiniboia-downs" >ASSINIBOIA DOWNS</a></li>
                              <li><a href="/fort-erie" >FORT ERIE</a></li>
                              <li><a href="/fraser-downs" >FRASER DOWNS</a></li>
                              <li><a href="/hastings-park" >HASTINGS PARK</a></li>
                              <li><a href="/mohawk-raceway" >MOHAWK RACEWAY</a></li>
                              <li><a href="/northlands-park" >NORTHLANDS PARK</a></li>
                              <li><a href="/western-fair-raceway" >WESTERN FAIR RACEWAY</a></li>
                              <li><a href="/woodbine" >WOODBINE</a></li></td>
                          </tr>
                          </table>
                        
                      
                    
                  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
