<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-12">

           
                                        
          
<div class="headline"><h1>Horse Racing Schedule</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<div class="tag-box tag-box-v4">
<h2 style="margin-bottom:0;">US Racing presents this week's horse racing schedule.  International horse racing schedule is available by selecting Today's Racing in the Club House.</h2>
<h2 style="margin-bottom:0;">You must Login to see ALL available Ractracks at US Racing.</h2>
</div>


{include file='includes/ahr_racing_schedule_page_byname.tpl'}        
        
         

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->




 
</div><!-- end/row -->
</div><!-- /#container --> 

{literal}
<script type="text/javascript">
var dom = $(".sRace");                    
				dom.find('.race:contains("Churchill Downs")').parent().remove();		
				dom.find('.race:contains("Fair Grounds")').parent().remove();	
				dom.find('.race:contains("Calder")').parent().remove();
				dom.find('.race:contains("Hoosier")').parent().remove();	
				dom.find('.race:contains("Indiana Downs")').parent().remove();	
				dom.find('.race:contains("Miami Valley")').parent().remove();	
				dom.find('.race:contains("Oaklawn Park")').parent().remove();
				dom.find('.race:contains("The Meadows")').parent().remove();
				dom.find('.race:contains("Arlington Park")').parent().remove();
				dom.find('.race:contains("Finger Lakes")').parent().remove();
				dom.find('.race:contains("Canterbury Park")').parent().remove();
</script>
{/literal}