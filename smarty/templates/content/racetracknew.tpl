 
  
  
  <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




                <div><a href="/join"><img src="/themes/images/banner-racebook.jpg" alt="Racebook" width="705" height="118" /></a></div>
              </div>
            
          
          <!-- /block-inner, /block --> 
          
        
        
                    
          
<div class="headline"><h1>Racetrack Name</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2></h2>
                <div>
                  <div class="RoundBody RaceTracks">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top" width="65%" style="padding: 0 20px 0 34px;"><table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                              <th width="175px">Track Length:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Stretch Length:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Stretch Width:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Infield Capacity:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Clubhouse Capacity:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Grand Stand Capacity:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Parking Capacity:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Price General Admission:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Price Clubhouse:</th>
                              <td></td>
                            </tr>
                            <tr>
                              <th>Price Turf Club: </th>
                              <td></td>
                            </tr>
                          </table>
                          <br />
                          <br />
                          <p>
                          <h2 > History</h2>
                          </p>
                          <p> </p>
                          <br />
                          <br />
                          <p>
                          <h2 >More About </h2>
                          </p>
                          <p> official NTRA Handicapping Contest, Lea County Inn, Hampton Inn   Suites, Holiday Inn Express, Roadway Inn, Best Western, Days Inn, Econo Lodge, Hobbs Family Inn, En Sueno Bed   Breakfast, Relax Inn, Lea Wood Inn, Desert Hills Motel, Hobbs Inn</p></td>
                        <td valign="top" width="35%"><div class="TrackInfo"> <img alt="racetracks" src="http://www.otbpicks.net/images/racetracks/tzia.gif" width="102" height="44"> <br>
                            <p> <b></b><br />
                              3901 W. Millen Drive <br/>
                              Hobbs, New Mexico, 88240<br/>
                              USA </p>
                            <p> (505)-492-7000<br />
                              (888)-ZIA-PARK<br />
                            </p>
                            <hr />
                            <p> <b>Racing Dates: </b> 2010-09-11 until 2010-12-07<br />
                              <b>Number of Racing Days: </b><br />
                            </p>
                            <hr />
                            <div style="height:150px; overflow:hidden;  "></div>
                          </div></td>
                      </tr>
                    </table>
                    <br />
                  </div>
                </div>
              
              
               
              
              
              
                <h2> Stakes</h2>
                <div>
                  <table id="infoEntries" cellpadding="0" border="0" cellspacing="0"      >
                    <tbody>
                      <tr>
                        <th width="50%">Stakes</th>
                        <th width="25">Grade</th>
                        <th width="25">Purse</th>
                        <th width="100">Date</th>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=Premiere_Cup_Handicap" title="Premiere Cup Handicap">Premiere Cup Handicap</a></td>
                        <td ></td>
                        <td >$100,000</td>
                        <td>September 13</td>
                      </tr>
                      <tr>
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Filly_Championship" title="New Mexico Classic Cup Filly Championship">New Mexico Classic Cup Filly Championship</a></td>
                        <td ></td>
                        <td >$140,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Juvenile" title="New Mexico Classic Cup Juvenile">New Mexico Classic Cup Juvenile</a></td>
                        <td ></td>
                        <td >$140,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr>
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Juvenile_Fillies" title="New Mexico Classic Cup Juvenile Fillies">New Mexico Classic Cup Juvenile Fillies</a></td>
                        <td ></td>
                        <td >$140,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Peppers_Pride_Filly%2FMare_Ch" title="New Mexico Classic Cup Peppers Pride Filly/Mare Ch">New Mexico Classic Cup Peppers Pride Filly/Mare Ch</a></td>
                        <td ></td>
                        <td >$170,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr>
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Rocky_Gulch_Championship" title="New Mexico Classic Cup Rocky Gulch Championship">New Mexico Classic Cup Rocky Gulch Championship</a></td>
                        <td ></td>
                        <td >$180,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=New_Mexico_Classic_Cup_Sprint_Championship" title="New Mexico Classic Cup Sprint Championship">New Mexico Classic Cup Sprint Championship</a></td>
                        <td ></td>
                        <td >$170,000</td>
                        <td>November 08</td>
                      </tr>
                      <tr>
                        <td><a href="stakes?name=Zia_Park_Derby" title="Zia Park Derby">Zia Park Derby</a></td>
                        <td ></td>
                        <td >$150,000</td>
                        <td>December 05</td>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=New_Mexico_Eddy_County_Stakes" title="New Mexico Eddy County Stakes">New Mexico Eddy County Stakes</a></td>
                        <td ></td>
                        <td >$120,000</td>
                        <td>December 06</td>
                      </tr>
                      <tr>
                        <td><a href="stakes?name=Zia_Park_Distance_Championship" title="Zia Park Distance Championship">Zia Park Distance Championship</a></td>
                        <td ></td>
                        <td >$200,000</td>
                        <td>December 06</td>
                      </tr>
                      <tr class="odd">
                        <td><a href="stakes?name=Premiere_Cup_Handicap" title="Premiere Cup Handicap">Premiere Cup Handicap</a></td>
                        <td ></td>
                        <td >$100,000</td>
                        <td>September 12</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              
              
               
              
              
              
                <h2>How to get there:</h2>
                <div>
                  <div style="padding: 20px 34px;">
                    <p>right of Lovington Highway, just west of the Lea County Event Center</p>
                  </div>
                  <div style="padding: 0 0 20px 34px;">
                    <iframe src="/Feeds/info/map.php?a=3901 W. Millen Drive  , Hobbs, New Mexico, 88240 , USA" height="300" width="630" scrolling="no"  allowtransparency="true" frameborder="0"></iframe>
                  </div>
                </div>
              
               
              

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


 
  

