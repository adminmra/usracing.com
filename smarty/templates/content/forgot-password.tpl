<div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-12">
            
            <div class="headline">
                <h1 id="trouble-title">Can't Get Out of the Gate?</h1>
            </div><!-- end/headline -->

            <div class="content">
                <div class="col-md-4 col-md-offset-4 calltoaction" id="pn_forgot_pass">
                    <div id="forgot_error">
                        <p style="font-size:20px;margin-bottom:5px;font-weight:bold;"><strong id="title">Reset Your Password</strong></p>
                        <p style="color:#444;font-size:14px;">Give us your email, and we'll send you a link to reset your password.</p>
                    </div>

                    <!-- start post submit -->
                    <div id="forgot_error_post" style="display:none;">
                        <p style="font-size:20px;margin-bottom:5px;font-weight:bold;">
                            <strong>Please check your email.</strong>
                        </p>
                        <p style="color:#444;font-size:14px;">Look for an email from us with a link to reset your password.</p>
                        <p id="login-checked" style="font-size:14px;">We've sent instructions for resetting your password to 
                        <strong id="forgot_email_field"></strong>
                        as long as that's the email you signed up with.If you don't receive an email (check your spam folder), please 
                        <a href='/login-troubles'>try a different email address</a> that you might have registered with instead.</p>
                    </div>
                    <!-- end post submit -->

                    <form class="form-signin" action="" method="POST" role="form">
                        <div class="form-group account-group">
                        <label for="forgot_email">Your Email</label>
                        <input class="form-control form-control-me" type="email" name="email" id='forgot_email' placeholder='Email address' required="" autofocus>
                        </div>
                        <div class="form-group login-group">
                        <p id="login-checked" style="font-size:12px;"></p>
                        <input class="btn btn-sm btn-red btn-red-form" type="submit" value="Send Email">
                        <!--<i class="fa fa-refresh fa-spin" style="display:none;" id="ajax-loader"></i>-->
                        <a id="link_help" style="color:#1B5F95;font-size:14px;display:block;margin-top:16px;" href="//www.betusracing.ag/login">Log In if you know your password</a>
                        </div>
                    </form>
                </div>
            </div> <!-- end/ content -->
        </div> <!-- end/ #left-col -->
    </div><!-- end/row --> 
</div><!-- /#container -->


{literal}
<script type="text/javascript">
$(document).ready(function() {
        $('#pn_forgot_pass form').submit(function(event) {
                var formData = { 'email' : $('#pn_forgot_pass form input[name=email]').val(), };
                $.ajax({
                    type        : 'POST',
                    url         : '//ww1.betusracing.ag/loginservicejson.php?a=sendpasswordtoken',
                    data        : formData,
                    dataType    : 'json',
                    encode      : true ,
                    beforeSend: function( xhr ) {
                        $('#pn_forgot_pass form input[type=submit]')[0].disabled = true;
                        //$('input.btn-red-form').css('width', '90%');
                        $('#ajax-loader').css("display","inline-block");
                    }
                })
                .done( function( data ) {
                    window.test = data;
                    if ( data.STATUS == 1 ) {
                        $('#trouble-title').text("Resetting Your Password");
                        $('.account-group').css("display","none");
                        $('#forgot_email_field').html($('#forgot_email').val())
                        $('#forgot_error').css("display","none");
                        $('#forgot_error_post').css("display","block");
                        $('#link_help').html("I know my password, let me Log In!");
                        $('input.btn-red-form').attr('value','Resend Email');
                        $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
                    }
                    else if ( data.STATUS == 0 ) {
                        $('#pn_forgot_pass #forgot_error').html("<h1>" + data.MSG + "</h1>");
                        $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
                    }
                    $('#ajax-loader').css("display","none");
                    //$('input.btn-red-form').css('width', '100%');
                })
                .fail( function( data ){
                    $('#pn_forgot_pass form input[type=submit]')[0].disabled = false;
                });
        // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });
});

</script>
{/literal}
