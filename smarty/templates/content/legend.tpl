{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">   
<div class="headline"><h1>Horse Racing Legend - Terms</h1></div>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Exacta Box</caption>
<tbody>
<tr valign="top">
<th  colspan="3">Therapeutic medications</th>
</tr>

<tr >
<td width="30%" ><b>L</b></td>
<td width="70%"  >Lasix</td>
</tr>
<tr >
<th  colspan="3">Horse Color</th>
</tr>
<tr >
<td width="30%" ><b>b</b></td>
<td width="70%"  >bay</td></tr>
<tr >
<td width="30%"  ><b>dkb</b></td>
<td width="70%"   >dark bay or brown</td>
</tr>
<tr >
<td width="30%" ><b>gr</b></td>
<td width="70%"  >gray</td>
</tr>
<tr >
<td width="30%"  ><b>ro</b></td>
<td width="70%"   >roan</td></tr>
<tr >
<td width="30%" ><b>ch</b></td>
<td width="70%"  >chestnut</td>
</tr>
<tr >
<td width="30%"  ><b>wh</b></td>
<td width="70%"   >white</td>
</tr>
<tr ><td width="30%" ><b>bl</b></td>
<td width="70%"  >black</td>
</tr>
<tr >
<th  colspan="3">Sex</th>
</tr>
<tr >
<td width="30%" ><b>f</b></td>
<td width="70%"  >filly (female, 4 years or younger)</td>
</tr>
<tr >
<td width="30%"  ><b>m</b></td>
<td width="70%"   >mare (female, 5 years or older)</td>
</tr>
<tr >
<td width="30%" ><b>c</b></td>
<td width="70%"  >colt (male 4 years or younger)</td>
</tr>
<tr >
<td width="30%"  ><b>h</b></td>
<td width="70%"   >horse ( non-neutered male 5 years or older)</td>
</tr>
<tr >
<td width="30%" ><b>g</b></td>
<td width="70%"  >gelding (neutered male any age)</td>
</tr>
<tr >
<th  colspan="3">Workout Notes</th>
</tr>
<tr ><td width="30%" ><b>B</b></td>
<td width="70%"  >breezing</td>
</tr>
<tr >
<td width="30%"  ><b>D</b></td>
<td width="70%"   >driving</td>
</tr>
<tr >
<td width="30%" ><b>E</b></td>
<td width="70%"  >easily</td>
</tr>
<tr >
<td width="30%"  ><b>g</b></td>
<td width="70%"   >worked from gate</td>
</tr>
<tr >
<td width="30%" ><b>H</b></td>
<td width="70%"  >handily</td>
</tr>
</tbody>
</table>

</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
