{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first active-trail"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li class="leaf active-trail"><a href="/beton/matchraces" title="Match Races at US Racing" class="active">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-81" class="block block-block region-even even region-count-2 count-2">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/virtualderby" title="Virtual Derby 3D"><img src="/themes/images/banner-col-virtualderby.jpg" alt="Play Virtual Derby" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



<div id="block-block-96" class="block block-block region-odd odd region-count-3 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/casino" title="Inbetween Races? Play Blackjack!"><img src="/themes/images/banner-play-blackjack.jpg" alt="Play Blackjack" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Match Races at US Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h3><span class="DarkBlue">Match Races - Bet on Horses</span></h3>
<br />
<p>Match races are simple wagers where you bet whether one particular horse will beat another horse.&nbsp; Sometimes this is called head-to-head betting.&nbsp; At the Kentucky Derby, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.&nbsp;</p><p>There are only&nbsp;a handful of companies that offer match races-- most of them are in the United Kingdom and Australia.</p><p>Good luck with your bets and when we get closer to the start of the&nbsp;<a title="Kentucky Derby" href="/kentuckyderby">Kentucky Derby</a>, you will find match race odds in our sportsbook&nbsp;section.</p>
<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    