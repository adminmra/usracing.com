{include file='inc/left-nav-btn.tpl'}
{*literal} 
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Articles",
  "name": "US Racing | Online Horse Betting",
  "description": "US Racing provides  horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit.",
  "image": "https://www.usracing.com/img/best-horse-racing-betting-site.jpg",
  "startDate": "2018-11-27 07:19", 
  "endDate": "2018-11-30 09:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
  "location": {
    "@type": "Place",
    "name": "US Racing | Online Horse Betting",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "123 street ,place park",
      "addressLocality": "miami",
      "addressRegion": "Florida",
      "postalCode": "33184",
      "addressCountry": "US"
    }
  }
}

</script> 
{/literal*}
<div id="left-nav"> 
  
  <!-- ---------------------- left menu contents ---------------------- --> 
  
  {include file='menus/triplecrown.tpl'} 
  
  <!-- ---------------------- end left menu contents ------------------- --> 
</div>
{*
<div class="container-fluid"> <a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a> <a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a> </div>
*}
<div id="main" class="container">
  <div class="row">
    <div id="left-col" class="col-md-9">
      <div class="headline">
        <h1>{$h1}</h1>
      </div>
      <div class="content"> 
        <!-- --------------------- content starts here ---------------------- --> 
        
        {include file='inc/triplecrown-slider.tpl'}
        <h2> {include file='/home/ah/allhorse/public_html/belmont/year.php'} Triple Crown Odds</h2>
        <p>{*include file='/home/ah/allhorse/public_html/misc/triple_crown_993_xml.php'*}</p>
        <p>{*include file='/home/ah/allhorse/public_html/belmont/props_odds.php'*}</p>
{literal} 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Triple Crown Odds"
}
</script> 
{/literal}
<!-- start schema dynamic table -->
    {*    <div itemscope itemtype="http://schema.org/Table">
          <p> {include file='/home/ah/allhorse/public_html/misc/triple_crown_special.php'} </p>
        </div> *}
<!-- end schema dynamic table -->
        <p class="cta-button" align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
        <p>Winning the Triple Crown, consisting of the Kentucky Derby, Preakness and Belmont Stakes, is horse racing’s greatest honor. Countless horses have tried and only 12 have succeeded. In fact, there have been 23 horses that won the first two jewels of the Triple Crown — the Kentucky Derby and the Preakness — only to fall short in the Belmont Stakes or miss the race entirely.</p>
        <p>In 1919, Sir Barton was the first horse to claim the Triple Crown. Gallant Fox (1930), Omaha (1935), War Admiral (1937), Whirlaway (1941), Count Fleet (1943), Assault (1946), Citation (1948), Secretariat (1973), Seattle Slew (1977), Affirmed (1978), American Pharoah (2015) and most recently, Justify (2018).</p>
        <p>In order to win the Triple Crown, a horse must win three long races in five weeks — at three different tracks in three different states. The first leg of the series is the Kentucky Derby, run at 1 ¼ miles on the first Saturday of May at <a href="https://www.usracing.com/churchill-downs"> Churchill Downs</a> in Louisville, Kentucky. The second leg is the Preakness Stakes, which is a 1 3/16-mile test run at <a href="https://www.usracing.com/pimlico">Pimlico Race Course</a> in Baltimore, Maryland, two weeks after the Derby. Lastly, the Belmont Stakes, often referred to as “The Test of the Champion,” is a grueling 1 ½ miles and takes place at <a href="https://www.usracing.com/belmont-park">Belmont Park </a>in Elmont, New York, three weeks after the Preakness.</p>
        <p>All of the Triple Crown races are restricted to 3-year-old thoroughbreds.</p>
        <p>After Affirmed won the Triple Crown in 1978, there was a 37-year drought before American Pharoah swept the three jewels in 2015 — though several horses came close.</p>
        <p>Both Silver Charm (1997) and Real Quiet (1998) finished second, the latter by a mere nose to Victory Gallop; and Funny Cide finished third in 2004. The following year (2004) there was yet another chance of a Triple Crown, but Smarty Jones was run down by Birdstone in deep stretch.</p>
        <p>Every year at US Racing we offer future proposition wagers on the Kentucky Derby, Preakness Stakes, Belmont Stakes and Triple Crown. <a href="https://www.betusracing.ag/login?">Login</a> or <a href="https://www.betusracing.ag/signup?">join</a> today to check out all the action!</p>
        <p class="cta-button" align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
        
        <!-- ------------------------ content ends -------------------------- --> 
      </div>
      <!-- end/ content --> 
    </div>
    <!-- end/ #left-col -->
    
    <div id="right-col" class="col-md-3"> {include file='inc/rightcol-calltoaction.tpl'}
      {*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} </div>
    <!-- end: #right-col --> 
    
  </div>
  <!-- end/row --> 
</div>
<!-- /#container --> 
