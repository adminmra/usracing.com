{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Bet on Breeders&#039; Cup</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p><a href="/bet-on/breeders-cup"><img class="img-responsive" src="/img/breeders/bet-on-breeders-cup.jpg" alt="Bet on the Breeders' Cup"  />  </a></p>

<p>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'} </p> 

<p>The primary goal of Breeders' Cup Limited is to build positive public awareness of Thoroughbred racing and to expand opportunities for enhancement of the Thoroughbred industry. These objectives are first accomplished through the Breeders' Cup Championship, a year-end international showcase of the sport's greatest stars.</p>
<p>Additionally, the Breeders' Cup supports these goals through the funding of a year-round series of stakes races, consumer marketing programs and nationally televised races.</p>
<p>It's the All Star Game of Thoroughbred racing - only better. Eight times better. It's the Breeders' Cup Championship, a multi-million dollar extravaganza that brings together the world's best horses to compete in eight sensational races.</p>

{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} 

<p>{include file='/home/ah/allhorse/public_html/breeders/races.php'} </p>

<p>&nbsp;</p>
{include file='/home/ah/allhorse/public_html/breeders/breederscup_links.tpl'}



  

   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-bc.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
    