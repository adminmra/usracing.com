{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first active-trail"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li class="leaf active-trail"><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge" class="active">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



{include file='banners/banner4.tpl'}             
{include file='banners/banner3.tpl'}
{include file='banners/banner2.tpl'}
{include file='banners/banner1.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Breeders’ Cup Classic Challenge 2010</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><img style="margin: 0 20px 20px 0;" src="/images/BC_challenge_AHR_thumbnail.jpg" border="0"  hspace="0" align="left"> Play for more than $100,000.00 in cash in the US Racing Breeders' Cup Classic Challenge. There is no better way to celebrate the 26th running of the Breeders' Cup World Championships than by cashing in on a six-figure payday.
</p>
  <p>Also, a $2,500.00 guaranteed prize will be up for grabs just in case no one collects the $100K.
</p><br><br>
 <p><strong>$100,000.00 Grand Prize</strong></p>
 <p>Bet at least $10.00 on any of the 14 Breeders' Cup races and correctly predict the finishing order of the Top 10 horses in Saturday's Breeders' Cup Classic and the $100,000.00 is yours.
</p>

<p><strong>$2,500.00 Breeders' Cup Classic Prize </strong></p>
<p>When you enter your picks for this race you'll also be earning points for every correct prediction. You'll earn 10 points for correctly predicting the winner, nine points for predicting the runner-up, all the way down to 1 point for the 10th Place finisher.  </p>

<p>1st Place - $2,500.00 (Winner take all)</p>
<p><strong>Point System for the $2,500.00 Guaranteed Prize:</strong></p>
<ul>
<li>1st Place: 10 points</li>
<li>2nd Place: 9 points</li>
<li>3rd Place: 8 points</li>
<li>4th Place: 7 points</li>
<li>5th Place: 6 points</li>
<li>6th Place: 5 points</li>
<li>7th Place: 4 points</li>
<li>8th Place: 3 points</li>
<li>9th Place: 2 points</li>
<li>10th Place: 1 point</li>
</ul>

<p><strong>To Qualify for Cash Prizes:</strong></p>
<ol>
<li>Enter your CID and Email to register.</li>
<li>Select the correct order of the Top 10 finishing horses for the Breeders' Cup Classic.</li>
<li>Place at least $10.00 worth of wagers in total on Friday's and/or Saturday's Breeders' Cup races at usracing.com .</li>
</ol>

<p>&nbsp;</p>
<p><strong>Terms &amp; Conditions</strong></p>
<ol>
    <li>This promotion is open to all <strong>usracing.com</strong> active       members.<strong></strong></li>
    <li>To participate, members must       register by submitting their correct Username and Email on the promotion       page. If you submit the incorrect Username and/or Email, your picks will       not be saved and you will not be eligible for any monetary prizing.</li>
    <li>This promotion begins shortly       after the final list of Breeders' Cup Classic contenders is posted on       Wednesday, November 4, 2009. This promotion will close on Saturday,       November 7, 2009 five minutes before the post time of the Breeders' Cup       Classic (approximately 6:40 PM ET). </li>
    <li>To be eligible for the any       monetary prizing, you must place at least $10.00 of wagers on the 2009       Breeders' Cup at <strong>usracing.com</strong> in either the Racebook or in the Sportsbook.       The $10.00 can be wagered on any of the 14 Breeders' Cup races scheduled for       November 6th &amp; 7th, 2009.</li>
    <li>This promotion will be graded       on Monday, November 9, 2009.</li>
    <li>The winner(s) will be       contacted by e-mail on Monday, November 9, 2009.</li>
    <li>There are no rollover       requirements associated with any monetary prizes.</li>
    <li><strong>usracing.com</strong> reserves the       right to change the terms and conditions of this promotion at any time.</li>
    <li>General House Rules apply.</li>
</ol>
<p>&nbsp;</p>
<p><strong>About the $100,000.00 Grand Prize:</strong></p>
<ul>
    <li>The $100,000.00 Grand Prize       will be awarded to the member that correctly predicts the order of the Top       10 finishing horses of the 2009 Breeders' Cup Classic. </li>
    <li>To be eligible for the       $100,000.00 Grand Prize, you must correctly predict the finishing order of       the Top 10 horses for the Breeders' Cup Classic.</li>
    <li>In the event that numerous       qualifying members correctly predict the finishing order of the Top 10 horses,       the $100,000.00 will be split evenly amongst the winning contestants.</li>
    <li>After making your picks, if       one or more of the horses you select is scratched, you will NOT be able to       re-submit your picks and will not be eligible for the Grand Prize for that       specific race.</li>
    <li>If fewer than 10 horses run       in the Breeders' Cup, the $100,000.00 Grand Prize will NOT be offered. </li>
    <li>If anywhere between six and       nine horses are scheduled to race, a $10,000.00 prize will be offered       instead.</li>
    <li>If fewer than six horses are       scheduled to race, only the $2,500.00 Guaranteed Prize will be offered.</li>
    <li>The $100,000.00 Grand Prize       will be paid out monthly in equal sums of $8,333.33 over one year and is       non-transferable. Winnings will be deposited directly into your       <strong>usracing.com</strong> account on the first Monday of every month.</li>
</ul>
<p>&nbsp;</p>
<p><strong>About the $2,500.00 Guaranteed Prize</strong></p>
                                    <ul>
                                      <li>The $2,500.00 Prize pool will       be awarded to the member(s) that accumulates the most points for the       Breeders' Cup Classic.</li>
                                      <li>Point breakdown for the       Breeders' Cup Classic Challenge $2,500.00 guaranteed prize is as follows:       10 points for correct 1st place, 9 points for correct 2nd       place, 8 points for correct 3rd place, 7 points for correct 4th       place, 6 points for correct 5th place, 5 points for correct 6th       place, 4 points for correct 7th place, 3 points for correct 8th       place, 2 points for correct 9th place and 1 point for correct       10th place.</li>
                                      <li>If numerous qualifying       members tie for first place, the $2,500.00 will be evenly split amongst       the tying members.</li>
                                      <li>Winnings associated with the       $2,500.00 guaranteed prize will be deposited into the member's       <strong>usracing.com</strong> account on the Tuesday, November 10, 2009.</li>
                                      <li>If fewer than 10 horses race       in the Breeders' Cup Classic, points will only be awarded for the number of       horses that race. For example, if only nine horses race, 9 points will be       awarded for 1st         Place down to 1 point for 9th Place.</li>
                                      <li>After making your picks, if       one or more of the horses you select is scratched, you will NOT be able to       re-submit your picks.</li></ul>

<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    