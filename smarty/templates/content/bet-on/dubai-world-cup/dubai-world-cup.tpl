{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}

{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
 {*schema for the page*}
{literal}
   <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[2]"
    }
	</script>
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress",
                "addressRegion": "Riyadh"
            },
            "name": "Meydan Racecourse" 
        },
        "name": "Dubai World Cup",
        "startDate": "2020-03-28T08:00-05:00",
        "endDate": "2020-03-28T12:40-05:00",
        "image": [
          "https://img/dubai-world-cup/dubai-world-cup-lg.jpg.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-md.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-sm.jpg"
        ],
        "description": "Dubai World Cup",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-10",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/saudi-cup/betting"
    }
	</script>
{/literal}
{*end of the shema*}


{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
     </style>
{/literal}
{*End*}

<div id="left-nav">
  {include file='menus/saudi-cup.tpl'}
</div>

{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>
    </div>
</div>
{*End*}


  {*This is the path for the countdown*}
     {include file="/home/ah/allhorse/public_html/dubai/block-countdown.tpl"}
  {*end*}


  {*This is the section for the odds tables*}
  <section class="saudi kd usr-section">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>
		  <h3 class="kd_subheading">{$h3}</h3>{*This h1 is change in the xml whit the tag h1*}
        {*The path for the main copy of the page*}
        {* <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb-odds.tpl'} </p> *}
        {*end*}

        {*The path for the year of the event*}
                {*end*}
        
        {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
        <p>{include file='/home/ah/allhorse/public_html/dubai/salesblurb.tpl'} </p>
    <p align="center"><a href="/signup?{$ref2}" rel="nofollow" class="btn-xlrg ">Bet on the Dubai World Cup</a></p>
    <p>{include_php file='/home/ah/allhorse/public_html/dubai/odds_dubai_1005_xml_new.php'}</p>
        <p align="center"><a href="/login?ref={$ref3}" rel="nofollow"
            class="btn-xlrg fixed_cta">Bet on the Dubai World Cup</a></p>
        {*end*}

        {*Second Copy of the page*}
      
        <p><a href="/signup?ref={$ref4}">BUSR</a> provides the earliest <a href="/signup?ref={$ref5}">Dubai World Cup Futures odds</a> of any website. If you see a horse you would like to be added, let us know and we might be able to get it added for you!</p>
        <p>Between now and the <a href="/signup?ref={$ref6}">Dubai World Cup</a>, the odds will be changing when we add new horses and remove others. Remember, when you a place future wager the odds are fixed and all wagers have action.</p>
        <p>Good luck and see you on {include_php file='/home/ah/allhorse/public_html/dubai/day.php'}!</p>
        {*end*}
        </li>
        </ul>
      </div>
    </div>
  </section>
{include file="/home/ah/allhorse/public_html/dubai/dubai block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/dubai/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/dubai/block-exciting.tpl"}

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}