           
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}

<p><em><strong>Kentucky Derby Future Wagers are now available in the&nbsp;</strong></em><a title="sportsbook" href="/sportsbook"><em><strong>sportsbook</strong></em></a><strong><em>&nbsp;section of your account-- place your bets today!</em>&nbsp;</strong></p>
<p>Churchill Downs has established the dates for the three wagering pools that will comprise the track's 2009 renewals of its Kentucky Derby Future Wager (KDFW) and Kentucky Oaks Future Wager (KOFW).</p>
<p>The three respective pools for the Kentucky Derby and Kentucky Oaks Future Wagers will again run concurrently. Pending final approval from the Kentucky Horse Racing Authority, the dates for the two wagers will be:</p>
<p>&nbsp;&nbsp;&nbsp; * Pool 1 Feb. 12-15<br /> &nbsp;&nbsp;&nbsp; * Pool 2 March 12-15<br /> &nbsp;&nbsp;&nbsp; * Pool 3 April 2-5</p>
<p>As in past years, wagering on each of the Kentucky Derby Future Wager pools will open at noon (all times Eastern) on the first day of the pool’s Thursday-Sunday run.<br /> Betting will conclude in the three Derby pools will conclude at 6 p.m. on Sunday. Wagering on the Kentucky Oaks Future pool will open at noon on Thursday, March 12, and that single pool will conclude at 6:30 p.m. on Sunday, March 15.</p>
<p> The Kentucky Derby and Kentucky Oaks Future Wagers are minimum $2 wagers and win bets only. The bets offers fans opportunities to wager on contenders for the Kentucky Derby and Kentucky Oaks at odds that could be greater than those they would receive on the their respective race days. There are no refunds in either the Kentucky Derby or Kentucky Oaks Future Wager.</p>
<p> The 135th running of the Kentucky Derby – a 1 ¼-mile test for 3-year-olds and America’s greatest race – is scheduled for Saturday, May 2 at Churchill Downs. The $500,000-added Kentucky Oaks (GI), a 1 1/8-mile race restricted to 3-year-old fillies, will be run over the track on Friday, May 1.</p>
<p> A total of $1,056,520 was wagered on the three Kentucky Derby Future Wager pools in 2008 as total betting on the wager topped the $1 million mark for the ninth consecutive time in its 10-year history. Wagering on the three Kentucky Oaks pools in 2008 totaled $174,838, the second highest three-pool total in the history of the bet.</p>
<p> “A wide open and very competitive Kentucky Derby picture is developing and Churchill Downs is excited to again offer three pools with plenty of betting value in the Kentucky Derby Future Wager, which has become part of the Derby tradition over the past decade,” said Churchill Downs General Manager Jim Gates. “After careful consideration we decided to return the Kentucky Oaks Future Wager to a single-pool format and believe the March pool offers the best opportunity to present a strong roster of serious Kentucky Oaks wagering interests. We think the single-pool format will provide the Kentucky Oaks Future Wager with its best opportunity to grow and present racing fans a chance to have a lot of fun while searching for the winner of what has become the nation’s most prestigious event for 3-year-old fillies.”</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span class="content-header-bar">&nbsp;&nbsp;<strong>KENTUCKY DERBY FUTURE WAGER STATISTICS</strong></span></p>
<p>&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="0" width="80%">
<tbody>
<tr>
<td><strong>Wagering</strong></td>
<td><strong>Pool 1</strong></td>
<td><strong>Pool 2</strong></td>
<td><strong>Pool 3</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr>
<td>2008</td>
<td>$439,397</td>
<td>$325,306</td>
<td>$291,835</td>
<td>$1,056,520</td>
</tr>
<tr>
<td>2007</td>
<td>$520,688</td>
<td>$379,613</td>
<td>$465,123</td>
<td>$1,365,424</td>
</tr>
<tr>
<td>2006</td>
<td>$552,627</td>
<td>$464,236</td>
<td>$454,743</td>
<td>$1,471,606</td>
</tr>
<tr>
<td>2005</td>
<td>$620,362**</td>
<td>$533,973</td>
<td>$533,973</td>
<td>$1,665,990****</td>
</tr>
<tr>
<td>2004</td>
<td>$536,958</td>
<td>$358,966</td>
<td>$386,244</td>
<td>$1,282,168</td>
</tr>
<tr>
<td>2003</td>
<td>$516,906</td>
<td>$391,002</td>
<td>$222,261</td>
<td>$1,130,169</td>
</tr>
<tr>
<td>2002</td>
<td>$577,889</td>
<td>$401,070</td>
<td>$524,847</td>
<td>$1,503,806</td>
</tr>
<tr>
<td>2001</td>
<td>$510,815</td>
<td>$372,961</td>
<td>$425,871</td>
<td>$1,309,647</td>
</tr>
<tr>
<td>2000</td>
<td>$465,454</td>
<td>$306,259</td>
<td>$387,206</td>
<td>$1,158,919&nbsp;</td>
</tr>
<tr>
<td>1999</td>
<td>$267,748</td>
<td>$178,811&nbsp;&nbsp;</td>
<td>$229,674</td>
<td>$676,233</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span class="content-header-bar">&nbsp;&nbsp;<strong>KENTUCKY DERBY FUTURE WAGER MUTUEL PAYOUTS</strong></span></p>
<p>&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="0" width="80%">
<tbody>
<tr>
<td><strong>Winner</strong></td>
<td><strong>Pool 1</strong></td>
<td><strong>Pool 2</strong></td>
<td><strong>Pool 3</strong></td>
<td><strong>Derby Day</strong></td>
</tr>
<tr>
<td>2008 Big Brown</td>
<td>$8.60 (f)*</td>
<td>$15.00 (f)</td>
<td>$8.60*</td>
<td>$6.80*</td>
</tr>
<tr>
<td>2007 Street Sense</td>
<td>$22.80</td>
<td>$18.20</td>
<td>$15.40</td>
<td>$11.80*</td>
</tr>
<tr>
<td>2006 Barbaro</td>
<td>$40.20</td>
<td>$32.20</td>
<td>$20.80&nbsp;</td>
<td>$14.20</td>
</tr>
<tr>
<td>2005 Giacomo</td>
<td>$52.00&nbsp;</td>
<td>$54.20</td>
<td>$103.60&nbsp;</td>
<td>$102.60</td>
</tr>
<tr>
<td>2004 Smarty Jones</td>
<td>$5.60 (f)</td>
<td>$10.80 (f)&nbsp;</td>
<td>$23.60&nbsp;</td>
<td>$10.20*</td>
</tr>
<tr>
<td>2003 Funny Cide</td>
<td>$188.00***&nbsp;</td>
<td>$120.80</td>
<td>$23.60</td>
<td>$27.60&nbsp;</td>
</tr>
<tr>
<td>2002 War Emblem</td>
<td>$7.60 (f)*&nbsp;</td>
<td>$16.00</td>
<td>$24.00</td>
<td>$43.00&nbsp;</td>
</tr>
<tr>
<td>2001 Monarchos&nbsp;</td>
<td>$36.60&nbsp;</td>
<td>$13.00</td>
<td>$15.80&nbsp;</td>
<td>$23.00&nbsp;</td>
</tr>
<tr>
<td>2000 Fusaichi Pegasus</td>
<td>$27.80&nbsp;</td>
<td>$26.40 (f)</td>
<td>$ 8.00*&nbsp;</td>
<td>$ 6.60*&nbsp;</td>
</tr>
<tr>
<td>1999 Charismatic&nbsp;</td>
<td>$10.20 (f)*&nbsp;</td>
<td>$30.20 (f)</td>
<td>$26.60 (f)</td>
<td>$64.60</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-size: x-small;">(f) – Mutuel field *Favorite **KDFW single pool wagering record<br />***Record KDFW win payout ****KDFW total wagering (all pool) record<br /></span></p>
<p>&nbsp;</p>
<p><span class="content-header-bar">&nbsp;&nbsp;<strong>KENTUCKY OAKS FUTURE WAGER STATISTICS</strong></span></p>
<p>&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="0" width="80%">
<tbody>
<tr>
<td><strong>Wagering</strong></td>
<td><strong>Pool 1</strong></td>
<td><strong>Pool 2</strong></td>
<td><strong>Pool 3</strong></td>
<td><strong>Total</strong></td>
</tr>
<tr>
<td>2008</td>
<td>$65,219</td>
<td>$62,503&nbsp;</td>
<td>$47,116&nbsp;</td>
<td>$174,838&nbsp;</td>
</tr>
<tr>
<td>2007</td>
<td>$66,206&nbsp;</td>
<td>$55,840</td>
<td>$71,040&nbsp;</td>
<td>$193,086****&nbsp;</td>
</tr>
<tr>
<td>2006</td>
<td>$66,548&nbsp;</td>
<td>$54,054&nbsp;</td>
<td>$51,212&nbsp;</td>
<td>$171,814&nbsp;</td>
</tr>
<tr>
<td>2005</td>
<td>$69,928&nbsp;</td>
<td>$53,106&nbsp;&nbsp;</td>
<td>$49,445&nbsp;&nbsp;</td>
<td>$172,479&nbsp;</td>
</tr>
<tr>
<td>2004</td>
<td>$66,425&nbsp;</td>
<td>$46,151&nbsp;</td>
<td>$45,635&nbsp;</td>
<td>$158,211</td>
</tr>
<tr>
<td>2003</td>
<td>$117,368**&nbsp;</td>
<td>&nbsp;$117,368**</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span class="content-header-bar">&nbsp;&nbsp;<strong>KENTUCKY OAKS FUTURE WAGER MUTUEL PAYOUTS</strong></span></p>
<p>&nbsp;</p>
<table border="0" cellspacing="0" cellpadding="0" width="80%">
<tbody>
<tr>
<td><strong>Wagering Payout</strong></td>
<td><strong>Pool 1</strong></td>
<td><strong>Pool2</strong></td>
<td><strong>Pool 3</strong></td>
<td><strong>Payout</strong></td>
</tr>
<tr>
<td>2008 Proud Spell</td>
<td>$15.60</td>
<td>$14.20</td>
<td>$15.60&nbsp;</td>
<td>$8.80*</td>
</tr>
<tr>
<td>2007 Rags to Riches&nbsp;&nbsp;</td>
<td>$9.60&nbsp;</td>
<td>$10.00*&nbsp;</td>
<td>$5.00*&nbsp;</td>
<td>$5.00*&nbsp;</td>
</tr>
<tr>
<td>2006 Lemons Forever&nbsp;&nbsp;</td>
<td>$9.00 (f)*&nbsp;</td>
<td>$11.00 (f)*</td>
<td>$26.00 (f)&nbsp;</td>
<td>$96.20&nbsp;</td>
</tr>
<tr>
<td>2005 Summerly</td>
<td>$31.80&nbsp;</td>
<td>$15.00</td>
<td>$28.40&nbsp;</td>
<td>$11.20</td>
</tr>
<tr>
<td>2004 Ashado&nbsp;</td>
<td>$48.40***</td>
<td>$21.00&nbsp;&nbsp;</td>
<td>$17.80&nbsp;</td>
<td>$6.60*</td>
</tr>
<tr>
<td>2003 Bird Town&nbsp;</td>
<td>$33.60&nbsp;</td>
<td>No Pool&nbsp;&nbsp;</td>
<td>No Pool&nbsp;&nbsp;</td>
<td>$38.40&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-size: x-small;">*Favorite **KOFW single pool wagering record ***KOFW win payout record ****KOFW total wagering (three pools) record (f) – Mutuel field, was the betting favorite on Derby Day, although he had not been favored in any of the three Kentucky Derby Future Wager pools. The mutuel field, or all others, was favored in Pools 1 and 2, while Curlin, the then-unbeaten winner of the Arkansas Derby (GII) and the eventual third-place finisher in the Kentucky Derby, was the favorite in the third and final pool. Kentucky Oaks winner Rags to Riches was a strong betting favorite on Kentucky Oaks Day, and was favored in two of 2007's three Kentucky Oaks Future Wager pools. The mutuel field was a narrow 3-1 choice over Rags to Riches (7-2) in last year's Pool 1.</span></p>
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}
