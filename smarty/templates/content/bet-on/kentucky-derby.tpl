{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/top-of-page-blocks.tpl"}
{*Page Unique content goes here*******************************}


<div class="justify-left">
 <p><strong>When is the Kentucky Derby?</strong> The {$KD_RUNNING} running of the Grade 1 Kentucky Derby will be held at Churchill Downs on {$KD_DAY} with the first-race post at noon.</p>

<p><strong>How can I watch the Kentucky Derby?</strong>  On race day complete TV coverage airs on NBC beginning at at 5 PM ET and can be lived streamed on NBCSports.com.   Post positions for all the contenders  are announced on the Wednesday before.</p>
<p>Place your bets and watch the races!</p>  
<h2 class="title">Kentucky Derby Betting Odds</h2>
{* <p>{include_php file='/home/ah/allhorse/public_html/kd/odds_kd.php'}</p> *}
<p>{include_php file="/home/ah/allhorse/public_html/generated/odds/kd/odds_kd.php"}</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/end-of-page-blocks.tpl"}
