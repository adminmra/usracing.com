{assign var="showclock" value="1"} 
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  


<p>The primary goal of Breeders' Cup Limited is to build positive public awareness of Thoroughbred racing and to expand opportunities for enhancement of the Thoroughbred industry. These objectives are first accomplished through the Breeders' Cup Championship, a year-end international showcase of the sport's greatest stars.</p>
<p>Additionally, the Breeders' Cup supports these goals through the funding of a year-round series of stakes races, consumer marketing programs and nationally televised races.</p>
<p>It's the All Star Game of Thoroughbred racing - only better. Eight times better. It's the Breeders' Cup Championship, a multi-million dollar extravaganza that brings together the world's best horses to compete in eight sensational races.</p>

<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>

<p>{include file='/home/ah/allhorse/public_html/breeders/races.php'} </p>




{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_breeders/end-of-page-blocks.tpl"}