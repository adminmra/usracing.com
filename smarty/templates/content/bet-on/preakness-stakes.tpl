{include file="/home/ah/allhorse/public_html/usracing/block_preakness/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  

<div class="justify-left">
          <p><strong>When is the Preakness Stakes?</strong> The {$PREAKNESS_RUNNING}  running of the Grade 1 Preakness Stakes will be held at Pimlico on {$PREAKNESS_DATE} with the first-race post at noon.</p>
<p><strong>How can I watch the Preakness?</strong>  On race day complete TV coverage airs on NBC beginning at at 5 PM ET and can be lived streamed on NBCSports.com.   Post positions for all the contenders  are announced on the Wednesday before.</p>

<p>Place your bets and watch the races!</p>  </div>
<h2 class="title">Preakness Stakes Betting Odds</h2>
<p>{*include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'*} </p>
<p>{include_php file='/home/ah/allhorse/public_html/ps/odds_ps_hardcoded.php'} </p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_preakness/end-of-page-blocks.tpl"}