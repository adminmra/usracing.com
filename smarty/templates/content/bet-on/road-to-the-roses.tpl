{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first active-trail"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li class="leaf active-trail"><a href="/beton/roadtoroses" title="Road to the Kentucky Derby" class="active">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



{include file='banners/banner4.tpl'}             
{include file='banners/banner3.tpl'}
{include file='banners/banner2.tpl'}
{include file='banners/banner1.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Road to the Roses | Kentucky Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span style="color: #0a5bb6;">Road to the Kentucky Derby</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div id="story_content">
<div>A look at the major stakes races that lead up to the Kentucky Derby:</div>

<p>&nbsp;</p>
<br />

<p><h4 title="Kentucky Derby Stakes Races in January, 2010"><span style="color: #0a5bb6;">January</span></h4></p>

<p>
  Jan. 1 - Tropical Park Derby, Calder, 1 1/8 miles T (Fly by Phil, Lost Aptitude, Cat Park)<br />
  Jan. 2 - Count Fleet, Aqueduct, 1 mile, 70 yards (Laus Deo, Worth a Buck, Turbo Speed).<br />
  Jan. 9 - Spectacular Bid, Gulfstream Park, A Little Warm, Wildcat Frankie, Westover Wildcat)<br />
  Jan. 16 - California Derby, Golden Gate, 1 1/16 miles (Ranger Heartly, Connemare, Thomas Baines)<br />
  Jan. 16 - San Rafael, Santa Anita, 1 mile (Conveyance, Cardiff Giant, Domonation)<br />
  Jan. 18 - Jimmy Winkfield, Aqueduct, 6 furlongs (Deputy Daney, Red Bengal, Mia's Angel)<br />
  Jan. 18 - Smarty Jones, Oaklawn Park, 1 mile (Dryfly, Pleasant Storm, Kitty's Turn)<br />
  Jan. 23 - Lecomte, Fair Grounds, 1 mile<br />
  Jan. 23 - Holy Bull, Gulfstream Park, 1 mile<br />
  Jan. 30 - WEBN, Turfway Park, 1 mile<br />
  </p>
  <br />
  <p><h4 kentucky derby stakes races in february, 2010><span style="color: #0a5bb6;">February</span></h4></p>
  <p>
  Feb. 6 - Whirlaway, Aqueduct, 1 1/16 miles<br />
  Feb. 6 - Robert B. Lewis, Santa Anita, 1 1/16 mile<br />
  Feb. 13 - Sam F. Davis, Tampa Bay Downs, 1 11/6 miles<br />
  Feb. 15 - Southwest, Oaklawn Park, 1 mile<br />
  Feb. 15 - San Vicente, Santa Anita, 7 furlongs<br />
  Feb. 20 - Risen Star, Fair Grounds, 1 1/16 miles<br />
  Feb. 20 - El Camino Real Derby, Golden Gate, 1 1/8 miles<br />
  Feb. 20 - Fountain of Youth, Gulfstream Park, 1 1/8 miles<br />
  Feb. 20 - Hutcheson, Gulfstream Park, 7 furlongs<br />
  Feb. 20 - Turf Paradise Derby, Turf Paradise, 1 1/16 miles<br />
  Feb. 27 - Sham, Santa Anita, 1 1/8 miles<br />
  Feb. 27 - Borderland Derby, Sunland Park, 1 1/16 miles.<br />
  Feb. 27 - John Battaglia Memorial, Turfway Park, 1 1/16 miles<br /></p>
  
  <br />
  <p><h4 title="Kentucky Derby Stakes Races in March, 2010"><span style="color: #0a5bb6;">March</span></h4></p>

  <p>
  March 6 - Gotham, Aqueduct, 1 1/16 miles.<br />
  March 13 - Rebel, Oaklawn Park, 1 1/16 miles<br />
  March 13 - San Felipe, Santa Anita, 1 1/16 miles<br />
  March 13 - Tampa Bay Derby, Tampa Bay Downs, 1 1/16 miles.<br />
  March 20 - Florida Derby, Gulfstream Park, 1 1/8 miles.<br />
  March 20 - Swale, Gulfstream Park, 7 furlongs<br />
  March 20 - Private Terms, Laurel Park, 1 mile<br />
  March 27 - Louisiana Derby, Fair Grounds, 1 1/8 miles<br />
  March 27 - UAE Derby, Meydan, 1 1/4 miles<br />
  March 27 - Lane's End, Turfway Park, 1/8 miles<br />
  March 27 - Rushaway, Turfway Park, 1 1/16 miles<br />
  March 28 - Sunland Derby, Sunland Park, 1 1/8 miles<br />
  </p>
  
  <br />
  <p><h4 title="Kentucky Derby Stakes Races in April, 2010"><span style="color: #0a5bb6;">April</span></h4></p>
  
  <p>
  April 3 - Wood Memorial, Aqueduct, 1 1/8 miles<br />
  April 3 - Bay Shore, Aqueduct, 7 furlongs<br />
  April 3 - Santa Anita Derby, Santa Anita, 1 1/8 miles<br />
  April 3 - Illinois Derby, Hawthorne Park, 1 1/8 miles<br />
  April 10 - Arkansas Derby, Oaklawn Park, 1 1/8 miles<br />
  April 10 - Blue Grass, Keeneland, 1 1/8 miles<br />
  April 17 - Lexington, Keeneland, 1 1/16 miles<br />
  April 24 - Withers, Aqueduct, 1 mile<br />
  April 24 - Derby Trial, Churchill Downs, 1 mile<br />
  </p>
  
  <br />
  <p><h4 title="Kentucky Derby Stakes Races in May, 2010"><span style="color: #0a5bb6;">May</span></h4></p>
  
  <p>
  <strong>May 7 - Kentucky Derby, Churchill Downs, 1 1/4 miles</strong></p>
<p>&nbsp;</p><h4 title="Triple Crown Horse Races"><span style="color: #0a5bb6;">Other Triple Crown events</span></h4>
<p><strong>May 21 -</strong> Preakness Stakes (G1), Pimlico, 1 3/16 miles.<br />
  <strong>June 11 -</strong> Belmont Stakes (G1), Belmont Park, 1 1/2 miles.
  </p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction-kd.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    