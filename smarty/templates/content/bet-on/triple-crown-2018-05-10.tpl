{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->


          

{include file='menus/triplecrown.tpl'}






<!-- ---------------------- end left menu contents ------------------- -->         
</div>
          
{*
    <div class="container-fluid">
    	<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a>
    	<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a>
    </div>        
      
*}
      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>{$h1}</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='inc/triplecrown-slider.tpl'}
<h2> {include file='/home/ah/allhorse/public_html/belmont/year.php'}  Triple Crown Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/misc/triple_crown_993_xml.php'}</p>

 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<p>Triple Crown immortality is horse racing’s greatest honor. Countless horses have tried and only 12 have succeeded. In fact, there have been 23 near winners, but who have not won the Belmont Stakes.<p>
In 1919, Sir Barton was the first horse to claim the Triple Crown, capturing the Kentucky Derby, Preakness Stakes and Belmont Stakes in the same year. Gallant Fox (1930), Omaha (1935), War Admiral (1937), Whirlaway (1941), Count Fleet (1943), Assault (1946), Citation (1948), Secretariat (1973), Seattle Slew (1977) and Affirmed (1978) have followed in infamy.<p>
In order to win a Triple Crown, a horse must win three long races in five weeks, at three different tracks, in three different states. Triple Crown hopefuls must first win the Kentucky Derby, where Aristedes grabbed the inaugural “Run for the Roses” in 1875. Barring injury, the Derby winner is shipped to the second jewel of the Triple Crown, the Preakness Stakes. Survivor was the first Preakness winner in 1873. A Derby-Preakness champ then has a shot at Triple Crown immortality at the Belmont Stakes. The first Belmont winner was Ruthless in 1867.<p>
There have been 23 horses that have won the first two jewels of the Triple Crown and failed to win at the Belmont. Two of them, Burgoo King in 1932 and Bold Venture in 1936, didn’t run in New York.<p>
In the 1990's, horses have traveled to Belmont with Triple Crown hopes. Silver Charm and Real Quiet finished second in 1997 and 1998, respectively, while Charismatic placed third in 1999.  In the 2000's, War Emblem finished 8th in the Belmont in 2002 while Funny Cide finished 3rd in 2003.  The following year there was yet another chance of a Triple Crown winner with Smarty Jones, unfortunately he finished 2nd to Birdstone.  In 2008, Big Brown finished in LAST PLACE.  In 2012 we had our hopes on I'll Have Another-- but an injury to his hoof lead his trainer to pull him from the race.</p>
<p>Then we had 2014.  The YEAR OF CALIFORNIA CHROME-- we hoped!</p>
<p>Well, it's not a walk in the park winning the Triple Crown, is it?  Unfortunately, California Chrome didn't have enough in the tank at the Belmont Stakes.  But will there be another Triple Crown winner?  Every year at US Racing we offer future proposition wagers on the Kentucky Derby, Preakness Stakes, Belmont Stakes and Triple Crown.  Login or join today to see the action!</p>  
<p>June 6, 2015 - 2015 Belmont Stakes.  It happened.  FINALLY.  American Pharoah and Victor Espinoza won the Belmont Stakes and captured the Triple Crown!  Can it happen again?  Of course!  Place a bet and watch the races with your family and friends.  History will be made. Will you be a part of it?</p>          
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      

