{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Bet on the Belmont Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 
{include file='inc/belmontstakes-slider.tpl'}

<p>The {include file='/home/ah/allhorse/public_html/belmont/running.php'} running of the Grade 1 Belmont Stakes will be held at Belmont Park on Saturday, {include file='/home/ah/allhorse/public_html/belmont/racedate.php'} with first-race post at noon.</p>

<p><span style="color: #000000;">On race day complete TV coverage airs on NBC beginning at at 5 PM ET and post positions for the {include file='/home/ah/allhorse/public_html/belmont/running.php'}  running of the Belmont Stakes will be announced on the Wednesday before.</span></p>


<p>The final jewel. . . the crowning achievement in horse racing-- the&nbsp;<a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a>!</p>
<p>Place your bets and watch the races!   And let's see if Victor Espinoza and American Pharoah can make history!</p>    

{include file='/home/ah/allhorse/public_html/belmont/odds.php'}



<p></p>   {include file='/home/ah/allhorse/public_html/belmont/links.tpl'}  
        
{include file='inc/disclaimer-bs.tpl'} 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
