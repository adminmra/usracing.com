{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/top-of-page-blocks.tpl"}
 {*schema for the page*}
{literal}
   <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[2]"
    }
	</script>
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress",
                "addressRegion": "Riyadh"
            },
            "name": "King Abdulaziz Racetrack" 
        },
        "name": "The Saudi Cup",
        "startDate": "2021-02-20T08:00-05:00",
        "endDate": "2021-02-20T12:40-05:00",
        "image": [
          "https://www.usracing.com/img/saudi-cup/saudi-cup-lg.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-md.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-sm.jpg"
        ],
        "description": "The Saudi Cup",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-10",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/saudi-cup/betting"
    }
	</script>
{/literal}
{*end of the shema*}
  {*This is the path for the countdown*}
    {include file="/home/ah/allhorse/public_html/saudi-cup/block-countdown-new.tpl"}
  {*end*}


  {*This is the section for the odds tables*}
  <section class="saudi kd usr-section">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>
		  <h3 class="kd_subheading">{$h3}</h3>{*This h1 is change in the xml whit the tag h1*}
      <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb.tpl'} </p>
      <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet on the Saudi Cup</a></p>
      <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds_xml.php'}</p>

        {*The path for the main copy of the page*}
        {* <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb-odds.tpl'} </p> *}
        {*end*}

        {*The path for the year of the event*}
                {*end*}
        
        {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
        {* <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/odds_table.php'}</p> *}
        
        {*end*}

        {*Second Copy of the page*}
        
        <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet on the Saudi Cup</a></p>
        <h2>The {include_php file='/home/ah/allhorse/public_html/saudi-cup/running.php'} Saudi Cup runs on
          {include_php file='/home/ah/allhorse/public_html/saudi-cup/date.php'}.</h2>
        <p>Horse racing fans are preparing for a one of kind event taking place on February 29th at <strong> King Abdulaziz Racetrack in Riyadh, the Capital.</strong> Conceived to have a maximum of 14 horses running over nine furlongs (1,800m) on its dirt track. All aspects are taken care of to the last detail to ensure the event will be as spectacular and unforgettable as it's meant to be. The venue is ready to take in numerous personalities, a diversity of horse racing fans and experts. The kingdoms decision of setting the prize purse in $20 million was no accident, on the contrary its completely intentional and kind of clever as it will most likely cause this new race to remain as the richest one in the world for a good few years.</p>
<p>Needless to say all aspects around the <a href="/signup?ref={$ref}">Saudi Cup</a> are quite ambitious. The usual fashionable turnout for this type of event, top of the line hospitality, up to the occasion organization and a spotless, breathtaking venue will be ready in a matter of days. Missing the <a href="/signup?ref={$ref}">Saudi Cup</a> is simply not an option.</p>
 </li>
        </ul>
      </div>
    </div>
  </section>
 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/end-of-page-blocks.tpl"}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}