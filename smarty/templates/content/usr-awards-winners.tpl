{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}

{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
.usrHero .text .text-xl {
    font-size: 36px;
    font-weight: 700;
    margin-top: 30px;
}
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

 {literal} 

<script type="application/ld+json">

{

  "@context": "http://schema.org",

  "@type": "SportsTeam",

  "name": "Pegasus World Cup",

  "sport": "Horses",

  "memberOf": [

    {

      "@type": "SportsOrganization",

      "name": "BUSR is the best place you can bet on the Pegasus Word Cup"

    },{

      "@type": "SportsOrganization",

      "name": "Only at BUSR, you get great future odds with amazing payouts on the Pegasus World Cup,"

    },{

      "@type": "SportsOrganization",

      "name": "Kentucky Derby and even the leading Derby "

    }

  ],

  "coach": {

    "@type": "Person",

    "name": "Pegasus slaying a dragon"

  },

  "athlete": [

    {

      "@type": "Person",

      "name": "The Stronach Group"

    },{

      "@type": "Person",

      "name": "US Racing"

    }

  ]

}

</script>

{/literal}


<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>    

{literal}
     <style>
		.responsive {
		width: 70%;
		height: auto;}
		.awards_heading {
    color: #1672bd;
    font-family: Lato,sans-serif;
    font-size: 50px;
    font-weight: 700;
    line-height: 1.3;
    margin: 0 0 20px;
    text-align: center;
}
	
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero} {literal});
         }
		 .desktop-hide {
			display: none;
		}
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero} {literal});
         }
		 .mobile-show {
           display: initial;
         }
		 
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_mobile} {literal})!important;
         }
		 .mobile-show {
			display: initial;
		}
       }

       @media (max-width: 479px) {
         .mobile-show {
           display: initial;
         }
		 .responsive {
		width: 100%;
		height: auto;
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
		 .responsive {
			width: 100%;
			height: auto;
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
		  .responsive {
			width: 100%;
			height: auto;
		  }
		
		}
		
     </style>
{/literal}


 <div class="usrHero" style="background-image: url({$hero});" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1}</span>
    </div>
    <div class="text text-md copy-md">
       <span> {$h2} </span>
        <a class="btn btn-red" href="#polls">Learn More</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2}</span>
        <a class="btn btn-red" href="#polls">Learn More</a>
    </div>
</div>
   
 

  <!------------------------------------------------------------------>
    
    
  <section class="pwc kd usr-section" style="background-size: 0 0 !important;">
    <div class="container">
      <div class="kd_content">
      
        <h1 class="awards_heading">USR Best of {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'}  Awards Winners</h1>
		<h2>Congratulations to the winners of the US Racing Best Of Awards for {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'}</h2>
        <p style="margin-bottom: 2px;">Your voice has been chosen! We are excited to announce the winners of the first edition of the USR Best of {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'} Awards.</p><br>

        <p>Our experts created seven categories with several representatives in each, all whose contributions and activities made positive impacts and substantial differences within the industry in {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'}. Each received hundreds of votes and for many, the final tally came down the wire.</p>

        <p>Congratulations to the top OTTB Organization winner, Unbridled Sprints. The entire industry remains grateful for the great work and contribution Unbridled Sprints makes to retired racehorses and the example of excellence they continue to set each year.</p>

        <p>Additional congratulations goes out to the top vote-getters from all of the other categories: Authentic (Top 3 Year Old), Sackatoga Stable (Best Owners), Umberto Rispoli (Best Jockey), Jonathan Wong (Best Trainer), Spendthrift Farm (Best Farm), Chris Brown (Best Social Media Account).</p>
	  
		
        <div class="polls">
          <div class="grid-item">
			<h2 class="table-title">Congratulations to the winners </h2>
			
      <div class="grid-item">
      <h3>Best OTTB Organization Winner </h3>
      <img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-OTTB-Organization.jpg" alt="best ottb" class="responsive">
      </div>
      <br>

      <div class="grid-item">
			<h3>Top 3 Year Old Winner </h3>
			<img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-3-Year-Old.jpg" alt="top 3 year" class="responsive">
      </div>
      <br>

      <div class="grid-item">
			<h3>USR Best Owners Winner </h3>
			<img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-Owner.jpg" alt="best owners" class="responsive">
      </div>
      <br>
      
      <div class="grid-item">
			<h3>USR Best Jockey Winner </h3>
      <img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-Jockey.jpg" alt="best jockey" class="responsive">
      </div>
      <br>

      <div class="grid-item">
			<h3>USR Best Trainer Winner </h3>
      <img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-Trainer.jpg" alt="best trainer" class="responsive">
      </div>
      <br>

      <div class="grid-item">
			<h3>USR Best Farm Winner </h3>
      <img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-Farm.jpg" alt="best farm" class="responsive">
      </div>
      <br>

      <div class="grid-item">
      <h3>USR Best Social Media Account</h3>
      <img src="/img/usr-awards-winners/USRAwards-Winners_twitter-Top-Social-Media-Account.jpg" alt="social media" class="responsive">
      </div>
      </div>
      <br>

		<p>We thank all the people who took time to vote and choose the best candidates for the USR Best of {include_php file='/home/ah/allhorse/public_html/usracing/usr_awards/year.php'} Awards.  See you again next year. </p>

         
      </div>
    </div>
  </section>

  <!------------------------------------------------------------------>


{include file="/home/ah/allhorse/public_html/presidential/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-signup-bonus-awards.tpl" }
{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"}



  <!------------------------------------------------------------------>
