
 
{ literal}
<script>
$().ready(function() {
	//for "select" validation
jQuery.validator.addMethod(  
          "notEqualTo",  
          function(elementValue,element,param) {  
            return elementValue != param;  
          },  
          "Value cannot be {0}"  
        );
								
	// validate signup form on keyup and submit
	$("#htmlform").validate({
		rules: {
			name: "required",
			email: {
				required: true,
				email: true
			},
			state: {
     required: true,
					notEqualTo: 0
        }
		},
		messages: {
			name: "Please enter your name",			
			email: "Please enter a valid email address",
			state: "Please select a State/Province"
		}
	});

});
</script>
{/literal}


{ literal}  
<style type="text/css"></style>
{/literal} 

   

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




{if isset($smarty.request.thanks)}
	<div class="joinBox thankyou">
 <p class="title.lrg"></p>
 <p class="title">Thank you for your interest in US Racing!</p>
 <p class="title">Stay tuned! We will notify you when US Racing has launched.</p>
 <p class="subTitle">Don't forget to Register after launch to claim your FREE OFFICIAL US Racing Merchandise!</p>
 <hr />
 <p class="title reg">In the meantime, mark your calendars for these big races:</p>
 
 <ul>
 <li><span><strong>Kentucky Derby</strong></span> <div class="small">May 3th, 2014</div></li>
  <li><span><strong>Preakness Stakes</strong></span><div class="small">May 17, 2014</div></li>
  <li><span><strong>Belmont Stakes</strong></span><div class="small">June 14th, 2014</div></li>
 </ul>
 </div>

{else}
  
<div class="joinBox">
<div class="content">
<!-- --------------------- content starts here ---------------------- -->



   <div class="titleBox">Be The First<span class="arrow"></span></div>
   <div class="titlesub">To Bet Online with US Racing</div>
   <hr />
   <ul>
   <li>The Easiest site to bet on Horses</li>
   <li>It's Legal, Licensed & Safe</li>
   <li>Free Bets & Cash Bonuses</li>
   <li>Computer, Tablet and Mobile Betting</li>
   <li>Over 150 racetracks</li>
   </ul>
   <hr />
   <div class="logo"><img alt="usr-logo-white" src="/themes/images/usr-logo-white.png" /></div>
   <div class="titleBox small">Launching Soon!</div>
</div>

	<form class="form-container" name="htmlform" id="htmlform" method="post" action="http://allhorse.com/capture/index.php">
 <div class="form-title small">LIKE FREE STUFF?</div>
 <div class="form-title lrg">Get Notified When We Launch*</div>
 <hr />
	<input type="hidden" name="action" value="usracing">
    <div class="form-box">
    <div class="form-title"><h2>Name</h2></div>
	<div class="form-title"><input name="name" type="text" class="form-field" value="" placeholder="REQUIRED"  /></div>
    <div class="form-title"><h2>Email Address</h2></div>
    <div class="form-title"><input name="email" type="text" class="form-field" value="" placeholder="REQUIRED"  /></div>
    
    
    <div class="select-container">
    <div class="form-title"><h2>State/Province</h2></div>
    <select name="state" id="state" required />
     <option value="0">Select</option>
     <option value="AL">Alabama</option>
   
	<option value="AK">Alaska</option>
	<option value="AZ">Arizona</option>
	<option value="AR">Arkansas</option>
	<option value="CA">California</option>
	<option value="CO">Colorado</option>
	<option value="CT">Connecticut</option>
	<option value="DE">Delaware</option>
	<option value="DC">District Of Columbia</option>
	<option value="FL">Florida</option>
	<option value="GA">Georgia</option>
	<option value="HI">Hawaii</option>
	<option value="ID">Idaho</option>
	<option value="IL">Illinois</option>
	<option value="IN">Indiana</option>
	<option value="IA">Iowa</option>
	<option value="KS">Kansas</option>
	<option value="KY">Kentucky</option>
	<option value="LA">Louisiana</option>
	<option value="ME">Maine</option>
	<option value="MD">Maryland</option>
	<option value="MA">Massachusetts</option>
	<option value="MI">Michigan</option>
	<option value="MN">Minnesota</option>
	<option value="MS">Mississippi</option>
	<option value="MO">Missouri</option>
	<option value="MT">Montana</option>
	<option value="NE">Nebraska</option>
	<option value="NV">Nevada</option>
	<option value="NH">New Hampshire</option>
	<option value="NJ">New Jersey</option>
	<option value="NM">New Mexico</option>
	<option value="NY">New York</option>
	<option value="NC">North Carolina</option>
	<option value="ND">North Dakota</option>
	<option value="OH">Ohio</option>
	<option value="OK">Oklahoma</option>
	<option value="OR">Oregon</option>
	<option value="PA">Pennsylvania</option>
	<option value="RI">Rhode Island</option>
	<option value="SC">South Carolina</option>
	<option value="SD">South Dakota</option>
	<option value="TN">Tennessee</option>
	<option value="TX">Texas</option>
	<option value="UT">Utah</option>
	<option value="VT">Vermont</option>
	<option value="VA">Virginia</option>
	<option value="WA">Washington</option>
	<option value="WV">West Virginia</option>
	<option value="WI">Wisconsin</option>
	<option value="WY">Wyoming</option>
	<option value="AS">American Samoa</option>
<option value="US Territory">US Territories</option>
<option value="Armed Forces">Armed Forces</option>	
<option value="Canada">Canada</option>
<option value="Outside US-Canada">Outside US and Canada</option>
</select>				
	</div>
    <div class="submit-container"><input type="submit" class="submit-button" value="Notify Me!" /></div>
    
    <div class="form-title small">* New Members get FREE Official US Racing merchandise when registering after launch. Limited supply.</div>
    </div>
    
    
	</form>
</div>    
{/if}




&nbsp;

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


 
    
  

