{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Big Red</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2 class="DarkBlue" style="margin: 20px 0 20px 0;">Secretariat</h2></div>
<div style="display: block;">There is very little one can say about Man o'War which has not already been said, but it would be unconscionable to remain silent in regards to the most beloved figure in American racing history.&nbsp;

<p>Secretariat was conceived in Kentucky and born in Virginia. When turned out to graze with the other foals and dams, Secretariat was seen to be more independent and masterful than the others. He didn't cling to his mother like newborns usually do. Owner, Penny Tweedy, recognized almost immediately that this was an intelligent, confident, and curious individual. He was going to be a "take-charge" kind of horse someday.&nbsp;</p>
<p>Some of the first names submitted to the Jockey Club for this colt were "Sceptre", "Royal Line", "Something Special", "Games of Chance", and "Deo Volente" (which is Latin for "God Willing"). But none of these names were available. Finally, the sixth name submitted was accepted. And that is how we came to know him as Secretariat.Trainer, Lucien Laurin, started the colt in a 5 1/2 furlong purse at Aqueduct on the fourth of July. His odds that day were 3-to-1 (the highest odds you would ever receive on Secretariat). The poor chestnut colt got mugged coming out of the gate, and suffered terrible interference. What a surprising introduction this was to his new career! He finished in fourth that day - the only time he would ever finish off the board. From this rough treatment, he learned to break slowly from the gate to avoid trouble. He was indeed a quick learner.&nbsp;</p>
<p>The remainder of his two-year-old season was an exhibition of consistency. He put together a string of eight consecutive first place finishes, including the best juvenile events in this country. The only black marks against an otherwise perfect season were the aforementioned debut race, and the Champagne Stakes at Belmont which was taken away from him on a disqualification and awarded to Stop The Music.&nbsp;</p>
<p>By year end, it was virtually unanimous that Secretariat was the most exciting horse in training (of any age), so at the age of two, he was crowned Horse Of The Year.&nbsp;</p>
<p>Secretariat wintered at Hialeah, Florida, while plans for his future were being made. In a deal arranged by Claiborne Farm, the colt was syndicated into shares and therefore the future breeding rights to him were purchased. The deal was worth a record $6,080,000 and stipulated the condition that the colt would only race though his three-year-old season.&nbsp;</p>
<p>At three, he debuted in the Bay Shore Stakes at Aqueduct at the shrunken odds of 1-to-5, so great was the public's faith in him, although he hadn't raced in four months. No worries - he won easily by 4 1/2 lengths. This was followed by a three length victory in the Gotham Stakes. Here was, at this point, a virtually unstoppable superstar. He had only one more prep planned before the Triple Crown and that was the Wood Memorial. "Superman" failed that day. The victory went to stablemate Angle Light, with Sham second. Secretariat's third place performance was disappointing to most, and unbelievable to others. Had someone placed Kryptonite in his saddlepads?&nbsp;</p>
<p>No - he was merely the victim of a slow pace, and simply could not reach the other two in time. Secretariat knew he had lost, and that angered him, according to his owner. Miss Tweedy also remarked that the loss was like a release valve - it took a great deal of pressure off the colt going into the Triple Crown. It showed the world that he was real, which made his outstanding victories all the more phenomenal.&nbsp;</p>
<p>The public was convinced the Wood had been an aberration. Secretariat and Angle Light were the bettors' choice in the Derby at 3-to-2, although Sham was getting very good support as second choice at 5-to-2 odds. Shecky Greene assumed the early lead with Gold Bag, Royal And Regal, Angle Light and Sham in closest pursuit. Where was the $6 million dollar colt? Dead last. Secretariat relaxed nicely from the gate, and settled in at the rear, allowing the others to show the way. Making his way into the first turn he moved to the outside and began picking off the competition one by one.&nbsp;</p>
<p>Secretariat charged up the backstretch with his sights set on the leader who was by this time - Sham. That one drew clear of his rivals rounding the final turn, but Secretariat was looming on the outside. Sham led the way into the stretch but quickly Secretariat was upon him. For a few strides they ran together. Clearly the winner would be one of these two, as the others were hopelessly outpaced. Jockey Ron Turcotte showed the whip to his mount just once, and the chestnut colt responded, drawing away from his pursuers. At the wire, he had won by 2 1/2 lengths over Sham, setting a new track record. Sham's race was also a marvelous one. His finishing time also bettered the previous race record. Our Native was 8 lengths further away in third place.&nbsp;</p>
<p>In the Preakness, Secretariat again began in last place, but was close to the rear of the pack, not far back. He passed the grandstand the first time still trailing. Then, far before Turcotte planned to urge him forward, Secretariat made up his own mind that the time had come to take the lead. He lunged forward with a mighty leap and burst past the others. He soared up the outside of the pack and assumed the lead before they had even gone a half mile. The remainder of the race was simply for the others to determine who would finish second behind him. Sham was once again the best of the rest, finishing (again) 2 1/2 lengths behind Secretariat, with Our Native (again) eight lengths further back in third place. The finishing time on the track's teletimer was 1:55, a full second off the track record, but Daily Racing Form clockers all recorded a time of 1:53 2/5, which shattered the old record. The matter of the official time of this race is still in deliberations.&nbsp;</p>
<p>For the Belmont Stakes, Secretariat emerged from an inside post position in a field of only 5 runners. The break was clean and My Gallant got out the quickest with Sham up on the outside, and Secretariat at the rail. Seeing no one to block his path and interfere with him, Secretariat went right for the lead. He assumed command almost immediately after the start, and Sham went right up with him to pressure him. The two raced together around the first turn, then Secretariat drew away, and continued drawing away. By the mid-backstretch his lead was 10 lengths. By the far turn it was 12 lengths. He turned into the homestretch at least 20 lengths ahead of the others.&nbsp;</p>
<p>Chick Anderson, who called the race that day, spoke the now immortal words "...but Secretariat is moving like a tremendous machine... he's a sixteenth of a mile ahead of the other horses!" And he continued to widen the margin. 22 lengths. 25 lengths. 28 lengths. At the wire, the official margin was an unbelieveable 31 lengths. Twice a Prince was far, far back in second place. There was no doubt about this finishing time - 2:24 flat - a new world record.&nbsp;</p>
<p>"Big Red" went next to the Arlington International where his odds were drummed down to microscopic proportions, 1-to-20. He won that race by 9 lengths. The he ran in the Whitney Stakes, carrying a low-grade fever, and lost to Onion, but only by a length. In the Marlboro Invitational, he spearheaded a Meadow Stable exacta, leading Riva Ridge to the line. His last start on the dirt came in the sloppy Woodward Stakes, and there he finished second to Prove Out.&nbsp;</p>
<p>His last two outings both came on the turf. Lucien Laurin convinced Penny Tweedy that Secretariat could be even better on grass than he had been on the dirt. Perhaps the trainer was right - his turf debut in the Man o'War Stakes resulted in a new course record at Belmont Park. For his final race, Secretariat would get the services of jockey Eddie Maple. Turcotte was serving a suspension and was not allowed to ride for a certain number of days. The swansong came in the Canadian International Championship Stakes at Woodbine, Ontario. The weather was miserable, foggy, wet and cold. But Secretariat let no one down. He powered around the course like a locomotive with steam blowing from his nostrils. He was an easy 6 1/2 length winner. This victory brought his earnings to a then-record of $1,316,808.&nbsp;</p>
<p>A few weeks later, he was paraded under tack at Aqueduct, site of his first race, before a crowd of admirers who longed for one more glimpse of him carrying the famous Meadow silks. The crowd of 6,000 was the largest ever to gather at Aqueduct on a non-racing day.&nbsp;</p>
<p>Secretariat was retired to Claiborne Farm where he sired Horse Of The Year LADY'S SECRET and champions MEDAILLE D'OR and RISEN STAR. Other outstanding offspring included GENERAL ASSEMBLY (Travers winner), IMAGE OF GREATNESS, FIESTA LADY, PANCHO VILLA, TERLINGUA, ATHYKA, CLEVER SECRET, TINNERS WAY and many others.&nbsp;</p>
<p>Even more impressive is the role-call of his daughters' produce. They include champions CHIEF'S CROWN, A.P. INDY, DEHERE, BORN WILD (in Austria), MIZORAM (in Dubai), CORREGIO (steeplechaser), and ISTABRAAQ (hurdler), and remarkable stakes winners LACOVIA, STORM CAT, CLASSIC CROWN, SECRETO, AL MAMOON, GONE WEST, SUMMER SQUALL, HONOR GRADES, LA GUERIERE, HEART OF JOY, CAPTIVE MISS, CHEROKEE ROSE (IRE), TA RIB, SPINNING ROUND, IRGUN, DARE AND GO, ATTICUS, HEART OF OAK, LISTENING, WELL CHOSEN, and so many others.&nbsp;</p>
<p>Secretariat died at age 19, relatively young for a thoroughbred. He had been suffering from an incurable foot disease called laminitis, which made it painful for him to walk or even stand. When his suffering became too great, the heart-wrenching decision was made to end his life peacefully. Secretariat was given the honor of being buried whole in the Claiborne cemetary. It is traditional for horses to be buried "head, heart, and hooves", but certain special horses deserve special treatment. Visitors still flock to Claiborne Farm, bearing flowers and gifts for their hero.&nbsp;</p>
<p><strong>Born:</strong> March 30, 1970, bred in Virginia by Meadow Stud, Inc.<br /><strong>Died:</strong> October 4, 1989, (age 19) at Claiborne Farm, Paris, Kentucky and is buried there.</p>
<p><strong>Pedigree:</strong>&nbsp;</p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td rowspan="8" width="22%">Secretariat ch.c.<br />born 1970</td>
<td rowspan="4" width="27%">Bold Ruler, 1954</td>
<td rowspan="2" width="30%">*Nasrullah, 1940</td>
<td width="21%">Nearco</td>
</tr>
<tr>
<td width="21%">Mumtaz Begum</td>
</tr>
<tr>
<td rowspan="2" width="30%">Miss Disco, 1944</td>
<td width="21%">Discovery</td>
</tr>
<tr>
<td width="21%">Outdone</td>
</tr>
<tr>
<td rowspan="4" width="27%">Somethingroyal, 1952</td>
<td rowspan="2" width="30%">*Princequillo, 1940</td>
<td width="21%">Prince Rose</td>
</tr>
<tr>
<td width="21%">Cosquilla</td>
</tr>
<tr>
<td rowspan="2" width="30%">Imperatrice, 1938</td>
<td width="21%">Caruso</td>
</tr>
<tr>
<td width="21%">Cinquepace</td>
</tr>
</tbody>
</table></div>
<p>(female family # 2)</p>

<p>&nbsp;<span style="text-decoration: underline;"><strong>Racing Record:</strong></span><strong><br /></strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<th>Year</th>
<th>Age</th>
<th>Starts</th>
<th>1st</th>
<th>2nd</th>
<th>3rd</th>
<th>unp.</th>
<th>earnings</th>
</tr>
<tr>
<td>1972</td>
<td align="center">2</td>
<td align="center">9</td>
<td>7</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td align="right">$ 456,404</td>
</tr>
<tr>
<td>1973</td>
<td align="center">3</td>
<td align="center">12</td>
<td>9</td>
<td>2</td>
<td>1</td>
<td>0</td>
<td align="right">$860,404</td>
</tr>
<tr>
<td>total</td>
<td>&nbsp;</td>
<td align="center">21</td>
<td>16</td>
<td>3</td>
<td>1</td>
<td>1</td>
<td align="right">$1,316,808</td>
</tr>
</tbody>
</table></div>

<p>&nbsp;<span style="text-decoration: underline;"><strong>Stakes Record:</strong></span><strong><br /></strong></p>
<p>at 2:</p>
<ul>
<li>won - Sanford Stakes </li>
<li>won - Hopeful Stakes </li>
<li>won - Belmont Futurity </li>
<li>won - Laurel Futurity </li>
<li>won - Garden Stake Stakes </li>
<li>2nd - Champagne Stakes (disqualified from 1st) </li>
<li><strong>Champion 2yo Colt</strong> </li>
<li><strong>Horse Of The Year</strong>&nbsp;</li>
</ul>
<p>&nbsp;at 3:</p>
<ul>
<li>won - Kentucky Derby (new track record) </li>
<li>won - Preakness Stakes (new track record) </li>
<li>won - Belmont Stakes (new <span style="text-decoration: underline;">WORLD</span> record on dirt) </li>
<li>won - Bay Shore Stakes </li>
<li>won - Gotham Stakes (equalled track record) </li>
<li>won - Arlington Invitational </li>
<li>won - Marlboro Cup (new <span style="text-decoration: underline;">world</span> record) </li>
<li>won - Man o' War Stakes (new course record) </li>
<li>won - Canadian International </li>
<li>2nd - Woodward Stakes </li>
<li>2nd - Whitney Stakes </li>
<li>3rd - Wood Memorial </li>
<li><strong>Champion 3yo Colt</strong> </li>
<li><strong>Champion Grass Horse</strong> </li>
<li><strong>Horse Of The Year</strong>&nbsp;</li>
</ul>


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    