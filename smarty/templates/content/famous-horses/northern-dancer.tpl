{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Northern Dancer</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<div><img style="float: left; margin:0 20px 10px 0;" src="/themes/images/famoushorses/northern-dancer.jpg" alt="Northern Dancer"  /></div>
<p>Northern Dancer was born on May 27, 1961, a homebred for E.P. Taylor's National Stud in Canada. A small bay with three white stockings and a crooked blaze, he was the first foal of the Native Dancer mare, Natalma. From the first crop of the farm's new stallion, Nearctic, no one could've been expected to see the magic in this mating.</p>
<p>Nor would they see it when Northern Dancer was a yearling. Mr. Taylor offered Northern Dancer at his annual sale, listing him at $25,000, but there were no takers for the late-foaled pint-sized colt. So Taylor shipped Northern Dancer to Fort Erie and trainer Tom Fleming, a former assistant to Horatio Luro. <br />Northern Dancer opened his career on Aug. 2, 1963 with a win, then promptly added a stakes, the Summer Stakes, to his resume a few weeks later. Fleming would have the colt for one more race, a second-place finish in the Vandal Stakes, before the colt was sent to Hall of Fame trainer Luro. Northern Dancer's first start for Luro was the Cup and Saucer Stakes at Woodbine. He lost to a longshot in that outing, but went on to close out the year with 5 straight wins, including the Remsen Stakes, to be named Canada's Champion 2-yr-old Colt.</p>
<p>The 3-yr-olds of 1964 were a nice group, but two colts, one on each coast, were early standouts. In the west was a tall elegant bay colt, Hill Rise, undefeated in 4 starts at 3, including a 6-length romp in the Santa Anita Derby. And in the east was the diminutive Northern Dancer, winner of 4 in 5 starts going into his last Derby prep, the Bluegrass Stakes.
</p><p>Before the running of the Bluegrass, Jockey Bill Shoemaker, regular rider for both Northern Dancer and Hill Rise, was asked by Luro to make a commitment to one horse or the other. Shoemaker, like the potential buyers of the colt 2 years earlier, didn't see the giant residing inside the small package -- he chose Hill Rise, a horse "a little more seasoned." This opened up the mount on Northern Dancer and Bill Hartack accepted the reins. The newly-formed team took the Bluegrass Stakes then headed to Louisville for the Kentucky Derby.<br />Hill Rise was favored to wear that year's blanket of roses. Northern Dancer was second-favorite, with only two other horses of the twelve at single-digits, The Scoundrel and Quadrangle. Also in the field were Roman Brother, Mr. Brick, and Dandy K.</p>
<p>The bells rang and the gates clanged open. Mr. Brick, as expected, was the pace-setter -- Northern Dancer settled mid-pack then steadily advanced through the far turn to catch the leader. As the turn straightened, Dancer was going 4-wide but had his head in front, and he began drawing away, by a length, then by two lengths. But a horse had kicked into gear on the outside -- Hill Rise was gaining on him with every stride. The photo showed Northern Dancer hanging on by half a length -- the only half, incidentally, visible beyond the comparative hulk to his outside. Northern Dancer stopped the clock at 2:00 flat, a new track record and a stakes record bested only by Secretariat in the quarter-century since.</p>
<p>The usually-astute Pimlico crowd didn't favor Northern Dancer for the Preakness. Hill Rise would again receive that honor, but he was no match for Dancer this day. The two raced neck and neck around the far turn but Northern Dancer shook loose at the top of the stretch. Never threatened, he won by 2 lengths.</p>
<p>Northern Dancer and his nemesis Hill Rise headed to Aqueduct and the last leg of the Triple Crown, the Belmont Stakes. Northern Dancer was finally installed the favorite but a win was too much to ask of the campaign-weary colt. He finished a well-beaten third to Quadrangle and Roman Brother.</p>
<p>Northern Dancer was sent to his homeland for his next start, the June 30 Queen's Plate, and he took the Canadian classic with ease. His next start was to be the Travers, but a tendon injury would force not only his withdrawal from the race but also his retirement. The little bay headed back to his birthplace to take up stud duty -- and to fulfill a greater destiny.</p>
<p>Initially standing for $10,000 live foal, Northern Dancer was a bargain-basement deal for those breeders who chose him. From his first crop came a Canadian 2-yr-old champion and Horse of the Year, Vice Regal, the undefeated juvenile winner of 7 stakes in 8 starts. In his second crop came his first European champion, Nijinsky II. Sold for $84,000 as a yearling, the highest paid for a Northern Dancer to that point, Nijinsky didn't disappoint his buyers. Undefeated at 2, he was named 2-yr-old champion in England and Ireland.</p>
<p>In December of 1968, Northern Dancer was shipped to stand at stud in the United States, at E.P. Taylor's newly-acquired Windfields farm in Maryland. The following year saw Northern Dancer's popularity soar higher as Nijinsky II won the '69 English Triple Crown -- the 2000 Guineas, Epsom Derby, and the St. Leger Stakes. Nijinsky II was again named divisional champion in England and Ireland as well as Horse of the Year in both countries.</p>
<p>Northern Dancer's sire statistics were unheard of -- the '66 crop of 21 foals, for example, contained 10 stakes winners from 18 starters. Though his average would settle around a more mortal rate of 21%, it was a remarkable average. Northern Dancer was leading sire in North America in 1971 and '77 and leading sire in England in 1970, '77, '83, and '84. Northern Dancer blood was so intensely sought after that his stud fee sky-rocketed from $25,000 live foal in '71 to over $1,000,000 no guarantee in the early 1980s. Even at that price, a date with Northern Dancer seemed safe -- in 1981, Northern Dancer's yearlings averaged over $1,000,000, and in 1984 his 14 yearlings averaged an astounding $3,300,000, a record average unfathomable to ever be surpassed.</p>
<p>Perhaps the most exciting auction in the history of Thoroughbred breeding occurred with the offer of hip #308 in the 1983 Keeneland Select Sale. In the days leading up to the sale, the dark bay or brown colt by Northern Dancer - My Bupers, by Bupers was expected to set a new auction record. Owner Don Johnson of Crescent Farm was nervous, stating that he only hoped the colt, the best individual he'd ever bred, would remain alive to make it through the auction ring.</p>
<p>The anticipated moment arrived, and trainer and bloodstock expert D. Wayne Lukas led off the bidding at $1,000,000. Quickly he was overtaken by two of the wealthiest owners in racing, Englishman Robert Sangster and Sheikh Maktoum of Dubai. Within 45 seconds, a new record of $4,500,000 was reached as the two men outbid each other. But the price kept climbing. As the bidding reached $6,000,000, the crowd couldn't contain their excitement, first gasping in astonishment then clapping to show the two bidders their appreciation. $7 million, $8 million, $9 million -- at each plateau the polite pandemonium grew. Keeneland employee Tom Caldwell stopped briefly the auctioneer, "Before you go much farther, would you like to come up and cut a new digit up here?" referring to the 7-digit toteboard. The crowd roared in delight. The toteboard flashed to zero as racing saw its first $10 million yearling, a colt who'd later be named Snaafi Dancer. Neither man was prepared to go much higher -- the gavel finally dropped at Sheikh Maktoum's bid of $10,200,000.</p>
<p>Northern Dancer would be productive at sire through 1986 -- he was pensioned in 1987 at age 26 with only 1 of 2 mares in foal. By this time, Northern Dancer had established himself as indisputedly the greatest sire of the past three centuries. Not only was he the sire of top-class runners, Northern Dancer was a rare sire of sires.</p>
<p>These are but a few of Northern Dancer's sons who would go on to found dynasties of their own: Danzig (sire of Danehill, Danzig Connection, Green Desert, Polish Precedent, Lure, Belong to Me, Honor Grades. Deerhound, Pine Bluff, Shaadi, Zieten, Ascot Knight, The Jogger, Lustra, Katowice, Polish Navy, Dayjur, etc.); Lyphard (Dancing Brave, Alzao, Elliodor, Ghadeer, Lypheor, Ends Well, etc.); Nijinsky II (Caerleon, Shadeed, Royal Academy, Green Dancer, Hostage, etc.); Nureyev (Theatrical, Polar Falcon, Zilzal, etc.); Sadler's Wells (Barathea, El Prado, Fort Wood, In The Wings, Saddlers Hall, Scenic, etc.); Storm Bird (Storm Cat, Personal Hope, Bluebird, Summer Squall, etc.); and Vice Regent (Deputy Minister, Regal Classic, etc.).</p>
<p>Northern Dancer passed away at Windfields Farm in the Fall of 1990. He traveled back once more to his birthplace, this time to be buried. To again fly without wings.</p>
<p><strong>Northern Dancer</strong>, May 27, 1961, bay, Neartic - Natalma, by Native Dancer<br /><strong>Owner</strong>: Windfields Farm<br /><strong>Trainer:</strong> H. A. Luro<br /><strong>Breeder:</strong> E.P. Taylor<br /><strong>Jockey:</strong> W. Hartack<strong>&nbsp;</strong></p>

<p><strong>Pedigree</strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td rowspan="8" width="25%">Northern Dancer, bay c.<br />foaled 1961</td>
<td rowspan="4" width="25%">Nearctic</td>
<td rowspan="2" width="25%">Nearco</td>
<td width="25%">Pharos</td>
</tr>
<tr>
<td>Nogara</td>
</tr>
<tr>
<td rowspan="2">Lady Angela</td>
<td>Hyperion</td>
</tr>
<tr>
<td>Sister Sarah</td>
</tr>
<tr>
<td rowspan="4">Natalma</td>
<td rowspan="2">Native Dancer</td>
<td>Polynesian</td>
</tr>
<tr>
<td>Geisha</td>
</tr>
<tr>
<td rowspan="2">Almahmoud</td>
<td>Mahmoud</td>
</tr>
<tr>
<td>Arbitrator</td>
</tr>
</tbody>
</table></div>
<p style="margin-left: 150px; display: block;">&nbsp;</p>
<p><strong>Career Highlights:</strong>&nbsp;</p>

<ul>
<li>1963 Champion 2-yr-old Male in Canada</li>
<li>1964 Champion 3-yr-old Male</li>
</ul>

<p><strong>Race Record</strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr align="right">
<td>Year</td>
<td>Age</td>
<td>Starts</td>
<td>1st</td>
<td>2nd</td>
<td>3rd</td>
<td height="37" align="right">
<div>Earnings</td>
</tr>
<tr align="right">
<td>1963</td>
<td>2</td>
<td>9</td>
<td>7</td>
<td>2</td>
<td>0</td>
<td>90,635</td>
</tr>
<tr align="right">
<td>1964</td>
<td>3</td>
<td>9</td>
<td>7</td>
<td>0</td>
<td>2</td>
<td>490,171</td>
</tr>
<tr align="right">
<td>Totals</td>
<td>&nbsp;
<div>&nbsp;</td>
<td>18</td>
<td>14</td>
<td>2</td>
<td>2</td>
<td>$580,806</td>
</tr>
</tbody>
</table>
</div>

<p style="margin-left: 150px; display: block;">&nbsp;</p>
<p><strong>At 2:</strong>&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>Maiden</strong>, 8-2-63, Fort Erie</li>
<li><strong>Summer S</strong>&nbsp;</li>
<li><strong>Allowance</strong>&nbsp;</li>
<li><strong>Allowance</strong>&nbsp;</li>
<li><strong>Coronation Futurity</strong>&nbsp;</li>
<li><strong>Carleton S</strong>&nbsp;</li>
<li><strong>Remsen S</strong>&nbsp;</li>
</ul>
<p><strong>2nd:</strong>&nbsp;</p>
<ul>
<li><strong>Vandal S</strong>&nbsp;</li>
<li><strong>Cup and Saucer S</strong>&nbsp;</li>
</ul>

<p><strong>At 3:</strong>&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>Flamingo Stakes</strong>, 3-3-64, Hialeah, dirt, 1 1/8 miles, by 2 lengths, defeating Mr. Brick, Quadrangle, etc.</li>
<li><strong>Allowance</strong>, 3-28-64, Gulfstream, dirt, 7 furlongs, by 2 lengths.</li>
<li><strong>Florida Derby</strong>, 4-4-64, Gulfstream, dirt, 1 1/8 miles, by 1 length, defeating The Scoundrel, Dandy K., etc.</li>
<li><strong>Blue Grass Stakes</strong>, 4-23-64, Keeneland, dirt, 1 1/8 miles, by 1/2 length, defeating Allen Adair, Royal Shuck, etc.</li>
<li><strong>Kentucky Derby</strong>, 5-2-64, Churchill, dirt, 1 1/4 miles, by neck, defeating Hill Rise, The Scoundrel, etc. <strong>New track and stakes record of 2:00 flat.</strong>&nbsp;</li>
<li><strong>Preakness Stakes</strong>, 5-16-64, Pimlico, dirt, 1 3/16 miles, by 2 1/4 lengths, defeating The Scoundrel, Hill Rise, etc.</li>
<li><strong>Queen's Plate</strong>, 6-30-64, Woodbine, dirt, 1 1/4 miles, by 7 1/2 lengths, defeating Langcrest, Grand Garcon, etc.</li>
</ul>
<p><strong>3rd:</strong>&nbsp;</p>
<ul>
<li><strong>Allowance</strong>, 2-10-64, Hialeah, dirt, 6 furlongs, by 2 lengths.</li>
<li><strong>Belmont Stakes</strong>, 6-6-64, Aqueduct, dirt, 1 1/2 miles, by 6 lengths, to Quadrangle and Roman Brother.</li>
</ul>



<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    