{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Seabiscuit</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Seabiscuit</h2>


<p><img style="float: left; margin: 0 20px 10px 0;" src="/images/horses/seabiscuit.jpg" alt="Seabiscuit"  class="img-responsive" /></p>
<p>On a drab Detroit side street in August, 1936, two hitchhikers hopped down from their last ride and walked onto the backstretch of Fair Grounds Racecourse. The stouter man was a jockey's agent people called Yummy; he was with his client Johnny Pollard, a flame-haired former prize fighter. Yummy liked to refer to him by his boxing name, the Cougar, but most knew him as Red.&nbsp;</p>

<p>The two had totaled Yummy's car a long way back, picked through the wreckage to salvage their most essential belongings--27 cents and half a pint of a wretched brandy they called "bow-wow wine"--and thumbed their way to the track. Desperate for work, they wound through the shed rows, petitioning nearly every trainer on the grounds. No one was willing to give Pollard a leg up on a horse.&nbsp;</p>

<p>The last barn they visited was run by an obscure trainer named Tom Smith, a jut-jawed old cowboy whose horsemanship ran its roots back through frontier cattle drives, the Boer War and a traveling Wild West show. Smith had come east with his new boss, San Francisco automobile magnate Charles Howard, in search of horses for Howard's racing stable. Owner and trainer needed a jockey sturdy enough to handle their new purchase, a rough, tempestuous colt named Seabiscuit, and Smith thought Pollard's boxer physique might do the trick.</p>
<p>The lives of three vastly different men had come to an intersection, and their crowded hour had begun. The improbable partnership they would form would cultivate each man's greatest, untapped talents, define a pivotal American era and turn a battered little horse into one of the century's most celebrated popular heroes.&nbsp;</p>
<p>Each of the men had traveled a long, hard road to the summer of 1936. For Charles Howard, the journey had begun in New York in 1903, when the young cavalry veteran quit his job as a bicycle repairman and headed west to try his luck. Arriving in San Francisco with two dimes and a penny in his pocket, he set up a bicycle repair shop downtown. His timing was perfect. It was the dawn of the automobile age, and the few locals who had invested in horseless carriages were discovering a flaw in the wondrous machines: Unreliability. The industry was so new that garages didn't yet exist, so owners began bringing their ailing cars to the closest thing to an automotive mechanic in the city, Charles Howard.&nbsp;</p>
<p>Tinkering with the "gasoline buggies," Howard became fascinated with the new technology, foresaw a revolution and headed to Detroit. There, he introduced himself to Will Durant, chief of Buick automobiles and future founder of General Motors. Howard walked away with the Buick franchise for all of San Francisco. It was 1905, and he was not yet 30 years old.&nbsp;</p>
<p>With three Buicks in tow, Howard returned to San Francisco, where he met a hostile marketplace. His commodities, which churned up dust clouds and bogged down in mud, were banned in the city's tourist areas. With no local gas stations, owners had to lug fuel cans to drugstores, filling them for 60 cents a gallon. And the "devilish contraptions" were prohibitively expensive, costing twice the average annual salary. As Howard wheeled his automobiles into his makeshift showroom, the parlor of his bicycle shop, his success was far from assured.&nbsp;</p>
<p>His turn of luck came in hideous guise. At 5:12 A.M. on April 18, 1906, the earth beneath San Francisco heaved inward upon itself and liquefied in a titanic convulsion. In 60 seconds, the city shuddered down. Fires licked to life and raced over the ruins towards Howard's shop, consuming four city blocks per hour. The horse-drawn city was in dire need of transport for water, firemen, the injured and 250,000 homeless--more than half the population--but conventional vehicles were crippled as wagon horses sagged from exhaustion. Howard, owner of three erstwhile unsalable automobiles, was suddenly the richest man in town. His cars joined others to become a lifeline, ferrying the wounded and probably bearing Army explosives used to blast burn-proof zones. As the fire swept toward them, soldiers and firefighters packed Howard's shop and the surrounding buildings with dynamite and detonated all in a desperate attempt to prevent the flames from swallowing the last of the city.</p>
<p>Howard, like virtually everyone else, lost everything. But as San Franciscans started over, Howard took the opportunity to lure them into the automotive age. The earthquake had proven the automobile's superiority to the horse in utility; two weeks after the quake, a day's rental of a sound horse and buggy was $5, while a two-seated runabout fetched $100 per day. Howard set out to prove its durability. He tested his Buicks in speed races, hill climbs and "stamina" runs, in which contestants raced up and down a local road until their beleaguered automobiles burst into flames or shed their wheels--the last one rolling was the winner. His aggressive promotion worked. By 1908, the one-man Howard Automobile Company had sold 85 two-lunger White Streaks at $1,000 each. "The day of the horse is past, and the people in San Francisco want automobiles," he wrote that year. "I wouldn't give five dollars for the best horse in this country."&nbsp;</p>
<p>He was half right. With the fortune yielded by his auto distributorship--by the 1920's, the world's biggest--Howard turned to philanthropy, building a hospital and providing free care for tubercular children. In 1932, after his first marriage ended, he thrilled the society columnists by marrying Marcela Zabala, a beautiful former convent student who had generated considerable local fame as an actress. In 1933, they took aim on a new venture, horse racing, a pastime that was enjoying such explosive growth that it would soon be far and away the most heavily-attended sport in America. Pari-mutuel racing had been banned in California for thirty years, but to boost revenues badly depleted by the Depression, the state legislature had just legalized it again. Betting that the sport would catch on in the state, the Howards invested heavily in the founding of the lavish Santa Anita Racecourse on the apron of the San Gabriels. The track inaugurated the $100,000 Santa Anita Handicap, the richest race in the world, and the Howards hung their hearts on winning it. They bought 15 Thoroughbred yearlings and went looking for a trainer.&nbsp;</p>
<p>Horses had been the silent study of Tom Smith's long life. As a boy on the western frontier, he had ridden in the last of the great cattle drives before being hired as a horsebreaker at just 13 years of age. In 1899, the dour, taciturn young man, known as "Silent Tom," began breaking American mustangs for use by the British in the Boer War, gentling as many as 30 horses per day for a daily wage of about $2.40. After the order for 50,000 horses had been filled, he became a cowpony trainer, veterinarian and blacksmith at a Colorado cattle range.&nbsp;</p>
<p>In 1923, after a brief stint training six woeful horses for rodeo relays, Smith signed on with "Cowboy Charlie" Irwin, a rancher who operated two businesses: a massive stable of two-bit racehorses, and a travelling Wild West show featuring stagecoach robberies, chariot races, pony express runs and relays. Smith first worked as a caretaker for the show's horses. He lived a nomadic, sometimes squalid life, eating out of horse stall kitchens and eventually going unpaid when the Depression killed business. But during one stretch, he was given some of Irwin's racehorses to train, and he made the most of it: At a little bullring in Cheyenne, his trainees won 29 of 30 races. The next year, during a slump for his stable, Irwin recalled Smith's Cheyenne triumphs and sent him to Seattle to train a string of horses. Smith gave Irwin his greatest season in racing.&nbsp;</p>
<p>In his meandering path along the American frontier, Smith had cultivated a wordless, near- mystical communion with horses. He knew their minds and how to sway them, he knew their bodies and how they telegraphed every emotion and sensation, and his quiet hands were a tonic for their ailments. His rustic methods and obsessive devotion to his job often struck other trainers as peculiar; he slept in rooms above his horses' stalls, and stood quietly by them for hours, just studying them. He carried a stopwatch--"one of those newfangled timepieces"--but never used it; he had an uncanny ability to judge a horse's pace by sight, and he resented any distraction which might make him miss a nuance of motion. He lived by a single maxim: "Learn your horse."&nbsp;</p>
<p>But success was fleeting. Irwin died in 1934, and his barn was dissolved. Tom Smith was nearly 60 years old, flat-broke and unemployed at the depths of the Depression. He wandered around the western racing circuit, mucking stalls and grooming horses. One of his charges was a horse named Oriley, who toiled in bottom-level selling events called claiming races. When the horse's owner retired, Smith inherited Oriley. He rebuilt the horse and soon had him winning low-grade races. But Smith was in trouble, barely able to survive off of the meager earnings of a one-claimer stable and living in a horse stall. His deliverance came via an accident of proximity: Oriley bedded down near the horses of a wealthy owner named George Giannini. Giannini saw Smith's progress with Oriley and introduced the old cowboy to his friend Charles Howard.&nbsp;</p>
<p>Howard had the means to hire the nation's most famous trainers, and Smith was nowhere near their class. The two men stood in different parts of the century, embodying two currents of American life in their era. Smith, a true frontiersmen, won over his slow, hard days with calloused, capable hands; Howard, a forward-thinking mass marketer, was paving Smith's old West under the urgent wheels of his automobiles. But Howard had already shown his ability to recognize potential in unlikely packages, and he had a cavalryman's eye for horsemen. Smith got the job.&nbsp;</p>
<p>On June 29, 1936, Tom Smith stood by the track rail at Massachusetts' Suffolk Downs, weighing the angles and gestures of low-level horses as they streamed to the post. Midway through the post parade, a weedy three year-old bay halted before the trainer and looked at him with a regal expression completely unsuited to such a rough-hewn animal. The two looked at each other for only a moment before the pony boy tugged the colt on his way.&nbsp;</p>
<p>Smith flipped to the horse's profile in the track program. The colt was descended from the mighty Man o' War through his sire, Hard Tack, a majestic animal who had squandered his talent in racetrack rages. But this creature's stunted build did nothing to recall the beauty and breadth of his forebears. His body was a study in unsound construction. His short legs, sporting asymmetrical knees that didn't quite straighten all the way, gave him a crouching stance and an odd, inefficient "eggbeater" gait that one writer likened to a duck waddle. His career had been noteworthy only in its appalling rigor. He had raced a staggering 35 times as a two year-old, at least three times the typical workload. He had found no takers in claiming races, running for yard sale pricetags. His owner even tried to hawk him to her brother as a polo pony, but the man took one look at those legs and passed. So the colt labored on, and by the time Smith saw him, his punishing schedule had left him with permanent foreleg ailments and a manner that one jockey described as "mean, restive and ragged."&nbsp;</p>
<p>But somehow, that afternoon, the colt won his race. While being unsaddled, he again focused his gaze on Smith. A man for whom words were encumbrances, Smith didn't take note of the name, but he learned the horse nevertheless. He was a colt whose quality, an admirer would write, "was mostly in his heart, and Tom Smith had been the first to recognize it." Smith spoke to the colt as he was led away: "I'll see you again."&nbsp;</p>
<p>In a private box above Saratoga Racecourse one month later, Charles and Marcela Howard surveyed a field of generic claimers. Charles pointed to an especially wretched colt named Seabiscuit and asked his wife what she thought of him. She offered a wager of a cool drink that the horse would lose, watched as the colt led from wire to wire, and bought her husband a lemonade. Sitting together in the clubhouse that afternoon, husband and wife felt a pull of intuition. Howard shared it with Smith, who walked to the stables to see the horse and found himself face to face with the colt he had seen at Suffolk Downs. Howard wrote a check for $7,500 and bought the colt.&nbsp;</p>
<p>Tom Smith inherited a sore, weary animal. He was two hundred pounds underweight and so nervous that he paced in his stall, lathered up upon being saddled and refused to eat. On the track, he displayed blistering speed but sulked when urged, bolted when checked, zigzagged and raised holy hell in the starting gate. In the barn, he terrorized the grooms. On the train to Detroit, Howard's next destination, Seabiscuit panicked so badly that sweat streamed from his belly.&nbsp;</p>
<p>Smith drew upon sixty years of frontier remedies to rehabilitate his charge. He doctored his body with homemade liniment, leg braces and knee-high bandages, and focused his mind by fitting him with blinkers that blocked his peripheral vision. To cure him of his obstreperousness, Smith waited for him to bolt, then simply let him go; Seabiscuit sprinted hell-for-leather around and around the track, attempted to leap the infield fence, came back exhausted and never tried it again. To soothe the colt's nerves, Smith showered him with affection, knocked down the wall between two stalls and moved him in with three roommates. One was a stray dog named Pocatell. A second was a spider monkey named Jo Jo. The third was a placid yellow cattleroping horse named Pumpkin, who would travel with Seabiscuit for the rest of his life. Playing with his bunkmates by day and sleeping with Jo Jo in the nook of his neck and Pocatell on his belly by night, Seabiscuit relaxed. Smith's headstrong colt was ready for training. He needed a very strong rider.</p>
<p>The jockey who walked into Tom Smith's barn in the summer of 1936 had learned early the sharp turns that fortune could take. In 1925, at just 15 years of age, Johnny Pollard had abandoned his formal education and his boyhood to ride racehorses. His parents, bankrupted by a flood that had wiped out their business and forced them to barter to support their six children, let him go on the condition that a family friend escort him. Johnny was taken to the little racetracks of Butte, Montana to learn the reinsman's trade. Soon after their arrival, the guardian abandoned Johnny, leaving the boy to fend for himself. Though his five foot seven inch height left him towering over other jockeys, Johnny wangled his way into the local fairs to ride racing Quarter Horses around ovals cut through hay fields. In spite of his talent, he didn't win a race for at least a year. To earn money for food, he moonlighted as a prize fighter, boxing under the name "The Cougar" in bouts at cowtown clubs. He lost, he said, "a lot of 'em."&nbsp;</p>
<p>From Butte, Pollard went to British Columbia and the bullring tracks of Vancouver, where he became an apprentice jockey, or "bug boy," contracted to ride for free for room and a $5 weekly food allowance. Racing in Vancouver in summer and Tijuana in winter, Pollard spent his days aboard the trainer's horses and his nights on a cot in a horse stall. Red, as most now called him, liked to play practical jokes, sometimes went hungry for lack of money, quoted aphorisms from Omar Khayyam and Emerson- -"Old Waldo"-- and became known as a buoyant, witty, brainy kid. He had chosen a grinding profession. In exchange for the exhilaration of mastering 1,200 pound animals at 45 mph, he subjected himself to torturous regimens to make the roughly 110-pound maximum riding weight. For most riders, this involved induced vomiting, laxatives, abusive exercise regimens and sweating rituals, sometimes including immersion in the fermenting track manure pit. Once a journeyman, he earned just $15 per winner, $5 per loser, minus fees for laundry ($.50), valet ($1) and agent (10 percent).&nbsp;</p>
<p>Worse was the sheer peril of his workday. His only protection was a cardboard skullcap; lacking a chin strap, it usually popped off before a rider hit the ground, and was rendered even less useful when jockeys cut out the lining and crown to lower their riding weight. Serious injuries were inevitable, but there was no protocol for handling them. In 1927, when the jockey Earl Graham broke his back, he was left on a saddle table until long after nightfall, when it was convenient for someone to drop him at a hospital. He died ten days later. Finally, like virtually every other jockey, Pollard couldn't afford the sky- high insurance rates his job warranted, and tracks, fearing rider unionizing, blocked jockeys' efforts to create their own insurance. When Tommy Luther, who had originally been slated to ride Graham's fatal mount, tried to create an injured rider's fund, he was banned from riding for a year. The lucky lost their savings and jobs when injured; the unlucky lost their lives. Very early in Pollard's career, a horse kicked debris into the back of his head. The blow cost him the sight in his right eye. Though the blindness greatly compounded his risk, Pollard kept it secret and kept riding.&nbsp;</p>
<p>In the 1930's, Pollard moved his tack to New York. His career foundered. By 1936, he was being booked on fewer and fewer horses, and rarely won. By the time he reached Detroit, he was drifting into the great slipstream in which many promising jockeys are lost, their talents never tried for lack of the skilled trainer, the wise owner, the gifted horse. But in Tom Smith, Pollard met a man who was intimately familiar with his hardscrabble world. Smith had a hunch that the jockey's boxer body and long tenure with troubled bullring horses would suit the explosive, neurotic Seabiscuit. He was right. The lost young man and the edgy little colt took to each other immediately, and Pollard won the job riding the horse he called "Pops."</p>
<p>In Smith and Pollard's care, Seabiscuit was transformed. In the barn, he became an easygoing, disarmingly affectionate glutton, "as gentlemanly a horse," said Smith, "as I ever handled." On the track, he displayed astonishing speed and bulldog tenacity. He had two weaknesses. One was a perpetually iffy left foreleg. The other was an evil sense of humor. He seemed to take sadistic pleasure in harassing and humiliating his rivals, slowing down to taunt them as he passed and pulling up when in front so other horses could draw alongside, then dashing their hopes with a killing burst of speed. But in a fight, he was all business. "Once a horse gives Seabiscuit the old look in the eye, he begins to run to parts unknown," said Pollard. "He might loaf sometimes when he's in front and think's he's got the race in the bag. But he gets gamer and gamer, the tougher it gets."&nbsp;</p>
<p>It took two months of minor-league test races and intensive schooling to get the bugs out. In Seabiscuit's first two races, he ran greenly but still claimed a fourth place and a third place. In his third start, he won a minor stakes race, earning back half his purchase price, then won a bigger one two weeks later. Smith sent his newly-polished competitor to New York, where he won the prestigious Scarsdale Handicap in track record time. Then, like his owner 30 years before, Seabiscuit travelled from New York to San Francisco to conquer the West. In Howard's hometown, he racked up electrifying victories in two major races, just missing two world records. He began 1937 at Santa Anita, where he trounced the superb closer Rosemont. Elated with Seabiscuit's success, the Howards dropped Seabiscuit's name in the entry box for the Santa Anita Handicap.&nbsp;</p>
<p>On February 27, 1937, 60,000 fans gathered to see the first of Seabiscuit's three appearances in the Handicap, an event that would come to define him. In a massive field of 18 horses, including favored Rosemont, Seabiscuit was crowded at the start, forcing Pollard to check him. On the backstretch, he began picking off horses in a rush, moving from ninth to fourth in a few yards. As they turned for home, Pollard threaded Seabiscuit through a hole and drove him to a commanding lead. Behind them were 17 of the best horses in the nation. Ahead was nothing but a furlong of red soil.&nbsp;</p>
<p>Pollard and Seabiscuit thought they had it won. The jockey sat absolutely still, his whip idle against his mount's shoulder. Seabiscuit pricked his ears and waited for someone to mock, seeing nothing around his half-moon blinker cups but the vacant track ahead of him. Neither horse nor jockey seemed to notice that toward the grandstand, rallying furiously, was Rosemont, swallowing a foot of Seabiscuit's lead with every stride. At the last moment, Seabiscuit saw his rival and lunged for a photo finish. He was too late.&nbsp;</p>
<p>Though Seabiscuit had lost, he was rapidly becoming a phenomenal celebrity. Two factors converged to create and nourish this intense popularity. The first was Charles Howard. A born ad man, Howard courted the nation on behalf of his horse much as he had hawked his first Buicks, carefully matching Seabiscuit's image to the public pulse and undertaking exhaustive promotion that presaged the modern marketing of athletes. Crafting daring, unprecedented coast-to-coast racing campaigns, he would ship Seabiscuit over 50,000 railroad miles to showcase his talent at 18 tracks in seven states and Mexico. The second factor was timing. The nation was sliding from unrelenting economic ruin into the whirling eddy of Europe's cataclysm. Seabiscuit, Howard, Pollard and Smith, whose fortunes swung in epic, poetic parabolas, would resonate in any age. But in these cruel years, the peculiar union between the four transcended the racetrack.</p>
<p>The result was stupendous popularity. In one year, Seabiscuit garnered more newspaper column inches than Roosevelt, Hitler, Mussolini or any other public figure. Every detail of his life was good copy; Life even ran a pictorial on his facial expressions. Cities had to route special trains to accommodate the invariably record-shattering crowds that came to see him run. He was swarmed by adorers at even the most remote of whistlestops, and requests for posing sessions came so incessantly that Smith, fearing Seabiscuit wouldn't get any rest, hoodwinked the press by trotting out a look-alike. Such fame fueled the immediate, immense success of Howard's Santa Anita and California's new racing industry, today a $4 billion business.&nbsp;</p>
<p>Equipped with a chastened Pollard and blinker eyecups with rearview peepholes cut in them, Seabiscuit embarked on a spectacular tear through the elite races of California, New York, Rhode Island, Massachusetts and Maryland, winning ten major events, eclipsing five track records and bankrolling 1937's highest earnings. But he was not named Horse of the Year. The emergence of greatness in any discipline is a rare event, but by fantastic luck, Seabiscuit had been born just a year before an eastern horse named War Admiral, an exquisite, near-black colt whose talents and achievements were comparable to his own. Though he had never met Seabiscuit, on the strength of his unbeaten season, during which he became just the fourth horse in history to sweep the Triple Crown, War Admiral was voted Horse of the Year. The ballot did not settle the issue. The nation was rapidly dividing into fervid Seabiscuit and War Admiral camps, the dispute taking on an East vs. West flavor. One of the century's most famous sports rivalries was born.&nbsp;</p>
<p>Seabiscuit's second try at the Santa Anita Handicap was just weeks away when Red Pollard, riding Howard's mare, Fair Knightess, in a race at Santa Anita, was caught in a pileup after a horse ahead of him stumbled. Badly hurt, he lingered near death, then stabilized. He was told he would not ride again for at least a year.&nbsp;</p>
<p>With Seabiscuit riderless for Santa Anita Handicap, Pollard suggested that Smith hire George Woolf, Pollard's best friend since their bug boy days. The son of a bronc buster and a circus rider, Woolf was a handsome, independent, utterly fearless young man. He also may have been the single greatest talent his sport ever saw. He could, horsemen marveled, "hold an elephant away from a peanut until time to feed," timing his mounts' rallies so precisely that he regularly won races with breathtaking, last- second dives. He knew his horses and everyone else's, and blew contests wide open by ruthlessly exploiting his rivals' weaknesses. Pollard, famously, took credit for giving Woolf his nickname, "The Iceman," explaining it thus. "In all the smoking car stories I have ever heard, icemen and travelling salesmen were very immoral characters. George does not have a pleasing enough personality to be a travelling salesman." The Iceman had only one hint of athletic mortality: Diabetes. He made a dangerous habit of juggling insulin dependence and drastic reducing, running a high risk of fainting in the saddle.</p>
<p>Woolf got the job and did his homework, learning every contour of Seabiscuit's personality from the hospitalized Pollard. He knew the horse to beat was Stagehand. The Santa Anita Handicap, like every race Seabiscuit ran, was a handicap, in which better horses carry higher weights to increase longshots' chances of winning. As usual, Seabiscuit was assigned by far the highest weight, this time a backbreaking 130 pounds, an impost that had proven too much for most of history's greatest horses. Stagehand, fresh off four straight victories, was assigned just 100 pounds, a preposterously light load. As every two to three pounds is believed to slow a horse by one body length at the distances Seabiscuit ran, 30 pounds was a massive concession, and Woolf knew it. Stagehand's silks and those of another competitor, his brother Sceneshifter, were identical, but for a single difference, a white cap on Stagehand and a colored cap on Sceneshifter. Woolf noted the difference and suited up to run down Stagehand as an ashen, anxious Pollard left his hospital bed to sit with Marcela Howard on the grandstand roof.&nbsp;</p>
<p>As Seabiscuit broke from the gate, he was broadsided by another horse, knocking him nearly to the ground and vaulting Woolf up onto his neck. By the time the jockey shinnied back into place, he and his mount were trapped in a pack of stragglers. On the backstretch, a hole between horses opened before them. Seeing the white cap bobbing ahead and fearing it would be his only chance to break loose, Woolf drove Seabiscuit through the gap and into a premature, open-throttle drive with six furlongs still to be run. In the next half mile, in which he swept past the entire field, Seabiscuit was clocked at 44-1/5 seconds, two seconds--the equivalent of some fourteen lengths--faster than the world record. He ran up alongside the white cap, and the horse beneath it faltered, exhausted, and dropped back. Seabiscuit hit the stretch in front and backed off to wait for challengers.&nbsp;</p>
<p>On the far outside, a closer broke clear of the pack and drove toward Seabiscuit as Rosemont had done a year before. Woolf glanced back. It was Stagehand. After a moment's confusion, he had a terrible realization: The caps of Stagehand and Sceneshifter had been switched, and Woolf had spent Seabiscuit's rally too early, in pursuit of the wrong horse. Riding furiously, Woolf asked Seabiscuit for still more speed. Stagehand drew even, and incredibly, Seabiscuit accelerated to match him. After a ferocious, head-bobbing duel, the pair tripped the win photo together. Stagehand had outbobbed Seabiscuit by two and a half inches. Atop the grandstand, Marcela and Red wept.&nbsp;</p>
<p>On the same afternoon, beneath the drowsing palms of Florida's Hialeah Racecourse, War Admiral cantered to his 10th consecutive win. The desire for a Seabiscuit-War Admiral match had become an international obsession. When New York's Belmont Park offered a dazzling $100,000 for a May match, Howard accepted the challenge and shipped Seabiscuit east, but a flare-up in Seabiscuit's bad leg forced a cancellation. In June, both horses were entered in the Massachusetts Handicap, and 70,000 fans packed into Suffolk Downs to see it. But minutes before post time, Smith unwound Seabiscuit's leg wraps and discovered that the horse had reinjured the leg. Seabiscuit was scratched again, but after convalescing, returned to California and carried 133 pounds to win the Hollywood Gold Cup in record- smashing style, then nipped Bing Crosby's Ligaroti in a raucous match race. Howard, hoping to meet War Admiral, brought Seabiscuit back east.&nbsp;</p>
<p>If the match race was going to occur, Red believed he would see it from Pops' back. By the summer of 1938, his body had healed, and he joined Seabiscuit in Massachusetts. One morning, fresh off of Pops and in jubilant spirits, he offered to ride a colt for another trainer. The colt rammed Pollard through the track rail and into the side of a barn, nearly severing the jockey's leg. Howard flew in a team of prominent doctors. They told Red he might never walk again.&nbsp;</p>
<p>What the famed sportswriter Grantland Rice would call the greatest horse race he ever saw was conceived in the fall of 1938 by Alfred Vanderbilt, the 26 year-old president of Baltimore's Pimlico Racecourse. Vanderbilt wanted to host a Seabiscuit-War Admiral match, but he was playing with a weak hand; his track could offer only a tiny fraction of the purse Belmont had put up. War Admiral's choleric owner, Samuel Riddle, erected his own obstacles, declaring that he would not run his colt from a conventional gate, preferring instead an antiquated, gateless "walk-up" start. But Vanderbilt was a master diplomat. Sixty years later, he recalls forging a deal by appealing to Riddle and Howard's one shared attribute, sportsmanship. "I told them that this was just a little track and we couldn't put up a lot of money, but that it would be a good thing for racing, which they both liked," Vanderbilt remembers. "It took a little doing."&nbsp;</p>
<p>Ultimately, Howard bowed to Riddle's demands, and Vanderbilt rushed to Manhattan's Penn Station, intercepted Riddle between trains, and refused to let him board until he signed the contract. Riddle gave in. The mile and three-sixteenths Pimlico Special was set for November 1, 1938 for a winner-take-all purse of $15,000. Each owner would put up $5,000, and each horse would carry 120 pounds and break from a walk-up start.&nbsp;</p>
<p>Though Seabiscuit was the sentimental choice, War Admiral was the overwhelming betting favorite, and he deserved to be. The most decisive weapon of match races is early speed, and in this department, the Triple Crown winner had a critical edge. While Seabiscuit liked to stalk pacesetters, War Admiral was a half-ton catapult, and he had drawn the favorable inside berth. That Seabiscuit could outbreak War Admiral was inconceivable, and most experts predicted that the race would be over the instant Riddle's colt rocketed off the line.&nbsp;</p>
<p>Tom Smith had other ideas. "I'll give them birds the biggest surprise they ever had in their lives," he told a friend. "I'm going to send Seabiscuit right out on the lead." He began by fashioning a starting bell from an alarm clock and telephone batteries and encasing it in a homemade redwood box with a button on the outside. He led Woolf and Seabiscuit to the training track. Standing behind Seabiscuit with a buggy whip, he hit the bell just as he tickled Seabiscuit's flanks with the whip and Woolf broke into frantic urging, sending Seabiscuit lunging forward. Woolf brought him back, and the drill was repeated. By the third repetition, Seabiscuit was long gone before Smith could wave the whip. The trainer then pitted the colt against top sprinters, sending them through countless walk-up starts to condition Seabiscuit to pour every amp of speed into the break.&nbsp;</p>
<p>Between workouts, Woolf traveled to the hospital to consult with Pollard. In traction, the redhead was swigging bow-wow wine Yummy had smuggled in and reciting Old Waldo to the nurses; he was trying to woo one of them, a beauty named Agnes, away from a resident doctor. Pollard told Woolf to gun to the lead at all costs, but to prevent Seabiscuit from loafing, let War Admiral catch up. Then, he concluded, "race him into the ground."&nbsp;</p>
<p>On the eve of the match, Woolf walked onto the Pimlico track alone, flashlight in hand. Rain had fallen that week, and Woolf worried that Seabiscuit would flounder on a damp, soft track. "'Biscuit wants to hear his feet rattle," he liked to say. The jockey scoured the track for the driest path, and at the top of the stretch, he found a hardened tractor wheel imprint, circling the course several feet from the inner rail. The path was obscured by harrows, so Woolf walked the track again, memorizing its location. "I knew it," he said, "like an airplane pilot knows a radio beam."&nbsp;</p>
<p>Vanderbilt, concerned that 16,000-seat Pimlico would be overwhelmed by spectators, had scheduled the match for a Tuesday in hopes that fewer people would attend. It was no use. A record crowd of 40,000 wedged into the little track. When the stands overflowed, 10,000 people spilled into the infield, bristling over the steeplechase fences and pressing against the infield rail inches from the horses. The clubhouse was so mobbed that NBC radioman Clem McCarthy couldn't reach his post, and was forced to call the race while perched on the track rail. His voice crackled over the radio waves to millions of listeners, including President Roosevelt, who delayed a press conference to hear the call.&nbsp;</p>
<p>At four o' clock, War Admiral and Seabiscuit stepped onto the track. The elegant War Admiral was a grand favorite, whirling and bobbing. Seabiscuit followed in his customary plodding way, "demurely as a deacon." On his saddlecloth, Marcela had pinned her medal of Saint Christopher, patron saint of travelers. Before a crowd, wrote Rice, "keyed to the highest tension I have ever seen in sport," Woolf worked to fray War Admiral's famously delicate nerves. While the Triple Crown winner waited with growing agitation at the starting line, Woolf put Seabiscuit into a long, lazy warmup, sailing past his rival and answering demands that he bring up his mount with a shrugging reply that he was under orders. After an agonizing delay, he walked Seabiscuit to the line. The starter's arm, flag in hand, went up. The two noses passed over the line together, and the arm came down.&nbsp;</p>
<p>At the sound of the bell, Seabiscuit awoke from his reverie to uncork the greatest burst of speed of his life. To the crowd's utter amazement, War Admiral could not keep up. Woolf drove Seabiscuit to a clear lead, then looked back, laughing, and dropped inside to claim the tractor wheelpath, instantly nullifying War Admiral's post position edge. He cruised into the backstretch on a two-length lead, and Woolf, heeding Pollard's advice, began to reel him in. To his outside, War Admiral started to roll. At the half mile pole, he was in full cry as he swept alongside Seabiscuit, who dug in, cocked an ear toward his rival, and refused to let him pass. For more than half a mile, the two dueled shoulder to shoulder. Then, as 40,000 voices shouted them on, War Admiral pushed his head in front.&nbsp;</p>
<p>Seabiscuit and the Iceman had been waiting for him. Woolf looked at War Admiral, and saw the depth of the colt's effort. "His eye was rolling in its socket as if the horse was in agony," he said later. "I knew we had him right then." He dropped low over the saddle and called into Seabiscuit's ear, asking him for everything he had. Seabiscuit gave it to him, delivering a breathtaking rally that carried him back to the lead. War Admiral's mouth dropped open; he had had enough. Seabiscuit galloped down the lane alone, ears wagging, as hundreds of joyous fans stretched their hands out over the rail to brush his shoulders. He hit the wire four lengths in front in near-world record time, completing what Rice called "one of the greatest competitive efforts I have ever seen." As thousands of frenzied spectators breached the rails and poured onto the track, a laughing Woolf stood in the irons and looked back at War Admiral, gesturing in triumph. After the race, an envelope from Woolf arrived at Pollard's hospital room. Inside was $1,500, half of the jockey's purse.</p>
<p>Seabiscuit was crowned Horse of the Year, but there remained one contest the Howards yearned to see him win: the Santa Anita Handicap. The horse returned to Santa Anita in January, 1939 to prepare for a third try at the race. But as Seabiscuit made his move for the lead in his prep race, Woolf heard a sharp crack, and the horse began to lurch. Woolf bailed out and dragged him to a halt. Seabiscuit's long-ailing left front tendon had ruptured at last, and his career was surely over. He was in his stall when his stablemate, Kayak II, won the Santa Anita Handicap. Marcela felt hollow.&nbsp;</p>
<p>For nine months, Seabiscuit stalked the fences at Howard's Ridgewood Ranch, fat and stir-crazy, trying to race deer who wandered nearby. Pollard, after several leg operations, left the hospital on crutches. He was now engaged to Nurse Agnes, but he was so frail that she was certain he was dying. A friend likened his leg to a charred broomstick. Medical bills had bankrupted him, and he had nowhere to go, so the Howards took him in at Ridgewood. There the invalid horse and jockey commiserated. Once Seabiscuit's lameness was gone, Pollard and Howard began cinching the horse into a stock saddle each morning. Red was too weak to hold the horse, so Howard lifted him into the saddle, swung aboard a lead pony and led the two around the meadows, gradually increasing the length and speed of each outing. "Our wheels went wrong together, but we were good for each other," said Pollard. "Out there among the hooting owls, we both got sound again."&nbsp;</p>
<p>By year's end, Seabiscuit was dollar sound, and Smith and the Howards decided to give him one more shot at the Santa Anita Handicap. Pollard's leg was so brittle that he needed a steel brace to prevent it from snapping, and he was under strict orders never to ride again. "One little tap," Pollard said. "Just one." But he couldn't bear to watch Pops race without him. He also had a family to think of; he had married Nurse Agnes, she was expecting a child, and he was dead broke. Howard reluctantly gave him permission to ride. The comeback, if successful, would be utterly unprecedented; no horse had ever returned to top form after such a serious injury and lengthy layoff. In addition, Seabiscuit was seven years old now, more than twice the age of some of his rivals. But Howard, Smith and Pollard thought he could do it. Seabiscuit and Pollard set out for Santa Anita to chase the one dream that had eluded them. "Old Pops and I have got four good legs between us," said Pollard. "Maybe that's enough."&nbsp;</p>
<p>As Seabiscuit and Pollard stepped onto the track for the 1940 Santa Anita Handicap, the record crowd of 78,000 delivered two emotional standing ovations. Howard watched his old warrior go from the paddock, his hands shaking so badly he couldn't light his cigarette. Marcela hid in the quiet of the barn. "I'd seen Johnny's leg," she explained. "I just couldn't watch it." At the last moment, she changed her mind and ran toward the track.&nbsp;</p>
<p>Seabiscuit broke well and settled into perfect striking position around the first turn and down the backstretch. As they leaned into the final turn, Pollard had dead aim on the leaders and an armful of horse beneath him. But a horse named Wedding Call suddenly shouldered Seabiscuit into a pocket, leaving Pollard standing half-upright to hold back his mount, straining his bad leg to the limit. There was no way out. Thinking that bad luck would cost his horse victory a fourth time, Pollard prayed aloud. A moment later, Wedding Call drifted out and Pollard hung on as Seabiscuit burst into the lead.&nbsp;</p>
<p>In the center of the track, a closer began to roll into Seabiscuit's lead like a ghost from his past. This time, it was Kayak II, his stablemate and the defending champion. For the last time, Seabiscuit eased up to tease a rival. Then, in one monumental effort, he swept away from Kayak to win the Santa Anita Handicap. He had run the second-fastest mile and a quarter in American racing history.&nbsp;</p>
<p>Across the track, Marcela Howard stood atop a water wagon. She had scrambled aboard just in time to see her horse realize her dream.&nbsp;</p>
<p>"Little horse, what next?" wrote a sportswriter after the race. In six years, Seabiscuit had won 33 races, set 16 track records and equalled another. He was literally worth his weight in gold, having earned a world record $437,730, nearly 60 times his purchase price. The Howards brought their horse home for good. The partnership was over.&nbsp;</p>
<p>On a January day six years later, George Woolf slid into the Santa Anita starting gate for a weekday race. At 36, he was feeling ill, and was ready to put an end to one of the greatest riding careers in history. But over Santa Anita's red soil that afternoon, something happened. Some witnesses thought his horse stumbled. But most said they saw Woolf sink from the saddle, unconscious, his dieting and diabetes finally taking their toll. The Iceman struck the track head first. He never woke up. At the funeral, Red sobbed as he said goodbye to his friend of 20 years. "I wonder, who has Woolf's book?" he said later. "St. Peter, or some other bird?"&nbsp;</p>
<p>Tom Smith parted amicably with Howard and joined Maine Chance Farm, where he became the nation's leading trainer. But in 1945, a steward caught a groom using a decongestant spray on Smith's horse before a race. Though the spray wasn't performance-enhancing, the horse tested negative and Smith likely didn't know the drug was being given, the trainer was held liable for his groom's actions and suspended for a year. In his 70 years, Smith had never known a life apart from horses. He spent his days sitting alone outside Santa Anita, watching his sport go on without him. Reinstated, he trained numerous top horses, including Kentucky Derby winner Jet Pilot, but he was omitted from the sport's Hall of Fame for almost half a century after his death in 1957.&nbsp;</p>
<p>Red Pollard retired after the Santa Anita Handicap and began training horses, but soon resumed riding. In 1942, he retired again to join the war effort, but his body had taken such a beating that all three services rejected him. He wound up back in the bush leagues, booting horses around Rhode Island's declining Narragansett Park, soon to meet the bulldozer, where he and Pops once raced. He continued to endure bonecrushing falls, but kept riding, struggling to get by. "Maybe I should have heeded the rumble of that distant drum when I was riding high," he once said, quoting Omar. "But I never did. Trouble is, you never hear it if you are a racetracker. Horses make too damned much noise." In 1955, his career petered out. For a while, he worked at backwater tracks, cleaning the boots of other riders. By age seventy, his debilitating, ceaselessly painful riding injuries landed him in a nursing home built over the ruins of Narragansett Park. There, for reasons no one ever knew, the eloquent reinsman simply stopped talking. He lived out the rest of his days in silence, and he died in 1981. No cause of death was found. It was as if, says his daughter, Norah Christianson, "he had just worn out his body."&nbsp;</p>
<p>As Seabiscuit settled into Ridgewood, drowsing under an oak tree, happily herding cattle around a pasture and greeting the 50,000 fans who eventually came to see him, Howard faded. When his heart became too frail for him to endure the tension of seeing his horses run, he came to the track anyway, sitting in the parking lot and listening to race calls on the radio of his Buick. On pleasant days, he would throw a saddle over Seabiscuit, and together they'd canter into the hills and lose themselves in the redwoods.&nbsp;</p>
<p>On the morning of May 17, 1947, Marcela met her husband at breakfast and told him his rough little horse was gone, dead of a heart attack at only 14 years of age. The sometime bicycle repairman, whose own heart would fail him just three years later, had the body carried to a secret site on the ranch. After Seabiscuit had been buried, Howard planted an oak sapling on the spot, telling only his sons the location. The memory of what tree entwines its roots with the bones of Howard's beloved Seabiscuit died with them.&nbsp;</p>
<p>Editor's Note: Ms. Hillenbrand's article on Seabiscuit first appeared in The Backstretch magazine and won Ms. Hillenbrand the 1998 Eclipse Award for magazine writing. Ms. Hillenbrand has since written a full-length novel on Seabiscuit which was&nbsp;published by Random House and&nbsp;appeared on bookstore shelves in early 2001. We urge you to visit Ms. Hillenbrand's Seabiscuit website for details about the&nbsp;book, a major motion picture based on the book, and for more about the great racehorse himself.<strong>&nbsp;</strong></p>
<p><strong>Born:</strong> 1933<br /><strong>Died:</strong> 1947<br /><strong>Trainer:</strong> Tom Smith<br /><strong>Owner:</strong> Charles Howard<br /><strong>Jockey:</strong> Red Pollard, George Woolf<br /><strong>Breeder:</strong> Mrs. H.C. Phipps, Wheatley Stable</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Pedigree:</strong>&nbsp;&nbsp;</p>

<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td rowspan="8" width="19%">Seabiscuit, b.c.<br />foaled 1933</td>
<td rowspan="4" width="24%">Hard Tack, 1926</td>
<td rowspan="2" width="25%">Man o' War, 1917</td>
<td width="32%">Fair Play, 1905</td>
</tr>
<tr>
<td width="32%">Mahubah, 1910</td>
</tr>
<tr>
<td rowspan="2" width="25%">Tea Biscuit, 1912</td>
<td width="32%">Rock Sand (GB), 1900</td>
</tr>
<tr>
<td width="32%">Tea's Over , 1893</td>
</tr>
<tr>
<td rowspan="4" width="24%">Swing On, 1926</td>
<td rowspan="2" width="25%">Whisk Broom, 1907</td>
<td width="32%">Broomstick, 1901</td>
</tr>
<tr>
<td width="32%">Audience, 1901</td>
</tr>
<tr>
<td rowspan="2" width="25%">Balance, 1919</td>
<td width="32%">Rabelais (GB), 1900</td>
</tr>
<tr>
<td width="32%">Balancoire (Fr), 1911</td>
</tr>
</tbody>
</table></div>

<p><span style="text-decoration: underline;"><strong>Racing Record:</strong></span>&nbsp;<strong><br /></strong></p>
<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="374">
<tbody>
<tr>
<th>Year</th>
<th>Age</th>
<th>Starts</th>
<th>1st</th>
<th>2nd</th>
<th>3rd</th>
<th>Earnings</th>
</tr>
<tr>
<td>1935</td>
<td>2</td>
<td align="center">35</td>
<td>5</td>
<td>7</td>
<td>5</td>
<td align="right">$12,510</td>
</tr>
<tr>
<td>1936</td>
<td>3</td>
<td align="center">23</td>
<td>9</td>
<td>1</td>
<td>5</td>
<td align="right">28,995</td>
</tr>
<tr>
<td>1937</td>
<td>4</td>
<td align="center">15</td>
<td>11</td>
<td>2</td>
<td>1</td>
<td align="right">168,580</td>
</tr>
<tr>
<td>1937</td>
<td>5</td>
<td align="center">11</td>
<td>6</td>
<td>4</td>
<td>1</td>
<td align="right">130,395</td>
</tr>
<tr>
<td>1937</td>
<td>6</td>
<td align="center">1</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td align="right">400</td>
</tr>
<tr>
<td>1937</td>
<td>7</td>
<td align="center">4</td>
<td>2</td>
<td>0</td>
<td>1</td>
<td align="right">96,850</td>
</tr>
<tr>
<td>Total</td>
<td>&nbsp;</td>
<td align="center">89</td>
<td>33</td>
<td>15</td>
<td>13</td>
<td align="right">$437,730</td>
</tr>
</tbody>
</table></div>

<p><span><strong>Career Highlights:</strong></span>&nbsp;<strong></strong></p>
<p></p>
<p>at 4:</p>
<ul>
<li>Champion Handicap Male </li>
<li>1st - San Juan Capistrano H </li>
<li>1st - Massachusetts H </li>
<li>1st - Brooklyn H </li>
<li>1st - Yonkers H </li>
<li>1st - Bay Meadows H </li>
<li>1st - Riggs H </li>
<li>1st - Butler H</li>
</ul>
<p>at 5:</p>
<ul>
<li>Horse of the Year </li>
<li>1st - Pimlico Special (match race with War Admiral) (set NTR) </li>
<li>1st - Hollywood Gold Cup </li>
<li>1st - Bay Meadows H</li>
</ul>
<p>at 7:</p>
<ul>
<li>1st - Santa Anita H</li>
</ul>

              
            
            
                      
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    