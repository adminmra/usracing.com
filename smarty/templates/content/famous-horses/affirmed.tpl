{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Affirmed</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<img style="float: left; margin:0 20px 10px 0;" src="/themes/images/famoushorses/affirmed.jpg" class="img-responsive" />

<p><strong>The Crown</strong></p>
<p>"And Affirmed puts his nose in front... It's Affirmed!" With those words track announcer Chick Anderson hailed the 11th American Triple Crown Winner as the gallant colt reached down for all his reserves, to out finish his rival, Calumet Farm's Alydar. Finally, this hearty competitor would receive the accolades and rewards of greatness that he so richly deserved. Until that shining moment, there were still more than a few people who held the belief that Alydar was the better colt. But the smaller colt had triumphed and the doubters retreated.</p>
<p><strong>The Beginnings</strong></p>
<p>It had been a long haul for the Florida-bred Affirmed from his humble beginnings. Owners Louis and Patrice Wolfson may have hoped for the best when they bred their Crafty Admiral mare, Won't Tell You, to the well-bred but modestly accomplished Exclusive Native, but realistically the mating looked like it would produce just another decent horse to wear the flamingo, black and white colors of their Harbor View Farm. This colt named Affirmed would debut in May of his 2-year-old year as he drew away from his rivals winning off at the generous odds of 13-1. Trainer Laz Barrera who had brought Bold Forbes to prominence in the preceding season appeared to have a colt of some potential. Meanwhile, there were whispers on the backstretch that Calumet had a good one, a well developed, blue-blooded colt named Alydar who was yet to start, but had on a couple of occasions worked a "hole in the wind." The rivalry between Affirmed and Alydar would soon begin. A few weeks after his impressive debut Affirmed was entered in the Youthful Stakes. The connections of the unraced Aldyar threw caution and conservatism to the wind and boldly decided to enter in the same race. The result was Affirmed winning, with Alydar finishing fifth. Shortly thereafter, Alydar dropped back to the maiden ranks and pulverized his competitors. In July, Affirmed and Alydar were to meet for the second time in the Great American Stakes with the stakes winning Affirmed giving his rival 5 lbs. Alydar drew off and beat Affirmed by 3 1⁄2 lengths as Affirmed held second. Score the rivalry 1-1.</p>
<p>Barrera chose to send Affirmed west for the rich prize of the Hollywood Juvenile Stakes. The race split into two divisions and Affirmed breezed in his division winning off by 7 lengths in a crisp 1:09.1. In the east, the focus remained on Alydar who easily handled his competitors in the Tremont Stakes.</p>
<p>Two weeks later Affirmed and Alydar met once again in the Futurity Stakes with the results identical to the Hopeful. Alydar again got on terms with his rival but could not go by. Once again, the smaller Affirmed showed his heart, and won by inches. Next they met in a muddy Champagne and Alydar won it by a head, running out in the middle of the track and Affirmed near the rail. Trainer Barrera reasoned that Affirmed could not see his rival well. Affirmed 3 - Alydar 2. The Laurel Futurity was to mark their last meeting of the year. The eastern crowd fully embraced their popular Alydar at 2-5 and made Affirmed second choice at 7-5. But again, it was Affirmed outgaming his rival, winning by a neck. Final Score for the year, Affirmed 4-Alydar 2. On the basis of his superior record over his rival Affirmed was given the year end 2 year old honors as best colt. The rivalry was to be continued.</p>
<p><strong>The Road to the Crown</strong></p>
<p>All was well in both camps as the three-year-old season began. Trainer Barrerra had brought Affirmed west for a winter campaign, Alydar remained in the east. In 1978, in the west, the rains poured down and Affirmed's training was often restricted. Nevertheless, physically, Affirmed was doing well and had narrowed the gap on rival Alydar. No longer was it like night and day, pony versus horse, Affirmed was developing into a fine looking horse. Described that winter as a "big, small-horse", reflecting his good muscling and development, Affirmed was moving into Alydar's realm. Not his equal, but closer.</p>
<p>As Affirmed and trainer Barrera waited out the rains, Alydar began. In February, winning his Hialeah debut easily while prepping for the Flamingo, Alydar looked every part the champion. Two weeks later he went in the Flamingo and crushed his rivals while getting the nine furlongs in a very fast 1:47.1. Meanwhile Affirmed was yet to make his 3-year-old debut. The pressure was on trainer Barrera and Affirmed to not get too far behind their rival. Finally, Affirmed found a spot. Debuting at 6 1⁄2 furlongs in early March, he cruised to an easy victory against moderate rivals in fast time. Affirmed, now only one step behind, came right back 10 days later in the San Felipe and won nicely, though in moderate time. Two weeks hence, in the first part of April, Alydar shipped south and went in the Florida Derby. Again, he won cleverly by two lengths in 1:47.1. Affirmed, not to be outdone, ran the very next day in the Santa Anita Derby and blitzed his rivals drawing off by eight lengths. Both colts appeared to be on target.</p>
<p>As a final prep for the Kentucky Derby, trainer Veitch chose the Blue Grass Stakes for Alydar and Affirmed stayed west and went in the Hollywood Derby. Both won convincingly. Next, it was on to Churchill Downs to run for the roses.</p>
<p>On a beautiful spring day in Kentucky, with a very large crowd on hand, the rivals entered the gate for the 1978 Kentucky Derby. Not mindful of the previous years results, the large crowd again embraced Alydar, making him the 6-5 favorite, while Affirmed was made the 9-5 second choice. Leaving promptly, Affirmed got his position and tracked the speed horses in front of him. Alydar, not gifted with great dexterity leaving the gate, fell back. As the race began in earnest approaching the quarter pole, jockey Cauthen allowed Affirmed to cruise up to the pacemakers, still well within himself. Meanwhile, Alydar was unwinding from the back. When set down in the lane, Affirmed spurted clear from Believe It to grab the lead, while Alydar came wide into the lane. Affirmed, enjoying all the best of it, finished well while never in danger. Alydar finished strongly along the outside. Again Affirmed has bested his rival. And on to the Preakness.</p>
<p>There had been some talk in the eastern press that Affirmed was again lucky to beat Alydar, but the Pimlico crowd was having none of it. They backed Affirmed strongly at 1-2 and let Alydar go at 9-5. The race ran to form. Affirmed again tracked the speed, always going well. Alydar laid closer. As they rounded into the turn for home, Cauthen sat calmly and still while outside and just in front of the other pace horses as his rival moved up. Turning for home, the crowd roared when Alydar got to Affirmed's throatlatch, expecting Alydar's usual strong finish, but he could gain no more. Affirmed stuck his neck forward, seemingly in great control throughout, set out for the finish line and turned back his arch-rival, winning by a long neck.</p>
<p>He had finally won the respect that he deserved but there was one more race to be won. Some in the press were confident that Alydar would prevail at the 12-furlong distance of the Belmont, and there seemed to be good reason to believe it. Though Alydar was getting beat, he wasn't losing by much and he always seemed very willing in the stretch drive. Trainer Veitch insisted that Affirmed would get no rest in the upcoming Belmont, planning to go after Affirmed right from the start and wear him down. Though he was right, and the race unfolded as trainer Veitch wished, Affirmed had the heart of a lion. Caught and pressed by his bigger, stronger rival, they raced as a team for the last mile. In the stretch, Alydar poked his neck in front while pinning his rival to the rail. Seemingly the Triple Crown had evaded the deserving Affirmed. But in a showing of extraordinary courage, Affirmed again reached down for all he had, and pulled back to his rival. With 100 yards left, with Cauthen whipping vigorously from the left side, Affirmed was back nose to nose with Alydar. Finally, while pushing forward with every vestige of his remaining strength, he got his nose in front and kept it there. The Triple Crown was his.</p>
<p><strong>The Aftermath</strong></p>
<p>Affirmed will always be remembered by the people for his great winning resolve and his victories in the 1978 Triple Crown series. Surely he was one of the greats. But he was to meet his match later on in that year. In an extraordinary matchup, he met the great Seattle Slew, winner of the previous year's Triple Crown, in the 1978 Marlboro Cup. Slew, blessed with an extraordinary turn of speed, got the jump on him out of the gate though Affirmed had broke running. Jockey Cauthen inexplicably took a long hold on Affirmed and let the brilliant Seattle Slew go his first quarter in a very leisurely 24 seconds. For all intents and purposes, at that point the race was over. Seattle Slew simply turned on his speed and finished brilliantly, getting his last 7 furlongs in 1:21.4. Affirmed stayed some 3 lengths behind him throughout and did his best, but he was helpless to match Slew's great speed. Nevertheless, though Seattle Slew finished up the year with gusto, the Eclipse voters opted for Affirmed as the Horse of the Year. A deserving reward for a great champion, but he was perhaps the second best colt in that year.</p>
<p>At four, Affirmed was again a champion and Horse of the Year. Though starting the year with two losses in the Malibu and San Fernando Stakes, he reeled off 7 consecutive wins to conclude the racing year. In June, while campaigning out west, he carried 132 lbs in the Hollywood Gold Cup and gave the high-class Irish colt Sirlad 12 lbs and a beating in a sprightly 1:58.2 for the mile and a quarter. In September, Affirmed shipped east to run in a couple of the prestigious and traditional races for top older horses. He took down the Woodward without trouble and then matched up with the terrific Spectacular Bid in the Jockey Club Gold Cup. At that time, Spectacular Bid was widely regarded as one of racing's greats and was considered unlucky to not have a Triple Crown of his own. In any event, the race lived up to its billing. Affirmed, always in front and racing out from the rail, turned back challenges by Spectacular Bid and maintained a safe lead throughout. In doing so he sealed the year's championship honors and a more exalted place amongst racing's greats.</p>
<p>At stud, Affirmed was a qualified success. Like many of the greats, high expectations are accorded them. He was often maligned for not siring one as good as himself. Nevertheless, he sired champions in many countries, including the very high-class American mare, Flawlessly. Oddly, though Affirmed was a great runner on the dirt, most of his best offspring were grass horses. Starting off his stud career at Spendthrift, Affirmed was later to join his rival Alydar, at Calumet Farms.</p>
<p>If horses could talk, I'd love to hear what those two might have said to each other.</p>

<p><strong>Pedigree</strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td style="padding: 8px;" rowspan="8" width="20%">Affirmed, ch.c.<br />foaled 1975</td>
<td rowspan="4" width="25%">Exclusive Native, 1965</td>
<td rowspan="2" width="24%">Raise A Native, 1961</td>
<td width="31%">Native Dancer</td>
</tr>
<tr>
<td width="31%">Raise You</td>
</tr>
<tr>
<td rowspan="2" width="24%">Exclusive, 1953</td>
<td width="31%">Shut Out</td>
</tr>
<tr>
<td width="31%">Good Example</td>
</tr>
<tr>
<td rowspan="4" width="25%">Won't Tell You, 1962</td>
<td rowspan="2" width="24%">Crafty Admiral, 1948</td>
<td width="31%">Fighting Fox</td>
</tr>
<tr>
<td width="31%">Admiral's Lady</td>
</tr>
<tr>
<td rowspan="2" width="24%">Scarlet Ribbon, 1957</td>
<td width="31%">Volcanic</td>
</tr>
<tr>
<td width="31%">Native Valor</td>
</tr>
</tbody>
</table></div>

<p>(female family # 23)</p>

<hr size="2" noshade="noshade" />
<p>Born: February 21, 1975, Harbor View Farm, Florida.<br />Current Location: Active stallion at Jonabell Farm, Lexington, Kentucky.</p>
<hr size="2" noshade="noshade" />

<p><strong>Racing Record:</strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr bgcolor="#edf6ff" bordercolor="#aad3ff">
<th>
<div>Year</div>
</th>
<th>
<div>Age</div>
</th>
<th>
<div>Starts</div>
</th>
<th>
<div>1st</div>
</th>
<th>
<div>2nd</div>
</th>
<th>
<div>3rd</div>
</th>
<th>
<div>unp.</div>
</th>
<th>
<div>earnings</div>
</th>
</tr>
<tr>
<td style="background-color: #edf6ff;">
<div>1977</td>
<td>2</td>
<td align="center">
<div>9</td>
<td>7</td>
<td>2</td>
<td>0</td>
<td>0</td>
<td align="right">
<div>$ 343,477</td>
</tr>
<tr>
<td>1978</td>
<td>3</td>
<td align="center">
<div>11</td>
<td>8</td>
<td>2</td>
<td>0</td>
<td>1</td>
<td align="right">
<div>901,541</td>
</tr>
<tr>
<td>1979</td>
<td>4</td>
<td align="center">
<div>9</td>
<td>7</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td align="right">
<div>1,148,800</td>
</tr>
<tr>
<td>total</td>
<td>&nbsp;</td>
<td align="center">
<div>29</td>
<td>22</td>
<td>5</td>
<td>1</td>
<td>1</td>
<td align="right">
<div>$2,393,818</td>
</tr>
</tbody>
</table></div>

<p><span style="text-decoration: underline;"><strong>Stakes Record:</strong></span><strong></strong></p>
<p>at 2:</p>
<ul>
<li>won - Hopeful Stakes [G1]</li>
<li>won - Futurity Stakes [G1]</li>
<li>won - Laurel Futurity [G1]</li>
<li>won - Hollywood Juvenile Championship Stakes [G2]</li>
<li>won - Sanford Stakes [G2]</li>
<li>won - Youthful Stakes</li>
<li>2nd - Champagne Stakes [G1]</li>
<li>2nd - Great American Stakes</li>
<li><strong>Champion 2yo Colt</strong>&nbsp;</li>
</ul>
<p>at 3:</p>
<ul>
<li>won - Kentucky Derby [G1]</li>
<li>won - Preakness Stakes [G1]</li>
<li>won - Belmont Stakes [G1]</li>
<li>won - Santa Anita Derby [G1]</li>
<li>won - Hollywood Derby [G1]</li>
<li>won - San Felipe Handicap [G2]</li>
<li>won - Jim Dandy Stakes [G3]</li>
<li>2nd - Marlboro Cup H. [G1]</li>
<li>2nd - Travers Stakes [G1]</li>
<li><strong>Champion 3yo Colt</strong>&nbsp;</li>
<li><strong>Horse Of The Year</strong>&nbsp;</li>
</ul>
<p>at 4</p>
<ul>
<li>won - Hollywood Gold Cup [G1]</li>
<li>won - Jockey Club Gold Cup [G1]</li>
<li>won - Santa Anita Handicap [G1] *NTR 10f 1:59 3/5</li>
<li>won - Californian Stakes [G1]</li>
<li>won - Charles H Strub Stakes [G1]</li>
<li>won - Woodward Stakes [G1]</li>
<li>2nd - San Fernando Stakes [G2]</li>
<li>3rd - Malibu Stakes [G1]</li>
<li><strong>Champion Handicap Horse</strong>&nbsp;</li>
<li><strong>Horse of The Year</strong>&nbsp;</li>
</ul>
             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    