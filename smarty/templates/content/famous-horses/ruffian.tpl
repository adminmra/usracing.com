{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Ruffian</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>Ruffian was bred and owned by Mr &amp; Mrs Stuart Janney for their Locust Hill Farm. She was born at Claiborne Farm in Kentucky from some breeding stock which the Janneys kept there. Her sire, Reviewer, won 9 of his 13 starts in a career plagued with injuries. Ruffian was a product of his first crop. Her dam, Shenanigans had won 3 of her 22 starts, and prior to foaling Ruffian, had already produced the good stakes winner Icecapade.&nbsp; <br /> 
<p>Under the care of trainer Frank Whiteley Jr, her very first start came on May 22 at Belmont. The event was a 5 1/2 furlong maiden purse and Ruffian equalled the track record by running the distance in 1:03 flat. She was immediately moved up to stakes company and won the 5 1/2 furlong Fashion Stakes at Belmont in an identical time. The Janneys knew they had something special. Ruffian then won the Astoria Stakes at Aqueduct (5 1/2 furlongs) in 1:02 4/5.</p>
<p>She appeared to be getting faster with every start. Her first attempt at 6 furlongs was in the Sorority Stakes at Monmouth in late July, and she won that clocking a very fast time of 1:09. This was followed up with a win in the Spinaway Stakes at Saratoga in which she ran the 6 furlongs in an even faster time of 1:08 3/5!&nbsp;</p>
<p>As an undefeated multiple stakes winner of clearly superior skill and lightning fast speed, Ruffian was selected as the champion juvenile filly of 1974.&nbsp;</p>
<p>Ruffian returned to action the following year on April 14 in the 6 furlong Caltha Purse at Aqueduct, which she won by 4 1/2 lengths. From this point onward, she raced at increasingly longer distances, and her winning margins became increasingly more impressive. She won the 7 furlong Comely Stakes by 7 3/4 lengths, then ten days later the 1 mile Acorn Stakes by 8 1/4 lengths. Ruffian stepped up to 9 furlongs for the Mother Goose Stakes and sailed home an easy 14 length winner.&nbsp;</p>
<p>The Acorn and Mother Goose Stakes constitute the first two legs of the American Filly Triple Crown. The 1 1/2 mile Coaching Club American Oaks is the third leg of this series. For the remaining event, Ruffian was held at odds of 1-to-20, the smallest pari-mutuel odds allowed by law in the modern era.&nbsp;</p>
<p>A crowd of almost 31,000 were on hand to see if Ruffian could win the title, and the great filly gave no anxious moments. She went straight to the front, as she always preferred to do, and fended off bids from several persistent pursuers. She did not even appear to be seriously extended during these momentary "challenges". Only one filly, Equal Change, could keep near in the closing stages, but Ruffian was clearly better, and won by 2 3/4 lengths. It was 9 lengths further back to third placed filly Let Me Linger.&nbsp;</p>
<p>At this point, a Belmont match race was being proposed to test the winners of the Triple Crown races - Foolish Pleasure, Master Derby, and Avatar. But Avatar had already returned to California to prepare for the Swaps Stakes, so a change to the proposal was made allowing Ruffian to substitute for Avatar. But Foolish Pleasure's trainer, LeRoy Jolley, objected to the suggestion. He stated that Foolish Pleasure's jockey would be so busy watching Ruffian, that Master Derby would very well steal the race.&nbsp;</p>
<p>So Master Derby's stable was paid $50,000 to withdraw him from the match, making it the first time a track ever paid a "non-appearance" fee to a horse.&nbsp;</p>
<p>A crowd of over 50,000 assembled at Belmont Park on the 6th of July to witness what all felt would be a terrific and well-fought battle between champions. Jacinto Vasquez, who was the regular rider of both of these exceptional racers was placed in the difficult position of selecting between the two, and opted to ride the filly. Braulio Baeza was aboard the Derby winner.&nbsp;</p>
<p>Foolish Pleasure bounded from the gate with his head in front, but Ruffian quickly sprinted up from the inside and stuck her head in front. Despite bearing out and brushing with the colt five times in the opening stages, she increased her margin over Foolish Please to about a half length as the pair approached the turn. The crowd was cheering loudly as the match appeared to be meeting their greatest expectations. But as the pair, still linked side-by-side, approached the mile marker, there was a sound which both jockeys described later as being "like the breaking of a board", and the great match was over.&nbsp;</p>
<p>Ruffian was pulled up; her off-fore sesamoid bones were shattered. Baeza and Foolish Pleasure were suddenly alone, and the jockey eased the Derby winner to a canter to complete the course, as soon as he realized what had happened.&nbsp;</p>
<p>The once-cheering crowd now watched in stricken silence as the ambulance sped toward the filly and veterinarians attended to her. Heroic efforts were made to save Ruffian, although the early prognosis gave her only a 10% chance of survival. A pneumatic cast was applied before she was loaded onto the ambulance and another was applied in the barn area. A team of four vets and an orthopaedic surgeon laboured for a total of 12 hours to accomplish the impossible. During the operation, Ruffian was twice revived after she had stopped breathing. Finally the surgery was done.&nbsp;</p>
<p>However, the worst was yet to come. The anesthesia wore off and the filly awoke, disoriented, confused, and in pain. She thrashed about wildly despite the attempts of several attendants to hold her down. She fractured the new cast and caused even greater damage to the fetlock. Knowing that she could not endure further surgery, the veterinarians put her mercifully to sleep.&nbsp;&nbsp;</p>
<p>It is ironic, and perhaps even more than mere coincidence, that Ruffian's parents would suffer her same fate and would both be dead within two years of the death of their great daughter. Shenanigans was undergoing emergency intestinal surgery, and upon waking from the anesthesia thrashed about, breaking two legs. She was humanely destroyed on May 21, 1977. Only a few days after her death, Reviewer suffered a fractured hind leg in a paddock accident at Claiborne Farm where he stood stud duty. He survived the initial surgery, but 15 days later when the cast was changed, he emerged from the anesthetic and became unmanageable, doing irreparable damage to the injured leg. Reviewer was euthanized on June 21, 1977.&nbsp;</p>
<p>Thus Ruffian and the pair which produced her were taken from us by an eerie and tragic set of circumstances. Although her career spanned only a shade over 13 months, and until that match she had only raced against her own gender, Ruffian is usually included in anyone's list of all-time great runners. She was not only unbeaten until her injury, she was also never headed in any race. She set a new stakes record in each of the eight stakes races which she won. She raced successfully from 5 1/2 furlongs to 1 1/2 miles with an average winning margin of 8 1/3 lengths.</p>
<p>Ruffian is buried near the flagpole at her home track of Belmont Park - the site of her first race where she blazed boldly onto the racing scene, and the site of her final race where a hundred thousand eyes watched her brilliant flame flicker out.<br /><strong>&nbsp;</strong></p>

<p><strong>Born:</strong> April 17, 1972 at Claiborne Farm, Paris KY<br /><strong>Died:</strong> July 7, 1975 (age 3) at a veterinary hospital adjacent to Belmont Park, buried near the flagpole at Belmont</p>

<p><strong>Pedigree:</strong> &nbsp;</p>

<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="93%">
<tbody>
<tr>
<td rowspan="8" width="18%">Ruffian, br.f. <br />foaled 1972</td>
<td rowspan="4" width="27%">Reviewer, 1966</td>
<td rowspan="2" width="22%">Bold Ruler, 1954</td>
<td width="33%">*Nasrullah</td>
</tr>
<tr>
<td width="33%">Miss Disco</td>
</tr>
<tr>
<td rowspan="2" width="22%">Broadway, 1959</td>
<td width="33%">Hasty Road</td>
</tr>
<tr>
<td width="33%">Flitabout</td>
</tr>
<tr>
<td rowspan="4" width="27%">Shenanigans, 1963</td>
<td rowspan="2" width="22%">Native Dancer, 1950</td>
<td width="33%">Polynesian</td>
</tr>
<tr>
<td width="33%">Geisha</td>
</tr>
<tr>
<td rowspan="2" width="22%">Bold Irish, 1948</td>
<td width="33%">Fighting Fox</td>
</tr>
<tr>
<td width="33%">Erin</td>
</tr>
</tbody>
</table></div>
<p>(female family # 8c)</p>

<p><span style="text-decoration: underline;"><strong>Racing Record:</strong></span><strong><br /></strong></p>
<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="399">
<tbody>
<tr>
<th>Year</th>
<th>Age</th>
<th>Starts</th>
<th>1st</th>
<th>2nd</th>
<th>3rd</th>
<th>unp.</th>
<th>earnings</th>
</tr>
<tr>
<td>1974</td>
<td  align="center">2</td>
<td  align="center">5</td>
<td>5</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td height="38" align="right">
<div>$ 134,073</td>
</tr>
<tr>
<td>1975</td>
<td  align="center">3</td>
<td  align="center">6</td>
<td>5</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td height="38" align="right">
<div>179,335</td>
</tr>
<tr>
<td>total</td>
<td>&nbsp;</td>
<td align="center">
<div>11</td>
<td>10</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>$313,408</td>
</tr>
</tbody>
</table></div>


<p><strong><span style="text-decoration: underline;">Stakes Record:</span><br />&nbsp;</strong></p>
<p>at 2:</p>
<ul>
<li>won - Fashion Stakes ................. (Equalled Track Record) </li>
<li>won - Astoria Stakes ................. (Equalled Track Record) </li>
<li>won - Sorority Stakes ................ (New Stakes Record) </li>
<li>won - Spinaway Stakes ................ (New Stakes Record) </li>
<li><strong>Champion 2yo Filly</strong>&nbsp;</li>
</ul>
<p>at 3:</p>
<ul>
<li>won - Comely Stakes .................. (New Stakes Record) </li>
<li>won - Acorn Stakes ................... (New Stakes Record) </li>
<li>won - Mother Goose Stakes ............ (New Stakes Record) </li>
<li>won - Coaching Club American Oaks .... (Equaled Stakes Record) </li>
<li><strong>Champion 3yo Filly</strong>&nbsp;</li>
</ul>

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    