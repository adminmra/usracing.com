{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Seattle Slew</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><img style="float: left; margin:0 20px 10px 0;" src="/images/horses/seattle-slew.jpg" alt="Seattle Slew" class="img-responsive" /></p>
<p>Other colts have gone into the Triple Crown series having not yet lost a race, but only Seattle Slew has emerged undefeated with the Triple Crown title.&nbsp;</p>

<p>Seattle Slew was purchased for the bargain price of $17,500 by Mickey Taylor for his wife Karen. Veterinarian Dr. James Hill had recommended the purchase and he shared in the ownership along with his wife Sally.&nbsp;</p>
<p>He trained in Maryland, and there was nicknamed "Baby Huey" after a clumsy cartoon character. Trainer Billy Turner began him in New York, where he won his first two starts, and was then entered in the Champagne Stakes.&nbsp;</p>
<p>He broke fast, grabbed the lead, and drew off to a nine and 3/4 length victory in time which was the fastest mile ever run by a two year old. After those three starts, he was turned out for the winter, and named champion 2yo colt. Turner started him back in March, in a Hialeah 7 furlong allowance race, which he won by 9 lengths. Then he was entered in the first major race for sophomores - the Flamingo Stakes. As usual, he shot to the front, and by the time he had reached the far turn, he was leading by ten lengths. Jockey Jean Cruget didn't wish to use up the colt in prep races, so he slowed Seattle Slew down and won by 4 lengths. Even so, the time was the third fastest running of the Flamingo Stakes in its history.&nbsp;</p>
<p>He reappeared a month later in the Wood Memorial at Aqueduct. Just as before, he opened a 6 length lead, by Cruget eased him to win by 3 1/4 lengths. To date, every race he had entered had been an easy romp, and understandably, he was a huge favorite to win the Kentucky Derby. The bettors hammered his odds down to 50 cents on the dollar.&nbsp;</p>
<p>Although greatly washed out in the paddock, Seattle Slew kept his wits about him. He would need them for the start. The gates opened and Slew emerged off balance. He swerved to the right, colliding with Get The Axe, and bumped almost sideways. But he kept his feet, and straightened himself out. Starting from behind was something Seattle Slew was not accustomed to. He charged into a wall of horses, comprised of Flag Officer, Bob's Dusty and Affiliate, shouldered his way through, and after a quarter mile found himself in second place behind For The Moment. The pair raced together swiftly through the opening fractions. The first half mile was run in :45 4/5, and at the mile post, Seattle Slew finally assumed command. He was leading by three in the homestretch, but was confident enough to allow the margin to narrow to 1 3/4 lengths at the wire. Run Dusty Run was second, Sanhedrin was third.&nbsp;</p>
<p>The Preakness saw a much better start. He came out clear and unobstructed. Cormorant took the lead, but Seattle Slew pressed in second place. The pair set a blistering pace, running the first mile in 1:34 4/5, the fastest first mile ever run in the race. In the homestretch, Seattle Slew took over, drawing out to 3 lengths, but as before, eased to a 1 1/2 length victory. Iron Constitution and Run Dusty Run followed. The final time of 1:54 2/5 was the second fastest Preakness ever, if one goes by the "official" time and assumes Secretariat's time of 1:55 was correct.&nbsp;</p>
<p>The Belmont Stakes marked the first time Seattle Slew had ever run on a muddy track, but it made little difference to him. He assumed the lead early and maintained it to the wire, winning by an easy 4 length margin. The pair chasing him home were the same two from the Derby - Run Dusty Run and Sanhedrin.&nbsp;</p>
<p>The owners wished for Seattle Slew to run on the west coast, in the Swaps Stakes in early July, but trainer Turner didn't see the move as being in the colt's best interests, and besides, Slew needed a rest. But the owners insisted and so they went. The Swaps Stakes turned into the worst race of Slew's career. He finished in 4th place, beaten over 16 lengths by J.O. Tobin. At that point, Turner was dismissed and new trainer Doug Peterson took over.&nbsp;</p>
<p>However, for a while it looked like Peterson would have no horse to train. Slew developed a life-threatening virus, and his future career looked to be in jeopardy. Slowly, he recovered, and began light training once again. By May of the following year, he was well enough to race. He won a 7 furlong Aqueduct allowance by 8 1/2 lengths on a sloppy surface, but then stayed away from the track for another three months.&nbsp;</p>
<p>In August he returned in a 7 furlong Saratoga allowance, winning by 6 lengths, and was ready to tackle the 9 furlong Paterson Handicap under the lights at The Meadowlands. Taking the lead from the start, he looked like a winner in the stretch, but his stamina had not yet returned from the layoff, and Dr. Patches got past him to win by a neck.&nbsp;</p>
<p>Eleven days later came the Marlboro Cup, also at 9 furlongs. That race would mark the first ever meeting between two Triple Crown winners, since that year's hero Affirmed was in the line up. The result was perfectly appropriate. The elder crown-wearer Seattle Slew won by three lengths over the younger Affirmed. And it was far back to the third place horse, Nasty and Bold.&nbsp;</p>
<p>Two weeks after that, he reappeared in the Woodward Stakes at Belmont, marking the third time within that month that he had raced. But he won easily over Exceller by 4 lengths. Then came the 1 1/2 miles of the Jockey Club Gold Cup. The going was sloppy, and the racing would be fierce. Seattle Slew bounded out to the lead, but Affirmed and Life's Hope went right with him. They blazed suicidal fractions, speed not typically seen in a distance event. Then, Affirmed's saddle slipped loose, and he was forced to ease up from the furious pace. (Affirmed would finish 5th in that race, the only time that champion ever finished off the board.) As Affirmed began to fade, the chase was picked up by Exceller. They turned into the stretch. Exceller had wrested a half length lead over Seattle Slew, but Slew fought back to return to Exceller's head. The pair crossed the line together, but it was Exceller who was judged to be the winner. This stretch drive is still recalled by many to be among the all-time best - right up there with Sunday Silence and Easy Goer's Preakness.&nbsp;</p>
<p>Slew made one more start - the Stuyvesant Handicap at Aqueduct and won without much effort by 3 1/4 lengths, despite carrying 134 lbs. Then he was retired to stud at Spendthrift Farm, where his estimated value had been placed at a record figure of $12,000,000. In time he was moved to Three Chimneys Farm where he has lived ever since, and where he will remain for the rest of his days. Even now, at the age 24, he is still saddled up and jogged at the farm every morning. It is the policy of Three Chimneys Farm to engage in this regimen with each of their stallions, as they believe it keeps them fit, healthy and happy.&nbsp;</p>
<p><strong>Prominent offspring:</strong> Horse of the Year A.P. INDY, champions SLEW O'GOLD, SWALE, CAPOTE, LANDALUCE, and DIGRESSION. Major stakes winners SLEW CITY SLEW, LIFE AT THE TOP, LAKEWAY, TSUNAMI SLEW, ADORED, SLEWPY, TAIKI BLIZZARD, and so many others.&nbsp;</p>
<p><strong>His daughter's best offspring include:</strong> CIGAR, GOLDEN ATTRACTION, HISHI AKEBONO, MULCA, JADE FLUSH, AMERICAN CHANCE, SEEKING THE PEARL, SLEW OF REALITY, and others.&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Born:</strong> Feb. 15, 1974, bred in Kentucky by Ben S. Castleman<br /><strong>Currently:</strong> Active stallion at Three Chimneys Farm, Midway, Kentucky&nbsp;</p>

<p><strong>Pedigree:</strong>&nbsp;</p>

<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td rowspan="8" width="25%">Seattle Slew br.c.<br />born 1974</td>
<td rowspan="4" width="25%">Bold Reasoning, 1968</td>
<td rowspan="2" width="25%">Boldnesian, 1963</td>
<td width="25%">Bold Ruler</td>
</tr>
<tr>
<td>Alanesian</td>
</tr>
<tr>
<td rowspan="2">Reason to Earn, 1963</td>
<td>Hail to Reason</td>
</tr>
<tr>
<td>Sailing Home</td>
</tr>
<tr>
<td rowspan="4">My Charmer, 1969</td>
<td rowspan="2">Poker, 1963</td>
<td>Round Table</td>
</tr>
<tr>
<td>Glamour</td>
</tr>
<tr>
<td rowspan="2">Fair Charmer, 1959</td>
<td>Jet Action</td>
</tr>
<tr>
<td>Myrtle Charm</td>
</tr>
</tbody>
</table></div>
<p>(female family # 13)</p>

<p>&nbsp;<span style="text-decoration: underline;"><strong>Racing Record:</strong></span></p>
<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="394">
<tbody>
<tr>
<th>Year</th>
<th>Age</th>
<th>Starts</th>
<th>1st</th>
<th>2nd</th>
<th>3rd</th>
<th>unp.</th>
<th>earnings</th>
</tr>
<tr>
<td>1976</td>
<td align="center">2</td>
<td align="center">3</td>
<td>3</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td align="right">$ 94,350</td>
</tr>
<tr>
<td>1977</td>
<td align="center">3</td>
<td align="center">7</td>
<td>6</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td height="32" align="right">
<div>641,370</td>
</tr>
<tr>
<td>1978</td>
<td height="33" align="center">
<div>4</td>
<td height="33" align="center">
<div>7</td>
<td>5</td>
<td>2</td>
<td>0</td>
<td>0</td>
<td align="right">473,006</td>
</tr>
<tr>
<td>total</td>
<td>&nbsp;</td>
<td align="center">
<div>17</td>
<td>14</td>
<td>2</td>
<td>0</td>
<td>1</td>
<td align="right">$1,208,726</td>
</tr>
</tbody>
</table></div>


<p><span style="text-decoration: underline;"><strong>&nbsp;</strong></span></p>
<p><span style="text-decoration: underline;"><strong>Stakes Record:</strong></span></p>
<p><strong><span style="text-decoration: underline;">&nbsp;</span><br /></strong>at 2:</p>
<ul>
<li>won - Champagne Stakes </li>
<li><strong>Champion 2yo Colt</strong>&nbsp;</li>
</ul>
<p>at 3:</p>
<ul>
<li>won - Kentucky Derby </li>
<li>won - Preakness Stakes </li>
<li>won - Belmont Stakes </li>
<li>won - Flamingo Stakes </li>
<li>won - Wood Memorial </li>
<li><strong>Champion 3yo Colt</strong> </li>
<li><strong>Horse Of The Year</strong></li>
</ul>
<p>at 4:</p>
<ul>
<li>won - Marlboro Cup Handicap </li>
<li>won - Woodward Stakes (new track record) </li>
<li>won - Stuyvesant Handicap </li>
<li>2nd - Jockey Club Gold Cup </li>
<li>2nd - Paterson Handicap </li>
<li><strong>Champion Handicap Horse</strong>&nbsp;</li>
</ul>


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    