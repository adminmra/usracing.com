{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Cigar</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><img style="float: left; margin:0 20px 10px 0;" src="/themes/images/famoushorses/cigar.jpg" alt="Cigar Race Horse"  /></p>

It took us awhile to warm to Cigar. Holy Bull had been racing's poster boy, and the one time he met up with Cigar, he broke down on the backstretch and had to be retired. As irrational as it is, we sometimes hold these tough losses against the horse who beat our champion.
<p>That race was the February 11, 1995 Donn Handicap. Cigar won easily by 5, but few noticed him pass the wire. We'd later come to adore Cigar as much if not more than we did Holy Bull, and we'd look back on this day as the "passing of the torch" between the two champions.<br />&nbsp;<br />Cigar was useful but hardly Eclipse material at ages 3 and 4. Whether he was a late-bloomer or simply placed on the wrong surface will forever be open to debate.</p>
<p>Stabled in California with trainer, Alex Hassinger, Cigar made an unsuccessful Feb. 21, 1993 career debut at 6 furlongs on the dirt. A few months later he returned to the track, again at 6 furlongs on the dirt, and broke his maiden.</p>
<p>Despite the win, Hassinger apparently didn't like what he saw, and opted to try the colt on the turf. Cigar made his turf debut in a May 23 allowance but failed to finish in the money. He'd do better the next few times out, finishing 3rd, 1st, and 2nd, respectively, in allowance company. He next tried stakes company and again did well, finishing 3rd in the Gr. III Ascot Handicap and 2nd in the Gr. III Volante Handicap. He then attempted Gr. I company for the first time in the Nov. 20 Hollywood Derby but finished off the board.</p>
<p>For his 4-year-old campaign, owner Allen Paulson had Cigar shipped to his east-coast trainer, Bill Mott. Mott gave Cigar the first half of the year off, not returning him to the races until July. After less-than-promising results in two 3rd-place finishes and a pair of off the board efforts in four tries, Mott decided to return him to the dirt.</p>
<p>Cigar was entered in an Aqueduct allowance for Oct. 28, and he easily dismissed the field, winning by 8 lengths. So impressed was Mott that he then entered him in the Gr. I NYRA Mile, going against the hard-knocking multiple-Gr-I winner, Devil His Due. The almost- black Devil would go off the favorite, but was no match for Cigar this day, losing by 7 lengths.</p>
<p>Cigar's 5-year-old campaign was one of perfection - 10 wins in 10 starts. He added 8 Gr. I races to his resume, including the Donn, Gulfstream Park, Oaklawn Park, and Woodward handicaps, the Pimlico Special, the Hollywood Gold Cup, and the Jockey Club Gold Cup. But his biggest win of that year was his last - the Breeders' Cup Classic.</p>
<p>The Oct. 18 Breeders' Cup Classic at Belmont Park set up to be the toughest of Cigar's career thus far. He was coming into the race off a long season, with basically one race a month since January. He'd drawn one of the farthest outside posts, the 10 post. And a dreary, rainy day on Long Island produced a sloppy track, a condition Cigar had not met before and was questionable to like.</p>
<p>Ten other horses went to post with Cigar, including Unaccounted For, Concern, Soul of the Matter, Tinners Way, L'Carriere, and Europe's best in Halling. But the betting public had confidence in the big bay Cigar, sending him off as the 4-5 favorite. Only two others went off in single-digit odds - Unaccounted For at 5 to 1, and Halling at 8 to 1.</p>
<p>Front-running Star Standard shot immediately to the lead, with L'Carriere just behind him at the rail, and Jerry Bailey "with a hard hold on the pent-up power of Cigar, restrained in third," according to race-caller Tom Durkin.</p>
<p>"Cigar makes his move, and he sweeps to the lead with a dramatic rush!... with 3 furlongs to go, and Jerry Bailey turns him loose... and he guides him down to the rail as the field turns for home. Unaccounted For is down inside... a quarter of a mile between Cigar and a perfect season!... Coming down to the last furlong with a two-and-a-half length lead, and Jerry Bailey calls on Cigar for everything he has!... L'Carriere is a weakening second, on the inside, Unaccounted For, on the outside, Soul of the Matter... And here he is!... the incomparable, invincible, unbeatable Cigar!"</p>
<p>Cigar stopped the clock in 1:59.2 for the 1 1/4 miles, establishing a new stakes record. He won his 12th consecutive race in the 12th Breeders' Cup, and did it easily. Hardly any doubters remained after this performance, and Cigar-mania commenced in full force.</p>
<p>At a time when most horses would be retired to stud, owner Allen Paulson delighted all of racing by stating that Cigar would return to racing the following year. The mainstream media picked up on the hoopla surrounding the horse, and article after article appeared comparing Cigar to other past all-time greats. Noted equine artist, Fred Stone, released his newest painting - of Cigar, with the "ghosts" of Man o'War and Secretariat in the background.</p>
<p>Cigar's 6-yr-old campaign started with the Feb. 10 Donn Handicap. Stablemate Wekiva Springs had been coming on strong as of late and figured to be his main competition. The wonderful filly, Heavenly Prize, also received some support. Cigar would win by 2, notching his 13th consecutive win.</p>
<p>The previous year, it had been announced that there would be a new race to take place at Nad Al Sheba, Dubai in 1996, called the Dubai World Cup. To be held in late March, it would offer the richest purse in racing, $5 million dollars, in order to attract the best horses in training in the world. Cigar was invited to participate in the inaugural running, and it was the next stop on his schedule.</p>
<p>All didn't go smoothly, however. Cigar had developed a quarter crack and missed about 4 days of training completely. His hoof patched up, Cigar and connections boarded the plane for the 6,000 mile excursion. But all of racing worried about a less-than- 100% Cigar taking on this field, the most accomplished ever assembled in racing history considering their combined earnings.</p>
<p>From coast to coast, racing fans took to the tracks and simulcast outlets to witness the race. Under the lights of the night-time racing at Nad Al Sheba, Cigar made his patented move and seemed to have put away the field at the top of the stretch as he had always done. But a horse was fast approaching, another American, Soul of the Matter. He pulled up alongside Cigar and even briefly headed him, but Cigar dug down and pulled away to win at the wire. For all racing fans, we could hear in our head the cheer that went up across the country. Cigar was America's best, and now he was the world's best.</p>
<p>Cigar returned to a hero's welcome, and throngs of media rarely seen in racing descended on the Mott barn. Here was a story - racing's greatest racehorse in two decades, on a 14-race win streak. Citation held the North American record for the most consecutive wins of the century at 16. Tying or surpassing Citation's record was in Cigar's grasp.</p>
<p>Cigar was given a couple months off to recuperate from the Dubai race and hopefully heal the quarter crack which still nagged at the horse, making his next start in the June 2 Massachusetts Handicap at Suffolk Downs. Some questioned Mott's choice of this race as it wasn't a graded stakes event, but it did offer a fine purse, and Suffolk Downs was pulling out all the stops to make it a grand day for racing fans. The attendance record was shattered for the track as fans came to witness Cigar in person.</p>
<p>Cigar was assigned 130 pounds in the race, the first time he'd been assigned this much weight. But it was no problem as Cigar dispensed of the field in 1:49 3/5 for the 1 1/8 miles. Number 15 in the bag.</p>
<p>Arlington International sought out Cigar's presence and offered to host the "Arlington Citation Challenge" for Cigar's attempt at a record-tying 16th straight win. Mott accepted the invitation. Also accepting invitations were Dramatic Gold, Unbridled's Song, Honour and Glory, Dr. Banting, Polar Expedition, and the good foreigner, Eltish. Unbridled's Song, still unhealthy with his own quarter crack, didn't put up a fight, but Cigar did meet a stubborn challenge in Dramatic Gold.</p>
<p>"Cigar and Dramatic Gold making runs together... they run to the front as they turn for home," calls track announcer Michael Wrona. "Honour and Glory dropping out to third... Cigar comes to the eighth pole taking over by half a length... Dramatic Gold is running a mighty race on the inside... but Cigar seems to have his measure... He's starting to stretch away now! Cigar is an unstoppable dynamo! The crowd rises to a champion... 16 in succession as Cigar assumes the crown of immortality!"</p>
<p>Cigar would attempt to break the record in the Aug. 10 Pacific Classic at Del Mar. Trainer Richard Mandella entered a duo of racers, the speedy Siphon and the closer Dare and Go. Siphon was a good horse - his wasn't cheap speed. Cigar's jockey, Jerry Bailey, was forced with a decision - to run with Siphon else risk quality speed loose on the lead.</p>
<p>In a decision Bailey would later look on as his mistake, Bailey raced Cigar neck-and-neck with Siphon, putting up fractions that no horse can withstand and be left standing at the end of 1 1/4 miles. Siphon was the first to throw in the towel, but Cigar was clearly spent by their duel. A fresh Dare and Go, coming from far behind, passed the weary Cigar for the victory.</p>
<p>The only soul who appeared happy with the results was Dare and Go's trainer, Richard Mandella. As the television cameras panned the stands, it was a sea of shock, frowns and tears from Cigar's adoring fans. Mott, Paulson, and Paulson's wife, Madeleine, were equally stunned and tearful. Cigar had been beaten -- it seemed so unbelievable.</p>
<p>Cigar returned to Belmont Park where he'd race twice more before heading to defend his crown in the Breeders' Cup Classic, this year to be held at Woodbine racecourse in Canada. These were to be the last three starts of his career, then he'd be retired to stud.</p>
<p>Cigar won the first, the Woodward Stakes, but lost the second, the Jockey Club Gold Cup, by a fast-diminishing neck to the 3-yr-old Skip Away. As Cigar approached his last career start, it was admitted that he'd lost a step and was at his most vulnerable.</p>
<p>A good field showed up to take on Cigar as he went for his second straight Breeders' Cup Classic, a feat which had not been done in the relatively short history of the series. Included were Dare and Go, Atticus, Will's Way, Dramatic Gold, Formal Gold, Alphabet Soup, and a pair of 3-yr-old classic winners, Editor's Note and Louis Quatorze.</p>
<p>Cigar wasn't alone at the top of the stretch as he had been the year before. From off the pace, he made his move but perhaps too late. Making up ground with every stride, Cigar failed to catch the gray Alphabet Soup and Louis Quatorze by the time they hit the wire. Only a half-length or so separated the three in what was Cigar's worst finish since being returned to the dirt. But it was a good effort, an honest effort, and Cigar's fans took solace in that as their champion left the track for the last time.</p>
<p>It was a grand career befitting a grand retirement, and Madeleine Paulson made sure it would be just that. Cigar was vanned to New York City and paraded down the street to Madison Square Garden. Fans of all ages lined the street, gazing in awe at the magnificent racehorse. Once inside Madison Square Garden, Cigar received a standing ovation as he was joined by his jockey, Jerry Bailey, for one last ride. Under the spotlight, Cigar pranced and arched his neck, passing on the torch for another Thoroughbred to someday carry.</p>
<p><strong>Cigar</strong>, Apr 18, 1990, bay, Palace Music - Solar Slew, by Seattle Slew<br /><strong>Owner</strong>: Allen Paulson<br /><strong>Trainer</strong>: Bill Mott (Alex Hassinger trained him at 3, the first 9 starts of Cigar's career)<br /><strong>Breeder</strong>: Allen Pauson (MD)<br /><strong>Jockey</strong>: Jerry Bailey (though four other jockeys, Pat Valenzuela, Chris McCarron, Mike Smith and Julie Krone, rode him in the early part of his career)<br /><strong>Current Status:</strong> Due to infertility after retiring to stud, Cigar now resides at the Kentucky Horse Park.
</p>
<p><strong>Pedigree</strong></p>
<p><strong><br /></strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr>
<td rowspan="7" width="25%">Cigar, bay colt<br />foaled 1990</td>
<td rowspan="2" width="29%">Palace Music, 1981</td>
<td width="46%">The Minstrel, 1974</td>
</tr>
<tr>
<td width="46%">Come My Prince, 1972</td>
</tr>
<tr>
<td rowspan="2" width="29%">Solar Slew, 1982</td>
<td width="46%">Seattle Slew, 1974</td>
</tr>
<tr>
<td width="46%">Gold Sun (Arg), 1974</td>
</tr>
</tbody>
</table></div>



<p><strong>Career Highlights:</strong>&nbsp;&nbsp;</p>
<ul>
<li>1995 Champion Handicap Male</li>
<li>1995 Horse of the Year</li>
<li>1996 Champion Handicap Male</li>
<li>1996 Horse of the Year</li>
<li>1996 Hwt Older Horse in UAE 9.5-11F</li>
<li>Carried 130 pounds to victory in the '96 Massachusetts Handicap.</li>
<li>Won 16 straight races from the Oct. 28, 1994 NYRA Mile through the July 13, 1996 Arlington Citation Challenge. Lost the 17th race, the Aug. 19, 1996 Pacific Classic (Gr. I) after engaging in a speed duel with Siphon, setting up a win by Dare and Go.</li>
<li>Cigar shares his 16 win streak with only one other North American runner in this century, Citation.</li>
<li>Undefeated in 1995, winning 10 of 10 starts, 8 of them Gr. I.</li>
<li>Won the inaugural running of the Dubai World Cup in 1996. Despite lacking a Gr. I status, it attracted the most accomplished (money earned) field in racing history to that point.</li>
<li>Won a total of 11 Gr. Is and the Dubai World Cup.</li>
<li>Retired as the leading North American money earner of all time with $9,999,813, a record which stands to this day.</li>
</ul>

<p><strong>Race Record<br />&nbsp;</strong></p>
<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered" width="100%">
<tbody>
<tr align="right">
<td>Year</td>
<td>Age</td>
<td>Starts</td>
<td>1st</td>
<td>2nd</td>
<td>3rd</td>
<td align="right">
Earnings</td>
</tr>
<tr align="right">
<td>1993</td>
<td>3</td>
<td>9</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>89,175</td>
</tr>
<tr align="right">
<td>1994</td>
<td>4</td>
<td>6</td>
<td>2</td>
<td>0</td>
<td>2</td>
<td>180,838</td>
</tr>
<tr align="right">
<td>1995</td>
<td>5</td>
<td>10</td>
<td>10</td>
<td>0</td>
<td>0</td>
<td>4,819,800</td>
</tr>
<tr align="right">
<td>1996</td>
<td>6</td>
<td>8</td>
<td>5</td>
<td>2</td>
<td>1</td>
<td>4,910,000</td>
</tr>
<tr align="right">
<td>Dirt</td>
<td>&nbsp;</td>
<td>22</td>
<td>18</td>
<td>2</td>
<td>1</td>
<td>&nbsp;</td>
</tr>
<tr align="right">
<td>Turf</td>
<td>&nbsp;</td>
<td>11</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>&nbsp;</td>
</tr>
<tr align="right">
<td>Totals</td>
<td>&nbsp;</td>
<td>33</td>
<td>19</td>
<td>&nbsp;</td>
<td>5</td>
<td>9,999,81</td>
</tr>
</tbody>
</table>
</div>

<p><strong>At 3:</strong>&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>Maiden Special Weight</strong>, 5-9-93, Santa Anita, dirt, 6f, defeating Golden Slewpy and Famous Fan.</li>
<li><strong>Allowance</strong>, 8-18-93, Del Mar, turf, 1 1/16 miles, defeating Our Motion Gran and The Berkeley Man.</li>
</ul>
<p><strong>2nd:</strong>&nbsp;</p>
<ul>
<li><strong>Allowance</strong>, 9-3-93, Del Mar, turf, 1 mile, to Kingdom of Spai, defeating Saturnino.</li>
<li><strong>Volante H</strong> (Gr III), 11-5-93, Santa Anita, turf, 1 1/8 miles, to Eastern Memories, defeating Snake Eyes.</li>
</ul>
<p><strong>3rd:</strong>&nbsp;</p>
<ul>
<li><strong>Allowance</strong>, 6-12-93, Hollywood Park, turf, 1 1/16 miles, to Nonproductiveassets, Tossofthecoin.</li>
<li><strong>Ascot H</strong> (Gr III), 9-25-93, Bay Meadows, turf, 1 1/16 miles, to Siebe, Nonproductiveassets.</li>
</ul>
<p><strong>OTB:</strong>&nbsp;</p>
<ul>
<li><strong>Maiden Special Weight</strong>, 2-21-93, Santa Anita, dirt, 6f, to Demigod, Cardiac, Sir Hutch. (Career debut)</li>
<li><strong>Allowance</strong>, 5-23-93, Hollywood Park, turf, to Pleasedontexplain, Stateley Warrior, Fleet Wizard. (Grass debut)</li>
<li><strong>Hollywood Derby</strong> (Gr I), 11-20-93, Hollywood Park, turf, 1 1/8 miles, to Explosive Red, Jeune Homme, Earl of Barking.</li>
</ul>


<p><strong>At 4:</strong> The streak commences - races numbered in burgandy.&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>1.</strong>&nbsp;<strong>Allowance</strong>, 10-28-94, Aqueduct, dirt, 1 mile, by 8 lengths, defeating Golden Plover, Gulliviegold. (Return to dirt and first race of his 16-race streak)</li>
<li><strong>2.</strong>&nbsp;<strong>NYRA Mile</strong> (Gr I), 11-26-94, Aqueduct, dirt, 1 mile, by 7 lengths, defeating Devil His Due, Punch Line.</li>
</ul>
<p><strong>3rd:</strong>&nbsp;</p>
<ul>
<li><strong>Allowance</strong>, 8-8-94, Saratoga, turf, 1 1/8 miles, to My Mogul, Next Endeavor.</li>
<li><strong>Allowance</strong>, 10-7-94, Belmont, turf, 1 1/16 miles, to Unaccounted For, Same Old Wish.</li>
</ul>
<p><strong>OTB:</strong>&nbsp;</p>
<ul>
<li><strong>Allowance</strong>, 7-8-94, Belmont, turf, 1 1/16 miles, to Dancing Hunter, Compadre, I'm Very Irish.</li>
<li><strong>Allowance</strong>, 9-16-94, Belmont, turf, 1 mile, to Jido, Bermuda Cedar, Limited War.</li>
</ul>


<p><strong>At 5:</strong>&nbsp;&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>3.</strong>&nbsp;<strong>Allowance</strong>, 1-22-95, Gulfstream Park, dirt, 1 1/16 miles, by 2, defeating Upping The Ante, Chasin Gold.</li>
<li><strong>4.</strong>&nbsp;<strong>Donn H</strong> (Gr I), 2-11-95, Gulfstream Park, dirt, 1 1/4 miles, by 5, in 1:49 3/5, defeating Primitive Hall, Bonus Money (also Holy Bull).</li>
<li><strong>5.</strong>&nbsp;<strong>Gulfstream Park H</strong> (Gr I), 3-5-95, Gulfstream Park, dirt, 1 1/4 miles, by 7 1/2, in 2:02 4/5, defeating Pride of Burkaan, Mahogany Hall.</li>
<li><strong>6.</strong>&nbsp;<strong>Oaklawn Park H</strong> (Gr I), 4-15-95, Oaklawn Park, dirt, 1 1/8 miles, by 2 1/2, in 1:47 1/5, defeating Silver Goblin, Concern.</li>
<li><strong>7.</strong>&nbsp;<strong>Pimlico Special</strong> (Gr I), 5-13-95, Pimlico, dirt, 1 3/16 miles, in 1:53 3/5, defeating Devil His Due, Concern.</li>
<li><strong>8.</strong>&nbsp;<strong>Massachusetts H</strong>, 6-3-95, Suffolk Downs, dirt, 1 1/8 miles, in 1:48 3/5, defeating Poor But Honest, Double Calvados.</li>
<li><strong>9.</strong>&nbsp;<strong>Hollywood Gold Cup</strong> (Gr I), 7-2-95, Hollywood Park, dirt, 1 1/4 miles, in 1:59 2/5, by 3 1/2, defeating Tinners Way, Tossofthecoin.</li>
<li><strong>10.</strong>&nbsp;<strong>Woodward H</strong> (Gr I), 9-16-95, Belmont Park, dirt, 1 1/8 miles, in 1:47, defeating Star Standard, Golden Larch.</li>
<li><strong>11.</strong>&nbsp;<strong>Jockey Club Gold Cup</strong> (Gr I), 10-7-95, Belmont Park, dirt, 1 1/4 miles, in 2:01 1/5, by 1, defeating Unaccounted For, Star Standard.</li>
<li><strong>12.</strong>&nbsp;<strong>Breeders' Cup Classic</strong> (Gr I), 10-28-95, Belmont Park, dirt, 1 1/4 miles, in 1:59 2/5 (new stakes record), defeating L'Carriere, Unaccounted For (also Halling).</li>
</ul>

<p><strong>At 6:</strong>&nbsp;&nbsp;</p>

<p><strong>1st:</strong>&nbsp;</p>
<ul>
<li><strong>13.</strong>&nbsp;<strong>Donn Handicap</strong> (Gr I), 2-10-96, Gulfstream Park, dirt, 1 1/8 miles, by 2 in 1:49, defeating Wekiva Springs, Heavenly Prize.</li>
<li><strong>14.</strong>&nbsp;<strong>Dubai World Cup</strong> (inaugural running - not graded), 3-27-96, Nad Al Sheba, United Arab Emirates, dirt, 1 1/4 miles, by 1/2, in 2:03 4/5, defeating Soul of the Matter, L'Carriere.</li>
<li><strong>15.</strong>&nbsp;<strong>Massachusetts H</strong>, 6-2-96, Suffolk Downs, dirt, 1 1/8 miles, carrying 130 pounds, in 1:49 3/5, defeating Personal Merit, Prolanzier.</li>
<li><strong>16.</strong>&nbsp;<strong>Arlington Citation Challenge</strong>, 7-13-96, Arlington International, dirt, 1 1/8 miles, in 1:48 1/5, defeating Dramatic Gold, Eltish (also Unbridled's Song).</li>
<li><strong>Woodward</strong> (Gr I), 9-14-96, Belmont Park, dirt, 1 1/8 miles, by 4, in 1:47, defeating L'Carriere, Golden Larch.</li>
</ul>
<p><strong>2nd:</strong>&nbsp;</p>
<ul>
<li><strong>Pacific Classic</strong> (Gr I), 8-10-96, Del Mar, dirt, 1 1/4 miles, to Dare and Go, defeating Siphon. (This race broke his win streak).</li>
<li><strong>Jockey Club Gold Cup</strong> (Gr I), 10-5-96, Belmont Park, dirt, 1 1/4 miles, to Skip Away, defeating Louis Quatorze.</li>
</ul>
<p><strong>3rd:</strong>&nbsp;</p>
<ul>
<li><strong>Breeders' Cup Classic</strong> (Gr I), 10-26-96, Woodbine, dirt, 1 1/4 miles, to Alphabet Soup, Louis Quatorze.</li>
</ul>





<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    