{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Man O&#039; War</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><img style="float: left; border: 1px solid black;" src="/images/horses/man-o-war.jpg" alt="Man o'War"  /></p>
<div>Man o'War was the second foal of his dam, the first having been a full sister named Masda. Masda was a stakes winner but is probably more important as the third dam of Triple Crown winner, Assault. Man o'War's dam, Mahubah was sometimes referred to as "Fair Play's wife" as she produced foals only to that stallion.&nbsp;

<p>Man o'War was born a few minutes before midnight, on March 29, and quickly grew into a big horse of great power. His chestnut coat and imposing presence earned him the nickname "Big Red."&nbsp;</p>
<p>When his trainer, Louis Reustel, first laid eyes on him as a yearling, he described him as "very tall, gangly and thin. So leggy as to give the same impression one gets when seeing a week-old foal." Perhaps not the powerhouse as a yearling that he would be later in life, he sold at Saratoga's yearling sale in August 1918 for $5000.&nbsp;</p>
<p>Man o'War debuted at Belmont Park on June 6th, 1919, in a purse race against six other contenders. He won easily by 6 lengths. Three days later, he stepped up to stakes company and dusted five others in the Keene Memorial Stakes.&nbsp;</p>
<p>Rested for eleven days, he reappeared to win the Youthful Stakes at Jamaica Park. Then, two days after that, was showed up at Aqeduct where he was entered in the Hudson Stakes. With only three races to his record to this point, he was already attracting great weights. For the Hudson, he carried 130 lbs (unheard of these days in the juvenile ranks.) Conceding lumps of weight to his competition, he stretched out easily and won unchallenged. Violet Tip finished 2nd, 1 1/2 lengths away, but was receiving 21 lbs in the bargain.&nbsp;</p>
<p>Another break of 12 days, and he was back with 130 lbs to steal the Tremont Stakes from two competitors. From here he traveled upstate to Saratoga and conceded weight all around to win the United States Hotel Stakes, defeating Upset by a length while carrying 15 lbs greater weight.&nbsp;</p>
<p>Eleven days later, on August 13, 1919, came a race they are STILL talking about, and one which undoubtedly earned Saratoga its nickname as "the house of upsets" and the "graveyard of favorites". This was the Sanford Memorial. Chief opposition was to come from Golden Broom, at level weights. Upset was not considered much of a threat - Man o'War had already beaten him in their last meeting, although Upset would again carry 15 lbs less.&nbsp;</p>
<p>These were the days before starting gates, and the group circled, approached the starting line as a team, and were released by signal of the starter's flag. On this day, Man o'War was still circling when the flag fell, and was in fact, not even yet facing the right direction. It was a horrible for him - the group was gone before he got himself righted, placing him at a distinct disadvantage in this 6 furlong race. He shot straight away after them, and very soon had passed Captain Alcock, The Swimmer and Armistice. Assuming the rail would be the shortest route, jockey Loftus steered Big Red to the rail and aimed for the leader, Golden Broom. Upset was challenging for the lead, and Donnacona was starting to fade. Unfortunately, Donnacona was on the rail also, and Man o'War was forced to check go to the outside to continue his run. The 130 lbs was beginning to tell on Golden Broom, and he relinquished the lead to the courageous Upset. Man o'War continued to close resolutely, but ran out of track with a half length to go.&nbsp;</p>
<p>It was an unfair start, and Man o'War was boxed into a hopeless position during the running. Despite the injustice, Man o'War was heaped with glory for a superhuman effort in defeat. Everyone at Saratoga that day knew the best horse had not won. Upset would face Man o'War a total of six times in their careers, and would lose all the other meetings, but his single victory, a feat no other horse could claim, was enough to immortalize him.&nbsp;</p>
<p>Ten days later Man o'War avenged his loss with a triumph in the Grand Union Hotel Stakes. Upset was relegated to his more familiar second-place position.&nbsp;</p>
<p>A week later, making his fourth Saratoga start, he put on an exhibition of his family's bad habits, holding up the start of the Hopeful Stakes by a full 12 minutes. Once the race was underway, however, he was all business and swept to a four length victory over the filly Cleopatra.&nbsp;</p>
<p>Although it was Upset who once defeated the champ, there was only one horse who could run with Man o'War, and that was John P Grier, a son of Whisk Broom II. The two met for the first time at Belmont for the Futurity Stakes. The field also boasted a stellar cast of supporting players - Dominique, Cleopatra, Upset, On Watch, Paul Jones, etc. John P Grier was not yet matured to his potential and Man o'War passed him easily to beat him into second place.&nbsp;</p>
<p>Man o'War himself was not done growing. At Saratoga he was a scrawny kid of 970 lbs. At Belmont, he was up to 1,020. By the time he debuted as a three-yr-old he tipped the scale at 1,150 lbs. He did not run in the Kentucky Derby. Owner Sam Riddle did not like racing in Kentucky, nor did he think 3-yr-olds were ready to go 10 furlongs as early as May. (He would change his mind by the time War Admiral came around.) Man o'War stayed in the east, and prepared for the Preakness.&nbsp;</p>
<p>Without Man o'War, the Kentucky Derby drew a large field of 17 entrants. It was won by Paul Jones who held off Upset by just a head. The Preakness came 10 days later, and several in the Derby field ventured on to face Man o'War. The champ assumed the lead soon after the start, set a terrific pace, and soon killed off the attempt of King Thrush to pace him. Man o'War cruised by a comfortable 4 length margin on the turn, and had plenty in reserve to fend off Upset in the homestretch. Man o'War won by 1 1/2 lengths from Upset and Wildair. Donnacona ran fifth, and might have done better but was cut off at the start.&nbsp;</p>
<p>Man o'War ran next in the Withers and somehow managed to be assigned a feathery impost of 118 lbs in a three horse field. He defeated Wildair by 2 lengths and set a new American record for the mile in 1:35 4/5. Next came the Belmont Stakes where only Donnacona accepted the challenge. Donnacona thus became only the 3rd horse in history to run in all three Triple Crown events. Before him were War Cloud (1918) and Sir Barton (1919).&nbsp;</p>
<p>At level weights, Man o'War drew off from Donnacona to win by 20 lengths. In doing so, he set another American record - 1 3/8th miles in 2:14 1/5, which bettered Sir Barton's old mark by over three full seconds. Ten days later, he faced one challenger in the Stuyvesant Handicap, and although carrying 32 lbs more than his rival Yellowhand, his betting odds were at the microscopic price of 1-to-100, meaning you'd have to wager $100 to earn back $101. Man o'War left no one wondering. He bounded out to an almost immediate 5 length margin and stretched it to 8 lengths at the wire.&nbsp;</p>
<p>Next came the Dwyer Stakes at Aqueduct. Only John P Grier opposed him. They went head to head for almost a mile, then Man o'War drew off to win by a length and a half. Grier was no cart horse. In a year when Man o'War was gobbling up stakes races anywhere and everywhere he pleased, John P Grier will managed to score some impressive stakes victories of his own. Considered by most to be the second best colt of that generation, Grier (unlike Upset) had the good sense not to go running around in Man o'War's shadow all the time. Perhaps it is a testament to John P Grier's quality that he finished only a 1 1/2 lengths behind Man o'War in the Dwyer, after running with him for so long. Man o'War's time of 1:49 1/5 in the Dwyer set another American record.&nbsp;</p>
<p>He ventured back to Saratoga and won the Miller Stakes and Travers Stakes. In the latter, he faced old rivals Upset and John P Grier once again. Man o'War put in a great race, winning by 2 1/2 lengths. Grier put in an uncommonly bad race and finished 3rd behind Upset.&nbsp;</p>
<p>Big Red returned to Belmont for the Lawrence Realization, but by this time, no one had mucch interest in running their horses against him. Only Hoodwink came forward to meet him in this event, and he was openly racing for the second-place purse money. Knowing there would be no real challenge, Feustel set Man o'War against the clock. The old time record, 2:45 flat, for the mile and five eights, would be his competition. Poor little Hoodwink, who was clearly not in the same league, was not persevered with, while Man o'War stormed away, out of sight, and won by an estimated 100 lengths, in a new American time of 2:40 4/5.&nbsp;</p>
<p>The following week, he set another American record, 2:28 4/5 for the 1 1/2 mile Jockey Club Stakes. He then won the Potomac Handicap defeating Wildair, Blazes, and Derby winner Paul Jones. That was the largest field he had faced in months! But he had struck himself in this race, and he started to bow a tendon. The leg began to cause him some trouble, but Feustel kept him fit for one more race.&nbsp;</p>
<p>The final start of Man o'War's career came in Canada, in the Kenilworth Park Gold Cup. For this 1 1/4 mile event, he was pitted against Sir Barton who was having a fabulous handicap year. This time it was Sir Barton who was high-weighted. Man o'War got in with 120, against Sir Barton's 126. Man o'War drew off to win easily by 7 lengths, but it was discovered afterward that Sir Barton was suffering from sore feet. Following the race, a good numbers of rumors and ill-will were flying. Willis Sharpe Kilmer had been offended because Exterminator had not been invited to participate. Moments before the race jockey Earle Sande was removed from Sir Barton, and Frank Keogh was substituted. Feustel said he found after the race that Man o'War's stirrup leather had been cut, but the job had been done badly and the leather held.&nbsp;</p>
<p>Matt Winn offered a $50,000 special versus Exterminator, but it was declined. An offer came from England, but it too was declined. Man o'War was shipped back Glen Riddle farm for the winter. He arrived in Lexington on January 27, 1921 and was ridden under silks before a huge crowd the following day at the Lexington Association track. While it is true that our greatest horse never raced in Kentucky, he DID set foot on a Kentucky racetrack.&nbsp;</p>
<p>Man o'War stood his first stud season at Hinata Farm, then the following year moved to Faraway Farm where he joined an old acquaintance Golden Broom. His groom at Faraway was Will Harbut who came to be closely associated with the horse. Harbut gladly showed the stallion to farm visitors and spoke at length of Man o'War's victories. Before long, Harbut's words were picked up through national magazines, and the whole country was quoting his now famous phrase "He wuz de mostest hoss... "&nbsp;</p>
<p>Man o'War was an outstanding sire, and might have been even better if Riddle had offered more than a handful of public seasons each year. Some of his famous offspring are WAR ADMIRAL, CRUSADER, AMERICAN FLAG, BATEAU, MARS, MAID AT ARMS, CLYDE VAN DUSEN, WAR RELIC, and BATTLESHIP (who won the Grand National Steeplechase at Aintree England even though they said he was too small to be a good jumper.) One of his famous grandsons was SEABISCUIT.&nbsp;</p>
<p>Man o'War died quietly on November 1, 1947 at the age of 30. He was embalmed and lay in state for three days while his final resting place was prepared in a portion of his old paddock. He was lowered into a moated enclosure, beneath a green marble pedestal from which rose Herbert Hazeltine's heroic bronze statue of the champion. Man o'War was eventually moved to the Kentucky Horse Park, where the original burial site was faithfully recreated. More than 50 years after his death, he still attracts thousands of visitors anually. And they still consider him to be the "mostest hoss."&nbsp;&nbsp;</p>
<p><strong>Born:</strong> March 29, 1917, at Nursery Stud, Lexington, Kentucky<br /><strong>Died:</strong> November 1, 1947 (age 30), at Faraway Farm, buried at the Kentucky Horse Park, Lexington, Kentucky&nbsp;</p>
<p><span style="text-decoration: underline;"><strong>Pedigree:</strong></span>&nbsp;</p>

<div class="table-responsive"><table align="center" class="data table table-condensed table-striped table-bordered" width="95%">
<tbody>
<tr>
<td rowspan="8" width="22%">Man o'War, ch.c.<br />foaled 1917</td>
<td rowspan="4" width="21%">Fair Play, 1905</td>
<td rowspan="2" width="23%">Hastings, 1893</td>
<td width="34%">Spendthrift</td>
</tr>
<tr>
<td width="34%">*Cinderella</td>
</tr>
<tr>
<td rowspan="2" width="23%">*Fairy Gold, 1896</td>
<td width="34%">Bend Or</td>
</tr>
<tr>
<td width="34%">Dame Masham</td>
</tr>
<tr>
<td rowspan="4" width="21%">Mahubah, 1910</td>
<td rowspan="2" width="23%">*Rock Sand, 1900</td>
<td width="34%">Sainfoin</td>
</tr>
<tr>
<td width="34%">Roquebrune</td>
</tr>
<tr>
<td rowspan="2" width="23%">*Merry Token, 1891</td>
<td width="34%">Merry Hampton</td>
</tr>
<tr>
<td width="34%">Mizpah</td>
</tr>
</tbody>
</table></div>
<p style="padding-left: 30px;">(female family # 4)</p>
<p style="padding-left: 30px;">&nbsp;</p>
<p><span style="text-decoration: underline;"><strong>Racing Record:</strong></span></p>
<p><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>

<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="377">
<tbody>
<tr>
<th>Year</th>
<th>Age</th>
<th>Starts</th>
<th>1st</th>
<th>2nd</th>
<th>3rd</th>
<th>unp.</th>
<th>earnings</th>
</tr>
<tr>
<td>1919</td>
<td align="center">2</td>
<td align="center">10</td>
<td>9</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td align="right">$ 83,325</td>
</tr>
<tr>
<td>1920</td>
<td align="center">3</td>
<td align="center">11</td>
<td>11</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td align="right">166,140</td>
</tr>
<tr>
<td>total</td>
<td>&nbsp;</td>
<td align="center">21</td>
<td>20</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td align="right">$249,465</td>
</tr>
</tbody>
</table></div>

<p><span style="text-decoration: underline;"><strong>Stakes Record:</strong></span></p>
<p>at 2:</p>
<ul>
<li>won - Keene Memorial Stakes </li>
<li>won - Youthful Stakes </li>
<li>won - Hudson Stakes ............ (carrying 130 lbs) </li>
<li>won - Tremont Stakes ........... (carrying 130 lbs) </li>
<li>won - United States Hotel Stakes (carrying 130 lbs) </li>
<li>won - Grand Union Hotel Stakes.. (carrying 130 lbs) </li>
<li>won - Hopeful Stakes ........... (carrying 130 lbs) </li>
<li>won - Belmont Futurity ......... (carrying 127 lbs) </li>
<li>2nd - Sanford Stakes </li>
<li><strong>Champion 2yo Colt</strong>&nbsp;</li>
</ul>
<p>&nbsp;at 3:</p>
<ul>
<li>won - Preakness Stakes </li>
<li>won - Belmont Stakes ........... (won by 20 lengths) </li>
<li>won - Dwyer Stakes </li>
<li>won - Withers Stakes </li>
<li>won - Stuyvesant Handicap ...... (carrying 135 lbs) </li>
<li>won - Miller Stakes </li>
<li>won - Travers Stakes </li>
<li>won - Lawrence Realization ..... (won by estimated 100 lengths) </li>
<li>won - Jockey Club Stakes ....... (won by 15 lengths) </li>
<li>won - Potomac Handicap ......... (carrying 138 lbs) </li>
<li>won - Kenilworth Park Gold Cup </li>
<li><strong>Horse of the Year</strong> </li>
<li><strong>Champion 3yo Colt</strong></li>
</ul>


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    