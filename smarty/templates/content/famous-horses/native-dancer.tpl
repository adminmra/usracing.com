{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/horses.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Native Dancer</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><img style="float: left; margin:0 20px 10px 0;" src="/images/horses/native-dancer.gif" alt="Native Dancer"  class="img-responsive" /></p>

<p>It was a cold winter day when my boyfriend called to tell me of a woman who was looking for a good home for one of her possessions -- a Native Dancer scrapbook. Would I be interested in going to take a look at it? Did Native Dancer interest me?&nbsp;</p>

<p>What racing fan wouldn't be interested in such an object! Of course I knew of the "Gray Ghost of Sagamore," winner of 21 of his 22 starts, beaten only once to the wire by Dark Star in the 1953 Kentucky Derby. But for that race, he would have been a Triple Crown winner. Horse of the Year at two and again at four. Other than that, however, I knew that he was an Alfred Vanderbilt color-bearer, and not many other details.&nbsp;</p>
<p>Within the hour, we were driving to Cambridge, New York; a small historic town with lovely homes and landscapes that seem to go on forever. We pulled into the driveway of a small home, and knocked on the door. An elderly woman greeted us warmly, beckoning us to enter.&nbsp;</p>
<p>Audrey Wallace. I had not met her before, but felt as though I could have always known her. Her home was filled with horse racing items, memorabilia collected over her years devotedly working with NYRA and Fasig-Tipton. In her retirement, she had moved but, without children to pass her treasures on to, she wanted to give them "the right home." She handed me some articles and photos of Alydar, Ruffian, Hoist the Flag -- all personal favorites of hers.&nbsp;</p>
<p>And then she gave me the scrapbook, an oversized leather-bound volume with yellowed pages. I thanked her and, after a short visit, drove home. Within moments, I had the album opened and on the first page, staring back at me, was an 11x14" photograph -- a composite of all of this grey's 1952 wins, with an accompanying chart. "Unbeaten Native Dancer retires for the 1952 Season; Money Winning Juvenile of All Time Win Record of $230,495". I gazed for moments at the picture of a young Alfred Vanderbilt leading in this dark grey, the proud rider Eric Guerin beaming for the camera.&nbsp;</p>
<p>And I slipped into their world...&nbsp;</p>
<p>Mr. Vanderbilt already had a great deal of success in racing when, in 1949 at age 37, he bred Native Dancer. His stallion, Discovery, was not only a wonderful racehorse and sire, but was fast becoming known as a broodmare sire as well. Native Dancer came about due to Vanderbilt's very simple breeding theory: "breed a Discovery mare to SOMETHING", in this case Geisha to Polynesian, and the result was one of America's greatest Thoroughbreds.&nbsp;</p>
<p>Geisha's foal was carried for almost a month more than the usual 11-month gestation period, and the gray colt was born large. Little did they know that he would become larger than life. Mr. Vanderbilt, looking at the sire's and dam's names, came up with a wonderful name for this colt: Native Dancer. And Native Dancer grew into an imposing racehorse, some 16.2 as a three year old and weighing in at 1200 pounds.&nbsp;</p>
<p>Native Dancer made his racetrack debut at Jamaica Racetrack on April 19, 1952, and the crowd was ready for him. Word had gotten around, and The Dancer was sent off at 7 to 5. He rewarded his backers with a 5-length score. And his odds were never as high as even money again.&nbsp;</p>
<p>1952 was a whirlwind of races and winner's circle -- nine starts and nine wins, eight of these events being stakes races. His Futurity was run in world-record equaling time, 6 1/2 furlongs in 1:14 2/5. Chart notes reading "much the best", "in hand" and "drew out easily" resulted in this 2-year-old being bestowed with an honor never before awarded a juvenile -- American champion, or Horse of the Year.&nbsp;</p>
<p>His return in the 1953 Gotham was highly anticipated, and also drew a national television audience -- who watched a bigger, stronger grey charge home two lengths the best. The television audience was hooked, and Native Dancer became our first nationally watched horse racing hero, in the time of TV's infancy. He certainly was easy to spot on black and white sets, with his grey coat and come from behind style -- and he riveted audiences throughout his Triple Crown events.&nbsp;</p>
<p>The Kentucky Derby of 1953 has become a thing of legend, surpassed perhaps only by Man o'War's loss to Upset -- although at the time people did not know of its mark on racing history. Accounts from the time certainly blame his bad trip, his bad luck on the first turn in getting boxed in, and they do applaud his strength in coming on at the wire to lose to Dark Star by a quickly diminishing head. But detractors, not unlike today's cynics, often wrote that the horse appeared sore, or that he ran awkwardly in the morning, and that he was not a very overwhelming or memorable champion.&nbsp;</p>
<p>Mr. Vanderbilt, always a witty gentleman with a wry sense of humor, of course heard these rumors. Word reached him that observers thought that Native Dancer "left the track wrong," looking either tired or sore. Vanderbilt's wonderful response was simple, "Oh, yeah? I never liked the way Citation walked off the track either -- he walked back to the Calumet barn instead of mine".&nbsp;</p>
<p>Native Dancer proved the critics wrong in winning the Preakness by a head and the Belmont by a neck, both times over a game Jamie K. In between he added the Withers, then finished off the year with wins in the Dwyer, Arlington Classic, Travers and American Derby.&nbsp;</p>
<p>After the American Derby, he bruised a foot and, rather than risk a serious injury, trainer Bill Winfrey put him away until 1954 -- putting to rest the hopes of a meeting between him and the top handicap runner Tom Fool.&nbsp;</p>
<p>1954 was a year of only three starts for The Grey Ghost, as he was affectionately known. An allowance in May was quickly followed with a Metropolitan Handicap score under 130 pounds -- and a star appearance on the cover of TIME Magazine. Then his foot went bad again, and he did not reappear until Saratoga in August. Under a career highweight of 137 pounds he easily put away two rivals in the Oneonta Handicap, pulling away on a very sloppy track to win by 9. He would never run again.&nbsp;</p>
<p>His recurrent injury caused them to retire The Dancer, and squelched Vanderbilt's hopes of sending his grey to France to contest the Prix de la Arc de Triomphe. Instead, Native Dancer gave a farewell appearance on a lovely October afternoon at Belmont Park, galloping to a large and saddened audience. And then the Grey Ghost returned to the wonderful rolling pastures of Sagamore, in the town of Glyndon, Maryland -- where he would stand at stud alongside his incredible grandsire, Discovery.&nbsp;</p>
<p><strong>Race Record:<br />&nbsp;</strong></p>
<div class="table-responsive"><table class="data table table-condensed table-striped table-bordered" width="50%">
<tbody>
<tr>
<td width="54">Age</td>
<td width="54">Starts</td>
<td width="28">1st</td>
<td width="32">2nd</td>
<td width="29">3rd</td>
<td width="83">earnings</td>
</tr>
<tr>
<td width="54">2</td>
<td width="54" align="center">9</td>
<td width="28" align="center">9</td>
<td width="32" align="center">0</td>
<td width="29" align="center">0</td>
<td width="83" align="right">$230,495</td>
</tr>
<tr>
<td width="54">3</td>
<td width="54" align="center">10</td>
<td width="28" align="center">9</td>
<td width="32" align="center">1</td>
<td width="29" align="center">0</td>
<td width="83" align="right">513,425</td>
</tr>
<tr>
<td width="54">4</td>
<td width="54" align="center">3</td>
<td width="28" align="center">3</td>
<td width="32" align="center">0</td>
<td width="29" align="center">0</td>
<td width="83" align="right">41,320</td>
</tr>
<tr>
<td width="54">Totals</td>
<td width="54" align="center">22</td>
<td width="28" align="center">21</td>
<td width="32" align="center">1</td>
<td width="29" align="center">0</td>
<td width="83" align="center">
<div>$785,240</td>
</tr>
</tbody>
</table></div>

<p><strong>Pedigree:<br /></strong></p>
<div class="table-responsive">
<table class="data table table-condensed table-striped table-bordered"  >
<tbody>
<tr>
<td rowspan="8" width="69" align="center">NATIVE<br />DANCER<br />gr.c. 1950</td>
<td rowspan="4" width="86" align="center">Polynesian<br />1942</td>
<td rowspan="2" width="97" align="center">Unbreakable<br />1935</td>
<td width="87" align="center">Sickle<br />1924</td>
<td width="168">Phalaris<br />Selene</td>
</tr>
<tr>
<td width="87" align="center">Blue Glass<br />1917</td>
<td width="168">Prince Palatine<br />Hour Glass II</td>
</tr>
<tr>
<td rowspan="2" width="97" align="center">Black Polly<br />1936</td>
<td width="87" align="center">Polymelian<br />1914</td>
<td width="168">Polymelus<br />Pasquita</td>
</tr>
<tr>
<td width="87" align="center">Black Queen<br />1930</td>
<td width="168">Pompey<br />Black Maria</td>
</tr>
<tr>
<td rowspan="4" width="86" align="center">Geisha<br />1943</td>
<td rowspan="2" width="97" align="center">Discovery<br />1931</td>
<td width="87" align="center">Display<br />1923</td>
<td width="168">Fair Play<br />Cicuta</td>
</tr>
<tr>
<td width="87" align="center">Ariadne<br />1926</td>
<td width="168">Light Brigade<br />Adrienne</td>
</tr>
<tr>
<td rowspan="2" width="97" align="center">1935</td>
<td width="87" align="center">John P. Grier<br />1917</td>
<td width="168">Whisk Broom II<br />Wonder</td>
</tr>
<tr>
<td width="87" align="center">La Chica<br />1930</td>
<td width="168">Sweep<br />La Grisette</td>
</tr>
</tbody>
</table></div>

<p>Time certainly marked Native Dancer as a top sire and a lasting influence on the breed. Among his top offspring were horses such as Dan Cupid, the sire of Sea-Bird; Kauai King, who won the 1966 Kentucky Derby and Preakness; French champion and classic winner Hula Dancer; 1968 Kentucky Derby victor Dancer's Image; and a horse with a very short racing career -- Raise a Native. He was a top 2-year-old in 1963, the same year in which a grandson of Native Dancer was fast emerging -- named Northern Dancer. The influence which Raise a Native and Northern Dancer alone have had on the breed is immeasurable.&nbsp;</p>
<p>Native Dancer became ill with what appeared to be a bout of colic on November 14, 1967. When treatment did not ease his discomfort, the famous grey stallion, now almost white with age, was rushed to the New Bolton Center for surgery. That surgery was performed on Wednesday evening, during which time some ten feet of intestine, and a sizable tumor, were removed. But, while coming out of the anesthesia, Native Dancer could not fight any more. He went into shock. And at 5:15, on the morning of November 16, the mighty gray died.&nbsp;</p>
<p>As I thumbed through the wonderful scrapbook which Audrey had given me, I thought of the sadness she must have felt when she clipped out the banner headline of that Morning Telegraph: "Native Dancer -- 'Gray Ghost of Racing' -- Is Dead." In the yellowed pages which followed, there were many articles: the New York Times had a large piece, and Time Magazine, who rarely covered such things, had an entire column about the passing of the great horse. Headlines ached with such titles as "Sad Loss", "Death of an Idol", "Native Dancer's Get to Keep Name Alive", "Matinee Idol" and "The Passing of the Ghost." As I leafed through the scrapbook's later pages, I felt a great sadness for a horse who had retired long before I was even born.&nbsp;</p>
<p>And the pictures which followed in the scrapbook were riveting -- photos which Audrey had taken just weeks before Native Dancer's death. The first was of a sign at the entry to Sagamore: "Sagamore Farm, Home of Native Dancer; visitors are welcome during visiting hours; closed Sunday Thank you -- Enjoy your visit Visiting hours 1 PM to 3 PM." The next photo was of a stallion barn, basked in bright sunlight and painted in the famous cerise and white trimmings of Alfred Vanderbilt's colors. And then, there they were -- seven jewels of images, all capturing a large thick grey horse, appearing almost white. His head was down grazing in most shots, his coat quite clean and well kept. In one, a woman peers at him over the fence, and I wondered if it were Audrey Wallace. And then the last photo on the page, which showed the grand Native Dancer peering off into the distance, as if surveying the lands which he helped to make famous. Underneath these photos was a woman's writing: "Monday, October 30, 1967."&nbsp;</p>
<p>The pages which followed held each of his 22 charts, lovingly clipped and placed on consecutive pages, as well as a note to Mr. Vanderbilt and a return letter from the farm's manager Harold Ferguson. And then a few more photos, taken in October of 1968. Several photographs of the last Native Dancers, now weanlings. A photograph of the Sagamore Farm cemetery, where such greats as Loser Weeper, Bed o' Roses and Discovery were put to rest. And a photograph of the grave of Native Dancer, showing the now-growing grass over the plot of Audrey's all- time favorite horse. It must have hurt to take these images.&nbsp;</p>
<p>Several years passed, and I worked lovingly to fill gaps in the scrapbook Audrey had given to me. I collected articles, bought old programs and printed up copy photographs of Native Dancer from books to place on the respective album pages. I found postcards and Native Dancer playing cards to include in the restoration. And then I looked up Audrey Wallace to show her the results of her gift, and the gift of history which she had given me.&nbsp;</p>
<p>There was no more listing. Audrey Wallace was no longer in Cambridge.&nbsp;</p>
<p>In 1997, while visiting a beautiful cemetery near Cambridge, I came across a stone which bore the name "Audrey Wallace". The years seemed right, and this woman had passed away just a year after I received the album.&nbsp;</p>
<p>Perhaps she knew she was ill. I am sure that she wanted the album to go to a person who would cherish it, both for the love put into making it and for the incredible story it contained. It was a story about a gray horse who burst upon the scene at the right time and truly "stole the show". A horse who not only etched his name in the history books, but created innumerable and rewrote the future of Thoroughbred racing through his offspring.&nbsp;</p>
<p>As Joe Estes wrote in 1954: "There would be other heroes, in racing and in other sports, but the stout grey from Sagamore Farm would be remembered as the horse which first revealed to the millions the courage and nobility of the Thoroughbred.</p>


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    