{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{assign var="ref" value="road-to-the-roses"}

{include file='inc/left-nav-btn.tpl'}

<div id="left-nav"> 

  <!-- ---------------------- left menu contents ---------------------- --> 

  {include file='menus/kentuckyderby.tpl'} 

  <!-- ---------------------- end left menu contents ------------------- --> 

</div>

<!--<div class="container-fluid">

	<a href="/signup?ref=kentucky-derby">

		<img class="img-responsive" src="/img/kentuckyderby/2017-Kentucky-Derby-road-to-the-roses.jpg" alt="Kentucky Derby Betting">

	</a>

</div> -->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">



	  <div class="text text-xl">{$h1}</div>



	  <div class="text text-xl" style="margin-top:0px;"></div>



	  <div class="text text-md">{$h2}<br>



	    <a href="/signup?ref={$ref}">



	    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i> {$signup_cta}</div>



	    </a> </div>



	</div>





<div id="main" class="container">

  <div class="row">

    <div id="left-col" class="col-md-9">

      <div class="content"> 

        <!-- --------------------- content starts here ---------------------- --> 

        

        {*<a href="/login?ref={$ref}"><img  class="img-responsive" width="100%" src="/img/kentuckyderby/kentucky-derby-roses.jpg" alt="Road to the Roses" /></a><br>*}

        {*include file='/inc/kentuckyderby-slider.tpl'*}

        <div class="headline">

          <h1>Road to the Roses</h1>

        </div>

        <p>The Kentucky Derby title is the most sought after prize in horse racing. It inspires thousands of horses owners, trainers and jockeys. But every year, on that first Saturday in September at the historic <a href="/churchill-downs">Churchill Downs</a> in Louisville, <a href="/kentucky">Kentucky</a>, only 20 thoroughbreds will have earned the right to compete. </p>

        <p> Before 2013, derby contenders became eligible to compete in the Kentucky Derby if they earned enough prize money during key Graded Stakes races on the Road to the Roses. Now, there is a points-based system where the points get higher during the prep races peaking with the Championship Series in the week prior to the Kentucky Derby on the long Road to the Roses.</p>

        <h2>{include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby Prep Races</h2>



        <!-- stats-->

        {* <p>{include_php file='/home/ah/allhorse/public_html/kd/road-to-roses.php'}</p> *}

        

        <p>{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}</p>

	<p> {include_php file='/home/ah/allhorse/public_html/kd/prepraces.php'} </p>

        <ul>

          <li>For a complete list please visit our <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li>

        </ul>

        <p>The top 4 finishing horses in each of the prep races that take place over the course of the 2-year-old and 3-year-old season are awarded points that are tallied across all the Derby Prep Races they compete in. In case of a points tie, earnings in non-restricted stakes will serve as a tiebreaker. Horses are then ranked to determine who is eligible to run in the Kentucky Derby and earn those red roses. <br />

        </p>

        <ul>

          <li>For a list of eligible horses please visit our <a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>

        </ul>

        <p></p>

        <p>A separate Road to the Kentucky Oaks competition is run during the same time period. Fillies that are eligible for the Kentucky Oaks, run at Churchill Downs the first Friday in September, the day before the Kentucky Derby. Any filly running in a race on the Road to the Roses can also use those points for the Road to the Kentucky Oaks. </p>

        <img  class="img-responsive" width="50%" src="/img/kentuckyderby/road-to-the-roses.jpg" alt="Road to the Roses" style="float:left; padding:25px;" />

        <h2>The Origin of the 'Road' to the Roses</h2>

        <p>In the winners circle, the top horse is presented with a beautiful garland made of more than 400 red roses. In 1904 the red rose was adopted as the official flower of the Kentucky Derby, hence the Run to the Roses! </p>

        <p> It was New York sports columnist Bill Corum, who dubbed the Kentucky Derby the &quot;Run for the Roses,&quot; in 1925.  The tradition of the rose is so strong that some owners have even had the flower dipped in silver to preserve it. In fact, a silver-dipped flower from the garland of the 1982 winner, Gato del Sol, is on display in the Kentucky Derby Museum, at Churchill Downs. </p>

        <br />

        <br />

        <img  class="img-responsive" width="50%" src="/img/kentuckyderby/kentucky-derby-garland-of-roses.jpg" alt="Garland of Roses" style="float:right; padding:25px;" />

        <h2>Garland of Roses</h2>

        <p>The rose garland is now synonymous with the Kentucky Derby. The first appearance of the rose in the winner&rsquo;s circle was in1896 when Ben Brush received a floral arrangement of white and pink roses. Today, the Governor of Kentucky and other dignitaries present the garland and the trophy in the winner&rsquo;s circle at the completion of the Derby. There&rsquo;s even a song called &ldquo;Run for the Roses&rdquo; composed by Dan Fogelberg which was released in time for the 1982 running of the roses.</p>

        <p>In 1987, the Kroger Company became the official florist of the Kentucky Derby and it creates the garland in the aisles of one of its stores on the eve of the Derby. It&rsquo;s typically a scene of long lines where race fans try to catch a peek of the Derby winner's garland of roses. Not your typical trip to the grocery store with winning jockeys signing autographs in the back of the store!<br />

        </p>

        <p>The roses are stitched one-by-one by hand to a green silk backing with the seal of the Commonwealth and Twin Spires on one side and that year&rsquo;s running of the roses on the other. There is a single rose pointing upward which is said to &ldquo;symbolize the struggle and heart necessary to reach the Derby Winner&rsquo;s Circle.&rdquo; It&rsquo;s a meticulous process and very labor intensive. The roses are shipped in specially, cut to size then stored in upright vials for the assembly team to get to work, some of whom have been participating in this tradition for more than the 27 years that it&rsquo;s been Kroger&rsquo;s job to create the work of art. </p>

        <p align="center"><a href="https://www.usracing.com/login?ref={$ref}" class="btn-xlrg ">Bet on the Road to the Roses</a></p>

        {*

        <h2>Road to the Roses Schedule  by Leg</h2>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Road to the Roses" >

          <tr>

            <th>Race</th>

            <th>Place</th>

            <th>Points Earned</th>

          </tr>

          <tr>

            <td > Royal Lodge (Newmarket, England)</td>

            <td >1st -2nd - 3rd - 4th</td>

            <td > 10 - 4 - 2 - 1</td>

          </tr>

          <tr class="odd">

            <td >Norfolk (Santa Anita, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Breeders' Futurity (Keeneland, KY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Champagne (Belmont, NY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Grey (Woodbine, Canada) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Breeders' Cup Juvenile </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Delta Downs Jackpot (Delta Downs, LA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Remsen (Aqueduct, NY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Kentucky Jockey Club (Churchill Downs, KY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >CashCall Futurity (Hollywood Park, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Sham (Santa Anita, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Lecomte (Fair Grounds, LA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Smarty Jones (Oaklawn Park, AR) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Holy Bull (Gulfstream Park, FL) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Robert B. Lewis (Santa Anita, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Sam F. Davis (Tampa Bay Downs, FL)</td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Withers (Aqueduct, NY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >El Camino Real Derby (Golden Gate, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Southwest (Oaklawn Park, AR)</td>

            <td ></td>

            <td ></td>

          </tr>

        </table>

        <h2>Championship Series - 1st Leg</h2>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Road to the Roses" >

          <tr>

            <th>Race</th>

            <th>Place</th>

            <th>Points Earned</th>

          </tr>

          <tr>

            <td >Risen Star (Fair Grounds, LA) </td>

            <td >1st -2nd - 3rd - 4th</td>

            <td > 50 - 20 - 10 - 5 </td>

          </tr>

          <tr class="odd">

            <td >Fountain of Youth (Gulfstream Park, FL) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Gotham (Aqueduct, NY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Tampa Bay Derby (Tampa Bay Downs, FL) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >San Felipe (Santa Anita, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Rebel (Oaklawn Park, AR) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Spiral (Turfway Park, KY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Sunland Derby (Sunland Park, NM) </td>

            <td ></td>

            <td ></td>

          </tr>

        </table>

        <h2>Championship Series - 2nd Leg</h2>

        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" title="Championship Series" summary="Road to the Roses" >

          <tr>

            <th>Race</th>

            <th>Place</th>

            <th>Points Earned</th>

          </tr>

          <tr>

            <td >UAE Derby (Meydan, Dubai) </td>

            <td >1st -2nd - 3rd - 4th</td>

            <td > 100 - 40 - 20 - 10 </td>

          </tr>

          <tr class="odd">

            <td >Louisiana Derby (Fair Grounds, LA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Florida Derby (Gulfstream Park, FL) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Wood Memorial (Aqueduct, NY) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Santa Anita Derby (Santa Anita, CA) </td>

            <td ></td>

            <td ></td>

          </tr>

          <tr class="odd">

            <td >Arkansas Derby (Oaklawn Park, AR)</td>

            <td ></td>

            <td ></td>

          </tr>

          <tr>

            <td >Blue Grass Stakes (Keeneland, KY) </td>

            <td ></td>

            <td ></td>

          </tr>

        </table>

        <h2>Championship Series - Wild Card Races</h2>

        <table  width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-condensed table-striped table-bordered" summary="Kentucky Derby TV Schedule" title="Kentucky Derby Betting - TV Schedule">

          <tbody>

            <tr>

              <th>Date</th>

              <th>Races</th>

              <th>Network</th>

            </tr>

            <!-- <tr>

    <td valign="top">March 26</td>



    <td>* Louisiana Derby, TBA</td>

  <td>USA Network</td>



  </tr> --> 

            <!-- <tr class="odd">

    <td valign="top">April 9 </td>

    <td>* Santa Anita Derby, TBA</td>



    <td>NBC</td>

  </tr> -->

            

            <tr  >

              <td  >{include file='/home/ah/allhorse/public_html/kd/racedate.php'}</td>

              <td>* {include file='/home/ah/allhorse/public_html/kd/year.php'} Kentucky Derby, TBA<br></td>

              <td>NBC</td>

            </tr>

          </tbody>

        </table>

        <p>The {include file='/home/ah/allhorse/public_html/kd/running.php'} Kentucky Derby<br />

          {include file='/home/ah/allhorse/public_html/kd/date.php'}, Kentucky Derby {include file='/home/ah/allhorse/public_html/kd/running.php'} (Churchill Downs, KY)</p>

        *} 

        <!-- ------------------------ content ends -------------------------- --> 

      </div>

      <!-- end/ content --> 

    </div>

    <!-- end/ #left-col -->

    

    <div id="right-col" class="col-md-3"> {include file='inc/rightcol-calltoaction-kd.tpl'} 

      

      

      {*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} </div>

    <!-- end: #right-col --> 

    

  </div>

  <!-- end/row -->

  

  <div class="row" >

<div class="col-md-12">

{include file='inc/disclaimer-kd.tpl'}

</div>

</div>

  

   </div>

<!-- /#container --> 

