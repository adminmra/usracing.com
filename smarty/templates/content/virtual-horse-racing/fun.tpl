 
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Virtual Derby - Play for Fun</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="field field-type-text field-field-body-logged-in">
    
            
             
<div class="virtualderby-box">
<iframe src=http://lobby.sb.betsoftgaming.com/login.do?siteID=7000&game=531&game_mode=free" style="display: block; height:630px;width:850px; margin: 0 auto; padding: 10px 0;" marginheight="0" marginwidth="0" allowtransparency="true"  frameborder="0" scrolling="no"></iframe>
<br>
<h3><span class="DarkBlue">Virtual Derby<sup><small>™</small></sup> Horse Racing Game</span></h3>
<p>&nbsp;</p>
<p><span class="DarkBlue"></span></p> 

<p>Virtual Derby is a <a title="horse race game" href="/virtualderby">horse race game</a> that you can play for fun or for real money. You can select your jockey's silks and the track is open 24 hours a day!</p>

<p>There are six horses and jockeys to choose from all with different odds of winning. Each and every horse's odds represent a true reflection of that horse's chances in the race. Factors such as the recent form of the horse, the experience of the jockey, the quality of the trainer and the going on the particular day of the race are all factors that contribute towards the compiling of the odds.</p>

<p>Play Virtual Derby for real or for fun. You must login to play for fun.</p><p>If you don't have a user name and password already and you want to play virtual horse racing for free, please join our site today!</p><p><a title="Join Today!" href="/join" target="_blank"><strong>Click here to sign up for free.</strong></a></p>
<p>&nbsp;</p>      </div>


<!-- <div class="virtualderby-box">
<iframe src="/virtual_derby_play" style="display: block; height:560px;width:755px;  margin: 0 auto; padding: 10px 0;" marginheight="0" marginwidth="0" allowtransparency="true"  frameborder="0" scrolling="no"></iframe>
</div> -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


