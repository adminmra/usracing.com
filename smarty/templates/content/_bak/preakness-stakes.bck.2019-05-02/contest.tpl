{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           
  {include file='menus/preaknessstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
        <div class="text text-md">{$h2}
                <br>
                        <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
        </div>
 </div>
</div>      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


          
                                                
                                      
          
<div class="headline"><h1>$5,000 FREE Handicapping Contest</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Preakness Stakes $5,000 FREE Handicapping Contest</h2></div>                    
                    <br />
                    <p>Preakness Stakes $5,000 FREE Handicapping Contest is here and promises to be more exciting than ever. Only US Racing brings to you all of the action of the Preakness Stakes for you to become a winner!</p>
                    <p>All you have to do is wager in the <a href="/racebook">Racebook</a> on any of the following races and you will be automatically entered to win part of our $5,000 prize package with no entry required. Make 36 wagers on the 136th Preakness Stakes.</p>
                    
<br />
<br />                    
{literal}<style type="text/css"></style>{/literal}

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="data">
<tr>
  <td colspan="5" class="content-header-bar"><strong>Friday, May 20th, 2011, BLACK EYED SUSAN DAY Race Schedule</strong></td>
</tr>
<tr>
  <th width="35%"><strong>Stakes</strong></th>
  <th><strong>Condition</strong></th>
  <th><strong>Distance</strong></th>
  <th><strong>Purse</strong></th>
  <th><strong>Post Time</strong></th>
</tr>
<tr>
                              <td > Unnamed </td>
                              <td >F&amp;M 3YO&amp;Up</td>
                              <td >6 furlongs</td>
                              <td >$33,000</td>
                              <td >12:00PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Female Jockey Challenge Race</td>
                              <td>3YO&Up</td>
                              <td>5 furlongs - Turf</td>
                              <td>$29,000</td>
                              <td>12:30PM</td>
                            </tr>
							  <tr>
                              <td > Unnamed </td>
                              <td >F&amp;M 3YO&amp;Up</td>
                              <td >1-1/16 Miles - Turf</td>
                              <td >$28,000</td>
                              <td >01:00PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Female Jockey Challenge Race</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>6 furlongs</td>
                              <td>$36,000</td>
                              <td>01:30PM</td>
                            </tr>
							 <tr>
                              <td > Jim McKay Turf Sprint </td>
                              <td >3YO&amp;Up</td>
                              <td >5 furlongs - Turf</td>
                              <td >$75,000</td>
                              <td >02:02PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Female Jockey Challenge Race</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>6 furlongs</td>
                              <td>$37,200</td>
                              <td>02:34PM</td>
                            </tr>
                            <tr >
                              <td>Legend's for the Cure</td>
                              <td>3YO&amp;Up</td>
                              <td>6 furlongs</td>
                              <td>$31,000</td>
                              <td>3:05PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Jockey Challenge Race</td>
                              <td>3YO&amp;Up</td>
                              <td>5 furlongs - Turf</td>
                              <td>$31,000</td>
                              <td>3:36PM</td>
                            </tr>
						    <tr >
                              <td>Ms. Preakness Pink Warrior Stakes</td>
                              <td>F 3YO</td>
                              <td>6 furlongs</td>
                              <td>$75,000</td>
                              <td>4:08PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Black Eyed Susan (G2)</td>
                              <td>F 3YO</td>
                              <td>1-1/8 Miles </td>
                              <td>$250,000</td>
                              <td>4:46PM</td>
                            </tr>
							<tr >
                              <td>The Very One</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>5 furlongs - Turf</td>
                              <td>$75,000</td>
                              <td>5:16PM</td>
                            </tr>
							
                            <tr class="odd">
                              <td>Kattegat's Pride Starter Handicap</td>
                              <td>F&M 3YO&Up </td>
                              <td>1-1/6 Miles </td>
                              <td>$25,000</td>
                              <td>5:46PM</td>
                            </tr>
                           
							 <tr>
                              <td>Hilltop Stakes</td>
                              <td>F 3YO</td>
                              <td>1-1/16 Miles - Turf</td>
                              <td>$75,000</td>
                              <td>6:16PM</td>
                            </tr> </table>

<br />
<br />

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="data">
<tr>
  <td colspan="5" class="content-header-bar"><strong>Saturday, May 21, 2011, PREAKNESS STAKES DAY Race Schedule</strong></td>
</tr>
<tr>
  <th  width="35%"><strong>Stakes</strong></th>
  <th><strong>Condition</strong></th>
  <th><strong>Distance</strong></th>
  <th><strong>Purse</strong></th>
  <th><strong>Post   Time</strong></th>
</tr>
<tr>
                              <td >Maryland Heritage Purse </td>
                              <td >3YO</td>
                              <td >6 furlongs</td>
                              <td >$31,000</td>
                              <td >10:45PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Deputed Testamony</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/6 Miles</td>
                              <td>$25,000</td>
                              <td>11:14AM</td>
                            </tr>
							  <tr>
                              <td > CoverGirl Purse </td>
                              <td >	3YO&amp;Up </td>
                              <td >1 1/16 mile (T)</td>
                              <td >$29,000</td>
                              <td >11:43AM</td>
                            </tr>
                            <tr class="odd">
                              <td>Allaire duPont Distaff (G2)</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/6 Miles - Dirt</td>
                              <td>$100,000</td>
                              <td>12:16PM</td>
                            </tr>
							 <tr>
                              <td >Sinai Good Neighbors </td>
                              <td >F&amp;M 3YO&amp;Up</td>
                              <td >	5 furlongs (T)</td>
                              <td >$31,000</td>
                              <td >12:53PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Chick Lang Stakes (G2)</td>
                              <td>3YO&amp;Up </td>
                              <td>6 furlongs - Dirt </td>
                              <td>$100,000</td>
                              <td>01:33PM</td>
                            </tr>
                            <tr >
                              <td>James Murphy Stakes</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/6 Miles (T)</td>
                              <td>$100,000</td>
                              <td>02:15PM</td>
                            </tr>
                            <tr class="odd">
                              <td>Maryland Sprint (G3)</td>
                              <td>3YO&amp;Up</td>
                              <td>6 furlongs (T)</td>
                              <td>$100,000</td>
                              <td>02:57PM</td>
                            </tr>
						    <tr >
                              <td>Gallorette Handicap (G3)</td>
                              <td>F&amp;M 3YO&amp;Up</td>
                              <td>1-1/6 Miles (T)</td>
                              <td>$100,000</td>
                              <td>03:39PM</td>
                            </tr>
                            <tr class="odd">
                              <td>William Donald Schaefer (G3)</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/6 Miles - Dirt </td>
                              <td>$100,000</td>
                              <td>4:22PM</td>
                            </tr>
							<tr >
                              <td>Dixie Stakes (G2)</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/8 Miles (T)</td>
                              <td>$200,000</td>
                              <td>5:07PM</td>
                            </tr>
							
                            <tr class="odd">
                              <td>Preakness Stakes (G1)</td>
                              <td>3Yo</td>
                              <td>1-3/16 Miles </td>
                              <td>$1,000,000</td>
                              <td>6:19PM</td>
                            </tr>
                           
							 <tr>
                              <td>Unnamed</td>
                              <td>3YO&amp;Up</td>
                              <td>1-1/16 Miles</td>
                              <td>$28,000</td>
                              <td>7:14PM</td>
                            </tr> 
</table>



<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding:0; vertical-align:top;" class="data">
<tbody>
<tr>
  <td style="width:13%;"><b>1st Prize</b></td>
  <td>$1,500 for the player with the most amount of wagers and highest winning percentage.</td>
</tr>
<tr>
  <td><b>2nd Prize</b></td>
  <td>$750 for the player with the highest winning percentage with a minimum of 36 wagers</td>
</tr>
<tr>
  <td><b>3rd Prize</b></td>
  <td>$250 for the player with the 2nd highest winning percentage with a minimum of 36 wagers.</td>
</tr>
<tr>
  <td><b>4th Prize</b></td>
  <td>The remainder of the $5,000 Handicapping contest will be a $2,500 pot to be divided evenly amongst the remainder of all players who place a minimum of 36 wagers.</td>
</tr>
<tr>
  <td><b>NOTE:</b></td>
  <td>In the event the 4th place prize is greater than the 1st-3rd place prize payouts, 1st-3rd place will be included in the 4th place prize pot payout in addition to their guaranteed winnings.</td>
</tr>
</tbody>
</table>

<br />
<b>Terms and Conditions:</b><br />
<br />
<ul>
<li>This promotion is open to all active players with no entry required.</li>
<li>Only wagers placed in the racebook on May 20th & 21st will be counted towards the 36 wager total.</li>
<li>Prize will be awarded as follows 1st Prize - $1,500 for the player with the most amount of wagers and highest winning percentage. 2nd Prize - $750 for the player with the highest winning percentage with a minimum of 36 wagers. 3rd Prize - $250 for the player with the 2nd highest winning percentage win a minimum of 36 wagers. 4th Prize - The remainder of the $5,000 Handicapping contest will be a $2,500 pot to be divided evenly amongst the remainder of all players who place a minimum of 36 wagers.</li>
<li>In the event the 4th place prize is greater than the 1st-3rd place prize payouts, 1st-3rd place will be included in the 4th place prize pot payout in addition to their guaranteed winnings.</li>
<li>Exotics account for 1 wager regardless of how many parts are involved.</li>
<li>All applicable customers will be credited the morning of May 22nd, 2011.</li>
<li>Only one entry per household/account is permitted.</li>
<li>US Racing reserves the right to alter or amend the terms and conditions of this promotion without notice.</li>
<li>Standard rollover requirements apply.</li>
<li>General <a href="/house-rules">house rules</a> apply</li>
</ul>
<br />
<br />
              
            
            

                      
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


