{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/preaknessstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
 
<!-- HERO -->
<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">
	<div class="text text-xl">{$h1}</div>
     	<div class="text text-md">{$h2}
          	<br>
               <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
         </div>
</div>
<!-- End HERO -->

<!-- <div class="container-fluid">
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a>
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a>
</div>-->

 

 
    
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
        



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/ps/wherewhenwatch.tpl'} </p>

<p>

The {include file='/home/ah/allhorse/public_html/ps/running.php'}  running of the $1,500,000 Preakness Stakes (Grade I), is the second jewel of the Triple Crown, set for {include file='/home/ah/allhorse/public_html/ps/racedate.php'}  at the historic track. Contender Information to be updated closer to race day.</p>

<div class="headline"><h1>{$h1}</h1></div>
<p>{include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'} </p>

 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  
 

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-ps.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}


</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 

<div id="main" class="container">
<div class="row" >
<div class="col-md-12">

 {*include file='inc/disclaimer-ps.tpl'*} 
</div>
</div>
</div><!-- /#container --> 