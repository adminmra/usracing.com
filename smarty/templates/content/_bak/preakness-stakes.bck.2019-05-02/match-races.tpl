{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <style>
    @media (min-width: 320px) and (max-width: 580px) {
      .newHero {
        background-position-x: 100%;
      }
    }
    @media (min-width: 581px) and (max-width: 599px) {
      .newHero {
        background-position-x: 55%;
      }
    }
  </style>
{/literal}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/preaknessstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>     
<!------------------------------------------------------------------>    
    
    <section class="ps-kd ps-kd--default usr-section">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/ps/salesblurb.tpl'} </p>

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}


</div>

            

<!-- --------------------- content starts here ---------------------- -->

Make head-to-head bets on horses at the Preakness! 


<h2>{include file='/home/ah/allhorse/public_html/ps/year.php'}  Preakness Stakes Props</h2>
<p>What are match races and why would you want to bet them at the Preakness Stakes?</p><p>Match races are simple wagers where you bet whether one particular horse will beat another horse.  Sometimes this is called head-to-head betting. At the Preakness Stakes, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.</p>

<p>Let's review: what are match races in horse racing?  Answer: Match races are bets that punters can place on individual horses in a particular race.  Instead of betting on whether a horse wins, places or shows, you are instead betting whether one horse will beat another horse!  Match races are extremely popular in the United Kingdom.</p>

<p>{include file='/home/ah/allhorse/public_html/ps/match_races_odds_1042.php'}</p>

<p>Good luck with your bets and when we get closer to the start of the <a href="/preakness-stakes/betting">Preakness Stakes</a>, you will find match race odds posted online.</p>
<p>&nbsp;</p>
              {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

        </div>
      </div>
    </section>
      <section class="ps-pimlico"><img src="/img/index-ps/bg-pimlico.jpg" alt="Pimlico" class="ps-pimlico_img"></section>
<!------------------------------------------------------------------>  

{include file="/home/ah/allhorse/public_html/ps/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/ps/block-triple-crown.tpl"}
{* {include file="/home/ah/allhorse/public_html/ps/block-testimonial-other.tpl"} *}
{include file="/home/ah/allhorse/public_html/ps/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/ps/block-exciting.tpl"}


<!------------------------------------------------------------------>  

{* <div id="main" class="container">
<div class="row" >
<div class="col-md-12">

 {include file='inc/disclaimer-ps.tpl'} 
</div>
</div>
</div><!-- /#container -->  *}

      
    
