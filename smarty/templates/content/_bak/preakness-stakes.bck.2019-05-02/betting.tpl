{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">



<!-- ---------------------- left menu contents ---------------------- -->



{include file='menus/preaknessstakes.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         

</div>

<!-- HERO -->

{literal}
<style>
  @media (min-width: 992px) {
    .newHero {
      background-image: url({/literal}{$hero}{literal});
    }
  }
</style>
{/literal}

<div class="newHero hidden-sm hidden-xs">
	<div class="text text-xl">{$h1}</div>
     	<div class="text text-md">{$h2}
          	<br>
               <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
         </div>
</div>
<!-- End HERO -->

<!--<div class="container-fluid">
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a>
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a>
</div> -->      

<!------------------------------------------------------------------>    

    

    <section class="ps-kd ps-kd--default usr-section">

      <div class="container">

        <div class="ps-kd_content">

          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">

          <h1 class="kd_heading">{$h1}</h1>

          <h3 class="kd_subheading">{$h2}</h3>

     <p>{include file='/home/ah/allhorse/public_html/ps/salesblurb.tpl'} </p>



{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}





{*  <div class="headline"><h1>{$h1}</h1></div> *}

<p>Well, if you want a BET on the&nbsp;<a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>, then you have found the right place!  Here are the live odds:</p> {* Will Nyquist continue his undefeated streak by winning the Preakness Stakes or will Exaggerator or Songbird deny him a chance at the Triple Crown?</p> *} 

{*  <p>{include file='/home/ah/allhorse/public_html/ps/wherewhenwatch.tpl'} </p> *}         

     <h2>{include file='/home/ah/allhorse/public_html/ps/year.php'} Preakness Stakes Odds</h2>   

     <p>{include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'} </p>

   <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>    



 <p>BUSR offers a bets on all the Triple Crown Races. Did you know that on May 23, 1873 the first edition of the Preakness took place?&nbsp;Two years before the inaugural&nbsp;<a title="Kentucky Derby" href="/kentucky-derby">Kentucky Derby</a>, the folks at Pimlico were busy working on a new three-year old stakes race of their own. Pimlico, which since its opening in 1870 had conducted all of its racing in the fall, ran its first Spring Meet in the year of 1873, with the initial running of the Preakness held on May 23. The Preakness was founded by then-Maryland governor Oden Bowie. Governor Bowie's term had actually ended in 1872, yet that did not prevent him from naming the then- mile and a half race in honor of the colt who won the Dinner Party Stakes in 1870 on the occasion of Pimlico's opening. At an 1868&nbsp;<a title="Saratoga" href="/saratoga">Saratoga</a>&nbsp;party hosted by a Milton Sanford, Bowie guaranteed that Maryland would have a track available for a race that was later dubbed the Dinner Party Stakes and had been instantly promoted by Bowie at the party when he offered $15,000 as a purse--no small sum at the time.</p>

 

 

 <p>{*Below is a video of how to bet on the Kentucky Derby.  As soon as post positions are drawn for this year's Preakness Stakes, a new tutorial will be posted.</p>



<br>

<!-- <iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/RCkRq_wydho" frameborder="0" allowfullscreen></iframe> -->

<!-- <iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/x2-VM8nx92M?rel=0" frameborder="0" allowfullscreen></iframe> -->

{include_php file='iframe/video/how_to_bet_video.php'}

<br>
<h2>{*Preakness Stakes Purse Structure</h2>



include file='/home/ah/allhorse/public_html/ps/purse.php'*} 





{*

<h2>{include file='/home/ah/allhorse/public_html/ps/racedate.php'}  - Preakness Stakes Day Race Schedule</h2>



<p>{include file='/home/ah/allhorse/public_html/ps/raceschedule.tpl'}  </p>  

*}  

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}



{*

<h2>{include file='/home/ah/allhorse/public_html/ps/racedate_BES.php'}  - BLACK EYED SUSAN DAY Race Schedule</h2>

                              

<p>{include file='/home/ah/allhorse/public_html/ps/raceschedule_BES.php'}</p>

   <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  

*}

            

            

                      







        </div>

      </div>

    </section>

      <section class="ps-pimlico"><img src="/img/index-ps/bg-pimlico.jpg" alt="Pimlico" class="ps-pimlico_img"></section>

<!------------------------------------------------------------------>  





{include file="/home/ah/allhorse/public_html/ps/block-free-bet.tpl"}

{include file="/home/ah/allhorse/public_html/ps/block-triple-crown.tpl"}

{* {include file="/home/ah/allhorse/public_html/ps/block-testimonial-other.tpl"} *}

{include file="/home/ah/allhorse/public_html/ps/block-countdown.tpl"}

{include file="/home/ah/allhorse/public_html/ps/block-exciting.tpl"}



<!------------------------------------------------------------------>  



<div id="main" class="container">
<div class="row" >
<div class="col-md-12">

 {*{include file='inc/disclaimer-ps.tpl'} *}
</div>
</div>
</div><!-- /#container --> 



      

    

