{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
 
  <!-- ---------------------- left menu contents ---------------------- --> 
  
  {include file='menus/preaknessstakes.tpl'} 
  
  <!-- ---------------------- end left menu contents ------------------- --> 
</div>

<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
{*  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}

 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>


 <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      
   
    
    <section class="ps-kd ps-kd--default usr-section">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/ps/salesblurb.tpl'} </p>

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}


<p>Find all the <a title="Preakness Stakes Odds" href="/preakness-stakes/odds">Preakness Stakes odds</a> for {include file='/home/ah/allhorse/public_html/ps/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more at our recommended ADWs and racebooks!</p>
</div>

{include file='/home/ah/allhorse/public_html/ps/wherewhenwatch.tpl'}  
{* {include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} *}
<h2> Preakness Stakes Props and Bets</h2>


<p>{include file='/home/ah/allhorse/public_html/ps/props_odds.php'}</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}


        </div>
      </div>
    </section>
      <section class="ps-pimlico"><img src="/img/index-ps/bg-pimlico.jpg" alt="Pimlico" class="ps-pimlico_img"></section>
<!------------------------------------------------------------------>  



{include file="/home/ah/allhorse/public_html/ps/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/ps/block-triple-crown.tpl"}
{* {include file="/home/ah/allhorse/public_html/ps/block-testimonial-other.tpl"} *}
{include file="/home/ah/allhorse/public_html/ps/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/ps/block-exciting.tpl"}


<!------------------------------------------------------------------>  

{* <div id="main" class="container">
<div class="row" >
<div class="col-md-12">

 {include file='inc/disclaimer-ps.tpl'} 
</div>
</div>
</div><!-- /#container -->  *}