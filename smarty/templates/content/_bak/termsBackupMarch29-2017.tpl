<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Terms and Conditions</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->  


<p><b>WELCOME!</b></p>
{*<h3>General House Rules</h3>
<ol>
<li>All rules and regulations are subject to revision by USRacing.com without prior written notice to USRacing.com members.</li>
<li>The management of USRacing.com reserves the right to refuse or limit any bet and to restrict betting on any event at any time without any advance notice.</li>
<li>Any service that USRacing.com offers, is intended for recreational players only. We reserve the exclusive right to refuse, limit or close any client account if we find that it is being used otherwise.</li>
<li>USRacing.com permits only one (1) account per person, household, IP address and shared computer environment such as public library.</li>
<li>You must be not less than 18 years of age and of legal age in your jurisdiction to use our services.</li>
<li>You must have funds in your account to place a wager. USRacing.com does not offer credit at anytime under any circumstance. Proof of payment must be made before USRacing.com credits your account balance. Wagers will not be accepted if you do not have funds in your account balance.</li>
<li>If you have a claim on a bet, you must contact us no later than (1) one week from the day the disputed bet was placed. Claims will not be honored after this period.</li>
<li>Claims on bets will normally be solved within 24 hours of having received the complaint.</li>
<li>Generally, postponed and/or rescheduled sports events will constitute a No Action Wager, and money will be credited back to the account once the game has been officially postponed. Certain sports have exceptions such as Soccer, Tennis and Golf matchups where wagers may be valid for 24 to 48 hours.</li>
<li>All wagers must be placed and accepted before the start of the sporting event being bet. Any bet placed after the event has started will be void unless approved by a manager.</li>
<li>USRacing.com reserves the right to close any account or group of accounts if they are suspected of fraud and or abuse of our bonuses program. In the event an account or group of accounts are found within the USRacing.com system, where the customers have conspired to fraudulently or maliciously manipulate our line offerings to guarantee themselves profits regardless of the outcome of the events, USRacing.com reserves the right to cancel any or all wagers and profits derived from such fraudulent activity as well as terminate the accounts in question permanently.</li>
<li>USRacing.com’s management reserves the right to refuse or limit the maximum amount allowed on any wager prior to acceptance of such wager. Members are not permitted to open multiple accounts either in their name or someone else’s in order to circumvent the limits imposed by our management. If multiple accounts are used all bets will be void and profits will be reversed.</li>
<li>All horse wagers must be placed two (2) minutes prior to the start of the race.</li>
<li>USRacing.com reserves the right to suspend any of its products at any time. When a product is suspended, any wagers entered will be rejected. USRacing.com also reserves the right to cease wagering on any of its products indefinitely at any time without prior notice.</li>
<li>In the case of an evident mistake on the posted line, scheduled time, or maximum wager, all wagers will be deemed “no action”, and funds will be credited accordingly. No player can benefit from wagering on a clear error in our lines, scheduled times or maximum wager limits.</li>
<li>When wagering on half-time lines, the overtime period(s) are included as a part of the second half.</li>
<li>All minimums, maximums, and betting payout prices are subject to change without prior written notice.</li>
<li>Maximum payout for any parlay is $100,000 and maximum combined daily winnings for any account is also $100,000.</li>
<li>You cannot parlay the same team with both the point spread and the money line. Multiple bets including parlays, teasers and if bets or reverses are not accepted where the outcome of one part of the bet contributes to the outcome of another; as these are considered correlated plays. It is also prohibited to parlay, tease, or play if bets with spread and total of any game where the full game spread is approximately 1/3 of the total for the full game.</li>
<li>Free Plays are only allowed on major sports and cannot be used on prop bets. Free Plays can ONLY be used as straight wagers. Buying points is not allowed in free plays.</li>
<li>If funds are credited to your account in error, it is incumbent upon you to notify USRacing.com of this mistake without delay. For accounts with negative balances, USRacing.com reserves the right to cancel any pending plays, whether placed with funds resulting from the error or not.</li>
<li>All winnings will be credited to the customer’s account. Any winnings credited to an account in error are not available for use, and USRacing.com reserves the right to void any transactions involving such funds, either at the time or retrospectively.</li>
<li>USRacing.com is a recreational sportsbook. Steamers and/or originators are not accepted. If an account happens to fall under any of those 2 categories (steamers and/or originators), management has the right to revoke free plays, cash bonuses and/or winnings and to limit and/or close the account.</li>
<li>USRacing.com defines “Playing Steam or Chasing Steam” as follows: Deliberately wagering on a line that is moving across several betting offices before it has been moved at our establishment with the purpose of gaining an advantage, this is also typically known as playing moves.</li>
<li>USRacing.com reserves the right to suspend any account based on abusive behavior from the customer towards any staff member of USRacing.com. The account may be re-opened after a warning has been issued to the client, however if such abusive behavior continues management reserves the right to terminate the account permanently.</li>
<li>House Rules will supersede at all times any verbal or written offering of any staff member of USRacing.com</li>
</ol>
<h3>Bets Placed Over the Phone</h3>
<ol>
<li>When calling USRacing.com the customer must give their personal account number and their password before placing a wager or gaining access to their account information.</li>
<li>All conversations will be recorded in case they are needed in the future to settle any discrepancies during a phone call. The recording will be listened to to settle any disputes and after reviewing the recording the wagers will be adjusted accordingly.</li>
<li>When receiving the final read back on a phone call from the customer service operator, it is up to the customer to listen carefully and verify that the wager(s) was read back correctly.</li>
<li>Once the customer has confirmed the read back all wagers will be confirmed. After accepting the read back, the only way that a wager will be cancelled is if the customer places a wager on the opposite side of the original wager.</li>
<li>If a dollar amount has been placed on a wager and the phone call is disconnected or lost, it is up to the customer to call back. The customer will have to call back before post time to assure that the wager was accepted and placed correctly.</li>
<li>$25.00 dollars is the minimum amount of any wager accepted on sporting events at USRacing.com.</li>
<li>All actions will be carried out and completed if the correct username and password is provided.</li>
</ol>
<h3>Bets placed over the Internet</h3>
<ol>
<li>The Customer’s must log in using their Personal Account Number and password to wager or access their account information.</li>
<li>It is the Customer’s responsibility to keep their password secure. All wagers placed in the account will be binding. Any unauthorized use of an account is the sole responsibility of the account holder and USRacing.com cannot be held liable.</li>
<li>It is the Customer’s responsibility to review the wagers before submitting them, and before logging out to make sure all bets were placed correctly. All wagers placed over the internet are final, and cannot be changed or deleted.</li>
<li>Minimum wager on sporting events is ($10.00) dollars.</li>
</ol>
<h3>Horse Wagering</h3>
<p>USRacing.com offers daily horse racing from various tracks around the world, you may call our wagering line to place your bets or if you prefer you may access our online racebook.<br>
All horse wagers will follow the rules detailed here:</p>
<ol>
<li>A time stamp will be placed on all horse wagers and wagers must be placed 2 minutes prior to the start of the race.</li>
<li>Bets will not be accepted after post time. In the event the race starts before the advertised post time and for that reason the customers are able to place wagers after the race has started all wagers are void.</li>
<li>USRacing.com will not be liable for wagers that were unsuccessfully entered before post time. We recommend that our clients check their pending wagers before login out of their accounts to ensure the wagers were placed correctly.</li>
<li>All wagers are final once they have been confirmed and submitted.</li>
<li>We reserve the right, at our discretion, to change, modify, add, or remove customers from our rebate programs at any time.</li>
</ol>
<h3>Rules regarding criminal activity, fraud, or collusion.</h3>
<ol>
<li>If reasonable cause to believe that you are involved in any type of criminal activity, fraud, or collusion, we reserve the right to suspend your account pending review. We also reserve the right to freeze your account balance and/or close your account permanently. We reserve the right to deduct from your account the amount of any payouts, bonuses or winnings due to activities which include:</li>
<li>Being linked to any form of collusion, cheating, unfair or deceitful practices, or any other criminal activity.</li>
<li>On bets placed or games played, that you have maliciously gained an unfair advantage on bets or games using collusive or deceitful practices.</li>
<li>That you have been found to engage in deceitful or collusive behaviors at other online gaming businesses including charge backs, or any criminal activities that we deem harmful to our establishment.</li>
<li>We become aware that you have “charged back” or denied any purchases or deposits that you make to your account.</li>
<li>Fraudulent practice is defined as any activity using stolen, cloned or otherwise illegal credit or debit cards to fund an account</li>
<li>Criminal activity is defined as money laundering and/or any other illegal activity</li>
<li>Unfair advantage is defined as the abuse of a fault, loophole, or error in our software. This also includes the use of computers known as ‘bots’ (Robots, or Spiders).</li>
<li>Collusion is defined as the use of multiple accounts to gain an unfair advantage as defined in these rules and limits per account. This includes the use of beards and movers as a way to place wagers.</li>
<li>We will conduct a thorough review of an account in which we believe collusion, cheating, fraud, or illegal activities have taken place.</li>
<li>We reserve the right to inform our business units, processing companies, electronic payment providers or other financial institutions of any unlawful, fraudulent or inappropriate activity.</li>
</ol>
<p><a name="bonus"></a></p>
<h3>Rules Regarding Bonuses</h3>
<ol>
<li>Bonuses at USRacing.com are all subject to the following rules and regulations. Promotions and bonuses are restricted to one per deposit, per person, account, household, email, telephone number; same payment account and shared computer such as school, public library or workplace. No exceptions are made to this rule.</li>
<li>All promotions are for recreational players only, USRacing.com reserves the right to refute or reverse any bonus given to any customer at any time if we determined that the customer is not eligible to take part in such promotion or was found to have abused our bonus and promotion program. Management reserves the right to refute or retract promotions at any time to any customer regardless of the situation.</li>
<li>If a member has numerous accounts, all promotional bonuses or credits achieved and winnings produced from the account will be void and subtracted from the account.</li>
<li>Any customer found to be playing steam plays or who is found to be part of a gambling organization as well as those who misuse our bonus and promotions program will have all promotions cancelled and all winnings from them will be forfeited and cancelled..</li>
<li>Bonuses are only offered on new money deposited to an account, if a client has recently withdrawn funds from his account, the client will be required to make up the total amount of the payout in deposits before being eligible for a bonus.</li>
<li>Bonuses are not credited automatically, you must contact Customer Service upon having made a deposit in order to receive any bonuses or promotions.</li>
<li>Once a bonus has been received it cannot be forfeited by the customer and all terms and conditions regarding bonuses must be followed before any payouts are issued.</li>
<li>For rollover determinations the lesser amount between the risk and the win on each eligible wager is the one that is measured.</li>
<li>Management reserves the right to refute or retract any bonus at any time to any customer it believes is not eligible to receive such promotions.</li>
<li>The “risk” amount of a free play does not count towards any of the rollover.</li>
<li>Rollover means you must wager the quantified amount at least that many times before requesting a payout. To calculate rollover for example, if you deposit $100 and receive a 10% ($10) Re-Up Bonus with a 3 times rollover prerequisite, you would only need an amount of $330 ($110 of the deposit plus the bonus amount given x 3) in action prior to requesting for a payout.</li>
<li>Once you have received a bonus, you cannot request a payout before the rollover obligation is met.</li>
<li>Rollover calculations start from the moment the first wager is placed after a deposit has been made unless otherwise specified in the bonus terms and conditions.</li>
<li>Rollover calculations do not include any pending wagers, only wagers that are settled and have a status of win or loss are tallied in the rollover report.</li>
<li>Wagers that result in a termination due to postponed events or the wager resulting in a push or no action do not count towards the rollover.</li>
<li>In the event that a Free Play wager results in a annulment or no action the customer must contact our wagering department by calling in or by email in order to discuss the amount risked and have it be refunded to your free play balance and permitted to be wagered at a later time.</li>
<li>In the event that a customer funds his account while still having a balance from a previous deposit where a bonus was issued, the client must then finish all rollover and holding periods that are required by each of these deposits before a withdrawal can be made.</li>
<li>Any customer who is given a promotion including no deposit promotions, contest winnings from affiliates or sponsor sites must comply with all terms and regulations of such promotion before any withdrawal can be made.</li>
<li>Promotions cannot be used with other bonuses or promotions.</li>
<li>Restrictions apply on circled and limited games.</li>
<li>USRacing.com considers wagering on both sides of the game using any form of promotional money (free play or cash bonus) duplicitous conduct. This includes making bets of this nature with anyone of our affiliate sites, and any money won from playing both sides of the same game will be deducted along with any other promotional money endorsed to the customer and may result in annulment of the clients wagering account and accruing more penalties based on the offense.</li>
<li>Wagering on both sides of the same game for the sole determination of finishing a rollover obligation is absolutely forbidden and such wagers will not count towards the rollover and any winnings resulting from such activity will be overturned.</li>
<li>All promotional offerings and policies are solely at the discretion of USRacing.com and are subject to annulment or alteration without warning or prior notice.</li>
<li>USRacing.com reserves the right to remove the availability of any and all offers to any customer at any time.</li>
<li>These Bonus and Promotions rules will supplant any verbal or written offering of any employee of USRacing.com.</li>
</ol>
<h3>$1,000 Welcome Bonus Terms and Restrictions</h3>
<ol>
<li>Limited Time Offer. Offer valid until.</li>
<li>This promotion is for NEW accounts only.</li>
<li>To qualify, bet a total of $1,000 within 30 days of opening your account then call us to request your bonus. Upon verification of the required action, we will credit a $1,000 free play to your account.</li>
<li>There is a 15x (ten time) rollover requirement associated with your 100% free play.</li>
<li>Only the winnings of graded Free Bets are paid out.</li>
<li>Free Bet applies to Straight Wagers only.</li>
<li>Free Bet expires within 30 days of issue.</li>
<li>We only allow one Free Bet per account/household or environments where computers are shared. A payout cannot be made until all rollover restrictions have been met. If you receive a payout before bonus terms are met, all bonus funds as well as any winnings made by wagering the bonus will be forfeited.</li>
<li>Bonus is subject to all other USRacing.com.ag Terms and Conditions.</li>
<li>This promotion is void where prohibited by law.</li>
<li>We reserve the right, in our sole discretion, to terminate or modify this promotion at any time without notice. Not valid with other promotions.</li>
</ol>
<h3>Casino Bonus Rules</h3>
<ol>
<li>Casino Bonuses are limited to play in the casino and this includes live or virtual which means that sports action will not be counted towards the rollover.</li>
<li>The minimum deposit amount to qualify for a bonus in the casino is $100 USD,</li>
<li>In the occasion that the client uses the bonus money or its winnings to place a bet on sports in the sportsbook section of the site, the amount of the bet cannot surpass 10 percent the amount of the original bankroll.</li>
<li>There will be no bet in the casino that can surpass 25 percent of the amount of the original bankroll when using any bonus or free play.</li>
<li>All bonuses offered in the USRacing.com Flash Casino carry a 50x rollover of the bonus plus deposit amount, which must be met before any withdrawal request will be approved.</li>
<li>Cash blackjack hands do not count towards a bonus’ rollover requirement.</li>
<li>The following games are not playable with bonus funds: craps, baccarat, roulette and pai gow poker.</li>
<li>Bonuses are only available to accounts with net losses. Accounts which are ahead of the house or have not covered recent payouts will not be eligible to claim any kind of bonus.</li>
<li>Bonuses cannot be issued retroactively.</li>
<li>The maximum bonus that can be claimed at the USRacing.com Flash Casino is $500.</li>
<li>The maximum payout that can be withdrawn at the USRacing.com Flash Casino on winnings is $100.</li>
<li>All bonuses must be claimed by contacting our Customer Service Department. Our toll free number is 866-567-2442</li>
<li>Before claiming a promotion, your account balance must be played down to 0.</li>
<li>Only three promotions can be claimed consecutively. After the third consecutive promotion, a deposit must be made before a new promotion can be considered.</li>
<li>Bonuses offered at the USRacing.com Flash Casino expire 14 days after being credited.</li>
</ol>
<h3>Casino Bonus Elegibity</h3>
<ol>
<li>Bonuses offered at the USRacing.com Flash Casino are intended for recreational bettors only. Players deemed to be professional and/or abusing the bonus system may have their bonus revoked and be subject to further sanctions at the discretion of USRacing.com management.</li>
<li>USRacing.com always reserves the right to refute or reverse any bonus and winnings resulting from such bonuses at any time if they believe a customer is not eligible for the bonuses or the promotions collected.</li>
</ol>
<h3>Free Play and Match Play Rules and Regulations</h3>
<p>Free Plays and Match Plays can only be used on any of the four major sports and only for straight bets on sides and totals. You cannot buy points on free plays. Free plays will be displayed in a different manner from the customer’s cash balance. All rollover requirements must be met by wagering on sports and does not include casino or horse racing action.</p>
<h3>Free Play Restrictions:</h3>
<ul>
<li>Proposition bets or futures are not permissible</li>
<li>Controlled or restricted games are not allowed for free plays.</li>
<li>Maximum wager limits recognized for each sporting event will apply when using a free play.</li>
<li>Free plays are limited to the winnings only.</li>
</ul>
<h3>Cash Bonus Rules</h3>
<p>We offer cash bonuses on select promotions from time to time, these promotions have a limited time offering to qualified clients.<br>
All rollover requirements for cash bonuses must be met by wagering on sports and does not include casino or horse racing action.. As with all bonuses and promotions offered by USRacing.com all rollover and play through periods must be accomplished before any withdrawal can be made.</p>
<h3>Rollover Calculation:</h3>
<p>The formula for the rollover calculation for free play, match play or cash bonuses at USRacing.com is as follows:</p>
<ul>
<li>Formula: The original deposit plus the transfer fees (when applicable), plus bonus amount given, multiplied by the definite rollover requirement of the bonus.</li>
<li>Significant Notice: Maximum winnings permissible from any wager made with a bonus are five thousand US dollars and any amount won above this figure will be overturned. All rollover and hold periods must be accomplished before a payout can be made.</li>
</ul>
<h3>Cash Back Bonus Rules</h3>
<p>USRacing.com offers Cash Back to our players in February, and again in August.<br>
Your Cash Back is calculated by subtracting the amount withdrawn from the amount deposited. We give you a bonus equaling 10% of the difference.</p>
<h3>Rules</h3>
<ol>
<li>Cash Back requests can be submitted February 1-15 and August 1-15.</li>
<li>New accounts must be active for the previous 5 months in order to qualify.</li>
<li>Existing accounts must be active for the previous 6 months in order to qualify.</li>
<li>A minimum of one deposit must be made during the measured period.</li>
<li>Cash Back is calculated using deposits and withdrawals from the specified period.</li>
<li>Account balance and pending withdrawals on February and August 1st will be included in the equation.</li>
<li>In order to maintain qualification clients must make minimum of two wagers every (7) days beginning the 1st day of each month.</li>
<li>Cash Back bonus Free Play cash is subject to a 3X rollover requirement before becoming eligible for withdrawal.</li>
<li>Players that do not fully qualify with six consecutive months sufficient activity may qualify for a prorated Net Losses bonus at management’s discretion.</li>
</ol>
<h3>Referral Bonus Rules</h3>
<p>Please read the following rules as they pertain to all referral bonuses given by USRacing.com</p>
<ol>
<li>Referral bonus requires a 3X rollover requirement before receiving a payout.</li>
<li>Both accounts must have all documents on file before a referral is awarded.</li>
<li>Referrer must have made deposit within last 30 days to receive the referral bonus.</li>
<li>Referrer must request bonus within 72 hours of time of referees initial deposit.</li>
<li>Each account must reside at a different address.</li>
<li>Referrer and referee has no negative transaction history.</li>
<li>Referrer and referee must not engage in syndicate wagering or bonus abuse by playing both sides of same game or line.</li>
<li>Referrer has not received a payout in the past 180 days.</li>
<li>Referrer and referee accounts will be subject to careful review prior to payout approval.</li>
</ol>
<h3 id="Depositterms">Deposits Terms</h3>
<ol>
<li>All deposits are subject to management approval, USRacing.com reserves the right to accept, deny or reverse any deposit.</li>
<li>The use of credit cards and other electronic payment methods is restricted to one account, meaning the same credit card number, bank account cannot be used by more than one client.</li>
<li>Credit cards must be in the name registered in the account with USRacing.com. In the event that the customer wishes to use a card that is not in their name, the deposit must first be approved by management and proper documentation of card must be on file prior to the deposit being made.</li>
<li>Deposit limits vary depending on the method used, any limit increase must be approved by management and USRacing.com reserves the right to request sufficient documentation from any customer to validate the identity of the client before any limits are increased. Providing such documentation will not guarantee any increase in the deposit limits.</li>
<li>Deposit options may vary depending on your location, for a complete list of deposit options please contact our Customer Service team at <a href="skype:8665672442?call" nr="8665672442" pattcc="+1" class="telified" title="Use as phone number" style="color:#c4c2e2;background-color:#ffffdf;-moz-border-radius:3px;cursor:pointer">866-567-2442</a> or via email at help@USRacing.com</li>
<li>Any customer found to be committing fraud of any kind with credit cards or any other electronic payment method will be subject to criminal and/or civil prosecution.</li>
<li>All winnings resulting from the illicit use of credit cards or any other electronic payment method will be automatically forfeited.</li>
<li>The descriptor on your credit card billing statement may vary from one transaction to the next as we use different processing companies. If you are unsure about a charge reflected on your statement please contact our security department to inquire about the charge help@USRacing.com</li>
<li>Credit card deposits are subject to a 30 business day hold before any payout can be processed and proper documentation will be required.</li>
<li>Currently the default currency in our cashier is USD, if your account is in a different currency you must contact our customer service department prior to any deposit to receive the proper instructions.</li>
<li>Deposits that are not processed through our cashier will not be credited automatically; it is the responsibility of the customer to contact our customer service department in order to inform them of the deposit to ensure it is credited in a timely manner.</li>
<li>USRacing.com is not responsible for deposits not notified to its customer service department.</li>
<li>In the event funds are credited to a customer’s account in error, it is incumbent upon the customer to notify USRacing.comof aforesaid error without delay. For accounts with negative balances, USRacing.com reserves the right to cancel any pending plays, whether placed with funds resulting from the error or not.</li>
<li>In the event of delays due to transaction errors or third party processing delays USRacing.com cannot not be held liable for any wagers missed due to the delay.</li>
<li>Fees will be covered on person to person transfers of $300 or more.</li>
<li>USRacing.com will not be liable for any additional fees charged by the members banking institution such as but no limited to; cash advance fees, international processing or currency fees, over draft fees or administrative banking fees.</li>
<li>USRacing.com relies on the service of third party processors and cannot be held liable for any failure on their part.</li>
<li>The availability of deposit methods are subject to change without prior notice.</li>
<li>The house deposit rules will supersede any verbal or written offering of any employee of USRacing.com.</li>
<li>Online Gaming activities may be restricted in some jurisdictions to government owned and managed lotteries, sports and horse wagering and may not be permitted in some cases. It is the sole responsibility of each customer to inform themselves of their local laws before funding their accounts.</li>
</ol>
<h3>How To Fund Your USRacing.com Account</h3>
<ol>
<li>There are several easy deposit methods to fund your USRacing.com account. Please feel free to contact us anytime at <a href="skype:8665672442?call" nr="8665672442" pattcc="+1" class="telified" title="Use as phone number" style="color:#c4c2e2;background-color:#ffffdf;-moz-border-radius:3px;cursor:pointer">866-567-2442</a> should you need any help with your deposits.</li>
<li>If you are sending a Person-to-Person money transfer, you will be charged a fee to send funds. USRacing.com will cover the transfer fees on applicable deposits.</li>
<li>If you are using your credit or debit card, please read the deposit policies carefully, as there are certain requirements that you must complete prior to requesting payout from your account, including a copy of a valid photo id, copies of the front and back of each card used, and a signed verification form.</li>
<li>Gambling may be restricted in some jurisdictions and may not be permitted where you live. It is your responsibility to know and comply with any local laws before funding your account.</li>
</ol>
<h3>Person to Person Money Transfers</h3>
<p>You can deposit to your USRacing.com account by sending a person-to-person cash transfer at a local outlet near you.<br>
Here are the easy steps to send a money transfer:</p>
<ol>
<li>Contact the USRacing.com Customer Service Team at <a href="skype:8665672442?call" nr="8665672442" pattcc="+1" class="telified" title="Use as phone number" style="color:#c4c2e2;background-color:#ffffdf;-moz-border-radius:3px;cursor:pointer">866-567-2442</a> to get the transfer details.</li>
<li>Go to a local money transfer outlet to pay for and send your transfer.</li>
<li>Call us back to give your confirmation number and the funds will be posted to your account in as little as 15 minutes</li>
</ol>
<p>Please note:</p>
<ul>
<li>The minimum deposit is $100 and the maximum is $5000, per transaction (see limits below).</li>
<li>When depositing via this method, you will be charged a fee for the money transfer; however, USRacing.com will reimburse the transfer fees for deposits above $300 USD</li>
<li>It may take up to 15 minutes for the funds to reflect in your USRacing.com account balance after you provided us with the confirmation number.</li>
<li>Fees covered on deposits of $300 or more.</li>
<li>Min: $100 USD</li>
<li>Max: $800 USD</li>
<li>For details on sending a larger person to person money transfer amount please contact customer service.</li>
</ul>
<h3>Cashier Checks</h3>
<ul>
<li>You can send a Cashier Checks to fund your account.</li>
<li>Min: 1,000 USD</li>
<li>Max: 5,000 USD</li>
<li>For details on sending a larger amount please contact customer service.</li>
</ul>
<h3>Deposits Made With Your Credit Card</h3>
<p>You can deposit money into your USRacing.com account using any Visa or Master Card branded credit card or check/debit card. Please ensure the card is eligible for international purchases, as US/domestic-only cards will not work.<br>
When you request a payout, you will need to complete one Credit Card Verification form for each credit card you use to deposit funds along with the following documents:</p>
<ul>
<li>A photocopy of your government issued photo ID (e.g., your driver’s license).</li>
<li>Photocopies of the front and back of your credit card.</li>
</ul>
<p>Please Note</p>
<ul>
<li>When you complete a credit card deposit, the descriptor for this transaction will appear on your credit card statement. Please take a moment to take note of the descriptor for your records.</li>
<li>It may take up to 10 business days for the transaction to show up as a completed debit on your credit card statement. It may show as a pending “Pre-Authorization” until the transaction has been debited by us. A pre-authorization is a temporary “hold” on your card’s available balance for the deposit amount you have made. This is done for any deposit attempts that are started but have yet to be completed and can often apply to failed deposit attempts. This is very common for electronic (online) transactions. Once payment is completed, the “hold” will be replaced by an actual charge to your card. If the transaction failed initially, the pre-authorized funds will become available in your card’s balance.</li>
<li>Many credit and debit cards have restrictions on online purchases or for use on gaming sites. If your card issuer restricts use on our site, we would suggest viewing alternate deposit options that may be convenient to you.</li>
<li>Deposits you make within a few days of one another may be deducted from your balance on the same day. While these may appear as ‘duplicated’ deposits, we ask that you carefully review your statement or contact us for clarification as this is most likely not the case.</li>
<li>If you should require help with reconciling your credit card statement, do not recognize a transaction that is appearing or feel that you may have been over or under charged, please contact us at <a href="skype:8665672442?call" nr="8665672442" pattcc="+1" class="telified" title="Use as phone number" style="color:#c4c2e2;background-color:#ffffdf;-moz-border-radius:3px;cursor:pointer">866-567-2442</a>. We are happy to help you and want to make sure everything is in order.</li>
<li>Remember that before you receive a payout, you are must provide a completed and signed Credit Card Verification Form. If you want to avoid unnecessary delays, we encourage you to send in any documents promptly.</li>
<li>If you are depositing using a Visa credit card, you might be prompted to enter the Verified By Visa password associated with your card.</li>
<li>If you need further assistance, please feel free to call us toll free <a href="skype:8665672442?call" nr="8665672442" pattcc="+1" class="telified" title="Use as phone number" style="color:#c4c2e2;background-color:#ffffdf;-moz-border-radius:3px;cursor:pointer">866-567-2442</a>.</li>
</ul>
<h3>Withdrawal Rules</h3>
<p>Payouts are processed daily on a first come, first served basis. As long as all pre-authorized or pending deposits to your USRacing.com account have settled we can process your payout in as little as 24 hours. Make sure that you have a sufficient balance in your account to cover your payout amount until the funds are removed from your balance, to avoid the payout from being declined. After a payout has been processed and the funds have been debited from your USRacing.com account, the transfer times and fees can be found below.</p>
<ol>
<li>Payout requests are accepted Monday through Sunday between the hours of 8:00 AM and 2:00 PM EST. Payouts will be sent Monday through Friday during normal banking hours.</li>
<li>Withdrawals will only be processed in the name of the account holder, in the event that a client cannot receive the funds in his/her name a written signed authorization must be received with the name of the person to whom the payout is endorsed, in addition to this a valid photo id of the account holder must accompany the authorization form.</li>
<li>The minimum withdrawal amount is $100 USD.</li>
<li>All withdrawal requests must be placed directly with a customer service representative by phone. Email withdrawal requests will not be honored.</li>
<li>Customers must provide account number and password when requesting a payout.</li>
<li>All terms and conditions including rollovers, play through periods and required documentation must be completed and received before a payout can be processed.</li>
<li>All payout requests have a processing period of a minimum of 48 hrs. The submission of a request by a representative on behalf of the customer does not guarantee approval.</li>
<li>All payouts are subject to the funds being available in the account at the time the debit is made; if funds are not available, the payout request will be cancelled.</li>
<li>USRacing.com reserves the right to request proper documentation to verify the account holder’s identity, including a valid photo id and copies of credit cards before processing a payout. Failure to provide this documentation will result in the cancellation of the request.</li>
<li>Customers are responsible for ensuring that all personal information including physical mailing address, phone number and email address are updated and correct, in the event that a redirect or re-send of a payment due to an incorrect address is necessary, there is a administrative fee of $50 USD</li>
<li>Processing of person to person transfer requests may take up to 48 to 72 hours depending on processor availability.</li>
<li>Although we strive to process payouts as quickly as possible, some methods may take longer to process due to transfer procedures and any payout may take up to 10 to 14 business days to be processed.</li>
<li>Depending on the deposit method and/or bonuses received, some payout requests may be subject to additional holding periods of up to 30 days.</li>
<li>Payouts may be sent via another method than the one originally requested on certain occasions, we will notify you of any changes should they be necessary.</li>
<li>Payout requests may be cancelled or modified prior processing, once processed however changes are not allowed, in the event that a payout is cancelled at the customer’s request, any applicable fees will not be reimbursed.</li>
<li>Withdrawal methods, limits and fees are subject to change without prior notification.</li>
<li>Non cashable bonuses will be reversed at the time of a withdrawal.</li>
<li>USRacing.com reserves the right to deny, reverse or hold any withdrawal request if fraud is suspected or if terms and conditions are violated in any way.</li>
<li>Withdrawal rules will have precedence over any verbal or written offering by any employee of the company.</li>
<li>USRacing.com reserves the right, in our sole discretion, to terminate or modify these rules at any time without notice.</li>
</ol>
<p>Additionally:</p>
<ol>
<li>If you have pending deposits to your USRacing.com account, we cannot process your payout until we receive the funds. This may take up to 5 business days after you make a deposit.</li>
<li>We can only process one payout request at a time. If you want to request another payout, please cancel the one that is pending.</li>
<li>Make sure that your address and account information is correct before making a payout request.</li>
<li>You may be required to submit additional information for account verification.</li>
<li>All rollover requirements must be met in order to process a payout, fees are subject to change without prior notice however we will take all precautions to ensure you are informed of any change in the fee structure.</li>
</ol>
*}
<p>This agreement describes the terms and conditions applicable to the use of the account wagering services provided at BUSR. US Racing is an online horse racing website that provides information about horse racing and betting. US Racing does not conduct any gambling activities.  All accounts created via links or banners on the US Racing website are BUSR wagering accounts and, as such, are subject to and protected by BUSR’s Terms and Conditions and Privacy Policy. The use of our services and products provided by BUSR will be taken as your acceptance of all of these terms, conditions, and disclosures contained herein.</p>

<p><b>House Rules</b></p>
<p>Our rules are governed on the principle of fair play. Horse wagering is a game of skill. The challenge is to gather and assess the information available, use your own knowledge and then compare your opinion to that of our linesmaker and/or the results at the racetrack after a race has been completed.</p>
<p>If you choose correctly you win.</p>
<p>All rules, regulations, and payoffs contained herein are subject to change and revision by the Management without prior notice.</p>
<p>AGE RESTRICTIONS</p>
<p>No one under the age of 18 years is permitted to wager on and products or platforms provided by our gaming provider.</p>
<p>DISCREPANCIES/DISPUTES</p>
<p>All members should verify their balances on each account log-in and prior to wagering.  When you verify and accept your account balance you agree that all previous transactions are correct and you do not have any claims.  Claims or disputes must be settled prior to making any further wagers.  All internet transactions will be logged and the log file will also be saved on a back-up system.  Any correspondence via telephone, email or live chat will be recorded and stored on file.</p>
<p>Notwithstanding anything in this agreement, in the event of any dispute regarding a wager or winnings, the decision of Management will be final and binding in all matters.</p>
<p>TAXES</p>
<p>Management will not disclose details of a member’s net winnings or losses except as required under applicable law.  If you reside in a jurisdiction where your winnings are taxable, it is your responsibility to comply with any appropriate laws.</p>
<p>LIMITATION OF PLAYS and/or TERMINATION OF AGREEMENT</p>
<p>The agreement between US Racing and the member may be terminated at any time upon the request of the member as long as there is no balance and no pending wagers.</p>
<p>Or, if deemed necessary, US Racing or its gaming provider reserve the right to limit a member’s plays or revoke the agreement between the member and US Racing. Reasons for limitation of plays or revocation of agreement include, but are not limited to:</p>
<p><b>Betting Syndicates</b>: Management reserves the right to limit or exclude any player who attempts to defraud the house either on his/her own or in collusion with other players or other racebooks or sportsbooks. Other sanctions could include:</p>
<ul>
  <li >Administration fees</li>
  <li >Refusal of payouts</li>
  <li >Reversal of wagering transactions</li>
</ul>
<p><b>Abuse of System Vulnerability</b>: If US Racing’s gaming provider determines that a member is taking advantage of a vulnerability in the system (software error, malfunction or ” bug”), we reserve the right to take some or all of the following actions:</p>
<ul>
  <li >Limit wagers</li>
  <li >Close the account</li>
  <li >Confiscate monies in the account, including deposits and winnings.</li>
</ul>
<p><b>Criminal Activity</b>: If there is reason to believe that criminal or other suspicious activities are occurring through one or more accounts (including attempted money-laundering or fraud), our Gaming Provider reserves the right to close those accounts and/or report such activity to the Gaming Commission and/or other regulatory bodies or services. All account balances (including both deposits and any winnings) shall be forfeited.</p>
<p>PASSWORD</p>
<p>Members are solely responsible for the security of their passwords.  Should you inadvertently let someone else know your password you must immediately change it via the secure “My Account” area of the website.</p>
<p>Members are responsible for any unauthorized use of their accounts.  In the event that a third party places a bet, or is thought to have placed a bet, the said bet shall be valid whether or not the alleged third party had the prior consent of the member.  Under no circumstances will any bet be set to “no action” for that reason.  If you suspect that a third party may have access to your password or username, you should proceed to the secure “My Account” area and change your password.</p>
<p>When choosing a password, Our Gaming provider recommends you follow these guidelines:</p>
<ul>
  <li >Do not use all lower case letters</li>
  <li >Do use numbers and or symbols</li>
  <li >Do use a mix of upper and lower case letters</li>
</ul>
<p>BONUSES</p>
<ol >
  <li >Members of US Racing are eligible to receive bonuses and/or free bets from time to time, including a bonus when signing-up and for each referral.</li>
  <li >Bonuses and/or free bets shall be non-transferable and non-refundable.</li>
  <li >We only allow one bonus/free bet per account/household or environments where computers are shared.</li>
  <li >A bonus/free bet is only valid if the rollover requirement associated with the said bonus has been fulfilled.</li>
  <li >These requirements/terms may vary depending on the bonus/free bet given and you must always refer to the terms and conditions of the particular bonus for clarification.</li>
  <li >Please note that bonuses/free bets are not cumulative, for example, we offer a standard 25% signup bonus; this cannot be combined with a 25% promotion or special to become a 50% bonus.</li>
  <li >Poker bonuses fall under an entirely separate agreement and are awarded and released within the poker application.</li>
  <li >>Match-play bonuses awarded in the Casino require a real money deposit to be made before the member is eligible to receive these bonuses. Match-play bonuses are only paid out on ante bets, i.e., the initial amount wagered. Any further play bets are not included in the Match Bet offer.</li>
  <li >9.25% Sign-up bonuses, reload bonuses and other cash rewards are subject to the following standard rollover requirements:</li>
  <ul>
    <li >Sports-betting activity equal to 30 times the amount of the bonus.</li>
    <li >Casino activity equal to 100 times the amount of the bonus. Craps and Roulette (all variations) and Certain Casino games, as specified in the Casino Rules, do not apply towards rollover requirements.</li>
    <li >Blackjack activity equal to 200 times the amount of the bonus.</li>
    <li >Rollover requirements must be met within 15 days of your deposit.</li>
    <li >These bonuses cannot be transferred to the poker room.</li>
    <li >10.Our current bonus policies can be viewed at: <a href="http://www.usracing.com/cash-bonus">Bonus Policy</a></li>
    <li >11.The recycling of funds is not permitted. Requesting a withdrawal to make a deposit with little or no activity between the withdrawal and deposit does not earn a bonus.</li>
    <li >12.When you withdraw, some (or all) of your bonus funds may be removed from your account if their rollover requirements have not been met. The amount removed is proportional to the amount you are withdrawing: for example, if you are withdrawing 25% of your available balance, 25% of your bonus is removed. If you are withdrawing 100% of your available balance, 100% of your bonus is removed. This does not apply to free bets.</li>
    <li >13.Members must play their own money in order to earn a bonus. If you deposit, receive a bonus and then withdraw the amount of the deposit without risking your own funds, we will deduct both the bonus and the winnings.</li>
    <li >14.Management reserves the right to revoke a bonus and winnings derived from the revoked bonus at any time.</li>
    <li >15.Real money play in the Poker Room, Live Casino and/or Racebook does not count towards the rollover requirement of bonuses awarded in sports betting and flash/download casino areas of the site.</li>
    <li ><b>16.</b><b>Refer a Friend Promotion</b><br>
The following terms and conditions apply:</li>
  </ul>
</ol>
<ol >
  <ol >
    <li >a.To be eligible for this offer the Referrer must have deposited before the Referred Friend.</li>
    <li >b.Referred Friend” Minimum initial deposit should be $100.</li>
    <li >c.Maximum referral bonus is $200: $100 as free cash bonus and $100 as free bet.</li>
    <li >d.Free bet expires 15 days from the date of issue.</li>
    <li >e.Bonus will be paid seven days after the Referred Friend has made their first deposit.</li>
    <li >f.In order to be eligible as a valid referral, the Referred Friend must have used at least 50% of his initial deposit and must not have requested a withdrawal before completing the wagering requirement on his own Sign-Up Bonus.</li>
    <li >g.If, after seven days of the initial deposit, 50% of the amount has not been wagered, your referral bonus will be credited once it is wagered.</li>
    <li >h.The Referred Friend still receives their 25% Free Cash Sign-up Bonus offer even if they have not met the wagering requirements for a referral bonus.</li>
    <li >i.The Referred Friend” 25% signup bonus offer supersedes all other signup bonus offers.</li>
    <li >j.There is no limit to the number of times you’re credited a referral bonus, but limited to one bonus per Referred Friend.</li>
    <li >k.Opening multiple accounts is strictly against the house rules. If the referee already has an account with us (Inactive or otherwise) you will not be entitled to receive any referral bonus funds.</li>
    <li >l.All referral claims for credit must be made within 60 days of your friend/acquaintance (the referee) signing up.</li>
  </ol>
</ol>
<p><b>Free money or Cash Code Bonuses are subject to the following restrictions:</b><br>
If a member attempts to withdraw winnings derived entirely from a free cash bonus prior to meeting the requirements specified in the promotion, both the winnings and promotional bonus shall be forfeited.</p>
<p><b>Abuse of Bonus Programs:</b></p>
<p>Our bonus program is intended for recreational bettors only.  Our Gaming provider reserves the right to revoke bonuses for and further sanction any member considered to be abusing the bonus system. Bonus abuse includes:</p>
<ul>
  <ul>
    <li >Cashing out for the purpose of redepositing</li>
    <li >Betting both sides of a single event for the purpose of falsely achieving bonus rollover requirements</li>
    <li >Referring new accounts for your own use</li>
  </ul>
</ul>
<p>The sanctions may take the form of:</p>
<ul>
  <ul>
    <li >Increased rollover requirement</li>
    <li >Loss of bonus privileges for the offending account and any linked accounts</li>
    <li >Geographical restrictions on bonus eligibility</li>
  </ul>
</ul>
<p>WITHDRAWING FUNDS</p>
<p>We accept withdrawal requests 24 hours a day, 7 days a week and the minimum withdrawal is $50. There is a maximum of 1 withdrawal request per day.</p>
<p>Unless otherwise stated, all members will be entitled to one free withdrawal per calendar year. Additional withdrawals sent within the same calendar year will incur a fee.</p>
<p><b>Please note:</b> On occasions we may have to send your withdrawal via a method other than the one you have chosen.  In the event that the method is changed, we will notify you.</p>
<p>Your Personal Identification Number (PIN) is required before you can withdraw any funds from Our Gaming provider.  It is a further step in securing that only you can access money in your account.  This number was sent to you via email when you signed up and can be retrieved at any time via the secure “My Account” area of the site.  You are solely responsible for the security of your PIN.</p>
<p>Before you request a withdrawal, please make sure your address is correct and up-to-date.</p>
<p>If a stop payment is requested by the member, it is the member’s responsibility to assume the stop-payment fees of a minimum of $50.</p>
<p>A <b>pending</b> withdrawal may be cancelled if desired; you must access the Withdrawal Confirm Cancel page within the Cashier by selecting “Cancel” from either:</p>
<ul>
  <ul>
    <li >Withdrawal Success Page</li>
    <li >Withdrawal/Transaction History page</li>
  </ul>
</ul>
<p>Once the withdrawal cancellation is submitted the money will be credited back to your wagering account immediately.</p>
<p>If Fedex are unable to deliver your package they will hold it at the local depot/office for a further period (usually about 7 days). You can call them quoting the tracking number and arrange a convenient time for delivery or arrange to collect the package yourself from the local depot/office. If the given time elapses and you have not contacted Fedex they will return the package to us, and once received we will credit the funds back into your wagering account.</p>
<p>If your check has been delivered to a previous residence please try and retrieve the check from that residence if at all possible. This is more likely to occur when delivering to apartments where the concierge or receptionist will sign for the package.</p>
<p>If all these avenues have been exhausted and you still do not have the check in your hands, please contact us and we will place a stop on the check. Once we ask the issuing bank to stop payment, we have to wait out the clearing process. This process may take from 2 – 8 weeks, or longer, to complete and may cost you up to $250. Once the ” stop” is confirmed, the funds are placed back in your wagering account (less the stop check fee which could be as much as $250) so the withdrawal process can be started again.</p>
<p>REVERSED DEPOSITS</p>
<p>Whenever a deposit is stopped/reversed Management reserves the right to void all activity in the account and the account holder will forfeit any accounts winnings deriving from the stopped/reversed deposit.</p>
<p>DORMANT AND ABANDONED ACCOUNTS</p>
<p>If, after your first deposit, there is no activity on your account within 7 days, the deposit will be refunded to your card.</p>
<p>If no real money transaction has been recorded on an account for more than twelve (12) months it shall be determined to be ” dormant” and we shall lock the account for the member” protection.  If requested by the member, the account will be unlocked, but the member will have to supply the correct answers to all security questions.  The unlocking of an account must also be approved by the Management.</p>
<p><b><i>“Management has the right to charge a $100 monthly account management fee on all accounts where there has been no activity in the preceding 12 months. Players who wish to return to play with us are eligible to have any account management fee previously charged re-credited back to their account with Management’s approval. Please note: for accounts located within a restricted region the monthly account management fee will be charged within 30 days of inactivity or the date of account. Additional Management fees may apply.”</i></b></p>
<p>If no real money transaction has been recorded on an account for more than five (5) years it shall be determined to be “abandoned” and Management is obliged to inform the member of any balance remaining in the account. If the member cannot be contacted then the balance will be remitted to the Antigua and Barbuda Financial Services Regulatory Commission where it will be held securely for the member.</p>
<p>CONFIDENTIALITY</p>
<p>US Racing shall undertake to maintain your anonymity unless you agree to your identity being used for future publicity or other purposes.</p>
<p>LIABILITY OF US RACING’s GAMING PROVIDER</p>
<p><b>1.</b> The decisions of the Management will be final and binding in all matters between the Gaming Provider and members.<br>
<b>2.</b> Laws regarding gaming vary throughout the world and internet casino gambling may be unlawful in some jurisdictions; it is the responsibility of members to ensure that they understand and comply fully with any laws or regulations relevant to themselves in their own country/state/locale.<br>
<b>3.</b> Management does not undertake to notify members that they have outstanding balances to collect.<br>
<b>4.</b> Any wager made by the member will be member’s liability in respect to any claim or loss.<br>
<b>5.</b> Management shall reserve the right to suspend or withdraw any game at its absolute discretion.<br>
<b>6.</b> All personal details of all members will be held in confidence unless members agree to their identities and details being used for future publicity purposes.<br>
<b>7.</b> Management reserves the right to at any time, request documentation from a member before releasing a withdrawal or for general auditing or identification purposes. We also reserve the right to request additional documentation or photographic proof of yourself with the documentation.<br>
<b>8.</b> Withdrawals based on account closures are subject to a $100 processing fee.<br>
<b>9.</b> In the event of the cancellation of any race or game for any reason, Management will not be liable.<br>
<b>10.</b> Management will not be liable in the following scenarios:<br>
In the event of force majeure the failure of the US Racing or its Gaming Provider’s central computer system or any part thereof for delays, losses, errors, or omissions resulting from failure of any telecommunications or any other data transmission system for any loss as a result of any act of God for an outbreak of hostilities, riot, civil disturbance, acts of terrorism for the acts of Government or authority (including refusal or revocation of any license or consent) for fire, explosion, flood, theft, malicious damage, strike, lockout, or industrial action of any kind.<br>
<b>11.</b> Management and the member shall not commit or purport to commit the other to honor any obligation other than is specifically provided for by these rules.<br>
<b>12.</b> US Racing and its Gaming Provider accept no liability for any damages, which may be caused to the member by the interception or misuse of any information transmitted over the internet.<br>
<b>13.</b> These rules constitute the entire agreement and understanding between US Racing, its Gaming Provider and the member.<br>
<b>14.</b> The maximum dollar amount that may be won by an individual member on a weekly basis is US$100,000.00. Please note, our weeks run Tuesday thru Monday. See below for large parlays and progressive jackpots.<br>
<b>15.</b> In the event that $50,000 or more of the weekly winnings is the result of a winning a 10, 11 or 12 team parlay or the $100,000.00 ” Pick 13″ Parlay Challenge, then the weekly maximum winnings is raised to $200,000.<br>
<b>16.</b> In the event of a member winning in excess of $50,000 on a progressive jackpot in any of the casino games where it is offered, the member agrees to be photographed while receiving a check from one of our representatives and gives US Racing and its Gaming Provider the exclusive right to use the photographic material at its own discretion.  The weekly limit on winnings of $100,000 does not apply to progressive jackpot winnings.<br>
<b>17.</b> Management reserves the right to refuse or limit any wager.<br>
<b>18.</b> Warning:  Gambling involves risk.  By gambling on this website, you run the risk that you may lose money or suffer psychological injuries.  You gamble at your own risk.</p>
<p>All gaming which is marketed by US Racing is provided by its Gaming Provider. US Racing is an online horse racing information website; it does not conduct any real money or play for fun gaming in any form. US Racing licenses its trademark and provides marketing and consulting services to companies involved in the horse racing industry.</p>
<p>OUR SOCIAL RESPONSIBILITY</p>
<p>As an internationally respected online gaming company we are committed to promoting a responsible attitude to gambling.</p>
<p>For most people gambling is entertainment – an enjoyable activity that has no harmful effect.  But for some it becomes a serious problem.  Compulsive gambling is not easily detected and individuals with gambling problems will often go to great lengths to disguise the addiction and the problems that it causes.</p>
<p>We are concerned about problem gambling.  We will immediately inactivate any wagering account belonging to a member for whom gambling has become a problem and that account will remain permanently closed.  Furthermore, any new accounts opened by that member will be inactivated.</p>
<p>Neither do we condone underage gambling.   Members must be at least 18 years of age.</p>

<p>HORSE RACING</p>
<p>US Racing offers daily racing from tracks within the USA (and internationally) via the racing interface which is provided by its Gaming Provider and which is accessible once you have logged-in to your wagering account.</p>
<p>We accept wagers up until post time. We cannot assume liability for wagers that are unsuccessfully entered before post time. Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted.</p>
<p>Once you submit a bet online, your wager is considered final.</p>
<p>Bets shall not be accepted after post-time.  In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started,<br>
that wager will be void.</p>
<p><b>Track Categories</b></p>
<p>Click here for an up-to-date list of tracks offered and their categories.</p>
<p><b>Odds and Limits</b></p>
<p>Each event has a bet limit (typically $1,000.00).</p>
<p>Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the tables below.</p>
<p>Payouts are based on the actual track payout. The morning lines provided when wagering on the Horse Racing Interface are for informational purposes only and not used to calculate the payout.</p>
<p>There are no house odds.  If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded.</p>
<table class="data" id="infoEntries" cellpadding="0" cellspacing="0" border="0" width="95%">
              
  <tbody><tr>
    <th class="data" colspan="2" height="15"><p style="data" align="center">Class A Tracks</p></th>
    <th class="data" colspan="2" valign="top"><p style="data" align="center">Class B Tracks</p></th>  </tr>
  <tr>

    <td class="data" width="25%"><p align="center"><strong>Bet Type</strong></p></td>
    <td class="data" width="25%"><p align="center"><strong>Max. Payoff</strong></p></td>
    <td class="data" width="25%"><p align="center"><strong>Bet Type</strong></p></td>
    <td class="data" width="25%"><p align="center"><strong>Max Payoff</strong></p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Win</p></td>

    <td valign="top"><p align="center">Track odds</p></td>
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr>
    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds</p></td>

    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
    <td valign="top"><p align="center">Show</p></td>

    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr>
    <td valign="top"><p align="center">Exacta</p></td>
    <td valign="top"><p align="center">500-1</p></td>
    <td valign="top"><p align="center">Exacta</p></td>
    <td valign="top"><p align="center">400-1</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Quinella</p></td>
    <td valign="top"><p align="center">400-1</p></td>
    <td valign="top"><p align="center">Quinella</p></td>
    <td valign="top"><p align="center">300-1</p></td>
  </tr>

  <tr>
    <td valign="top"><p align="center">Trifecta</p></td>
    <td valign="top"><p align="center">1000-1</p></td>
    <td valign="top"><p align="center">Trifecta</p></td>
    <td valign="top"><p align="center">750-1</p></td>
  </tr>
  <tr class="odd">

    <td valign="top"><p align="center">Superfecta</p></td>
    <td valign="top"><p align="center">1500-1</p></td>
    <td valign="top"><p align="center">Superfecta</p></td>
    <td valign="top"><p align="center">1000-1</p></td>
  </tr>
  <tr>
    <td valign="top"><p align="center">Pick 3</p></td>

    <td valign="top"><p align="center">1500-1</p></td>
    <td valign="top"><p align="center">Pick 3</p></td>
    <td valign="top"><p align="center">1000-1</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Pick 4</p></td>
    <td valign="top"><p align="center">5000-1</p></td>

    <td valign="top"><p align="center">Pick 4</p></td>
    <td valign="top"><p align="center">2000-1</p></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Daily Double</p></td>
    <td valign="top"><p align="center">750-1</p></td>
    <td valign="top"><p align="center">Daily Double</p></td>
    <td valign="top"><p align="center">400-1</p></td>
  </tr>

  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr>
<th class="data" colspan="2" height="15"><p style="data" align="center">Class C Tracks</p>
</th>
    <th class="data" colspan="2" height="15"><p style="data" align="center">Class D Tracks (Max Bet = $250)</p>
  </th></tr>
  <tr class="odd">
    <td><p align="center"><strong>Bet Type</strong></p></td>
    <td><p align="center"><strong>Max. Payoff</strong></p></td>
     <td><p align="center"><strong>Bet Type</strong></p></td>
    <td><p align="center"><strong>Max. Payoff</strong></p></td>
  </tr>

  <tr>
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Place</p></td>

    <td valign="top"><p align="center">Track odds</p></td>
    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr>
    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds</p></td>

    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds</p></td>
  </tr>
  <tr class="odd">
    <td colspan="2"><p align="center"><strong>All Exotics 300-1</strong></p></td>
    <td colspan="2"><p align="center"><strong>All Exotics 300-1</strong></p></td>
  </tr>
</tbody></table>
<p>Maximum pay-outs per event are as follows:</p>
<ul>
  <ul>
    <li >Class A tracks: $20,000.00</li>
    <li >Class B tracks: $15,000.00</li>
    <li >Class C tracks: $10,000.00</li>
    <li >Class D tracks: $3,000.00</li>
    <li >Class E tracks: $1,000.00</li>
  </ul>
</ul>
<p>In the event a new track is not listed in one of the categories, its default payout limit will be Category E.</p>
<p>In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded.</p>
<p>Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice.</p>
<p><b>Scratches</b></p>
<p>If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded.  <i>(This does not apply to ” Odds to Win” bets placed via the sports betting interface.  On those bets a scratch counts as a loss). </i> In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly.</p>
<p>In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded.  We do not honor ” all” payouts in any exotic wager even though the track may do so.  In the case that there is an ” all” payout on a particular race, we will replace the ” all” payout with the rightful winner of that race and/or position.</p>
<p>On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse.  There will be no consolation payouts.  No special track payouts will be recognized:</p>
<p><b><i>Pick 3 and Pick 4 Grading and Payout Rules</i></b></p>
<p>All Pick 3 and Pick 4 tickets are paid out based on the official race results  published by the track at which the race was run.  If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded.</p>
<p>There will be no consolation payouts.  No special track payouts (such as payouts on scratched horses, 2 out of 3 special Pick 3 payouts and/or 3 out of 4 special Pick 4 payouts) will be recognized.</p>
<p><b>Official Source of Results</b></p>
<p>For thoroughbred racing we use <a href="http://www.equibase.com/%22%20%5Ct%20%22_blank">www.equibase.com</a> as our official source of results.  For harness racing we use <a href="http://www.ustrotting.com/">www.ustrotting.com</a> or the tracks’s.</p>
<p>In the event of a discrepancy, we will contact the track for the official result.</p>
<p><b>Daily Rebate</b></p>
<p>US Racing offers a generous rebate program:</p>
<table width="313.0" cellspacing="0" cellpadding="0" class="t3">
  <tbody>
    <tr>
      <td valign="top" class="td1">
        
      </td>
      <td valign="top" class="td1">
       <b>Straights</b>
      </td>
      <td valign="top" class="td1">
        <b>Exotics</b>
      </td>
    </tr>
    <tr>
      <td valign="top" class="td1">
        <b>Category A</b>
      </td>
      <td valign="top" class="td1">
       3%
      </td>
      <td valign="top" class="td1">
        8%
      </td>
    </tr>
    <tr>
      <td valign="top" class="td1">
       <b>Category B</b>
      </td>
      <td valign="top" class="td1">
        3%
      </td>
      <td valign="top" class="td1">
        5%
      </td>
    </tr>
    <tr>
      <td valign="top" class="td1">
        <b>Category C</b>
      </td>
      <td valign="top" class="td1">
        3%
      </td>
    </tr>
    <tr>
      <td valign="top" class="td1">
        <b>Category D</b>
      </td>
      <td valign="top" class="td1">
        No rebate
      </td>
    </tr>
    <tr>
      <td valign="top" class="td1">
       <b>Category E</b>
      </td>
      <td valign="top" class="td1">
        <p>No rebate
      </td>
    </tr>
  </tbody>
</table>
<p>This rebate only applies to wagers placed through the horse-wagering interface.  It does not apply to any horse matchups or other future wagers made through the sports betting login.  No rebate will be given on cancelled wagers or wagers refunded due to a scratch.  No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less.  The rebate bonus will be paid on a daily basis.  There is no rollover requirement associated with the rebate.</p>
<p><b>Matchups</b></p>
<p>The following rules apply to horse matchup bets made via the sports betting interface.</p>
<ul>
  <ul>
    <li >In matchups between two or more named horses, all horses must go for wagers to have action.</li>
    <li >Matchups are determined by horse name. Any track coupling is irrelevant towards determining the winner of a matchup.</li>
    <li >In matchups between one named horse and the Field, wagers will have action if and only if the named horse starts. Scratches of horses in the Field will not affect the standing of the wager.</li>
    <li >Matchups are not eligible for rebates.</li>
    <li >The winner of a matchup will be determined by the highest placed finisher involved in the matchup. If no horse finishes, the matchup will be graded no action.</li>
    <li >Matchup wagers which indicate “Turf Only” will be cancelled if the surface is changed.</li>
    <li >In the case of a dead heat, the matchup will be graded no action.</li>
  </ul>
</ul>
<p>PROMOTIONS</p>
<p>If, as part of a promotion offered by US Racing, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company.  You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion.</p>
<p>Only members of US Racing or its Gaming Provider can redeem promotional prizes and winnings from promotions.</p>
<p>If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account.</p>
<p>If a member wagers both sides of a game using a promotional bet (to bet at the same time on the favorite and the underdog) or using bonus money, Management considers that to be fraudulent behavior.  This includes making bets of this nature in two or more of our affiliated websites.<b>  </b>Any money won playing both sides of the game will be deducted, together with any other promotional money deposited in the member” account.</p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
<p class="p10"><br></p>
<p>SPORTING EVENTS</p>
<p>Las Vegas rules, regulations, payoffs and wager types apply where not covered herein.</p>
<p>All sporting events with the exception of the Super Bowl* must be played on the date and site scheduled unless otherwise specified. Sporting events postponed by more than 12 hours and or rescheduled, will constitute ” no-action” and all money will be credited accordingly.</p>
<p>All bets must be made and accepted before the start of the game/event wagered upon.  Any bet placed or received after the start of the game/event wagered upon will be void.</p>
<p>Management reserves the right to limit the maximum amount wagered prior to acceptance of any bet. Members are not permitted to open multiple accounts (either within US Racing itself or affiliated websites on the same platform) in order to circumvent the limits imposed by our wagering system.  If multiple accounts are used then all bets will be void.</p>
<p>In the event that monies are credited to a member’s account in error, it is incumbent upon the member to notify Management of the error without delay.  Any winnings realized after the error, whether directly or indirectly subsequent to the error but prior to notification of Management, shall be void.</p>
<p>Management does not recognize suspended games, protests, scoring amendments, result reversals and overturned decisions for wagering purposes.</p>
<p>In order for a bet to be valid, the member must receive a bet identification number from the Gaming Provider confirming that the bet has been accepted.  This number appears via a confirmation message that will appear on your betting ticket.</p>
<p>In general, accepted bets cannot be cancelled or amended in any way, either by the member or by Management, except in the following circumstances:</p>
<ul>
  <ul>
    <li >In the unusual case that a bet is duplicated by the system (the time-stamp on the two bets will be identical) one of the bets may be voided as long as the member contacts us before the event has taken place.</li>
    <li >If the member has placed a bet that is stated as forbidden in our House Rules then Management reserves the right to void that bet regardless of whether the game has started or not.</li>
    <li >If the bet is placed after the result is public knowledge (Exotics, Television, Politics, etc).</li>
  </ul>
</ul>
<p>Every effort will be made to ensure that all information available on the internet site is accurate.  However, if an error is made in the prices or conditions published on-line, we reserve the right to either correct any mistakes or to void the affected bets or to settle any bets already laid at the correct price.</p>
<p><b>Winners and losers are official after:</b></p>
<ul>
  <ul>
    <li >Football – NCAA &amp; NFL – 55 minutes of play</li>
    <li >Basketball – NBA – 43 minutes of play</li>
    <li >Basketball – NCAA – 35 minutes of play</li>
    <li >Hockey – NHL – 55 minutes of play</li>
    <li >All other sporting events – 55 minutes of play</li>
    <li >Overtime periods, quarters or extra innings are counted in the final score when wagering on totals, money line and spread betting.</li>
    <li >On half time wagers, overtime periods are included as part of the 2nd half.</li>
    <li >Other sports: All other contests that involve a scheduled length of play or time limit must play to their conclusion or have five minutes or less of the scheduled playing time remaining when the contest concludes to be considered official for wagering purposes.</li>
  </ul>
</ul>
<p><b>All Bets Action:</b>  On certain future (long term) bets there may be a clause stating: ” All Bets Action”   This means that the wager will have action whether or not the contestant on whom you have bet takes part in the tournament/event.</p>
<p><b>Live Scores disclaimer:</b>  We are not responsible for any inaccuracies in scores posted in our live scores.</p>
<p>*All wagers on the Super Bowl stand even if the date, time or site has been changed.</p>
<p>Special Football Season Win Rules</p>
<p>” NFL Season Win Totals” only include regular season games and all 16 regular season games must be played for action.  Play-off games and pre-season games do not count for this bet offering.</p>
<p>” NCAA Season Win Totals” only include regular season games.  Each team must play their scheduled 12 regular season games.  Championship games and Bowl games do not count for this bet offering.  Teams must play all scheduled opponents.</p>
<p>Special Major League Baseball Rules</p>
<p>Major League Baseball games are official after 5 innings of play, or 4″ innings if the home team is leading.  If a game is subsequently called or suspended, the winner is determined by the score after the last full inning unless the home team ties the score or takes the lead in the bottom half of the inning in which the game was suspended.  In that case the winner will be determined by the score at the time the game is called.  (This rule holds for betting purposes even if the game is suspended and /or completed on a different day than it began).</p>
<p>When wagering on total runs or run lines, the game must go the regulation 9 innings, or 8″ ” “” innings if the home team is leading, otherwise it will constitute a ” no-action” wager, and all money will be credited accordingly.</p>
<p>Baseball wagers are accepted as ” Listed. ”   In the event of a pitching change prior to the game, all wagers on the game with said listed pitchers are considered ” no-action” .  All wagers will be refunded to members” accounts as soon as official confirmation is received allowing you to make the bet again with the new pitcher and the new price if you so wish.  Starting pitchers must throw at least one pitch for wager to be deemed ” Action” .  Each team” starting pitcher is defined for betting purposes as the pitcher who throws the first pitch.</p>
<p>Any and all baseball series wagers must have all 3 games played by the end of a specified date for the wager to have action.  If all 3 games are not played by that specified date then all wagers have ” no-action” , regardless if one team has 2 wins.  All 3 games must be official by Las Vegas Gaming Standards (see our rules regarding baseball wagers and what constitutes an official result).</p>
<p>Note:  Each team” starting pitcher is defined for betting purposes as the pitcher that throws the first pitch.</p>
<p>Note:  If we provide a baseball series price for a series that has 4 games, for grading purposes, the wager is based on the initial 3 games.</p>
<p>All baseball props must go the full 9 innings (8″ if the home team is winning) unless otherwise stated.  They also must start with the listed pitchers.  If a game is suspended and/or completed on a different day than it began, props will be graded ” no-action” unless otherwise stated.</p>
<p>In baseball, all games that go at least the full 9 innings and finish tied as a result of a suspension of play, shall be graded as a push on all money line bets.  However, all run line and total bets will have action.  Note that this is an extremely rare occurrence.  In the event of a change in Las Vegas rules, Management will adhere to the decision set forth in Las Vegas.</p>
<p>” First Five Innings Bets” are accepted as “listed pitchers” and the game must go a full 5 innings to have action regardless if the game is called or postponed after the 5th inning.</p>
<p>” Team to Score First” props are accepted as ” listed pitchers” and will be graded as soon as the first team has scored and the home team has had a turn at bat.  If the home team scores first, that bet will be graded at that time and bets are action regardless of game suspension or postponement.   If the visiting team scores first, the bet will be graded once the home team has completed its turn at bat.</p>
<p>” Will there be a Score in the First Inning? ” props are accepted as” listed pitchers” and will be graded as soon as the first inning ends.  All bets will be action regardless of game suspension or postponement, as long as the first inning is completed.</p>
<p>” Total Runs + Hits + Errors” props are accepted as ” listed pitchers” and the game must go the full 9 innings (8″ ” ” ” if the home team is winning) for bets to have action.</p>
<p>For ” In-Game Betting” games must go a full 8″ ” innings for bets to have action.</p>
<p>Baseball Player and Pitcher Props are accepted as ” listed pitchers” .  The game must go a full 9 innings (8″ “if the home team is winning) for bets to have action.  If there is a pitching change, all player props for that game will be closed immediately.  When the game starts and it is confirmed that a pitcher other than the pitcher listed on the bet threw out the first pitch, those props will have ” no-action” .  (If the game is suspended and/or completed on a different day than it began, Player/Pitcher Props will have ” no-action” ).</p>
<p><b>Baseball Grand Salami:</b> The Baseball Grand Salami will be decided by the total runs scored in all major-league games scheduled for that day. All scheduled games must go at least 9 innings (8.5 if the home team is winning). If any scheduled game is canceled or stopped before the completion of 8.5 innings, all wagers on the Baseball Grand Salami will be canceled. No pitchers will be listed for the Grand Salami; all wagers will have action regardless of the starting pitchers.</p>
<p><b>Other Baseball Tournaments or Leagues</b></p>
<p>For NCAA Baseball games, or any other non-MLB league, pitchers are generally not listed or required so pitching changes will not affect the bet. If pitchers are listed, games will be treated in exactly the same way as MLB games.</p>
<p>College World Series Moneylines, Runlines and Totals are all action regardless of when the game is completed. Unlike MLB rules, if a game is postponed your bet will not be graded until the completion of that game, at which point all bets will be graded and paid.</p>
<p>Boxing</p>
<p>When the bell sounds for the first round, the bout will be considered official, regardless of the scheduled length or title.</p>
<p>The official stopping of a round before the sounding of the bell does not constitute a full round.  A full round is only considered for wagering purposes when the bell sounds signifying the end of said round.</p>
<p><b>Special Boxing Rule:</b> Fights must be run within two months of the specified date for your bet to have action.</p>
<p><b>Boxing Proposition Bets</b></p>
<p>For any proposition bets the following apply:</p>
<ul>
  <ul>
    <li >The rules stated on the card at the time of placing the wager will govern.</li>
    <li >When the bell sounds for the first round, the bout will be considered official.</li>
  </ul>
</ul>
<p>When wagering on” Pick the Round” propositions, in which you must pick the round in which the bout is won, the following will apply:</p>
<ul>
  <ul>
    <li >If the fight ends in a points verdict or a draw, the bettor loses.</li>
    <li >If either fighter fails to answer the bell for a round, the fight is adjudged to have finished in the previous round.</li>
  </ul>
</ul>
<p>However, this does not apply to specific fight props which must come-out as stated, such as a draw, either fighter by knockout or decision, or a specific round knockout etc.</p>
<p>Golf</p>
<p>For tournament match-up betting, both players listed in the match-up must tee-off for your bet to be deemed ” Action” .  The player with the most completed holes wins.  If the players complete the same number of holes, then the player with the lowest score wins.  If the players are still tied then the wager shall be deemed ” no-action” and all monies will be refunded.  If both golfers in a match-up are in a playoff, the winner of play-off wins the match-up.</p>
<p>Should a tournament be shortened, or otherwise affected, due to weather conditions the official result will be used when settling, regardless of the number or rounds played.  However, should there be no further play after a bet is struck that bet will be void.</p>
<p>Single day match-ups are wagers on the particular day” 18-holes.  Single Day match-ups do not include holes played as part of a completion from the previous day” round or playoff holes considered part of the overall tournament score.  Should a day” round be shortened, or otherwise affected, due to weather conditions and the round is continued the next day, the full 18 holes shall be considered in determining the outcome of the bet even if they are played over two days.  Both golfers must tee off for action.  If both players end the 18 holes in a tie, the money line wagers shall be refunded and stroke line wagers will be deemed ” Action” .</p>
<p>For single round-single player propositions, all 18 holes must be completed.<br>
For single round betting on match play tournaments, the players/team must tee off for action.  18 holes do not necessarily need to be played.</p>
<p>Golf Odds to Win: Wagers on a golfer who does not play in the tournament are graded as ” no-action” and all monies refunded (unless otherwise specified).  A golfer is deemed to have played once he or she has teed off.  In the event of a player withdrawing after having teed off, wagers on that player will be lost.</p>
<p>Las Vegas Rules apply for any rules not mentioned here.</p>
<p>Stroke Line Betting: In golf the lower score wins.  If you bet (-1/2) stroke, your golfer must shoot 1 or more shots lower than the opponent.  If you bet (+1/2) your golfer must shoot the same score or a lower score than the opponent.  Example: should you bet Singh (-1/2) vs. Mickelson and Singh shoots 69, Mickelson 70, then Singh (-1/2) is the winner. If Singh shoots 69 and Mickelson shoots 69, then Mickelson (+1/2) is the winner.</p>
<p>Rules for Finishing Top 3: In the case of a tie or multiple players finishing in the top 3 position, wagers will be paid using our Dead Heat Rule.</p>
<p>General Soccer Rules</p>
<p>All soccer bets (except those on Under 17s matches) will be settled on 90 minutes of play.  This includes any time added by the referee in respect of injuries and other stoppages.  In major knock-out tournaments, for example the latter stages of the World Cup, where a winner is required in order to progress to the next leg, bets are still settled on 90 minutes of play.  Extra Time, Golden Goals and Penalty Shoot-Outs do not count.</p>
<p>Bets on Under 17s Soccer matches will be settled on 80 minutes of play plus any time added by the referee in respect of injuries and other stoppages.</p>
<p>Match details, such as dates and kick-off times, displayed on the website are for guidance only and may be amended or taken off the board at any time. Soccer events officially postponed by more than 24 hours and/or rescheduled, will constitute ” no-action” and all money will be credited accordingly. The exceptions to this rule are official international and club tournament games (e.g. World Cup or Champions League) where a match must be played regardless, in which case all bets will be actioned on the rearranged game.</p>
<p>If a match is abandoned and/or suspended all bets shall be void unless the relevant wagering option has already been decided.  For example, a bet on the ” First Goal Scorer” will stand if a goal has already been scored.</p>
<p>Where a venue is changed, bets will stand unless the game is to be played at the original away team” ground in which case all bets will be voided.</p>
<p>For soccer wagering, in order to place your bet correctly you must predict the result of at least one game choosing either:</p>
<ul>
  <ul>
    <li >the Home team,</li>
    <li >the Away team or</li>
    <li >a draw.</li>
  </ul>
</ul>
<p>Should you parlay multiple games together, you must win all games in the parlay in order to collect any winnings.</p>
<p>Bets taken on <b>” Half Time Results” </b>will be settled on 45 minutes of the first half plus any added injury time or stoppage time.</p>
<p>Bets taken on <b>2nd Half Lines” </b>will be settled on the 45 minutes of the second half plus any added injury or stoppage time.  Extra Time, Golden Goals and Penalty Shoot-Outs do not count.  Goals scored in the first half do not count toward the second half wager.</p>
<p>Bets taken on the <b>” Soccer Grand Salami” </b>must predict the total (over/under) goals scored in a particular soccer league per round or league fixture (including games played on both weekdays and weekends as long as they comprise part of the same round). If any scheduled game is canceled/postponed/rearranged all wagers on the Soccer Grand Salami will be canceled/no action.</p>
<p>Bets taken on <b>” Draw No Bet” </b>must predict which team wins the match in regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).  In the event of the match ending in a draw (tie), bets will have ” no action” .</p>
<p>Future bets placed on <b>” Odds to Win” </b>must predict the team winning the division, league or any other tournament and will be paid as soon as the said division, league or tournament has finished.</p>
<p>Bets taken on <b>” Correct Score” </b>must predict the match score at the end of regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).</p>
<p>Bets taken on <b>” Double Chance” </b>must predict the outcome of the match based on three selections, either:</p>
<ul>
  <ul>
    <li >the Home or Away team wins,</li>
    <li >the Home team wins or draws or</li>
    <li >the Away team wins or draws.</li>
  </ul>
</ul>
<p>Bets taken on <b>” Half with Most Goals” </b>must predict which half has most goals scored by either team in regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).</p>
<p>When betting on <b>” Double Result” </b>(half time – full time) you must predict the result at half time and at the end of normal time (90 minutes).</p>
<p>When betting on <b>” Goals: odds or even” </b>you must predict if the total score by both teams ends up as an odd or an even number.  A 0-0 score counts as an even number.  Bets will be settled on 90 minutes of play plus any time added by the referee in respect of injuries and other stoppages.</p>
<p>Bets taken on <b>” Kick Off Bet” </b>must predict which team will kick-off the match.  Odds will be taken off the board 15 minutes before the match” starting time.</p>
<p>For the <b>” Goal/No Goal” </b>bet you can choose to wager on either ” Goal” for both teams to score, or ” No Goal” for either or both teams not to score.</p>
<p>When betting on <b>” Soccer Special Props” </b>or <b>” Match Specials” </b>you are required to predict the team or teams to win the game, event, tournament, competitions, match-up or any other situation within the game.  Any cancelled or postponed game will cause the entire event to have ” no action” .  Soccer Specials will be settled on 90 minutes of play.</p>
<p>Soccer Goal-scorer Markets</p>
<p><b>1. To score the first goal:</b></p>
<ul>
  <ul>
    <li >Bets on players not taking part in the match will be no-actioned. Bets on player ” to score the first goal” where the selection comes on after the first goal is scored will be no-actioned, unless the first goal is an own goal, in which case bets will be actioned.</li>
    <li >In the event of a dispute over the award of a goal, for betting purposes, settlement will be based on the goal-scorer listed by the official sources immediately after the match has finished. Any subsequent amendments to official sources” records will not count.</li>
    <li >Own goals do not count in pre-match first goal-scorer betting. In the event of an own goal first goal-scorer bets are not no-actioned but carry forward to the next goal.</li>
    <li >Extra time does not count.</li>
  </ul>
</ul>
<p><b>2. To score the last goal:</b></p>
<ul>
  <ul>
    <li >Predict the player who will score the last goal in the match. All players that take part will be considered ” runners” for last goal-scorer purposes. Every effort will be made to quote last goal-scorer odds for all participants. However, other players will always be quoted on request and will count as winners should they score the last goal.</li>
    <li >Bets on last goal-scorers that are substituted/sent off before the last goal is scored will be treated as losing selections.</li>
    <li >In the event of a dispute over the award of a goal, for betting purposes, settlement will be based on the goal-scorer listed by the official sources immediately after the match has finished. Any subsequent amendments to official sources” records will not count.</li>
    <li >Own goals do not count. If the last goal is an own goal bets default to the previous goal.</li>
    <li >Extra time does not count.</li>
  </ul>
</ul>
<p><b>3. Anytime Scorer, to Score 2 or more goals, to Score a hat-trick:</b></p>
<ul>
  <ul>
    <li >Select a player to score at any time during the match. Unless otherwise stated, extra time does not count.</li>
    <li >Players must start the match for bets to be valid. Stakes will be refunded on anytime goal-scorers who do not start, whether or not they score.</li>
    <li >Any selection taken from a match that is not completed will be treated as a non-participant.</li>
    <li >Own goals do not count.</li>
  </ul>
</ul>
<p><b>4. First goal-scorer and correct score (scorecast):</b></p>
<ul>
  <ul>
    <li >Predict the first goal-scorer and the correct score in a double (parlay). Bets will be settled at the combined odds displayed.</li>
    <li >If a player comes on after a goal has been scored or does not take part in the game, a bet involving that player will be settled as a straight wager on the selected correct score at the appropriate odds.</li>
    <li >For first goal-scorer purposes, own goals do not count. If the only goals in the match are own goals, bets will be settled as straight wagers on the selected correct score at the appropriate odds.</li>
    <li >If the match is abandoned after a goal has been scored, bets will be settled as straight wagers on the selected first goal-scorer at the appropriate odds.</li>
  </ul>
</ul>
<p><b>5. Score First and Win:</b></p>
<ul>
  <ul>
    <li >A Score First and Win bet requires you to select a player to score first and his team to win in a double (parlay). Bets will be settled at the combined odds displayed.</li>
    <li >Unless otherwise stated, extra time does not count.</li>
    <li >If the player does not start, the bet will be no-actioned.</li>
    <li >If the only goals in the match are own goals, bets will be settled as a straight wager on the team to win at the appropriate odds.</li>
    <li >If the match is abandoned after a goal is scored, bets will be settled on the selected first goal-scorer at the appropriate odds.</li>
  </ul>
</ul>
<p><b>6. Score Anytime and Win:</b></p>
<ul>
  <ul>
    <li >A Score Anytime and Win bet requires you to select a player to score anytime and his team to win in a double (parlay). Bets will be settled at the combined odds displayed.</li>
    <li >Unless otherwise stated, extra time does not count.</li>
    <li >If the player does not start the match or if a match is abandoned, bets are void.</li>
    <li >If the only goals in the match are own goals, a Score Anytime and Win bet will be settled as a straight wager on the team to win at the appropriate odds.</li>
  </ul>
</ul>
<p><b>7. Player vs Player match bets:</b></p>
<ul>
  <ul>
    <li >This bet requires you to select a player to score more goals than his opponent.</li>
    <li >Extra time does not count.</li>
    <li >The two selections do not necessarily have to play each other in a win/lose situation; the grading is determined by one selection” performance relative to the other.</li>
    <li >Both players must start their match(es) for bets to stand.</li>
    <li >Unless odds are quoted for a tie, in the event of a tie bets will be void.</li>
  </ul>
</ul>
<p><b>8. Anytime Goal-scorer Double (parlay):</b></p>
<ul>
  <ul>
    <li >Predict a pair of players who will each score a goal at any time in the match in a double (parlay).</li>
    <li >If either or both players do not start the match the bet is void.</li>
    <li >Extra time does not count.</li>
  </ul>
</ul>
<p>When betting on <b>” Asian Handicap” </b>you are essentially betting on a point (or goal) spread.  Asian Handicap offers only two betting options rather than the three possible outcomes normally offered on a soccer game (namely home, away or draw).</p>
<p>A      +0.5    (1.95)<br>
B       -0.5    (1.95)</p>
<p>In this example, if you bet on team A they must win or draw the match for you to win your bet.  If you bet on team B they must win by at least one goal for you to win your bet.</p>
<p>Betting on one of just two outcomes makes things simpler and it also allows you to get a better price when betting on the favorite.  Where offered, the Asian handicap will always include a half-ball, i.e. the spread will be +/- 0.5, +/- 1.5 or higher.  All Asian Handicap bets are based on regular time (90 minutes plus any time added by the referee in respect of injuries and other stoppages).</p>
<p>Betting on <b>” Extra Time” </b>: In knock-out tournaments where a winner has not been decided after 90 minutes of play, extra time is added.  The Extra Time consists of 30 minutes of play (two halves of 15 minutes each) plus any time added to this by the referee in respect of injuries and other stoppages.  Where bets on Extra Time are offered, they are settled only on these additional 30 minutes.  Results achieved during regular time and/or penalty shoot-outs do not count.</p>
<p>In a ” <b>Fantasy Soccer Match-up” </b>two teams compete virtually.  The winner of this match-up is the team that scores the most goals on the actual match-day against a real opponent.  Only goals are counted, victory or loss is irrelevant.</p>
<p><b><i>For example:</i></b> On a particular day Manchester United plays Manchester City and Chelsea plays Bolton.<br>
You choose to bet on a Fantasy Match-up, Manchester United vs. Chelsea.<br>
Manchester United win their match 1-0 against Manchester City, while Chelsea loses 2-4 to Bolton.  However, Chelsea would win the Fantasy Match-up with a 2 -1 score.</p>
<p><b>In-Running Soccer</b> betting is offered on selected games.  Once the game starts, in-running odds will be posted on the site under ” In-Running Soccer” and will be updated constantly during the game, unless the game becomes extremely one-sided, in which case in-running betting will be suspended.  In-Running Soccer wagers are based on the final result of the entire game (90 minutes plus any added injury time or stoppage time).</p>
<p><b>Beach Soccer:</b> Beach Soccer is based on 36 minutes of play (3 periods of 12 minutes) followed by extra periods and penalty shots, if necessary, to decide who wins the match.  Bets are graded on normal regulation time (36 minutes plus any time added by the referee in respect of injuries and other stoppages).  Extra Time and Penalty Shoot-Outs do not count for wagering purposes.</p>
<p><b>Futsal: </b>All bets on Futsal matches are based on the score at the end of 40 minutes” play unless otherwise stated or if the relevant wagering option has already been decided.  Extra Time and Penalty Shootouts do not count.  If a match is abandoned and/or suspended before the completion of 40 minutes” play, all bets shall be void unless the relevant wagering option has already been decided.  All bets on matches that are postponed, rearranged or moved to a different venue shall be void.</p>
<p><b>Cricket</b></p>
<p>For Cricket games, if the match is shortened by the weather, bets will be governed by the official competition rules.</p>
<p><b>Hockey</b></p>
<p>For wagering purposes, winners and losers are determined by the final score, provided that the game has gone the minimum time as specified above.</p>
<p>Overtime: For wagering on games in the National Hockey League, the final score includes overtime. Regardless of the number of goals scored during the shootout portion of overtime, the final score recorded for the game will give the winning team one more goal than the score at the end of regulation time.</p>
<p>Personal Player Stats and other Stats achieved during an NHL Shootout are not taken into account for certain proposition bets, for example: Player Total Shot on Goals, Player Total Points, Player Total Goals, Team to Score First, Team to Score Last.</p>
<p>Las Vegas rules, regulations, payoffs and wager types apply where not covered herein.</p>
<p>Rugby</p>
<p>For Rugby Games, if any match is abandoned or postponed all bets will be void, except bets on the first try scorer if a try has been scored prior to abandonment.<br>
Where a venue is changed, bets will stand unless the game is to be played at the original away team” ground in which case all bets will be void.</p>
<p>Tennis</p>
<p>For tennis match betting, two full sets must be completed for wagers to stand.  If less than two sets are completed then all wagers will be considered ” no-action” .  In the event of any of the following circumstances taking place, all bets will stand:</p>
<ul>
  <ul>
    <li >a change of playing surface</li>
    <li >a change of venue</li>
    <li >a change from indoor court to outdoor court or vice versa</li>
    <li >The match is delayed or postponed due to inclement weather/bad light and is completed on a later date.</li>
  </ul>
</ul>
<p>Motorsports</p>
<p>When placing a wager on ” Odds to Win” a race, wagers shall be deemed ” no-action” should the driver not start the race.  For a motor sports match-up to be deemed ” Action” both drivers must start the race.  The start of any motor race is defined as the signal to start the warm-up lap.  The official winner at the conclusion of the race will be the winner for betting purposes.  In League Championship wagering, drivers must race in at least one race during the season to be deemed ” Action” .</p>
<p>Special Motorsport Rule: Races must be run within eight (8) days of the scheduled date for your bet to have action.</p>
<p>The NASCAR.com website is our official source for the grading of all NASCAR wagers.  The race must complete the scheduled number of laps and/or distance for specific proposition wagers (pertaining to number of laps and/or distance) to be deemed ” Action” .  Match-ups, finishing position props and future wagers will still have action regardless.</p>
<p>Rules for Finishing Top 3:  In the case of a tie or multiple players finishing in the top 3 position, wagers will be paid using our Dead Heat Rule.</p>
<p>Formula 1: At least 15 drivers must start the race for action.</p>
<p>NCAA Basketball</p>
<p>In the event of a tie in NCAA conference wagering, the team entering the conference tournament ” seeded” higher shall be deemed the winner.</p>
<p>Parlay or Multiple Betting</p>
<p>No parlay wagers can be accepted where individual wagers are” connected” , ” dependent” or ” correlated” . Using a baseball game as an example, let” say you want to take the Boston Red Sox to win on the money-line and the Boston Red Sox on the run-line. If the Boston Red Sox win the game, it is also likely that they will win by at least two runs therefore the selections are said to be ” correlated” and cannot be parlayed together. For the same reason, we do not allow the favorite/underdog on the runline and the over/under of the same game to be placed in the same parlay.</p>
<p>In the same way, you cannot parlay the line for the first half of a football game with the line for the whole game or the total for the first half of a football game with the total for the whole game, as the two are ” dependent” . Less obviously, but nonetheless correlated, are the favorite/underdog on the spread and the over/under of the same game placed together in a parlay or teaser bet when the spread is greater than or equal to 30% of the total.</p>
<p>Dead Heat Rule</p>
<p>In the event of a dead heat (a tie), stakes will be divided by the number of selections that have tied, with the divided stake settled at full odds.  Remaining stakes are lost.  If the tie was a betting option, then the dead heat rule does not apply.  The dead heat rule only applies to ” future” wagers.  For example, if there is a 3 way tie for the top scorer in a basketball game, then your winnings are calculated by taking your stake, dividing it by 3, and multiplying that amount by the odds on your betting ticket.</p>
<p>Buying Points</p>
<p>Buying points allows you to change the point-spread or total of a football or basketball game.</p>
<p>You can move the point-spread so you get more points when betting the underdog or you give away fewer points when betting the favorite.  For each half point that you change the point spread you need to pay an extra 10c.</p>
<p>So, for example, if you are betting the Lakers -5 -110 (lay $110 to win $100), you can buy the Lakers a half point to -4.5 -120 (lay $120 to win $100). You can buy the Lakers a full point to -4 -130 (lay $130 to win $100).</p>
<p>The same theory is true for the underdog.  If the Bulls are +4 -110, you buy the game to +4.5 -120 or +5 -130.</p>
<p>You can only buy a maximum of 3 points in Football and Basketball, both College and Professional.</p>
<p>There are two special rules:</p>
<ol >
  <ol >
    <li >1.When buying on or off the number 3 in the Pro Football, there is a premium cost of 25c, not 10c.  So, for example, to move the Chargers from -3 -110 to -2.5, the new line will be -2.5 -135 (lay $135 to win $100).  The same is true for the underdog, to buy Bears from +3 -110 to +3.5 will be -135 (lay $135 to win $110).</li>
    <li >2.On half, reduced or no juice games or half, reduced or no juice days, there are additional premium costs for buying points:</li>
  </ol>
</ol>
<p>a) In college football there is a 5c additional charge per half point purchased.<br>
b) For buying on or off the number 7 in college football, the premium is 25c.<br>
c) In professional football there is a 5c additional charge per half point purchased.<br>
d) For buying on or off the numbers 3 or 7 in professional football, the premium is 25c.</p>
<p>Please note: these premium costs for buying points apply from 12:01am to 11:59pm EST on the specified half, reduced or no juice day.</p>
<p>Matchplay Matchups</p>
<p>If a match does not start (e.g. player injured or disqualified before the start of a match) then all bets on that match will be void.</p>
<p>Bets on markets that can be settled by using the official tournament and match results (including final match correct score and individual match betting) will be settled using those results. This includes where a match finishes early either by agreement of the players or through injury.</p>
<p>All other markets where a match finishes before completion of 18 holes (e.g. by agreement), such as match score, will be settled as if the remaining uncompleted holes are ties. For example, a player 2 up at the 13th hole when the match finishes will be deemed to have won 2 and 1 (at the 17th hole). Uncompleted single hole bets will be void.</p>
<p>Free Bets</p>
<p>The following terms and conditions apply to all free bets:</p>
<ul>
  <ul>
    <li >One wager per member.</li>
    <li >You must have funds in your wagering account to cover the cost of the free bet (usually $10) and this will be refunded.</li>
    <li >All sides must play in order for the bet to have action.</li>
    <li >In case of tie, all bets will have “no-action.”</li>
    <li >Wagering on both sides of this free bet or opening multiple accounts to take advantage of this offer will result in non-payment of all free wagers in the related accounts and a $5 penalty charge per free wager and may result in the disabling of the accounts and forfeiture of winnings.</li>
    <li >The free bets are open to all active members who make a minimum of 5 sports wagers per week or total action of $50 in poker, casino or horses per week.  Making only free bets may result in the disabling of the accounts and forfeiture of winnings.</li>
  </ul>
</ul>
<p>CASINO</p>
<p>In the event of a member winning in excess of $50,000 on a Progressive Jackpot in any of the casino games where a jackpot is offered, the member agrees to be photographed while receiving a check from one of our representatives and gives US Racing and its Gaming Provider the exclusive right to use the photographic material at its own discretion.   The weekly limit on winnings of $100,000 does not apply to progressive jackpot winnings.</p>
<p>Management reserves the right to change the casino settings and payouts without any forewarning to the members at any given time.</p>
<p>Wagering in any of the casino games listed below does not count towards any bonus rollover requirements and Management shall make all decisions with regard to the application and retention of bonuses:</p>
<ul>
  <ul>
    <li >BlackJack Switch</li>
    <li >Blackjack 52</li>
    <li >American Roulette</li>
    <li >European Roulette</li>
    <li >Mini Roulette</li>
    <li >Baccarat</li>
    <li >Pai Gow Poker</li>
    <li >Craps</li>
    <li >Touchdown Frenzy</li>
    <li >Football Frenzy</li>
    <li >Red Dog</li>
    <li >Sic Bo</li>
    <li >Battle Royale</li>
  </ul>
</ul>
<p>Note:  For general casino disputes we provide a record that contains the bet amount, running balance, game type, Round ID and time for each single casino round only.   Every casino round has a unique Round ID and members should provide it in their casino claim.   Due to technology constraints we can only provide the member with the card/number/dice details and outcomes for a limited number of casino rounds only (25 rounds maximum).  If a unique Round ID or exact time for a particular casino round is not provided by the member we cannot guarantee the funds will be reimbursed.</p>
<p>All of the casino games provided by US Racing’s gaming provider are TST certified (see TST site logo displayed on our pages).</p>
<p>POKER</p>
<p>Providing a safe and secure environment is essential to players enjoying their online poker experience.  Our Gaming Provider is able to achieve this by ensuring cards are dealt randomly and taking the necessary steps to eliminate cheating.</p>
<p><b>Integrity</b><br>
The solution chosen for Random Number Generator/Shuffling is critical to ensuring a fair game for all players in the Poker room.  The Random Number Generator ensures the even distribution of cards whereby no sequence or relationship can be discovered in any way.  The RNG solution used in the Poker room is the cryptographically-secure RNG system found in Microsoft’s CryptoAPI.</p>
<p><b>Etiquette</b><br>
Any player using racist, derogatory or abusive language on the chat within the Poker room will have his/her chat privileges removed and may be banned from the Poker room.  Only the English language is permitted on the chat.  Neither shall the practices of begging, solicitation or chat-flooding be tolerated.</p>
<p><b>Bonuses</b><br>
Bonuses for Poker play shall be awarded and earned out in $10 increments within the Poker room.  Once a portion of a bonus has been earned it may be removed from the Poker room.</p>
<p><b>Disputes<br>
</b>The random number generator (” RNG” will determine the outcome of the games or other promotions that utilize the “RNG”  Furthermore, in the event of a discrepancy between the result showing on the software and the gaming server, the result showing on the gaming server shall be the official and governing result of the game.</p>
<p><b>Disclaimer</b></p>
<ul>
  <ul>
    <li >Any player found to be tampering with the poker software will be banned permanently from the Poker room.</li>
    <li >The use of Artificial Intelligence or “bots” is strictly forbidden.</li>
    <li >Any players found to be colluding or participating in unethical play will be banned permanently from the Poker room.</li>
    <li >Any funds gained by cheating will be confiscated.</li>
  </ul>
</ul>
<p>PROMOTIONS</p>
<p>If, as part of a promotion offered by US Racing, you receive betting cash, you can only use such amount as wagers for playing in the games offered by the company.  You are not entitled to withdraw such amounts and we reserve the right to reclaim those amounts if you do not use them to play the games during the period specified in the promotion.</p>
<p>Only members of US Racing or its Gaming Provider can redeem promotional prizes and winnings from promotions.</p>
<p>If you have more than one betting account, all promotional credits and/or winnings will be null and void and they will be deducted from your account.</p>
<p>If a member wagers both sides of a game using a promotional bet (to bet at the same time on the favorite and the underdog) or using bonus money, Management considers that to be fraudulent behavior.  This includes making bets of this nature in two or more of our affiliated websites.<b>  </b>Any money won playing both sides of the game will be deducted, together with any other promotional money deposited in the member” account.</p>
<p>Management reserves the right to modify any promotion rules or cancel promotions at its own discretion.</p>
<p>MULTIPLE ACCOUNTS</p>
<p>Members are not permitted to open multiple accounts (either within US Racing itself or affiliated websites on the same platform) in order to circumvent the limits imposed by our wagering system.  Performing such an action may trigger an audit of the member” accounts.  If multiple accounts have been used then all bets may be voided, accounts may be inactivated and winnings may be forfeited.</p>
<p>ACCOUNT CLOSURES</p>
<p>If an account is closed per Management’s decision, all pending wagers will be graded as No Action.</p>
<p>MEMBER” ADDRESS</p>
<p>It is your responsibility to ensure that we have your current address on file at all times.  You may only have one address on file at a time, and this address MUST match the billing address of ALL cards that have been used in the account.  Any failure on your part to do this may result in an audit on your account.</p>
<p>Before making a deposit and to avoid unnecessary delays in processing your payouts, please make sure your address is correct and that it matches bank records.  In the event that we are not able to verify your account information you may be required to submit additional documentation for this purpose.  This may include the following:</p>
<ul>
  <ul>
    <li >A photocopy of your picture ID (e.g. Driver’s License, passport)</li>
    <li >Photocopies (front and back) of your credit cards</li>
    <li >A credit card statement or utility bill</li>
    <li >A credit card verification form</li>
    <li >Imprint of your credit card</li>
  </ul>
</ul>
<p>When requesting a withdrawal, please ensure that your current address is correct and up-to-date, also ensure we have received and verified all the required “Know Your Customer” documents at least 72 hours prior to making your request; we will need you to provide us with a copy of a valid form of ID and a copy of a utility bill showing your name and address. Depending on your method of withdrawal, we may need to provide these documents to our payment processor prior to processing your withdrawal.  If a stop-payment request is necessary due to an out-of-date address, it will be your responsibility to assume the stop payment fees of a minimum of $50.</p>
<p>MULTIPLE CREDIT CARDS</p>
<p>The maximum number of credit cards allowed for use in one account is five. All cards used on an account must be in the name of the US Racing/Gaming Provider member.</p>
<p>LEGALITY</p>
<p>All bets made on products provided by BUSR website, in the Gaming Provider’s Casino or the Gaming Provider’s Sportsbook, are placed over the internet which reaches virtually every country in the world. Some jurisdictions have not addressed the legality of internet gaming, some have specifically legalised internet gaming, while others may take the position that internet wagering or gaming is illegal. In practical terms, it is impossible for BUSR to determine the state of the law in every country, state and province around the world on an ongoing basis. Therefore, by clicking the ” agree” button, you are acknowledging that it is responsibility of each individual to determine the law that applies in the jurisdiction in which he or she is present and that accordingly (a) you have determined what the laws are in your jurisdiction; and (b) it is legal for you to place a bet via the internet and for the Gaming Provider to receive your bet via the internet.</p>
<p>The Gaming Provider has offices in South America, North America, Hong Kong, China and Central America. The Gaming Provider holds a license to conduct on-line wagering and gaming and is legally authorized to conduct online gaming operations. All bets with US Racing’s Gaming Provider, BUSR, are considered to be placed and received in Antigua and Barbuda. Any matter to be adjudicated shall be determined utilizing the laws of Antigua and Barbuda.</p>
<p>Should there be any discrepancy in the legality of any transactions between you and Management, or any of its affiliates, the matter shall be determined by a court of competent jurisdiction in the country of Antigua and Barbuda.</p>
<p>This website does not constitute an offer, solicitation or invitation by US Racing or its Gaming Provider for the use of or subscription to betting or other services in any jurisdiction in which such activities are prohibited by law. All contractual issues between you and US Racing, US Racing’s Gaming Provider, or any of its affiliates, that are disputed, shall be resolved by a court of competent jurisdiction in the country of Antigua and Barbuda. All contracts shall be interpreted in accordance with the laws of Antigua and Barbuda.</p>
<p>US Racing and its Gaming Provider, BUSR, do not guarantee the accuracy of verbal or written communications. House Rules have precedence over any verbal or written communications and, in the event of any dispute, the policies and terms and conditions posted on our site are final. Furthermore, Management does not guarantee the privacy of live chat conversations.</p>
<p>US Racing and the website usracing.com do not provide any gambling services or financial instruments which allow for the transactions of wagers over the internet.</p>







{*<p><h2>WELCOME TO US RACING!</h2></p>

<p>This agreement describes the terms and conditions applicable to the use of the account wagering services provided by US Racing. US Racing is an online wagering service powered by TVG.  All accounts created through the US Racing website as well as those accessed through the site are TVG wagering accounts and, as such, are subject to and protected by TVG's Terms and Conditions and Privacy Policy.  The use of our wagering system will be taken as your acceptance of all of these terms, conditions, and disclosures contained herein.</p>

<p><strong>SUBSCRIPTION ELIGIBILITY</strong></p>

<p>To establish or maintain a wagering account, you must be:</p>
<ul>
<li>At least 21 years of age;</li>
<li>A United States citizen or resident alien; and</li>
<li>A resident of a state where wagering services are available.</li>
</ul>


<p><strong>WAGER REWARDS</strong></p>
<p>When you sign up for a US RACING wagering account you may enroll in our US RACING Rewards Program (the &ldquo;Rewards Program&rdquo;) by checking the Opt-In box. You may also enroll at any other point by clicking on the Wager Rewards link at www.USracing.com. Membership in the Rewards Program is subject to the Member&rsquo;s continued status as a holder in good standing of a US RACING wagering account and compliance in all respects to these Terms and Conditions as well as all of the Terms and Conditions applicable to Member&rsquo;s Rewards Program. To opt-out of participation in the Rewards Program simply call US RACING Customer Relations at 1- 855-399-RACE (7223).</p>


<p><strong>WAGERING ACCOUNT PROCEDURES</strong></p>
<p>You may access your account and place wagers through our website at usracing.com, or through our mobile site, mobile.usracing.com, using tablets or mobile devices.<br />
For your protection and privacy, a wagering account can only be accessed by the registered account holder with a valid account number and Personal Identification Number (PIN).<br />
For your benefit, wagers placed through US RACING wagering systems are sent into combined (commingled) pools at the host racetrack and are subject to all host racetrack rules and restrictions. From time to time US RACING may operate separate wagering pools from the host racetrack when pools cannot be commingled. Separate pools will be subject to rules and regulations of the applicable jurisdictional authority.</p>

<p>All wagers are considered final when a confirmation is received from our totalizator service provider and your wager is commingled with the host pool. If for some reason we are unable to commingle your wager with the host pool, your wager will be refunded to your account.</p>
<p>We reserve the right to refuse any wagering transaction for any reason.</p>

<p>Proceeds from successful wagers will be deposited into your account as soon as each race is posted official and winning payouts are available from the host racetrack.</p>

<p>We reserve the right, at our sole discretion, to terminate an account at any time and for any reason. In the event an account is terminated, we shall return to you the balance of any funds in your account at the time of termination. If the balance of funds in your account at the time of termination is less than $2.00, we will retain the balance as an account termination fee. If your account is inactive for 6 consecutive months, a $1.75 per month account maintenance fee may be assessed until the balance reaches zero and the account is terminated.</p>

<p>We reserve the right, at our sole discretion, to refer any dishonored financial instruments to collection agencies, check registries and credit reporting agencies and to collect any fees or charges incurred due to dishonored financial instruments and expenses, incurred as a result of our collection efforts, from the responsible accountholder.</p>
<p>
You authorize us to obtain, at our discretion, credit bureau reports in connection with your request for an account. If an account is opened, we may obtain, at our discretion, credit bureau reports in connection with the review of your account.</p>

<p><strong>WAGERING TRANSACTION CANCELLATIONS*</strong></p>
<p>As a US RACING subscriber you may request to cancel wagering transactions on any race, with the exception of pools where cancellation is prohibited by the racetrack (Future Pools) or prohibited by the racetrack&rsquo;s state jurisdictional authority. You may request to cancel up to twenty wagering transactions in any one day and up to two hundred wagering transactions in any calendar month (but not more than twenty in any one day) up to a maximum aggregate wagering transaction amount of $5,000 in a day and of $15,000 during that calendar month. Once the daily or monthly limits have been met, cancellations will no longer be allowed during the remainder of the day or calendar month. Wagering transaction cancellations may be made as follows:</p>
<p><ul>
<li>Online wagering transaction cancellations - A wagering transaction in the amount of $1,000* or less may be cancelled online at USracing.com.</li>
<li>Wagering transaction cancellations through US RACING&rsquo;s Customer Relations Department - A wagering transaction in the amount of $1,000* or less may be cancelled upon your request and US RACING&rsquo;s verification that you have not reached your daily limit of $5,000 or monthly limit of $15,000 and/or daily and monthly limits of twenty wagering transactions per day or two hundred wagering transactions per month by calling 1- 855-399-RACE (7223). Wagering transaction cancellation requests in the amount of more than $1,000* for a wagering transaction may be made by calling 1- 855-399-RACE (7223) and must be approved by a US RACING Customer Relations Supervisor after investigating the wager transaction cancellation request and verifying that you have not reached your daily limit of $5,000 or monthly limit of $15,000 and/or daily or monthly limits of twenty wagering transactions per day or two hundred wagering transactions per month. Accountholders requesting cancellations through the US RACING Customer Relations Department must remain on the line until the wagering transaction cancellation is finalized. You will be notified of the completed cancellation or the reason the wagering transaction could not be cancelled.</li>
</ul></p>

<p><strong>Important Wager Cancellation Restrictions</strong></p>
<p><ul>
<li>US RACING may refuse to accept a wager transaction cancellation request for any reason.</li>
<li>US RACING may refuse to permit any account holder the privilege of requesting or processing wagering cancellation requests for any reason.</li>
<li>Wagering transactions may be cancelled until wagering is closed by the racetrack totalizator company for the racetrack hosting the live race on which the wagering transaction was made.</li>
<li>Wagering transaction cancellations made by calling customer relations staff will be processed in the order received. US RACING does not guarantee cancellation of a wager.</li>
<li>Wagering transaction cancellations per subscriber account, whether made online or by customer relations staff, are limited to up to twenty wagering transactions in any one day and up to two hundred wagering transactions in any calendar month (but not more than twenty in any one day). No account holder may cancel wagering transactions over a maximum aggregate wagering transaction amount of $5,000 per day or $15,000 during a calendar month.</li>
<li>Wagering transaction cancellations will not be provided via the IVR or Mobile.</li>
</ul>
</p>
<p><em>*VA Residents Only --Wagering transaction cancellations are limited to $250 online at USracing.com. All wagering transaction cancellation requests over $250 must be reviewed and approved by a US RACING customer relations supervisor and will be reported to the VA Racing Commission.</em></p>

<p><strong>TAX WITHHOLDING</strong></p>
<p>By law, any wager which results in proceeds of $600.00 or more must be reported to the Internal Revenue Service (IRS), if the amount of such proceeds is at least 300 times as large as the amount wagered. Any wager, which results in proceeds of more than $5,000.00, is subject to reporting and withholding, if the amount of such proceeds is at least 300 times as large as the amount wagered. If you are subject to IRS reporting and/or withholding requirements, we will send you Form W2-G summarizing information for tax purposes following the winning wager, less any applicable withholding, being deposited into your account. Upon written request, we will provide you with summarized tax information on your wagering activities. </p>

<p><strong>WAGERING ACCOUNT BALANCE, DEPOSIT, AND WITHDRAWAL PROCEDURES</strong></p>
<p>Account deposits will be made available for customer use in accordance with normal financial industry availability standards. <strong>Deposits may be subject to deposit fee charges.</strong> Withdrawal requests will be processed within five business days after receipt. The availability of withdrawn funds is subject to standard banking restrictions.<br />
Your account balance bears no interest.</p>

<p>You can check account balances through our various wagering platforms or by calling our Customer Relationship Representatives at 1- 855-399-RACE (7223).</p>

<p><strong>DISCLOSURES AND POLICIES</strong></p>

<p>Please note: Your wagering account is for your personal use and is non-transferable. We reserve the right to void any wagering transaction if there is reason to believe that someone other than you deposited funds in or placed a wager from your account. By law, you must immediately notify us of a change in your state of residency by calling 1- 855-399-RACE (7223). If you move to a state where wagering services are not available, we are required to terminate the account and return to you all remaining funds. We reserve the right to terminate an account at any time and for any reason.</p>
<p>For your information, the data we (or any of our suppliers, including Equibase Company LLC) provide or compile generally is accurate, but occasionally errors and omissions occur as a result of incorrect data received from others, mistakes in processing and other causes. Accordingly, we along with our data providers and suppliers, including Equibase Company LLC, disclaim responsibility for the consequences, if any, of such errors and omissions, but would appreciate notification of any errors.</p>

<p>US RACING will report any activities that we reasonably believe constitute fraud or theft to the appropriate law enforcement authorities and may prosecute such activities to the full extent of the law. To the extent permitted by law, US RACING will retain the proceeds resulting from such fraudulent activity or theft and use those funds to pay for damages and losses resulting from such fraudulent activity or theft.</p>

<p><strong>PATENT NOTICE</strong></p>

<p>Products and services offered by US RACING, including the USracing.com website, are covered under one or more of the following United States patents: 6,554,709; 6,554,708; 6,089,981; 6,004,211; 5,830,068 to ODS Technologies, L.P. and/or its subsidiaries.</p>

<p><strong>LIABILITY LIMIT</strong></p>

<p>Except where provided otherwise by the Oregon Racing Commission, in no event will we or our suppliers be liable for lost profits or any special, incidental or consequential damages arising out of or in connection with our services (however arising, including negligence). Our liability, and the liability of our suppliers, to you and any third parties in any circumstance is limited to the greater of (A) the total amount of wagers made by you through your account in the month prior to the action giving rise to liability, and (B) $100. Some states may not allow this limitation of liability, so the foregoing limitation may not apply to you.</p>

<p><strong>INDEMNIFICATION</strong></p>
<p>You agree to defend, indemnify and hold harmless US RACING, its suppliers, distributors, contractors, subcontractors and its affiliates and related entities and their respective directors, officers, employees and agents, from and against all claims, losses, damages, liabilities and costs (included but not limited to reasonable attorneys&rsquo; fees and court costs), arising out of or relating to your breach of these Procedures, Terms and Conditions. The foregoing indemnification obligation shall survive termination of these Procedures, Terms and Conditions and your purchase of any product or service provided to you by US RACING, its suppliers, distributors, contractors, subcontractors and its affiliates.</p>

<p><strong>ARBITRATION*</strong></p>
<p>Any claim alleging that a non-payment has occurred will be resolved under the rules issued by the Oregon Racing Commission. All other controversies or claims arising out of or relating to our services (including but not limited to our account wagering services) will be settled by binding arbitration in accordance with the rules of the American Arbitration Association. Any such controversy or claim will be arbitrated on an individual basis, and may not be consolidated in any arbitration with any claim or controversy or any other party. The arbitration will be conducted in Beaverton, Oregon and judgment on the arbitration award may be entered into any court having jurisdiction over the losing party. Any party to the arbitration may seek any interim or preliminary relief from a court of competent jurisdiction in Beaverton, Oregon necessary to protect the rights or property of such party pending the completion of arbitration.</p>

<p><em>*Maryland Residents Only: Unresolved claims alleging that nonpayment has occurred or unresolved exceptions to an account activity report will be resolved under the Telephone Account Betting Rules issued by the Maryland Racing Commission and provided along with your new subscriber information.</em></p>

<p><strong>WARRANTIES</strong></p>
<p>TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, WE DISCLAIM ALL WARRANTIES, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OR MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.</p>

<p><strong>INFORMATION REQUESTS</strong></p>
<p>All requests for information, questions, account reconciliation, wagering discrepancies, etc., must be made to our Customer Relations Department by calling toll free 1- 855-399-RACE (7223). For your protection, all conversations with our Customer Relationship Representatives are recorded and archived.</p>

<p><strong>OTHER</strong></p>
<p>Account wagering rules, policies, terms, conditions, and procedures are subject to change at any time. Changes will be posted on our Web site USracing.com - or can be obtained by calling 1- 855-399-RACE (7223). It is your responsibility to be aware of all wagering rules, policies, terms, conditions, and procedures, including any subsequent changes.<br />
This service has been approved by the Oregon Racing Commission. All accounts are maintained and wagering information is processed at a licensed facility located in Oregon. It is not intended to be an offer or solicitation in those states where prohibited.</p>

<p><strong>PROTECTING YOUR PRIVACY</strong></p>
<p>Protecting your privacy, along with financial transactions, is at the core of US RACING&rsquo;s business. This notice describes the type of personally identifiable information we collect and how we use such information.</p>


<p><strong>TYPE OF PERSONALLY IDENTIFIABLE INFORMATION COLLECTED</strong></p>
<p>We collect, retain and use information about you to protect and administer your account and funds; to comply with applicable state and federal laws and regulations; and to help us design, improve and provide products and services in order to better serve you. When you subscribe as a US RACING customer or when you submit a request or electronically pass information to US RACING, you are sometimes asked to provide certain information, such as your name, e-mail address, mailing address, telephone numbers, social security number, birth date, credit card or bank account information. We ask for this in order to facilitate your requests, provide personalized services and communicate separately with you. Without your written or electronic consent, we will not collect personal information over our system unless it is necessary to provide you with our services or to prevent unauthorized access to our services.</p>

<p><strong>DISCLOSURE OF PERSONALLY IDENTIFIABLE INFORMATION</strong></p>

<p>You have chosen to do business with us, and we recognize our obligation to keep the information you provide to us secure and confidential. In our continuous effort to provide services that we believe you may find useful or of value, we may make your records available to better facilitate your requests, provide more personalized services and communicate separately with you. From time to time, we will use the information we collect to coordinate data entry and payments with the company that provides you with handicapping information, to coordinate marketing/rewards programs with partner racetracks, and to offer other enhancements to the service. Any third party receiving such information will have an obligation to hold it confidential. Without your written or electronic consent or where necessary to provide our services, we will not disclose your personally identifiable data to other companies outside of US RACING or our affiliated companies. US RACING does not currently sell customer information to advertisers or third party marketers. If we choose to do so in the future, we will provide you with information about how to remove your name from such lists. You will be provided with at least thirty (30) days within which to remove your name from such lists.</p>

<p>If you would prefer that we not share any of your information with third parties, except under the circumstances where we are legally required, please contact us in writing at US RACING, 19545 NW Von Neumann Drive, Suite 210, Beaverton, OR 97006-6935 to this effect.</p>

<p>In circumstances where we inadvertently obtain information that appears to pertain to the commission of a crime or where we reasonably believe that an emergency involving immediate danger of death or serious physical injury to any person exists, US RACING may voluntarily disclose the record or the contents of your communications or other information about you to law enforcement agencies and governmental entities without providing you advance notice. Upon receipt of an applicable search warrant or administrative, grand jury or trial subpoena, US RACING may be required to disclose to the government or law enforcement agencies, without advance notice to you, the content of your communications and other records relating to your electronic communications, as well as the following records: your name and address; records of your online communication (including session times and duration); how long you have subscribed to our service(s) (including start date) and the type(s) of service(s) utilized; your telephone number or other subscriber account identifying number(s), including any Internet or network address(es) assigned to you by our network; and the means and source of your payment(s) (including any credit card or bank account number).</p>

<p><strong>PROTECTING ONLINE ACCOUNT INFORMATION AND TRANSACTIONS</strong></p>

<p>When you apply online to establish an account, access services or utilize online wagering, you provide personal and confidential information that is necessary for us to process your request. To ensure that your information remains confidential, the information is sent to us in a &ldquo;secure session.&rdquo; Using the industry standard, Secure Socket Layer (SSL)-capable web browsers support encryption technology that helps prevent unauthorized users from viewing or manipulating your account information as it travels over the Internet. We will permit only authorized parties, who are trained in the proper handling of customer information, to have access to that information. Employees who violate our privacy policy will be subject to our disciplinary process.</p>

<p><strong>RETENTION</strong></p>

<p>We destroy customer information that is no longer necessary for the purpose for which it is collected unless there is a legitimate request or order to inspect the information still outstanding or the information remains in routine records that are periodically discarded under our document retention policies. The information that you have provided us is maintained in our management information system and billing systems, and is updated as new information is added. Accounting and billing records are retained for ten years for tax and accounting purposes or until the relevant income tax years for which the document was created have been closed for income tax purposes and/or all appeals have been exhausted. Records may remain on file even after you have terminated service. Subject to applicable law, we may also keep records to facilitate collection and to evaluate credit worthiness.</p>

<p><strong>YOUR RIGHTS AND RESPONSIBILITIES</strong></p>
<p>It is extremely important that you keep your account number, Personal Identification Number and other confidential account data protected and secure. Do not share your PIN or leave your computer unattended during an online wagering session. Please make sure that all information you provide to US RACING is accurate and complete. Contact us immediately by email at comments@usracing.com, or by calling 1- 855-399-RACE (7223) if you find any discrepancies in your account data or if you wish to inspect the records pertaining to you at our offices.</p>

<p><strong>THIRD PARTY WEB SITES</strong></p>

<p>US RACING&rsquo;s website contains links to other sites. US RACING cannot be responsible for the privacy practices or the content of other web sites. When linking to other sites, please review their security and privacy policies.</p>


<p><strong>RESPONSIBLE WAGERING</strong></p>

<p>US RACING is a recognized leader in the area of responsible wagering. US RACING does not solicit any information from or market our services to anyone under the age of 18.</p>
<p><strong>National Gambling Helpline: 1-800-522-4700. </strong> </p>
<p><strong>We reserve the right to revise this policy, or any part thereof. Use of our service following notice of such revisions constitutes your acceptance of the revised policy.</strong></p>

<p>US Racing and TVG reserve the right to revise this policy, or any part thereof. Use of our service following notice of such revisions constitutes your acceptance of the revised policy.  In accordance, when in doubt, TVG's <a href="https://www.tvg.com/terms-and-conditions" target="_blank"><strong>Terms and Conditions</strong></a> and  <a href="https://www.tvg.com/privacy-policy" target="_blank"><strong>Privacy Policy</strong></a> shall supersede the Terms and Conditions and Privacy Policy found at the US Racing website.</p>

       *}      

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div><!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
