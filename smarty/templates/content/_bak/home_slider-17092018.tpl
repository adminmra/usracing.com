<!--=== Home Slider ===-->
<div class="sliderContainer fullWidth clearfix margin-bottom-30">
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>

				


				
<!-- slide KENTUCKY DERBY -->    
{*
        
					<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/racetracks/churchill-downs/churchill-downs-online-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Bet on the Kentucky Derby">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--Kentucky Derby Futures-->Bet the Kentucky Derby
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now
					</a></li>
*}
<!-- end slide -->


{*
				<!-- slide Preakness STAKES -->    
        			<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="/img/preakness-stakes-slider.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Preakness Stakes Betting">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--Kentucky Derby Futures-->Bet the Preakness Stakes
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup?ref=slider-preakness-stakes" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now
					</a></i>
*}

<!-- end slide -->	
		{*
		
				
	<!-- slide BELMONT STAKES -->    
        			<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="/img/belmont-stakes-slider.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Live Horse Racing">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--Kentucky Derby Futures-->Bet the Belmont Stakes
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">Exclusive Odds Found Nowhere Else!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup?ref=slider-bet-the-belmont" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i>Sign Up Now
					</a></i>

<!-- end slide -->		
*}		
{*<!-- slide FIRST SLIDE HAPPY YOUNG LADIES --> *}
               
 					<li data-transition="fade" data-slotamount="9" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					{* <img src="img/kentuckyderby/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Betting"> *}
					<img src="img/kentuckyderby/index-kentucky-derby-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Kentucky Derby Betting">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"> Online Horse Betting {* Get a Free Bet! *}
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"> {* No-Risk for All Members* *}Bet on  Over 200 Tracks!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup/?ref=slide-rebates" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Sign Up Now
					</a></li>
				
<!-- end slide -->	
<!-- slide YOUNG GUYS - REBATES-->  
             
 					<li data-transition="fade" data-slotamount="6" data-masterspeed="1000" >
					<!-- MAIN IMAGE -->
					<img src="img/online-horse-betting.jpg" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Rebates">
					<!-- LAYERS -->
					
                    <!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;">Rebates Paid Daily
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="0"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;"><!--You Play. We Pay.-->Up to 8%!  The Most Anywhere!
					</div>
                    
                    <!-- Button--> 
					<a href="/signup?ref=slide-200-tracks" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="80"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-thumbs-o-up left"></i> Sign Up Now
					</a></li>
					
<!-- end slide -->
<!-- slide HAPPY MAN PARTY IMAGE -->   
         
					<li data-transition="fade" data-slotamount="7" data-masterspeed="1000" >
					<!-- IMAGE -->
					<!-- <img src="/img/online-horseracing-bonus.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus">--><!-- slide HAPPY MAN --> 
                    <img src="/img/heros/horse-racing-bonus2.jpg" alt = "online horse racing" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat" alt="Horse Racing Bonus">
					<!-- Lrg Text -->
					<div class="tp-caption sfl fadeout txtLrg"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-80"
						data-speed="400"
						data-start="1000"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:90px;"><!--$150-->10% Instant Cash Bonus
					</div>
                    
                    <!-- Secondary Text -->
					<div class="tp-caption sfl fadeout txtMed"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="-10"
						data-speed="400"
						data-start="1300"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:40px;">and Qualify for Another $150! <!--Cash Back and Bonuses from US Racing-->
					</div>
                    
                    <!-- Button -->

					<a href="/signup?ref=slide-instant-cash" rel="nofollow" class="tp-caption sfl fadeout btn btn-red"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="70"
						data-speed="400"
						data-start="1600"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 4; font-size:30px;" ><i class="fa fa-star left"></i> Sign Up Now
					</a>                    
                    <!-- Learn More Button 
					
					<a href="#" rel="nofollow" class="tp-caption sfl fadeout btn btn-txt"
						data-x="right"
						data-hoffset="-100"
						data-y="center"
                        data-voffset="130"
						data-speed="400"
						data-start="1700"
						data-easing="Power4.easeOut"
						data-endspeed="400"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
                        data-toggle="modal" data-target="#bonus"
						style="z-index: 4; font-size:22px;">Learn More <i class="fa fa-question-circle"></i>
					</a>-->
					</li>
					
<!-- end slide -->





</ul>
<!--<div class="tp-bannertimer"></div>-->
</div><!-- end/tp-banner-container -->
</div><!-- end/tp-banner -->
</div><!-- end/SliderContainer -->


<!-- Modal1 -->
				{*
				<div class="modal fade" id="bonus" tabindex="-1">
				 <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"><button class="close" data-dismiss="modal" type="button">X</button>
				 <h3 class="modal-title" id="bonusLabel">US Racing's {include file='/home/ah/allhorse/public_html/shared/bonus-amount.tpl'} Free Cash Bonus</h3>
				 </div>
				 <div class="modal-body">
				 <p><strong>How our Bonus Works:</strong> &nbsp; <em>( Terms and Conditions )</em></p>
				 
				 {include file='/home/ah/allhorse/public_html/shared/bonus-terms-150.php'}
				 
				 </div>
				 <div class="modal-footer">
				 <button class="btn btn-lrg btn-default pull-left" data-dismiss="modal" type="button">I am not ready yet.</button>
				 <a class=" btn btn-lrg btn-red" href="/signup/?lm=Cash-Bonus" rel="nofollow" >Let's Start Wagering!</a>
				 </div>
				 </div>
				 </div>
				</div>

<!-- Modal2 -->
				<div class="modal fade" id="cashier" tabindex="-1">
				 <div class="modal-dialog"><div class="modal-content"><div class="modal-header"> <button class="close" data-dismiss="modal" type="button">X</button>
				 <h3 class="modal-title" id="cashierLabel">US Racing Easy Deposits & Withdrawals</h3>
				 </div>
				 <div class="modal-body">
				 <p><strong>How our Cashier Works:</strong></p>
				 <ul>
				 <li>Withdrawals are processed within 5 business days a the very latest.</li>
				 <li>You can deposit with either your MasterCard or Visa credit cards.</li>
				 <li>For your convenience, we also accept Western Union and Money Gram.</li>
				 <li>If you prefer, you can transact via our toll free number: 1-844-US-RACING.</li>
				 <li>All transactions are securely processed with 256-bit SSL Encryption.</li>
				 <li>New customers can qualify for our free $150 bonus. Conditions Apply.</li>
				 </ul>
				 </div>
				 <div class="modal-footer">
				 <button class="btn btn-lrg btn-default pull-left" data-dismiss="modal" type="button">I am not ready yet.</button>
				 <a class=" btn btn-lrg btn-red" href="/signup/?lm=Instant-Deposits" rel="nofollow" >Let's Start Wagering!</a>
				 </div>
				 </div>
				 </div>
				</div>
*}