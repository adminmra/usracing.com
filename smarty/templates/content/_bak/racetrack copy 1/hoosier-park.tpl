{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Hoosier Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Hoosier Park is located in Anderson, Indiana and is the first racetrack outside Kentucky owned by Churchill Downs since 1939.</p>
                    <p> The race course offers Harness, Thoroughbred, and Quarter horse racing, as well as high-profile Stakes events every year including the Plymouth Stakes; the Muskegon Classic; the Frontier Handicap; the Michigan Sire Stakes; and the Michigan Futurity.<br>
                          <br>
                          <strong>Hoosier Park Race Course <strong>Information:</strong></strong><br>
                          <br>
                          <strong>Live Racing:</strong> Tuesday through Saturday<br>
                          <strong>Live Racing Calendar:</strong> Live Standardbred Racing: March  - July , Live Thoroughbred &amp; Quarter Horse Racing August - October <strong><br>
                          <strong>Course Type:</strong></strong> Thoroughbred and standardbred<br>
                          <strong>Notable Races:</strong> City Of Anderson Stakes, Richmond Stakes, Indiana Breeders' Cup Oaks (Grade II), Indiana Derby (Grade II), Hillsdale Stakes, Gus Grissom Stakes, Hoosier Silver Cup Stakes - Filly Division, Hoosier Silver Cup Stakes, The Merrillville Stakes, Brickyard Stakes, Miss Indiana Stakes, Indiana Futurity<br>
                          <strong>Main Track:</strong> 7/8 mile<br>
                          <strong>Distance from last turn to finish line:</strong> 1,255 feet<br>
                          <strong>Turf Course:</strong> No turf course </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
