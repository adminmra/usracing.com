{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Pompano Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The Winter Home of Harness Racing," as Pompano Park is dubbed, this tracks is a standard bred harness racing track located in Pompano Beach, Florida. The 5/8th's mile track has some of harness racing's best and brightest stars competing on a  regular basis. The Isle offers the unique experience of a &ldquo;racino&rdquo; &ndash; a horse racing track and casino in a single, all-inclusive location. <br>
                      <br>
Opened in 1965, the track operates its' live racing meet for about ten months out of every year.<br>
<br>
The Mohegan Tribal Gaming Authority purchased the racetrack and its off-track wagering facilities in 2005 from Penn National Gaming and turned it into a "racino" called The Isle.</p>
                    <p><br>
                        <strong>Pompano Park </strong><strong>Racetrack </strong><strong>Information:</strong><br>
                        <br>
                        <strong>Live Racing: </strong>Monday, Tuesday, Wednesday and Saturday.<strong><br>
                          Live Racing Calendar:&nbsp; </strong>September - July&nbsp; <strong><br>
                            Course type: </strong>Standardbred harness<br>
  <strong>Notable Races: </strong>N/A<strong><br>
  </strong><strong>Main track:</strong>&nbsp; 5/8 mile, oval<br>
  <strong>Distance from last turn to finish line:</strong> 608 feet<br>
  <strong>Turf course:</strong> -</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
