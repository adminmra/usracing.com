{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Dover Downs</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</a>Located in Dover, Delaware, Dover Downs Racetrack has hosted harness racing events since 1969.<br>
<br>
Dover Downs has held major races, including the Dover Downs Progress Pace introduced in 1996. Other stakes events regularly held at Dover Downs include the Matron Series, the Classic Series and the Delaware Standard bred Breeders Fund.<br>
<br>
<strong>Dover Downs Racetrack Information:</strong><br>
<br>
<strong>Live Racing: </strong>November - April<br>
<strong>Live Racing Calendar: </strong>Sunday, Wednesday, Thursday &amp;<strong>Saturday </strong><br>
<strong>Course type: </strong>Thoroughbred - Flat racing<br>
<strong>Notable Races:</strong> Dover Downs Progress Pace, Matron Series<strong><br>
</strong><strong>Main track:</strong> 5/8 mile, bowl-shaped<br>
<strong>Distance from last turn to finish line:</strong>&nbsp; 500 feet<br>
<strong>Turf course:</strong> N/A



        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
