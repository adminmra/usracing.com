{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Hialeah Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Racing Dates:</strong> December to February<br />
                    </p>
                    <p>Hialeah Park Race Track &ndash; A Florida landmark, rich in history and excitement. </p>
                    <p>Hialeah Park Race Track, also known as the Miami Jockey Club and Hialeah Park, is a historic site located in Hialeah, Florida, just minutes from the Miami International Airport and Miami Beach. It is one of the oldest existing recreational facilities in southern Florida. Originally opened in 1921 by aviation pioneer Glenn Curtiss and his partner James Bright, the Miami Jockey Club re-launched the Hialeah Park Race Track four years later. </p>
                    <p>The Hialeah Park Race Track is a historic site in Hialeah, Florida. Its site covers 40 square blocks of central-east side Hialeah from Palm Avenue east to East 4th Avenue, and from East 22nd Street on the south to East 32nd Street on the north.</p>
                    <p>After the facility was severely damaged by a hurricane in 1926, it was sold to Joseph E. Widener, a wealthy Philadelphia-native horsemen, in 1930. Widener hired architect Lester W. Geisler to redesign the Hialeah Park Race Track, which included a new grandstand, Renaissance Revival facilities, landscaped gardens, and a lake in the infield that was stocked with flamingos. Hailed as one of the most beautiful racetracks in the world, the new and improve Hialeah Park Racetrack celebrated a grand reopening on January 14, 1932. Over time, it became so famous for its flocks of flamingos that it has been officially designated a sanctuary for the American Flamingo by the Audubon Society. </p>
                    <p>Over the course of the past eight decades, the Hialeah Park Racetrack hosted a number of high-profile races, including the aptly named Flamingo Stakes and Widener Handicap. It also played to host to movie productions, including Let It Ride with Richard Dreyfuss, Terri Garr, and Jennifer Tilly, as well as Public Enemies. On March 2, 1979, Hialeah Park was listed on the National Register of Historic Places. Later, in 1988, it was made eligible for designation as a National Historic Landmark. </p>
                    <p>To experience all that the Hialeah Park Racetrack has to offer, along with 150 of the world&rsquo;s best horse racing destinations, check out TVG. Our expansive list of horseracing tracks includes some of the most exclusive destinations, including Woodbine Race Course, Keeneland Race Course, Hollywood Park Racetrack, Meadowlands, Suffolk Downs, Monmouth Park, as well as popular international horseracing tracks in the United Kingdom, Ireland, and Japan!</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
