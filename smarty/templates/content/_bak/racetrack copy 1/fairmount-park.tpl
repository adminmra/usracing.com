{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Fairmount Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div>
                      <div>
                        <div>
                          <div id="trackContent">
                            <p><strong>Racing Dates:</strong> April to September<br />
                              <br />
                              Fairmount Park commenced its inaugural meet on   September 26, 1925. </p>
                            <p>The track record for 1 3/16 miles,   1:58 1/5, was set a 7-year-old gelding named Edward Gray on September 3, 1927   and the track record for 1 1/2 miles, 2:33, was set by a 5-gelding named Firth   of Tay on September 21 of that same season with both standards lasting for more   than seven decades. </p>
                            <p>Smile, the 1985 Fairmount Derby winner, was North America's   champion sprinter the following year. Smile's victory at Fairmount was achieved   with Hall of Famer jockey Jacinto Vasquez aboard.</p>
                          </div>
                        </div>
                      </div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
