{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Gulfstream Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Gulfstream Park is one of the most important venues for horse racing in the United States.</p>
                    <p>                      Gulfstream Park commenced its inaugural meet on February 1, 1939, it is now the premier East Coast track that operates during the winter. The annual Florida Derby Festival is a month-long celebration, culminating with the race itself. </p>
                    <p>                      During its annual meet, some of the world�s top thoroughbred contenders pass through Gulfstream Park with their eye winning the Triple Crown, making the track one of the most important venues in the country.<br>
                            <br>
                      Track Highlights include the $1,000,000 Florida Derby, which has generated 21 Kentucky Derby winners, including Barbaro and Big Brown, as well as the Sunshine Millions series, the $500,000 Donn Handicap, the Gulfstream Park Turf Handicap, the Fountain of Youth and Holy Bull stakes.</p>
                    <p>Gulfstream hosted the Breeders Cup in both 1989 and 1992, and will do so again during a special three-date meet in November of 1999
                      <br>
                            <br>
                            <span style="font-size: 14px;"><strong>Gulfstream Park </strong><strong>Information:</strong></span><br>
                            <br>
                          Live Racing: &nbsp;Thursday - Saturday</p>
                    <p><strong>                      Live Racing Calendar:&nbsp; </strong>December to April</p>
                    <p><strong>                      Course type: </strong>Flat</p>
                    <p><strong>                      Notable races: </strong>Sunshine Millions Day, Florida Derby, Donn Handicap, Gulfstream Park Turf Handicap, Fountain of Youth and Holy Bull stakes, and Gulfstream Park, Pan Am and Orchid handicaps</p>
                    <p><strong>                      Main track: </strong>1 1/18 mile, oval</p>
                    <p><strong>Distance from last turn to finish line:</strong> 898 feet</p>
                    <p><strong>Turf course:</strong> &nbsp;Seven furlongs<strong>      </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
