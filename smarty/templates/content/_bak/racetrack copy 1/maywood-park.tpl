{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Maywood Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Located along busy North Avenue between First and Fifth Avenues, less than 10   miles west of Chicago's bustling downtown area and only minutes away from O'Hare   airport, Maywood Park can trace its harness racing roots back to a time when the   racetrack was surrounded only by forest preserves and farm land.<br />
                        <br /> Built in 1946, Maywood Park has a long racing history The original home of the Cook County Fairgrounds, Maywood has held race meets since 1946, the longest in the State's standard bred racing history.<br>
                            <br>
                      In 1984, Maywood Park became the first track in Chicago, and only the second ever - to host a Breeders' Crown event. Maywood Park is the only Chicagoland racetrack devoted exclusively to standardbred racing.<br>
                            <br>
                            <strong>Maywood Park Racetrack Information:</strong><br>
                            <br>
                            <strong>Live Racing: </strong>January - December<br>
                            <strong>Live Racing Calendar: </strong>Thursday and Friday<br>
                            <strong>Race type: </strong>Standardbred<br>
                            <strong>Notable Races:</strong> Windy City Pace, Abe Lincoln, Maywood Filly Pace, Galt, Cinderella<strong><br>
                            </strong><strong>Main track:</strong> 1/2 mile, oval.<br>
                                                <strong>Distance from last turn to finish line:</strong>&nbsp; 594 feet<br>
                                                <strong>Turf course:</strong> N/A                    </p>
                    
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
