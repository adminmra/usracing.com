{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Tampa Bay Downs</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Tampa Bay Downs is the only thoroughbred race track on the West Coast of Florida. The track opened in 1926 under the name of Tampa Downs, and has been renamed several times since. It was   originally called &quot;Tampa Downs,&quot; then &quot;Sunshine Park,&quot; and after that &quot;Florida   Downs;&quot; the track was given its present name by George M. Steinbrenner III, wnen   he purchased a 50% interest in 1980.<br>
                        <br>
                      Tampa Bay Downs set new records in handle, attendance and purses paid, including an all-time attendance mark of 11,014 on Kentucky Derby Day, which featured Tampa Bay Derby winner Street Sense becoming the first graduate of the Tampa Bay Derby to win the prestigious Kentucky Derby.</p>
                    <p>                      The track's annual showcase is Festival Day held every March. The centerpiece is the $350,000, Grade II Tampa Bay Derby for 3-year-olds at a mile-and-a-sixteenth.<br>
                      <br>
                      <span style="font-size: 14px;"><strong>Tampa Bay Track Information:</strong></span><br>
                      <br>
                      <strong> Live Racing:</strong> Saturday to Tuesday<br>
                      <strong>Live Racing Calendar:</strong> December &nbsp;to May<strong><br>
                        Course type:</strong> Flat/Thoroughbred<br>
                      <strong>Notable Races:&nbsp;</strong> Tampa Bay Derby (G2), Endeavour Breeders Cup Stakes (G3), Hillsborough Stakes (G3), Sam F. Davis Stakes (G3)<strong><br>
                        Main track (dirt):</strong> 1 mile oval<br>
                        <strong>Distance from last turn to finish line:&nbsp;</strong> 1050 feet<br>
                        <strong>Turf course:</strong> 7 furlongs, 1/4 Mile Chute </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
