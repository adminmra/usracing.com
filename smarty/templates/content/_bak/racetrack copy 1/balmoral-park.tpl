{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Balmoral Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address: </strong>26435 S Dixie Hwy, Crete, IL 60417, United States</p>
                    <p>&nbsp;</p>
                    <p>Balmoral Park is a horse racing track located just south of Crete, Illinois, United States. The track hosts harness racing for most of the year except for a short time in January every year. Thoroughbred racing has been hosted occasionally throughout the years.</p>
                    <p>Founded in 1925, Balmoral Park officially held its first horse racing event in 1927 under the name of Lincoln Fields. The large oval was surrounded by Kentucky bluegrass which was imported from Kentucky. Red Spanish tile was used for the rooftops and spring-fed lakes were built in the infield. In 1955, Balmoral Jockey Club purchased Lincoln Fields. The same year, the name of the racetrack was changed to Balmoral Park. In 1967, one of Balmoral Park's tracks was converted to a half-mile track for harness racing track. </p>
                    <p>Regular summer Thoroughbred meets ran from 1978 through 1985. In 1988,   a new one-mile racing strip was built around the existing 5/8-mile oval.                      In 1991, Balmoral Park became a harness racing facility exclusively. And, in   1998, the track became Chicago&rsquo;s premier harness racing venue when Sportman&rsquo;s   Park ended their harness meet permanently.</p>
                    <p>                      There is no Live Racing on Super Bowl Sunday, Easter Sunday and Christmas Day (December 25). </p>
                    <p>&nbsp;</p>
                    <h2>Bet on Horses at Balmoral Park</h2>
                    <p>Place your bet at Balmoral Park online anywhere, anytime at US Racing. Betting on stakes races at Balmoral Park has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Balmoral Park Racetrack has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p>&nbsp;</p>
                    <h2>Balmoral Park Racetrack Information</h2>
                    <p><br>
                      <strong>Live Racing: </strong>All Year Long<br>
                    </p>
                    <p><strong>Live Racing Calendar: </strong><strong>Saturday, Sunday, Wednesday</strong><br>
                          </p>
                    <p><strong>Course type: </strong>Standardbred<br>
                          </p>
                    <p><strong>Notable Races:</strong> Hanover Stakes, Orange and Blue, Midwest Derby, Filly Orange and Blue, Pete Langley Memorial, Grandma Ann, Dan Patch Championship, Ann Vonian, Su Mac Lad, Lady Ann Reed, American National series, Lincoln Land, Lady Lincoln Land<strong><br>
                          </strong></p>
                    <p><strong>Main track:</strong> One Mile. Oval<br>
                            </p>
                    <p><strong>Distance from last turn to finish line:</strong>&nbsp; 1,360 feet<br>
                            </p>
                    <p><strong>Turf course:</strong> No Turf Course </p>
                    <p>&nbsp;</p>
                    <h2>Stake Races Held at Balmoral Park </h2>
                    <blockquote>
                      <p>Orange and Blue<br />
                        Filly Orange and Blue<br />
                        Pete Langley Memorial<br />
                        Grandma Ann<br />
                        Dan Patch Championship<br />
                        Ann Vonian<br />
                        Su Mac Lad<br />
                        Lady Ann Reed<br />
                        American National series<br />
                        Lincoln Land<br />
                        Lady Lincoln Land<br />
                      </p>
                    </blockquote>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
