{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Atlantic City</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address:</strong>  4501 Black Horse Pike, Mays Landing, NJ 08330, United States</p>
                    <p>&nbsp;</p>
                    <p>The Atlantic City Race Course (ACRC), formerly the Atlantic City Race Track, is a thoroughbred horse race track located in the Mays Landing section of Hamilton Township, in Atlantic County, New Jersey, United States.</p>
                    <p>Atlantic City  opened in 1946 but the Association was founded in 1944 by a group of businessmen, athletes, and Hollywood personalities, who all shared an unbridled passion for horse racing. Original stockholders included the legendary Bob Hope, Frank Sinatra, and six of the nation's big band leaders. John B. Kelly Sr., Olympic gold medalist and father of the late Princess Grace, was Atlantic City's first president.</p>
                    <p>Kelso, considered to be among the best racehorses of the twentieth century, made his two-year-old debut on September 4, 1959 at ACRC, at that time one of the country's premier tracks.</p>
                    <p>&nbsp;</p>
                    <h2>Atlantic City's All-Turf Festival</h2>
                    <ul>
                      <li>6 Turf Races Daily featuring the 8th Running of the $50,000-Added Tony Gatto Dream Big Stakes</li>
                      <li>Gates open at 11:00 am </li>
                      <li>3:30 First Post daily</li>
                      <li>Free addmission and parking</li>
                      <li>Free handicapping &amp; Newbie Seminars with &ldquo;Hudg&rdquo; beginning at 2pm in the Paddock</li>
                      <li>Open-air bar in the Beer Garden</li>
                      <li>Outdoor betting pavilion</li>
                      </ul>
                    <p>&nbsp;</p>
                    <h2>Bet on Horses at Atlantic City (ACRC)</h2>
                    <p>Place your bet at Atlantic City online anywhere, anytime at US Racing. Betting on stakes races at Atlantic City has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Atlantic City Race Course has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p>&nbsp;</p>
                    <h2>Atlantic City Race Course Information</h2>
                    <p><strong><br>
                      Live Racing Calendar:&nbsp;</strong>6 days - Late April / Early May<strong><br>
                        </strong></p>
                    <p><strong>Course type: </strong>&nbsp;Flat/Thoroughbred<strong><br>
                      </strong></p>
                    <p><strong>Main track: </strong>1 1/8 miles in length and 100 feet (30 m) wide, with a 7 furlong chute<br>
                          </p>
                    <p><strong>Distance from last turn to finish line:<br>
                      </strong></p>
                    <p><strong>Turf course:</strong> 1 mile oval </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
