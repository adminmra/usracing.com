{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Prairie Meadows</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


Located in the Altoona, about five miles from Des Moines, Iowa, Prairie Meadows Raceway has a one mile dirt track with a chute for quarter horses.<br>
<br>
The track, which conducted its first race on March 1<sup>st</sup>, 1989, was granted a horse-racing license from the Iowa State Racing Commission in 1984 and in 1987 a groundbreaking ceremony was held for Iowa's first horse track.<br>
<br>
Prairie Meadows runs several stakes and handicap events during its April to October season. From April &nbsp;to August the track conducts all thoroughbred races. From August to September all quarter horse races. Finally, all standardbred races are held in October.<br>
<br>
<strong>Prairie Meadows </strong><strong>Racetrack </strong><strong>Information:</strong><br>
<br>
<strong>Live Racing: </strong>Friday - Sunday<strong><br>
Live Racing Calendar:&nbsp; </strong>Thoroughbred racing: April through August. Quarter Horse racing: August through October<br>
<strong>Course type: </strong>Thoroughbred and Quarter Horse<br>
<strong>Notable Races: </strong>Cornhusker Handicap (Grade 2), Iowa Oaks (Grade 3), Iowa Derby (Grade 3), Bank of America Challenge Championships<strong><br>
</strong><strong>Main track:</strong> 1 mile, oval<br>
<strong>Distance from last turn to finish line:</strong> 1,033 feet<br>
<strong>Turf course:</strong> -<strong><br>
Bets available:<br>
Max Bet:</strong><br>



      
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
