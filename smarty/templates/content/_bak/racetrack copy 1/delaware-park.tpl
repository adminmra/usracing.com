{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Delaware Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Delaware Park Racetrack is the only thoroughbred horse racing track in the state of Delaware.<br>
                        <br>
                      Near-Triple Crown winner Afleet Alex and Kentucky Derby winner Barbaro, both won their career debuts at Delaware.<br>
  <br>
                      Races at Delaware Park are held from April to November, and include Grade II events such as Delaware Oaks, as well as Grade III races such as the annual Endine Stakes, Kent Stakes and Obeah Stakes.</p>
                    <p><strong>Address:</strong></p>
                    <p>777 Delaware Park Blvd<br />
                      Wilmington, DE 19804<br>
                    </p>
                    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Delaware+Park,+Wilmington,+DE&amp;aq=2&amp;oq=delaware+park&amp;sll=35.770436,-78.833415&amp;sspn=0.301959,0.566483&amp;ie=UTF8&amp;hq=Delaware+Park,+Wilmington,+DE&amp;t=m&amp;ll=39.703721,-75.667444&amp;spn=0.021683,0.030319&amp;output=embed"></iframe>
<p><br />
    <small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Delaware+Park,+Wilmington,+DE&amp;aq=2&amp;oq=delaware+park&amp;sll=35.770436,-78.833415&amp;sspn=0.301959,0.566483&amp;ie=UTF8&amp;hq=Delaware+Park,+Wilmington,+DE&amp;t=m&amp;ll=39.703721,-75.667444&amp;spn=0.021683,0.030319" style="color:#0000FF;text-align:left">View Larger Map</a></small>  </p>
<p>      <strong>Delaware Park </strong><strong>Race Course <strong>Information:</strong></strong><br>
      <br>
  Live Racing:Every Saturday, Monday, Wednesday &amp; Thursday<br>
      <strong>Live Racing Calendar:</strong> April through November<strong><br>
      <strong>Course Type:</strong></strong> Thoroughbred - Flat racing<br>
      <strong>Notable Races:</strong>&nbsp;Grade II: Delaware Oaks, Delaware Handicap. Grade III: Barbaro Stakes, Endine Stakes, Kent Stakes, Obeah Stakes, Robert G. Dick Memorial Handicap.<strong><br>
      <strong>Main Track:</strong></strong> One mile oval with six furlong and 1 1/4 mile chutes<br>
      <strong>Distance from last turn to finish line:</strong> 990 feet<br>
      <strong>Turf Course:</strong> 7 furlongs
  
  
</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
