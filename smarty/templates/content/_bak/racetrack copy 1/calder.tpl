{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Calder Race Course</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address:</strong> 21001 NW 27th Ave, Miami Gardens, FL</p>
                    <p>&nbsp;</p>
                    <p>Calder Race Course is a thoroughbred horse racing track in Miami Gardens, Florida. It's located on the Miami-Dade/Broward County line, approximately 15 miles from the centers of Miami and Ft. Lauderdale.</p>
                    <p>On May 6, 1971 Calder Race Course held its first day of racing and in January of 1999, Churchill Downs Incorporated purchased Calder Race Course for $86 million.</p>
                    <p>Calder Race Course hosts the biggest racing events in the Sunshine State, including the annual Florida Million, a $1.2 million, 8 race stakes program for Florida breds, and the Summit of Speed.</p>
                    <p>Calder's Summit of Speed has produced several <a href="/breeders-cup">Breeders' Cup</a> champions and Eclipse Award winners since it started in 2000. </p>
                    <p>Since 1982, Calder has hosted the Florida Stallion Stakes series annually which is a series of races for two-year-olds. Calder Race Course also holds several overnight stakes and handicaps. </p>
                    <p>Fun Facts about Calder:</p>
                    <ul>
                      <li>June 4, 2005, jockey Eddie Castro set the North American record for the most wins in a day at one track, winning 9 races at Calder.</li>
                      <li>The world record for the highest sale price of a thoroughbred took place at Calder in 2006. The two-year old horse who was called The Green Monkey sold for $16 million.</li>
                      </ul>
                    <p>&nbsp;</p>
                    <h2>Bet on Horses at Calder Race Course</h2>
                    <p>Place your bet at Calder Race Course online anywhere, anytime at US Racing. Betting on stakes races at Calder Race Course has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Calder Race Course has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p></p>
                    <h2><br>
                            <strong>Calder Race Course Information</strong><br>
                            <br>
                                      </h2>
                    <p><strong>Live Racing:</strong> Tuesday, Friday, Saturday and Sunday<br>
                      </p>
                    <p><strong>Live Racing Calendar:</strong> Summer Meet is held from April to October and the Fall / Winter Meet, is from October to January.<strong><br>
                      </strong></p>
                    <p><strong>Course type:</strong> Thoroughbred<br>
                        </p>
                    <p><strong>Notable Races:</strong>&nbsp; Princess Rooney Handicap, Carry Back Stakes, Smile Sprint Handicap<strong><br>
                      </strong></p>
                    <p><strong>Main track (Sand &amp; Clay):</strong> 1 mile with 1/4 and 7/8 chutes<br>
                          </p>
                    <p><strong>Distance from last turn to finish line:</strong> 990 feet<br>
                          </p>
                    <p><strong>Turf course:</strong> 8 furlongs            </p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Calder </h2>
                    <blockquote>
                      <p><br />
                        <strong>Grade I:</strong><br />
                        Princess Rooney Handicap</p>
                      <p><strong>Grade II:</strong><br />
                        Carry Back Stakes<br />
                        La Prevoyante Handicap<br />
                        Smile Sprint Handicap<br />
                        W.L. McKnight Handicap</p>
                      <p><strong>Grade III:</strong><br />
                        Azalea Stakes<br />
                        Calder Derby<br />
                        Frances A. Genter Stakes<br />
                        Fred W. Hooper Handicap<br />
                        Kenny Noe, Jr. Handicap<br />
                        Memorial Day Handicap<br />
                        Miami Mile Handicap<br />
                        My Charmer Handicap<br />
                        Stage Door Betty Handicap<br />
                        Spend A Buck Handicap<br />
                        Tropical Park Derby<br />
                        Tropical Turf Handicap</p>
                      <p><strong>Non-graded stakes:</strong><br />
                        Brave Raj Stakes<br />
                        J J's Dream Stakes<br />
                        Tropical Park Oaks<br />
                        What a Pleasure Stakes<br />
                      </p>
                      <p><strong>Florida Stallion Stakes:</strong><br />
                      Desert Vixen Stakes<br />
                        Dr. Fager Stakes<br />
                        Susan's Girl Stakes<br />
                        Affirmed Stakes<br />
                        My Dear Girl Stakes<br />
                        In Reality Stakes<br />
                      </p>
                      </blockquote>
                    <p>&nbsp;</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
