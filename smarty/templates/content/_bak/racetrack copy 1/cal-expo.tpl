{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                    {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>CAL EXPO</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address</strong>: 1600 Exposition Blvd., Sacramento, California 95815</p>
                    <p>&nbsp;</p>
                    <p>Cal Expo is home to the only live Harness Racing track on the West Coast. For a short number of action-packed days every fall, live harness racing thunders through the Cal Expo racetrack with 14 days of those racing dates during the California State Fair. It opens the 1st Saturday in October for Saturdays only in October then moves to 2 day per week schedule (Friday and Saturday) in November, December.</p>
                    <p>The mile-long racetrack encircles a lake with fountains surrounded by a lush green lawn. The track is overseen by a multi-purpose grandstand.</p>
                    <p>There is a Clubhouse located above the Sports &amp; Wagering Center which  is only open outside of live harness racing as a party venue during the four big race days of the year:</p>
                    <ul>
                      <li><a href="/breeders-cup">Breeders&rsquo; Cup</a></li>
                      <li><a href="/kentucky-derby">Kentucky Derby</a></li>
                      <li><a href="/belmont-stakes">Belmont Stakes</a></li>
                      <li><a href="/preakness-stakes">Preakness Stakes</a></li>
                    </ul>
                    <p>.</p>
                    <h2>Bet on Horses at Cal Expo</h2>
                    <p>Place your bet at Cal Expo online anywhere, anytime at US Racing. Betting on stakes races at Cal Expo has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Cal Expo Racetrack has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p><br />
                    </p>
                    <h2><br />
                        <strong>Cal Expo Racetrack Information</strong></h2>
                    <p><br />
                        <strong>Live Racing:</strong> Friday, Saturday <strong><br />
                      </strong></p>
                    <p><strong>Racing Dates:</strong> October to December</p>
                    <p><strong>Course type:</strong> Harness<strong><br />
                    </strong></p>
                    <p><strong>Main track:</strong> 1 mile<br />
                    </p>
                    <p></p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
