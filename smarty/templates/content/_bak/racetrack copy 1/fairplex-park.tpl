{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                    {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Fairplex Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


Located in Pomona, California, Fairplex Park Racetrack is a 5/8 mile racetrack.<br>
</span><br>
<br>
Fairplex Park features 13 days of live thoroughbred racing every September during the L.A. County Fair (September 9 - 25), plus several stakes events running each year. The racing season at Fairplex Park opens with the Beverly J. Lewis Stakes. Other events include Jim Kostoff Stakes, Governorís Cup, Bangles and Beads Stakes, Friday, Sept. 23. The C.B. Afflerbaugh Stakes (for 2-year-olds); the Phil D. Shepherd Stakes (for 2-year-old fillies),&nbsp; E.B. Johnston Stakes for Cal Breds (3-year-olds and upward); Las Madrinas Handicap and Pomona Derby, and the meetings signature race, the $75,000 guaranteed Ralph M. Hinds Pomona Invitational Handicap.<br>
<br>
<strong> Fairplex Park Track Information:</strong><span style="font-size: 14px;"><strong><br>
</strong></span><br>
<br>
<span style="font-size: 14px;"><strong> Live Racing:</strong> Wednesday to Wednesday</span><br>
<span style="font-size: 14px;"> <strong>Live Racing Calendar:</strong> Live racing runs for about 17 days at Fairplex Park race track during the LA County Fair usually held in September each year.</span><br>
<span style="font-size: 14px;"> <strong>Course type:</strong> Flat/Thoroughbred</span><br>
<span style="font-size: 14px;"> <strong>Notable Races:&nbsp;</strong> Derby Trial Stakes, Beau Brummel Stakes, Governorís Cup, Pio Pico Stakes, Barretts Debutante, Barretts Juvenille, Las Madrinas Handicap, Pomona Derby</span><br>
<span style="font-size: 14px;"> <strong>Main track</strong> (sandy loam surface): 5/8 mile oval</span><br>
<span style="font-size: 14px;"> <strong>Distance from last turn to finish line:&nbsp;</strong> 757 feet</span><br>
<span style="font-size: 14px;"> <strong> Turf course:</strong> N/A</span>


        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
