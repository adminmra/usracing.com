{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Aqueduct</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address: </strong>110-00 Rockaway Blvd, South Ozone Park, NY 11420, United States</p>
                    <p>Aqueduct Racetrack is a thoroughbred horse-racing facility and racino in South Ozone Park, Queens, New York and it's the only racetrack located within the New York City limits.</span></span><br>
                          <br>
                      Aqueduct has three courses: </p>
                    <ul>
                      <li>The Main Track (dirt) measuring 11/8 miles (1.8 km) </li>
                      <li>The inner track (dirt), which is exactly 1 mile (1.6 km) long</li>
                      <li>The innermost course is a turf (Tall Fescue) course, spanning 7 furlongs plus 43 feet (1.4 km)<br>
                      </li>
                    </ul>
                    <p><br />
</p>
<h2>Bet on Horses at Aqueduct</h2>
<p>Place your bet at Aqueduct online anywhere, anytime at US Racing. Betting on stakes races at Aqueduct has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Aqueduct Racetrack has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
<p><br>
</p>
<h2><strong>History of Aqueduct Racetrack</strong></h2>
<p>The racetrack is owned by State of New York &amp; Operated by New York Racing Association. Aqueduct closed in 1956 and re-opened in September 14, 1959 after a $33 million  renovationdesigned by noted racetrack architect Arthur Froehlich of the firm Arthur Froehlich and Associates of Beverly Hills, California.</p>
<p>Aqueduct commenced its inaugural meet on September 14, 1959. It is actually the second New York track to bear the name with the first, located on almost exactly the same plot of ground, was demolished in the mid-1950s.</p>
<p>Aqueduct was the first New York track to host a <a href="/breeders-cup">Breeders' Cup</a> race on November 2, 1985. The Cigar Mile is named for one of the greatest horses, Cigar, who won the race  in 1994.</p>
<p>&nbsp;</p>
<h2>Aqueduct Racetrack Information</h2>
<blockquote>
  <p><strong>  Live Racing: </strong>&nbsp;Wednesday - Sunday.<strong><br>
  </strong></p>
  <p><strong>Live Racing Calendar:&nbsp; </strong>January- April/November-December<strong><br>
  </strong></p>
  <p><strong>Course type: </strong>&nbsp;Flat/Thoroughbred<strong><br>
  </strong></p>
  <p><strong>Notable races: </strong>Wood Memorial Stakes, Cigar Mile Handicap, Carter Handicap, Gazelle Stakes, Gotham Stakes, Wood Memorial Stakes, Comely Stakes, Demoiselle Stakes, Distaff Handicap, Jerome Stakes, Red Smith Handicap, Jerome Stakes, Top Flight Handicap<strong><br>
  </strong></p>
  <p><strong>Main track: </strong>1 1/8&nbsp; miles, oval<br>
            </p>
  <p><strong>Distance from last turn to finish line:</strong> 1,346 feet<strong><br>
  </strong></p>
  <p><strong>Turf course:</strong> 7/8 mile</p>
</blockquote>
<p>&nbsp;</p>
<h2>Stake Races Run at Aqueduct</h2>
<blockquote>
  <p><strong>Grade I:</strong></p>
              <p>Carter Handicap<br />
                Cigar Mile Handicap<br />
                Wood Memorial Stakes</p>
              <p><strong>Grade II:</strong></p>
              <p>Comely Stakes<br />
                Demoiselle Stakes<br />
                Distaff Handicap<br />
                Go For Wand Handicap<br />
                Gazelle Handicap<br />
                Jerome Stakes<br />
                Red Smith Handicap<br />
                Remsen Stakes<br />
                Top Flight Handicap</p>
              <p><strong>Grade III:</strong></p>
              <p>Aqueduct Handicap<br />
                Athenia Handicap<br />
                Bay Shore Stakes<br />
                Beaugay Handicap<br />
                Bold Ruler Handicap<br />
                Busher Stakes<br />
                Cicada Stakes<br />
                Excelsior Handicap<br />
                Fort Marcy Handicap<br />
                Gotham Stakes<br />
                Knickerbocker Handicap<br />
                Long Island Handicap<br />
                Nashua Stakes<br />
                Next Move Handicap<br />
                Queens County Handicap<br />
                Stuyvesant Handicap<br />
                Tempted Stakes<br />
                Toboggan Handicap<br />
                Tom Fool Handicap<br />
                Turnback The Alarm Handicap<br />
                Valley Stream Stakes<br />
                Withers Stakes<br />
                Sport Page Handicap</p>
              <p>&nbsp;</p>
              <p><strong>Non-Graded:</strong></p>
              <p>Affectionately Handicap<br />
                Busanda Stakes<br />
                Count Fleet Stakes<br />
                Damon Runyon Stakes<br />
                East View Stakes<br />
                Gallant Fox Handicap<br />
                Gravesend Handicap<br />
                Hollie Hughes Handicap<br />
                Interborough Handicap<br />
                Jimmy Winkfield Stakes<br />
                Ruthless Stakes<br />
                Stymie Handicap<br />
                Whirlaway Stakes</p>
</blockquote>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
