{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Arlington Park</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address</strong>: 2200 W Euclid Ave, Arlington Heights, IL 60006, United States</p>
                    <p>&nbsp;</p>
                    <p>Arlington Race Park was founded by Harry D. &quot;Curly&quot; Brown and is located in the Chicago suburb of Arlington Heights in Illinois. The track is a major hub for thoroughbred horse racing in the United States and is described as &quot;the most beautiful track in America.&quot;</p>
                    <p>The history of Arlington Park spans more than 80 years. Arlington began   Thoroughbred racing on October 13, 1927, when more than 20,000 fans braved the   cold weather to celebrate the event in high style. In 1981, it was the site of the first thoroughbred race with a million-dollar purse.<br>
                          <br>
                      In addition to hosting some of the most important horse racing events in the North West, Arlington Park is known for introducing many innovations to the horse racing industry. The was the first track that installed a public-address system, in, 1933, which greatly reduced the time between races. In 1936 it added a photo finish camera. Arlington also introduced the first electric starting gate in 1940, the largest closed circuit TV system in all of sports in 1967, the trifecta wagering in 1971.</p>
                    <p>                      Arlington Park has invested heavily in the future of racing.</p>
                    <p>&nbsp;</p>
                    <h2>Bet on Horses at Arlington Park</h2>
                    <p>Place your bet at Arlington Park online anywhere, anytime at US Racing. Betting on stakes races at Arlington Park has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Arlington Park Racetrack has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p>&nbsp;</p>
                    <h2>Arlington Park Track Information</h2>
                    <p><br>
                      <strong>Live Racing:</strong> Tuesday, Friday, Saturday and Sunday<br>
                    </p>
                    <p><strong>Live Racing Calendar:</strong> 90 days live racing from May 4th to September 30th<strong><br>
                      </strong></p>
                    <p><strong>Course type:</strong> Flat<br>
                      </p>
                    <p><strong>Notable Races:&nbsp;</strong> Arlington Million, Beverly D. Stakes,&nbsp; Secretariat Stakes,&nbsp; American Derby<strong><br>
                    </strong></p>
                    <p><strong>Main track:</strong> 1 mile and one-eighth dirt oval and a 1 mile turf oval<br>
                    </p>
                    <p><strong>Distance from last turn to finish line:</strong> 1,028 feet<br>
                        </p>
                    <p><strong>Turf course:</strong> 7 1/2 furlongs, oval </p>
                    <p>&nbsp;</p>
                    <h2>Stakes Races at Arlington Park</h2>
                    <p>The following graded stakes are run at Arlington Park:</p>
                    <p>&nbsp;</p>
                    <blockquote>
                      <p><strong>Grade I</strong><br />
                        Arlington Million<br />
                        Beverly D. Stakes<br />
                        Secretariat Stakes<br />
                      </p>
                      <p><strong>Grade II    </strong><br />
                        American Derby<br />
                      </p>
                      <p><strong>Grade III</strong><br />
                        Arlington Classic Stakes<br />
                        Arlington Handicap<br />
                        Arlington Matron Handicap<br />
                        Arlington Oaks<br />
                        Arlington-Washington Futurity Stakes<br />
                        Arlington-Washington Lassie Stakes<br />
                        Chicago Handicap<br />
                        Hanshin Cup Handicap<br />
                        Modesty Handicap<br />
                        Pucker Up Stakes<br />
                        Sea o'Erin Stakes<br />
                        Stars and Stripes Turf Handicap<br />
                        Washington Park Handicap<br />
                      </p>
                      <p><strong>Listed                      </strong><br />
                        Arlington Sprint Handicap<br />
                        Isaac Murphy Handicap<br />
                        Lincoln Heritage Handicap<br />
                        Round Table Stakes (on hiatus)<br />
                        American 1000 Guineas Stakes</p>
                      <p><br />
                      </p>
                    </blockquote>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
