{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Hawthorne Race Course</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Hawthorne Race Track is the oldest continually-run family-owned racetrack in North America and has celebrated live thoroughbred racing in Chicago for over 120 years.</p>
                    <p>In 1890, Edward Corrigan, a Chicago businessman who owned the 1890 Kentucky   Derby winner, Riley, bought 119 acres of land in Cicero and started constructing   a grandstand for a new racecourse. His track opened in 1891 with a five-race   card including the featured Chicago Derby. In 1902, the grandstand burned to the   ground, which moved all racing to the Harlem racetrack in Chicago. The re-opened   track held a 12-day summer meet at its own facility later that year. </p>
                    <p>By 1927, the racetrack was gaining prominence on the national scene. A new   starting was introduced as was the Hawthorne Gold Cup Handicap, a major stakes   event. In 1929, Sun Beau won his first Gold Cup. He would later capture two   more. In 1931, an electric time was introduced as was an infield tote board. By 1970, harness racing was held at Hawthorne in an effort to offer a product to   lovers of standardbred racing.</p>
                    <p>                      As one of Illinois' most prominent horse racing tracks, Hawthorne hosts a big list of relevant racing events, including the annual Hawthorne Gold Cup Handicap and the Illinois Derby.  Hawthorne is now second tier to <a href="/racetrack/arlington-park">Arlington Park</a>, the premier thoroughbred track in Chicago.<br>
                            <br>
                            <strong>Hawthorne Race Course Information:</strong><br>
                            <br>
                            <strong>Live Racing Calendar:&nbsp;&nbsp;</strong>Hawthorne race track hosts thoroughbred racing in the Spring and Fall and harness racing in the Summer.<strong><br>
                            Course type:</strong> &nbsp;Flat/Thoroughbred<strong><br>
                            Notable races: </strong>Hawthorne Gold Cup Handicap - Grade II, Illinois Derby - Grade II, Hawthorne Derby - Grade III, National Jockey Club Handicap - Grade III, Robert F. Carey, Memorial Handicap - Grade III, Sixty Sails Handicap - Grade III<strong><br>
                            Main track: </strong>1 mile, oval<br>
                            <strong>Distance from last turn to finish line:</strong> 1,320 feet<strong><br>
                            Turf course:</strong> 7&nbsp; furlongs<span style="font-size: 14px;"><strong><br>
                            </strong></span>                                  </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
