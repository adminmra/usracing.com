{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Batavia Downs</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Address:</strong> 8315 Park Rd, Batavia, NY 14020, United States</p>
                    <p>&nbsp;</p>
                    <p>Batavia Downs is a harness racing track in Batavia, New York which is located in Genesee County between Buffalo and Rochester, just off of the New York State Thruway. The track first opened on September 20, 1940.</p>
                    <p><br />
                    </p>
                    <h2>Bet on Horses at Batavia Downs</h2>
                    <p>Place your bet at Batavia Downs online anywhere, anytime at US Racing. Betting on stakes races at Batavia Downs has never been easier. View live results, up to the minute schedules, betting odds and ive video. To experience all that the Batavia Downs Racetrack has to offer, along with   150 of the world&rsquo;s best horse racing destinations, <a href="/join">join</a> US Racing today!</p>
                    <p>&nbsp;</p>
                    <h2>Batavia Downs  Racetrack Information</h2>
                    <p><br />
                        <strong>Live Racing: </strong>July to December<br />
                    </p>
                    <p><strong>Live Racing Calendar: </strong>Saturday, Sunday, Monday, Tuesday, Wednesday<br />
                    </p>
                    <p><strong>Course type: </strong>Harness<strong><br />
                      </strong></p>
                    <p><strong>Main track:</strong> 1/2 mile<br />
                    </p>
                    <p><strong>Turf course:</strong> No Turf Course </p>
                    <p>&nbsp;</p>
                    <h2>Batavia Downs Track Records</h2>
                    <p><strong>Pacers</strong><br />
                      All Age Track Record: Aracache Hanover 1:51.1 2011 <br />
                      Aged Mares: Up front Kellie Jo 1:53.4 2012 <br />
                      Two-Year Old Colt: Doctor Butch 1:54.2 2012 <br />
                      Two-Year Old Filly: Paula&rsquo;s Best 1:55.0 2006 <br />
                      Three-Year Old Colt: Joey The Czar 1:52.1 2010 <br />
                      Three-Year Old Filly: New Album 1:52.4 2011 </p>
                    <p> <strong><br />
                      Trotters</strong><br />
                      All Age Track Record: Archangel 1:54.3 2012 <br />
                      Aged Mares: Cowgirl Hall 1:57 2013 <br />
                      Two-Year Old Colt: Dejarmbro 1:58.4 2010 <br />
                      Two-Year Old Filly: Dreamy Dawn 1:59 2010 <br />
                      Three-Year Old Colt: Archangel 1:54.3 2012 <br />
                      Three-Year Old Filly: Cowgirl Hall 1:57.2 2012 </p>
                    <p>&nbsp;</p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
