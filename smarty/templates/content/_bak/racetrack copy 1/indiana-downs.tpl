{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Indiana Downs</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>Racing Dates:</strong> April to November                     </p>
                    <p> Indiana Downs Race Track is located in Shelbyville, Indiana, which is roughly 15 minutes from Indianapolis. The Indiana Downs Race Track is a $35 million dollar state-of-the-art facility that hosts both Thoroughbred and harness races. </p>
                    <p>It has an indoor seating capacity of approximately 3,000 and is sited on a 200-acre tract. The main track is one mile long with a secondary turf track that measures seven furlongs. In March 2009, the 233,000-square-foot permanent Indiana Live! Casino opened, which is connected to the track�s clubhouse. </p>
                    <p>Indiana Downs Race Track is owned by Oliver Racing, LLC. 
                      
                      Horse racing fans of all ages and skill levels can take advantage of three levels of racing, dining, and entertainment at the Indiana Downs Racetrack. </p>
            
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
