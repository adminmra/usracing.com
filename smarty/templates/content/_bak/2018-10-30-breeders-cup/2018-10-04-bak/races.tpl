{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          

                    
          
<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Races
</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}  


<p>The Breeders' Cup changes its line up of races from time to time.  Here are the races for the {include file='/home/ah/allhorse/public_html/breeders/year.php'} year.</p><p></p><p></p>

    
				            
                  
     

<h2>Breeders' Cup  Classic 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/classic/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/classic/description.php'}


     
				            
                  
   
   

<h2>Breeders' Cup  Marathon 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/marathon/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/marathon/description.php'}


     
				            
                  
   
 
<h2>Breeders' Cup  Juvenile Fillies Turf 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Juvenile Fillies 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies/description.php'}


     
				            
                  
  

<h2>Breeders' Cup  Filly &  Mare Turf
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/filly-mare-turf/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/filly-mare-turf/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Ladies' Classic 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Juvenile Turf 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/juvenile-turf/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Filly & Mare Sprint 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Dirt Mile 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/dirt-mile/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/dirt-mile/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Turf Sprint 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/turf-sprint/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/turf-sprint/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Juvenile 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/juvenile/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Turf 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/turf/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/turf/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Expressbet Sprint 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/sprint/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/sprint/description.php'}


     
				            
                  
   

<h2>Breeders' Cup  Mile 
</h2>



<p>{include file='/home/ah/allhorse/public_html/breeders/mile/purse.php'}</p>
{include file='/home/ah/allhorse/public_html/breeders/mile/description.php'}


     
				            
                  

              
            {include file='inc/disclaimer-bc.tpl'} 
            
                      
     
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 