{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
     <!--------------- NEW HERO 2017 START -------------->
 
 <div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
  <!--------------- NEW HERO 2017 END -------------->      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
              
                                                
                                      
          
<div class="headline"><h1>Breeders' Cup Contenders</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='inc/breederscup_wherewhenwatch.tpl'}  
            {*include_php file='../smarty/libs/breederscup-race/contender.php'*} 
        
        

  
            
            
       {include file='inc/disclaimer-bc.tpl'}                
  
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 