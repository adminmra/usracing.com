{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
	{include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->
</div>

 <!--------------- NEW HERO 2017 START -------------->
 
 <div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
  <!--------------- NEW HERO 2017 END -------------->


<div id="main" class="container">
	<div class="row">

	<div id="left-col" class="col-md-9">

		<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup  Challenge</h1></div>


		<div class="content">
			<!-- --------------------- content starts here ---------------------- -->

			{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatchNew.tpl'}
			<p>Win a Breeders’ Cup Challenge race, and you’re in the Breeders’ Cup. With 67 qualifying races spanning 10 countries, each of the 14 Breeders’ Cup divisions have Challenge races. And a win gets you more than just an invitation. The Breeders’ Cup pays entry fees for the connections of the Challenge winners if they are nominated to the Breeders’ Cup program by October 21st. </p><p>All Championships starters that are not based in California will receive travel awards of $10,000 (US) if traveling within North America or $40,000 (US) if traveling from outside of North America. Last year, 45 winners of Breeders’ Cup Challenge races participated in the World Championships.</p>

			<h2>Breeders' Cup  Challenge Schedule</h2>
			{*include file='/home/ah/allhorse/public_html/breeders/challenge_scraped.php'*}
            {include_php file='/home/ah/allhorse/public_html/breeders/challenge.php'}
			{*include file="includes/ahr_block_breeders_challenge.tpl"*}
{include file='inc/disclaimer-bc.tpl'}
			<!-- ------------------------ content ends -------------------------- -->
		</div> <!-- end/ content -->
	</div> <!-- end/ #left-col -->


	<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction-bc.tpl'}
		{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}

	</div><!-- end: #right-col -->

	</div><!-- end/row -->
</div><!-- /#container -->
