{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
       <!--------------- NEW HERO 2017 START -------------->
 
 <div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
  <!--------------- NEW HERO 2017 END -------------->

      <!--{include file='/home/ah/allhorse/public_html/breeders/classic/video.php'}-->
      <div id="main" class="container">
<div class="row">

<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Classic</h1></div>
<div id="left-col" class="col-md-9">

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/breeders/classic/purse.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/description.php'}</p>

<!-- -->

<h2>Breeders' Cup Classic Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic.php'} </p>
{* <p>{include file='/home/ah/allhorse/public_html/breeders/odds_classic_static.php'} </p> *}
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<hr><!-- --------------------- HR ---------------------- -->

 <!-- <div><p><a href="/bet-on/breeders-cup"><img class="img-responsive" src="/img/breeders/breeders-cup-winner.jpg" alt="Bet on the Breeders' Cup"  />  </a></p></div>-->
<P>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}</p> 

<!-- -->

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Classic Results</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/results.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Classic Payouts</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/payouts.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->


<h2> Breeders' Cup  Classic Winners</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/winners.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->


<p>      {include file='inc/disclaimer-bc.tpl'} </p>
	
      
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/breeders/classic/call_to_action.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
