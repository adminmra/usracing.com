    {include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->
</div>

{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/video.php'}

      <div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">
<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Juvenile Fillies Turf
</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>
{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/purse.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/description.php'}</p>

<!-- 

<h2>Breeders Cup'  Juvenile Fillies Turf Odds</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_juvenile_fillies_turf_xml.php'} </p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>-->

<hr><!-- --------------------- HR ---------------------- -->
<p>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'} </p>

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Fillies Turf Results
</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/results.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Juvenile Fillies Turf Payouts
</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/payouts.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>

<h2> Breeders' Cup  Juvenile Fillies Turf Winners
</h2>

<p>{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/winners.php'}</p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>



         {include file='inc/disclaimer-bc.tpl'}
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='/home/ah/allhorse/public_html/breeders/juvenile-fillies-turf/call_to_action.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}

</div><!-- end: #right-col -->


</div><!-- end/row -->
</div><!-- /#container -->