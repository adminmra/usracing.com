{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<!--------- NEW HERO  ---------------------------------------->

<div class="newHeroM" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl topMargin" >{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   
     <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      


   <section class="kd bcs usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bc/bc.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/breeders/salesblurb.tpl'} </p>


     <p>Bet on the fastest horses from all over the world as they meet in    {include file='/home/ah/allhorse/public_html/breeders/location.php'}  this year to compete in the Breeders' Cup Races. With $25 million in prize money, it's the richest, most exciting event in sports making most other sporting events look quaint.</p>
<p>The end of year event attracts a lot of interest from Europe as well as the US. Horses go head to head in various grade divisions and distances for some of the biggest purses in horse racing which also makes it one of the best betting opportunities for horseplayers all year.  All race tracks that have hosted the <a href="/breeders-cup">Breeders' Cup</a> have been in the United States, except in 1996, when the Breeders' Cup was held at the <a href="/woodbine">Woodbine Racetrack</a> in Canada.</p>

 <h2>Bet on the Breeders' Cup</h2>
   <p> {include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}  </p>


				            



      </div>
      </div>
<!--    </section>
  <section> -->
 <div class="container">
 <div class="kd_content">
 
 <h2>Breeders' Cup Classic Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>

 <p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic.php'} </p>

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
 

<h2>How to Bet on the Breeders' Cup</h2>
<p><a href="/signup" rel="nofollow">Open an account</a> online at BUSR, America's online betting choice for horse racing. Fund your account then select the race you want to wager on. Pick your bet type, horse and you're off to the races. </p>
<p>When it comes to betting on the Breeders&rsquo; Cup, there are 4 races to choose from on Friday and 9 races on Saturday. The biggest event of the 2-day Championships is the last race on Saturday for a prize of $6,000,000! While the <a href="/breeders-cup/classic">Classic</a> has the biggest purse, the Mile is usually the most contentious race. </p>
<h2>Breeders' Cup Betting History</h2>
<p>Breeders' Cup reported that approximately $144.3 million was wagered on the World Championships in 2012 at <a href="/santa-anita-park">Santa Anita Park</a> which was down slightly from the previous year's figure of $155.5 million when the event was hosted by <a href="/churchill-downs">Churchill Downs</a>, in Louisville, Kentucky. Breeders' Cup officials indicated the handle totals were in line with expectations based on the interruption of service experienced by many east coast pari-mutuel outlets which were affected by Hurricane Sandy.</p>


<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Sign Up Now</a></p>  
 




 </div>
	</div>
  

    
 </section> 
            
      <!------------------------------------------------------------------>  
  <!--  <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> -->
    
  <!------------------------------------------------------------------>      
   {**}
   {*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-3.tpl"*}
  {*include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"*}

{* {include file="/home/ah/allhorse/public_html/belmont/block-testimonial-other.tpl"} *}
 {include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 

 {include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
 {include file="/home/ah/allhorse/public_html/usracing/block-50-states.tpl"} 
  {*include file="/home/ah/allhorse/public_html/breeders/block-countdown.tpl"*}
{include file="/home/ah/allhorse/public_html/breeders/block-exciting.tpl"}

  


<div id="main" class="container">
  {include file='inc/disclaimer-bc.tpl'} 
</div><!-- /#container --> 

