{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
 <!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
 
 	<div class="text text-md">{$h2}<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
                  

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<div class="headline"><h1>{*include file='/home/ah/allhorse/public_html/breeders/year.php'*} Breeders' Cup  Odds</h1></div>
<p>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatchNew.tpl'}  </p>

          
<h2>Bet on the Breeders' Cup</h2>

{* {include file='inc/btn-red.tpl'}  *}
<a name="classic"></a><h2>Breeders' Cup Classic Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/breeders/classic/odds_classic.php'}  </p>

	<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>  
{include file='inc/disclaimer-bc.tpl'} 
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
		{include file='inc/rightcol-calltoaction-bc.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
