{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          

                    
          
<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders Cup'  Ladies' Classic
</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}  
<p>{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/purse.php'}
</p>

{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/description.php'}



    
				            
                  
     

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Ladies' Classic Replay
</h2>



{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/video.php'}


     
				            
                  
   

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Ladies' Classic Results
</h2>



{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/results.php'}


     
				            
                  
   


<h2> Breeders' Cup  Ladies' Classic Winners
</h2>



{include file='/home/ah/allhorse/public_html/breeders/ladies-classic/winners.php'}


      
				            
                  
{include file='inc/disclaimer-bc.tpl'} 
              
            
            
                      
        
   
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 