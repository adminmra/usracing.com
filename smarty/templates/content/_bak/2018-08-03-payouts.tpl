<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
   
<div class="headline"><h1>Get Your Money Fast With Our Fast Payout Policy!</h2></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- --> 
<p><a href="/signup?i=Fast-Payouts"><img class="img-responsive" src="/img/horse-racing-rebates.jpg" alt="BUSR Payouts"  />  </a></p>

<p><blockquote><b>Why wait weeks or even months to get paid?  </b>
You can get paid out in as little as 2 days with bet usracing.    At bet usracing  you can bet from your home computer, mobile or tablet.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</blockquote></p>





<h3>How to Qualify</h3>
<p> Some websites will ask you for your Social Security Number (SSN) and ID BEFORE you even log in for the first time.  With bet usracing, you can sign up and log in without giving your SSN. Sign up and look around - if you decide you want to place wagers on horses or other sports or play exciting casino games, it's all there in one account.  You will be  asked for your Know Your Customer (KYC) documentation before your first withdrawal (if you had deposited by credit card) in order to get you playing as soon as possible. Yes, you can deposit instantly.</p>
<p>
	With bet usracing your financial and personal security is absolutely paramount. With KYC's it can be validated that:</p>

<ul>
    <li>You are at least 18 years of age.</li>
   <li> You are the true owner of the credit card (KYC, Know Your Customer).</li>
    <li>Your winnings go to the correct person: <b> you</b>.</li>

</ul>

<h3>Only Required for Members who Deposited by Credit Cards </h3>
	
	<ul>
    <li>A signed copy of the KYC Form.</li>
   <li>A scanned copy or photograph of your credit card and photo ID.</li>
    <li>A proof of address if different from your ID.</li>
   

</ul>
	<p>Thats it!</p>
	

	<p>The form can be quickly found in the Cashier and we suggest sending in the documentation before your payout in order to get you paid without delay. For members who use other deposit methods such as MoneyGram or Bitcoin, a proof of address may be requested. </p>
	<p><strong>Still not sure?</strong> This is standard industry practice for everybody's safety.  Find out more about why at <a href="http://www.sportsbookreview.com/sbr-news/online-sportsbooks-kyc-policies-2015-edition-59739/" target="_blank">SB Review</a>.
	<p><i>May not apply for high volume special events such as  the Kentucky Derby or Super Bowl.</i></p>
	<p>{* Please keep in mind, while there is a nominal fee for withdrawals, this is to ensure the integrity of the payment system and is require in order to process payouts in two days.  Without the fee, withdrawals would take significantly longer to process.  *}If you have any questions about your payout at all please call, toll free, at <a href="tel:1-844-BET-HORSES">1-844-BET-HORSES</a>.</p>
	
             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



{literal}
<script type="text/javascript">
$(document).ready(function() {
  $('.lnkDropItem').css("display","none");
});
function slideIt(thechosenone) {
     $('.lnkDropItem').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).slideDown(200);
			    $(this).prev().addClass("active");
          }
          else {
               $(this).slideUp(200);
			    $(this).prev().removeClass("active");
          }
     });
}
</script>
{/literal}
