<div class="container-fluid">{*include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"*} </div>

    <section class="ps-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-ps/friends.png" alt="" class="img-responsive ps-friends_img"></div>
      </div>
    </section>
    <section class="ps-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="ps-testimonial">
            <figure class="ps-testimonial_figure"><img src="/img/index-ps/derek-simon.jpg" alt="Derek Simon, Senior Editor, BUSR" class="ps-testimonial_img"></figure>
            <div class="ps-testimonial_content">
           <p class="testimonial_p">At BUSR, in addition to all the traditional Preakness wagers, you can bet on individual horse match-ups, and whether there will be a Triple Crown winner in {'Y'|date}!  {* At BUSR, in addition to all the traditional Derby bets, you can wager on individual horse match-ups, bet on jockeys and trainers and play a variety of fun and exciting props too!At BUSR, in addition to all the traditional Preakness bets, you can wager on individual horse match-ups, bet on jockeys and trainers and play a variety of fun and exciting props too! *}</p><span class="testimonial_name">Derek Simon</span><span class="testimonial_profile">Senior Editor and Handicapper at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
    
<!------------------------------------------------------------------>  
    
    <section class="ps-countdown ps-countdown--ps usr-section">
      <div class="container">
        <div class="ps-countdown_content">
          <h3 class="ps-countdown_heading"><span class="ps-countdown_heading-text">Countdown <br>to the Preakness Stakes</span></h3>
          <div class="ps-countdown_items">
            <div class="ps-countdown_item ps-countdown_item--yellow clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_days">days</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="ps-countdown_item ps-countdown_item--yellow clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="ps-countdown_num val">0</span><span class="ps-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("20 May 2017 18:15:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
    
    
<!------------------------------------------------------------------>    
    
    <section class="ps-kd ps-kd--default usr-section">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">Preakness Stakes Betting</h1>
          <h3 class="kd_subheading">With the Most Preakness Stakes Bet Options Anywhere</h3>
          <p>{* Only at Bet BUSR, you get great future odds with amazing payouts on the leading Kentucky Oaks and Preakness horses and even the leading jockeys and trainers. *}Only at BUSR, you get great future odds with amazing payouts on the leading Preakness horses.</p>
          <p>{* Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more.  Preakness Stakes Odds are live, bet now. *}Will the winner of the {'Y'|date}  Kentucky Derby win the Preakness Stakes?  Will a horse lead wire to wire? {* Will Secretariat's 1973 Record be broken? *} <p> BUSR offers more wagering choices than other site.</p><a href="/preakness-states/odds" class="kd_btn">LIVE ODDS</a>
        </div>
      </div>
      </div>
    </section>
<!------------------------------------------------------------------>  

    <section class="ps-pimlico"><img src="/img/index-ps/bg-pimlico.jpg" alt="Pimlico" class="ps-pimlico_img"></section>
    <section class="ps-card">
      <div class="ps-card_wrap">
        <div class="ps-card_half ps-card_content"><img src="/img/index-ps/icon-preakness-stakes-betting.png" alt="Bet on the Preakness Stakes" class="ps-card_icon">
        <h2 class="card_heading">Free Preakness  Bet</h2>
          <h3 class="card_subheading">$10 FREE Preakness Stakes Bet for Members</h3>
          <p>As a member, you'll can qualify for a guaranteed no-lose bet.  If you win your bet, great! If you lose the wager, your $10 will be returned to your account.</p>
          <p>You can only win with BUSR</p><a href="/signup?ref=Free-Preakness-Bet" class="card_btn">Join Now</a>
        </div>
        <div class="ps-card_half ps-card_hide-mobile"><img src="" data-src-img="/img/index-ps/preakness-stakes-betting.jpg" alt="Preakness Stakes Betting" class="ps-card_img"></div>
      </div>
    </section>
 <!------------------------------------------------------------------>      
    
    <section class="ps-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="ps-testimonial-blue">
          <figure class="ps-testimonial-blue_figure"><img src="/img/index-kd/horseracingguy.jpg" alt="Racing Dudes" class="ps-testimonial-blue_img"></figure>
          <div class="ps-testimonial-blue_content">
            <p class="ps-testimonial-blue_p">When it comes to the Preakness Stakes, BUSR has you covered. They provide may unique betting options; more than andy body else.</p><span class="ps-testimonial-blue_name">
               
              - The Racing Dudes
             {*  <cite class="ps-testimonial-blue_profile">Feature Writer for BUSR, Lady and the Track & Horse Racing Nation.</cite> *}</span>
          </div>
        </blockquote>
      </div>
    </section>
    
<!------------------------------------------------------------------>    
        <section class="ps-card">
      <div class="ps-card_wrap">
        <div class="ps-card_half ps-card_hide-mobile"><img src="" data-src-img="/img/index-ps/triple-crown-blackjack-tournament.jpg" alt="Preakness Stakes Blackjack" class="ps-card_img">
        </div>
        <div class="ps-card_half ps-card_content"><img src="/img/index-ps/icon-triple-crown-blackjack-tournament.png" alt="Preakness Stakes Tournament" class="ps-card_icon">
          <h2 class="ps-card_heading">
             
    Preakness Stakes<br>Blackjack Tournament
          </h2>
          <p>BUSR invites you test your mettle and win cash in our  Blackjack Tournament.</p>
          <p>Play any Blackjack game from May 6th to May 19th and get ready to compete for real cash. Thousands of dollars in cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/promos/blackjack-tournament?ref=PS-TC-Blackjack-Tournament" class="card_btn">Play Now</a>
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------>  
{*
    <section class="ps-card">
      <div class="ps-card_wrap">
        <div class="ps-card_half ps-card_hide-mobile"><img src="" data-src-img="/img/index-ps/bet-kentucky-derby.jpg" alt="Bet on the Kentucky Derby" class="ps-card_img"></div>
        <div class="ps-card_half ps-card_content"><img src="/img/index-ps/icon-bet-kentucky-derby.png" alt="Bet on Kentucky Derby" class="ps-card_icon">
          <h2 class="ps-card_heading">Bet on the Trainers</h2>
          <h3 class="ps-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  BUSR is the only place to bet on Trainers and Jockeys for the Kentucky Derby.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/kentucky-derby/trainer-betting" class="ps-card_btn">Live Odds</a>
        </div>
      </div>
    </section>*}
  <!------------------------------------------------------------------>     
    
    <section class="ps-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="ps-bonus_heading">10% Sign Up Bonus</h2>
         <h3 class="bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>{* For your first deposit with BUSR, you'll get an additional 10% bonus to your deposit absolutely free. No Limits! *}For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>{* Deposit a minimum of $100 and you could qualify to earn an additional $150! *}Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref=cash-bonus-10" class="bonus_btn">CLAIM YOUR BONUS</a>

          </div>
        </div>
      </div>
    </section>

    
 <!------------------------------------------------------------------>   
    
   
    
    <section class="ps-card">
      <div class="ps-card_wrap">
        <div class="ps-card_half ps-card_content"><img src="/img/index-ps/icon-casino-rebate.png" alt="BUSR Casino Rebate" class="ps-card_icon">
          <h2 class="ps-card_heading">50% Casino Cash Back</h2>
          <h3 class="ps-card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/login?ref=Thursday-Casino-Cash-Back" class="ps-card_btn">Play Now</a>
        </div>
        <div class="ps-card_half ps-card_hide-mobile"><img src="" data-src-img="/img/index-ps/casino-rebate.jpg" alt="Casino Rebate" class="ps-card_img">
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------>      
    <section class="ps-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="ps-testimonial-blue">
          <div class="ps-testimonial-blue_center">
            <p class="ps-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="ps-testimonial-blue_name">
               
              Tom G.<br>
              <cite class="ps-testimonial-blue_profile">BUSR Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    
 <!------------------------------------------------------------------>      
    
    <section class="ps-card ps-card--bg">
      <div class="ps-card_wrap">
        <div class="ps-card_half ps-card_hide-mobile"><img src="" data-src-img="/img/index-ps/horse-betting.png" alt="" class="ps-card_img">
        </div>
        <div class="ps-card_half ps-card_content"><img src="/img/index-ps/icon-horse-betting.png" alt="Horse Betting" class="ps-card_icon">
          <h2 class="ps-card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="ps-card_subheading">Members Can Bet from Anywhere</h3>
          <p>With BUSR, you can bet on horses and more from anywhere in the great U.S. of A. and it's territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, players from Canada, the United Kingdom, and most other countries* are welcome to be members, too. We look forward to welcoming you aboard.</p><a href="/signup/?ref=50-States" class="ps-card_btn">Join Now</a>
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------>      
    
{*
    <section class="ps-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="ps-payouts_heading">
               
              fast<span>2</span>day payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with Bet BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="ps-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index-ps/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
*}
    
 <!------------------------------------------------------------------>      
    
  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Preakness Stakes Action at <br>BUSR</h2>
            <a href="/signup?ref=index-PS-CTA-bottom" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>
    </section>

