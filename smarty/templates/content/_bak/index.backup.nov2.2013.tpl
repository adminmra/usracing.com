
  
    
    
    
     
    
    
    <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




                  <div class="homeIntro">
                    <h1 class="title">Online Horse Racing</h1></div>
                    
                    	<div class="copy first" style="clear: right;">
                     Welcome to US Racing - a licensed <a href="/adw" title="Advance Deposit Wagering">ADW</a> provider and the leader in online horse racing betting. US Racing offers betting at over 150 racetracks  with authentic <a href="/otb" title="Off Track Betting">otb off-track</a> racing odds right from the comfort of your computer or <a href="/mobile" title="Mobile Horse Betting">mobile</a> phone. Bet on horses legally with the name you trust. Get paid full track odds. Whether you want to place a <a href="/bet-on/kentucky-derby" title="Kentucky Derby Betting">bet on the Kentucky Derby</a> at <a href="/churchill-downs" title="Churchill Downs">Churchill Downs</a> or the summer races at <a href="/saratoga" title="Saratoga Race Course">Saratoga</a>, US Racing is the <a href="/" title="Best Horse Racing Online">best horse racing site</a> for the person who is looking for legal and licensed <a href="/online-horse-wagering" title="Online Horse Wagering">horse wagering</a>.
</p>
                      
                      
                    </div>
                    
                    <div class="copy">
                    <p>Live Horse Racing video is available at US Racing along with race replays of horse racing. Free <a href="/handicapping" title="Horse Racing Handicapping">handicapping</a> material for our members. It's time to get ready for the world championships of racing-- the <a href="/breeders-cup" title="Bet on Breeders' Cup">2013 Breeders' Cup</a>. The 2013 <a href="/breeders-cup/challenge" title="Breeders' Cup Challenge">Breeders' Cup Championships</a> will take place on November 1st and 2nd at beautiful <a href="/santa-anita-park" title="Bet Santa Anita">Santa Anita Park</a>. <a href="/breeders-cup/betting" title="Breeders' Cup Betting">Breeders' Cup Betting</a> is available at US Racing. Full track odds. US Licensed horse racing site. Legal horse betting for US citizens.
</p>                     </div>
                     
                  </div>
                
              
            
          
          <!-- /block-inner, /block -->
          
          
          
          
            
          

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



    <div id="bottom_content">
      <div id="block-block-6" class="block block-block region-odd odd region-count-1 count-5">
        
      </div>
      <!-- /block-inner, /block --> 
      
    </div>
  

<!-- /#main-inner -->

<!-- /#main --> 

