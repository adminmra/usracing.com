{include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"}

<div class="container">

 <!-- Service Blocks -->
 <div id="service" class="row margin-bottom-30">
  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h1>Online Horse Betting</h1>
     </div>
     <div class="thumbnail margin-bottom-20"><img src="img/online-horse-racing.jpg" class="img-responsive" alt="Online Horse Betting" /></div>
     <div class="content">
     BUSR is an <a href="/advance-deposit-wagering">Advance Deposit Wagering</a> provider and  the leader in online horse racing betting. BUSR is the <a href="/best-horse-racing-site" title="best horse racing site">best horse racing site</a> for the person who is looking for  horse wagering. <a href="/bet-on-horses">Bet on horses</a> with the name you trust!
     </div>

     <div class="blockfooter">
      <a href="/online-horse-racing" title="Online Horse Racing" class="btn btn-primary">Learn More<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>

  {*NOTE:  Swapped middle and right columns*}
  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h2>Bet Horses Online</h2>
     </div>
     <div class="thumbnail margin-bottom-20">



  <img src="img/horse-racing-bonus.jpg" class="img-responsive" alt="Horse Racing Bonus" /></div>


     <div class="content">
     <strong> Free for Joining!</strong> With BUSR you get a 8% rebate, Horse Matchup Wagers, handicapping and more. We trust you'll love it that much we'll give you a <a href="/cash-bonus">$150 CASH Bonus</a> on your first deposit. Just <a href="/signup/?i=ohb" rel="nofollow" >Sign Up</a>, play and away you go! {* Daily and weekly Horse Racing Specials give you a chance to double your winnings every single day.   *} </div>

     <div class="blockfooter">
       <a rel="nofollow" href="/signup/?i=ohb" title="Sign Up for Free" class="btn btn-primary btn-red">Sign Up for Free<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>

  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h2>Fast Two Day Payouts</h2>
     </div>
     <div class="thumbnail margin-bottom-20"><img src="img/horse-betting-payouts.jpg" class="img-responsive" alt="Horse Wagering"/></div>
     <div class="content">
	     <strong>Why wait weeks or even months to get paid with those other guys?</strong> You can get <a href="/payouts">paid out in two days</a> with BUSR, the fastest in the industry.  You'll get fast service and fast payouts every time via Bank Wire, Cashier Check, Money Gram, and Western Union.

{*    With the secure and easy betting interface, you'll get fast service and fast payouts every time and we never share your personal information.  With your credit card, making a deposit  and wagering is now easier than ever!   *}
     </div>

     <div class="blockfooter">
      <a href="/payouts" title="Learn More" class="btn btn-primary">Learn More<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>




 </div> <!-- end/row-->
</div> <!-- end/container-->




<!-- === Divider Bar ========================================================================== -->
{*
<div id="dvdr1" class="dividerBar margin-bottom-40">
 <div class="container">
  <i class="icon fa fa-ticket"></i>
  <table class="info" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
  <strong>Bet on Horses</strong>
  <br />
  </td></tr></table>

  <h3>Saratoga Horse Racing</h3>
  <a href="/saratoga" class="btn btn-sm btn-simple-white hidden-xs" >Learn More<i class="fa fa-angle-right"></i></a>
  <a href="/saratoga" class="btn btn-sm btn-simple-white visible-xs"><i class="fa fa-angle-right"></i></a>
 </div>
</div><!-- end/dividarBar-->
*}
<div id="dvdr2" class="dividerBar margin-bottom-40">
 <div class="container">
  <i class="icon fa fa-film"></i>
  <table class="info" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
  <strong>Bet online horse racing</strong>
  <br />Desktop, Mobile & Tablet
  </td></tr></table>

  <h3>Bet Horse Racing Live!</h3>
  <a href="/best-horse-racing-site" class="btn btn-sm btn-simple-white hidden-xs" >Learn More<i class="fa fa-angle-right"></i></a>
  <a href="/best-horse-racing-site" class="btn btn-sm btn-simple-white visible-xs" ><i class="fa fa-angle-right"></i></a>
 </div>
</div>

<div class="container">
<!-- === Content Row =========================================================================== -->
<div class="row home-mid margin-bottom-30">
<div class="col-md-8">

{*
 <div class="events margin-bottom-30">
	<div class="headline"><h2>Big Horse Races</h2></div>
	<div class="row margin-bottom-15">
	<div class="col-md-4 first"><span class="thumbnail"><img class="img-responsive" src="/img/kentucky-derby-roses.jpg" alt="Kentucky Derby Betting" /></span></div>
	<div class="col-md-4"><span class="thumbnail"><img class="img-responsive" src="/img/kentucky-derby-racing.jpg" alt="Bet Kentucky Derby"/></span></div>
	<div class="col-md-4 last"><span class="thumbnail"><img class="img-responsive" src="/img/kentucky-derby-winner.jpg" alt="Kentucky Derby Winner" /></span></div>
	</div>

	{include file="inc/home_events.tpl"}

<div class="blockfooter"><a href="/road-to-the-roses" class="btn btn-primary">Road to the Roses <i class="fa fa-angle-right"></i></a></div>
</div>
*}



<!-- NEWS -->
{*
<div class="news news-home margin-bottom-30">
<div class="headline"><h2>Horse Racing News</h2></div>
{include file='includes/ahr_block_racing_news.tpl'}
<div class="blockfooter">
      <a href="/news" class="btn btn-primary">Horse Racing News <i class="fa fa-angle-right"></i></a>
</div>
</div>
*}
<!-- end/NEWS -->
 {include file='inc/home_blog.tpl'}

 <!--<img src="/img/kentuckyderby/2016-kentucky-derby-odds.jpg" class="img-responsive" alt="Responsive image" style="width;100%">-->

{*// ODDS BLOCK
<div class="blog-home margin-bottom-20">
    <div class="headline"><h2>Breeders' Cup Betting</h2></div>
    <div class="blog" >
	    <P>Can American Pharoah complete  the most compelling season in the history of horse racing?  He's got eight challengers.  Place your bets and be a part of history.  Afterwards enjoy our Breeders' Cup Blackjack Tournament or bet on the World Series with Live-In-Game wagering.</P>

    {include_php file='/home/ah/allhorse/public_html/breeders/odds_classic_static.php'}
   <p align="center"><br><a href="/signup" class="btn-xlrg ">Bet Now</a></p>
    </div>
</div>
*}



</div> <!-- end col-md-8-->



<div class="col-md-4">
{**********COUNTDOWN**************}
{include file='/home/ah/allhorse/public_html/weekly/countdown.tpl'}

{**********CARRY OVERS***********}
{*<div id="carryovers" class="margin-bottom-30">
<div class="headline"><h2>Today's Carryovers</h2><a data-toggle="modal" data-target="#carryoverHelp" class="btn btn-sm btn-default"><i class="fa fa-question"></i></a></div>*}
{*include file="inc/home_carryovers.tpl"*}
 <!-- </div>  -->





<div id="graded" class="gradedStakes margin-bottom-30">
    <div class="headline"><h2>Horse Races to Watch</h2></div>

 	<div id="graded-content" class="dateCarousel carousel slide carousel-v1" >
    <div class="carousel-inner margin-bottom-20">
		{include file="includes/graded-stakes-home.tpl"}
	</div><!-- end/carousel-inner -->

	  <div class="carousel-arrow">
      <a data-slide="prev" href="#graded-content" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
      <a data-slide="next" href="#graded-content" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
      </div>
      <div class="blockfooter">
      <a href="/graded-stakes-races" class="btn btn-primary">Horse Races to Watch <i class="fa fa-angle-right"></i></a>
      </div>

</div> <!-- end/graded-content -->
</div> <!-- end/graded -->




<div id="leaders" class="topLeaders margin-bottom-30">

{*


    <div class="headline"><h2>Horse Racing Leaders</h2></div>
*}

    {*include file="includes/ahr_block_top_leaders.tpl"*}



</div> <!-- end/leaders -->
     <div class="headline">

     	{include file="inc/home_twitter.tpl"}
     {* <div class="headline"> *}
{*include file='inc/rightcol-calltoaction.tpl'*}

</div><!-- end/col-md-4-->
</div><!-- end/row-->
</div><!-- end/container-->

</div>

{*
<!-- === Divider Bar ========================================================================== -->

<div id="dvdr2" class="dividerBar margin-bottom-40">
 <div class="container">
  <i class="icon fa fa-film"></i>
  <table class="info" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
  <strong>Bet online horse racing</strong>
  <br />Desktop, Mobile & Tablet
  </td></tr></table>

  <h3>Bet Horse Racing Live!</h3>
  <a href="/signup/" class="btn btn-sm btn-simple-white hidden-xs" >Learn More<i class="fa fa-angle-right"></i></a>
  <a href="/signup/" class="btn btn-sm btn-simple-white visible-xs" ><i class="fa fa-angle-right"></i></a>
 </div>
</div>

<!-- end/dividarBar-->
*}



{*
<!-- === Content Row ========================================================================== -->
<div class="container">
 <div class="row bottom home-bottom">

 <div class="col-md-4">

   		{include file="/home/ah/allhorse/public_html/weekly/video_usr.tpl"}

  </div>
<!-- end/col-md-4-->

  <div class="col-md-4">

     	{include file="inc/home_twitter.tpl"}

  </div><!-- end/col-md-4-->

  <div class="col-md-4">

   		{include file="inc/home_shop.tpl"}

  </div><!-- end/col-md-4-->

 </div><!-- end/row-->
</div><!-- end/container-->
*}

