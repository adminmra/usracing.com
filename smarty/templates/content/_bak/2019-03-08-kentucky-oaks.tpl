{assign var="ref" value="kentucky-oaks"}
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

 {include file='menus/kentuckyderby.tpl'}



<!-- ---------------------- end left menu contents ------------------- -->         

</div>



<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl-left">{$h1}</div>
        <div class="text text-md-left">{$h2}
                <br>
                        <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
        </div>
 </div>


<!--

<div class="newHero" style="background-image: url(/img/kentuckyderby/2019_kd_hero01.jpg);" alt="Kentucky Derby Betting">



  <div class="text text-xl">Bet on the Kentucky Oaks</div>



  <div class="text text-xl" style="margin-top:0px;"></div>



  <div class="text text-md">The Most Kentucky Oaks Bets Anywhere!<br>



    <a href="/signup?ref=Kentucky Derby">



    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>Sign Up Now</div>



    </a> </div>



</div>
-->

<!--<div class="container-fluid">

	<a href="/signup?ref=kentucky-derby">

		<img class="img-responsive" src="/img/kentuckyderby/2017-Kentucky-Derby-betting.jpg" alt="Kentucky Derby Betting">

	</a>

</div> -->    

      



<div id="main" class="container">

<div class="row">





<div id="left-col" class="col-md-9">                                  

          

<div class="headline"><h1>The Kentucky Oaks</h1></div>







<div class="content">

<!-- --------------------- content starts here ---------------------- -->





{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch_ko.tpl'} 

    {include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'}<br>

{include file='/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_1006.php'}</p>
 {*include file="/home/ah/allhorse/public_html/oddsfeed/odds/1006.php"*}

<h2>{*History of Kentucky Oaks</h2>

<p>The Kentucky Oaks dates back to the earliest days of Churchill Downs, which was known as the Louisville Jockey Club when it conducted its first race meet in 1875. Its first running was held Wednesday, May 19, 1875 as one of four stakes races developed by founder M. Lewis Clark for the track's inaugural meet. The others were the Kentucky Derby, the Clark Handicap and the Falls City Handicap. The Kentucky Oaks is renewed each year on the Friday before the <a title=" Kentucky Derby," href="/kentucky-derby">Kentucky Derby,</a>&nbsp;but it is much more than a sister race to the famed "Run for the Roses."</p>

<p>The roster of horses that have won the 1 1/8 mile (1811 metres) race in its first 130 years includes some of the greatest fillies in racing history. Three of those races — the <a title=" Kentucky Derby" href="/kentucky-derby">Kentucky Derby</a>, the Kentucky Oaks and the Clark — were modeled after Classic races in England. The Kentucky Oaks was fashioned in the image of the English Oaks at Epsom Downs.</p>

<p>The distance of that first running of the Kentucky Oaks was 1 ½ miles (2414 metres) and A.B. Lewis &amp; Company's Vinaigrette was the winner. She earned a winning purse of $1,175 and was timed over the 12 furlong (2414 metres) distance in 2:39 ¾.</p>

<p>The victory by Vinaigrette launched a strong tradition for the Kentucky Oaks, which — like the Kentucky Derby — has been renewed each year without interruption since its inaugural running.</p>

<p>Louisvillians once referred to the Oaks as "our Derby," implying that the Derby itself had become the domain of celebrities and out-of-town visitors. In recent years that has changed, as the Oaks has become nearly as popular as the Derby. Indeed, the spectator attendance on "Oaks Day" is usually second only to Derby Day, surpassing attendance at the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a> and the <a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a>, as well as the <a title="Breeders' Cup" href="/breeders-cup">Breeders' Cup</a>.</p>

<p>The Kentucky Oaks is one of three races that are the <em>de facto</em> distaff counterparts to the Triple Crown races, along with the Black-Eyed Susan Stakes at Pimlico Race Course and the Acorn Stakes at Belmont Park. They have not, however, been officially referred to as the "Filly Triple Crown"; that distinction goes to three races run at Belmont and Saratoga Race Course in New York, now officially called the Triple Tiara to avoid trademark conflicts. However, consideration has been given within the National Thoroughbred Racing Association (NTRA), the sport's governing body in the United States, to change the Triple Tiara series to the Kentucky Oaks, Black-Eyed Susan and Acorn.</p>>



<h2>The Kentucky Oaks has been run at four different distances:</h2>

<ul>

<li>1 1/2 miles (2414 metres) — 1875 to 1890 </li>

<li>1 1/4 miles (2012 metres) — 1891 to 1895 </li>

<li>1 1/16 miles (1710 metres) — 1896 to 1919 and 1942 to 1981 </li>

<li>1 1/8 miles (1811 metres) — 1920 to 1941 and 1982 to present*}</li>

</ul>



             <p align="center"><a href="https://www.usracing.com/login?ref={$ref}" class="btn-xlrg ">Bet on the Kentucky Oaks</a></p>



<!-- ------------------------ content ends -------------------------- -->

</div> <!-- end/ content -->

</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-ko.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 



 

</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 

</div><!-- /#container --> 









      

    
