
 {include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"} 

    <section class="bs-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index/friends.png" alt="friends" class="bs-friends_img img-responsive"></div>
      </div>
    </section>
    <section class="bs-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bs-testimonial">
            <figure class="bs-testimonial_figure"><img src="/img/index/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="bs-testimonial_img"></figure>
            <div class="bs-testimonial_content">
              <p class="bs-testimonial_p">At BUSR, in addition to all the traditional horse bets, you can bet on individual horse match-ups, sports and awesome casino games on your phone!</p><span class="bs-testimonial_name">Derek Simon</span><span class="bs-testimonial_profile">Senior Editor and  Handicapper  at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
<!------------------------------------------------------------------>      
   {* // COUNTDOWN CODE
 <section class="bs-countdown bs-countdown--belmont usr-section">
      <div class="container">
        <div class="bs-countdown_content">
          <h3 class="bs-countdown_heading"><span class="bs-countdown_heading-text">Countdown <br>to Belmont Stakes</span></h3>
          <div class="bs-countdown_items">
            <div class="bs-countdown_item bs-countdown_item--green clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_days">days</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("11 Jun 2016 18:30:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
*}
  <!------------------------------------------------------------------>    
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
       {*    <img src="/img/index/bs.png" alt="Online Horse Betting" class="kd_logo img-responsive"> *}
          <h1 class="kd_heading">Horse Betting</h1>
          <h3 class="kd_subheading">With the Most Betting Options Anywhere</h3>
          <p>Bet in the comfort of you home or on the go with BUSR's mobile betting.  Whether you want to bet on horses or sports, you can bet easily with your iPhone. <p> Waiting at the bar for your friends to arrive?  How about playing a few hands of blackjack or roulette!  BUSR - super easy and super fun. </p><a href="/odds?ref={$ref}" class="bs-kd_btn">SEE THE LIVE ODDS</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
{*     <section class="bs-belmont_"><img src="/img/index/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> *}
    
    
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-horse-betting-rebates.png" alt="Horse Betting Rebates Icon" class="bs-card_icon">
          <h2 class="bs-card_heading">Up to 8% Horse Racing Rebates</h2>
          <h3 class="bs-card_subheading">Paid Daily Into Your Account</h3>
          <p>Every day when you log into your account you account you will find an 8% or 5% rebate on your of your exotic wagers and a 3% rebate on Win, Place and Show in your account from yesterday's bets!</p>
          <p>You can only win with BUSR!</p><a href="/rebates?ref={$ref}" class="bs-card_btn">Learn More About Rebates</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/horse-betting-rebates.jpg" alt="Horse Betting Rebates" class="bs-card_img"></div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <figure class="bs-testimonial-blue_figure">{* <img src="/img/index/laura-pugh.jpg" alt="Laura Pugh, Feature US Racing Writer" class="bs-testimonial-blue_img"> *}</figure>
          <div class="bs-testimonial-blue_content">
            <p class="bs-testimonial-blue_p">When it comes to  horse betting, BUSRg has you covered. They provide many unique betting options, more than anybody else.</p>{*
<span class="bs-testimonial-blue_name"> Laura Pugh, <br>
              <cite class="bs-testimonial-blue_profile">Feature Writer for US Racing, Lady and the Track & Horse Racing Nation.</cite></span>
*}
          </div>
        </blockquote>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/triple-crown-blackjack-tournament.jpg" alt="Belmont Stakes Blackjack" class="bs-card_img">
        </div>
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-triple-crown-blackjack-tournament.png" alt="Belmont Stakes Tournament" class="bs-card_icon">
          <h2 class="bs-card_heading">Enjoy Over 150 of Your Favorite Casino Games</h2>
          <p>At BUSR you can play video slots, Blackjack, Roulette, Video Poker, Table and Lottery Games, Keno  fully 3D Poker with stunning graphics and 3D animation.</p><p>Play now and enjoy jackpots up to $63,000!</p><a href="/signup??ref={$ref}-Casino-Games" class="bs-card_btn">Play Now</a>
          
{*
                    <h2 class="bs-card_heading">Triple Crown<br>Blackjack Tournament</h2>
          <p>US Racing invites you test your mettle and win cash in our Triple Crown Blackjack Tournament.</p>
          <p>Play any Blackjack game from May 2nd to June 12th and get ready to compete for real cash. Thousands of dollars in cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/login?ref=TC-Blackjack-Tournament" class="bs-card_btn">Play Now</a>
          
*}
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
     {*
   <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/bet-belmont-stakes.jpg" alt="Bet on the Belmont Stakes" class="bs-card_img"></div>
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-bet-belmont-stakes.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Bet on the Trainers</h2>
          <h3 class="bs-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Belmont Stakes.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/belmont-stakes/trainer-betting" class="bs-card_btn">Live Odds</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
*}
    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref={$ref}-cash-bonus-10" class="bs-bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  

    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-casino-rebate.png" alt="US Racing Casino Rebate" class="bs-card_icon">
          <h2 class="bs-card_heading">50% Casino Cash Back</h2>
          <h3 class="bs-card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/promos/casino-rebate?ref={$ref}" class="bs-card_btn">Find Out More</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/casino-rebate.jpg" alt="Casino Rebate" class="bs-card_img">
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
      <section class="bc-racetracks">
      <div class="container">
        <h2 class="bc-racetracks_title">
           
          Choose from <strong>211 </strong>different racetracks
        </h2>
        <ul class="bc-racetracks_list">
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-thoroughbred-racing.png" alt="Thoroughbred Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Thoroughbred</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-harness-racing.png" alt="Harness Racing Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Harness</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-quarter-horse-racing.png" alt="Quarter Horse Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Quarter Horses</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-international-horse-racing.png" alt="International  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">International</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="/racetracks" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/index/icon-greyhound-racing.png" alt="Greyhound  Racetracks" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Greyhound</h4>
              </div></a></li>
        </ul>
      </div>
    </section>
    
  <!------------------------------------------------------------------>   
    
    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/mobile-horse-betting.png" alt="Mobile Horse Betting" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-mobile-horse-betting.png" alt="Mobile Horsed Betting Icon" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the go!</h2>
          <p>Take all the races with you! All tracks are available on phone or tablet at the track, at home, everywhere.</p> <p>Are you out at the bar with your friends? Bet on the game any time, anywhere.  Or, while waiting for your friends to arrive,  how about playing a few hands of blackjack or roulette! <p> BUSR - super easy and super fun.</p><a href="/mobile-horse-betting?ref={$ref}" class="bc-card_btn-red">See Your Mobile Betting Options</a>
        </div>
      </div>
    </section>
    
 <!------------------------------------------------------------------> 
    
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-online-sports-betting.png" alt="icon-online-sports-betting" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on Sports</h2>
          <h3 class="bc-card_subheading">Football Season is Here!</h3>
          <p>Not only can you bet on horses but  also sports with:

    <br><br>Live In-Game Betting
     <br>Best Player and Team Propositions
   <br> Complete Baseball Betting Odds
   <br>Fights, Politics and Entertainment Bets
 <br>Quick & Easy Payouts Within 48 Hours


</p><a href="/signup?ref={$ref}" class="bc-card_btn-red">Bet on sports</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/online-sports-betting.jpg" alt="Online Sports Betting" class="bc-card_img"></div>
      </div>
    </section>

    
 <!------------------------------------------------------------------>    
    
    
    <section class="bs-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <div class="bs-testimonial-blue_center">
            <p class="bs-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="bs-testimonial-blue_name">Tom G.<br>
              <cite class="bs-testimonial-blue_profile">BUSR Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-card bs-card--bg">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index/horse-betting.png" alt="horse-betting" class="bs-card_img"></div>
        <div class="bs-card_half bs-card_content"><img src="/img/index/icon-horse-betting.png" alt="icon-horse-betting" class="bs-card_icon">
          <h2 class="bs-card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="bs-card_subheading">Members Can Bet from Anywhere</h3>
          <p>With BUSR, you can bet on horses and more from anywhere in the great U.S. of A. and its territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, players from Canada, the United Kingdom, and most other countries are welcome to be members, too! We look forward to welcoming you aboard.</p><a href="/signup/?ref={$ref}-50-States" class="bs-card_btn">Join Now</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bs-payouts_heading">
               
              fast payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bs-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    
    <section class="bc-card bc-card--hre">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index/icon-horse-racing-experts.png" alt="Horse Racing Experts" class="bc-card_icon">
          <h2 class="bc-card_heading">Horse Racing Experts</h2>
          <p>US Racing's Authors and Handicappers will make you a better player.  Get the latest news, odds, race reports and betting advice from our experienced group of horse racing writers.</p><a href="/news" class="bc-card_btn-red">Read the Latest </a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index/horse-racing-experts.png" alt="Expert Handicappers" class="bc-card_img"></div>
      </div>
    </section>

    
    <!------------------------------------------------------------------> 
    
    
  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Horse Betting Action<br> at BUSR</h2>
            <a href="/signup?ref={$ref}-CTA2" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>
    </section>


