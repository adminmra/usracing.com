<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Responsible Gaming</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->





<p >US Racing and its partners are committed to responsible gaming. For the vast majority of players, online gaming is a fun and recreational pastime. We also realize that, for some, it can become a problem that adversely affects their lives and the lives of those around them.</p>
<p >While we pride ourselves on marketing services and products that are accessible to our members whenever and wherever they are, unfortunately our service has the potential to be abused. Like so many other activities, when online gaming becomes compulsive and uncontrolled, it becomes a problem. As such, we take very seriously our responsibility to provide a gaming service in a socially responsible way.</p>
<p >Here is how we promote responsible gaming:</p>
<p ><b>Age Limits:</b></p>
<p >Online gaming is a form of adult entertainment and it is illegal for anyone under the age of 18 to play on the products or services provided by our partner. All of our members must be 18 or older and we reserve the right to ask for verification documents from any member and may deactivate any account until such proof is received.</p>
<p >Anyone under the age of 18 that is found to be gaming on our partner platforms will have all winnings forfeited.</p>
<p >To prevent underage access we recommend the Internet's most trusted parental control filtering sites:</p>
<ul >
  <li ><a href="http://www.netnanny.com" rel="nofollow">www.netnanny.com</a></li>
  <li ><a href="http://www.cybersitter.com" rel="nofollow">www.cybersitter.com</a></li>
  <li ><a href="http://www.cyberpatrol.com" rel="nofollow">www.cyberpatrol.com</a></li>
  <li ><a href="http://www.contentwatch.com" rel="nofollow">www.contentwatch.com</a></li>
</ul><br>
<p ><b>Personal Limits:</b></p>
<p >It's easy for unmanaged gaming to become problem gaming, which is why we can limit player's betting amounts however they wish. We also have the ability to block a member's access to the site if they have a gaming problem. If you think you have problem we urge you to take this self-assessment test. The few minutes it takes could be warning sign you need.</p>
<p ><b>Getting Help:</b></p>
<p >If you believe you have a gaming problem there are always people who understand your issue and help is always available. The Nevada Council on Problem Gambling has a 24-hour, toll-free hot line at 1-800-522-4700. You can also visit the Gamblers Anonymous website here. This is a non-profit organization that is in no way affiliated with US Racing or its gaming partners.</p>
<p ><b>Working Together:</b></p>

<p >If you have any queries about these policies or our stance on responsible gaming, please <a href="/support">Contact Us</a>.



             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div><!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
