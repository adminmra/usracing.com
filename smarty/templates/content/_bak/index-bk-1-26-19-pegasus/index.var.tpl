<?xml version="1.0" encoding="ISO-8859-1"?>
<pagevariables>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>US Racing | Online Horse Betting</title>
<!--<meta name="description" content="US Racing provides online horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit."  /> Original before 12/20/18-TF -->


<!--Regular Season!--><!--TF 12/20/18 -->
<!-- <meta name="description" content="Bet on horses with 200 racetracks and an 8% rebate paid to you daily. Join now to get a 10% Cash Bonus instantly and qualify for a $150 bonus. Learn more here about online horse betting"  />  -->
<!--End Regular Season-->

<!--Pegasus!--><!--TF 01/23/12 -->
<meta name="description" content="Bet the Pegasus World Cup now, odds are live!  New members get up to a $625, exclusive odds, Kentucky Derby Futures and more!"  /> 
<!--End Pegasus-->

<meta name="keywords" content="online horse betting, horse racing, bet on derby, otb, offtrack, kentucky derby betting, preakness stakes, belmont stakes, horse betting, odds"  />
<link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />

<meta property="og:title" content="US Racing | Online Horse Betting" />

<meta property="og:url" content="https://www.usracing.com/" />
<meta property="og:image" content="https://www.usracing.com/img/best-horse-racing-betting-site.jpg" />


<!--Regular Season!--><!--TF 12/20/18 -->
<!-- <meta property="og:description" content="Bet on horses with 200 racetracks and an 8% rebate paid to you daily. Join now to get a 10% Cash Bonus instantly and qualify for a $150 bonus. Learn more here about online horse betting" /> -->
<!--End Regular Season-->

<!--Pegasus!--><!--TF 01/23/12 -->
<meta property="og:description"  content="Bet the Pegasus World Cup now, odds are live!  New members get up to a $625, exclusive odds, Kentucky Derby Futures and more!"  /> 
<!--End Pegasus-->



<css rel="stylesheet" type="text/css" href="/assets/plugins/rs/css/settings.css" media="screen" />
<css rel="stylesheet" type="text/css" href="/assets/css/rs_v5.css" media="screen" />
<js type="text/javascript" src="assets/plugins/countdown/jbclock.js" />
<js type="text/javascript" src="assets/plugins/rs/js/rs.plugins.min.js" />
<js type="text/javascript" src="assets/plugins/rs/js/rs.revolution.min.js" />
<js type="text/javascript" src="assets/plugins/rs/js/rs.revolution.init.js" />
<js type="text/javascript" src="assets/js/index.js" />	

<variables>
	  	   <ref>index</ref>
</variables>



</pagevariables>
