{include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"}

    <section class="friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-kd/friends.png" alt="" class="img-responsive friends_img"></div>
      </div>
    </section>
    <section class="testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="testimonial">
            <figure class="testimonial_figure"><img src="/img/index-kd/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="testimonial_img"></figure>
            <div class="testimonial_content">
              <p class="testimonial_p">At US Racing, in addition to all the traditional Derby wagers, you can bet on individual horse match-ups, jockeys, trainers and whether there will be a Triple Crown winner in 2016!  {* At US Racing, in addition to all the traditional Derby bets, you can wager on individual horse match-ups, bet on jockeys and trainers and play a variety of fun and exciting props too! *}</p><span class="testimonial_name">Derek Simon</span><span class="testimonial_profile">Senior Editor, Handicapper and Horse Player at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
    <section class="countdown usr-section">
      <div class="container">
        <h3 class="countdown_heading"><span class="countdown_heading-text">Countdown <br>to Kentucky Derby 142</span></h3>
        <div class="countdown_items">
          <div class="countdown_item clock_days">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_days"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_days">days</span></div>
          </div>
          <div class="countdown_item clock_hours">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_hours"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_hours">hours</span></div>
          </div>
          <div class="countdown_item clock_minutes">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_minutes"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_minutes">minutes</span></div>
          </div>
          <div class="countdown_item clock_seconds">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_seconds"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_seconds">seconds</span></div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("7 May 2016 18:34:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
         
            <img src="/img/index-kd/kd.png" alt="Online Horse Betting" class="kd_logo img-responsive">
        
          <h1 class="kd_heading">Online Horse Betting</h1>
          <h3 class="kd_subheading">With the Most Kentucky Derby Bet Options Anywhere</h3>
          <p>{* Only at BUSR, you get great future odds with amazing payouts on the leading Kentucky Oaks and Derby horses and even the leading jockeys and trainers. *}Only at US Racing, you get great future odds with amazing payouts on the leading Kentucky Oaks and Derby horses and even the leading jockeys and trainers.</p>
          <p>{* Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more. Kentucky Derby Odds are live, bet now. *}Will the winner of the 2016 Kentucky Derby win the Preakness Stakes?  Will a horse lead wire to wire? Will Secretariat's 1973 Record be broken? <p> US Racing offers more wagering choices than other site.</p><a href="/kentucky-derby/odds" class="kd_btn">LIVE ODDS</a>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content"><img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby" class="card_icon">
          <h2 class="card_heading">Kentucky Derby Bet</h2>
          <h3 class="card_subheading">$10 FREE Kentucky Derby Bet for Members</h3>
          <p>As a member, you'll get a free bet on the Kentucky Derby.  If you win your bet, great! If you lose the wager, your $10 will be returned to your account.</p>
          <p>You can only win with US Racing!</p><a href="/signup?ref=Free-Derby-Bet" class="card_btn">Join Now</a>
        </div>
        <div class="card_half card_hide-mobile"><img src="" data-src-img="/img/index-kd/kentucky-derby-betting.jpg" alt="Kentucky Derby Betting" class="card_img"></div>
      </div>
    </section>
    <section class="testimonials--blue usr-section">
      <div class="container">
        <blockquote class="testimonial-blue">
          <figure class="testimonial-blue_figure"><img src="/img/index-kd/laura-pugh.jpg" alt="Laura Pugh, Feature US Racing Writer" class="testimonial-blue_img"></figure>
          <div class="testimonial-blue_content">
            <p class="testimonial-blue_p">When it comes to the Kentucky Derby, US Racing has you covered. They provide may unique betting options, even allowing you to wager on the trainers and jockeys! How cool is that?</p><span class="testimonial-blue_name">
               
               Laura Pugh, <br>
              <cite class="testimonial-blue_profile">Feature Writer for US Racing, Lady and the Track & Horse Racing Nation.</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile"><img src="" data-src-img="/img/index-kd/bet-kentucky-derby.jpg" alt="Bet on the Kentucky Derby" class="card_img"></div>
        <div class="card_half card_content"><img src="/img/index-kd/icon-bet-kentucky-derby.png" alt="Bet on Kentucky Derby" class="card_icon">
          <h2 class="card_heading">Bet on the Trainers</h2>
          <h3 class="card_subheading">And Other Exclusive Derby Props</h3>
          <p>{* Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher? *}Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Kentucky Derby. </p>
          <p>{* Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more. Kentucky Derby Odds are live, bet now. *}Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/kentucky-derby/trainer-betting" class="card_btn">Live Odds</a>
        </div>
      </div>
    </section>
{*
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content"><img src="/img/index-kd/icon_3.png" alt="" class="card_icon">
          <h2 class="card_heading">Bet on JOCKEYS</h2>
          <h3 class="card_subheading">142ND Kectucky Derby</h3>
          <p>With over 85 free high-converting mobile responsive templates within Unbounce and the nearly 200 available for purchase on ThemeForest, your landing pages will look as good as they perform. Customize them to suit your needs using the most flexible and powerful landing page builder available.</p><a href="#" class="card_btn">Live Odds</a>
        </div>
        <div class="card_half"><img src="" data-src-img="/img/index-kd/3.jpg" alt="" class="card_img"></div>
      </div>
    </section>
*}
    <section class="bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
            <h2 class="bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>{* For your first deposit with US Racing, you'll get an additional 10% bonus to your deposit absolutely free. No Limits! *}For your first deposit with US Racing, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>{* Deposit a minimum of $100 and you could qualify to earn an additional $150! *}Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref=cash-bonus-10" class="bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section>
   
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile"><img src="" data-src-img="/img/index-kd/triple-crown-blackjack-tournament.jpg" alt="Kentucky Derby Blackjack" class="card_img">
        </div>
        <div class="card_half card_content"><img src="/img/index-kd/icon-triple-crown-blackjack-tournament.png" alt="kentucky Derby Tournament" class="card_icon">
          <h2 class="card_heading">
             
            Triple Crown<br>Blackjack Tournament
          </h2>
          <p>US Racing invites you test your mettle and win cash in our Triple Crown Blackjack Tournament.</p>
          <p>Play any Blackjack game from May 2nd to June 12th and get ready to compete for real cash. Thousands of dollars in cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/login?ref=TC-Blackjack-Tournament" class="card_btn">Play Now</a>
        </div>
      </div>
    </section>
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content"><img src="/img/index-kd/icon-casino-rebate.png" alt="US Racing Casino Rebate" class="card_icon">
          <h2 class="card_heading">50% Casino Cash Back</h2>
          <h3 class="card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at US Racing because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday. </p><a href="/login?ref=Thursday-Casino-Cash-Back" class="card_btn">Play Now</a>
        </div>
        <div class="card_half card_hide-mobile"><img src="" data-src-img="/img/index-kd/casino-rebate.jpg" alt="Casino Rebate" class="card_img">
        </div>
      </div>
    </section>
     <section class="testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="testimonial-blue">
         {*  <figure class="testimonial-blue_figure"><img src="/img/index-kd/testimonial-user-3.jpg" alt="Testimonial" class="testimonial-blue_img"></figure> *}
          <div class="testimonial-blue_center">
            <p class="testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!! </p><span class="testimonial-blue_name">
               
             Tom G. <br>
              <cite class="testimonial-blue_profile">US Racing Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    <section class="card card--bg">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile"><img src="" data-src-img="/img/index-kd/horse-betting.png" alt="" class="card_img">
        </div>
        <div class="card_half card_content"><img src="/img/index-kd/icon-horse-betting.png" alt="" class="card_icon">
          <h2 class="card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="card_subheading">Members Can Bet from Anywhere</h3>
          <p>With US Racing, you can bet on horses and more from anywhere in the great U.S. of A. and it's territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, Canadians players, the United Kingdom and most other countries* are welcome to be members, too. We look forward to welcoming you aboard.</p><a href="/signup/?ref=50-States" class="card_btn">Join Now</a>
        </div>
      </div>
    </section>
{*
    <section class="payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
            <h2 class="payouts_heading">
               
              fast<span>2</span>day payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">
            <figure class="payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index-kd/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
*}
    <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Kentucky Derby Action at US Racing</h2>
            <a href="/signup?ref=CTA" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>





















