
 

 


<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

 
                                        
          
<div class="headline"><h1>US Racing Privacy Policy</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p class="h6">Last modified: March 28, 2017</p>
<p>US Racing and it’s gaming provider, BUSR, are 100% committed to protecting your privacy. The following discloses the information we gather and why. Equally important, we disclose what we will not do with information gathered.</p>
<p>We never disclose information to any agencies, nor do we sell the information that we may obtain from you. Please read on for more specific details of our privacy policy.</p>
<p><b>Our Commitment to You</b></p>
<p>Protecting your privacy is a top priority. We use the information we collect on the site only to facilitate the exchange of wagering funds, and to enhance your overall gaming experience.</p>
<p>We do not sell, trade, or rent your personal information to others. Our gaming provider, BUSR, is a duly licensed gaming establishment. We are not required to disclose any player of financial information to any government agency.</p>
<p><b>What information do we collect and why?</b></p>
<p>Our policy is to collect no more information than is absolutely necessary.</p>
<p>When you make a deposit, in order to place a wager, our gaming provider, BUSR, needs to know your name, e-mail address, mailing address, credit card number, and expiration date. This allows them to process your request and payout any winnings.</p>
<p>When you enter a contest or other promotion, we may ask for your name, address, and e-mail address so we can run the contest and notify winners.</p>
<p>We monitor player traffic patterns and site usage to help us develop and enhance the design of our gaming web sites.</p>
<p>We may use your IP address to help diagnose problems with our server, and to administer our Website. Your IP address is used to gather broad demographic information.</p>
<p><b>Will we disclose a player’s personal info to other parties?</b></p>
<p>Never. We do not sell, trade, or rent your personal information to others. However, we do use aggregate statistics about our players, sales, Internet traffic, and related site information to better deliver our services. We are not required to disclose player or financial information to government agencies nor will we do so.</p>
<p>
	<b>Public Forums</b></p>
<p>We may make chat rooms, forums, message boards, and/or news groups available to its users. Please remember that any information that is disclosed in these areas becomes public information and you should exercise caution when deciding to disclose your personal information.</p>
<p><b>Security</b></p>
<p>When you access your account information, it is protected by our highly secure encoding and processing technologies, utilizing the SSL Protocol. SSL uses 128 bit encryption which ensures server and client (us and you) authentication through encrypted algorithms, and is the highest industry standard. All of the player data we collect is protected against unauthorized access. For further protection we use Financial Services proprietary Star-MX technology for sophisticated encryption.</p>
<p><b>Corrections/Update</b></p>
<p>This site gives users the following options for changing and modifying information previously provided.</p>
<p><b>Your Consent</b></p>
<p>If we decide to change our privacy policy, we will post those changes here so that you are always aware of what information we collect and how we use it.</p>
<p><b>Conclusion</b></p>
<p>Protecting your privacy is very important to us. We use the information we collect on the site to facilitate the exchange of funds and enhance the quality of our product. We do not sell, trade, rent or otherwise disclose your personal information to others.</p>
<p><b>Choice/Opt-Out</b></p>
<p>This function only provides members/visitors the opportunity to opt-out of receiving communications from us.</p>
<p>You can contact Customer Support to indicate the e-mail address you want to remove from our list.</p>
<p>You may request the closure of your wagering account at any time as long as there is no balance and no pending wagers.</p>
<p><b>Correcting or Updating Personal Information</b></p>
<p>US Racing’s gaming provider, BUSR, provides members with the secure ‘My Account’ area for changing and modifying information previously provided.</p>
<p><b>Contacting US Racing</b></p>
<p>If you have any questions about this privacy statement, our practices or your dealings with us, contact: <a href="http://www.usracing.com/support">Customer Support</a></p>



             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


{literal} 
<script type="text/javascript">
$(document).ready(function() {
  $('.faqs').css("display","none");
});
function slideIt(thechosenone) {
     $('div[class|="faqs"]').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).slideDown(200);
          }
          else {
               $(this).slideUp(200);
          }
     });
}
</script> 
{/literal} 