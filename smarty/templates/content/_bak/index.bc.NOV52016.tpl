 {include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"}

    <section class="bc-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-bs/friends.png" alt="" class="bc-friends_img img-responsive"></div>
      </div>
    </section>
    <section class="bc-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bc-testimonial">
            <figure class="bc-testimonial_figure"><img src="/img/index-bs/derek-simon.jpg" alt="Derek Simon, Senior Editor, BUSR" class="bc-testimonial_img"></figure>
            <div class="bc-testimonial_content">
              <p class="bc-testimonial_p">At BUSR, in addition to all the traditional Breeders' Cup wagers, you can bet on individual horse match-ups, and whether there will be a Triple Crown winner in 2017!</p><span class="bc-testimonial_name">Derek Simon</span><span class="bc-testimonial_profile">Senior Editor, Handicapper and Horse Player at BUSR.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
<!------------------------------------------------------------------>      
    <section class=" bc-countdown  bc-countdown-belmont usr-section">
      <div class="container">
          <h3 class="bc-countdown_heading"><span class="bs-countdown_heading-text">&nbsp;Countdown <br>to Breeders' Cup&nbsp;</span></h3>
          <div class="bc-countdown_items">
            <div class="bc-countdown_item bc-countdown_item--green clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_days">days</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="bc-countdown_item bc-countdown_item--green clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
      </div>
    </section>
      {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("5 Nov 2016 20:35:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
  <!------------------------------------------------------------------>    
    <section class="bc-kd bc-kd--default usr-section">
      <div class="container">
        <div class="bc-kd_content">
          <img src="/img/index-bc/bc.png" alt="Online Horse Betting" class="bc-kd_logo img-responsive">
          <h1 class="bc-kd_heading">Online Horse Betting</h1>
          <h3 class="bc-kd_subheading">With the Most Breeders' Cup Bet Options Anywhere</h3>
          <p>Only at BUSR, you get great future odds with amazing payouts on the leading Breeders' Cup .</p>
          <p> Will California Chrome triumph or Arrogate reign supreme?   Will a horse lead wire to wire? Check out the props and match races!
{* Will Secretariat's 1973 Record be broken?  *}</p>
        
          <p>BUSR offers more wagering choices than other site.</p><a href="/breeders-cup/odds" class="bs-kd_btn">LIVE ODDS</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bc-belmont_"><img src="/img/index-bc/santa-anita-park-breeders-cup.jpg" alt="Belmont Park" class="bc-belmont_img"></section>
    
    
    <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-belmont-stakes-betting.png" alt="Bet on Breeders' Cup" class="bc-card_icon">
          <h2 class="bc-card_heading">Breeders' Cup Bet</h2>
          <h3 class="bc-card_subheading">$10 FREE Breeders' Cup Bet for Members</h3>
          <p>That's right, as a member of BUSR you get a $10 FREE Bet for the Breeders' Cup! Everybody's got a horse in the race - whether it's the Breeders' Cup or the United States Presidential Election.</p><a href="/signup?ref=Free-Breeders-Cup-Bet" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bc/breeders-cup-betting.jpg" alt="Breeders Cup Betting" class="bc-card_img"></div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bc-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bc-testimonial-blue">
          <div class="bc-testimonial-blue_content">
            <p class="bc-testimonial-blue_p">
When it comes to horse betting, BUSR has you covered. They provide many unique betting options, more than anybody else.
          </p>
          </div>
        </blockquote>
      </div>
    </section>
    <!------------------------------------------------------------------>  
  <!--  <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bs/triple-crown-blackjack-tournament.jpg" alt="Breeders Cup Blackjack" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-triple-crown-blackjack-tournament.png" alt="Breeders Cup Tournament" class="bc-card_icon">
          <h2 class="bc-card_heading">Triple Crown<br>Blackjack Tournament</h2>
          <p>US Racing invites you test your mettle and win cash in our Triple Crown Blackjack Tournament.</p>
          <p>Play any Blackjack game from May 2nd to June 12th and get ready to compete for real cash. Thousands of dollars in cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/login?ref=TC-Blackjack-Tournament" class="bc-card_btn">Play Now</a>
        </div>
      </div>
    </section>--><!------------------------------------------------------------------>  
    
     {*
   <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bs/bet-belmont-stakes.jpg" alt="Bet on the Breeders' Cup" class="bc-card_img"></div>
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-bet-belmont-stakes.png" alt="Bet on Breeders' Cup" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the Trainers</h2>
          <h3 class="bc-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  BUSR is the only place to bet on Trainers and Jockeys for the Breeders' Cup.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/belmont-stakes/trainer-betting" class="bc-card_btn">Live Odds</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
*}
    <section class="bc-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bc-bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bc-bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref=cash-bonus-10" class="bs-bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  

    <section class="bc-card">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-casino-rebate.png" alt="BUSR Casino Rebate" class="bc-card_icon">
          <h2 class="bc-card_heading">50% Casino Cash Back</h2>
          <h3 class="bc-card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at BUSR because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/login?ref=Thursday-Casino-Cash-Back" class="bs-card_btn">Play Now</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bs/casino-rebate.jpg" alt="Casino Rebate" class="bc-card_img">
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bc-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="bc-testimonial-blue">
          <div class="bc-testimonial-blue_center">
           <p class="bc-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="bc-testimonial-blue_name">Tom G.<br>
              <cite class="bc-testimonial-blue_profile">BUSR Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bc-card bc-card--bg">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/index-bs/horse-betting.png" alt="" class="bc-card_img"></div>
        <div class="bc-card_half bc-card_content"><img src="/img/index-bs/icon-horse-betting.png" alt="" class="bc-card_icon">
          <h2 class="bc-card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="bc-card_subheading">Members Can Bet from Anywhere</h3>
          <p>With BUSR, you can bet on horses and more from anywhere in the great U.S. of A. and it's territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, players from Canada, the United Kingdom, and most other countries* are welcome to be members, too. We look forward to welcoming you aboard.</p><a href="/signup/?ref=50-States" class="bs-card_btn">Join Now</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
  <!-- <section class="bc-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bc-payouts_heading">
               
              fast<span>2</span>day payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bc-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index-bs/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>--><!------------------------------------------------------------------>  
  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap" style="font-size:39px;">Get in on the Exciting Breeders' Cup Action at BUSR</h2>
            <a href="/signup?ref=CTA" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>
    </section>


