{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
        
 {include file='menus/belmontstakes.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>      
 <div class="container-fluid">
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a>
	<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a>
</div>  
     
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          

<div class="content">
<!-- --------------------- content starts here ---------------------- -->
 

<div class="headline"><h1>{$h1}</h1></div>

{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 

{*include file='inc/belmontstakes-slider.tpl'*}

<h2>The Final Jewel of the Triple Crown</h2>

<p>The {include file='/home/ah/allhorse/public_html/belmont/running.php'} running of the Grade 1 Belmont Stakes will be held at Belmont Park on {include file='/home/ah/allhorse/public_html/belmont/racedate.php'} with first-race post at noon. NBC will provide live coverage of the day’s events.</p>
<p> Unfortunately, with Nyquist's loss to Exaggerator at the Preakness Stakes, there will be no chance for a Triple Crown winner in 2016.  In fact, an elevated fever has forced Nyquist's connections to pull him from racing at the Belmont Stakes against Exaggerator which, of course, brings up the question of who will be able to beat Exaggerator?  The Belmont will be a fantastic race-- Triple Crown or not-- and you can bet on the Belmont on your mobile phone with US Racing.</p>
<p>The first Belmont in the United States was not the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a race horse that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby.</p>
<p><strong>The Belmont's Age</strong></p>
<p> One thing the Belmont does have over the Derby is that it is the oldest of the three Triple Crown events. The Belmont predates the Preakness by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings.</p>
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  

{include file='/home/ah/allhorse/public_html/belmont/links.tpl'}
<p><span><strong><span>Some Monumental Belmont Moments</span></strong></span></p>
<p> In 1890, the Belmont was moved from Jerome Park to Morris Park, a mile and three-eighths track located a few miles east of what is now Van Cortlandt Park in the Bronx. The Belmont was held at Morris Park until Belmont Park's opening in 1905.</p>
<ul>
<li>Here's a tidbit you didn't see in Derby or Preakness history. When Grey Lag won the Belmont in 1921, it marked the first running of the Belmont Stakes in the counter-clockwise manner of American fashion. This 53rd running was a mile and three-eighths over the main course; previous editions at Belmont Park had been run clockwise, in accordance with English custom, over a fish-hook course which included part of the training track and the main dirt oval.</li>
<li>The first post parade in this country came in the 14th running of the Belmont in 1880. Until then the horses went directly from paddock to post.</li>
<li>The Belmont has been run at various distances. From 1867 tp 1873 it was 1 5/8 miles; from 1874 to 1889 it was 1 1/2 miles; from 1890 through 1892, and in 1895, it was held at 1 1/4 miles; from 1896 through 1925 it was 1 5/8 miles; since 1925 the Belmont Stakes has been a race of 1 1/2 miles.</li>
</ul>
<p><strong><span>Champion Sires</span></strong></p>
<p> As we saw in the breeding section of the Call To The Derby Post Betting How-To Page, champions horses breed champion horses. This certainly holds form in the Belmont Stakes. A total of eleven Belmont Stakes winners have sired at least one other Belmont winner.</p>
<ul>
<li>Man o' War heads the list of Belmont champion sires. Not only did he win the race himself in 1920, but three of his subsequent sires won it as well: American Flag in 1925, Crusader in 1926 and War Admiral in 1937, who went on to win the Triple Crown.</li>
<li>Commando won the 1901 running, then sired Peter Pan, the 1907 champ and the Colin, the 1908 winner.<br /> 1930 champion Gallant Fox sired both Omaha (1935) and Granville (1936).</li>
<li>Count Fleet won the 1943 edition, and then sired back-to-back Belmont winners with Counterpoint (1951) and One Count (1952).</li>
<li>1977 Triple Crown winner Seattle Slew sired a Call To The Derby Post favorite in Swale, who won both the Derby and the Belmont in 1984, as well as A.P. Indy, who won the Belmont in 1992. 1999 Belmont winner Lemon Drop Kid is also a descendant of the Slew.</li>
<li>The following horses have sired one Belmont winner each: Duke of Magenta of 1878 sired Eric (1889); Spendthrift of 1879 sired Hastings (1896); Hastings then followed his again by siring Masterman, the 1902 winner. The Finn of 1915 sired Zev (1923); Sword Dancer of 1959 sired Damascus (1967); last but not least, Triple Crown winner Secretariat of 1973 sired Risen Star, the 1988 winner.</li>
</ul>
<p><strong>Money at the Belmont</strong></p>
<p> Oh, have times changed. The purse for the first running of the Belmont was $1,500 added with a total purse of $2,500, with the winner's share taken by the filly Ruthless. The lowest winner's share in Belmont history was the $1,825 earned by The Finn in 1915. The Belmont set an opposite record in 1992, in which the richest Belmont purse ever totaled 1,764,800. Five times in Belmont history only two horses entered the race: 1887, 1888, 1892, 1910 and sadly, 1920, the year Man O'War triumphed. The largest field, on the other hand, was 15 in 1983, when Caveat defeated Slew O' Gold. In 1875 14 horses ran, when Calvin outdueled stablemate Aristides, that year's winner of the inaugural Kentucky Derby. The Belmont's lowest paid winner: Count Fleet in 1943, who paid a paltry $2.10. The Belmont's highest winner: Sherluck in 1961, who dished out $132.10. A favorite's race: Of the 129 Belmont runnings through 1997, the favorite had won 58 times, including 9 out of the last 25. There have been some strange twists of betting in Belmont history. Since the advent of mutuels in New York in 1940 there have been six times when no place or show betting was taken on the Belmont Stakes. The last time there was no show wagering was in 1978 when Affirmed and Alydar held their famous confrontation. There was also no show betting when Secretariat won his Triple Crown in 1973; no wonder--Secretariat won by a record 31 lengths. Show betting was also eliminated in 1957 when Gallant Man defeated Bold Ruler, and also in 1953 when Native Dancer won. In 1943, believe it or not, there was no place or show wagering when Triple Crown winner Count Fleet went off $.05 to the dollar and won by 25 lengths. To wrap it up, Whirlaway completed his Triple Crown victory in 1941 without show betting. In other words, by the time horses dominate the Derby and Preakness, there just might not be that many challengers when the horse goes to complete the sweep. Since 1940 there have also been 30 horses listed as odds-on favorites in the Belmont Stakes. In 1957, there were two: Gallant Man, who won at 19-20, and Bold Ruler, who finished third at 17-20. Of these 30, only 12 went on to win. The highest on-track mutuel handle on the Belmont: 1993. A total of $2,793,320 was bet on the Belmont that year, with $1,409,970 wagered on win, place and show betting, and $1,293,954 on the daily double, exacta and triple.</p>
<p> <strong><span>The Fastest Belmont</span></strong></p>
<p> Who else?&nbsp;<a title="Secretariat" href="/famous-horses/secretariat">Secretariat</a>&nbsp;set a world-record that still stands for the mile and a half distance on a dirt track at 2:24. (He had finished a mile and a quarter at 1:59, faster than his own Derby record of 1:59 2/5.)</p>
<p> <strong><span>Belmont Trophies</span></strong></p>
<p>"The Belmont Stakes trophy is a Tiffany-made silver bowl, with cover, 18 inches high, 15 inches across and 14 inches at the base. Atop the cover is a silver figure of Fenian, winner of the third running of the Belmont Stakes in 1869. The bowl is supported by three horses representing the three foundation thoroughbreds--Eclipse, Herod and Matchem. The trophy, a solid silver bowl originally crafted by Tiffany's, was presented by the Belmont family as a perpetual award for the Belmont Stakes in 1926. It was the trophy August Belmont's Fenian won in 1869 and had remained with the Belmont family since that time. The winning owner is given the option of keeping the trophy for the year their horse reigns as Belmont champion."</p>
<p>&nbsp;</p>
        
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  

  
            {include file='inc/disclaimer-bs.tpl'} 
            
                      
        


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



      
    