{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/kentuckyderby.tpl'}
</div>
<!-- ---------------------- end left menu contents ------------------- -->   
<div class="container-fluid">
	
{*
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/2017-Kentucky-Derby-odds.jpg" alt="Kentucky Derby Odds"></a>
		
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/2017-Kentucky-Derby.jpg" alt="Kentucky Derby Odds">  </p> </a>
	*}
	
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-derby-betting-hero.jpg" alt="Kentucky Derby Betting"></a>
		
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-derby-betting-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>	
	
	
</div>  


<!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
    
<!------------------------------------------------------------------>       

<!------------------------------------------------------------------>    
  
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
         
           <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Betting" class="kd_logo img-responsive"></a>
          
          <h1 class="kd_heading">Kentucky Derby Odds</h1>
          <h3 class="kd_subheading">With the Most Derby Bet Options Anywhere</h3>
       
<p>{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/kd/odds_kd_2018.xml'} </p>
<p>{* include file='/home/ah/allhorse/public_html/kd/odds_kd.php' *}</p>

{* <p>Think you know what trainer will win the Kentucky Derby?  You can bet on trainers like <a href="/kentucky-derby/trainer-betting">Todd Pletcher</a> and Trainer Odds will be coming soon!</p><br> *}

<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on the Kentucky Derby</a></p>  

 <p>The {include file='/home/ah/allhorse/public_html/kd/running.php'}  Kentucky Derby will be run on {include file='/home/ah/allhorse/public_html/kd/date.php'}.  Helpful trivia:  The Derby always takes place on the first Saturday in May!  This year will undoubtedly feature a large field or horses,  up to 20 three-year-old colts, fillies and geldings can run.</p>

<p>US Racing provides the earliest Kentucky Derby Futures odds of any website.  If you see a horse you would like to be added, let us know and we might be able to get it added for you!</p>

<p>Between now and the Kentucky Derby, the odds will be updated by adding new horses and removing others.  Remember, when you place future wagers the odds are all action and fixed. {* Good luck and see you in May for the Run for the Roses! *}</p>


        </div>
      </div>
    </section>
    
<!------------------------------------------------------------------>    

{include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/kd/block-exciting.tpl"}
<!------------------------------------------------------------------>      
<div id="main" class="container">
<p>{include file='inc/disclaimer-kd.tpl'} </p>
</div><!-- /#container --> 
