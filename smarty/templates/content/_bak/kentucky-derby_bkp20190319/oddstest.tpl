{assign var="ref" value="kentucky-derby-odds"}
{*New header*}
<h1 style="margin:0;"><a href="/kentucky-derby/odds"><img class="img-responsive" src="/img/kentuckyderby/2016-kentucky-derby-odds.jpg" alt="Kentucky Derby Odds">  </a></h1>
{*End New  header*}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          
{* <div class="headline"><h1>Kentucky Derby Odds</h1></div> *}
<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'} 
{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'} 
{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}

<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Odds</h2>
<p>What are the Kentucky Derby Odds for 2016?</p>

<p>{include file='/home/ah/allhorse/public_html/kd/odds_to_win_998_xml.php'}</p><p>Place your bets now and your odds are locked in. These odds are only available for a limited time!</p>
<p align="center"><a href="/login?ref=kentucky-derby-odds" class="btn-xlrg ">Bet Now on the Kentucky Derby</a></p>  <br>
<h2> Kentucky Derby Props</h2>
<p>Great unique bets for extra fun at the Derby.</p>
<p>{include file='/home/ah/allhorse/public_html/kd/odds_926_xml.php'}</p>
<p align="center"><a href="/login?ref=kentucky-derby-props" class="btn-xlrg ">Bet Now on Kentucky Derby Props</a></p>  


 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row --><p>{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 