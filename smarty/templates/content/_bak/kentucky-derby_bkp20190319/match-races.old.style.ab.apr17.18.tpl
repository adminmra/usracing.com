
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<div class="container-fluid">
	
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-derby-match-hero.jpg" alt="Kentucky Derby Betting"></a>
		
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-derby-match-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
	
</div>
   
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
         
                                                 
                                      
          
<div class="headline"><h1>Kentucky Derby Match Races!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

Make head-to-head bets on horses at the Derby! 


<h2>{include file='/home/ah/allhorse/public_html/kd/year.php'}   Kentucky Derby Props</h2>
<p>What are match races and why would you want to bet them at the Kentucky Derby?</p><p>Match races are simple wagers where you bet whether one particular horse will beat another horse.&nbsp; Sometimes this is called head-to-head betting. At the Kentucky Derby, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.</p>

<p>Once the field for the Kentucky Derby is finalized and post positions drawn, you will be able to see the odds of "match races."  Let's review: what are match races in horse racing?  Answer: Match races are bets that punters can place on individual horses in a particular race.  Instead of betting on whether a horse wins, places or shows, you are instead betting whether one horse will beat another horse!  Match races are extremely popular in the United Kingdom.</p>

<p>{include file='/home/ah/allhorse/public_html/kd/odds_1042_xml.php'}</p>

<p>Good luck with your bets and when we get closer to the start of the <a href="/kentucky-derby/betting">Kentucky Derby</a>, you will find match race odds in our sportsbook&nbsp;section.</p>
<p>&nbsp;</p>
              
              <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on these Derby Props</a></p>  
            
                      
        
            
            
                      
 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
