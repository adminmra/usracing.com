{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->



  {include file='menus/kentuckyderby.tpl'}



<!-- ---------------------- end left menu contents ------------------- -->         

</div>


		<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">

	  <div class="text text-xl">{$h1}</div>

	  <div class="text text-xl" style="margin-top:0px;"></div>

	  <div class="text text-md">{$h2}<br>

	    <a href="/signup?ref={$ref}">

	    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i> {$signup_cta}</div>

	    </a> </div>

	</div>




<div id="main" class="container">

<div class="row">





<div id="left-col" class="col-md-9">

         

                                        

          

<div class="headline">
  <h1>Kentucky Derby Racing Schedule</h1></div>

<div class="content">

<!-- --------------------- content starts here ---------------------- -->





{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'} 



<!--<div class="tag-box tag-box-v4">


</div>-->

<p>The Kentucky Derby day is full of action and excitement. From tradition of sipping on a few Mint Julep's to singing of the National Anthem, to the final drapping of the Garland of Roses on the winning horse, there is something for everyone. Although the main attraction is the Kentucky Derby race itself, the track hosts a total of 14 races during the day.</p>

<h2>{include file='/home/ah/allhorse/public_html/kd/year.php'}  Racing Schedule</h2>

<p>Below is a list of all the races for this year's Kentucky Derby. Looking to place a wager on the big race? Check out the  <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
<p>{include file='/home/ah/usracing.com/smarty/templates/includes/apj_block_kd_racing_schedule.tpl'} </p>

<!-- ------------------------ content ends -------------------------- -->

</div> <!-- end/ content -->

</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-kd.tpl'} 



{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 



</div><!-- end: #right-col --> 



 

</div><!-- end/row -->

<div class="row" >
<div class="col-md-12">
{include file='inc/disclaimer-kd.tpl'}
</div>
</div> 

</div><!-- /#container --> 