
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<div class="container-fluid">
	
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-margin-hero.jpg" alt="Kentucky Derby Betting"></a>
		
		<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-margin-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
	
</div>

</div>      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
         
                                        
          
<div class="headline"><h1>Kentucky Derby Margin of Victory Bets</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'} 
{include file="/home/ah/allhorse/public_html/kd/salesblurb.tpl"}
{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Kentucky Derby odds" href="/kentucky-derby/odds">Kentucky Derby odds</a> for {include file='/home/ah/allhorse/public_html/kd/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}

<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Derby Margin of Victory</h2>


{*<p>{include file='/home/ah/usracing.com/smarty/templates/includes/apj_kd_marginvictory_2015.tpl'}</p>*}
<p>{include file='/home/ah/allhorse/public_html/kd/gen_margin_victory.php'}</p>
{*Leo please move to /allhorse.../kd/gen_margin_victory.php*}
  <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now</a></p>


 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	 <!-- ------------------------ CTA -------------------------- -->

<div class="calltoaction">
	
	<h2>Bet the<BR>Kentucky Derby<br>Margin of Victory!</h2>
<h3>Only available at<br>BUSR</h3>
	
	{include file="/home/ah/usracing.com/smarty/templates/inc/rightcol-calltoaction-form.tpl"}

	 <!-- ------------------------ CTA ends -------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- /#container --> 
