   {include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {*include file='menus/racetracks.tpl'*}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>US Racetracks</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p></p>
            <table class="infoEntries"><tr valign="top"><td width="246">
            <dl><dt> {*<a href="/state/arizona" title="">*}ARIZONA</a></dt>
<li>{*<a href="/racetrack/turf-paradise" title="">*}TURF PARADISE</a></li></dl>


<dl><dt>{*<a href="/state/arkansas" title="">*}ARKANSAS</a></dt>
<li>{*<a href="/racetrack/oaklawn-park" title="">*}OAKLAWN PARK</a></li></dl>

<dl><dt>{*<a href="/state/california" title="">*}CALIFORNIA</a></dt>
<li>{*<a href="/racetrack/hollywood-park" title="">*}HOLLYWOOD PARK</a></li>
<li>{*<a href="/racetrack/cal-expo" title="">*}CAL EXPO</a></li>
<li>{*<a href="/delmar" title="">*}DELMAR</a></li>
<li>{*<a href="/racetrack/fairplex" title="">*}FAIRPLEX</a></li>
<li>{*<a href="/racetrack/ferndale" title="">*}FERNDALE</a></li>
<li>{*<a href="/racetrack/fresno" title="">*}FRESNO</a></li>
<li>{*<a href="/racetrack/golden-gate-fields" title="">*}GOLDEN GATE FIELDS</a></li>
<li>{*<a href="/racetrack/los-alimitos" title="">*}LOS ALIMITOS</a></li>
<li>{*<a href="/racetrack/pleasanton" title="">*}PLEASANTON</a></li>
<li>{*<a href="/racetrack/sacremento" title="">*}SACREMENTO</a></li>
<li>{*<a href="/racetrack/santa-anita-park" title="">*}SANTA ANITA PARK</a></li>
<li>{*<a href="/racetrack/santa-rosa" title="">*}SANTA ROSA</a></li>
<li>{*<a href="/racetrack/stocton" title="">*}STOCTON</a></li></dl>

<dl><dt>{*<a href="/state/colorado" title="">*}COLORADO</a></dt>
<li>{*<a href="/racetrack/arapahoe-park" title="">*}ARAPAHOE PARK</a></li></dl>

<dl><dt>{*<a href="/state/delaware" title="">*}DELAWARE</a></dt>
<li>{*<a href="/racetrack/delaware-park" title="">*}DELAWARE PARK</a></li>
<li>{*<a href="/racetrack/dover-downs" title="">*}DOVER DOWNS</a></li>
<li>{*<a href="/racetrack/harrington-park" title="">*}HARRINGTON PARK</a></li></dl>

<dl><dt>{*<a href="/state/florida" title="">*}FLORIDA</a></dt>
<li>{*<a href="/racetrack/calder" title="">*}CALDER</a></li>
<li>{*<a href="/racetrack/gulfstream-park" title="">*}GULFSTREAM PARK</a></li>
<li>{*<a href="/racetrack/hialeah-park" title="">*}HIALEAH PARK</a></li>
<li>{*<a href="/racetrack/pampano-park" title="">*}PAMPANO PARK</a></li>
<li>{*<a href="/racetrack/tampa-bay-downs" title="">*}TAMPA BAY DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/illinois" title="">*}ILLINOIS</a></dt>
<li>{*<a href="/racetrack/arlington-park" title="">*}ARLINGTON PARK</a></li>
<li>{*<a href="/racetrack/balmoral-park" title="">*}BALMORAL PARK</a></li>
<li>{*<a href="/racetrack/fairmount-park" title="">*}FAIRMOUNT PARK</a></li>
<li>{*<a href="/racetrack/hawethorne-race-course" title="">*}HAWTHORNE RACE COURSE</a></li>
<li>{*<a href="/racetrack/maywood-park" title="">*}MAYWOOD PARK</a></li></dl>

<dl><dt>{*<a href="/state/indianna" title="">*}INDIANA</a></dt>
<li>{*<a href="/racetrack/hoosier-park" title="">*}HOOSIER PARK</a></li>
<li>{*<a href="/racetrack/indianna-downs" title="">*}INDIANA DOWNS</a></li>
</dl>

<dl><dt>{*<a href="/state/iowa" title="">*}IOWA</a></dt>
<li>{*<a href="/racetrack/prarie-meadows" title="">*}PRAIRIE MEADOWS</a></li>
</dl>

</td><td width="278">



<dl><dt>{*<a href="/kentucky" title="">*}KENTUCKY</a></dt>
<li>{*<a href="/racetrack/churchill-downs" title="">*}CHURCHILL DOWNS</a></li>
<li>{*<a href="/racetrack/ellis-park" title="">*}ELLIS PARK</a></li>
<li>{*<a href="/racetrack/keenland" title="">*}KEENELAND</a></li>
<li>{*<a href="/racetrack/kentucky-downs" title="">*}KENTUCKY DOWNS</a></li>
<li>{*<a href="/racetrack/oaks-derby-double" title="">*}OAKS DERBY DOUBLE</a></li>
<li>{*<a href="/racetrack/oaks-woodward-derby p3" title="">*}OAKS WOODWARD DERBY P3</a></li>
<li>{*<a href="/racetrack/turfway-park" title="">*}TURFWAY PARK</a></li></dl>

<dl><dt>{*<a href="/state/louisiana" title="">*}LOUISIANA</a></dt>
<li>{*<a href="/racetrack/delta-downs" title="">*}DELTA DOWNS</a></li>
<li>{*<a href="/racetrack/evangeline-dxowns" title="">*}EVANGELINE DOWNS</a></li>
<li>{*<a href="/racetrack/fair-grounds" title="">*}FAIR GROUNDS</a></li>
<li>{*<a href="/racetrack/louisiana-downs" title="">*}LOUISIANA DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/maine" title="">*}MAINE</a></dt>
<li>{*<a href="/racetrack/hollywood-slots-raceway" title="">*}HOLLYWOOD SLOTS RACEWAY</a></li></dl>

<dl><dt>{*<a href="/state/maryland" title="">*}MARYLAND</a></dt>
<li>{*<a href="/racetrack/laurel-park" title="">*}LAUREL PARK</a></li>
<li>{*<a href="/racetrack/pimlico" title="">*}PIMLICO</a></li>
<li>{*<a href="/racetrack/rosecroft-raceway" title="">*}ROSECROFT RACEWAY</a></li></dl>

<dl><dt>{*<a href="/state/massachusettes" title="">*}MASSACHUSETTES</a></dt>
<li>{*<a href="/racetrack/suffolk-downs" title="">*}SUFFOLK DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/minnesota" title="">*}MINNESOTA</a></dt>
<li>{*<a href="/racetrack/canterburry-park" title="">*}CANTERBURY PARK</a></li>
<li>{*<a href="/racetrack/runnng-aces" title="">*}RUNNING ACES</a></li></dl>

<dl><dt>{*<a href="/state/nebraska" title="">*}NEBRASKA</a></dt>
<li>{*<a href="/racetrack/fonner-oark" title="">*}FONNER PARK</a></li></dl>

<dl><dt>{*<a href="/sttate/new-jersey" title="">*}NEW JERSEY</a></dt>
<li>{*<a href="/racetrack/atlantic-city-race-course" title="">*}ATLANTIC CITY RACE COURSE</a></li>
<li>{*<a href="/racetrack/freehold-raceway" title="">*}FREEHOLD RACEWAY</a></li>
<li>{*<a href="/racetrack/meadowlands" title="">*}MEADOWLANDS</a></li>
<li>{*<a href="/racetrack/monmouth-park" title="">*}MONMOUTH PARK</a></li></dl>

<dl><dt>{*<a href="/state/new-mexico" title="">*}NEW MEXICO</a></dt>
<li>{*<a href="/racetrack/ruidoso-downs" title="">*}RUIDOSO DOWNS</a></li>
<li>{*<a href="/racetrack/ruidoso-quarter-horse" title="">*}RUIDOSO QUARTEHORSE</a></li>
<li>{*<a href="/racetrack/sundland=-park" title="">*}SUNLAND PARK</a></li>
<li>{*<a href="/racetrack/zia-park" title="">*}ZIA PARK</a></li></dl>


</td><td width="235">


<dl><dt>{*<a href="/state/new-york" title="">*}NEW YORK</a></dt>
<li>{*<a href="/racetrack/aqueduct" title="">*}AQUEDUCT</a></li>
<li>{*<a href="/racetrack/belmont-park" title="">*}BELMONT PARK</a></li>
<li>{*<a href="/racetrack/buffalow-raceway" title="">*}BUFFALO RACEWAY</a></li>
<li>{*<a href="/racetrack/finger-lakes" title="">*}FINGER LAKES </a></li>
<li>{*<a href="/saratoga" title="">*}SARATOGA</a></li>
<li>{*<a href="/racetrack/tioga-downs" title="">*}TIOGA DOWNS</a></li>
<li>{*<a href="/racetrack/vernon-downs" title="">*}VERNON DOWNS</a></li>
<li>{*<a href="/racetrack/yonkers-racewway" title="">*}YONKERS RACEWAY</a></li></dl>

<dl><dt>{*<a href="/state/ohio" title="">*}OHIO</a></dt>
<li>{*<a href="/racetrack/northfield-park" title="">*}NORTHFIELD PARK</a></li>
<li>{*<a href="/racetrack/raceway-park" title="">*}RACEWAY PARK</a></li>
<li>{*<a href="/racetrack/scioto-downs" title="">*}SCIOTO DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/oklahoma" title="">*}OKLAHOMA</a></dt>
<li>{*<a href="/racetrack/fair-meadows" title="">*}FAIR MEADOWS</a></li>
<li>{*<a href="/racetrack/remington-park" title="">*}REMINGTON PARK</a></li>
<li>{*<a href="/racetrack/will-rogers-downs" title="">*}WILL ROGERS DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/oregon" title="">*}OREGON</a></dt>
<li>{*<a href="/racetrack/portland-meadows" title="">*}PORTLAND MEADOWS</a></li></dl>

<dl><dt>{*<a href="/state/pennsylvania" title="">*}PENNSYLVANIA</a></dt>
<li>{*<a href="/racetrack/harrahs-philadelphia" title="">*}HARRAHS PHILADELPHIA</a></li>
<li>{*<a href="/racetrack/parx-racing" title="">*}PARX RACING</a></li>
<li>{*<a href="/racetrack/penn-national" title="">*}PENN NATIONAL</a></li>
<li>{*<a href="/racetrack/pocono-downs" title="">*}POCONO DOWNS</a></li>
<li>{*<a href="/racetrack/presque-isle-downs" title="">*}PRESQUE ISLE DOWNS</a></li>
<li>{*<a href="/racetrack/the-meadows" title="">*}THE MEADOWS</a></li></dl>

<dl><dt>{*<a href="/state/texas" title="">*}TEXAS</a></dt>
<li>{*<a href="/racetrack/lone-star-park" title="">*}LONE STAR PARK</a></li>
<li>{*<a href="/racetrack/retama-park" title="">*}RETAMA PARK</a></li>
<li>{*<a href="/racetrack/sam-houston-race-park" title="">*}SAM HOUSTON RACE PARK</a></li></dl>

<dl><dt>{*<a href="/state/washington" title="">*}WASHINGTON</a></dt>
<li>{*<a href="/racetrack/emeral-downs" title="">*}EMERALD DOWNS</a></li></dl>

<dl><dt>{*<a href="/state/west-virgina" title="">*}WEST VIRGINIA</a></dt>
<li>{*<a href="/racetrack/charles-town" title="">*}CHARLES TOWN</a></li>
<li>{*<a href="/racetrack/mountaineer" title="">*}MOUNTAINEER</a></li></dl>

</td></tr></table>

 <table class="infoEntries"><tr valign="top"><td>
            <dl><dt>

</td></tr></table>

 <table class="infoEntries"><tr valign="top"><td>
            <dl><dt>

</td></tr></table>

                  {*OLD CODE include_php file='../smarty/libs/racetracks/racetracks.php'*}





        
        

  </div>
              
            
              
            
            
             
                                        
          
<div class="headline"><h1>Canadian Racetracks</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p></p>
            <table class="infoEntries"><tr valign="top"><td>
        
            
<li>{* href="/racetrack/ajax-downs" title="">*}AJAX DOWNS</a></li>
<li>{* href="/racetrack/assiniboia-downs" title="">*}ASSINIBOIA DOWNS</a></li>
<li>{* href="/racetrack/fort-erie" title="">*}FORT ERIE</a></li>
<li>{* href="/racetrack/fraser-downs" title="">*}FRASER DOWNS</a></li>
<li>{* href="/racetrack/hastings" title="">*}HASTINGS</a></li>
<li>{* href="/racetrack/mohawk-raceway" title="">*}MOHAWK RACEWAY</a></li>
<li>{* href="/racetrack/northlands-park" title="">*}NORTHLANDS PARK</a></li>
<li>{* href="/racetrack/western-fair-raceway" title="">*}WESTERN FAIR RACEWAY</a></li>
<li>{* href="/racetrack/woodbine" title="">*}WOODBINE</a></li>
<li>{* href="/racetrack/woodbine-harness" title="">*}WOODBINE HARNESS</a></li>

           

</td></tr></table>

        
        

  </div>
              
            
            
                      
                         
                                        
          
<div class="headline"><h1>International Racetracks</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p></p>
            <table class="infoEntries"><tr valign="top"><td>
         
            
<li>{* href="/racetrack/ajax-downs" title="">*}AUSTRALIA A</a></li>
<li>{* href="/racetrack/assiniboia-downs" title="">*}AUSTRALIA B</a></li>
<li>{* href="/racetrack/fort-erie" title="">*}AUSTRALIA C</a></li>
<li>{* href="/racetrack/fraiser-downs" title="">*}DUBAI</a></li>
<li>{* href="/racetrack/hastings" title="">*}GREAT BRITAIN 1</a></li>
<li>{* href="/racetrack/mohawk-raceway" title="">*}GREAT BRITAIN 2</a></li>
<li>{* href="/racetrack/hastings" title="">*}GREAT BRITAIN 3</a></li>
<li>{* href="/racetrack/mohawk-raceway" title="">*}GREAT BRITAIN 4</a></li>
<li>{* href="/racetrack/northlands-park" title="">*}GREAT BRITAIN 5</a></li>
<li>{* href="/racetrack/western-fair-raceway" title="">*}IRELAND 1</a></li>
<li>{* href="/racetrack/woodbine" title="">*}IRELAND 2</a></li>
<li>{* href="/racetrack/woodbine-harness" title="">*}IRELAND 3</a></li>
<li>{* href="/racetrack/woodbine-harness" title="">*}JAPAN</a></li>

</td></tr></table>


        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


