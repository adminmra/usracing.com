<section class="bc-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/indexbecca/friends.png" alt="friends" class="bc-friends_img img-responsive"></div>
      </div>
    </section>
    <section class="bc-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bc-testimonial">
            <figure class="bc-testimonial_figure"><img src="/img/indexbecca/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="bc-testimonial_img"></figure>
            <div class="bc-testimonial_content">
              <p class="bc-testimonial_p">At US Racing, in addition to all the traditional Derby wagers, you can bet on individual horse match-ups, jockeys, trainers and whether there will be a Triple Crown winner in 2016!</p><span class="bc-testimonial_name">Derek Simon</span><span class="bc-testimonial_profile">Senior Editor, Handicapper and Horse Player at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
    <section class="bc-countdown usr-section">
      <div class="container">
        <h3 class="bc-countdown_heading"><span class="bc-countdown_heading-text">Countdown <br>to Kentucky Derby 142</span></h3>
        <div class="bc-countdown_items">
          <div class="bc-countdown_item clock_days">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_days"></canvas>
            </div>
            <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_days">days</span></div>
          </div>
          <div class="bc-countdown_item clock_hours">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_hours"></canvas>
            </div>
            <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_hours">hours</span></div>
          </div>
          <div class="bc-countdown_item clock_minutes">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_minutes"></canvas>
            </div>
            <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_minutes">minutes</span></div>
          </div>
          <div class="bc-countdown_item clock_seconds">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_seconds"></canvas>
            </div>
            <div class="text"><span class="bc-countdown_num val">0</span><span class="bc-countdown_label time type_seconds">seconds</span></div>
          </div>
        </div>
      </div>
    </section>
    <section class="bc-kd bc-kd--horse-2 usr-section">
      <div class="container">
        <div class="bc-kd_content">
          <h1><img src="/img/indexbecca/kd.png" alt="Online Horse Betting" class="bc-kd_logo img-responsive"></h1>
          <h2 class="bc-kd_heading">Online Horse Betting</h2>
          <h3 class="bc-kd_subheading">With the Most Kentucky Derby Bet Options Anywhere</h3>
          <p>Only at US Racing, you get great future odds with amazing payouts on the leading Kentucky Oaks and Derby horses and even the leading jockeys and trainers.</p>
          <p>Will the winner of the 2016 Kentucky Derby win the Preakness Stakes?  Will a horse lead wire to wire? Will Secretariat's 1973 Record be broken? </p>
          <p>US Racing offers more wagering choices than other site.</p><a href="/kentucky-derby/odds" class="bc-kd_btn-red">LIVE ODDS</a>
        </div>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-bet-kentucky-derby.png" alt="icon-bet-kentucky-derby" class="bc-card_icon">
          <h2 class="bc-card_heading">Kentucky derby betting</h2>
          <h3 class="bc-card_subheading">More than 256 diferent Kentucky Derby Bets</h3>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Live Odds</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/kentucky-derby-betting.jpg" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
      </div>
    </section>
    <section class="bc-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bc-testimonial-blue">
          <figure class="bc-testimonial-blue_figure"><img src="/img/indexbecca/laura-pugh.jpg" alt="Laura Pugh, Feature US Racing Writer" class="bc-testimonial-blue_img"></figure>
          <div class="bc-testimonial-blue_content">
            <p class="bc-testimonial-blue_p">When it comes to the Kentucky Derby, US Racing has you covered. They provide may unique betting options, even allowing you to wager on the trainers and jockeys! How cool is that?</p><span class="bc-testimonial-blue_name"> Laura Pugh, <br>
              <cite class="bc-testimonial-blue_profile">Feature Writer for US Racing, Lady and the Track & Horse Racing Nation.</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/bet-kentucky-derby.jpg" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-bet-kentucky-derby.png" alt="icon-bet-kentucky-derby" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the Trainers</h2>
          <h3 class="bc-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Kentucky Derby.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Live Odds</a>
        </div>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon_3.png" alt="icon_3" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on JOCKEYS</h2>
          <h3 class="bc-card_subheading">142ND Kectucky Derby</h3>
          <p>With over 85 free high-converting mobile responsive templates within Unbounce and the nearly 200 available for purchase on ThemeForest, your landing pages will look as good as they perform. Customize them to suit your needs using the most flexible and powerful landing page builder available.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Live Odds</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/3.jpg" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
      </div>
    </section>
    <section class="bc-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bc-bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bc-bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with US Racing, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref=cash-bonus-10" class="bc-bonus_btn-red">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section>
    <section class="bc-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="bc-testimonial-blue">
          <div class="bc-testimonial-blue_center">
            <p class="bc-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="bc-testimonial-blue_name">Tom G.<br>
              <cite class="bc-testimonial-blue_profile">US Racing Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/triple-crown-blackjack-tournament.jpg" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-triple-crown-blackjack-tournament.png" alt="icon-triple-crown-blackjack-tournament" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the Trainers</h2>
          <h3 class="bc-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Kentucky Derby.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Live Odds</a>
        </div>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-casino-rebate.png" alt="icon-casino-rebate" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on JOCKEYS</h2>
          <h3 class="bc-card_subheading">142ND Kectucky Derby</h3>
          <p>With over 85 free high-converting mobile responsive templates within Unbounce and the nearly 200 available for purchase on ThemeForest, your landing pages will look as good as they perform. Customize them to suit your needs using the most flexible and powerful landing page builder available.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">CLAIM YOUR BONUS</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/casino-bonus.jpg" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
      </div>
    </section>
    <section class="bc-card bc-card--bg">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/horse-betting.png" alt="horse-betting" class="bc-card_img"></div>
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-horse-betting.png" alt="icon-horse-betting" class="bc-card_icon">
          <h2 class="bc-card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="bc-card_subheading">Members Can Bet from Anywhere</h3>
          <p>With US Racing, you can bet on horses and more from anywhere in the great U.S. of A. and it's territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, Canadians players, the United Kingdom and most other countries* are welcome to be members, too. We look forward to welcoming you aboard.</p><a href="/signup/?ref=50-States" class="bc-card_btn-red">Join Now</a>
        </div>
      </div>
    </section>
    <section class="bc-racetracks">
      <div class="container">
        <h2 class="bc-racetracks_title">
           
          Choose from <strong>211 </strong>different racetracks
        </h2>
        <ul class="bc-racetracks_list">
          <li class="bc-racetracks_item"><a href="#" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/indexbecca/icon-racetracks-1.png" alt="icon-racetracks-1" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Thoroughbred</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="#" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/indexbecca/icon-racetracks-2.png" alt="icon-racetracks-2" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Harness</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="#" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/indexbecca/icon-racetracks-3.png" alt="icon-racetracks-3" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Quarter Horses</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="#" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/indexbecca/icon-racetracks-4.png" alt="icon-racetracks-4" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">International</h4>
              </div></a></li>
          <li class="bc-racetracks_item"><a href="#" class="bc-racetracks_link">
              <div class="bc-racetrack"><img src="/img/indexbecca/icon-racetracks-5.png" alt="icon-racetracks-5" class="bc-racetrack_icon">
                <h4 class="bc-racetrack_title">Greyhound</h4>
              </div></a></li>
        </ul>
      </div>
    </section>
    <section class="bc-card bc-card-mobile">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/mobile.png" alt="Kentucky Derby Blackjack" class="bc-card_img">
        </div>
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-mobile.png" alt="kentucky Derby Tournament" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on the go!</h2>
          <p>Take all the races with you, Bet & Watch the seasons on your phone or tablet.</p><a href="/login?ref=TC-Blackjack-Tournament" class="bc-card_btn-red">View Odds</a>
        </div>
      </div>
    </section>
    <section class="bc-card undefined">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-betonsports.png" alt="icon-betonsports" class="bc-card_icon">
          <h2 class="bc-card_heading">Bet on Sports</h2>
          <h3 class="bc-card_subheading">Super Bowl is comming!</h3>
          <p>With over 85 free high-converting mobile responsive templates within Unbounce and the nearly 200 available for purchase on ThemeForest, your landing pages will look as good as they perform. Customize them to suit your needs using the most flexible and powerful landing page builder available.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Bet on sports</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/betonsports.jpg" alt="Casino Rebate" class="bc-card_img"></div>
      </div>
    </section>
    <section class="bc-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="bc-testimonial-blue">
          <div class="bc-testimonial-blue_center">
            <p class="bc-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="bc-testimonial-blue_name">Tom G.<br>
              <cite class="bc-testimonial-blue_profile">US Racing Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
    <section class="bc-card bc-card--hre">
      <div class="bc-card_wrap">
        <div class="bc-card_half bc-card_content"><img src="/img/indexbecca/icon-horse-racing-experts.png" alt="icon-horse-racing-experts" class="bc-card_icon">
          <h2 class="bc-card_heading">Horse Racing Experts</h2>
          <p>With over 85 free high-converting mobile responsive templates within Unbounce and the nearly 200 available for purchase on ThemeForest, your landing pages will look as good as they perform. Customize them to suit your needs using the most flexible and powerful landing page builder available.</p><a href="/signup?ref=Free-Derby-Bet" class="bc-card_btn-red">Read the news</a>
        </div>
        <div class="bc-card_half bc-card_hide-mobile"><img src="" data-src-img="/img/indexbecca/horse-racing-experts.png" alt="Bet on the Kentucky Derby" class="bc-card_img"></div>
      </div>
    </section>
    <section class="bc-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bc-payouts_heading">
               
              fast<span>2</span>day payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bc-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/indexbecca/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
    <section class="bc-register">
      <div class="container">
        <div class="bc-card_wrap">
          <div class="bc-card_half bc-card_content bc-card_content--red"><img src="/img/indexbecca/icon-horse-racing.png" alt="icon-horse-racing" class="bc-register_icon">
            <h2 class="bc-register_heading">Become the <br>next BIG thing!</h2>
            <h3 class="bc-register_heading-2">Receive free all the US Racing Benefits</h3><a href="/signup?ref=CTA" class="bc-register_btn">Register Now</a>
          </div>
        </div>
      </div>
    </section>