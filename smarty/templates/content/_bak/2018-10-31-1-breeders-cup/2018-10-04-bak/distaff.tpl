{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->
</div>

<!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
 
 	<div class="text text-md">{$h2}<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->

<!--{include file='/home/ah/allhorse/public_html/breeders/distaff/video.php'}-->
<div id="main" class="container">
<div class="row">

<div class="headline"><h1>{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup  Distaff</h1></div>
<div id="left-col" class="col-md-9">




<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<p>{include file='/home/ah/allhorse/public_html/breeders/distaff/purse.php'}</p> </p>
<P>{include file='/home/ah/allhorse/public_html/breeders/distaff/description.php'}</p> 

<!-- -->

<h2>Breeders' Cup Distaff Odds</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/odds_combined_inc.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/breeders/distaff/odds_distaff.php'} </p>
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  


<hr><!-- --------------------- HR ---------------------- -->


<P>{include file='/home/ah/allhorse/public_html/breeders/breederscup_wherewhenwatch.tpl'}</p> 

<!-- -->

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Distaff Results</h2>
<P>{include file='/home/ah/allhorse/public_html/breeders/distaff/results.php'}</p> 
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->

<h2>{include file='/home/ah/allhorse/public_html/breeders/year_results.php'} Breeders' Cup  Distaff Payouts</h2>
<P>{include file='/home/ah/allhorse/public_html/breeders/distaff/payouts.php'}
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->

<h2> Breeders' Cup  Distaff Winners</h2>
<P>{include file='/home/ah/allhorse/public_html/breeders/distaff/winners.php'}</p>  
<p align="center"><a href="/signup?ref=breeders-cup-odds" class="btn-xlrg ">Bet Now</a></p>  

<!-- -->

<P>{include file='inc/disclaimer-bc.tpl'}</p> 

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
	{include file='/home/ah/allhorse/public_html/breeders/distaff/call_to_action.tpl'}</p> 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}

</div><!-- end: #right-col -->
</div><!-- end/row -->
</div><!-- /#container -->
