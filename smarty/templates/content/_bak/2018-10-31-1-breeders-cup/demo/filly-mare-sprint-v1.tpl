{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
            {include file='menus/breederscup.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<!--------- NEW HERO  ---------------------------------------->
{literal}
<style type="text/css" >

	@media screen and (min-width: 800px) {
		.newHeroM { background-image: url("/img/breeders/filly-mare-sprint.jpg"); }
	}
	
	@media screen and (max-width: 800px) {
		.newHeroM { background-image: url("/img/breeders/filly-mare-sprint-sm.jpg"); }
	}
</style>
{/literal}
<div class="newHeroM"  alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl topMargin" >{$h1b}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>

 
<!--------- NEW HERO END ---------------------------------------->   
     <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      


   <section class="kd bcs usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bc/bc.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
     <p>{include file='/home/ah/allhorse/public_html/breeders/salesblurb.tpl'} </p>

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
 
<p>{include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/purse-new.php'}</p>
<p>{include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/description.php'}</p>




      </div>
      </div>
<!--    </section>
  <section> -->
 <div class="container">
 <div class="kd_content">
 
   <h2>Breeders' Cup Filly &amp; Mare Sprint Winners</h2>
<p>{include file='/home/ah/allhorse/public_html/breeders/filly-mare-sprint/winners-new.php'}</p>
 <p> {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"} </p>



 </div>
	</div>
  

    
 </section> 
            
      <!------------------------------------------------------------------>  
  <!--  <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> -->
    
  <!------------------------------------------------------------------>      
   {**}
   {*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-3.tpl"*}
  {*include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"*}

{* {include file="/home/ah/allhorse/public_html/belmont/block-testimonial-other.tpl"} *}
 {include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 

 {include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
 {include file="/home/ah/allhorse/public_html/usracing/block-50-states.tpl"} 
  {*include file="/home/ah/allhorse/public_html/breeders/block-countdown.tpl"*}
{include file="/home/ah/allhorse/public_html/breeders/block-exciting.tpl"}

  


<div id="main" class="container">
  {include file='inc/disclaimer-bc.tpl'} 
</div><!-- /#container --> 
