{include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"} 

    <section class="bs-friends block-center usr-section">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-bs/friends.png" alt="" class="bs-friends_img img-responsive"></div>
      </div>
    </section>
    <section class="bs-testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="bs-testimonial">
            <figure class="bs-testimonial_figure"><img src="/img/index-bs/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="bs-testimonial_img"></figure>
            <div class="bs-testimonial_content">
              <p class="bs-testimonial_p">At BUSR, in addition to all the traditional Belmont Stakes wagers, you can bet on individual horse match-ups, and whether there will be a Triple Crown winner in {'Y'|date}!</p><span class="bs-testimonial_name">Derek Simon</span><span class="bs-testimonial_profile">Senior Editor and Handicapper at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
<!------------------------------------------------------------------>      
    <section class="bs-countdown bs-countdown--belmont usr-section">
      <div class="container">
        <div class="bs-countdown_content">
          <h3 class="bs-countdown_heading"><span class="bs-countdown_heading-text">Countdown <br>to Belmont Stakes</span></h3>
          <div class="bs-countdown_items">
            <div class="bs-countdown_item bs-countdown_item--green clock_days">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_days"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_days">days</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_hours">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_hours"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_hours">hours</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_minutes">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_minutes"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_minutes">minutes</span></div>
            </div>
            <div class="bs-countdown_item bs-countdown_item--green clock_seconds">
              <div class="bgLayer">
                <canvas width="188" height="188" class="bgClock"></canvas>
              </div>
              <div class="topLayer">
                <canvas width="188" height="188" id="canvas_seconds"></canvas>
              </div>
              <div class="text"><span class="bs-countdown_num val">0</span><span class="bs-countdown_label time type_seconds">seconds</span></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("10 Jun 2017 18:30:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
  <!------------------------------------------------------------------>    
    <section class="bs-kd bs-kd--default usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bs/bs.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="bs-kd_heading">Belmont Stakes Betting</h1>
          <h3 class="bs-kd_subheading">With the Most Belmont Stakes Bet Options Anywhere</h3>
          <p>Only at BUSR, you get great future odds with amazing payouts on the leading Belmont Stakes .</p>
          <p>Will the winner of the {'Y'|date}  Kentucky Derby win the Belmont Stakes?  Will a horse lead wire to wire? {* Will Secretariat's 1973 Record be broken?  *}</p>
          <p>BUSR offers more wagering choices than other site.</p><a href="/belmont-stakes/odds" class="bs-kd_btn">LIVE ODDS</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section>
    
    
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-belmont-stakes-betting.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Belmont Stakes Bet</h2>
          <h3 class="bs-card_subheading">$10 FREE Belmont Stakes Bet for Members</h3>
          <p>As a member, you'll get a free bet on the Belmont Stakes.  If you win your bet, great! If you lose the wager, your $10 will be returned to your account.</p>
          <p>You can only win with BUSR!</p><a href="/signup?ref=Free-Belmont-Stakes-Bet" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index-bs/belmont-stakes-betting.jpg" alt="Belmont Stakes Betting" class="bs-card_img"></div>
      </div>
    </section><!------------------------------------------------------------------>  
  {*
  <section class="bs-testimonials--blue usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <figure class="bs-testimonial-blue_figure"><img src="/img/index-kd/horseracingguy.jpg"alt="Racing Dudes" class="bs-testimonial-blue_img"></figure>
          <div class="bs-testimonial-blue_content">
            <p class="bs-testimonial-blue_p">When it comes to the Belmont Stakes, BUSR has you covered. They provide may unique betting options; more than andy body else.</p><span class="bs-testimonial-blue_name"> - The Racing Dudes</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
*}
    <!------------------------------------------------------------------>  
{*
    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index-bs/triple-crown-blackjack-tournament.jpg" alt="Belmont Stakes Blackjack" class="bs-card_img">
        </div>
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-triple-crown-blackjack-tournament.png" alt="Belmont Stakes Tournament" class="bs-card_icon">
          <h2 class="bs-card_heading">Triple Crown<br>Blackjack Tournament</h2>
          <p>US Racing invites you test your mettle and win cash in our Triple Crown Blackjack Tournament.</p>
          <p>Play any Blackjack game from May 2nd to June 12th and get ready to compete for real cash. Thousands of dollars in cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/login?ref=TC-Blackjack-Tournament" class="bs-card_btn">Play Now</a>
        </div>
      </div>
    </section>
*}<!------------------------------------------------------------------>  
    
     {*
   <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index-bs/bet-belmont-stakes.jpg" alt="Bet on the Belmont Stakes" class="bs-card_img"></div>
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-bet-belmont-stakes.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Bet on the Trainers</h2>
          <h3 class="bs-card_subheading">And Other Exclusive Derby Props</h3>
          <p>Bob Baffert or Todd Pletcher?  Can Steve Asmussen add a Derby Victory in 2016?  US Racing is the only place to bet on Trainers and Jockeys for the Belmont Stakes.</p>
          <p>Future Wagers for the Breeders' Cup Classic, Triple Crown races and many other events are live, bet now.</p><a href="/belmont-stakes/trainer-betting" class="bs-card_btn">Live Odds</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
*}
    <section class="bs-bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
            <h2 class="bs-bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bs-bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with BUSR, you'll get an additional 10% cash bonus added to your account, absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you can qualify to earn an additional $150!  It doesn't get any better. Join today.</p><a href="/signup?ref={$ref}-cash-bonus-10" class="bs-bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  

    <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-casino-rebate.png" alt="US Racing Casino Rebate" class="bs-card_icon">
          <h2 class="bs-card_heading">50% Casino Cash Back</h2>
          <h3 class="bs-card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is extra special at US Racing because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/login?ref={$ref}-Thursday-Casino-Cash-Back" class="bs-card_btn">Play Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index-bs/casino-rebate.jpg" alt="Casino Rebate" class="bs-card_img">
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="bs-testimonial-blue">
          <div class="bs-testimonial-blue_center">
            <p class="bs-testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!!</p><span class="bs-testimonial-blue_name">Tom G.<br>
              <cite class="bs-testimonial-blue_profile">US Racing Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section><!------------------------------------------------------------------>  
    <section class="bs-card bs-card--bg">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_hide-mobile"><img src="" data-src-img="/img/index-bs/horse-betting.png" alt="" class="bs-card_img"></div>
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-horse-betting.png" alt="" class="bs-card_icon">
          <h2 class="bs-card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="bs-card_subheading">Members Can Bet from Anywhere</h3>
          <p>With BUSR, you can bet on horses and more from anywhere in the great U.S. of A. and it's territories. We have proud members from the Big Apple and Lone Star state.  Come join them!</p>
          <p>Oh yes, players from Canada, the United Kingdom, and most other countries* are welcome to be members, too. We look forward to welcoming you aboard.</p><a href="/signup/?ref={$ref}-50-States" class="bs-card_btn">Join Now</a>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
{*
    <section class="bs-payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-7 col-lg-6">
            <h2 class="bs-payouts_heading">
               
              fast<span>2</span>day payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
            <figure class="bs-payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index-bs/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
*}<!------------------------------------------------------------------>  
  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Belmont Stakes Action at<br>BUSR</h2>
            <a href="/signup?ref={$ref}-CTA-bottom" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>
    </section>


