{assign var="ref" value="kentucky-oaks-betting"}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->           
{include file='menus/kentuckyderby.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<!--<div class="container-fluid">
	<a href="/signup?ref=kentucky-derby">
		<img class="img-responsive" src="/img/kentuckyderby/2017-Kentucky-Derby-betting.jpg" alt="Kentucky Derby Betting">
	</a>
</div> -->     

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">                                     
          
<div class="headline"><h1>Kentucky Oaks Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch_ko.tpl'} 
{include file='/home/ah/allhorse/public_html/kd/salesblurb.tpl'}
<h2>{include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Oaks</h2>
{*
<p>The Kentucky Oaks is a Grade I Thoroughbred horse race for three-year-old fillies, staged annually in Louisville, Kentucky.</p>

<p>The race currently covers 1 1/8 miles (1811 meters) at Churchill Downs; carry 121 pounds (55 kg).</p>
*}


<p>The Kentucky Oaks is America's richest race for three-year-old fillies (female horses) and takes place each year on the Friday before the Kentucky Derby. Like the Derby, it was founded by Colonel Meriwether Lewis Clark - grandson of William Clark (of Lewis and Clark fame) - and is modeled after the Epsom Oaks. Since it achieved Grade I status in 1978, the Kentucky Oaks winner has gone on to be named champion three-year-old filly 12 times.

<p>This year, Breeders' Cup Juvenile Fillies victress Songbird is a prohibitive favorite to be draped in a garland of lilies on the first Friday in May. She's undefeated (as of March 24) and has been geared down by regular rider Mike Smith in each of her last two starts. In the Oaks, Songbird is expected to be challenged by another undefeated filly, Cathryn Sophia, as well as last year's BC Juvenile Fillies runner-up Rachel's Valentina (daughter of Horse of the Year Rachel Alexandra, who won the Oaks in 2009).  Sadly, Songbird was pulled from the Oaks due to a high fever.  This, of course, was the best decision made by her connections.  We hope she gets healthy and look to her racing at the Preakness Stakes or possibly the Belmont Stakes.</p>
<h2> {include file='/home/ah/allhorse/public_html/kd/year.php'}  Kentucky Oaks Odds</h2><p>{include file='/home/ah/allhorse/public_html/kd/odds_kentucky_oaks_1006_xml.php'}</p>

 <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Kentucky Oaks</a></p>


<div>   
{*include file="includes/ahr_block_koaks_odds_2011.tpl*"}
{*include file="includes/ahr_block-koaks-contenders.tpl"*}
</div>
					


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction-ko.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->{include file='inc/disclaimer-kd.tpl'} 
</div><!-- end/container -->
