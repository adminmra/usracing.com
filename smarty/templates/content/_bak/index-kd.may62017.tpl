
{include file="/home/ah/allhorse/public_html/weekly/home_slider.tpl"}

    <section class="friends block-center usr-section" style="margin-top:20px;">
      <div class="block-center_content">
        <div class="container"><img src="/img/index-kd/friends.png" alt="" class="img-responsive friends_img"></div>
      </div>
    </section>
    <section class="testimonials block-center usr-section">
      <div class="block-center_content">
        <div class="container">
          <blockquote class="testimonial">
            <figure class="testimonial_figure"><img src="/img/index-kd/derek-simon.jpg" alt="Derek Simon, Senior Editor, US Racing" class="testimonial_img"></figure>
            <div class="testimonial_content">
              <p class="testimonial_p">At BUSR, in addition to all the traditional Derby bets, you can wager on individual horse match-ups, bet on jockeys and trainers and play a variety of fun and exciting props too!</p><span class="testimonial_name">Derek Simon</span><span class="testimonial_profile">Senior Editor and Handicapper at US Racing.</span>
            </div>
          </blockquote>
        </div>
      </div>
    </section>
 <!------------------------------------------------------------------>     
    
    <section class="countdown usr-section">
      <div class="container">
        <h3 class="countdown_heading"><span class="countdown_heading-text">Countdown <br>to the {include file='/home/ah/allhorse/public_html/kd/running.php'} Kentucky Derby  </span></h3>
        <div class="countdown_items">
          <div class="countdown_item clock_days">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_days"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_days">days</span></div>
          </div>
          <div class="countdown_item clock_hours">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_hours"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_hours">hours</span></div>
          </div>
          <div class="countdown_item clock_minutes">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_minutes"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_minutes">minutes</span></div>
          </div>
          <div class="countdown_item clock_seconds">
            <div class="bgLayer">
              <canvas width="188" height="188" class="bgClock"></canvas>
            </div>
            <div class="topLayer">
              <canvas width="188" height="188" id="canvas_seconds"></canvas>
            </div>
            <div class="text"><span class="countdown_num val">0</span><span class="countdown_label time type_seconds">seconds</span></div>
          </div>
        </div>
      </div>
    </section>
    {php}
      $startDate = strtotime("now");
      $endDate  = strtotime("6 May 2017 18:34:00");
    {/php}

    {literal}
    <script type="text/javascript">
      $(document).ready(function(){
        JBCountDown({
          secondsColor : "#ffdc50",
          secondsGlow  : "none",
          minutesColor : "#9cdb7d",
          minutesGlow  : "none", 
          hoursColor   : "#378cff", 
          hoursGlow    : "none",  
          daysColor    : "#ff6565", 
          daysGlow     : "none", 
          startDate   : "{/literal}{php} echo $startDate; {/php}{literal}", 
          endDate : "{/literal}{php} echo $endDate; {/php}{literal}", 
          now : "{/literal}{php} echo strtotime('now'); {/php}{literal}" 
        });
      });
    </script>
    {/literal}
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
         
            <a href="/signup?ref=index-KD-CTA"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Betting" class="kd_logo img-responsive"></a>
          
          <h1 class="kd_heading">Kentucky Derby Betting</h1>
          <h3 class="kd_subheading">With the Most Derby Bet Options Anywhere</h3>
          <p>Only at BUSR, you get great future odds with amazing payouts on the leading Kentucky Oaks and Derby horses and even the leading jockeys and trainers.</p>
          <p>Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more. {* Kentucky Derby Odds are live, bet now. *}</p><a href="/signup?ref=index-KD-CTA" class="kd_btn">Join Now</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content">
	        <a href="/kentucky-derby/free-bet?ref=index-KD-Free-Derby-Bet">
	        <img src="/img/index-kd/icon-kentucky-derby-betting.png" alt="Bet on Kentucky Derby" class="card_icon">
	        </a>
          <h2 class="card_heading">Get a Free Bet</h2>
          <h3 class="card_subheading">All Members get a free $10 FREE  Bet </h3>
          <p>As a member, you'll can qualify for a guaranteed no-lose bet.  If you win your bet, great! If you lose the wager, your $10 will be returned to your account.</p>
          <p>You can only win with BUSR!</p><a href="/kentucky-derby/free-bet?ref=index-KD-Free-Derby-Bet" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile"><a href="/kentucky-derby/free-bet?ref=index-KD-Free-Derby-Bet"><img src="" data-src-img="/img/index-kd/kentucky-derby-betting.jpg" alt="Kentucky Derby Betting" class="card_img"></a></div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    
    <section class="testimonials--blue usr-section">
      <div class="container">
        <blockquote class="testimonial-blue">
          <figure class="testimonial-blue_figure"><img src="/img/index-kd/horseracingguy.jpg" alt="Horse Racing Dudes" class="testimonial-blue_img"></figure>
          <div class="testimonial-blue_content">
            <p class="testimonial-blue_p">Bet on your Favorite Trainer to win the Kentucky Derby at BUSR!  Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher? <br>Why Not!”  </p><span class="testimonial-blue_name">
               
               - The Racing Dudes<br>
              </span>
          </div>
        </blockquote>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile">
	        <a href="/kentucky-derby/trainer-betting?ref=index-KD-Trainer-Odds">
		        <img src="" data-src-img="/img/index-kd/bet-kentucky-derby.jpg" alt="Kentucky Derby Odds:  Trainers"  class="card_img"></a></div>
        <div class="card_half card_content">
	        <a href="/kentucky-derby/trainer-betting?ref=index-KD-Trainer-Odds">
	        <img src="/img/index-kd/icon-bet-kentucky-derby.png"alt="Kentucky Derby Odds:  Trainers"  class="card_icon">
	        </a>
          <h2 class="card_heading">Bet on the Trainers</h2>
          <h3 class="card_subheading">And Other Exclusive Derby Props</h3>
          <p>Sure, anybody can bet the Kentucky Derby Future Wagers but what about making a bet on a horse trainer like Bob Baffert or Todd Pletcher?</p>
          <p>Bet on the margin of victory, the winning time vs Secretariat, a Triple Crown contention and more. Kentucky Derby Odds are live, bet now.</p><a href="/kentucky-derby/trainer-betting?ref=index-KD-Trainer-Odds" class="card_btn">See the Trainer Odds</a>
        </div>
      </div>
    </section>
    
      <!------------------------------------------------------------------>  
        <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content"><a href="/kentucky-derby/jockey-betting?ref=index-KD-Jockey-Odds">
	        <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Kentucky Derby Odds:  Jockeys" class="card_icon"></a>
          <h2 class="card_heading">Bet on JOCKEYS</h2>
          <h3 class="card_subheading">{include file='/home/ah/allhorse/public_html/kd/running.php'}  Kentucky Derby</h3>
          <p>Sure, anyone can offer Kentucky Derby Odds but what about making a bet on a jockey? 
	          <p> BUSR offers more wagers on the Kentucky Derby than any other horse betting site</p><a href="/kentucky-derby/jockey-betting?ref=index-KD-Jockey-Odds" class="card_btn">See the Jockey Odds</a>
        </div>
        <div class="card_half"><a href="/kentucky-derby/jockey-betting?ref=index-KD-Jockey-Odds">
	        <img src="" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg" alt="Kentucky Derby Odds:  Jockeys" class="card_img"></a></div>
      </div>
    </section>
    
    <!------------------------------------------------------------------>  


    <section class="bonus">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
            <h2 class="bonus_heading">10% Sign Up Bonus</h2>
            <h3 class="bonus_subheading">Exceptional New Member Bonuses</h3>
            <p>For your first deposit with BUSR, you'll get an additional 10% bonus to your deposit absolutely free. No Limits!</p>
            <p>Deposit a minimum of $100 and you could qualify to earn an additional $150!</p><a href="/signup?ref=index-KD-Cash-Bonus-10" class="bonus_btn">CLAIM YOUR BONUS</a>
          </div>
        </div>
      </div>
    </section><!------------------------------------------------------------------>  
   
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile"><a href="/promos/blackjack-tournament?ref=KD-TC-Blackjack-Tournament"><img src="" data-src-img="/img/index-kd/triple-crown-blackjack-tournament.jpg" alt="Kentucky Derby Blackjack" class="card_img"></a>
        </div>
        <div class="card_half card_content"><a href="/promos/blackjack-tournament?ref=KD-TC-Blackjack-Tournament"><img src="/img/index-kd/icon-triple-crown-blackjack-tournament.png" alt="kentucky Derby Tournament" class="card_icon"></a>
          <h2 class="card_heading">
             
            Triple Crown<br>Blackjack Tournament
          </h2>
          <p>BUSR invites you test your mettle and win cash in our Triple Crown Blackjack Tournament.</p>
          <p>Play any Blackjack variation from May 6th and May 19th and get ready to compete for real cash. Cash prizes are paid to the top 20 players with the highest payout percentage!</p><a href="/promos/blackjack-tournament?ref=KD-TC-Blackjack-Tournament" class="card_btn">Learn More</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
    <section class="card">
      <div class="card_wrap">
        <div class="card_half card_content">
	        <a href="/promos/casino-rebate?ref=index-KD-Thursday-Casino-Cash-Back">
	        <img src="/img/index-kd/icon-casino-rebate.png" alt="BUSR Casino Rebate" class="card_icon">
	        </a>
          <h2 class="card_heading">50% Casino Cash Back</h2>
          <h3 class="card_subheading">Every Thursday!</h3>
          <p>Every Thursday at the Casino is special at BUSR because you get 50% Cash returned to your account of any losses!</p>
          <p>That's right, you will get 50% Cash Back to your account every Thursday.</p><a href="/promos/casino-rebate?ref=index-KD-Thursday-Casino-Cash-Back" class="card_btn">Learn More</a>
        </div>
        <div class="card_half card_hide-mobile"><a href="/promos/casino-rebate?ref=index-KD-Thursday-Casino-Cash-Back"><img src="" data-src-img="/img/index-kd/casino-rebate.jpg" alt="Casino Rebate" class="card_img"></a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  
     <section class="testimonials--blue-dark usr-section">
      <div class="container">
        <blockquote class="testimonial-blue">
         {*  <figure class="testimonial-blue_figure"><img src="/img/index-kd/testimonial-user-3.jpg" alt="Testimonial" class="testimonial-blue_img"></figure> *}
          <div class="testimonial-blue_center">
            <p class="testimonial-blue_p">Finally finding a site that allowed me to bet with my mobile phone on horses and blackjack was just what I was looking for.  THANK YOU SO MUCH!! </p><span class="testimonial-blue_name">
               
             Tom G. <br>
              <cite class="testimonial-blue_profile">BUSR Member Since 2014</cite></span>
          </div>
        </blockquote>
      </div>
    </section>
  
<!------------------------------------------------------------------>  
    <section class="card card--bg">
      <div class="card_wrap">
        <div class="card_half card_hide-mobile"><a href="/signup/?ref=index-KD-CTA-50-States"><img src="" data-src-img="/img/index-kd/horse-betting.png" alt="" class="card_img"></a>
        </div>
        <div class="card_half card_content"><a href="/signup/?ref=index-KD-CTA-50-States"><img src="/img/index-kd/icon-horse-betting.png" alt="" class="card_icon"></a>
          <h2 class="card_heading">
             
            Members Accepted from<br>All 50 States
          </h2>
          <h3 class="card_subheading">Members Can Bet from Anywhere</h3>
          <p>With BUSR, you can bet on horses and more from anywhere in the great U.S. of A. and its territories. We have proud members from the Big Apple and Lone Star state. Come join them!</p>
          <p>Oh yes, players from Canada, the United Kingdom, and most other countries are welcome to be members, too! We look forward to welcoming you aboard.</p><a href="/signup/?ref=index-KD-CTA-50-States" class="card_btn">Join Now</a>
        </div>
      </div>
    </section>
    <!------------------------------------------------------------------>  

{*
    <section class="payouts usr-section">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6">
            <h2 class="payouts_heading">
               
              Super Fast Payouts
            </h2>
            <p>Why wait weeks or even months to get paid?</p>
            <p>You can get paid out in 2 days with BUSR.</p>
          </div>
          <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">
            <figure class="payouts_figure">
              <div class="block-center">
                <div class="block-center_content"><img src="/img/index-kd/visa.png" alt="Horse Racing Payouts" class="img-responsive"></div>
              </div>
            </figure>
          </div>
        </div>
      </div>
    </section>
*}
<!------------------------------------------------------------------>  
    <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
            <h2 class="card_heading card_heading--no-cap">Get in on the Exciting Kentucky Derby Action <br>at BUSR</h2>
            <a href="/signup?ref=index-KD-CTA-bottom" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>





















