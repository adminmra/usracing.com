{include file="/home/ah/allhorse/public_html/usracing/schema/belmont-stakes.tpl"}
{* {literal}
  <style>
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 80%;
        } 
      }
    </style>
{/literal} *}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/belmontstakes.tpl'} 
</div>
<!-- ---------------------- end left menu contents ------------------- -->   
{*
		<!-- <div class="container-fluid"> <a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/kentucky-derby-betting-hero.jpg" alt="Kentucky Derby Betting"></a>

				<a href="/signup?ref={$ref}" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/kentucky-derby-betting-small-hero.jpg" alt="Kentucky Derby Betting">  </p> </a>
        </div>  
        -->
*}
<!--------- NEW HERO  ---------------------------------------->
  {*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }
       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
         .kd {
            background: transparent url(/img/belmontstakes/live-belmont-odds.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/belmontstakes/belmont-current-live-odds.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>
{*End*}
<!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/belmont/block-countdown-new.tpl"}
    <section class="kd usr-section">
      <div class="container">
        <div class="kd_content">
            <img src="/img/index-bs/bs.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">Belmont Stakes</h1>
          <h2 class="kd_subheading">The Most Belmont Bets Anywhere</h2>
<p>{include file='/home/ah/allhorse/public_html/belmont/salesblurb.tpl'} </p>
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now On The Belmont Stakes</a></p>

<h2>The First Jewel of the Triple Crown</h2>
<p>The {include_php file='/home/ah/allhorse/public_html/belmont/running.php'} running of the Belmont Stakes will be held at Belmont Park on {include_php file='/home/ah/allhorse/public_html/belmont/racedate.php'} with first-race post just before noon ET. NBC will be providing live coverage of the day's events.</p>
<p><h3>The Belmont's Age</h3></p>
<p> One thing the Belmont does have over the Derby is that it is the oldest of the three Triple Crown events. The Belmont predates the Preakness by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings.</p>
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>  
{*include file='/home/ah/allhorse/public_html/belmont/links.tpl'*}
  <h3>Some Monumental Belmont Moments</h3>
<p>In 1890, the Belmont was moved from Jerome Park to Morris Park, a mile and three-eighths track located a few miles east of what is now Van Cortlandt Park in the Bronx. The Belmont was held at Morris Park until Belmont Park's opening in 1905.</p>
<p>Here's a tidbit you didn't see in Derby or Preakness history. When Grey Lag won the Belmont in 1921, it marked the first running of the Belmont Stakes in the counter-clockwise manner of American fashion. This 53rd running was a mile and three-eighths over the main course; previous editions at Belmont Park had been run clockwise, in accordance with English custom, over a fish-hook course which included part of the training track and the main dirt oval.</p>
<p>The first post parade in this country came in the 14th running of the Belmont in 1880. Until then the horses went directly from paddock to post.</p>
<p>The Belmont has been run at various distances. From 1867 tp 1873 it was 1 5/8 miles; from 1874 to 1889 it was 1 1/2 miles; from 1890 through 1892, and in 1895, it was held at 1 1/4 miles; from 1896 through 1925 it was 1 5/8 miles; since 1925 the Belmont Stakes has been a race of 1 1/2 miles.</p>
  <h3>Champion Sires</h3>
<p> As we saw in the breeding section of the Call To The Derby Post Betting How-To Page, champions horses breed champion horses. This certainly holds form in the Belmont Stakes. A total of eleven Belmont Stakes winners have sired at least one other Belmont winner.</p>
<p>Man o' War heads the list of Belmont champion sires. Not only did he win the race himself in 1920, but three of his subsequent sires won it as well: American Flag in 1925, Crusader in 1926 and War Admiral in 1937, who went on to win the Triple Crown.</p>
<p>Commando won the 1901 running, then sired Peter Pan, the 1907 champ and the Colin, the 1908 winner.</p>
<p>1930 champion Gallant Fox sired both Omaha (1935) and Granville (1936).</p>
<p>Count Fleet won the 1943 edition, and then sired back-to-back Belmont winners with Counterpoint (1951) and One Count (1952).</p>
<p>1977 Triple Crown winner Seattle Slew sired a Call To The Derby Post favorite in Swale, who won both the Derby and the Belmont in 1984, as well as A.P. Indy, who won the Belmont in 1992. 1999 Belmont winner Lemon Drop Kid is also a descendant of the Slew.</p>
<p>The following horses have sired one Belmont winner each: Duke of Magenta of 1878 sired Eric (1889); Spendthrift of 1879 sired Hastings (1896); Hastings then followed his again by siring Masterman, the 1902 winner. The Finn of 1915 sired Zev (1923); Sword Dancer of 1959 sired Damascus (1967); last but not least, Triple Crown winner Secretariat of 1973 sired Risen Star, the 1988 winner.</p>
 <h3>Money at the Belmont</h3>
<p>Oh, have times changed. The purse for the first running of the Belmont was $1,500 added with a total purse of $2,500, with the winner's share taken by the filly Ruthless.</p>
<p>The lowest winner's share in Belmont history was the $1,825 earned by The Finn in 1915. The Belmont set an opposite record in 1992, in which the richest Belmont purse ever totaled 1,764,800. Five times in Belmont history only two horses entered the race: 1887, 1888, 1892, 1910 and sadly, 1920, the year Man O'War triumphed.</p>
 <p>The largest field, on the other hand, was 15 in 1983, when Caveat defeated Slew O' Gold. In 1875 14 horses ran, when Calvin outdueled stablemate Aristides, that year's winner of the inaugural Kentucky Derby. The Belmont's lowest paid winner: Count Fleet in 1943, who paid a paltry $2.10. The Belmont's highest winner: Sherluck in 1961, who dished out $132.10.</p>
 <p>A favorite's race: Of the 129 Belmont runnings through 1997, the favorite had won 58 times, including 9 out of the last 25. There have been some strange twists of betting in Belmont history. Since the advent of mutuels in New York in 1940 there have been six times when no place or show betting was taken on the Belmont Stakes. The last time there was no show wagering was in 1978 when Affirmed and Alydar held their famous confrontation.</p>
 <p>There was also no show betting when Secretariat won his Triple Crown in 1973; no wonder--Secretariat won by a record 31 lengths. Show betting was also eliminated in 1957 when Gallant Man defeated Bold Ruler, and also in 1953 when Native Dancer won.</p>
 <p>In 1943, believe it or not, there was no place or show wagering when Triple Crown winner Count Fleet went off $.05 to the dollar and won by 25 lengths. To wrap it up, Whirlaway completed his Triple Crown victory in 1941 without show betting.</p>
 <p>In other words, by the time horses dominate the Derby and Preakness, there just might not be that many challengers when the horse goes to complete the sweep. Since 1940 there have also been 30 horses listed as odds-on favorites in the Belmont Stakes. In 1957, there were two: Gallant Man, who won at 19-20, and Bold Ruler, who finished third at 17-20.</p>
 <p>Of these 30, only 12 went on to win. The highest on-track mutuel handle on the Belmont: 1993. A total of $2,793,320 was bet on the Belmont that year, with $1,409,970 wagered on win, place and show betting, and $1,293,954 on the daily double, exacta and triple.</p>
  <h3>The Fastest Belmont</h3>
<p> Who else? <a title="Secretariat" href="/famous-horses/secretariat">Secretariat</a> set a world-record that still stands for the mile and a half distance on a dirt track at 2:24. (He had finished a mile and a quarter at 1:59, faster than his own Derby record of 1:59 2/5.)</p>
  <h3>Belmont Trophies</h3>
<p>"The Belmont Stakes trophy is a Tiffany-made silver bowl, with cover, 18 inches high, 15 inches across and 14 inches at the base. Atop the cover is a silver figure of Fenian, winner of the third running of the Belmont Stakes in 1869.</p> 
<p>The bowl is supported by three horses representing the three foundation thoroughbreds--Eclipse, Herod and Matchem. The trophy, a solid silver bowl originally crafted by Tiffany's, was presented by the Belmont family as a perpetual award for the Belmont Stakes in 1926.</p>
<p>It was the trophy August Belmont's Fenian won in 1869 and had remained with the Belmont family since that time. The winning owner is given the option of keeping the trophy for the year their horse reigns as Belmont champion."</p>
<p>&nbsp;</p>
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now On The Belmont Stakes</a></p>
 <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
<!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/belmont/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for free bet*}
{include file="/home/ah/allhorse/public_html/belmont/block-signup-bonus.tpl"}
{* End*}
{* Block for triple crown*}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-props.tpl"}
{* End*}
{* Block for the block blue*}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-blue-signup-bonus-belmont-stakes.tpl"}
{* End*}
{* Block for the countdown*}
{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"}
{* Block for the block exciting  and path *}
{* End*}
{include file="/home/ah/allhorse/public_html/belmont/block-exciting.tpl"}  