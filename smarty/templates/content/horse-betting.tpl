{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
 
  <!-- ---------------------- left menu contents ---------------------- --> 
  
  {include file='menus/horsebetting.tpl'} 
  <!-- ---------------------- end left menu contents ------------------- --> 
</div>
<div id="main" class="container">
  <div class="row">
    <div id="left-col" class="col-md-9"> 
                
          
<div class="headline"> <h1>Horse Betting with US Racing</h1> </div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Horse Betting</h2>

        <p>US Racing is the number one website for horse betting and greyhound dog racing.  Whether you refer to the "Sport of Kings" as horse betting, offtrack betting or OTB-- you have found the best website for secure, entertaining horse betting online.  Wagering on horses for beginners or experienced handicappers is made easy with the free tools and expert opinion provided at US Racing.</p>
        <p align="left">Want to <a  href="/bet-on/kentucky-derby">bet on the Kentucky Derby</a>? How about the <a  href="/bet-on/preakness-stakes">Preakness Stakes</a> and <a  href="/bet-on/belmont-stakes">Belmont Stakes</a>? How about a head to head matchup against your favorite horse at the <a   href="/santa-anita-derby">Santa Anita Derby</a> or the <a  href="/del-mar">Del Mar</a>?</p>
        
        <p>US Racing aims to provide customers the absolute best in customer service. Our constant news updates, up to date stats and race information give you the edge so you can bet with knowledge and Setting the Pace.</p>
   
      


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->
  
<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
   {*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}
</div><!-- end: #right-col -->


  
</div><!-- end/row -->
</div><!-- end/container --> 


