<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>DELETE THIS TEXT AND ADD A TITLE FOR THE PAGE BETWEEN THESE H1 tags </h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->



THIS IS WHERE YOU PUT CONTENT. YOU CAN DELETE EVERYTHING IN HERE AND REPLACE IT WITH YOUR OWN. 
USE TAGS <p></p> AROUND PARAGRAPHS. 
USE <br /> LINE BREAKS. <BR /> WILL NOT ADD MARGINS LIKE <p></p> WILL.
USE <h2></h2> AROUND SUB TITLES.

*** DONT FORGET TO RENAME THE .var.xml FILE AND ITS FOLDER NAME! *****
             


<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->


IF YOU DON"T WANT THE NEWS IN THE RIGHT COLUMN, REMOVE THE INCLUDE, OR ADD A * INSIDE THE BRACKETS. SO LIKE THIS: {* include/blahblah *}
THEN ADD WHAT EVER YOU LIKE.
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
