{literal}
  <style>
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 0%;
        } 
      }

      @media (max-width: 991px) {
        #right-col {
          display: flex;
          justify-content: center;
        }
      }
    </style>
{/literal}

{literal}
<script src="/assets/js/iframeResizer.min.js"></script>

<script>
    $(document).ready(function () {
        $("#iframeResize").iFrameResize({
            log: false,                   // Enable console logging
            enablePublicMethods: false,  // Enable methods within iframe hosted page
            checkOrigin: false,
            bodyMargin: 0

        });
    })
</script>
{/literal}

{* {include file='inc/left-nav-btn.tpl'} *}

{literal}
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Horse Racing  Odds | Online Horse Betting",
  "description": "US Racing provides  horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit.",
  "image": "https://www.usracing.com/img/best-horse-racing-betting-site.jpg",
  "startDate": "{/literal}{php}echo date('Y-m-d');{/php}{literal}", 
  "endDate": "{/literal}{php}echo date('Y-m-d', strtotime('+7 days'));{/php}{literal}",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
    "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
      "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],
  "location": {
    "@type": "Place",
    "name": "US Racing | Online Horse Betting",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "123 street ,place park",
      "addressLocality": "miami",
      "addressRegion": "Florida",
      "postalCode": "33184",
      "addressCountry": "US"
    }
  }
}

</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Today's Horse Racing Odds",
    "item": "https://www.usracing.com/odds"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "Kentucky Derby Odds",
    "item": "https://www.usracing.com/kentucky-derby/odds"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "Triple Crown Odds",
    "item": "https://www.usracing.com/bet-on/triple-crown"
  },
  {
    "@type": "ListItem",
    "position": 4,
    "name": "Pegasus World Cup Odds",
    "item": "https://www.usracing.com/pegasus-world-cup/odds"
  },
  {
    "@type": "ListItem",
    "position": 5,
    "name": "Pegasus World Cup Odds",
    "item": "https://www.usracing.com/pegasus-world-cup/odds"
  },
  {
    "@type": "ListItem",
    "position": 6,
    "name": "Grand National Odds",
    "item": "https://www.usracing.com/grand-national"
  },
  {
    "@type": "ListItem",
    "position": 7,
    "name": "Dubai World Cup Odds",
    "item": "https://www.usracing.com/dubai-world-cup"
  },
  {
    "@type": "ListItem",
    "position": 8,
    "name": "US Presidential Election Odds",
    "item": "https://www.usracing.com/odds/us-presidential-election"
  }
  ]
}
</script>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

          @media (max-width: 800px) {
         .none {
           display: none !important;
         }
       }
     </style>
{/literal}
{*End*}

{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="Horse Racing Odds" "{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>
{*End*}



  {*This is the section for the odds tables*}

  <section class="odds kd usr-section" style="padding-bottom: 0px;">

    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Horse Racing Odds</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">{* The Most Horse Racing Betting Options Anywhere *}New Members Get up to {$BONUS_WELCOME} cash!</h3>
		  
        {*The path for the main copy of the page*}
        <p>These are the latest Morning Line odds for almost all racetracks worldwide.</p>

<p>You want the best place to bet on horses, at <a href="/signup?ref={$ref}">BUSR</a> you can bet from anywhere. Get the VIP treatment from day one with a special <a href="/promos/cash-bonus">Welcome Bonus up to {$BONUS_WELCOME}</a>. Not only that, you'll get up to an <a href="/rebates">8% rebate</a> on all your horse bets placed!</p>
<p>Place your bets today! </p>
		  <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
		  
		  <h2>Latest Horse Racing Odds</h2>
		  <p><i>*Live odds take a moment to load*</i></p>
        {*end*}

		<p>
		{* <iframe src="https://rb.busr.ag/BOSSWagering/Racebook/InternetBetTaker/?siteid=usracing" frameborder="0" id="iframeResize" height="1250" width="100%" class=" racebook " scrolling="no" style="overflow: hidden; height: 2230px;"></iframe> *}
   <iframe src="https://rb.busr.ag/BOSSWagering/Racebook/InternetBetTaker/?siteid=busr" frameborder="0" id="iframeResize" height="1000" width="100%" class=" racebook " scrolling="no" style="overflow: hidden; height: 1000px;"></iframe>
	  </p> 
	  	{*	<p>
		<iframe src="https://rb.busr.ag/BOSSWagering/Racebook/InternetBetTaker/?siteid=usracing" frameborder="0" id="racebook" height="1250" width="100%" class="racebook" scrolling="no" style="overflow: hidden; height: 2230px;"></iframe>
	  </p> *}

     <h2>How to Calculate Betting Odds and Payoffs</h2>
        <p>Betting on the outcome of horse racing can be fun and profitable and, if you know what you're doing, you might even be able to beat the odds.</p>
        <p>The horse racing betting odds for each horse are displayed on a tote board at the track or on the betting ticket online. The basic information on the betting ticket window is the &quot;odds to win&quot; for each horse. Although it won't tell you how much the horse will pay, it does show the amount of profit you will get if you win and the amount you have to bet to get it.  For example, 3-5 odds means that for every $5.00 you wager, you'll win $3.00. </p>
        <p>Payouts are determined by how much is in the total pool (all the money bet on all the horses to win)  minus the &quot;take&quot; which is usually between 14%-20% depending on the track. The take varies from state to state and the money is used to pay taxes, purse money for the horsemen, expenses at the track, and the track's profit.</p>
        <p>Most tracks require a $2.00 minimum bet, here are the pay-offs for $2 win bets:</p>
		 <p>&nbsp;</p> 
          <div id="no-more-tables">
          <table class="oddstable"  width="100%" cellpadding="0" cellspacing="0" border="0">
                  <table id="o0t" width="100%" cellpadding="0" cellspacing="0"
                    class="data table table-condensed table-striped table-bordered">
            <tr class="none">
              <td width="80" align=center><b>ODDS</b></td>
              <td width="70" align=right><div align="center"><b>Payout</b></div></td>
              <td width="11"></td>
              <td width="80" align=center><b>ODDS</b></td>
              <td width="79" align=right><div align="center"><b>Payout</b></div></td>
              <td width="11"></td>
              <td width="80" align=center><b>ODDS</b></td>
              <td width="90" align=right><div align="center"><b>Payout</b></div></td>
            </tr>
            <tr>
              <td align=center data-title="Odds">1-5</td>
              <td align=right data-title="Payout"><div align="center">$2.40</div></td>
              <td></td>
              <td align=center data-title="Odds">8-5</td>
              <td align=right  data-title="Payout"><div align="center">$5.20</div></td>
              <td></td>
              <td align=center data-title="Odds">6-1</td>
              <td align=right data-title="Payout"><div align="center">$14.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">2-5</td>
              <td align=right data-title="Payout"><div align="center">$2.80</div></td>
              <td></td>
              <td align=center  data-title="Odds">9-5</td>
              <td align=right data-title="Payout"><div align="center">$5.60</div></td>
              <td></td>
              <td align=center  data-title="Odds">7-1</td>
              <td align=right data-title="Payout"><div align="center">$16.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">1-2</td>
              <td align=right data-title="Payout"><div align="center">$3.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">2-1</td>
              <td align=right data-title="Payout"><div align="center">$6.00</div></td>
              <td></td>
              <td align=center data-title="Odds">8-1</td>
              <td align=right data-title="Payout"><div align="center">$18.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">3-5</td>
              <td align=right data-title="Payout"><div align="center">$3.20</div></td>
              <td></td>
              <td align=center  data-title="Odds">5-2</td>
              <td align=right data-title="Payout"><div align="center">$7.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">9-1</td>
              <td align=right data-title="Payout"><div align="center">$20.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">4-5</td>
              <td align=right data-title="Payout"><div align="center">$3.60</div></td>
              <td></td>
              <td align=center  data-title="Odds">3-1</td>
              <td align=right data-title="Payout"><div align="center">$8.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">10-1</td>
              <td align=right data-title="Payout"><div align="center">$22.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">1-1</td>
              <td align=right data-title="Payout"><div align="center">$4.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">7-2</td>
              <td align=right data-title="Payout"><div align="center">$9.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">15-1</td>
              <td align=right data-title="Payout"><div align="center">$32.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">6-5</td>
              <td align=right data-title="Payout"><div align="center">$4.40</div></td>
              <td></td>
              <td align=center  data-title="Odds">4-1</td>
              <td align=right data-title="Payout"><div align="center">$10.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">20-1</td>
              <td align=right data-title="Payout"><div align="center">$42.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">7-5</td>
              <td align=right data-title="Payout"><div align="center">$4.80</div></td>
              <td></td>
              <td align=center  data-title="Odds">9-2</td>
              <td align=right data-title="Payout"><div align="center">$11.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">30-1</td>
              <td align=right data-title="Payout"><div align="center">$62.00</div></td>
            </tr>
            <tr>
              <td align=center  data-title="Odds">3-2</td>
              <td align=right data-title="Payout"><div align="center">$5.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">5-1</td>
              <td align=right data-title="Payout"><div align="center">$12.00</div></td>
              <td></td>
              <td align=center  data-title="Odds">50-1</td>
              <td align=right data-title="Payout"><div align="center">$102.00</div></td>
            </tr>
            </table>
          </table>
        </div>
		  <br>
		  <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<p>&nbsp;</p>
<br />
  </div>
  </div>
  </section>
  {*This is the section for the odds tables*}
{* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/templates/block-testimonial-pace-advantage.tpl"}
{* End *}

{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}{* End*}


{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/odds_page/block-exciting.tpl"}
{* end *}

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}