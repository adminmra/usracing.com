<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
{*<link rel="shortcut icon" href="favicon.ico">*}
<link rel="shortcut icon" href="/themes/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" type="text/css" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
<link rel="stylesheet" type="text/css" href="/assets/css/custom_me.css">

<link rel="stylesheet" type="text/css" href="/assets/css/app.css">

<link rel="stylesheet" type="text/css" href="/assets/css/index-ps.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-bs.css">
<link rel="stylesheet" type="text/css" href="/assets/css/index-becca.css">

{if $page == 'index'}
<link rel="stylesheet" type="text/css" href="/assets/css/home_v4.css?v=1">
{/if}

<link rel="stylesheet" type="text/css" href="/assets/css/plugins.css?v=2">
<!--[if IE 9]>
     <link rel="stylesheet" type="text/css" href="/assets/css/ie9.css" />
<![endif]-->
{literal}
<script async type="text/javascript" src="/news/wp-content/themes/usracing/js/put5qvj.js"></script>
<script async type="text/javascript">try{Typekit.load();}catch(e){}</script>
<link href="//fonts.googleapis.com/css?family=Lato:400,300,700,900" rel="stylesheet" type="text/css">
<link href='//fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>

<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
{/literal}


<script src="/assets/js/index-kd.js"></script>

{if $smarty.server.REQUEST_URI == '/index-ps' or $smarty.server.REQUEST_URI == '/index-bs' }
    <script type="text/javascript" src="/assets/plugins/countdown/jbclock.js"></script>
{/if}

{literal}

<!--<script type="text/javascript" src="/assets/plugins/jquery-1.10.2.min.js"></script>-->
<!--[if lt IE 9]>
	 <link rel="stylesheet" type="text/css" href="/assets/plugins/iealert/alert.css" />
     <script type='text/javascript' src="/assets/js/html5.js"></script>
     <script type="text/javascript" src="/assets/plugins/iealert/iealert.min.js"></script>
     <script type="text/javascript">$(document).ready(function() { $("body").iealert({  closeBtn: true,  overlayClose: true }); });</script>
<![endif]-->

<script type="application/ld+json">
    {  "@context" : "http://schema.org", "@type" : "WebSite", "name" : "US Racing", "url" : "https://www.usracing.com"
    }
</script>
{/literal}