{include file='inc/left-nav-btn.tpl'} <div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/stakes.tpl'}
{include file='menus/horsebetting.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->


</div> 
<!--------- NEW HERO  ---------------------------------------->

<div class="newHeroU" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
{*  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   

<div id="main" class="container">
<div class="row"> <div id="left-col" class="col-md-9">

<!--
<div class="headline"><h1>{'Y'|date}  Stakes Races Schedule</h1></div>
-->
<div class="headline"><h1>Graded  Stakes Races Schedule</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>US Racing presents the the complete graded stakes races schedule for {'Y'|date}.  At BUSR can bet from your home computer, mobile or tablet for any of these following races.  You will get up to an {$VARDAILYREBATESref} <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">{$VARBONUSref} Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>

{*include file='includes/ahr_block_graded_stakes_schedule.tpl'*}
<h2>Upcoming Stakes Races</h2>
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<p>{include file='includes/ahr_block_graded-schedule-new.tpl'}</p>
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<h2>Stakes Results</h2>

<p>{include file='includes/ahr_block_graded-schedule-old.tpl'}</p>
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}
 </div><!-- end: #right-col -->

</div><!-- end/row -->
</div><!-- /#container -->

