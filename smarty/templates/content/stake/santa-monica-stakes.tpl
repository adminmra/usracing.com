{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
 <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Santa Monica Stakes</h1> </div>
  
  
  <div class="content">
 
                 
                 
                   <p>Location: 	Santa Anita Park, Arcadia, California, United States<br />
                     Inaugurated: 	1957<br />
                     Race type: 	Thoroughbred - Flat racing<br />
                     Distance: 	7 furlongs<br />
                     Track: 	Dirt, left-handed<br />
                     Qualification: 	Fillies and Mares, four years old &amp; up<br />
                     Weight: 	Assigned<br />
                     Purse: 	$200,000<br />
                     Grade: II </p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Bet on La Santa Monica Stakes</h2>
                   
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join </a>US Racing and bet on Stakes Races today!<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Santa Monica Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/santa-monica-stakes/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
