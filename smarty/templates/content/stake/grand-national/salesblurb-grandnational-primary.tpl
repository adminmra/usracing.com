

	
<p>The<strong> Grand National</strong> is set for April 10, 2021 at Aintree Racecourse, Liverpool, England. </p>

<p>Inaugurated in 1839, the <strong> Grand National</strong> is a handicap steeplechase in which FORTY horses run on turf over a distance of 4 miles, 514 yards, with 30 fences and a purse of £1 million (approximately 1.3 million USD). This marathon of a race is famous around the world as one of the most grueling challenges for any jockey and is the climax of Liverpool's Aintree festival, which comes just a few weeks after the Cheltenham Festival.</p>

<p>Over half a billion people tune in to watch the Grand National each year and it is considered a “must see” event in the United Kingdom, even for people who do not wager on horses.</p>

<p>The leading horse of all time at the <strong>Grand National</strong> was Red Rum (no, we are not talking about what that little kid said in “The Shining”). Red Rum won the Grand National three times - in 1973, 1974 and 1977. Tiger Roll having won the race in 2018 and 2019 now has the chance to equal that incredible hat-trick.</p>

<p><a href='/signup?ref={$ref2}'>Bet on the <strong> Grand National</strong></a> at <a href='/signup?ref={$ref3}'>BUSR </a> and <a href='/signup?ref={$ref4}'>place your bet </a> right now!</p>

