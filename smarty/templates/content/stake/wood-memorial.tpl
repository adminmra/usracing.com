{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1>Wood Memorial</h1>
</div>
  
  
<div class="content">
    
    
            <p>The Wood Memorial is currently one of five Grade I races that serve as prep races for the US Triple Crown. The others include the Blue Grass Stakes, Florida Derby, Santa Anita Derby and the Arkansas Derby. Oddly, the Wood Memorial sends less first place finishers to the Kentucky Derby than the other races mentioned for various reasons (injuries, etc.)<br />
</p>
                 
                   <h2 class="title-custom">Wood Memorial  Stakes Race Information </h2>
                   <p>Location:         Aqueduct Racetrack - Ozone Park, Queens, New York<br />
                     Inaugural Race:         1925<br />
                     Race type:      Thoroughbred - Flat racing<br />
                     Distance         1 1/8 miles (9 furlongs)<br />
                     Track: Dirt, Left-handed<br />
                     Qualification:             Three-year-olds<br />
                     Weight:           123 lbs (55.8 kg)<br />
                     Purse:             $1,000,000<br />
                     Grade I <br />
                   </p>
                   <h2><br />
                     2014 Wood Memorial </h2>
                   <p> Top horses expected to start in the Wood Memorial, are Samraat, Uncle Sigh and Social Inclusion. Samraat's victory over Uncle Sigh went down from one length in the Withers Stakes  to a neck in the Gotham Stakes, what will happen when the two meet again at the Big A? In other news, Honor Code, will miss the Wood Memorial on April 5 and the Derby a month later because of a slight ligament tear.</p>
                   <h2 class="title-custom">Bet on the Wood Memorial   Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <h2 class="title-custom">Wood Memorial  Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/wood-memorial/winners.tpl"}
   
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
