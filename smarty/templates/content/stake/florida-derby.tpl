{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
@media (min-width: 1024px){
.kd {
    padding: 20px 0 0px !important;
}
}

.card_btn--default {

    text-transform: none;

}
</style>
{/literal}

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }
       @media (max-width: 364px) {
         .fixed_cta {
           font-size: 13px !important;
         }
       }
       @media(min-width: 501px) and (max-width: 648px) {
         .fixed_cta {
           font-size: 16px !important;
         }
       }
     </style>
{/literal}
{literal}
<script type="application/ld+json">
    {
    "@context": "https://schema.org",
    "@type": "Event",
    "name": "Florida Derby Stakes",
    "startDate": "2020-03-28T00:00",
    "endDate": "2020-03-28T23:59",
    "eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
    "eventStatus": "https://schema.org/EventScheduled",
    "location": {
        "@type": "Place",
        "name": "Gulfstream Park",
        "address": {
            "@type": "PostalAddress"
        }
    },
    "image": "https://www.usracing.com/img/florida_derby_og.jpg",
    "description": "The Florida Derby Stakes, held annually at Gulfstream Park, is one of the preparatory races for 3-year-olds running for the Kentucky Derby.",
    "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/promos/cash-bonus-20",
        "price": "500",
        "priceCurrency": "USD",
        "availability": "https://schema.org/InStock",
        "validFrom": "2020-01-01T12:00"
    }
}
    </script>
{/literal}


{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>
    </div>
</div>
{*End*}

 {*This is the path for the countdown*}

    {include file="/home/ah//usracing.com/smarty/templates/content/stake/florida-derby/block-countdown_florida derby.tpl"}

  {*end*}



  {*This is the section for the odds tables*}
  <section class=" kd usr-section">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		  <h3 class="kd_subheading">The Most Florida Derby Betting Options Anywhere</h3>
        {*The path for the main copy of the page*}
        <p>{include file="/home/ah/allhorse/public_html/stakes/florida-derby/Salesblurb-primary-copy.tpl"} </p>
        {*end*}
        <p align="center"><p align="center"><a href="/signup?ref={$ref5}" rel="nofollow" class="btn-xlrg">Bet on the Florida Derby</a></p>

		    {*The path for the main copy of the page*}

   </style>

<div id="no-more-tables">



    <table class="data table table-condensed table-striped table-bordered"  border="1" cellpadding="0" cellspacing="0" width="100%">
           <caption class="table-title"><h2>Florida Derby Betting Information</h2></caption> 
           <tr class="no-display">

                <th> Location </th>

                <th> Inaugural Race </th>

                <th> Race Type </th>

                <th> Distance </th>

                <th> Track </th>

                <th> Qualification </th>

                <th> Purse</th>

                <th>Race Type</th>

            </tr>

        </thead>

        <tbody>

            <tr>

                <td data-title="Location"> Gulfstream Park - Hallandale Beach, Florida, USA </td>

                <td data-title=" Inaugural Race"  style="word-break: keep-all !important;"> 19529 </td>

                <td data-title=" Race Type "> Thoroughbred - Flat racing </td>

                <td data-title=" Distance " style="word-break: keep-all !important;"> 1 1/8 miles (9 furlongs) </td>

                <td data-title=" Track " style="word-break: keep-all !important;"> Dirt, Left-handed </td>

                <td data-title=" Qualification " style="word-break: keep-all !important;"> Three-year-olds </td>

                <td data-title=" Purse "> $1,000,000 </td>

                <td data-title=" Race Type "> Grade I </td>

            </tr>

            </tbody>

    </table>
		 <p align="center"><p align="center"><a href="/signup?ref={$ref6}" rel="nofollow" class="btn-xlrg">Bet on the Florida Derby</a></p>
		
    
        {*Winners*}
        <p>{include file="/home/ah/allhorse/public_html/graded_stakes/florida-derby/winners.tpl"}</p>
        <p align="center"><a href="/signup?ref={$ref7}" rel="nofollow" class="btn-xlrg">Bet on the Florida Derby</a></p>
        {*end*}

        {*Second Copy of the page*}
        <p>{include file="/home/ah/allhorse/public_html/stakes/florida-derby/Salesblurb-secondary-copy.tpl"} </p>
		
		<p align="center"><a href="/signup?ref={$ref8}" rel="nofollow" class="btn-xlrg">Bet on the Florida Derby</a></p>
		
        {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}

{* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-exciting.tpl"}
{* end *}

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}