{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
 {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
 <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
                              
          
<div class="headline"><h1>Delta Downs Jackpot Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Delta Downs Jackpot is centerpiece of meet</h2>
                 <div class="news-date">October 9, 2013</div> 
                 <p>The Grade 3, $1 million Delta Downs Jackpot that produced three starters for this year&rsquo;s Kentucky Derby will again serve as the high point of the Delta Downs meet that opens Friday night. The 88-date season in Vinton, La., will continue through March 15.</p>
                   <p>Goldencents, Mylute, and Itsmyluckyday all advanced to the Kentucky Derby following starts in last year&rsquo;s Jackpot, a 1  1/16-mile race for 2-year-olds to be renewed Nov. 23. Goldencents won the Jackpot and went on to later win the Santa Anita Derby, while Mylute turned in the best Kentucky Derby showing among the trio when fifth. The three also ran in the Preakness, with Itsmyluckyday and Mylute finishing a respective second and third behind winner Oxbow.</p>
                   <p>Delta has built its meet around the Jackpot, which will be one of eight stakes worth $2.25 million on the season&rsquo;s lone afternoon card. The program also includes the Grade 3, $500,000 Delta Downs Princess for 2-year-old fillies, a one-mile race won a year ago by Rose to Gold, who went on to win the Fantasy and finish eighth in the Kentucky Oaks. There will be a $200,000-guaranteed pick four that covers both the Jackpot and Princess, with the guaranteed pool doubled in size from last year&rsquo;s $100,000. First post for the most important card of the meet be 1:15  p.m. Central.</p>
                   <p>&ldquo;This will be the fourth under the afternoon post,&rdquo; said Chris Warren, director of racing and simulcasting for Delta. &ldquo;It&rsquo;s worked out so good. It&rsquo;s getting more of a following every year.&rdquo;</p>
                   <p>Delta handled $4.3 million on its Jackpot card last year, the track&rsquo;s second highest all-time handle.</p>
                   <p>Business leading into this year&rsquo;s meet has been on the upswing. Warren said play on the track&rsquo;s 1,639 slot machines has been strong, while there also has been a slight uptick in handle on simulcast races. Those factors have helped Delta make a small increase to its average daily purse structure, which is projected to be $250,000 a program, Warren said.</p>
                   <p>The track&rsquo;s 28-race stakes schedule is worth $4.37 million. It includes preps for the Princess and Jackpot, the respective $150,000 My Trusty Cat on Oct.  25 and $200,000 Jean Lafitte on Oct. 26. The Louisiana Premier Night program on Feb. 1 features eight statebred stakes worth $800,000.</p>
                   <p>Delta&rsquo;s purse structure, one of the richest in the Mid-South region of Arkansas, Louisiana, Oklahoma, and Texas, is the reason Steve Asmussen, who currently leads all trainers in wins in North America, said he has sent a division of horses to Vinton.</p>
                   <p>&ldquo;We [last] had a barn there about three years ago,&rdquo; he said.</p>
                   <p>Karl Broberg, who won the training title last season, is back at Delta, while Gerard Melancon, the defending riding champ, also has returned for the new meet. Both will be active on Friday&rsquo;s opener, an 11-race card that drew an overflow of 120 horses. Field size on the six-furlong track is capped at 10. Entries were as furious for Saturday, with 119 head entered in 11 races.</p>
                   <p>Warren said Delta will bring back two wagers introduced late last meet. There will be rolling daily doubles, he said, and a pick five on the final five races on the card.</p>
                   <p>Nominations for the Jackpot close Oct. 26, with pre-entries for the race to be taken Nov. 12. The Jackpot will again offer its winner 10 points toward the Kentucky Derby on the new preference system introduced last year by Churchill Downs. A second is worth 4 points; third, 2; and fourth, 1. The Princess is part of the Kentucky Oaks points system, with its winner to earn 10 points. </p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Delta Downs Jackpot Stakes</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
                   <p><br />
                   </p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
