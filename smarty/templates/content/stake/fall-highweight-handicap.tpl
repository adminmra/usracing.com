{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Fall Highweight Handicap Video</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Fall Highweight Handicap</h1>
                 <div class="news-date">November 5, 2013</div>
                 
                   <p>Location: <a href="/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance: 6 furlongs <br />
                     Track: Dirt, Left-handed <br />
                     Qualification: Three years old &amp; up <br />
                     Weight: Handicap (top-weighted horse assigned minimum 140 lb) <br />
                     Purse: $100,000-added </p>
                   <p>The race was given its current Grade III status in 2009 by the American Graded Stakes Committee. Run at Belmont Park from its inception in 1914 to 1959 and again from 1963 to 1993, the race was open to horses of any age until 1959 when it was changed to its present format.</p>
                   <p><br />
                   </p>
                   <h2>Fall Highweight Handicap Video</h2>
                   <iframe class="video" src="//www.youtube.com/embed/ST5vVDzS-H8?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Fall Highweight Handicap</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Fall Highweight Handicap Past Winners</h2>
                    {include file="/home/ah/allhorse/public_html/graded_stakes/fall-highweight-handicap/winners.tpl"}                  


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
