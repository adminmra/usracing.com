{literal}
	<style>
		img.rsImg {
		    max-width: 100%;
		}
	</style>
{/literal}

{include file="/home/ah/allhorse/public_html/usracing/block_haskell/block-hero.tpl"} 

<section class="bs-kd usr-section bs">

      <div class="container">

        <div class="bs-kd_content">
        	<br><br>
          	<h1 class="kd_heading">{$h1}</h1>
          	<h3 class="kd_subheading">
	          {if $BS_Switch}{$h2_bs_switch} 
	          {elseif $BS_Switch_Phase_1}{$h2_phase_1} 
	          {elseif $BS_Switch_Race_Day}{$h2_race_day}
	          {else}{$h2}{/if}
	      	</h3>
			<!-- --------------------- content starts here ---------------------- -->
			<p>{*include file='/home/ah/allhorse/public_html/graded_stakes/haskell_stakes/slider.tpl'*} </p>

			<ul style="max-width:310px; text-align:left; margin: auto; font-size: 20px">
			<li><strong>Track:</strong> Monmouth Park </li>
			<li><strong>Distance:</strong> 9 Furlongs </li>
			<li><strong>Surface:</strong> Dirt </li>
			<li><strong>Purse:</strong> $1,000,000</li>
			</ul>

			<h2>Haskell Stakes Betting</h2>

			<p>The Haskell Invitational Handicap is named for the first president and chairman of the Monmouth Park Jockey Club who served two decades until his death in 1966. It is a major race for three year olds in between the Triple Crown series and the Breeders' Cup, it currently offers a purse of $1,000,000 USD</p>
			<p>The track on the Jersey Shore has had more ups and downs than the stock market since opening on July 4, 1870. By 1890, it was completely rebuilt. But the following year, Monmouth's meeting was moved to Jerome Park in New York because of repressive legislation against gambling. Then the track was shuttered for more than half a century.</p>
			<p>It wasn't until 1980 that the present-day Haskell Invitational Handicap became a fixture for three-year-olds and Monmouth Park officially dates the race back to 1968.</p>
			<p>Amory L. Haskell, vice president in charge of General Motors' export division after serving in the Navy during World War I, led a successful fight in 1939 to legalize pari-mutuel wagering in New Jersey. Although World War II delayed construction of a new facility, it opened in 1946.</p>
			<p>In 1968, Monmouth directors honored his memory with the Haskell Handicap for older horses. In 1981, the race was made an invitation-only stakes for 3-year-olds at 1 1/8 miles with a purse of $200,000.</p>
			<p>With 2015 and the end to the Triple Crown draught (Thanks to <a href=/bet-on/american-pharoah>American Pharoah</a>, Bob Baffert and Victor Espinoza)-- many fans were eager to know if American Pharoah would be retired to stud or continue racing.  Lucky for us all, his owner, Ahmed Zayat, said that the fans deserve to see American Pharoah run.  And run he shall!  American Pharoah is the odds favorite to win the 2015 Haskell Invitational Stakes. </p>
			<p>American Pharoah is also slated to run at the 2015 <a href=/breeders-cup/betting>Breeders' Cup</a> -- setting up the first "Grand Slam" of Horse Racing!</p>

			<p>{*include file='/home/ah/allhorse/public_html/graded_stakes/haskell_stakes/video.tpl'*} </p>
        </div>
    </div>
</section>

{*include file="/home/ah/allhorse/public_html/belmont/block-testimonial-pace-advantage.tpl"*}
{*include file="/home/ah/allhorse/public_html/belmont/block-signup-bonus.tpl"*}
{*include file="/home/ah/allhorse/public_html/usracing/block_haskell/block-props.tpl"*}

{* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{*include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"*}
    <section class="card card--red" style="margin-bottom:-40px; background:#1672bd;">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red" style="background:#1672bd; color:#fff;">
            <h2 class="card_heading card_heading--no-cap">Get your New Member Bonus Up To $500 Cash!<br>at BUSR</h2>
            <a href="https://busr.ag?ref={$ref}&signup-modal=open" rel="nofollow" class="card_btn card_btn--default">Sign Up Now</a>
          </div>
        </div>
      </div>
    </section>
{* end *}