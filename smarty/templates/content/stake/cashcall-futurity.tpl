{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>CashCall Futurity annual stop for Triple Crown contenders</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">CashCall Futurity</h1>
                 <div class="news-date">December 1, 2013</div>
                 
                   <p><strong>Location:</strong> Hollywood Park Racetrack, Inglewood, California<br />
                     <strong>Inaugurated:</strong> 1981<br />
                     <strong>Race type: </strong>Thoroughbred &ndash; Flat racing<br />
                     <strong>Distance:</strong> 1 1/16 miles (8.5 furlongs)<br />
                     <strong>Track:</strong> Cushion Track, left-handed<br />
                     <strong>Qualification:</strong> Two-year-olds<br />
                     <strong>Weight: </strong>All starters carry 121 pounds<br />
                     <strong>Purse:</strong> $750,000</p>
                   <p><strong>Race Type: </strong><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> </p>
                   <p><strong>Road to the Roses Points</strong>: 1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point  /road-to-the-roses</p>
                   <p><br />
                   </p>
                   <h2>Bet on CashCall Futurity</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>CashCall Futurity  annual stop for Triple Crown contenders</h2>
                   <p>The $750,000 CashCall Futurity (gr. I), an annual stop for future Triple Crown contenders, will once again be the most lucrative event on the stakes calendar for the final meet at Betfair Hollywood Park.<br />
                       <br />
                     In all, 21 stakes worth $4 million are scheduled for the 29-day season, which begins Nov. 7 and continues through Dec. 22.<br />
                       <br />
                     In its seventh year of sponsorship by the consumer loans company founded by owner/breeder J. Paul Reddam, who won the 2012 Kentucky Derby Presented by Yum! Bands and Preakness Stakes (both gr. I) with I'll Have Another  , the Futurity has produced six Kentucky Derby champions and winners of 15 Triple Crown races since its inaugural running in 1981.<br />
                       <br />
                     The 33rd CashCall Futurity&ndash;for 2-year-olds at 1 1/16 miles on Cushion Track&ndash;will be part of a stakes tripleheader Dec. 14. The $250,000 Hollywood Turf Cup (gr. IIT) at 1 1/2 miles, and the $100,000 Native Diver Stakes (gr. III) for older horses at 1 1/8 miles onCushion Track, will also be offered.<br />
                       <br />
                     &quot;We are very grateful for Mr. Reddam and CashCall's sponsorship through the years, and believe the Futurity will again be an important step on the road to the Triple Crown,'' said Martin Panza, Betfair Hollywood Park's vice president of racing and racing secretary. &quot;We're looking forward to an exciting final season.''</p>
                   <p>&nbsp;</p>
                   <h2>CashCall Futurity Past Winners</h2>           
                   {include file="/home/ah/allhorse/public_html/graded_stakes/cashcall-futurity/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
