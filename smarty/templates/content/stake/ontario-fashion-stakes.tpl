{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Eye on the Ontario Fashion Stakes at Woodbine</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Ontario Fashion Stakes</h1>
                 <div class="news-date">October 24, 2013</div>
                 <p>FOR FILLIES AND MARES, THREE-YEAR-OLDS AND UPWARD By subscription of $150 each, which shall accompany the nomination and an additional $1,500 when making entry. The purse to be divided: 60% to the winner, 20% to second, 10% to third, 5% to fourth, 2% to fifth, 1% to sixth, 1% to seventh, 1% to eighth. Weight Three-Year Olds 121 lbs.; Older 124 lbs. Non-winners of a Sweepstakes of $60,000 three times in 2013, allowed 3 lbs.; Of a Sweepstakes of $60,000 twice in 2013, 5 lbs.; Of a Sweepstakes of $60,000 once in 2013, 7 lbs.; Of two races other than maiden or claiming or restricted allowance in 2013, 9 lbs. (No Canadian Bred Allowance.) Final entries to be made through the entry box at the closing time then in effect for overnight events. A supplemental nomination may be made no later than the time of final entry, by a non-refundable fee of $3,000, which includes the entry fee. *Plus up to $19,500 Ontario Sired/Ontario Bred Breeder Awards.</p>
                   <p>&nbsp;</p>
                   <h2>2013 Contenders</h2>
                   <p>Alythiela<br />
                     Bear's Gem<br />
                     Crysta's Court<br />
                     Dance to the Moon<br />
                     Dene Court<br />
                     Goldstryke Glory<br />
                     Henny's Heart<br />
                     Lemon Splash<br />
                     Roxy Gap<br />
                     Runfor Ro<br />
                     Strike the Moon<br />
                     Youcan'tcatchme</p>
                   <p>&nbsp;</p>
                   <h2>Bet on Ontario Fashion Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Stakes Race Details</h2>
                   <p>Location: <a href="/woodbine">Woodbine</a> Racetrack<br />
                     Distance: Six furlong sprint <br />
                     Track: Polytrack, left-handed <br />
                     Qualification: Fillies &amp; Mares, Three-Years-Old &amp; Up <br />
                     Weight: Assigned <br />
                     Purse: $150,000 (Plus $50,000 for eligible Ontario Breds) </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/ontario-fashion-stakes/winners.tpl"} 


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
