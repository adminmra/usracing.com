{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>More Chocolate wins in dirt debut at La Canada Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">La Canada Stakes</h1>
                 <div class="news-date">December 29, 2013</div>
                 <p>More Chocolate, making her first start on dirt for trainer John Sadler, put away pacesetter Willa B Awesome in the stretch and shook clear for her first stakes victory in the $150,000 La Canada Stakes Jan. 20 at Santa Anita Park. </p>
                   <p>With Garrett Gomez aboard, Michael Talla's More Chocolate held sway in deep stretch over the oncoming Book Review to register a 1 3/4-length win in her dirt bow after seven starts on grass to begin her career. The final time for the 1 1/16-mile event for 4-year-old fillies was 1:42.59 over a fast track. </p>
                   <p>&quot;We wanted to try her on the dirt,&quot; said Sadler. &quot;It's the last chance to run her with her own age group. We thought we might be back a little further, but with a 48 [second] half [mile], I felt good with where we were sitting. We're going to look at the Santa Margarita [$300,000 at 1 1/8 miles for older fillies and mares March 16], that's for sure.&quot; </p>
                   <p>Bred in Kentucky by Calumet Farm, More Chocolate is a daughter of Malibu Moon out of the French-bred Night Shift mare Little Treasure, winner of the 2002 San Clemente Handicap at Del Mar and a lifetime earner of $305,262, all on grass. More Chocolate is the first stakes winner from four foals to race produced by her dam. </p>
                   <p>More Chocolate sold for $310,000 to Martin Anthony from consigner Cary Frommer at the 2011 Fasig-Tipton Florida February Select Two-Year-Olds in Training Sale. The bay filly was sold as a Keeneland September yearling for $45,000 in 2010 by Calumet, as agent, to PHB Racing. </p>
                   <p>Sent off at odds of 5-1 in the La Canada, More Chocolate was coming off a third-place finish against older females in the Robert J. Frankel Stakes, her stakes debut Dec. 30 on the Santa Anita turf course. An allowance winner on turf at nine furlongs Nov. 1, she had been training solidly over the Santa Anita main track. She most recently recorded a bullet six-furlong move in 1:12 2/5 Jan. 14. </p>
                   <p>A late betting move made Lady of Fifty the narrow 6-5 favorite over Book Review, bet to 7-5, in the six-horse field. </p>
                   <p>With little speed signed on, Willa B Awesome and Martin Pedroza, from between horses, made the early running into the clubhouse turn as More Chocolate and outsider Ubelongtomemissy took up position about a length off the pacesetter. Willa B Awesome brought them along at an even clip through quarter mile fractions of :24.34, :48.03, and 1:12.00. More Chocolate, turning up the pressure on the outside, put her head in front midway on the far turn. </p>
                   <p>More Chocolate took a narrow edge into the lane and inched away to a 1 1/2-length lead over her pace rival mid-stretch. She finished encouragingly under steady left-handed urging from Gomez as Book Review and Rafael Bejarano, after swinging wide for the drive, tried in vain to make a race of it. </p>
                   <p>&quot;She was relaxed going into the first turn then she jumped up in the bridle on me,&quot; said Gomez. &quot;I didn't want to get into a wrestling match with her so I let her be. She settled into a good rhythm going down the backside. I haven't had a horse turn for home like she did for me in a long time.&quot; </p>
                   <p>Book Review was a clear second by 2 1/4 lengths over Willa B Awesome. Lady of Fifty, the 123-pound highweight, was a disappointing fourth after floating wide leaving the far turn and failing to muster much of a charge. Ubelongtomemissy and Open Water trailed. </p>
                   <p>The $90,000 purse nearly doubled More Chocolate's earnings to $182,790 with her third win from eight lifetime starts. </p>
                   <p>&nbsp;</p>
                   <h2>La Canada StakesRace Information</h2>
                   <p><br />
                     <strong>Location: </strong>Santa Anita Park, Arcadia, California <br />
                     <strong>Inaugurated: </strong>1975 <br />
                     <strong>Race type: </strong>Thoroughbred &ndash; Flat racing <br />
                     <strong>Distance:</strong> 1 1/16 miles (8.5 furlongs) <br />
                     <strong>Track: </strong>Dirt, left-handed <br />
                     <strong>Qualification: </strong>Four years old fillies &amp; mares<br />
                     <strong>Weight:</strong> Assigned <br />
                     <strong>Purse: </strong>$200,000 <br />
                     <strong>Grade:</strong> II<br />
                   </p>
                   <p><strong>La Canada Stakes</strong> is part of Santa Anita Park's La Canada Series of races open to newly turning/turned 4-year old fillies and run at an increasing distance. The series begins with the La Brea Stakes at 7 furlongs in December of the previous year. It is followed by the G-II El Encino Stakes at 1 1/16 miles in mid January, then the La Canada Stakes during the second week of February. </p>
                   <p>Since the creation of the series in 1975, only three fillies have ever won all three races: Taisez Vous (1978), Mitterand (1985), and Got Koko (2003). The Santa Anita Park counterpart series for male horses is the <a href="/strub-stakes">Strub Stakes</a> Series.</p>
                   <p>&nbsp;</p>
                   <h2>Bet on La Canada Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                   </p>
                   <h2>La Canada Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/la-canada-stakes/winners.tpl"} 

             
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
