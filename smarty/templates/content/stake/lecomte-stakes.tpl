{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>Lecomte Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



                   <p><strong>Location:</strong> <a href="/fair-grounds-race-course">Fair Grounds</a> Race Course, New Orleans, Louisiana, United States<br />
                     <strong>Inaugurated:</strong> 1970<br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing<br />
                     <strong>Distance: </strong>1 mile 70 yards<br />
                     <strong>Track: </strong>Dirt, left-handed<br />
                     <strong>Qualification: </strong>Three-year-olds<br />
                     <strong>Purse: </strong>$175,000</p>
                   <p><strong>Race Type:</strong> <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> </p>
                   <p><strong>Road to the Roses Points: </strong>1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point<br />
                   </p>
                   <p>Many horse trainers view the Lecomte Stakes as a prep race for the Louisiana Derby, and potentially the <a href="/kentucky-derby">Kentucky Derby</a>. Lecomte is the first Kentucky Derby points race of the year at New Orleans' Fair Grounds, in late January with 10 points available to the winners.</p>
                   <p><br />
                   </p>
                   <h2>Lecomte Stakes and the Kentucky Derby <br />
                   </h2>
                    <p>&nbsp;</p>
                   <p>War Emblem is the only horse to win the Kentucky Derby after running in the Lecomte, in which he was fifth in 2002. Hard Spun won the 2007 Lecomte before finishing second at Churchill Downs.<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on <span class="title-custom">Lecomte Stakes</span></strong></h2>
                     <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2>Lecomte Stakes Past Winners<br />
                   </h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/lecomte-stakes/winners.tpl"} 
   

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
