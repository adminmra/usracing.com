{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Honor Code looking to Remsen Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Remsen Stakes</h1>
                 <div class="news-date">October 11, 2013</div>
                 <p>Foxwoods Champagne Stakes runner-up Honor Code will bypass the $2 million Breeders' Cup Juvenile at Santa Anita Park in favor of the $250,000 Remsen Stakes at Aqueduct Racetrack, Hall of Fame trainer Shug McGaughey said Oct. 10.</p>
                 <p>The 2-year-old from the last crop of A.P. Indy ran lights out in the one-mile Champagne Oct. 5 at Belmont Park, closing eight wide with a tremendous rally under Javier Castellano to just miss catching Havana by a neck. It was his second career start and came off an Aug. 31 victory by 4 &frac12; lengths going seven furlongs at Saratoga Race Course.</p>
                   <p>&quot;The major part of that decision was that we just want to have a nice horse next year,&quot; McGaughey said of skipping the Breeders' Cup with the Dell Ridge Farm-bred colt, who runs for Dell Ridge and Lane's End Racing. &quot;We just felt like taking a young one out there that's only run two times, especially one we have a lot of high hopes for, might not be in the best interests of the horse.&quot;</p>
                   <p>McGaughey said following the Champagne that he was inclined to stay home rather than send Honor Code cross country to compete in the Nov. 2 Juvenile, where he would have been rematched with Havana.</p>
                   <p>&quot;With the Remsen here going a mile and an eighth around two turns at Aqueduct -- which, barring bad weather, at that time of year the track is usually really good -- we decided to stay home and train him a little bit and have him ready to run in that race instead,&quot; he explained.</p>
                   <p>The Remsen is Nov. 30.</p>
                   <p>Honor Code was bred in Kentucky out of the Storm Cat mare Serena's Cat.</p>
                   <p>&quot;He was a little wired after the Champagne but he's settled in fine now,&quot; McGaughey said. &quot;Obviously we'd like to be able to compete in the Breeders' Cup any time we feel llike we've got a good chance, but we felt like in this case, the right thing to do would be to stay here.&quot; </p>
                   <p>&nbsp;</p>
                   
                   <h2><strong>Bet on Remsen Stakes</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>Track: <a href="/racetrack/aqueduct">Aqueduct</a> Racetrack<br />
Distance: 1 1/8 miles (9 furlongs) <br />
Track: Dirt, left-handed <br />
Qualification: Two-year-olds <br />
Weight: Handicap <br />
Purse: $200,000<br />
                   Race Type: <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race </a><br />
                     Road to the Roses Points: 1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                  
                  <h2>Remsen Stakes Video</h2>
                   <p>Published on Nov 24, 2012 
                     
                     Repole Stable's Overanalyze re-rallied to nip Normandy Invasion and capture the Remsen Stakes (gr. II) Nov. 24 by a nose at Aqueduct Racetrack. </p>
                   <p align="center"><iframe width="100%" height="360" src="//www.youtube.com/embed/DtQl0xsYpkg?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <h2>Past Winners of the Remsen Stakes</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/remsen-stakes/winners.tpl"}

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
