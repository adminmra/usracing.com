{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Whitney Handicap Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file='/home/ah/allhorse/public_html/graded_stakes/whitney_handicap/slider.tpl'} </p>



<h2>Whitney Stakes Betting</h2>

<ul>
<li><strong>Stakes:</strong> Grade I for 3-year-old thoroughbreds and fillies </li>
<li><strong>Distance:</strong> 1 1/8 miles </li>
<li><strong>Track:</strong> Sar </li>
<li><strong>Purse:</strong> $1,000,000</li>
</ul>
<p>The Whitney Handicap (often referred to as the "Whitney Stakes") is an American Grade 1 handicap race for Thoroughbred racehorses three years of age and older run at a distance of 1 1?8 miles. In 2007, the Breeders' Cup Ltd. introduced the "Win and You're In" qualification format that means the winner of the Whitney Handicap will automatically qualify for the fall running of the Breeders' Cup Classic.</p>
<p>Held annually in late July/early August at the Saratoga Race Course in Saratoga Springs, New York, the race is named for the Whitney family, whose members have and remain prominent participants and supporters of the sport of Thoroughbred horse racing.</p>
<p>Raced at a distance of 1 1?4 miles from inception in 1928 until 1940, it was closed to geldings. The inaugural running was won by William R. Coe's two-time Champion Filly, Black Maria. During World War II, the race was run at Belmont Park from 1943 through 1945, and again once in 1961. Between 1957 and 1969 the race was restricted to horses four years and older.</p>
<p>Some of the greatest horses in American racing history have won the Whitney Handicap, including Kelso, who won it for the third time in 1965 at the age of eight. The race also saw one of the most dramatic upsets in racing history when Secretariat finished second in the 1973 Whitney to Allen Jerkens's colt, Onion. Six fillies have won the race: Black Maria (1928), Bateau (1929), Esposa (1937), Gallorette (1948), Lady's Secret (1986), and Personal Ensign (1988).</p>
<p>In the 2015 listing of the International Federation of Horseracing Authorities (IFHA), the Whitney tied with the Kentucky Derby as the top Grade 1 race in the United States outside of the Breeders' Cup races.[1]</p>
{include file='/home/ah/allhorse/public_html/graded_stakes/whitney_handicap/video.tpl'}
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/whitney_handicap/calltoaction.tpl'}

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
