{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Coil Goes Out with San Pasqual Win</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">San Pasqual Stakes</h1>
                 <div class="news-date">                   January 5, 2013</div>
                 <p>In his final start before he heads off to stud in California, Grade 1 winner Coil outlasted Ultimate Eagle by a short head following a terrific stretch duel in the $150,000 <strong>San Pasqual Stakes</strong> Jan. 5, 2013 at Santa Anita Park. </p>
                   <p>Trained by Hall of Fame conditioner Bob Baffert for owners Karl Watson, Mike Pegram, and Paul Weitman, million-dollar earner Coil recorded his first two-turn victory since the 2011 Haskell Invitational. </p>
                   <p>The gorgeous 5-year-old chestnut, by 2001 Horse of the Year Point Given out of the winning Theatrical mare Eversmile, is to stand at Magali Farms in Santa Ynez. Glen Hill Farm bred the winner in Florida. </p>
                   <p>&quot;He's had some issues that I just didn't want to run him this year,&quot; Baffert said. &quot;I'd have to run him sparingly. I think he deserves to be retired. He's good; he's very healthy right now, but it was time to send him to stud. </p>
                   <p>&quot;He's got so much heart,&quot; the trainer added. &quot;That was all Coil &hellip; We're really going to help the horse in California. Mike [Pegram], one of the main reasons he wanted to retire him here also is because Mike's on the TOC board. He wants to help California racing and he's putting his money behind California racing.&quot; </p>
                   <p>Coil won the the Santa Anita Sprint Championship Oct. 6 prior to a disappointing seventh in the Xpressbet Breeders' Cup Sprint. In his most recent effort, Coil ran third after pressing the pace in the Cigar Mile Nov. 24 at Aqueduct Racetrack. But he is probably best known for his thrilling neck victory over Preakness winner Shackleford after a terrible start in the Haskell at Monmouth Park as a 3-year-old in the summer of 2011. </p>
                   <p>The bettors had this year's San Pasqual pegged spot on, sending off Coil as the slight 9-5 favorite over 2-1 Ultimate Eagle in a field reduced to six by three scratches. Those two separated themselves from the rest leaving the far turn and put on a superb show through the homestretch. Coil, ridden on the outside by Martin Garcia, posted a winning time of 1:42.52 in the 1 1/16-mile test of older horses over a fast track. </p>
                   <p>&quot;I had a really good horse inside of me [Ultimate Eagle], and I just tried to follow the way he was going,&quot; said Garcia, who rode Coli in most of his races. &quot;When I asked him [Coil] to go, he just responded, and that was it. He has been a really good horse for a long time.&quot; </p>
                   <p>Ultimate Eagle, making only his second start since faltering as the favorite in the Santa Anita Handicap 10 months ago, was using the race as a prep for this year's Big 'Cap. He set a solid pace (:23.56, :47.64, and 1:11.28) under Martin Pedroza while leading Coil, Bank the Eight, and John Scott into the far turn, and was inching away from Coil rounding the bend. </p>
                   <p>But Coil reeled him in on the outside as they battled through the lane, getting a slight advantage past the eighth pole under urging from Garcia. He prevailed gamely in the final strides as Ultimate Eagle fought back all the way to the wire. </p>
                   <p>B.J. Wright's Ultimate Eagle, winner of last year's Strub Stakes over the track, was 6 1/4 lengths clear of third-place finisher John Scott. Then came Tres Borrachos and the Baffert-trained duo of Bank the Eight and Jaycito, who lugged out on the final turn after making an early run and was eased in the stretch. </p>
                   <p>Coil's victory was his seventh in 14 lifetime races to go with two seconds and three thirds. He ends his career with earnings of $1,154,360. Comfortable on dirt or synthetics and sprinting or routing, his only other start at the San Pasqual's distance of 1 1/16 miles produced a victory in the Affirmed Handicap on Cushion Track at Hollywood Park prior to the Haskell as a 3-year-old. </p>
                   <p>&quot;He won Grade 1's short and long and he's by Point Given, and he's one of my favorite horses, so it's good that he's here,&quot; Baffert said of the decision to stand Coil in California. </p>
                   <p>Coil carried 118 pounds, two fewer than the high-weighted Ultimate Eagle, and paid $5.60, $3, and $2.20. He topped a $20.60 exacta with Ultimate Eagle. The latter returned $3.80 and $2.80 in his first start since a runner-up finish in the Citation Handicap on grass during the Autumn Turf Festival at Hollywood Nov. 23. John Scott, fourth in the Breeders' Cup Dirt Mile in his most recent start for trainer Carla Gaines, paid $2.80 to show.</p>
                   <p>&nbsp;</p>
                   <h2>Bet on San Pasqual Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                   <h2>San Pasqual Stakes Race Information</h2>
                       
                   <p>                     <strong>Location: </strong>Santa Anita Park, Arcadia, California <br />
                     <strong>Inaugurated: </strong>1935 <br />
                     <strong>Race type: </strong>Thoroughbred &ndash; Flat racing <br />
                     <strong>Distance: </strong>1 1/16 miles (8.5 furlongs) <br />
                     <strong>Track:</strong> Dirt, left-handed <br />
                     <strong>Qualification:</strong> Four years old and up <br />
                     <strong>Weight: </strong>Assigned <br />
                     <strong>Purse: </strong>$150,000 <br />
                     <strong>Grade:</strong> II</p>
                   <p>&nbsp;</p>
                   <h2>San Pasqual Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/san-pasqual-stakes/winners.tpl"}
          

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
