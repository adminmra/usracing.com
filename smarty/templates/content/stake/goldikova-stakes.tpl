{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Preparing for the Goldikova Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Goldikova Stakes</h1>
                 <div class="news-date">October 24, 2013</div>
                 <p>For fillies and mares, three years old and upward by subscription of $50 each or by supplementary nomination of $3,000 at time of entry. All horses to pay $2,250 additional to start, with $150,000 guaranteed of which $90,000 to the winner, $30,000 to second, $18,000 to third, $9,000 to fourth and $3,000 to fifth. Three-year-olds: 121 lbs. Older: 124 lbs. Non-winners of  a Grade I or Grade II race at one mile or over since May allowed 3 lbs. Non-winners of a Grade I or Grade II race at one mile or over or two Grade III races at one mile or over since November, 5 lbs. Highweights preferred, followed by graded stake winners within 18 months of race, followed by graded stakes placed horses within 18 months of race, followed by highest earners within 12 months of race. Starters to be named through the entry box by the closing time of entries. A trophy will be presented to the winning owner.</p>
                 <p>&nbsp;</p>
                 <h2>Bet on Goldikova Stakes</h2>
                 <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                 <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                 <p>&nbsp;</p>
                 <h2>Stakes Race Details</h2>
                 <p>Track: <a href="/santa-anita-park">Santa Anita Park</a><br />
                     Track: 1 mile (Turf)<br />
                     Grade: II<br />
                     Purse: $150,000 Guaranteed</p>
                 <p>&nbsp;</p>
                   <h2>2013 Contenders</h2>
                   <p>A TIME TO LOVE b.f.4 Petrick or Tannenbaum Tim Yakteen<br />
                     APPEALING (IRE) b.f.4 Yvonne Jacques Patrick Gallagher<br />
                     CARAQUISTA gr/ro.f.3 Rontos Racing Stable Corp. Jeff Bonde<br />
                     CUSTOMER BASE dk b/.f.4 Glen Hill Farm Thomas F. Proctor<br />
                     E Z KITTY dk b/.m.5 Highlander Racing Stable, LLC Jeffrey Metz<br />
                     EGG DROP gr/ro.f.4 Little Red Feather Racing Mike Mitchell<br />
                     HEIR KITTY b.f.3 Bernsen or Makin Peter Miller<br />
                     ISMENE dk b/.f.4 Stephen Ferraro Bill Spawr<br />
                     MISS EMPIRE b.f.3 Barber or Barber or Tsujihara Peter Miller<br />
                     MISS PIPPA gr/ro.f.4 Joy Ride Racing John W. Sadler<br />
                     MORE CHOCOLATE b.f.4 Michael Talla John W. Sadler<br />
                     QIAONA ch.f.3 Curt &amp; Lila Lanning, LLC Ed Moger, Jr.<br />
                     SISTER KATE gr/ro.m.5 Mary H. Caldwell Jerry Hollendorfer<br />
                     THE GOLD CHEONGSAM (IRE) b.f.3 Kerri Radcliffe Jeremy Noseda<br />
                     ULTRASONIC b.f.4 Juddmonte Farms Bob Baffert<br />
                     VIONNET gr/ro.f.4 Ran Jan Racing, Inc. Richard Mandella<br />
                     WINDING WAY dk b/.f.4 Spendthrift Farm, LLC Carla Gaines<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
                   <p>2012 (inaugural year) Rhythm of Light</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
