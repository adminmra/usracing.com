{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Arlington Million</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file='/home/ah/allhorse/public_html/graded_stakes/arlington_million/slider.tpl'} </p>



<h2>Arlington Million Betting</h2>

<ul>
<li><strong>Stakes:</strong> Grade I for 3-year-old thoroughbreds and fillies </li>
<li><strong>Distance:</strong> 1 1/8 miles </li>
<li><strong>Track:</strong> Dirt </li>
<li><strong>Purse:</strong> $1,000,000</li>
</ul>
<p>The Arlington Million is a Grade 1 flat horse race in the United States for thoroughbred horses aged three years and upward. It is raced annually in August over a distance of 1 1?4 miles (approximately 2,000 metres) on the turf at Arlington Park in Arlington Heights, Illinois.</p>

<p>The Arlington Million was the first thoroughbred race to offer a purse of US$1,000,000. It is part of the Breeders' Cup Challenge series, and the winner automatically qualifies for the Breeders' Cup Turf.</p>

<p>The Arlington Million was introduced in 1981 by Joe Joyce, the president of Arlington Park at the time. The winner receives 60% of the million dollar purse and the Arlington Million Trophy. The race was graded after only its second running and was awarded a grade one status in 1983 based on the talent of the runners that raced in its first two years.</p>

<p>The horse John Henry won the race twice.[3][4] On August 30, 1981, Willie Shoemaker became the first jockey to win a $1 million thoroughbred horse race when John Henry took the inaugural Arlington Million by a nose over The Bart. The track famously ran the Arlington Million in 1985 under the shadow of a burnt-out grandstand, after a fire had occurred there 25 days earlier. In 2007, Jambalaya became the first Canadian bred horse to win the Arlington Million, with his trainer, Catherine Day Phillips, being the first female trainer to ever win the race.</p>

<p>The race had been run under several different names in the past: In 1981, it was called the Arlington Million Invitational Stakes; from 1982 through 1984 it was known as the Budweiser Million Stakes; from 1985 through 1987, it was the Budweiser-Arlington Million. In 1988, the race was held at Woodbine Racetrack in Toronto, Ontario to accommodate for the completion of repairs from a 1985 fire, and there was no race held in 1998 or 1999 during a two-year shutdown of Arlington Park.</p>

{include file='/home/ah/allhorse/public_html/graded_stakes/arlington_million/video.tpl'}
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/arlington_million/calltoaction.tpl'}

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    