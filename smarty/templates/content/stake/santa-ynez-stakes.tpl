{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>Santa Ynez Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



                   <p><strong>Location:</strong> Santa Anita Park, Arcadia, California, United States<br />
                     <strong>Inaugurated:</strong> 1952<br />
                     <strong>Race type:</strong> Thoroughbred, Flat racing<br />
                     <strong>Distance:</strong> 61&#381;2 furlongs<br />
                     <strong>Track:</strong> Dirt, left-handed<br />
                     <strong>Qualification:</strong> Three-year-old fillies<br />
                     <strong>Weight:</strong> Assigned<br />
                     <strong>Purse:</strong> $150,000<br />
                     <strong>Grade:</strong> II</p>
                   <p>The <strong>Santa Ynez Stakes</strong> is a Grade II event with a purse of $150,000 and has been a prep race to the Triple Tiara of Thoroughbred Racing, including the Kentucky Oaks, the Black-Eyed Susan Stakes and Mother Goose Stakes. The Santa Ynez Stakes is run in mid January at Santa Anita Park, every year.</p>
                   <p>&nbsp;</p>
                  <h2>Bet on Santa Ynez Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                   <h2>Santa Ynez Stakes 2014 Contenders</h2>
                         
                   <p>Ascription<br />
                     Awesome Baby<br />
                     Be Proud<br />
                     Crushed Velvet<br />
                     Koukla<br />
                     Loan Savant<br />
                     On The Backstreets<br />
                     Saintly Joan<br />
                     Taste Like Candy<br />
                     That's the Idea<br />
                     Uzziel<br />
                     Zensational Bunny</p>
                   <p>&nbsp;</p>
                   <h2>Santa Ynez Stakes Past Winners</h2>
                         
{include file="/home/ah/allhorse/public_html/graded_stakes/santa-ynez-stakes/winners.tpl"}
          

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
