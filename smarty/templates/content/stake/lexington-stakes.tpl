{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1><br />
    Lexington Stakes</h1>
</div>
  
  
<div class="content">
    
    
            <h2><br />
        Lexington Stakes Race Information </h2>
        <p>Location: 	Keeneland Race Course - Lexington, Kentucky, United States<br />
  Inaugural Race: 	1936<br />
  Race type: 	Thoroughbred - Flat racing<br />
  Distance: 	1 &amp; 1/16 miles (8.5 furlongs)<br />
  Track: 	Polytrack, left-handed<br />
  Qualification: 	Three-year-olds<br />
  Weight: 	Assigned<br />
  Purse: 	$200,000<br />
  Grade III race<br />
  </p>
        <p>The Lexington Stakes is sponsored by, Coolmore Stud,  the world's largest breeding operation of thoroughbred racehorses, located in Ireland. </p>
        <p>Lexington has been known as a major center for Thoroughbred breeding since the late 18th century due to the high calcium content in the soils of the Inner Bluegrass Region, which leads to stronger bones and greater durability in horses.</p>
        <p>Lexington, KY is the second-largest city in Kentucky and the 62nd largest in the United States. Known as the &quot;Horse Capital of the World&quot;, it is located in the heart of Kentucky's Bluegrass region. </p>
        <h2 class="title-custom">Bet on the Lexington Stakes</h2>
        <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
        <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
        <h2>Lexington Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/lexington-stakes/winners.tpl"} 
                   
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
