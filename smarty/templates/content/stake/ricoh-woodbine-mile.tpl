{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{assign  var="stakesname" value="Woodbine Mile"} 
{assign  var="stakespath" value="woodbine-mile"} 
{assign  var="racetrackpath" value="woodbine"} 
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           

<div class="headline"><h1>{$stakesname} Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/slider.tpl"} </p>

<h2>Bet on {$stakesname} </h2>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/specs.tpl"}
{*WIKI CONTENT-------------------------------*}

<p>The Woodbine Mile is a Grade I stakes race on turf for Thoroughbred racehorses three years old and up held annually in mid September at <a href="/woodbine">Woodbine Racetrack</a> in Toronto, Canada. The Woodbine Mile is Part of the <a href="/breeders-cup/mile">Breeders' Cup Challenge</a> series, the winner of the  Woodbine Mile automatically qualifies for the <a href="/breeders-cup/mile">Breeders' Cup Mile</a>.</p>


<p>The inaugural race in 1988 was sponsored by Molson Breweries with a purse of $750,000 and run as the Molson Export Challenge, reflecting the name of the company's flagship beer. It was changed to the Molson Export Million when the purse was increased to $1 million. In 1999, under new sponsorship it was renamed and raced as the Atto Mile until 2006.</p>

<p>Since inception, the race has been run at three different distances:</p>

   <blockquote> 1 mile : 1997–present<br>
    1 1⁄8 miles : 1991-1996<br>
    1 1⁄4 miles : 1988-1990</blockquote>

<p>Originally restricted to three-year-olds, with the modification to one mile in 1997 the race was also made open to older horses.</p>

<h2>{$stakesname} Odds</h2>
<p>For the latest {$stakesname} odds, please login into BUSR and check the racebook.</p>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/video.tpl"}
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    