{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                        
          
<div class="headline"><h1>Pacific Classic Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{include file='/home/ah/allhorse/public_html/graded_stakes/pacific_classic/slider.tpl'}

<h2>Pacific Classic Stakes Betting</h2>
<ul>
<li><strong>Grade:</strong> I for 3-year-old thoroughbreds and fillie</li>
<li><strong>Distance:</strong> 1 1/4 miles</li>
<li><strong>Track:</strong> Dirt</li>
<li><strong>Purse:</strong> $1,000,000</li>
</ul>

<p>

The Pacific Classic Stakes is an American Thoroughbred horse race for runners aged 3-year-old or up run at the classic American distance of a mile and a quarter (10 furlongs, 2 km). It is run each year in August at <a href="/del-mar">Del Mar Racetrack</a> in Del Mar, California, and the Grade 1 race has become one of the top prizes for older horses racing in the United States each year.</p>

<p>First run in 1991, the Pacific Classic offers a purse of $1-million and is the premier event of the season at one of the nation's premier race meetings conducted for seven weeks each year at the seaside racing plant just north of San Diego.</p>

<p>Notable among the runnings of the race was the 1996 renewal that saw champion Cigar attempt to best Citation's mark of 16 straight victories, but fall to a shocking upset ($81.20) by the unheralded Dare And Go and jockey Alex Solis. Another exciting running saw Hall of Fame Jockey Julie Krone become the first female rider to win a million-dollar race when she guided Sid and Jenny Craig's Candy Ride to victory in track record time of 1:59:11.</p>

<p>Del Mar switched its main racing surface from dirt to synthetic Polytrack in 2007 and the Pacific Classic winner that year was long-shot Student Council, who was handled by veteran Richard Migliore.</p>

<p>Starting in 2008, the Pacific Classic is a part of the <a href="/breeders-cup/challenge">Breeders' Cup Challenge</a>, where the winner of the race will automatically earn a spot in the  <a href="/breeders-cup/classic">Breeders' Cup Classic</a> at Oak Tree.[1]

</p>


{include file='/home/ah/allhorse/public_html/graded_stakes/pacific_classic/video.tpl'}

        
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/pacific_classic/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    