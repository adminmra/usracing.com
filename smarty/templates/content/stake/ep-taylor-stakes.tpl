{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/states.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>E.P. Taylor Stakes attracts 24 nominees</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">E. P. Taylor Stakes</h1>
                 <div class="news-date">October 10, 2013</div>
                 <p>The first three finishers from Woodbine's Canadian Stakes on Sept. 15 head a list of 24 nominees to this month's $500,000 E.P. Taylor Stakes. The 1 1/4-mile turf stakes is for fillies and mare.</p>
                   <p>Minakshi, ridden by Luis Contreras, defeated stablemate Colonial Flag by a half-length in the $306,400 Canadian Stakes. Both horses are trained by Michael Matz. Finishing third was Moment of Majesty.</p>
                   <p>The victory qualified Minakshi for a berth in the $2 million Breeders' Cup Filly &amp; Mare Turf at Santa Anita Park on Nov. 1.</p>
                   <p>Minakshi, owned by Northern Bloodstock, has five wins in 18 lifetime starts for $345,669. Earlier this year, the 5-year-old mare was second in the Owsley Stakes at Belmont Park followed by a third at Belmont in the Sheepshead Bay Stakes and a fourth in the New York Stakes. She then finished third in the Matchmaker at Monmouth Park.</p>
                   <p>&quot;She's a bit of a quirky filly so you got to be really patient with her,&quot; said Joy Cooper, assistant to Matz. &quot;She has to have the pony in the paddock and you just hope everything goes her way because the switch can flip and she can lose it, so it is more so just trying to let things go her way.&quot;</p>
                   <p>Colonial Flag, owned by W. S. Farish, Skara Glen Stable and George Prussin, won the Rosenna Stakes at Delaware Park on Aug. 17. The 4-year-old filly has banked $313,424 with four wins in 12 starts.</p>
                   <p>Trained by Roger Attfield, Moment of Majesty is a veteran of 27 career starts with four wins, seven seconds and six thirds for $635,351. All but one of her starts have come at Woodbine.</p>
                   <p>Moment of Majesty, owned by Regis Racing, was seventh in last year's Taylor behind Siyouma.</p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on E.P. Taylor Stakes</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
