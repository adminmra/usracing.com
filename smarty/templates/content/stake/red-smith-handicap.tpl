{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Who will take the Red Smith Handicap</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



                 <h1 class="news-date">Red Smith Handicap</h1> 
                 <div class="news-date">November 5, 2013</div>
                 <p>Location: <a href="/aqueduct">Aqueduct</a> Racetrack<br />
                     Inaugurated: 1960 <br />
                     Race type: Thoroughbred - Flat racing <br />
                     Distance 1 3/8 miles (11 furlongs) <br />
                     Track: Grass, left-handed <br />
                     Qualification: Three-year-olds &amp; up <br />
                     Weight: Assigned <br />
                     Purse: $150,000 </p>
                   <p>The Red Smith Handicap is a race on turf open to horses age three and older, it is run on the first week of November at Aqueduct. The inaugural race was in 1960 under the name, the Edgemere Handicap. In 1981, it changed to Red Smith Handicap.<br />
                   </p>
                   <p>In his second year, Boisterous scored an easy victory in the Grade 2, $200,000 Red Smith Handicap at Aqueduct Racetrack on Saturday, wrapping up his season with identical back-to-back stakes wins. </p>
                   <p>A repeat winner of last month&rsquo;s Grade 3 Knickerbocker at Belmont Park, Boisterous lived up to his role as the 2-5 favorite as he successfully defended his title in the 53rd edition of the Red Smith in 2012. With Hall of Fame jockey Edgar Prado aboard, the 5-year-old son of Distorted Humor tracked Bombaguia through tepid fractions of 52.29 seconds for the opening half-mile mile and 1:43.23 for the mile, moved into striking position around the final turn and took command at the head of the stretch before drawing clear to a three-length victory over Lake Drop.</p>
                   <p>Boisterous completed 1 3/8 miles in 2:19.38 over the yielding turf course.</p>
                   <p>&ldquo;The horse ran huge,&rdquo; said Prado. &ldquo;He showed up one more time and did it pretty easily.&rdquo;</p>
                   <p>The victory was the third in seven 2012 starts for Boisterous, who also won the Grade 3 Fort Marcy in May at Belmont, ran second in the Grade 3 Arlington Handicap and third in the Grade 1 Manhattan Handicap. His only off-the-board performance came in August when he finished ninth, beaten 5 &frac34; lengths, in the Grade 1 Arlington Million.</p>
                   <p>&ldquo;He wasn&rsquo;t beaten that badly [in the Million], but I think he&rsquo;s the kind of horse who appreciates the fall weather more than the summer heat,&rdquo; said Buzzy Tenney, assistant to winning Hall of Fame trainer Shug McGaughey. &ldquo;I believe this will be the last race of the year for him. We&rsquo;ll take him down to Florida and bring him back in the spring, just like last year.&rdquo;</p>
                   <p>&nbsp;</p>
                   <h2>Bet on Red Smith Handicap</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <h2>Red Smith Handicap Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/red-smith-handicap/winner.tpl"}


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


</div>
