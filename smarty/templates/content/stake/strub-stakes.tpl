{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>Strub Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="news-date"></div>
                 
                   <p><strong>Location: </strong>Santa Anita Park, Arcadia, California <br />
                     <strong>Inaugurated: </strong>1948 <br />
                     <strong>Race type:</strong> Thoroughbred &ndash; Flat racing <br />
                     <strong>Distance: </strong>1 1/8 miles (9 furlongs) <br />
                     <strong>Track:</strong> Dirt, left-handed <br />
                     <strong>Qualification: </strong>Four years olds<br />
                     <strong>Weight:</strong> Assigned <br />
                     <strong>Purse: </strong>$200,000 <br />
                     <strong>Grade: </strong>II<br />
                   </p>
                   <p>The <strong>Strub Stakes</strong> was inaugurated in 1948 as the Santa Anita Maturity but the name was changed to the Charles H. Strub Stakes in 1963 in honor of Charles H. Strub who built and owned Santa Anita Park. In 1994 the stake&rsquo;s name was shortened to the &ldquo;Strub Stakes.&rdquo; </p>
                   <p>                     Charles H. Strub was an American dentist and entrepreneur. A fan of thoroughbred horse racing, he decided to enter the business when California passed a paramutuel wagering bill in the early 1930s.</p>
                   <p>                     Strub instituted a number of innovations for thoroughbred racing which included: finish line cameras to verify race results, electronic timing, and electronic starting gates. These improvements were later adopted by the rest of racing industry.</p>
                   <p>                     The Strub Stakes is the final leg of the Strub Series of three open races for newly turned 4-year-old horses held over several weeks during the first two months of each year, at Santa Anita Park. The Series consists of the Malibu Stakes, raced at 7 furlongs, the San Fernando Stakes, at 1 1/16 miles and the Strub Stakes.</p>
                   <p>                     Only five horses have ever won all three legs of the Strub Series: Round Table (1958), Hillsdale (1959), Ancient Title (1974), Spectacular Bid (1980) and Precisionist (1985).</p>
                   <p><br />
                   </p>
                   <h2>Strub Stakes Past Winners</h2>
                   
{include file="/home/ah/allhorse/public_html/graded_stakes/strub-stakes/winner.tpl"}

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
