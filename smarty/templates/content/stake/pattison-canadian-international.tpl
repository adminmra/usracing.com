{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Joshua Tree, Forte Dei Marmi top list of Pattison Canadian International Nominees</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Pattison Canadian International</h1>
                 <div class="news-date"> October 10, 2013</div>
                 
                   <p>Joshua Tree, defending and two-time champion of the Pattison Canadian International and Canada&rsquo;s top turf stayer, Forte Dei Marmi, head a list of 17 nominees for the 76th edition of the historic Grade 1, $1 million race.</p>
                   <p>The Pattison Canadian International, contested at 1 &frac12; miles over the E.P. Taylor Turf Course, is slated for Sunday, October 27 at Woodbine.</p>
                   <p>Joshua Tree, celebrated hero of the 2010 and 2012 renewals of the International, may return to Woodbine to defend his title. The Montjeu six-year-old has been in moderate form in 2013, but is Grade 1-placed this year and clearly a big fan of the Woodbine turf circuit. The bay was also second to Sarah Lynx in the 2011 edition of the International. </p>
                   <p>Joshua Tree finished 13th behind Treve in the famed l&rsquo;Arc de Triomphe earlier this month for trainer Ed Dunlop and owners Khalid Al Nabooda and Kamel Albahou.</p>
                   <p>Forte Dei Marmi, trained by Roger Attfield for Stella Perdomo, will look to cement his status as Canada&rsquo;s top Thoroughbred in the International.  The seven-year-old son of Selkirk has won three straight, including the Grade 1 Northern Dancer Turf event last month. The bay was a solid third to his chief rival in the race last year.</p>
                   <p>The U.S. is well represented on the nominees list with the improving Group 1 winner Lucayan, a California-based four-year-old trained by Neil Drysdale, Chad Brown&rsquo;s Group 2 winner, Hyper, Slumber, a Bill Mott trainee, who just missed in the Grade 1 Turf Classic last month, dangerous Grade 1 placed filly, Tannery and double Grade 2 stakes winner, Twilight Eclipse.</p>
                   <p>Other Euros named to the International include French-based Group 2 winning filly, Chalnetta, German Group 1 champion, Seismos and Godolphin&rsquo;s Group 2 winner, Prince Bishop.</p>
                   <p>Irish Mission, Canada&rsquo;s reigning champion turf filly and Perfect Timber, who was second beaten a neck to Forte Dei Marmi last time out in the Northern Dancer, round out the Canadian contingent of nominees.</p>
                   <p>The International is one of the Canada&rsquo;s classic horse races, boasting a roster of winners that has few rivals.  Sulamani, Chief Bearhart, Singspiel, All Along, Youth, Dahlia and the mighty Secretariat, who won this race 40 years ago this month, visited the race&rsquo;s winner&rsquo;s circle.</p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Pattison Canadian International Stakes</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
                   
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
