{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}



{literal}

<link rel="stylesheet" href="/assets/css/sbstyle.css">

<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">

<style type="text/css" >

.infoBlocks .col-md-4 .section {

    height: 250px !important;

	margin-bottom:16px;

}

@media screen and (max-width: 989px) and (min-width: 450px){

.infoBlocks .col-md-4 .section {

    height: 165px !important;

}

}

@media screen and (max-width: 800px){

.no-display{

  display:none !important;

}

}

.card_btn--default{

  text-transform:none !important;

}

@media (min-width: 1024px){
.kd {
    padding: 20px 0 0px !important;
}
}



</style>

{/literal}

 {*schema for the page*}

{literal}

  <script type="application/ld+json">

	  {

      "@context": "http://schema.org",

      "@type": "Event",

			"name": "The Grand National",

			"location": {

				"@type": "Place",

				"name": "Aintree Racecourse",

				"address": {

					"@type": "PostalAddress",

					"addressLocality": "Aintree, Merseyside",

					"addressRegion": "England"

				}

			},

			"startDate": "2021-04-10",

      "endDate" : "2021-04-10",

      "offers": {

        "@type": "Offer",

        "url": "https://www.usracing.com/promos/cash-bonus-20"

      },

      "description": "Bet on The Grand National",

      "image": "/img/grand_national_og.jpg"

    }

	</script>


{/literal}

{*end of the shema*}





{*styles for the new hero dont touch*}

{literal}

     <style>

       @media (min-width: 1024px) {

         .usrHero {

           background-image: url({/literal} {$hero_lg} {literal});

         }

       }



       @media (min-width: 481px) and (max-width: 1023px) {

         .usrHero {

           background-image: url({/literal} {$hero_md} {literal});

         }

       }



       @media (min-width: 320px) and (max-width: 480px) {

         .usrHero {

           background-image: url({/literal} {$hero_sm} {literal});

         }

       }



       @media (max-width: 479px) {

         .break-line {

           display: none;

         }

       }



       @media (min-width: 320px) and (max-width: 357px) {

         .fixed_cta {

           font-size: 13px !important;

           }

        }

     </style>

{/literal}

{*End*}



{*Hero seacion change the xml for the text and hero*}

<div class="usrHero">

    <div class="text text-xl">

        <span>{$h1_line_1}</span>

        <span>{$h2_line_2}</span>

    </div>

    <div class="text text-md copy-md">

        <span>{$h2_line_1}</span>

        <span>{$h2_line_2}</span>

        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>

    </div>

    <div class="text text-md copy-sm">

        <span>{$h2_line_1_sm}</span>

        <span>{$h2_line_2_sm}</span>

        <a class="btn btn-red" href="/signup?ref={$ref1}">{$signup_cta}</a>

    </div>

</div>

{*End*}


  {*This is the path for the countdown*}

    {include file="/home/ah/usracing.com/smarty/templates/content/stake/grand-national/block-countdown_grand_national.tpl"}

  {*end*}





  {*This is the section for the odds tables*}

  <section class="kd usr-section">

    <div class="container">

      <div class="kd_content">

        <h1 class="kd_heading">The Grand National </h1>{*This h1 is change in the xml whit the tag h1*}

		  <h3 class="kd_subheading">The Best Betting On The Biggest Jump Race In The World!</h3>

		   

{*This is the section for the primary copy*}	  

		   {include file="/home/ah/usracing.com/smarty/templates/content/stake/grand-national/salesblurb-grandnational-primary.tpl"}

       	<p align="center"><a href="/signup?ref={$ref5}" rel="nofollow" class="btn-xlrg">Bet on The Grand National</a></p>

		  

		   {*end*}

		  

	

		  

		  

        {*The path for the main copy of the page*}

   </style>

<div id="no-more-tables">



    <table class="data table table-condensed table-striped table-bordered"  border="1" cellpadding="0" cellspacing="0" width="100%">

            <tr class="no-display">

                <caption><h3>The Grand National Betting Information</h3></caption>
                <th> Location </th>

                <th> Inaugural Race </th>

                <th> Race Type </th>

                <th> Distance </th>

                <th> Track </th>

                <th> Qualification </th>

                <th> Purse</th>

                <th>Race Type</th>

            </tr>

        </thead>

        <tbody>

            <tr>

                <td data-title="Location"> Aintree Racecourse, Liverpool, United Kingdom </td>

                <td data-title=" Inaugural Race"  style="word-break: keep-all !important;"> 1839 </td>

                <td data-title=" Race Type "> Thoroughbred - Steeplechase </td>

                <td data-title=" Distance " style="word-break: keep-all !important;"> 4 miles (6.907 km) </td>

                <td data-title=" Track " style="word-break: keep-all !important;"> Turf </td>

                <td data-title=" Qualification " style="word-break: keep-all !important;"> Seven-years-old and up </td>

                <td data-title=" Purse "> £1,000,000 </td>

                <td data-title=" Race Type "> Grade III </td>

            </tr>

            </tbody>

    </table>
<br>
    </div>

<p align="center"><a href="/signup?ref={$ref6}" rel="nofollow" class="btn-xlrg">Bet on The Grand National</a></p>

	  <p> {include file='/home/ah/allhorse/public_html/misc/grand_national_1279.php'} </p>
    <p align="center"><a href="/signup?ref={$ref7}" rel="nofollow" class="btn-xlrg">Bet on The Grand National</a></p>

			  {*end*}						

	  {*Secundary copy*}
	  
      <p> {include file='/home/ah/usracing.com/smarty/templates/content/stake/grand-national/salesblurb-grandnational-secondary.tpl'} </p>
	<p align="center"><a href="/signup?ref={$ref10}" rel="nofollow" class="btn-xlrg">Bet on The Grand National</a></p>

        {*end*}



      

        <br>

        </li>

        </ul>
        

      </div>

    </div>
  

  </section>

  {*This is the section for the odds tables*}



{* Block for the testimonial  and path *}

{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}

{* End *}



{* Block for the signup bonus and path*}

{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}

{* End*}



{* Block for the block exciting  and path *}

{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}

{* end *}



{literal}

<script src="/assets/js/jquery.sortElements.js"></script>

<script src="/assets/js/sort_odds.js"></script>

<script type="text/javascript">

    addSort('o0t');

    addSort('o1t');

</script>

{/literal}



