{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Contenders for the Long Island Handicap</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Long Island Handicap</h1>
                 <div class="news-date">November 4, 2013</div>
                 
                   <p>Track: <a href="/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance: 1 1/2 miles (12 furlongs) <br />
                     Track: Turf, left-handed <br />
                     Qualification: Fillies &amp; mares, three-years-old &amp; up <br />
                     Weight: Assigned <br />
                     Purse: $150,000 <br />
                   </p>
                   <p>Formerly a Grade II event, the Long Island Handicap was down-graded to Grade III status in 2007.</p>
                   <p>No nomination fee. $2,000 to pass the entry box. The purse to be divided 60% to the winner, 20% to second, 10% to third, 5% to fourth, 3% to fifth and 2% divided equally among remaining finishers. </p>
                   <p>A trophy will be presented to the winning owner. The New York Racing Association reserves the right to transfer this race to the Main Track. In the event that this race is taken off the turf, it may be subject to downgrading by the Graded Stakes Committee.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Long Island Handicap</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2> Entries and Contenders</h2>
                   <p>AIGUE MARINE (GB) Haras du Mezeray Christophe Clement <br />
                     AND WHY NOT Helen K. Groves Michael R. Matz <br />
                     ANGEL TERRACE Augustin Stable Jonathan E. Sheppard<br />
                     ANJAZ Godolphin Stable Thomas Albertrani<br />
                     AUSUS Shadwell Stable Daniel C. Peitz <br />
                     CLARINDA Cheyenne Stables, LLC Christophe Clement <br />
                     DAYLIGHT RIDE Augustin Stable Jonathan E. Sheppard <br />
                     DREAMING OF CARA Richard Greeley Mitchell E. Friedman <br />
                     FINAL ESCROW Waterville Lake Stables Arnaud Delacour <br />
                     INIMITABLE ROMANEE Gallagher's Stud H. Graham Motion <br />
                     LEFT A MESSAGE Glen Hill Farm Thomas F. Proctor <br />
                     LISVERNANE Ninety North Racing Stable James J. Toner <br />
                     MALIBU YANKEE Family First Stable Niall Saville <br />
                     MINAKSHI (FR) Northern Bloodstock Michael R. Matz <br />
                     PREFERENTIAL (GB) Juddmonte Farms William I. Mott <br />
                     QUSHCHI (GB) Andrew Stone H. Graham Motion <br />
                     ROYAL LAHAINA James A. Riccio Todd A. Pletcher <br />
                     SHEPPARD'S PIE William L. Pape Jonathan E. Sheppard <br />
                     SILKY (IRE) Richard Santulli Alan E. Goldberg <br />
                     STRATHNAVER (GB) Andrew Stone H. Graham Motion <br />
                     TABREED (GB) China Horse Club Christophe Clement <br />
                     TANNERY (IRE) Richard Santulli Alan E. Goldberg <br />
                     TOPIC Alice Bamford Neil D. Drysdale<br />
                     VALIANT GIRL (GB) Antoinette Oppenheimer H. Graham Motion <br />
                     WHITE ROSE Jake Ballis William I. Mott <br />
                     ZERO GAME (IRE) Joann Robinson Niall Saville </p>
                   <p><br />
                   </p>
                   <h2><strong>Long Island Handicap</strong> Past Winners </h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/long-island-handicap/winners.tpl"} 

  

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
