{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Holy Bull Stakes</h1></div>
  
  
<div class="content">
    
    
            <p><strong>Cairo Prince drills another bullet ahead of Holy Bull</strong>  </p>
                 
                   <p>Everything remains on target for Nashua winner Cairo Prince, who is scheduled to make his three-year-old debut in the Grade 2, $400,000 Holy Bull at Gulfstream Park on January 25.</p>
                   <p>Working in company with four-year-old Shadwell-owned colt Qeyaas, the gray son of Pioneerof the Nile breezed five furlongs in :59 4/5 on Sunday morning at Palm Meadows. Cairo Prince posted the fastest of 29 moves at the distance on the fast main track, and easily surpassed his workmate's time of 1:00 4/5.</p>
                   <p>It was the fourth work for Cairo Prince since coming to South Florida following his runner-up finish to Honor Code in the Remsen at Aqueduct on November 30, and nearly identical to his breeze one week ago. Last Sunday, he fired a bullet :59 3/5.</p>
                   <p>&quot;I wasn't looking for :59-and-4 again, but he's just doing great,&quot; said Kiaran McLaughlin, who trains Cairo Prince for Namcook Stables, Paul Braverman, Harvey Clarke and Craig Robertson III.</p>
                   <p>&quot;He had company, but he hooked into a horse that was in front of him a little bit. It kind of made him work a little faster than we needed, but he's doing that well. No worries, no negatives; he worked great.&quot;</p>
                   <p>McLaughlin said he will give Cairo Prince one more work leading into the Holy Bull, which is run at 1 1/16 miles and is the first in a series of preps for the Grade 1, $1 million Florida Derby on March 30.</p>
                   <p>&quot;We'll work again next Saturday or Sunday, a half-mile probably on his own now, hopefully in something like :49,&quot; McLaughlin said. &quot;When you ask a horse to go a little slower and they end up going fast, that's a good sign that he's doing extremely well. He's happy, and we're happy.&quot;</p>
                   <p>McLaughlin picked up a pair of wins on Saturday's card at Gulfstream, with four-year-old Elnaawi in the 7TH race, a 1 1/16-mile optional claiming allowance on dirt, and three-year-old filly Macaroon in the 8TH, a 7 1/2-furlong turf sprint. </p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Holy Bull Stakes Race Information </h2>
                   <p>&nbsp;</p>
                   <p>Location:	Gulfstream Park, Hallandale Beach, Florida<br />
                     Inaugurated: 	1972<br />
                     Race type: 	Thoroughbred - Flat racing<br />
                     Distance: 	1 mile (8 furlongs)<br />
                     Track: 	Dirt, left-handed<br />
                     Qualification: 	Three-year-olds<br />
                     Weight: 	Assigned<br />
                     Purse: 	$400,000<br />
                     Grade: II<br />
                   </p>
                   <p><strong>Race Type:</strong> <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> </p>
                   <p><strong>Road to the Roses Points: </strong>1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point</p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Bet on Holy Bull Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Holy Bull Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/holy-bull-stakes/winners.tpl"} 

                   
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
