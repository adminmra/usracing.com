{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Special Races</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/royal-ascot" title="">ASCOT</a></li>
<li><a href="/breeders-cup" title="">BREEDERS&#039; CUP</a></li>
<li><a href="/dubai-world-cup" title="">DUBAI WORLD CUP</a></li>
<li><a href="/hong-kong-cup" title="">HONG KONG CUP</a></li>
<li><a href="/kentucky-oaks" title="">KENTUCKY OAKS</a></li>
<li><a href="/melbourne-cup" title="">MELBOURNE CUP</a></li>
<li class="leaf last active-trail"><a href="/prix-de-larc-de-triomphe" title="" class="active">PRIX DE L&#039;ARC DE TRIOMPHE</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9"><!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Prix de l&#039;Arc de Triomphe - Vive la France!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Each year, thousands of racing fans flock to Longchamp from all over the world to witness what is generally considered to be the ultimate test for thoroughbreds.</p>
<p>The PRIX DE L'ARC DE TRIOMPHE visitor's book is a veritable who's who: Rothschild, Boussac, Plesch, the Aga Khan, Mme Cino Del Duca, Wildenstein, Mellon, Robert Sangster, Wertheimer and Mrs Alec Head are just some of the names that grace its pages.</p>
<p>Down the years, the ARC has been won by a host of truly great horses: Ribot, Sea Bird, Allez France, Peintre Célèbre and Dancing Brave to name but a few.</p>
<p>Aside from providing a magnificent sporting spectacle, the PRIX DE L'ARC DE TRIOMPHE also plays a major role in dictating bloodlines. An Arc winner is dearly sought after at stud, and his progeny will go on to improve thoroughbred performance from generation to generation. In terms of fostering future champions, the PRIX DE L'ARC DE TRIOMPHE has proven to be in a class of its own.</p>
<p>The LUCIEN BARRIERE PRIX DE L'ARC DE TRIOMPHE also offers more prize money than any other European race, more even than the Epsom Derby, with total spoils of FFR 10.5m (shared between the first five), 6 million of which goes to the winner.On 18 August 1854, the Paris city council authorized the town Prefect to acquire property in the Bois de Boulogne where a racetrack could be built in the city's name. By the terms of the lease, signed in June 1856, the city of Paris granted the French racing authorities 60 hectares at Longchamp (from 1st July 1856 to 30 June 1906).</p>
<p><strong><br />The Hippodrome of Longchamp</strong></p>
<p>On 15 December 1856, the State accorded "Société d'Encouragement" the right to run the "Autumn" race series hitherto held at the Champs-de-Mars under the management of Haras.</p>
<p>The first ever race was run at Longchamp on Sunday 27 April 1857 in front of a massive crowd. The Emperor Napoleon III and his wife Eugénie were present, having sailed down the Seine on their private yacht to watch the third race. Until 1930, many Parisians came to the track down the river on steamboats and various other vessels, the trip taking around an hour to the Pont de Suresnes.</p>
<p>The royal couple joined Prince Jerome Bonaparte and his son Prince Napoleon in the Royal Enclosure alongside the Prince of Nassau, Prince Murat and the Duke of Morny, an avid racegoer. Non-aristocratic members of the upper classes were not permitted into the Royal enclosure and had to be content with watching from their barouche carriages on the lawn.</p>
<p>Charabancs, Victoria carriages and paddle boats all brought Parisians to Longchamp. And they came as much for a day out as from curiosity for this new form of entertainment.</p>
<p>The card for the opening day contained five races. The first horse ever to cross the finishing line was Eclaireur, in the black and red colors of Auguste Lupin. A short length behind was Miss Gladiator, destined, a few years later, to become one of the most famous brood mares in French racing history when she foaled the celebrated Gladiator, whose bronze statue still stands at the main entrance to the racetrack.</p>
<p>In the late Spring of 1914, Longchamp opened its doors to the Grand Prix de Paris - at that time the world's richest race, with prize money totaling FF 300,000. At the beginning of August, however, all racecourses were requisitioned for the war effort. Racing officially began again on 5 May 1919 at Maisons Laffitte, then again at Longchamp on the 8th.</p>
<p>Since it was first run on 3 October 1920, the Prix de l'Arc de Triomphe has become the world's foremost race for three-year-olds and above over the classic mile and a half distance. It takes place on the first Sunday in October. The "Arc", as it is known, is every bit as prestigious as the English Derby at Epsom or the Kentucky Derby at Churchill Downs, and equally steeped in tradition, style and glamour.</p>
<p>&nbsp;</p>
<p><strong>HISTORY</strong></p>
<p>First run on 3 October 1920, the Prix de l'Arc de Triomphe was a "spin-off" from the Grand Prix de Paris.</p>
<p>The French racing authorities initially only wanted to develop racing for thoroughbreds born and bred in France. The reputation of French horses, however, quickly spread beyond its borders, and it was eventually decided to open the Grand Prix de Paris to three-year-olds from all over the world.</p>
<p>During a committee meeting of 11 January 1862, it was decided that FF 100,000 would be awarded to the winning horse: a staggering amount at the time.</p>
<p>With the help of the Duke of Morny, the city of Paris agreed to contribute FF 50,000, and five different railway companies FF10,000 apiece. The Grand Prix de Paris was first run in 1863 in accordance with the following conditions:</p>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<td class="bodyregular">
<ul>
<li>A race for colts and fillies from any country.<br />&nbsp; </li>
<li>To be run on a Sunday.<br />&nbsp; </li>
<li>Over 3,000 m.</li>
</ul>
</td>
</tr>
</tbody>
</table>
<p>The first running of the Grand Prix de Paris on 31 May 1863 with a success that exceeded all expectations.</p>
<p>Thirty years later, in 1893, the Grand Prix de Paris became the Prix du Conseil Municipal, open to three-year-olds and older. Thanks to the huge amount of prize money, the race became extremely successful with foreign owners. However, as it was a Handicap race, the Prix du Conseil Municipal could never become a Classic and was therefore never considered important in breeding terms.</p>
<p>At a committee meeting on 24 January 1920, attended by the British ambassador, the Count of Derby, the French racing authorities decided to create a highly prestigious international race that would be complementary to the Grand Prix de Paris.</p>
<p>And so the Prix de l'Arc de Triomphe came into being, its title a tribute to the French soldiers that served in the Great War. The name was preferred to another contender, the "Prix de la Victoire".</p>
<p>The "Arc" was created just after the First World War, which had put paid to so much of France's best bloodstock. France Galop's decision to found a classic race was thus both courageous and optimistic. Since that time, the Arc's reputation has never ceased to grow, and today the race stands as a glowing tribute to the many men and women who have worked tirelessly to promote flat racing in France.</p>
<p>The Prix de l'Arc de Triomphe was first run at Longchamp racecourse on Sunday 3 October 1920. The first horse across the line was Comrade, who picked up FF150,000; second was King's Cross, winning FF18,000, and Pleurs was third, winning FF10,000.</p>
<p>The Arc has now been run 78 times, always on the first Sunday in October (with the exception of 1939 and 1940).</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
 <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    