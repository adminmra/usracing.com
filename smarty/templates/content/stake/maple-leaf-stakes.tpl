{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Maple Leaf Stakes Receives Graded Status Once Again</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Maple Leaf Stakes</h1>
                 <div class="news-date">September 30, 2013</div>
                 <p>The Jockey Club of Canada&rsquo;s Graded Stakes Committee held its annual review of the Graded Stakes in Canada, and has announced there will be a total of 43 Graded Stakes in 2013, up from 42 in 2012. </p>
                   <p>Woodbine Racetrack in Ontario will once again host Canada&rsquo;s five Grade 1 events in 2013.</p>
                   <p>Three Grade 3 races at Woodbine have received an upgrade to Grade 2 status for 2013.  Three and up sprints, the Kennedy Road and the Bessarabian Stakes for fillies and mares, as well as the Eclipse Stakes, a distance race for older horses, have each earned an upgrade for 2013.  The Hendrie Stakes for fillies and mares three and up, which was upgraded to G2 status in 2012 has been downgraded back to a G3 race for 2013.</p>
                   <p>The Maple Leaf Stakes for fillies and mares three and up has once again been elevated to Grade 3 status in 2013 after being downgraded to Listed status in 2007.</p>
                   <p>The Jockey Club of Canada Graded Stakes Committee evaluates and reviews the Graded Stakes in Canada annually and determines whether races should be upgraded, downgraded or remain the same. The Committee base their decisions on the cumulative NARC ratings for the previous years along with the new Race Quality Score (RQS) figures introduced this year by the North American International Catalogue Standards Committee (NAICSC).</p>
                   <p>Graded Stakes in Canada must carry the following minimum purses: $250,000 for Grade 1; $150,000 for Grade 2 and $100,000 for Grade 3.</p>
                   <p>Beginning this year, the Graded Stakes Committee was also responsible for evaluating all Listed Stakes in Canada using the same evaluation process used for the Graded Stakes.</p>
                   <p>Beginning January 1, 2014 NAICSC will implement an additional quality control requirement for races in North America eligible for non-Listed black-type in SITA catalogues (those compliant with guidelines required by the Society of International Thoroughbred Auctioneers).</p>
                   <p>&nbsp;</p>
                   <h2>Bet on Maple Leaf Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>The Maple Leaf Stakes is a Canadian Thoroughbred horse race run annually at Woodbine Racetrack in Toronto, Ontario. Run during the first part of November, the ungraded stakes is open to fillies aged three or older. Raced over a distance of one and one-quarter miles on Polytrack synthetic dirt, it currently offers a purse of $175,000.<br />
</p>
                   <p>Location: <a href="/racetrack/woodbine">Woodbine</a> Racetrack<br />
                     Distance: 1 1/4 miles (10 furlongs) <br />
                     Track: Polytrack, left-handed <br />
                     Qualification: Fillies &amp; Mares, 3-Years-Old &amp; Up <br />
                     Weight: Allowances <br />
                     Purse: $175,000 </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/maple-leaf-stakes/winners.tpl"} 


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
