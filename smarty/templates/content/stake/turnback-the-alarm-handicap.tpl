{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Street Secret tunes up for Turnback the Alarm</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Turnback The Alarm Handicap</h1>
                 <div class="news-date">September 17, 2013</div>
                 <p>When Street Secret came to Chad Brown&rsquo;s barn, it looked like she would be another one to add to his deep roster of potential turf stakes fillies and mares.</p>
                   <p>But Street Secret has proven to be perhaps a shade better on dirt, and with an eye toward next month&rsquo;s Turnback the Alarm, Brown will prep Street Secret in Thursday&rsquo;s $100,000 Parlo Stakes at Belmont Park.</p>
                   <p>After a win and a third in two turf starts by Street Secret, Brown tried her on dirt in the Open Mind Stakes at Belmont in June. Street Secret seized the initiative from expected pacesetter Mischief Maker and scored a 1 3/4-length front-running victory. In her most recent start, the Alada Stakes going 1 1/8 miles in the slop at Saratoga, Street Secret finished second to Flashy American, who came back to win the Locust Grove at Churchill Downs in her next start. Lady Cohiba, who finished third in the Alada, won the off-the-turf Glens Falls Handicap in her next start, a race from which Street Secret scratched.</p>
                   <p>&nbsp;</p>
                   <p><br />
                   </p>
                   <h2><strong>Bet on Turn Back the Alarm Handicap</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>                     Location: 	Aqueduct Racetrack<br />
                     Distance: 	1 1/2 miles (9 furlongs)<br />
                     Track: 		Dirt, left-handed<br />
                     Qualification: 	Fillies &amp; Mares, three-years-old &amp; up<br />
                     Weight: 	Assigned<br />
                     Purse: 		$100,000+</p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/turnback-the-alarm-handicap/winners.tpl"}
          

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
