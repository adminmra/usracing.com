{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Broken Dreams won  Senator Ken Maddy Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Senator Ken Maddy Stakes</h1>
                 <div class="news-date"></div>
                 <p>Location: 	<a href="/santa-anita-park">Santa Anita Park</a><br />
                     Distance: 	6.5 furlongs<br />
                     Track: 		Turf<br />
                     Qualification: 	Fillies &amp; Mares, three-years-old &amp; up<br />
                     Weight: 	Gr. III<br />
                     Purse: 		$100,000</p>
                   <p>Entry -- By subscription of $50 each if made on or before Sunday, October 20, 2013 or by supplementary nomination of $2,000 by time of entry. All horses to pay $500 to pass the entry box and $1,000 to start with $100,000 guaranteed, of which $60,000 to first, $20,000 to second, $12,000 to third, $6,000 to fourth, $2,000 to fifth. Three-year-olds: 120 lbs. Older: 123 lbs. Non-winners of a Graded 
                     Stakes since April 19, allowed 3 lbs. Non-winners of a Graded Stakes since October 19, 2013, 5 lbs. Highweights preferred, followed by Graded Stake 
                     Winners within 18 months of race, followed by Graded Stakes placed horses within 18 months of race, followed by highest earners within 12 months of race.
                     Starters to be named through the entry box by the usual time of closing.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Senator Ken Maddy Stakes</strong></h2>
                   <p>US Racing features race analysis, interviews, free   handicapping tips and stories on the trainers, jockeys, owners and   horses that make up the colorful sport of horse racing.  US Racing   features the best tracks in the United States, including Churchill   Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.    Internationally, US Racing offers content from Australia, Dubai, Great   Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <h2>&nbsp;</h2>
                   <h2>Senator Ken Maddy Stakes Video</h2>
                   <p>Oct 20, 2012 -- Garrett Gomez and Tom Proctor after Broken Dreams wins the Senator Ken Maddy Stakes (Gr. III)</p>
                   <p><iframe width="100%" height="360" src="//www.youtube.com/embed/CcLScP4ufFY?feature=player_detailpage" frameborder="0" allowfullscreen></iframe><br />
                   </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>

{include file="/home/ah/allhorse/public_html/graded_stakes/senator-ken-maddy-stakes/winners.tpl"}
           

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
