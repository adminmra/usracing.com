{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                                
                                      
          
<div class="headline"><h1>Monrovia Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



                   <p><strong>Location:</strong> Santa Anita Park, Arcadia, California, United States <br />
                     <strong>Inaugurated:</strong> 1967 <br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing <br />
                     <strong>Distance:</strong> 6 1/2 furlongs<br />
                     <strong>Track:</strong> Turf, left-handed <br />
                     <strong>Qualification: </strong>Four-years-old &amp; up <br />
                     <strong>Weight:</strong> Assigned <br />
                     <strong>Purse:</strong> $200,000 <br />
                     <strong>Grade:</strong> III <br />
                   </p>
                   <p>The <strong>Monrovia Stakes</strong> is named for the local city near the track. Monrovia is a city located in the foothills of the San Gabriel Mountains in the San Gabriel Valley of Los Angeles County, California, United States. Monrovia has been used for filming TV shows, movies and commercials. Monrovia is the fourth oldest general law city in Los Angeles County and the L.A. Basin (after Los Angeles, Santa Monica, and Pasadena). Incorporated in 1887, Monrovia has grown from a sparse community of orange ranches to a residential community of 37,000.</p>
                   <p><br />
                     <strong>Monrovia Trivia:</strong> The house seen in the 1986 horror-comedy cult film House (1986 film) is located at 329 Melrose Avenue in Monrovia</p>
             
                   <p>&nbsp;</p>
                   <h2>Bet on Monrovia Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                   
                   <h2>Monrovia Stakes 2014 Contenders</h2>
                   <p>                   
                   <p>Here is a list of Contenders for the 47th Running Of at Santa Anita Park, with $200,000 Guaranteed<br />
                   <p>Appealing (IRE)<br />
                     Blues and Silvers<br />
                     Camryn Kate<br />
                     Charlie Em (GB)<br />
                     Golden Production<br />
                     Judy in Disguise (GB)<br />
                     Kindle<br />
                     Kinz Funky Monkey<br />
                     Lady Ten<br />
                     Miss Lucky Sevens<br />
                     Ondine<br />
                     Pontchatrain<br />
                     Purim's Dancer<br />
                     Sky High Gal<br />
                     Social Bug<br />
                     Teddy's Promise<br />
                     Ultrasonic<br />
                     Unusual Hottie<br />
                     Winding Way</p>
                   <p>&nbsp;</p>
                   <h2>Monrovia Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/monrovia-stakes/winners.tpl"} 


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
