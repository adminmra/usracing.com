{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                        
          
<div class="headline"><h1>Travers Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{*include file='/home/ah/allhorse/public_html/graded_stakes/travers_stakes/slider.tpl'*}

<h2>Travers Stakes Betting</h2>
<ul>
<li><strong>Grade:</strong> I for 3-year-old thoroughbreds and fillies</li>
<li><strong>Distance:</strong> 1 mile</li>
<li><strong>Track:</strong> Dirt</li>
<li><strong>Purse:</strong> $1,600,000</li>
</ul>

<p>The <strong>Travers Stakes</strong>, held yearly at the <a title="Saratoga Race Course" href="/saratoga"><strong>Saratoga Race Course</strong></a> in Saratoga Springs, New York, is America's oldest stakes race in continuous existence. First run in 1864, it was named so in honor of William Travers, who had the Saratoga race track built the year before, along with Leonard Jerome and John Hunter.</p>
<p>The race was inaugurated in 1864 at Saratoga Race Course and has been run in all but five years since, and with all but three runnings at Saratoga. Due to war-time restrictions on travel, the 1943, 1944 and 1945 Saratoga meetings were conducted at Belmont Park.</p>
<p>Travers was a prominent businessman in New York City for most of his life, and for many years, bred and owned some of the period's better horses.</p>
<p>The inaugural meeting at Saratoga was short and sweet. The Travers Stakes was the highlight of the meeting, being named, of course, for one of the track's founders and its first president.</p>
<p>That Travers and Hunter co-owned the first winner, a colt named Kentucky, of the race named for Travers is not unusual during those times. In the 1800s, the men who built racetracks were often the ones who bred and owned many of the best horses at these meetings. They built tracks to showcase their horses more than to make money.</p>
<p>The field for the Travers Stakes is limited to three-year-olds, Colts and geldings carrying 126 pounds (57 kg) and fillies carrying 123 pounds (56 kg). The first prize is $1,250,000. However, with Triple Crown Winner American Pharoah participating in his run up to the 2015 Breeders' Cup Classic in Keeneland Kentucky, the purse has been increased to $1,600,000!  The race also known as the Midsummer Derby is the highlight of the summer race meeting at Saratoga, just as the <a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a> is the highlight of the spring meeting at <a title="Belmont Park" href="/belmont-park">Belmont Park</a>.</p>
<p><span><strong><span>Where can I bet on the Travers Stakes?</span></strong><br /><br />

So, you want to bet on the American Pharoah?  Keen Ice?  Frosted? You want to to bet on the <a title="Travers Stakes" href="/travers-stakes">Travers Stakes</a> online? BUSR offers Futures Wagers, Match Races and track betting options for the Travers Stakes. Bet from your mobile phone or your desktop computer and enjoy the races!</p>

<p>Well.  Okay.  Breathe.  It's okay.  He is STILL A GREAT HORSE.  Unfortunately for the fans of American Pharoah, he was not able to win the Travers Stakes.  In the end, he just didn't seem to have the strength although Victor and him tried with guts.  16-1 longshot Keen Ice, ridden by Javier Castellano and trained by Dale Romans, put the hammer down winning by three quarters of a length over American Pharoah and Frosted in a time of 2:01.57 and returned $34, $6.50 and $3.80</p>
<p>The question on everybody's mind is whether American Pharoah will retire or go for the Breeder'Cup Classic (<a title="Breeders' Cup Classic Odds" href="/breeders-cup/odds">Breeders' Cup Odds</a>).  No decision has been made yet but we expect to know within 1-2 weeks.</p>

{*include file='/home/ah/allhorse/public_html/graded_stakes/travers_stakes/odds.php'*}


{include file='/home/ah/allhorse/public_html/misc/travers_stakes_1451_manually.php'}
<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>

{include file='/home/ah/allhorse/public_html/graded_stakes/travers_stakes/video.tpl'}

        
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/travers_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
