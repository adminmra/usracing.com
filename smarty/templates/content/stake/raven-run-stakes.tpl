{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Keeneland's upcoming Lexus Raven Run Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Raven Run Stakes</h1>
                 <div class="news-date">October 4, 2013 </div>
                 <p>The 17-day meet at Keeneland opens Friday, Oct. 4 and runs through Saturday, Oct. 26. Keeneland has traditionally offered race fans a unique experience in addition to a stellar class of horses and stakes races and this year, they&rsquo;ve added a few options to keep fans entertained the whole day through.</p>
                   <p>The meet starts with a bang as Keeneland holds Fall Stars Weekend this Friday through Sunday. Nine of the 16 stakes to be held during the fall meet will run this weekend, seven of which are part of the Breeders&rsquo; Cup Win &amp; You&rsquo;re In challenge series. Winners of the challenge series races win an automatic berth in the corresponding Breeders' Cup race, as well as a travel stipend.</p>
                   <p>Some of the nation&rsquo;s top horses are expected to participate in this weekend's races, including My Conquestadory, a filly who comes in off a win against males in the Summer Stakes (G2), who will run in the $400,000 Darley Alcibiades (G1) on Friday. Reigning Horse of the Year Wise Dan is entered in the $750,000 Shadwell Turf Mile (G1) on Saturday. The Central Bank Ashland (G1) winner Emollient is entered in Sunday&rsquo;s $500,000 Juddmonte Spinster (G1).</p>
                   <p>Friday&rsquo;s stakes action consists of the $200,000 Stoll Keenon Ogden Phoenix Stakes (G3) for three-year-olds and up going six furlongs and the $400,000 Darley Alcibiades (G1) for fillies two years old and up going 1 1/16 miles.</p>
                   <p>Saturday&rsquo;s stakes include the $150,000 Woodford Stakes (G3) for three-year-olds at a 5 &frac12; furlongs; the $200,000 Thoroughbred Club of America Stakes for fillies and mares, three years old and up going six furlongs; the $400,000 First Lady Stakes (G1)  for fillies and mares, three years old and up going one mile on the turf; the $400,000 Dixiana Breeders&rsquo; Futurity (G1) for two-year-olds going 1 1/16 miles; and the $750,000 Shadwell Turf Mile Stakes (G1) for three-year-olds and up going a mile on turf.</p>
                   <p>Sunday&rsquo;s stakes action includes the $150,000 Bourbon Stakes (G3) for two-year-olds going 1 1/16 miles on turf and the $500,000 Juddmonte Spinster Stakes (G1) for fillies and mares, three years old and up going 1 1/8 miles. </p>
                   <p>Other stakes throughout the meet include the $150,000 JP Morgan Chase Jessamine (G3) (Oct. 9), the $100,000 Buffalo Trace Franklin County (L) (Oct. 11), the $100,000 Sycamore (G3) (Oct. 17), the $150,000 Pin Oak Valley View (G3) (Oct. 18), the $250,000 Lexus Raven Run (G2) (Oct. 19), the $125,000 Rood &amp; Riddle Dowager (L) (Oct. 20), and the $200,000 Hagyard Fayette (G2) (Oct. 26). </p>
                   <p>Ciao Bella Luna, winner of this spring&rsquo;s Beaumont (G2), worked a best of 16 five furlongs in :59.40 Monday morning in preparation for a Saturday start in the $250,000 Lexus Raven (G2) for 3-year-old fillies going seven furlongs on the main track. Ciao Bella Luna will attempt to join Gypsy Robin, who last year became the first filly to sweep the Beaumont and Lexus Raven Run. &hellip;</p>
                   <p>Richlyn Farm's Lighthouse Bay, winner of the Prioress (G1) at Saratoga in July, headlines the prospective field for the 15th running of the $250,000 Lexus Raven Run (G2) at seven furlongs for 3-year-old fillies on the main track scheduled for Saturday, Oct. 19.</p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Raven Run Stakes</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
