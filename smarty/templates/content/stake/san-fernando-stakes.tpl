
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>San Fernando Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>&nbsp;</p>
                 <p><strong>Location: </strong>Santa Anita Park, Arcadia, California, United States <br />
                     <strong>Inaugurated: </strong>1952 <br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing <br />
                     <strong>Distance: </strong>11/16 miles (8.5 furlongs) <br />
                     <strong>Track:</strong> Dirt, left-handed <br />
                     <strong>Qualification: </strong>Four-years-old <br />
                     <strong>Weight: </strong>Assigned <br />
                     <strong>Purse: </strong>$150,000 <br />
                     <strong>Grade:</strong> II </p>
                   <p>&nbsp;</p>
                   <h2>Santa Anita Archives: Zignew Wins the San Fernando Stakes</h2>
                   
                    <p>&nbsp;</p>
                    <p class="style1">January 17, 1994</p>
                    <p>A grandson of <a href="/famous-horses/northern-dancer">Northern Dancer</a> and a son of Danzig, Zignew is a 4-year-old colt whose races have never been as good as his breeding.</p>
                   <p>Northern Dancer won the Kentucky Derby in 1964; 29 years later, Sea Hero, a grandson of Danzig, did the same. But Zignew spent his 3-year-old season unable to bridge the competitive gap between allowance company and stakes races.</p>
                   <p>Trainer Speedy Smithwick moved the colt from dirt to grass and back to dirt while trying to find the answer. Smithwick's patience might pay off. Waiting until his 12th start before scoring his first stakes victory, Zignew scored a 21-1 upset on dirt Sunday at Santa Anita in the $225,400 San Fernando Stakes. </p>
                   <p>Zignew, bred and owned by Jack Kent Cooke and ridden by Chris McCarron, earned $135,400, which was $22,875 more than his purses for 11 previous starts. He had won four times, but was not better than fourth in three stakes starts. Sunday marked Zignew's first stakes race since an eighth-place finish in the Ascot Handicap at Bay Meadows on Sept. 25.</p>
                   <p>Zignew, who paid $44.80 after running 1 1/8 miles in 1:47 4/5, came into the San Fernando off an allowance victory at Hollywood Park a month ago. He carried 116 pounds Sunday, four less than the top-weighted Diazo.</p>
                   <p>&quot;He's getting better with age,&quot; said Smithwick, who comes from a family of Hall of Fame steeplechase horsemen. &quot;I think he's matured, he's learned how to rate and he didn't stall when he made the lead this time. He's out of a Prince John mare (Newfoundland) who's had nothing but nice horses, and they've all put it together late in life, too.&quot;</p>
                   <p>Zignew broke from the outside post, and with Bat Eclat, Diazo and Del Mar Dennis doing the fastest early running, McCarron was able to drop his mount into fourth place after a half-mile.</p>
                   <p>At the top of the stretch, Bat Eclat and Diazo dropped out of contention. Del Mar Dennis had the lead with an eighth of mile to go, and although McCarron could have gone to the front then, he was reluctant to do so.</p>
                   <p>&quot;I didn't want to open up three lengths or so leaving the eighth pole,&quot; McCarron said. &quot;Because then he might stick his toes into the ground and lose momentum.&quot;</p>
                   <p>McCarron said that after he heard the Santa Anita announcer, Trevor Denman, say that Pleasant Tango was moving, he asked Zignew to dig in. McCarron, who rode Bertrando to victory last year, became only the third back-to-back winner of the stake, which was first run in 1952. Bill Shoemaker rode the winners in 1957-58 and Eddie Belmonte had a three-race streak that started in 1969.</p>
                   <p>McCarron rode Zignew early in his career and has been aboard for the horse's last three races.<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2>Bet on San Fernando Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and   stories on the trainers, jockeys, owners and horses that make up the colorful   sport of horse racing. US Racing features the best tracks in the United States,   including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.   Internationally, US Racing offers content from Australia, Dubai, Great Britain,   Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing   and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>San Fernando Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/san-fernando-stakes/winners.tpl"}


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
