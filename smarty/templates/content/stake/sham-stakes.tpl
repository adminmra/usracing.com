{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Kentucky Derby Prep Race Sham Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Sham Stakes</h1>
                 <div class="news-date"></div>
                 <p><strong>Location: </strong>Santa Anita Park, Arcadia, California, United States<br />
                     <strong>Inaugurated:</strong> 2001<br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing<br />
                     <strong>Distance:</strong> 1 mile (8 furlongs)<br />
                     <strong>Track: </strong>Dirt, left-handed<br />
                     <strong>Qualification:</strong> Three-year-olds<br />
                     <strong>Weight:</strong> Assigned<br />
                     <strong>Purse: </strong>$100,000<br />
                   </p>
                   <p><strong>Race Type:</strong> <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> </p>
                   <p><strong>Road to the Roses Points: </strong>1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point</p>
                   <p><br />
                   </p>
                   <h2>Bet on Sham Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                   </p>
                   <h2>Sham Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/sham-stakes/winners.tpl"}

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
