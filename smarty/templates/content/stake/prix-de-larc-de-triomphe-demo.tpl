{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{assign  var="stakesname" value="Prix de l'Arc de Triomphe"} 
{assign  var="stakespath" value="arc-de-triomphe"} 

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/specialraces.tpl'}         

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<div class="container-fluid">
		<a href="/signup?ref=prix-de-larc-de-triomphe"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/prix-de-larc-de-triomphe/2017-prix-de-larc-triomphe-betting.jpg" alt="Bet on the 2018 Prix De L'arc De Triomphe"></a>
		<a href="/signup?ref=prix-de-larc-de-triomphe"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/prix-de-larc-de-triomphe/2017-prix-de-larc-triomphe-betting-usracing.jpg" alt="Bet on the 2018 Prix De L'arc De Triomphe">  </p> </a>	
</div>        

<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>Bet on Prix de l&#039;Arc de Triomphe </h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->



<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

<h2>Bet on {$stakesname} </h2>

{*
{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/specs.tpl"}
*}

<p>Each year, thousands of racing fans flock to Longchamp from all over the world to witness what is generally considered to be the ultimate test for thoroughbreds.</p>
  <h2>{$stakesname} Odds</h2>
  {*include file='/home/ah/allhorse/public_html/misc/odds_arc_de_triomphe_xml.php'*}
   {include file='/home/ah/allhorse/public_html/misc/odds_arc_de_triomphe.php'}
<p>For the latest {$stakesname} odds, please login into BUSR and check the racebook.</p>
<!-- <p align="center"><a href="https://www.usracing.com/signup?ref={$stakespath}" class="btn-xlrg ">Bet on Prix de l&#039;Arc de Triomphe</a></p> -->
<p align="center"><a href="https://www.usracing.com/signup?ref={$stakespath}" class="btn-xlrg ">Bet Now</a></p>

<hr>
<p>The Prix de l'Arc de Triomphe is a Group 1 turf  race in France (similar to a Grade 1 race in America)for three-year-old thoroughbreds  and older. It is run at Longchamp over a distance of 2,400 metres (about 1 &frac12; miles) and istypically contested on the first Sunday in October.<br>
  Often referred to as the &quot;Arc&quot;, the  Prix de l'Arc de Triompheis one of the most prestigious horse races in the  world, with a list of winners that includes Ribot, Alleged and Treve (all of  whom won it twice), as well as Topyo (who bested a record 29 rivals), Mill  Reef, Allez France, Dancing Brave, Sakhee, Sea the Stars and many others.</p>
<p>After years of being billed the &ldquo;richest turf race on the planet,&rdquo; with a purse  of 5 million Euros (about $5.8 million), the Arc is now the second-richest turf  race on the planet, behind The Everest - a 1,200-meter race (approximately six  furlongs) that debuted at Royal Randwick racecourse in Australia in 2017.</p>
<p>Aside from providing a magnificent sporting  spectacle, the Prix de l'Arc de Triomphealso plays a major role in dictating  bloodlines. Male Arc winners are dearly sought after for stud duty, and their  progeny have gone on to improve thoroughbred performance from generation to  generation. In terms of fostering future champions, the Prix de l'Arc de Triomphehas  proven to be in a class of its own.</p>
<p><strong><u>Longchamp Racecourse</u></strong></p>
<p>The first-ever race was run at Longchamp on  April 27, 1857, in front of a massive crowd. The Emperor Napoleon III and his  wife Eugénie were present, having sailed down the Seine on their private yacht  to watch the third race (until 1930, many Parisians came to the track down the  river on steamboats and various other vessels).</p>
<p>The royal couple joined Prince Jerome  Bonaparte and his son Prince Napoleon in the Royal Enclosure alongside the  Prince of Nassau, Prince Murat and the Duke of Morny, an avid racegoer.  Non-aristocratic members of the upper classes were not permitted into the Royal  enclosure and had to be content with watching from their barouche carriages on  the lawn.</p>
<p> Charabancs, Victoria carriages and paddle  boats all brought Parisians to Longchamp. And they came as much for a day out  as from curiosity for this new form of entertainment.</p>
<p> The card on the opening day of Longchamp  consisted of five races. The first horse ever to cross the finishing line was  Eclaireur, in the black and red colors of AugusteLupin. A short length behind  was Miss Gladiator, destined, a few years later, to become one of the most  famous broodmares in French racing history when she foaled the celebrated  Gladiator, whose bronze statue still stands at the main entrance to the  racetrack.</p>
 <p> In the late spring of 1914, Longchamp introduced  the Grand Prix de Paris - at the time, the world's richest race, with prize  money totaling 300,000 French francs (roughly $1.4 million today). At the  beginning of August, however, all racecourses were requisitioned for the war  effort. Racing officially began again on May 5, 1919 at MaisonsLaffitte,  followed by Longchamp again on May 8, 1919.<br>
</p>
<p><strong><u>Arc History</u></strong></p>
<p>First run on Oct. 3, 1920, the Prix de l'Arc  de Triomphe was a spinoffof the Grand Prix de Paris, its title a tribute to the  French soldiers that served in the Great War. The name was preferred to another  contender, the &quot;Prix de la Victoire&quot;.</p>
<p>The first horse to win the Arc was Comrade,  who picked up FF150,000; second was King's Cross, winning FF18,000, and Pleurs  was third, winning FF10,000.</p>
        
        


            
            
 <!--<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p> -->
<p>{include file='/home/ah/allhorse/public_html/usracing/odds/marketing_inc.tpl'} </p>
                     
  
  
<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->




<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

      
    
