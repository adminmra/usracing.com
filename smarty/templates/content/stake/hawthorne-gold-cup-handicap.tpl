{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Hawthorne Gold Cup moved to after Thanksgiving</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date"><span class="title-custom">Hawthorne Gold Cup</span> Handicap</h1>
                 <div class="news-date">October 3, 2013</div>
                 <p>Anticipation for the fall Thoroughbred meet at Hawthorne is high as racing action kicks off.</p>
                   <p>Full fields will be the focus this fall for new Director of Racing and Racing Secretary Allan Plever. Plever's condition book has been well received as stall applications exceeded the available stalls for the fall meet. Large stables from Roger Brueggemann, Frank Kirby, Chris Dorris and Chris Block return to Hawthorne from a summer at Arlington while Clay Brinson, Joel Berndt, Nevada Litfin and Randy Rarick come to Hawthorne from Canterbury. A 35-year racing veteran, Plever has worked at tracks throughout the country in numerous management positions.</p>
                   <p>Plever will also oversee a stakes schedule that provides stakes actions on most weekends throughout the meet. The main changes come in the graded stakes for the fall meet. Opening weekend will provide evening stakes excitement with the 49th running of the Grade 3, $200,000 Hawthorne Derby on the turf. The Derby will be joined by the $75,000 Indian Maid Handicap on turf for fillies and mares. </p>
                   <p>The other major change comes with the move of the Grade 2, $350,000 <strong>Hawthorne Gold Cup</strong>. Traditionally run opening weekend, the Gold Cup has been moved to the Saturday following Thanksgiving on November 30.</p>
                   <p>Following the pattern set in 2012, the Illinois-bred stakes races will again be run four weekends throughout the meet. The $75,000 Robert F. Carey Memorial will be contested on the grass on October 12. The stakes schedule closes with Illinois-bred juveniles going two turns in the Pat Whitworth Illinois Debutante on December 7 and the Jim Edgar Illinois Futurity on December 14.</p>
                   <p>Hawthorne's main track is in excellent shape for the meet following a surface refresher and the turf course has never looked better. The Illinois Thoroughbred Horsemen's Association (ITHA) examined the main track and is very pleased with it. With the help of the ITHA, the full backstretch is in great shape as barns have been power washed, roads paved and trees trimmed and cleared.</p>
                   <p>&quot;We're very excited about this fall meet,&quot; Hawthorne President and General Manager Tim Carey said. &quot;We had to deal with everything under the sun last fall from poor weather to the virus that nearly crippled us. We have been proactive with a full cleaning of the backstretch this summer and are pleased to be working in hand with the ITHA. </p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Hawthorne Gold Cup Handicap</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
