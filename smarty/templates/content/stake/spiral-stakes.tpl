{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1>Spiral Stakes</h1>
</div>
  
  
<div class="content">
    
    
            <p>The Horseshoe Casino Cincinnati <a href="/spiral-stakes">Spiral Stakes</a> is for three year old Colts and Geldings (123 lbs.); Fillies (118 lbs.) The $550,000 purse will be split as follows:  1% to all owners of horses finishing 7th through last, 60% of the remaining purse shall be paid to the owner of the winner plus 50 Road To The Kentucky Derby Points, 20% to second plus 20 Road To The Kentucky Derby Points; 10% to third plus 10 Road To The Kentucky Derby Points; 5% to fourth plus 5 Road To The Kentucky Derby Points; 3% to fifth and 2% to sixth.</p>
            <h2>Kentucky Derby Prep Race</h2>
            <p>The Spiral Stakes is part of the <a href="/kentuckyderby-prepraces">Kentucky Derby Prep Races</a> - Championship Series, where the first four finishers will receive the following points in their race for the roses: 1st - 50points, 2nd - 20points, 3rd - 10points, 4th - 5points.</p>
            <h2>              2014 Spiral Stakes</h2>
            <p>The Spiral Stakes will be held on Saturday, March 22, 2014 at Turfway Park. Nominations closed  with a total of 137 horses but the starting line up is limited to 12. Final entries for the Spiral Stakes will be drawn Wednesday, March 19, at Horseshoe Casino Cincinnati which is an invitation only event. </p>
            <h2 class="title-custom">Spiral Stakes Race Information </h2>
<p>Location: 	<a href="/turfway-park">Turfway Park</a>, Florence, Kentucky, United States<br />
  Inauguratal Race: 	1972<br />
  Race type: 	Thoroughbred - Flat racing<br />
  Distance: 	1 1/8 miles (9 furlongs)<br />
  Track: 	PolyTrack, left-handed<br />
  Qualification: 	Three-year-olds<br />
  Weight: 	Assigned<br />
  Purse: 	$550,000<br />
  Grade III race<br />
</p>
        <h2 class="title-custom">Bet on Spiral Stakes </h2>
        <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <h2>Spiral Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/spiral-stakes/winners.tpl"}

        <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
