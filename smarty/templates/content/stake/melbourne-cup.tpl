{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{literal}
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "name": "Bet on Melbourne Cup",
        "description": "Bet on the Melbourne Cup. Place your bet at BUSR and get a daily rebate and sign up bonus. Melbourne Cup betting at its best.",
        "publisher": {
            "@type": "Organization",
            "name": "US Racing"
        }
    }
    </script>
{/literal}
{literal}
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Melbourne Cup",
  "description": "Bet on the Melbourne Cup. Place your bet at BUSR and get a daily rebate and sign up bonus.  Melbourne Cup betting at its best.",
  "image": "https://www.usracing.com/img/best-horse-racing-betting-site.jpg",
  "startDate": "2019-11-03 19:19", 
  "endDate": "2019-11-03 20:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
  "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
      "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],
  "location": {
    "@type": "Place",
    "name": "Flemington Racecourse",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "448 Epsom Rd",
      "addressLocality": "Flemington",
      "addressRegion": "Flemington",
      "postalCode": "VIC 3031",
      "addressCountry": "Australia"
    }
  }
}

</script>

{/literal}     
{assign  var="stakesname" value="Melbourne Cup"} 
{assign  var="stakespath" value="melbourne-cup"} 
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           

<div class="headline"><h1>{$stakesname} Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{* <p>{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/slider.tpl"} </p> *}

<h2>Bet on {$stakesname} </h2>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/specs.tpl"}
{*WIKI CONTENT-------------------------------*}

<p>The Melbourne Cup is Australia's major thoroughbred horse race. Marketed as "the race that stops a nation", it is a 3,200 meters race for three-year-olds and over. It is the richest "two-mile" handicap in the world, and one of the richest turf races. Conducted annually by the Victoria Racing Club on the Flemington Racecourse in Melbourne, Victoria, the event starts at 3pm (daylight saving time) on the first Tuesday in November.</p>

<p>The first race was held in 1861 over two miles (3.219 km) but was shortened to 3,200 meters (1.988 mi) in 1972 when Australia adopted the metric system. This reduced the distance by 18.688 meters (61.312 ft), and Rain Lover's 1968 race record of 3:19.1 was accordingly adjusted to 3:17.9. The present record holder is the 1990 winner Kingston Rule with a time of 3:16.3.</p>

<h2>{$stakesname} Odds</h2>
{include file='/home/ah/allhorse/public_html/misc/odds_melbourne_cup_xml.php'}
<p>For the latest {$stakesname} odds, please login into BUSR and check the racebook.</p>

{* {include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/video.tpl"} *}
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
