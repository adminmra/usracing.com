{literal}
  <link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      

{literal}
    <style>
        .background{
        background-image: url("/img/index-bs/bg-belmont.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 110%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 104%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 98%;
  }
}

@media (min-width: 1024px) and (max-width: 1366px) {
  .background{
    background-position: 0% 100% !important;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 89%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 89%;
  }
}

    </style>
  {/literal}
  

{include file="/home/ah/allhorse/public_html/belmont/block-hero.tpl"} 


<section class="bs-kd bs-kd--default usr-section bs">

    <div class="container">

        <div class="bs-kd_content">

            <h1 class="kd_heading" style="margin-top:45px">{$h1}</h1>

            <h3 class="kd_subheading">
              {if $BS_Switch}{$h2_bs_switch} 
              {elseif $BS_Switch_Phase_1}{$h2_phase_1} 
              {elseif $BS_Switch_Race_Day}{$h2_race_day}
              {else}{$h2}{/if}
            </h3>

            <p>Twenty three <a href="/kentucky-derby/odds">Kentucky Derby</a> winners have used the Blue Grass as a prep for the road to the roses, 11 have completed the Blue Grass-Derby double. Since 1997 the Blue Grass has produced one <a href="/kentucky-derby/winners">Kentucky Derby winner</a> from a total of 85 starters, The Blue Grass Stakes at Keeneland sends out the most starters for any <a href="/kentucky-derby/odds">Kentucky Derby</a> Prep Race.</p>
            <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>

            <p><a href="/signup?ref={$ref} ">BUSR</a>&nbsp;is the best site to place your bet on the&nbsp;Blue Grass Stakes.</p>
            <p>Why? Because, all new members can get up to a&nbsp;<a href="/cash-bonus-20">$500 welcome bonus!</a>&nbsp;You'll also get&nbsp;<strong>free bets</strong> and great&nbsp;<a href="/bluegrass-stakes">bonuses</a>&nbsp;found nowhere else online!</p>
            <p>So what are you waiting for?&nbsp;<a href="/signup?ref=bluegrass-stakes"> Blue Grass Stakes Odds are live.</a></p>

            <h2>2020 Blue Grass Stakes Odds<br /></h2>
            {include_php file='/home/ah/allhorse/public_html/stakes/bluegrass-stakes/odds.php'}

            <br>
            <br>
            <p align="center"><a href="/signup?ref=bluegrass-stakes-mid" rel="nofollow" class="btn-xlrg ">{$button_cta}</a></p>  

            <h2><br />2020 Blue Grass Stakes Day Schedule<br /></h2>
            <ul style="max-width:500px; text-align:left; margin: auto; font-size: 20px">
                <li>$150,000 Appalachian Stakes (G2T)<br /></li>
                <li>$400,000 Central Bank Ashland Stakes (G1)<br /></li>
                <li>$350,000 Coolmore Jenny Wiley Stakes (G1T)<br /></li>
                <li>$250,000 Madison Stakes (G1)<br /></li>
                <li>$150,000 Shakertown Stakes (G2T)<br /></li>
                <li>$600,000 Toyota Blue Grass (G2)</li>
            </ul>


            <h2><br />Blue Grass Stakes Past Winners<br /></h2>
            
            {include_php file='/home/ah/allhorse/public_html/stakes/bluegrass-stakes/past-winners.php'}
              
        </div>
    </div>
</section>


      
    