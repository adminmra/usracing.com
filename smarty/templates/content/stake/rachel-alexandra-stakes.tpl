{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>Rachel Alexandra Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>&nbsp;</p>
                   
                 <p><strong>Location: </strong><a href="/fair-grounds-race-course">Fair Grounds</a> Race Course, New Orleans, Louisiana, United States<br />
                   <strong>Inaugurated:</strong> 1982<br />
                   <strong>Race type:</strong> Thoroughbred &ndash; Flat racing<br />
                   <strong>Distance:</strong> 1 1/16 (8.5 furlongs)<br />
                   <strong>Track:</strong> Dirt, left-handed<br />
                   <strong>Qualification:</strong> Three-year-old fillies<br />
                   <strong>Weight:</strong> Assigned<br />
                   <strong>Purse:</strong> $200,000<br />
                 </p>
                 <p>The Rachel Alexandra Stakes is an important prep race for developing three-year-old fillies. The race is a Grade II event with a purse of $200,000 and is a prep race to the Triple Tiara of Thoroughbred Racing, including the <a href="/kentucky-oaks">Kentucky Oaks</a>, the Black-Eyed Susan Stakes and Mother Goose Stakes.<br />
                   </p>
                   <p>Named after racehorse, Rachel Alexandra, who is a retired American Thoroughbred and the 2009 Horse of the Year winner. When Rachel Alexandra won the 2009 Preakness Stakes, the second leg of the Triple Crown, she became the first filly to win the race in 85 years. </p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Rachel Alexandra Stake</strong>s</h2>
                      <p>&nbsp;</p>
                      <p>US Racing features race analysis, interviews, free handicapping tips and   stories on the trainers, jockeys, owners and horses that make up the colorful   sport of horse racing. US Racing features the best tracks in the United States,   including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.   Internationally, US Racing offers content from Australia, Dubai, Great Britain,   Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing   and bet on Stakes Races today!</p>
                   <p><br />
                   </p>
                   <h2>Rachel Alexandra Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/rachel-alexandra-stakes/winner.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
