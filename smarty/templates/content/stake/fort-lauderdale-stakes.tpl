{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Bet on Ft. Lauderdale Contenders</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Ft. Lauderdale Stakes </h1>
                 <div class="news-date"></div>
                 
                   <p><strong>Location: </strong>Gulfstream Park Hallandale Beach, Florida, USA <br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing <br />
                     <strong>Distance:</strong> 1-1/16 miles (8.5 furlongs) <br />
                     <strong>Track: </strong>Turf, left-handed <br />
                     <strong>Qualification: </strong>Four-year-olds &amp; up <br />
                     <strong>Purse:</strong> $200,000<br />
                     <strong>Grade: </strong>II </p>
                   <p>&nbsp;</p>
                   <h2>Bet on Ft. Lauderdale Stakes <br />
                                      </h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Ft. Lauderdale Stakes Contenders</h2>
                  {include file="/home/ah/allhorse/public_html/graded_stakes/fort-lauderdale-stakes/winners.tpl"} 
    

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
