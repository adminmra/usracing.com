{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Travers Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2><strong>Travers Stakes </strong></h2>
<ul>
<li><strong>Grade:</strong> I for 3-year-old thoroughbreds and fillie</li>
<li><strong>Distance:</strong> 1 mile</li>
<li><strong>Track:</strong> Dirt</li>
<li><strong>Purse:</strong> $1,000,000</li>
</ul>
<p>&nbsp;</p>
<p>The <strong>Travers Stakes</strong>, held yearly at the <a title="Saratoga Race Course" href="/saratoga"><strong>Saratoga Race Course</strong></a> in Saratoga Springs, New York, is America's oldest stakes race in continuous existence. First run in 1864, it was named so in honor of William Travers, who had the Saratoga race track built the year before, along with Leonard Jerome and John Hunter.</p>
<p>The race was inaugurated in 1864 at Saratoga Race Course and has been run in all but five years since, and with all but three runnings at Saratoga. Due to war-time restrictions on travel, the 1943, 1944 and 1945 Saratoga meetings were conducted at Belmont Park.</p>
<p>Travers was a prominent businessman in New York City for most of his life, and for many years, bred and owned some of the period's better horses.</p>
<p>The inaugural meeting at Saratoga was short and sweet. The Travers Stakes was the highlight of the meeting, being named, of course, for one of the track's founders and its first president.</p>
<p>That Travers and Hunter co-owned the first winner, a colt named Kentucky, of the race named for Travers is not unusual during those times. In the 1800s, the men who built racetracks were often the ones who bred and owned many of the best horses at these meetings. They built tracks to showcase their horses more than to make money.</p>
<p>The field for the Travers Stakes is limited to three-year-olds, Colts and geldings carrying 126 pounds (57 kg) and fillies carrying 123 pounds (56 kg). Since 1999 the first prize has been $1,000,000. The race is the highlight of the summer race meeting at Saratoga, just as the <a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a> is the highlight of the spring meeting at <a title="Belmont Park" href="/belmont-park">Belmont Park</a>.</p>
<p>&nbsp;</p>

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    