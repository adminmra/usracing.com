{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Kentucky Jockey Club Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Distance: 1 1/16 miles (8.5 furlongs)<br />
                     Track: 	Dirt, left-handed<br />
                     Qualification: 	Two-year-olds<br />
                     Weight: Assigned<br />
                     Purse: 	US$150,000+<br />
                     Location: Churchill Downs<br />
                   </p>
                   <p>Race Type: <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> <br />
                     Road to the Roses Points: 1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point</p>
                   <p>The Kentucky Jockey Club Stakes is an American Thoroughbred horse race run annually during the last week of November at Churchill Downs in Louisville, Kentucky. A Grade II event, the race is open to two-year-olds willing to race one and one-sixteenth miles on the dirt.</p>
                   <p>Since it was inaugurated  in 1920, five horses have won the Kentucky Jockey Club Stakes and then returned the next spring to the win the Kentucky Derby.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on <span class="title-custom">Jockey Club Stakes</span></strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/kentucky-jockey-club-stakes/winners.tpl"} 

                   
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
