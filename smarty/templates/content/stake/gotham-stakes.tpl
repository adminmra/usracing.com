{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
 {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"> <h1>Gotham Stakes</h1></div>
  
  
<div class="content">
    
    
            <p>The 61st Running is scheduled for Saturday, March 1, 2014. The Gotham Stakes is the final local prep to the Wood Memorial Stakes and an official prep race for the Kentucky Derby. A number of Derby winners have competed in this race including U.S. Triple Crown winner, Secretariat.</p>

<p>Of course, GOTHAM is another name for New York City and the city in which Aqueduct Racetrack is located.</p>

<p>The Gotham STakes was run at Jamaica race track at 1 1/16 miles prior to 1960, and in 1977 and 1979. From 1960-78, 1980-83, 1985-2002, and 2004-05, it was run at one miles. Race horse, "Easy Goer," established the stakes and track record of 1:32 2/5 in 1989.</p>

<h2><span class="title-custom">Gotham Stakes</span> Video</h2>

<iframe frameborder="0" class="video" src="//www.youtube.com/embed/dkAif-xi0kY"></iframe>

<p><br></p>

<h2 class="title-custom">Gotham Stakes Race Information</h2>

<p>Location: Aqueduct Racetrack - Queens, New York, United States<br>
Inaugurated: 1953<br>
Race type: Thoroughbred - Flat racing<br>
Website: Aqueduct Racetrack<br>
Distance: 11/16 miles (8.5 furlongs)<br>
Track: Dirt, left-handed<br>
Qualification: Three-year-olds<br>
Weight: Assigned<br>
Purse: $500,000 (2014)<br>
Grade: III<br></p>

<h2 class="title-custom">Bet on Gotham Stakes</h2>

<p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>

<p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>

<h2 class="title-custom">Gotham Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/gotham-stakes/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
      
   
