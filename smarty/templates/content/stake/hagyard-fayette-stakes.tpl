{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Willcox Inn is nominated to the Hagyard Fayette Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">                   Hagyard Fayette Stakes</h1>
                 <div class="news-date">October 6, 2013</div>
                 <p>Finishing third, 3&frac12; lengths behind Wise Dan was Willcox Inn, who earned $75,000 and pushed his career earnings past the million-dollar mark to $1,014,543. Trainer Mike Stidham was pleased with the effort from the 5-year-old son of Harlan's Holiday who races for Lael Stable and All In Stable.</p>
                   <p>&quot;He'd just won the Washington Park Handicap (G3) on the Poly so it actually played in our favor to move to the Poly because he does not like soft turf, so I was hoping for it,&quot; Stidham said, referring to Willcox Inn's victory on Aug. 31 at Arlington Park. &quot;(Jockey) James (Graham) sat back - they were going pretty fast up front. He kicked in and made a nice run down the stretch to pick up third money.&quot;</p>
                   <p>Stidham said Willcox Inn is nominated to the Hagyard Fayette (G2) on closing day.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Hagyard Fayette Stakes</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>Location: 	<a href="/racetrack/keeneland">Keeneland</a> Racetrack<br />
                     Grade: 		2<br />
                     Distance: 	1 1/8 miles<br />
                     Track:		Main<br />
                     Purse: 		$200,000<br />
                   </p>
                   <p>&nbsp;</p>
                   <p><br />
                   </p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
