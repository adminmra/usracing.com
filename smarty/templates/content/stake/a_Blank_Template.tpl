{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{assign  var="stakesname" value="Stakes"} 
{assign  var="stakespath" value="Path"} 
{assign  var="racetrackpath" value="Track"} 
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           

<div class="headline"><h1>{$stakesname} Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/slider.tpl"} </p>

<h2>Bet on {$stakesname} </h2>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/specs.tpl"}
{*WIKI CONTENT-------------------------------*}

<p></p>

<h2>{$stakesname} Odds</h2>
<p>For the latest {$stakesname} odds, please login into BUSR and check the racebook.</p>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/video.tpl"}
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    