{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Lady Ten wins Autumn Miss Stakes at Santa Anita - looking ahead</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Autumn Miss Stakes</h1>
                 <div class="news-date">October</div>
                 
                   <p>Lady Ten held off a furious rally from Customer Base to win the $100,000 Autumn Miss Stakes by a neck for trainer Bob Baffert on Saturday at Santa Anita.</p>
                   <p>Ridden by Rafael Bejarano, Lady Ten ran a mile on the turf in 1:34.95 and paid $6.80, $4.20 and $2.80 as the 2-1 favorite.</p>
                   <p>Customer Base returned $8.60 and $4.80, while Charm the Maker was another 1 1/4 lengths back in third and paid $4 to show.</p>
                   <p>Bejarano, serving a three-day suspension, was allowed to ride because the Grade 3 race is considered a designated race by track stewards. He is 3 for 4 on Lady Ten.</p>
                   <p>''This filly is very nice to ride, and easy to ride,'' Bejarano said. ''She likes to be close to the lead so I was a little worried in the beginning. She broke good from the gate but I knew there would be a lot of speed so I tried to make sure to give her help from the gate so she could find a position in the race.''</p>
                   <p>The victory, worth $60,000, increased Lady Ten's career earnings to $246,244, with four wins in 11 starts.</p>
                   <p>&nbsp;</p>
                   <h2>Bet on Autumn Miss Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>Location: 	<a href="/santa-anita-park">Santa Anita Park</a><br />
                     Distance: 	1 mile <br />
                     Track: 		Turf<br />
                     Qualification: 	Fillies, three-years-old<br />
                     Weight: 	G III<br />
                     Purse: 		$100,000</p>
                   <p>&nbsp;</p>
                   <h2>Remsen Stakes Video</h2>
                   Oct 28, 2012 -- Interview with jockey Rafael Bejarano after Lady Ten wins the Autumn Miss Stakes (Gr. III)

                   <iframe class="video" width="640" height="360" src="//www.youtube.com/embed/86qztZugg5s?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
