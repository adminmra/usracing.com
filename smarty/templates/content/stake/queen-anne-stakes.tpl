<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
    <div class="text text-md">{$h2}
        <br>
            <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
    </div>
 </div>

<!--------- NEW HERO END ---------------------------------------->


{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/specialraces.tpl'}         

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      

<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>Queen Anne Stakes Betting</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->



<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

{*include file='inc/maps/royalascot.tpl'*} 

<p><strong>Where is the {'Y'|date}  Queen Anne Stakes?</strong> &nbsp; Ascot Racecourse<br />
<strong>When is the {'Y'|date}  Queen Anne Stakes?</strong> &nbsp; June 19 , {'Y'|date} </p>

<p>{include file="/home/ah/allhorse/public_html/usracing/odds/marketing_inc.tpl"} </p>
{*include file='/home/ah/allhorse/public_html/misc/queen_anne_stakes.php'*}
{include file='/home/ah/allhorse/public_html/misc/queen_anne_stakes_3445.php'}

<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
  
<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
<div class="calltoaction">
 {* <h2>Get a Free Bet <br>for the <br>Breeders' Cup! </h2>  *}

 <h2>Bet the <br>Royal Ascot!</h2> 
<h3>Sign Up Now<br>  to Get Your <br>10% Bonus!</h3>
{*
<h2>Get your <br>$150  Bonus!</h2>
<h3>Sign Up Now to <br>  Get Started Today</h3>
*}
{*
<h2>Free Bet on the<br>Belmont Stakes!</h2>
<h3>Exclusive Belmont odds and more.</h3>
*}
{include file="/home/ah/usracing.com/smarty/templates/inc/rightcol-calltoaction-form.tpl"}

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
