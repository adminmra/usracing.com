{include file="/home/ah/allhorse/public_html/usracing/schema/stake/virtual-kentucky-derby.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{literal}
<style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref2}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref2}">{$signup_cta}</a>
    </div>
</div>
{*End*}
 <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
          <h1 class="kd_heading">Virtual Kentucky Derby Betting</h1>
              <h3 class="kd_subheading">Bet the  Virtual Kentucky Derby Race Today</h3>
<p>{include file='/home/ah/allhorse/public_html/stakes/virtual-kentucky-derby-betting/salesblurb.tpl'} </p>
<p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">Bet Now on Virtual Kentucky Derby</a></p>
	 <h2>Virtual Kentucky Derby Betting</h2>
    <p><strong>Secretariat</strong> or <strong>Justify</strong>? Who will win?</p>
<p>The race is virtual but the action is real!</p>
<p>Watch the race this Saturday on NBC and wager online at <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a></p>
<p>Sign up, get your <a href="/promos/cash-bonus-20"> new member bonus up to $500</a>, and place your bets today on your favorite Derby champion!</p>
<table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Virtual Kentucky Derby Post Positions"
        summary="Virtual Kentucky Derby Post Positions. Only available at BUSR.">
        <caption><h3>Virtual Kentucky Derby Post Positions</h3></caption>
        <thead>
            <tr>
                <th> Position </th>
                <th> Horse </th>
                <th> Odds </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Position">1</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Affirmed (1978) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 5-1 </td>
            </tr>
             <tr>
                <td data-title="Position">2</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Assault (1946) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 20-1 </td>
            </tr>
               <tr>
                <td data-title="Position">3</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Secretariat (1973) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 7-2 </td>
            </tr>
              <tr>
                <td data-title="Position">4</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Sir Barton (1919) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 20-1 </td>
            </tr>
                 <tr>
                <td data-title="Position">5</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Seattle Slew (1977) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 5-1 </td>
            </tr>
                 <tr>
                <td data-title="Position">6</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> American Pharoah (2015) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 6-1 </td>
            </tr>
                <tr>
                <td data-title="Position">7</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Gallant Fox (1930) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 20-1 </td>
            </tr>
              <tr>
                <td data-title="Position">8</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Citation (1948) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 4-1 </td>
            </tr>
             <tr>
                <td data-title="Position">9</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> War Admiral (1937) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 8-1 </td>
            </tr>
                <tr>
                <td data-title="Position">10</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Whirlaway (1941) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 6-1 </td>
            </tr>
             <tr>
                <td data-title="Position">11</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Count Fleet (1943) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 6-1 </td>
            </tr>
            <tr>
                <td data-title="Position">12</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Justify (2018) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 15-1 </td>
            </tr>
            <tr>
                <td data-title="Position">13</td>
                <td data-title="Horse" style="word-break: keep-all !important;"> Omaha (1935) </td>
                <td data-title="Odds" style="word-break: keep-all !important;"> 20-1 </td>
            </tr>
					     </tbody>	
    </table>			
	<br>
<p><a href="/signup?ref={$ref}" rel="nofollow"
            class="btn-xlrg fixed_cta">Bet Now on Virtual Kentucky Derby</a></p>
 </div>
</section>
{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}