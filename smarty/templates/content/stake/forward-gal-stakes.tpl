{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Forward Gal Stakes</h1> </div>
  
  <div class="content">
    
    
            
                 
                
                 
                   <p><strong>Location: </strong>Gulfstream Park, Hallandale, Florida, United States<br />
                     <strong>Inaugurated:</strong> 	1981<br />
                     <strong>Race type</strong>:	Thoroughbred - Flat racing<br />
                     <strong>Distance:</strong> 	7 furlongs<br />
                     <strong>Track: </strong>Dirt, left-handed<br />
                     <strong>Qualification:</strong> 	Three-year-old fillies<br />
                     <strong>Weight :</strong>	Assigned<br />
                     <strong>Purse:</strong> 	$200,000<br />
                     <strong>Grade: </strong>II <br />
                   </p>
                   <p>A prep race to the Triple Tiara of Thoroughbred Racing, including the <a href="/kentucky-oaks">Kentucky Oaks</a>, the Black-Eyed Susan Stakes and Mother Goose Stakes.</p>
                   <p>The Forward Gal Stakes is named for &quot;Forward Gal,&quot; the American Champion Two-Year-Old Filly of 1970.</p>
                   <p>Forward Gal (foaled 1968 in Florida) was an American Thoroughbred Champion racehorse. Out of the mare, Forward Thrust, he was sired by Florida Derby winner Native Charger who also sired 1970 Belmont Stakes winner High Echelon. In 1970 Forward Gal's performances earned her American Champion Two-Year-Old Filly honors and although she did not repeat as champion at age three, she was one of the top fillies in her age group and under jockey Michael Hole won several important races including the 1971 Gazelle Handicap and Comely Stakes.</p>
                   <p>&nbsp;</p>
                   
                   <h2 class="title-custom">Bet on Forward Gal Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2 class="title-custom">Forward Gal Stakes Past Winners</h2>
                    {include file="/home/ah/allhorse/public_html/graded_stakes/forward-gal-stakes/winners.tpl"} 

            
       
      
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
