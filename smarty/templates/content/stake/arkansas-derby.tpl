{include file="/home/ah/allhorse/public_html/usracing/schema/stake/arkansas-derby.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{literal}
<style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
        .kd {
            background: transparent url(/img/racetracks/oaklawn-park/oaklawn-park-horse-betting-racetrack.jpg) center bottom no-repeat;
            background-size: initial;
            padding: 20px 0 0;
            text-align: center
        }
        @media (min-width:768px) {
            .kd {
                background-size: 100%;
                padding: 20px 0 0
            }
        }
        @media (min-width:1024px) {
            .kd {
                padding: 20px 0 0
            }
        }
        @media (max-width:768px) {
            .kd {
                background: transparent url(/img/racetracks/oaklawn-park/oaklawn-park-racetrack-horse-betting.jpg) center bottom no-repeat;
                background-size: contain;
                text-align: center
            }
        }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
{*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">Arkansas Derby</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Arkansas Derby Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
<!-- --------------------- content starts here ---------------------- -->
<div class="racetrack-text">
<p><strong>Stakes:</strong> Grade II for 3-year-old thoroughbreds and fillies<br>
<strong>Distance:</strong> 1 1/8 miles<br>
<strong>Track:</strong> Dirt<br>
<strong>Purse:</strong> $1,000,000</p></div>
<br>
<p>The Racing Festival of the South culminates with the annual Arkansas Derby race. Horse racing fans are eager to see
  if the next Oaklawn Park champ will finally bring home the Triple Crown trophy.</p>
<p>The Arkansas Derby is the second leg of the <strong>Big 3 Pick 3</strong>, a $1 minimum national wager where horse
  bettors will try to pick the winner of three important prep races for three-year-olds planning on running in the <a
    title="Kentucky Derby" href="/kentucky-derby"><strong>Kentucky Derby</strong></a> and the prestigious Triple Crown.</p>
<p>Two favorite horses in recent years, Smarty Jones and Afleet Alex, both captured the Arkansas Derby and finished
  their careers as top three-year-olds.</p>
<p>The Arkansas Derby has really gained in stature because of this, keeping its reputation as a vital race for any
  Kentucky Derby and Triple Crown hopeful.</p>
<p>The race is being held at Oaklawn Park, which was incorporated in 1904 and one of the select few that features the
  finest in live thoroughbred horse racing in America.</p>
  <p style="text-align: center;"><a href="/signup?ref={$ref2}" rel="nofollow" class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
<table class="data table table-condensed table-striped table-bordered ordenable" border="0" width="100%" cellspacing="0" cellpadding="0">
<caption><h3>$500,000 Arkansas Derby Betting (First Division)</h3></caption>
<tbody>
<tr>
<th>PP<i class="title=&quot;Sort"></i></th>
<th>Horse<i class="title=&quot;Sort"></i></th>
<th>Trainer<i class="title=&quot;Sort"></i></th>
<th>Jockey<i class="title=&quot;Sort"></i></th>
<th>M/L<i class="title=&quot;Sort"></i></th>
</tr>
<tr>
<td>1</td>
<td><strong>Charlatan</strong></td>
<td>Bob Baffert</td>
<td>Martin Garcia</td>
<td>1/1</td>
</tr>
<tr>
<td>2</td>
<td><strong>My Friends Beer</strong></td>
<td>Jeremiah O’Dwyer</td>
<td>Declan Cannon</td>
<td>20/1</td>
</tr>
<tr>
<td>3</td>
<td><strong>Mo Mosa</strong></td>
<td>Mike Maker</td>
<td>Kendrick Carmouche</td>
<td>30/1</td>
</tr>
<tr>
<td>4</td>
<td><strong>Gouverneur Morris</strong></td>
<td>Todd Pletcher</td>
<td>John Velazquez</td>
<td>9/2</td>
</tr>
<tr>
<td>5</td>
<td><strong>Jungle Runner</strong></td>
<td>Steve Asmussen</td>
<td>Tyler Baze</td>
<td>30/1</td>
</tr>
<tr>
<td>6</td>
<td><strong>Shooters Shoot</strong></td>
<td>Peter Eurton</td>
<td>Joe Talamo</td>
<td>SCR</td>
</tr>
<tr>
<td>7</td>
<td><strong>Wrecking Crew</strong></td>
<td>Peter Miller</td>
<td>Flavien Prat</td>
<td>SCR</td>
</tr>
<tr>
<td>8</td>
<td><strong>Anneau d’Or</strong></td>
<td>Blaine Wright</td>
<td>Juan Hernandez</td>
<td>6/1</td>
</tr>
<tr>
<td>9</td>
<td><strong>Winning Impression</strong></td>
<td>Dallas Stewart</td>
<td>Julien Leparoux</td>
<td>15/1</td>
</tr>
<tr>
<td>10</td>
<td><strong>Crypto Cash</strong></td>
<td>Kenny McPeek</td>
<td>Corey Lanerie</td>
<td>20/1</td>
</tr>
<tr>
<td>11</td>
<td><strong>Basin</strong></td>
<td>Steve Asmussen</td>
<td>Ricardo Santana, Jr.</td>
<td>8/1</td>
</tr>
</tbody>
</table>
<br>
<table class="data table table-condensed table-striped table-bordered ordenable" border="0" width="100%" cellspacing="0" cellpadding="0">
<caption><h3>$500,000 Arkansas Derby Betting (Second Division)</h3></caption>
<tbody>
<tr>
<th>PP<i class="title=&quot;Sort"></i></th>
<th>Horse<i class="title=&quot;Sort"></i></th>
<th>Trainer<i class="title=&quot;Sort"></i></th>
<th>Jockey<i class="title=&quot;Sort"></i></th>
<th>M/L<i class="title=&quot;Sort"></i></th>
</tr>
<tr>
<td>1</td>
<td><strong>Finnick the Fierce</strong></td>
<td>Rey Hernandez</td>
<td>Martin Garcia</td>
<td>15/1</td>
</tr>
<tr>
<td>2</td>
<td><strong>Saratogian</strong></td>
<td>Rodolphe Brisset</td>
<td>Joe Talamo</td>
<td>SCR</td>
</tr>
<tr>
<td>3</td>
<td><strong>Storm the Court</strong></td>
<td>Peter Eurton</td>
<td>Flavien Prat</td>
<td>6/1</td>
</tr>
<tr>
<td>4</td>
<td><strong>King Guillermo</strong></td>
<td>Juan Carlos Avila</td>
<td>Samuel Camacho, Jr.</td>
<td>3/1</td>
</tr>
<tr>
<td>5</td>
<td><strong>Nadal</strong></td>
<td>Bob Baffert</td>
<td>Joel Rosario</td>
<td>5/2</td>
</tr>
<tr>
<td>6</td>
<td><strong>Code Runner</strong></td>
<td>Steve Asmussen</td>
<td>Stewart Elliott</td>
<td>50/1</td>
</tr>
<tr>
<td>7</td>
<td><strong>Silver Prospector</strong></td>
<td>Steve Asmussen</td>
<td>Ricardo Santana, Jr.</td>
<td>10/1</td>
</tr>
<tr>
<td>8</td>
<td><strong>Fast Enough</strong></td>
<td>Rafael Becerra</td>
<td>Tyler Baze</td>
<td>SCR</td>
</tr>
<tr>
<td>9</td>
<td><strong>Taishan</strong></td>
<td>Richard Baltas</td>
<td>David Cohen</td>
<td>15/1</td>
</tr>
<tr>
<td>10</td>
<td><strong>Farmington Road</strong></td>
<td>Todd Pletcher</td>
<td>Javier Castellano</td>
<td>12/1</td>
</tr>
<tr>
<td>11</td>
<td><strong>Wells Bayou</strong></td>
<td>Brad Cox</td>
<td>Florent Geroux</td>
<td>7/2</td>
</tr>
</tbody>
</table>
	<br>
<p style="text-align: center;"><a href="/signup?ref={$ref2}" rel="nofollow" class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
	<br>
  <p><iframe src="//www.youtube.com/embed/7yT6rYb5y_w" frameborder="0" class="video"></iframe></p>
  <br>
  </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/stakes/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/stakes/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}