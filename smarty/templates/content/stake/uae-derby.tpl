{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1>UAE Derby</h1>
</div>
  
  
<div class="content">
    
    
            <h2>Toast Of New York tackles top opposition in UAE Derby</h2>
            <p>March 25, 2014</p>
            <p>Toast Of New York is the wildcard in the UAE Derby at Meydan on Saturday, according to trainer Jamie Osborne.</p>
            <p>A 16-length winner of a novice event at Wolverhampton on his final juvenile start, the Michael Buckley-owned colt has wowed work-watchers in Lambourn and arrives in the emirate with quiet confidence.</p>
                   <p>Osborne said: &quot;After winning his maiden at Wolverhampton there was the issue of whether we targeted the Group Three Autumn Stakes at Ascot or went for a novice.</p>
                   <p>&quot;We did the latter and he won it by 16 lengths - it was a complete mismatch.</p>
                   <p>&quot;In this race, with a three-year-old that lacks experience it's harder than with an older horse, but I wouldn't be out here unless I thought he could be competitive.</p>
                   <p>&quot;This morning was his first experience of cantering on the Tapeta surface and I thought he moved beautifully on it.<br />
                   </p>
        <h2 class="title-custom">&nbsp;</h2>
                   <h2 class="title-custom">UAE Derby Stakes Race Information </h2>
<p> <strong>Location:</strong> 	Meydan Racecourse - Dubai, United Arab Emirates<br />
  <strong>Inaugural Race: </strong>2000<br />
<strong>Race type:</strong> 	Thoroughbred - Flat racing<br />
<strong>Distance: </strong>1,900 metres (1 3/16 miles or approx. 9.5 furlongs)<br />
<strong>Track: </strong>Tapeta, left-handed<br />
<strong>Qualification:</strong> 	3-year-olds from the Northern &amp; 4-year-olds from the Southern Hemisphere<br />
<strong>Purse: </strong>US$2,000,000<br />
Group 2 race</p>
<h2>Kentucky Derby Prep Race</h2>
<p>The UAE Derby is part of the 2nd leg of the <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a> - Championship Series, where the first four finishers will receive the following points in their race for the roses: 1st - 100points, 2nd - 40points, 3rd - 20points, 4th - 10points.</p>
<h2>Bet on UAE Derby Stakes
</h2>
<p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
        <h2 class="title-custom">UAE Derby Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/dubai/pastwinners.php"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
