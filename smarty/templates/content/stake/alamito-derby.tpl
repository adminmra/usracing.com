{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{assign  var="stakesname" value="Alamito Derby"} 
{assign  var="stakespath" value="indiana-derby"} 
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           

<div class="headline"><h1>{$stakesname} Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<h2>Bet on {$stakesname} </h2>
<h2>{$stakesname} Odds</h2>
{*include file='/home/ah/allhorse/public_html/misc/odds_melbourne_cup_xml.php'*}
<p>{include file='/home/ah/allhorse/public_html/usracing/odds/marketing_inc.tpl'} </p>
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    