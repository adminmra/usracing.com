{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Risen Star Stakes</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<strong>Location:</strong> Fair Grounds Race Course New Orleans, Louisiana <br />
                     <strong>Inaugurated: </strong>1973 <br />
                     <strong>Race type: </strong>Thoroughbred, Flat racing <br />
                     <strong>Distance:</strong> 1 1/16 miles (8.5 furlongs) <br />
                     <strong>Track:</strong> Dirt, left-handed <br />
                     <strong>Qualification: </strong>Three-year-olds <br />
                     <strong>Weight: </strong>Assigned <br />
                     <strong>Purse: </strong>$300,000 <br />
                     Grade II race <br />
            
                 <p><strong>Race Type:</strong> <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race </a></p>
                   <p><strong>Road to the Roses Points:</strong> 1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point<br />
                   </p>
                   <p>The Risen Star Stakes is run annually in February at the Fair Grounds Race Course. It is open to three year old horses and is raced over a distance of one and one-sixteenth miles on the dirt.The race is the major prep race to the Louisiana Derby and one of many prep race preparing colts for the Kentucky Derby and the Triple Crown. Previously known as the Louisiana Derby Trial Stakes, in 1989 it was renamed to honor locally-owned Risen Star who won the race in 1988 and went on to win the Preakness and Belmont Stakes.</p>
                   <p>&nbsp;</p>
                   <h2>Ive Struck a Nerve 135-1 upset in Risen Star </h2>
                   <p>If you went simply by the odds on the tote board, there was no way for Ive Struck a Nerve could win the Risen Star Stakes at the Fair Ground. But, the 135-1 long shot took the Risen Star Stakes at Fair Grounds Race Course &amp; Slots February 23, 2013, Matthew W. Bryan's Ive Struck a Nerve closing late in the furlong grounds and catapulting himself onto the road to the Kentucky Derby. </p>
                   <p>The 3-year-old son of Yankee Gentleman paid $272.40 to win and earned 50 qualifying points for the Derby with victory by a nose while giving trainer J. Keith Desormeaux his first graded stakes win. For his victory in the Risen Star, Ive Stuck a Nerve earns 50 points toward a spot in the starting gate in the 2013 Kentucky Derby.</p>
                   <p>&nbsp;</p>
                   <h2>Bet on Risen Star Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Risen Star Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/risen-star-stakes/winners.tpl"}
   

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
