{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Thoroughbred Stakes Schedule featuring Delta Downs Princess Stakes</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Delta Downs Princess Stakes</h1>
                 <div class="news-date">October 13, 2013</div>
                 
                   <p>Delta Downs has released its 2013-14 Thoroughbred stakes schedule, which includes 32 races and $4.6 million in total purse money.</p>
                   <p>&ldquo;We are extremely excited about the upcoming season at Delta Downs,&rdquo; said Steve Kuypers, Vice President and General Manager of Delta Downs Racetrack Casino &amp; Hotel. &ldquo;The tremendous support we receive from horsemen each year along with the tireless work of our team members make Delta Downs racing something special.&rdquo;</p>
                   <p>The highlight of the upcoming 88-day season, which runs from October 11 through March 15, will come on Saturday, November 23. On that day the track will host eight stakes races and offer $2.25 million in total purse money during the 11th running of the $1,000,000 Delta Downs Jackpot (Gr. 3). There will be a special post time of 1:15 p.m. CST for the Jackpot Day program.</p>
                   <p>Last year&rsquo;s Delta Downs Jackpot Day program produced the second highest single-day handle in track history of $4,359,362, only 1.57% less than 2011 when fans poured a record $4,434,099 through the wagering windows.</p>
                   <p>Other highlights of the new stakes schedule include a pair of prep races on the third weekend of the meet. On Friday, October 25 the $150,000 My Trusty Cat will serve as a prelude to the $500,000 <strong>Delta Downs Princess</strong> (Gr. 3) and on Saturday, October 26 the $200,000 Jean Lafitte will be a stepping stone to the $1,000,000 Delta Downs Jackpot. The $500,000 Delta Downs Princess also takes place on the Delta Downs Jackpot program in November.</p>
                   <p>The richest night-time program of the season will take place on Saturday, February 1 when another edition of Louisiana Premier Night takes place. The LAPN card features 10 stakes races for Louisiana-bred horses and over $1 million in total purse money. The highlight of LAPN is the $200,000 Louisiana Premier Night Championship for older horses competing at 1-1/16 miles.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Delta Downs Princess Stakes</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
