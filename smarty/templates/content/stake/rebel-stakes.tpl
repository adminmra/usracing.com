{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1>Rebel Stakes</h1>
</div>
  
  
<div class="content">
    
    
            <p>The Rebel Stakes is a thoroughbred race for three year olds with a purse of $600,000 Guaranteed. Of which, 60% goes to the Owner of the winner, 20% to second, 10% to third, 5% to fourth, 3% to fifth and 2% to be divided equally among all other starters. Weights: 122 lbs. Non-winners of $60,000 at a mile or over in 2014 allowed 3 lbs.; $50,000 at a mile or over anytime allowed 5 lbs.; $35,000 at any distance allowed 7 lbs. (Maiden and Claiming races are not considered.) </p>
                 
        <h2 class="title-custom">Rebel Stakes Race Information </h2>
<p>  Location: 	<a href="/oaklawn-park">Oaklawn Park</a> Race Track, Hot Springs, Arkansas, United States<br />
  Inauguratal Race: 	1961<br />
  Race type: 	Thoroughbred - Flat racing<br />
  Distance: 	11/16 miles (8.5 furlongs)<br />
  Track: 	Dirt, left-handed<br />
  Qualification: 	Three-year-olds<br />
  Weight: 	Assigned<br />
  Purse: 	$600,000<br />
  Grade II race<br />
</p>
        <h2>Kentucky Derby Prep Race</h2>
        <p>The Rebel Stakes is part of the <a href="/kentuckyderby-prepraces">Kentucky Derby Prep Races</a> - Championship Series, where the first four finishers will receive the following points in their race for the roses: 1st - 50points, 2nd - 20points, 3rd - 10points, 4th - 5points<br />
        </p>
        <h2 class="title-custom">Bet on Rebel Stakes</h2>
        <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <h2>News for Rebel Stakes</h2>
                   <p><strong>Kobe's Back versus Tapiture in Rebel Stakes</strong><br />
        March 12, 2014</p>
                   <p>Hot Springs, AR (SportsNetwork) - San Vicente Stakes winner Kobe's Back and local stakes champ Tapiture headline a field of eight 3-year-olds for Saturday's $600,000 Rebel Stakes at Oaklawn Park.</p>
                   <p>The 1 1/16-mile Rebel is a prep for the $1 million Arkansas Derby on April 12 and offers 50 points for the winner toward a place in the Kentucky Derby.</p>
                   <p>Kobe's Back, trained by John Sadler, has drawn the far outside post with Jose Lezcano getting the mount on the gray colt for the first time.</p>
                   <p>Owned by C R K Stable, Kobe's Back made his 2014 debut a winning one with a convincing 5 1/4-length victory in last month's seven-furlong San Vicente at Santa Anita.</p>
                   <p>The gray 3-year-old opened his career at Hollywood Park by winning the 5 1/2- furlong Willard Proctor Stakes on June 15. As the 9-10 favorite, he was second in the Hollywood Prevue in November, but finished 10th the next month in the CashCall Futurity after clipping heels.</p>
                   <p>&quot;I've said all along that he's had three bad starts,&quot; said Lee Searing of C R K Stable after the San Vicente. &quot;He won the first one, got hammered in the second and we never had a chance out in the 12 hole last out. So finally, all the work John has put in and the right rider (has made the difference).&quot;</p>
                   <p>Kobe's Back has taken two of his four starts for $200,250.</p>
                   <p>Tapiture will again be ridden by Ricardo Santana Jr. and break from post 3 for trainer Steve Asmussen.</p>
                   <p>Owned by Winchell Thoroughbreds, Tapiture won the Southwest Stakes at Oaklawn on Feb. 17 by 4 1/4 lengths in his season debut.</p>
                   <p>&quot;It was a big step forward for him,&quot; said Asmussen following the win. &quot;I was impressed with what he did. I saw some things we need to improve and he'll need to continue to improve with the races coming up. I think the series sets up really well for him.&quot;</p>
                   <p>The chestnut colt is on a two-race win streak which includes last November's Kentucky Jockey Club Stakes at Churchill Downs. Tapiture began his career at Saratoga last August with a second-place result. He came back with a third in Churchill Downs' Iroquois Stakes on Sept. 7 and faded to third in a maiden event at Churchill on Oct. 27. He broke his maiden in the Kentucky Jockey Club by the same 4 1/4 lengths as his Southwest win.</p>
                   <p>Tapiture has two wins, a second and two thirds in five starts for $320,838.<br />
                   </p>
        <h2 class="title-custom">Rebel Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/rebel-stakes/winners.tpl"}

                   
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
