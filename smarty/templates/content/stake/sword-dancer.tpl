{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                        
          
<div class="headline"><h1>Sword Dancer Invitational Handicap</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
{include file='/home/ah/allhorse/public_html/graded_stakes/sword_dancer/slider.tpl'}

<h2>Sword Dancer Stakes Betting</h2>
<ul>
<li><strong>Grade:</strong> I for 3-year-olds and up</li>
<li><strong>Distance:</strong> 1 1/2 miles (12 furlongs)</li>
<li><strong>Track:</strong> Turf</li>
<li><strong>Purse:</strong> $1,000,000</li>
</ul>

<p>
The Sword Dancer Invitational Handicap is an American race for thoroughbred horses, aged three and up, run annually in mid August at <a href="/saratoga">Saratoga Race Course</a> in Saratoga Springs, New York. A prep for the <a href="/breeders-cup/turf">Breeders' Cup Turf</a>, it is set at a distance of one and one-half miles (12 furlongs) on the turf. A Grade I event, the race currently offers a purse of $1,000,000 in 2015.</p>

<p>The Sword Dancer is named for the best three-year-old colt or gelding of 1959, best three-year-old, best handicap horse, and the American Horse of the Year. Sword Dancer was elected to the United States Racing Hall of Fame in Saratoga Springs, New York in 1977.</p>

<p>The event was inaugurated at <a href="/aqueduct">Aqueduct Racetrack</a> in 1975 as a six furlong sprint on dirt for three-year-old horses. Beginning in 1977, it was hosted by <a href="/belmont-park">Belmont Park</a> then in 1992 was moved to the Saratoga Race Course. 


</p>


{include file='/home/ah/allhorse/public_html/graded_stakes/sword_dancer/video.tpl'}

        
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/sword_dancer/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    