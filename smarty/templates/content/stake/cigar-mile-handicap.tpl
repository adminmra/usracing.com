{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Orb pointed to Cigar Mile Handicap</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Cigar Mile Handicap</h1>
                 <div class="news-date">October 14, 2013</div>
                 
                   <p>Working toward a possible start in the Cigar Mile Handicap, <a href="/kentucky-derby">Kentucky Derby</a> winner Orb breezed a half-mile in :49.77 on the main track at Belmont Park Oct. 13.</p>
                   <p>It was the first work for Orb since finishing last of eight in the Jockey Club Gold Cup Invitational Sept. 28 at Belmont. The Stuart Janney III and Phipps Stable homebred will bypass the Breeders' Cup but could return in the Cigar Mile Nov. 30 at Aqueduct Racetrack.</p>
                   <p>&quot;I thought he worked fine,&quot; Hall of Fame trainer Shug McGaughey said of the Malibu Moon colt. &quot;I asked him for :50, he went in :49 and change, 25 and change, 24 and change, and galloped out in 1:02. I'm trying to get him started again and go from there.&quot;</p>
                   <p>Orb has run twice at a mile, finishing fourth in a maiden race at Belmont a year ago and breaking his maiden at Aqueduct at the distance last November.</p>
                   <p>&quot;I'll make up my mind in a couple of weeks,&quot; McGaughey said. &quot;I'll just fool with him and see where it takes us.&quot; </p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Cigar Mile Handicap</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
