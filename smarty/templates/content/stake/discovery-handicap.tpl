{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Called to Serve turns tables in Discovery Handicap at Aqueduct</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Discovery Handicap</h1>
                 <div class="news-date">Archives: November 17, 2012</div>
                 
                   <p>Called to Serve, who had been knocking on the door by finishing in the money in three consecutive starts, kicked it down with a triumph in Saturday&rsquo;s Grade 3, $150,000 Discovery Handicap, New York&rsquo;s final graded stakes of the season for 3-year-old males at Aqueduct.</p>
                   <p>Called to Serve, ridden for the first time by Joel Rosario, draws clear in the Discovery.</p>
                   <p>Beaten a head by odds-on choice Willy Beamin in the Oklahoma Derby for former trainer Peter Eurton, Called to Serve gained revenge with a comprehensive victory from off the pace in his second start for Nick Canani, and the gelding by Afleet Alex did it on the square.</p>
                   <p>Willy Beamin was able to set up shop on an easy lead through moderate fractions of 24.63 seconds, 49.36 and 1:13.29, and appeared to be holding all the cards. But Called to Serve began picking off horses on the outside turning for home with new pilot Joel Rosario, caught the favorite just outside the eighth pole, and drew off with every stride to win by 4 3/4 lengths.</p>
                   <p>&ldquo;It was easy for me,&rdquo; said Rosario. &ldquo;I just had to put him in the right spot. He was all game today. Every time I asked him, he was there. I like to ride that kind of horse.&rdquo;</p>
                   <p>Called to Serve paid $9.60 as the second choice in the six-horse field, and ran the 1 1/8 miles in 1:49.77 over a fast main track. The Discovery was his first stakes win, and upped his record to 3-1-3 from 10 starts.</p>
                   <p> &ldquo;He came to New York and ran in the Temperence Hill as a prep for this,&rdquo; said Canani. &ldquo;I was a bit concerned when I saw the slow fractions, but I can only control my own horse. The last three weeks he&rsquo;s made great strides, put on weight, filled out, and he was doing super. Around the turn he was in the clear and didn&rsquo;t have any traffic trouble.&rdquo;</p>
                   <p>&nbsp;</p>
                   <h2>Discovery Handicap Video</h2>
                   <p>Nov 17, 2012 -- Called to Serve stormed from last to first to earn his first graded stakes victory in Saturday's 68th running of the Grade 3, $150,000 Discovery Handicap at Aqueduct Racetrack. Ridden by Joel Rosario, the 3-year-old gelded son of Afleet Alex sat patiently behind tepid fractions of 24.63, 49.36 and 1:13.29 set by 3-4 favorite Willy Beamin, then launched a four-wide rally through the stretch to take command before the eighth pole. His winning time for the 1 1/8 miles over a fast track was 1:49.77.</br>
                   <p>
                   
                   <p>
                     <iframe class="video" src="//www.youtube.com/embed/Cjl8t1MEAwU?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>
                   <p>&nbsp;</p>
                     <p>&nbsp;</p>
                   <h2>Bet on Discovery Handicap</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>                     Location: 	<a href="/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance:	1 1/2 miles (9 furlongs)<br />
                     Track:	 	Dirt, left-handed<br />
                     Qualification: 	Three-year-olds<br />
                     Weight: 	Gr. III<br />
                     Purse: 		$100,000</p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/discovery-handicap/winners.tpl"}          

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
