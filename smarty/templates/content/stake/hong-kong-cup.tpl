{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Special Races</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/royal-ascot" title="">ASCOT</a></li>
<li><a href="/breeders-cup" title="">BREEDERS&#039; CUP</a></li>
<li><a href="/dubai-world-cup" title="">DUBAI WORLD CUP</a></li>
<li class="leaf active-trail"><a href="/hong-kong-cup" title="" class="active">HONG KONG CUP</a></li>
<li><a href="/kentucky-oaks" title="">KENTUCKY OAKS</a></li>
<li><a href="/melbourne-cup" title="">MELBOURNE CUP</a></li>
<li><a href="/prix-de-larc-de-triomphe" title="">PRIX DE L&#039;ARC DE TRIOMPHE</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9"><!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Hong Kong Cup - Happy Valley</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The Emirates World Series Racing Championship is one of the most exciting prospects for thoroughbred racing, where the best horses in the world compete at the best racetracks and in the best races to establish a global champion.</p>
<p>The race was run on December 14th, 2009 and was won by Vision d'Etat. The 2008 Prix du Jockey Club winner travelled strongly throughout, and his rivals had no hope once winning jockey Oliver Peslier asked his mount to quicken shortly after turning for home.</p>
<p>First launched in March 1999, the inaugural competition brought together horses from the United States, Canada, New Zealand, Australia, Japan, Hong Kong, Germany, France, Ireland and Great Britain in a nine-race series to decide which horse truly befits the title of world champion.</p>
<p>In the second year of the Series, two more races were added so that the the Emirate World Series&nbsp;now comprises 11 races spanning both hemispheres and held in nine countries and over four continents.</p>
<p>Each leg of the Emirates World Series Racing Championship is beamed into over 200 countries worldwide reaching an estimated audience of between 1.5 and 2 billion viewers.</p>
<p>The Emirates World Series is a global competition that will continue to develop and grow, bringing pleasure and reward to all those involved, players and punters alike.</p>
<p>The Hong Kong Cup was initially run as the Hong Kong Invitation Cup in January 1988 when the overseas entries were confined Malaysia and Singapore. The race was granted Group3 status in 1993, Group 2 in 1994, and it became the first ever race in Hong Kong with full international Group 1 ranking in Jim And Tonic's year - 1999 - when it also emerged as the final leg of the Emirates World Series.</p>
<p>In 1999,&nbsp;the Cup's distance was increased to 2000m from 1800m and, at the time,&nbsp;the Hong Kong Cup became the richest race in the world on turf over 2000m at HK$18 million (approx. US$2.3 million, GB¢G1.56 million, Aus$4.5 million).</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
