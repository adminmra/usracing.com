{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->


{include file='menus/stakes.tpl'}


<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


                                       
              
          
          
<div class="headline"><h1>Illinois Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Illinois Derby&nbsp;</h2>
<ul>
<li><strong>Stakes:</strong> Grade II for 3-year-old thoroughbreds </li>
<li><strong>Distance:</strong> 1 1/8 miles </li>
<li><strong>Track:</strong> Dirt </li>
<li><strong>Purse:</strong> $500,000</li>
</ul>
<p>The Illinois Derby was first run in 1923 at Hawthorne Race Course located in Stickney/Cicero, Illinois just west of Chicago. After being run at Aurora in 1938, it was discontinued and was only revived at Sportsman's Park in 1963.</p>
<p>Apparently, horse racing handicappers convinced Bill Johnston, the late Sportsman's Park president, into resurrecting the 1 1/8-mile race.</p>
<p>"Good idea, Dave," Johnston said when it was first suggested in 1960. But his racing secretary, the late Robert P. McAuliffe, said, "Feldman is crazy. This race won't draw flies at 1 1/8 miles."</p>
<p>The purse, at that time, was only $25,000, but a full field competed, and Lemon Twist won it before a packed house.</p>
<p>The Illinois Derby was such a success that owners and trainers started to ship horses from everywhere. The race has grown to be one of the richest for 3-year-olds.</p>
<p>Since 2001, the Illinois Derby is a prep race to the <a title="Bet the Kentucky Derby!" href="/kentucky-derby">Kentucky Derby</a>. Prior to that it was run between the Kentucky Derby and the <a title="Bet the Preakness Stakes!" href="/preakness-stakes">Preakness Stakes</a>. The 2002 Illinois Derby won by War Emblem was the last one run at Sportsman's Park Racetrack.</p>
<p>&nbsp;</p>

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->




      
    