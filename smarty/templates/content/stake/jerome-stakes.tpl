{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Jerome Stakes Contenders Tight Field</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Jerome Stakes <br />
</h1>
                 <div class="news-date">January 2, 2014</div>
                 
                   <p><strong>Location:</strong> <a href="/aqueduct">Aqueduct</a> Racetrack, Queens, New York, United States <br />
                     <strong>Inaugurated: </strong>1866 <br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing <br />
                     <strong>Distance:</strong> 1 mile 70 yards <br />
                     <strong>Track: </strong>Dirt, left-handed <br />
                     <strong>Qualification:</strong> Three-year-olds <br />
                     <strong>Weight: </strong>Assigned <br />
                     <strong>Purse: </strong>$200,000 added <br />
                     <strong>Grade:</strong> II</p>
                   <p><strong>Race Type: </strong><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Race</a> </p>
                   <p>Road to the Roses Points: 1st=10 points; 2nd=4 points; 3rd=2 points; 4th=1 point</p>
                   <p>The race is a Road to the <a href="/kentucky-derby">Kentucky Derby</a> Prep Season qualifying race. The winner receives 10 points toward qualifying for the Kentucky Derby.</p>
                   <p>The Jerome is the second oldest stakes race in the country. The Jerome was first run at Jerome Park from 1866-1889, then at Morris Park Racecourse until 1905, at Belmont Park from 1906-1959 and 1968-2009, and also at Aqueduct Racetrack in 1960, from 1962-1967 and in 2011. There was no race run from 1910 to 1913 and there were two divisions from 1866 to 1870.</p>
                   <p>New York's opening stakes for 3-year- olds of 2014, Saturday's $200,000 Jerome at Aqueduct, has attracted a field of eight for the mile and 70 yard contest.</p>
                   <p>Breaking from the inside post will be Noble Moon, third in the Nashua Stakes at Aqueduct on Nov. 3. Trained by Leah Gyarmati for Treadway Racing Stable, Noble Moon will be ridden by Irad Ortiz Jr. for the first time.The colt was beaten three lengths in the <a href="/nashua-stakes">Nashua Stakes</a> by Cairo Prince after winning his debut race by setting the entire pace. The 3-year-old has earned $73,000 from his two starts.</p>
                   <p>Lawmaker will be coming into the Jerome after winning his initial start at <a href="/laurel-park">Laurel Park</a>. The colt, trained by Chad Brown, will start from post 3 with Jose Ortiz riding.Owned by John Gunther, Lawmaker won a maiden special weight race on Thanksgiving Day by a length after rallying down the stretch. The win was worth $22,800.</p>
                   <p>Owner/trainer Lisa Guerrero has entered Classic Giacnroll for the colt's second try at a stakes. Classic Giacnroll and jockey Alex Solis will start from post 7.On Dec. 7, Classic Giacnroll finished third in the Marylander Stakes at Laurel Park over a muddy track. The colt was coming off consecutive wins after finishing fourth and second, respectively, in his first two career starts. Classic Giacnroll has banked $78,100.</p>
                   <p>Here is the full field for the Jerome in post position order: Noble Moon, Irad Ortiz Jr.; Scotland, David Cohen; Lawmaker, Jose Ortiz; Pin and Win, Abel Castellano Jr.; Matuszak, Rajiv Maragh; Mental Iceberg, Angel Arroyo; Classic Giacnroll, Alex Solis and Master Lightning, Cornelio Velazquez.</p>
                   <p>The Jerome, won last year by Vyjack, has an approximate post-time Saturday of 3:48 p.m. (ET). </p>
                   <p><br />
                   </p>
                   <h2>Bet on Jerome Stakes<br />
                                      </h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Jerome Stakes Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/jerome-stakes/winners.tpl"} 
        

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
