{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Woodbine offers horse racing's Autumn Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<article class="article">




<h1 class="news-date">Autumn Stakes</h1>
                 <div class="news-date">November 5, 2013</div>
                 <p>Location: <a href="/woodbine">Woodbine</a> Racetrack<br />
                     Distance: 1 1/16 miles (8.5 furlongs) <br />
                     Track: Polytrack, left-handed <br />
                     Qualification: Three-year-olds and up <br />
                     Weight: Assigned, allowances <br />
                     Purse: $150,000 <br />
                     Grade II Stakes Race</p>
                 <p>Woodbine Entertainment Group (WEG) announced Woodbine&rsquo;s entire 2013 Thoroughbred stakes calendar. A total of 107 stakes races, worth over $20.25 million, are scheduled.</p>
              

                   <p>The first portion of the stakes calendar, which features 37 added-money races through July 7, including the date of the 154th Queen&rsquo;s Plate, was announced in March.</p>
                   <p>Key features in the &ldquo;second half&rdquo; of the schedule include traditional late season spotlight events, the Ricoh Woodbine Mile and Pattison Canadian International.</p>
                   <p>The Grade 1, $1 million Ricoh Woodbine Mile is set for Sunday, September 15 this year. The Mile is once again a Breeders&rsquo; Cup &ldquo;Win and You&rsquo;re In&rdquo; event (Turf Mile), as is its filly and mare companion on the date, the Grade 2, $300,000 Canadian Stakes (Filly &amp; Mare Turf).  Also on the power-packed card is the Grade 1, $300,000 Northern Dancer Stakes and the Grade 3, $150,000 Ontario Derby.</p>
                   <p>Many racing fans will remember Wise Dan crushing a strong field in the Mile last year en route to a multiple Eclipse Award-winning campaign, including Horse of the Year.</p>
                   <p>&ldquo;The Ricoh Woodbine Mile is a premier event in the Thoroughbred racing world,&rdquo; said Steve Koch, WEG&rsquo;s Vice-President of Thoroughbred Racing, &ldquo;Wise Dan obviously reinforces that distinction. We anticipate offering another brilliant field of world-class horses for the Mile, as well as the Northern Dancer, the Canadian and Ontario Derby that day.&rdquo;</p>
                   <p>The Grade 1, $1 million Pattison Canadian International and its filly and mare complement, the Grade 1, $500,000 E.P. Taylor Stakes has found a later spot on the calendar. The pair is now set for Sunday, October 27.</p>
                   <p>&ldquo;The later date allows every opportunity for runners coming out of major U.S. and European races to take advantage of Woodbine&rsquo;s turf course,&rdquo; said Koch.  &ldquo;Those that run well here usually move on to other races besides the Breeders&rsquo; Cup later in the year, such as Hong Kong or Japan.&rdquo;</p>
                   <p>Three other &ldquo;Win and You&rsquo;re In&rdquo; races are set for Woodbine. The Grade 1, $300,000 Nearctic Stakes returns as a WAYI race (Turf Sprint) and is set for Sunday, October 13, as does the Grade 2 two-year-old turf events, the $200,000 Natalma Stakes (Juvenile Fillies Turf) and $200,000 Summer Stakes, presented by TVG (Juvenile Turf). Both are set for Saturday, September 14.</p>
                   <p>The third and turf jewel of Canada&rsquo;s Triple Crown, the $500,000 Breeders&rsquo; Stakes, is set for Sunday, August 18 at Woodbine. The Prince of Wales, the middle gem, is scheduled for Tuesday, July 30 at Fort Erie.</p>
                   <p>&nbsp;</p>
                   <p>&nbsp;</p>
<h2>Autumn Stakes Video</h2>
 
    <iframe class="video" src="//www.youtube.com/embed/xrQ_wW2bN4c?rel=0" frameborder="0" allowfullscreen></iframe>

</article>
    <p>&nbsp;</p>    
    <p>&nbsp;</p>
                  
                   </p>
                   <h2>Bet on Autumn Stakes</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/autumn-stakes/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
