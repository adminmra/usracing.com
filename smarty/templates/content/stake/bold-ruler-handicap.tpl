{include file='inc/left-nav-btn.tpl'}
{assign  var="stakesname" value="Bold Ruler Handicap"} 
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Buffum Sprints to Victory in Bold Ruler</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Bold Ruler Handicap</h1>
                 <div class="news-date">Archives: October 2012</div>
                 <p>Godolphin Racing's Buffum came out running and never stopped as he posted a gate-to-wire score in the $150,000 Bold Ruler Handicap (gr. III) at Belmont Park Oct. 27.</p>
                   <p>Buffum, a 4-year-old Bernardini   colt, shot to the lead from the inside post and ticked off fractions of :22.55 and :45.24 while being tracked intently by Associate and Little Drama on the far outside. Facing a serious challenge from those two rivals straightening for home, Buffum found an extra gear inside the eighth pole. He held well for a 1 1/4-length victory over Little Drama, with Associate finishing third, another half-length back.</p>
                   <p>It was the first stakes victory for Buffum, whose time for the seven furlongs was 1:21.68 over a fast track.</p>
                   <p>&quot;He broke good and put himself right there, it was the only choice,&quot; said winning jockey Eddie Castro.</p>
                   <p>The victory was the second in three 2012 starts for Buffum, who was coming off a 3 1/2-month layoff following a sixth-place finish in the Suburban Handicap (gr. II) at Belmont. He began the year with an eight-length score at Belmont in May in which he recorded a career-best 105 Beyer speed figure at a mile.</p>
                   <p>&nbsp;</p>
                   <h2><strong>Bet on Bold Ruler Handicap</strong></h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>Location: 	<a href="/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance: 	6 furlong sprint<br />
                     Track: 	Dirt, left-handed<br />
                     Qualification: 	Three-years-old &amp; up<br />
                     Weight: 	Assigned<br />
                     Purse: 	$150,000</p>
                   <p>&nbsp;</p>
                   <h2>Remsen Stakes Video</h2>
                   <p>Nov 9, 2011

-- Calibrachoa captured his fifth race in as many starts at Aqueduct Racetrack with a victory in Sunday's Grade 3, $100,000 Bold Ruler Handicap.

Making his first start at the Big A since March, Calibrachoa prompted pacesetter Rule by Night through opening fractions of 22.29 and 45.25 seconds, claimed a narrow lead at the quarter pole, and shook clear of his rivals in the stretch to prevail by 2 3/4 lengths over Sangaree. </p>
                   <p>&nbsp;</p>
                   <iframe class="video" src="//www.youtube.com/embed/98UO6DEwRow?feature=player_detailpage" frameborder="0" allowfullscreen></iframe>

                 <p><br />
                   </p>
                 <p>&nbsp;</p>
                 <h2>Past Winners of the Bold Ruler Handicap</h2>
                 
                 {* <p><br />
                   2012	Buffum<br />
                   2011	Calibrachoa<br />
                   2010	Bribon<br />
                   2009	Le Gran Cru<br />
                   2008	Lucky Island<br />
                   2007	Songster<br />
                   2006	Tiger<br />
                   2005	Uncle Camie<br />
                   2004	Canadian Frontier<br />
                   2003	Shake You Down<br />
                   2002	Left Bank<br />
                   2001	Say Florida Sandy<br />
                   2000	Brutally Frank</p>
                 <p>&nbsp;</p>
                 <p>&nbsp;</p>
               *}

               <table id="sortTable" class="table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
                 cellspacing="0" border="0" title="Past Winners of the Bold Ruler Handicap">
                 <thead>
                   <tr>
                     <th><strong>Year</strong></th>
                     <th><strong>Winner</strong></th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr>
                     <td>2012</td>
                     <td>Buffum</td>
                   </tr>
                   <tr>
                     <td>2011</td>
                     <td>Calibrachoa</td>
                   </tr>
                   <tr>
                     <td>2010</td>
                     <td>Bribon</td>
                   </tr>
                   <tr>
                     <td>2009</td>
                     <td>Le Gran Cru</td>
                   </tr>
                   <tr>
                     <td>2008</td>
                     <td>Lucky Island</td>
                   </tr>
                   <tr>
                     <td>2007</td>
                     <td>Songster</td>
                   </tr>
                   <tr>
                     <td>2006</td>
                     <td>Tiger</td>
                   </tr>
                   <tr>
                     <td>2005</td>
                     <td>Uncle Camie</td>
                   </tr>
                   <tr>
                     <td>2004</td>
                     <td>Canadian Frontier</td>
                   </tr>
                   <tr>
                     <td>2003</td>
                     <td>Shake You Down</td>
                   </tr>
                   <tr>
                     <td>2002</td>
                     <td>Left Bank</td>
                   </tr>
                   <tr>
                     <td>2001</td>
                     <td>Say Florida Sandy</td>
                   </tr>
                   <tr>
                     <td>2000</td>
                     <td>Brutally Frank</td>
                   </tr>
                 </tbody>
               </table>

            
            
 
 
          
            
            

{include file="/home/ah/allhorse/public_html/graded_stakes/bold-ruler-handicap/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='inc/rightcol-calltoaction-stakes.tpl'*}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
