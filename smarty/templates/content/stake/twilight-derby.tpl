{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Del Mar Derby winners could meet in Twilight Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Twilight Derby</h1>
                 <div class="news-date">September 2, 2013</div>
                 
                   <p>Ethnic Dance and Gabriel Charles, who won divisions of the Grade 2 Del Mar Derby on Sunday, are likely to face off in the Grade 2 Twilight Derby at Santa Anita on Nov. 1, their trainers said Monday morning.</p>
                   <p>Gabriel Charles, who closed sharply to win the first division, was making only his second start since December, when he fractured his pelvis. His only start since then was on opening day of the Del Mar meet, July 17, when he was an unlucky loser of a division of the Oceanside Stakes. He skipped the La Jolla on Aug. 10 by design.</p>
                   <p>&ldquo;The longer time between races the better for him,&rdquo; said his trainer, Jeff Mullins.</p>
                   <p>Ethnic Dance scored a front-running victory in the second division of the Del Mar Derby. It was his first start against open company and in a stakes race. He has run five times in the last four months.</p>
                   <p>&ldquo;He&rsquo;s done a lot,&rdquo; said his trainer, John Sadler. &ldquo;I don&rsquo;t want to run him in anything early at Santa Anita. I&rsquo;m inclined to give him some spacing.&rdquo;<br />
                   </p>
                   <p>&nbsp;</p>
                   <h2>Bet on Twilight Derby</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   
                   <h2>Stakes Race Details</h2>
                   <p>Location: 	<a href="/santa-anita-park">Santa Anita Park</a><br />
                     Distance: 	1 1/8 miles<br />
                     Track: 		Turf<br />
                     Qualification: 	Fillies &amp; Mares, three-years-olds<br />
                     Weight: 	Gr. II<br />
                     Purse: 		$150,000<br />
                   </p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
