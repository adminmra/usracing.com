{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Santa Anita Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Santa Anita Derby</h2>
<ul>
<li><strong>Stakes:</strong> Grade I for 3-year-old thoroughbreds </li>
<li><strong>Distance:</strong> 1 1/8 miles </li>
<li><strong>Track:</strong> Dirt </li>
<li><strong>Purse:</strong> $750,000</li>
</ul>
<p>The <strong>Santa Anita Derby</strong> is considered the most important 3-year old race in the West run at&nbsp;Santa Anita Park&nbsp;in Arcadia, California.</p>
<p>The Santa Anita Derby has a rich history as an important prep race for the <a title="Kentucky Derby" href="/kentucky-derby"><strong>Kentucky Derby</strong></a>, which leads to the Triple Crown big event for thoroughbred horse racing. It has yielded Kentucky Derby winners such as <strong>Silver Charm</strong>, <strong>Sunday Silence</strong>, and <strong>Winning Colors</strong> plus Triple Crown winner <strong>Affirmed</strong>.</p>
<p>With a total purse of US$750,000, the Santa Anita Derby is a Grade I stakes race for three year old thoroughbred horses, run at a distance of 1 1/8 miles (9 furlongs) on the dirt. The Santa Anita Derby stands out with the third highest number of winners - at a total of 14 - who have made it to the Kentucky Derby.</p>

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    