{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>In Trouble is on point for the Nashua Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Nashua Stakes</h1>
                 <div class="news-date">September 29, 2013</div>
                 <p>There were five 2-year-olds entered in the $196,000 Futurity Stakes Sept. 29 at Belmont Park, but it turned out to be a two-horse race as fast-developing In Trouble topped favored Corfu for a comfortable 2 1/4-length victory. </p>
                   <p>With Joe Rocco Jr. aboard, In Trouble ran six furlongs in 1:09.64, nearly a second faster than 2-year-old fillies ran in the Matron Stakes (gr. II) a race earlier. </p>
                   <p>In Trouble, winner of a key maiden special weight heat Aug. 10 at Saratoga Race Course in his debut, is now 2-for-2 for trainer Tony Dutrow and owner Team D. </p>
                   <p>&quot;Having done this a long time, I was looking forward to today,&quot; Dutrow said. &quot;I was feeling that we have a talented horse and I'm not surprised at what I saw. I wasn't predicting this, but I'm not surprised. He's pretty good.&quot; </p>
                   <p>Leaving from the rail, In Trouble, the 5-2 second choice, had the lead out of the gate before overtaken by Corfu, the 9-5 pick with John Velazquez aboard. Corfu, who stumbled a bit out of the gate, clipped off quick quarter mile splits of :22.09 and :44.75 while opening a 2 1/2-length advantage over In Trouble as the rest of field fell in behind. </p>
                   <p>In Trouble responded to urging from Rocco to challenge on the outside in the stretch, reached even terms with the Corfu past the furlong marker. He took over with a sixteenth of a mile to run and was kept to his task by Rocco while widening his lead to the wire. </p>
                   <p>Rocco said he was able to get In Trouble, a son of Tiz Wonderful , to rate kindly before launching the winning bid. </p>
                   <p>&quot;He broke really sharp, and I'd rather be outside dictating when we can move than be inside and be under someone else who can force us,&quot; Rocco said. &quot;It worked out when I saw I could get behind (Corfu) and get outside of him, that's when I went outside and did it.&quot; </p>
                   <p>Dutrow said In Trouble is to point for the Nashua Stakes Nov. 3 at Aqueduct Racetrack. </p>
                   <p>&quot;This was the second race of his life and he was able to relax and give up the lead and finish up against quality horses,&quot; Dutrow said. &quot;I think that's a big statement for him.&quot; </p>
                   <p>&nbsp;</p>
                   <h2>Bet on Nashua Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Stakes Race Details</h2>
                   <p>Location: <a href="/racetrack/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance: 1 mile (8 furlongs) <br />
                     Track: Dirt, left-handed <br />
                     Qualification: Two-year-olds <br />
                     Weight: Assigned <br />
                     Purse: $100,000-added </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/nashua-stakes/winners.tpl"} 
      

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
