{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Phil's Dream prevails in Nearctic Stakes at Woodbine</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Nearctic Stakes</h1>
                 <div class="news-date">October 13, 2013</div>
                 
                   <p>The late bloomer Phil&rsquo;s Dream ($20) emerged from the pack in the stretch to win the $340,400 <strong>Nearctic Stakes</strong> going away under Justin Stein at Woodbine on Sunday.</p>
                   <p>The Grade 1 Nearctic is a Breeders&rsquo; Cup Win and You&rsquo;re In qualifier for the BC Turf Sprint on Nov. 2 at Santa Anita, but Phil&rsquo;s Dream is not BC-eligible, and owner/trainer Paul Buttigieg said he has no plans to supplement the 5-year-old gelding to the race.</p>
                   <p>Phil&rsquo;s Dream raced in fifth on the turn in the six-furlong turf sprint while Fatal Bullet set fractions of 22.80 and 46.47 seconds, with Kentucky shipper Longhunter in close proximity. Phil&rsquo;s Dream was blocked briefly early in the stretch before closing strongly to prevail by two lengths, in 1:10.83, over a rain-softened course labeled &ldquo;good.&rdquo; </p>
                   <p>&ldquo;When he found room, he exploded,&rdquo; Buttigieg said. &ldquo;He&rsquo;s a good horse.&rdquo;</p>
                   <p>Mr. Online rallied for second, and Longhunter weakened to finish third. Excaper, the lukewarm 3-1 favorite, checked in ninth in a field reduced to 10 with the scratch of Tripski.</p>
                   <p>For notching his sixth win in eight starts this year, Phil&rsquo;s Dream earned $216,000 and boosted his career earnings to $511,179.</p>
                   <p>&nbsp;</p>
                   <p><strong>Bet on Nearctic Stakes</strong></p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet onStakes Races today!</p>
                   <p>&nbsp;</p>
              
            
            
            
 
 
          
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
