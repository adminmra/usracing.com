{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
          
<div class="headline"><h1>San Gabriel Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>&nbsp;</p>
                 <p><strong>Location:</strong> Santa Anita Park, Arcadia, California, United States <br />
                     <strong>Inaugurated: </strong>1935 <br />
                     <strong>Race type:</strong> Thoroughbred - Flat racing <br />
                     <strong>Distance:</strong> 1 1/2 miles (9 furlongs) <br />
                     <strong>Track: </strong>Grass, left-handed <br />
                     <strong>Qualification: </strong>Three-years-old &amp; up <br />
                     <strong>Weight:</strong> Assigned <br />
                     <strong>Purse:</strong> $150,000 <br />
                     <strong>Grade: </strong>II </p>
               <p>&nbsp;</p>
                   <h2>Bet on San Gabriel Stakes</h2>
                   <p>&nbsp;</p>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
                   <p><br />
                 </p>
                 <h2>2014 San Gabriel Stakes Contenders</h2>
                    
                   <p>Here is a list of Contenders for the 67th running Of San Gabriel Stakes at Santa Anita Park, with $200,000 Guaranteed</p>
                   <p>Arctic North<br />
                     Big Bane Theory<br />
                     Bio Pro<br />
                     Black Spirit<br />
                     Den's Legacy<br />
                     Dreamy Kid<br />
                     Drill<br />
                     Dubai You X Y Z<br />
                     El Commodore<br />
                     Empty Headed<br />
                     Fire With Fire<br />
                     Huntsville<br />
                     Jeranimo<br />
                     Si Sage<br />
                     Slim SHady (GB)<br />
                     Te Rapa<br />
                     Utopian<br />
                     Willyconker (IRE)</p>
                   <p>&nbsp;</p>
                   <h2>Jeranimo Wins the 2013 San Gabriel Stakes</h2>
                   <p>&nbsp;</p>
                   <p>B.J. Wright's Jeranimo propelled himself from last to first with a blistering turn of foot in Sunday's Grade 2, $150,500 San Gabriel Stakes at Santa Anita, regaining the title he first won in 2010. With Rafael Bejarano aboard, the Michael Pender veteran inhaled pacesetter Chosen Miracle inside the final furlong and kicked away to score by an emphatic 2 3/4 lengths. </p>
                   <p>Jeranimo benefited from a fast pace that set up his trademark late kick, a scenario that had been lacking in last year's San Gabriel. That running was actually his title defense, for the turf feature was not run in 2011, having been moved from its late December spot to January 16, 2012. With no pace on tap at all, Jeranimo uncharacteristically found himself on the early lead, and he was outfinished by Norvsky. In his third straight appearance in the San Gabriel on Sunday, there was no such problem. </p>
                   <p>The speedy Chosen Miracle bounded right to the front and established quick splits of :22 4/5, :46 3/5 and 1:10 4/5. Fast Track was his nearest pursuer, followed by Temple's Door and Tale of a Champion. Parked back in fifth was 3-2 favorite Slim Shadey, who crept closer approaching the far turn. Juniper Pass raced in the penultimate spot, and Jeranimo, the 2-1 second choice, was happy to bring up the rear. </p>
                   <p>Turning into the stretch, Chosen Miracle hung tough and was still on the lead at the mile mark in 1:35. Slim Shadey's brief run into contention flattened out, but Jeranimo was just beginning to hit top gear. The seven-year-old rapidly built up a head of steam and blasted right on by, finishing 1 1/8 miles on the firm turf in 1:46 2/5 and returning $6, $3.80 and $2.60. </p>
                   <p>&nbsp;</p>
                   <h2>San Gabriel Handicap Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/san-gabriel-stakes/winners.tpl"}

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
