{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Battle Heats Up in Tempted Stakes</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h1 class="news-date">Tempted Stakes</h1>
                 <div class="news-date">October 24, 2013</div>
                 <p>The 2012 winner of the Tempted Stakes, My Happy Face, won her first stakes after two tries when she won the Tempted Stakes (gr. III) at Aqueduct with jockey Ramon Dominguez. The 2-year-old filly by Tiz Wonderful went off at odds of 1-4 for the win.</p>
                   <p>My Happy Face battled with Cue the Moon for most of the race and the winning decision came within a head. Ruby Lips finished third with Unabashed in fourth.</p>
                   <p>The filly won in a time of 1:36.32. She was trained by Rudy Rodriguez and owned by Michael Dubb.  She is out of the Siberian Summer mare Summer Star and was bred by Spendthrift Farm and Shawhan Place.</p>
                   <p>My Happy Face is the 9-to-5 morning line choice for teh 2013 Charles Town Oaks. She made her seasonal debut in January&rsquo;s Forward Gal (G2) at Gulfstream where she finished second to Kauai Katie, then won the listed Lotka Stakes at Belmont Park before posting a runner-up effort behind divisional leader Princess of Sylmar in the Coaching Club American Oaks (G1). Most recently, she endured a wide trip and came up a neck short as the 2.00-to-1 second choice in the Test (G1) at Saratoga.</p>
                   <p><br />
                   </p>
                   <h2>Bet on Tempted Stakes</h2>
                   <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing.  US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita.  Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA. <a href="/signup/">Join</a> US Racing and bet on Stakes Races today!</p>
                   <p>&nbsp;</p>
                   <h2>Stakes Race Details</h2>
                   <p>Location <a href="/racetrack/aqueduct">Aqueduct</a> Racetrack<br />
                     Distance 1 mile (8 furlongs) <br />
                     Track Dirt, left-handed <br />
                     Qualification Two-year-old fillies </p>
                   <p>&nbsp;</p>
                   <h2>Past Winners</h2>
{include file="/home/ah/allhorse/public_html/graded_stakes/tempted-stakes/winners.tpl"}
        

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
