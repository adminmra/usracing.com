{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
        
          
          
                  {include file='menus/racetracks.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline">
  <h1>Tampa Bay Derby<br />
  </h1>
</div>
  
  
<div class="content">
    
    
            <p>The Tampa Bay Derby has a purse of $350,000 Guaranteed, with 60% going to the winner, 20% to the second place finisher, 10% to third, 5% to fourth, 3% to fifth and 2% to sixth.</p>
                 
                   <p>This field will be limited to twelve starters. Nominations closed two weeks before the race. If more than 12 horses are nominated, the twelve starters will be determined with preference given to Grade I, II or III Stakes winners in that order, then those that have accumulated the highest career earnings.<br />
        </p>
        <h2 class="title-custom">Tampa Bay Derby 
                   Race Information </h2>
<p>Location: 	Tampa Bay Downs, Tampa, Florida, United States<br />
  Inaugurated: 	1981<br />
  Race type: 	Thoroughbred - Flat racing<br />
  Distance: 	11/16 miles (8&frac12; furlongs)<br />
  Track: 	Dirt, left-handed<br />
  Qualification: 	Three-year-olds<br />
  Weight: 	Assigned<br />
  Purse: 	$350,000<br />
  Grade: II</p>
<h2>Kentucky Derby Prep Race</h2>
<p>  The Tampa Bay Derby is part of the <a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a> - Championship Series, where the first four finishers will receive the following points in their race for the roses: 1st - 50points, 2nd - 20points, 3rd - 10points, 4th - 5points<br />
</p>
<h2>Tampa Bay Derby Race Facts:</h2>
        <p>Fastest Time: 1:43:11  Street Sense (2007)<br />
        Largest Winning Margin: 5 lengths</p>
        <h2 class="title-custom">Bet on Tampa Bay Derby<br />
        </h2>
        <p>US Racing features race analysis, interviews, free handicapping tips and stories on the trainers, jockeys, owners and horses that make up the colorful sport of horse racing. US Racing features the best tracks in the United States, including Churchill Downs, Belmont, Pimlico, Saratoga, Del Mar and Santa Anita. Internationally, US Racing offers content from Australia, Dubai, Great Britain, Ireland and Japan.</p>
                   <p>US Racing - The Official OTB of the USA.<a href="/signup/"> Join</a> US Racing and bet on Stakes Races today!</p>
        <h2 class="title-custom">Tampa Bay Derby 
                   Past Winners</h2>
<p>Winners of the Tampa Bay Derby</p>
{include file="/home/ah/allhorse/public_html/graded_stakes/tampa-bay-derby/winners.tpl"}

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">

{include file='inc/rightcol-calltoaction-stakes.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->

    
      
   
