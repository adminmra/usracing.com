{* Instructions
	Add Stakes name and Path Variables - They automatically fill set the name and location of files.
	Grab Content from Wikipidia for track info
	On Allhorse duplicate template directory and rename to same as stakespath 
	Add images to img/stakespath/
	Update slider
	Update video
	
	*}
{assign  var="stakesname" value="Pennnsylvania Derby"} 
{assign  var="stakespath" value="pennsylvania-derby"} 
{assign  var="racetrackpath" value="parx-racing"} 
{include file='inc/left-nav-btn.tpl'}

<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            {include file='menus/stakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           

<div class="headline"><h1>{$stakesname} Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/slider.tpl"} </p>

<h2>Bet on {$stakesname} </h2>

{include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/specs.tpl"}
{*WIKI CONTENT-------------------------------*}

<p>The Pennsylvania Derby is a race for thoroughbred horses run at Parx Racing and Casino (formerly known as Keystone Race Track, then from 1986 through 2010 as Philadelphia Park) each year. The track's premiere event is open to horses, age three, willing to race 1.125 miles (1.811 km) (9 furlongs) on the dirt and as of 2007, offers a purse of $1 million.</p><p>

The Pennsylvania Derby began on Memorial Day in 1979 and achieved Graded status in 1981. From 1990 until 2009, with the exception of 2006 due to extensive renovations, the race was held on Labor Day; in 2004, it was elevated to a Grade II event. Starting in 2010, the race moved to the last Saturday of September in an attempt to get a stronger field preparing for the Breeders' Cup; the move to late September also made it possible to move away from another premier event, the Travers Stakes at Saratoga Race Course in New York State.</p>

<h2>{$stakesname} Odds</h2>
<p>For the latest {$stakesname} odds, please login into BUSR and check the racebook.</p>

{* {include file="/home/ah/allhorse/public_html/graded_stakes/$stakespath/video.tpl"} *}
 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/graded_stakes/calltoaction.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    