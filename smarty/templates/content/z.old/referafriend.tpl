<div id="main" class="logged-in">

      
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Tell your Friends and get some Cash in your Account!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span class="DarkBlue">Referral Bonus for US Racing</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />
<p>Tell a friend about US Racing and make some cash! If you have a friend that signs up because of your referral, then your friend will receive his <strong>10%</strong> sign up Cash Bonus and YOU will ALSO receive a cash bonus equal to 15% of whatever your friend deposits! So, if your friend deposits $500, then you will receive $75 to your account.</p>

<p><strong>There are NO LIMITS</strong>-- refer as many friends as you want.</p>
<p>If you have a friend that signs up because of your referral, then your friend will receive his <strong>10%</strong> sign up Cash Bonus and YOU will ALSO receive a cash bonus equal to 15% of whatever your friend deposits! So, if your friend deposits $500, then you will receive $75 to your account. There are NO LIMITS-- refer as many friends as you want.</p>

<p>&nbsp;</p>
<div>
<table id="infoEntries" style="border-collapse: collapse;" border="1" bgcolor="#ffffff" bordercolor="#aad3ff" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/1.gif" alt="" /></strong></td>
<td>Call or email your friends and let them know that you bet on horses, poker and sports at <strong>US Racing.</strong></td>
</tr>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/2.gif" alt="" /></strong></td>
<td>Tell your friends to go to <strong>US Racing</strong> so they can make easy money too!</td>
</tr>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/3.gif" alt="" /></strong></td>
<td>
<p>After your friend signs up, email me with his/her CUSTOMER ID NUMBER or username and I will credit your account 15% of whatever your friend's first deposit is-- maximum of $10,000 referral bonus.</p>

<p>Your friend opens an account for $1,000 - you will get $150 added to your account. Remember, your buddy gets a 10% bonus on his first deposit, too! And don't forget the daily 3% horse betting rebates and the <a href="promotions/casino/casino-cashback">50% Casino Cash Back</a> special!</p>

<p>Have a great day! Email me for your bonus: <a title="vip@usracing.com" href="mailto:vip@usracing.com">vip@usracing.com</a></p>
<p>Michael</p>
</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<p><strong>TELL YOUR FRIENDS DETAILS:</strong>
<br />
<p>This promotion <em>cannot</em> be used for any buddies that ALREADY have accounts at US Racing. You must tell us about your friends new account within 24 hours from when the new account is established. Your friend must deposit and wager funds in order for you to be eligible for the cash bonus. What's that mean? Your buddy doesn't deposit ten grand and then withdraw it the next day after you receive $1,000 dollar bonus! Let's be fair here! There are NO limits to the number of buddies you refer. To be eligible for the $100 CASH bonus (for referring 5 buddies or more), all your buddies must have joined within a 1 year period and all must have had initial deposits of $100 or more. Remember, if you refer the most buddies in the course of a year, you will get a SUPER BONUS! The "Super Bonus" is changed from year to year-- but believe us when we say it is better than Youbet, TwinSpires and Brisbet put together!! So what are you waiting for? Do you actually like reading the fine print?!?</p>
<br />
<ul>
<li>Referrer must have deposited before referral</li>
<li>Minimum initial deposit $20</li>
<li>Maximum referral bonus is $10,000</li>
<li>Bonus will be paid 7 days after the new customer has made the first deposit</li><li>In order to be eligible as a valid referral, the new customer must have used at least 50% of his initial deposit and must not have requested a withdrawal before completing the wagering requirement on his own sign-up bonus.</li>
<li>If after 7 days of the initial deposit, 50% of the amount has not been wagered, your referral bonus will not be automatically credited. To request this bonus after 7 days, please send an email to <a title="vip@usracing.com" href="mailto:vip@usracing.com?subject=Referral%20Bonus%20Request">vip@usracing.com</a></li>

<!-- <li>*Refer a friend to sign up with usracing.com.  After your friend notifies us, you'll be credited with $50 for deposits up to $749, or 15% of his/her initial deposit if the deposit amount is $750 or more. <li> For deposits of $7000 and up, the referral bonus is 15% of your friend's deposit.</li>
<li> A minimum initial deposit of $50 is required for the $50 referral bonus.</li>-->

<li>Your friend still receives his/her <strong>10%</strong> free cash signup bonus!</li>
<li>There is no limit to the number of times you're credited.</li>
<li>Regular bonus rules apply.</li><li>All referral claims for credit must be made within 60 days of your friend/acquaintance (the referee) signing up.</li>
<li>$20 cash will only be awarded after at least one friend has been referred within a month and at least one bet has been made by the referred friend.</li>
<li>Referrals do not carry over from month to month. For example, September referrals will not count as part of your October referrals.</li>
</ul>
<p>&nbsp;</p>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> <!-- end: main .logged-in -->
<!-- end: LOGGED IN -->



<!-- start: LOGGED OUT -->
<div id="main" class="logged-out">

      
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Tell your Friends and get some Cash in your Account!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span class="DarkBlue">Referral Bonus for US Racing</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />
<p>Tell a friend about US Racing and make some cash! If you have a friend that signs up because of your referral, then your friend will receive his <strong>10%</strong> sign up Cash Bonus and YOU will ALSO receive a cash bonus equal to 15% of whatever your friend deposits! So, if your friend deposits $500, then you will receive $75 to your account.</p>

<p><strong>There are NO LIMITS</strong>-- refer as many friends as you want.</p>
<p>If you have a friend that signs up because of your referral, then your friend will receive his <strong>10%</strong> sign up Cash Bonus and YOU will ALSO receive a cash bonus equal to 15% of whatever your friend deposits! So, if your friend deposits $500, then you will receive $75 to your account. There are NO LIMITS-- refer as many friends as you want.</p>

<p>&nbsp;</p>
<div>
<table id="infoEntries" style="border-collapse: collapse;" border="1" bgcolor="#ffffff" bordercolor="#aad3ff" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/1.gif" alt="" /></strong></td>
<td>Call or email your friends and let them know that you bet on horses, poker and sports at <strong>US Racing.</strong></td>
</tr>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/2.gif" alt="" /></strong></td>
<td>Tell your friends to go to <strong>US Racing</strong> so they can make easy money too!</td>
</tr>
<tr valign="top">
<td bgcolor="#ffffff" width="20"><strong><img src="/images/3.gif" alt="" /></strong></td>
<td>
<p>After your friend signs up, email me with his/her CUSTOMER ID NUMBER or username and I will credit your account 15% of whatever your friend's first deposit is-- maximum of $10,000 referral bonus.</p>

<p>Your friend opens an account for $1,000 - you will get $150 added to your account. Remember, your buddy gets a 10% bonus on his first deposit, too! And don't forget the daily 3% horse betting rebates and the <a href="promotions/casino/casino-cashback">50% Casino Cash Back</a> special!</p>

<p>Have a great day! Email me for your bonus: <a title="vip@usracing.com" href="mailto:vip@usracing.com">vip@usracing.com</a></p>
<p>Michael</p>
</td>
</tr>
</tbody>
</table>
</div>
<p>&nbsp;</p>
<p><strong>TELL YOUR FRIENDS DETAILS:</strong>
<br />
<p>This promotion <em>cannot</em> be used for any buddies that ALREADY have accounts at US Racing. You must tell us about your friends new account within 24 hours from when the new account is established. Your friend must deposit and wager funds in order for you to be eligible for the cash bonus. What's that mean? Your buddy doesn't deposit ten grand and then withdraw it the next day after you receive $1,000 dollar bonus! Let's be fair here! There are NO limits to the number of buddies you refer. To be eligible for the $100 CASH bonus (for referring 5 buddies or more), all your buddies must have joined within a 1 year period and all must have had initial deposits of $100 or more. Remember, if you refer the most buddies in the course of a year, you will get a SUPER BONUS! The "Super Bonus" is changed from year to year-- but believe us when we say it is better than Youbet, TwinSpires and Brisbet put together!! So what are you waiting for? Do you actually like reading the fine print?!?</p>
<br />
<ul>
<li>Referrer must have deposited before referral</li>
<li>Minimum initial deposit $20</li>
<li>Maximum referral bonus is $10,000</li>
<li>Bonus will be paid 7 days after the new customer has made the first deposit</li><li>In order to be eligible as a valid referral, the new customer must have used at least 50% of his initial deposit and must not have requested a withdrawal before completing the wagering requirement on his own sign-up bonus.</li>
<li>If after 7 days of the initial deposit, 50% of the amount has not been wagered, your referral bonus will not be automatically credited. To request this bonus after 7 days, please send an email to <a title="vip@usracing.com" href="mailto:vip@usracing.com?subject=Referral%20Bonus%20Request">vip@usracing.com</a></li>

<!-- <li>*Refer a friend to sign up with usracing.com.  After your friend notifies us, you'll be credited with $50 for deposits up to $749, or 15% of his/her initial deposit if the deposit amount is $750 or more. <li> For deposits of $7000 and up, the referral bonus is 15% of your friend's deposit.</li>
<li> A minimum initial deposit of $50 is required for the $50 referral bonus.</li>-->

<li>Your friend still receives his/her <strong>10%</strong> free cash signup bonus!</li>
<li>There is no limit to the number of times you're credited.</li>
<li>Regular bonus rules apply.</li><li>All referral claims for credit must be made within 60 days of your friend/acquaintance (the referee) signing up.</li>
<li>$20 cash will only be awarded after at least one friend has been referred within a month and at least one bet has been made by the referred friend.</li>
<li>Referrals do not carry over from month to month. For example, September referrals will not count as part of your October referrals.</li>
</ul>
<p>&nbsp;</p>
<div style="width:100%;"><a href="/join"><img style="margin: 0 auto; width: 150px;" title="Want to bet on Horses Online? Join Today!" alt="Want to bet on Horses Online? Join Today!" src="/themes/images/join_button.gif" border="0" /></a></div> 
<p>&nbsp;</p>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> <!-- end: main .logged-out -->