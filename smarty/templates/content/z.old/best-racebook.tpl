{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Why Us?</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/welcome" title="">WELCOME</a></li>
<li><a href="/learnmore" title="">LEARN MORE</a></li>
<li><a href="/seedifference" title="">SEE THE DIFFERENCE</a></li>
<li><a href="/racebookreviews" title="">COMPARE US</a></li>
<li><a href="/readreviews" title="">READ REVIEWS</a></li>
<li><a href="/betwithconfidence" title="">Setting the Pace</a></li>
<li class="leaf last active-trail"><a href="/bestracebook" title="" class="active">ABOUT US Racing</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->

            
{include file='banners/banner3.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>All About US Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>US Racing is an international gaming company focused on the thoroughbred and harness horse racing market.&nbsp; Members have the ability to place wagers online on over 150 racetracks including Churchill Downs, Del Mar, Philadelphia Park, Pimlico Race Course and Belmont Park.&nbsp; Real time audio and video feeds are available for free.&nbsp;</p>
<p>US Racing also offers ESPN style Poker, Casino games and a Sportsbook which are all accessible with a single account.&nbsp;</p>
<p>Our goal is to provide you with the absolute best in customer service. What does "best" mean to us? It means the customer comes first, period. "Customer First" is only part of our commitment to providing you with unparalleled customer support, a company-wide philosophy to ensure our customers remain our number one priority. Our goal is simple: to provide you with safe and exciting Internet gaming.&nbsp;</p>
<p>Free membership includes access to online poker, casino games, sports wagering and daily horse racing news. US Racing also gives a 8% REBATE to all customers for Exotics and 3% for Win, Place or Show wagers.&nbsp;</p>
<p>This website is owned&nbsp;by Domain Holdings Limited, Suite 14, Watergardens 5, Gibraltar.&nbsp;</p>
<p>&nbsp;&nbsp;</p>
<p>To contact us, please go to&nbsp;<a href="/support">Customer Support</a>.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    