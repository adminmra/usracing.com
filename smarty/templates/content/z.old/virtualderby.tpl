 <div id="main" class="logged-in">
 
      

<div id="left-col" class="append-logged-in">
 


                                      
          
<div class="headline"><h1>Virtual Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="field field-type-text field-field-body-logged-in">



<div class="virtualderby-box" style="padding: 10px 0 10px 0;">

<div style="float: left; width: 454px;">
<a href="/virtualderby/classic" title="Play Virtual Derby Classic"><img src="/themes/images/virtualderby-titlebar.gif" /></a>
<a href="/virtualderby/classic" title="Play Virtual Derby Classic"><img src="/themes/images/virtualderby-btn.jpg" /></a>
</div>

<div style="width: 454px; margin-left: 464px; ">

<a href="/virtualderby/3d" title="Play Virtual Racebook 3D"><img src="/themes/images/virtualracebook-titlebar.gif" /></a>
<a href="/virtualderby/3d" title="Play Virtual Racebook 3D"><img src="/themes/images/virtualracebook-btn.jpg" /></a>
</div>

</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->


<div id="main" class="logged-out">

      
      

      

<div id="left-col" class="append-logged-out">
         
          
            <div id="block-block-4" class="block block-block region-odd odd region-count-1 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/join"><img style="margin:0; padding:0; display: block;" src="/themes/images/banner-virtualderby.jpg" alt="Virtual Derby" width="705" height="206" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Virtual Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>&nbsp</p>
<br />
<h3><span class="DarkBlue">Virtual Derby<sup><small>TM</small></sup> Horse Racing Game</span></h3>
<p>&nbsp</p>
<p><span class="DarkBlue"></span></p> 

<p>Virtual Derby is a <a title="horse race game" href="/virtualderby" title="Horse Racing Game">horse race game</a> that you can play for fun or for real money. You can select your jockey's silks and the track is open 24 hours a day!</p>

<p>There are six horses and jockeys to choose from all with different odds of winning. Each and every horse's odds represent a true reflection of that horse's chances in the race. Factors such as the recent form of the horse, the experience of the jockey, the quality of the trainer and the going on the particular day of the race are all factors that contribute towards the compiling of the odds.</p>

<p>Play Virtual Derby for real or for fun. You must login to play for fun.</p><p>If you don't have a user name and password already and you want to play virtual horse racing for free, please join our site today!</p><p><a title="Join Today!" href="/join" target="_blank"><strong>Click here to sign up for free.</strong></a></p>
<p>&nbsp</p>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->
