
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Table Demo Page</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->




      <h2 class="title">Test</h2>
  



    teset  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-44" class="block block-block region-even even region-count-2 count-2">

      <h2 class="title">Breeders Cup 2010</h2>
  



     
<table  width="100%" id="infoEntries" title="Breeders' Cup 2010" summary="Date, Post Time, Distance, Purse for Betting the 2009 Breeders' Cup">
                                            
                                          <tr >
										  	<th width="10%" >Date</th>
                                            <th width="10%" >Post</th>
                                            <th width="47%">Race</th>
                                            <th width="21%">Age</th>
                                            <th width="12%">Distance</th>
                                            <th width="10%">Purse</th>
                                          </tr>
                                        <tr>                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Ladies' Classic&id=2" title="Breeders' Cup Ladies' Classic" >Breeders' Cup Ladies' Classic</a></td>
                                            <td width="119">3yo & up, F&M</td>
                                            <td width="66">1 1/8 Miles</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Emirates Airline Breeders' Cup Filly & Mare Turf&id=3" title="Emirates Airline Breeders' Cup Filly & Mare Turf" >Emirates Airline Breeders' Cup Filly & Mare Turf</a></td>
                                            <td width="119">3yo & up, F&M</td>
                                            <td width="66">1 1/4 Miles (T)</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Grey Goose Breeders' Cup Juvenile Fillies&id=4" title="Grey Goose Breeders' Cup Juvenile Fillies" >Grey Goose Breeders' Cup Juvenile Fillies</a></td>
                                            <td width="119">2yo, fillies</td>
                                            <td width="66">1 1/16 Miles</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Sentient Jet Breeders' Cup Filly & Mare Sprint&id=5" title="Sentient Jet Breeders' Cup Filly & Mare Sprint" >Sentient Jet Breeders' Cup Filly & Mare Sprint</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">7 Furlongs</td>
                                            <td width="53">$1,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Juvenile Fillies Turf&id=6" title="Breeders' Cup Juvenile Fillies Turf" >Breeders' Cup Juvenile Fillies Turf</a></td>
                                            <td width="119">2yo, fillies</td>
                                            <td width="66">1 mile (T)</td>
                                            <td width="53">$1,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 05</td>
										    <td width="51" >3:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Marathon&id=14" title="Breeders' Cup Marathon" >Breeders' Cup Marathon</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">1 3/4 Mile</td>
                                            <td width="53">$500,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Juvenile Turf&id=13" title="Breeders' Cup Juvenile Turf" >Breeders' Cup Juvenile Turf</a></td>
                                            <td width="119">2yo, colts & geldings</td>
                                            <td width="66">1 Mile (T)</td>
                                            <td width="53">$1,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Dirt Mile&id=12" title="Breeders' Cup Dirt Mile" >Breeders' Cup Dirt Mile</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">1 Mile</td>
                                            <td width="53">$1,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Grey Goose Breeders' Cup Juvenile&id=11" title="Grey Goose Breeders' Cup Juvenile" >Grey Goose Breeders' Cup Juvenile</a></td>
                                            <td width="119">2yo, colts & geldings</td>
                                            <td width="66">1 1/16 Miles</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Sentient Jet Breeders' Cup Sprint&id=10" title="Sentient Jet Breeders' Cup Sprint" >Sentient Jet Breeders' Cup Sprint</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">6 Furlongs</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=TVG Breeders' Cup Mile&id=9" title="TVG Breeders' Cup Mile" >TVG Breeders' Cup Mile</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">1 mile (T)</td>
                                            <td width="53">$2,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Emirates Airline Breeders' Cup Turf&id=8" title="Emirates Airline Breeders' Cup Turf" >Emirates Airline Breeders' Cup Turf</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">1 1/2 Miles (T)</td>
                                            <td width="53">$3,000,000</td>
                                          </tr>
										  
										 <tr>                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Classic&id=7" title="Breeders' Cup Classic" >Breeders' Cup Classic</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">1 1/4 Miles</td>
                                            <td width="53">$5,000,000</td>
                                          </tr>
										  
										 <tr class="odd">                                        
                                           	 <td width="51" >Nov 06</td>
										    <td width="51" >12:00 PM</td>
                                            <td width="226"><a href="/breederscup-race?stakes=Breeders' Cup Turf Sprint&id=15" title="Breeders' Cup Turf Sprint" >Breeders' Cup Turf Sprint</a></td>
                                            <td width="119">3yo & up</td>
                                            <td width="66">6.5 Furlongs (T)</td>
                                            <td width="53">$1,000,000</td>
                                          </tr>
										  
										                                          
                          </table>  </div>

  

</div><!-- /block-inner, /block -->



<div id="block-block-43" class="block block-block region-odd odd region-count-3 count-3">

      <h2 class="title">Dubai World Cup 2009 Schedule</h2>
  



     

 <table width="100%" id="infoEntries" summary="Dubai World Cup 2009"  title=" Dubai World Cup 2009 Schedule ">
                                           <tr >
                                            <th width="100" >Post</th>
                                            <th>Race</th>
                                            <th width="200">Age and Weight </th>
                                            <th width="100">Distance</th>
                                            <th width="100">Purse</th>
                                          </tr>
                                        <tr  >                                         
                                            <td >5:00 pm</td>
                                            <td>KAHAYLA CLASSIC (Group 1)</td>
                                            <td></td>
                                            <td>2,000m</td>
                                            <td> $    2,500,000.00</td>
                                          </tr>
										  <tr  class=odd>                                         
                                            <td >5:40 pm</td>
                                            <td>GODOLPHIN MILE (Group 2)</td>
                                            <td></td>
                                            <td>1,600m</td>
                                            <td> $    1,000,000.00</td>
                                          </tr>
										  <tr  >                                         
                                            <td >6:15 pm</td>
                                            <td>UAE DERBY (Group 2)</td>
                                            <td></td>
                                            <td>1,800m</td>
                                            <td> $    2,000,000.00</td>
                                          </tr>
										  <tr  class=odd>                                         
                                            <td >6:55 pm</td>
                                            <td>DUBAI GOLDEN SHAHEEN (Group 1)</td>
                                            <td></td>
                                            <td>1,200m</td>
                                            <td> $    2,000,000.00</td>
                                          </tr>
										  <tr  >                                         
                                            <td >7:55 pm</td>
                                            <td>DUBAI DUTY FREE (Group 1)</td>
                                            <td>2nd leg of the 2009 Asian Mile Challenge</td>
                                            <td>1,777m</td>
                                            <td> $    5,000,000.00</td>
                                          </tr>
										  <tr  class=odd>                                         
                                            <td >8:40 pm</td>
                                            <td>DUBAI SHEEMA CLASSIC (Group 1)</td>
                                            <td></td>
                                            <td>2,400m</td>
                                            <td> $    5,000,000.00</td>
                                          </tr>
										  <tr  >                                         
                                            <td >9.30pm</td>
                                            <td>DUBAI WORLD CUP (Group 1)</td>
                                            <td></td>
                                            <td>2,000m</td>
                                            <td> $    6,000,000.00</td>
                                          </tr>
										                                           
                          </table>  </div>

  

     

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->

</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    