{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Promotions</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/promotions" title="Current Promotions, Specials and Bonuses">CURRENT</a></li>
<li><a href="/promotions/horseracing" title="Horse Racing Promotions and Rebates">HORSE RACING</a></li>
<li class="expanded active-trail"><a href="/promotions/sportsbook/football" title="Sportsbook Rebates and Promotions">SPORTSBOOK</a><ul class="menu"><li class="leaf first"><a href="/promotions/sportsbook/football" title="">NFL &amp; NCAA FOOTBALL</a></li>
<li><a href="/promotions/sportsbook/mlb" title="MLB Baseball Rebates and Promotions">MLB</a></li>
<li><a href="/promotions/sportsbook/nba" title="">NBA</a></li>
<li class="leaf active-trail"><a href="/promotions/sportsbook/nhl" title="NHL Rebates and Promotions" class="active">NHL</a></li>
<li><a href="/promotions/sportsbook/sports-cashback" title="5% Sports Cash Back Rebate">5% SPORTS CASH BACK</a></li>
</ul></li>
<li class="expanded"><a href="/promotions/casino/blackjack-lucky7s" title="Casino Rebates and Promotions">CASINO</a><ul class="menu"><li class="leaf first"><a href="/promotions/casino/blackjack-lucky7s" title="Blackjack Bonuses and Rebates">BLACKJACK LUCKY 7s&#039; BONUS</a></li>
<li><a href="/promotions/casino/paigow-challenge" title="Pai Gow Challenge Rebates and Promotions">PAI GOW CHALLENGE</a></li>
<li><a href="/promotions/casino/casino-cashback" title="">5% CASINO CASH BACK</a></li>
</ul></li>
<li><a href="/promotions/deposit-specials" title="US Racing Deposit Specials">DEPOSIT SPECIALS</a></li>
<li><a href="/poker/tournaments" title="">POKER TOURNAMENTS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



<div id="block-menu-menu-racebook-help" class="block block-menu region-even even region-count-2 count-2">

      <h2 class="title">Help / Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="Customer Care">CUSTOMER CARE</a></li>
<li><a href="/house-rules" title="House Rules">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="Horse Racing Rules">HORSE RACING RULES</a></li>
<li><a href="/rebates" title="8% Rebate Wagers">8% REBATE WAGERS</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/handicapping-horses" title="Handicapping">HANDICAPPING</a></li>
<li><a href="/legend" title="Horse Racing Legend">HORSE RACING LEGEND</a></li>
<li><a href="/horseracingterms" title="Horse Racing Terms">HORSE RACING GLOSSARY</a></li>
<li><a href="/horseracingbets" title="Horse Racing Wagers">HORSE RACING WAGERS</a></li>
<li><a href="/horseracing-history" title="History of Horse Racing">HORSE RACING HISTORY</a></li>
<li><a href="/famoushorses" title="Famous Horses">FAMOUS HORSES</a></li>
<li><a href="/famousjockeys" title="Famous Jockeys">FAMOUS JOCKEYS</a></li>
<li><a href="/bestracebook" title="About US Racing">ABOUT US</a></li>
</ul>  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>NHL - Promotions</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>NHL - Promotions</h2></div>
<div id="promos-content">

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlgrandsalami.gif" /></div>	
			<div class="header">NHL Grand Salami</div>        
			<div class="current-info">
<p>Think you know the NHL? If so then you can't miss out on our NHL Grand Salami where all you have to do is wager on the total amount of goals scored by all NHL teams combined and select the over or under, it's that easy! ( <span class="text-note">All Games Must Go Official Time</span> )</p></div>          
			</div>
            
            <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlfirstperiod.gif" /></div>	
			<div class="header">1st Period Line & Total</div>        
			<div class="current-info">
<p>Others may offer the game but at US Racing we give you more ways to win, bet all NHL games 1st period line and total so you can get off to an early start and avoid overtime.</p></div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlprops.gif" /></div>
			<div class="header">Game & Player Props</div>        
			<div class="current-info">
<p>Ever get the feeling that a player is going to light it up one night or that a team might fall flat? If you have then check out all our Game and Player Props that will challenge even the most diehard hockey fan.</p></div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    