{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/datatransmission" title="" class="active">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Data Transmission</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Due to the fact that transmission of data over the internet is beyond our control, occasionally some bets submitted do not reach us.</p>
<p>For you to be assured your bet was submitted properly, always check your 'Pending Wagers' button, and see if the bet is there. If the bet is not in your Pending Wagers, please click your 'Refresh/Reload' button on the top of the page. If the bet is not in your pending wagers there is no bet, your balance was not affected in anyway.</p>
<p>We will not honor any bets said to have taken place, unless it is in your pending or scored history sheet. Please, when you submit a bet and you get an error. Do not try again until you check your 'Pending Wagers'. The bet might have gone in, this will prevent double betting.</p>
<p>If you have any questions about any wagers, please call us before the event begins. We do not accept wagers over the telephone under any circumstance.</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    