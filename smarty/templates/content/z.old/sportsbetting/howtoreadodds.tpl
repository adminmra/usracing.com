{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/howtoreadodds" title="" class="active">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Reading the Odds</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>European Odds</strong>&nbsp;</p>
<p>All European football lines are quoted using European odds. European odds are different from the North American style of odds. In North America money lines are used to determine the line. European odds are very simple to understand. Let's say the team you want to bet is on offer at 4.00.</p>
<p>That means that for every $1 you bet on them, you get back $4 if they win. That means you have a profit of $3 plus your $1 stake returned. If you lose, you lose your $1.</p>
<p>If the team is at 1.60, you only make $0.60 profit on your $1 stake and you get your $1 stake back.</p>
<p>Some bettors are used to the American system of betting. This is where odds are expressed as -180 or +300. If you place a bet at -180, it means that you need to bet $180 to win $100. That is the same as 1.55 in European betting. And a bet at +300 means that you win $300 for every $100 you bet - the same as 4.00 in European odds.</p>
<p><strong>Combination Bets (parlays)</strong>&nbsp;</p>
<p>A Combination bet involves several different propositions. The customer must correctly choose all the propositions to win. The customer can combine a minimum of 2 propositions and a maximum of 12.</p>
<p>Calculating combination odds using European odds is very simple. You simply take the price of each event that you wagered on and multiply them together.</p>
<p>For example, let's say you wish to wager on Arsenal at 2.5, Manchester at 1.7 and Liverpool at 2.1. A combination bet of all three teams would pay 2.5 x 1.7 x 2.1 = 8.93. If you wager amount is $100 then the payoff would be $100 x 8.93 = $893 (this includes your original stake, so your winnings are 893 -100 = 793)</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    