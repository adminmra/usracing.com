{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/specialsports" title="" class="active">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Special Sports</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Baseball</strong></p>
<blockquote><p>Wagering on baseball is done by laying or taking money odds.<br />The minus (-) indicates the favorite<br />The plus (+) indicates the underdog</p>
</blockquote>
<p>On money odds whenever there is a minus (-) you lay that amount to win a dollar, where there is a plus (+)you get that amount for every dollar wagered.</p>
<p>Example:</p>
<blockquote><p>Dodgers: +1.50<br />Phillies: -1.60</p>
</blockquote>
<p>Player would lay $160 to win $100 or $16 to win $10 on the Phillies.<br />Player wagers $100 to win $150 or $10 to win $15 on the Dodgers.</p>
<p><strong>Boxing</strong></p>
<p>Wagering is similar to baseball</p>
<p>In the event of a draw, moneys on the fight itself will be refunded. However, specific fight propositions must come as stated, such as a draw, either fighter by knockout or decision or a specific round knockout etc. Rounds propositions will be governed by the rules posted on our wagering sheets or as stated at the time of the wager. In the event that either fighter does not make the required weight but the bout goes on, the following stipulations apply: When the bell sounds for the first round, the bout will be considered official, regardless of the scheduled length or title. The bout must have the original scheduled length to have action on a rounds proposition.</p>
<p><strong>Hockey</strong></p>
<blockquote><p>Wagering on hockey is done by laying or taking goals and money odds.<br />The minus (-) indicates the favorite<br />The plus (+) indicates the underdog</p>
</blockquote>
<p>Example:</p>
<blockquote><p>Bruins: + 1/2 Even<br />Kings: - 1/2 -1.40</p>
</blockquote>
<p>Player would lay 1/2 goal and $140 to win $100 on the Kings. Player would wager $100 and take 1/2 goal to win $100 on the Bruins.</p>
<p><strong>Soccer</strong></p>
<p>Bets taken on <strong>'2nd Half Lines'</strong> will be settled on the 45 minutes of the second half plus any added injury or stoppage time. &nbsp;Extra Time, Golden Goals and Penalty Shoot-Outs do not count. &nbsp;Goals scored in the first half do not count toward the second half wager.</p>
<p><strong>In-Running Soccer</strong> betting is offered on selected games. &nbsp;Once the game starts, in-running odds will be posted on the site under ‘In-Running Soccer’ and will be updated constantly during the game, unless the game becomes extremely one-sided, in which case in-running betting will be suspended. &nbsp;In-Running Soccer wagers are based on the final result of the entire game (90 minutes plus any added injury time or stoppage time).</p>
<p><strong>Beach Soccer</strong></p>
<p>Beach Soccer is based on 36 minutes of play (3 periods of 12 minutes) followed by extra periods and penalty shots, if necessary, to decide who wins the match.&nbsp; Bets are graded on normal regulation time (36 minutes plus any time added by the referee in respect of injuries and other stoppages).&nbsp; Extra Time and Penalty Shoot-Outs do not count for wagering purposes.*<br />&nbsp;<br /><strong>Futsal</strong></p>
<p>All bets on Futsal matches are based on the score at the end of 40 minutes' play unless otherwise stated or if the relevant wagering option has already been decided.&nbsp; Extra Time and Penalty Shootouts do not count.&nbsp; If a match is abandoned and/or suspended before the completion of 40 minutes' play, all bets shall be void unless the relevant wagering option has already been decided.&nbsp; All bets on matches that are postponed, rearranged or moved to a different venue shall be void.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    