{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/inrunningwagering" title="" class="active">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>In Running Wagering</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><span style="font-size: small; color: #0958a6; font-family: Arial, Helvetica;">&nbsp;</span></p>
<p><span style="font-size: small; font-family: Arial, Helvetica;">We are happy to now offer you in running odds. </span></p>
<p><span style="font-size: small; font-family: Arial, Helvetica;">&nbsp;</span></p>
<ul>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Odds will be opened and posted on the site under "In Running" once the game starts. Odds will be posted throughout the whole game. </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;"><strong>Wagers on "In Running" are based on the final result of the entire game.</strong> </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">The betting limit for the entire "event" will be $1,500. If you wish to make multiple wagers throughout the event, please make them in smaller increments that do not exceed $1,500 in total. </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">We will most likely close the in running odds during the last 1 or 2 minutes of each Quarter. </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Please be sure to click on "Update Current Odds" before placing your wager, to insure you have the most current changes. </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Any wager on the In Running odds is a wager on the final game outcome, including any overtimes. </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">The game must be complete for bets to be considered action. If a game is cancelled or postponed premature to its originally intended length of play, then ALL trades for the event will be refunded. </span></li>
</ul>
<p><span style="font-size: small; font-family: Arial, Helvetica;">&nbsp;</span></p>
<p><span style="font-size: small; font-family: Arial, Helvetica;">We will be offering Quarter Lines as well during the game. </span></p>
<p><span style="font-size: small; font-family: Arial, Helvetica;">&nbsp;</span></p>
<ul>
<li><span style="font-size: small; font-family: Arial, Helvetica;">First Quarter Line </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">First Half Line </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Second Quarter Line offered at the end of the 1st Quarter </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Second Half Line offered during Half Time </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Third Quarter Line offered during Half Time </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Fourth Quarter Line offered at the end of the 3rd Quarter </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">As well, as described above, a full game line offered throughout the whole game (In-Running) </span></li>
<li><span style="font-size: small; font-family: Arial, Helvetica;">Fourth Quarter line does not include overtime. Second Half and In-Running lines include overtime. </span></li>
</ul>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    