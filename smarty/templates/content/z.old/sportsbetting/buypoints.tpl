{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/buypoints" title="" class="active">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Buy Points</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Buying Points allows you the ability to change the point spread of a football or basketball game. You can move the point spread so you get more points if you are betting the underdog or you give away less points if you are betting the favorite. For each half point that you change the point spread you need to pay an extra 10%.</p>
<p>So for example, if you are betting the Lakers -5 -110 (lay $110 to win $100), you can buy the Lakers a half point to -4.5 -120 (lay $120 to win $100). You can buy the Lakers a full point to -4 -130 (lay $130 to win $100).</p>
<p>Same theory is true for the underdog. If the Bulls are +4 -110, you buy the game to +4.5 -120 or +5 -130.</p>
<p>You can only buy a maximum of 3 points in Football and Basketball, both college and pro.</p>
<p>There is one special rule. When buying on or off the number 3 in the NFL, there is a premium cost of 20%, not 10%. So for example to move the Chargers from -3 -110 to -2.5, the new line will be -2.5 -130 (lay $130 to win $100). The same is true for the underdog, to buy Bears from +3 -110 to +3.5 will be -130 (lay $130 to win $110).</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    