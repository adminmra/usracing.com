{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/halfpricesales" title="" class="active">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Half Price Sales</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Football Season:</strong> 5 Cent Fridays on NCAA and NFL games<br /><strong>Basketball Season:</strong> 5 Cent Fridays on NCAA and NBA games</p>
<table class="show_design_border">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
<ul>
<li>During the basketball season, there is 5% juice on TNT games and 5% Juice on NBA and NCAA games on Friday. </li>
<li>NCAA and NFL football sides lay -105 on Friday afternoons. </li>
<li>Some selected lines are excluded from this promotion at the lines-makers discretion </li>
<li>Friday from 3pm to 10pm EST </li>
<li>Raging Parlay Odds (regular odds in parentheses) at 5%</li>
</ul>
<table class="show_design_border" style="padding-left: 30px; text-align: center;">
<tbody>
<tr>
<td>2 teams -</td>
<td><strong>2.8/1</strong>&nbsp; &nbsp; &nbsp;</td>
<td>(13/5)</td>
</tr>
<tr>
<td>3 teams -</td>
<td><strong>6.4/1</strong></td>
<td>(6/1)</td>
</tr>
<tr>
<td>4 teams -</td>
<td><strong>13.5/1</strong></td>
<td>(10/1)</td>
</tr>
<tr>
<td>5 teams -</td>
<td><strong>27.4/1</strong></td>
<td>(25/1)</td>
</tr>
<tr>
<td>6 teams -</td>
<td><strong>54/1</strong></td>
<td>(40/1)</td>
</tr>
<tr>
<td>7 teams -</td>
<td><strong>107/1</strong></td>
<td>(75/1)</td>
</tr>
<tr>
<td>8 teams -</td>
<td><strong>210/1</strong></td>
<td>(150/1)</td>
</tr>
<tr>
<td>9 teams -</td>
<td><strong>411/1</strong></td>
<td>(300/1)</td>
</tr>
<tr>
<td>10 teams -</td>
<td><strong>804/1</strong></td>
<td>(700/1)</td>
</tr>
<tr>
<td>11 teams -</td>
<td><strong>1,570/1</strong></td>
<td>(1,100/1)</td>
</tr>
<tr>
<td>12 teams -</td>
<td><strong>3,066/1</strong></td>
<td>(1,800/1)</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    