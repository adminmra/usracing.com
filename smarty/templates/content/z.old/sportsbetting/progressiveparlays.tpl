{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/progressiveparlays" title="" class="active">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Progressive Parlays</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong class="subtitle">&nbsp;</strong></p>
<p><strong class="subtitle">Progressive Parlays are the Real Payoff</strong>&nbsp;</p>
<p>There are very few casinos or sportsbooks that offer Progressive Parlays, less than a handful and you will not find these anywhere in Las Vegas.</p>
<p><strong>US Racing continues to dominate the competition in Payout Value.</strong></p>
<p>How does a Progressive Parlay work? In fact, it's quite simple. In exchange for not getting quite as big a payout for picking a perfect combination, you are getting insurance for those rare occasions when one or more or the teams in your parlay losses.</p>
<p>Let us look at an example. An 8 team progressive parlay for $10 will payout $750 for making a perfect 8 team winning combination. If you are only able to find 7 winners then you win $100. And if you only bring home 6 winners then you will still find a winning ticket for $20.</p>
<p>&nbsp;</p>
<table class="maincontent2" border="0" cellspacing="0" cellpadding="3" width="100%">
<tbody>
<tr>
<th>&nbsp;</th>
<th style="text-align: center;">All Teams Won</th>
<th style="text-align: center;">1 Team Loss</th>
<th style="text-align: center;">2 Teams Lose</th>
<th style="text-align: center;">3 Teams Lose</th>
</tr>
<tr>
<td align="center">4 Teams</td>
<td align="center">5/1</td>
<td align="center">1/1</td>
<td align="center">--</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">5 Teams</td>
<td align="center">11/1</td>
<td align="center">2/1</td>
<td align="center">--</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">6 Teams</td>
<td align="center">20/1</td>
<td align="center">7/2</td>
<td align="center">--</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">7 Teams</td>
<td align="center">40/1</td>
<td align="center">4/1</td>
<td align="center">1/2</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">8 Teams</td>
<td align="center">75/1</td>
<td align="center">10/1</td>
<td align="center">2/1</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">9 Teams</td>
<td align="center">150/1</td>
<td align="center">20/1</td>
<td align="center">5/2</td>
<td align="center">--</td>
</tr>
<tr>
<td align="center">10 Teams</td>
<td align="center">250/1</td>
<td align="center">25/1</td>
<td align="center">3/1</td>
<td align="center">1/1</td>
</tr>
<tr>
<td align="center">11 Teams</td>
<td align="center">400/1</td>
<td align="center">60/1</td>
<td align="center">5/1</td>
<td align="center">1/1</td>
</tr>
<tr>
<td align="center">12 Teams</td>
<td align="center">900/1</td>
<td align="center">80/1</td>
<td align="center">10/1</td>
<td align="center">3/1</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><strong>Take a look at the extraordinary value that US Racing is offering.</strong></p>
<p style="padding-left: 30px;">*&nbsp;Place a $10 wager on a 12 team parlay at our competitors and you win $6,000 if you get all 12 right. You lose $10 if you miss just one team.<br />*&nbsp;Place a $10 wager on a 12 team parlay at US Racing and not only do you win 50% more, or $9,000, if you get all 12 correct, but you still win $100 even if 2 of your&nbsp;&nbsp; teams lose.</p>
<p><strong>Note:</strong> Progressive parlay odds are based on point spread wagering options only. The payout calculations are based on these odds. Progressive parlays are not available for MLB, NHL or money line wagers in football and basketball.</p>
<p>A push or a tie is considered a loss and a No-Actioned bet would revert to the next lowest progressive parlay.</p>
<p>Please note that wagering on both sides of the same event in Parlays and Teasers is not allowed and will result in all wagers in which teams are bet on opposite sides of the event being graded as a loss.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    