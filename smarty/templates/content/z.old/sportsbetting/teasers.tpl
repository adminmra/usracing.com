{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/teasers" title="" class="active">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Teasers</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p style="text-align: left;"><strong>US Racing offers the Highest Teaser Payouts on the Internet</strong>&nbsp;</p>
<p>&nbsp;</p>
<table id="infoEntries" border="0" cellspacing="0" cellpadding="0" width="100%">
<tbody>
<tr>
<th>Basketball:</th>
<th align="center">4 Points</th>
<th align="center">4.5 Points</th>
<th align="center">5 Points</th>
</tr>
<tr>
<th>Football:</th>
<th align="center">6 Points</th>
<th align="center">6.5 Points</th>
<th align="center">7 Points</th>
</tr>
<tr>
<td>2 Teams</td>
<td>-110</td>
<td>-120</td>
<td>-130</td>
</tr>
<tr>
<td style="text-align: left;">3 Teams</td>
<td style="text-align: left;">+180</td>
<td style="text-align: left;">+160</td>
<td style="text-align: left;">+120</td>
</tr>
<tr>
<td style="text-align: left;">4 Teams</td>
<td style="text-align: left;">3/1 (+300)</td>
<td style="text-align: left;">5/2 (+250)</td>
<td style="text-align: left;">2/1 (+200)</td>
</tr>
<tr>
<td style="text-align: left;">5 Teams</td>
<td style="text-align: left;">4.5/1</td>
<td style="text-align: left;">4/1</td>
<td style="text-align: left;">3.5/1</td>
</tr>
<tr>
<td style="text-align: left;">6 Teams</td>
<td style="text-align: left;">6/1</td>
<td style="text-align: left;">5.5/1</td>
<td style="text-align: left;">5/1</td>
</tr>
<tr>
<td style="text-align: left;">7 Teams</td>
<td style="text-align: left;">10/1</td>
<td style="text-align: left;">9/1</td>
<td style="text-align: left;">8/1</td>
</tr>
<tr>
<td style="text-align: left;">8 Teams</td>
<td style="text-align: left;">15/1</td>
<td style="text-align: left;">12/1</td>
<td style="text-align: left;">10/1</td>
</tr>
<tr>
<td style="text-align: left;">9 Teams</td>
<td style="text-align: left;">20/1</td>
<td style="text-align: left;">15/1</td>
<td style="text-align: left;">12/1</td>
</tr>
<tr>
<td style="text-align: left;">10 Teams</td>
<td style="text-align: left;">25/1</td>
<td style="text-align: left;">20/1</td>
<td style="text-align: left;">15/1</td>
</tr>
<tr>
<td style="text-align: left;">11 Teams</td>
<td style="text-align: left;">35/1</td>
<td style="text-align: left;">25/1</td>
<td style="text-align: left;">20/1</td>
</tr>
<tr>
<td style="text-align: left;">12 Teams</td>
<td style="text-align: left;">50/1</td>
<td style="text-align: left;">35/1</td>
<td style="text-align: left;">25/1</td>
</tr>
<tr>
<td style="text-align: left;">13 Teams</td>
<td style="text-align: left;">75/1</td>
<td style="text-align: left;">50/1</td>
<td style="text-align: left;">35/1</td>
</tr>
<tr>
<td style="text-align: left;">14 Teams</td>
<td style="text-align: left;">100/1</td>
<td style="text-align: left;">75/1</td>
<td style="text-align: left;">50/1</td>
</tr>
<tr>
<td style="text-align: left;">15 Teams</td>
<td style="text-align: left;">150/1</td>
<td style="text-align: left;">100/1</td>
<td style="text-align: left;">75/1</td>
</tr>
</tbody>
</table>
<p></p>
<p>*3 Team 10 point Football and 7 Point Basketball Teasers Odds are 10/12 --Ties Lose</p>
<p><strong>Teasers:</strong>&nbsp;</p>
<p>A Teaser is a selection of two or more teams in one wager in which the point spread is adjusted in your favor. The number of teams selected and the number of points selected determines the payout odds. A "Tie" or "No Action" and a "Win" on a two team teaser shall constitute a "No Action" wager. A "Tie" or a "No Action" and a "Loss" on a two team teaser shall constitute a "Losing" wager. Ties on a three or more team teaser shall revert to the next lowest betting bracket. (Example: A tie on a three team becomes a two team teaser.)</p>
<p>An example of a teaser wagers is as follows. For our example, let us say you wish to choose three football teams: Denver -7, New York -3 and Chicago +15. The payout of the wager is dependant upon how many points you wish to receive. Let us choose a 3 team 7 point teaser. If we look at the chart below, this wager is listed as 12/10, which means we will win $12 for every $10 that we wager. The point spreads are adjusted as follows:</p>
<ul>
<li>Denver -7 plus the adjusted seven points = Even </li>
<li>New York -3 plus the adjusted seven points = +4 </li>
<li>Chicago +15 plus the adjusted seven points = +22</li>
</ul>
<p>Therefore, if each and every one of the three teams wins against the adjusted spread, you win $12 for every $10 you wager.</p>
<p>Please note that wagering on both sides of the same event in Parlays and Teasers is not allowed and will result in all wagers in which teams are bet on opposite sides of the event being graded as a loss.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    