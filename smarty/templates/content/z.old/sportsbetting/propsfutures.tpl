{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/propsfutures" title="" class="active">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Props &amp; Futures</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Looking for Props and Futures?&nbsp; US Racing has it ALL!</strong></p>
<table class="show_design_border">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
<ul>
<li>Hundreds of Props Every Day </li>
<li>Daily Football, Basketball &amp; Baseball Propositions </li>
<li>Dozens of Futures, including College Championship Odds, Homerun Derbys, Scoring Titles and More </li>
<li>Dozens of Props for every Sunday and Monday Night Football Games </li>
<li>Daily Matchups on all European and American Golf Matchups </li>
<li>Special Tiger Woods Props every week </li>
<li>Masters Tournament odds Year Round </li>
<li>Weekly Winston and Busch NASCAR odds and matchups </li>
<li>Formula 1 race adds always at US Racing </li>
<li>Odds for all the major golf players to Win a Major in 2000 </li>
<li>Super Bowl, AFC, NFC, NBA Champs, Stanley Cup, World Series </li>
<li>US Presidential Race </li>
<li>Props on Every Major Golf and Tennis Match </li>
<li>You can Parlay together most proposition wagers at True Parlay Odds </li>
<li>Sign up today online and get 10%</li>
</ul>
</td>
</tr>
</tbody>
</table>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    