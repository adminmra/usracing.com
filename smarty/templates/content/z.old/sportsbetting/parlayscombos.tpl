{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/parlayscombos" title="" class="active">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Parlays &amp; Combos</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Parlays:</strong>&nbsp;</p>
<p>A parlay is the selection of 2 or more teams (up to 10) in no particular order in a single wager. All teams in the parlay must win, if there is a tie it reverts to the next lowest number for payoff. (Example: If you select 4 teams and one ties, it becomes a three team parlay). Tie and win on two teamer becomes straight bet (90% of wager)</p>
<p>&nbsp;</p>
<table class="maincontent2" width="100%">
<tbody>
<tr>
<th>&nbsp;</th>
<th>Friday Only Special<br />Parlay Odds[-105]</th>
<th>Regular Daily Parlay Odds</th>
</tr>
<tr bgcolor="#eeeeee">
<td>2 Teams</td>
<td><strong>2.8/1</strong></td>
<td>13/5</td>
</tr>
<tr>
<td>3 Teams</td>
<td><strong>6.4/1</strong></td>
<td>6/1</td>
</tr>
<tr bgcolor="#eeeeee">
<td>4 Teams</td>
<td><strong>13.5/1</strong></td>
<td>10/1</td>
</tr>
<tr>
<td>5 Teams</td>
<td><strong>27/1</strong></td>
<td>20/1</td>
</tr>
<tr bgcolor="#eeeeee">
<td>6 Teams</td>
<td><strong>54/1</strong></td>
<td>40/1</td>
</tr>
<tr>
<td>7 Teams</td>
<td><strong>107/1</strong></td>
<td>75/1</td>
</tr>
<tr bgcolor="#eeeeee">
<td>8 Teams</td>
<td><strong>210/1</strong></td>
<td>150/1</td>
</tr>
<tr>
<td>9 Teams</td>
<td><strong>411/1</strong></td>
<td>300/1</td>
</tr>
<tr bgcolor="#eeeeee">
<td>10 Teams</td>
<td><strong>804/1</strong></td>
<td>700/1</td>
</tr>
<tr>
<td>11 Teams</td>
<td><strong>1,570/1</strong></td>
<td>1,100/1</td>
</tr>
<tr bgcolor="#eeeeee">
<td>12 Teams</td>
<td><strong>3,066/1</strong></td>
<td>1,800/1</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><a title="Progressive Parlays" href="http://usracing.com/sportsbettingrules/progressiveparlays">Progressive Parlays - Click Here</a>&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p><strong>True Odds for Baseball and European Lines</strong></p>
<p>The odds on a multiple or an accumulator are calculated by multiplying the prices of the selections together, but remembering to include the stake amount. Let's assume you place a double on two selections, with odds of a/b and c/d. The calculation to work out your returns (including your stake) is as follows:</p>
<p style="padding-left: 30px;">[ (a+b)/b * (c+d)/d ] times your stake</p>
<p>Example:</p>
<p style="padding-left: 30px;">You place 10 on two selections, with odds of 2/1 and 7/2. The calculation is as follows:</p>
<p style="padding-left: 30px;">[ (2+1)/1 * (7+2)/2 ] times your stake of 10 = 13.5 times your stake of 10</p>
<p style="padding-left: 30px;">Your returns are therefore 135.00 (including your stake)</p>
<p>Please note that wagering on both sides of the same event in Parlays and Teasers is not allowed and will result in all wagers in which teams are bet on opposite sides of the event being graded as a loss.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    