{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/placingwagers" title="" class="active">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Placing Wagers</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>There are different ways to place wagers within the different sports, both individually and geographically. North American betting is different from wagering on European sports while betting on a hockey game is different from betting on a basketball game. Here is a basic outline for betting on all sports types:</p>
<p>First choose the sport you wish to bet on. Click on the pull-down menu that contains the words "Select A Bet Type" to choose which kind of wager you want to place. Then select how much you wish to wager by entering it into the wager box beside the pull-down menu. Now scroll through the available games and choose which team or teams you wish to bet on. To choose a team click on the small box beside the line and you will have chosen that team. If you are making a combination bet, known as a parlay or teaser, you must choose as many teams as is necessary to complete the multiple team wager. This is determined by the type of bet you choose.</p>
<p>After you are sure of your selection, go to the top and click on the "Place Bet" button. This will now place your wager on the right side of the screen, which is your betting ticket. If you try to place a wager and a red bar comes up on your betting ticket you have made a mistake in selecting your bet. Read the message in the red bar and it will inform you of the error and give you appropriate instructions to correct the mistake.</p>
<p>Before you submit your betting ticket please make sure to reload or refresh your page so you can see the most current lines. The lines can change often and quickly, so make sure the lines are where you want them before you submit your betting ticket. If you find that one or more of the wagers on your betting ticket is not what you want, simply click the "Cancel" or "Cancel All" button and the wagers will be dropped from your betting ticket. Once completely satisfied with the wager you have made, go to the bottom of the betting ticket and type the word "Yes" in the confirmation box and then click on the "Submit" button. Your wager will now be submitted. A confirmation notice will appear and you can view your wagers in the "Pending Wagers" section.</p>
<p>Still have some questions?&nbsp; <a href="/tutorials/sportsbook/tour_sports.php" target="_blank">Take a look at the Tour to see how it is done!</a></p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    