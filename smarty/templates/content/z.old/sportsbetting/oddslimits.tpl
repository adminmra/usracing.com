{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/oddslimits" title="" class="active">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li><a href="/sportsbetting/terminology" title="">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Odds &amp; Limits</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table id="infoEntries" width="100%">
<tbody>
<tr>
<th>&nbsp;</th>
<th><strong>Previous Days</strong></th>
<th><strong>Over Night</strong></th>
<th><strong>Day of Game</strong></th>
</tr>
<tr>
<td><strong>NFL Sides / Totals</strong></td>
<td>$5,500 / $2,200</td>
<td>$1,100 / $550</td>
<td>$11,000 / $5,500</td>
</tr>
<tr>
<td>1st Half</td>
<td>$550 /$220</td>
<td>$550 / $220</td>
<td>$2,200 / $1,100</td>
</tr>
<tr>
<td>2nd Half</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$2,200 / $1,100</td>
</tr>
<tr>
<td><strong>NCAA Football</strong></td>
<td>$1,100 / $550</td>
<td>$1,100 / $550</td>
<td>$5,500 / $2,200</td>
</tr>
<tr>
<td>1st Half</td>
<td>$550 /$220</td>
<td>&nbsp;$550 /$220</td>
<td>$2,200 / $1,100</td>
</tr>
<tr>
<td>2nd Half</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$2,200 / $1,100</td>
</tr>
<tr>
<td><strong>NFL / NCAAFB 1st Qtr</strong></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$550 / $550</td>
</tr>
<tr>
<td><strong>MLB (World Series)</strong></td>
<td>$3,000 / $1,500</td>
<td>$1,000 / $500</td>
<td>$5,000 / $2000</td>
</tr>
<tr>
<td><strong>NBA Sides / Totals</strong></td>
<td>&nbsp;</td>
<td>$1,100 / $550</td>
<td>$3,300 / $1,650</td>
</tr>
<tr>
<td>1st Half</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$1,100 / $550</td>
</tr>
<tr>
<td>2nd Half</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$1,650 / $550</td>
</tr>
<tr>
<td>1st Qtr</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$550 / $550</td>
</tr>
<tr>
<td><strong>NCAA Hoops Sides / Totals</strong></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$2,200 / $1,100</td>
</tr>
<tr>
<td>1st &amp; 2nd Half Sides / Totals</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$1,100 / $550</td>
</tr>
<tr>
<td><strong>NHL</strong></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$2,000 / $1,000</td>
</tr>
<tr>
<td><strong>Arena Football</strong></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$1,100 / $550</td>
</tr>
<tr>
<td><strong>Canadian Football - CFL</strong></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>$1,100 / $550</td>
</tr>
</tbody>
</table>
<p class="content_header">&nbsp;</p>
<table class="show_design_border" width="100%">
<tbody>
<tr>
<td>
<p>&nbsp;</p>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td><strong>Football Parlays and Teasers</strong></td>
<td>$2,000</td>
</tr>
<tr>
<td><strong>NFL Propositions</strong></td>
<td>$500</td>
</tr>
<tr>
<td><strong>Other Propositions and Futures</strong></td>
<td>$500</td>
</tr>
<tr>
<td><strong>Soccer Parlays</strong></td>
<td>$500</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><strong>Other Considerations:</strong></p>
<ul>
<li>2 Team Parlay limit for First and Second half football is $200. </li>
<li>Minimum wager is $5 and minimum deposit is $20 </li>
<li>On 10, 11 and 12 team parlays, the minimum bet is $1 </li>
<li>Should a member feel that he would like to set his own betting limits per play, or prefer to exclude himself from using usracing.com for a specified period of time, please contact <a href="mailto:cashier@usracing.com">cashier@usracing.com</a> </li>
<li>Limits can be raised by sending an email to <a href="mailto:cashier@usracing.com">cashier@usracing.com</a> </li>
<li>During Playoffs, maximum betting limits are increasedYou can wager over any of these limits, simply by calling us up. </li>
<li><span style="color: #000000;"><strong class="red"><span style="color: #000000;">Minimum wager is $5 and minimum deposit is $20</span></strong>&nbsp;</span> </li>
<li><strong class="red"><span style="color: #000000;">On 10, 11 and 12 team parlays, the minimum bet is $1</span></strong>&nbsp; </li>
<li><a href="http://usracing.com/sportsbettingrules/parlayscombos">Click here for our Parlay Payout Table</a>&nbsp; </li>
<li><a href="http://usracing.com/sportsbettingrules/teasers">Click here for our Teaser Payout Table</a>&nbsp; </li>
<li><a href="http://usracing.com/sportsbettingrules/halfpricesales">On Fridays, we have 5% reduced juice for more value for your money</a>&nbsp; </li>
<li><a href="http://usracing.com/sportsbettingrules/inrunningwagering">We're offering in running betting on selected games</a>&nbsp; </li>
<li>Please Note: During Playoffs we increase our maximum limits.</li>
</ul>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    