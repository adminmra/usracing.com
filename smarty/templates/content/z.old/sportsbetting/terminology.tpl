{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Betting Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/flash/howtobetonsports" title="">HOW TO PLACE A SPORTS BET</a></li>
<li><a href="/sportsbetting/datatransmission" title="">DATA TRANSMISSION</a></li>
<li><a href="/sportsbetting/propsfutures" title="">PROPS &amp; FUTURES</a></li>
<li><a href="/sportsbetting/halfpricesales" title="">HALF PRICE SALES</a></li>
<li><a href="/sportsbetting/oddslimits" title="">ODDS AND LIMITS</a></li>
<li><a href="/sportsbetting/buypoints" title="">BUY POINTS</a></li>
<li><a href="/sportsbetting/teasers" title="">TEASERS</a></li>
<li><a href="/sportsbetting/parlayscombos" title="">PARLAYS &amp; COMBOS</a></li>
<li><a href="/sportsbetting/roundrobins" title="">ROUND ROBINS</a></li>
<li><a href="/sportsbetting/progressiveparlays" title="">PROGRESSIVE PARLAYS</a></li>
<li><a href="/sportsbetting/placingwagers" title="">PLACING WAGERS</a></li>
<li><a href="/sportsbetting/confirmwagers" title="">CONFIRMATION OF WAGERS</a></li>
<li class="leaf active-trail"><a href="/sportsbetting/terminology" title="" class="active">TERMINOLOGY</a></li>
<li><a href="/sportsbetting/specialsports" title="">SPECIAL SPORTS</a></li>
<li><a href="/sportsbetting/howtoreadodds" title="">READING THE ODDS</a></li>
<li><a href="/sportsbetting/inrunningwagering" title="">IN RUNNING WAGERING</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Roses Kentucky Derby">ROAD TO THE ROSES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Terminology</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Straight Bets:</strong> The team wagered on must win by the point spread given at the time of the wager. For straight wagers, unless otherwise indicated, the odds are $1.10 to win $1.00. Games that tie are considered "no action" and money is re-deposited back into your account. Due to changing events, the point spread may fluctuate at any time.</p>
<p>Example: Player wagers $110.00 on Miami -7 to beat Buffalo +7</p>
<p>If Miami wins by more than 7 points the player wins $100.00 so the total payout including the initial wager would be $210.00. If Miami only wins by 7 points then the game is considered "no action" and all money is returned to the bettor. If the Miami wins by less than 7 points the wagers is lost.</p>
<p><strong>Totals:</strong>&nbsp;The combined score of both teams for games wagered on, all totals lay $1.10 to win $1.00 unless it is otherwise indicated.</p>
<p>Example: The player lays $220.00 on the OVER in the Miami/Buffalo game. The total for the game is 45. If both teams' combined score is more than 45 than the player would win $200.00. If the combined score is equal 45 the game is considered "no action" and the money is returned. If the combined score is less than 45 then the wager is lost.</p>
<p><strong>Money Line Wagers:</strong> The team wagered on just has to win the game. The amount you lay or take for each game may vary. The money line that is read to at the time of your wager is the money line you keep regardless of any line movement.</p>
<p>Example: The player wagers on Miami -160 at Buffalo +260. If Miami wins then the player would lay $160.00 to win $100.00. If the player bet on Buffalo then the player would lay $100.00 to win $260.00</p>
<p><strong>Halftime:</strong> The wager is placed on the odds posted for the second half of a game only. All wagers must go the full second half for action. All halftime wagers are calculated the same as a straight wager unless otherwise indicated. There are no teaser plays or buying of points on halftimes. First half lines are also available.</p>
<p><strong>Round Robin:</strong> Adventurous bettors who enjoy betting parlays sometimes put together a series of parlays called a Round Robin. A three-team Round Robin consists of one three-team parlay and three two-team parlays. For example,&nbsp;Mikey likes teams A,B,C - with a Round Robin he has a three-teamer with ABC, and two-teamers with AB, AC, and BC.</p>
<p><strong>Parlays:</strong> This is a selection of 2 or more teams (up to 10 teams) or propositions in no particular order. All teams wagered on in a parlay must win. If there is a tie, it reverts down to the next lowest number for payoff.</p>
<p>Example: Player wants to make a 5 team parlay:</p>
<p>Miami -7 New York +3 Seattle -4 Denver -10 San Francisco -8</p>
<p>If all these games win then the player would have won at 20-1 odds. If only 1 team loses then the entire parlay loses.</p>
<p><strong>Futures and Propositions:</strong> Future wagers are based on the outcome of events that happen each year. All wagers are final. There will be no payouts until the conclusion of the specified season. Proposition plays are wagers based on the outcome of events that happen each year. Odds change daily and all wagers are considered action at the odds quoted at the time of the wager.</p>
<p><strong>Circled Game:</strong> A game where the maximum bet is restricted, usually due to injuries.</p>
<p><strong>Lines:</strong> Another word for odds.</p>
<p><strong>Pick:</strong> A game where no team or betting option is favorite.</p>
<p><strong>Pointspread:</strong> The handicap, or head start, which the favorite gives to the underdog for betting purposes.</p>
<p><strong>Push:</strong> A game which, with the pointspread, is tied or when the combined scores of the two teams ties the total.</p>
<p><strong>Single:</strong> A bet on a single result or outcome.</p>
<p><strong>Spread:</strong> An abbreviated form of pointspread.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    