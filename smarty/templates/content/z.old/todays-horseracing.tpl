{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    
<h2>Upcoming Races</h2>
<ul class="menu upcoming">
                   <li class="first leaf"><a href="/todayshorseracing?id=HRT&race=1&racename=Harrington Raceway" title="Harrington Raceway Results for Race 1"><div class="racename">Harrington Raceway</div><div class="raceinfo">Race #1 - <span style="color:#7f0000;">9 MTP</span></div></a></li><li><a href="/todayshorseracing?id=IJN&race=3&racename=Indiana Downs (H)" title="Indiana Downs (H) Results for Race 3"><div class="racename">Indiana Downs (H)</div><div class="raceinfo">Race #3 - <span style="color:#7f0000;">11 MTP</span></div></a></li><li><a href="/todayshorseracing?id=MNE&race=1&racename=Mountaineer Race Track" title="Mountaineer Race Track Results for Race 1"><div class="racename">Mountaineer Race Track</div><div class="raceinfo">Race #1 - <span style="color:#7f0000;">99 MTP</span></div></a></li><li><a href="/todayshorseracing?id=NPN&race=1&racename=Northfield Park" title="Northfield Park Results for Race 1"><div class="racename">Northfield Park</div><div class="raceinfo">Race #1 - <span style="color:#7f0000;">99 MTP</span></div></a></li><li><a href="/todayshorseracing?id=YON&race=1&racename=Yonkers" title="Yonkers Results for Race 1"><div class="racename">Yonkers</div><div class="raceinfo">Race #1 - <span style="color:#7f0000;">99 MTP</span></div></a></li></ul>
  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-32" class="block block-block region-even even region-count-2 count-2">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    
<h2>Completed Races</h2>
<ul class="menu completed">
               <li class="first leaf"><a href="/todayshorseracing?id=DLD&race=10&racename=Delaware Park" title="Delaware Park Results for Race 10"><div class="racename">Delaware Park</div></a></li><li><a href="/todayshorseracing?id=FCE&race=9&racename=Fort Erie" title="Fort Erie Results for Race 9"><div class="racename">Fort Erie</div></a></li><li><a href="/todayshorseracing?id=FIM&race=9&racename=Finger Lakes" title="Finger Lakes Results for Race 9"><div class="racename">Finger Lakes</div></a></li><li><a href="/todayshorseracing?id=MEE&race=15&racename=The Meadows" title="The Meadows Results for Race 15"><div class="racename">The Meadows</div></a></li><li><a href="/todayshorseracing?id=PHD&race=10&racename=Philadelphia Park" title="Philadelphia Park Results for Race 10"><div class="racename">Philadelphia Park</div></a></li><li><a href="/todayshorseracing?id=PRD&race=9&racename=Plainridge Racecourse" title="Plainridge Racecourse Results for Race 9"><div class="racename">Plainridge Racecourse</div></a></li><li><a href="/todayshorseracing?id=SUM&race=9&racename=Suffolk Downs" title="Suffolk Downs Results for Race 9"><div class="racename">Suffolk Downs</div></a></li><li><a href="/todayshorseracing?id=TDM&race=8&racename=Thistledown" title="Thistledown Results for Race 8"><div class="racename">Thistledown</div></a></li></ul>
  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Today&#039;s Horse Racing Tracks</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- start code -->

<table   id="infoEntries" summary="Horse Racing Graded Stakes Schedule"> 
                     <tbody>
                       <tr >
                         <th valign="top"  >Date</th>
                         <th  >Graded Stakes </th>
                       </tr>
                       <tr class="odd" >                       
                         <td valign="top"  >Sep 22</td>
                         <td  >    * Woodbine: King Corrie S., $100,000, 3&up, 6f </td>
                       </tr>
                       <tr>                       
                         <td valign="top"  >Sep 25</td>
                         <td  >    * Belmont Park: Gallant Bloom H, (G2), $150,000, 3&up, F&M, 6.5f<br />
    * Belmont Park: Brave Raj Breeders' Cup S., $85,000, 2yo f, 8.32f<br />
    * Belmont Park: Foolish Pleasure Breeders' Cup S., $85,000, 2yo, 8.32f<br />
    * Delaware Park: Kent Breeders' Cup S, (G3), $200,000, $50,000 Breeders' Cup Fund, 3yo, 9f T<br />
    * Hastings Racecourse: British Columbia Breeders' Cup Oaks, $100,000, 3yo f, 9f<br />
    * Hastings Racecourse: Fantasy S., $75,000, 2yo f, 8.5f<br />
    * Louisiana Downs: Super Derby (G2), $500,000, 3yo, 9f<br />
    * Louisiana Downs: Louisiana Stallion S., $100,000, 2yo fd, 7f<br />
    * Louisiana Downs: Louisiana Stallion S., $100,000, 2yo, C&Gd, 7f<br />
    * Louisiana Downs: Sunday Silence Breeders' Cup, $100,000, 2yo, 8.5f T<br />
    * Louisiana Downs: Happy Ticket Breeders' Cup, $75,000, 2yo f, 8.5f T<br />
    * Monmouth Park: Helen Haskell Sampson S, (NSA-I), $100,000, 4&up, 20f T<br />
    * Parx Racing: Pennsylvania Derby (G2), $1,000,000, 3yo, 9f<br />
    * Parx Racing: Turf Amazon H., $200,000, 3&up, F&M, 5f T<br />
    * Presque Isle Downs: Fitz Dixon Jr. Memorial Juvenile S., $100,000, 2yo, 6.5f<br />
    * Presque Isle Downs: H.B.P.A. Stakes, $100,000, 3&up, F&M, 8.32f<br />
    * Presque Isle Downs: Presque Isle Debutante, $100,000, 2yo f, 6f<br />
    * Retama Park: Darby's Daughter Texas Stallion S., $100,000, 2yo f, 6f<br />
    * Retama Park: My Dandy Texas Stallion S., $100,000, 2yo, C&G, 6f<br />
    * Woodbine: Ontario Derby, $150,000, 3yo, 9f </td>
                       </tr>
                                             </tbody>
                     </table>
   <!-- end code -->
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    