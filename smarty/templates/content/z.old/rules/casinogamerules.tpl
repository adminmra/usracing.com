{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Support</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/support/gamingknowledgebase" title="">GAMING KNOWLEDGEBASE</a></li>
<li class="expanded active-trail"><a href="/house-rules" title="">RULES</a><ul class="menu"><li class="leaf first"><a href="/house-rules" title="">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
<li class="leaf active-trail"><a href="/rules/casinogamerules" title="" class="active">CASINO RULES</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
</ul></li>
<li><a href="/horseracing-affiliate-program" title="">WEBMASTERS</a></li>
<li><a href="/responsibilities" title="">RESPONSIBLE GAMING</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Learn to play Casino Games | US Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<link type="text/css" rel="stylesheet" media="all" href="/modules/allhorseracing/tabs/tools.tabs-general.css" />
{literal}<script type="text/javascript" src="/modules/allhorseracing/tabs/tools.tabs-1.0.4.min.js"></script>{/literal}
{literal}<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>{/literal}



<div id="basic-tabs">
		<ul class="tabs casinorules"><!-- the tabs -->
		<li class="first"><a href="#">BLACKJACK</a></li>
		<li class=""><a href="#">BLACKJACK 7s</a></li>
		<li class=""><a href="#">SPANISH BLACKJACK</a></li>
		<li class=""><a href="#">BLACKJACK SWITCH</a></li>
		<li class=""><a href="#">ROULETTE - AMERICAN</a></li>
                <li class=""><a href="#">ROULETTE EUROPEAN</a></li>
		<li class=""><a href="#">CRAPS 7s</a></li>
		<li class=""><a href="#">THREE CARD POKER</a></li>
		<li class=""><a href="#">JACKS OR BETTER</a></li>
		<li class=""><a href="#">DEUCES WILD</a></li>
                <li class=""><a href="#">CARIBBEAN STUD</a></li>
		<li class=""><a href="#">SHOWDOW POKER</a></li>
		<li class=""><a href="#">CASINO HOLDEM</a></li>
		<li class=""><a href="#">BACCARAT</a></li>
		<li class=""><a href="#">FREE RIDE</a></li>
		<li class=""><a href="#">POKER SIDE BET</a></li>
		<li class=""><a href="#">JOKERS WILD</a></li>
		<li class=""><a href="#">ROCK 'N SLOTS</a></li>
		<li class=""><a href="#">HOOK, LINE & SINKER</a></li>
		<li class=""><a href="#">SUPER HI LO</a></li>
		<li class=""><a href="#">VIRTUAL DERBY</a></li>
		<li class=""><a href="#">WHEEL OF FORTUNE</a></li>
		<li class=""><a href="#">POKER DICE</a></li>
		<li class=""><a href="#">RED DOG</a></li>
		<li class=""><a href="#">SICBO</a></li>
		<li class=""><a href="#">PAI GOW POKER</a></li>
		<li class=""><a href="#">BATTLE ROYALE</a></li>
		<li class=""><a href="#">BLACKJACK TERMS</a></li>
		<li class=""><a href="#">OPTIONS TOOLBAR</a></li>	
		</ul><!-- end tabs -->
</div> 
<div class="panes">
<!-- ======== START ALL TABS ==================================================== -->


<!-- ======== NEW TAB ==================================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane1 -->



						          
          
<div class="headline"><h1>Blackjack</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Blackjack originated in France and is one the most popular casino games in the world. The main objective is to get as close to 21 without going over 21 while still beating the dealer's hand. In our game you can play up to three hands at once.  The name Blackjack arose when an American casino decided to popularize the game by paying a bonus to any player holding the ace of spades and either the jack of clubs or jack of spades.</p><p><b>Rules</b> </p><p>Our blackjack uses 6 standard 52 card decks in each shoe which is reshuffled after each hand.</p><p>In the beginning you are dealt two cards on each hand you placed your bets on (up to three). The dealer is dealt two cards also, one "face" up and other closed, known as a "hole" card. There are numerous things you can perform to increase your chances of winning, please refer to the 'Gimmicks' section below.</p><p>All cards numbered 2 through 10 are at their face value. All face cards are valued at 10 while an ace is valued at either 1 or 11 at the player's discretion. Hands containing an ace valued at 11 are considered to be "soft" hands (i.e. A, 6 is a soft 17) while hands containing an ace valued at 1 are considered "hard" hands (i.e. A, 6, Q is a hard 17).</p><p>A blackjack is an original hand of two cards: ace and card valued at 10 (10, J, Q, K). A two-card blackjack always beats three or more card hands which equal 21. If you split two tens or two aces and get two cards 21's, it is considered to be a simple 21 and not a blackjack and it is paid 1:1. Dealer's blackjack versus player's blackjack is a tie. The dealer must stand on any 17 and above and draw on a 16 and below. If dealer's card is a 10 or any other card valued at 10 (J, Q, K), the dealer will check for an ace in his hole card and if it is an ace the hand will be instantly over and players will not have an option to take any cards.</p><p><b>Gimmicks</b> </p><p><b>Hit</b> - This deals the player another card. If your 2 cards were to equal 10 you have nothing to lose by requesting another card since your total could not exceed 21.</p><p><b>Stand</b> - This means that the player is satisfied with his hand and does not want to risk another hit.</p><p><b>Double Down</b> - Upon dealing of your original two cards, you have the option to double your initial wager. Once you have chosen to do this you will be dealt a third card and no more. Your hand will only consist of the 3 cards.</p><p><b>Splitting</b> - If you are dealt two cards with identical value you can then split them (i.e. 8,8 or 10,Q). You would now be playing 2 separate hands, each starting with one of the two split cards. You will now be dealt a second card to one of the hands. After this hand is played out the second hand will be dealt a second card and continued until that hand is played out. All regular rules apply to each hand with the ability to double down. You may only split your hand once and a new wager equal to the initial wager will be placed for each split hand. When splitting aces, the player will only receive one card on each hand.</p><p><b>Insurance</b> - When the dealer's exposed card is an ace, the player will be given the option to place an insurance wager. This wager will equal one half of the original bet and will be placed on the insurance line. If the dealer has blackjack the player will get paid 2 to 1 on the insurance bet with the original bet being lost. This evens out and acts similar to a push. If the dealer does not have blackjack the insurance bet is lost immediately.</p><p><b>Payouts</b> </p><p>If your hand exceeds 21, you bust and automatically lose the hand. If the player busts, then the dealer wins the hand before turning over a card. Player wins the wager if his/her total hand is higher than the dealer's without going over 21. All hands that total less than the dealer's hand lose. Hands that are equal in value will be considered a tie, or a push. If the player gets a blackjack the payout is 3:2, all other winning combinations are paid 1:1.</p><p><b>Tips</b> <br /></p>

<div align="center"><img height="340" src="/rules/RULES/rules_bj_files/bj_table.gif" width="520" /> </div>

<p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p><p>For general casino disputes we provide a record that contains the bet amount, running balance, game type, Round ID and time for each single casino round only. Every casino round has a unique Round ID and customers should provide it in their casino claim. Due to technology constraints we can only provide the customer with the card/number/dice details and outcomes for a limited number of casino rounds only. If a unique Round ID or exact time for a particular casino round is not provided by the customer we cannot guarantee the funds will be reimbursed.</p>
					



</div><!-- End Pane1 -->
</div>
<!-- ======== END TAB ============================================================ -->


<!-- ======== NEW TAB ============================================================ -->
<div>
<div class="basic-tabs-content"><!-- Pane2 -->


						          
          
<div class="headline"><h1>Blackjack 7s</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The rules of the Super 7 Blackjack are identical to our regular Las Vegas style Blackjack rules which <a href="http://www.sportsbook.com/javagames/rules_bj.html">you can find here.</a> The Super 7 bet is optional and must be placed next to the regular wager before any cards are dealt. The Super 7 lets you shoot for the big win while playing up to three hands of Blackjack at a time. The minimum Super 7 wager is $1 and the maximum is $10. You have to place the Super 7 bet next to the regular bet on an attached semi-circle.</p><p><b>Tips</b> <br /></p><p align="center"><img height="157" src="/rules/RULES/rules_bj7_files/bj7_table.gif" width="482" /></p><div align="center"></div><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					

	
</div><!-- End Pane2 -->
</div>
<!-- ======== END TAB ============================================================ -->


<!-- ======== NEW TAB ============================================================ -->
<div>
<div class="basic-tabs-content"><!-- Pane3 -->


						          
          
<div class="headline"><h1>Spanish Blackjack</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Spanish Blackjack is a variation of Blackjack. The objective of Spanish Blackjack is for a player to come as close to 21 without going over, while still having a higher total than the dealer. Spanish Blackjack is played with eight decks of cards with the 10's removed. You can play up to three hands at once.<br /><br />
panish Blackjack uses an eight deck shoe that is reshuffled after each hand.<br /><br />
Each of the 8 decks has had the four 10's removed.<br /><br />
You can play up to three hands at once.<br /><br />
If your cards have the same value you can split, this includes hands containing any combination of J, Q, K. Players can only split a hand once.<br /><br />
You can double down on any hand. You can re-double once.<br /><br />
If your hand is greater than the dealer<span class="Step">’</span>s and does not go over 21, you win.<br /><br />
The face cards—jack, queen and king—all have a value of 10, aces have a value of 1 or 11, and the remaining cards are worth their face value.<br /><br />
If your hand exceeds 21, you bust and automatically lose the hand. If you have the same card total as the dealer, there is no winner and your bet is returned. This is known as a push.<br /><br />
The dealer hits on Soft 17, stands on 17 and above, and draws on 16 and below.<br /><br />
If the dealer<span class="Step">’</span>s face-up card is an ace, you can buy insurance.<br /><br /></p><h1 class="Heading-2"><span class="content_header"><b class="big">Tips</b></span></h1><p>The following are some Blackjack betting tips:<br /><br />
Should I hit or stand?<br /></p><table class="rules" cellspacing="0" cellpadding="0" width="650" border="1"><tbody><tr class="rules"><th width="233">If the dealer<span class="Step">’</span>s face-up card is:</th><th width="181">Hit on:</th><th width="234">Stand on:</th></tr><tr class="rules"><td>3, 4, 5, or 6</td><td>11 and lower<br />
Soft 17 and lower</td><td>12 and higher<br />
Soft 18 and higher</td></tr><tr class="rules"><td>7, 8, 9, 10, or ace</td><td>16 and lower<br />
Soft 17 and lower</td><td>17 and higher<br />
Soft 18 and higher</td></tr></tbody></table><br />
Should I split?<br /><table class="rules" cellspacing="0" cellpadding="0" width="650" border="1"><tbody><tr class="CellHeading"><th width="50%">If the dealer<span class="Step">’</span>s face-up card is:</th><th width="50%">Split:</th></tr><tr><td>2, 3, 4, 5, 6, or 7</td><td>2s, 3s, 8s, and aces</td></tr><tr class="odd"><td>8, 9, 10, or ace</td><td>8s and aces</td></tr></tbody></table><p class="Heading-3">Should I double?</p><p></p><table class="rules" cellspacing="0" cellpadding="0" width="650" border="1"><tbody><tr class="CellHeading"><th width="50%">If your cards total:</th><th width="50%">Double if the dealer<span class="Step">’</span>s face-up card is:</th></tr><tr><td>11</td><td>Anything except an ace</td></tr><tr class="odd"><td>10</td><td>Anything except a 10 or an ace</td></tr><tr><td>9</td><td>2, 3, 4, 5, 6, 7, or 8</td></tr><tr class="odd"><td>7 or 8</td><td>3, 4, 5, or 6</td></tr></tbody></table>
<br />
<h1 class="content_header"><b class="big">You Win!</b></h1></div><p>If you are dealt an ace and a card with a value of 10, you have Blackjack (21). The payout is 3:2.<br /><br />
For all other hands that beat the dealer<span class="Step">’</span>s hand, the payout is 1:1.<br /><br />
If you <span class="Heading-3">split</span> and are dealt Blackjack (21), the payout is 1:1.<br /><br />
If the dealer is dealt a hand equal to yours, including Blackjack, it is a push. Your wager is returned to you.</p><p></p><table class="rules" cellspacing="0" cellpadding="0" width="500" border="1"><tbody><tr class="CellHeading"><th width="66%">Hand</th><th width="34%">Payout</th></tr><tr><td>Dealer busts (hand exceeds 21)</td><td><p>Player wins</p></td></tr><tr class="odd"><td>Player's hand is closest to 21</td><td>Player wins</td></tr><tr><td>Player busts (hand exceeds 21)</td><td>Dealer wins</td></tr><tr class="odd"><td>Dealer's hand is closest to 21</td><td>Dealer wins</td></tr><tr><td>Hands are equal or a push is achieved</td><td>No loss or gain</td></tr></tbody></table><p class="Body"><span class="Body">Bonus w<span class="Step">innings are paid out at the end of the round only if the hand has not been split or doubled. The optional Match Bonus bet is calculated separately from your regular Blackjack wager.</span></span></p><p></p><table class="rules" cellspacing="0" cellpadding="0" width="500" border="1"><tbody><tr class="CellHeading"><th width="66%">Hand</th><th width="34%">Who wins?</th></tr><tr><td>Spanish Blackjack (7-7-7), dealer has 7 face up</td><td><p>1000 for $5-$24 wager, $5000 on bets $25 and higher up to and including the wager limit.</p></td></tr><tr class="odd"><td>7+ card 21 pays: 3 to 1</td><td>3:1</td></tr><tr><td><p>6-card 21 pays: 2 to 1</p></td><td>2:1</td></tr><tr class="odd"><td><p>6-7-8 Spades pays: 3 to 1</p></td><td>3:1</td></tr><tr><td>6-card 21 pays: 2 to 1</td><td>2:1</td></tr><tr class="odd"><td><p>7-7-7 suited pays: 2 to 1</p></td><td>2:1</td></tr><tr><td><p>7-7-7 non-suited pays: 3 to 2</p></td><td>3:2</td></tr><tr class="odd"><td><p>6-7-8 non-suited pays: 3 to 2</p></td><td>3:2</td></tr><tr><td><p>5-card 21 pays: 3 to 2</p></td><td>3:2</td></tr><tr class="odd"><td>Match the Dealer side bet pays when player card(s) matches dealer's face-up card:</td><td> </td></tr><tr><td><p>Suited matches: 12 to 1</p></td><td>12:1</td></tr><tr class="odd"><td><p>Non-suited matches: 3 to 1</p></td><td>3:1</td></tr></tbody></table><p> </p>
					</div>


	
</div><!-- End Pane3 -->
 </div>
<!-- ======== END TAB  ============================================================ -->

<!-- ======== NEW TAB =========================================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane4 -->


						          
          
<div class="headline"><h1>Blackjack Switch</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Blackjack Switch adds a new wrinkle to the classic Blackjack casino game: card swtiching. The object of the game is the same: obtain a total card count higher than that of the dealer without going over 21. However, now you can switch cards between your two hands in an attempt to get a better hand and improve your chances of winning.</p><p>First you make two bets of equal size and are then dealt two hands instead of the normal one. You then have the option of switching the second card dealt to each hand. With a bit of luck, better hands are created and you win.</p><p>Because of the increased odds of winning, if the dealer has 22 a push is declared and Blackjack pays even money (1:1) instead of the normal 3:2.</p>          
          
<div class="headline"><h1><span class="style3">Rules</span> </h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Blackjack Switch uses a six deck shoe that is reshuffled after each hand.</p><p>If your hand is greater than the dealer’s and does not go over 21, you win.</p><p>The face cards—jack, queen, and king—all have a value of 10, aces have a value of 1 or 11, and the remaining cards are worth their face value.</p><p>If your hand exceeds 21, you bust and automatically lose the hand. If you have the same card total as the dealer, there is no winner and your wager is returned. This is known as a push.</p><p>The dealer stands on 17 and above and draws on 16 and below.</p><p>The dealer hits on soft 17 (ace and a six).</p><p>If the dealer has 22, a push is declared and bets are returned unless the player has Blackjack in which case the player wins.</p><p>A hand may only be split once.</p><p>Only one card is dealt if aces are split.</p><p>If the dealer’s face-up card is an ace, you can buy insurance.</p><p>The optional Super Match bet lets you wager on whether the first cards dealt to you will be a pair, three of a kind, two pair, or four of a kind.</p><h1 class="style3">You Win!</h1><p>If you are dealt an ace and a card with a value of 10, you have Blackjack (21). The payout is 1:1 (unless you split).</p><p>If you split and are dealt Blackjack (21), the payout is 1:1.</p><p>For all other hands that beat the dealer’s hand, the payout is 1:1.</p><p>If the dealer is dealt a hand equal to yours, including Blackjack, it is a push. Your wager is returned to you.</p><p></p><table class="maincontent" cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><th width="66%">Hand</th><th width="34%">Payout</th></tr><tr><td>Dealer busts (hand exceeds 22)</td><td><p>Player wins</p></td></tr><tr><td>Dealer pushes (hand is 22)</td><td><p>No loss or gain</p></td></tr><tr><td>Dealer pushes (hand is 22), Player gets Blackjack</td><td><p>Player wins</p></td></tr><tr><td>Player’s hand is closest to 21</td><td>Player wins</td></tr><tr><td>Player busts (hand exceeds 21)</td><td>Dealer wins</td></tr><tr><td>Dealer's hand is closest to 21</td><td>Dealer wins</td></tr><tr><td>Hands are equal or a push is achieved</td><td>No loss or gain</td></tr></tbody></table><p> </p><p>The optional Super Match bet is calculated separately from your regular Blackjack wager. If a pair, three of a kind, two pair, or four of a kind is dealt, winnings are calculated before the round ends. Winnings are paid out at the end of the round.</p><p></p><table class="maincontent" cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><th width="66%">Hand</th><th width="34%">Odds</th></tr><tr><td>A pair</td><td><p>1:1</p></td></tr><tr><td>Three of a kind</td><td>5:1</td></tr><tr><td>Two pair</td><td>8:1</td></tr><tr><td>Four of a kind</td><td>40:1</td></tr></tbody></table><p> </p><h1 class="style3">Let’s Play!</h1></div><p>To play Blackjack Switch:</p><ol><li>Select a chip value.</li><li>Click either betting circle until the number of chips displayed equals the amount you wish to bet.</li><li>Optionally, click the Super Match circle to place a Super Match bonus bet.<br />
The Super Match bonus bet pays out a bonus when the cards dealt to you contain one of the following combinations: one pair, three of a kind, two pair, or four of a kind. The win ratios are shown below.</li><li>Optionally, click Clear to remove your bets from the betting circles.</li><li>Click Deal.<br />
You are dealt two hands of two cards and the dealer is dealt two cards. Your cards are dealt face up. Only the dealer’s second card is dealt face up. If the dealer is dealt an ace face up, you may buy insurance.</li><li>Review your cards and then do one of the following:</li></ol><div style="MARGIN-LEFT: 2em" start="3"><ul><li>Click <strong>Switch</strong> to switch the second card dealt to each hand to the other hand.</li><li>Click <strong>Hit</strong> to have another card dealt to you.</li><li>Click <strong>Stand</strong> if you are satisfied with your hands.</li><li>Click <strong>Double</strong> if you are satisfied with your hand and wish to double your initial wager.</li><li>Click <strong>Split</strong> if you receive two cards of equal value and wish to split them into two new hands of one card each.<br /><br />
After your betting decisions are made, the dealer plays out the round and calculates any winnings.</li></ul></div><p>To play again:</p><ol><li>Click Play Again.<br />
Your bets from the last round are placed in the betting circles.</li><li>Optionally, click Clear to remove your bets from the betting circles.<br />
If Clear is clicked, the Repeat Bets button appears. You may place a new bet or click Repeat Bets to repeat your bets from the last round you played.</li><li>Click Deal.<br />
You are dealt two hands of two cards and the dealer is dealt two cards. Your cards are dealt face up. Only the dealer’s second card is dealt face up. If the dealer is dealt an ace face up, you may buy insurance.</li><li>Review your cards and then do one of the following:</li></ol><div style="MARGIN-LEFT: 2em" start="3"><ul><li>Click <strong>Switch</strong> to switch the second card dealt to each hand to the other hand.</li><li>Click <strong>Hit</strong> to have another card dealt to you.</li><li>Click <strong>Stand</strong> if you are satisfied with your hand and your wager.</li><li>Click <strong>Double</strong> if you are satisfied with your hand and wish to double your initial wager.</li><li>Click <strong>Split</strong> if you receive two cards of equal value and wish to split them into two new hands of one card each.<br /><br />
After your betting decisions are made, the dealer plays out the round and calculates any winnings.</li></ul></div>
					</div>

		
<!-- End Pane4 -->

<!-- ======== END TAB ============================================================ -->

<!-- ======== NEW TAB ============================== ============================ -->
<div>
<div class="basic-tabs-content"><!-- Pane5 -->


<div id="Roulette-American" class="RoundBody">
						          
          
<div class="headline"><h1>American Roulette</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table cellspacing="0" cellpadding="0"><tbody><tr><td><p class="rules">In the early eighteenth century, Hoca was played in casinos across Europe. The game evolved to include a spinning wheel with numbered slots and a numbered table.</p><p class="rules">In American Roulette, the wheel is spun counter-clockwise and then a ball is spun clockwise around the wheel. Eventually, the ball drops into one of the wheel’s numbered slots. Players place bets on the numbered table in an attempt to determine which number or color the ball will land on.</p><p class="rules">American Roulette has 38 numbered slots, including 0 and 00.</p><p class="rules">To play American Roulette:</p><ol><li class="rules">Select a chip value.</li><li class="rules">Click an area or areas of the betting table until the number of chips displayed equals the amount you wish to bet. As your mouse hovers over a square on the table, the square is highlighted indicating this is a potential betting spot.<br />
When your first bet is placed on the betting table, the Remove, Clear All, and Spin buttons are displayed.</li><li><span class="rules">Optionally, click Clear All to remove all your chips from the betting table.</span><p class="rules"><br />
Or<br /><br />
To remove a single chip:</p><ol><li class="rules">Click Remove.</li><li class="rules">Click the chip or chips you want removed from the betting table.</li><li class="rules">Click Remove: On.<br />
You can now add chips to the betting table.</li></ol></li><li class="rules">Click Spin.<br />
The wheel is spun counter-clockwise, and the ball is spun clockwise. As the wheel slows down, the ball drops into one of the numbered grooves. If you placed a bet on either the number or the color, you win.</li></ol><p class="rules">To play again:</p><ol><li class="rules">Click Play Again.<br />
Your bet or bets from the last round are placed on the betting table.</li><li><span class="rules">Optionally, click Clear All to remove all your chips from the betting table.<br />
If Clear All is clicked, the Repeat Bets button appears. You may place a new bet or click Repeat Bets to repeat your bet or bets from the last round you played.</span><p class="rules"><br />
Or<br /><br />
To remove a single chip:</p><ol><li class="rules">Click Remove.</li><li class="rules">Click the chip or chips you want removed from the betting table.</li><li class="rules">Click Remove: On.<br />
You can now add chips to the betting table.</li></ol></li><li class="rules">Click Spin.<br />
The wheel is spun counter-clockwise, and the ball is spun clockwise. As the wheel slows down, the ball drops into one of the numbered grooves. If you placed a bet on either the number or the color, you win.</li></ol><h1 class="rules">Bet Types</h1></div><p class="rules">There are multiple types of bets that can be placed simultaneously when playing American Roulette. They are categorized as either Inside or Outside bets. With each spin of the wheel, you can place multiple bets.</p><h3 class="rules"><br />
Inside Bets</h3><p class="rules">The following Inside bets can be made in American Roulette:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="19%">Bet Type</td><td class="rules" width="81%">Definition</td></tr><tr><td class="rules">Straight</td><td class="rules">A bet on one number. The chip is placed in the center of the number you want to bet on (1 – 36, 0, 00). If the ball lands on this number, you win.</td></tr><tr><td class="rules">Split or Two Number</td><td class="rules">A bet on two numbers. The chip is placed on the line between the two numbers you want to bet on. If the ball lands on either of these numbers, you win.</td></tr><tr><td class="rules">Street or Three Number</td><td class="rules">A bet on three adjacent numbers. The chip is placed on the outside line of the row you want to bet on. If one of these three numbers comes up, you win.</td></tr><tr><td class="rules">Corner</td><td class="rules">A bet on four numbers. The chip is placed in the center of four numbers. If the ball lands on one of these numbers, you win.</td></tr><tr><td class="rules">Five-Number Line</td><td class="rules">A bet on five numbers; however, only the numbers 0, 00, 1, 2, and 3 are wagered on. The chip is placed on the outside line that divides the 0, 00 rows and the 1, 2, 3 rows. If the ball lands on one of these five numbers, you win.</td></tr><tr><td class="rules">Six-Number Line</td><td><p class="rules">A bet on two rows of three numbers. The chip is placed between the two rows along the outside edge. If the ball lands on one of these six numbers, you win.</p></td></tr></tbody></table><p> </p><h3 class="rules">Outside Bets</h3><p class="rules">The following Outside bets can be made in Roulette:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="19%">Bet Type</td><td class="rules" width="81%">Definition</td></tr><tr><td class="rules">Any Red or Black</td><td class="rules">A bet on the color of the winning number. The chip is placed on either the black or red diamond.</td></tr><tr><td class="rules">Any Low or High Number</td><td class="rules">A bet on the range within which the winning number will fall. The chip is placed on the 1 To 18 spot or the 19 To 36 spot.</td></tr><tr><td class="rules">Any Even or Odd</td><td class="rules">A bet on all odd or even numbers. The chip is placed on either the even or odd spot.</td></tr><tr><td class="rules">Column</td><td class="rules">A bet on one of the three columns of twelve numbers. The chip is placed on one of the 2 To 1 spots. The 0 and 00 spots are not covered by this bet.</td></tr><tr><td class="rules">Dozen</td><td class="rules">A bet on one of the table’s three sections of numbers. The chip is placed on the 1 To 12, 13 To 24, or 25 To 36 spot.</td></tr></tbody></table><p> </p><h1 class="rules">History Table</h1></div><p class="rules">The previous winning numbers are displayed in the History Table found in the upper right corner of the screen. The most recently called number is always displayed at the top of the History Table.</p><p class="rules"><br />
Winning numbers are displayed in one of three possible colors:<br /></p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="87">Color</td><td class="rules" width="325">Used to display the following numbers</td></tr><tr><td class="rules">Red</td><td><div class="rules" align="left">Red</div></td></tr><tr><td class="rules">Yellow</td><td><div class="rules" align="left">Black</div></td></tr><tr><td class="rules">Green</td><td><div align="left"><p class="rules">00 or 0</p></div></td></tr></tbody></table><p> </p><h1 class="rules">You Win!</h1></div><p class="rules">The following bet types pay out according to the following odds:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="278">Bet Type</td><td class="rules" width="91">Payout</td></tr><tr><td class="rules">Straight</td><td><div class="rules" align="left">35:1</div></td></tr><tr><td class="rules">Split or Two Number</td><td><div class="rules" align="left">17:1</div></td></tr><tr><td class="rules">Street or Three Number</td><td><div align="left"><p class="rules">11:1</p></div></td></tr><tr><td class="rules">Corner</td><td class="rules">8:1</td></tr><tr><td class="rules">Five-Number Line</td><td class="rules">6:1</td></tr><tr><td class="rules">Six-Number Line</td><td class="rules">5:1</td></tr><tr><td class="rules">Any Red or Black</td><td class="rules">1:1</td></tr><tr><td class="rules">Any Low Number or High Number</td><td class="rules">1:1</td></tr><tr><td class="rules">Any Even or Odd</td><td class="rules">1:1</td></tr><tr><td class="rules">Dozen</td><td class="rules">2:1</td></tr><tr><td class="rules">Column</td><td class="rules">2:1</td></tr></tbody></table></td></tr></tbody></table>
					


</div><!-- End Pane5 -->		
</div>
<!-- ======== END TAB  ============================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->

<div id="Roulette-European" class="RoundBody">
						          
          
<div class="headline"><h1>European Roulette</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table cellspacing="0" cellpadding="0"><tbody><tr><td><p class="rules">In the early eighteenth century, Hoca was played in casinos across Europe. The game evolved to include a spinning wheel with numbered slots and a numbered table.</p><p class="rules">In European Roulette, the wheel is spun counter-clockwise and then a ball is spun clockwise around the wheel. Eventually, the ball drops into one of the wheel’s numbered slots. Players place bets on the numbered table in an attempt to determine which number or color the ball will land on.</p><p class="rules">European Roulette has 37 numbered slots, labeled from 0 to 36 in non-sequential order.</p><p class="rules">To play European Roulette:</p><ol><li class="rules">Select a chip value.</li><li class="rules">Click an area or areas of the betting table until the number of chips displayed equals the amount you wish to bet. As your mouse hovers over a square on the table, the square is highlighted indicating this is a potential betting spot.<br />
When your first bet is placed on the betting table, the Remove, Clear All, and Spin buttons are displayed.</li><li><span class="rules">Optionally, click Clear All to remove all your chips from the betting table.</span><p class="rules"><br />
Or<br /><br />
To remove a single chip:</p><ol><li class="rules">Click Remove.</li><li class="rules">Click the chip or chips you want removed from the betting table.</li><li class="rules">Click Remove: On.<br />
You can now add chips to the betting table.</li></ol></li><li class="rules">Click Spin.<br />
The wheel is spun counter-clockwise, and the ball is spun clockwise. As the wheel slows down, the ball drops into one of the numbered slots. If you placed a bet on either the number or the color, you win.</li></ol><p class="rules">To play again:</p><ol><li class="rules">Click Play Again.<br />
Your bet or bets from the last round are placed on the betting table.</li><li><span class="rules">Optionally, click Clear All to remove all your chips from the betting table.<br />
If Clear All is clicked, the Repeat Bets button appears. You may place a new bet or click Repeat Bets to repeat your bet or bets from the last round you played.</span><p class="rules"><br />
Or<br /><br />
To remove a single chip:</p><ol><li class="rules">Click Remove.</li><li class="rules">Click the chip or chips you want removed from the betting table.</li><li class="rules">Click Remove: On.<br />
You can now add chips to the betting table.</li></ol></li><li class="rules">Click Spin.<br />
The wheel is spun counter-clockwise, and the ball is spun clockwise. As the wheel slows down, the ball drops into one of the numbered slots. If you placed a bet on either the number or the color, you win.</li></ol><h1 class="rules">Bet Types</h1></div><p class="rules">There are multiple types of bets that can be placed simultaneously when playing European Roulette. They are categorized as either Inside or Outside bets. With each spin of the wheel, you can place multiple bets.</p><h3 class="rules"><br />
Inside Bets</h3><p class="rules">The following Inside bets can be made in European Roulette:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="19%">Bet Type</td><td class="rules" width="81%">Definition</td></tr><tr><td class="rules">Straight</td><td class="rules">A bet on one number. The chip is placed in the center of the number you want to bet on (1 – 36, 0). If the ball lands on this number, you win.</td></tr><tr><td class="rules">Split or Two Number</td><td class="rules">A bet on two numbers. The chip is placed on the line between the two numbers you want to bet on. If the ball lands on either of these numbers, you win.</td></tr><tr><td class="rules">Street or Three Number</td><td class="rules">A bet on three adjacent numbers. The chip is placed on the outside line of the row you want to bet on. If one of these three numbers comes up, you win.</td></tr><tr><td class="rules">Corner</td><td class="rules">A bet on four numbers. The chip is placed in the center of four numbers. If the ball lands on one of these numbers, you win.</td></tr><tr><td class="rules">Six-Number Line</td><td><p class="rules">A bet on two rows of three numbers. The chip is placed between the two rows along the outside edge. If the ball lands on one of these six numbers, you win.</p></td></tr></tbody></table><p> </p><h3 class="rules">Outside Bets</h3><p class="rules">The following Outside bets can be made in Roulette:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="19%">Bet Type</td><td class="rules" width="81%">Definition</td></tr><tr><td class="rules">Any Red or Black</td><td class="rules">A bet on the color of the winning number. The chip is placed on either the black or red diamond.</td></tr><tr><td class="rules">Any Low or High Number</td><td class="rules">A bet on the range within which the winning number will fall. The chip is placed on the 1 To 18 spot or the 19 To 36 spot.</td></tr><tr><td class="rules">Any Even or Odd</td><td class="rules">A bet on all odd or even numbers. The chip is placed on either the even or odd spot.</td></tr><tr><td class="rules">Column</td><td class="rules">A bet on one of the three columns of twelve numbers. The chip is placed on one of the 2 To 1 spots. The 0 spot is not covered by this bet.</td></tr><tr><td class="rules">Dozen</td><td class="rules">A bet on one of the table’s three sections of numbers. The chip is placed on the 1 To 12, 13 To 24, or 25 To 36 spot.</td></tr></tbody></table><p> </p><h1 class="rules">History Table</h1></div><p class="rules">The previous winning numbers are displayed in the History Table found in the upper right corner of the screen. The most recently called number is always displayed at the top of the History Table.</p><p class="rules"><br />
Winning numbers are displayed in one of three possible colors:<br /></p><p class="rules"></p>

<table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="87">Color</td><td class="rules" width="325">Used to display the following numbers</td></tr><tr><td class="rules">Red</td><td><div class="rules" align="left">Red</div></td></tr><tr><td class="rules">Yellow</td><td><div class="rules" align="left">Black</div></td></tr><tr><td class="rules">Green</td><td><div align="left"><p class="rules">0</p></div></td></tr></tbody></table><p> </p><h1 class="rules">You Win!</h1></div><p class="rules">The following bet types pay out according to the following odds:</p><p class="rules"></p><table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><td class="rules" width="278">Bet Type</td><td class="rules" width="91">Payout</td></tr><tr><td class="rules">Straight</td><td><div class="rules" align="left">35:1</div></td></tr><tr><td class="rules">Split or Two Number</td><td><div class="rules" align="left">17:1</div></td></tr><tr><td class="rules">Street or Three Number</td><td><div align="left"><p class="rules">11:1</p></div></td></tr><tr><td class="rules">Corner</td><td class="rules">8:1</td></tr><tr><td class="rules">Six-Number Line</td><td class="rules">5:1</td></tr><tr><td class="rules">Any Red or Black</td><td class="rules">1:1</td></tr><tr><td class="rules">Any Low Number or High Number</td><td class="rules">1:1</td></tr><tr><td class="rules">Any Even or Odd</td><td class="rules">1:1</td></tr><tr><td class="rules">Dozen</td><td class="rules">2:1</td></tr><tr><td class="rules">Column</td><td class="rules">2:1</td></tr></tbody></table></td></tr></tbody></table>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Craps</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Craps has been a popular casino game since the era of the Roman gladiators. Players bet on whether the shooter will or will not be able to roll a winning combination with a pair of six-sided dice. You'll be hard-pressed to find a game with more excitement and fun than Craps.</p><p><b>Rules</b> </p><p>To understand craps the first thing you need to learn is that it is a game of rounds. The first roll is called the "come out" roll. Sometimes the outcome of a round will be determined on the come out roll. A 2, 3, 7, 11, or 12 on the come out roll immediately ends a round. If any other total is rolled (4, 5, 6, 8, 9, or 10) that number is called the point. The dealer will place a white puck (white circle which can be either "on" or "off") on an area of the table to designate which point is "on". If a point is rolled, the dice will be rolled continuously until the same point is rolled again or a 7 is rolled, which ends the round and all bets that are still outstanding, lose. When that happens, the point comes off and new round starts.</p><p><b>Bet types:</b> </p><p><b>Pass</b></p><p>This is the most common bet in craps. You place your bet on the Pass Line area on the table on a come out roll. If the come out roll is a 7 or 11 you win. If the come out roll is a 2, 3, or 12 you lose. If any point is rolled on the come out roll and if the same point is rolled before a 7 is rolled, you win. If a 7 comes before the point you lose. You cannot remove a Pass Line bet after the point has been made. Pass Line bet pays 1:1.</p><p><b>Taking the Odds (Pass Line Odds)</b> </p><p>The odds are simply an additional wager that the point will be rolled before a 7 and can be only placed after the come out role if the point is made. Odds bets are paid out at: 2:1 if the point is 4 or 10, 3:2 if the point is 5 or 9, and 6:5 if the point is 6 or 8. Odds bets can be removed or reduced at any time. You can only place the Odds bet if you have a Pass bet on. The maximum amount of the Odds bet is the amount of the Pass Line bet.</p><p><b>Don't Pass</b> </p><p>This is almost the opposite of the Pass Line bet. This bet is placed against the shooter and wins if craps (2 or 3) is thrown on the come-out roll and loses if 7 or 11 are thrown. A 12 is a push. If any point is rolled on the come out roll and if the same point is rolled before a 7 is rolled, you lose. If the 7 comes before the point you win. You cannot remove a Don't Pass bet after the point has been made. Don't Pass Line bet pays 1:1.</p><p><b>Laying the Odds (Don't Pass Line Odds)</b> </p><p>On the contrary to Taking the Odds, Laying the odds is an additional wager that 7 will be rolled before the point; and it can be only placed after the point has been made. Odds bets are paid out at: 1:2 if the point is 4 or 10, 2:3 if the point is 5 or 9, and 5:6 if the point is 6 or 8. Odds bet can be removed or reduced at any time.</p><p><b>Come</b></p><p>This bet is exactly the same as a Pass Line bet but it only can be made after the point is established. If 7 or 11 rolls, you win. If a 2, 3, or 12 come, you lose. If 4, 5, 6, 8, 9, 10 rolls, the dealer moves your bet to the number that rolled which becomes your 'point' for that bet. If you throw this same number again, you win; if a 7 is rolled before your point, you lose. Once your bet has been moved to the appropriate box, if 4, 5, 6, 8, 9, 10 was thrown when you placed your Come bet, you can take the odds on it just like taking the odds on a Pass Line Bet.</p><p><b>Don't Come</b></p><p>Again, this bet is similar to the Don't Pass bet with only one difference, it can be made only after a point has been made. You cannot remove the Don't Come bet after it has been placed until it is resolved. Once your bet has been moved to the appropriate box, if 4, 5, 6, 8, 9, 10 was thrown when you placed your Don't Come bet, you can take the odds on it just like taking the odds on a Don't Pass Line Bet.</p><p><b>Buy</b> </p><p>This bet can be made on the following numbers: 4, 5, 6, 8, 9, or 10. It is a bet that the relevant number will be thrown before a 7. Buy bets can only be made or altered when the point is on, as identified by the puck. Note: Our Craps game charges a 5% Lay Commission on both winning and losing Buy wagers.</p><p><b>Lay</b></p><p>Lay bet is the opposite of the buy bet. This bet can be made on the following numbers: 4, 5, 6, 8, 9, or 10. It is a bet that a 7 will be thrown before the relevant number. Lay bets can only be made or altered when the point is on, as identified by the puck. Note: Our Craps game charges a 5% Lay Commission on both winning and losing Lay wagers.</p><p><b>Field</b> </p><p>This is a bet on the outcome of the next throw only. It pays even money if a 3, 4, 9, 10 or 11 is thrown and 2:1 if Snake Eyes (2) or Boxcars (12) are thrown; if 5,6,7 or 8 come, you lose.</p><p><b>Hard Way</b> </p><p>This bet can be made on 4, 6, 8 or 10. It is a bet that the relevant number will be thrown in its double form (for instance, a hard 8 occurs when two 4s are thrown) before a 7 and before that number is thrown in any other combination.</p><p><b>Propositions</b> </p><p>A number of wagering options that includes any craps, any 7 and either 2, 12, 3 or 11 being thrown on the next roll. For details and payouts please see the table below.</p><p><b>Big 6</b></p><p>A Big 6 bet is even money bet that 6 will be rolled before 7.</p><p><b>Big 8</b> </p><p>A Big 8 bet is even money bet that 8 will be rolled before 7.<br /></p><p><b>You Win!</b> </p><table class="rules" cellspacing="0" cellpadding="2" width="100%" border="0"><tbody><tr><td width="40%"><b>Bet Type</b></td><td width="53%" height="25"><b>Payout</b></td></tr><tr><td width="40%">Pass Line</td><td width="53%">1:1</td></tr><tr><td width="40%">Don't Pass Line</td><td width="53%">1:1</td></tr><tr><td width="40%">Come Bet</td><td width="53%">1:1</td></tr><tr><td width="40%">Don't Come Bet</td><td width="53%">1:1</td></tr><tr><td width="40%">Pass/Come Taking the Odds</td><td width="53%"> </td></tr><tr><td width="40%">- 6 or 8</td><td width="53%">6:5</td></tr><tr><td width="40%">- 5 or 9</td><td width="53%">3:2</td></tr><tr><td width="40%">- 4 or 10</td><td width="53%">2:1</td></tr><tr><td width="40%">Pass/Come Laying the Odds</td><td width="53%"> </td></tr><tr><td width="40%">- 6 or 8</td><td width="53%">5:6</td></tr><tr><td width="40%">- 5 or 9</td><td width="53%">2:3</td></tr><tr><td width="40%">- 4 or 10</td><td width="53%">1:2</td></tr><tr><td width="40%">Field Bet</td><td width="53%"> </td></tr><tr><td width="40%">- 3, 4, 9, 10, 11</td><td width="53%">1:1</td></tr><tr><td width="40%">- 2 or 12</td><td width="53%">2:1</td></tr><tr><td width="40%">Buy Bets</td><td width="53%"> </td></tr><tr><td width="40%">- 6 or 8</td><td width="53%">6:5 less 5% of the Wager Amount</td></tr><tr><td width="40%">- 5 or 9</td><td width="53%">3:2 less 5% of the Wager Amount</td></tr><tr><td width="40%">- 4 or 10</td><td width="53%">2:1 less 5% of the Wager Amount</td></tr><tr><td width="40%">Lay Bets</td><td width="53%"> </td></tr><tr><td width="40%">- 6 or 8</td><td width="53%">5:6 less 5% of the Wager Amount</td></tr><tr><td width="40%">- 5 or 9</td><td width="53%">2:3 less 5% of the Wager Amount</td></tr><tr><td width="40%">- 4 or 10</td><td width="53%">1:2 less 5% of the Wager Amount</td></tr><tr><td width="40%">Big 6 or 8</td><td width="53%">1:1</td></tr><tr><td width="40%">Proposition Bets</td><td width="53%"> </td></tr><tr><td width="40%">- Any 7</td><td width="53%">4:1</td></tr><tr><td width="40%">- Any 11</td><td width="53%">15:1</td></tr><tr><td width="40%">- Any Craps (2, 3, or 12)</td><td width="53%">7:1</td></tr><tr><td width="40%">- Craps 2</td><td width="53%">30:1</td></tr><tr><td width="40%">- Craps 3</td><td width="53%">15:1</td></tr><tr><td width="40%">- Craps 12</td><td width="53%">30:1</td></tr><tr><td width="40%">Hard Way Bets</td><td width="53%"> </td></tr><tr><td width="40%">- Hard 6 or 8</td><td width="53%">9:1</td></tr><tr><td width="40%">- Hard 4 or 10</td><td width="53%">7:1</td></tr></tbody></table><br /><p><b>Craps Terms and Lingo</b> </p><div align="center"><img height="540" src="/rules/RULES/rules_cr_files/craps_table2.gif" width="521" /> </div><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Three Card Poker</h1><p class="purplebodycopy">Three Card Poker brings together the strategy of poker with the excitement of jackpot style bonuses. In Three Card Poker, a multi-hand variation of poker, you can play up to three hands against the dealer's hand. Beat the dealer and you'll win big. Play Pair Plus, get a pair or better and win even more!</p><p><strong><a class="bookmark" id="letsplay" title="letsplay" name="letsplay"></a><span class="rules style1">Let'sPlay!</span></strong></p><p class="purplebodycopy">At the beginning of each game you must place an initial bet on the Ante and/or the Pair Plus betting areas.</p><ol><li class="purplebodycopy">From the lower right of the screen, choose a chip value.</li><li class="purplebodycopy">In the betting areas, click the following:</li></ol><ul><li style="LIST-STYLE-TYPE: none"><ul><li><span class="purplebodycopy">Ante - until the number of chips displayed equals the amount you wish to bet.</span></li><li><span class="purplebodycopy">Pair Plus - until the number of chips displayed equals the amount you wish to bet.</span></li></ul></li><li><span class="purplebodycopy">Click Clear to remove your chips from the betting area.</span></li></ul><ol class="purplebodycopy" start="3"><li>Click Deal. Three cards are dealt to each of the active seats.</li></ol><ol class="purplebodycopy" start="4"><li>Click Play or Fold for each hand starting with the seat to the left of the dealer. 

<ul><li>Choose Fold, if you have a hand that you do not believe will win. When you choose Fold, the game ends with the dealer winning. You will lose all bets made for the current betting position.</li><li>Choose Play, if you have a hand that you believe will win. When you choose Play, a bet equal to your initial Ante will be placed on the Play betting area.</li><li class="purplebodycopy">Do not fold a hand that qualifies for the Pair Plus bonus or you will not be eligible for payout according to the Pair Plus bonus payout tables.</li></ul></li></ol><p class="menurightheading"><strong><a class="bookmark" id="rules" title="rules" name="rules"></a><span class="style1">Rules</span></strong></p><p class="purplebodycopy">Three Card Poker uses a one deck shoe.</p><p class="purplebodycopy">If the dealer's hand does not have a rank of Queen or higher, the dealer fails to qualify and the game is over. The Ante bonus and Pair Plus bonuses are paid and your Play bet is returned to you with double your Ante.</p><ul><li class="purplebodycopy">You place a $25 dollar bet on the Ante and $25 on the Pair Plus. You are dealt a straight, choose Play, and place another $25 bet. Your total bet is now $75.<br /><br />
The dealer shows his cards and fails to qualify.<br /><br />
Your Play bet is returned to you, along with double your Ante bet, a total of $75.<br /><br />
Because you were dealt a straight, the Ante bonus is paid at 1:1, $25 and the Pair Plus bet is paid out at the bonus rate of 6:1, $175 including your initial outlay.<br /><br />
You win a total of $275 including your initial outlay.<br /></li></ul><p><span class="purplebodycopy">If the dealer's hand qualifies, it is compared to your hand and actioned as follows:</span></p><ul><li><span class="purplebodycopy">If the dealer's hand is higher, you lose both your Ante and your Play bets.</span></li><li><span class="purplebodycopy">If the dealer's hand is lower, you receive a 1:1 payout on your Ante bet and your Play bet</span></li><li><span class="purplebodycopy">If your hand and the dealer's tie on rank, whoever has the next highest ranking card is the winner.</span></li><li><span class="purplebodycopy">If there is an absolute tie, you get your Ante and Play bets returned.</span></li></ul><p class="purplebodycopy">Bonuses will be paid according to the Ante bonus and Pair Plus payout tables. The odds that your winning hand pays out are also shown on the table top next to the betting area.</p><p class="purplebodycopy">Once the game is over, you can change your bets or click Repeat Bet and start a new round.</p><p class="menurightheading style1"><strong><a class="bookmark" id="hands" title="hands" name="hands"></a>Three Card Poker Hand Rankings</strong></p><table cellspacing="0" cellpadding="0" width="90%" align="center" border="0"><tbody><tr><td class="rules" width="125">Hand</td><td class="rules" width="364">Definition</td><td class="rules" width="139">Example</td></tr><tr><td class="rules">Straight Flush</td><td class="rules">All three cards are in sequence and all the same suit with Ace-King-Queen being the highest</td><td><img height="35" alt="Royal Flush" src="/rules/RULES/images/HA.gif" width="30" /> <img height="35" alt="Royal Flush" src="/rules/RULES/images/HK.gif" width="30" /> <img height="35" alt="Royal Flush" src="/rules/RULES/images/HQ.gif" width="30" />  </td></tr><tr><td class="rules">Three of a Kind</td><td class="rules">All three cards are of the same face value (different suits)</td><td><img height="35" src="/rules/RULES/images/DA.gif" width="30" /> <img height="35" src="/rules/RULES/images/SA.gif" width="30" /> <img height="35" src="/rules/RULES/images/HA.gif" width="30" /></td></tr><tr><td class="rules">Straight</td><td class="rules">All three cards are in sequence, but not all in the same suit</td><td><img height="35" src="/rules/RULES/images/DA.gif" width="30" /> <img height="35" src="/rules/RULES/images/CK.gif" width="30" /> <img height="35" src="/rules/RULES/images/SQ.gif" width="30" /></td></tr><tr><td class="rules">Flush</td><td class="rules">All three cards are in the same suit, but not all in a sequence</td><td><img height="35" src="/rules/RULES/images/HA.gif" width="30" /> <img height="35" src="/rules/RULES/images/HQ.gif" width="30" /> <img height="35" src="/rules/RULES/images/H10.gif" width="30" /></td></tr><tr><td class="rules">Pair</td><td class="rules">Two cards of the same face value</td><td><img height="35" src="/rules/RULES/images/DA.gif" width="30" /> <img height="35" src="/rules/RULES/images/HA.gif" width="30" /> <img height="35" src="/rules/RULES/images/HK.gif" width="30" /> </td></tr><tr><td class="rules">High Card</td><td class="rules">Highest single card</td><td><img height="35" src="/rules/RULES/images/HA.gif" width="30" /> <img height="35" src="/rules/RULES/images/CQ.gif" width="30" /> <img height="35" src="/rules/RULES/images/S10.gif" width="30" /> </td></tr></tbody></table><ul><li class="purplebodycopy">Aces play both high and low in a straight.</li><li class="purplebodycopy">In Three Card Poker a three card straight is higher than a three card flush, because the probability of a flush is higher when three cards are dealt.</li></ul><p class="menurightheading style1"><strong><a class="bookmark" id="youwin" title="youwin" name="youwin"></a>You Win!</strong></p><p class="purplebodycopy">There are three ways to win in Three Card Poker. Beat the dealer and win a 1:1 payout on your Ante and Play bet. Receive a bonus when your hand qualifies for one of the Ante bonus or Pair Plus hand rankings.</p><p class="menurightheading"><strong><a class="bookmark" id="antebonus" title="antebonus" name="antebonus"></a><span class="style1">Ante Bonus</span></strong></p><p class="purplebodycopy">When your hand qualifies for one of the Ante bonus hand rankings listed below, you are paid out regardless of the outcome of the initial Ante bet.</p><table cellspacing="0" cellpadding="0" width="90%" align="center" border="0"><tbody><tr><td class="rules" width="175">Hand</td><td class="rules" width="175">Payout</td></tr><tr><td class="rules">Straight Flush</td><td class="rules"><div align="left">5:1</div></td></tr><tr><td class="rules">Three of a Kind</td><td class="rules"><div align="left">4:1</div></td></tr><tr><td class="rules">Straight</td><td class="rules"><div align="left">1:1</div></td></tr></tbody></table><p class="menurightheading"><strong><a class="bookmark" id="pairsplus" title="pairsplus" name="pairsplus"></a><span class="style1">Pair Plus</span></strong></p><p class="purplebodycopy">When your hand qualifies for one of the Pair Plus hand rankings listed below, you are paid out regardless of the outcome of the initial Ante bet.</p><table cellspacing="0" cellpadding="0" width="90%" align="center" border="0"><tbody><tr><td class="rules" width="175">Hand</td><td class="rules" width="175">Payout</td></tr><tr><td class="rules">Straight Flush</td><td class="rules"><div align="left">40:1</div></td></tr><tr><td class="rules">Three of a Kind</td><td class="rules"><div align="left">30:1</div></td></tr><tr><td class="rules">Straight</td><td class="rules"><div align="left">6:1</div></td></tr><tr><td class="rules">Flush</td><td class="rules"><div align="left">4:1</div></td></tr><tr><td class="rules">Pair</td><td class="rules"><div align="left">1:1</div></td></tr></tbody></table><p class="LinkHeader"> </p><p><b>Important Note:</b> <span class="purplebodycopy">Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</span></p><p> </p><p><b>You Win!</b> <br /></p><div align="center"><img height="674" src="/rules/RULES/rules_sb_files/sb_table2.gif" width="516" /> </div><p><b>Addendum:</b> Three Dice Totals that roll 4 or 17 pay 60:1</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Jacks or Better Video Poker</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Jacks or Better video poker is extremely popular because it moves fast and is easy to play, while retaining an element of strategy. The object of Jacks or Better video poker is to attain the highest ranked poker hand. The payout for each poker combination is displayed on the video poker machine face.</p><p><b>Rules</b> </p><p>Jacks or Better video poker uses a one deck shoe.</p><p>The deck is re-shuffled after every hand.</p><p>Five cards are dealt to you after you place the bet. Once the cards are dealt, you may choose to keep any of the cards by either clicking Hold, found directly below the card, or on the card. Once you have selected which cards to hold, click Deal and the remaining cards will be exchanged for new cards.</p><p>After this second deal, you are paid out based on the rank of the resulting poker hand.</p><p><b>Jacks or Better Video Poker</b> <br /></p><div align="center"><img height="452" src="/rules/RULES/rules_jb_files/jb_table1.gif" width="518" /> </div><p><b>You Win!</b> <br />
A winning hand pays out according to the number of coins wagered and their value.<br /><b>Example:</b> If you select the $1 coin and then Bet Max your wager will be 5 coins of $1 so therefore $5. If you get a Straight Flush then according to the payout schedule below you will win 250 coins with the same value as your original bet so therefore $250.<!--
<DIV ALIGN=CENTER>
<IMG SRC="images/jb_table2.gif">
</DIV>--> </p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Deuces Wild Video Poker</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The object of Deuces Wild video poker is to attain the highest ranked poker hand using two as a wildcard (a two can represent any card in the deck). The payout for each poker card combination is displayed on the video poker machine face.</p><p><b>Rules</b> </p><p>Deuces Wild video poker uses a one deck shoe.</p><p>The deck is re-shuffled after every hand.</p><p>Five cards are dealt to you after you place the bet. Once the cards are dealt, you may choose to keep any of the cards by either clicking Hold, found directly below the card, or on the card. Once you have selected the cards you want to keep, click Deal and the remaining cards will be exchanged for new cards.</p><p>After this second deal, hands are compared and a winning hand is paid out based on the rank of the resulting poker hand.</p><p><b>Deuces Wild Poker Hands</b> <br /></p><div align="center"><img height="501" src="/rules/RULES/rules_dw_files/dw_table1.gif" width="515" /> </div><p><b>You Win!</b> <br />
A winning hand pays out according to the number of coins wagered and their value.</p><p><b>Example:</b> If you select the $1 coin and then Bet Max your wager will be five coins of $1 or $5. If you get a Straight Flush then according to the payout schedule below you will win 65 coins with the same value as your original bet or $65 (65 x $1).<br />
The following payout schedule illustrates the number of coins to be won if you bet 1, 2, 3, 4 or 5 coins. <!--
<DIV ALIGN=CENTER>
<IMG SRC="images/dw_table2.gif">
</DIV>--></p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Caribbean Stud (aka Bermuda Poker)</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Caribbean Stud (Bermuda Poker) is a solo version of the most popular card game, Caribbean poker. Player and dealer receive five cards each and you are paid out according to the rank of your poker hand. The better your hand is the bigger is your payout. Caribbean Stud also offers a separate optional progressive jackpot bet for $1. If you play the progressive and your hand qualifies, you win big!</p><p><b>Rules</b> </p><p>Caribbean Stud uses a one deck shoe which is reshuffled after every hand.</p><p>Before the cards are dealt, you need to place the 'ante' bet on the table. Then the player receives five cards face down. The dealer also receives five cards, four faces down and one face up. At this point player has two options: raise or fold. If you decide to raise, you will need to put double 'ante' amount to play. If you decide to fold, you only lose your 'ante'.</p><p>Dealer must have a minimum combination of an ace/king to qualify. If dealer does not qualify, you only get paid for the 'ante' bet (with the exception of a separate progressive bet payout, which is paid regardless of the dealer qualifying or not).</p><p>If the dealer's hand qualifies, it is compared to your hand. If the dealer's hand is higher, you will lose both your ante and your bet. If the dealer's hand is lower, your ante is paid 1:1 and the will be paid out on your bet according to the payout schedule.</p><p><b>Caribbean Stud Hands</b> <br /></p><div align="center"><img height="488" src="/rules/RULES/rules_cs_files/cs_table.gif" width="514" /> </div><p><b>You win! Winning Hands pay out the following odds:</b> <br /></p><div align="center"><img height="219" src="/rules/RULES/rules_cs_files/cs_table2.gif" width="518" /> </div><p><b>Progressive Jackpot</b> <br />
You can place a $1 bonus bet if you wish to participate in a progressive jackpot. By placing an additional $1 wager, you have the chance to win even when the dealer does not qualify! Below are the winnings based on the optional $1 wager.</p><div align="center"><img height="140" src="/rules/RULES/rules_cs_files/cs_table3.gif" width="518" /> </div><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Showdown Poker</h1><p>Showdown Poker is an exciting variation of the classic poker game Texas Hold’em. Showdown Poker differs from Texas Hold’em in that you play against the house rather than against other players.</p><p>The object of the game is to beat the dealer’s hand by getting the best possible five-card hand from the cards you are dealt and five community cards. If you beat the dealer, you win! To add to the excitement, you can also place a side bet, which pays out if the player’s final hand is three of a kind (or flush) or higher.</p><h1 class="style3">Let’s Play!</h1></div><p>To play Showndown Poker:</p><ol><li>Select a chip value.</li><li>Click the betting circle until the number of chips displayed equals the amount you want to bet.</li><li>If you want to place a side bet, click the<span class="Objects"> Bonus</span> betting circle. To bet a different amount from the ante, select another chip value and then click the <span class="Objects">Bonus</span> betting circle.</li><li>Optionally, click <span class="Objects">Clear</span> to remove all your chips from the table.</li><li>Click <span class="Objects">Deal</span>.</li></ol><p class="Note">You must place the ante bet before dealing can begin.</p><blockquote><p>You are dealt four cards face up, and the dealer is dealt four cards face down.</p></blockquote><ol start="6"><li>Review your cards and do one of the following: 

<ol><li type="a" value="1">Select two cards from your hand and click <span class="Objects">Keep</span>. The cards you have not selected will be discarded.</li><li type="a" value="2">Select two cards from your hand and click <span class="Objects">Split</span> to divide your four cards into two separate hands. When you split your cards, the second hand created is assigned an ante bet and showdown bonus bet equal to your original wager(s), with the chips deducted from your bank.</li></ol><p>After you make your choice, the dealer’s hand will be revealed after first discarding two cards. Five community cards will then be dealt and the winner of the game will be determined.</p></li></ol><ul><li>If you beat the dealer's hand, a <span class="Objects">Win</span> message appears displaying the amount of your total winnings with a summary of both hands. To see a breakdown of your winnings, hover the mouse over the <span class="Objects">Win</span> message.</li><li>If you do not beat the dealer’s hand, a <span class="Objects">Lose</span> message appears and your chips are removed from the betting circles.</li><li>If the play pushes, a <span class="Objects">Push</span> message appears and your chips are removed from the betting circles. All pushes are awarded to the dealer.</li></ul><p>If you placed a bonus bet:</p><ul><li>Your cards are evaluated at the end of the game, after the ante bet has been won or lost.</li><li>You can win or lose your bonus bet regardless of the outcome of the main game.</li></ul><p>To play again:</p><ol><li class="Step">Click <span class="Objects">Play Again</span>.</li></ol><blockquote><p>Your bets from the last round are placed on the table.</p></blockquote><ol start="2"><li>To bet a different amount from the last round, click <span class="Objects">Clear</span>, select another chip value, and click the betting circles that you want to bet on.</li><li>Continue with play as described in steps 5 to 6, above.</li></ol>          
          
<div class="headline"><h1><span class="style3">Rules</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Showdown Poker is a five-card poker game using six 52-card decks that are reshuffled after each hand. To win, you must beat the dealer by getting a higher five-card hand. You and the dealer can both use community cards to complete a hand.</p><p>Play begins when you place an initial bet, the ante bet. You may also place an optional bonus (or showdown) bet. You win this bet if your final hand, including the community cards, is a three of a kind or better.</p><p>After you place the ante, four cards are dealt to you face up and four cards are dealt to the dealer face down. At this point, you must either choose to keep two cards or split your hand. If you split your hand, your original ante bet and bonus bet are duplicated for the second hand, with the balance deducted from your bank.</p><p>After you split or drop two cards, five community cards are dealt and the dealer’s cards are shown. The winner is determined automatically. If your hand beats the dealer’s hand, you win and your ante bet pays 1:1. If the dealer’s hand is better than the player’s, or if there is a push, you lose and your ante bet is removed from the bet circle.</p><p>Even if you lose against the dealer, you still receive bonus winnings that you qualify for, paid out according to the Bonus Bet Pay Table.</p><p> </p><h1 class="style1"><a class="bookmark" id="bettypes" title="showdownhands" name="showdownhands"></a>Showdown Poker Hands</h1></div><p>Paying hands are ranked in descending order, as follows:</p><table class="rules" cellspacing="0" cellpadding="0" border="0"><tbody><tr class="CellHeading"><td width="125">Hand</td><td width="*">Definition</td><td width="175">Example</td></tr><tr><td>Five of a Kind, suited</td><td>Five cards of the same denomination, in the same suit.</td><td><img height="35" alt="Five of a Kind" src="/images/HK.gif" width="30" /> <img height="35" alt="Five of a Kind" src="/images/HK.gif" width="30" /> <img height="35" alt="Five of a Kind" src="/images/HK.gif" width="30" /> <img height="35" alt="Five of a Kind" src="/images/HK.gif" width="30" /> <img height="35" alt="Five of a Kind" src="/images/HK.gif" width="30" /> </td></tr><tr><td>Royal Flush</td><td>10, jack, queen, king, and an ace all of the same suit.</td><td><img height="35" alt="Royal Flush" src="/images/H10.gif" width="30" /> <img height="35" alt="Royal Flush" src="/images/HJ.gif" width="30" /> <img height="35" alt="Royal Flush" src="/images/HQ.gif" width="30" /> <img height="35" alt="Royal Flush" src="/images/HK.gif" width="30" /> <img height="35" alt="Royal Flush" src="/images/HA.gif" width="30" /> </td></tr><tr><td>Straight Flush</td><td>Five cards in sequence, all of the same suit</td><td><img height="35" alt="Straight Flush" src="/images/S3.gif" width="30" /> <img height="35" alt="Straight Flush" src="/images/S4.gif" width="30" /> <img height="35" alt="Straight Flush" src="/images/S5.gif" width="30" /> <img height="35" alt="Straight Flush" src="/images/S6.gif" width="30" /> <img height="35" src="/images/S7.gif" width="30" /></td></tr><tr><td>Five of a Kind</td><td>Five cards of the same denomination</td><td><img height="35" src="/images/D10.gif" width="30" /> <img height="35" src="/images/S10.gif" width="30" /> <img height="35" src="/images/H10.gif" width="30" /> <img height="35" src="/images/C10.gif" width="30" /> <img height="35" src="/images/D10.gif" width="30" /></td></tr><tr><td>Four of a Kind</td><td>Four cards of the same denomination, one in each suit</td><td><img height="35" src="/images/D10.gif" width="30" /> <img height="35" src="/images/S10.gif" width="30" /> <img height="35" src="/images/H10.gif" width="30" /> <img height="35" src="/images/C10.gif" width="30" /> <img height="35" src="/images/D4.gif" width="30" /></td></tr><tr><td>Full House</td><td>Three cards of one denomination and two cards of another denomination</td><td><img height="35" src="/images/HK.gif" width="30" /> <img height="35" src="/images/CK.gif" width="30" /> <img height="35" src="/images/H7.gif" width="30" /> <img height="35" src="/images/D7.gif" width="30" /> <img height="35" src="/images/C7.gif" width="30" /></td></tr><tr><td>Flush</td><td>Five cards all of the same suit</td><td><img height="35" src="/images/H2.gif" width="30" /> <img height="35" src="/images/H6.gif" width="30" /> <img height="35" src="/images/H9.gif" width="30" /> <img height="35" src="/images/HQ.gif" width="30" /> <img height="35" src="/images/HK.gif" width="30" /> </td></tr><tr><td>Straight</td><td>Five cards in sequence of any suit</td><td><img height="35" src="/images/H3.gif" width="30" /> <img height="35" src="/images/C4.gif" width="30" /> <img height="35" src="/images/D5.gif" width="30" /> <img height="35" src="/images/C6.gif" width="30" /> <img src="/images/S7.gif" /></td></tr><tr><td>Three of a Kind</td><td>Three cards of the same denomination and two unmatched cards</td><td><img height="35" src="/images/C9.gif" width="30" /> <img height="35" src="/images/D9.gif" width="30" /> <img height="35" src="/images/H9.gif" width="30" /> <img height="35" src="/images/C6.gif" width="30" /> <img height="35" src="/images/H2.gif" width="30" /></td></tr></tbody></table><p> </p><h1 class="style1"><a class="bookmark" id="letsplay" title="youwin" name="youwin"></a>You Win!</h1></div><p>You win if your hand ranks higher than the dealer’s hand. You win the bonus bet if your final hand is a three of a kind or higher.</p><h3 class="style3"><a class="bookmark" id="letsplay" title="callbetpayout" name="callbetpayout"></a>Ante Bet Payout</h3><p>When you beat the dealer’s hand, the ante bet pays out at even money (1:1).</p><h3 class="style3"><a class="bookmark" id="letsplay" title="showdownbonus" name="showdownbonus"></a>Bonus Bet Pay Table</h3><p>The bonus bet pays out at the following odds:</p><table class="rules" cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr class="CellHeading"><td width="278">Hand</td><td width="91">Payout</td></tr><tr><td>Five of a Kind, suited</td><td><div align="left">1000:1</div></td></tr><tr><td>Royal Flush</td><td><div align="left">200:1</div></td></tr><tr><td>Straight Flush</td><td><div align="left">75:1</div></td></tr><tr><td>Five of a Kind</td><td><div align="left"><p>40:1</p></div></td></tr><tr><td>Four of a Kind</td><td><div align="left"><p>7:1</p></div></td></tr><tr><td>Full House</td><td>3:1</td></tr><tr><td>Flush</td><td>2:1</td></tr><tr><td>Straight</td><td>2:1</td></tr><tr><td>Three of a Kind</td><td>Push</td></tr></tbody></table><p>The above is a default pay table. The final pay table, determined by the options selected by the casino operator, is displayed on the game table.</p>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Casino Hold’em</h1><p>Casino Hold’em is a variation of the popular poker game Texas Hold’em. Both games are easy to learn and offer the rewards of a side bet. Casino Hold’em differs from Texas Hold’em in that you play against the house rather than against other players.</p><p>The object of the game is to beat the dealer’s hand by getting the best possible five-card hand out of the two cards you are dealt and five community cards. If you beat the dealer, you win! To add to the excitement, you can also place a side bet, which pays out if a pair of aces or higher is dealt in the first five cards.</p><h1 class="style3"><br /><br />Let’s Play!</h1></div><p>To help you with the game, Casino Hold’em displays messages when you have the option of side bets or when you are about to fold a paying hand. You can use the Options menu to turn these messages on or off.</p><p> </p><p>To play Casino Hold’em:</p><ol><li>Select a chip value.</li><li>Click the Ante betting circle until the number of chips displayed equals the amount you want to bet.</li><li>If you want to place a side bet, click the AA+ betting circle. To bet a different amount from the ante, select another chip value and then click the AA+ betting circle.</li><li>Optionally, click Clear to remove all your chips from the table.</li><li>Click Deal.</li></ol><p>You must place the ante bet before dealing can begin.</p><blockquote><p>The first card is burned, you are dealt two cards face up, and the dealer is dealt two cards face down. Three flop cards are also dealt face up. The Call and Fold buttons appear.</p></blockquote><ol start="6"><li>Review your cards and do one of the following: 

<ol><li type="a" value="1">Click Call, if you think you have a winning hand. 

<p>A bet, double the amount of your ante, is placed in the Call betting circle. The last two cards are dealt and the dealer’s cards are shown.</p></li></ol><ul><li>If you beat the dealer's hand, a Win message appears displaying the amount of your total winnings with a summary of both hands. To see a breakdown of your winnings, hover the mouse over the Win message.</li><li>If the dealer's hand does not qualify, a Dealer does not qualify message appears and you win the game as described.</li><li>If you do not beat the dealer’s hand, a Lose message appears and your chips are removed from the betting circles.</li><li>If the play pushes, a Push message appears and your bets are returned to you.</li></ul><ol><li style="LIST-STYLE-TYPE: none"><p></p></li><li type="a" value="2">Click Play Again to start a new game.</li></ol><p> </p><p>Or:</p><ol><li type="a" value="1">Click Fold, if you do not think you have a winning hand.</li></ol><blockquote><p>Your chips are removed from the table and you forfeit any AA+ bets you may qualify for. If warning messages are turned on, a text message appears indicating you are folding a paying hand.</p></blockquote><ol><li type="a" value="2">Click Play Again to start a new game.</li></ol></li></ol><blockquote><p></p></blockquote><p>If you placed an AA+ bet:</p><ul><li>Your cards are evaluated after you click Call.</li><li>If you win the bet, your winnings are stacked next to the AA+ betting circle, and the amount of your winnings is displayed.</li><li>If you do not qualify, your chips are removed from the AA+ circle.</li></ul><p>To play again:</p><ol><li>Click Play Again.</li></ol><blockquote><p>Your bets from the last round are placed on the table.</p></blockquote><ol start="2"><li>To bet a different amount from the last round, click Clear, select another chip value, and click the betting circles that you want to bet on.</li><li>Continue with play as described in steps 4 to 6, above.</li></ol>          
          
<div class="headline"><h1><span class="style3">Rules</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Casino Hold’em is a five-card poker game using a single 52-card deck. To win, you must beat the dealer by getting a higher five-card hand. You and the dealer can both use community cards to complete a hand.</p><p>Play begins when you place an initial bet, the ante bet. You may also place an optional AA+ bet. You win this bet if a pair of aces or higher is dealt in the first five cards.</p><p>After you place the ante, two cards are dealt to you face up, two cards are dealt to the dealer face down, and three flop cards are dealt on the table. At this point, you evaluate your hand to decide if you want to continue. If you fold, you forfeit your ante and any AA+ winnings you qualify for. If you call, your call bet is placed at double the ante.</p><p>If you call, the last two community cards are dealt and the dealer’s cards are shown. The dealer must have a pair of 4s or higher to qualify. If the dealer’s hand does not qualify, you win, the ante bet is paid out according to the pay table, and the call bet pushes.</p><p>If the dealer’s hand qualifies, it is compared to your hand. If the dealer's hand is lower, you are paid out according to the ante pay table and the call bet pays 1:1. If the dealer's hand is higher, you lose both your ante and your call bets. Even if you lose against the dealer, you still receive AA+ winnings that you qualify for, paid out according to the AA+ pay table.</p><p>When you and the dealer tie, the next highest card that is not part of the paying hand, known as the kicker, determines the winner.</p><p> </p><ul><li>Dealer has Q/4</li><li>Player has Q/3</li><li>Community cards are 9/9/Q/5/2</li></ul><p>The play pushes because the dealer and player both have Two Pairs (a pair of queens and a pair of 9s) and the next highest card, the 5, is in the community cards, acting as the kicker. Neither the dealer nor the player has a high enough card to beat the kicker.</p><p>If the player had a Q/7, then the 7 card would be the kicker and the player would win.</p><p> </p><h1 class="style3"><a class="bookmark" id="bettypes" title="casinoholdemhands" name="casinoholdemhands"></a>Casino Hold’em Hands</h1></div><p>Paying hands are ranked in descending order, as follows:</p><table class="maincontent" cellspacing="0" cellpadding="0" border="0"><tbody><tr><th width="125">Hand</th><th width="*">Definition</th><th width="175">Example</th></tr><tr><td>Royal Flush</td><td>The highest hand in poker. It consists of the following cards: 10, jack, queen, king, and an ace all of the same suit.</td><td><img height="35" alt="Royal Flush" src="http://gaminghelp.com/common/images/deckofcards/H10.gif" width="30" /> <img height="35" alt="Royal Flush" src="http://gaminghelp.com/common/images/deckofcards/HJ.gif" width="30" /> <img height="35" alt="Royal Flush" src="http://gaminghelp.com/common/images/deckofcards/HQ.gif" width="30" /> <img height="35" alt="Royal Flush" src="http://gaminghelp.com/common/images/deckofcards/HK.gif" width="30" /> <img height="35" alt="Royal Flush" src="http://gaminghelp.com/common/images/deckofcards/HA.gif" width="30" /> </td></tr><tr><td>Straight Flush</td><td>Five cards in sequence, all of the same suit</td><td><img height="35" alt="Straight Flush" src="http://gaminghelp.com/common/images/deckofcards/S3.gif" width="30" /> <img height="35" alt="Straight Flush" src="http://gaminghelp.com/common/images/deckofcards/S4.gif" width="30" /> <img height="35" alt="Straight Flush" src="http://gaminghelp.com/common/images/deckofcards/S5.gif" width="30" /> <img height="35" alt="Straight Flush" src="http://gaminghelp.com/common/images/deckofcards/S6.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/S7.gif" width="30" /></td></tr><tr><td>Four of a Kind</td><td>Four cards of the same denomination, one in each suit</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/D10.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/S10.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H10.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C10.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D4.gif" width="30" /></td></tr><tr><td>Full House</td><td>Three cards of one denomination and two cards of another denomination</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/HJ.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/CJ.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H7.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D7.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C7.gif" width="30" /></td></tr><tr><td>Flush</td><td>Five cards all of the same suit</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/H2.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H6.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H9.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/HQ.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/HK.gif" width="30" /> </td></tr><tr><td>Straight</td><td>Five cards in sequence of any suit</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/H3.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C4.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D5.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C6.gif" width="30" /> <img src="http://gaminghelp.com/common/images/deckofcards/S7.gif" /></td></tr><tr><td>Three of a Kind</td><td>Three cards of the same denomination and two unmatched cards</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/C9.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D9.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H9.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C6.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H2.gif" width="30" /></td></tr><tr><td>Two Pairs</td><td>Two sets of two cards of the same denomination and any fifth card</td><td><img src="http://gaminghelp.com/common/images/deckofcards/D4.gif" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/S4.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/CJ.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/HJ.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/H9.gif" width="30" /></td></tr><tr><td>One Pair</td><td>Any hand of two cards of the same denomination and one unmatched card</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/C6.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D10.gif" width="30" /> <img src="http://gaminghelp.com/common/images/deckofcards/C3.gif" /> <img src="http://gaminghelp.com/common/images/deckofcards/DQ.gif" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C10.gif" width="30" /></td></tr><tr><td>High card</td><td>Any hand of three cards of different rank and a variety of suits</td><td><img height="35" src="http://gaminghelp.com/common/images/deckofcards/C6.gif" width="30" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/D10.gif" width="30" /> <img src="http://gaminghelp.com/common/images/deckofcards/C3.gif" /> <img src="http://gaminghelp.com/common/images/deckofcards/DQ.gif" /> <img height="35" src="http://gaminghelp.com/common/images/deckofcards/C10.gif" width="30" /></td></tr></tbody></table><p> </p>          
          
<div class="headline"><h1><a class="bookmark" id="letsplay" title="youwin" name="youwin"></a><span class="style3">You Win!</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>You win if:</p><ul><li>the dealer’s hand does not qualify (dealer does not get a pair of 4s or higher)</li><li>your hand ranks higher than the dealer’s hand</li></ul><p>You win the AA+ bet if a pair of aces or higher is dealt in the first five cards.</p><h3><a class="bookmark" id="letsplay" title="antebetpayout" name="antebetpayout"></a><span class="style3"><br /><br />Ante Bet Pay Table</span></h3><p>The ante bet pays out at the following odds:</p><table class="maincontent" cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><th width="278">Hand</th><th width="91">Payout</th></tr><tr><td>Royal Flush</td><td><div align="left">100:1</div></td></tr><tr><td>Straight Flush</td><td><div align="left">20:1</div></td></tr><tr><td>Four of a Kind</td><td><div align="left"><p>10:1</p></div></td></tr><tr><td>Full House</td><td>3:1</td></tr><tr><td>Flush</td><td>2:1</td></tr><tr><td>Straight or lower</td><td>1:1</td></tr></tbody></table><h3 class="style1 style2"><a class="bookmark" id="letsplay" title="callbetpayout" name="callbetpayout"></a><br /><br />Call Bet Payout</h3><p>When you beat the dealer’s hand, the call bet pays out at even money (1:1).When the dealer’s hand does not qualify, the call bet pushes.</p><h3 class="style1"><a class="bookmark" id="letsplay" title="aaplus" name="aaplus"></a><span class="style2"><br /><br />AA + Bet Pay Table</span> </h3><p>The AA+ bet pays out at the following odds:</p><table class="maincontent" cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr><th width="278">Hand</th><th width="91">Payout</th></tr><tr><td>Royal Flush</td><td><div align="left">100:1</div></td></tr><tr><td>Straight Flush</td><td><div align="left">50:1</div></td></tr><tr><td>Four of a Kind</td><td><div align="left"><p>40:1</p></div></td></tr><tr><td>Full House</td><td>30:1</td></tr><tr><td>Flush</td><td>20:1</td></tr><tr><td>Straight</td><td>7:1</td></tr><tr><td>Three of a Kind</td><td>7:1</td></tr><tr><td>Two Pairs</td><td>7:1</td></tr><tr><td>Pair of Aces</td><td>7:1</td></tr><tr><td>Other</td><td>Nothing</td></tr></tbody></table>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Baccarat</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Baccarat is a sophisticated game which originated in Europe. Players can wager on the player, the banker, or a tie, while trying to obtain a total card value closest to nine. Two hands of two cards are dealt, one representing the player's hand and one representing the banker's hand. Depending on the value of the hands, a third card may be drawn for each hand. The winning hand is the one with a card total closest to nine.</p><p><b>Rules</b> </p><p>Baccarat uses a four deck shoe.</p><p>No hand receives more than three cards.</p><p>Tens and face cards are worth zero, aces are worth one and all other cards are worth their face value. If the value of a hand exceeds nine, then the value is adjusted by subtracting 10 from the total. No hand receives more than three cards.</p><p><b>Third Card Rule</b> </p><p>If either the player or the banker has a total of an 8 or a 9 on the first two cards they both stand. This rule overrides all other rules.</p><p>If the player's total is 5 or less then the player hits, otherwise the player stands.</p><p>When the player and banker hands are equal then the hand is declared a tie.</p><p>After the first two cards are dealt to both the player and banker, a third card may be dealt, subject to the Third Card Rule. The player's hand is always played first followed by the banker's hand. If the player stands, then the banker hits on a total of 5 or less. If the player does hit then use the chart below to determine if the banker hits (H) or stands (S).</p><p><img height="203" alt="baccarat table" src="/rules/RULES/rules_bacc_files/bacc_table-1.gif" width="323" /> </p><p>In baccarat, if you bet on:</p><p>- Player and win, you get paid 1:1</p><p>- Banker and win, you get paid 1:1 (less 5% house commission)</p><p>- Tie and win, you get paid 8:1 (if the tie wager mentioned above is part of a combination bet; for example, a tie wager plus a player hand wager, and the outcome is a tie, then the player hand wager becomes a push and the payout results from the tie wager alone).</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Free Ride</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Free Ride is based on stud poker. You are dealt three cards and given two opportunities to raise your bet and receive two additional cards. If your five-card hand ranks high enough you win - it's as easy as that! Free Ride also offers a separate progressive jackpot bet for $1. If you play the progressive and your hand qualifies, you win big!</p><p><b>Rules</b> </p><p>Free Ride uses a one deck shoe which is reshuffled after each hand.</p><p>After you place your bet, three cards are dealt to you. Then you are dealt two more cards one by one to make it five. Each time prior to receiving the fourth and fifth cards you can either raise your bet on an amount equal to your initial bet or pass. Thus you can triple the win amount on favorable occasions! The smallest winning combination is pair of tens. For a complete payout list please refer to the table below.</p><p><b>Note:</b> Maximum payout on our Free Ride table is $50,000.00.</p><p><b>Free Ride Poker Hands</b> <br /></p><div align="center"><img height="430" src="/rules/RULES/rules_fr_files/fr_table1.gif" width="516" /> </div><p><b>Free Ride Poker Hands</b> <br /><b>Note:</b> Maximum payout on our Free Ride table is <b>$50,000.00.</b> <br /><b>Example:</b> Casino A has a $20,000 maximum payout limit on Free Ride and a progressive jackpot of $5,000. A player bets $60 on Free Ride, and makes the $1 progressive bet.</p><p>He/she obtains a Royal Flush and expects to win $60,000 but actually only wins $20,000, regardless of the odds given below, because $20,000 is the maximum payout amount designated for that table.</p><p>This player also played the progressive so, according to the progressive jackpot payout schedule, his/her winnings would include 100% of the progressive jackpot giving her a grand total of $25,000 in winnings.</p><div align="center"><img height="222" src="/rules/RULES/rules_fr_files/fr_table2.gif" width="517" /> </div><p><b>Progressive Jackpot</b> <br />
You can place a $1 bonus bet if you wish to participate in a progressive jackpot. By placing an additional $1 wager, you have the chance to win a portion or all of the jackpot based on the strength of your hand! Below is the payout schedule for this optional $1 wager.</p><div align="center"><img height="127" src="/rules/RULES/rules_fr_files/fr_table3.gif" width="508" /> </div><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Poker Side Bet</h1><p>Video Poker SideBet is a game designed on the original casino game. The idea is to get the best poker hand from five cards within two turns of the cards. Five cards are dealt face up, from here you have the choice to hold any of those five cards, the cards which have not been held are placed back in the pack and new cards dealt in their place. Players are trying to get the best poker hand possible within two turns of the cards.</p><span class="style1"><b>Stage 1:</b></span><p>At the beginning of the game, players may enter the desired stake, all bets placed at this stage are placed at the odds of each hand being dealt within two turns of the cards: if you place a £1 bet before the first cards are turned and your hand mirrors that of a winning hand after two turns of the cards, you will be paid the winnings to your stake at the odds displayed.</p><span class="style1"><b>Stage 2:</b></span><p>Once the cards have been turned once, you will be able to hold any card/cards or your choice, each time a card is selected of deselected, the possible winning hands and their odds will be displayed, you may now place a bet on your chosen hand at the fixed odds shown, the start button may now be clicked and the cards not held will be stacked and new cards dealt.</p><span class="style1"><b>Stage 3:</b></span><p>Once the cards have been turned twice, you will be paid accordingly to bets placed on before and after the first turn of the cards.</p>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Jokers Wild Poker</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Jokers' Wild video poker is extremely popular because it moves fast and is easy to play, while retaining an element of strategy. The object of Jokers Wild video poker is to attain the highest ranked poker hand; using a joker as a wildcard (a joker can represent any card in the deck). The payout for each poker combination is displayed on the video poker machine face.</p><p><b>Rules</b> </p><p>Jokers' Wild video poker uses a one deck shoe.</p><p>The deck includes one joker and is re-shuffled after every hand.</p><p>Five cards are dealt to you after you place the bet. Once the cards are dealt, you may choose to keep any of the cards by either clicking Hold, found directly below the card, or on the card. Once you have selected which cards to hold, click Deal and the remaining cards will be exchanged for new cards.</p><p>After this second deal, you are paid out based on the rank of the resulting poker hand.</p><p><b>Jokers Wild</b> <br /></p><div align="center"><img height="517" src="/rules/RULES/rules_jw_files/jw_table1.gif" width="521" /> </div><p><b>You Win!</b> <br />
A winning hand pays out according to the number of coins wagered and their value.<br /><b>Example:</b> If you select the $1 coin and then Bet Max your wager will be five coins of $1 or $5. If you get a Straight Flush then according to the payout schedule below you will win 250 coins with the same value as your original bet or $250 (250 x $1).<!--<DIV ALIGN=CENTER>
<IMG SRC="images/jw_table2.gif">
</DIV>--> </p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Rock 'n Slots</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The slot machine or a one-armed bandit, as it has become known, was invented in the early 1800's by Charles Fey. The object of these mechanical games of chance is to match the winning combination of symbols displayed in the payout schedule on the front of the slot machine.</p><p><b>Rules</b> </p><p>Before spinning the reels, you must place a bet.</p><p>1. Select a coin value of 5 ¢, 25¢, $1, or $5.<br />
2. Click the coin slot, Bet One or Bet Max.<br />
· Click the coin slot, up to three times, until the number of coins displayed equals the amount you wish to bet.<br />
· Click Bet One, up to three times, until the number of coins displayed equals the amount you wish to bet.<br />
· Click Bet Max once to bet the maximum amount allowed. The reels will automatically spin after choosing this option.<br />
3. To reduce your bet amount, right-click on either the coin slot or Bet One to remove coins. Your bet will be reduced by the amount of the currently selected coin each time you right-click. You can also click Cash Out to remove all coins prior to spinning the reels.<br />
4. Click Spin or the slot machine lever to spin the reels.</p><p><b>Payouts</b> </p><p>If the symbols displayed, once the machine comes to rest, match any of the combinations in the payout schedule, you win. You don't need to hit Cash Out to retrieve your winnings. Every time you win, your casino account balance is automatically updated. Your winnings appear as credits.</p><p><b>Example:</b> If you're playing the $5 machine and get a combination that pays out 4 credits, you win 4 x $5 = $20.</p><p><b>Progressive Jackpot</b> </p><p><b>Note:</b> Progressive jackpots may not be available on all slot machines.</p><p>The progressive jackpot can be won if you meet the following criteria:<br />
1. The symbols displayed, once the machine comes to rest, match the highest winning combination of symbols found in the payout schedule and,<br />
2. The maximum number of coins is used to place the bet.</p><p>If these two conditions are met then you win the credits shown plus the progressive jackpot. Progressive jackpots are shown in dollars and the amount won is adjusted according to the denomination played. Once a progressive jackpot is won, it is converted into credits, added to the jackpot credits and displayed in the win column.</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Hook, Line &amp; Sinker</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>The slot machine or a one-armed bandit, as it has become known, was invented in the early 1800's by Charles Fey. The object of these mechanical games of chance is to match the winning combination of symbols displayed in the payout schedule on the front of the slot machine.</p><p><b>Rules</b> </p><p>Before spinning the reels, you must place a bet.</p><p>1. Select a coin value of 5 ¢, 25¢, $1, or $5.<br />
2. Click the coin slot, Bet One or Bet Max.<br />
· Click the coin slot, up to three times, until the number of coins displayed equals the amount you wish to bet.<br />
· Click Bet One, up to three times, until the number of coins displayed equals the amount you wish to bet.<br />
· Click Bet Max once to bet the maximum amount allowed. The reels will automatically spin after choosing this option.<br />
3. To reduce your bet amount, right-click on either the coin slot or Bet One to remove coins. Your bet will be reduced by the amount of the currently selected coin each time you right-click. You can also click Cash Out to remove all coins prior to spinning the reels.<br />
4. Click Spin or the slot machine lever to spin the reels.</p><p><b>Payouts</b> </p><p>If the symbols displayed, once the machine comes to rest, match any of the combinations in the payout schedule, you win. You don't need to hit Cash Out to retrieve your winnings. Every time you win, your casino account balance is automatically updated. Your winnings appear as credits.</p><p><b>Example:</b> If you're playing the $5 machine and get a combination that pays out 4 credits, you win 4 x $5 = $20.</p><p><b>Progressive Jackpot</b> </p><p><b>Note:</b> Progressive jackpots may not be available on all slot machines.</p><p>The progressive jackpot can be won if you meet the following criteria:<br />
1. The symbols displayed, once the machine comes to rest, match the highest winning combination of symbols found in the payout schedule and,<br />
2. The maximum number of coins is used to place the bet.</p><p>If these two conditions are met then you win the credits shown plus the progressive jackpot. Progressive jackpots are shown in dollars and the amount won is adjusted according to the denomination played. Once a progressive jackpot is won, it is converted into credits, added to the jackpot credits and displayed in the win column.</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<p>          
          
<div class="headline"><h1>Super HiLo</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</p>
<p>Very exiting new game which gives you an opportunity to win tons of cash - up to $100,000.00 in one game! You can choose just one of the game selections or a combination. Bet that the next card will be high or low? Will it be red or black? Will it be a Heart, Club, Diamond or a Spade?</p><p>If it is your first time on Super HiLo we recommend you choose the play for fun option which allows you to practice using fun money. This will help you understand how to play and as soon as you are comfortable with play for fun you are ready to play for real.</p><p><b>Rules:</b> </p><p>The game is played with one standard deck of 52 cards. Cards are shuffled after each game. Aces are always low and pairs lose.</p><p>At the beginning you will see nine cards displayed on the right hand side of the screen. Eight of them are face down and the center one is face up. How much you could win depends both on how specific your selections are and the chances of your selection being correct. In each single round all selections you make have to win in order for you to win your bet. The 'you could win' window displays your payout based on the selections you made and it includes your stake bet. Once you have made your selection you need to click the 'submit your bet' button and then you will be directed to 'select a card'. If you have won you have the opportunity to carry on playing using the money you have just won as your stake, or cash out your winnings.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<p>          
          
<div class="headline"><h1>Virtual Derby</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</p><p>"Virtual Derby" is a virtual horse racing betting game. There are six runners and riders to choose from all with different odds of winning. Each and every horse's odds represent a true reflection of that horse's chances in the race. Factors such as the recent form of the horse, the experience of the jockey, the quality of the trainer and the going on the particular day of the race are all factors that contribute towards the compiling of the odds.</p><p>If it is your first time on Virtual Derby we recommend you choose the play for fun option which allows you to practice using fun money. This will help you understand how to play and as soon as you are comfortable with play for fun you are ready to play for real.</p><p><b>Rules:</b> </p><p>Players may place 'Win Only' and 'Exacta' bets at fixed odds on the outcome of virtual races that start 'on demand'. This means that the races are not pre-scheduled; instead they start when you confirm your bets. You will not receive a confirmation message in order for your bet to be valid.</p><p>For 'Win Only' bets, players can choose to bet on as many horses as they like per race: from 1 to 6 runners. If your horse passes the post first then you'll win the odds for that horse.</p><p>For 'Exacta', players can choose to bet on any combination of two horses. If the horses you bet to finish 1st and 2nd pass the post in that order then you will win the odds for that bet.</p><p>If you click on ? sign, the screen will become dim and if you mouse over different areas you will see explanatory notes attached; click on the ? sign again to return to the betting screen. You can find information on each horse by clicking on the 'Info' arrow next to the horse name. 'Bet slip' shows all the bets you made so far for the upcoming race and 'History' page shows your results for the past races.</p>
					


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Wheel of Fortune</h1><p class="big style1"><strong>(Sorry, Vanna White not included)</strong></p><b class="style1">Stage 1:</b><p>Choose your chip value.</p><b class="style1">Stage 2:</b><p>Next, choose your bet type, each time you scroll over a betting area, the odds are shown in the message box. Once you have chosen your bet, click your mouse on that area, you have now placed a bet. Click again on the chip to increase your stake, to remove your last bet click undo or to remove all chips click clear. Once you have placed your bet, displayed under your Total Stake box will be the total of all bets placed within the game, you may of course bet as many times as you wish.<br /></p><b class="style1">Stage 3:</b><p>Once you are satisfied with your placed bets you may click your mouse on "Spin Wheel", the wheel will then spin. The amount of time the wheel will spin for is random. If your bet is a successful one, you will be shown the amount you have won and will be able to carry on with the next game. If you would like to place the same bets on the next spin of the wheel, you may click "Redo Last Bet" and all of your previous bets will be placed on the wheel.<br /><br />
At the completion of the game, you will be paid out on each winning bet placed on each of the three betting rounds.</p>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<h1 class="big style1">Poker Dice</h1><p>Poker dice is a game played with 5 dice, each dice has a card value on each of its six sides. The value of the card starts at nine and continues up to an ace.<br /><br />
The aim of the game is to build the best possible poker hand using the five dice. You will have up to three throws of the dice to reach the best hand possible. After each throw, you may place aside (hold) any of the dice you wish to use in your hand, rolling the remaining dice. You may stop after the first or second throw, whatever the dice show after the third throw; all five dice will be deemed to be the final hand.<br /><br />
The winning hand order is as follows:<br /><br />
1. Five of a Kind<br />
2. Royal Straight<br />
3. Four of a kind<br />
4. Full house.<br />
5. Straight.<br />
6. Three of a kind.<br />
7. Two pairs.<br />
8. One pair.<br /></p><span class="style1"><b>Stage 1:</b></span><p>At the outset of the game, you will be shown the odds available for throwing each winning hand with the throw of the dice three times, you may place a bet at these odds to the stake of your choice, you will then throw the dice.</p><span class="style1"><b>Stage 2:</b></span><p>After the first throw of the dice you will be offered the opportunity to hold any of the five dice of your choice; each time you hold any of the dice, the possible winning bets left available will be shown, with the odds for each bet alongside. You may now place a bet on any of the available bets.<br /></p><span class="style1"><b>Stage 3:</b></span><p>After holding the dice of your choice and placing a bet, you may now roll the dice again. After the remaining dice have been rolled, you will again be offered bets and odds for all possible combinations, the bet combinations and odds will adjust each time a dice is selected or deselected.<br /><br />
At the completion of the game, you will be paid out on each winning bet placed on each of the three betting rounds.</p>
					</div>


<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Red Dog</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Red Dog, also known as Acey-Deucey or Between the Sheets, is easy to play. Two cards are drawn. You can wager on whether the third card drawn will rank between the first two cards. The tighter the spread between the first two cards the higher the payout if the third card drawn ranks between the first two cards.</p><p><b>Rules</b> </p><p>Red Dog uses a one deck shoe which is reshuffled after each hand. After your initial bet is placed, two cards are dealt face up to either side of the table. There are three possible outcomes: pair, consecutive card or non-consecutive card. You can double your wager by pressing 'Raise' button if you believe the next card dealt will rank between the two face-up cards.</p><p>The tables below illustrate when a third card would be dealt and payouts.</p><p></p><div align="center"><img height="566" src="/rules/RULES/rules_rd_files/rd_table1.gif" width="518" /> </div><p><b>You Win!</b> </p><div align="center"><img height="318" src="/rules/RULES/rules_rd_files/rd_table2.gif" width="514" /> </div><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->



<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Sic Bo</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Sic Bo is an ancient Chinese game that is easy to learn and fun to play. Sic Bo literally means: dice pairs. It is similar to a popular English game known as Grand Hazard, and the Philippine game, Hi-Lo.</p><p><b>Rules</b> </p><p>Players bet on combinations of the numbers appearing on three dice. Bets are placed on one or more of the fifty possible wagers.</p><p><b>Bet Types</b> <br /></p><div align="center"><img height="334" src="/rules/RULES/rules_sb_files/sb_table1.gif" width="514" /> </div><p><b>You Win!</b> <br /></p><div align="center"><img height="674" src="/rules/RULES/rules_sb_files/sb_table2.gif" width="516" /> </div><p><b>Addendum:</b> Three Dice Totals that roll 4 or 17 pay 60:1</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->



<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Pai Gow Poker </h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Pai Gow poker is a westernized version of the Chinese dominoes game Pai Gow. Players must arrange seven cards into two poker hands - one five-card and one two-card hand, so that each hand beats the dealer's corresponding hand.</p><p><b>Rules</b> </p><p>Pai Gow poker uses a one deck shoe which is reshuffled after each hand.</p><p>The deck includes one joker. Joker is used to complete straights, flushes and straight flushes only; otherwise it is used as an ace.</p><p>Each seven-card hand must be split into two hands, a five-card hand (high hand) and a two-card hand (low hand). The five-card hand must always rank higher than the two-card hand and it is calculated according to a standard stud poker hand hierarchy. You must win both hands to receive a payout. If both your hands are winners then you win and on the contrary. Payout is 1:1 less 5% house commission.</p><p>Any combination of win and loss across both hands results in a push/tie. In this case your bet is returned to you. All ties are in dealer's favor. For example, if one of your hands loses to the dealer and the other ties - you lose; if one hands wins and other pushes, you push.</p><p><b>Pai Gow Poker Hands</b> <br /></p><div align="center"><img height="514" src="/rules/RULES/rules_pg_files/pg_table1.gif" width="510" /> </div><p><b>Please note:</b> In Pai Gow Poker, the A-2-3-4-5 straight is considered the second highest straight after the 10-J-Q-K-A straight.</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->



<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						          
          
<div class="headline"><h1>Battle Royale</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Battle Royale is a simple game based on the children's game War. Once the ante has been placed, you and the dealer each receive one face-up card. The high card wins and if there is a tie, you have an opportunity to surrender or battle for the chance to win at 3:1 odds.</p><p><b>Rules</b> </p><p>Battle Royale uses a four deck shoe which is reshuffled after each hand.</p><p>Suits do not matter in Battle Royale. Aces are always high.</p><p>If your card is higher than the dealer's card, you win the round and are paid an amount equal to your initial bet.</p><p>If your card is lower than the dealer's card, you lose.</p><p>If in the event of a tie the player shall have two choices:</p><p>1) Surrender and forfeit half of the bet.</p><p>2) Go to battle. If you elect to battle you must raise your bet by an amount equal to your original wager. The dealer will then burn three cards and give you and him another card each. The outcome of the battle round is:</p><p>a) If your card is higher than the dealer's card, you win the round and are paid an amount equal to your battle bet only.<br />
b) If your card is lower than the dealer's card, you lose initial bet plus battle bet.<br />
c) You win 3:1 on your battle bet only.</p><p><b>Important Note:</b> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
					

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->



<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


<h1 class="content_header"><b class="big">Blackjack Terms</b></h1>
<br />
<table class="rules" cellspacing="0" cellpadding="0" border="1"><tbody><tr class="CellHeading"><th width="19%">Term</th><th width="81%">Definition</th></tr><tr><td>Blackjack</td><td>A two-card hand of 21 points.</td></tr><tr class="odd"><td><a class="bookmark" id="bust" title="bust" name="bust"></a>Bust</td><td>A hand in Blackjack that goes over 21. If you bust, you lose.</td></tr><tr><td><a class="bookmark" id="double" title="double" name="double"></a>Double</td><td>Allows you to double your initial bet. The bet is placed after the first two cards are dealt. Your bet will be placed automatically and you will be dealt another card. This is also known as doubling down.</td></tr><tr class="odd"><td>Double down rescue</td><td>Allows you to surrender half of your total bet.</td></tr><tr><td>Face-up card</td><td>The card in the dealer<span class="Step">’</span>s hand that is face up for you to see before you play your hand. This is also known as the open card or up card.</td></tr><tr class="odd"><td><a class="bookmark" id="softhand2" title="hardhand" name="hardhand"></a>Hard hand</td><td>Any hand that does not contain an ace or a hand that contains one or more aces that count as only one point.</td></tr><tr><td><a class="bookmark" id="hit" title="hit" name="hit"></a>Hit</td><td>To ask the dealer for another card.</td></tr><tr class="odd"><td><a class="bookmark" id="insurance" title="insurance" name="insurance"></a>Insurance</td><td><p>If a dealer is dealt an ace face up, you may buy insurance. You are buying insurance against the dealer<span class="Step">’</span>s odds of having Blackjack. Insurance is a side bet that equals half your original wager.<br /><br />
If the dealer is dealt an ace face up and has Blackjack, it is an automatic loss and the round ends. If you placed an insurance bet, you are paid back at odds of 2:1. This payment equals your original bet.<br /><br />
If you have placed an insurance bet and the dealer does not have Blackjack, you lose your insurance bet, but retain your original wager and continue to play.</p></td></tr><tr><td>Natural</td><td>A two-card hand of 21 points. This is known as Blackjack.</td></tr><tr class="odd"><td><a class="bookmark" id="push" title="push" name="push"></a>Push</td><td>A tie hand between you and the dealer. A push occurs when you and the dealer have un busted hands with the same total points.</td></tr><tr><td>Re-double</td><td>Allows you to double after you have already doubled your original bet.</td></tr><tr class="odd"><td><a class="bookmark" id="softhand" title="softhand" name="softhand"></a>Soft hand</td><td>Any hand that contains an ace that can be counted as 11. For example, an ace and a 6 is known as a soft 17.</td></tr><tr><td><a class="bookmark" id="split" title="split" name="split"></a>Split</td><td><p>If you are dealt two cards of identical value, you can split them into two new hands of one card each. Each new hand can then be played out in turn.<br /><br />
You may only split a hand once. The wager placed on each new hand is equal to the original bet. You may double down on one or both of your new hands. When aces are split, only one card is dealt to each new hand. A split hand resulting in blackjack pays out at 1:1 rather than the normal 3:2.</p></td></tr><tr class="odd"><td><a class="bookmark" id="stand" title="stand" name="stand"></a>Stand</td><td>To be satisfied with your hand and refuse another card.</td></tr></tbody></table>
					</div>

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== NEW TAB  =============================================== -->
<div>
<div class="basic-tabs-content"><!-- Pane -->
<!-- ==ENTER CONTENTS  FROM HERE ================== -->


						<p>The Options toolbar gives you access to Help, Cashier, and game settings.<br /><br /><img height="29" src="/images/menu_option.gif" width="240" /></p><p><strong>Options toolbar</strong></p>
<p>Click the following icons to access variousoptions:</p>
<br />
<table class="data" cellspacing="0" cellpadding="0" width="500" border="0"><tbody><tr class="CellHeading"><th width="9%">Icon</th><th width="91%">Description</th></tr><tr>
<td valign="middle" align="center"><img height="29" src="/images/help_icon.gif" width="30" /></td>

<td>Opens Casino’s Help</td></tr>
<tr>
<td valign="middle" align="center"><img height="29" src="/images/cashier_icon.gif" width="30" /></td>
<td class="odd">Opens Casino’s Cashier</td>
</tr>
<tr>
<td valign="middle" align="center"><img height="29" src="/images/options_icon.gif" width="30" /></td>
<td>Turns the game’s background music on or off</td>
</tr>
<tr>
<td class="odd" valign="middle" align="center"><img height="29" src="/images/muisc_icon.gif" width="30" /></td><td>Turns the game’s background music on or off</td>
</tr>
<tr>
<td valign="middle" align="center"><img height="29" src="/images/tabletop_icon.gif" width="30" /></td><td>Changes the game table’s color scheme and dealer hands.</td></tr></tbody></table>
					

<!-- ==ENTER CONTENTS  FROM HERE =================== -->
</div><!-- End Pane -->		
</div>
<!-- ======== END TAB ================================================ -->


<!-- ======== END ALL TABS ==================================================== -->

<!-- ======== DO NOT EDIT BELOW HERE ============================================== -->
</div><!-- End Panes -->	

{literal}<script type="text/javascript">
<!--//--><![CDATA[//><!--
    $(function() {
      // setup ul.tabs to work as tabs for each div directly under div.panes
      $("#basic-tabs ul.tabs").tabs("div.panes > div");
    });
//--><!]]>
</script>{/literal}        
        

  </div>
              </div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    