{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Support</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/support/gamingknowledgebase" title="">GAMING KNOWLEDGEBASE</a></li>
<li class="expanded"><a href="/house-rules" title="">RULES</a><ul class="menu"><li class="leaf first"><a href="/house-rules" title="">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
<li><a href="/rules/casinogamerules" title="">CASINO RULES</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
</ul></li>
<li><a href="/horseracing-affiliate-program" title="">WEBMASTERS</a></li>
<li><a href="/responsibilities" title="">RESPONSIBLE GAMING</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Play Blackjack</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1>Blackjack</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Blackjack originated in France and is one the most popular casino games in the world. The main objective is to get as close to 21 without going over 21 while still beating the dealer's hand. In our game you can play up to three hands at once. The name Blackjack arose when an American casino decided to popularize the game by paying a bonus to any player holding the ace of spades and either the jack of clubs or jack of spades.</p>
<p><strong>Rules</strong></p>
<p>Our blackjack uses 6 standard 52 card decks in each shoe which is reshuffled after each hand.</p>
<p>In the beginning you are dealt two cards on each hand you placed your bets on (up to three). The dealer is dealt two cards also, one "face" up and other closed, known as a "hole" card. There are numerous things you can perform to increase your chances of winning, please refer to the 'Gimmicks' section below.</p>
<p>All cards numbered 2 through 10 are at their face value. All face cards are valued at 10 while an ace is valued at either 1 or 11 at the player's discretion. Hands containing an ace valued at 11 are considered to be "soft" hands (i.e. A, 6 is a soft 17) while hands containing an ace valued at 1 are considered "hard" hands (i.e. A, 6, Q is a hard 17).</p>
<p>A blackjack is an original hand of two cards: ace and card valued at 10 (10, J, Q, K). A two-card blackjack always beats three or more card hands which equal 21. If you split two tens or two aces and get two cards 21's, it is considered to be a simple 21 and not a blackjack and it is paid 1:1. Dealer's blackjack versus player's blackjack is a tie. The dealer must stand on any 17 and above and draw on a 16 and below. If dealer's card is a 10 or any other card valued at 10 (J, Q, K), the dealer will check for an ace in his hole card and if it is an ace the hand will be instantly over and players will not have an option to take any cards.</p>
<p><strong>Gimmicks</strong></p>
<p><strong>Hit</strong> - This deals the player another card. If your 2 cards were to equal 10 you have nothing to lose by requesting another card since your total could not exceed 21.</p>
<p><strong>Stand</strong> - This means that the player is satisfied with his hand and does not want to risk another hit.</p>
<p><strong>Double Down</strong> - Upon dealing of your original two cards, you have the option to double your initial wager. Once you have chosen to do this you will be dealt a third card and no more. Your hand will only consist of the 3 cards.</p>
<p><strong>Splitting</strong> - If you are dealt two cards with identical value you can then split them (i.e. 8,8 or 10,Q). You would now be playing 2 separate hands, each starting with one of the two split cards. You will now be dealt a second card to one of the hands. After this hand is played out the second hand will be dealt a second card and continued until that hand is played out. All regular rules apply to each hand with the ability to double down. You may only split your hand once and a new wager equal to the initial wager will be placed for each split hand. When splitting aces, the player will only receive one card on each hand.</p>
<p><strong>Insurance</strong> - When the dealer's exposed card is an ace, the player will be given the option to place an insurance wager. This wager will equal one half of the original bet and will be placed on the insurance line. If the dealer has blackjack the player will get paid 2 to 1 on the insurance bet with the original bet being lost. This evens out and acts similar to a push. If the dealer does not have blackjack the insurance bet is lost immediately.</p>
<p><strong>Payouts</strong></p>
<p>If your hand exceeds 21, you bust and automatically lose the hand. If the player busts, then the dealer wins the hand before turning over a card. Player wins the wager if his/her total hand is higher than the dealer's without going over 21. All hands that total less than the dealer's hand lose. Hands that are equal in value will be considered a tie, or a push. If the player gets a blackjack the payout is 3:2, all other winning combinations are paid 1:1.</p>
<p><strong>Tips</strong></p>
<div><img src="/rules/RULES/rules_bj_files/bj_table.gif" alt="" width="520" height="340" /></div>
<p><strong>Important Note:</strong> Your winnings do not accumulate as credits within our Table Games, Slots and Video Games. Each time you win, your winnings are deposited directly into your account balance, so you do not have to hit Cash Out when you want to leave the game - your winnings have already been placed in your account.</p>
<p>For general casino disputes we provide a record that contains the bet amount, running balance, game type, Round ID and time for each single casino round only. Every casino round has a unique Round ID and customers should provide it in their casino claim. Due to technology constraints we can only provide the customer with the card/number/dice details and outcomes for a limited number of casino rounds only. If a unique Round ID or exact time for a particular casino round is not provided by the customer we cannot guarantee the funds will be reimbursed.</p>

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    