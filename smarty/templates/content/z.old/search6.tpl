<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en" dir="ltr">
{if $header ne ''}
{include file=$header}
{/if}
<body class="{$bodyclass}" onLoad="showAuthContent();">
{$ahr_mess}
{include file=$topcontent}
{include file='content/search.tpl'}
{if $footer ne ''}
{include file=$footer}
{/if}
</body>
</html>