{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Special Races</h2>
  



    <ul class="menu"><li><a href="/royalascot" title="" class="active">ASCOT</a></li>
<li><a href="/breederscup" title="">BREEDERS&#039; CUP</a></li>
<li><a href="/dubaiworldcup" title="">DUBAI WORLD CUP</a></li>
<li><a href="/hongkongcup" title="">HONG KONG CUP</a></li>
<li><a href="/kentuckyoaks" title="">KENTUCKY OAKS</a></li>
<li><a href="/melbournecup" title="">MELBOURNE CUP</a></li>
<li><a href="/prixdelarcdetriomphe" title="">PRIX DE L&#039;ARC DE TRIOMPHE</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/specialraces/ascot.jpg" alt="Ascot Bets" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Ascot - Champagne, Strawberries,Cream and Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><a title="Join Now" href="http://usracing.com/join"><img style="float: right;" src="/media/images/content/allhorseracingcom/Pages/Special_Races/join-star.gif" alt="Join US Racing" width="180" height="175" /></a><strong>Where is the 2010 Royal Ascot?</strong>&nbsp;&nbsp;Ascot Racecourse, <br /><strong>When is the 2010 Royal Ascot Race?</strong> June 15 - 19, 2010</p>
<p>&nbsp;</p>
<p>What would the English Summer be without Royal Ascot?</p>
<p>Royal Ascot is one of the most prestigious race meetings with nearly 300 years of tradition and is one of the highlights of Britain's social and sporting calendar. Smart dress, fine food, champagne and excellent racing are all combined in one social event which lasts 4 days in June.</p>
<p>In 1711 Queen Anne, whilst riding in the forests around Windsor Castle, discovered some land near a village called East Cote, now named Ascot, which seemed to her ideal for racing horses. The area was acquired for just £558 to become the Royal Racecourse with the first horse race taking place that year in the presence of the Queen and her Court.</p>
<p>After the death of Queen Anne, racing declined in the reign of King George I who disregarded all sports, but in 1920 racing began again at the Royal Racecourse following a format of procession and races that has hardly changed since.</p>
<p>&nbsp;</p>
          
          
<div class="headline"><h1>More Ascot Racing History and Information</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Tradition is still continued today with the Queen, now Queen Elizabeth II, leaving Windsor Castle every afternoon in an open horse-drawn carriage, arriving at Royal Ascot through the Golden Gates and leading the Royal Procession along the entire length of the racecourse. Only when the royal party are seated can racing begin.</p>
<p>The race meeting starts with the Queen Anne Stakes and commemorates the foundation of the course.</p>
<p>Royal Ascot is a social event as much as a race meeting with a tradition of smart dress and fashion. On Gold Cup Day, or Ladies Day as it is also known the ladies attending often wear spectacular hats and Ascot is famous for the hats and fashion on show by race goers. Equally famous is the Gold Cup race which is one of the longest flat races being run over 2-and-half miles.</p>
<p>Ascot Racecourse is divided into three main enclosures for the public: The Member's Enclosure, The Grandstand which includes access to the paddock and the Silver Ring - only the privileged have access to the Royal Enclosure.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Ascot Racecourse, founded in 1711 is the world’s most famous racecourse.</strong></p>
<p>It was Queen Anne who first saw the potential of a racecourse for Ascot, which in those days was known at East Cote. Whilst out riding she came across an area of open heath, not far from Windsor Castle, that looked an ideal place for “horses to gallop at full stretch.”</p>
<p>The first race meeting ever held at Ascot took place on Saturday, August 11, 1711. Her Majesty’s Plate, worth 100 guineas was the inaugural event, open to any horse, mare or gelding over the age of six. Each horse was required to carry a weight of 12st and seven runners took part.</p>
<p>The contest bore little resemblance to racing seen at Ascot today. The seven horses were all English Hunters, quite different to the speedy thoroughbreds that race on the flat now. The race consisted of three separate heats, each four miles long, so the winner would have been a horse with tremendous stamina.</p>
<p>Racing at Ascot became very popular and, in 1813 Parliament passed an Act of Enclosure. This act ensured that Ascot Heath, although property of the crown, would be kept and used as a racecourse for the public in the future. Today the racecourse is managed by the Ascot Authority, created by an Act of Parliament passed in 1913. His Majesty’s Representative became Chairman of the Authority with the Clerk of the Course acting as secretary. Today Ascot retains both these positions, but with the additional appointment of a Chief Executive and departmental directors under him.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>The Royal Enclosure</strong></p>
<p>The first reference of a Royal Stand at Ascot Racecourse dates back to the 1790’s, when a temporary stand was established during the Royal Meeting. Entrance to this exclusive area was strictly by invitation only from King George III himself. However, according to our records it was not officially named the Royal Enclosure until May 1845.</p>
<p>The Royal Enclosure was originally established to provide the Royal Family and their selected guests with privacy, security and exclusivity, allowing them to enjoy their day in comfort and style. This was deemed as essential following an incident that occurred in June 1832 When William IV was hit by a stone thrown by former Sailor, Dennis Collins.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Racing at Ascot today</strong></p>
<p>Up until 1945 the only racing to take place at Ascot was the four day Royal Meeting. Since then the number of fixtures has steadily increased, with the introduction of the steeple chase and hurdle course in 1965, allowing National Hunt fixtures to be held during the winter months.</p>
<p>Today Ascot stages 25 days of racing throughout the year, 16 Flat meetings between the months of May and October and 9 National Hunt meetings between November and April. The Royal Meeting, held in June is undoubtedly the most famous of these, where top class racing is combined with tradition, pageantry and style. Other highlights include Diamond Day, featuring the King George VI and Queen Elizabeth Diamond Stakes, The Blue Square Shergar Cup, racing’s only team competition and the BETDAQ Festival of racing, featuring The Queen Elizabeth II Stakes.</p>
<p>Ascot Racecourse welcomes some 500,000 racegoers through the gates each year and continues to offer unbeatable racing action. Queen Anne would surely be very proud of her sporting legacy to the nation.</p>
<p>More than 300,000 people visit the racecourse during Royal Ascot week, making this Europe's most popular race meeting, and the event still continues to grow each year.</p>
<p>Royal Ascot is an internationally renowned sporting and social occasion, where tradition, pageantry and style all meet in a glorious setting - against the spectacular backdrop of top class thoroughbreds and world famous jockeys competing for some of the highest accolades in horseracing.</p>
<p>To know more about Royal Ascot, visit the official website: <a href="http://www.royalascot.co.uk/" target="_blank">http://www.royalascot.co.uk/</a>.</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    