
      
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Kentucky Horse Racing History</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />
<p><h3><span class="DarkBlue">Kentucky Horse Racing</span> </h3><p>

<p>This second part of the history lesson introduces you to the world of Kentucky horse racing. How ironic that this chapter of horse racing is influenced not by thoroughbreds but by a different type of horse: the Iron Horse.</p>

<p>There are three chapters, <a title="A History of Horse Racing" href="/horseracing-history">A History of Horse Racing</a>, <a title="Horse Racing in Kentucky" href="/kentuckyhorseracing">Horse Racing in Kentucky</a> and <a title="A History of Churchill Downs and the Kentucky Derby" href="/churchilldowns-history">A History of Churchill Downs and the Kentucky Derby</a>. The material is meant to be read straight through, but feel free to roam and jump around as you please. The main reference for the US Racing history section is the fantastic website <a href="http://www.derbypost.com/"><cite>Call to the Derby Post</cite></a> which takes its source from the book "Jockeys, Belles and Bluegrass Kings, The Official Guide to Kentucky Racing" by Lynn S. Renau <cite>(Herr House Press, Louisville, Kentucky: 1995).</cite></p>

<p>          
          
<div class="headline"><h1><span class="DarkBlue">Foundations: Bluegrass and Lexington</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</p>
<br />
<p>After the Revolutionary War more and more immigrants poured into Kentucky and horse racing became more and more of a Kentucky institution. At the 1775 Transylvania Convention Daniel Boone introduced the first bill "to improve the breed of horses in the Kentucky territory." Many Kentucky settlements--with the notable exception of Louisville which already had a race track--featured a Race Street, a straight stretch located just off the main thoroughfare and named after what went on there. In 1797 Kentucky's first Jockey Club was founded at a formal race meet, then was reorganized as the Lexington Jockey Club in 1809. (Kentucky statesman Henry Clay was a founding member.)</p>

<p>Helping Kentucky establish its foothold on horse racing was Virginia's struggle to maintain racing under the burden of religious censure and bad business practices. The Revolution had damaged their stock, which was then replaced by low quality horses from dishonest British horse merchants. Diomed, who never performed up to par after his 1780 Epsom Darby win, was them sent to stud, although his sires floundered in England. He was then considered useless and thereby suitable for trade to America and shipped to Virginia in 1800. For some reason his luck changed in Virginia; each year his crop of sire champions grew, even to the point of joining the line of Aristides, who won the first <a title="Kentucky Derby" href="/kentuckyderby">Kentucky Derby</a> in 1875.</p>

<p>The War of 1812 took a heavy toll on horses. Afterwards, racing was slow to recover in the South and reformers shut it down entirely in the North and East. Lexington, however, always had a track where owners competed their best homebreds; horsemen quickly realized there was no equal to the Bluegrass when it came to nurturing pedigreed stock. Bluegrass, for those who have always wondered, is a deep-rooting, thin bladed hardy perennial (a plant that lives for an indefinite number of years), native to the steppes of the Black Sea. Some credit Quaker leader William Penn with its importation, but the seed probably came to America in the pockets of Mennonites ousted from Russia, for whom Pennsylvania was a safe haven before they headed west. Settlers quickly cleared land, maximizing the pasture available for grazing not just for horses but for hogs, sheep and cattle as well.</p><p>In 1826, 60 prominent Bluegrass businessmen organized the Kentucky Association for the Improvements of Breeds of Stock. Thoroughbred breeding records were too jumbled at that point in time, however, and a centralized breed registry comparable to Weatherby's General Stud Book (see Part I) until Lexington native Col. Sanders D. Bruce compiled the American Stud Book in 1868. Nevertheless, the first Kentucky Association races took place at the mile-log, circular Old William Track in Lee's Wood, before a course was laid out nearer to downtown Lexington; it was later remodeled in 1832, becoming America's second mile-long, fenced, dirt track. By 1850 landlocked Lexington lacked only one thing: a railroad system with direct access to Ohio River trade. The lack of cheap transportation greatly handicapped farmers, whose incomes were linked to their ability to ship their wares around the country. As trade became pivotal to economic survival, Lexington became reliant on the Louisville &amp; Nashville Railroad, Louisville's "iron horse."</p><p>By contrast, Louisville was a swampy, riverfront settlement named for France's King Louis XVI, and had developed from Portland, where the treacherous Falls of the Ohio frequently forced hapless travelers ashore. From the beginning, Louisville was a brawling river town, home to successive waves of German and Irish immigrants making their way up river from New Orleans. They became hardworking citizens but had no desire or money to buy and race thoroughbreds. Townspeople with English roots, however, organized the Louisville Jockey Club and arranged matches down by the river. Another track was built on the east side of the county, and both tracks flourished and gave rise to the notion of the "Louisville races."</p>

<p><a title="NEXT CHAPTER" href="/churchilldowns-history">NEXT CHAPTER</a></p>

<p align="center"><a title="A History of Horse Racing" href="/horseracing-history">A History of Horse Racing</a> </p><p align="center"><a title="Horse Racing in Kentucky" href="/kentuckyhorseracing">Horse Racing in Kentucky</a></p>

<p align="center"><a title="A History of Churchill Downs and the Kentucky Derby" href="/chuchilldowns-history">A History of Churchill Downs and the Kentucky Derby</a></p>
			        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    