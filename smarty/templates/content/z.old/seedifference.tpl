{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Why Us?</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/welcome" title="">WELCOME</a></li>
<li><a href="/learnmore" title="">LEARN MORE</a></li>
<li class="leaf active-trail"><a href="/seedifference" title="" class="active">SEE THE DIFFERENCE</a></li>
<li><a href="/racebookreviews" title="">COMPARE US</a></li>
<li><a href="/readreviews" title="">READ REVIEWS</a></li>
<li><a href="/betwithconfidence" title="">Setting the Pace</a></li>
<li><a href="/bestracebook" title="">ABOUT US Racing</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>US Racing Offers More than TwinSpires, TVG and DRFBets</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table><tbody><tr>
<td><table cellspacing="0" cellpadding="0" width="665" border="0"><tbody><tr>

<td><img title="" alt="" src="/themes/images/whyus/seediff_1.gif" border="0" /></td>

<td><table cellspacing="0" cellpadding="0" width="56" border="0"><tbody><tr>
<td><img title="" height="44" alt="" src="/themes/images/whyus/seediff_2.gif" width="56" border="0" /></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_1.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="29" src="/themes/images/whyus/seediff_3.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_2.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_3.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_4.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_5.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_6.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_7.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_4.gif" width="56" border="0" /></a></td></tr><tr>

<td><a onmouseover="MM_swapImage('swapper','','/themes/images/whyus/see_8.jpg',1)" onmouseout="MM_swapImgRestore()" href="javascript:void(0);"><img height="20" src="/themes/images/whyus/seediff_5.gif" width="56" border="0" /></a></td></tr><tr>

<td><img height="69" src="/themes/images/whyus/seediff_6.gif" width="56" /></td></tr></tbody></table></td><td>

<table cellspacing="0" cellpadding="0" width="430" border="0"><tbody><tr>

<td><img id="swapper" title="" height="213" alt="" src="/themes/images/whyus/seediff_7.gif" width="430" border="0" name="swapper" /></td></tr><tr>

<td><a href="/join"><img height="69" src="/themes/images/whyus/seediff_8.gif" width="430" border="0" /></a></td></tr></tbody></table>
</td></tr></tbody>
</table></td></tr><tr>

<td bgcolor="#105ca9">

<table cellspacing="0" cellpadding="0" border="0">
<tbody><tr>
<td width="38">&nbsp;</td><td><a title="Be online for less" href="/join"><img src="/themes/images/whyus/whyus_betonlineforless.gif" width="176" height="36" border="0" /></a></td>

<td width="30">&nbsp;</td><td><a title="Any Dollar Amount" href="/join"><img src="/themes/images/whyus/whyus_anydollaramount.gif" width="176" height="36" border="0" /></a></td>
    
<td width="30">&nbsp;</td><td><a title="Free Account" href="/join"><img src="/themes/images/whyus/whyus_freeaccount.gif" width="176" height="36" border="0" /></a></td><td width="37" bgcolor="#105ca9">&nbsp;</td>
</tr></tbody>
</table>

</td></tr></tbody>
</table>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    