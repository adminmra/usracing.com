{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Support</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/support/gamingknowledgebase" title="">GAMING KNOWLEDGEBASE</a></li>
<li class="expanded"><a href="/house-rules" title="">RULES</a><ul class="menu"><li class="leaf first"><a href="/house-rules" title="">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
<li><a href="/rules/casinogamerules" title="">CASINO RULES</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
</ul></li>
<li><a href="/horseracing-affiliate-program" title="">WEBMASTERS</a></li>
<li class="leaf last active-trail"><a href="/responsibilities" title="" class="active">RESPONSIBLE GAMING</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/help/customer_support.jpg" alt="Call us at 1-888-BET-WITH-ME" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Responsible Gaming</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Online gaming is supposed to be a fun and recreational pastime but we at US Racing realize that, for some, it can become a problem that adversely affects their lives and the lives of those around them.</p>
<p>While we pride ourselves on offering a service that is accessible to our members wherever they are, unfortunately our service has the potential to be abused. And, like so many other activities, when online gaming becomes compulsive and unmanaged, it becomes a problem.<br />As such we take very seriously our responsibility to provide a gaming service in a socially responsible way.<br />Here is how we promote responsible gaming:</p>
<p><strong>Age Limits:</strong></p>
<p>Online gaming is a form of adult entertainment and it is illegal for anyone under the age of 18 to play on US Racing. All of our members must be 18 or older and we reserve the right to ask for verification documents from any member and may deactivate any account until such proof is received.<br />Anyone under the age of 18 that is found to be gaming on <strong>US Racing</strong> will have all winnings forfeited.<br />To prevent underage access we recommend the Internet's most trusted parental control filtering sites:</p>
<p><a href="http://www.netnanny.com/">www.netnanny.com</a><br /><a href="http://www.cybersitter.com/">www.cybersitter.com</a><br /><a href="http://www.cyberpatrol.com/">www.cyberpatrol.com</a><br /><a href="http://www.contentwatch.com/">www.contentwatch.com</a></p>
<p><strong>Personal Limits:</strong></p>
<p>It’s easy for unmanaged gaming to become problem gaming, which is why we  can limit player’s betting amounts however they wish. We also have the  ability to block a member’s access to the site if they have a gaming  problem. If you think you have problem we urge you to take this <a href="http://www.antiguagaming.gov.ag/self_awareness.asp" target="_blank">self-assessment test</a>. The few minutes it takes could  be warning sign you need.</p>
<p><a title="Personal Warning System - Click Here" href="http://www.antiguagaming.gov.ag/self_awareness.asp" target="_blank">Personal&nbsp;Assesment Test- Click Here</a>&nbsp;</p>
<p><strong>Getting Help:</strong></p>
<p>If you believe you have a gaming problem there are always people who  understand your issue and help is always available. The Nevada Council  on Problem Gambling has a 24-hour, toll-free hot-line at <strong>1-800-522-4700</strong>.  You can also visit the Gamblers Anonymous website <strong><a href="http://www.gamblersanonymous.org/" target="_blank">here</a></strong>&nbsp;or  &nbsp; <strong><a href="http://www.gamcare.org.uk/" target="_blank">GamCare</a></strong>,  the leading UK provider of information, advice, support and free  counseling for the prevention and treatment of problem gambling, has  extended the opening hours of its national help lines by 2 hours. From  1st March 2010, for a trial period of 6 months, the help lines will be  opening from 8am - 2am. This initiative follows in response to an  apparent increase in the number of missed calls occurring during closed  hours.</p>
<p>These are both non-profit organizations that are in no way affiliated  with US Racing</p>
<p><strong>Working Together:</strong></p>
<p>works closely with the Antiguan government and Financial Services  Regulatory Commission (FSRC) to ensure that we exceed international  standards of best practice in regards to responsible gaming.</p>
<p> In support of this, the Antiguan Online Gaming Association, in  conjunction with the FSRC, has established a ‘Gaming Addiction Research  and Education Fund’ to help educate the public on the risks associated  with gaming, increase social awareness, particularly with minors, and  provide resources for the treatment of problem gambling. <strong>US Racing</strong> is united with both the AOGA and FSRC on the utmost  importance of these measures.</p>
<p><strong>US Racing</strong> is united with both the AOGA and  FSRC on the utmost importance of these measures.</p>
<p>Lastly, if you have any queries about these policies or our stance on responsible gaming,&nbsp;<a title="please don't hesitate to contact us." href="mailto:support@usracing.com"><strong>please don't hesitate to contact us.</strong></a>&nbsp;</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    