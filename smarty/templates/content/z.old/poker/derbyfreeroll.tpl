
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->




{include file='menus/promotions.tpl'}          
{include file='menus/help.tpl'}




<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
<!-- ======== STYLES NEEDED TO HIDE BLOCK ELEMENTS ================== -->

<!-- ============================================================== -->


<h2 class="title">Kentucky Derby $5,000 Freeroll</h1>              
<div class="content">
<!-- --------------------- content starts here ---------------------- -->



        

<h2>The Kentucky Derby $5,000 Freeroll</h2></div>

<p>As the first jewel in the Triple Crown of Thoroughbred horseracing, the Kentucky Derby has been held annually on the first Saturday of every May since 1875. While US Racing Poker hasn't been around for nearly that long we thought that such a prestigious event deserved to be recognized here in our poker room. </p>
<br />
<p><strong>How to qualify for the $5,000 Freeroll</strong></p>

<p>From Thursday, May 5th through to Saturday, May 7th  you'll notice several $1 buy-in tournaments listed in the poker lobby that start with "KENTUCKY DERBY" and have a name after it. That name corresponds with a horse scheduled to run in this year's Kentucky Derby set to take place on Saturday, May 7th. </p>

<p>Every player that participates in the tournament named after the horse that wins this year's Kentucky Derby will receive a seat into the $5,000 Kentucky Derby Final. </p>

<p>Play in one qualifier tournament or play in them all. The more you play in the better the chance you'll have of qualifying for the $5,000 Final</p>

<p>IF YOU WANT TO PLACE A REAL MONEY BET ON THE KENTUCKY DERBY <a href="/racebook">CLICK HERE</a> TO VISIT OUR RACEBOOK. </p>
<br />
<br />
<h4 class="DarkBlue">The $5,000 Freeroll</h4>

<table id="infoEntries" class="data" border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
<tr class="odd">
	<td width="35%"><strong>Tourney Name:</strong></td>
<td>The Kentucky Derby $5,000 Freeroll</td>
</tr>
<tr >
	<td><strong>Date:</strong></td>
<td>Sunday, May15th at 14:00 (as per the clock in the poker lobby)</td>
</tr>
<tr class="odd">
	<td><strong>Buy-in:</strong></td>
	<td>FREE</td>
</tr>
<tr>
	<td><strong>Prize Pool:</strong></td>
	<td>$5,000<strong></strong></td>
</tr>
</tbody>
</table>

<br />
<p>You'll find this tournament under the Private tab in our tournament lobby. Players that participate in the tournament that corresponds to the winning horse will receive a tournament coupon within 48 hours of the Kentucky Derby's finish. Players must register themselves into the $5,000 Kentucky Derby Final. ONLY QUALIFYING PLAYERS WILL BE ABLE TO REGISTER.</p>
<br />
<p><a href="http://download.merge-hosting.com/brand/everybodyplays.com/everybodyplaysinstall.exe" title="Download Poker"><b>Download Poker Now!</b></a></p>
<br />
<br />








</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      

