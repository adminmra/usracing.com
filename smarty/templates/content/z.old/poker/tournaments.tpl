
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->




{include file='menus/promotions.tpl'}          
{include file='menus/help.tpl'}




<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




<div><a href="/poker"><img src="/themes/images/poker/tournament_header.jpg" alt="Online Poker Tournaments" width="473" height="79" /></a></div>
</div>  

<!-- /block-inner, /block -->






                                      
          
<div class="headline"><h1>Poker Tournaments</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- ======== STYLES NEEDED TO HIDE BLOCK ELEMENTS ================== -->

<!-- ============================================================== -->






<h2>Poker Tournaments</h2>
<div>
<table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><p><strong>Everybody Plays Poker is pleased to offer a wide variety of tournament formats and games available to you 24 hours a day! </strong></p>
<p>In addition to being a fun and exciting way to gain a greater all-round poker experience, tournaments are also a powerful way to build your bankroll and get a great return on a minimal investment. In a tournament, as opposed to a live ring-game, the most money one can risk is the buy-in for that tournament. All players start with the same amount of chips and play continues until one player has won all the chips. Tournament chips have no cash value. Tournaments are available in all the poker games we offer; Hold’em, Omaha, Omaha Hi-Lo, Stud and Stud Hi-Lo.</p>
<p>We offer Multi-table, Sit &amp; Go, Bounty and Heads-up tournaments in various Freeze-out, Re-buy and Shoot-out formats. We also offer satellite tournaments to gain entry into much bigger events and freerolls that cost nothing to enter. Scroll down the page to read more about the different tournaments available.</p>
<br /></td>
</tr>
</table>
</div>
 
 
 
   

<h2>100 Seats to $100k</h2>
<div>
<table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><p><strong>100 Seats to $100k</strong></p>
<p>We are giving away a massive 100 seats into our first ever 100k Guaranteed, which will be held this Sunday the 27th of February at 15:00!  Qualifying for the freeroll is simple.  Just earn 500 VIP points between 00:01 on Monday the 21st of February, and the freeroll start time of 15:00 on Saturday the 26th of February.  The Freeroll is listed in your lobby as &quot;100 Seats to 100k Freeroll&quot;.</p>
<p>To help you keep track please check the counter below which will show your progress to the 500 points.  Once you reach 500 points you can simply click the register button of the tournament to gain access.</p>
<p>Tournament Details:</p>
<p>100 Seats to 100k Freeroll</p>
<p> *   Start time: 15:00 poker time on Saturday 26 February<br />
  *   Entry: Free!!! Just earn 500 points between Monday 00:01 poker time and Saturday 15:00 poker time and click the Register button!<br />
  *   Top 100 places will receive a $109 coupon which can be used to enter the 100k Guaranteed.</p>
<p>100k Guaranteed 2R/A</p>
<br /></td>
</tr>
</table>
</div>
 
       

  
<!-- Mega Millions Poker Points Race<div class="block">
<h2>Mega Millions Points Race</h2>
<div>
<table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td><p><strong>Mega Millions Point Race </strong></p>
<p>During the month of August, US Racing is giving you the chance win your piece of $1,000,000.00 in the Mega Millions Points Race. <a href="http://www.everybodyplays.com/poker/mega_million_point_race" target="_blank">Click here</a> for more details.</p>
<br /></td>
</tr>
</table>
</div>
</div> 
  -->    
 


<h2>Guaranteed Tournaments</h2>
<div>
<table id="infoEntries" cellpadding="0" cellspacing="0"  title="5 Million Guaranteed in Poker Tournaments">
<tr> 
<td valign="top"><strong>EveryBodyPlays 5 Million Dollar Guarantee</strong>
<br>
<br>
<p>Guaranteed poker tournaments carry a minimum guaranteed prize pool regardless of the number of players that buy-in. You’ll find a variety of Guaranteed tournaments ranging in buy-ins and guaranteed levels running daily here at Everybody Plays Poker. Check the tournament lobby (Tournament &gt;&gt; Scheduled &gt;&gt; All) for dates, times and full details.</p>
<p>        
<strong>$50,000 Guaranteed, Sunday @ 15:00 ET - </strong><br />
Buy In + Fee: $100 + 9 or VIP Point Buy In + Fee: 10,000 + 900
</p></td>
</tr>
</table>

</div>
 
 
 
 
    
 


<h2>Bounty Poker Tournaments</h2>
<div>
<table id="infoEntries" cellpadding="0" cellspacing="0"  title="Bounty Tournaments ">
 <tr> 
<td valign="top"><strong>Bounty Tournaments</strong><br><br>
      Probably the most satisfying tournament here at Everybody Plays Poker. When you knock another player out of the tournament you’ll collect the cash bounty on that player’s head. So not only do you rake a huge pot but you’re getting paid cash to ‘felt’ a fellow competitor. <br />
      In a Bounty tournament, a percentage of the prize pool will be taken out and used as a bounty on the heads of each player. The overall prize payout structure remains the same. As you eliminate players from a tournament you’ll collect that player’s bounty.<br />
       <br />
      Regardless of your tournament placing, you can still make a profit on bounties. For example, in a $2 buy-in tournament, $1 will contribute to the prize pool, and $1 will be paid to the bounty field. Every player has a $1 price on their head, so simply eliminate two players and you have already made your buy-in back, with only profit to come!<br />
      Bounty tournament will be available with many different prize structures. The bounty prize pool can be customized, anywhere from 10% of the buy-in to a full 100%! Simply check out our tournament listings in the Everybody Plays Poker lobby to see the prize pool structure for any one of our upcoming tournaments.</td>

</td>
</tr>
</table>

</div>
 

 
    
 


<h2>Shootout Tournaments</h2>
<div>
<table id="infoEntries" border="0" cellpadding="0" cellspacing="0"   title="Gold Chip Giveaway Tournaments">
<tr> 
<td valign="top"><strong>Shootout Tournaments</strong><br>
<p>Unlike a normal multi-table tournament where tables are balanced by moving players aroun, shoot out tournaments will play each table down to a set number of players in each round until there is a final table remaining.<br /></p>

<p> A simple example is a Shootout tournament with 100 entrants. Ten tables with ten players each will begin the tournament for round one. A certain number of players from each table will progress to round two (the number will change depending on the tournament). In our example, the top three players from each table will advance, meaning a total of 30 players remain. Round two will begin with three tables of ten, and again the top three players of each table will progress to the next round, with nine players battling it out at the final table!</p>
<p>Shootouts are definitely exciting but require a different strategy then regular MTT’s.</p>
<p><strong>&nbsp;Shootout Payout Structure</strong></p>

<table class="data" align="center" border="0" cellpadding="1" cellspacing="0" width="400">
            <tbody><tr class="tableTitle">

              <td><strong>Place Finished</strong></td>
              <td colspan="6"><strong>Number of Players</strong></td>
            </tr>
            <tr>
              <td></td>
              <td><strong>1 to 2</strong></td>

              <td><strong>3 to 8</strong></td>

              <td><strong>9 to 32</strong></td>
              <td><strong>33 to 64</strong></td>
              <td><strong>65 to 128</strong></td>
              <td><strong>128+</strong></td>
            </tr>

            <tr class="odd">

              <td><strong>1st</strong></td>
              <td>100%</td>
              <td>75%</td>
              <td>45%</td>
              <td>35%</td>

              <td>30%</td>

              <td>27%</td>
            </tr>
            <tr>
              <td><strong>2nd</strong></td>
              <td></td>
              <td>25%</td>

              <td>25%</td>

              <td>15%</td>
              <td>18%</td>
              <td>15%</td>
            </tr>
            <tr class="odd">
              <td><strong>3rd-4th</strong></td>

              <td></td>
              <td></td>
              <td>15%</td>
              <td>10%</td>
              <td>10%</td>
              <td>8%</td>
            </tr>

            <tr>
              <td><strong>5th-8th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>7.5%</td>
              <td>4%</td>

              <td>3.5%</td>
            </tr>
            <tr class="odd">
              <td><strong>9th-16th</strong></td>
              <td></td>
              <td></td>
              <td></td>

              <td></td>

              <td>2%</td>
              <td>1.5%</td>
            </tr>
            <tr>
              <td><strong>17th-32nd</strong></td>
              <td></td>

              <td></td>

              <td></td>
              <td></td>
              <td></td>
              <td>1%</td>
            </tr>
          </tbody></table>
          <p><br>

            Final Table uses Standard Tournament Payout based on number of players at final  table.</p>

          <p><strong>Percentage of Total Prize Pool paid to each round ¹</strong></p>
          <table class="odd" align="center" border="0" cellpadding="1" cellspacing="0" width="400">
            <tbody><tr class="tableTitle">
              <td><strong>No. of Rounds</strong></td>
              <td><strong>Round 1</strong></td>

              <td><strong>Round 2</strong></td>
              <td><strong>Round 3</strong></td>

              <td><strong>Round 4+</strong></td>
            </tr>
            <tr>
              <td>1</td>

              <td>100%</td>
              <td></td>
              <td></td>

              <td></td>
            </tr>
            <tr class="odd">
              <td>2</td>

              <td>70%</td>
              <td>30%</td>
              <td></td>

              <td></td>
            </tr>
            <tr>
              <td>3</td>

              <td>50%</td>
              <td>30%</td>
              <td>20%</td>

              <td></td>
            </tr>
            <tr class="odd">
              <td>4</td>

              <td>50%</td>
              <td>25%</td>
              <td>15%</td>

              <td>10%</td>
            </tr>
          </tbody></table>

          <p>¹ When this  structure is applied, only rounds 1,2 and 3 will received the guaranteed  payouts shown in this table. The final 10% of the winnings will contribute to  the payout for the rest of the tournament (round 4 and any subsequent rounds)  and will be paid out using the existing MTT payout structure shown here: <a href="Payout_Structure.php">Standard Tournament Payout </a></p>
          <p><strong>Example 1</strong></p>
          <p>            $10+$1, 100  Players, 10 Player tables, 3 Players Advancing<br>

            Number of  Rounds is 3, total prize pool $1000</p>
          <table class="data" align="center" border="0" cellpadding="1" cellspacing="0" width="400">

            <tbody><tr class="tableTitle">
              <td></td>
              <td><strong>Round 1</strong></td>
              <td><strong>Round 2</strong></td>
              <td><strong>Round 3</strong></td>

            </tr>
            <tr>

              <td><strong>Prize Pool </strong></td>
              <td>$500.00</td>
              <td>$300.00</td>
              <td>$200.00</td>
            </tr>

            <tr class="odd">

              <td><strong>Tables</strong></td>
              <td>10</td>
              <td>3</td>
              <td>1</td>
            </tr>
            <tr>

              <td><strong>Players </strong></td>
              <td>100</td>
              <td>30</td>
              <td>9</td>
            </tr>
            <tr class="odd">
              <td><strong>Table Prizes</strong></td>

              <td>$50.00</td>
              <td>$100.00</td>
              <td>$200.00</td>
            </tr>
            <tr>
              <td><strong>Split</strong></td>

              <td>50/30/20</td>

              <td>50/30/20</td>
              <td>70/30</td>
            </tr>
            <tr class="odd">
              <td><strong>1st</strong></td>

              <td>$25.00</td>
              <td>$50.00</td>

              <td>$140.00</td>
            </tr>
            <tr>
              <td><strong>2nd</strong></td>

              <td>$15.00</td>
              <td>$30.00</td>
              <td>$60.00</td>
            </tr>
            <tr class="odd">
              <td><strong>3rd</strong></td>

              <td>$10.00</td>
              <td>$20.00</td>
              <td></td>
            </tr>
          </tbody></table>
          <p><strong>Example 2</strong></p>
          <p>          $10+$1, 240  Players, 6 Player tables, 2 Players Advancing</p>

<p>Number of  Rounds is 5, total prize pool $2400</p>
          <table class="data" align="center" border="0" cellpadding="1" cellspacing="0" width="400">
            <tbody><tr class="tableTitle">
              <td></td>

              <td><strong>Round 1</strong></td>
              <td><strong>Round 2</strong></td>
              <td><strong>Round 3</strong></td>

              <td><strong>Round 4</strong></td>
              <td><strong>Round 5</strong></td>
            </tr>

            <tr>
              <td><strong>Prize Pool </strong></td>
              <td>$1200.00</td>

              <td>$600.00</td>
              <td>$360.00</td>
              <td></td>
              <td>$240.00</td>
            </tr>
            <tr class="odd">
              <td><strong>Tables</strong></td>

              <td>40</td>
              <td>14</td>
              <td>5</td>
              <td>2</td>

              <td></td>
            </tr>

            <tr>
              <td><strong>Players </strong></td>
              <td>240</td>
              <td>80</td>
              <td>28</td>

              <td>10</td>

              <td>4</td>
            </tr>
            <tr class="odd">
              <td><strong>Table Prizes</strong></td>
              <td>$30.00</td>
              <td>$42.86</td>

              <td>$72.00</td>
              <td>*</td>
              <td></td>
            </tr>
            <tr>
              <td><strong>Split</strong></td>
              <td>70/30</td>

              <td>70/30</td>
              <td>70/30</td>
              <td></td>
              <td>50/30/20</td>
            </tr>
            <tr class="odd">
              <td><strong>1st</strong></td>

              <td>$21.00</td>
              <td>$30.00</td>
              <td>$50.40</td>
              <td></td>
              <td></td>
            </tr>
            <tr>

              <td><strong>2nd</strong></td>
              <td>$9.00</td>
              <td>$12.86</td>
              <td>$21.60</td>
              <td></td>
              <td></td>
            </tr>

            <tr class="odd">
              <td><strong>3rd</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody></table>
          <p>* No payouts  for this table as it now uses a standard MTT payout,which pays 50/30/20 for 10  players.          </p>                  </td>
                </tr>
</table>

</div>
 
      





<h2>Freeroll Tournaments</h2>
<div>
<table id="infoEntries" border="0" cellpadding="0" cellspacing="0"  title="Gold Card Tournaments">
     
     
                <tr> 
                  <td valign="top"><strong>Freeroll Tournaments</strong>
                    <p>
We host several FREE tournaments throughout the day that feature cash prize pools ranging from $50 to $50,000! Try your hand in one of our $50 freerolls starting every couple of hours or play in our bigger $2,000 &amp; $10,000 tournaments once a week or even the massive $50,000 that runs once a month!</p>                  </td>
                </tr>
</table>
</div>
 
     
 
 

<h2>Exclusive VIP Freerolls</h2>
<div>
<table id="infoEntries" border="0" cellpadding="0" cellspacing="0"  title="Gold Card Tournaments">
     
     
                <tr> 
                  <td valign="top"><strong>Exclusive VIP Freerolls</strong>
                    <p>Each VIP tier offers a private freeroll for all players in that tier. To find out what your tier has to offer, visit our VIP Rewards.</p><p>
                      For more information on VIP tiers, visit our VIP section</p>
                  <p>To see your current VIP tier, login to your player admin and view &quot;VIP Status&quot;</p>                  </td>
                </tr>
</table>
</div>

     
 


<h2>New Player Freerolls</h2>
<div>
<table id="infoEntries" border="0" cellpadding="0" cellspacing="0"  title="Gold Card Tournaments">
     
     
                <tr> 
                  <td valign="top"><strong>New Player Freerolls</strong>
                    <p>We also host a new player daily freeroll. Make your first deposit here at Everybody Plays Poker and you’ll automatically receive a ticket to enter one of these daily freerolls that carry a $500 prize pool.</p></td>
                </tr>
</table>
</div>

     
 


<h2>Sit'n'Go Tournaments</h2>
<div>
<br /><br />
<p><strong>Sit'n'Go Tournaments</strong></p>
<br />
<p>Sit and Go tournaments are usually single table tournaments, but can also be multitable. Sit and Go tournaments have a specific buy-in and entry fee amount, which then forms the prize pool. This is split up by the winners, generally amongst the top 3, following a payout schedule.</p>
<br />

<table id="infoEntries" class="data" align="center" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr class="tableTitle" style="padding:0;">
<td colspan="4"><strong>Blind Structure</strong></td>
</tr>

            <tr>
              <td width="25%"><strong>SNG's</strong></td>
              <td width="25%"> <strong>9 mins </strong></td>
              <td width="25%"><strong>Stack:1500 </strong></td>
              <td width="25%"></td>
            </tr>
            <tr class="odd">
              <td><strong>LEVEL</strong></td>
              <td><strong>SB</strong></td>
              <td><strong>BB</strong></td>
              <td><strong>ANTE</strong></td>
            </tr>
            <tr>

              <td>1</td>
              <td>5</td>
              <td>10</td>
              <td></td>
            </tr>
            <tr class="odd">
              <td>2</td>

              <td>10</td>
              <td>20</td>
              <td></td>
            </tr>
            <tr>
              <td>3</td>
              <td>15</td>

              <td>30</td>
              <td></td>
            </tr>

            <tr class="odd">
              <td>4</td>
              <td>25</td>
              <td>50</td>

              <td></td>
            </tr>
            <tr>

              <td>5</td>
              <td>50</td>
              <td>100</td>
              <td></td>
            </tr>
            <tr class="odd">
              <td>6</td>

              <td>75</td>
              <td>150</td>
              <td></td>
            </tr>

            <tr>
              <td>7</td>
              <td>100</td>

              <td>200</td>
              <td></td>
            </tr>
            <tr class="odd">

              <td>8</td>
              <td>150</td>
              <td>300</td>

              <td>25</td>
            </tr>
            <tr>

              <td>9</td>
              <td>200</td>
              <td>400</td>
              <td>25</td>
            </tr>
            <tr class="odd">

              <td>10</td>
              <td>300</td>
              <td>600</td>
              <td>50</td>
            </tr>

            <tr>

              <td>11</td>
              <td>400</td>
              <td>800</td>
              <td>75</td>
            </tr>
            <tr class="odd">

              <td>12</td>
              <td>600</td>
              <td>1200</td>
              <td>100</td>
            </tr>
            <tr>
              <td>13</td>

              <td>800</td>
              <td>1600</td>
              <td>125</td>
            </tr>
            <tr class="odd">
              <td>14</td>

              <td>1000</td>

              <td>2000</td>
              <td>150</td>
            </tr>
            <tr>
              <td>15</td>

              <td>1500</td>
              <td>3000</td>

              <td>200</td>
            </tr>
            <tr class="odd">
              <td>16</td>

              <td>2000</td>
              <td>4000</td>
              <td>250</td>
            </tr>
            <tr>
              <td>17</td>

              <td>3000</td>
              <td>6000</td>
              <td>300</td>
            </tr>

            <tr class="odd">
              <td>18</td>

              <td>4000</td>
              <td>8000</td>
              <td>400</td>
            </tr>
            <tr>

              <td>19</td>

              <td>6000</td>
              <td>12000</td>
              <td>600</td>
            </tr>
            <tr class="odd">
              <td>20</td>

              <td>8000</td>
              <td>16000</td>
              <td>800</td>
            </tr>
            <tr>
              <td>21</td>
              <td>10000</td>

              <td>20000</td>
              <td>1000</td>
            </tr>
            <tr class="odd">
              <td>22</td>
              <td>15000</td>

              <td>30000</td>

              <td>1500</td>
            </tr>
            <tr>
              <td>23</td>
              <td>20000</td>

              <td>40000</td>
              <td>2000</td>
            </tr>
            <tr class="odd">
              <td>24</td>
              <td>30000</td>

              <td>60000</td>
              <td>3000</td>
            </tr>

            <tr>
              <td>25</td>
              <td>40000</td>

              <td>80000</td>
              <td>4000</td>
            </tr>
            <tr class="odd">

              <td>26</td>
              <td>60000</td>

              <td>120000</td>
              <td>6000</td>
            </tr>
            <tr>
              <td>27</td>

              <td>80000</td>

              <td>160000</td>
              <td>8000</td>
            </tr>
            <tr class="odd">
              <td>28</td>
              <td>100,000</td>

              <td>200000</td>
              <td>10000</td>
            </tr>
            <tr>
              <td>29</td>
              <td>150,000</td>
              <td>300000</td>

              <td>15000</td>
            </tr>
            <tr class="odd">
              <td>30</td>
              <td>200,000</td>
              <td>400000</td>

              <td>20000</td>
            </tr>
</tbody>
</table>
<br /><br />
<p><strong>Live Event Satellites</strong></p>
<p><strong>Tournament Payout  Schedule</strong></p>
<p>This table  shows the standard payouts for multi-table tournaments run here at Everybody Plays Poker.  The left column shows the finishing place, while the column headers displays  the amount of entries (players) in the tournament. For example, if you finish  in 9th place in a tournament with 250 people, you would be paid 2% of the total  prize pool.</p>
<br /><br />
{literal}
<style type="text/css"></style>
{/literal}
<table id="infoEntries" class="data payschd" align="center" border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr class="tableTitle">

              <td></td>
              <td><strong>1-9</strong></td>
              <td><strong>10-<br>29</strong></td>
              <td><strong>30-<br>49</strong></td>
              <td><strong>50-<br>99</strong></td>

              <td><strong>100-<br>199</strong></td>

              <td><strong>200-<br>299</strong></td>
              <td><strong>300-<br>399</strong></td>
              <td><strong>400-<br>499</strong></td>

              <td><strong>500-<br>749</strong></td>
              <td><strong>750+</strong></td>
            </tr>

            <tr>
              <td><strong>1st</strong></td>
              <td>70%</td>

              <td>50%</td>
              <td>40%</td>
              <td>30%</td>

              <td>30%</td>
              <td>27%</td>
              <td>26%</td>

              <td>25%</td>
              <td>25%</td>
              <td>25%</td>
            </tr>
            <tr class="odd">
              <td><strong>2nd</strong></td>

              <td>30%</td>
              <td>30%</td>
              <td>25%</td>
              <td>20%</td>

              <td>20%</td>
              <td>18%</td>

              <td>17%</td>
              <td>16%</td>
              <td>15%</td>
              <td>14%</td>
            </tr>
            <tr>

              <td><strong>3rd</strong></td>
              <td></td>
              <td>20%</td>
              <td>15%</td>
              <td>12%</td>

              <td>10%</td>

              <td>9%</td>
              <td>8%</td>
              <td>8%</td>
              <td>7.5%</td>
              <td>7.5%</td>
            </tr>

            <tr class="odd">
              <td><strong>4th</strong></td>
              <td></td>
              <td></td>
              <td>12%</td>
              <td>10%</td>

              <td>8%</td>

              <td>7%</td>
              <td>6%</td>
              <td>6%</td>
              <td>5.5%</td>
              <td>5%</td>
            </tr>

            <tr>
              <td><strong>5th</strong></td>
              <td></td>
              <td></td>
              <td>8%</td>
              <td>8%</td>

              <td>6%</td>

              <td>6%</td>
              <td>5%</td>
              <td>5%</td>
              <td>4.5%</td>
              <td>4.25%</td>
            </tr>

            <tr class="odd">
              <td><strong>6th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>6%</td>
              <td>5%</td>

              <td>5%</td>
              <td>4%</td>
              <td>4%</td>
              <td>3.5%</td>
              <td>3.5%</td>
            </tr>

            <tr>
              <td><strong>7th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>5%</td>
              <td>4%</td>

              <td>4%</td>
              <td>3%</td>
              <td>3%</td>
              <td>3%</td>
              <td>2.75%</td>
            </tr>

            <tr class="odd">
              <td><strong>8th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>4%</td>
              <td>3%</td>

              <td>3%</td>
              <td>2.5%</td>
              <td>2.5%</td>
              <td>2.5%</td>
              <td>2.25%</td>
            </tr>

            <tr>
              <td><strong>9th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>3%</td>
              <td>2.25%</td>

              <td>2%</td>
              <td>2%</td>
              <td>2%</td>
              <td>2%</td>
              <td>1.75%</td>
            </tr>

            <tr class="odd">
              <td><strong>10th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td>2%</td>
              <td>1.75%</td>

              <td>1.5%</td>
              <td>1.5%</td>
              <td>1.5%</td>
              <td>1.5%</td>
              <td>1.5%</td>
            </tr>

            <tr>
              <td><strong>11th-<br>20th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>1%</td>

              <td>1%</td>

              <td>1%</td>
              <td>1%</td>
              <td>1%</td>
              <td>1%</td>
            </tr>

            <tr class="odd">
              <td><strong>21st-<br>30th</strong></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>0.75%</td>

              <td>0.75%</td>
              <td>0.75%</td>

              <td>0.75%</td>
              <td>0.75%</td>
            </tr>
            <tr>

              <td><strong>31st-<br>40th</strong></td>
              <td></td>
              <td></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>0.5%</td>

              <td>0.5%</td>
              <td>0.5%</td>

              <td>0.5%</td>
            </tr>
            <tr class="odd">
              <td><strong>41st-<br>50th</strong></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>

              <td></td>
              <td></td>
              <td>0.25%</td>
              <td>0.25%</td>

              <td>0.25%</td>
              <td>0.25%</td>
            </tr>

            <tr>
              <td><strong>51st-<br>60th</strong></td>
              <td></td>
              <td></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>

              <td></td>
              <td>0.2%</td>
              <td>0.2%</td>

              <td>0.2%</td>
            </tr>
            <tr class="odd">
              <td><strong>61st-<br>80th</strong></td>

              <td></td>
              <td></td>
              <td></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>0.15%</td>

              <td>0.15%</td>
            </tr>
            <tr>
              <td><strong>81st-<br>100th</strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>

              <td></td>

              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>0.125%</td>
            </tr>
          </tbody></table>
<p>&nbsp;</p>
<p>* We reserve  the right to change tournament payouts and/or the schedule at any time without  notice.</p>
<p>&nbsp;</p>
</div>






        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    