{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">How To Bet On Horses</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/howto/placeabet" title="">LEARN HOW TO PLACE A BET</a></li>
<li><a href="/howto/betonhorses" title="">LEARN HOW TO PLACE A HORSE WAGER</a></li>
<li><a href="/howto/readthedailyracingform" title="">LEARN HOW TO READ THE DAILY RACING FORM</a></li>
<li class="leaf active-trail"><a href="/howto/readpastperformances" title="" class="active">PAST PERFORMANCES</a></li>
<li><a href="/flash/howtobetonsports" title="">LEARN HOW TO PLACE A SPORTS BET</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>PAST PERFORMANCES</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>

<style></style>

    
<div>

<div style="padding-top: 10px;" id="flashmovie3">Past Perfomance Flash Movie not loaded</div>

<script type="text/javascript">		
  var so = new SWFObject("/flash/form.swf", "pastPerformances", "685", "393", "8", "#FFFFFF"); 
  so.addParam("allowscriptaccess","always"); so.useExpressInstall("/flash/expressinstall.swf"); 
  so.addParam("bgcolor", "#105ca9");
  so.addParam("wmode", "transparent");
  so.write("flashmovie3");		
</script>

<div class="tabcontent">
<p align="left"><span class="mediumtext">Tours require a Flash Player plug in. Having problems viewing the tutorials?</span></p>

<p>Download the latest Flash Player plug-in.</p>

<p><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank"><img title="Flash Player" border="0" alt="Flash Player" src="/themes/images/get_flash_player.gif" /></a></p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container -->

          <div id="footer"><div id="footer-inner" class="region region-footer">

        
        

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div class="logos" style="padding-right: 50px;"><img src="/themes/images/footer-logos.gif" alt="" title="" width="592" height="45" usemap="#MapPayments" /></div>
<map name="MapPayments" id="MapPayments">
	<!--<area shape="rect" coords="0,0,46,45" href="javascript:void(0);" onclick="open_banking();" />-->
	<area shape="rect" coords="54,0,108,45" href="javascript:void(0);" onclick="open_banking();" />
	<area shape="rect" coords="121,0,202,45" href="javascript:void(0);" onclick="open_banking();" /> 
	<area shape="rect" coords="209,0,253,45" href="http://www.gamblersanonymous.org/" target="_blank" rel="nofollow" alt="Gamblers Anonymous" />
	<area shape="rect" coords="257,0,325,45" href="http://www.moneybookers.com/app/" target="_blank" alt="Money Bookers" />
	<!--<area shape="rect" coords="332,0,422,45" href="" /> over18only-->
	<area shape="rect" coords="428,0,473,45" href="http://www.antiguagaming.gov.ag/SealValidator.asp?ID=2282020410" rel="nofollow" target="_blank" alt="E-GAMING Operator License" />
	<area shape="rect" coords="480,0,616,45" href="http://www.tstglobal.com/" target="_blank" rel="nofollow" alt="Technical Systems Testing" />
</map>  </div>

  

<!-- /block-inner, /block -->



<div id="block-menu-menu-footer-links" class="block block-menu region-even odd region-count-2 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <ul class="menu"><li class="leaf first"><a href="/" title="Horse Racing">Horse Racing</a></li>
<li><a href="/otb" title="Offtrack Betting">Offtrack Betting</a></li>
<li><a href="/advancedepositwagering" title="Advance Deposit Wagering">Advance Deposit Wagering</a></li>
<li><a href="/responsibilities" title="Responsible Gaming">Responsible Gaming</a></li>
<li><a href="/" title="Online Horse Racing">Online Horse Racing</a></li>
</ul>  </div>

  

</div><!-- /block-inner, /block -->




      </div></div> <!-- /#footer-inner, /#footer -->
    
  