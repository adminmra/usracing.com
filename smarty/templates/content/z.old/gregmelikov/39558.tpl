<div id="main" class="delete"><div id="main-inner" class="clearfix with-navbar">
      
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          <div id="col3_inside" class="floatbox">
                                                
                                      
          
<div class="headline"><h1>US Racing | Daily Racing News</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />
<div class="article">

<div class="InfoRibbon">
<span class="Source">Daily Racing News</span><!-- SOURCE -->
<span class="ShortDate">9/6/2010</span><!-- DATE - SHORT -->
</div>

<!-- AddThis Button BEGIN -->
<div style="display: block; height: 16px; width:125px; padding:0; margin-top: 6px;" >
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b7342703113cb61"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>{literal}<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b7342703113cb61"></script>{/literal}
</div>
<!-- AddThis Button END -->

<div class="Author">Greg Melikov</div><!-- AUTHOR -->
<div class="LongDate">Tuesday, September 6th, 2010</div>
<!-- DATE - LONG -->

<div class="inner">
<div class="body"><!-- ARTICLE -->

<p><strong>Greg Melikov’s Horses to Watch</strong><br />
   <br />
  Horses worth watching, a list of runners compiled by handicapper/turf writer Greg Melikov, should do well next time out. These thoroughbreds won seven times, ran second four times and finished third seven times since Aug. 4. They’re worth considering when developing your wagering strategy for online betting or at your favorite track.<br />
   <br />
  ARLINGTON PARK<br />
   <br />
  I’m Not Afraid: Raced six lengths behind in fifth after three-        eighths of a mile, shifted out for drive, split horses in stretch,   rallied to make up 4 ½ lengths and finished second beaten a half-length at 5 ½ furlongs on the turf Aug. 26.<br />
   <br />
  Jingle Jangle: Raced 12 ½ lengths behind in fifth after a half-mile, circled four wide into contention through far turn, rallied in stretch to make up more than two lengths and finished second beaten a nose at 1 1/16 miles on Aug. 20.   <br />
   <br />
                                          <br />
  Golden Teardrop: Raced fifth early, dropped to sixth more than nine lengths behind after a half-mile, shifted out falling to seventh on far turn, rallied in upper stretch from fifth to make up more than four lengths and finished second beaten a length at 1 1/16 miles on Aug. 15.<br />
                                                                     <br />
  CALDER<br />
   <br />
  Baystreet Bully: Raced more than 9 ½ lengths behind in fifth after a half-mile, angled out for room in stretch, rallied to make up more than 3 ½ lengths and finished second beaten a neck at a mile on Sept. 3.<br />
   <br />
  Fusafast: Raced five lengths behind in second after a half-mile, dropped to fourth after six furlongs, rallied in stretch to make up more than 4 ¾ lengths and finished second beaten a neck at 1 1/16 miles on the turf Aug. 20, ran final 2 ½ furlongs in 30 1/5.<br />
   <br />
  Massecuite: Raced fifth early, dropped to sixth more than eight length behind after a half-mile, angled out widest of all in stretch losing position to seventh and last, rallied to make up 4 ½ lengths and finished third beaten two heads at 6 ½ furlongs on Aug. 11. (Raced more than 5 ½ lengths behind in fifth after a half-mile, rallied mildly and finished third by 2 ½ lengths at seven furlongs on Sept. 2.)<br />
   <br />
  Kieman’s Legacy: Chased pace two lengths behind in third after a half-mile and rallied to finish second by 3 ¼ lengths at seven furlongs on Aug. 1. (Tracked pace a length behind in third after a half-mile, gained lead by a head leaving the turn, weakened and finished second beaten a neck ay 6 ½ furlongs on Sept. 2.)<br />
   <br />
  Alpha Glitter: Raced more than five lengths behind in fifth after a half-mile, advanced to third outside in stretch, but didn’t close gap and finished in that position at 1 1/16 miles on the turf Aug. 8. (Tracked pace in fourth more than 4 ½ lengths behind after a half-mile, rallied from third in deep stretch and won by a nose at a mile on the turf Aug. 27.)    </p>
<p>DELAWARE PARK<br />
   <br />
  Music Is My Life: Raced 10 ½ lengths behind in fifth after a half-mile, rallied outside in stretch to make up 5 ½ lengths and finished second beaten a neck at 5 ½ furlongs on Aug. 14. <br />
   <br />
  Safety Valve: Chased pace 2 ½ lengths behind in second after a half-mile, kept to the task and finished in that position beaten 2 ¼ lengths at 1 1/16 miles on the turf Aug. 6 at Colonial Downs. (Raced third early, dropped to fifth more than 2 ½ lengths behind after a half-mile, ran down leader in stretch and won by a half-length at a mile and 70 yards on the turf Aug. 31; returned $13.40 on front end.)    <br />
                                                                                                      <br />
  Cray Cray: Tracked pace a length behind in second after a half-mile, responded smartly when asked to gain lead by 1 ½ lengths in stretch and drew off to win by 7 ¼ lengths ridden out in 1:10 3/5 for six furlongs on July 24; ran final quarter in 24 1/5. (Raced more than 5 ½ lengths behind in sixth after a half-mile, advanced wide to third in stretch and ran down leader to win by 1 ¾ lengths at six furlongs on Aug. 16.)<br />
   <br />
  Barefoot Landing: Raced 4 ½ lengths behind in fifth after a half-mile, forced way between rivals in mid-stretch from sixth and finished third beaten a neck and a head at 1 1/16 miles on the turf July 3. (Raced 7 ½ lengths behind in  fourth after a half-mile, rallied from far turn into stretch and won by a half-length at a mile and 70 yards on the turf Aug. 4.)<br />
   <br />
  Andwhatelse: Raced more than seven lengths behind in seventh after a half-mile, made a big middle move after five furlongs from sixth to second in stretch and drew off to win by three lengths at 1 1/16 miles on a sealed sloppy surface July 13. (Raced more than 12 ½ lengths behind in seventh after a half-mile, made a middle move to mid-stretch, failed to sustain bid from fourth and finished third by three-quarters of a length at 1 1/16 miles on Aug. 12.)<br />
   <br />
  DEL MAR<br />
   <br />
  Pink Diamond: Stalked pace 1 ½ lengths behind in third after three-eighths of a mile, rallied a bit in stretch and finished second beaten a length at five furlongs on the turf Aug. 9. (Raced 4 ½ lengths behind in fifth after three-eighths of a mile, rallied four wide into stretch, made up more than 2 ¾ lengths and finished second beaten three-quarters of a length at five furlongs on the turf Sept. 2)<br />
   <br />
  ELLIS PARK<br />
   <br />
  Cornwhiskey: Raced three lengths behind in third after a half-mile, advanced leaving far turn, took lead in stretch by a head and won by a half-length at 1 1/16 miles on the turf Aug. 13. (Dueled for lead a head behind in second after a half-mile, put a head in front down the lane, increased margin to 1 ½ lengths in stretch and won by a half-length at 1 1/16 miles on the turf on Sept. 4.)<br />
   <br />
  EVANGELINE DOWNS<br />
   <br />
  Regency Square: Raced more than six lengths behind in sixth after three-eighths of a mile, saved ground on the turn, closed fast in stretch to make up more than 4 ¼ lengths and finished third beaten two necks at 5 ½ furlongs on July 15. (Raced more than three lengths behind in fifth after a half-mile and rallied a bit to gain third by three-quarters of a length at six furlongs on Aug. 25.)<br />
   <br />
  LOUISIANA DOWNS</p>
<p>All Out Blitz: Raced in traffic between rivals more than 2 ½ lengths in fifth after a half-mile, steadied on far turn, came four wide into stretch and rallied from fourth to gain second by 1 ¼ lengths at 7 ½ furlongs on the turf Aug. 26.<br />
   <br />
  Truly Judy: Broke outward, bobbled, raced more than 3 ½ lengths behind in seventh after a half-mile, came five wide out of the turn, rallied to make up more than 3 ½ lengths in stretch and finished second beaten a half-length at six furlongs on Aug. 20.<br />
   <br />
  Native Holly: Broke on top, set easy pace under restraint, led by 10 lengths and won by 11 ¼ lengths ridden out at 1:37 3/5 for about a mile on the turf Aug. 12; cruised final quarter in 22 3/5. (Led early, forced pace in second a head behind after a half-mile, took led at three-sixteenths pole, weakened and finished second by 2 ½ lengths at 7 ½ furlongs on the turf Sept. 3.)<br />
   <br />
  Lori’s Dream: Dueled for lead between rivals a head back in second at a half-mile, got a half-length in front in stretch, weakened and finished third beaten a half-length and a head at six furlongs on July 31. (Pressed pace two heads behind in third after three-eighths of a mile, rallied four wide on turn, led by three lengths in stretch and drew off to win by 5 ¾ lengths at 5 ½ furlongs on Sept. 3; returned $11.40 on front end.)<br />
   <br />
  MONMOUTH PARK</p>
<p>Waltzing With Blue: Broke outward, bumped, raced eighth more than five lengths behind after a half-mile, came four wide into the lane and rallied to finish second beaten a nose at 1 1/16 miles on the turf Aug. 27.<br />
   <br />
  Purrfect Bluff: Raced more than four lengths behind in fifth after three-eighths of a mile, rallied four wide to fourth in stretch, closed gamely to make up nearly 3 ½ lengths and finished second beaten a head at five furlongs on the turf Aug. 20.<br />
   <br />
  Secret Getaway: Bumped after start, raced fourth more than 1 ½ lengths behind after a half-mile, bumped with rival turning for home, split foes in stretch and closed gamely inside to finish second beaten a neck at 1 1/16 miles on Aug. 8.<br />
   <br />
  Most Wanted Affair: Raced more than eight lengths behind in seventh after a half-mile, came four wide into stretch, rallied from sixth to make up more than 2 ¼ lengths and finished second beaten three-quarters of a length at a mile and 70 yards on July 25. (Raced more than 5 ½ lengths behind in sixth after a half-mile, came three wide into the lane and finished third by 3 ¾ lengths at a mile and 70 yards on Aug. 21.)<br />
   <br />
  PENN NATIONAL<br />
   <br />
  Hey Doc Holiday: Broke slowly 10th and last, trailed by more than 5 ½ lengths after a half-mile, lost ground circling six wide into stretch, made up more than 4 ½ lengths and gained second by 1 ¼ lengths at seven furlongs on July 31 at Laurel Park. (Broke slowly seven and last, raced nine lengths behind in sixth after a half-mile, advanced in stretch and finished third by 3 ¼ lengths at six furlongs on Aug. 25.)<br />
   <br />
  PHILADELPHIA PARK<br />
   <br />
  Show the Flag: Raced more than 2 ½ lengths behind in sixth after a half-mile, lost a couple lengths off the turn, split rivals in stretch, made up nearly 3 ¾ lengths and finished second beaten a nose and three-quarters of a length at 6 ½ furlongs on Sept. 3.<br />
   <br />
  RETAMA PARK<br />
   <br />
  Timber Play: Raced more than 11 ½ lengths behind in ninth after a half-mile, swung out seven wide in upper stretch, rallied from sixth to make up more than 5 ¼ lengths and finished third beaten a nose and 1 ¼ lengths at six furlongs on Sept. 4; ran final quarter in 24 1/5.<br />
   <br />
  SARATOGA</p>
<p>Chestoria: Raced more than 4 ½ lengths behind in fourth after a half-mile, roused nearing stretch, gained ground looking for room outside at eighth pole to split rivals when seam closed, steadied, altered course inside at sixteenth pole, finished strongly to make up more than 2 ¼ lengths and finished second nosed out at 1 1/16 miles on inner turf course July 31; ran finial 1½ furlongs in 28 1/5. (Raced more than 5 ½ lengths behind in eighth after a half-mile, bulled way outward to avoid traffic ahead and finished third by 1 ¼ lengths at 1 1/8 miles on the turf Aug. 20.)  <br />
   <br />
  Blazing Honor: Raced four lengths behind in seventh after a half-mile, came five wide into stretch, closed readily from third around sixteenth pole and drew off to win by 1 ¼ lengths at seven furlongs on the turf July 17 at Belmont Park. (Bumped at break on rail, raced 10th and last 12 ½ lengths behind after a half-mile, moved outside from between horses on far turn, brushed with rival, advanced to fourth in stretch and closed under right hand urging to win by a length at 1 1/16 miles on a good turf course Aug. 16; returned $19.20 on front end.)</p>
<p>SUFFOLK DOWNS<br />
   <br />
  Albright: Rushed up to front early, led by a length after three-eighths of a mile, increased margin to seven lengths in stretch and won by 13 lengths ridden out at 5 ½ furlongs on Aug. 21; ran final 1 ½ furlongs in 18 3/5.<br />
   <br />
  Security Guard: Broke seventh and last, raced sixth more than 11 lengths behind after a half-mile, closed gap into stretch, came flying late to make up more than 6 ¼ lengths and finished third beaten a nose and a half-length at six furlongs on July 14. (Raced seven lengths behind in sixth after a half-mile, angled out three wide off far turn and rallied to gain second by a nose at a mile and 70 yards on Aug. 11.)<br />
   <br />
  WOODBINE<br />
   <br />
  Howwondofulitis: Raced more than 1 ½ lengths behind in fifth after a half-mile, lost ground angling out in upper stretch, rallied to make up nearly 2 ½ lengths and finished second beaten a neck at seven furlongs on Sept. 3.<br />
   <br />
  Something Extra: Broke a step slow, bobbled at break, checked in tight entering turn and rallied in stretch from third to gain second by a half-length at six furlongs on Aug. 12.<br />
   <br />
  Moral: Raced more than 7 ½ lengths behind in seventh after a half-mile, rallied smartly between rivals in stretch from fifth to make up six lengths and finished second beaten a head at 1 1/16 miles on July 28.<br />
   <br />
  Sugar Bay: Raced third early, dropped to fourth more than eight lengths behind after a half-mile, rallied along rail in stretch and won by a half-length at seven furlongs on the turf Aug. 13. (Raced eighth and last early, advanced to fifth 2 ½ lengths behind after a half-mile and rallied wide in stretch to finish third by three-quarters of a length at seven furlongs on the turf Sept. 5.)<br />
   <br />
  Freddie Anteros: Raced more than nine lengths behind in 12th after a half-mile, hustled between rivals on turn, closed fast from sixth in stretch to make up 4 ¾ lengths and finished second beaten three-quarters of a length at 6 ½ furlongs on June 17.<br />
   </p>
