
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->




{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}





<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">








<div class="promos-title"><h2>Kentucky Derby Virtual Derby Tournament</h2></div>
    
<div id="promos-content">

<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-vd-tourney.gif" /></div>
			<div class="header">Kentucky Derby Virtual Derby Tournament</div>        
			<div class="current-info">
<p>Road to the Roses FREE Virtual Derby Tournament <b>$1,000 Up for Grabs</b></p>

<p>Celebrate the 137 Kentucky Derby on May 7th in the Casino by entering our free tournament. Just play our Virtual Derby and you're automatically entered into the tournament. There's $1,000.00 up for grabs for the players with the more bets and highest payout percentage:</p>
<br />
<b>First Prize:</b> &nbsp; $500<br />
<b>Second Prize:</b> &nbsp; $375<br />
<b>Third Prize:</b> &nbsp; $125<br />
<br />

<ol>
<li>Promotion is open to all active members.</li>
<li>Registration is not required.</li>
<li>To be eligible you must have action on the featured game between Wednesday May 4th  11am and Thursday May 5th  11pm ET.</li>
<li>To qualify for prizes, player must play a minimum 50 rounds.</li>
<li>Any other Casino games other than the featured will not count for this tournament.</li>
<li>Tournament prizes awarded based on number of bets and highest payout percentage.</li>
<li>Standard casino rollover requirements apply.</li>
<li>We only allow one bonus per account/household or environment where computers are shared.</li>
<li>US Racing reserves the right to modify or cancel this promotion at any time.</li>
<li>General <a href="/house-rules">House Rules</a> apply.</li>
</ol>


			</div><!-- end: .current-info --> 
</div><!-- end: .promo-box -->


</div><!-- end: #promos-content -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





