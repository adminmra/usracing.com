{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Blackjack Lucky 7s&#039; Bonus</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>Blackjack Lucky 7s' Bonus</h2></div>
    
<div id="promos-content">
    
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-blackjackLucky7s.gif" /></div>
			<div class="header">Casino Promo!</div>        
			<div class="current-info"><p>Ever play a hand and instantly wish you had just played that separate side bet? Well now is your chance to reap the rewards without the risk! Check out our Blackjack Super 7’s game – all the fun of Blackjack with that extra something – and you can win without even placing the side bet every day of the week!<br /><br />
Play the extra bet and get paid instantly, however, if you miss out on placing this bet and hit three 7’s of the same suit in the same hand – we’ll pay you $500.00!</p>
 
<br /><br />
<p class="text-note"><strong>Rules:</strong> Simply provide the Round ID for ANY winning round. If the Round ID is not provided, winnings cannot be credited. This offer applies on Tuesdays only 12:01am PST through 11:59pm PST. Offer only applies to Blackjack Super 7’s. No other Blackjack variation will be taken into consideration.</p>
<br /><br />
<p>Information should be sent to <a href="mailto:VIP@usracing.com">VIP@usracing.com</a></p><br /></div>
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    