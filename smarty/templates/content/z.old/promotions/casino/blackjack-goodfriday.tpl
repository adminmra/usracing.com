{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          


{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      

      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    		

<div class="promos-title"><h2>Good Friday FREE Blackjack Tournament</h2></div>
<div id="promos-content">    
			
<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-blackjack-goodfri.gif" /></div>
			<div class="header">Good Friday FREE Blackjack Tournament</div>        
			<div class="current-info">
<br />
<strong>$1,000 UP FOR GRABS!</strong>
<br />
<br />
Celebrate Good Friday on April 22nd in the Casino by entering our free tournament. Just play the featured tournament games and you're automatically entered into the tournament. There's $1,000.00 up for grabs for the players with the highest payout percentage:
<br />
<br />
<strong>1st</strong> &nbsp;&nbsp;&nbsp; $500<br />
<strong>2nd</strong> &nbsp;&nbsp;&nbsp; $375<br />
<strong>3rd</strong> &nbsp;&nbsp;&nbsp; $125<br />
<br />
This week's tournament games: All Blackjack variations
<br />
<br />

<strong>Terms and conditions:</strong><br />
<ul>
<li>Promotion is open to all active members.</li>
<li>Registration is not required.</li>
<li>To be eligible you must have action on the featured game between 11am and 11pm ET on Friday, April 22nd 2011.</li>
<li>Tournament prizes will be credited within 24 hours.</li>
<li>To qualify for prizes, player must play a minimum 100 rounds.</li>
<li>Blackjack activity in our Poker Software will not count for this tournament.</li>
<li>Tournament prizes awarded based on highest payout percentage.</li>
<li>Standard casino rollover requirements apply.</li>
<li>Games which will not count towards wagering requirements are Craps & Roulette.</li>
<li>We only allow one bonus per account/household or environment where computers are shared.</li>
<li>usracing.com reserves the right to modify or cancel this promotion at any time.</li>
<li><a href="/house-rules">House Rules</a> apply.</li>
</ul>
<br />
<br />

			</div><!-- end: .current-info -->
</div><!-- end: .promo-box -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    