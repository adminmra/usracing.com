{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          


{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    		
    		
            

<div class="promos-title"><h2>CASINO HAPPY HOUR</h2></div>
<div id="promos-content">    
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-happyhour.gif" /></div>
			<div class="header">Casino Happy Hour All Week</div>        
			<div class="current-info">

Get Cash Back in the US Racing Casino. Play any of the featured happy hour casino games each week Monday through Friday between the selected time and If you walk away without winning during this period, we'll give you 10% of your losses back.
<br />
<br />
<strong>This week Happy hour calendar:</strong>
<br />
- Monday between 8-9 pm ET<br />
- Tuesday between 7-8pm ET<br />
- Thursday between 8-9 pm ET<br />
- Friday between 7-8 pm ET<br />
<br />
There's no registration required. Just play the featured casino games within the Casino Happy Hour.
<br /><br />
<strong>Featured Games:</strong>
<br />
Baccarat<br />
Sunken Treasure<br />
Gold Digger<br />
Mr.Vegas<br />
<br />

<a href="/casino"><strong>Play Now!</strong></a>
<br />
<br />
<strong>Terms and Conditions:</strong>

<ul>
<li>Promotion is open to all active members.</li>
<li>Registration is not required.</li>
<li>To be eligible for cash back you must have activity in any of the featured games between the time specified on the promo calendar of each week.
<li>10% Cash Back is based on net losses.</li>
<li>10% Cash Back is capped at $250.00</li>
<li>Cash back will be credited within 24 hours.</li>
<li>Casino rollover requirements apply.</li>
<li>Games which will not count towards wagering requirements are Craps & Roulette.</li>
<li>We only allow one bonus per account/household or environment where computers are shared.</li>
<li>US Racing reserves the right to modify or cancel this promotion at any time.</li>
<li>General <a href="/house-rules">house rules</a> apply.</li>
</ul>

			</div>
          
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    