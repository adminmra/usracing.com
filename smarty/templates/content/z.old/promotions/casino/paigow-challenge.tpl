{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          


{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Pai Gow Challenge</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>Pai Gow Challenge</h2></div>
    
<div id="promos-content">
    
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-paigow.gif" /></div>
			<div class="header">Pai Gow Challenge</div>        
			<div class="current-info">
<p>Turn over a 7-Card Straight Flush while you’re enjoying our Pai Gow Poker game and we’ll pay you $2,500.00.</p>
<br />
<p>Turn over a 7-Card Straight Flush Ace High and we’ll raise the prize to $5,000.00!</p>
<br /><br />
<span class="text-note">
<p>* Offer only applies to the Instant casino. You must provide the Round ID for ANY winning round via email. The email address to claim is <a title="VIP@usracing.com" href="mailto:vip@usracing.com">VIP@usracing.com</a>. If the Round ID is not provided, winnings cannot be credited.</p>
<p>Offer only applies to Pai Gow Poker. No other casino game will be taken into consideration. $2,500.00 prize will be paid at $1,250.00 per month for 2 months. $5,000.00 prize will be paid at $1000.00 per month for 5 months.</p>
<p> Please note, as per company policy, any bonus which is not used within 30 days from the date of credit will expire.</p>
</span><br /></div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    