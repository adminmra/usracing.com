{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>$1,000,000.00 March Madness Perfect Bracket</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>$1,000,000.00 March Madness Perfect Bracket</h2></div>
<div id="promos-content">
<h2>$10,000.00 in Guaranteed Cash Prizes                          </h2>
                            <p>64 teams, 63 games and a huge $1,000,000.00 if you have  what it takes to fill out the perfect March Madness bracket.</p>
							<p>For the sixth year in a row we&rsquo;re putting an eight figure  payout on the line; $1 Million for every year that US Racing has been  taking action on March Madness. </p>
							<p>Pick the Perfect Bracket and you&rsquo;ll never have to work  another day in your life. Miss a few picks and you&rsquo;ll still be playing for  $10,000.00 in Guaranteed Cash Prizes.</p>

							<p>
								Your office pool doesn’t boast a $1 Million Reward or $10,000.00 in Guaranteed Cash Prizes so join ours!</p>
							<p>Submit your picks and place $20.00 in wagers during each round of the tournament to be eligible for the cash prizes.							</p>
							<p>
						If no-one picks a Perfect Bracket you&rsquo;ll be eligible for  some of the $10K in cash prizes that we&rsquo;re giving out to the members that  accumulate the most points throughout the tournament.							</p>
								
								                            <p><strong>Please ensure you make your picks before Thursday, March 18, 2010 at 12:15 PM ET, as all entries after this point will be considered void.</strong></p>

							<p align="center"> <a href="http://brackets.outgard.net/gui/loginForm.php?casinoid=7630&contestid=10"><strong>Click Here To Enter</strong></a>
                            </p>
            
<p>&nbsp;</p>
<p><strong>Terms &amp; Conditions</strong></p>
<ol>
              <li>The March Madness Perfect Bracket Contest starts on Monday, March 15, 2010 and closes on Thursday, March 18, 2010 at 12:15 PM ET. All entries after this point will be considered void and will not be eligible for any of the prizes below. </li>
<li>	In order to be eligible for any prizing, you must correctly enter your Username and Password. If you enter an incorrect Username or Password, your selections will not be properly saved.</li>
<li>	In order to qualify for any winnings, members must wager at least $20.00 on each of the six rounds of the 2010 Men’s NCAA Basketball Tournament. </li>
<li>	The dates of the rounds are as follows: Round 1 (March 18 & 19), Round 2 (March 20 & 21), Round 3 (March 25 & 26), Round 4 (March 27 & 28), Round 5 (April 3), Round 6 (April 5). </li>

<li>	In order to qualify for the $1,000,000.00 prize, all tournament games must selected. </li>
<li>	Our site reserves the right to amend this promotion at any time. </li>
<li>	Only one entry per person, per household is allowed. </li>

</ol>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p><strong>About the Grand Prize</strong></p>
<ol>

									
<li>The Grand Prize is One Million Dollars (US $1,000,000.00) and it will be awarded to the person who correctly predicts every winning team in the Bracket and scores 192 points. </li> 
<li>	The Grand Prize shall be paid in monthly installments of $4,166.66 over a period of 20 years.  </li>
<li>	The Grand Prize must be claimed by the member(s) by midnight EST on April 20, 2010. If the Grand Prize is not claimed within this period, it will be forfeited. </li>
<li>	The first payment will be paid on May 1, 2010 and each subsequent payment shall be paid in exactly one month’s time until all payments are made in full.  </li>
									
									</ol>
									<p>&nbsp;</p>
<p><strong>About the $10,000.00 prize Pool</strong></p>
									
<ol>								
									
					<li>	In addition to the Grand Prize, we will offer a guaranteed prize pool of $10,000.00, which will be awarded to the contestants who accumulate the most points as described below. </li>
<li>If the $1,000,000.00 Grand Prize is awarded, the $10,000.00 Prize Pool will not be awarded. </li>
<li>	If numerous contestants tie, the winnings will be split along with the next lowest prize. For example: If two contestants tie for first with 160 points, they will split the 1st and 2nd Place prizes; the contestant with the next highest point total will win the 3rd Place prize. </li>
<li>	Winnings will be deposited into our member’s account within 72 hours after the completion of the 2010 NCAA Men’s Championship Basketball game.</li> </ol>
<p>&nbsp;</p>
<p><strong>$10,000.00 Prize Pool</strong></p>

				
				
				
				<p>1st Place  -  $5,000.00   |    2nd Place - $3,000.00  | 3rd Place – $2,000.00</p>
				<br>
				
				<p><strong>Point System</strong></p>

				<table width="475" border="0" cellspacing="0" cellpadding="0" class="infoEntries">
	<tr>
		<td>1 point for every correct selection in the 1st Round</td>
	</tr>
	<tr class="AlternateRow">

		<td>2 points for every correct selection in the 2nd Round</td>
	</tr>
	<tr>
		<td>4 points for every correct selection in the 3rd Round (Sweet Sixteen)</td>
	</tr>
	<tr class="AlternateRow">
		<td>8 points for every correct selection in the 4th Round (Elite Eight)</td>

	</tr>
	<tr>
		<td>16 points for every correct selection in the 5th Round (Final Four)</td>
	</tr>
	<tr class="AlternateRow">
		<td>32 points for picking the winner of the Championship Game (Round 6)</td>
	</tr>
</table>

    <br>
</div>

                  

        
        
</div>
              
            
            
                      
        </div>
      
      <!-- end: #col3 -->

      
    