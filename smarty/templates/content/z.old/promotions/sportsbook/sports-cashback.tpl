{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>5% Sports Cash Back</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>5% Sports Cash Back</h2></div>
    
<div id="promos-content">
    
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-5percent.gif" /></div>
			<div class="header">5% Rebate on your Sports wagering losses</div>        
			<div class="current-info">
<p>Receive a 5% rebate on your Sports wagering losses with no limit as to how much money you can be credited every calendar month. Start getting the value you deserve.</p>
<p>But you have got to be in it to win it. <a href="" title="Login">Login</a> and play today!</p><br />
<p class="text-note"> * If you have a losing month wagering on sports you will be credited back 5% of your losses on the 1st day of the following calendar month with no limit as to how much you can be credited.</p></div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    