{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1> 2010 Sweet 16 Perfect Bracket</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>2010 Sweet 16 Perfect Bracket</h2></div>
<div id="promos-content">
 <p>Did you come up a bit short on your Perfect Bracket during the first weekend of March Madness?                          </p>
                            <p>Here's a second chance to fill out a perfect bracket – but this time it is a whole lot easier. Enter our Sweet Sixteen bracket and take home a $1,500.00 prize when the Champion is crowned on Monday, April 5th.</p>
							<p>Simply make your selections and bet $20.00 or more during each of the remaining rounds of the tournament and if your bracket is perfect, we'll put $1,500.00 in your account. </p>
							<p>Sign-up below, make your picks and follow the heart-pounding action as March Madness continues this weekend.</p>

							
							<p align="center"> <a href="http://brackets.outgard.net/gui/contest.php?casinoid=7630&contestid=13"><strong>Click Here To Enter</strong></a>
                            </p>
            
<p>&nbsp;</p>
<p><strong>Terms &amp; Conditions</strong></p>
<ol>
<li>The  Sweet 16 Perfect Bracket Contest starts on Monday March 22, 2010 and closes on Thursday, March 25, 2010 at 7:00 PM ET. All entries after this point will be considered void and will not be eligible for any of the prizes below. </li>
<li>	In order to be eligible for any prizing, you must correctly enter your Username and Password. If you enter an incorrect Username or Password, your selections will not be properly saved.</li>
<li>	In order to qualify for the $1,500.00 Prize, you must wager at least $20.00 on each of the four remaining rounds of the 2010 Men’s NCAA Basketball Tournament. </li>
<li>	The dates of the four remaining rounds are as follows: Round 1 (March 25 and 26) Round 2 (March 27 and 28), Round 3 (April 3), Round 4 (April 5) </li>

<li>	The $1,500.00 Prize will be awarded to the person(s) who fill out the Sweet 16 Bracket perfectly. Perfectly is described as correctly picking the winner of the 15 games from the Sweet 16 through the Championship game. </li>
<li>	If numerous people correctly fill out a perfect bracket, the $1,500.00 will be split up evenly amongst the winning members. </li>
<li>	If no one fills out a perfect bracket, the $1,500.00 will not be awarded. </li>
<li>Winnings will be deposited into the member(s) account within 72 hours after the completion of the Championship game on April 5, 2010.</li>
<li>There are no rollover requirements associated with the $1,500.00 Prize.</li>
<li>Only one entry per person per household is allowed.</li>
<li>We reserve the right to amend this promotion at any time.</li>
<li>Selections may not be changed once they have been submitted.</li>

</ol>
<p>&nbsp;</p>

<p>&nbsp;</p>
    <br>
</div>

                  

        
        
</div>
              
            
            
                      
        </div>
      
      <!-- end: #col3 -->

      
    