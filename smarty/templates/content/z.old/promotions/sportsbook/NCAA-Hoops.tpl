
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">








<div class="promos-title"><h2>NCAA Promotions</h2></div>
<div id="promos-content">

<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-basketball-finalfour.gif" /></div>
			<div class="header">NCAA Hoops Final Four Insurance</div>        
			<div class="current-info">
			<p>The March to Madness is on at US Racing. College hoops is officially back for another action-packed season and we have something for all of our loyal basketball members.</p>		
			<p>Wager now on our 2010/11 NCAA Men's NCAA Basketball Championship odds. If the team you bet on makes it to the Final Four, but fails to win-it-all, your bets will be refunded up to $100.00. </p>		
			<p>Who do you think will cut down the nets on April 4th? </p>		
			<p>The <strong>Duke Blue Devils</strong> are the +350 favorites to win their second straight championship. Or maybe you are looking for more of a long-shot like the Florida Gators (+2500) and their five returning starters. See <a href="/sportsbook" >Odds</a>.</p>		
			<p>This generous offer doesn't last long. To qualify for this promotion, you must place your bet between Friday, November 12, 2010 at 9 AM ET and Friday, November 19, 2010 at 11:59 PM ET. <a  href="/sportsbook"><strong>Bet Now</strong></a>!</p>	
			<br />
			<p><strong>Terms &amp; Conditions</strong></p>
<ol>
  <li>To qualify for the NCAA Hoops Final Four Insurance refund, member must place a minimum $10.00 wager on the '2011 NCAA Basketball Championship' odds.</li>
  <li>To qualify for a refund, bets must be placed between <strong>Friday, November 12, 2010</strong> at <strong>9 AM ET</strong> and <strong>Friday, November 19, 2010</strong> at <strong>11:59 PM ET</strong>.</li>
  <li> Refund will be credited within 24 hours after the completion of the 2011 Men's NCAA Basketball Championship game.</li>
  <li>Maximum refund for this promotion is $100.00.</li>
  <li>Standard rollover requirements apply to all refunds.</li>
  <li>  <a href="/house-rules" style="color: rgb(78, 80, 75); font-weight: bold; text-decoration: underline;" target="_blank">General House Rules</a>  apply.</li>
</ol></div>          
			</div>           
            
</div><!-- end:promos-content -->




</div>

<!-- end:content-wrapper -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
