{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>NHL - Promotions</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>NHL - Promotions</h2></div>
<div id="promos-content">

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlgrandsalami.gif" /></div>	
			<div class="header">NHL Grand Salami</div>        
			<div class="current-info">
<p>Think you know the NHL? If so then you can't miss out on our NHL Grand Salami where all you have to do is wager on the total amount of goals scored by all NHL teams combined and select the over or under, it's that easy! ( <span class="text-note">All Games Must Go Official Time</span> )</p></div>          
			</div>
            
            <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlfirstperiod.gif" /></div>	
			<div class="header">1st Period Line & Total</div>        
			<div class="current-info">
<p>Others may offer the game but at US Racing we give you more ways to win, bet all NHL games 1st period line and total so you can get off to an early start and avoid overtime.</p></div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nhlprops.gif" /></div>
			<div class="header">Game & Player Props</div>        
			<div class="current-info">
<p>Ever get the feeling that a player is going to light it up one night or that a team might fall flat? If you have then check out all our Game and Player Props that will challenge even the most diehard hockey fan.</p></div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    