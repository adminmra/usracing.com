{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Sportsbook Promotions</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>NFL &#38; NCAA Football - Promotions</h2></div>
    
<div id="promos-content">
    
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-10percent.gif" /></div>
			<div class="header">10&#37; Initial Deposit Bonus</div>        
			<div class="current-info">Get started today and enjoy 10% FREE CASH instantly when you make your first deposit!</div>          
			</div>

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-5percent.gif" /></div>
			<div class="header">5&#37; FREE CASH Rebate on Sports Action</div>        
			<div class="current-info">Have a tough month? Get 5% cash back on all Sports and Casino losses every month with no limits. Sports and Casino rebates are credited on the 1<sup>st</sup> day of the following month year round.</div>          
			</div>
            
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-dollar.gif" /></div>
			<div class="header">Double Your Money Mondays</div>        
			<div class="current-info">Double your winnings on Monday Night Football games throughout the year, we will have regular contests where you can double your winnings on selected games. Best part about it is that the chance to Double your $$$ is for FREE! To win, all you have to do is place your bets and guess the final score of the game. Get it right and we'll double the winnings for your wager on the Monday Night Game!
<br /><br />
<strong>**DYM maximum bonus amount is capped at $1,000**</strong></div>          
			</div>
  
   
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-juiced.gif" /></div>
			<div class="header">NFL Juiceless prop Sundays</div>        
			<div class="current-info">Everyone likes to have the edge so we are doubling our Juiceless Game of the week promotion and giving you back to back days with a juice free wager! EVERY SUNDAY we will pick one NFL Prop bet and offer that bet at even odds, lay $100 to win $100. No Juice! This special will run throughout the entire regular football season.</div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-5percent.gif" /></div>
			<div class="header">Bet on Football games on Friday at Nickel Odds</div>        
			<div class="current-info">Get your weekend bets in on Friday at HALF PRICE. Nickel Odds means you only pay 5% juice to bet the games bet less to win more. Combined with your 10% sign-up bonus and you have the edge over the house. We dare you to do the math. That means on select Friday NFL games you only have to lay $105 down to win $100. Every Friday we run selected Football odds at Half Price (-105). Friday - 3pm to 10pm EST.</div>          
			</div>


                        <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nfl-shutoutrefund.gif" /></div>
			<div class="header">NFL Shutout Refund</div>        
			<div class="current-info">If your NFL team is shutout, we will refund ALL Money Line Bets. It's that simple, a shutout means a FULL refund! 
                        <br /><br  /><strong>Note: </strong>Money line wagers on the game itself only; half time wagers, props and parlays are not included in the promotion. Promotion applies to Thursday and Monday night games only. Wagers must be placed the week of the game.</div>          
			</div>

            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-juiced.gif" /></div>	
			<div class="header">NCAA Juiceless Game of the week Every week</div>        
			<div class="current-info">
Every Saturday, we will pick one afternoon nationally televised game and offer the game at even odds, lay $100 to win $100. No Juice! What better way to kill a Saturday afternoon and make some quick cash.
<br /><br />
<b>Terms &amp; Conditions:</b>
<br />
<b>General</b>
<ul class="numberedlist">
<li>Make your pick before the first game of the week</li>
<li>This promotion is open to all active US Racing members.</li>
<li>You must register with your CID and E-mail to qualify. If you do not enter your correct CID and E-mail, your picks will not be properly saved and you will not be eligible for any monetary prizing.</li>
<li>To qualify for this promotion, you must play a minimum of $50.00 on the Casino games</li>

<li>Rollover requirement does apply</li>
<li>US Racing reserves the right to alter or amend the terms and conditions of this promotion without notice</li>
<li><a href="houserules.php">General house rules apply.</a></li>
</ul></div>          
			</div>

                                   
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-plusminus.gif" /></div>
			<div class="header">Buy points online for all football games</div>        
			<div class="current-info">
If you don't like the point spread we are offering, simply change it. Move the Patriots from -4 to -3 with a few clicks of the mouse. Betting could not be any easier than with usracing.com</div>
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-ifbets.gif" /></div>
			<div class="header">If Bets</div>        
			<div class="current-info">
On an NFL Sunday there is just minutes between the morning games and the afternoon games. How do you get your second bet down? US Racing has just the solution for you, If Bets. Its a simple bet in which you wager a fixed amount on one team and if that team wins, then the same amount as your original wager is placed on a different team. Now you never have to miss another game!</div>          
			</div>            
                      
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-parlay.gif" /></div>
			<div class="header">Progressive Parlays</div>
			<div class="current-info">
Keep you in it even with a loss. Play an 8 team parlay and if 6, 7, or 8 teams win, you win. Play a 4 team parlay, if 3 or 4 teams win, you win. Even if you don't win all the games in your Progressive Parlay, you can Still Get Paid!</div>          
			</div>


                        <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-dollar.gif" /></div>
			<div class="header">Daily Money Back Specials</div>        
			<div class="current-info">Every day we will offer one game or prop to wager on and you can double your winnings up to $50 or even be refunded $50 for a losing bet. Check out our Sports Betting menu for today's Daily Money Back Special posted daily in the "Hot Promotions" section.</div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    