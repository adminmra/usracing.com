{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>MLB Opening Day Gateway</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>MLB Opening Day Gateway</h2></div>
<div id="promos-content">

 <p>
		<p>You pick the park, we'll provide the tickets.<br>
<br>
Wherever you want to spend Opening Day, US Racing will get you there with airfare, accommodations, cash, and, of course, two tickets.<br>
<br>
Valued at $3,000.00, all you have to do to win is wager on Spring Training Baseball.</p>

		  </p><br>




<p><strong>Here’s how to win:</strong></p><br>
<ul>
<li>Wager on Spring Training Baseball. </li>
<li>For Every $10.00 wagered, we'll give you one entry into a random draw. </li>
<li>Draw will take place on the morning of Monday, March 29th, 2010. </li>
</ul>
<br>
{literal}<script language="javascript">
			function checkForm()
			{
				if (document.getElementById("_01_CID").value == "")
				{
					alert("Please enter your username.");
					return false;
				}
				if (document.getElementById("_02_EMAIL").value == "")
				{
					alert("Please enter your email.");
					return false;
				}
				return true;
			}
			
			
			function opencid()
			{
				mywindow=window.open("/popups/cid.html","mywindow","location=0,status=0,scrollbars=0, width=230,height=130");
				mywindow.moveTo(350,450);
			}
		</script>{/literal}
<form action="http://tools.outgard.net/php/promoFiles/writeforminfonewformat.php" method="post" name="SBmlbOpening" id="SBmlbOpening" onsubmit="return checkForm();">
					<input type="hidden" value="http://www.usracing.com/promotions/sportsbook/thank-you-MLB" name="redirect" />
					<input type="hidden" value="SBmlbOpening" name="fname" />
					<input type="hidden" value="," name="fdelimiter" />
					<input type="hidden" value="usracing.com" name="site" />
					<input type="hidden" value="SBmlbOpening" name="contestid" />
                    <input type="hidden" name="from" value="vip@usracing.com" /> 
					<table cellpadding="5" cellspacing="2">
					<tr>
					<td><strong>Registration Form</strong></td>
					</tr>
					<tr>
					<td><table align="center" border="0" cellpadding="5" cellspacing="2" width="460" id="infoEntries">
                        <tr>
<td> *Enter your CID: </td>
  <td><input name="_01_CID" type="text" id="_01_CID" value="" maxlength="15"/>&nbsp;&nbsp;<a href="javascript:opencid()">Forgot CID</a>    </td>
</tr>
<tr class="odd">
  <td>*Enter your Email:<br /></td>
  <td><input name="_02_EMAIL" id="_02_EMAIL" type="text" value="" /></td>
</tr>
<tr>
  <td colspan="2">
  <div align="center">
    <input type="submit" name="button" id="button" value="Submit" />
  </div></td>
</tr>
                    </table>					  </td></tr>
					</table>
			  </form>
            
<p>&nbsp;</p>
<p><strong>Terms &amp; Conditions</strong></p><br>
<ol>
                <li>	Registration is required to win the Opening Day Prize Pack.</li>

                <li>	You must enter your correct CID and Email on the promo page to qualify. If you enter the incorrect information, your action will not be properly tracked.</li>
                <li>	The winner of the Prize Pack will be awarded by a random drawing conducted on Monday, March 29th, 2010.</li>
                <li>	For every $10.00 you wager on MLB Spring Training games you will earn one entry for the random drawing for the Spring Training Prize Pack.</li>
                <li>	The money you wager will be rounded down to the next lowest denomination of $10 to determine your final number of entries. For example, if you wagered $99 on Spring Training games, you will earn 9 entries.</li>
                <li>	Prize Pack is valued at $3,000.00.</li>

                <li>	Prize Pack includes: (2) Tickets to an Opening Day game of your choice, (2) Roundtrip Airline Tickets if needed, Hotel Accommodations for three nights and $500.00 worth of spending cash.</li>
                <li>	If US Racing is unable to make appropriate travel arrangements, the $3,000.00 will be deposited into your account.</li>
                <li> 	US Racing reserves the right to amend or change this promotion at anytime. </li>
                <li> 	<strong><a href="/house-rules" style="color: rgb(78, 80, 75); font-weight: bold; text-decoration: underline;" target="_blank">General house rules apply. </a></strong> </li>
</ol>
<p>&nbsp;</p>
<br>
</div>

        
        
</div>
              
            
            
                      
        </div>
      
      <!-- end: #col3 -->

      
    