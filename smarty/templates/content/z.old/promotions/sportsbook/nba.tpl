{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            


      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">








<div class="promos-title"><h2>NBA - Promotions</h2></div>
<div id="promos-content">

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-10percent.gif" /></div>	
			<div class="header">Lay Less on Monday NBA Sides</div>        
			<div class="current-info">
<p>Every Monday, all our NBA sides are at Half Price. Lay only -105 when you bet the games. Combined with your 10% sign-up bonus and you have the edge over the house. We dare you to do the math.</p></div>          
			</div>
            
            <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-5percent.gif" /></div>	
			<div class="header">Get Paid to Play</div>        
			<div class="current-info">
<p>Have a tough month? We will give you a 5% rebate on your losses with no limit as to how much money you can be credited. Start getting the value you deserve.
But you have got to be in it to win it. Login and play today.</p></div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-parlay.gif" /></div>
			<div class="header">Progressive Parlays</div>        
			<div class="current-info">
<p>Progressive Parlays keep you in it even with a loss. Play an 8 team parlay and if 6, 7, or 8 teams win, you win. Play a 4 team parlay, if 3 or 4 teams win, you win. Even if you don't win all the games in your Progressive Parlay, you can Still Get Paid!</p></div>          
			</div>

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-ifbets.gif" /></div>
			<div class="header">If Bets</div>        
			<div class="current-info">
<p>If Bets are a simple bet in which you wager a fixed amount on one team and if that team wins, then the same amount as your original wager is placed on a different team. Now you never have to miss another game!</p></div>          
			</div>
            

</div><!-- end:promos-content -->



  
</div>


<!-- end:content-wrapper-->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      

