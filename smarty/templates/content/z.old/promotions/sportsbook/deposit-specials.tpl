{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      

      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Deposit Specials</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>Deposit Specials</h2></div>
    
<div id="promos-content">

<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-20percent.gif" /></div>
			<div class="header">10% Cash bonus</div>        
			<div class="current-info">
      <p>All new members recieve a 10% cash bonus on their initial deposit with no caps or limits. Others claim to pay more but read the fine print. Nobody has a more generous bonus policy than US Racing. Join Today!</p>
      </div> 
</div>

<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-5percent.gif" /></div>
			<div class="header">5% Free cash award</div>        
			<div class="current-info"><p>Get 5% free cash awarded to all your deposits every day at US Racing. Others restrict bonuses to a single day or offer them on their terms, we just give you want you want, when you want it with no limits!</p></div> 
</div>

<!-- Start Poker Reload Bonus -->    
<!--			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-poker-reload.gif" /></div>
			<div class="header">100% Poker Reload Bonus: <span class="red">September 2nd - 9th</span></div>        
			<div class="current-info">
<strong>$600 Poker Sign-up Bonus!</strong>
<p>Get a 100% Deposit Bonus on your first deposit up to $600 in three easy steps!</p>

<p>
1. <a href="/poker" title="Download Poker Now!"><strong>Download our poker software.</strong></a><br />
2. Make a real money purchase.<br />
3. Use Poker Promo Code: EPP100
</p>

<p>
<strong>How do I earn out my bonus money? Get PAID to Play!</strong>

When you have bonus money sitting in your pending bonus balance you  can earn it back by playing in real money ring or tournament games.
<br />
<br />
When you play for real money you&rsquo;ll earn <strong>Frequent Player Points (FPP)</strong>.  Each FPP that you earn is worth $0.06 towards your bonus. Once you&rsquo;ve  earned $10 worth of bonus money, the funds are removed from your  Pending Bonus account and added to your Everybody Plays regular balance.  For more details on FPPs please see below.
<br />
<br />
This will continue until your pending bonus account reaches $0. Note  that your last payout could be less than $10 if the remaining amount in  your pending bonus is less than $10.
<br />
<br />
Please note that you only have 90 days (dated from the creation of  the Pending Bonus) to earn back that bonus. Otherwise, you will lose  your bonus.
</p>

<br />
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="data" title="Poker Bonus" summary="Poker Bonus:  How do I earn Frequent Player Points">
<tr>
<th colspan="2">How do I earn Frequent Player Points (FPP)</th>
</tr>
<tr>
<td valign="top"><img src="/themes/images/poker-bonus-real.gif" alt="Frequent Player Point" width="89" height="43"></td>
<td valign="top">
<strong>Real Money Ring Games:</strong> For each dollar that is raked at a table where you are dealt cards, you  will earn one Frequent Player Point (FPP). We are accurate to the  penny, so if your table was raked $1.25, you will receive 1.25 FPP or  if your table was raked $3.00, you will receive 3 FPP. Each FPP is  worth $.06 towards your bonus.
<br />
<br />
</td>
</tr>
<tr class="odd">
<td valign="top"><img src="/themes/images/poker-bonus-tourneys.gif" alt="Real Money Tournaments" width="89" height="43"></td>
<td valign="top">
<strong>Real Money Tournaments:</strong> For each dollar worth of tournament  fees you pay, you&rsquo;ll earn 7 Frequent Player Points (FPP) for them once  you&rsquo;re done in the tournament. Just like Real Money Games, we are  accurate to the penny, so for a $0.50 tournament fee, you will receive  3.5 FPP. </td>
</tr>
</table>
<br />
<br />
<strong>Additional Bonuses</strong>
<br />
If you are currently earning a bonus and have the opportunity to  receive another bonus (reload bonus), we&rsquo;ll not only add the bonus  amount to your current pending bonus balance, but we will give you  another 90 days to clear the entire bonus amount from your new total.<br /><br />
<strong>Bonus Condition</strong>
<br />
Everybody Plays  Poker offers a generous first time deposit bonus as well  as reload bonuses to reward our loyal players. As the nature of these  bonuses is to reward players when they deposit additional funds,  players who withdraw funds during a bonus period will have the amount  of their withdrawal subtracted from their buy-in amount.The player  will receive a bonus on the remaining balance. If the balance is  negative no bonus will be awarded.
<br />
<br />      
                        </div> 
			</div>
-->
<!-- End Poker Reload Bonus -->    

<!--<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-dollar.gif" /></div>
			<div class="header">Deposit $25 or more and get $25 or 10% of your deposit in FREE CASH</div>        
			<div class="current-info"><p>Join Now and make an initial deposit of $25 or more and receive $25 free. If you deposit $251 or more you will receive 10% Free Cash</p>
            <br/>
            <br/>
            <p><strong>Note:</strong> This promotion is for new members only between November 1st - 7th</p></div> 
</div>-->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    