{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Major League Baseball Promotions</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="promos-title"><h2>MLB - Promotions</h2></div>
<div id="promos-content">

			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-10percent.gif" /></div>	
			<div class="header">True Dime Lines on Baseball</div>        
			<div class="current-info">
<p>Nobody lets you bet baseball for less juice. Reduced Juice every day means you'll collect more cash when you win, and get increased odds when you parlay your picks at usracing.com. With Dime Lines offered on most games, MLB bettors get the most value at usracing.com</p></div>          
			</div>
            
            <div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-nohitter.gif" /></div>	
			<div class="header">MLB No-Hitter</div>        
			<div class="current-info">
<p>If there is a No Hitter in a MLB game that you wagered on, we will refund back to your account any wagers that you may have lost on that game.We refund all losing wagers on the Money Line or Run Line of the game in question. Totals, parlays, ifbets, proposition and future wagers not included in this promotion. Applies to Regular Season Games only. Cycle Hitters and No Hitters as defined by MLB rules. Refunds will be posted to your account within 24 hours..</p></div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-cyclehitter.gif" /></div>
			<div class="header">Hit for the Cycle</div>        
			<div class="current-info">
<p>If there is a Cycle Hitter in a MLB game that you wagered on, we will refund back to your account any wagers that you may have lost on that game. We refund all losing wagers on the Money Line or Run Line of the game in question. Totals, parlays, ifbets, proposition and future wagers not included in this promotion. Applies to Regular Season Games only. Cycle Hitters and No Hitters as defined by MLB rules. Refunds will be posted to your account within 24 hours.</p></div>          
			</div>
            
			<div class="promos-box">
			<div class="icon"><img src="/themes/images/promo-under.gif" /></div>	
			<div class="header">Baseball Slaughter Rule</div>        
			<div class="current-info">
<p>Your team get slaughtered? We refund all losing wagers for those who bet on the Under of the Total. Money Lines, Run Lines, parlays, ifbets, proposition and future wagers not included in this promotion. Applies to Regular Season Games only. Refunds will be posted to your account within 24 hours.</p></div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-refund.gif" /></div>	
			<div class="header">Futures Grand Slam</div>        
			<div class="current-info">
<p>We refund all losing wagers for those who bet on the Under of the Total. Money Lines, Run Lines, parlays, ifbets, proposition and future wagers not included in this promotion. Applies to Regular Season Games only. Refunds will be posted to your account within 24 hours.</p>	</div>          
			</div>
            
			<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-4x.gif" /></div>	
			<div class="header">Futures Grand Slam</div>        
			<div class="current-info">
<p>This is your chance to Quadruple the Winnings of Four Futures Wagers. Enter our contest and bet on the four futures to win. If each of your selections win, your payout will be multiplied by four.</p></div>          
			</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    