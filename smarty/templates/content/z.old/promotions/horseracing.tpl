
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->




{include file='menus/promotions.tpl'}
{include file='menus/help.tpl'}





<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      

      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">








<div class="promos-title"><h2>Horseracing - Promotions</h2></div>
    
<div id="promos-content">



<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-freebet.gif" /></div>
			<div class="header">FREE $5 Belmont Stakes Bet</div>        
			<div class="current-info">
<p>Make a free horse wager for the 2011 Belmont Stakes on June 11th, 2011</p>
<p>US Racing is offering you a chance to win <b>FREE MONEY</b> with a <a href="/triplecrown/freebet"><b>FREE TRIPLE CROWN BET</b></a> for each of the Triple Crown races.</p>
<p><a href="/triplecrown/freebet">Click Here for full details.</a></p>
			</div><!-- end: .current-info --> 
</div><!-- end: .promo-box -->



<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-horseracing-8percent.gif" /></div>
			<div class="header">8% Horse Rebate</div>        
			<div class="current-info">
<p>It doesn't matter if you win or lose, here at US Racing we will credit you a 8% rebate  on Exotics every day based on how much you wager. What are you waiting for, play today and also receive your 3% rebate for all Win, Place and show wagers; afterall, there is nothing better than a sure thing!</p>
<br />
<ul class="star">
<li>We pay out Full Track Odds on Win, Place and Show bets and we pay out a maximum 5000-1 on Pick 4's at class A tracks and up to $20,000.00 per event.</li>
<li>We offer real time wagering with more than 100 great tracks to choose from.</li>
<li>Our grading of horses is the fastest in the industry.</li>
<li>No betting limits means you can win as much as you want.</li>
<li>Bet now and win, easy signup, no fees, insant deposits.</li>
</ul>

<p>Here at US Racing we offer a great variety of Wager types like: Win, Place, Show, Daily Doubles, Exactas, Quinellas, Trifectas, Superfectas, Pick 3, and Pick 4's. Covering all your daily wagering and the most extensive Triple Crown coverage and wagering options that can be found period!</p>
<br />
<p class="text-note">The 8% & 3% rebate programs only apply to bets placed directly with the horse wagering interface. It does not apply to matchups or other wagers made through the sportsbook login.</p>
			</div><!-- end: .current-info --> 
</div><!-- end: .promo-box -->


<!--<div class="promos-box">			
			<div class="icon"><img src="/themes/images/promo-dollar.gif" /></div>
			<div class="header">Double Your Winnings on 2 Races!</div>        
			<div class="current-info">
<p>This Breeders' Cup Weekend when you bet on either of the following races you can instantly Double Your Winnings while watching the races LIVE!</p>
<br />
{literal}<style type="text/css">
#infoEntries th { padding: 4px 6px; font-size:11px; line-height: 11px; text-align:center; }
#infoEntries td { padding: 7px; line-height: 12px; text-align:center; }
</style>{/literal}
			<table id="infoEntries" width="100%" border="0" cellspacing="0" cellpadding="0">
  			<tr>
    		<th>RACE</th>
    		<th>PURSE</th>
    		<th>DISTANCE</th>
    		<th>DATE</th>
    		<th>POST (ET)</th>
    		<th>TV</th>
  			</tr>
  			<tr>
            <td style="text-align:left;"><script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'horsefutures')+'" title="Breeders\' Cup Marathon"><strong>Breeders\' Cup Marathon</strong></a> (GIII)');</script></td>
    		<td>$500K</td>
    		<td>1 3/4 miles</td>
    		<td>Nov. 5th</td>
    		<td>4:10pm</td>
    		<td>ESPN2</td>
  			</tr>
  			<tr>
            <td style="text-align:left;"><script language="javascript">document.write('<a href="/'+((logged_in())?'sportsbook':'horsefutures')+'" title="Breeders\' Cup Juvenile Turf"><strong>Breeders\' Cup Juvenile Turf</strong></a> (GII)');</script></td>
    		<td>$1M</td>
    		<td>1 Mile (T)</td>
    		<td>Nov. 6th</td>
    		<td>1:50pm</td>
    		<td>ABC</td>
  			</tr>
			</table>
<br />
<b>Terms and Conditions:</b><br />
The 100% Win Bonus is on 'Win' bets only placed in the Racebook on the race listed above.
<br />
<br />
<ol>
<li>To qualify for the 100% Win Bonus, the 'Odds to Win' wager must pay out 2-1 or greater.</li>
<li>The 100% Win Bonus will be credited within 48 hours.</li>
<li>Standard rollover terms apply. </li>
<li><strong>usracing.com</strong> reserves the right to cancel or amend this promotion at any time. </li>
<li>Maximum Win Bonus per single winning wager is $10.00</li>
<li><a href="house-rules"><strong>House Rules</strong></a> apply.</li>
</ol>

			</div>
</div>--><!-- end: .promo-box -->




</div><!-- end: #promos-content -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 





