{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Why Us?</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/welcome" title="">WELCOME</a></li>
<li><a href="/learnmore" title="">LEARN MORE</a></li>
<li><a href="/seedifference" title="">SEE THE DIFFERENCE</a></li>
<li><a href="/racebookreviews" title="">COMPARE US</a></li>
<li class="leaf active-trail"><a href="/readreviews" title="" class="active">READ REVIEWS</a></li>
<li><a href="/betwithconfidence" title="">Setting the Pace</a></li>
<li><a href="/bestracebook" title="">ABOUT US Racing</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Read about US Racing in the News</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table><tbody><tr><td valign="top">

<table cellspacing="0" cellpadding="0" width="685" border="0"><tbody><tr><td valign="top" rowspan="3"><img src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_1.jpg" /></td>
<td valign="top" height="321" rowspan="3">

<table height="321" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
<td valign="top"><img src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_2.jpg" /></td>
</tr>
<tr>
<td valign="top" width="200" height="242"><h5>"If you wager online, your first stop should be US Racing." -- Maxim Magazine</h5><h5>"A++. . . A great gaming destination." --- Gambler's Guide</h5><h5>"US Racing makes online betting easy." -- Details Magazine</h5><h5>"Horse racing made simple, safe and fun." --- Miami Herald</h5><h5>"A horse racing site that doesn't charge a monthly fee." -- Real Deal Sports</h5></td>
</tr>
<tr>
<td valign="top"><img title="" alt="" src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_3.jpg" width="290" border="0" /></td>
</tr>
</tbody>
</table>
</td>

<td valign="top" rowspan="3"><img src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_4.jpg" /></td><td valign="top"><img src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_6.jpg" /></td>
<td valign="top" rowspan="2"><img src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_7.jpg" /></td>
</tr>
<tr>
<td valign="top"><h5>"Betting on horses always seemed so difficult--it is fun and I have actually made $3,000 in two months!"  -- George H</h5><h5>"Excellent service--they really care and you can tell.  I recommend it to my friends." -- Sam F</h5><h5>"Casino jackpots and sportsbook are the best I've played." -- Gwen D</h5></td>
</tr>
<tr>
<td valign="top" colspan="2"><a href="/join"><img style="DISPLAY: block" src="/media/images/content/allhorseracingcom/Sub_Headers/Why_Us/review_8.jpg" border="0" /></a></td>
</tr>
</tbody>
</table></td>
</tr>
<tr>

<td valign="top" bgcolor="#105ca9">

<table cellspacing="0" cellpadding="0" border="0">
			<tbody>
			<tr>
			<td width="48" bgcolor="#105ca9">&nbsp;</td>
            <td><a title="Be online for less" href="/join"><img src="/themes/images/whyus/whyus_betonlineforless.gif" width="176" height="36" border="0" /></a></td>
			<td width="30" bgcolor="#105ca9">&nbsp;</td><td><a title="Any Dollar Amount" href="/join"><img src="/themes/images/whyus/whyus_anydollaramount.gif" width="176" height="36" border="0" /></a></td>
    
			<td width="30" bgcolor="#105ca9">&nbsp;</td><td><a title="Free Account" href="/join"><img src="/themes/images/whyus/whyus_freeaccount.gif" width="176" height="36" border="0" /></a></td>
            <td width="47" bgcolor="#105ca9">&nbsp;</td>
			</tr>
			</tbody>
			</table>



</td></tr></tbody></table>


<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left"><img title="Online Horse Racing - Best of Baltimore" alt="Online Horse Racing - Best of Baltimore" src="/media/images/content/allhorseracingcom/Pages/Baltimore.jpg" border="0" /></td>
    <td align="center"><img title="Bet the Ponies - Austin Magazine" alt="Bet the Ponies - Austin Magazine" src="/media/images/content/allhorseracingcom/Pages/Austin.jpg" border="0" /></td>
    <td align="center"><img title="Where Texas Bets on Horses - Houston Magazine" alt="Where Texas Bets on Horses - Houston Magazine" src="/media/images/content/allhorseracingcom/Pages/Houston.jpg" border="0" /></td>
    <td align="right"><img title="Setting the Pace and Online Horse Racing" alt="Setting the Pace and Online Horse Racing" src="/media/images/content/allhorseracingcom/Pages/Louisville.jpg" border="0" /></td>
  </tr>
</table>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    