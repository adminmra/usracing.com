<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div class="homeIntro" style="height: 196px;">
	<div class="quote"><span style="float: left;">"<strong>If you wager online, your first stop should be US Racing</strong>"</span> <img style="float: right; display: block; margin-top: 3px;" src="/themes/images/maxim-logo.gif" alt="Maxim" width="51" /></div>
	<div class="copy first" style="clear: right;">
	<p><a title="Horse Racing" href="/"><strong>US Racing</strong></a> is one of the leading websites for horse betting and is regularly featured in <a href="readreviews">magazines and newspapers</a>.</p>
    
	<p><a href="join">Free membership</a> allows you to <a href="racebook">bet on horses</a> from home. Just like <a title="Offtrack betting" href="otb">offtrack betting</a> and <a title="OTB" href="otb">OTB</a>.&nbsp; Get 8% <a title="Rebates for Horse Racing" href="rebates">rebates</a> on your horse wagers, too.</p>
	<p>Besides the best in daily racing, US Racing offers exciting games with mega jackpots.&nbsp; 2011 <a href="/kentuckyderby-odds" title="2011 Kentucky Derby Odds">Kentucky Derby Odds</a> are now available!</p>
	</div>
    
	<div class="copy">
	<p>You will also be entitled to great monthly rebates such as 5% cash back on <a href="/sportsbook">sports betting</a> and <a href="/virtualderby">virtual horse racing</a> and 50% <a href="/casino">Casino</a> cash back.</p>
	<p>With <a title="Horse Races" href="beton/race-of-the-week">Races of the Week</a> you can double your money.&nbsp; Try your luck at our <a title="3D Virtual Racebook" href="virtualderby">3D Virtual Racebook</a> - stunning graphics and sound!</p>

	<p>Over 100 <a href="racebook">racetracks</a> to select from around the world including <a title="Churchill Downs Racecourse" href="/churchilldowns" target="_self">Churchill Downs</a>.&nbsp; So what are you waiting for?&nbsp; <a title="Set up Free Horse Betting Account" href="join">Join today</a> and Setting the Pace.</p>
	</div>

</div>
</div>

  

<!-- /block-inner, /block -->






  


		
                                      
          
<div class="headline"><h1>Home</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
 

 

