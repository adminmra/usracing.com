

      
      



      

<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>US Racing | Kentucky Derby News</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="article">

<div class="InfoRibbon">
<span class="Source">Daily Racing News</span><!-- SOURCE -->
<span class="ShortDate">5/20/2011</span></div>

<!-- AddThis Button BEGIN -->
<div style="display: block; height: 16px; width:125px; padding:0; margin-top: 6px;" >
<a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b7342703113cb61" rel="nofollow"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b7342703113cb61"></script>
</div>
<!-- AddThis Button END -->


<div class="Author">Greg Melikov</div><!-- AUTHOR -->
<div class="LongDate">Friday, May 20th, 2011</div>
<!-- DATE - LONG -->

<div class="inner">
<div class="body"><!-- ARTICLE BODY -->


<p>The 136th Preakness: Derby Runners Hold Edge<br />
  By GREG MELIKOV</p>
<p>This is the third time during the past two decades that the Preakness has a full field – 14 horses going 1 3/16 miles at Pimlico on Saturday</p>
<p>Both times the favorites in the second leg of the Triple Crown bounced back from defeats in the Kentucky Derby. Back in 1992, Pine Bluff, who finished off the board in ther Derby, won the Preakness by three-quarters of a length.</p>
<p>In ’05 Afleet Alex captured the Preakness by 4 ¾ lengths after finishing third at Churchill Downs. He ran third to longshot Giacomo in the Derby. Ironically, Giacomo finished third at Pimlico. </p>
<p>The number of Derby horses in the Preakness has averaged a bit more than five the past 25 years. This go-round there is five again, including Derby winner Animal Kingdom, the 2-1 morning line favorite that breks from post 11.</p>
<p>The others include defeated Derby favorite Dialed In who drew post 10 and was made the early 9-2 second choice. </p>
<p>The son of Mineshaft was squeezed back at the start of the Derby and trailed the 19-horse field 20 lengths behind Shackleford.<br />
  The Nick  Zito trainee closed strongly to finish eighth, beaten less than eight lengths covering the final half-mile in an amazing 47 seconds..</p>
<p>“You just can’t win in those situations, but he came with his run,” Zito said. “It was probably the best eighth-place finish in Kentucky Derby history.”</p>
<p>If Dialed In can rebound and triumph, he would canter away with the new Preakness 5.5 bonus program. Because he captured the Holy Bull and the Florida Derby this season at Gulfstream Park, he would rake in a grand total of $6.1 million. </p>
<p>Dialed In would earn the $600,000 winner's share of the $1 million Preakness purse, owner Robert LaPenta would collect $5 million and Zito would receive $500,000 as the winning trainer.</p>
<p>I’m going to use several Derby horses in exotic wagers, especially the lightly raced Animal Kingdom, who is only running for the sixth time. It appears the son of Leroidesanimous can run on any surface.<br />
  I’m including Dialed In because I recall Lookin at Lucky had a not-so-good trip in last year’s Derby, finishing sixth, but won the Preakness.</p>
<p>Shackleford, who led most of the way in this year’s Derby before finishing fourth, will be on some exotic tickets. That’s because the son of Forestry, who breaks from post 5 and is 12-1 in the early odds, reminds me of Louis Quatorze.</p>
<p>The son of Sovereign Dancer finished 16th to Grindstone in the ’96 Derby. But in the Preakness he led at every call. I had him to win. He was 8 ½-1. </p>
<p>Third in the Derby, Mucho Macho Man, seldom runs a bad race, hitting the board eight of times. The son of Macho Uno is 6-1 and breaks from post 9.<br />
</p>
<p><br />
   <br />
   <br />
   </p>
<p>&nbsp;</p>
</div><!-- END ARTICLE BODY -->
</div><!-- END .inner -->

</div><!-- END .article -->











  
</div><!-- END .content -->

<!-- END #content-box -->

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


<!-- end: #main-inner -->
 <!-- end: #main -->

