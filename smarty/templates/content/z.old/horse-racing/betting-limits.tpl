{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li><a href="/horseracing/bettinglimits" title="" class="active">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Betting Limits</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div style="padding: 0 34px;">
<p>&nbsp;</p>
          
          
<div class="headline"><h1><strong>General Racing Rules, Tracks and Limits:</strong></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br />
<p><strong>Horse Racing Interface</strong><br />US Racing offers daily racing from tracks within the USA (and some other regions) via the racing interface which is accessible once you have logged-in to your wagering account.
We accept wagers up until post time.  We cannot assume liability for wagers that are unsuccessfully entered before post time.  Before logging-out, please check your Active Wagers on the Reports menu to make sure your bets were accepted.
Once you submit a bet online, your wager is considered final.
Bets shall not be accepted after post-time.  In the event that the race begins before the advertised post-time and for that reason (or any other reason) you are able to place a wager after the race has started, that wager will be void.</p>
<p><strong>Track Categories</strong><br />
  <a href="/horseracing/trackcategories">Click here</a> for an up-to-date list of tracks offered and their categories. </p>
  <p><strong>Odds and Limits</strong><br />
  Each event has a bet limit (typically $1,000.00).
Win, Place, Show wagers pay full track odds. All Exotic wagers pay full track odds up to the maximum pay-outs as shown in the tables below.
Payouts are based on the actual track payout. The morning lines provided when wagering on the Horse Racing Interface are for informational purposes only and not used to calculate the payout.
There are no house odds.  If there are no track payoffs for a certain type of wager, all wagers on that type will be refunded.</p>
</div>
<p>&nbsp;</p>
 <table id="infoEntries"  style="width:100%; " >
              
  <tr>
    <th  colspan="2" height="15"><p align="center" style="data">Class A Tracks</p></th>
    <th  colspan="2" valign="top"><p align="center" style="data">Class B Tracks</p></th>  </tr>
  <tr>

    <td class="data"><p align="center" ><strong>Bet Type</strong></p></td>

    <td class="data"><p align="center"><strong>Max. Payoff</strong></p></td>
    <td class="data"><p align="center"><strong>Bet Type</strong></p></td>
    <td class="data"><p align="center"><strong>Max Payoff</strong></p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Win</p></td>

    <td valign="top"><p align="center">Track odds up to $20,000 per event </p></td>
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds up to $15,000 per event</p></td>
  </tr>
  <tr >
    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds up to $20,000 per event</p></td>

    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds up to $15,000 per event</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds up to $20,000 per event</p></td>

    <td valign="top"><p align="center">Show</p></td>

    <td valign="top"><p align="center">Track odds up to $15,000 per event</p></td>
  </tr>
  <tr >
    <td valign="top"><p align="center">Exacta</p></td>
    <td valign="top"><p align="center">750-1</p></td>

    <td valign="top"><p align="center">Exacta</p></td>
    <td valign="top"><p align="center">500-1</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Quinella</p></td>
    <td valign="top"><p align="center">500-1</p></td>
    <td valign="top"><p align="center">Quinella</p></td>

    <td valign="top"><p align="center">300-1</p></td>
  </tr>

  <tr >
    <td valign="top"><p align="center">Trifecta</p></td>
    <td valign="top"><p align="center">1000-1</p></td>
    <td valign="top"><p align="center">Trifecta</p></td>

    <td valign="top"><p align="center">750-1</p></td>
  </tr>
  <tr class="odd">

    <td valign="top"><p align="center">Superfecta</p></td>
    <td valign="top"><p align="center">2000-1</p></td>
    <td valign="top"><p align="center">Superfecta</p></td>

    <td valign="top"><p align="center">1000-1</p></td>
  </tr>
  <tr >
    <td valign="top"><p align="center">Pick 3</p></td>

    <td valign="top"><p align="center">2000-1</p></td>
    <td valign="top"><p align="center">Pick 3</p></td>

    <td valign="top"><p align="center">1000-1</p></td>
  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Pick 4</p></td>
    <td valign="top"><p align="center">5000-1</p></td>

    <td valign="top"><p align="center">Pick 4</p></td>

    <td valign="top"><p align="center">2000-1</p></td>
  </tr>
  <tr >
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>

  <tr class="odd">
    <td valign="top"><p align="center">Daily Double</p></td>
    <td valign="top"><p align="center">1000-1</p></td>
    <td valign="top"><p align="center">Daily Double</p></td>
    <td valign="top"><p align="center">500-1</p></td>
  </tr>

  <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <tr >
<th class="data" colspan="2" height="15"><p align="center" style="data">Class C Tracks</p>
</th>

    <th class="data" colspan="2" height="15"><p align="center" style="data">Class D Tracks (Max Bet = $250)</p>
  </tr>
  <tr class="odd">
    <td><p align="center" ><strong>Bet Type</strong></p></td>
    <td><p align="center"><strong>Max. Payoff</strong></p></td>
     <td><p align="center" ><strong>Bet Type</strong></p></td>
    <td><p align="center"><strong>Max. Payoff</strong></p></td>

  </tr>

  <tr >
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds up to $10,000 per event</p></td>
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds up to $3,000 per event</p></td>

  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Place</p></td>

    <td valign="top"><p align="center">Track odds up to $10,000 per event</p></td>
    <td valign="top"><p align="center">Place</p></td>
    <td valign="top"><p align="center">Track odds up to $3,000 per event</p></td>

  </tr>
  <tr >
    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds up to $10,000 per event</p></td>

    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">TTrack odds up to $3,000 per event</p></td>

  </tr>
  <tr class="odd">
    <td colspan="2"><p align="center"><strong>All Exotics 300-1, up to a max of $10,000 per event </strong></p></td>
    <td colspan="2"><p align="center"><strong>All Exotics 300-1 with $3000 max per event </strong></p></td>
  </tr>
   <tr>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
    <td valign="top">&nbsp;</td>
  </tr>
  <th class="data" colspan="2" height="15"><p align="center" style="data">Class E Tracks</p>
</th>

    <th class="data" colspan="2" height="15"><p align="center" style="data">&nbsp; </p>
  </tr>
  <tr class="odd">
    <td><p align="center" ><strong>Bet Type</strong></p></td>
    <td><p align="center"><strong>Max. Payoff</strong></p></td>
     <td><p align="center" ><strong> </strong></p></td>
    <td><p align="center"><strong> </strong></p></td>

  </tr>

  <tr >
    <td valign="top"><p align="center">Win</p></td>
    <td valign="top"><p align="center">Track odds up to $1,000 per event</p></td>
    <td valign="top"><p align="center">&nbsp; </p></td>
    <td valign="top"><p align="center">&nbsp; </p></td>

  </tr>
  <tr class="odd">
    <td valign="top"><p align="center">Place</p></td>

    <td valign="top"><p align="center">Track odds up to $1,000 per event</p></td>
    <td valign="top"><p align="center">&nbsp; </p></td>
    <td valign="top"><p align="center">&nbsp; </p></td>

  </tr>
  <tr >
    <td valign="top"><p align="center">Show</p></td>
    <td valign="top"><p align="center">Track odds up to $1,000 per event</p></td>

    <td valign="top"><p align="center">&nbsp; </p></td>
    <td valign="top"><p align="center">&nbsp; </p></td>

  </tr>
  <tr class="odd">
    <td colspan="2"><p align="center"><strong>All Exotics 300-1, up to a max of $1,000 per event </strong></p></td>
    <td colspan="2"><p align="center"><strong>  </strong></p></td>
  </tr>
</table>

<p>&nbsp;</p>
<div style="padding: 0 34px;">
<p>Maximum pay-outs per event are as follows:
<ul><li>Class A Tracks: $20,000.00</li>
<li>Class B Tracks: $15,000.00</li>
<li>Class C Tracks: $10,000.00</li>
<li>Class D Tracks: $3,000.00</li>
<li>Class E Tracks: $1,000.00</li></ul><br />
In the event a new track is not listed in one of the categories, its default payout limit will be Category E.
In the event that there is evidence of pool manipulation, all affected wagers will be deemed void and all monies wagered on the affected wagers will be refunded.
Management reserves the right to refuse or limit any wager and to restrict wagering on any event at any time without any advance notice.</p>
<br>
<p><strong>Scratches</strong><br />
If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded.  (This does not apply to 'Odds to Win' bets placed via the sports betting interface.  On those bets a scratch counts as a loss).  In case a horse is considered a non-starter by the track, that horse will be considered a scratch and refunded accordingly.
In exotic wagers (Exacta, Trifecta, Superfecta, Quinella) the portion containing the scratched horse will be refunded.  We do not honor ‘all’ payouts in any exotic wager even though the track may do so.  In the case that there is an ‘all’ payout on a particular race, we will replace the ‘all’ payout with the rightful winner of that race and/or position.
On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse.  There will be no consolation payouts.  No special track payouts will be recognised.</p>
<p><strong>Pick 3 and Pick 4 Grading and Payout Rules</strong><br />
All Pick 3 and Pick 4 tickets are paid out based on the official race results  published by the track at which the race was run.  If there is a scratched horse in any Pick 3 or Pick 4, only combinations including the scratched horse will be refunded.
There will be no consolation payouts.  No special track payouts (such as payouts on scratched horses, 2 out of 3 special Pick 3 payouts and/or 3 out of 4 special Pick 4 payouts) will be recognised.</p>
<p><strong>Official Source of Resutls</strong><br />
For thoroughbred racing we use <a href="www.equibase.com">www.equibase.com</a> as our official source of results.  For harness racing we use <a href="www.racingchannel.com">www.racingchannel.com</a> or the tracks’ websites.
In the event of a discrepancy, we will contact the track for the official result.</p>
<p>&nbsp;</p>
</div>
<p><strong>Rebates</strong>
 <table id="infoEntries"  style="width:100%; " >
  <tr>
    <th scope="col"></th>
    <th scope="col">Straights</th>
    <th scope="col">Exotics</th>
  </tr>
  <tr>
    <th scope="row">Class A</th>

    <td align="center">3%</td>
    <td align="center">8%</td>
  </tr>
  <tr class="odd">
    <th scope="row">Class B</th>
    <td align="center">3%</td>
    <td align="center">5%</td>

  </tr>
  <tr>
    <th scope="row">Class C</th>
    <td colspan="2" align="center">3%</td>    
  </tr>
  <tr class="odd">
    <th scope="row">Class D</th>
   <td colspan="2" align="center">No Rebate</td> 
  </tr>

  <tr>
    <th scope="row">Class E</th>
    <td colspan="2" align="center">No Rebate</td> 
  </tr>

</table>
 </p>
<p>&nbsp;</p>
This rebate only applies to wagers placed through the horse wagering interface.  It does not apply to any horse matchups or other future wagers made through the sportsbook login.  No rebate will be given on cancelled wagers or wagers refunded due to a scratch.  No rebate will be paid on Win, Place and Show tickets that pay $2.20 to $2 or less.  The rebate bonus will be paid on a daily basis.  There is no rollover requirement associated with the rebate.
<p>&nbsp;</p>

<div style="padding:0 34px;">
 <p><strong>Matchups</strong><br />
 The following rules apply to horse matchup bets made via the sports betting interface.<ol>
 <li>In matchups between two or more named horses, all horses must go for wagers to have action.</li>
 <li>Matchups are determined by horse name. Any information pertaining to race number, race track, etc are for curtsey purposes only. Also, any track coupling is
irrelevant towards determining the winner of a matchup.</li>
 <li>In matchups between one named horse and the Field, wagers will have action if and only if the named horse starts. Scratches of horses in the Field will not affect the standing of the wager.</li>
 <li>Matchups are not eligible for rebates.</li>
 <li>The winner of a matchup will be determined by the highest placed finisher involved in the matchup. If any of the horses involved in the matchup do not finish the race, the matchup will be graded no action.</li>
 <li>Matchup wagers which indicate "Turf Only" will be cancelled if the surface is changed.</li>
 <li>In the case of a dead heat, the matchup will be graded no action.</li></ol></p>
</div>
<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    