{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li class="leaf active-trail"><a href="/horseracing/pick3pick4grading" title="" class="active">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>PK3 and PK4 Grading and Payouts</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div>
<p>&nbsp;</p>
<h2>PK3 and PK4 Tickets</h2>
<p>All PK3 and PK4 tickets are paid out based on the official race results as published by the host track at which the race was run.</p>
</div>
<p>&nbsp;</p>
<p>If there is a scratched horse in any PK3 or PK4 selection wagers will be refunded.</p>
<p>No consolation payouts as we do not recognize any 3 out of 4 special Pick 4 track payouts.</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    