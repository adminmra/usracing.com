{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li class="leaf active-trail"><a href="/horseracing/scratches" title="" class="active">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Scratches</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div><p>&nbsp;</p>
<h2>How Scratches are Handled</h2>
 <p>If a horse is scratched, all Win/Place/Show wagers placed via the racing interface will be refunded. (This does not apply to ‘Odds to Win’ bets placed via the sportsbook. On those bets a scratch counts as a loss).</p>
</div>
<p>Only the portion of Exacta/Trifecta/Superfecta/Quinella combos that contain the scratched horse will be refunded.</p>
<p>In case a horse is considered a Non Starter by the track, that horse will be considered a scratch and refunded accordingly.</p>
<p>In exotics wagers (Exacta, Trifecta, Superfecta, Quinella) scratches will be refunded. We do not honor “ALL” payouts in any exotic wager. If there is an “all” payout, this will be replaced by the horse that came in that position rightfully and wagers will be graded accordingly.</p>
<p>On Daily Doubles, Pick 3 and Pick 4 wagers, a scratch will result in an automatic refund of the combination including the scratched horse.</p>
<p>There will be no consolation payouts, special payouts such as 2 out of 3 and/or Pick 3 out of 4 in a Pick 3 or Pick 4 will be considered consolation payouts, whenever that happens the Pick 3 and/or Pick 4 will be paid with the rightful winner for each race involved in the Pick 3 and/or Pick 4.</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    