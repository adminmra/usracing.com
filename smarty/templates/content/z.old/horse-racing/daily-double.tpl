{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li class="leaf active-trail"><a href="/horseracing/dailydouble" title="" class="active">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Daily Double Grading and Payouts</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div>
<p>&nbsp;</p>
<h2>Daily Double</h2>
<p>All Daily Double tickets are paid out based on the official race results as published by the host track at which the race was run.</p>
</div>
<p>Daily Double tickets are automatically refunded if there is a scratch in any of the legs.</p>
<p>No consolation payouts will be paid.</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    