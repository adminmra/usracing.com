{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li class="leaf active-trail"><a href="/horseracing/rebates" title="" class="active">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Rebates at US Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><h2>Rebates</h2>A rebate of 8% for Exotics and 3% for Win, Place or Show will be paid on all wagers on a daily basis. There are no minimum daily volume requirements and there is no maximum on how much you can earn.</p>

<p>No rebate will be given on cancelled wagers or wagers refunded due to a scratch.</p>

<p>Important Note: The 8% &amp; 3% Rebates only apply to wagers placed through the horse wagering interface. It does not apply to any matchups or other wagers made through the sportsbook login. The 3% rebate bonus will be paid on a daily basis.</p><p>Wagers placed on a horse to Show that payout less than 2.20 will not be eligible for the daily rebate.</p>
<br>
  <table id="infoEntries"  style="width:100%; " >
  <tr>
    <th scope="col">&nbsp;</th>
    <th scope="col">Straights</th>
    <th scope="col">Exotics</th>
  </tr>
  <tr>
    <th scope="row">Class A</th>

    <td align="center">3%</td>
    <td align="center">8%</td>
  </tr>
  <tr class="odd">
    <th scope="row">Class B</th>
    <td align="center">3%</td>
    <td align="center">5%</td>

  </tr>
  <tr>
    <th scope="row">Class C</th>
    <td colspan="2" align="center">3%</td>    
  </tr>
  <tr class="odd">
    <th scope="row">Class D</th>
   <td colspan="2" align="center">No Rebate</td> 
  </tr>

  <tr>
    <th scope="row">Class E</th>
    <td colspan="2" align="center">No Rebate</td> 
  </tr>

</table>
<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    