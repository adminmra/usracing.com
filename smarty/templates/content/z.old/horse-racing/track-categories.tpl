{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li><a href="/horseracing/posttime" title="">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li class="leaf last active-trail"><a href="/horseracing/trackcategories" title="" class="active">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Track Categories</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<table id="infoEntries"  style="width:100%; " >
  <col width="33%" />
  <col width="33%" />
  <col width="33%" />
  <tr  height="22">

    <th height="22" width="25%"><p><strong>Class A</strong></p></th>
    <th width="25%"><p><strong>Class B</strong></p></th>
    <th width="25%"><p><strong>Class C</strong></p></th>
     <th width="25%"><p><strong>Class D</strong></p></th>
	  <th width="25%"><p><strong>Class E</strong></p></th>
  </tr>
 <tr  height=15>
  <td height=15 >Aqueduct</td>
  <td >Arlington</td>
  <td >Beulah</td>
  <td >Albuquerque</td>
  <td class=xl27><!-- Remington --></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 >Belmont</td>
  <td class=xl27>Calder</td>
  <td >Balmoral Park Harness</td>
  <td class=xl27><!-- Delta Downs (QH) --></td>
  <td >Retama</td>
 </tr>
 <tr  height=15>
  <td height=15 >Churchill Downs</td>
  <td >Charles Town</td>
  <td ><!-- Cal Expo --></td>
  <td class=xl27>Evangeline (QH)</td>
  <td >Zia Park</td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 >Del Mar</td>
  <td >Delaware Park</td>
  <td class=xl27>Canterbury</td>
  <td >Fairgrounds (QH)</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 >Gulfstream</td>
  <td >Delta Downs (T)</td>
  <td >Colonial Downs</td>
  <td >Ferndale **</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 >Hollywood</td>
  <td >Emerald Downs</td>
  <td >Dover Downs Harness</td>
  <td ><span class="xl27">Fort Erie</span></td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 >Keeneland</td>
  <td >Evangeline (T)</td>
  <td class=xl27>Ellis Park</td>
  <td >Fresno **</td>
  <td ></td>
 </tr>
 <tr class="odd "height=15>
  <td height=15 >Oak Tree</td>
  <td >Fairgrounds (T)</td>
  <td ><!-- Fairmount --></td>
  <td class=xl27>Hastings</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 >Santa Anita</td>
  <td >Finger Lakes</td>
  <td >Fairplex</td>
  <td >Lone Star (QH)</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 >Saratoga</td>
  <td >Golden Gate</td>
  <td >Harrington</td>
  <td >Lousiana Downs (QH)</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Indiana Downs (T)</td>
  <td >Hawthorne</td>
  <td >Pinnacle</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Laurel</td>
  <td >Hoosier</td>
  <td >Pleasanton **</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Indiana Downs Harness</td>
  <td class=xl27>Indiana Downs Harness</td>
  <td >Prairie Meadows (QH)</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Lone Star (T)</td>
  <td >Maywood</td>
  <td >Ruidoso Downs</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Los Alamitos</td>
  <td >Meadowlands Harness</td>
  <td >Sacramento ** </td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Lousiana Downs (T)</td>
  <td >Mohawk</td>
  <td >Sam Houston (QH)</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Meadowlands (T)</td>
  <td >Monticello Harness</td>
  <td >Santa Rosa ** </td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Monmouth</td>
  <td >Northfield Park Harness</td>
  <td >Stockton **</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Mountaineer</td>
  <td >Portland Meadows</td>
  <td >Vallejo **</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Oaklawn</td>
  <td >River Downs</td>
  <td >&nbsp;</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Penn National</td>
  <td >Suffolk</td>
  <td >&nbsp;</td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Philadelphia Park</td>
  <td >Thistledown</td>
  <td >&nbsp;</td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >	Pimlico***</td>
  <td >Yavapai Downs</td>
  <td ></td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Prairie Meadows (T)</td>
  <td >Yonkers Harness</td>
  <td ></td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Sam Houston (T)</td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
  <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Sunland</td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>

 <tr  height=15>
  <td height=15 ></td>
  <td >Tampa Bay Downs </td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Turf Paradise</td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
 <tr  height=15>
  <td height=15 ></td>
  <td >Turfway</td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 ></td>
  <td >Woodbine</td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
 <tr height=15>
  <td height=15 ></td>
  <td ></td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>
 <tr class="odd" height=15>
  <td height=15 >** California Fairs</td>
  <td ></td>
  <td ></td>
  <td ></td>
  <td ></td>
 </tr>

</table>
<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    