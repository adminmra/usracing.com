{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Horse Racing Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/horseracing/bettinglimits" title="">BETTING LIMITS</a></li>
<li class="leaf active-trail"><a href="/horseracing/posttime" title="" class="active">POST TIME</a></li>
<li><a href="/horseracing/rebates" title="">REBATES</a></li>
<li><a href="/horseracing/pick3pick4grading" title="">PK3 &amp; PK4 GRADING AND PAYOUTS</a></li>
<li><a href="/horseracing/dailydouble" title="">DAILY DOUBLE</a></li>
<li><a href="/horseracing/scratches" title="">SCRATCHES</a></li>
<li><a href="/horseracing/trackcategories" title="">TRACK CATEGORIES</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Post Time at the Races</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Wagers are accepted until post time. If a wager is made after the race has begun, it will not be accepted. We do not assume liability for wagers that are unsuccessfully entered before post time. Only wagers confirmed&nbsp;by&nbsp;clicking&nbsp;“Submit” are accepted, please insure to check&nbsp;your "reports"screen before logging out to see that your wagers were confirmed.</p>
<p>Once you confirm a bet online, your wager is considered final, we cannot cancel confirmed wagers.</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    