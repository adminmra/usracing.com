
       {literal}<script type="text/javascript">  
	 if(Check_Cookie(rot13("UTMU"))){
	Delete_Cookie( rot13("UTMU"), "/", "usracing.com" );
	Delete_Cookie( rot13("UTMP"), "/", "usracing.com" );
	Delete_Cookie( rot13("TRACK"), "/", "usracing.com" );
	Set_Cookie( 'REDIRECT', 'usracing.com', 0.5, "/", ".usracing.com", "" );
	window.location="/support/loginhelp?error=The credentials you provided cannot be determined to be authentic.";
	}
</script>{/literal}{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Support</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/support/gamingknowledgebase" title="">GAMING KNOWLEDGEBASE</a></li>
<li class="expanded"><a href="/house-rules" title="">RULES</a><ul class="menu"><li class="leaf first"><a href="/house-rules" title="">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
<li><a href="/rules/casinogamerules" title="">CASINO RULES</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
</ul></li>
<li><a href="/horseracing-affiliate-program" title="">WEBMASTERS</a></li>
<li><a href="/responsibilities" title="">RESPONSIBLE GAMING</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/help/customer_support.jpg" alt="Call us at 1-888-BET-WITH-ME" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Unsuccessful Login</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div style="padding:20px; border: 1px solid #aad3ff;">


<p><strong>Having some trouble logging into your account?</strong>
<p>Please remember that your <u><strong>username</strong></u> and <u><strong>password</strong></u> are case sensitive. </p>
<p>Please try logging in again.</p>
</div>
<p>&nbsp;</p>
<br />
<span><a onclick="MM_openBrWindow(this.href, 'help','width=480,height=410,resizable=no,scrollbars=no,status=no'); return false;" href="https://server.lon.liveperson.net/hc/66702201/?cmd=file&amp;file=visitorWantsToChat&amp;site=66702201&amp;byhref=1&amp;SESSIONVAR!skill=All%20Horse%20Racing" target="_blank"><img style="padding-left: 20px;" title="Live Assistance" alt="Live Assistance" "align="right" src="/images/click-here.gif" border="0" /></a></span>
<p>If you have forgotten your username or password, you can retrieve them in the <a title="Club House" href="/clubhouse">Club House</a>, using the menu on the left. </p>
<p>Or, please give us a call 24 x 7 x 365 days a year!</p>

<p>Go to <a title="Support Centre" href="/support">Support Centre</a> and you will see all the ways to contact us.</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



    