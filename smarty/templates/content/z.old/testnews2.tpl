
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>testnews2</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- ======== STYLES NEEDED TO HIDE BLOCK ELEMENTS ================== -->

<!-- ============================================================== -->
testing year 
<h2>California horse trainer Noble Threewit dies at 99</h2>
<div style="padding: 0 34px; ">
<p class="news-date">Daily Racing News<br />Posted on  September 20 2010</p>
<p><text>
<p>WEST COVINA, Calif. (AP) - Noble Threewit, a trainer and beloved figure in Southern California horse racing for more than seven decades, has died. He was 99.</p>
<p><text>
<p>WEST COVINA, Calif. (AP) - Noble Threewit, a trainer and beloved figure in Southern California horse racing for more than seven decades, has died. He was 99. ... &nbsp;&nbsp;<a href="news/11098?title=california-horse-trainer-noble-threewit-dies-at-99">Read Article</a> </p>
<!-- <p><p><Text>
<p>WEST COVINA, Calif. (AP) - Noble Threewit, a trainer and beloved figure in Southern California horse racing for more than seven decades, has died. He was 99.</p>
<p>Fairplex Park spokesman Ed Golden said Threewit died Friday at an assisted living facility in West Covina.</p>
<p>Except f...</p>  -->

</div>
<pre>Array
(
    [39] => Array
        (
            [date] => 1285138800
            [locations] => Array
                (
                    [0] => Array
                        (
                            [location] => Woodbine
                            [details] =>  King Corrie S., $100,000, 3&amp;up, 6f
                        )

                )

        )

    [40] => Array
        (
            [date] => 1285398000
            [locations] => Array
                (
                    [0] => Array
                        (
                            [location] => Belmont Park
                            [details] =>  Gallant Bloom H, (G2), $150,000, 3&amp;up, F&amp;M, 6.5f
                        )

                    [1] => Array
                        (
                            [location] => Belmont Park
                            [details] =>  Brave Raj Breeders&#039; Cup S., $85,000, 2yo f, 8.32f
                        )

                    [2] => Array
                        (
                            [location] => Belmont Park
                            [details] =>  Foolish Pleasure Breeders&#039; Cup S., $85,000, 2yo, 8.32f
                        )

                    [3] => Array
                        (
                            [location] => Delaware Park
                            [details] =>  Kent Breeders&#039; Cup S, (G3), $200,000, $50,000 Breeders&#039; Cup Fund, 3yo, 9f T
                        )

                    [4] => Array
                        (
                            [location] => Hastings Racecourse
                            [details] =>  British Columbia Breeders&#039; Cup Oaks, $100,000, 3yo f, 9f
                        )

                    [5] => Array
                        (
                            [location] => Hastings Racecourse
                            [details] =>  Fantasy S., $75,000, 2yo f, 8.5f
                        )

                    [6] => Array
                        (
                            [location] => Louisiana Downs
                            [details] =>  Super Derby (G2), $500,000, 3yo, 9f
                        )

                    [7] => Array
                        (
                            [location] => Louisiana Downs
                            [details] =>  Louisiana Stallion S., $100,000, 2yo fd, 7f
                        )

                    [8] => Array
                        (
                            [location] => Louisiana Downs
                            [details] =>  Louisiana Stallion S., $100,000, 2yo, C&amp;Gd, 7f
                        )

                    [9] => Array
                        (
                            [location] => Louisiana Downs
                            [details] =>  Sunday Silence Breeders&#039; Cup, $100,000, 2yo, 8.5f T
                        )

                    [10] => Array
                        (
                            [location] => Louisiana Downs
                            [details] =>  Happy Ticket Breeders&#039; Cup, $75,000, 2yo f, 8.5f T
                        )

                    [11] => Array
                        (
                            [location] => Monmouth Park
                            [details] =>  Helen Haskell Sampson S, (NSA-I), $100,000, 4&amp;up, 20f T
                        )

                    [12] => Array
                        (
                            [location] => Parx Racing
                            [details] =>  Pennsylvania Derby (G2), $1,000,000, 3yo, 9f
                        )

                    [13] => Array
                        (
                            [location] => Parx Racing
                            [details] =>  Turf Amazon H., $200,000, 3&amp;up, F&amp;M, 5f T
                        )

                    [14] => Array
                        (
                            [location] => Presque Isle Downs
                            [details] =>  Fitz Dixon Jr. Memorial Juvenile S., $100,000, 2yo, 6.5f
                        )

                    [15] => Array
                        (
                            [location] => Presque Isle Downs
                            [details] =>  H.B.P.A. Stakes, $100,000, 3&amp;up, F&amp;M, 8.32f
                        )

                    [16] => Array
                        (
                            [location] => Presque Isle Downs
                            [details] =>  Presque Isle Debutante, $100,000, 2yo f, 6f
                        )

                    [17] => Array
                        (
                            [location] => Retama Park
                            [details] =>  Darby&#039;s Daughter Texas Stallion S., $100,000, 2yo f, 6f
                        )

                    [18] => Array
                        (
                            [location] => Retama Park
                            [details] =>  My Dandy Texas Stallion S., $100,000, 2yo, C&amp;G, 6f
                        )

                    [19] => Array
                        (
                            [location] => Woodbine
                            [details] =>  Ontario Derby, $150,000, 3yo, 9f
                        )

                )

        )

)
</pre>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    