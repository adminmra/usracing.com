{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Why Us?</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/welcome" title="">WELCOME</a></li>
<li><a href="/learnmore" title="">LEARN MORE</a></li>
<li><a href="/seedifference" title="">SEE THE DIFFERENCE</a></li>
<li><a href="/racebookreviews" title="">COMPARE US</a></li>
<li><a href="/readreviews" title="">READ REVIEWS</a></li>
<li><a href="/betwithconfidence" title="">Setting the Pace</a></li>
<li><a href="/bestracebook" title="">ABOUT US Racing</a></li>
<li class="leaf last active-trail"><a href="/privacy_policy" title="" class="active">PRIVACY POLICY</a></li>

</ul>  </div>

  

<!-- /block-inner, /block -->

            
{include file='banners/banner3.tpl'}


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>US Racing Privacy Policy</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>At US Racing, we have created this statement in order to demonstrate our firm commitment to privacy.</h2>

<p>
<p><strong>IP Address</strong></p>
<p>We use your IP address to help diagnose problems with our server and to administer our web site.  When placing a wager, or logging onto our secure server, your IP address is tracked against your user ID for the dual purposes of preventing fraud, and creating an audit trail.  This information is not used or accessed for any other purpose. </p>
<p>We collect aggregate information site-wide, including anonymous site statistics (eg. the number of page views or visits a certain section of our web site has received).  We review and discard the information in some cases (eg. IP address logs for file transfers), while in others we may use the information to improve our users experience and analyze the effectiveness of our advertising.</p>
</p>
<p>
<p><strong>Registration Form, Contests, Surveys and Order Forms</strong></p>
<p>The online forms at US Racing request that you provide us with contact and/or financial information which may include some or all of the following: name, address, cell phone number, email address, credit card number(s).  We use the contact details provided to send you information about our company and to communicate with you when necessary.  You may opt-out of receiving future mailings or SMS text messages at any time; see the choice/opt-out section below.  </p>
<p>This personal information is not gathered by us without your knowledge, permission and active participation and is only utilized by US Racing in the course of running the site or during its own marketing mail-outs.  Participants details are not sold or shared with any other entities.  </p>
<p>The financial information that is collected is used to check members qualifications for registration and/or to bill members for products and services.  </p>
</p>
<p>
<p><strong>Security</strong></p>
<p>The US Racing site has stringent security measures in place to protect the loss, misuse and alteration of the information under our control.</p>
<p>Our member database is stored on a secure remote offsite server.  It is protected with an industrial strength firewall and monitored 24/7 by skilled security specialists and automated intruder detection mechanisms. </p>
<p>Personal information is submitted using high security 128bit encryption.  Any wagers or transactions are transmitted using high security 128bit encryption. </p>
</p>
<p>
<p><strong>Choice/Opt-Out of US Racing</strong></p>
<p>This function only provides members/visitors the opportunity to opt-out of receiving communications from us.</p>
<p>If you wish to be removed from our mailing list and stop communication from US Racing, please email vip@usracing.com and write UNSUBSCRIBE in the subject of the email</p>
<p>You may request the closure of your wagering account at any time as long as there is no balance and no pending wagers. </p>
</p>
<p>
<p><strong>Correcting or Updating Personal Information</strong></p>
<p>US Racing provides members with the secure 'My Account' area for changing and modifying information previously provided. </p>
</p>
<p>
<p><strong>Contacting the Web Site</strong></p>
<p>If you have any questions about this privacy statement, the practices of this site or your dealings with this web site, you can contact:</p>
<p>support@usracing.com
1-877-BET-WITH-ME
1-877-238-9484
</p>
</p>

<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    