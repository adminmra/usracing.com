    {include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Support</h2>
  



    <ul class="menu"><li><a href="/support" title="" class="active">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/support/gamingknowledgebase" title="">GAMING KNOWLEDGEBASE</a></li>
<li class="expanded"><a href="/house-rules" title="">RULES</a><ul class="menu"><li class="leaf first"><a href="/house-rules" title="">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
<li><a href="/rules/casinogamerules" title="">CASINO RULES</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
</ul></li>
<li><a href="/horseracing-affiliate-program" title="">WEBMASTERS</a></li>
<li><a href="/responsibilities" title="">RESPONSIBLE GAMING</a></li>
</ul>

          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/help/customer_support.jpg" alt="Call us at 1-888-BET-WITH-ME" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Contact Us</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>We have over 200 Customer Support Representatives ready to help you by phone, email or chat 24 hours a day, 7 days a week, 365 days a year!</p>
<p>All email inquiries and live help are available between 3AM and 9PM EST, daily.</p>
<br />
<table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody><tr>
<td width="45%" valign="top">
<strong>Customer Support Phone:</strong><br />
<br /><br /><br />
<b>Fax from North America:</b><br />
<b>Outside North America Fax</b>:<br />
<br />
<b>Support Email:</b><br />
<br /><b>Horse Racing Support:</b><br />
<br /><br />
<b>Poker Support Email:</b>
</td>

<td valign="top">
<strong>1-877-BET-WITH-ME</strong><br />
1-877-238-9484<br />
1-800-891-7806<br />
<br />
1-800-201-4919<br />
1-208-567-3627<br />
<br /><a href="mailto:support@usracing.com">support@usracing.com</a><br />
<br />
1-866-845-RACE<br />
1-866-845-7223<br /><br /><a href="mailto:poker@usracing.com">poker@usracing.com</a>
</td>

<td><!-- <a onclick="MM_openBrWindow(this.href, 'help','width=480,height=410,resizable=no,scrollbars=no,status=no'); return false;" href="https://server.lon.liveperson.net/hc/66702201/?cmd=file&amp;file=visitorWantsToChat&amp;site=66702201&amp;byhref=1&amp;SESSIONVAR!skill=All%20Horse%20Racing" target="_blank"><img style="padding-left: 20px;" title="" alt="" src="/images/click-here.gif" border="0" /></a> -->
<!-- BEGIN LivePerson Code -->

<a href='https://server.lon.liveperson.net/hc/66702201/?cmd=file&file=visitorWantsToChat&site=66702201&byhref=1&SESSIONVAR!skill=GoHorse Betting<https://owa.electricmail.com/owa/UrlBlockedError.aspx>' target='chat66702201'  onClick="javascript:window.open('https://server.lon.liveperson.net/hc/66702201/?cmd=file&file=visitorWantsToChat&site=66702201&SESSIONVAR!skill=All%20Horse%20Racing&referrer='+escape(document.location),'chat66702201','width=472,height=320');return false;" ><img style="padding-left: 20px;" title="" alt="" src="/images/click-here.gif" border="0" /></a><!-- END LivePerson code -->
</td>
</tr></tbody></table>
<p>&nbsp;</p>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 


