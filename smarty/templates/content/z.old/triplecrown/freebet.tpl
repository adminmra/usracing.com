
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->



            
            
{include file='menus/triplecrown.tpl'}


<div id="block-menu-menu-racebook-help" class="block block-menu region-even even region-count-2 count-2">

      <h2 class="title">Help / Rules</h2>
  



    <ul class="menu"><li class="leaf first"><a href="/support" title="Customer Care">CUSTOMER CARE</a></li>
<li><a href="/house-rules" title="House Rules">HOUSE RULES</a></li>
<li><a href="/horseracing/bettinglimits" title="Horse Racing Rules">HORSE RACING RULES</a></li>
<li><a href="/rebates" title="8% Rebate Wagers">8% REBATE WAGERS</a></li>
<li><a href="/howto/betonsports" title="">SPORTS BETTING RULES</a></li>
<li><a href="/handicapping-horses" title="Handicapping">HANDICAPPING</a></li>
<li><a href="/legend" title="Horse Racing Legend">HORSE RACING LEGEND</a></li>
<li><a href="/horseracingterms" title="Horse Racing Terms">HORSE RACING GLOSSARY</a></li>
<li><a href="/horseracingbets" title="Horse Racing Wagers">HORSE RACING WAGERS</a></li>
<li><a href="/horseracing-history" title="History of Horse Racing">HORSE RACING HISTORY</a></li>
<li><a href="/famoushorses" title="Famous Horses">FAMOUS HORSES</a></li>
<li><a href="/famousjockeys" title="Famous Jockeys">FAMOUS JOCKEYS</a></li>
<li><a href="/bestracebook" title="About US Racing">ABOUT US</a></li>
</ul>  </div>

  

</div><!-- /block-inner, /block -->


{include file='banners/banner3.tpl'}
{include file='banners/rotw.tpl'}
          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Triple Crown Triple Your Winnings FREE BET Extravaganza!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div  class="promos-welcome" style="margin: 34px 0 0 0; padding:20px 0 10px 0; text-align: center;"><h4>Welcome to the Triple Crown Triple Your Winnings FREE BET Extravaganza!</h4></div>

<div style="width: 100%;"><img style="margin: 0px auto; padding-left: 30px; width: 200px;" title="Free Gift" src="/themes/images/ahr-freegift.jpg" border="0" alt="Free Gift Image"></div>

	<p>The wagers are up and waiting for you make your free horse pick for the 2011 Triple Crown races. The Triple Crown races include: the 138th running of the Kentucky Derby on May 5th, 2012; the 137th running of the Preakness Stakes on May 19, 2012 and the 143rd running of the Belmont Stakes on June 11th, 2011.</p>
	<p><a href="/">US Racing</a> is offering you a chance to win <strong>FREE MONEY</strong>  with a <strong><a href="/triplecrown/freebet">FREE TRIPLE CROWN BET</a></strong> for each of the Triple Crown races. </p>
	<p>Not a member? <a href="/join">JOIN NOW</a>  to get your Triple Crown Triple Wager! That's one free bet on each of the Triple Crown races.</p>
	<p>Members are permitted to wager on one side of each of the Triple Crown Free Bets. Simply place your bet on the over/under for the finishing time of any or all races and when you win your money will be doubled, should your wager lose your $5 Free Bet will be refunded to your account. </p>
	<p>You can't go wrong with these three easy steps:</p>
<ul>
		<ol>
			<li>Pick your over/under for each of the Triple Crown Races</li>
			<li>When you win, your <strong>winnings will be doubled!</strong></li>
			<li>If you lose, your <strong>$5 will be refunded back to your account</strong></li>
		</ol>
</ul>
	<p>Vist the <strong><a href="/sportsbook">Horse Betting</a></strong>  tab in the sportsbook to make your online horse wager today.</p>
	<p><strong>Please note:</strong> Wagering on both sides of any or all free bets will result in a forfeit of funds and possible account closure.  Your account will be debited for each bet at the time of your pick(s).  Once the race(s) is over, you will get your free money or the money you bet will be returned to your account.  Good luck and enjoy the races!</p>
	<p>Here are the betting options for your <strong>Triple Crown Double Your Winnings FREE BET</strong> Extravaganza:</p>
  
  
	<p><strong>The 138th Kentucky Derby $5.00 Free Bet</strong>
  	<br />
		Saturday, May 5, 2012 6:30 p.m.<br />
    <strong>Finishing Time of Winning Horse:</strong> Over or under 2:02.15</p>
	
  <br /><br />
	<p><strong>The 137th Preakness Stakes $5.00 Free Bet</strong>
  <br />
  Saturday, May 19, 2012 6:00 p.m.
  <br />
	<strong>Finishing Time of Winning Horse:</strong> Over/Under 1:55:15
  </p>

<br /><br />
	<p><span class="DarkBlue"><font size="2"><strong>The 143rd Belmont Stakes $5.00 Free Bet</strong></font></span>
  <br />
	Saturday, June 11, 2011 6:00 p.m.<br />
	<strong>Finishing Time of Winning Horse:</strong> Over/Under 2:28:75
  </p>
    
    <br />
	<em style="color: #808080;">Log in and visit Sportsbook for the latest $5.00 Free Bet Odds</em>
 <br /> <br />
<p>&nbsp;</p>
<div style="width: 100%;"><a href="/join"><img style="margin: 0px auto; width: 150px;" title="Bet Now!" src="/themes/images/betnow-button.gif" border="0" alt="Bet Now!"></a></div>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    