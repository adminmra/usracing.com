
      
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Kentucky Derby goes steroid-free</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="article">

<div class="InfoRibbon">
<span class="Source">AP</span><!-- SOURCE -->
<span class="ShortDate">4/30/2009</span><!-- DATE - SHORT -->
</div>

<div class="Author">JEFFREY McMURRAY</div><!-- AUTHOR -->
<div class="LongDate">Thursday, April 30, 2009</div><!-- DATE - LONG -->

<div class="inner">
<div class="body"><!-- ARTICLE -->

		<p>LOUISVILLE, Ky. (AP)—The biggest change at this year’s Kentucky Derby won’t be noticed by any fan or disrupt the routine of any horse. In fact, its only evidence will be sealed and stored in a padlocked refrigerator minutes after the race.</p><p>For the first time, the signature American thoroughbred race is screening for anabolic steroids—a quiet step that industry officials are hoping will make a loud splash in public relations.</p><p>“Our existence depends on public confidence,” longtime breeder Arthur Hancock said. “If we lose that, we lose our livelihood. Its extremely important we get this mess cleaned up.”</p><p>Last year’s Derby winner, Big Brown, was on steroids at the time of his victory—a fact known only because trainer Rick Dutrow acknowledged it. Dutrow explained he injected the horse with regular doses of the then-legal steroid stanozolol, sold under the brand name Winstrol, although he insisted the intent was not to build muscle but to increase his appetite and brighten his coat.</p><p>If the winner of this year’s Derby tests positive for more than a trace amount of stanozolol, the horse will be disqualified and the trainer will be subjected to a lengthy suspension.</p><p>That drug is now allowed in the sport only for therapeutic uses, and no shot can be given in Kentucky within 60 days of a race—more than enough time for any performance-enhancing effect to wear off. Only three other anabolic steroids are allowed even in minuscule doses anymore. Dozens of others have been banned completely.</p><p>At the time of last year’s Derby, only 12 of 38 racing states had banned steroids. Now 35 have, representing more than 99 percent of the races subjected to pari-mutuel betting, according to the Racing Medication and Testing Consortium, an industry group that pushed a national testing standard.</p><p>“We went from completely unregulated to uniformly regulated in a year and a half,” said Scot Waterman, RMTC’s executive director. “There are plenty of people in this industry that never thought they’d see that day.”</p><p>Hall of Fame trainer Jack Van Berg compared horse training to “chemical warfare” last year while testifying to Congress on safety problems with the sport. While few in the industry are using that kind of terminology, there has been some evidence steroids were the rule, not the exception, for the 1,200-pound athletes.</p><p>Pennsylvania banned anabolic steroids last April but had previously conducted tests to see how many of its racehorses would have tested positive. Waterman said the results were staggering—showing nearly two-thirds of the horses tested had a noticeable concentration of the drugs in their bloodstream.</p><p>Those numbers don’t surprise Larry Jones, who trained last year’s Derby runner-up Eight Belles. The filly pulled up lame jogging past the finish and had to be euthanized with two fractured ankles. With speculation swirling the muscular horse was using steroids, Jones called for not just the regular necropsy but a more sophisticated one that included a drug test to prove she was clean.</p><p>“Every wrongdoing you could do to a horse, we had been accused of it,” Jones said. “We were probably the only one of the 20 (Derby starters) not on steroids.”</p><p>Bryce Peckham, Kentucky’s chief equine veterinarian, said he has heard such claims from many owners and trainers. Now the testing will prove it.</p><p>“If everybody’s on the same level playing field, I don’t think you’ll find people squeaking so much,” Peckham said.</p><p>The blood and urine samples collected from the top four finishers of the Derby will be sealed with tamperproof evidence tape and stored in a locked refrigerator until officials from Kentucky’s new testing lab at the University Florida can examine them. If there is a positive test at the Derby, it could be several days before that result is known, Peckham said.</p><p>Science still hasn’t determined the true effect steroids have on racehorses. While there is clear evidence that the drugs build muscle mass in human athletes that lead to more power or strength, racing for years focused on other performance-enhancing drugs instead that were perceived as more dangerous to the animals.</p><p>Although equine athletes still aren’t raised on a diet of just hay, oats and water, as the adage goes, Waterman says the sport has taken some dramatic strides in recent months. The RMTC is still active and working on another ban on a type of steroids that reduce inflammation, unlike anabolic steroids, which are designed to build muscle. There is more industry resistance to that, he said.</p><p>Alan Foreman, chairman and CEO of the Thoroughbred Horsemen’s Association, said he used to think racing could shield itself from public outcry on drugs by pointing to the most comprehensive testing anywhere in American sports—one so strict that athletes cant even medicate for a headache on the day of a race.</p><p>But Foreman said his views changed after he looked at the results of a survey of racing fans after Eight Belles’ accident that exposed many concerns about the integrity of the sport—medication top among them.</p><p>“In the court of public opinion, which in today’s world is the only thing that truly matters, the perception is that our sport is not clean,” Foreman said. “In today’s world, perception is reality no matter how unfair or inaccurate that perception may be. We may think our deterrent system is effective but apparently many people think it’s not.”</p>

</div><!-- END ARTICLE -->
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    