{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded active-trail"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li class="leaf active-trail"><a href="/beton/politics" title="Political Betting" class="active">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-81" class="block block-block region-even even region-count-2 count-2">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/virtualderby" title="Virtual Derby 3D"><img src="/themes/images/banner-col-virtualderby.jpg" alt="Play Virtual Derby" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



<div id="block-block-96" class="block block-block region-odd odd region-count-3 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/casino" title="Inbetween Races? Play Blackjack!"><img src="/themes/images/banner-play-blackjack.jpg" alt="Play Blackjack" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/banner-sportsbook-horses.jpg" alt="Sportsbook" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Political Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Political Betting - Bet on Politics</h2></div>
<br />
<p>The race for who will be President of the United States is gearing up-- John McCain has certainly secured the Republican Nomination for his party with endorsements from President Bush (Father and Son).</p><p>The real question on people's minds is whether Hillary Clinton or Barack Obama will emerge as the Democratic Party's Nomination. A few months ago, a Clinton victory seemed all but assured. However, Obama has been able to leverage grass roots support and a message of hope that is not mired in "politics as usual."&nbsp; Lately the contest has gotten tight.</p><p>Both candidates have focused on the economy recently. Ohio and Wisconsin are swing states with economic woes and large populations of blue-collar Democrats, a key part of Clinton's constituency.</p><p>Clinton also hopes to profit in Texas through her strength among Hispanics, who are expected to be at least one-quarter of the state's Democratic vote.</p><p>Victories in Texas and Ohio have become vital for Clinton as she tries to make up a gap with Obama in the race for pledged delegates awarded by the state-by-state contests to pick a Democratic nominee.</p><p>Clinton adviser Harold Ickes said she would nearly catch Obama in the delegate race if she won those two states, and the two would be roughly even when the primary process ends in June. He said she would battle all the way to the convention if necessary.</p><p>The ultimate winner could be determined by support from 796 "superdelegates" -- party insiders and elected officials who are free to back any candidate.</p><p>Who will be the Democratic Nominee?</p><p>Who will be the next President?</p><p>Cast your vote and place your bets!&nbsp; Politcal Futures wagers are available in the sportsbook at US Racing.</p><p>Presidential Election 2009 is over.</p>
<p>&nbsp;</p>
              
            
            
                      
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    