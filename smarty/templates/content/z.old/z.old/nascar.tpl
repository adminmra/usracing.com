{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded active-trail"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li class="leaf active-trail"><a href="/beton/nascar" title="NASCAR Betting Autoracing" class="active">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



{include file='banners/banner4.tpl'}             
{include file='banners/banner3.tpl'}
{include file='banners/banner2.tpl'}
{include file='banners/banner1.tpl'}    


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join" title="Bet on Nascar"><img src="/themes/images/sports/motorsport_bets.jpg" alt="Nascar Bets" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>NASCAR Betting Autoracing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span class="DarkBlue">NASCAR Betting - Bet on NASCAR and Autoracing</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<br /> 

<p>NASCAR is the world renowned abbreviation for the National Association for Stock Car Auto Racing. There are several different racing series or divisions that make up NASCAR, with different sizes of cars and engines in each division. Sitting at the top of the NASCAR divisions is the Sprint Cup Series, which derives its name from the sponsorship it receives from the Sprint Telephone Company. The NASCAR Sprint Cup replaces the previous Nextel Cup and origional Winston Cup which got its name from the Winston cigarette division of the R. J. Reynolds Tobacco Co. that sponsored the series for over 30 years beginning in 1972. The Cup Series is the elite in stock car racing with the best drivers and the fastest and most powerful cars. Cup racing also draws the most recognition and pays the most prize money to its team owners and drivers for their accomplishments at each of the more than 20 tracks that are sanctioned by NASCAR for the Cup races.</p>

<p><br />If you want more we have Formula 1 and Indy racing so if have gasoline running through your veins then we've got your action!</p>

<p><strong>2010 NASCAR Schedule:</strong></p>

<p>&nbsp;</p>

<table border=0 cellpadding=0 cellspacing=0 width=453 style='border-collapse:
 collapse;table-layout:fixed'>
 <col width=223>
 <col width=230>
 <col width=79>
 <col width=69>
 <col width=50>
 <col width=92>
 <tr height=13>
  <td height=13 width=223>Date  </td>
  <td width=230>Race</td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Feb 6</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>8:10 pm ET </td>
  <td>Budweiser Shootout</td>
 </tr>
 <tr height=13>
  <td height=13>Daytona International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Thu. Feb 11</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>2:00 pm ET </td>
  <td>Gatorade Duel 1</td>
 </tr>
 <tr height=13>
  <td height=13>Daytona International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Thu. Feb 11</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>4:00 pm ET </td>
  <td>Gatorade Duel 2</td>
 </tr>
 <tr height=13>
  <td height=13>Daytona International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Feb 14</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:19 pm ET </td>
  <td>Daytona 500</td>
 </tr>
 <tr height=13>
  <td height=13>Daytona International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Feb 21</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>3:00 pm ET </td>
  <td>Auto Club 500</td>
 </tr>
 <tr height=13>
  <td height=13>Auto Club Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Feb 28</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>3:00 pm ET </td>
  <td>Shelby American</td>
 </tr>
 <tr height=13>
  <td height=13>Las Vegas Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Mar 7</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Kobalt Tools 500</td>
 </tr>
 <tr height=13>
  <td height=13>Atlanta Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Mar 21</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Food City 500</td>
 </tr>
 <tr height=13>
  <td height=13>Bristol Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Mar 28</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Goody's Fast Pain Relief 500</td>
 </tr>
 <tr height=13>
  <td height=13>Martinsville Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Apr 10</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Subway Fresh Fit 600</td>
 </tr>
 <tr height=13>
  <td height=13>Phoenix International Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Apr 18</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Samsung Mobile 500</td>
 </tr>
 <tr height=13>
  <td height=13>Texas Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Apr 25</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Aaron's 499</td>
 </tr>
 <tr height=13>
  <td height=13>Talladega Superspeedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. May 1</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Crown Royal 400</td>
 </tr>
 <tr height=13>
  <td height=13>Richmond International Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. May 8</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Southern 500</td>
 </tr>
 <tr height=13>
  <td height=13>Darlington Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. May 16</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Dover 400</td>
 </tr>
 <tr height=13>
  <td height=13>Dover International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. May 22</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Sprint Showdown</td>
 </tr>
 <tr height=13>
  <td height=13>Charlotte Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13 style='page-break-before:always'>
  <td height=13>Sat. May 22</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>9:00 pm ET </td>
  <td>NASCAR Sprint All-Star Race</td>
 </tr>
 <tr height=13>
  <td height=13>Charlotte Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. May 30</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>5:45 pm ET </td>
  <td>Coca-Cola 600</td>
 </tr>
 <tr height=13>
  <td height=13>Charlotte Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Jun 6</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Pocono 500</td>
 </tr>
 <tr height=13>
  <td height=13>Pocono Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Jun 13</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Heluva Good! Sour Cream Dips 400</td>
 </tr>
 <tr height=13>
  <td height=13>Michigan International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Jun 20</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>3:00 pm ET </td>
  <td>Toyota/Save Mart 350</td>
 </tr>
 <tr height=13>
  <td height=13>Infineon Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Jun 27</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Lenox Industrial Tools 301</td>
 </tr>
 <tr height=13>
  <td height=13>New Hampshire Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Jul 3</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Coke Zero 400 Powered By Coca-Cola</td>
 </tr>
 <tr height=13>
  <td height=13>Daytona International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Jul 10</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>LifeLock.com 400</td>
 </tr>
 <tr height=13>
  <td height=13>Chicagoland Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Jul 25</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Brickyard 400</td>
 </tr>
 <tr height=13>
  <td height=13>Indianapolis Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Aug 1</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Pennsylvania 500</td>
 </tr>
 <tr height=13>
  <td height=13>Pocono Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Aug 8</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Heluva Good! Sour Cream Dips at The Glen</td>
 </tr>
 <tr height=13>
  <td height=13>Watkins Glen International</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Aug 15</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Carfax 400</td>
 </tr>
 <tr height=13>
  <td height=13>Michigan International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Aug 21</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Irwin Tools Night Race</td>
 </tr>
 <tr height=13>
  <td height=13>Bristol Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Sep 5</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Labor Day Classic 500</td>
 </tr>
 <tr height=13>
  <td height=13>Atlanta Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Sep 11</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>Richmond 400</td>
 </tr>
 <tr height=13>
  <td height=13>Richmond International Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Sep 19</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Sylvania 300</td>
 </tr>
 <tr height=13>
  <td height=13>New Hampshire Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Sep 26</td>
  <td></td>
 </tr>
 <tr height=13 style='page-break-before:always'>
  <td height=13>1:00 pm ET </td>
  <td>AAA 400</td>
 </tr>
 <tr height=13>
  <td height=13>Dover International Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Oct 3</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Price Chopper 400</td>
 </tr>
 <tr height=13>
  <td height=13>Kansas Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Oct 10</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>3:00 pm ET </td>
  <td>Pepsi Max 400</td>
 </tr>
 <tr height=13>
  <td height=13>Auto Club Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sat. Oct 16</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>7:30 pm ET </td>
  <td>NASCAR Banking 500</td>
 </tr>
 <tr height=13>
  <td height=13>Charlotte Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Oct 24</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>TUMS Fast Relief 500</td>
 </tr>
 <tr height=13>
  <td height=13>Martinsville Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Oct 31</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>AMP Energy 500</td>
 </tr>
 <tr height=13>
  <td height=13>Talladega Superspeedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Nov 7</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Lone Star 500</td>
 </tr>
 <tr height=13>
  <td height=13>Texas Motor Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Nov 14</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>3:00 pm ET </td>
  <td>Arizona 500</td>
 </tr>
 <tr height=13>
  <td height=13>Phoenix International Raceway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>Sun. Nov 21</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13>1:00 pm ET </td>
  <td>Ford 400</td>
 </tr>
 <tr height=13>
  <td height=13>Homestead-Miami Speedway</td>
  <td></td>
 </tr>
 <tr height=13>
  <td height=13 colspan=2></td>
 </tr>
</table>

<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    