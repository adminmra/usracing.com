{literal}
    <script language="javascript">
	var username = Check_Cookie(rot13('UTMU'));
	if(username && gup('megamenu')){
	window.location="http://www.usracing.com/sportsbook?line=Golf";
	}
	</script>
	{/literal}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded active-trail"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li class="leaf active-trail"><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting" class="active">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



{include file='banners/banner4.tpl'}             
{include file='banners/banner3.tpl'}
{include file='banners/banner2.tpl'}
{include file='banners/banner1.tpl'}    


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join"><img src="/themes/images/sports/golf_bets.jpg" alt="Bet on Golf" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Bet on Golf | Ryder Cup, The Masters, PGA betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<span class="DarkBlue"><h3>Golf Betting - Bet on Golf</h3></span>
<br />

<p>Ready to tee off?&nbsp; US Racing is your best caddy for betting on Golf.&nbsp;</p>

<p>Whether you want to bet on Tiger Woods to win all the Majors or stroke line betting between 2 golfers, our sportsbook offers every conceivable choice for betting on golf. The annual PGA tour heats up when you can place a bet online and then watch the match.</p>

<p>Whether you have a great handicap or a decent drive-- get on the golf betting at US Racing.</p>
<p>&nbsp;</p>
<tr>          
          
<div class="headline"><h1><td class="data">2010 PGA TOUR schedule</td></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


</tr>
				<tr>
			<td>
				<table width="100%">
				<tbody><tr>
	<h3><td class="data" style="text-align: left;">Date</td></h3>

	<h3><td class="data" style="text-align: left;">Tournament</td></h3>
	<h3><td class="data" style="text-align: left;">Location</td></h3>
	<h3><td class="data" style="text-align: left;">Purse</td></h3>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Jan. 4-10</td>
	<td style="text-align: left;">SBS Championship</td>

	<td style="text-align: left;">Kapalua, Hawaii</td>
	<td style="text-align: left;">$5,600,000 </td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Jan. 11-17</td>
	<td style="text-align: left;">Sony Open in Hawaii</td>
	<td style="text-align: left;">Honolulu, Hawaii</td>

	<td style="text-align: left;">$5,500,000 </td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Jan. 18-24</td>
	<td style="text-align: left;">Bob Hope Classic</td>
	<td style="text-align: left;">La Quinta, Calif.</td>
	<td style="text-align: left;">$5,000,000</td>

</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Jan. 25-31</td>
	<td style="text-align: left;">San Diego Open</td>
	<td style="text-align: left;">La Jolla, Calif.</td>
	<td style="text-align: left;">$5,300,000 </td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Feb. 1-7</td>

	<td style="text-align: left;">Northern Trust Open</td>
	<td style="text-align: left;">Pacific Palisades, Calif.</td>
	<td style="text-align: left;">$6,400,000 </td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Feb. 8-14</td>
	<td style="text-align: left;">AT&amp;T Pebble Beach National Pro-Am</td>

	<td style="text-align: left;">Pebble Beach, Calif.</td>
	<td style="text-align: left;">$6,200,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Feb. 15-21</td>
	<td style="text-align: left;">World Golf Championships-Accenture Match Play Champ.<br>Mayakoba Golf Classic at Riviera Maya-Cancun</td>
	<td style="text-align: left;">Marana, Ariz.<br>Playa del Carmen, Mexico</td>

	<td style="text-align: left;">$8,500,000<br>$3,600,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Feb. 22-28</td>
	<td style="text-align: left;">Waste Management Phoenix Open</td>
	<td style="text-align: left;">Scottsdale, Ariz.</td>
	<td style="text-align: left;">$6,000,000</td>

</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">March 1-7</td>
	<td style="text-align: left;">The Honda Classic</td>
	<td style="text-align: left;">Palm Beach Gardens, Fla.</td>
	<td style="text-align: left;">$5,600,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">March 8-14</td>

	<td style="text-align: left;">World Golf Championships-CA Championship<br>Puerto Rico Open presented by Banco Popular</td>
	<td style="text-align: left;">Doral, Fla.<br>Rio Grande, Puerto Rico</td>
	<td style="text-align: left;">$8,500,000<br>$3,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">March 15-21</td>

	<td style="text-align: left;">Transitions Championship</td>
	<td style="text-align: left;">Palm Harbor, Fla.</td>
	<td style="text-align: left;">$5,400,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">March 22-28</td>
	<td style="text-align: left;">Arnold Palmer Invitational presented by MasterCard</td>

	<td style="text-align: left;">Orlando, Fla.</td>
	<td style="text-align: left;">$6,000,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">March 29-April 4</td>
	<td style="text-align: left;">Shell Houston Open</td>
	<td style="text-align: left;">Humble, Texas</td>

	<td style="text-align: left;">$5,800,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">April 5-11</td>
	<td style="text-align: left;">The Masters</td>
	<td style="text-align: left;">Augusta, Ga.</td>
	<td style="text-align: left;">$7,000,000*</td>

</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">April 12-18</td>
	<td style="text-align: left;">Verizon Heritage</td>
	<td style="text-align: left;">Hilton Head Island, S.C.</td>
	<td style="text-align: left;">$5,700,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">April 19-25</td>

	<td style="text-align: left;">Zurich Classic of New Orleans</td>
	<td style="text-align: left;">Avondale, La.</td>
	<td style="text-align: left;">$6,400,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">April 26-May 2</td>
	<td style="text-align: left;">Quail Hollow Championship</td>

	<td style="text-align: left;">Charlotte, N.C.</td>
	<td style="text-align: left;">$6,500,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">May 3-9</td>
	<td style="text-align: left;">THE PLAYERS Championship</td>
	<td style="text-align: left;">Ponte Vedra Beach, Fla.</td>

	<td style="text-align: left;">$9,500,000*</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">May 10-16</td>
	<td style="text-align: left;">Valero Texas Open</td>
	<td style="text-align: left;">San Antonio, Texas</td>
	<td style="text-align: left;">$6,100,000</td>

</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">May 17-23</td>
	<td style="text-align: left;">HP Byron Nelson Championship</td>
	<td style="text-align: left;">Irving, Texas</td>
	<td style="text-align: left;">$6,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">May 24-30</td>

	<td style="text-align: left;">Crowne Plaza Invitational at Colonial</td>
	<td style="text-align: left;">Fort Worth, Texas</td>
	<td style="text-align: left;">$6,200,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">May 31-June 6</td>
	<td style="text-align: left;">the Memorial Tournament presented by Morgan Stanley</td>

	<td style="text-align: left;">Dublin, Ohio</td>
	<td style="text-align: left;">$6,000,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">June 7-13</td>
	<td style="text-align: left;">St. Jude Classic</td>
	<td style="text-align: left;">Memphis, Tenn.</td>

	<td style="text-align: left;">$5,600,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">June 14-20</td>
	<td style="text-align: left;">U.S. Open</td>
	<td style="text-align: left;">Pebble Beach, Calif.</td>
	<td style="text-align: left;">$7,500,000*</td>

</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">June 21-27</td>
	<td style="text-align: left;">Travelers Championship</td>
	<td style="text-align: left;">Cromwell, Conn.</td>
	<td style="text-align: left;">$6,000,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">June 28-July 4</td>

	<td style="text-align: left;">AT&amp;T National</td>
	<td style="text-align: left;">Newtown Square, Pa.</td>
	<td style="text-align: left;">$6,200,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">July 5-11</td>
	<td style="text-align: left;">John Deere Classic</td>

	<td style="text-align: left;">Silvis, Ill.</td>
	<td style="text-align: left;">$4,400,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">July 12-18</td>
	<td style="text-align: left;">The Open Championship<br>Legends Reno-Tahoe Open</td>
	<td style="text-align: left;">St. Andrews, Scotland<br>Reno, Nev.</td>

	<td style="text-align: left;">$7,000,000<br>$3,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">July 19-25</td>
	<td style="text-align: left;">RBC Canadian Open</td>
	<td style="text-align: left;">Etobicoke, Ontario</td>
	<td style="text-align: left;">$5,100,000</td>

</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">July 26-Aug. 1</td>
	<td style="text-align: left;">The Greenbrier Classic</td>
	<td style="text-align: left;">White Sulphur Springs, W. Va.</td>
	<td style="text-align: left;">$6,000,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Aug. 2-8</td>

	<td style="text-align: left;">World Golf Championships-Bridgestone Invitational<br>Turning Stone Resort Championship</td>
	<td style="text-align: left;">Akron, Ohio<br>Verona, N.Y.</td>
	<td style="text-align: left;">$8,500,000<br>$4,000,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Aug. 9-15</td>

	<td style="text-align: left;">PGA Championship</td>
	<td style="text-align: left;">Kohler, Wis.</td>
	<td style="text-align: left;">$7,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Aug. 16-22</td>
	<td style="text-align: left;">Wyndham Championship</td>

	<td style="text-align: left;">Greensboro, N.C.</td>
	<td style="text-align: left;">$5,100,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Aug. 23-29</td>
	<td style="text-align: left;">The Barclays</td>
	<td style="text-align: left;">Paramus, N.J.</td>

	<td style="text-align: left;">$7,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Aug. 30-Sept. 6</td>
	<td style="text-align: left;">Deutsche Bank Championship</td>
	<td style="text-align: left;">Norton, Mass.</td>
	<td style="text-align: left;">$7,500,000</td>

</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Sept. 6-12</td>
	<td style="text-align: left;">BMW Championship</td>
	<td style="text-align: left;">Lemont, Ill.</td>
	<td style="text-align: left;">$7,500,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Sept. 20-26</td>

	<td style="text-align: left;">THE TOUR Championship presented by Coca-Cola</td>
	<td style="text-align: left;">Atlanta, Ga.</td>
	<td style="text-align: left;">$7,500,000 </td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Sept. 27-Oct. 3</td>
	<td style="text-align: left;">Ryder Cup<br>Viking Classic</td>

	<td style="text-align: left;">Newport, Wales<br>Madison, Miss.</td>
	<td style="text-align: left;">&nbsp;<br>$3,600,000</td>
</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Oct. 11-17</td>
	<td style="text-align: left;">Frys.com Open</td>
	<td style="text-align: left;">San Martin, Calif.</td>

	<td style="text-align: left;">$5,000,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Oct. 18-24</td>
	<td style="text-align: left;">Justin Timberlake Shriners Hospitals for Children Open</td>
	<td style="text-align: left;">Las Vegas, Nev.</td>
	<td style="text-align: left;">$4,300,000</td>

</tr>
<tr class="tourStoryTableRow">
	<td style="text-align: left;">Nov. 1-7</td>
	<td style="text-align: left;">World Golf Championships-HSBC Champions</td>
	<td style="text-align: left;">Shanghai, China</td>
	<td style="text-align: left;">$7,000,000</td>
</tr>
<tr class="tourStoryTableRowAlt">
	<td style="text-align: left;">Nov. 8-14</td>

	<td style="text-align: left;">Children's Miracle Network Classic</td>
	<td style="text-align: left;">Lake Buena Vista, Fla.</td>
	<td style="text-align: left;">$4,700,000</td>
</tr>
				</tbody></table>        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    