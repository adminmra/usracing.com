{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Sports Lines</h2>
  



    <ul class="menu"><li class="expanded first"><a href="/beton/race-of-the-week" title="Bet on Horses">BET ON HORSES</a><ul class="menu"><li class="leaf first"><a href="/beton/race-of-the-week" title="Horse Race of the Week">HORSE RACE OF THE WEEK BETS</a></li>
<li><a href="/beton/kentuckyderby" title="Bet on Kentucky Derby - Run for the Roses at Churchill Downs">BET ON KENTUCKY DERBY</a></li>
<li><a href="/beton/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">KENTUCKY DERBY FUTURE WAGER</a></li>
<li><a href="/beton/preakness-stakes" title="Bet on Preakness Stakes at US Racing">BET ON PREAKNESS STAKES</a></li>
<li><a href="/beton/belmontstakes" title="Bet on Belmont Stakes at US Racing">BET ON BELMONT STAKES</a></li>
<li><a href="/beton/breederscup" title="Bet on Breeders&#039; Cup at US Racing">BET ON BREEDERS CUP</a></li>
<li><a href="/beton/dubaiworldcup" title="Bet on the Dubai World Cup - Richest Horse Race">DUBAI WORLD CUP</a></li>
<li><a href="/beton/matchraces" title="Match Races at US Racing">MATCH RACES</a></li>
<li><a href="/beton/roadtoroses" title="Road to the Kentucky Derby">ROAD TO THE KENTUCKY DERBY</a></li>
<li><a href="/beton/breederscup_challenge" title="Breeders Cup Challenge">BREEDERS CUP CHALLENGE</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
</ul></li>
<li class="expanded"><a href="/beton/football" title="Bet on Football">BET ON FOOTBALL</a><ul class="menu"><li class="leaf first"><a href="/beton/nfl" title="Bet on NFL Football">BET ON NFL</a></li>
<li><a href="/beton/college-football" title="Bet on College Football">BET ON COLLEGE</a></li>
</ul></li>
<li class="expanded active-trail"><a href="/beton/baseball" title="Bet Sports">BET SPORTS</a><ul class="menu"><li class="leaf first"><a href="/beton/baseball" title="Bet on Baseball">BASEBALL</a></li>
<li><a href="/beton/basketball" title="Bet on Basketball">BASKETBALL BETTING</a></li>
<li><a href="/beton/collegebasketball" title="NCAA College Basketball Betting">NCAA BASKETBALL</a></li>
<li><a href="/beton/hockey" title="Bet on Hockey | NHL Betting Online">BET ON HOCKEY</a></li>
<li><a href="/beton/boxing" title="Bet on HBO and Showtime Boxing">BOXING</a></li>
<li><a href="/beton/mma" title="UFC, MMA bet on Mixed Martial Arts">MIXED MARTIAL ARTS</a></li>
<li><a href="/beton/golf" title="Bet on Golf | Ryder Cup, The Masters, PGA betting">GOLF BETTING</a></li>
<li><a href="/beton/nascar" title="NASCAR Betting Autoracing">NASCAR</a></li>
<li><a href="/beton/soccer" title="Soccer Betting">SOCCER BETTING</a></li>
<li class="leaf last active-trail"><a href="/beton/tennis" title="Tennis Betting at Wimbledon and Majors" class="active">TENNIS</a></li>
</ul></li>
<li class="expanded"><a href="/beton/oscars" title="Bet on Politics, Entertainment and More!">BET MORE</a><ul class="menu"><li class="leaf first"><a href="/beton/oscars" title="Bet on the Academy Awards and Oscars">ACADEMY AWARDS</a></li>
<li><a href="/beton/politics" title="Political Betting">POLITICS</a></li>
<li><a href="/beton/reality-tv" title="Bet on TV and Reality Shows">REALITY SHOWS</a></li>
</ul></li>
<li><a href="/promotions" title="Promotions and Specials">PROMOTIONS AND SPECIALS</a></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



{include file='banners/banner4.tpl'}             
{include file='banners/banner3.tpl'}
{include file='banners/banner2.tpl'}
{include file='banners/banner1.tpl'}    


          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">




    <div><a href="/join" title="Bet on Tennis"><img src="/themes/images/sports/tennis_bets.jpg" alt="Tennis Bets" width="705" height="118" /></a></div>  </div>

  

<!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Tennis Betting at Wimbledon and Majors</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Tennis Betting - Bet on Tennis</h2></div>
<br />
<p>Bet on your favorite players at all the major and minor tennis tournaments in the world. Whether it is Wimbeldon or the US Open-- US Racing consistently offers the best odds for futures tennis bets as well as head-to-head match up bets.</p><p>Place you tennis bets online and watch from courtside to see the action.</p>

<p>&nbsp;</p>
<strong>2010 Tennis Tournaments Schedule</strong>

<p>&nbsp;</p>

<p>January 2010<br />
  10th - 17th October The LTA AEGON Men’s &amp; Women’s Pro-Series, Scotstoun Leisure Centre, Scotstoun, Glasgow, Scotland.<br />
  18th - 31st Australian Open, Melbourne Park, Melbourne, Australia.<br />
  23rd - 27th ITF: Tennis Seniors New Zealand Nationals, Hamilton, New Zealand.<br />
  23rd - 30th ITF: British Open Veterans' Indoor Championships, Dudley, Great Britain.<br />
  24th - 31st ITF: 3rd International Senior Indoors, Wegberg bei Monchengladbach.<br />
  24th - 31st ITF: Babolat World Tennis Classic, Rancho Mirage, United States.<br />
  25th - 31st ITF: USTA Super Senior Grand Prix, Academia, Naples, Sanchez-Casal, United States.<br />
  31st - 6th February AEGON British Tour, Graves Tennis &amp; Leisure Centre, Graves.<br /></p>
<p>February 2010<br />
  1st - 7th ATP SA Tennis Open, Johannesburg, South Africa.<br />
  1st - 7th ATP PBZ Zagreb Indoors, Zagreb, Croatia.<br />
  1st - 7th ATP Movistar Open, Santiago, Chile.<br />
  3rd - 7th ITF Les Grandes Dames - The Checket Cup, Fort Lauderdale, United States.<br />
  4th - 7th ITF International Indoor Seniors Tennis Championships, Budapest, Hungary.<br />
  5th - 7th ITF 2010 Wilson/Mayfair ITF Senior Circuit 2, Markham, Canadate.<br />
  6th - 13th ITF Copa Pajarito Reyes y Yola Ramirez, Mexico City, Mexico.<br />
  6th - 7th 2010 Fed Cup World Group II, 1st Round: Spain v Australia, TBC.<br />
  6th - 7th 2010 Fed Cup World Group 1st Round: Serbia v Russia , TBC.<br />
  6th - 7th 2010 Fed Cup World Group II, 1st Round: Slovak Republic v China, TBC.<br />
  6th - 7th 2010 Fed Cup World Group 1st Round: Czech Republic v Germany, TBC.<br />
  6th - 7th 2010 Fed Cup World Group II, 1st Round: Belgium v Poland , TBC.<br />
  6th - 7th 2010 Fed Cup World Group 1st Round: France v United States, TBC.<br />
  6th - 7th 2010 Fed Cup World Group 1st Round: Italy v Ukraine , TBC.<br />
  6th - 7th 2010 Fed Cup World Group II, 1st Round: Estonia v Argentina, TBC.<br />
  8th - 14th ATP SAP Open, San Jose, U.S.A..<br />
  8th - 14th Sony Ericsson WTA Tour: PTT Pattaya Women's Open , Pattaya City, Thailand.<br />
  8th - 14th Sony Ericsson WTA Tour: OPEN GDF SUEZ, Paris, France.<br />
  8th - 14th ATP ABN AMRO World Tennis Tournament, Rotterdam, The Netherlands.<br />
  8th - 14th ATP Brasil Open, Costa do Sauipe, Brazil.<br />
  15th - 22nd ITF est Coast Super-Senior Grand Prix, Meadows CC, Sarasota, United States.<br />
  15th - 21st Sony Ericsson WTA Tour: Copa Sony Ericsson Colsanitas, Bogota.<br />
  15th - 21st ATP Regions Morgan Keegan Championships, Memphis, USA.<br />
  15th - 21st ITF USTA National Men's 55 Indoor Championships, Boise, United States.<br />
  15th - 21st Sony Ericsson WTA Tour: Regions Morgan Keegan Champions &amp; the Cellular South Cup, Memphis, Tennessee.<br />
  15th - 21st ATP Copa Telmex, Buenos Aires, Argentina.<br />
  15th - 21st ATP Open 13, Marseille, France .<br />
  15th - 20th AEGON British Tour .<br />
  15th - 21st Sony Ericsson WTA Tour: Barcaly's Dubai Tennis Championships, Dubai, UAE.<br />
  20th - 23rd ATP Champions Tour, Delray Beach, Florida, USA .<br />
  22nd - 28th ATP Delray Beach International Tennis Championships, Delray Beach, USA.<br />
  22nd - 28th ATP Abierto Mexicano Telcel, Acapulco, Mexico.<br />
  22nd - 28th ATP Barclays Dubai Tennis Championships, Dubai, UAE.<br />
  22nd - 3rd March Sony Ericsson WTA Tour: Abierto Mexicano TELCEL presentado por HSBC , Acapulco, Mexico.<br />
  22nd - 27th AEGON British Tour Swansea, Swansea Tennis Centre, Swansea .<br />
  22nd - 3rd March Sony Ericsson WTA Tour: Malaysia Classic 2010, Kuala Lumpur, Malaysia.<br />
  25th - 28th ITF Scottish Seniors Indoor Championships 2010, Glasgow, Great Britain.<br />
  27th - 5th March ITF Swedish National Seniors Championship, Stockholm, Sweden.<br />
  </p>
<p>March 2010<br />
  1st - 7th Davis Cup First Round, TBC.<br />
  1st - 7th ITF USTA National Women's Clay Court Championship, Houston, United States.<br />
  1st - 6th ITF USTA National Men's 40 Indoor Championship, Park City, United States.<br />
  1st - 7th Sony Ericsson WTA Tour: Monterrey Open, Monterrey, Mexico.<br />
  1st - 6th AEGON British Tour, Bolton Arena, Bolton.<br />
  1st - 6th ITF 2nd UAE 1 - ITF Seniors Championship, Abu Dhabi, United Arab Emirates.<br />
  4th - 7th ITF 1st Spring Senior Open Tennis Tournament, Marbella Don Carlos, Spain.<br />
  4th - 13th ITF Tournoi International Veterans Menton, Menton, France.<br />
  5th - 8th ITF 2010 Victorian Seniors State Championship, Melbourne, Australia.<br />
  5th - 7th ITF 2010 Wilson/Mayfair ITF Senior Circuit 3, Markham, Canada.<br />
  6th - 10th ITF 2010 ITF South African Seniors Nationals, Cape Town, South Africa.<br />
  7th - 13th AEGON British Tour, Sutton Tennis Academy, Sutton.<br />
  8th - 15th ATP BNP Paribas Open, Indian Wells, USA.<br />
  8th - 15th ITF Raymond James Super-Senior Grand Prix, St Petersburg CC, United States.<br />
  8th - 13th ITF 2nd Spring Senior Open Tennis Tournament, Marbella Puente Romano, Spain.<br />
  8th - 14th Sony Ericsson WTA Tour: BNP Paribas Open, Indian Wells, USA.<br />
  8th - 13th ITF 1st UAE 2 - ITF Seniors Championship, Fujairah, United Arab Emirates.<br />
  9th - 13th ATP Champions Tour, Zurich, Switzerland .<br />
  9th - 14th ITF Triple Open, Logoisk, Belarus.<br />
  15th - 20th ITF Seniors Brasila 2010, Brasilia, Brazil.<br />
  18th - 21st ATP Champions Tour, Bogota, Colombia .<br />
  19th - 21st ITF Copa Ibérica 1, Lisbon/Estoril, Portugal.<br />
  19th - 22nd 3rd Czech International Seniors Indoors Championship, ITF Republic Pribram, Czech.<br />
  22nd - 27th ITF GD Tennis Senior 2010, Antalya, Turkey.<br />
  22nd - 29th Sony Ericsson WTA Tour: Sony Ericsson Open, Miami, USA.<br />
  22nd - 28th ITF Gunther Kadel Memorial, Paguera/Mallorca, Spain.<br />
  22nd - 28th ITF USTA National Men's 70 &amp; 75 Indoor Championship, Houston, United States.<br />
  22nd - 28th ITF 3rd International Seniors Tennis Tournament, Estoril/Lisboa, Portugal.<br />
  22nd - 29th ATP Sony Ericsson Open, Miami, USA.<br />
  24th - 28th ITF Maadi International Seniors Tournament, Cairo, Egypt.<br />
  27th - 1st April ITF International Agadir Tennis Week, Agadir, Morocco.<br />
  27th - 2nd April ITF USTA National Men's 65 Clay Court Championship, New Orleans, United States.<br />
  </p>
<p>April 2010<br />
  2nd - 5th ITF 40th ACT Seniors Tennis Championship, Canberra, Australia.<br />
  3rd - 11th ITF 1 Torneo Internazionale di Roma, Rome, Italy.<br />
  4th - 10th ITF Maccabi Tzafon 1, Tel Aviv, Israel.<br />
  5th - 11th ATP US Men’s Clay Court Championship, Houston, USA.<br />
  5th - 11th Sony Ericsson WTA Tour: MPS Group Championships, Ponte Vedra Beach, USA.<br />
  5th - 11th Sony Ericsson WTA Tour: Andalucia Tennis Experience, Marbella, Spain.<br />
  5th - 11th ATP Grand Prix Hassan II, Casablanca, Morocco.<br />
  5th - 11th TBD, Warsaw, Poland.<br />
  6th - 11th ITF 8th International Seniors Championship, Palafrugell, Spain.<br />
  8th - 11th ATP Champions Tour, Sao Paulo, Brazil .<br />
  10th - 17th ITF 17eme Tournoi International de Tennis Veterans, Bagnoles de L'Orne, France.<br />
  10th - 16th ITF USTA National Women's Clay Court Championship, Huntsville, United States.<br />
  12th - 17th ITF Torneo Internacional de la Republica, Buenos Aires, Argentina.<br />
  12th - 18th ATP Monte-Carlo Rolex Masters , Monte-Carlo, Monaco.<br />
  12th - 18th Sony Ericsson WTA Tour: Barcelona Ladies Open, Barcelona, Spain.<br />
  12th - 16th ITF Aldiana Tunisia Senior Open, Nabeul, Tunisia.<br />
  12th - 18th Sony Ericsson WTA Tour: Family Circle Cup, Charleston, USA.<br />
  12th - 17th AEGON British Tour, Tipton Sports Academy, Tipton.<br />
  12th - 17th ITF 7th International WILSON Senior Open by Club Ali Bey, Manavgat/Antalya, Turkey.<br />
  13th - 24th ITF Macedonia 2010, Thessaloniki, Greece.<br />
  15th - 18th ITF 6th AOTK Seniors Cup, Tallinn, Estonia.<br />
  16th - 25th ITF VLTBC Masters Indoor Tennis Championships, Vancouver, Canada.<br />
  16th - 18th ATP Champions Tour, Barcelona, Spain .<br />
  17th - 25th ITF 41st International Championships of Italy for Seniors, Alassio, Italy.<br />
  19th - 25th ATP Open Sabadell Atlantico, Barcelona, Spain.<br />
  19th - 24th ITF Campeonato del Sur de la Republica, Buenos Aires, Argentina.<br />
  25th - 2nd May ITF USTA National Men's 45 Clay Court Championship, Fort Lauderdale, United States.<br />
  26th - 2nd May Sony Ericsson WTA Tour: Grand Prix De SAR La Princesse Lalla Meryem, Fes, Morocco.<br />
  26th - 2nd May Sony Ericsson WTA Tour: Porsche Tennis Grand Prix, Stuttgart, Germany.<br />
  26th - 2nd May ATP Internazionali BNL d’Italia, Rome, Italy.<br />
  26th - 1st May ITF Torneo Internacional Omar Pabst, Santiago, Chile.<br />
  </p>
<p>May 2010<br />
  1st - 11th ITF 1st T.C. Napoli ITF Seniors, Napoli, Italy.<br />
  1st - 4th ATP Champions Tour, Chengdu, China .<br />
  1st - 8th ITF Cyprus Seniors Cup, Larnaca, Cyprus.<br />
  2nd - 9th ITF Georgian Open, Tbilisi, Georgia.<br />
  3rd - 9th Sony Ericsson WTA Tour: Internazionali BNL d'Italia, Rome, Italy.<br />
  3rd - 9th ATP Estoril Open, Estoril, Portugal.<br />
  3rd - 9th Sony Ericsson WTA Tour: Estoril Open, Estoril, Portugal.<br />
  3rd - 9th ATP BMW Open, Munich, Germany.<br />
  3rd - 9th ATP Serbia Open, Belgrade, Serbia.<br />
  6th - 8th ATP Champions Tour, Luxembourg .<br />
  7th - 16th ITF Adriatic International Seniors Championship, Milano Marittima-Cervia, Italy.<br />
  8th - 16th ITF USTA National Women's Hard Court Championship, La Jolla, United States.<br />
  8th - 14th ITF Siauliai Open 2010, Siauliai, Lithuania.<br />
  8th - 11th ITF SCORP Michalovce Senior Cup, Michalovce, Slovakia.<br />
  10th - 15th AEGON British Tour, Queenswood Tennis Centre, Hatfield.<br />
  10th - 15th ITF USTA National Men's Indoor Championship, Vancouver, United States.<br />
  10th - 16th ATP Mutua Madrileña Madrid Open, Madrid, Spain.<br />
  10th - 16th Sony Ericsson WTA Tour: Mutua Madrilena Madrid Open, Madrid, Spain.<br />
  13th - 16th ITF Scottish Seniors Open Championship, Glasgow, Great Britain.<br />
  15th - 29th ITF Acropolis Cup, Athens Greece.<br />
  17th - 23rd Sony Ericsson WTA Tour: Polsat Warsaw Open, Warsaw, Poland.<br />
  17th - 22nd ITF USTA National Men' Hard Court Championship, Santa Fe, United States.<br />
  17th - 23rd Sony Ericsson WTA Tour: Internationaux de Strasbourg, Strasbourg, France.<br />
  17th - 23rd ATP Interwetten Austrian Open Kitzbühel, Kitzbühel, Austria.<br />
  17th - 23rd ATP ARAG ATP World Team Championship, Düsseldorf, Germany.<br />
  17th - 21st ITF International Seniors Championship, Karlovy Vary, Czech Republic.<br />
  19th - 23rd ITF Seniors Federation Cup, Bucharest, Romania.<br />
  19th - 23rd ITF USTA National Women's Indoor Championship, Homewood, United States.<br />
  21st - 23rd ITF 32nd International Seniors Tournament of Slovenia, Rogaska Slatina, Slovenia.<br />
  22nd - 30th ITF Memorial Franco Zampori, Milano San Felice, Italy.<br />
  23rd - 29th ITF GMP CUP 2010, Umag, Croatia.<br />
  23rd - 28th AEGON British Tour, Esporta Cardiff (Welsh NTC), Cardiff.<br />
  24th - 6th June French Open, Roland Garros, Paris.<br />
  24th - 31st ITF Pacific Coast Seniors/Family Tournament, Berkeley, United States.<br />
  24th - 29th ITF Campeonato Internacional de Villa Maria, Villa Maria, Argentina.<br />
  25th - 30th ITF USTA National Women's Indoor Championship, Overland Park, United States.<br />
  28th - 30th ITF Palic Seniors Open, Palic, Serbia.<br />
  29th - 6th June ITF Memorial Mario Renosto, Trieste, ITaly.<br />
  29th - 5th June ITF British Open Veterans' Clay Court Championships, Bournemouth, Dorset, Great Britain.<br />
  31st - 5th June AEGON British Tour, City of Nottingham TC, Nottingham.<br />
  31st - 6th June ITF Vilcabamba Open, Quito, Ecuador.<br />
  31st - 6th June ITF Istanbul Seniors Cup, Tarabya/Istanbul, Turkey.<br />
</p>
<p>June 2010<br />
  1st - 6th ITF Belarus Open, Minsk, Belarus.<br />
  1st - 6th ITF National Seniors Championship, Campina, Romania.<br />
  2nd - 6th ITF Alpe-Adria Seniorenturnier, Klagenfurt, Austria.<br />
  5th - 10th ITF Vinkovci Cup 2010, Vinkovci, Croatia.<br />
  5th - 12th ITF Spanish National Seniors Championship, Murcia, Spain.<br />
  6th - 12th ITF Tenis Senior Terrazas Ciudad, Lima, Peru.<br />
  6th - 12th ITF International Seniors de La Baule, La Baule, France.<br />
  7th - 12th AEGON British Tour, Hallamshire T&amp;SC, Sheffield.<br />
  7th - 13th ITF 27th Int. Austrian Championships &quot;Werzer Generali Cup&quot;, Pörtschach, Austria.<br />
  7th - 13th ATP Gerry Weber Open, Halle, Germany.<br />
  7th - 13th ATP AEGON Championships , London, England.<br />
  7th - 13th Sony Ericsson WTA Tour: AEGON Classic , Birmingham, England.<br />
  7th - 13th July ITF USTA National Men's 45 Hard Court Championship, Westlake Village, United States.<br />
  11th - 14th ITF 2010 Australian Tennis Seniors Claycourt, Melbourne, Australia.<br />
  11th - 14th ITF JUF Open 2010, Skopje, Macedonia, Greece.<br />
  12th - 19th ITF T.C.J.A St Malo, Saint-Malo, France.<br />
  12th - 19th ITF Memorial Nicola de Dominicis, Napoli, Italy.<br />
  14th - 20th ATP Ordina Open, 's-Hertogenbosch, The Netherlands.<br />
  14th - 20th Sony Ericsson WTA Tour: Ordina Open, 's-Hertogenbosch, Netherlands.<br />
  14th - 20th Sony Ericsson WTA Tour: AEGON International, Eastbourne, England.<br />
  14th - 20th ATP AEGON International, Eastbourne, England.<br />
  14th - 20th ITF USTA National Men's 65 &amp; 70 Hard Court Championships, Irvine.<br />
  17th - 20th ITF 1st Marina Portoroz Cup, Porotroz, Slovenia.<br />
  18th - 20th ITF Czech Seniors Grand Prix, Prague, Praha, Czech Republic.<br />
  19th - 23rd ITF Croatian National Championship, Zagreb, Croatia.<br />
  21st - 4th July The All England Lawn Tennis Championships, Wimbledon, Wimbledon, England.<br />
  21st - 25th ITF XIV Internacionales de Espana &quot;Memorial Simon Mateo&quot;, Barcelona 1 (Teia), Spain.<br />
  22nd - 27th ITF Corfu Seniors, Corfu, Greece.<br />
  22nd - 8th July ITF French Senior Championship, Paris, France.<br />
  25th - 11th July ITF 9th International Seniors Open, Arosa, Switzerland.<br />
  26th - 4th July ITF Belgian Championship for Seniors, TBC.<br />
  26th - 4th July ITF 50th International Seniors Polish Championship, Sopot, Poland.<br />
  27th - 1st July ITF Swedish Midsummer Senior Trophy, Jenkoping, Sweden.<br />
  27th - 2nd July ITF 39th Ernst Potten Turnier, Bad Herrenalb, Germany.<br />
  28th - 3rd July AEGON British Tour, City of Nottingham TC, Nottingham.<br />
  30th - 4th July ITF Tenis Club Pro Cup, Arad, Romania.<br />
</p>
<p>July 2010<br />
  2nd - 4th ITF Czech Seniors Grand Prix, Plzen, Czech Republic.<br />
  2nd - 4th ITF Copa Iberica 2, Madrid, Spain.<br />
  2nd - 5th ITF 2010 Queensland Tennis Seniors Championship, Gold Coast, Australia.<br />
  4th - 10th ITF USTA National Men's 50 Hard Court Championship, Santa Barbara, United States.<br />
  5th - 11th ATP Campbell’s Hall of Fame Tennis Championships, Newport, USA.<br />
  5th - 10th AEGON British Tour, Felixstowe LTC, Felixstowe.<br />
  5th - 11th Sony Ericsson WTA Tour: GDF SUEZ Grand Prix, Budapest, Hungary.<br />
  5th - 10th ITF Frinton-on-Sea 34th International Seniors Tournament, Frinton-on-Sea, Essex, Great Britain.<br />
  5th - 11th Sony Ericsson WTA Tour: Collector Swedish Open Women, Bastad, Sweden.<br />
  10th - 17th ITF 21st TIS + Cognac, Cognac, France.<br />
  11th - 17th ITF 18th Waldhaus International Seniors Open, Switzerland.<br />
  11th - 16th ITF Sardinia International Senior Tennis Championship 2010, Cagliari, Italy.<br />
  12th - 18th ATP Catella Swedish Open, Båstad, Sweden.<br />
  12th - 18th ITF 8h Muenster Open, Muenster, Germany.<br />
  12th - 17th ITF Seniors Angra Dos Reis, Angra Dos Reis, Brazil.<br />
  12th - 18th Sony Ericsson WTA Tour: Internazionali femminili di tennis di Palermo, Palermo, Italy.<br />
  12th - 18th Sony Ericsson WTA Tour: ECM Prague Open , Prague, Czech Republic.<br />
  12th - 18th ATP MercedesCup, Stuttgart, Germany.<br />
  15th - 18th ITF National Seniors Championships of , Usti nad Orlici, Czech Republic.<br />
  17th - 21st ITF 26th National Seniors Championships of Poland, TBC.<br />
  17th - 24th ITF Palanga Open 2010 Championship of Lithuanian Seniors Tennis, Palanga, Lithuania.<br />
  17th - 24th ITF Memorial Baci Galupo, Rapallo, Italy.<br />
  19th - 25th Sony Ericsson WTA Tour: NURNBERGER Gastein Ladies, Bad Gastein, Austria.<br />
  19th - 25th ATP Indianapolis Tennis Championships, Indianapolis, USA.<br />
  19th - 25th ITF USTA National Men's 65 Indoor Championship, Vancouver, United States.<br />
  19th - 25th ITF USTA National Women's Grass Court Championship, Forest Hills, United States.<br />
  19th - 25th Sony Ericsson WTA Tour: Banka Koper Slovenia Open, Portoroz, Slovenia.<br />
  19th - 25th ITF 5th International Senior Tennis Tournament Barcelona, Barcelona, Spain.<br />
  19th - 26th ITF Bodas de Oro, Guayaquil, Ecuador.<br />
  19th - 24th ITF Brussels Senior Tennis Cup, Brussels, Belgium.<br />
  20th - 25th ITF International Shot Seniors Open, Zeist, Netherlands.<br />
  21st - 25th ITF City of Nottingham Veterans Grass Court, Nottingham, Great Britain.<br />
  22nd - 25th ITF 13th International Schorndorf Open, Schorndorf, Germany.<br />
  22nd - 25th ITF 28th European Veterans Championships 35+, Rottach, Germany.<br />
  23rd - 25th ITF Czech Seniors Grand Prix, Ceske Budejovice, Czech Republic.<br />
  25th - 31st ITF 123rd Vancouver Island Grass Court Championship, Duncan, Canada.<br />
  26th - 1st August ATP Studena Croatia Open Umag, Umag, Croatia.<br />
  26th - 1st August Sony Ericsson WTA Tour: Bank Of The West Classic , Stanford, USA.<br />
  26th - 1st August ATP LA Tennis Open, Los Angeles, USA.<br />
  26th - 1st August Sony Ericsson WTA Tour: Istanbul Cup , Istanbul, turkey.<br />
  26th - 31st ITF Amsterdam Senior International Open, Amstelveen, Netherlands.<br />
  26th - 31st AEGON British Tour, Ilkley LT&amp;SC, Ilkley.<br />
  26th - 1st August ATP Allianz Suisse Open Gstaad, Gstaad, Switzerland .<br />
  26th - 1st August ITF 2nd International Open Seniors Championship, Bern, Switzerland.<br />
  26th - 31st ITF Knokke Zoute Senior Cup, Knokke-Heist, Belgium.<br />
  27th - 1st August ITF St. Georges Hill Veterans Open Tournament, Weybridge, Great Britain.<br />
  29th - 1st August ITF 26th Estonian Seniors Open, Tallinn, Estonia.<br />
  29th - 1st August ITF Hungarian National Seniors Championshipsi.m. Istvan Gulyas, Budapest, Hungary.<br />
  31st - 4th August ITF 21st International Seniors Tennis Tournament, Katowice, Poland.<br />
</p>
<p>August 2010<br />
  2nd - 7th ITF Tunbridge Wells Lawn Tennis Tournament, Tunbridge Wells, Kent, Great Britain.<br />
  2nd - 7th ITF Bavarian Senior Open, Munich, Germany.<br />
  2nd - 8th ATP Legg Mason Tennis Classic, Washington D.C., USA.<br />
  2nd - 8th Sony Ericsson WTA Tour: e-Boks Danish Open, Copenhagen, Denmark.<br />
  2nd - 7th AEGON British Tour, Tunbridge Wells LTC, Tunbridge Wells.<br />
  2nd - 8th Sony Ericsson WTA Tour: San Diego Open, San Diego, California, USA.<br />
  2nd - 8th ITF HealthCity Dutch Open Seniors Championships, Seniors Championships, Netherlands.<br />
  4th - 8th ATP Champions Tour, Graz, Austria .<br />
  5th - 8th ITF Crystal Cup Seniors Houstka - Stara Boleslav, Stara Boleslav, Czech Republic.<br />
  8th - 14th ITF British Veterans Grass Court Championships, London, Great Britain.<br />
  8th - 15th ITF German Senior Open 2010, Dachau, Germany.<br />
  8th - 14th ITF 4th International Seniors Championship of Russia, Moscow, Russia.<br />
  8th - 15th ITF 63rd ITF Swiss Seniors, Klosters, Switzerland.<br />
  9th - 15th ATP Rogers Cup, Toronto, Canada.<br />
  9th - 15th Sony Ericsson WTA Tour: Westerm &amp; Southern Financial Group Women's Open , Cincinnati, USA.<br />
  9th - 14th AEGON British Tour, West Worthing T&amp;SC, West Worthing.<br />
  10th - 15th ITF 49th International SeniorsTennis tournament TC Parkclub Igls, Igls, Austria.<br />
  10th - 15th ITF Hanko Scandinavian EB Championships, Hanko, Finland.<br />
  10th - 13th ATP Champions Tour, Algarve, Portugal .<br />
  11th - 15th ITF International Seniors Championships of Westphalia, Bielefeld, Germany.<br />
  13th - 22nd ITF National Austrian Championships Outdoor, Vienna, Austria.<br />
  14th - 19th ITF 17th International Seniors Tournament &quot;Black Diamonds&quot; Bytom, Bytom, Poland.<br />
  14th - 17th ITF Slovak Open, Spisska Nova Ves, Slovak Republic.<br />
  15th - 22nd ITF 23rd International Seniors Tournament of Bad Breisig, Bad Breisig, Germany.<br />
  16th - 21st AEGON British Tour, Sutton Tennis &amp; Squash Club, Sutton.<br />
  16th - 22nd ATP Western &amp; Southern Financial Group Masters, Cincinnati, USA.<br />
  16th - 22nd Sony Ericsson WTA Tour: Rogers Cup presented by National Bank , Montreal, Canada.<br />
  19th - 23rd ITF Senior Champion Trophy Summer, Kreuzlingen, Switzerland.<br />
  19th - 22nd ITF Bulgarian Open 2010, Sofia, Bulgaria.<br />
  20th - 22nd ITF Czech Seniors Grand Prix Olomouc, Olomouc, Czech Republic.<br />
  21st - 29th ITF 12th International Seniors de Grasse, Grasse, France.<br />
  22nd - 28th ITF 2010 Outdoor Senior Nationals, Vancouver, Canada.<br />
  22nd - 27th ITF 46th International Tennis Seniors Championship of Croatia, Opatjia, Croatia.<br />
  23rd - 28th ITF Torneo Tono Vera, Asuncion, Paraguay.<br />
  23rd - 29th ATP Pilot Pen Tennis, New Haven, USA.<br />
  23rd - 29th Sony Ericsson WTA Tour: Pilot Pen Tennis presented by Schick , New Haven, USA .<br />
  23rd - 29th ITF National Championship of the Netherlands, TBC.<br />
  23rd - 28th ITF Seniors Grand Prix de Liege, Rocourt-Liege, Belgium.<br />
  24th - 29th ITF Schoenbuch Cup, Schoenbuch, Germany.<br />
  24th - 29th ITF 21st International Veterans Championship, Constanta, Romania.<br />
  26th - 29th ITF Luben Genov Memorial 2010, Sofia, Bulgaria.<br />
  27th - 29th ITF Nordic Team Championship, Oslo, Norway.<br />
  28th - 5th September ITF 24th International Rothaus Cup, Hinterzarten, Germany.<br />
  30th - 4th September ITF South of England Championship, Eastbourne, Great Britain.<br />
  30th - 4th September ITF Tournoi International Seniors, Le Touquet, Le Touquet, France.<br />
  30th - 12th September US Open, Flushing Meadows, New York, USA.<br />
  30th - 4th September ITF Szczecin International Senior Open, Szczecin, Poland.<br />
  30th - 4th September ITF Salta International Tournament, Salta, Argentina.<br />
  30th - 4th September ITF 34th International Seniors Tennis Championship, Keszthely, Hungary.<br />
  31st - 5th September ITF 21st International Senior Tennis Championship, Lenk, Switzerland.<br />
</p>
<p>September 2010<br />
  4th - 11th ITF Open Veterans du Perigord, Perigueux, France.<br />
  5th - 10th ITF Zemek Cup Vrsar 2010, Vrsar, Croatia.<br />
  6th - 11th AEGON British Tour, Paddington Sports Club, Paddington.<br />
  6th - 10th ITF Senior Geneva Open, Geneva, Switzerland.<br />
  8th - 18th ITF National Seniors Championship, Milano Marittima, Italy.<br />
  8th - 12th ITF OP Prijedor, Prijedo, Bosnia.<br />
  9th - 12th ITF Gerrards Cross International, Buckinghamshire, Great Britain.<br />
  9th - 17th ITF Charles Simon ITF Seniors Championship 2010, Singapore, Singapore.<br />
  12th - 19th ITF 24eme Tournoi International Seniors de Beaulieu sur Mer, Beaulieu-sur-Mer, France.<br />
  13th - 19th Sony Ericsson WTA Tour: Bell Challenge, Quebec City, Canada.<br />
  13th - 19th Sony Ericsson WTA Tour: Guangzhou International Women's Open 2010, Guangzhou, China.<br />
  13th - 18th ITF Copa Santa Cruz Seniors, Santa Cruz, Bolivia.<br />
  13th - 19th Davis Cup Semifinal, TBC.<br />
  13th - 18th AEGON British Tour, City of Nottingham TC, Nottingham.<br />
  15th - 19th ITF 1st InternationalSenior Open, Madrid, Spain.<br />
  15th - 19th ITF Lida Autumn, Lida, Belarus.<br />
  15th - 19th ITF Woking International Veterans Tournament, Woking, Great Britain.<br />
  17th - 19th ITF Pancevo Seniors Open, Pancevo, Serbia.<br />
  17th - 20th ITF 2010 TSQ Sunshine Coast Seniors Championship, Tewantin, Australia.<br />
  18th - 22nd ITF 1st W. Lewandowski Open - Lodz 2010, Lodz, Poland.<br />
  20th - 25th ITF CoInternational Seniors Tournament Saint Tropez, Saint Tropez, France.<br />
  20th - 26th Sony Ericsson WTA Tour: Hansol Korea Open , Seoul, Korea.<br />
  20th - 26th Sony Ericsson WTA Tour: Tashkent Open , Tashkent, Uzbekistan.<br />
  20th - 26th ITF Burgenland Seniors Trophy, Oberpullendorf, Austria.<br />
  20th - 26th ATP Open de Moselle, Metz, France.<br />
  20th - 25th ITF Mendoza International Tournament, Mendoza, Argentina.<br />
  20th - 26th ATP BCR Open Romania , Bucharest, Romania.<br />
  22nd ITF 4th Macedonian Open, Macedonia, Greece.<br />
  23rd - 26th ATP Champions Tour, Paris, France .<br />
  25th - 27th ITF SEP Geraldton Seniors Tournament, Geraldton. Australia.<br />
  26th - 1st October ITF 5th International Adidas Bol Senior Open by Bluesun Hotels, Bol, Croatia.<br />
  27th - 3rd October ATP Malaysian Open, Kuala Lumpur, Kuala Lumpur, Malaysia.<br />
  27th Sony Ericsson WTA Tour: Toray Pan Pacific Open , Tokyo, Japan.<br />
  27th - 2nd October AEGON British Tour, North Wales Regional TC, Wrexham.<br />
  27th - 2nd October ITF Seniors Sao Paulo, Sao Paolo, Brazil.<br />
  27th - 3rd October ATP Thailand Open , Bangkok, Thailand .<br />
  27th - 2nd October ITF 32nd International Senior Tennis Championship, Marbella, Spain.<br />
</p>
<p>October 2010<br />
  1st - 4th ITF Tennis Seniors NSW State Championships, Sydney, Australia.<br />
  2nd - 10th ITF Tennis Seniors WA State Titles, North Perth, Australia.<br />
  3rd - 9th ITF Maccabi Tzafon 2, Tel Aviv, Israel.<br />
  3rd - 8th ITF 15th Mallorca Seniors Open, Mallorca, Spain.<br />
  4th - 10th ITF Copa Kurt Heyn, Mexico City, Mexico.<br />
  4th - 10th ATP Japan Open, Tokyo, Japan.<br />
  4th - 10th Sony Ericsson WTA Tour: China Open, Bejing, China.<br />
  4th - 10th ATP China Open, Beijing, China .<br />
  6th - 10th ITF Sporting Club International Seniors Tournament, Alexandria, Egypt.<br />
  7th - 11th ITF Senior Krka Sun Mix Cup, Otocec, Slovenia.<br />
  11th - 17th Sony Ericsson WTA Tour: Generali Ladies Linz, Linz, Austria.<br />
  11th - 16th ITF 4th Open International Senior Tennis Championship, Capdepera, Spain.<br />
  11th - 17th Sony Ericsson WTA Tour: HP Japan Women's Open Tennis 2010, Osaka, Japan.<br />
  11th - 17th ITF PSO Pakistan International Seniors Tennis Championship, Karachi, Pakistan.<br />
  11th - 17th ATP Shanghai ATP Masters 1000 Presented by Rolex, Shanghai, China.<br />
  11th - 15th ITF Merimbula Tennis Club Seniors Championships, Merimbula, Australia.<br />
  11th - 16th AEGON British Tou, Easton College TC, Norwich.<br />
  14th - 19th ITF Aldemar Crete Open, Crete -Hersonissos, Greece.<br />
  18th - 24th ATP If Stockholm Open, Stockholm, Sweden.<br />
  18th - 24th ATP Kremlin Cup, Moscow, Russia.<br />
  18th - 24th Sony Ericsson WTA Tour: Kremlin Cup, Moscow, Russia.<br />
  18th - 24th Sony Ericsson WTA Tour: BGL Luxembourg Open , Luxembourg.<br />
  21st - 24th ITF 1st Senior Open Tennis Tournament, Palma Nova/Calvia, Spain.<br />
  24th - 30th AEGON British Tour, Taunton Tennis Centre, Taunton.<br />
  25th - 30th ITF Copa Challenger Francisco Lombardi, Buenos Aires, Argentina.<br />
  25th - 31st Sony Ericsson WTA Tour: Sony Ericsson Championships , Doha, Qatar.<br />
  25th - 30th ITF Seniors Tournament - Chandigarh, Chandigarh, India.<br />
  25th - 31st ATP Bank Austria Tennis Trophy, Vienna, Austria.<br />
  25th - 31st ATP St. Petersburg Open, St. Petersburg, Russia.<br />
  25th - 31st ATP Grand Prix de Tennis de Lyon, Lyon, France.<br />
  27th - 1st November ITF Torneo Internacional de Veterans Acapulco, Acapulco, Mexico.<br />
  29th - 31st ITF Copa Iberica 3, Vilamoura, Portugal.<br />
  30th ITF Mildura Seniors Tennis Championship, Mildura, Australia.<br />
  31st - 6th November ITF 7th Isla de Tenerife Open, Los Realejos,Tenerife, Spain.<br />
</p>
<p>November 2010<br />
  1st - 7th Sony Ericsson WTA Tour: Commonwealth Bank Tournament of Champions, Bali, Indoneisia.<br />
  1st - 7th ATP Open de Tenis Comunidad Valenciana, Valencia, Spain .<br />
  1st - 7th ITF Internacional La Serena, La Serena, Chile.<br />
  1st - 7th ATP Davidoff Swiss Indoors Basel, Basel, Switzerland.<br />
  7th - 10th ITF 2010 Tennis Seniors SA State Championship, Unley, Australia.<br />
  8th - 14th ATP BNP Paribas Masters, Paris, France.<br />
  8th - 13th AEGON British Tour, Nuffield Tennis Academy, Hull.<br />
  8th - 13th ITF Seniors Tournament - Jorhat, Jorhat, India.<br />
  8th - 13th ITF Internacional Seniors Alfredo Trullenque, Santiago, Chile.<br />
  11th - 14th ITF Romai Cup International Senior Tennis Tournament, Budapest, Hungary.<br />
  12th - 14th ITF 2010 Wilson/Mayfair Senior Circuit 4, Markham, Canada.<br />
  13th - 20th AEGON British Tour, City of Nottingham TC, Nottingham.<br />
  15th - 21st ITF Asian Senior Open 2010, Chonburi, Thailand.<br />
  15th - 20th ITF Seniors Internacional de Porto Alegre &quot;Copa Yone Borba Dias&quot;, Porto Alegre, Brazil.<br />
  21st - 27th ATP Barclays ATP World Tour Finals, London, England.<br />
  22nd - 27th ITF III Open Internacional de Tenis Senior, Marbella, Spain.<br />
  22nd - 27th ITF Seniors Tournament -Delhi, New Dehli, India.<br />
  22nd - 26th ITF 17th Aldiana Cyprus Open, Larnaca, Cyprus.<br />
  22nd - 27th ITF Campeonato Nacional de Campeonato Nacional de, Naucalpan, Mexico.<br />
  22nd - 27th ITF 27th Náutico San Isidro International Tournament, Buenos Aires, Argentina.<br />
  29th - 4th December AEGON British Tour, Gosling Tennis Centre, Welwyn.<br />
  29th - 4th December ITF 6th International Senior Open Puente Romano, Marbella, Spain.<br />
  29th - 5th December Davis Cup Final, TBC.<br />
  29th - 4th December ITF Copa Otto Hauser, Uruguay.<br />
  30th - 5th December ATP Champions Tour, London, UK .<br />
</p>
<p>December 2010<br />
  2nd - 7th ITF Longboat Key Seniors Clay Courts, Longboat Key.<br />
  6th - 11th ITF 8th Seniors Tennis Murcia Turistica Open, Cartagena, Spain.<br />
  13th AEGON British Tour Masters, City of Nottingham TC, Nottingham.<br />
  14th - 19th ITF Rajabhau Ranade Memorial Seniors Tournament, Pune, India.<br />
  16th - 19th ITF 7th Snow Seniors Tennis Open Arosa, Arosa, Switzerland.</p>
              
            
            
                      
        
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    