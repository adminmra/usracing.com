
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

      <h2 class="title">Triple Crown</h2>
  



    <ul class="menu"><li class="expanded first"><a href="/triplecrown" title="Triple Crown">TRIPLE CROWN</a><ul class="menu"><li><a href="/triplecrown/freebet" title="Triple Your Winnings FREE BET Extravaganza!">FREE TRIPLE CROWN GIFT!</a></li>
</ul></li>
<li class="expanded active-trail"><a href="/kentuckyderby" title="Kentucky Derby">KENTUCKY DERBY</a><ul class="menu"><li class="leaf first"><a href="/kentuckyderby-betting" title="Kentucky Derby Betting">KENTUCKY DERBY BETTING</a></li>
<li><a href="/kentuckyderby-contenders" title="Kentucky Derby Contenders">KENTUCKY DERBY CONTENDERS</a></li>
<li><a href="/kentuckyderby-prepraces" title="Derby Prep Races">DERBY PREP RACES</a></li>
<li class="leaf active-trail"><a href="/kentuckyderby-odds" title="Kentucky Derby Odds" class="active">KENTUCKY DERBY ODDS</a></li>
<li><a href="/kentuckyderby-matchraces" title="Kentucky Derby Match Races">DERBY MATCH RACES</a></li>
<li><a href="/kentuckyderby-futurewager" title="Kentucky Derby Future Wager">DERBY FUTURE WAGER</a></li>
<li><a href="/kentuckyderby-pastwinners" title="Kentucky Derby Past Winners">KENTUCKY DERBY PAST WINNERS</a></li>
<li><a href="/stakes?name=Kentucky_Derby" title="Kentucky Derby Results">KENTUCKY DERBY RESULTS</a></li>
<li><a href="/beton/makememillionaire" title="Make Me A Millionaire Challenge">MAKE ME A MILLIONAIRE</a></li>
<li><a href="/triplecrown/freebet" title="Triple Your Winnings FREE BET Extravaganza!">FREE KENTUCKY DERBY GIFT</a></li>
<li><a href="/triplecrown/triple-crown-nominations" title="2011 Triple Crown Nominations List">TRIPLE CROWN NOMINATIONS</a></li>
</ul></li>
<li class="expanded"><a href="/preakness-stakes" title="Preakness Stakes">PREAKNESS STAKES</a><ul class="menu"><li class="leaf first"><a href="/preaknessstakes-betting" title="Preakness Stakes Betting">PREAKNESS STAKES BETTING</a></li>
<li><a href="/preaknessstakes-contenders" title="Preakness Stakes Contenders">PREAKNESS STAKES CONTENDERS</a></li>
<li><a href="/preaknessstakes-odds" title="Preakness Stakes Odds">PREAKNESS STAKES ODDS</a></li>
<li><a href="/preaknessstakes-pastwinners" title="Preakness Stakes Past Winners">PREAKNESS STAKES PAST WINNERS</a></li>
<li><a href="/stakes?name=Preakness_Stakes" title="Preakness Stakes Results">PREAKNESS STAKES RESULTS</a></li>
<li><a href="/triplecrown/freebet" title="Triple Your Winnings FREE BET Extravaganza!">FREE PREAKNESS STAKES GIFT</a></li>
</ul></li>
<li class="expanded last"><a href="/belmontstakes" title="Belmont Stakes">BELMONT STAKES</a><ul class="menu"><li class="leaf first"><a href="/belmontstakes-betting" title="Belmont Stakes Betting">BELMONT STAKES BETTING</a></li>
<li><a href="/belmontstakes-contenders" title="Belmont Stakes Contenders">BELMONT STAKES CONTENDERS</a></li>
<li><a href="/belmontstakes-odds" title="Belmont Stakes Odds">BELMONT STAKES ODDS</a></li>
<li><a href="/belmontpark" title="Belmont Park">BELMONT PARK</a></li>
<li><a href="/belmontstakes-pastwinners" title="Belmont Stakes Past Winners">BELMONT STAKES PAST WINNERS</a></li>
<li><a href="/stakes?name=Belmont_stakes" title="Belmont Stakes Results">BELMONT STAKES RESULTS</a></li>
<li><a href="/triplecrown/freebet" title="Triple Your Winnings FREE BET Extravaganza!">FREE BELMONT STAKES GIFT</a></li>
</ul></li>
</ul>  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-81" class="block block-block region-even even region-count-2 count-2">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/virtualderby" title="Virtual Derby 3D"><img src="/themes/images/banner-col-virtualderby.jpg" alt="Play Virtual Derby" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



<div id="block-block-54" class="block block-block region-odd odd region-count-3 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/beton/race-of-the-week"><img src="/themes/images/stakesraces/bigraceoftheweek.jpg" alt="Big Race of the Week Bets" title="Big Race of the Week Bets" width="225" height="223" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



{include file=banners/banner-stakesraces.tpl}





<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            <div id="block-block-26" class="block block-block region-odd odd region-count-1 count-5">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div class="logged-out"><a href="/join"><img src="/themes/images/stakesraces/kentucky_derby.jpg" alt="Kentucky Derby Bets" width="705" height="118" /></a></div>
    <div class="logged-in"><a href="/racebook"><img src="/themes/images/stakesraces/kentucky_derby.jpg" alt="Kentucky Derby Bets" width="705" height="118" /></a></div>
</div>

  

</div><!-- /block-inner, /block -->





                                      
          
<div class="headline"><h1>Kentucky Derby Odds for Churchill Downs in Louisville, Kentucky</h2>
  



        
<div>
<p><span><a title="Bet on the Kentucky Derby" href="/join"><img style="float: right; margin: 20px 0 30px 30px;" title="Kentucky Derby Betting" src="/themes/images/specialraces/kentucky_derby300x250.jpg" border="0" alt="Kentucky Derby Betting" align="right" /></a></span></p>
</div>
<p>&nbsp;</p>
          
          
<div class="headline"><h1><span class="DarkBlue">Kentucky Derby Odds</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>&nbsp;</p>
<p>US Racing offers various horse wager types for the Kentucky Derby.</p>
<p>2011 Kentucky Derby <a href="http://usracing.com/sportsbook">Odds to Win</a> are now available!</p>
<p>You can place&nbsp;<a title="Derby Future Wagers" href="http://usracing.com/kentuckyderby-futurewager">Derby Future Wagers</a>&nbsp;and Match Races&nbsp;in the&nbsp;<a title="sportsbook" href="http://usracing.com/sportsbook">sportsbook</a>, for other horse tickets like win, place, show, trifecta and more, please go to the&nbsp;<a title="racebook" href="http://usracing.com/racebook">racebook</a>.</p>
<p>You can bet on whether the winner of the Kentucky Derby will go on to win the Preakness Stakes and Belmont Stakes.&nbsp; Bet on the number of the saddle cloth!&nbsp; Horse vs Horse match races bets.&nbsp; These exotic wagers are available in the&nbsp;<a title="sportsbook" href="http://usracing.com/sportsbook">sportsbook</a>.</p>
<p>&nbsp;</p>
        
        

  </div>
              
            </div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->




<h2>
<span>2011 Kentucky Derby Odds</span>
<a style="float: right; display:block; width:115px; font-size: 12px;" title="Download Odds to Win PDF" href="/PDF/ahr-kd-odds.pdf">
<span style="float:left; cursor:pointer;">Download PDF</span>
<img style="float:right; margin:-6px 0 0 0;" width="22" border="0" src="/themes/images/pdf-icon.gif"></a>
</span>
</h2>
  



{*include file="inc/ahr_block_kd_odds.tpl"*}
{include file=includes/ahr_kd_odds_2011.tpl}


  

</div><!-- /block-inner, /block -->



          
        
       <!-- end: #col3 -->


      
    