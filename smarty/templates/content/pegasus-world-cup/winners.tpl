{* {include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"} *}

{include file='inc/left-nav-btn.tpl'}

{literal}
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

{literal}
<script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "SportsEvent",
  "name": "Pegasus World Cup 2022",
  "description": "Pegasus World Cup 2022 Winners at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022",
  "startDate": "2022-01-29T00:00",
  "endDate": "2022-01-29T23:59",
  "location": {
    "@type": "Place",
    "name": "Gulfstream Park Racing and Casino",
    "sameAs": "Gulfstream Park Racing and Casino",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "901 S Federal Hwy",
      "addressLocality": "Florida",
      "addressRegion": "FL",
      "postalCode": "3009",
      "addressCountry": "US"
    }
  }
}
</script>
{/literal}

<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>    

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (max-width: 364px) {
         .fixed_cta {
           font-size: 13px !important;
         }
       }

       @media(min-width: 501px) and (max-width: 648px) {
         .fixed_cta {
           font-size: 16px !important;
         }
       }

       @media(max-width: 800px) {
         .pwc {
           background: none;
         }
       }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

<!--------- NEW HERO  ---------------------------------------->

{* <div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl centerText">{$h1}</div> *}
{*  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}
  {* <div class="text text-md centerText">{$h2}
    <br>
      <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
  </div>
 </div> *} 
<!--------- NEW HERO END ----------------------------------------> 

 {* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-white.tpl"} *}
 {* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"} *}
 {include_php file="/home/ah/allhorse/public_html/pegasus/block-countdown.php"}

  <!------------------------------------------------------------------>
    {*include file="/home/ah/allhorse/public_html/kd/block-countdown-white.tpl"*}
  <section class="pwc kd usr-section" style="background-position: center;">
    <div class="container">
      <div class="kd_content">
      <!--  <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Odds"
            class="kd_logo img-responsive"></a>  -->
        <h1 class="kd_heading">{$h1}</h1>


{include file='/home/ah/allhorse/public_html/pegasus/salesblurb-winners.tpl'} 

{* <h2 class="table-title">Pegasus World Cup Winners</h2>  *}
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p> 
{include file='/home/ah/allhorse/public_html/pegasus/pastwinners.php'} 

<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p> 



 

{* <h2 class="table-title">Pegasus World Cup Turf Winners</h2> *}
{include file='/home/ah/allhorse/public_html/pegasus/pastwinners-turf.php'}
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p> 



<!-- Filly and Mare Turf Block -->
{include file='/home/ah/allhorse/public_html/pegasus/pastwinners-filly-mare-turf.php'}
<br>
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>



						

            

            

                      

        



<!-- ------------------------ content ends -------------------------- -->

<br><br>
        {* <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2020/01/25*12" class="btn-xlrg ">Bet Now on Pegasus World Cup</a>
        </p> *}


         
         
      </div>
    </div>
  </section>

  <!------------------------------------------------------------------>

{include file="/home/ah/allhorse/public_html/pegasus/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}
{*include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"*}
{*{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}*}
{*include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"*}
{*include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-derek.tpl"*}


  <!------------------------------------------------------------------>
