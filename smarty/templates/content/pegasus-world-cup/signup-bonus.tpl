{* {include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"} *}

{include file='inc/left-nav-btn.tpl'}

{literal}
<script type='application/ld+json'>
{
  "@context": "http://www.schema.org",
  "@type": "SportsEvent",
  "name": "Pegasus World Cup 2022",
  "description": "Pegasus World Cup 2022 Signup Bonus at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022",
  "startDate": "2022-01-29T00:00",
  "endDate": "2022-01-29T23:59",
  "location": {
    "@type": "Place",
    "name": "Gulfstream Park Racing and Casino",
    "sameAs": "Gulfstream Park Racing and Casino",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "901 S Federal Hwy",
      "addressLocality": "Florida",
      "addressRegion": "FL",
      "postalCode": "3009",
      "addressCountry": "US"
    }
  }
}
</script>
{/literal}

<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>    

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        .termsConditions {
            list-style: none;
        }

        li.terms {
            margin-bottom: 10px;
            list-style: square;
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            font-weight: 300;
        }

        @media (min-width: 480px) {
          li.terms {
            font-size: 24px;
          }
        }

         @media (min-width: 349px) and (max-width: 365px) {
           .usrHero .text.text-xl {
             font-size: 35px;
            }
        }

        @media (min-width: 377px) and (max-width: 480px) {
           .kd_heading {
             font-size: 40px;
           }
        }

         @media (min-width: 364px) and (max-width: 376px) {
           .kd_heading {
             font-size: 38px;
           }
         }

         .break-line-start {
             display: none;
          }

         @media (min-width: 341px) and (max-width: 354px) {
           .break-line-start {
             display: inline;
           }
         }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>
{* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"} *}
{include_php file="/home/ah/allhorse/public_html/pegasus/block-countdown.php"}
<section class="pwc kd usr-section">
    <div class="container">
        <div class="kd_content">
            <h1 class="kd_heading" style="margin-bottom: 0px">{$h1}</h1>
            <h1 class="kd_heading">{$h1b}</h1>
            <p>At BUSR, you are entitled to exceptional new member bonuses. {* For your first deposit
                with <br class="break-line-start"> BUSR, you'll get an additional 10% bonus to your deposit absolutely free. No
                Limits!*}
            </p>
            {* <p>For your first deposit with BUSR during the Pegasus World Cup, you'll get an additional 30% Cash bonus to your deposit + $50 Super Bowl Free Bet. So, if you deposit $2500, you'll get $750 free!</p>*}
            <p>For your first deposit with BUSR, you'll get an additional 20% bonus to your deposit absolutely free. So, if you deposit $2500, you'll get $500 free!</p>
            <p>If you wish to deposit more, please contact Customer Service and ask about your VIP Program options. You will be guided into a program tailored for just for you.</p>
          {*  <p>
                Plus for the Pegasus World Cup weekend, all your bets at Gulfstream Park qualify for double rebates. That's right! Get 6% back for straight wagers (win, place and show) and 16% back on your exotic wagers (everything but win, place and show).
            </p> *}
            <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg">{$button_cta}</a></p>   
          {*  <h2>Terms & Conditions</h2>
            <p>
                <li class="terms">The 10% Sign Up Bonus is for new members and is applied to your initial deposit.</li>
                <li class="terms">The 10% bonus is added to your account within 10 minutes of making your deposit.</li>
                <li class="terms">You must wager your initial deposit and sign up bonus 1 (one) time before it can be included
                    in a withdraw from your account. This is a 1 time rollover of your initial deposit and 10% signup
                    bonus.</li>
                <li class="terms">You cannot make a deposit, receive your 10% bonus and then withdraw your initial deposit and
                    bet with the bonus funds. That's a no-no. Play fair and we will, too!</li>
                <li class="terms">The double rebates on the Pegasus World Cup are only valid for new members of
                    BUSR.</li>
                <li class="terms">Promotion is valid on selected races only at Gulfstream Park on Saturday, Jan 25 and Sunday,
                    Jan 26.</li>
                <li class="terms">Your initial rebate of 6% for Straight Wagers and 16% for Exotic Wagers will be credited to
                    your account on Monday, Jan 27.</li>
                <li class="terms">Standard rebate rules apply to all other racetracks. (3% Straight Wagers and 8% Exotic Wagers)</li>
                <li class="terms">BUSR may limit the eligibility of customers to participate in this promotion at
                    its sole discretion.</li>
                <li class="terms">BUSR reserves the right to change, terminate or suspend this promotion at any
                    time and without prior notification.</li>
                <li class="terms">General House Rules and Regulations apply.</li>
            </p> *}
        </div>
    </div>
</section>

{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}
