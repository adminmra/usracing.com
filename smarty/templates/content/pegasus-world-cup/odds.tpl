{* {include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"} *}

{include file='inc/left-nav-btn.tpl'}

{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}

{literal}
  <script type="application/ld+json">
	  {
      "@context": "http://schema.org",
      "@type": "Event",
			"name": "Pegasus World Cup 2022",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2022-01-29",
      "endDate" : "2022-01-29",
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/pegasus-world-cup/signup-bonus"
      },
      "description": "Latest Pegasus World Cup 2022 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022.",
      "image": "/img/pegasus-world-cup/pegasus-world-cup-odds-og.jpg"
    }
	</script>
  <script type="application/ld+json">
	  {
      "@context": "http://schema.org",
      "@type": "Event",
			"name": "Pegasus World Cup Turf 2022",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2022-01-29",
      "endDate" : "2022-01-29",
      "offers": {
        "@type": "Offer",
        "url": "https://www.usracing.com/pegasus-world-cup/signup-bonus"
      },
      "description": "Latest Pegasus World Cup 2022 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus Turf 2022.",
      "image": "/img/pegasus-world-cup/pegasus-world-cup-odds-og.jpg"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/p[1]"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[2]"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/p[8]",
      "value": "US Racing provides the earliest Pegasus World Cup Futures odds of any website. If you see a horse you would like to be added, let us know and we might be able to get it added for you! Between now and the Pegasus World Cup, the odds will be changing when we add new horses and remove others. Remember, when you place a future wager the odds are fixed and all wagers have action."
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h2[@class='table-title'][1]"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-kentuckyderby-odds section-kentuckyderby-odds ']/section[@class='pwc kd usr-section']/div[@class='container']/div[@class='kd_content']/h2[@class='table-title'][2]"
    }
  </script>
{/literal}

{literal}
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Event",
  "name": "Pegasus World Cup 2022",
  "description": "Latest Pegasus World Cup 2022 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022",
  "startDate": "2022-01-29T00:00",
  "endDate": "2022-01-29T23:59",
  "eventStatus": "https://schema.org/EventScheduled",
  "eventAttendanceMode": "https://schema.org/OfflineEventAttendanceMode",
  "location": {		
    "@type": "Place",
    "name": "Gulfstream Park Racing and Casino, Hallandale Beach, FL",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "901 S Federal Hwy",
      "addressLocality": "Hallandale Beach",
      "addressRegion": "FL",
      "postalCode": "33009",
      "addressCountry": "US"
    }
  }
}
</script>
{/literal}

<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>    

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>


<!--------- NEW HERO  ---------------------------------------->

{* <div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
 <div class="text text-xl centerText">{$h1}</div> *}
{*  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}
  {* <div class="text text-md centerText">{$h2}
    <br>
      <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
  </div>
 </div> *}
 
<!--------- NEW HERO END ----------------------------------------> 


  <!------------------------------------------------------------------>
    {* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-white.tpl"} *}
    {* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"} *}
    {include_php file="/home/ah/allhorse/public_html/pegasus/block-countdown.php"}
  <section class="pwc kd usr-section">
    <div class="container">
      <div class="kd_content">
      <!--  <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Odds"
            class="kd_logo img-responsive"></a>  -->
        <h1 class="kd_heading">{$h1}</h1>
		 <p>{include file='/home/ah/allhorse/public_html/pegasus/salesblurb-odds.tpl'} </p>
         


        {*
                 <p>{include file='/home/ah/allhorse/public_html/pegasus/wherewhenwatch.tpl'} </p>
        <div class="tag-box tag-box-v4">

          <p>Find all the <a title="Pegasus World Cup odds" href="/kentucky-derby/odds">Pegasus World Cup odds</a> for {include file='/home/ah/allhorse/public_html/pegasus/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>

        </div>

        *}

        <h2 class="table-title">Pegasus World Cup {include file='/home/ah/allhorse/public_html/pegasus/year.php'} Contenders</h2>

         <p>Who do you think is going to win this years? Check out The Pegasus World Cup Betting Odds for {include file='/home/ah/allhorse/public_html/pegasus/year.php'} </p> 
       {* <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_pwc_2020_automatic.php'}</p> *}
       <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p> 
       <h3 class="h3-disclaimer">Live Odds will be updated as of January 2023</h3>
       <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_1120_xml.php'}</p>
        <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p> 
{*
    <h2 class="table-title">Pegasus World Cup {include_php file='/home/ah/allhorse/public_html/pegasus/year.php'} Turf</h2>
<br>
                <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup Turf</a></p>
                       <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_turf_xml_new.php'}</p>
                               <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World CupTurf</a></p>
*}
{*  <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2022/01/23*12" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup</a></p> *}
                                            
{*
        <h2 class="table-title">Pegasus World Cup Turf <br> {include_php file='/home/ah/allhorse/public_html/pegasus/year.php'} Contenders</h2>
       <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_pwc_turf_2020_automatic.php'}</p>
        <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2022/01/23*11" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup Turf</a></p> *}

        <h1>The {include_php file='/home/ah/allhorse/public_html/pegasus/running.php' once=false} Pegasus World Cup runs on {include_php file='/home/ah/allhorse/public_html/pegasus/day.php' once=false}.</h1>
        <p>US Racing provides the earliest odds of any racing website. If you see a horse you would like to be added, let us know and we might be able to get it added for you! Between now and the Pegasus World Cup, the odds will be changing when we add new horses and remove others. Remember, when you place a future wager the odds are fixed and all wagers have action.</p>
        <p>Good luck and see you on {include_php file='/home/ah/allhorse/public_html/pegasus/day.php' once=false} ! </p>


        <br>

        {*

        <h2> Pegasus World Cup Props</h2>

        <p>Great unique bets for extra fun at the Derby.</p>

        <p>{include file='/home/ah/allhorse/public_html/pegasus/odds_926_xml.php'}</p>

        <p align="center"><a href="/signup?ref=kentucky-derby-props" class="btn-xlrg ">Bet Now on Pegasus World Cup Props</a></p>

        *} <br>

        {*

        <iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/P3pLgjTPXCQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

        *}<br>
        {*
        <p>The Pegasus World Cup is the brainchild of Frank Stronach, the chairman of the Stronach Group.  With a $12 Million dollar purse, it is the richest race in horse racing-- before, those bragging rights belonged to the <a href="/dubai-world-cup">Dubai World Cup</a>.  The winner of the Pegasus World Cup wins $7 million, the rest is divided amongst the other finishers.  The race will take place on the dirt track at <a href="/gulfstream-park">Gulfstream Park</a> in Hallandale Beach, Florida.  How long is the Pegasus World Cup?  The Pegasus World Cup is 1 1/8 miles long.  There are a total of 12 entries for the race.  As an interesting twist, each of these entries were purchased for $1 million dollars a piece!  Those people who purchased the entries will share in the profits the race generates.</p>

        <p>If you want to attend the race in person, make sure to bring a fat wallet or purse-- general admission is $100.  Yes. One hundred dollars. Great way to get the general public interested in the race, right? (Please note sarcasm).</p>

        <p>Okay, why is it called the Pegasus World Cup?  If you have been to Gulfstream Park to bet on the races, you would have seen the massive 110-foot-tall sculpture that Frank Stronach said represents the eternal struggle between good (in this case, the winged horse of Greek mythology, Pegasus) and bad (evil dragon spitting fire).  So, what did this statute cost?  $30 million dollars. For. A. Statue.  Hmmmmm.  Okay.  According to Jerry Iannelli of the Miami New Times, a Gulfstream employee said, "I think it's an absolute waste of money. This used to be the top place for racing, but now the money's being put into something ridiculous. This place used to be all about horseracing. Now it's about something else. It's a shame all that money's gone into something like that."   Perhaps the inaugural Pegasus World Cup will renew interest in horse racing.  From our sources, we hear that the Pegasus World Cup will be run at sunny <a href="/santa-anita">Santa Anita Park</a> in 2018.  They don't have a 100 ft dragon there.  Yet.</p>

        <p>Pegasus by the Numbers (credit to the Miami Herald)

        <ul>

          <li><strong>What:</strong> Pegasus slaying a dragon</li>

          <li><strong>Idea conceived:</strong> April 2011</li>

          <li><strong>Dimensions:</strong> 110 feet tall, 115 feet wide, 200 feet long</li>

          <li><strong>Materials:</strong> Pegasus, 330 tons of steel and 132 tons of bronze; dragon, 110 tons of steel and 132 tons of bronze</li>

          <li><strong>Journey to Hallandale Beach:</strong> Hundreds of bronze pieces were packed in 26 shipping containers and sent by boat which took about six weeks. The steel was packed in 23 containers and also sent by boat.</li>

          <li><strong>Nuts and bolts:</strong> 18,000 screws</li>

          <li><strong>Workers: </strong>More than 200, including four translators</li>

          <li><strong>Cost:</strong> About $30 million</li>

          <li><strong>Construction:</strong> Took about eight months<br>
            *}
          </li>

        </ul>


         
         
      </div>
    </div>
  </section>

  <!------------------------------------------------------------------>



{include file="/home/ah/allhorse/public_html/pegasus/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}
{*include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"*}
{*{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}*}
{*include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"*}
{*include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-derek.tpl"*}


  <!------------------------------------------------------------------>

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}
