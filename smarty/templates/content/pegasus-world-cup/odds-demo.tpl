{assign var="ref" value="pegasus-world-cup-odds"}
{*
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

  {include file='menus/kentuckyderby.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
*}
{*
  <div class="container-fluid">
	
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/kentuckyderby/2017-Kentucky-Derby-odds.jpg" alt="Pegasus World Cup Odds"></a>
		
		<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/kentuckyderby/2017-Kentucky-Derby.jpg" alt="Pegasus World Cup Odds">  </p> </a>
	
</div>
*}  
<div class="container-fluid">
	
		<a href="/signup?ref=pegasus-world-cup" rel="nofollow"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="/img/pegasus-world-cup/pegasus-world-cup.jpg" alt="Bet on the Pegasus World Cup"></a>
		
		<a href="/signup?ref=pegasus-world-cup" rel="nofollow"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="/img/pegasus-world-cup/2017-pegasus-world-cup.jpg" alt="Bet on the Pegasus World Cup">  </p> </a>
</div>      
<div id="main" class="container">

<div class="row">


<div id="left-col" class="col-md-9">
         



{literal}
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Pegasus World Cup",
  "description": "Official 2019 Pegasus World Cup Odds available for betting.  The most racing bets anywhere: bet on jockeys, trainers and contenders.",
  "image": "https://www.usracing.com/img/pegasus-world-cup/2017-pegasus-world-cup.jpg",
  "startDate": "2018-11-27 07:19", 
  "endDate": "2018-11-30 09:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "Pegasus World Cup"
  },
  "location": {
    "@type": "Place",
    "name": "Pegasus World Cup",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "123 street ,place park",
      "addressLocality": "miamihhh",
      "addressRegion": "Florida",
      "postalCode": "33184",
      "addressCountry": "US"
    }
  }
}

</script>
{/literal}




                                        
          
<div class="headline"><h1>Pegasus World Cup</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>{include file='/home/ah/allhorse/public_html/pegasus/salesblurb.tpl'} </p>
<p>{include file='/home/ah/allhorse/public_html/pegasus/wherewhenwatch.tpl'} </p>


{*
<div class="tag-box tag-box-v4">
<p>Find all the <a title="Pegasus World Cup odds" href="/kentucky-derby/odds">Pegasus World Cup odds</a> for {include file='/home/ah/allhorse/public_html/pegasus/year.php'} at US Racing. You can place a bet for win, place, show, trifecta and more!</p>
</div>
*}

<h2> {include file='/home/ah/allhorse/public_html/pegasus/year.php'}  Pegasus World Cup Odds</h2>
<p>What are the Pegasus World Cup Odds for {include file='/home/ah/allhorse/public_html/pegasus/year.php'}?</p>

<!-- --------------------- schema table start here ---------------------- -->
{literal}
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Table",
  "about": "Odds Pegasus"
}
</script>
{/literal}


<p><div itemscope itemtype="http://schema.org/Table">{include file='/home/ah/allhorse/public_html/pegasus/odds_to_win_1120_xml_newtable_template.php'}</div></p>
<!-- --------------------- schema table end here ---------------------- -->

<p>Place your bets now and your odds are locked in. These odds are only available for a limited time!</p>
<p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now on the Pegasus World Cup</a></p>  <br>
{*
<h2> Pegasus World Cup Props</h2>
<p>Great unique bets for extra fun at the Derby.</p>
<p>{include file='/home/ah/allhorse/public_html/pegasus/odds_926_xml.php'}</p>
<p align="center"><a href="/signup?ref=kentucky-derby-props" class="btn-xlrg ">Bet Now on Pegasus World Cup Props</a></p>  
*}
<br>
{* <iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/P3pLgjTPXCQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe> *}
<p>The Pegasus World Cup is the brainchild of Frank Stronach, the chairman of the Stronach Group.  With a $12 Million dollar purse, it is the richest race in horse racing-- before, those bragging rights belonged to the <a href="/dubai-world-cup">Dubai World Cup</a>.  The winner of the Pegasus World Cup wins $7 million, the rest is divided amongst the other finishers.  The race will take place on the dirt track at <a href="/gulfstream-park">Gulfstream Park</a> in Hallandale Beach, Florida.  How long is the Pegasus World Cup?  The Pegasus World Cup is 1 1/8 miles long.  There are a total of 12 entries for the race.  As an interesting twist, each of these entries were purchased for $1 million dollars a piece!  Those people who purchased the entries will share in the profits the race generates.</p>
<p>If you want to attend the race in person, make sure to bring a fat wallet or purse-- general admission is $100.  Yes. One hundred dollars. Great way to get the general public interested in the race, right? (Please note sarcasm).</p>
<p>Okay, why is it called the Pegasus World Cup?  If you have been to Gulfstream Park to bet on the races, you would have seen the massive 110-foot-tall sculpture that Frank Stronach said represents the eternal struggle between good (in this case, the winged horse of Greek mythology, Pegasus) and bad (evil dragon spitting fire).  So, what did this statute cost?  $30 million dollars. For. A. Statue.  Hmmmmm.  Okay.  According to Jerry Iannelli of the Miami New Times, a Gulfstream employee said, "I think it's an absolute waste of money. This used to be the top place for racing, but now the money's being put into something ridiculous. This place used to be all about horseracing. Now it's about something else. It's a shame all that money's gone into something like that."   Perhaps the inaugural Pegasus World Cup will renew interest in horse racing.  From our sources, we hear that the Pegasus World Cup will be run at sunny <a href="/santa-anita">Santa Anita Park</a> in 2018.  They don't have a 100 ft dragon there.  Yet.</p>
<p>Pegasus by the Numbers (credit to the Miami Herald)
<ul>
<li><strong>What:</strong> Pegasus slaying a dragon</li>

<li><strong>Idea conceived:</strong> April 2011</li>

<li><strong>Dimensions:</strong> 110 feet tall, 115 feet wide, 200 feet long</li>

<li><strong>Materials:</strong> Pegasus, 330 tons of steel and 132 tons of bronze; dragon, 110 tons of steel and 132 tons of bronze</li>

<li><strong>Journey to Hallandale Beach:</strong> Hundreds of bronze pieces were packed in 26 shipping containers and sent by boat which took about six weeks. The steel was packed in 23 containers and also sent by boat.</li>

<li><strong>Nuts and bolts:</strong> 18,000 screws</li>

<li><strong>Workers: </strong>More than 200, including four translators</li>

<li><strong>Cost:</strong> About $30 million</li>

<li><strong>Construction:</strong> Took about eight months</li>
</ul>

 <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-pegasus.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row --><p>{include file='inc/disclaimer-pegasus.tpl'} 
</div><!-- /#container --> 
