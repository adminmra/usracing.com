{* {include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"} *}

{include file='inc/left-nav-btn.tpl'}

{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

 {* {literal} 

  <script type="application/ld+json">
	  {
      "@context": "http://schema.org",
      "@type": "Event",
			"name": "Pegasus World Cup 2021",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2021-01-22",
      "description": "Latest Pegasus World Cup 2021 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2021."
    }
	</script>
  <script type="application/ld+json">
	  {
      "@context": "http://schema.org",
      "@type": "Event",
			"name": "Pegasus World Cup Turf 2021",
			"location": {
				"@type": "Place",
				"name": "Gulfstream Park",
				"address": {
					"@type": "PostalAddress",
					"addressLocality": "Hallandale Beach",
					"addressRegion": "Florida"
				}
			},
			"startDate": "2021-01-22"
    }
	</script>

{/literal} *}

{literal}
<script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "SportsEvent",
  "name": "Pegasus World Cup 2022",
  "description": "Pegasus World Cup 2022 Contenders at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022",
  "startDate": "2022-01-29T00:00",
  "endDate": "2022-01-29T23:59",
  "location": {
    "@type": "Place",
    "name": "Gulfstream Park Racing and Casino",
    "sameAs": "Gulfstream Park Racing and Casino",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "901 S Federal Hwy",
      "addressLocality": "Florida",
      "addressRegion": "FL",
      "postalCode": "3009",
      "addressCountry": "US"
    }
  }
}
</script>
{/literal}


<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>    

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

  <!------------------------------------------------------------------>
   {* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"} *}
  {include_php file="/home/ah/allhorse/public_html/pegasus/block-countdown.php"}
  <section class="pwc kd usr-section">
    <div class="container">
      <div class="kd_content">
      
        <h1 class="kd_heading">{$h1}</h1>
		 <p>{include file='/home/ah/allhorse/public_html/pegasus/salesblurb-horses.tpl'} </p>

     <p>{include file='/home/ah/allhorse/public_html/pegasus/contenders.tpl'} </p>

        <h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/yearlast.php'*} Invitational</h2>

        <br/>
	<p align="center"><a href="/signup?ref={$ref}"  rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>

       {* <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2020/01/25*12" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup</a></p> *}
        {*<p>{include_php file='/home/ah/allhorse/public_html/pegasus/horses/pegasus_worldcup_horses.php'}</p>*}
        <h3 class="h3-disclaimer">Live Odds will be updated as of January 2023</h3>
        <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_1120_xml_new.php'}</p>
        {* <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2020/01/25*12" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup</a></p> *}
		<p align="center"><a href="/signup?ref={$ref}"  rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
		

        <h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/yearlast.php'*} Turf</h2>
        {*<p>{include_php file='/home/ah/allhorse/public_html/pegasus/horses/pegasus_worldcup_turf_horses.php'}</p>*}
        <h3 class="h3-disclaimer">Live Odds will be updated as of January 2023</h3>
		<p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_turf_xml_new.php'}</p>
        {*  <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2020/01/25*11" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup Turf</a></p> *}
		<p align="center"><a href="/signup?ref={$ref}"  rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
     <h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/yearlast.php'*} Filly & Mare Turf</h2>
     <h3 class="h3-disclaimer">Live Odds will be updated as of January 2023</h3>
    <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_filly_mare_turf_xml_new.php'}</p>
    	<p align="center"><a href="/signup?ref={$ref}"  rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
         <h1>The {include_php file='/home/ah/allhorse/public_html/pegasus/running.php' once=false} Pegasus World Cup runs on {include_php file='/home/ah/allhorse/public_html/pegasus/day.php' once=false}.</h1>
		 <p>US Racing provides the earliest odds of any racing website. If you see a horse you would like to be added, let us know and we might be able to get it added for you! Between now and the Pegasus World Cup, the odds will be changing when we add new horses and remove others. Remember, when you place a future wager, the odds are fixed and all wagers have action.</p>
		<p>Good luck and see you on {include_php file='/home/ah/allhorse/public_html/pegasus/day.php' once=false}!</p>
		<br><br>		 
         
      </div>
    </div>
  </section>

  <!------------------------------------------------------------------>
{include file="/home/ah/allhorse/public_html/pegasus/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}


  <!------------------------------------------------------------------>

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
{/literal}
