{* {include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"} *}

{include file='inc/left-nav-btn.tpl'}

{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css">
  .infoBlocks .col-md-4 .section {
    height: 250px !important;
    margin-bottom: 16px;
  }

  @media screen and (max-width: 989px) and (min-width: 450px) {
    .infoBlocks .col-md-4 .section {
      height: 165px !important;
    }
  }
</style>

{/literal}


{* {literal}
  <script type="application/ld+json">
	  {
      "@context": "http://schema.org",
      "@type": "Question",
      "name": "What day is the Pegasus World Cup?",
      "acceptedAnswer": {
          "@type": "Answer",
          "text": "The 5th Pegasus World Cup runs on January 23rd, 2021"
      }
    }
	</script>
{/literal} *}

{literal}
<script type='application/ld+json'> 
{
  "@context": "http://www.schema.org",
  "@type": "SportsEvent",
  "name": "Pegasus World Cup 2022",
  "description": "Bet on Pegasus World Cup 2022 odds at Gulfstream Park. Find out which horse, jockey and trainer will win North America's Richest Horse Race plus access to all betting odds for Pegasus 2022",
  "startDate": "2022-01-29T00:00",
  "endDate": "2022-01-29T23:59",
  "location": {
    "@type": "Place",
    "name": "Gulfstream Park Racing and Casino",
    "sameAs": "Gulfstream Park Racing and Casino",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "901 S Federal Hwy",
      "addressLocality": "Florida",
      "addressRegion": "FL",
      "postalCode": "3009",
      "addressCountry": "US"
    }
  }
}
</script>
{/literal}

<div id="left-nav">
  {include file='menus/pegasus-world-cup.tpl'}
</div>


{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

<!--------- NEW HERO  ---------------------------------------->

{* <div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
  <div class="text text-xl centerText">{$h1}</div> *}
  {* <div class="text text-xl" style="margin-top:0px;">{$h1b}</div> *}
  {* <div class="text text-md centerText">{$h2}
    <br>
    <a href="/signup?ref={$ref}">
      <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a>
  </div>
</div> *}

<!--------- NEW HERO END ---------------------------------------->


<!------------------------------------------------------------------>
{* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-white.tpl"} *}
{* {include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"} *}
{include_php file="/home/ah/allhorse/public_html/pegasus/block-countdown.php"}
<section class="kd pwc usr-section">
  <div class="container">
    <div class="kd_content">
      <!--  <a href="/signup?ref={$ref}" rel="nofollow"><img src="/img/index-kd/kd.png" alt="Kentucky Derby Odds"
            class="kd_logo img-responsive"></a>  -->
      <h1 class="kd_heading">{$h1}</h1>
      {* <h3 class="kd_subheading">Sign Up today and get up to $500 Cash</h3> *}
      <p>{include file='/home/ah/allhorse/public_html/pegasus/salesblurb.tpl'} </p>

      <!--<p><strong>What time is the Pegasus World Cup? </strong></br>
               The Pegasus World Cup is typically the 8th race of the day at Churchill Downs.   The Derby usually starts at approximately 6PM EST.</br>
               
               
               
               <p><strong>Where can I bet on the  Pegasus World Cup online?</strong></br>
               
               You can bet on the Pegasus World Cup online at US Racing, the Official OTB of the USA.</br>-->
      {*include('http://www.allhorse.com/kd/running2.php'); *}
      {* <p><a href="/pegasus-world-cup/odds"><strong>Betting on Pegasus World Cup</strong></a>
        rivals the Super Bowl for the excitement
        that it generates each year. Even though the race is only 2 minutes long,
        it consistently draws the attention of horse racing fans as well as people
        whose only knowledge of horse racing comes from &quot;The Pegasus World
        Cup.&quot; </p>
        <p>From small bets between friends at their horse racing parties to the
        crazy big money horse bets at <a title="Off Track Horse Betting" href="/otb">offtrack horse betting</a>
        parlors, casinos in Las Vegas and online horse betting sites, placing a wager on The Pegasus World Cup can&#39;t
        get any easier. </p>
      <p>The Pegasus World Cup is a Grade I stakes race for three-year-old
        thoroughbred horses each year in January. It is recognized as the richest
        horse race in North America. In {include
        file='/home/ah/allhorse/public_html/pegasus/year.php'}, the prize purse is over 9 million
        dollars. Every year, fans descend to South Florida to take part of the
        spectacle that is The Pegasus World Cup. It draws over 20,000 screaming
        fans so it is a great way to kick off the racing season. </p> *}
     {* <p align="center"><a href="/login?ref={$ref}&to=sports?leagueId=1120" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on Pegasus World Cup</a></p> *}
     
     <h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/year.php'*} Invitational</h2>
      {* <p>What are the Pegasus World Cup Odds for {include file='/home/ah/allhorse/public_html/pegasus/year.php'}</p> *}
      {*<p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_pwc_2020_automatic.php'}</p> *}
      <br/>
     <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
      <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_1120_xml_new.php'}</p> 
     <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>

<h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/year.php'*} Turf</h2>
      {* <p>What are the Pegasus World Cup Odds for {include file='/home/ah/allhorse/public_html/pegasus/year.php'}</p> *}
            {*<p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_pwc_2020_automatic.php'}</p> *}
                  <br/>
                       <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
                             <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_turf_xml_new.php'}</p>
                                  <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
                                  <h2 class="table-title">Pegasus World Cup {*include_php file='/home/ah/allhorse/public_html/pegasus/year.php'*} Filly and Mare Turf</h2>
                                   <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
                                   <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_to_win_filly_mare_turf_xml_new.php'}</p>
                                   <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>

{* <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2021/23*12" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup</a></p> *}

    {*  <h2 class="table-title">Pegasus World Cup Turf <br> {include_php file='/home/ah/allhorse/public_html/pegasus/year.php'} Contenders</h2>
        <p>{include_php file='/home/ah/allhorse/public_html/pegasus/odds_pwc_turf_2020_automatic.php'}</p>
    <p align="center"><a href="/login?ref={$ref}&to=to/racebook?meetingRace=GP*2021/01/23*11" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Pegasus World Cup Turf</a></p> *}

      <h1>The {include_php file="/home/ah/allhorse/public_html/pegasus/running.php" once=false} Pegasus World Cup runs on {include_php file='/home/ah/allhorse/public_html/pegasus/day.php' once=false}</h1>
      <p>US Racing provides the earliest odds of any racing website. If you see a horse you would like to be added, let us know and we might be able to get it added for you!<br>
      Between now and the Pegasus World Cup, the odds will be changing when we add new horses and remove others. Remember, when you place a future wager, the odds are fixed and all wagers have action.</p>
      <p>Good luck and see you on {include_php file="/home/ah/allhorse/public_html/pegasus/day.php" once=false}!</p>
      {*
      <h2>How to Place your Pegasus World Cup Bet at BUSR</h2>
      <p><iframe class="video" height="100%" src="https://www.youtube.com/embed/adPhcXE2JGk" frameborder="0"
          allowfullscreen></iframe>
      </p>
      *} {*
      <h2>Pegasus World Cup Betting from Horseplayers on Esquire</h2>
      <iframe src="https://tv.esquire.com/videos/embed/rQ-NXyTW5NA=" width="100%" style="min-height:480px;"
        frameborder="0" seamless></iframe>
      *}
      <br>
      {*<br>
      <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now on Pegasus World Cup</a>
      </p>
      <br>
      <h2>Pegasus World Cup Facts</h2>
      <p><a href="/">US Racing</a>, an online horse racing service, compiled the following Pegasus World Cup facts for
        horse racing fans.
      </p>
      <o><b>The Crowd</b>
      </o>
      <p>In 2014, there was a crowd of 164,905 which almost matched the record of 165,307 in 2012. If we have sunny
        weather, we will break 165,000 with people cheering American Pharaoh and Dortmund to victory!</p>
      <p><b>The Booze</b>
      </p>
      <p>122,000 Mint Juleps 475,000 beers sold in Grand Stands and Infield</p>
      <p><b>The Bling</b>
      </p>
      <p>90 Woodford Reserve $1,000 Mint Julep Cups 10 Legendary Woodford Reserve Cups – starting bid $2,000
        Ingredients: Ice made from pure mineral water sourced from glacial ice of Nova Scotia. Gold dusted mint from
        Woodford County, Kentucky and Woodford Reserve Distiller’s Select Bourbon. Hmm, for that much money you could
        get a group of your friends feeling right and <a title="make a great mint julep from scratch!"
          href="/mint-julep-recipe">make great a mint julep from scratch</a>
      </p>
      <p><b>The Benjamins</b>
      </p>
      <p>$345 Million in revenue for Kentucky $65 bucks to get into Churchill Downs $5,000 to get in like a Playha</p>
      <p><b>The View</b>
      </p>
      <p>15,224 square foot HD Video Board installed at Churchill Downs- $12 Million bucks for a TV! That’s larger than
        3 NBA basketball courts—combined!</p>
      <h2> My First Pegasus World Cup Bet</h2>
      <p><strong>By the US Racing Team</strong>
      </p>
      <p>I remember placing my first bet at the Pegasus World Cup online. </p>
      <p>It was in 2005, and I decided to bet on a horse called Giacomo being ridden by Hall of Fame jockey, Mike Smith.
        Giacomo was certainly not a favorite. </p>
      <p>What were the odds? 50-1. Yes, the same odds as Mine that Bird who won in 2009! </p>
      <p>The only horse with worse odds who won the derby was Donerail who started at 91-1! So, odds don’t tell the full
        story with the horse and jockey who end up winning the garland of red roses in Lousiville.</p>
      <p>Anyway, Giacomo was named after the singer Sting’s son. Jerry Moss was the owner - Jerry Moss and Herb Albert
        (yes, of the Whipped Cream and Other Delights!) were the founders of A&amp;M Records. Sting performed and
        recorded under that label as a solo act and also when he was in the rock group The Police.</p>
      <p>Giacomo is Italian for James. Ironically, Giacomo has a half-brother named Tiago which in Portugese means
        James!</p>
      <p>But, I digress. . .</p>
      <p>So, why did I bet on Giacomo at that year’s Derby? </p>
      <p>Let’s put it this way: I’m a sucker for the underdog. If there was a horse called Rocky, I would have bet on
        him based on principle. </p>
      <p>I didn’t bet on Charismatic at the 1999 Pegasus World Cup (I would have if I was there- he was a long shot,
        too—31-1 odds) but I bet on Lemon Drop Kid at the 1999 Belmont Stakes. </p>
      <p>It’s not that I wasn’t on the Charismatic bandwagon—I wasn’t a Triple Crown hater. </p>
      <p>I bet Lemon Drop Kid because my sister loved lemon drops (I think it was the Cosmopolitan drink of ’99 before
        Sex and the City ushered in Cosmos). I wasn’t following racing back then—I didn’t know Chris Antley’s story and
        Charismatic’s, either. </p>
      <p>So, now the Derby is coming close. The first Saturday in May, it’s easy to remember. And it always takes place
        at Churchill Downs.</p>
      <p>I hope that if you are betting on the Pegasus World Cup for the first time you will consider a long shot. Bet
        for the underdog. Sometimes, he may not have fleas. Sometimes, he might just rise to the occasion. And
        sometimes. . . just sometimes, a 50-1 long shot will put a bankroll in your pocket!</p>
      <p>Good luck with your bets and everybody at US Racing hopes you pick a winner! (If you happen to lose, we wish
        you have an extra mint julep at arm’s length.).<br /> *}
      </p>
      <p>{*</p>
      <p>Check out the Pegasus World Cup Prep Race Schedule</p>
      <h2>Pegasus World Cup Prep Race Schedule</h2>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"
        class="table table-condensed table-striped table-bordered">
        <tr>
          <td>Grade II Churchill Downs</td>
          <td>$250,000-added, seven furlongs, 4-year-olds and up</td>
        </tr>
        <tr>
          <td>Grade III La Troienne</td>
          <td>$150,000-added, seven furlongs, 3-year-old fillies</td>
        </tr>
        <tr>
          <td>Grade III Distaff Turf Mile</td>
          <td>$150,000-added, one mile, fillies and mares, 3-year-olds and up</td>
        </tr>
        <tr>
          <td>Grade I Distaff</td>
          <td>$300,000 added, seven furlongs, fillies and mares, 4-year-olds and up</td>
        </tr>
        <tr>
          <td>Grade I Turf Classic</td>
          <td>$500,000-added, 1 1/8 mile, 3-year-olds and up</td>
        </tr>
        <tr>
          <td>Grade I Pegasus World Cup</td>
          <td>$2,000,000-guaranteed, 1 1/4 mile, 3-year-olds</td>
        </tr>
        <tr>
          <td colspan="2" class="center">*$40 general admission</td>
        </tr>
      </table>*}
      {* <h2> <br>
        {include file='/home/ah/allhorse/public_html/pegasus/year.php'} Pegasus World Cup TV Schedule</h2>
      <p>
        <table width="100%" border="0" cellspacing="0" cellpadding="0"
          class="table table-condensed table-striped table-bordered" summary="Pegasus World Cup TV Schedule"
          title="Pegasus World Cup Betting - TV Schedule">
          <tbody>
            <tr>
              <th>Date</th>
              <th>Races</th>
              <th width="97">Network</th>
            </tr>
            <!-- <tr>
                     <td valign="top">March 26</td>
                     
                     
                     
                     <td>* Louisiana Derby, TBA</td>
                     
                     <td>USA Network</td>
                     
                     
                     
                     </tr> -->
            <!-- <tr class="odd">
                     <td valign="top">April 9 </td>
                     
                     <td>* Santa Anita Derby, TBA</td>
                     
                     
                     
                     <td>NBC</td>
                     
                     </tr> -->
            <tr>
              <td>{include file='/home/ah/allhorse/public_html/pegasus/racedate.php'}</td>
              <td> {include file='/home/ah/allhorse/public_html/pegasus/year.php'} Pegasus World Cup, TBA<br>
              </td>
              <td>NBC</td>
            </tr>
          </tbody>
        </table>
      </p>
      <p align="center"><a href="/login?ref={$ref}" class="btn-xlrg ">Bet Now on Pegasus World Cup</a>
      </p> *}




    </div>
  </div>
</section>

<!------------------------------------------------------------------>

{*include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-testimonial-other.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"*}
{*include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"*}
{*{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}*}
{include file="/home/ah/allhorse/public_html/pegasus/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}
{*include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-derek.tpl"*}

<!------------------------------------------------------------------>

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}
