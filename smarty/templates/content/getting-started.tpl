<!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 	<div class="text text-md">{$h2} {$logo}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->   


<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
   
<div class="headline"><h1>Getting Started at an Online Racebook</h2></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- --> 

<p>Here are some frequently asked questions for people interested in betting on horses at an ADW or racebook. First, there are many websites to choose from and will offer signup bonuses and rebates on your horse wagers.  For example, both Xpressbet and TVG offer solid sign up bonuses-- but you must make a certain amount of wagers within a timeframe (usually 30 days from the day you signed up) in order to get the bonus.  Offtrackbetting offers some nice rebates.  BUSR and Bovada offer solid rebates and signup bonuses with BUSR also giving away free bets!  We hope these answers help you decide which is the best racebook or ADW for your gaming needs.  Good luck!
</p>
<h3>Frequently Asked Questions</h3>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq1');"> Can anyone join a racebook or ADW? </a></div>
<div class="faqs lnkDropItem" id="faq1">
<p>To open a racebook or ADW account, you must be at least 18 years old.  Some websites like TVG require you to be 21 years old.  Any website that accepts a player under the age of 18 years should be avoided.</p>
</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq2');">Can I bet on horses from my state?</a></div>
<div class="faqs lnkDropItem" id="faq2">
<p>Laws for online horses betting and online gambling vary from country to country and state to state.  It is up to the individual member to determine whether they are eligible to play at the racebook or ADW.</p>
</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq3');"> How can I open an ADW or racebook account?</a></div>
<div class="faqs lnkDropItem" id="faq3">
<p>Sign up for an account at BUSR, TVG, TwinSpires, Xpressbet or any of the reviewed racebooks by going to their websites or calling them on the phone. </p>
</div>

<!-- -->
<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq4');">What information do I need to open an ADW or racebook account?</a></div>
<div class="faqs lnkDropItem" id="faq4">
<p>The answer really depends on the company and your place of residence.  Some websites require your social security number while others will only need a photo ID like a driver's license.  You will need to provide your basic personal and mailing address to open an account. For your convenience to be able to play right away, some websites allow you to make your deposit immediately - before providing proof of ID.  Just be aware, that before you make a withdraw, you will need to prove you are who you say you are!</p>

<p>If you deposit via credit card, a photo ID will be needed. It is best to have them submitted before your payout request so you can receive your winnings without delay. You also should confirm with your credit card company that your card is approved.  Even though horse betting is legal, sometimes credit card companies will decline the transaction.  Depending on where the merchant provides processing, your car could also be declined unless you request it be approved for foreign transactions. </p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq5');">Why do they need my ID?</a></div>
<div class="faqs lnkDropItem" id="faq5">
<p>
    Your financial security is absolutely paramount. With this in formation ADWs and racebooks
    can validate that:
</p>
<ul>
    <li>
        You are at least 18 years of age (or 21 years, depending on the company).
    </li>
    <li>
        You are the true owner of the credit card (KYC, Know Your Customer).
    </li>
    <li>
        Your winnings go to the correct person: <strong>you.</strong>
    </li>
</ul>
<p>
    This is standard industry practice for everybody's safety. Find out more
    about why at
    <a
        href="http://www.sportsbookreview.com/sbr-news/online-sportsbooks-kyc-policies-2015-edition-59739/"
    >
        SB Review
    </a>
    .
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq6');">What if I don't have a credit card.</a></div>
<div class="faqs lnkDropItem" id="faq6">
<p>Several alternate deposit methods available including Paypal, Netller, MoneyGram, Bank Wire, ACH, Peer-to-Peer and even Bitcoin!
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq7');">I have a Promo Code. Where do enter it to get my bonus?</a></div>
<div class="faqs lnkDropItem" id="faq7">
<p>The area to put the Promo Code in is in the deposit section of the cashier. Promo codes do expire quickly so please use it as soon as possible. If you have any questions, please contact Customer Service. TVG, Twinspires, BUSR, Bet America and nearly all of the major ADWs and racebooks offer promos throughout the year.  Sometimes you can even get a special bonus by simply asking!  These companies want you as a customer! 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq8');">I know a lot of places can take weeks, even months to pay out. How long does it take?</a></div>
<div class="faqs lnkDropItem" id="faq8">
<p>If you have your documents in place, payouts generally take 48 hours to 7 business days depending on the method your request for payout. 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq9');">Is there a fee for my payout?</a></div>
<div class="faqs lnkDropItem" id="faq9">
<p>Yes, there is usually a nominal charge that will vary depending on payout of choice. Some companies offer 1 free payout per month, but most change a fee. 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq10');">Will my winnings be taxed?</a></div>
<div class="faqs lnkDropItem" id="faq10">
<p>For ADW's they will automatically deduct winnings and give to the IRS.  For non-parimutuel racebooks, it generally up to the members to represent to their tax authorities their own winnings/losses. 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq11');">Do all ADWs and racebooks offer rebates or bonuses?</a></div>
<div class="faqs lnkDropItem" id="faq11">
<p>Bonuses? Yes.  Rebates?  No. Although there is up to an 8% daily rebate on horse racing depending on the specific type of wager and the racetrack at BUSR, some websites will ONLY provide rebates to the "whales"; the people betting $50,000 or more per month. First deposits at almost every website are eligible for various cash bonuses and promotions. Please check out the bonuses offerred on each site before you join.  We also recommend joining a few websites so that you have a backup site incase there is a technical problem with your main racebook or just so that you can get more bonuses! 
</p>

</div>


<h3>Wagering Answers</h3>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq13');">How do I place a bet? </a></div>
<div class="faqs lnkDropItem" id="faq13">
<p><i>ADW and racebook account holders can typically wager online using a computer, mobile device or tablet. Telephone wagering is also available at some websites.  Tutorials on how to place your wager are available at the website or if you do a search on Youtube for "how to place a horse bet."</i>
</p>
	</div>
<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq14');">What are the minimum and maximum limitations on wagering amounts?</a></div>
<div class="faqs lnkDropItem" id="faq14">
<p>Wager amounts typically ranging from $0.50 to $20,000.00 per ticket, subject to host track restrictions. In the case of multiple wagers, such as a Daily Double or Exacta box, the base wager is multiplied by the number of combinations to determine the amount of the total wager. If a customer wishes to place more than a set amount on a specific wager, then multiple bets can be made for the same horse at the same racetrack. Please see the betting rules section of your ADW or racebook. 

</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq15');">When can I bet on the races? </a></div>
<div class="faqs lnkDropItem" id="faq15">
<p>You can place wagers 24x 7 for all of the day's races. Besides popular tracks like Churchill Downs, Saratoga and Santa Anita, online horse betting allows you to bet on international racetracks, too. Bet on international races in Hong Kong, Australia, UK, France, Ireland and Japan! Wagers can be placed up to post time of each race. For special events like the Kentucky Derby, some companies like TwinSpires, TVG and BUSR will provide advance wagering as well as wagering on the day of the racing event. 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq16');">What wagering choices do I have?</a></div>
<div class="faqs lnkDropItem" id="faq16">
<p>The full menu of wagering options are typically you would have available at the racetrack. Although the track list will vary from website to website.  This is another reason we suggest opening accounts at multiple ADWs and racebooks.  Cover all your bases!

</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq17');">When do I get confirmation that my wager has been accepted?</a></div>
<div class="faqs lnkDropItem" id="faq17">
<p>Normally, within 5 seconds after the wager has been accepted. Your betting ticket and confirmation number serves as evidence of your wager being successfully accepted. Your receipt of the confirmation may be slowed due to site volume, internet traffic, or connection speed for your computer or mobile device.  Check with your ADW or racebook provider for details. 
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq18');">How are the wagers processed?</a></div>
<div class="faqs lnkDropItem" id="faq18">
<p>All wagers are processed at the ADW or racebook provider's hub system for each particular race. Races are graded as soon as the official results from the track have been announced.  On heavy volume days like the Kentucky Derby, please be patient for your account to be updated. 
</p>

</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq20');">Are winners paid full-track odds?</a></div>
<div class="faqs lnkDropItem" id="faq20">
<p>Parimutuel websites pay out full track odds, period. Non-parimutuel websites typically offer full track odds on the most popular wagers (WPS) although they impose maximum payouts for certain types of wagers. Please see the Rules section of your website for more details.
</p>

</div>

<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq21');">How do I collect my winnings?</a></div>
<div class="faqs lnkDropItem" id="faq21">
<p>Your winnings are automatically credited to your account as soon as the race is declared "official" and results are posted. This may take a few minutes on higher volume races such as the Kentucky Derby. Payouts are requested through an online cashier or "withdraw" section within your account at the website.
</p>

</div>



<h3>More about US Racing</h3>
<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq23');"> Who is US Racing?</a></div>
<div class="faqs lnkDropItem" id="faq23">
<p>US Racing is an award winning online horse racing service that provides ADW and racebook reviews, news, stories and horse racing information. No actual wagering activity takes place at USRacing.com. US Racing reviews ADWs and racebooks like Twin Spires, TVG, Xpressbet, Betfair, Paddy Power, Bovada, Betonline and BUSR.  US Racing will be adding more reviews for ADWs and racebooks so that you, the consumer and fan of horse racing, are provided with the best information in order to make an informed decision of what racebook is best for you.
</p>

</div>


<div class="lnkDrop"><i class="fa fa-info"></i> <a rel="nofollow" href="javascript:slideIt('faq309');"> Is there a responsible wagering program?</a></div>
<div class="faqs lnkDropItem" id="faq309">
<p>
US Racing supports responsible wagering by anybody who gambles on horse racing or any type of game.  For some people, betting on horses is a past time; for others, it is a full time occupation.  In either case, learn more about our commitment to reasonable gaming<a rel="nofollow" href="https://www.usracing.com/responsible-gaming"> Here</a>.
</p>
</div>             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='/home/ah/allhorse/public_html/usracing/calltoaction_about.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



{literal}
<script type="text/javascript">
$(document).ready(function() {
  $('.lnkDropItem').css("display","none");
});
function slideIt(thechosenone) {
     $('.lnkDropItem').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).slideDown(200);
			    $(this).prev().addClass("active");
          }
          else {
               $(this).slideUp(200);
			    $(this).prev().removeClass("active");
          }
     });
}
</script>
{/literal}
