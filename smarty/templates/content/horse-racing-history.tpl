<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">
                                     
          
<div class="headline"><h1>A History of Horse Racing</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<h2>History of Horse Racing</h2>

<p>Welcome to the <a title="US Racing" href="/">US Racing</a> History Section.</p>
    
<p>There are three chapters, <a title="A History of Horse Racing" href="/horse-racing-history">A History of Horse Racing</a>, <a title="Horse Racing in Kentucky" href="/kentucky">Horse Racing in Kentucky</a> and <a title="A History of Churchill Downs and the Kentucky Derby" href="/churchill-downs">A History of Churchill Downs and the Kentucky Derby</a>. The material is meant to be read straight through, but feel free to roam and jump around as you please. The main reference for the US Racing history section is the  website Call to the Derby Post</a> which takes its source from the book "Jockeys, Belles and Bluegrass Kings, The Official Guide to Kentucky Racing" by Lynn S. Renau <cite>(Herr House Press, Louisville, Kentucky: 1995).</cite></p>

 <p>What follows is the first chapter, A History of Horse Racing. This 3-segment history traces horse racing from its development overseas to its beginnings in the United States.</p>

   
          
<h2>Cocktails with the Sport of Kings</h2>

                
<p>As early as 1140, the first of a long line of kings named Henry tried to improve Hobby horses--pony-sized Irish horses--by importing Arab stallions to give them more speed and stronger power. Throughout the Crusades, from 1096 to 1270, Turkish cavalry horses dominated the larger English warhorses, leading the Crusaders to buy, capture or steal their share of the stallions. After the War of the Roses, which decimated England's horse population, King Henry aimed to rebuild his cavalry. Both the king and his son, Henry VIII, imported horses from Italy, Spain and North Africa, and maintained their own racing stable. Henry's Hobbys, as they were called, raced against horses owned by other nobility, leading the word "hobby" to mean a "costly pastime indulged in by the idle rich." It also lends credibility to horse racing being labeled as the Sport of Kings, although this phrase's origination comes later, as found in Part II.</p><p>Henry used tax revenues to maintain his stables, claiming that by breeding winners with winners he could improve the quality of the cavalry. While certainly a landmark philosophy in horse racing, Henry was unable to apply its practice; his Master of the Horse, the title of Henry's racing stable director, was not a professional horseman and recklessly crossbred the entire stable. The stable consisted of a variety of international horses with an even wider mix of genes, so well mixed they earned the moniker "cocktails," our current word for a mixed drink. It is not known for sure, but this may be the oldest piece of evidence linking horse racing with drinking!</p><p>Anyway, Henry's daughter, Elizabeth I, drastically improved her father's stable during her fifty-year reign, dispensing of horses not qualified for racing or the cavalry and moving the best horses to new barns at Tutbury near Staffordshire. Elizabeth kept a close watch on matings and systematically recorded pedigrees. On the advice of her Master of the Stable the Queen added more Arabian horses to the stable, breeding Arab stallions to Hobby and Galloway (Scottish) mares. When Elizabeth I died, James VI of Scotland, son of Mary, Queen of Scots, and his son, Charles--who became king in 1625--expanded both the palace and royal racing stables at the track of Newmarket. In 1647 Oliver Cromwell's army defeated Charles' Cavaliers, forcing Charles back to Scotland and allowing Cromwell to capture the royal stables at Tutbury and take inventory; he swiftly sold most of the Royal Mares, keeping fewer than 100 to breed stronger, lighter horses to replace the slower, heavier ones no longer suited for warfare due to the development of gunpowder.</p><p>Cromwell's focus was on the cavalry, not racing. He even passed several laws prohibiting racing and went so far as to confiscate horses and cause pedigree records to be ruined. Royalists and Cavaliers were either forced out of England or in retreat to their country estates where they could do two things: maintain their records of horses bred for stag hunting and racing, and wait for the end of Cromwell's repressive religious throne. When Cromwell died and Charles II became king, the wait was over.</p>
                
                <p><a title="NEXT CHAPTER" href="/kentucky">NEXT CHAPTER</a></p>
                
                <p><a title="A History of Horse Racing" href="/horse-racing-history">A History of Horse Racing</a> </p><p><a title="Horse Racing in Kentucky" href="/kentucky">Horse Racing in Kentucky</a></p>

                
                <p><a title="A History of Churchill Downs and the Kentucky Derby" href="/churchill-downs">A History of Churchill Downs and the Kentucky Derby</a></p>
			        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



