{include file='inc/left-nav-btn.tpl'} <div id="left-nav"> 

<!-- ---------------------- left menu contents ---------------------- --> 
{include file='menus/horsebetting.tpl'} 
<!-- ---------------------- end left menu contents ------------------- --> 
</div>

<div id="main" class="container">
<div class="row"> <div id="left-col" class="col-md-9">


<div class="headline"><h1>Online Horse Racing</h1></div>

<div class="content"> 
<!-- --------------------- content starts here ---------------------- --> 
<p>You can bet from your home computer, mobile or tablet at BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>
<h2>Bet on  Horse Racing Online - Anytime!</h2>

<p>Never miss another race! Us Racing is the leader for online horse racing wagering. There was a time when you had to go to the track or an off-track betting parlor to wager on horse racing. By 2007, the horse racing industry recognized that future growth lay in modern off track betting online. Since then, online growth for horse racing betting has averaged roughly 4%. </p>
<p>US Racing offers off-track betting on horse racing at over 150 tracks worldwide. We have a full menu of wagering options. In fact, we offer everything that's available to you at the track online or on your mobile device. When you make a bet online with US Racing, you can feel secure that your wagers are encrypted and backed by a company who has paid out over a billion USD in wagers since 1999. 8% Rebates with no limits. </p>
<p>Get real-time entries, race results, video, free bets and cash bonus racing rewards. Select from a wide variety of US and international racing throughout the day. Place your bets on the races up until post time, just like you would be able to at the track. US Racing provides you with access to the latest odds, horse betting tips, news and more. It's fun, secure, online horse racing wagering with video broadcasts of the races and horse racing replays. </p>


<h2><strong>Horse Racing Bet Types </strong></h2>
<p>The most common bet types for horse racing online include: </p>


<div class="tag-box tag-box-v2">
 <h3><strong>Straight Horse Racing Bets </strong></h3>

<ul>
<li><strong>Win</strong>: select the horse that wins the race. </li>
<li><strong>Place</strong>: select a horse that finishes either first or second. </li>
<li><strong>Show</strong>: select a horse that finishes first, second or third. </li>
</ul>               
</div>
            
<div class="tag-box tag-box-v2">
<h3><strong>Exotic Horse Racing Bets </strong></h3>
<ul>
<li><strong>Exacta, perfecta, or exactor</strong>: select the two horses that finish first and second, in the exact order. </li>
<li><strong>Trifecta</strong>: select the three horses that finish first, second, and third, in the exact order. </li> <li><strong>Superfecta</strong>: select the four horses that finish first, second, third and fourth, in the exact order. </li>
<li><strong>Box:</strong> a box can be placed around exotic betting types such as exacta, trifecta or superfecta bets. This places a bet for all variations of the numbers in the box. </li>
</ul>    
</div>
      

<h3>Important Horse Races</h3>
<p>The high point of US horse racing is the <a href="/kentucky-derby">Kentucky Derby</a>. The Derby is followed 2 weeks later by the <a href="/preakness-stakes">Preakness Stakes</a>, at Pimlico Race Course in Baltimore, Maryland. Three weeks after the Preakness, is the <a href="/belmont-stakes">Belmont Stakes</a>, held Belmont Park on Long Island. Together, these key races form the Triple Crown of Thoroughbred Racing for three-year-olds. Every year the excitement builds to see if there will be a new Triple Crown Champion which considered the greatest accomplishment of Thoroughbred horse racing. Bet on the <a href="/triple-crown">Triple Crown</a> races online. Join Now! </p>

<!-- ------------------------ content ends -------------------------- --> 
</div> <!-- end/ content --> 
</div> <!-- end/ #left-col --> 

<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}
</div><!-- end: #right-col --> 

</div><!-- end/row --> 
</div><!-- /#container --> 
