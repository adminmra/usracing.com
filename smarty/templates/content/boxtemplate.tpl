
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Custom Boxes - Template</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- ======== STYLES NEEDED TO HIDE BLOCK ELEMENTS ================== -->

<!-- ============================================================== -->

<!-- ======== BOX WITH TITLE ======================================== -->

<h2>Testing</h2>
<div>
<!-- ========ENTER CONTENTS HERE======== -->

Enter Content Here:
<br />
10px top padding is automatically included in the CSS for #infoEntries and  #raceEntries tables.

<!-- ========END CONTENT ================ -->
</div>


<!-- ======== END BOX WITH TITLE =================================== -->


<!-- ======== BOX WITH TITLE ======================================== -->

<h2>Testing Next Box</h2>
<div class="box-text">
<!-- ========ENTER CONTENTS HERE======== -->

This one has a class called "box-text" in the DIV contents container. 
<br />
It gives the top padding 20px. Use this for  the individual racetrack pages..

<!-- ========END CONTENT ================ -->
</div>


<!-- ======== END BOX WITH TITLE =================================== -->

        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    