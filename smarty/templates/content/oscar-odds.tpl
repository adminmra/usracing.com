{literal}<link rel="stylesheet" href="/assets/css/sbstyle.css">

<style type="text/css">

@media screen and (min-width: 1024px) {.newHero { background-image: url("/img/heros/sm_oscars_odds_usr.jpg"); }}	

@media screen and (max-width: 1024px) {.newHero { background-image: url("/img/heros/lg_oscars_odds_usr.jpg"); }}	

</style> 

{/literal}<!--------- NEW HERO  ---------------------------------------->

<div class="newHero"  alt="Oscar Odds">

  <div class="text text-xl">Oscar Odds</div>

  <div class="text text-xl" style="margin-top:0px;"></div>

  <div class="text text-md">Place a wager on the Oscar Awards<br>

    <a href="/signup?ref={$ref}">

    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>Sign Up Now</div>

    </a> </div>

</div>



<!--------- NEW HERO END ----------------------------------------> 

<div id="main" class="container">

  <div class="row">

    <div id="left-col" class="col-md-9">

      <div class="headline"> 

        <!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- -->

        

        <h1>Oscar Odds</h1>

        

        <!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 

      </div>

      <!-- end/headline -->

      

      <div class="content"> 

        <!-- --------------------- CONTENT starts here --------------------------------------------------- --> 



        <p>The Oscar Awards, also known as the Academy Awards, celebrates the world of film  each year. Live from bright lights of Los Angeles, California, the Oscar Awards are watched from people all across the US and around the globe. <br>
          <br>
          Ready to place a wager on the Oscar Awards? 
			Check out the odds below at BUSR. </p>

        <p>
          
          
          
          
          
          {include file="/home/ah/allhorse/public_html/oddsfeed/odds/946.php"}
          
          
          
        </p>





        <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet on the Oscar Odds</a></p>

        

        <!-- ------------------------ CONTENT ends ------------------------------------------------------- --> 

      </div>

      <!-- end/ content --> 

    </div>

    <!-- end/ #left-col -->

    

    <div id="right-col" class="col-md-3"> 

      <!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- --> 

      {include file='inc/rightcol-calltoaction-oscar.tpl'}

      <!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- --> 

    </div>

    <!-- end: #right-col --> 

    

  </div>

  <!-- end/row --> 

</div>

<!-- end/container --> 

