{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
           {include file='menus/connections.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Famous Breeders</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Breeders of Thoroughbred racehorses have a motto: "Breed the best to the best, and hope for the best." Farms from California to New York, Florida to Maryland, are all trying to emulate what breeders in the state of Kentucky have done for centuries -- produce champions.</p>
<p>For breeders, the study of bloodlines is a way of life. The art of matching a dam (the horse's mother) with a sire (the father) to produce a top foal is part art form, part science, and a great deal of luck. For instance, Alydar, who finished second to Affirmed in all the 1978 Triple Crown races, sired a pair of Kentucky Derby winners in Alysheba (1987) and Strike the Gold (1991); Affirmed has yet to sire a Derby winner.</p>
<p>After that colt (male) or filly (female) is born, the breeder puts in countless hours of hard work to get the horse ready for the races. While most thoroughbreds are born between January and June, they have a universal birthday of January 1. This standard was set up to simplify the process of creating races for horses of a certain age. (For example, the Kentucky Derby is restricted to three year olds).</p>
<p>During the young horse's first autumn, they are separated from the mother and grouped together with other "weanlings"; continuing to grow and learn. The following Spring, the developing horses, now known as "yearlings" will begin to be taught the ways of the racetrack. Beginning with the first days of placing a saddle on their back, they are being trained for a career at the races. The young horses begin training at on their "home" farm or shipped to a training facility to be "broken" for riding and eventually racing.</p>
<p>Developing a good racehorse takes considerable time and patience. Training begins slowly, with light jogs and gallops around the track; developing a routine to get the horse accustomed to track life. Later, serious training begins; they start to put in longer gallops to build stamina, and work their way up to a "two-minute lick", meaning a robust mile gallop in two minutes. As it gets closer to their second birthday, the casual farm life has been left behind. Horses at this age now begin workouts, usually starting with 1/8 of a mile, called their first "blowout", and working their way up as the day of moving from training center to racetrack nears.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    