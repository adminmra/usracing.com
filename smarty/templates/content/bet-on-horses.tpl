
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
<!-- --------------------- content starts here ---------------------- -->
{*<p><a href="/signup/" rel="nofollow"><img class="img-responsive" src="/img/best-horse-racing-betting-site.jpg" alt="Bet on Horses">  </a></p>*}
{*<p>You can bet from your home computer, mobile or tablet at BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">{$BONUS_WELCOME} Bonus</a> for new members. <a href="/signup/">Join </a> Today!</blockquote></p>
<h2>Why Bet on Horses with BUSR?*}
</h2>
                    <p>Get up to a {$BONUS_WELCOME} bonus Bet on horses at over 200 <a href="/racetracks">racetracks</a> worldwide. Providing a fun, easy and SECURE way to bet on horses online, US Racing is regarded as one of the <a href="">best online horse betting websites</a>.  US Racing provides odds for every major racetrack and graded stakes races in North America and overseas. Bet on horses racing in the <a href="/breeders-cup">Breeders' Cup</a>, <a href="/kentucky-derby">Kentucky Derby</a>, <a href="/preakness-stakes">Preakness Stakes</a>, <a href="/belmont-stakes">Belmont Stakes</a> and everything in between. </p>
                    <p>US Racing gives you access to the latest odds, horse betting tips, news and more. Get  horse racing betting online with video broadcasts of the races and horse racing replays. US Racing offers unlimited 8% Rebates to its members as well as weekly cash bonuses and free wagers.</p>
                    <p></p>
                    {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}  
                    <h2>Betting on Horses Online</h2>
                    <p>Bet on horses from your living room, favorite coffee shop, the office or <a href="/mobile-horse-betting">bet on horses with with your mobile phone</a> while traveling to your next destination! It's easy, convenient and most importantly, US Racing is user friendly and we are setting the pace in off track betting on horses.</p>
                    <p>Looking for something different for a Saturday afternoon get together with friends?  Some of our race fans throw OTB parties. Nothing beats a little friendly competition among friends as you bet on your favorite horses online and watch horse racing live feed on the big screen TV. Visit Saratoga, Churchill Downs and Belmont Park all in the same day (virtually, of course!)</p>
                    <p>Bet online from your laptop, smart phone, Mac or PC and check out our up-to-the-minute betting odds. Once the Kentucky Derby, Preakness Stakes and Belmont Stakes come and go, move on to the Summer Horse Racing series at <a href="/saratoga">Saratoga</a> & <a href="/del-mar">Del Mar</a> and on to the <a href="/breeders-cup/challenge">Breeders' Cup Challenge</a>. In the Fall, we keep an eye on the up and coming two year olds as the <a href="/road-to-the-roses">Road to the Roses</a> begins and brings us full circle again to Louisville, Kentucky, on the first Saturday in May.</p>
                   <div class="tag-box tag-box-v4">
<h2>Your BUSR Membership Includes</h2> 
<ul>

<li>200+ <a href="/racetracks">Racetracks</a></li>
<li>Great deposit methods such as Bitcoin and Credit Card</li>
<li>Quick Payouts – Get paid FAST. How fast? We have the fastest payouts of any online horse racing site.</li>
{* <li>Live Video (where available)</li> *}
<li><a href="/mobile-horse-betting">Mobile Horse Betting</a> - Play on your Mobile Device or Tablet</li>
<li>Free <a href="/news"> Horse Racing News</a></li>
<li><a href="/cash-bonus">Cash Bonuses</a></li>
<li><a href="/rebates">Horse Betting Rebates</a> up to 8%! And no limits</li>
{*
<li>Daily Race of the Day Special</li>
<li>Weekly Race Special</li>
*}
<li>Excellent Customer Support – The customer comes first at BUSR. We will always try our best to deliver quality service to our valued members</li>
</ul>
 </div>
                    
      
<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}