{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  What is a Pick Three?</h1></div>
<p>You must select the correct first place finisher in each of the three consecutive races that make up the Pick Three. The bet must be placed before the first race on the Pick Three.</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
