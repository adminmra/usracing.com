{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="headline"><h1>Horse Betting:  What is the Across the Board?</h1></div>
<p><strong>Across the board</strong> - You are betting win, place and show on one ticket. You'll collect all three pay-outs if the horse comes in first; place and show if the horse comes in second, and show only if the horse comes in third.</p>
        
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
