{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  Quarter Horse Tips</h1></div>
{* <p><a href="/horse-betting/quarter-horse-tips"><img class="img-responsive" src="/img/quarter-horse-racing.jpg" alt="Quarter Horse Racing">  </a></p> *}
<p><strong>ITEMS TO CONSIDER WHEN WAGERING ON QUARTER HORSE RACING</strong></p>
<p>Following are some handicapping factors that can be used to aid in placing a wager:</p>
<p>Straightaway American Quarter Horse racing is an all-out burst of speed from the starting gate with every horse trying to put a head in front at the finish. There is no time to maneuver for position or come from behind in the final stretch run as in Thoroughbred racing. Therefore, the experienced handicapper can concentrate on speed, class, jockey/trainer combinations and track conditions without having to spend time trying to predict how the race will be run.</p>
<p>The American Quarter Horse may be America's most consistent athlete. In 1993, American Quarter Horse wagering favorites finished in the money (first, second or third) 71% of the time, while winning 35%. Those are figures that can't be claimed by Thoroughbred or Standardbred racing! But that's not saying American Quarter Horse racing lacks the excitement of winning long shots. With 39% of the horses finishing first, second or third going off with odds of 5-1 or greater, you know the exotic wagers must have paid some handsome rewards.</p>
<p>The key is knowing how to cash in on those rewards yourself. But how? The answer is knowledge. If you're a smart handicapper you'll do your homework by learning everything you can about the horse, its rider, trainer, bloodline, competition and even the surface the horse will run on.</p>
<p><strong>1. Class</strong><br />Class is probably the most important factor in handicapping. Analyze everything you see, hear or read in the context of class. In the most basic sense, class refers to the ability to win, produce winners and develop high quality, competitive races. Class not only involves racehorses, but sires and dams, owners, breeders, trainers, jockeys, races and even tracks. Think of class in terms of levels of excellence and a competitive edge. Here's an example of class in a racehorse compared to other athletes. During the 1987 National Football League strike, many talented athletes replaced striking players. The replacements made for some exciting Sunday afternoons. But only a few of the replacements were able to keep their jobs when the striking players returned. The reason? They were simply not up to the superior play of the regulars. In other words, they were outclassed. The same can be true of a racehorse. For instance, a horse that may win with a fast time in claiming races would probably lose when going against horses that regularly participate in stakes race competition with similar race times. Here's how class relates to a racetrack. A horse with a 96 speed index and first place finish at Sunland Park is not necessarily the same as a horse with a 96 speed index and first place finish at Remington Park. Although both are two of American Quarter Horse racing's finest racing facilities, Remington Park emphasizes American Quarter Horse racing, carries many more graded stakes and the average purse structure is significantly higher, thus attracting more of American Quarter Horse racing's premier performers.</p>
<p><strong>2. Before Race Day Preparations</strong><br />In order to evaluate the entrants in any race, you'll need historical data or past performances, as they are called.</p>
<p><strong>3. Reading between the lines</strong><br />The past performance information for each horse in a race is there in black and white for everyone wagering on the race to follow. The only way you can out-handicap the competition is if you can read between the lines.</p>
<p><strong>4. RACE FREQUENCY</strong><br />As many as 10 of a horse's past races are listed by date beginning with the most recent race. TIP - Look for excessive or irregular layoffs between races which could indicate a fitness problem that could keep the horse from running true to form. Conversely, look for a history of regular layoffs with an immediate return to peak form.</p>
<p><strong>5. TRACK CONDITION</strong><br />Weather can change a track's condition quickly. Dirt tracks are rated as follows: ft-fast; sy-sloppy; m-muddy; gd-good; sl-slow; hy-heavy; fr-frozen. TIP - Horses that performed well in conditions similar to today's could have the edge.</p>
<p><strong>6. DISTANCE</strong><br />There are three types of races in American Quarter Horse racing: short sprints of 220, 250, 300, 330 and 350 yards; long sprints of 400, 440, 550 and 660 yards; and distance or races around one turn of 770, 870 and 1,000 yards. TIP - Determine the horse's suitability to today's distance. A horse that performs well at short distances could fade during a longer race. And conversely, a horse that breaks slowly but performs well in the stretch may need the extra distance to win. In some cases, American Quarter Horses, which have not been top performers at short or long sprints, may become competitive at 870 yards.</p>
<p><strong>7. TYPES OF RACES</strong><br />Maiden, Speed Index, Trial, Claiming, Allowance, Handicap and Stakes. You'll find a more complete description later and in the glossary. TIP - Think of race classifications as levels of the class of horse they attract with stakes races being the highest and $2,000 maiden claiming the lowest. The conditions of a race (non-winners of two, three or four) or claiming prices ($2,000-$20,000) are significant differences in the same type of race.</p>
<p><strong>8. POST POSITION</strong><br />At certain times or conditions, tracks might have surfaces which favor the inside, middle or outside post positions. Most programs list the percentage of wins from each post position. Some horses also favor certain post positions. TIP - On races around the turn, unless a horse has a great deal of early speed to go to the front and take the inside rail, an outside post position is definitely a disadvantage. A fast-breaking horse that usually wins when it breaks first can have an edge when positioned with room to run. Look for horses that break well with a post position between two horses that generally do not get away fast. In straightaway races, the one hole is generally a disadvantage, while the outside might be an advantage.</p>
<p><strong>9. POSITION CALLS</strong><br />A horse's position during a race and its lengths behind the front-runner are generally described at four locations along the race: the break call at two strides or ten yards from the starting gate; the first stretch call at the 1/8 pole or 220 yards from the finish; the second stretch call at the 1/16 pole or 110 yards from the finish; and at the finish. TIP - Positioning and ability to make up ground can reveal a lot about the horse you're evaluating; his suitability to today's distance; and a pattern of improvement as the race progresses.</p>
<p><strong>10. MEDICATIONS</strong><br />The two types of legally permitted medications are Bute and Lasix. Bute is an anti-inflammatory drug used to reduce stiffness. Lasix is a diuretic used to treat respiratory bleeding some athletes experience. TIP - If a horse is on the first-time Lasix list and has shown good early performance in past races but has faded toward the end, the effects of Lasix might make a difference in his next race.</p>
<p><strong>11. EQUIPMENT</strong><br />The symbol "b" indicates the horse wore blinkers during a race. TIP - A horse which has been running erratically and shows to be wearing blinkers for the first time might be a good wager.</p>
<p><strong>12. WEIGHT</strong><br />The total amount of weight a horse is required to carry (including jockey and tack). TIP - In American Quarter Horse racing, weight is not a major factor in the shorter races. However, in races of 440 yards or more, it does have some importance. The horses with the best performance records may be required to carry the most weight in an attempt to make a more even contest. Look for a horse's ability to carry more weight in his previous races.</p>
<p><strong>13. TIME and INDIVIDUAL TIME</strong><br />The time of the race (the winner's time) and the time of the individual horse are shown in seconds and hundredths of seconds. TIP - Use best recent times at today's distance, conditions and racetrack. Keep the different elements of class in mind while making your comparisons.</p>
<p><strong>14. SPEED INDEX</strong><br />The speed index is an evaluation of a horse's speed in a race versus the three fastest winning times for the same distance each year for the previous three years at the same racetrack. TIP - An average of best recent speed indexes is a good basis for comparison. Again, keep the elements of class, wind and track condition in mind.</p>
<p><strong>15. JOCKEYS</strong><br />At any given track, there is a broad range of talent. Generally speaking, the best horses will have the best riders (ones who can be found on the leading rider list in your program). TIP - Jockey changes can affect the outcome of a race. For instance, if you note that a leading jockey has been taken off his regular mount and switched to another, you could have a better chance of a winning wager on his new mount.</p>
<p><strong>16. TRAINERS</strong><br />As with jockeys, any given track will have a broad range of trainers, with varying degrees of expertise and experience. TIP - Pay attention to their winning percentages, not just their total wins, as a good trainer with a few horses will never lead the trainer's list. Try to keep track of a trainer's ability with different classes or ages of horses. Some trainers are better with claiming horses, while others concentrate on stakes performers.</p>
<p><strong>17. BREEDING</strong><br />A listing of horse's sire, dam and dam's sire. TIP - As a general rule, pedigree has a significant influence on a horse's natural ability to run. Keep note of the leading sires and dams as well as breeders and owners when considering class. This is especially true when handicapping two-year-olds and first-time starters.</p>
<p><strong>18. MONEY LINE</strong><br />The number of starts, wins, places (second), shows (thirds) and purse money won this year and last year. The lifetime total of wins and in-the-money (first, seconds and thirds) and lifetime purse money. TIP - Can be an indication of improvement or deterioration and class. Compare percentages of wins, in-the-money or average money earned per start.</p>
<p><strong>GLOSSARY OF KEY TERMS</strong></p>
<p><strong>Blanket Finish</strong> - Or <strong>photo-finish</strong> in which two or more horses are very close at finish (one can "Throw a blanket over them"). Very common in American Quarter Horse racing.</p>
<p><strong>Bullring</strong> - A racetrack with either a half-mile or 5/8ths mile oval.</p>
<p><strong>Register of Merit</strong> - A Register of Merit is designed to establish a record of outstanding performance. There are three Registers of Merit- one for racing, one for halter and one for performance events- but not a separate Register of Merit for each performance event. A horse has received at least one official Speed Index Rating of 80 or higher in racing. Qualified horses registered with the Jockey Club of New York City will be listed and treated as racing Register of Merit qualifiers for all purposes except that they shall not receive a certificate of Register of Merit or year-end awards. Until 1956 a Grade A was a 75 or better speed index, 1957 to 1975 Grade AA was an 85 or better. From 1976 to 1985 a Register of merit could be earned by an 80 or better speed index or by earning 10 racing points. From 1986 to present a Register of Merit could only be earned by a speed index of 80 or better.</p>

<table border="1" cellspacing="0" cellpadding="3" width="200" bordercolor="#999999">
<tbody>
<tr>
<td class="bodyregular" width="147" bgcolor="#cccccc">
<div>TAAA-</div>
</td>
<td class="bodyregular" width="251" bgcolor="#cccccc">
<div>100</div>
</td>
</tr>
<tr class="bodyregular">
<td>
<div>AAA-</div>
</td>
<td>
<div>95</div>
</td>
</tr>
<tr class="bodyregular" bgcolor="#cccccc">
<td bgcolor="#cccccc">
<div>AA-</div>
</td>
<td bgcolor="#cccccc">
<div>85</div>
</td>
</tr>
<tr class="bodyregular">
<td>
<div>A-</div>
</td>
<td>
<div>75</div>
</td>
</tr>
<tr class="bodyregular" bgcolor="#cccccc">
<td>
<div>B-</div>
</td>
<td>
<div>65</div>
</td>
</tr>
<tr class="bodyregular" bgcolor="#cccccc">
<td bgcolor="#ffffff">
<div>C-</div>
</td>
<td bgcolor="#ffffff">
<div>55</div>
</td>
</tr>
<tr class="bodyregular" bgcolor="#cccccc">
<td>
<div>D-</div>
</td>
<td>
<div>45</div>
</td>
</tr>
</tbody>
</table>
<p><strong>Schooling Race</strong> - A non-pari-mutuel preparatory race, which conforms to requirements adopted by the state racing commission.</p>
<p><strong>Tongue Strap</strong> - Strap or tape bandage used to tie down a horse's tongue to prevent choking in a race or workout.</p>
<p><strong>Trial</strong> - Race in which eligible horses compete to determine the finalists in a nomination race.</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
