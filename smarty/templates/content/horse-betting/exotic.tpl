{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                               
          
<div class="headline"><h1>Horse Betting:  What is the Exotic Horse Bet</h1></div>
<p>These bets require you to pick more than one horse in the correct order, so they are generally harder to win than a straight wager. However, the potential for reward or the "big score" is much greater.</p>
      </div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
