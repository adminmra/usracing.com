{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  What is the Quinella?</h1></div>

<p>You win when your horses finish first and second, in either order. Example: Tell the clerk, "Give me a $2 quinella, 3-7." In order to win, #3 and #7 may finish first and second in either order.</p>
        
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
