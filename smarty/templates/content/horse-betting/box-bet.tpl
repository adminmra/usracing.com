{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
 <div class="justify-left">     
<div class="headline"><h1>Horse Betting:  What is a Box Bet?</h1></div>
<p>The term "box" tells the mutuel clerk that you want all possible combinations of the horses selected in wagers such as exactas, trifectas and superfectas.</p>
<p>Example: Tell the clerk, "Give me a $1 trifecta box numbers 1, 3, and 7. If you box your selections, as long as the horses you have picked finish first, second, and third in any order, you win.</p>
<p>As the number of horses in the bet are increased, the key and box bets can quickly become very costly:<br />&nbsp;</p>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0"><caption>Exacta Boxes</caption>
<tbody>
<tr>
<td>Number of horses in box</td>
<td>Cost for $1 bet</td>
</tr>
<tr>
<td>3</td>
<td>$6</td>
</tr>
<tr>
<td>4</td>
<td>$12</td>
</tr>
<tr>
<td>5</td>
<td>$20</td>
</tr>
<tr>
<td>6</td>
<td>$30</td>
</tr>
</tbody>
</table>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0"><caption>Trifecta Boxes</caption>
<tbody>
<tr>
<td>Number of horses in box</td>
<td>Cost for $1 bet</td>
</tr>
<tr>
<td>3</td>
<td>$6</td>
</tr>
<tr>
<td>4</td>
<td>$24</td>
</tr>
<tr>
<td>5</td>
<td>$60</td>
</tr>
<tr>
<td>6</td>
<td>$120</td>
</tr>
</tbody>
</table>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}

