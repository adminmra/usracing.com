{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Kelly Advantage Calculator</h1></div>

<div class="content">
  <div>
   
    <!--
    <h2>Results</h2>
    <ul>
      <li>The odds are in your favor, but read the following carefully:</li>
      <li>According to the Kelly criterion your optimal bet is about 5.71% of your capital, or <b>$57.00</b>.</li>
      <li>On 40% of similar occasions, you would expect to gain $99.75 in addition to your stake of $57.00 being returned.</li>
      <li>But on those occasions when you lose, you will lose your stake of $57.00.</li>
      <li>Your fortune will grow, on average, by about 0.28% on each bet.</li>
      <li>Bets have been rounded <em>down</em> to the nearest multiple of $1.00.</li>
      <li>If you do not bet exactly $57.00, you should bet <em>less than $57.00</em>.</li>
      <li>The outcome of this bet is assumed to have no relationship to any other bet you make.</li>
      <li>The Kelly criterion is <i>maximally aggressive</i> — it seeks to increase
          capital at the maximum rate possible. Professional gamblers typically
          take a less aggressive approach, and generally won't bet more than
          about 2.5% of their bankroll on any wager. In this case that would be <b>$25.00.</b>
      </li>
      <li>A common strategy (see discussion below) is to wager half the Kelly amount, which in this case would be <b>$28.00.</b></li>
      <li>If your estimated probability of 40% is too high, you will bet too much and lose over time. Make sure you are using a conservative (low) estimate. </li>
      <li>Please read the <a href="#disclaimer">disclaimer</a> as well as the notes below.</li>
    </ul>
    -->
   
      <p>
        Among the most well-known bet optimization methods is the Kelly Criterion. Named after its inventor, John L. Kelly of AT&T Bell Laboratories, the Kelly Criterion is a formula designed to boost betting profits by determining the ideal percentage of capital to be staked on each wagering event.
      </p>
 <p>       <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Kelly Advantage Calculator</caption>
        <tbody>
          <tr>
            <td class="V">Win Rate:</td>
            <td class="F"><input type="number" id="WinRate" name="WinRate" size="8" value="" autocomplete="off" min="1" max="100" ></td>
            <!--style="padding: 0 3.9em;"-->
          </tr>
          <tr>
            <td class="V">Average Odds to $1:</td>
            <td class="F">
              <!--
              1) An input field for "Win Rate" (this will be a percentage). --- basicamente un input
              2) An input field for "Average Odds to $1" (a number >0) --  otro input
              3) An output field "Kelly Advantage"
              -->

              <input type="number" name="oddsWin" size="8" value="" id="oddsWin" autocomplete="off" min="1"  >
              <!--to
              <input type="number" name="oddsLose" size="4" value=""  id="oddsLose" autocomplete="off">-->
             <!-- e.g. 7 to 4, 2 to 1, etc.<br>
              Odds of <i>2 to 1 <u>on</u></i> should be entered as <i>1 to 2</i>,<br>
              Odds of <i>11 to 10 <u>on</u></i> should be entered as <i>10 to 11</i>
              -->
            </td>
          </tr>
          <tr>
            <td class="V">Kelly Advantage: </td>
            <td class="F">
            <input type="text" name="percentWin" size="8" value="" id="percentWin" autocomplete="off" readonly="readonly" style="border-style: none; background: #f9f9f9   ">%
            </td>
          </tr>
        <!--
        <tr>
          <td class="V">Bets must be multiples of: </td>
          <td class="F"> $<input type="text" name="betMultiple" size="8" value="1.00"> </td>
        </tr>
        <tr>
          <td class="V">The minimum bet allowed is: </td>
          <td class="F"> $<input type="text" name="betMinimum" size="8" value="10.00"> </td>
        </tr>
        -->
      </tbody>
      </table>
     <center> <button class="btn btn-sm btn-red" id="btn_calculate"  >Calculate Bet</button></center>
    </p>
      <p>The formula is as follows:</p>
      <h2 >WINNING RATE - LOSING RATE / AVG. WINNING ODDS</h2>
      <p><i>(Note: Remember your order of operations — division before subtraction.)</i></p>
      <p>Hence, if one typically wins 30 percent of her bets and they pay $7 on average, the amount wagered to optimize profits would be approximately two percent of capital (0.3 – 0.7 ÷ 2.5 = 2 percent).</p>
      <p>Because there is such risk of tapping out if one overestimates his true advantage, most bettors use a fraction of Kelly to determine their stake.</p>
      <p>Now, it should be clear from this explanation that the Kelly Criterion is used only to exploit an edge, not to create one. In other words, if the Kelly Advantage derived from this calculator or the formula above is negative, you don’t have an edge; you’re better off trusting in Fortuna, the Roman god of fortune and chance, than Kelly.</p>
      <p>For example, let’s say that through record-keeping, you discover that you cash 30 percent of your win bets (like above), but with an average winning payoff of $6 (or 2-1 odds). This yields a -5 percent Kelly Advantage… which is no advantage at all.</p>
      <p>Please read this carefully: You cannot, via any wagering method, scheme or spiritual revelation, turn a negative return on investment (ROI) into a positive one. The Kelly Criterion only works if one has an advantage over the game they are playing. (You can turn a negative ROI into a positive one — at least in games of skill like horse racing — through greater education and better information.)</p>
      <p>Of course, this begs the question: Why use the Kelly Criterion at all? If one already has an edge, what difference does it make whether he follows the advice of John Kelly or R. Kelly? The answer is simple: It gives one an idea as to how much she should bet on each race to maximize profits (again, assuming one has an inkling as to what his betting statistics are — and they show a positive return).</p>
      <p>For instance, let’s say you plan on risking up to $1,000 on the card at Belmont Park today. The first race is for 3-year-old state-bred maidens and your records show that you typically hit 20 percent winners at average odds of 3-1 in such affairs. Plugging those numbers into the Kelly calculator, we find that your “advantage” is -6.67 percent. This means you have no edge in the race and should pass (or bet very lightly).</p>
      <p>
        The second race is a $40K claiming sprint for older horses — you love this race and have a “best bet” named Ima Winner. Traditionally, your best bets have hit at a 40 percent rate with average winning odds of 2-1 — a whopping 10 percent Kelly edge. Hence you wager $100 (10 percent of your $1,000) bankroll.
      </p>
      <p>Ima Winner lives up to its name and pays $6.40. You collect $220 in profit and your bankroll now stands at $1,220.</p>
      <p>
        In the third event on the Belmont program, you find an angle play that has a 15 percent strike rate and 8-1 average winning odds. This equates to a 4.38 percent Kelly bet, or $53 of your current bankroll ($1,220 x 4.38% = $53).
      </p>
      <p>Unfortunately, the angle play finishes last and your bankroll now stands at $1,167.</p>
      <p>You continue sizing your bets in this manner for the rest of the card, adjusting your bankroll as you go.</p>
    <p>
      Again, it needs to be stressed that the Kelly Criterion is not a handicapping tool, it just helps one make better betting decisions. Still, it’s a lot of fun to use and can really help those who too often find themselves on the losing side of the ledger after a great day of handicapping.
    </p>
  </div>

  <script>

  {literal}
    var btn_calculate = document.querySelector( "#btn_calculate" );

    btn_calculate.addEventListener( "click" , function( evt ){
      //alert();
      if ( document.querySelector( "input#WinRate" ).value == '' || document.querySelector( "input#oddsWin" ).value == '' ) {
        return
      }
      WinRate    = parseFloat( document.querySelector( "input#WinRate" ).value ) / 100 ;
      oddsWin    = parseFloat( document.querySelector( "input#oddsWin" ).value ) ;
      //oddsLose   = parseFloat( document.querySelector( "input#oddsLose" ).value ) ;
      percentWin =  ( WinRate - ( ( 1 - WinRate ) / oddsWin ) ) * 100;

      if(percentWin>100)percentWin=100

      output = percentWin.toFixed(2);
      console.log("output: "+output);

      document.querySelector( "input#percentWin" ).value = output;
    })
  {/literal}
  </script>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
