{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                                
          
<div class="headline"><h1>Horse Betting: How to Place a Straight Wager</h1></div>
<p>A Straight Wager is a bet on the Win, Place or Show:

<li>Win is where the horse finishes first</li>
<li> Place being first or second and </li>
<li>Show being finishing in the top 3 spots.</li>
</p> 
<p>The minimum that you are allowed to wager is $2 for straight bet.  This means that all the  the final winning prices are all based on a $2 bet.</p><p> <strong>For example:</strong>  if you bet $10 to win, and the win price was $5.00, you would win a total of $25.</p>

<p>The math works like this: $10 (amount bet) multiplied by $5 (winning payoff for 5/1 odds) divided by 2 (as the price was based on a $2 wager) = $25.  </p> 

</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
