{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                          
          
<div class="headline"><h1>Horse Betting:  How to Key a Horse</h1></div>

<p>For all exotic wagers, you may key a single horse in any race or in a certain finishing position. When you feel that one of your selections will finish in a certain position, either first, second, third or fourth, the horse you select to finish in a certain position is your "key" horse. The other horses you have picked may then finish in any order and you are a winner.</p>
<p>For example, look at a trifecta with the #1 the "key" horse in the race. Remember, to cash a trifecta ticket you must also have the second and third-place finishers, in order; and for this example, say the #3, #7 and #10 all look like possible contenders. In this situation, the #1 is "keyed" on top (to win) with the #3, #4 and #5. If #1 wins and any of the other selections finish second and third, you win. Example: Tell the clerk, "Give me a $1 trifecta key 1 with 3/7/10 with 3/7/10." To calculate the cost of the bet, multiply the number of possible horses in each position by the dollar amount bet. In the above example, the cost would be 1x3x2x$1=$6.</p>
        
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
