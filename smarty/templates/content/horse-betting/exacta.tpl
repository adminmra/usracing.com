{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  What is an Exacta?</h1></div>

<p> <i>"Give me a $20 exacta, 3-7.  I have have this in the bag."</i></p>
        
<h2>What is an Exacta,  Exactor or Perfecta?</h2>       

<p>They are all one in the same.</p><p>At the ticket window, pick the two horses that will finish in first and second place in the EXACT order.  You win your bet if the horses you pick finish in first and second place in the EXACT order.

The minimum amount that can be wagered is $1 but keep in mind that payouts would be paid out at half as the odds are calculated at a $2 bet value.</p>

<h2>Exacta Box</h2> 

<p> With an Exacta Box you will win your bet if the horses you pick finish in first and second place in either order with as many horses as you wish.</p>

<p> The minimum amount that can be wagered is $1 per combination. So a 2 horse exacta box at $1 will cost a minimum of $2.  A further breakdown:</p>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Exacta Box</caption>
<tbody>
<tr > <td>$1 exacta box of 3 horses =   6 combinations  = $6</td></tr>
<tr > <td>$1 exacta box of 4 horses =  12 combinations = $12</td></tr>
<tr > <td>$1 exacta box of 5 horses = 20 combinations  = $20</td></tr>
<tr > <td>$1 exacta box of 6 horses =  30 combinations  = $30</td></tr>
<tr > <td>$1 exacta box of 7 horses =   42 combinations = $42</td></tr>
<tr > <td>$1 exacta box of 8 horses =   56 combinations  = $56</td></tr>
 
</tbody>
</table>
{* </div> *}

<p>Exacta boxes they are the most popular method of playing exactas because they are easier to understand and offer the greatest chance of cashing a ticket.  </p> 

<p>It is important to note that  this is  not a wise betting strategy simply because all of the horses in the box do not have the same chance of finishing first or second.  Your ticket may pay out at a loss.</p> 

<p>Because exacta boxes rate each combination in the box as having the exact same probability of winning (which is never the case) they produce a bias in the exacta betting pools. It is this bias that smart bettors look for and try to take advantage of.</p> 

 <p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Exacta Boxes</a></p> 
<hr>

<h2>Exacta Wheels</h2> 

<p>Pick a horse to finish either first or second and wheel it in the exacta, you cover every combination so that if your horse finishes in the first or second position, you will have the winning combination. </p>

<p>For example, if you were to play a $1 exacta wheel  with a 'key' horse of #3 to come first, the #3 horse would have to win and any of 1,2,4,5,6,7,8 would have to finish second in order for you to cash your ticket.</p>

<p>Or, if you think the 'key' horse #3  has a better chance of finishing second, you might play a $1 exacta wheel. In this case any horse in the race could win and the 3 horse would have to finish second in order for you to cash your ticket.</p>

<p>Either way, in an eight-horse field it would cost you $7 as you are wagering 7 separate $1 exactas betting horses 3-1, 3-2, 3-4, 3-5, 3-6, 3-7, 3-8 or horses 1-3, 2-3, 4-3, 5-3, 6-3, 7-3, 8-3. </p>

<p>Exactas can also be played as part-wheels where  you might play a $1 exacta part-wheel horse #3 with 2, 4, 5 (three possible winning combinations of 3-2, 3-4, 3-5) at a cost of $3. Or you might play the part-wheel the other way, 2, 4, 5 with 3 (also three possible winning combinations of 2-3, 4-3, 5-3) at a cost of $3. Part exacta wheels reduce some of the bias of betting exacta boxes. </p>

 <p class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg">Bet Exacta Wheels</a></p>
<hr>
<h2>Exacta Betting Strategies</h2>

<p>Through handicapping you determine horse #5 has a 50% chance of winning the race, horse #6 has a 20% chance of winning and horse #7 has a 10% chance of winning. You will be spending $60 on this race. </p>


<p><strong>Option 1. </strong> <br>
You play an exacta box of 5-6-7 which is 6 exactas at $10 each.</p>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Exacta Wheel Option 1</caption>
<tbody>
<tr><td>$10 exacta 5-6 = $10</td></tr>
<tr><td>$10 exacta 5-7 = $10</td></tr>
<tr><td>$10 exacta 6-5 = $10</td></tr>
<tr><td>$10 exacta 6-7 = $10</td></tr>

<tr><td>$10 exacta 7-5 = $10</td></tr>

<tr><td>$10 exacta 7-6 = $10</td></tr>
</tbody>
</table>{* </div> *}

<p><strong>Option 2.</strong><br>
You play an exacta part-wheel  keying #5 to win with 6, 7 and a second part-wheel with  6, 7 with keying #5 to place which is 4 exactas at $15 each.</p>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Exacta Wheel Option 2</caption>
<tbody>
<tr><td>$15 exacta 5-6 = $15</td></tr>
<tr><td>$15 exacta 5-7 = $15</td></tr>
<tr><td>$15 exacta 6-5 = $15</td></tr>
<tr><td>$15 exacta 7-5 = $15</td></tr>
</tbody>
</table>{* </div> *}

<p><strong>Option 3.</strong><br>
You play an exacta part-wheel  keying #5 to win with 6, 7 and a second part-wheel with  6, 7 with keying #5 to place  based on the winning percentages you determined through handicapping: #5 at 50%, #6 at20%, and #7 at 10%.  </p>

     <table class="data table table-condensed table-striped table-bordered ordenable"  width="100%" cellspacing="0" cellpadding="0" border="0">
      <caption>Exacta Wheel Option 3</caption>
<tbody>

<tr><td>$30 exacta 5-6</td></tr>
<tr><td>$20 exacta 5-7</td></tr>
<tr><td>$6 exacta 6-5</td></tr>
<tr><td>$4 exacta 7-5</td></tr>
</tbody>
</table>{* </div> *}


<p><strong>Lets break it down by risk vs reward.</strong></p>

<p><strong>Option 1</strong><br>
The exacta box rates each possible combination as having the same probability of winning, which according to your handicapping is not correct. </p>

<p><strong>Option 2.</strong><br>
The part-wheel in this case is more efficient, leaving out the less probable combinations of 6-7 and 7-6, but still rating both the 6 and 7 horses as having the same probability of winning or finishing second.</p>

<p><strong>Option 3.</strong><br>
Offers the highest risk/reward ratio as determined #5 at 50%, #6 at20%, and #7 at 10%. </p> 

<p><strong>The results:</strong><br>
You killed it!  The race ran exactly as your handicapping said it would – the 5 horse won and the 6 horse ran second. The $2 exacta 5-6 paid $20 so your returns are $10 per dollar wagered.   Let's see your possible payouts:</p>

<p><strong>Option 1.  Exactor Box</strong><br>
$10 exacta 5-6 pays $100.  $100 - $60 ticket cost =  $40 Profit</p>

<p><strong>Option 2. Part Wheel</strong><br>
$15 exacta 5-6 pays  $150.  $150 - $60 ticket cost  = $90 Profit</p>

<p><strong>Option 3.  Part Wheel correlated to probabilities. </strong><br>
$30 exacta 5-6 pays $300.  $300 - $60 ticket cost = $240 Profit</p>

<p>The final exacta wagering strategy, which places more money on the higher probability combinations as identified by your handicapping.</p>

<p>Due to bias in the exacta pools, it is best to avoid combining favorites or long shots in your exactas. The best payouts are for  two medium-priced horses.</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
