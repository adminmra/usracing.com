{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                             
<div class="headline"><h1>Horse Betting:  What is the Daily Double?</h1></div>
<p>Your horses must come in first in two consecutive races. You place your bet before the first of the two races -- it's all or nothing. Example: Tell the clerk, "Give me a $2 double 3-7."</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
