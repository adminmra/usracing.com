{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">   
<div class="headline"><h1>Horse Betting:  What is the Trifecta?</h1></div>
<p>You must select the first three horses in the exact order of finish. It's tough to pull off, but in addition to a huge potential payoff you get bragging rights for the next 24 hours, as in "I hit the trifecta!"</p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
