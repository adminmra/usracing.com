{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  Maiden Races</h1></div>

<p>The term "maiden" refers to a horse that has never won a race. There are two types of maiden races: maiden special weight and maiden claiming. The maiden special weight race, also called a maiden allowance, is the highest quality. A horse cannot be claimed out of a maiden special weight race. As a general rule, the maiden special weight races have the best youngest horses on the racetrack and also often some of the best bred horses. The purses are also better than in a maiden claiming race. In maiden claiming races, all the horses are for sale at the price stated in the program. Claiming prices vary from track to track, and can range from $5,000 to $150,000.</p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
