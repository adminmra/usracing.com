{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">
<div class="headline"><h1>Horse Betting:  How to Place a Show Bet</h1></div>
<p><strong>Show</strong> - Your horse comes in first, second, or third. (On average 65% of favorites win, place or show.)</p>
<h2>How to Place a Show Bet</h2>
<p><strong>Show</strong> - Your horse comes in first, second, or third. (On average 65% of favorites win, place or show.) The payout will be the same in either of the three positions.  Also referred to as in the money.</p>
<p>Show bets are typically much more fun for new bettors.  Since it is the most conservative bet at the track, the payouts will be the lowest, however  it offers you the highest probability of winning.   At the ticket window just say: “$2 to show on number 5”. At US Racing it is as easy as selecting your horse and wager amount.  </p>
<p>Show bets are more profitable when field size is large, preferably 8 or more. </p>
<p>The objective of betting on horses is to win. Show bets offer the highest chances of winning out of all the bets thereby making this wager the best for those new to racing.</p>
<h2>Show Bet Strategies:</h2>
<p><strong>Show Parlay.</strong>  This is when make a show bet on a horse and then wager the winnings of that wager on another race. Also, the winnings can be divided up and to place two show wagers on different races.  </p>
<p>Sometimes, a group people will all put in $5 to wager show bet to win dinner that evening.  The winnings of this 'table bet' can be parlayed into another wager.</p>
<p><strong>Insurance Bets.</strong>  Sometimes bettors use show bets for insurance.  They will wager $50 to win on a horse but then back it up with a $20 show bet in case the winner loses.  </p>
<p>Also, at times, the estimated show payoff will be the same or larger than the place payoff. In that case, why not bet to show and take advantage of an aberration in the betting pools?</p>
<p><strong>Better Pools.</strong> Be sure to check the show pool amount as it maybe larger than the place pool.</p>
{*Calculating Show Payouts:<!--edit below-->

Here's the equation for figuring an estimated place payoff:

1. Start with the total amount bet to place and subtract 15 percent for the takeout (the percentage withheld from the betting pool by the host track).

2. From that total, subtract the place money wagered on your horse and the highest amount of place money bet on another horse to get the profit from the place pool.

3. Split the profit amount between the two place horses.

4. Divide that amount by the number of $2 place bets on your horse.

5. Add $2, and you get your estimate place price.

The estimate show payoffs are done the same way as the place payoffs. In step 2, subtract the show money wagered on your horse and the two highest amounts of show money bet on other horses. In step 3, divide the profits by three — not two — horses.*}

 
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
