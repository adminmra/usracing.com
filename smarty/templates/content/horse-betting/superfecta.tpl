{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                 
          
<div class="headline"><h1>Horse Betting:  What is a Superfecta?</h1></div>

<p>You must select the first four horses in the exact order of finish. (See <a href="/horse-betting/trifecta">Trifecta</a> for bragging rights.)</p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
