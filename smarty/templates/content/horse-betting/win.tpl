{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">                      
          

<p><strong>Win</strong> - Your horse must come in first. Simple stuff.
<p>Betting to win  is the easiest to wager to make because you can tell what your horse will roughly pay out from the odds posted on the board.  But keep in mind this changes due to the pooling of the wagers giving the exact odds just before the races starts.
<p>If you win the wager you will receive the payout plus your original bet.  So if the odds are 3-1 then you will $8 back on a $2 bet.  The math works out like this: <Blockquote>  3/1 (odds) x $2 (bet) = %6 + $2 (original bet returned) totaling $8.</blockquote>
<p>Betting to win is generally the preferred wager of experienced bettors and handicappers. Experienced bettor  wont wager on every race, they will wager on races that gives them an acceptable risk/reward ratio.  This comes with understanding the value of the wager.  To make money over the long haul, value is what you are trying to achieve. 
<h2>Beginners Tips and Win Bet Strategy</h2>
<p><strong>Avoid betting the favorite</strong> unless you are certain.  On average, only 30% of favorites win.  
</p>
<p><strong>Look for Overlays and Underlays.</strong>   An overlay is when the odds will pay out more than the bettor thinks they should.  So finding a horse that pays 5-1 when you believe it has a 50/50  or 2-1 chance of winning the races would be an Overlay representing a good value bet.
<p>An Underlay is exactly the opposite where the odds just dont work out in your favor for the horse you determine is going to win.  Why place a wager when your horse  has 5/4 odds and had only a 75% chance of winning?  
<p><strong>Bet to Show.</strong>  Most  handicapping books advise their readers to bet on a horse to win only and to avoid   place and show bets.  If you're a good handicapper and can pick a lot of winners and all the extra money you bet to win on a horse earns enough profit to compensate for all the lost place and show wagers that you would've cashed.
<p>In the long run this strategy will turn  out to be true, however, beginning horseplayers should bet to place and show to not be discouraged and get a feel for the game.
</p>
        
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
