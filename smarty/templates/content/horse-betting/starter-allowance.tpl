{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left"> 
<div class="headline"><h1>Horse Betting:  Starter Allowance Races</h1></div>
<p>Similar to an allowance race, a horse entered in a <strong>starter allowance</strong> race cannot be claimed. The horse, however, must have run at or below a certain claiming level (for example "$8,000 or less") during a designated time period (for example "in 2002-2003").</p>
<p>A starter allowance can bring together a wide range of claiming and allowance horses. If the race is a handicap, then the Racing Secretary assigns weight to each horse similar to a stakes handicap.</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
