{include file="/home/ah/allhorse/public_html/usracing/horse-betting/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<div class="justify-left">         
<div class="headline"><h1>Horse Betting:  What is a Pick Four</h1></div>
<p>You must select the correct first place finisher in each of the four consecutive races that make up the Pick Four. The bet must be placed before the first race on the Pick Four.</p>
</div>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/horse-betting/end-of-page-blocks.tpl"}
