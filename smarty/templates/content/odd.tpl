{literal}
  <style>
      @media (min-width: 320px) and (max-width: 480px) {
        .newHero {
          background-image: url({/literal} {$hero_mobile} {literal}) !important;
          background-position-y: 0%;
        } 
      }

      @media (max-width: 991px) {
        #right-col {
          display: flex;
          justify-content: center;
        }
      }
    </style>
{/literal}

{literal}
<script src="/assets/js/iframeResizer.min.js"></script>

<script>
    $(document).ready(function () {
        $("#iframeResize").iFrameResize({
            log: false,                   // Enable console logging
            enablePublicMethods: false,  // Enable methods within iframe hosted page
            checkOrigin: false,
            bodyMargin: 0

        });
    })
</script>
{/literal}

{include file='inc/left-nav-btn.tpl'}

{literal}
<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Horse Racing  Odds | Online Horse Betting",
  "description": "US Racing provides  horse betting. Bet on horses, sports and casino games.  Over 200 racetracks and rebates paid daily. Get a 10% Cash Bonus added to your first deposit.",
  "image": "https://www.usracing.com/img/best-horse-racing-betting-site.jpg",
  "startDate": "2018-11-27 07:19", 
  "endDate": "2020-11-30 09:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing | Online Horse Betting"
  },
    "offers": [
    {
      "@type": "Offer",
      "url": "https://www.usracing.com/promos/cash-bonus-150",
      "validFrom": "2015-10-01T00:01",
      "validThrough": "2026-01-31T23:59",
      "price": "150.00",
	  "availability": "https://schema.org/InStock",
      "priceCurrency": "USD"
    } ],
  "location": {
    "@type": "Place",
    "name": "US Racing | Online Horse Betting",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "123 street ,place park",
      "addressLocality": "miami",
      "addressRegion": "Florida",
      "postalCode": "33184",
      "addressCountry": "US"
    }
  }
}

</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Today's Horse Racing Odds",
    "item": "https://www.usracing.com/odds"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "2019 Kentucky Derby Odds",
    "item": "https://www.usracing.com/kentucky-derby/odds"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "Triple Crown Odds",
    "item": "https://www.usracing.com/bet-on/triple-crown"
  },
  {
    "@type": "ListItem",
    "position": 4,
    "name": "Pegasus World Cup Odds",
    "item": "https://www.usracing.com/pegasus-world-cup/odds"
  },
  {
    "@type": "ListItem",
    "position": 5,
    "name": "Pegasus World Cup Odds",
    "item": "https://www.usracing.com/pegasus-world-cup/odds"
  },
  {
    "@type": "ListItem",
    "position": 6,
    "name": "Grand National Odds",
    "item": "https://www.usracing.com/grand-national"
  },
  {
    "@type": "ListItem",
    "position": 7,
    "name": "Dubai World Cup Odds",
    "item": "https://www.usracing.com/dubai-world-cup"
  },
  {
    "@type": "ListItem",
    "position": 8,
    "name": "US Presidential Election Odds",
    "item": "https://www.usracing.com/odds/us-presidential-election"
  }
  ]
}
</script>
{/literal}
<div id="left-nav"> 
  
  <!-- ---------------------- left menu contents ---------------------- --> 
  
  {include file='menus/horsebetting.tpl'} 
  <!-- ---------------------- end left menu contents ------------------- --> 
</div>

<!--------- NEW HERO  ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>
  <div class="text text-md">{$h2} <br>
    <a href="/signup?ref={$ref}">
    <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a> </div>
</div>

<!--------- NEW HERO END ---------------------------------------->

<div id="main" class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="headline">
        <h1>{$h1}</h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div id="left-col" class="col-md-9">
    {*
      <p><a href="/signup?ref={$ref}"><img class="img-responsive" src="/img/horse-racing-odds.jpg" alt="Horse Racing Odds"> </a></p>
      *}
      <div class="content"> 
        
        <!-- --------------------- content starts here ---------------------- -->
        <p>These are the latest Morning Line odds for almost all worldwide racetracks. </p>
        {assign var="cta_append" value=" at any of these races"}
        <p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>
        <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
        <p>Please note:  The morning odds, also known as the 'Morning Lines' (M/L) are set by the track for the day's races.  During the course of the day up until the actual race, the actual odds may change despite the fact that posted M/L in the racebook does not change. Winners are always paid out at full track odds*. For more information on odds and payouts, please visit the BUSR Track Payouts in the clubhouse. </p>
        <center>
          <p>Live odds can take a moment to load.</p>
        </center>

<p>
<iframe src="https://rb.betusracing.ag/BOSSWagering/Racebook/InternetBetTaker/?siteid=usracing" frameborder="0" id="iframeResize" height="1250" width="100%" class=" racebook " scrolling="no" style="overflow: hidden; height: 2230px;"></iframe>
</p>
{*
<!--
        <p>

          <iframe src="https://rb.betusracing.ag/BOSSWagering/Racebook/InternetBetTaker/?siteid=usracing" scrolling="no" width="100%" height="750px" style="border:0;">Live Horse Racing Odds...</iframe>
        </p>
      -->
*}
        <h2>How to Calculate Betting Odds and Payoffs</h2>
        <p>Betting on the outcome of horse racing can be fun and profitable and, if you know what you're doing, you might even be able to beat the odds.</p>
        <p>The horse racing betting odds for each horse is displayed on a tote board at the track or on the betting ticket online. The basic information on the betting ticket window is the &quot;odds to win&quot; for each horse. Although it won't tell you how much the horse will pay, it does show the amount of profit you will get if you win and the amount you have to bet to get it.  For example, 3-5 odds means that for every $5.00 you wager, you'll win $3.00. </p>
        <p>Payouts are determined by how much is in the total pool (all the money bet on all the horses to win)  minus the &quot;take&quot; which is usually between 14%-20% depending on the track. The take varies from state to state and the money is used to pay taxes, purse money for the horsemen, expenses at the track, and the track's profit.</p>
        <p>Most tracks require a $2.00 minimum bet, here are the pay-offs for $2 win bets:</p>
        <p>&nbsp;</p> 
        <div class="table-responsive">
          <table class="data table table-condensed table-striped table-bordered"  border="1" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td width="80" align=center><b>ODDS</b></td>
              <td width="70" align=right><div align="center"><b>PAYOUT</b></div></td>
              <td width="11"></td>
              <td width="80" align=center><b>ODDS</b></td>
              <td width="79" align=right><div align="center"><b>PAYOUT</b></div></td>
              <td width="11"></td>
              <td width="80" align=center><b>ODDS</b></td>
              <td width="90" align=right><div align="center"><b>PAYOUT</b></div></td>
            </tr>
            <tr>
              <td align=center>1-5</td>
              <td align=right><div align="center">$2.40</div></td>
              <td></td>
              <td align=center>8-5</td>
              <td align=right><div align="center">$5.20</div></td>
              <td></td>
              <td align=center>6-1</td>
              <td align=right><div align="center">$14.00</div></td>
            </tr>
            <tr>
              <td align=center>2-5</td>
              <td align=right><div align="center">$2.80</div></td>
              <td></td>
              <td align=center>9-5</td>
              <td align=right><div align="center">$5.60</div></td>
              <td></td>
              <td align=center>7-1</td>
              <td align=right><div align="center">$16.00</div></td>
            </tr>
            <tr>
              <td align=center>1-2</td>
              <td align=right><div align="center">$3.00</div></td>
              <td></td>
              <td align=center>2-1</td>
              <td align=right><div align="center">$6.00</div></td>
              <td></td>
              <td align=center>8-1</td>
              <td align=right><div align="center">$18.00</div></td>
            </tr>
            <tr>
              <td align=center>3-5</td>
              <td align=right><div align="center">$3.20</div></td>
              <td></td>
              <td align=center>5-2</td>
              <td align=right><div align="center">$7.00</div></td>
              <td></td>
              <td align=center>9-1</td>
              <td align=right><div align="center">$20.00</div></td>
            </tr>
            <tr>
              <td align=center>4-5</td>
              <td align=right><div align="center">$3.60</div></td>
              <td></td>
              <td align=center>3-1</td>
              <td align=right><div align="center">$8.00</div></td>
              <td></td>
              <td align=center>10-1</td>
              <td align=right><div align="center">$22.00</div></td>
            </tr>
            <tr>
              <td align=center>1-1</td>
              <td align=right><div align="center">$4.00</div></td>
              <td></td>
              <td align=center>7-2</td>
              <td align=right><div align="center">$9.00</div></td>
              <td></td>
              <td align=center>15-1</td>
              <td align=right><div align="center">$32.00</div></td>
            </tr>
            <tr>
              <td align=center>6-5</td>
              <td align=right><div align="center">$4.40</div></td>
              <td></td>
              <td align=center>4-1</td>
              <td align=right><div align="center">$10.00</div></td>
              <td></td>
              <td align=center>20-1</td>
              <td align=right><div align="center">$42.00</div></td>
            </tr>
            <tr>
              <td align=center>7-5</td>
              <td align=right><div align="center">$4.80</div></td>
              <td></td>
              <td align=center>9-2</td>
              <td align=right><div align="center">$11.00</div></td>
              <td></td>
              <td align=center>30-1</td>
              <td align=right><div align="center">$62.00</div></td>
            </tr>
            <tr>
              <td align=center>3-2</td>
              <td align=right><div align="center">$5.00</div></td>
              <td></td>
              <td align=center>5-1</td>
              <td align=right><div align="center">$12.00</div></td>
              <td></td>
              <td align=center>50-1</td>
              <td align=right><div align="center">$102.00</div></td>
            </tr>
          </table>
        </div>
        <p><br />
        </p>
        {*
        <p>View Live Odds for Today's <a href="/horse-racing-schedule">Horse Racing Schedule</a></p>
        *}
        <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>
        
        <!-- ------------------------ content ends -------------------------- --> 
      </div>
      <!-- end/ content --> 
    </div>
    <!-- end/ #left-col -->
    
    {php}
      $cdate = date("Y-m-d H:i:s");
      $traversStakesDateStart = "2019-08-29 00:00:00";
      $traversStakesDateEnd = "2019-08-31 18:00:00";
    {/php}

    <div id="right-col" class="col-md-3">

        {literal}
            <!-- Odds [async] -->
            <script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'https://servedbyadbutler.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
            <script type="text/javascript">
                var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
                var abkw = window.abkw || '';
                var plc378181 = window.plc378181 || 0;
                document.write('<'+'div id="placement_378181_'+plc378181+'"></'+'div>');
                AdButler.ads.push({handler: function(opt){ AdButler.register(169350, 378181, [280,404], 'placement_378181_'+opt.place, opt); }, opt: { place: plc378181++, keywords: abkw, domain: 'servedbyadbutler.com', click:'CLICK_MACRO_PLACEHOLDER' }});
            </script>
        {/literal}


      {php}if ($cdate > $traversStakesDateStart && $cdate < $traversStakesDateEnd) { {/php}
        {*<a href="https://www.betusracing.ag/signup?ref=banner&utm_source=USR-Odds-Banner&utm_medium=CTA-GET-IT-NOW&utm_campaign=WoodWard-Stakes">
          <img src="banners/usr_wood_odds_.jpg">
        </a>  *}
      {php} } else { {/php}
        {*include file='inc/rightcol-calltoaction.tpl'*}
      {php} } {/php}      
   </div>
    <!-- end: #right-col --> 
    
  </div>
  <!-- end/row --> 
</div>
<!-- /#container --> 

