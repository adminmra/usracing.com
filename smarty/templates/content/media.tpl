<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">
  
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 

<h1>US Racing - Media and Press Release Information</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

<div class="tag-box tag-box-v4">
<h2>Official US Racing Logos</h2>

<p>  US Racing logos are registered trademarks and may be freely used by media, affiliates, partners, horse racing industry participants and fans.</p>
</div>



<div class="logos dark">
<a target="_blank" href="/banners/usracing-vertical-darkbg.png" ><img class="pull-left"  src="/banners/usracing-vertical-darkbg.png" alt="US Racing | Online Horse Betting" /></a>
<a target="_blank" href="/banners/usracing-vertical-darkbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-vertical-darkbg-slogan.png" alt="US Racing | Online Horse Betting" /></a>
<a target="_blank" href="/banners/usracing-horizontal-darkbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-horizontal-darkbg-slogan.png" alt="US Racing | Online Horse Betting" /></a>
<a target="_blank" href="/banners/usracing-horizontal-darkbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-horizontal-darkbg-slogan.png" alt="US Racing | Online Horse Betting" /></a>
</div>

<div class="logos">
<a target="_blank" href="/banners/usracing-vertical-lightbg.png" ><img class="pull-left"  src="/banners/usracing-vertical-lightbg.png" alt="US Racing | Online Horse Betting" /></a>
<a target="_blank" href="/banners/usracing-vertical-lightbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-vertical-lightbg-slogan.png" alt="US Racing | Online Horse Betting"/></a>
<a target="_blank" href="/banners/usracing-horizontal-lightbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-horizontal-lightbg-slogan.png" alt="US Racing | Online Horse Betting"/></a>
<a target="_blank" href="/banners/usracing-horizontal-lightbg-slogan.png" ><img class="pull-left"  src="/banners/usracing-horizontal-lightbg-slogan.png" alt="US Racing | Online Horse Betting"/></a>
</div>

<div class="logos">
<a target="_blank" href="/banners/usracing-vertical-lightbg-dark.png" ><img class="pull-left"  src="/banners/usracing-vertical-lightbg-dark.png" alt="US Racing | Online Horse Betting" /></a>
<a target="_blank" href="/banners/usracing-vertical-lightbg-slogan-dark.png" ><img class="pull-left"  src="/banners/usracing-vertical-lightbg-slogan-dark.png" alt="US Racing | Online Horse Betting"/></a>
<a target="_blank" href="/banners/usracing-horizontal-lightbg-slogan-dark.png" ><img class="pull-left"  src="/banners/usracing-horizontal-lightbg-slogan-dark.png" alt="US Racing | Online Horse Betting"/></a>
<a target="_blank" href="/banners/usracing-horizontal-lightbg-slogan-dark.png" ><img class="pull-left"  src="/banners/usracing-horizontal-lightbg-slogan-dark.png" alt="US Racing | Online Horse Betting"/></a>
</div>

<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
