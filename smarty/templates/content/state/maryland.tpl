{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/laurel-park">Laurel Park</a><br>
Located between Washington D.C. and Baltimore, Laurel Park race track has hosted thoroughbred horse racing since 1911. It is one of the regular hosts of the Maryland Million, as well as the Frank J. De Francis Memorial Dash, Barbara Fritchie Handicap and General George Handicap.</p>          
<p class="list-elem"><a href="/pimlico">Pimlico</a><br>
Pimlico Race Course is located in Baltimore, Maryland and offers Thoroughbred horse racing from spring to fall each year. It is home of the Preakness Stakes, the second leg of the Triple Crown, after the Kentucky Derby. The Preakness is held on the third Sunday of May every year.</p>          
<p class="list-elem"><a href="/rosecroft-raceway">Rosecroft Raceway</a><br>
Rosecroft Raceway is located in Washington, Maryland.  It is a harness racing track in Fort Washington, Maryland that first opened in 1949.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}