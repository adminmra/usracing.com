{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
                <p>Horse racing is the largest spectator sport in New Mexico attracting more than 2 million out-of-state visitors every year.</p>
 <div class="list"><p class="list-elem"><a href="/ruidoso-downs">Ruidoso Down</a><br>
Situated in southern New Mexico, Ruidoso Downs is a growing city with all the ambiance of a small town. The Ruidoso Downs Race Track, Billy the Kid Casino and the Hubbard Museum of the American West are located in the city. In 1947, the first pari-mutuel races were held at what was then called Hollywood Park. The name of the track was changed to Ruidoso Park in 1953.</p>          
<p class="list-elem"><a href="/sunland-park">Sunland Park</a><br>
Sunland Park Racetrack opened in 1959. Its located in Sunland Park, New Mexico, a suburb of El Paso, Texas. The race track features both thoroughbreds and quarter horses. The signature event at Sunland of the same name is held in March each year and is a prep race for the Kentucky Derby.</p>          
<p class="list-elem"><a href="/zia-park">Zia Park</a><br>
Zia Park is a thoroughbred horse racing track owned by Penn National Gaming, Inc.and located in the city of Hobbs, NM. It offers live horse racing from September to December.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}