{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
        <p>Horse racing is a very popular sport in California. The California Horse Racing Board has authority over the regulation of horse racing and parimutuel betting at licensed California race tracks. was established in 1933 as an independent agency of the State of California.</p>
        
             <div class="list">
                        <p class="list-elem"><a href="/cal-expo">Cal Expo</a>
Cal Expo is home to the only live Harness Racing track on the West Coast. For   a short number of action-packed days every fall, live harness racing thunders   through the Cal Expo racetrack with 14 days of those racing dates during the   California State Fair. It opens the 1st Saturday in October for Saturdays only   in October then moves to 2 day per week schedule (Friday and Saturday) in   November, December.</p>
<p class="list-elem"><a href="/del-mar">Del Mar</a>
Del Mar Spanish-style architecture and laid-back attitude make it   the annual setting for one of America's foremost salutes to the sport, where   horses, trainers, jockeys and owners come together for racing beside the sea.   The track sits a couple of furlongs from the shimmering blue Pacific (where the turf meets the surf), about 20 miles north of San Diego and 100 miles   south of Los Angeles.</p>
 <p class="list-elem"><a href="/fairplex-park">Fairplex Park</a>
Located in Pomona, California, Fairplex Park featured 13 days of live thoroughbred racing every September during the L.A. County Fair in addition to several stakes events running each year. In 2013 Barretts Equine Ltd. and Fairplex Park Racing have merged to form Barretts Sales and Racing Barrett's Park.</p>
 <p class="list-elem"><a href="/ferndale">Ferndale Fairgrounds</a>
Races are held at Ferndale Fairgrounds during the Humboldt County Fair Monday,   Tuesday, Thursday in August every year. Eight action-packed days racing mules,   thoroughbred, quarterhorse, appaloosa and Arabian horses.</p>
<p class="list-elem"><a href="/fresno">Fresno</a>
Fresno horse racing is held once a year in the Brian I. Tatarian Grandstand at The Big Fair. The meet happens in October annually. the Big  Fair was   established in 1882 and in 1941, offered its first pari-mutuel horse-racing meeting.</p>
<p class="list-elem"><a href="/golden-gate-fields">Golden Gate Fields</a>
Located along the San Francisco Bay in Berkeley, Golden Gate Fields is Northern California's premier horse racing destination. It has been offering thoroughbred horse racing to the Bay Area since 1941 and with   the closing of the Bay Meadows racetrack in 2008, it became the only major   racetrack in Northern California.</p>
<p class="list-elem"><a href="/los-alamitos">Los Alamitos</a>
Los Alamitos Race Course has been offering Quarter Horse racing since 1947. The   track hosted its first pari-mutual meet in 1951. What started out as informal   match races on the Vessels Ranch in the late '40s, has grown into year-round   Quarter Horse racing in California. Quarterhorse racing makes up to 60% of the  horse-racing revenue at Los Alamitos.</p>
 <p class="list-elem"><a href="/pleasanton">Pleasanton</a>
Built in 1858, Pleasanton is located at the Alameda County Fairgrounds in   California. Pleasanton Fairgrounds Racetrack is the oldest horse racing track of   its kind in the United States. The one-mile horse track features thoroughbred and quarter horse racing between June and July.</p>
 <p class="list-elem"><a href="/sacramento">Sacramento</a>
 If you are in Sacramento during the summer, you can come   watch some great horse racing. Horse Racing is offered every July, during the   California State Fair.</p>
<p class="list-elem"><a href="/santa-anita-park">Santa Anita</a>
Santa Anita Park conducted its inaugural card on Christmas Day, 1934. . . on February   4, 1938, a $2,000 claimer named Playmay won the first race on the card and   returned $673.40 for a two-dollar win mutuel.</p>
 <p class="list-elem"><a href="/santa-rosa">Santa Rosa</a>
Santa Rosa is part of the California State Fair horse racing circuit. The Sonoma County Fair happens every year in July / August.</p>
 <p class="list-elem"><a href="/stockton">Stockton</a>
Stockton is part of the California Racing Fair Circuit. The San Joaquin County Fair happens every year in September.</p>
  </div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}