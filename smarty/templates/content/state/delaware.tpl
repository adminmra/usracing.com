{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
        <p>The State of Delaware offers thoroughbred racing at Delaware Park and harness racing at Harrington Raceway and Dover Downs.</p>
 <div class="list">
<p class="list-elem"><a href="/delaware-park">Delaware Park</a><br>
Delaware Park Racetrack is the only thoroughbred horse racing track in the state of Delaware. Races at Delaware Park are held from April to November, and include Grade II events such as Delaware Oaks.</p> 
<p class="list-elem"><a href="/dover-downs">Dover Downs</a><br>
Dover Downs Racetrack has hosted harness racing events since 1969. It features premier live harness racing from late-October through mid-April. Stakes races regularly held at Dover Downs include the Matron Series, the Classic Series and the Delaware Standard bred Breeders Fund.</p> 
<p class="list-elem"><a href="/harrington">Harrington Raceway</a><br>
Harrington Raceway is one of the nation's oldest racetracks and has run a race meet every year since 1946. Live racing at Harrington is from August to October with races on Sunday through Thursday and a 5:30 p.m. post time.</p>         
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}