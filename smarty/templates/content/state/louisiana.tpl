{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/delta-downs">Delta Downs</a><br>
Delta Downs is an American thoroughbred and quarter horse racetrack in   Calcasieu Parish, near Vinton, Louisiana in the southwest portion of the state.   The track opened in September, 1973. The track usually holds races from November to mid-July, with the   Thoroughbred meet beginning in November and the Quarter Horse meet commencing in April.</p>          
<p class="list-elem"><a href="/evangeline-downs">Evangeline Downs</a><br>
Evangeline Downs race track is one of the first horse racing tracks in   Louisiana. The track features thoroughbred and quarter horse racing in a season   that typically spans from April to October. Evangeline Downs is known for having more horses per race than any other   track in the country. The more horses in a race, the better the odds of winning   and the larger the payouts.</p>          
<p class="list-elem"><a href="/fair-grounds-race-course">Fair Grounds Race Course</a><br>
Fair Grounds Race Course is located in historic Mid-City and has   been a New Orleans staple since 1842. It is often referred to as New Orleans   Fair Grounds and is ranked #12, behind Evangeline   Downs in Opelousas, Louisiana, which was ranked #6. The racetrack is   operated by Churchill Downs Louisiana Horseracing Company, LLC. Fair Grounds is the home of the Louisiana Derby, a Grade II stakes prep for the Kentucky Derby.</p>          
<p class="list-elem"><a href="/louisiana-downs">Louisiana Downs</a><br>
Louisiana Downs horse racing track and racino   opened in 1974 with 15,000 fans in attendance for the first day of its inaugural   meet. Its located near Shreveport in Bossier City, northwest Louisiana. It was purchased by Caesars Entertainment in December, 2002. Louisiana Downs is considered to be a showcase for horses from Louisiana, Arkansas, and Texas. Thoroughbred meet takes place from early May through   October. A quarter horse meet is held from January through March.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}