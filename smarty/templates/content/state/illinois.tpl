{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
        <p>There are 37 authorized OTB parlors in Illinois and five horse racing tracks.</p>
      <div class="list">
<p class="list-elem"><a href="/arlington-park">Arlington Park</a><br>
Arlington Park is located in the Chicago suburb of Arlington Heights in Illinois. The track is a major hub for thoroughbred horse racing in the United States and is described as the most beautiful track in America.</p>         
<p class="list-elem"><a href="/balmoral-park">Balmoral Park</a><br>
Balmoral Park is a horse racing track located just south of Crete, Illinois, United States. The track hosts harness racing for most of the year except for a short time in January every year. </p>         
<p class="list-elem"><a href="/fairmount-park">Fairmount Park</a><br>
Fairmount Park opened in 1925 and is part of the St. Louis metropolitan area. The track hosts Thoroughbred racing. It is one of five horse racing venues in Illinois and the only one outside the metropolitan Chicago area.</p>         
<p class="list-elem"><a href="/hawthorne-race-course">Hawthorne Race Course</a><br>
Hawthorne Race Course is the oldest continually-run, family-owned, racetrack in North America and has celebrated live thoroughbred racing in Chicago for over 120 years.</p>         
<p class="list-elem"><a href="/maywood-park">Maywood Park</a><br>
Maywood Park is located in Melrose Park, just ten miles west of downtown Chicago. The Maywood race track was opened in 1946 on the original site of the Cook County Fairgrounds. Maywood Park is the only racetrack in the Chicago area devoted exclusively to standardbred horse racing.</p>         
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}