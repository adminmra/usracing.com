{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/harrahs-philadelphia">Harrah's Philadelphia</a><br>
Harrah's Philadelphia held its first harness racing   season opened on September 10, 2006. It's located in Chester,  Pennsylvania, just south of the Philadelphia International Airport and   situated in the tri-state area of Pennsylvania, New Jersey and Delaware. It is a very short ride from nearby Maryland as well. The track is the  first-of-its-kind in North   America, boasting a miracle turn that actually extends over the   Delaware River due to a specially constructed bridge.</p>          
<p class="list-elem"><a href="/parx-racing">Parx Racing</a><br>
Parx Racing is one of the largest gaming complexes in   Pennsylvania. Located approximately 30 minutes north of  downtown  Philadelphia and 40 minutes from the Philadelphia International Airport. Parx Racing  is the home of Pennsylvania's two premier Thoroughbred   races, the US $1 million Grade II Pennsylvania Derby and the US $500,000   Grade I Fitz Eugene Dixon Cotillion Handicap.</p>          
<p class="list-elem"><a href="/penn-national">Penn National</a><br>
Penn National Race Course is located in Grantville, Pennsylvania, 17 miles east of Harrisburg, PA. The track opened its doors for racing on August 30 1972. Penn National is owned and operated by Penn National Gaming and is one of the few horse racing facilities in the United States that offers year   round live horse racing. In 2013 the inaugural Penn Mile was run on turf.</p>          
<p class="list-elem"><a href="/pocono-downs">Pocono Downs</a><br>
Pocono Downs is located on 400 acres on the hillside of Plains, Pennsylvania. It is within   easy access of New York, Philadelphia, New Jersey and Delaware. Pocono Downs is a Harness racing   track and is a member of the U.S. Trotting Association.</p>          
<p class="list-elem"><a href="/presque-isle-downs">Presque Isle Downs</a><br>
Presque Isle Downs is located just south of Erie and within 100 miles of   Buffalo, New York and Cleveland, Ohio. It opened on February 28, 2007   and is the first synthetic horse racetrack that is longer than 1 mile    in the Northeast and the first racetrack paved with Tapeta (synthetic, all-weather turf for horses) in the United States.</p>          
<p class="list-elem"><a href="/the-meadows-racetrack">The Meadows</a><br>
The Meadows Racetrack was the first parimutuel horse racing track in   Western Pennsylvania is and the oldest track overall in Pennsylvania. It   opened in November of 1962 and has a racing schedule that goes on year-round. The Meadows is  known for introducing major technological advancements.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}