{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/northfield-park">Northfield Park</a><br>
Northfield Park was constructed in 1934 by Al Capone for midget car racing. At the time, it was called Sportsman Park which was demolished in 1956 to make way for what would eventually become Northfield Park racetrack, the only remaining commercial harness track in Northeast Ohio. The track's nickname is The Home of the Flying Turns, and its motto, Every nineteen minutes the place goes crazy.</p>          
<p class="list-elem"><a href="/raceway-park">Raceway Park</a><br>
Raceway Park opened in 1959, it originally hosted car racing and thoroughbred racing. The first harness race was run in 1962. In 2005, the track was purchased by Penn National Gaming, Inc. Raceway Park Racetrack offers weekend live harness racing and hosts multiple legs of the Ohio Sires Stakes and the Champion of Champions event, which showcases the best of the Northwest Ohio Fair Circuits.</p>          
<p class="list-elem"><a href="/scioto-downs">Scioto Downs</a><br>
Scioto Downs has offered harness racing since 1959. The track is located a half-mile south of Columbus, Ohio. It offers the State Fair Stakes in Auguat of every year.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}