{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
                <div class="list">
                        <p class="list-elem"><a href="/charles-town">Charles Town</a><br>
Charles Town Race track and casino is located just outside the   eastern city limits of Charles Town, West Virginia. The track opened its   doors on December 3, 1933. The facility is home of the West Virginia   Breeders' Classic and is one of the busiest thoroughbred tracks in the   country with over 240 racing dates scheduled. It&rsquo;s open all year long   and runs a 5 days a week schedule (four days per week in July, August, and some major holidays).</p>          
                        <p class="list-elem"><a href="/mountaineer-park">Mountaineer Park</a><br>
Mountaineer is a thoroughbred racetrack   located in Chester, West Virginia. The facility hosts over 200 race   dates a year and runs 5 nights per week. The only day race offered is   the yearly West Virginia Derby, a Grade II race held the first Saturday in August every year. Prior to   1998, the West Virginia Derby was timed to one-fifth of a second. Since then, the race has been timed to one-hundredth of a second.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}