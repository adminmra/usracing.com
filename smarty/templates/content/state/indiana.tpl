{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/hoosier-park">Hoosier Park</a><br>
Hoosier Park is located in Anderson, Indiana and was   the first racetrack outside Kentucky owned by Churchill Downs since   1939. In 2001, Indiana Downs became the second horse racing track in the   state. It is rated #4 in US Thoroughbred horse tracks.</p>         
<p class="list-elem"><a href="/indiana-downs">Indiana Downs</a><br>
Indiana Downs Race Track is located in Shelbyville,   Indiana, which is roughly 15 minutes from Indianapolis. The Indiana   Downs Race Track is a $35 million dollar state-of-the-art facility that   hosts both Thoroughbred and Harness races.</p>         
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}