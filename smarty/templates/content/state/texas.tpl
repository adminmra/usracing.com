{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
                <p>Texas is home to different yearly races of national interest at local tracks including:</p>
                <div class="list">
                        <p class="list-elem"><a href="/lone-star-park">Lone Star Park</a> located in Grand Prairie,
                                Texas,
                                offers both Thoroughbred racing and Quarter Horse racing.</p>
                        <p class="list-elem"><a href="/retama-park">Retama Park</a> is open for simulcasting every day
                                except
                                Christmas Day. It is located in Selma, Texas which is part of the San Antonio
                                Metropolitan Area.</p>
                        <p class="list-elem"><a href="/sam-houston-race-park">Sam Houston Race Park</a> is located just
                                15
                                minutes from downtown Houston offering Thoroughbred and Quarter horse racing
                                and high-dollar stakes races every weekend.</p>
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}