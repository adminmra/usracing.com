{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/fair-meadows">Fair Meadows</a><br>
Fair Meadows Racetrack is a large race track and betting center located   in Tulsa, Oklahoma. The track features horse races Thursday through   Sunday from early June to late July at its facility in Expo Square. Fair   Meadows operates under the direction, control and authority of the   Tulsa County Public Facilities Authority.</p>          
<p class="list-elem"><a href="/remington-park">Remington Park</a><br>
Remington Park is located in Oklahoma City, Oklahoma. Built in 1988, it offers betting on Quarter Horse and Thoroughbred horse racing. The Horseplayers Association of North America rated Remington Park #3 in 2009.</p>          
<p class="list-elem"><a href="/will-rogers-downs">Will Rogers Downs</a><br>
Will Rogers Downs is located 20 minutes from downtown Tulsa, Oklahoma and owned and operated by the Cherokee Nation. The track offers Quarter, Paint and Appaloosa   horses from Oklahoma and around the country. Quarter Horse Racing is   hosted from September through November and Thoroughbred racing from   March to May with simulcast racing daily.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}