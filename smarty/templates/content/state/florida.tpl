{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<p>The sunny state of Florida is host to many horse tracks that are popular during the winter months.</p>
<p class="list-elem"><a href="/calder-race-course">Calder Race Course</a><br>This is a thoroughbred horse racing track in Miami Gardens, Florida. It's located on the Miami-Dade/Broward County line, approximately 15 miles from the centers of Miami and Ft. Lauderdale.</p>
<p class="list-elem"><a href="/gulfstream-park">Gulfstream Park</a><br>
This is one of the most important venues for horse racing in the United States. It opened on February 1, 1939. It is the premier East Coast track that operates during the winter months.</p>
<p class="list-elem"><a href="/hialeah-park">Hialeah Park</a><br>This race Track originally opened in 1921. It offers Quarter Horse races from November to January.</p>
<p class="list-elem"><a href="/pompano-park">Pompano Park</a><br> This track is known as &quot;The Winter Home of Harness Racing&quot;and it has been the home of world-class standardbred racing since 1964.</p>
<p class="list-elem"><a href="/tampa-bay-downs">Tampa Bay Downs</a><br>  
                    It first opened its doors in 1926 and is the only thoroughbred race track on the West Coast of Florida. It hosts two Kentucky Derby prep races, The Sam F. Davis Stakes and The Tampa Bay Derby every year. </p>
                  </div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}