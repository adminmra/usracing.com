{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
                <div class="list">
                        <p class="list-elem"><a href="/emerald-downs">Emerald Downs</a><br>
Emerald Downs is a thoroughbred racetrack in Auburn, Washington. Emerald Downs made its official debut as a replacement of Long Acres Racetrack, which closed in September 1992. The 167-acre facility, located midway between Seattle and Tacoma, has   established as the premier thoroughbred racing venue in the Pacific   Northwest. The facility hosts several large ungraded stakes and many   overnight handicaps and stakes. There are an average of 8 races on week   nights, and 10 races per race weekend.</p>          
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}