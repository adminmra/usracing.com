{include file="/home/ah/allhorse/public_html/usracing/states/top-of-page-blocks.tpl"}
{************PAGE SPECIFIC COPY*****************************************}
<p><div class="list-text">
<div class="list">
<p class="list-elem"><a href="/churchill-downs">Churchill Downs</a><br>
Churchill Downs located on Central Avenue in south Louisville, Kentucky. It is a racetrack for thoroughbred racing, the most famous race being the Kentucky Derby held annually. It officially opened in 1875, and held the first Kentucky Derby and the first Kentucky Oaks in that same year along with the Clark Handicap. in 2009, Churchill Downs was ranked number 5 out of 65 thoroughbred tracks by the Horseplayers Association of North America</p>          
<p class="list-elem"><a href="/ellis-park">Ellis Park</a><br>
Built in Henderson, Kentucky, Ellis Park held its first thoroughbred meet in 1922. The track has undergone many changes since the 1920's because of ownership changes, barn fires, floods, and a tornado that almost destroyed the facility in 2005</p>          
<p class="list-elem"><a href="/keeneland">Keeneland</a><br>
Keeneland is a top-notch  track located in Lexington, Kentucky which is also known for its reference library on the sport. The racetrack hosted its first day of live racing on October 15, 1936. The track has received many great honors over the years, including being named by the Horseplayers Association of North America (HANA) as the continent's top racetrack, for the 3rd consecutive year, in 2011.</p>        
<p class="list-elem"><a href="/kentucky-downs">Kentucky Downs</a><br>
Kentucky Downs Opened in 1990, Kentucky Downs racetrack is located in Franklin, Kentucky. The track is a European-style course which means the surface is all turf (grass) not dirt, and it is not oval in shape. In 2009, Kentucky Downs was ranked #2 out of 65 thoroughbred racetracks in North America by the Horseplayers Association of North America.</p>        
<p class="list-elem"><a href="/turfway-park">Turfway Park</a><br>
Turfway Park is located just outside the city limits to the north of Florence, Kentucky. Turfway offers live Thoroughbred racing during the fall, winter, and early spring. It is open all year for simulcast racing from top tracks around the country. It hosts the Turfway Park Fall Championship (G3) which was named a Breeders' Cup Challenge "Win and You're In" race, with its winner guaranteed a spot in the Breeders' Cup Marathon.</p>     
</div> </div></p>
{************END PAGE SPECIFIC COPY*****************************************}
{include file="/home/ah/allhorse/public_html/usracing/states/end-of-page-blocks.tpl"}