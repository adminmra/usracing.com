{literal}
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "legalName" : "US Racing",
  "url" : "https://www.usracing.com/",
  "contactPoint" : [{
    "@type" : "ContactPoint",
    "telephone" : "+1-844-238-4677",
    "contactType" : "customer service"
  }],
  "logo" : "https://www.usracing.com/img/usracing-betonhorses.gif",
  "sameAs" : [ "https://www.facebook.com/usracingtoday",
    "https://twitter.com/usracingtoday",
    "https://plus.google.com/+Usracingcom",
    "https://www.youtube.com/user/betusracing",
    "https://www.instagram.com/usracingtoday/"]
}
</script>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebPage",
        "name": "US Racing: Online Horse Betting",
        "description": "US Racing provides reviews of racebooks and AWDS so you can choose where to bet on horses at with your mobile device. Bet on horses with 300+ racetracks, get bonuses and daily rebates.  Mobile phone betting, bet where and when you want. Online Horse Betting",
        "publisher": {
            "@type": "Organization",
            "name": "US Racing"
        }
    }
    </script>
<!-- <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "headline": "US Racing - Online Horse Betting at its Best",
  "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
"keywords": ["usracing","horses","race","entertainment","tv","bet"],
  "datePublished": "2018-12-28T13:39:38Z",
  "articleSection": "entertainment",
  "creator": "Derek Simon",
  "author": "Derek Simon",
  "publisher": "US Racing",
  "articleBody": "US Racing is an online horse racing service that offers reviews of thoroughbred, harness and greyhound wagering content from over 200 of the world's premier racetracks. US Racing features reviews of the best tracks in the United States, including Churchill Downs, Santa Anita, Keeneland, Gulf Stream, Belmont, Pimlico, Saratoga and Del Mar.",
  "mainEntityOfPage": "True"
}
</script> -->
{/literal}
<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



{* <h1>US Racing - Online Horse Betting at its Best</h1> *}
<h1>US Racing</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->


{*<h2>Online Horse Betting</h2>*}
<img alt="Bet on horses" src="/img/usracing-betonhorses.gif" class="pull-left overflow-hidden img-responsive" width="200" style="margin:0 20px 10px 0;" />

<p>US Racing is an award winning, online horse racing service that offers reviews of thoroughbred, harness and greyhound wagering content from over {$RACETRACKS} of the world's premier racetracks. US Racing features reviews of the best tracks in the United States, including Churchill Downs, Santa Anita, Keeneland, Gulf Stream, Belmont, Pimlico, Saratoga and Del Mar.</p>
<p>Internationally, US Racing also provides information on racing in Hong Kong, Australia, Dubai, Great Britain, France, Ireland and Japan.  Membership is free and there are no fees or charges to read the reviews of the best racebooks and ADWs.</p>
<p>Online horse betting is available from several companies, public and private, located all around the world via desktop computer, laptop, iPad, iPhone or Android powered devices.</p>
<iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/P3pLgjTPXCQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<br><p></p>
<p>US Racing features <a href="https://www.usracing.com/stan-bergstein-media-eclipse-award">award winning </a> race analysis, racebook reviews, interviews, free handicapping tips and stories on trainers, jockeys, owners, breeders, horses and new technology and developments in the horse racing industry. No wagering of any form takes place at US Racing.</p>
<p>The goal of US Racing to entertain, educate and inform people about the sport of thoroughbred horse racing.</p>
<p>US Racing is proud that its writers have been awarded the top honors in the horse racing industry, including a <a href="https://www.usracing.com/stan-bergstein-media-eclipse-award">Media Eclipse Award </a>(honorable mention) and the <a href="https://www.usracing.com/stan-bergstein-media-eclipse-award">Stan Bergstein Writing Award</a>.</p>
<p>US Racing is privately held with offices in Hong Kong and the Isle of Man.</p>

<p>How does US Racing review racebooks and ADWs? Depending on what kind of wagering experience you are after, US Racing can help inform your decision by providing in depth and insider information on the top ADWs and racebooks around the world. Compare the <a href="/best-horse-racing-site">world's best racebooks and ADWs</a>.</p>



</ul>

<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='/home/ah/allhorse/public_html/usracing/calltoaction_about.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
