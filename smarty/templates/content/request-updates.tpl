<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>US Racing Grand Opening on Tuesday, April 28, 2015!</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->

<p><b>US Racing is proud of having over 20,000 members.  We are excited to debut our new software platform on April 28th.  With more betting options and easier deposits and withdraws, we hope our current members enjoy the change.  For our new members, please let us know what you think.  We genuinely care and listen to our members.  And we read ever email and consider every suggestion with 1 goal in mind: HOW CAN WE DO BETTER?  Everyday we ask ourselves this question.  And while it may seem a little old fashion, in the end, we believe it really matters. A lot. </b> </p>
<p>On April 28th, you will need to reregister your account.  Let us tell you why it's  great news for you!</p>
<p>US Racing Members are eligible for the following special cash bonuses when they reopen an account:</p>
<p>


 <ol>
  <li>25% CASH BONUS for their first deposit</li>
  <li>8% Daily Rebates on Horse Wagers</li>
  <li>Free Bets for the Kentucky Derby, Preakness Stakes and Belmont Stakes</li> 
</ol> 
 

</p><p><b>Please enter your contact details below for chances to win free bets, official US Racing gear and other valuable gifts.</b></p>
  
<IFRAME SRC="http://www.allhorse.com/capture/usrtemp.php" WIDTH="100%" HEIGHT="200" frameborder="0">

</IFRAME>





<p><b>You want more tracks?  We got'em here! </b></p>
<p>The new US Racing software platform will provide members will over 150 race tracks from around the world including Churchill Downs, Pimlico, Belmont, Santa Anita and Hong Kong.  That is almost 5 times more than our current list of tracks.</p>

<p>US Racing will also be offering unique Match Race wagers.  You will be able to wager between 2 horses (or jockeys) as to which one will beat the other within an individual race.  </p>
<p>US Racing will also be offering a Daily Racing Special as well as a Race of the Week where members can get extra cash back on their wagers if they are winners.</p>
<p>US Racing will continue to strive to provide our members the best in entertainment and customer support.</p>
<p>We appreciate your loyalty and it means a lot to us that you choose to wager with us.</p>
<p>
In addition to worldwide betting on horses, US Racing will also be introducing greyhound betting from all the top tracks.</p>
<p>We look forward to welcoming you to our new site and we look forward to hearing your feedback.</p>
<p>If you have any questions or concerns, please do not hesitate to contact us at cs@usracing.com
</p>















<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->

{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
