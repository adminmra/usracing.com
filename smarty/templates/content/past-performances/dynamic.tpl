{php}
$GetTrack= explode('/',''. trim($_SERVER['REQUEST_URI']));
$Trackname= ucwords(str_replace('-',' ',$GetTrack[2]));
{/php}

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="home/ah/usracing.com/htdocs/assets/css">
   <div id="main" class="container">
    <div class="row">
        <div id="left-col" class="col-md-10">
            <div class="headline"><h1>Free Past Performances Demo - {php} echo $Trackname; {/php}</h1></div>
            <div id="pp-sorter" style="float: right;"> Sort By:
             <select name="pass-performances-sorter" id="pass-performances-sorter">
                 <option value="race">Race</option>
                 <option value="race_date"> Race Date</option>
                 <option value="claimamt">Claiming</option>
                 <option value="dist_disp">Distance</option>
                 <option value="todays_cls">Todays CLS</option>
                 <option value="purse">Purse</option>
             </select>
         </div>
            <div class="content">
                {include file="/home/ah/usracing.com/smarty/templates/content/parsing/parsing.tpl"}
            </div> <!-- end/ content -->
        </div> <!-- end/ #left-col -->
    </div>
</div>
{literal}

<script type="text/javascript">
             $("#pass-performances-sorter").on('change',function(e){
                  tinysort(".table-responsive",{attr:this.value});
           });
             $(".race-tables").on("click",function(e){
                    if ( $(this).parent().siblings('tbody').is(':visible') ){
                        $(this).parent().siblings('tbody').hide();
                            $(this).attr("title","Click here for more information");
                    } else {
                        $(this).parent().siblings('tbody').show();
                        $(this).attr("title","Hide Table");
                    }
                    $(this).toggleClass("past-performance-minus").toggleClass("past-performance-plus");
             });
             $(".horse_data").on("click",function (e ) {
                var race = $(this).attr('race');
                var program = $(this).attr('program');
                if ( $(".horse_" + program + "_" + race ).is(':visible') ){
                    $(".horse_" + program + "_" + race ).hide();
                    $(this).attr("title","Show More...");
                } else {
                    $(".horse_" + program + "_" + race ).show();
                    $(this).attr("title","Hide");
                }
                $(this).children("span").toggleClass("past-performance-minus-b").toggleClass("past-performance-plus-b");
             });

     
</script>
<script src='../js/footable.min.js'></script>
<script>
	jQuery(function($){
	$('.tables').footable();
});

</script>
{/literal}