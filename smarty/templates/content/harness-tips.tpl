
      
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

           
                                        
          
<div class="headline"><h1>Harness Racing Handicapping Tips</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span class="DarkBlue">&nbsp;</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="headline"><h1><span class="DarkBlue">Harness Racing Handicapping Tips &amp; Key Terms</span></h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><span class="DarkBlue"><strong>ITEMS TO CONSIDER WHEN WAGERING ON STANDARDBRED RACING</strong></span></p>
<p>Here are some simple tactics which may be helpful to a new bettor:<br /><strong><br />1. The Driver</strong><br />Drivers, much as other athletes, possess varying amounts of ability, and even the good ones have slumps. Fortunately, race tracks keep records of how a driver performs. Next to his or her name, you'll find statistics about their performance at the given track. For example, you might see something like this:</p>
<p>JOE DOKES red-gr-blu (112-23-15-11-.313)</p>
<p>Simply stated, driver Dokes (who wears red, green and blue colors) has driven in 112 races, winning 23 of them, finishing second 15 times, and finishing third 11 times. His "Universal Driver Rating" -- a number calculated like a batting average -- is .313. Any number over .300 is considered excellent. At most racing locations, the program will include a listing of the track's leading drivers. The drivers may be listed by the Universal Driver Rating (UDR), or by the total number of wins. The most useful piece of information in the driver statistics is a driver's winning percentage. The drivers who can "get 'em home first" at the highest rate should merit extra handicapping points. A horse that receives a switch from an unranked driver to a top-rate pilot should get further handicapping points. Often, though, the top drivers are bet heavily by the public, resulting in odds lower than what the horse's chances might truly warrant. Also, drivers not listed among the leaders may still qualify as a possible bet if the horse passes other handicapping tests, AND the unranked driver has driven the horse successfully in the past. Top trainers, sometimes listed in your program, usually have their horses in peak condition and ready to win. As with drivers, isolate the trainers who have a high win percentage. A horse "claimed" in his last start (the "c" or "z" after the claiming price shows a claim) and moving into a top trainer's care may show dramatic improvement for his new stable.</p>
<p><strong>2. Consistency</strong><br />Consistency is perhaps the most outstanding characteristic of the standardbred horse. Good horses are able to perform well, week after week. Most racing programs will show records of these performances. In the upper left-hand portion of each horse's past performances, you'll see the fastest winning mile of the year listed, the number of starts, wins, seconds, thirds, and money won for the present and past years.</p>
<p><strong>3. Class</strong>&nbsp;<br />Harness horses tend to race against other horses of comparable ability, and it's the job of the race secretary to design races that will bring together well-matched and competitive fields. Race types fall into various "classes," such as: conditioned races (grouping done by the horses' earnings and other factors); claiming races (grouping done by the estimated value of the horses); or "feature" events (Open, Invitation, Stakes, etc.). The best way to judge whether a horse is moving "up" or "down" in class is to compare the purse of the race in question with the present race's purse. Within a given track's class structure, the purse is often a useful barometer; however, comparing purses from one track to another is a less reliable guide. Horses dropping in class are generally a good bet -- if they meet other handicapping criteria. An edge in class is worth one or two handicapping points, as horses dropping in class are meeting softer competition tonight. Horses moving up, however, may still rate consideration if they have been winning impressively or posting fast times while facing horses in a lower class. Younger horses who have made only a few starts also may move in class readily, as their true class might not yet be established.</p>
<p><strong>4. Post Position</strong><br />Generally speaking, the inside post positions (numbers one through four) are an advantage, especially on half-mile tracks. Horses who do not have good post position risk the possibility of being "parked out" (marked by the "0" symbol in the program) and losing considerable ground while racing on the turns. The inside post position bias is most pronounced on half-mile tracks, where there is a short distance between the start and first turn. The bias still exists on five-eighths-mile and mile tracks, but to a lesser extent. Most programs list the number of winners coming from each position, making the job of evaluating post positions easy. It's also important to check the racing style of a horse and figure out his likely racing position throughout the mile. If there are many horses whose past performance show early speed in a race, they may wear themselves out fighting among themselves, and a fast-finishing horse may catch them before the wire Similarly, a good come-from-behind horse from a bad post position may find other fast finishers in a better position than he is when he starts to make his move. That horse may not be able to make up enough ground on his rivals to win. Finally, a horse who raced either spectacularly or poorly from a bad post position last week may have a better chance of winning from an inside starting slot tonight.</p>
<p><strong>5. Time</strong><br />An important handicapping concept to understand is that the final time posted by a horse is not as important as his individual quarter-mile times. For example, "Able Almahurst" may race in a trailing position in a fast-paced race and merely finish in average time, but record a fast victory due to the fast early pace. "Baker Hanover," meanwhile, may trail far behind a slow pace and finish very rapidly, but not gain much ground during a fast final quarter. Yet ... "Baker" may well be sharper than "Able!" A horse that "does work" (races on the lead or outside in a challenging position, or close strongly) in a fast quarter should get extra points in handicapping. Although fractional times are more important than final time, it is a fact that some horses are just faster than others. It is important to check how fast horses have been clocked in recent races, although the swiftest ones, and ones who are merely "sucked along" (stayed behind other horses in the pack), are often over bet by the public. Times posted at other tracks may be adjusted, when handicapping, by checking the "Comparative Speed Ratings" in the program. Take the difference between the ratings and add or subtract the result to the time posted at the other track. It will show what the time might have been if the race had been at the track where the horse is on the present night.</p>
<p><strong>6. Form</strong><br />Like any other athlete, a harness horse's performances cross peaks and valleys, but most every race winner has shown that he's been racing at or near top "form." In the most recent races listed for each horse in the racing program, the running positions (where the horse was in a race: 1st, 6th, etc.) are the indicators of form. Horses tend to fall into two broad categories: those that race on or near the lead, and those that race farther back and come on strongly at the finish. Front-running horses displaying good form show that they can hold the lead all the way, while the latter type horses show come-from-behind rushes to either win or just miss. Changes in form can be spotted by comparing race lines week to week. Once a front-runner's past performance lines start to show he can't hold the lead all the way, he's going "off form." But when each line shows he's getting closer to going "wire-to-wire," he's coming back into form...and is worth a bet. Sometimes, though, what appears to be a downward swing in form may not be that at all. A dull-appearing performance may be the result of an "off" track (sloppy, muddy, etc.), interference, show fractional times, or simply of having raced against horses of superior class. Horses must also race frequently to keep their form, and they compete on an average of every six to ten days. Long layoffs are almost always a bad sign.</p>
<p><strong>GLOSSARY OF KEY TERMS</strong></p>
<p><strong>Age</strong> - Every horse shares a "birthday" of January first. A horse becomes one year old on the first of January after he or she is born, and turns two one year later -- regardless the actual date of his or her birth.</p>
<p><strong>Break</strong> - When a horse gallops, instead of trotting or pacing, it's on a break. The driver must get the horse out of the way of the others, must not improve their position, and must attempt to get the horse back on its proper gait. A horse is not automatically disqualified by making a break.</p>
<p><strong>Colt</strong> - A male horse, age three or under.<br /><strong><br />Filly</strong> - A female horse, age three or under.</p>
<p><strong>First-Over</strong> - A horse racing on the outside without another horse directly in front of him or her. A foreshortening of of the phrase "first overland."</p>
<p><strong>Foal</strong> - A newly-born horse. (verb) The act of giving birth.</p>
<p><strong>Gelding</strong> - A de-sexed horse of any age.</p>
<p><strong>Green Horse</strong> - A horse that has not raced, or has raced only a few times.</p>
<p><strong>Horse</strong> - A male horse, age four and up.</p>
<p><strong>Maiden</strong> - A horse (male or female) who has never won a racing purse.</p>
<p><strong>Mare</strong> - A female horse, age four and up.</p>
<p><strong>Parked-out</strong> - A horse racing on the outside with at least one horse between him and the inside rail or barrier.</p>
<p><strong>Purse</strong> - The cash prize won by the owner. The purse is usually paid to the first five finishers; 50% is paid to the winner, 25% for place, 12% for third, 8% for fourth, and 5% for fifth.</p>
<p><strong>Purse</strong> - Prize money earned for winning a race.<br />Qualifying Race - A race without a purse or betting used to determine a horse's ability and manners. Horses who have made repeated breaks in stride or have been away from the races for a long period of time must race in a qualifying race before being allowed to race in a betting race.</p>
<p><strong>Scratch</strong> - A horse who is withdrawn before the race starts.</p>
<p><strong>Sire</strong> - The male parent of a horse.<br /><strong><br />Trainer</strong> - The person responsible for keeping a horse in top racing condition. In harness racing the trainer is often the driver.</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    