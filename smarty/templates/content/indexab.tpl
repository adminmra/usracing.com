{include file="inc/home_slider_abtest.tpl"}

<div class="container">
 
 <!-- Service Blocks -->
 <div id="service" class="row margin-bottom-30">
  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h1>Online Horse Betting</h1>
     </div>
     <div class="thumbnail margin-bottom-20"><img src="img/online-horse-racing.jpg" class="img-responsive" alt="Online Horse Betting" /></div>
     <div class="content">
     US Racing is a licensed <a href="/advance-deposit-wagering">Advance Deposit Wagering</a> provider and the leader in online horse racing betting. US Racing is the <a href="/best-horse-racing-site" title="best horse racing site">best horse racing site</a> for the person who is looking for 100% legal and licensed horse wagering. <a href="/bet-on-horses">Bet on horses</a> legally with the name you trust!
     </div>
      
     <div class="blockfooter">
      <a href="/online-horse-racing" title="Online Horse Racing" class="btn btn-primary">Learn More<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>
  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h2>Legal, Safe & Secure</h2>
     </div>
     <div class="thumbnail margin-bottom-20"><img alt="legal-safe-secure" src="img/legal-safe-secure.jpg" class="img-responsive" /></div>
     <div class="content">
      As a US Licensed horse racing site, we provide <a href="/legal-online-horse-betting" title="Legal Online Horse Betting">legal online horse betting</a> for US citizens. With our secure and easy betting interface, you'll get fast service and fast payouts everytime and we never share your personal information.
     </div>
     
     <div class="blockfooter">
      <a href="/who-is-usracing" title="Learn More" class="btn btn-primary">Learn More<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>
  <div class="col-md-4">
   <div class="service">
    <div class="desc">
     <div class="headline">
      <h2>Free $125 Signup Offer</h2>
     </div>
     <div class="thumbnail margin-bottom-20"><img alt="horse-racing-bonus" src="img/horse-racing-bonus.jpg" class="img-responsive" /></div>
     <div class="content">
      <strong>Get $125 Free for Joining!</strong> We trust you'll love us that much we'll give you $125. <a href="/signup/" rel="nofollow">Sign Up</a>, make a  $25 deposit, and away you go! With Visa / Mastercard and Bet Cash, making a deposit  and wagering is now easier than ever!  Visit our <a href="/cash-bonus">Cash Bonus</a> page to learn more.     </div>
     
     <div class="blockfooter">
      <a href="/signup/" title="Sign Up for Free" class="btn btn-primary">Sign Up for Free<i class="fa fa-angle-right"></i></a>
     </div>
    </div>
   </div>
  </div>
 </div> <!-- end/row-->
</div> <!-- end/container-->




<!-- === Divider Bar ========================================================================== -->
<div id="dvdr1" class="dividerBar margin-bottom-40">
 <div class="container">
  <i class="icon fa fa-ticket"></i>
  <table class="info" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
  <strong>{include file='/home/ah/allhorse/public_html/ps/racedate.php'}</strong>
  <br />Pimlico
  </td></tr></table>
  
  <h3>Bet on the Preakness Stakes</h3>
  <a href="http://www.usracing.com/preakness-stakes/betting" class="btn btn-sm btn-simple-white hidden-xs" rel="nofollow">Learn More<i class="fa fa-angle-right"></i></a>
  <a href="http://www.usracing.com/preakness-stakes/betting" class="btn btn-sm btn-simple-white visible-xs" rel="nofollow"><i class="fa fa-angle-right"></i></a>
 </div>
</div><!-- end/dividarBar-->


<div class="container">
<!-- === Content Row =========================================================================== -->
<div class="row home-mid margin-bottom-30">
<div class="col-md-8">

{* <div class="events margin-bottom-30">
	<div class="headline"><h2>Big Horse Races</h2></div>
	<div class="row margin-bottom-15">
	<div class="col-md-4 first"><span class="thumbnail"><img alt="kentucky-derby-roses" class="img-responsive" src="/img/kentucky-derby-roses.jpg" /></span></div>
	<div class="col-md-4"><span class="thumbnail"><img alt="kentucky-derby-racing" class="img-responsive" src="/img/kentucky-derby-racing.jpg" /></span></div>
	<div class="col-md-4 last"><span class="thumbnail"><img alt="kentucky-derby-winner" class="img-responsive" src="/img/kentucky-derby-winner.jpg" /></span></div>
	</div>*}

	{*include file="inc/home_events.tpl"*}

{*<div class="blockfooter"><a href="/road-to-the-roses" class="btn btn-primary">Road to the Roses <i class="fa fa-angle-right"></i></a></div>
</div>*}

{include file='inc/home_blog.tpl'}   

<!-- NEWS -->
<div class="news news-home margin-bottom-30">
<div class="headline"><h2>Horse Racing News</h2></div>
{include file='includes/ahr_block_racing_news.tpl'}  
<div class="blockfooter">
      <a href="/news" class="btn btn-primary">Horse Racing News <i class="fa fa-angle-right"></i></a>
</div>
</div>
<!-- end/NEWS -->
 


</div> <!-- end col-md-8-->



<div class="col-md-4">
{include file="inc/home_countdown.tpl"}	

<div id="carryovers" class="margin-bottom-30">
<div class="headline"><h2>Today's Carryovers</h2><a data-toggle="modal" data-target="#carryoverHelp" class="btn btn-sm btn-default"><i class="fa fa-question"></i></a></div>
{include file="inc/home_carryovers.tpl"}
</div>


 
{*
<div id="graded" class="gradedStakes margin-bottom-30">
    <div class="headline"><h2>Horse Races to Watch</h2></div>
    
 	<div id="graded-content" class="dateCarousel carousel slide carousel-v1" >
    <div class="carousel-inner margin-bottom-20">*}
		{*include file="includes/graded-stakes-home.tpl"*}
{*	</div><!-- end/carousel-inner -->
     
	  <div class="carousel-arrow">
      <a data-slide="prev" href="#graded-content" class="left carousel-control"><i class="fa fa-angle-left"></i></a>
      <a data-slide="next" href="#graded-content" class="right carousel-control"><i class="fa fa-angle-right"></i></a>
      </div>
      <div class="blockfooter">
      <a href="/horse-races-to-watch" class="btn btn-primary">Horse Races to Watch <i class="fa fa-angle-right"></i></a>
      </div>
     
</div> <!-- end/graded-content -->
</div> <!-- end/graded -->*}



    
<div id="leaders" class="topLeaders margin-bottom-30">

    <div class="headline"><h2>Horse Racing Leaders</h2></div>
	
    {include file="includes/ahr_block_top_leaders.tpl"}  
       

    
</div> <!-- end/leaders -->
   
    
</div><!-- end/col-md-4-->
</div><!-- end/row-->
</div><!-- end/container-->



<!-- === Divider Bar ========================================================================== -->
<div id="dvdr2" class="dividerBar margin-bottom-40">
 <div class="container">
  <i class="icon fa fa-film"></i>
  <table class="info" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td>
  <strong>Watch online horse racing</strong>
  <br />Desktop, Mobile & Tablet
  </td></tr></table>
  
  <h3>Watch Horse Racing Live!</h3>
  <a href="/live-horse-racing" class="btn btn-sm btn-simple-white hidden-xs" rel="nofollow">Learn More<i class="fa fa-angle-right"></i></a>
  <a href="/live-horse-racing" class="btn btn-sm btn-simple-white visible-xs" rel="nofollow"><i class="fa fa-angle-right"></i></a>
 </div>
</div>
<!-- end/dividarBar-->



<!-- === Content Row ========================================================================== -->
<div class="container">
 <div class="row bottom home-bottom">
  <div class="col-md-4">
   
   		{include file="inc/home_videos.tpl"}
   
  </div><!-- end/col-md-4-->
  
  <div class="col-md-4">
      
     	{include file="inc/home_twitter.tpl"} 
    
  </div><!-- end/col-md-4-->
  
  <div class="col-md-4">
   
   		{include file="inc/home_shop.tpl"}
   
  </div><!-- end/col-md-4-->
  
 </div><!-- end/row-->
</div><!-- end/container-->

