{literal}
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite", 
  "name" : "US Racing",
  "url" : "https://www.usracing.com/",
  "potentialAction" : {
    "@type" : "SearchAction",
    "target" : "https://www.usracing.com/?s={search_term}",
    "query-input" : "required name=search_term"
  }                     
}
</script>
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "legalName" : "US Racing",
  "url" : "https://www.usracing.com/",
  "logo" : "https://www.usracing.com/img/usracing-betonhorses.gif",
  "sameAs" : [ "https://www.facebook.com/usracingtoday",
    "https://twitter.com/usracingtoday",
    "https://www.instagram.com/usracingtoday/"]
}
</script>
{/literal}



{if $PWC_Switch} 
	{include file="/home/ah/allhorse/public_html/usracing/block-slider-pegasus.tpl"} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/usracing/block-main-pegasus.tpl" } 
	{include file="/home/ah/allhorse/public_html/usracing/block-blue-signup-bonus-pegasus.tpl"}    
	{include file="/home/ah/allhorse/public_html/usracing/block-mobile-pegasus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 
		{include file="/home/ah/allhorse/public_html/usracing/block-testimonial-pegasus.tpl" } 
{/if}
{if $DB_Switch_2PM} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/slider-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/main-dubai.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/rebates-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/blue-signup-bonus-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/mobile-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/racetracks-dubai.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/experts-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/exciting-dubai.tpl"}
		{include file="/home/ah/allhorse/public_html/usracing/block_dubai/testimonial-dubai.tpl"} 
	{assign var="Default" value=false} 
{/if}

{if $KD_Switch_Phase_1 } {*Day after DWC*}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/newSlider.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 		
	{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}	
	{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
	{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 	
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{assign var="Default" value=false}
{/if}

{if $KD_Switch_Phase_2} {*Two weeks out*}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/newSlider.tpl"} 
	{* {include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-derby-phase-2.tpl"}  *}
	{include file="/home/ah/allhorse/public_html/kd/block-countdown.tpl"}	
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/kd/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/kd/block-jockeys.tpl"}
	{include file="/home/ah/allhorse/public_html/kd/block-trainers.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 

	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 	
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{assign var="Default" value=false}
{/if}
{if $KD_Switch_Phase_3} {*one week out*}
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-derby-phase-2.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 	
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{assign var="Default" value=false}
{/if}
{if $KD_Switch_Phase_4} {*Kentucky Oaks*}
	{include file="/home/ah/allhorse/public_html/usracing/index/block-slider.tpl"} 
{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-oaks.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 	
	{assign var="Default" value=false}
{/if}
{if $KD_Switch_Phase_5} {*Post Oaks*}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-derby-phase-2.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 	
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{assign var="Default" value=false}
{/if}

{if $PS_Switch}
	{include file="/home/ah/allhorse/public_html/usracing/index/block-slider.tpl"} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 
	{assign var="Default" value=false}
{/if}


{if $BS_Switch}
	{include file="/home/ah/allhorse/public_html/usracing/index/block-slider.tpl"} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 
	{assign var="Default" value=false}
{/if}

{if $BC_Switch}
	{*include file="/home/ah/allhorse/public_html/usracing/block-slider-breeders.tpl"*} {*Expires Nov 3*}
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-breeders.tpl" *} {*Expires Nov 3 *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-countdown-breeders.tpl" *}  {*Expires Nov 3 *}    
	{*include file="/home/ah/allhorse/public_html/usracing/block-countdown-breeders.tpl" *} {*Belmont*} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-main-breeders.tpl" *}  {*Expires Nov 3 *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-free-bet-breeders.tpl" *}  {*Expires Nov 3 *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-blue-signup-bonus-breeders.tpl"*}    {*expires Nov 3*} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-mobile-breeders.tpl"*} {*expires Nov 3*}
	{*include file="/home/ah/allhorse/public_html/usracing/block-exciting-breeders.tpl"*} {*Expires Nov 3*}     
{/if}



{if $Default} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-slider.tpl" } 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-final-cta.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-testimonial.tpl"} 
{/if}



{*include file="/home/ah/usracing.com/smarty/templates/content/home_slider.tpl"*} {*is this a webpage?  No.  So why is it in the content directory???n  Clean this up please.*}
{*<section class="bs-belmont_"><img src="/img/index/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section> *} {*Legacy to be dealt with at TC*}
{*include file="/home/ah/allhorse/public_html/usracing/block-casino-free-chips.tpl"*}  {*Expires Nov 3 *}
{*include file="/home/ah/allhorse/public_html/usracing/block-casino-cash-back.tpl"*}  {*Resumes Nov 3 - dont enable this*}
{*include file="/home/ah/allhorse/public_html/usracing/block-blue-testimonial.tpl"*} 
{*include file="/home/ah/allhorse/public_html/usracing/block-casino-games.tpl"*}
{*include file="/home/ah/allhorse/public_html/usracing/block-bet-trainers-belmont.tpl"*}  {*Resumes after Preakness*}
{*include file="/home/ah/allhorse/public_html/usracing/block-casino-cash-back.tpl"*} 
{*include file="/home/ah/allhorse/public_html/usracing/block-bet-on-football.tpl"*} {*Expires after Superbowl *}     
{*include file="/home/ah/allhorse/public_html/usracing/block-bet-on-sports.tpl"*}  
{*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-3.tpl"*}   
{*include file="/home/ah/allhorse/public_html/usracing/block-50-states.tpl"*}  
{*include file="/home/ah/allhorse/public_html/usracing/block-fast-payouts.tpl"*} 
  
           
 {* </section> *}
 {literal}
 <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
		$('.testSlider').slick({
				dots: true,
				speed: 500,
				fade: true,
				autoplay: true,
  			autoplaySpeed: 2000,
		}
</script>
{/literal}


