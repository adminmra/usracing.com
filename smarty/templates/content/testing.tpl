<div id="main" class="container">
<link rel="stylesheet" href="/assets/css/tooltip.css"/>
<script src="/assets/js/boxover.js"></script>
    <div class="row">
        <div id="left-col" class="col-md-9">

            <div class="headline">
            <!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- -->
                <h1>Testing Page AMP</h1>
            <!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ -->
            </div><!-- end/headline -->

            <div class="content">
            <!-- --------------------- CONTENT starts here --------------------------------------------------- -->

            <!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
            </div> <!-- end/ content -->
        </div> <!-- end/ #left-col -->

        <div id="right-col" class="col-md-3">
        <!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
        {include file='inc/newsletter_form.tpl'}
        {*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}
        <!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
        </div><!-- end: #right-col -->
    </div><!-- end/row -->
</div><!-- end/container -->
