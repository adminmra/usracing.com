<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

 
                                        
          
<div class="headline"><h1>US Racing Privacy Policy</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p class="h6">Last modified: March 28, 2017</p>

<p class="h4">1.  Introduction</p>
	<p>US Racing (“Company” or “We”) respect your privacy and are committed to protecting it through our compliance with this policy.<br><br>
	This policy describes the types of information we may collect from you or that you may provide when you visit the website <a href="https://www.usracing.com">https://www.usracing.com</a> or our US Racing Application (collectively “Website”) and our practices for collecting, using, maintaining, protecting and disclosing that information.<br><br>
	his policy applies to information we collect:</p>
		<ul>
			<li>On this Website</li>
			<li>In e-mail, text and other electronic messages between you and this Website.</li>
		</ul><br>
			<p>It does not apply to information collected by:</p>
			<ul>
				<li>us offline or through any other means, including on any other website operated by Company or any third party; or
				any third party, including through any application or content  that may link to or be accessible from or on the Website</li>
			</ul>
				<br>
					<p>Please read this policy carefully to understand our policies and practices regarding your information and how we will treat it. If you do not agree with our policies and practices, your choice is not to use our Website. By accessing or using this Website, you agree to this privacy policy. This policy may change from time to time. Your continued use of this Website after we make changes is deemed to be acceptance of those changes, so please check the policy periodically for updates.
					</p>
					
<!-- --------------------- Under the age of 18  ---------------------- -->

<p class="h4">2.  Children Under the Age of 18</p>
	<p>Our Website is not intended for children under 18 years of age. No one under age 18 may provide any personal information to or on the Website. We do not knowingly collect personal information from individuals under 18. If you are under 18, do not use or provide any information on this Website or on or through any of its features/register on the Website, make any purchases through the Website, use any of the interactive or public comment features of this Website or provide any information about yourself to us, including your name, address, telephone number, e-mail address or any screen name or user name you may use. If we learn we have collected or received personal information from an individual under 18 without verification of parental consent, we will delete that information. If you believe we might have any information from or about an individual under 18, please contact us at cs@usracing.com.</p>

<!-- --------------------- Information we collect about you  ---------------------- -->

<p class="h4">3.  Information We Collect About You and How We Collect It</p>
	<p>We collect several types of information from and about users of our Website, including information:</p>
	<ul>
		<li>by which you may be personally identified, such as name, postal address, e-mail address, username and password, Apple iTunes ID, and Facebook username and password and profile information (“personal information”);</li>
		<li>about your internet connection, the equipment you use to access our Website and usage details.</li>
	</ul>
	<br><p>We collect this information:</p>
	<ul>
		<li>Directly from you when you provide it to us.</li>
		<li>Automatically as you navigate through the site. Information collected automatically may include usage details, IP addresses and information collected through cookies, web beacons, and other tracking technologies.</li>
		<li>From third parties such as Facebook and Twitter, with your approval.</li>
	</ul>
	<br>
	
<!-- --------------------- Information You Provide to Us  ---------------------- -->
<p class="h4">4.  Information You Provide to Us</p>
            <p>The information we collect on or through our Website may include:</p>
            <ul>
            	<li>Information that you provide by filling in forms on our Website. This includes information provided at the time of registering to use our Website. We may also ask you for information when you enter a contest or promotion sponsored by us, and when you report a problem with our Website</li>
            	<li>Records and copies of your correspondence (including e-mail addresses), if you contact us.</li>
            	<li>Your search queries on the Website.</li>
            </ul>
            <br>
             <p>You also may provide information to be published or displayed (hereinafter, “posted”) on public areas of the Website, or transmitted to other users of the Website or third parties (collectively, “User Contributions”). Your User Contributions are posted on and transmitted to others at your own risk. please be aware that no security measures are perfect or impenetrable. Additionally, we cannot control the actions of other users of the Website with whom you may choose to share your User Contributions. Therefore, we cannot and do not guarantee that your User Contributions will not be viewed by unauthorized persons.</p>

<!-- --------------------- Usage Details, IP Addresses   ---------------------- -->

<p class="h4">5.  Usage Details, IP Addresses And Cookies/, Cookies and Other Technologies</p>
<p>As you navigate through and interact with our Website, we may automatically collect certain information about your equipment, browsing actions and patterns, including:</p>
	<ul>
		<li>Details of your visits to our Website, including traffic data, location data, and other communication data and the resources that you access and use on the Website.</li>
		<li>Information about your computer and internet connection, including your IP address, operating system and browser type.</li>
	</ul>
	<br>
	<p>The information we collect automatically is statistical data, and does not identify any individual. It helps us to improve our Website and to deliver a better and more personalized service by enabling us to:</p>
	<ul>
		<li>Estimate our audience size and usage patterns.</li>
		<li>Store information about your preferences, allowing us to customize our Website according to your individual interests.</li>
		<li>Speed up your searches.</li>
		<li>Recognize you when you return to our Website.</li>
		<li>Understand which parts of the Website to optimize based on user traffic.</li>
		<li>Provide targeted advertising or relevant content.</li>
		<li>Provide only news and information relevant to usage data</li>
	</ul>
	<br>
		<p>The technologies we use for this automatic data collection may include:</p>
		<ul>
			<li><strong>Cookies (or browser cookies).</strong> A cookie is a small file placed on the hard drive of your computer. You may refuse to accept browser cookies by activating the appropriate setting on your browser. However, if you select this setting you may be unable to access certain parts of our Website. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you direct your browser to our Website.</li>
			<li><strong>Flash Cookies.</strong> Certain features of our Website may use local stored objects (or Flash cookies) to collect and store information about your preferences and navigation to, from and on our Website. Flash cookies are not managed by the same browser settings as are used for browser cookies. For information about managing your privacy and security settings for Flash cookies.</li>
			<li><strong>Web Beacons.</strong> Pages of our the Website may contain small electronic files known as web beacons (also referred to as clear gifs. pixel tags and single-pixel gifs) that permit the Company, for example, to count users who have visited those pages or and for other related website statistics (for example, recording the popularity of certain website content and verifying system and server integrity).</li>
		</ul>
		<br>
		
		<!-- --------------------- Third-party Advertiser   ---------------------- -->
		
		<p class="h4">6.  Third-party Advertiser Use of Cookies and Other Tracking Technologies</p>
		<p>Some advertisements on the Website are served by third-party advertisers, ad networks and ad servers. These third parties may use cookies alone or in conjunction with web beacons or other tracking technologies to collect information about our users. This may include information about users’ behavior on this and other websites to serve them interested-based (behavioral) advertising. We do not control these third parties’ tracking technologies or how they may be used. If you have any questions about an advertisement, you should contact the responsible advertiser directly.</p>
		
		<!-- --------------------- How We Use Your Information   ---------------------- -->
		
		<p class="h4">7.  How We Use Your Information</p>
		<p>We use information that we collect about you or that you provide to us, including any personal information:</p>
		<ul>
			<li>To present our Website and its contents to you.</li>
			<li>To provide you with information, products or services that you request from us.</li>
			<li>To fulfill any other purpose for which you provide it.</li>
			<li>To provide you with notices about your account/subscription, including expiration and renewal notices.</li>
			<li>	To carry out our obligations and enforce our rights arising from any contracts entered into between you and us, including for billing and collection.</li>
			<li>To notify you about changes to our Website or any products or services we offer or provide though it.</li>
			<li>To allow you to participate in interactive features on our Website.</li>
			<li>In any other way we may describe when you provide the information.</li>
			<li>For any other purpose with your consent.</li>
		</ul>
		<br>
		<p>We may also use your information to contact you about our own and third-parties’ goods and services that may be of interest to you. If you do not want us to use your information in this way, you may opt out by sending an email to cs@usracing.com. We may use the information we have collected from you to enable us to display advertisements to our advertisers’ target audiences. Even though we do not disclose your personal information for these purposes without your consent, if you click on or otherwise interact with an advertisement, the advertiser may assume that you meet its target criteria.</p>
		
		<!-- --------------------- Disclosure Information   ---------------------- -->
		
		<p class="h4">8.  Disclosure of Your Information</p>
			<p>We may disclose aggregated information about our users, and information that does not identify any individual, without restriction.</p>
			
			<p>We may disclose personal information that we collect or you provide as described in this privacy policy:</p>
			<ul>
				<li>To our subsidiaries and affiliates.</li>
				<li>To contractors, service providers and other third parties we use to support our business and who are bound by contractual obligations to keep personal information confidential and use it only for the purposes for which we disclose it to them.</li>
				<li>To a buyer or other successor in the event of a merger, divestiture, restructuring, reorganization, dissolution or other sale or transfer of some or all of US Racing’s assets, whether as a going concern or as part of bankruptcy, liquidation or similar proceeding, in which personal information held by US Racing about our Website users is among the assets transferred.</li>
				<li>To third parties to market their products or services to you if you have not opted out of these disclosures. We contractually require these third parties to keep personal information confidential and use it only for the purposes for which we disclose it to them.</li>
				<li>To fulfill the purpose for which you provide it.</li>
				<li>For any other purpose disclosed by us when you provide the information.</li>
				<li>With your consent.</li>
			</ul>
			<br>
				<p>We may also disclose your personal information:</p>
				<ul>
					<li>To comply with any court order, law or legal process, including to respond to any government or regulatory request.</li>
					<li>To enforce or apply our terms of use and other agreements, including for billing and collection purposes.</li>
					<li>If we believe disclosure is necessary or appropriate to protect the rights, property, or safety of US Racing, our customers or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction.</li>
				</ul>
				<br>
				
				<!-- --------------------- 9.  Choices About How We Use and Disclose Your Information ----------- -->
				
			<p class="h4">9.  Choices About How We Use and Disclose Your Information</p>
			<p>We strive to provide you with choices regarding the personal information you provide to us. We have created mechanisms to provide you with the following control over your information:</p>
			<ul>
				<li><strong>Tracking Technologies and Advertising.</strong> You can set your browser to refuse all or some browser cookies, or to alert you when cookies are being sent. To learn how you can manage your Flash cookie settings, visit the Flash player settings page on Adobe’s website. If you disable or refuse cookies, please note that some parts of this site may then be inaccessible or not function properly.</li>
				<li><strong>Disclosure of Your Information for Third-Party Advertising. </strong>If you do not want us to share your personal information with unaffiliated or non-agent third parties for promotional purposes, you may opt out by sending an email to cs@usracing.com.</li>
				<li><strong>Promotional Offers from the Company.</strong> If you do not wish to have your e-mail address used by the Company to promote our own or third parties’ products or services, you may opt out by sending an email to cs@usracing.com. If we have sent you a promotional e-mail, you may send us a return e-mail asking to be omitted from future e-mail distributions.</li>
				<li><strong>Targeted Advertising.</strong> If you do not want us to use information that we collect or that you provide to us to deliver advertisements according to our advertisers’ target-audience preferences, you may opt out by sending an email to cs@usracing.com.</li>
			</ul>
			<br>
				<p>We do not control third parties’ collection or use of your information to serve interest-based advertising. However, these third parties may provide you with ways to choose not to have your information collected or used in this way. You can opt out of receiving targeted ads from members of the Network Advertising Initiative (“NAI”) on the NAI’s website.</p>
				<ul>
					<li><strong>Geographic Location.</strong>  If you do not want us to collect real-time information regarding your geographic location, you can disable geographic location tracking by adjusting your browser or mobile phone’s settings.</li>
				</ul>
				<br>
				
				<!-- --------------------- California Privacy Rights ----------- -->
				
					<p class="h4">10.  California Privacy Rights</p>
					<p>California Civil Code Section § 1798.83 permits users of our Website that are California residents to request certain information regarding our disclosure of personal information to third parties for their direct marketing purposes. To make such a request, please send an e-mail to cs@usracing.com.  We do not respond to Do Not Track browser signals.</p>
					
				<!-- --------------------- Data Security -------------------------- -->
				
					<p class="h4">11.  Data Security</p>
					<p>We have implemented measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration and disclosure. All information you provide to us is stored on our secure servers behind firewalls.</p>
					
					<p>The safety and security of your information also depends on you. Where we have given you (or where you have chosen) a password for access to certain parts of our Website, you are responsible for keeping this password confidential. We ask you not to share your password with anyone. We urge you to be careful about giving out information in public areas of the Website like message boards. The information you share in public areas may be viewed by any user of the Website.</p>
					
					<p>Unfortunately, the transmission of information via the internet is not completely secure. Although we do our best to protect your personal information, we cannot guarantee the security of your personal information transmitted to our Website. Any transmission of personal information is at your own risk. We are not responsible for circumvention of any privacy settings or security measures contained on the Website.</p>
					
					<p>US Racing reviews and recommends third party websites.  If a visitor to US Racing goes to a third party website via a hyperlink or banner, US Racing is not responsible for the data security of such third party websites.</p>
					
					<!-- --------------------- Changes to our privacy policy -------------------------- -->
				
					<p class="h4">12.  Changes to Our Privacy Policy</p>
					
					<p>It is our policy to post any changes we make to our privacy policy on this page with a notice that the privacy policy has been updated on the Website home page. If we make material changes to how we treat our users’ personal information, we will notify you by e-mail to the e-mail address specified in your account or through a notice on the Website home page. The date the privacy policy was last revised is identified at the top of the page. You are responsible for ensuring we have an up-to-date active and deliverable e-mail address for you, and for periodically visiting our Website and this privacy policy to check for any changes.</p>
					
					<!-- --------------------- Contacting US Racing -------------------------- -->
					<p class="h4">13.  Contacting US Racing</p>
					<p>If you have any questions about this privacy statement, our practices or your dealings with us, contact: <a href="http://www.usracing.com/support">Customer Support.</a></p>
					
					
				
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 
 

 
</div><!-- end/row -->
</div><!-- /#container --> 


{literal} 
<script type="text/javascript">
$(document).ready(function() {
  $('.faqs').css("display","none");
});
function slideIt(thechosenone) {
     $('div[class|="faqs"]').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).slideDown(200);
          }
          else {
               $(this).slideUp(200);
          }
     });
}
</script> 
{/literal} 