<div id="main" class="container">
<div class="row">

<div id="left-col" class="col-md-9">

<div class="headline"><h1>States with Racetracks and information for Horse Betting</i></h1></div>

<div class="content"> 
<!-- --------------------- content starts here ---------------------- -->
   <i>Certain states in the USA have racetracks and allow online horse betting.  Some states do allow online horse wagering and some do not depending on whether you are a resident of that state. In fact, sometimes certain states change their rules regarding horse racing. Click on the state for more infrmation but always check your local laws. </p>
 <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">{$button_cta}</a></p>   



<h2><i class="flag US"></i>US Horse Racing Racetracks</h2>


<ul>
	<dt><a href="/alabama" >Alabama</a></dt>

</ul>

<ul>
	<dt><a href="/alaska" >Alaska</a></dt>
	
</ul>

<ul>
<dt> <a href="/arizona" >Arizona</a></dt>

</ul>

<ul>
	<dt><a href="/arkansas" >Arkansas</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/california" >California</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/colorado" >Colorado</a></dt>
	
</ul>

<ul>
	<dt><a href="/conneticut" >Connecticut</a></dt>
	
</ul>

     
<ul>
	<dt><a href="/delaware" >Delaware</a></dt>

</ul>
     
<ul>
	<dt><a href="/florida" >Florida</a></dt>
	
</ul>
 
 <ul>
	<dt><a href="/georgia" >Georgia</a></dt>
	
</ul>

 <ul>
	<dt><a href="/hawaii" >Hawaii</a></dt>
	
</ul>    
<ul>
	<dt><a href="/idaho" >Idaho</a></dt>
	
</ul>     
     
     
<ul>
	<dt><a href="/illinois" >Illinois</a></dt>

</ul>
     
<ul>
	<dt><a href="/indiana" >Indiana</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/iowa" >Iowa</a></dt>
	
</ul>

<ul>
	<dt><a href="/kansas" >Kansas</a></dt>
	
</ul>     
     
          
<ul>
	<dt><a href="/kentucky" >Kentucky</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/louisiana" >Louisiana</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/maine" >Maine</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/maryland" >Maryland</a></dt>
	

</ul>
     
<ul>
	<dt><a href="/massachusetts" >Massachusetts</a></dt>
	
</ul>

<ul>
	<dt><a href="/michigan" >Michigan</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/minnesota" >Minnesota</a></dt>
	
</ul>

<ul>
	<dt><a href="/mississippi" >Mississippi</a></dt>
	
</ul>   

<ul>
	<dt><a href="/missouri" >Missouri</a></dt>
	
</ul>   

<ul>
	<dt><a href="/montana" >Montana</a></dt>

</ul>     
     
<ul>
	<dt><a href="/nebraska" >Nebraska</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/nevada" >Nevada</a></dt>
	
</ul>

<ul>
	<dt><a href="/new-hampshire" >New Hampshire</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/new-jersey" >New Jersey</a></dt>

</ul>
     
<ul>
	<dt><a href="/new-mexico" >New Mexico</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/new-york" >New York</a></dt>
	
</ul>

<ul>
	<dt><a href="/north-dakota" >North Dakota</a></dt>
	
</ul>

<ul>
	<dt><a href="/north-carolina" >North Carolina</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/ohio" >Ohio</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/oklahoma" >Oklahoma</a></dt>

</ul>
     
<ul>
	<dt><a href="/oregon" >Oregon</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/pennsylvania" >Pennsylvania</a></dt>

</ul>


<ul>
	<dt><a href="/tennessee" >Tennessee</a></dt>
	
</ul>

<ul>
	<dt><a href="/rhode-island" >Rhode Island</a></dt>
	
</ul>

<ul>
	<dt><a href="/south-carolina" >South Carolina</a></dt>
	
</ul>

<ul>
	<dt><a href="/south-dakota" >South Dakota</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/texas" >Texas</a></dt>
	
	</ul>

<ul>
	<dt><a href="/tennessee" >Tennessee</a></dt>
	
</ul>

<ul>
	<dt><a href="/utah" >Utah</a></dt>
	
</ul>

<ul>
	<dt><a href="/vermont" >Vermont</a></dt>
	
</ul>

<ul>
	<dt><a href="/virginia" >Virginia</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/washington" >Washington</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/west-virginia" >West Virginia</a></dt>
	
</ul>
     
<ul>
	<dt><a href="/wyoming" >Wyoming</a></dt>
	
</ul>     
        
 
      
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
