{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
{include file='menus/instructionalvideos.tpl'}
{include file='menus/howtobetonhorses.tpl'}
{include file='menus/advancedhorsebets.tpl'}
{include file='menus/horsebetting101.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
               

 
                                        
          
<div class="headline"><h1>How to Place a Horse Bet</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{literal}<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>{/literal}




<p>Learning to bet on horses may seem challenging at first, but it is really very easy and fun once you understand the basics.  Click on the icons below to see how easy it is to win at horse racing! To learn about how to read the Daily Racing Form and understand Past Performances, click on the associated icons.</p>
<br /><br />
<p align="center"><a title="Learn to Bet on Horses" href="/how-to/bet-on-horses"><img title="Learn to bet on horses!" border="0" alt="Learn to bet on horses!" src="/themes/images/tutorial_horse.jpg" /></a></p>
<p align="center"><a title="Learn to Bet on Horses" href="/how-to/read-the-daily-racing-form"><img title="Learn to read the Daily Racing Form" border="0" alt="Learn to read the Daily Racing Form" src="/themes/images/readthedailyracingform.jpg" /></a></p>
<p align="center"><a title="Learn Sports Betting"  href="/how-to/read-past-performances"><img title="Past Performances" border="0" alt="Past Performances" src="/themes/images/pastperformances.jpg" /></a></p>
<br />
<br />
<p><span>Tours require a Flash Player plug in. Having problems viewing the tutorials?</span></p>
<p>Download the latest Flash Player plug-in.</p><p><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank"><img title="Flash Player" border="0" alt="Flash Player" src="/themes/images/get_flash_player.gif" /></a></p>


        
        

  <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
