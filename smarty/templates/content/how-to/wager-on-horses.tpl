{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          

{include file='menus/instructionalvideos.tpl'}
{include file='menus/howtobetonhorses.tpl'}
{include file='menus/advancedhorsebets.tpl'}
{include file='menus/horsebetting101.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
               

 
                                        
          
<div class="headline"><h1>How to Wager on Horses</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>Conventional wisdom holds that a fan will do OK at the racetrack if he or she grabs a tip sheet or a local newspaper and follows the picks of the pros. That may or may not be so, but it is not nearly as much fun as making your own selections and trying to outsmart everyone else at the track. This is the essence of pari-mutuel wagering: each bettor is wagering against all the others making the same type of bet.</p>
<p>Pari-mutuel means that bets by race fans go into a common pool, which is then distributed to the winning ticket-holders after the track's takeout for the race's purse, taxes, and track profits, is subtracted. The more fans holding a winning ticket, the less the pay-out will be. This spawned the term "favorite", which is used to describe the horse that has taken the most money. However, favorites win only about 30 percent of the time. It makes sense for the bettor to study the program with the day's racing information before making a pick.</p>
<p>Picking winners at the track is one of the most fun and exciting forms of entertainment around, as long as you bet with your head. Those who want to minimize betting risk should stick to less aggressive bets. Those looking to "hit the big one" usually opt for the exotic wagers, such as a trifecta or a pick three. All bets can be fun and profitable if managed correctly. Like playing the stock market, the key words are money management, performance, and value. Use the section below to aid your understanding of betting terminology and rules.</p>
<p>And remember, never bet more than you can stand to lose.</p>

<p><a href="/how-to/bet-on-horses" rel="nofollow"><strong>Click Here</strong></a> for step by step instructions on how to bet on horse at US Racing.</p>        
        

  <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
