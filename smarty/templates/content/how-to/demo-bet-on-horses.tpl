{literal}
<style>
img#logo-header {
    margin-left: 222px !important;
}
.block-betting-guide ul li:last-child {
    list-style: none;
}
</style>
{/literal}

{assign var="ref" value="how-to-bet-on-horses"}
{assign var="cta" value="how-to-bet-on-horses"}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{*include file='menus/instructionalvideos.tpl'*}
{include file='menus/howtobetonhorses.tpl'}
{include file='menus/advancedhorsebets.tpl'}
{include file='menus/horsebetting101.tpl'} 
<!-- ---------------------- end left menu contents ------------------- -->         
</div> 
    
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">          
                                        
          
<div class="headline"><h1>How to Bet on the Horses</h1></div>
{* <div class="headline"><h1>How to Bet on Horses at US Racing - Belmont Stakes</h1></div> *}
<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>Learn to bet on horses with an interactive tutorial.  </p>


{assign var="cta_append" value=" at any of these trainers"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

<p align="center"><a href="#" class="btn-xlrg ">Bet Now</a></p>

{include file="/home/ah/allhorse/public_html/usracing/betting-guide.tpl"}

<h2>How to place a bet on the ponies?  {* Betting on the Belmont Stakes and any horse race is *}It is as easy as 1, 2, 3!</h2>
{*
<p>Watch the videos below on how to place a wager at US Racing.  In the first video, Forgive Jimmy T., he gets emotional about horse racing!</P>
<br>
<P>If you want to know how to bet on the Belmont Stakes quick and to the point, just watch the second video!  As far as Jimmy's advice?  No guarantees there!  And remember, the Belmont Stakes is at PIMLICO racecourse and it is Race #13.</p>
*}
<br>
<p>For information on betting on horses, please click the "<strong>MORE</strong>" button to the left or the &nbsp;<i class="fa fa-bars" style="font-size:20px;"></i> &nbsp; button on your mobile phone.</p>
{*</div> *}
<p>Below is an example of how to place a $100 win bet on Justify to win the 2018 Belmont Stakes.</p>

{include_php file='iframe/video/how_to_bet_video.php'}
<br>
<br>
{* <p>Below is a general tutorial on how to bet on horses, check it out!</p>
<br>
<iframe class="video"  height="100%" width="100%" src="https://www.youtube.com/embed/B1Yh94tEh3Y?rel=0" frameborder="0" allowfullscreen></iframe> *}

{*include file='inc/video-howtowager.tpl'

<br /><br />
<div class="tag-box tag-box-v4">
<p>Follow these step by step instructions on how to place a wager or watch the video above.</p>
</div>




<h2>Step 1</h2>
<p>Visit the <a href="/bet" target="_blank"><strong>BET NOW</strong></a> tab & select from a wide variety of US and international tracks. Races can be selected from the pulldown menu or from the <strong>Upcoming Races</strong> section in the left column.</p>
<p><img class="img-responsive"  src="/img/help/help-selecttrack.png" alt="how to bet on horses" /></p>


<h2>Step 2</h2>
<p>Pick the Race #, Bet Type & Amount you want to wager. We offer all bet types like Win, Place, Show, Daily Doubles, Exactas and Pick 6 bets. If you can bet it at the track, you can bet on it at US Raing<!--hung-test-->. Post time and Minutes to Post (<strong>MTP</strong>) is conveniently displayed inside the wagering area.</p>
<p><img class="img-responsive"  src="/img/help/help-mtp.png" alt="Place a horse bet" /></p>


<h2>Step 3</h2>
<p>Now it's time to Pick the Winning Horse(s)! View the entries, post number, scratches and current odds and place a check mark in the box to the left of the horse to pick one or more horses for your ticket.</p>
<p><img class="img-responsive"  src="/img/help/help-entries.png" alt="horse racing entries" /></p>


<h2>Step 4</h2>
<p><strong>Daily Doubles and Multi-leg wagers</strong>
<br />
Hover your cursor over the first column to view Horses and Odds for the 1st &quot;Leg&quot; of the bet. When you hover your mouse over the second column of check boxes, you'll see  the Entries area update, listing the horses &amp; odds for the second leg of the bet. Again, click the box to the left of your horse(s) to add it to the betting ticket..</p>
<p><img class="img-responsive"  src="/img/help/help-dailydouble.png" alt="daily double" /></p>


<h2>Step 5</h2>
<p><strong>Review your wager</strong>.
<br />The number of bets and total wager price will update as you pick horses. Click the "<strong>SUBMIT YOUR WAGER</strong>" button at the bottom of the entries section to make your bet.</p>
<p><img class="img-responsive"  src="/img/help/help-submitwager.png" alt="sumit my horse bet" /></p>


<h2>Step 6</h2>
<p><strong>Confirm your wager on your Betting Ticket.</strong></p>
<p><img class="img-responsive"  src="/img/help/help-wagersuccessful.png" /></p>

*}
<br></br>

<h3>And that's it!</h3>
<p>Happy betting, we hope you win!. Login at the top of the page, or <a href="/signup/"><strong>Sign Up here</strong></a> if you are a new member!</p>

 <!--hung-test-->
<a href="/famous-horses/secretarait">.</a>
<!--end-hung-test-->

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
