{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
{include file='menus/instructionalvideos.tpl'}
{include file='menus/howtobetonhorses.tpl'}
{include file='menus/advancedhorsebets.tpl'}
{include file='menus/horsebetting101.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
               

 
                                        
          
<div class="headline"><h1>How to Read the Daily Racing Form</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{literal}<script type="text/javascript" src="/smarty-include/js/swfobject.js"></script>{/literal}




<div style="padding-top: 10px;" id="flashmovie2">Form Flash Movie not loaded</div>
{literal}<script type="text/javascript">		
  var so = new SWFObject("/flash/learnhowtoreadthedailyracingform.swf", "racingform", "685", "515", "8", "#FFFFFF"); 
  so.addParam("allowscriptaccess","always"); so.useExpressInstall("/flash/expressinstall.swf"); 
  so.addParam("wmode", "transparent");
  so.write("flashmovie2");		
</script>{/literal}


<p>Need more information on how to read the Daily Racing Form?  Here is a great explanation of what all those numbers and statistics mean: <a title="Learn how to understand the Daily Racing Form" href="/files/howtoreadthedailyracingform.pdf">Learn how to understand the Daily Racing Form</a> </p>

<p><span class="mediumtext">Tours require a Flash Player plug in. Having problems viewing the tutorials?</span></p><p>Download the latest Flash Player plug-in.</p>

<p><a href="http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" target="_blank"><img title="Flash Player" border="0" alt="Flash Player" src="/themes/images/get_flash_player.gif" /></a></p>

<p>Download the latest Adobe Reader Plug-in.</p>

<p><a class="linkheader" href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img title="Adobe Reader" border="0" alt="Adobe Reader" src="/themes/images/get_adobe_reader.gif" /></a></p>


        
        

  <!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
