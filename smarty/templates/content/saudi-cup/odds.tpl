{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/top-of-page-blocks.tpl"}
{include file="/home/ah/allhorse/public_html/usracing/schema/generated/saudi_cup_odds.tpl"}

 {*Page Unique content starts here*******************************}
    {include file="/home/ah/allhorse/public_html/saudi-cup/block-countdown-new.tpl"}
    <section class="saudi kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
      <h3 class="kd_subheading">{$h3}</h3>
      <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb.tpl'} </p>
      <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now</a></p>
      <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds and Contenders</h2>
    <br>
      

        {* <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds and Contenders</h2> *}
        {* <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds_xml.php'}</p> *}
        <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds.php'}</p>
        <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now </a></p>
        <h2>The {include_php file='/home/ah/allhorse/public_html/saudi-cup/running.php'}  Saudi Cup runs on  {include_php file='/home/ah/allhorse/public_html/saudi-cup/date.php' once=false}.</h2>
        <p>US Racing provides the earliest <a href="/signup?ref={$ref}">Saudi Cup Futures odds</a> of any website. If you see a horse you would like to be added, let us know and we might be able to get it added for you!</p>
        <p>Between now and the <a href="/signup?ref={$ref}">Saudi Cup</a>, the odds will be changing when we add new horses and remove others. Remember, when you a place future wager the odds are fixed and all wagers have action.</p>
        <p>Good luck and see you on {include_php file='/home/ah/allhorse/public_html/saudi-cup/day.php'}!</p>
    </div>
  </div>
</section>


 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/end-of-page-blocks.tpl"}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}