{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{include file='inc/left-nav-btn.tpl'}

{literal}
<!-- <link rel="stylesheet" href="/assets/css/sbstyle.css"> -->
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}

{literal}
  <script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='saudi kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[2]"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front ']/section[@class='saudi kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front ']/section[@class='saudi kd usr-section']/div[@class='container']/div[@class='kd_content']/div/table[@class='data table table-condensed table-striped table-bordered ordenable']/caption"
    }
	</script>
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "SportsEvent",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress"
            }
        },
        "name": "The Saudi Cup",
        "startDate": "2020-02-29T08:00-05:00",
        "endDate": "2020-02-29T12:40-05:00",
        "image": [
          "https://www.usracing.com/img/saudi-cup/saudi-cup-lg.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-md.jpg",
          "https://www.usracing.com/img/saudi-cup/saudi-cup-sm.jpg"
        ],
        "description": "The Saudi Cup",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-10",
            "availability": "http://schema.org/InStock"
        },
        "sport": "Horse Racing",
        "url": "https://www.usracing.com/saudi-cup/betting"
    }
	</script>
{/literal}


{literal}
     <style>
     .kdv-embed iframe {
    height: 700px !IMPORTANT;
}
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }

        @media (max-width: 324px) {
          .usr-section .container {
            padding: 0px;
          }
        }
     </style>
{/literal}

<div id="left-nav">
  {include file='menus/saudi-cup.tpl'}
</div>

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>Saudi Cup Livestream</span>
    </div>
    <div class="text text-md copy-md">
        <span>New members get up to {include_php file='/home/ah/allhorse/public_html/common/bonus-welcome.php' once=false}  cash! </span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

    {include file="/home/ah/allhorse/public_html/saudi-cup/block-countdown-new.tpl"}
    <section class="saudi kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">Watch the 2022 Saudi Cup Live</h1>
      <h2 class="kd_subheading">Saudi Cup Online Livestream</h2>
       <h4 style="font-size:32px;">Where to Bet on the Saudi Cup?</h4>
      <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb.tpl'} </p>
    <!--  <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet on the Saudi Cup</a></p>-->
      <div data-id="2p8p8" class="kdv-embed" data-type="v" data-ap="true"></div>
     
      <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">Bet Now</a></p>
       <h4 style="font-size:32px;">Where to Watch the Saudi Cup?</h4>
      <p style="margin-bottom:5px;">You can livestream the Saudi Cup race and pre-race show at US Racing. You can also watch TV coverage on:</br></p>
      <p style="margin-bottom:5px;">USA - Fox Sports 1, Fox Sport 2</p>
      <p style="margin-bottom:5px;">Canada - TSN</p>
      <p style="margin-bottom:5px;">UK - Sky Sports</p>
      <p style="margin-bottom:5px;">Ireland - Virgin</p>
      <p style="margin-bottom:5px;">Middle East - Saudi TV, Twitter</p>
      <p style="margin-bottom:40px;">South America – ESPN Latin America</p>
   
      <h4 style="font-size:32px;margin-bottom:40px;">What time is the Saudi Cup Post Time?</h4>
 <p>The post time for the $20M Saudi Cup is at 12:40 Eastern Time which is 8:40 PM KAS (Saudi Arabia Time)</p>
        {* <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds and Contenders</h2> *}
        <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds_xml.php'}</p></br>
        <p align="center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now</a></p>
        <h4 style="font-size:32px;">The {include_php file='/home/ah/allhorse/public_html/saudi-cup/running.php'} Saudi Cup runs on {include_php file='/home/ah/allhorse/public_html/saudi-cup/date.php' once=false }.</h4>
        <h3 style="font-size:32px;">What are the Saudi Cup Payouts?</h3>
        <p style="bold;font-size:32px;margin-bottom:5px">$20 million Saudi Cup Payouts</br>
        <table id="infoEntries" class="table table-condensed table-striped table-bordered ordenableResult data" title="$20 million Saudi Cup Payouts" border="0" summary="The 10 horses given first preference (in alphabetical order):" cellspacing="0" cellpadding="0">
<thead>
<tr>
<th style="width: 8%;"><center>Finish</center></th>
<th style="width: 8%;"><center>Payout</center></th>
</tr>
</thead>
<tbody>
<tr>
<td data-title="Horse"><center>First</center></td>
<td data-title="Trainer"><center>$10,000,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Second</center></td>
<td data-title="Trainer"><center>$3,500,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Third</center></td>
<td data-title="Trainer"><center>$2,000,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Fourth</center></td>
<td data-title="Trainer"><center>$1,500,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Fifth</center></td>
<td data-title="Trainer"><center>$1,000,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Sixth</center></td>
<td data-title="Trainer"><center>$600,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Seventh</center></td>
<td data-title="Trainer"><center>$500,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Eighth</center></td>
<td data-title="Trainer"><center>$400,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>Ninth</center></td>
<td data-title="Trainer"><center>$300,000</center></td>
</tr>
<tr>
<td data-title="Horse"><center>10th</center></td>
<td data-title="Trainer"><center>$200,000</center></td>
</tr>
</tbody>
</table>
</b>
        </p>
    </div>
  </div>
</section>

  <!------------------------------------------------------------------>

{include file="/home/ah/allhorse/public_html/saudi-cup/block-testimonial-pace-advantage.tpl"}
{include file="/home/ah/allhorse/public_html/saudi-cup/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/saudi-cup/block-exciting.tpl"}


  <!------------------------------------------------------------------>

{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}
