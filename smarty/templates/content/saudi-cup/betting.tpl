{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/top-of-page-blocks.tpl"}
{include file="/home/ah/allhorse/public_html/usracing/schema/generated/saudi_cup_betting.tpl"}
{*Page Unique content goes here*******************************}
    {include file="/home/ah/allhorse/public_html/saudi-cup/block-countdown-new.tpl"}
    <section class="saudi kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
	<h3 class="kd_subheading">{$h3}</h3>
    <p>{include file='/home/ah/allhorse/public_html/saudi-cup/salesblurb.tpl'} </p>
    		<p align="center"><a href="/signup?{$ref}" rel="nofollow" class="btn-xlrg ">Bet Now</a></p>
    <h2 class="table-title">{include file='/home/ah/allhorse/public_html/saudi-cup/year.php'} Saudi Cup Odds and Contenders</h2>
    <br>

               {* <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds_xml.php'}</p> *}
        <p>{include_php file='/home/ah/allhorse/public_html/saudi-cup/saudi_cup_odds.php'}</p>
        <p align="center"><a href="/signup?{$ref}" rel="nofollow" class="btn-xlrg ">Bet Now</a></p>
        <h2> {include_php file='/home/ah/allhorse/public_html/saudi-cup/running.php'} The Saudi Cup runs on {include_php file='/home/ah/allhorse/public_html/saudi-cup/date.php' once=false}.</h2>
      <p> The <a href="/signup?ref={$ref}"}>Saudi Cup</a> is built around enthusiasm and preparations ensuring even the smallest details are taken care of.</p>

<p>The edition at this venue promises a unique display of talent and effort. Expectations are high and so is the mind-blowing {include_php file='/home/ah/allhorse/public_html/saudi-cup/purse.php'} prize purse. The <a href="/signup?ref={$ref}"}>Saudi Cup</a> attracts thousands of horse racing fans meeting to enjoy an unforgettable horse racing experience in all its splendor.</p>

<p>Mark your calendar for this high profile event on {include_php file='/home/ah/allhorse/public_html/saudi-cup/day.php'} The <a href="/signup?ref={$ref}"}>Saudi Cup</a> is a world class sports celebration. held at the elegant King Abdulaziz Racetrack, located in Riyadh, Saudi Arabia.</p>
				
    </div>
  </div>
</section>
 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_saudi-cup/end-of-page-blocks.tpl"}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}