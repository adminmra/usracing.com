{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
            

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    
<h2>Thoroughbred Racetracks</h2>

<div style="margin-top: 10px;" class="thoroughbred-racetracks">
							  			
                            <table class="racetrack-menu" width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Albuquerque_Downs"  title="Albuquerque Downs">
    <div class="racename">Albuquerque Downs</div>    
    <div class="raceinfo">Aug 14 - Nov 14 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Aqueduct_Racetrack"  title="Aqueduct Racetrack">
    <div class="racename">Aqueduct Racetrack</div>    
    <div class="raceinfo">Jan 01 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Arlington_Park"  title="Arlington Park">
    <div class="racename">Arlington Park</div>    
    <div class="raceinfo">Apr 29 - Sep 26 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Belmont_Park"  title="Belmont Park">
    <div class="racename">Belmont Park</div>    
    <div class="raceinfo">Apr 30 - Oct 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Beulah_Park"  title="Beulah Park">
    <div class="racename">Beulah Park</div>    
    <div class="raceinfo">Jan 09 - Dec 22 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Calder_Race_Course"  title="Calder Race Course">
    <div class="racename">Calder Race Course</div>    
    <div class="raceinfo">Apr 23 - Jan 01 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Canterbury_Park"  title="Canterbury Park">
    <div class="racename">Canterbury Park</div>    
    <div class="raceinfo">May 14 - Aug 29 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Charles_Town_Races_&_Slots"  title="Charles Town Races & Slots">
    <div class="racename">Charles Town Races & Slots</div>    
    <div class="raceinfo">Jan 01 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Churchill_Downs_Racetrack"  title="Churchill Downs Racetrack">
    <div class="racename">Churchill Downs Racetrack</div>    
    <div class="raceinfo">Apr 24 - Nov 27 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Colonial_Downs"  title="Colonial Downs">
    <div class="racename">Colonial Downs</div>    
    <div class="raceinfo">Apr 10 - Sep 12 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Del_Mar"  title="Del Mar">
    <div class="racename">Del Mar</div>    
    <div class="raceinfo">Jul 21 - Sep 08 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Delaware_Park"  title="Delaware Park">
    <div class="racename">Delaware Park</div>    
    <div class="raceinfo">May 01 - Nov 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Delta_Downs"  title="Delta Downs">
    <div class="racename">Delta Downs</div>    
    <div class="raceinfo">Jan 01 - Jul 10 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Ellis_Park_Race_Course"  title="Ellis Park Race Course">
    <div class="racename">Ellis Park Race Course</div>    
    <div class="raceinfo">Jul 10 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Emerald_Downs"  title="Emerald Downs">
    <div class="racename">Emerald Downs</div>    
    <div class="raceinfo">Apr 09 - Sep 26 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Evangeline_Downs"  title="Evangeline Downs">
    <div class="racename">Evangeline Downs</div>    
    <div class="raceinfo">Apr 07 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Fair_Grounds_Race_Course"  title="Fair Grounds Race Course">
    <div class="racename">Fair Grounds Race Course</div>    
    <div class="raceinfo">Jan 01 - Mar 28 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Fairmount_Park"  title="Fairmount Park">
    <div class="racename">Fairmount Park</div>    
    <div class="raceinfo">Apr 27 - Aug 24 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Fairplex_Park"  title="Fairplex Park">
    <div class="racename">Fairplex Park</div>    
    <div class="raceinfo">Sep 10 - Sep 27 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Finger_Lakes_Race_Track"  title="Finger Lakes Race Track">
    <div class="racename">Finger Lakes Race Track</div>    
    <div class="raceinfo">Apr 17 - Dec 05 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Fort_Erie_Race_Track"  title="Fort Erie Race Track">
    <div class="racename">Fort Erie Race Track</div>    
    <div class="raceinfo">May 01 - Oct 26 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Golden_Gate_Fields"  title="Golden Gate Fields">
    <div class="racename">Golden Gate Fields</div>    
    <div class="raceinfo">Jan 02 - Jun 13 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Gulfstream_Park"  title="Gulfstream Park">
    <div class="racename">Gulfstream Park</div>    
    <div class="raceinfo">Jan 03 - Apr 24 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Hastings_Park"  title="Hastings Park">
    <div class="racename">Hastings Park</div>    
    <div class="raceinfo">Apr 24 - Oct 17 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Hawthorne_Racecourse"  title="Hawthorne Racecourse">
    <div class="racename">Hawthorne Racecourse</div>    
    <div class="raceinfo">Feb 12 - Apr 25 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Hollywood_Park"  title="Hollywood Park">
    <div class="racename">Hollywood Park</div>    
    <div class="raceinfo">Apr 23 - Jul 18 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Hoosier_Park"  title="Hoosier Park">
    <div class="racename">Hoosier Park</div>    
    <div class="raceinfo">Jul 30 - Oct 24 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Indiana_Downs"  title="Indiana Downs">
    <div class="racename">Indiana Downs</div>    
    <div class="raceinfo">Apr 16 - Nov 09 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Keeneland"  title="Keeneland">
    <div class="racename">Keeneland</div>    
    <div class="raceinfo">Apr 02 - Apr 23 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Laurel_Park"  title="Laurel Park">
    <div class="racename">Laurel Park</div>    
    <div class="raceinfo">Jan 01 - Apr 10 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Lone_Star_Park"  title="Lone Star Park">
    <div class="racename">Lone Star Park</div>    
    <div class="raceinfo">Apr 08 - Jul 18 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Los_Alamitos"  title="Los Alamitos">
    <div class="racename">Los Alamitos</div>    
    <div class="raceinfo">Jan 01 - Dec 19 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Louisiana_Downs"  title="Louisiana Downs">
    <div class="racename">Louisiana Downs</div>    
    <div class="raceinfo">May 29 - Oct 17 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Meadowlands_Racetrack"  title="Meadowlands Racetrack">
    <div class="racename">Meadowlands Racetrack</div>    
    <div class="raceinfo">Jan 01 - Aug 21 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Monmouth_Park"  title="Monmouth Park">
    <div class="racename">Monmouth Park</div>    
    <div class="raceinfo">May 08 - Sep 26 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Mountaineer_Race_Track_&_Gaming_Resort"  title="Mountaineer Race Track & Gaming Resort">
    <div class="racename">Mountaineer Race Track & Gaming Resort</div>    
    <div class="raceinfo">Mar 01 - Dec 21 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Oak_Tree_@_Santa_Anita"  title="Oak Tree @ Santa Anita">
    <div class="racename">Oak Tree @ Santa Anita</div>    
    <div class="raceinfo">Sep 29 - Oct 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Oaklawn_Park"  title="Oaklawn Park">
    <div class="racename">Oaklawn Park</div>    
    <div class="raceinfo">Jan 15 - Apr 10 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Penn_National"  title="Penn National">
    <div class="racename">Penn National</div>    
    <div class="raceinfo">Jan 01 - Dec 30 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Philadelphia_Park"  title="Philadelphia Park">
    <div class="racename">Philadelphia Park</div>    
    <div class="raceinfo">Jan 01 - Dec 28 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Pimlico_Race_Course"  title="Pimlico Race Course">
    <div class="racename">Pimlico Race Course</div>    
    <div class="raceinfo">Apr 17 - May 22 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Pleasanton"  title="Pleasanton">
    <div class="racename">Pleasanton</div>    
    <div class="raceinfo">Jul 01 - Jul 19 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Portland_Meadows"  title="Portland Meadows">
    <div class="racename">Portland Meadows</div>    
    <div class="raceinfo">Jan 05 - May 01 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Prairie_Meadows"  title="Prairie Meadows">
    <div class="racename">Prairie Meadows</div>    
    <div class="raceinfo">Apr 23 - Oct 22 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Remington_Park"  title="Remington Park">
    <div class="racename">Remington Park</div>    
    <div class="raceinfo">Mar 05 - May 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Retama_Park"  title="Retama Park">
    <div class="racename">Retama Park</div>    
    <div class="raceinfo">Aug 27 - Nov 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=River_Downs"  title="River Downs">
    <div class="racename">River Downs</div>    
    <div class="raceinfo">Apr 16 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Ruidoso_Downs"  title="Ruidoso Downs">
    <div class="racename">Ruidoso Downs</div>    
    <div class="raceinfo">May 28 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Sacramento"  title="Sacramento">
    <div class="racename">Sacramento</div>    
    <div class="raceinfo">Aug 26 - Sep 07 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Sam_Houston_Race_Park"  title="Sam Houston Race Park">
    <div class="racename">Sam Houston Race Park</div>    
    <div class="raceinfo">Jan 01 - May 03 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Santa_Anita_Park"  title="Santa Anita Park">
    <div class="racename">Santa Anita Park</div>    
    <div class="raceinfo">Jan 01 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Santa_Rosa"  title="Santa Rosa">
    <div class="racename">Santa Rosa</div>    
    <div class="raceinfo">Jul 29 - Aug 10 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Saratoga_Race_Course"  title="Saratoga Race Course">
    <div class="racename">Saratoga Race Course</div>    
    <div class="raceinfo">Jul 23 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Stockton"  title="Stockton">
    <div class="racename">Stockton</div>    
    <div class="raceinfo">Jun 18 - Jun 28 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Suffolk_Downs"  title="Suffolk Downs">
    <div class="racename">Suffolk Downs</div>    
    <div class="raceinfo">May 01 - Nov 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Sunland_Park"  title="Sunland Park">
    <div class="racename">Sunland Park</div>    
    <div class="raceinfo">Jan 01 - Dec 30 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Tampa_Bay_Downs"  title="Tampa Bay Downs">
    <div class="racename">Tampa Bay Downs</div>    
    <div class="raceinfo">Jan 01 - May 02 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Thistledown"  title="Thistledown">
    <div class="racename">Thistledown</div>    
    <div class="raceinfo">Apr 23 - Nov 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Turf_Paradise"  title="Turf Paradise">
    <div class="racename">Turf Paradise</div>    
    <div class="raceinfo">Jan 01 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Turfway_Park"  title="Turfway Park">
    <div class="racename">Turfway Park</div>    
    <div class="raceinfo">Jan 01 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Will_Rogers_Downs"  title="Will Rogers Downs">
    <div class="racename">Will Rogers Downs</div>    
    <div class="raceinfo">Mar 06 - May 15 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Woodbine_Racetrack"  title="Woodbine Racetrack">
    <div class="racename">Woodbine Racetrack</div>    
    <div class="raceinfo">Apr 02 - Dec 05 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Woodbine_Thoroughbred"  title="Woodbine Thoroughbred">
    <div class="racename">Woodbine Thoroughbred</div>    
    <div class="raceinfo">Jan 01 - Dec 29 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr>    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Yavapai_Downs"  title="Yavapai Downs">
    <div class="racename">Yavapai Downs</div>    
    <div class="raceinfo">May 29 - Sep 07 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row" >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Zia_Park"  title="Zia Park">
    <div class="racename">Zia Park</div>    
    <div class="raceinfo">Sep 11 - Dec 07 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
    
</table>

</div>
  </div>

  

<!-- /block-inner, /block -->



<div id="block-block-36" class="block block-block region-even even region-count-2 count-2">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    
<h2>Harness Racetracks</h2>

<div style="margin-top: 10px;" class="harness-racetracks">
  							  <table class="racetrack-menu" width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Balmoral_Park"  title="Balmoral Park">
    <div class="racename">Balmoral Park</div>    
    <div class="raceinfo">Jan 27 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row">    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Cal_Expo"  title="Cal Expo">
    <div class="racename">Cal Expo</div>    
    <div class="raceinfo">Jan 26 - Jun 19 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Dover_Downs_Harness_Racing"  title="Dover Downs Harness Racing">
    <div class="racename">Dover Downs Harness Racing</div>    
    <div class="raceinfo">Apr 15 - Nov 01 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row">    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Indiana_Downs"  title="Indiana Downs">
    <div class="racename">Indiana Downs</div>    
    <div class="raceinfo">Apr 16 - Nov 09 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Maywood_Park"  title="Maywood Park">
    <div class="racename">Maywood Park</div>    
    <div class="raceinfo">Jan 28 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row">    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Meadowlands_Racetrack"  title="Meadowlands Racetrack">
    <div class="racename">Meadowlands Racetrack</div>    
    <div class="raceinfo">Jan 01 - Aug 21 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Mohawk_Racetrack"  title="Mohawk Racetrack">
    <div class="racename">Mohawk Racetrack</div>    
    <div class="raceinfo">Jan 01 - May 17 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row">    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Monticello_Raceway"  title="Monticello Raceway">
    <div class="racename">Monticello Raceway</div>    
    <div class="raceinfo">Jan 04 - Dec 31 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Northfield_Park"  title="Northfield Park">
    <div class="racename">Northfield Park</div>    
    <div class="raceinfo">Jan 01 - Dec 29 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr class="row">    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Saratoga_Race_Course"  title="Saratoga Race Course">
    <div class="racename">Saratoga Race Course</div>    
    <div class="raceinfo">Jul 23 - Sep 06 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
  <tr >    <td style="border:0;">
    <ul class="menu">
    <li class="leaf first">
    <a href="racetrack?name=Yonkers_Raceway"  title="Yonkers Raceway">
    <div class="racename">Yonkers Raceway</div>    
    <div class="raceinfo">Jan 08 - Dec 21 </div>
    </a>
    </li>
    </ul>
    </td>
  </tr>
    
</table>

</div>
  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            <div id="block-block-34" class="block block-block region-odd odd region-count-1 count-3">

  
  <div class="content">
<!-- --------------------- content starts here ---------------------- -->



    <div><a href="/join"><img src="/themes/images/banner-racebook.jpg" alt="Racebook" width="705" height="118" /></a></div>  </div>

  

</div><!-- /block-inner, /block -->



                                    
                                      
          
<div class="headline"><h1>Racetrack Temporary Example Page - Albuquerque Downs</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<!-- ======== STYLES NEEDED TO HIDE BLOCK ELEMENTS ================== -->

<!-- ============================================================== -->

<!-- ======== BOX WITH TITLE ======================================== -->

<h2>Albuquerque Downs</h2>
<div>
<!-- ========ENTER CONTENTS HERE======== -->

<div class="RoundBody RaceTracks">

		<div class="TrackInfo">
			<img alt="TrackLogo" class="TrackLogo" src="http://www.otbpicks.net/images/racetracks/talb.gif" style="border-width:0px;" />
			<p>
				<b>Albuquerque Downs</b><br />
				201 California St NE <br/>Albuquerque, New Mexico 87198<br/>USA
			</p>
			<p>
				(505)-266-5555<br />
				(505)-268-1670<br />
			</p>
			
			<hr />
			
			<p>
				<b>2009 Racing Dates: </b>2008-08-16 until 2008-11-16<br />
				<b>Number of Racing Days: </b>52<br />
			</p>
			
			<hr />
			
			
<img alt="yimg" src="http://l.yimg.com/a/i/us/we/52/30.gif"/><br />
<b>Current Conditions:</b><br />
Partly Cloudy, 46 F<br />
<br /><b>Forecast:</b><br />
Wed - Partly Cloudy. High: 46 Low: 29<br />
Thu - Sunny. High: 52 Low: 29<br />
<br />

		</div>
		
		
		
<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<th width="175px">Track Length:</th>
				<td>1 Mile, Oval</td>
			</tr>
			<tr>
				<th>Stretch Length:</th>
				<td>1,114 Feet</td>
			</tr>
			<tr>
				<th>Stretch Width:</th>
				<td></td>
			</tr>
			<tr>
				<th>Infield Capacity:</th>
				<td></td>
			</tr>
			<tr>
				<th>Clubhouse Capacity:</th>
				<td></td>
			</tr>
			<tr>
				<th>Grand Stand Capacity:</th>
				<td></td>
			</tr>
			<tr>
				<th>Parking Capacity:</th>
				<td></td>
			</tr>
			<tr>
				<th>Price General Admission:</th>
				<td></td>
			</tr>
			<tr>
				<th>Price Clubhouse:</th>
				<td></td>
			</tr>
			<tr>
				<th>Price Turf Club: </th>
				<td></td>
			</tr>
			</table>	
<br />


<p><h2>Albuquerque Downs History</h2>
			<p>The Albuquerque Downs Racetrack organizes thoroughbred and quarter horseracing meets in each spring and seventeen days of racing during the State Fair in September. The Downs that is featured with 310 slots for casino on eight thousands square feet of land, is a part of the New Mexico State Fair Grounds. However, the Albuquerque Downs Racetrack bears a mellow history of its existence.</p>

<p>In September of 1997, the New Mexico State Fair futurity was dedicated with a special marker at the Downs. The New Mexico State Fair futurity was the oldest American Quarter Horse stakes race started in 1946 and thenceforth has been taking place without any discontinuation. At that time, this race was participated by Mexico-bred American Quarter Horses.</p>

<p>The Fair Commission was planning to keep all actions of the Albuquerque Downs Racetrack out of the competitive bid due to legal barriers in 2003. The State Governor issued a writ to put all such possibilities away and entered into a lease for up to 25 years. In 2004, the Downs took initiative for rebuilding the horse’ barn at the Fairgrounds.</p>
</p>
		
<p><h2>More About Albuquerque Downs</h2>
		<p>Grandstand of the Downs at Albuquerque is exquisitely structured with all the add-on luxurious amenities for extracting real entertainment from horseracing. Over 500 television sets installed in different parts of the grandstand help to get pleasant view from all corners. The Jockey Club, Weekly Specials and Bus Party are some of the attractive areas of the Albuquerque racetrack. At Casino, there are many gaming options available for visitors.</p>
</p>

			
			
<p><h2>How to get there:</h2>
			<p>The Downs is located on the Expo New Mexico State Fairgrounds, between Lomas and Central on the north and south, and San Pedro and Lousiana on the west and east. Enter through Gate 3, which is at the intersection of San Pedro and Copper, at a traffic light.</p>
			
			<div style="margin-top: 1em;">
				<iframe id="ctl00_Column1Content_ctl00_ifGoogleMap" height="300" width="720px" scrolling="no" allowtransparency="true" frameborder="0" src="/map.aspx?a=201 California St NE  Albuquerque, New Mexico 87198 USA"></iframe>
</p>
			</div>
	</div>

<!-- ========END CONTENT ================ -->
</div>


<!-- ======== END BOX WITH TITLE =================================== -->


        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    