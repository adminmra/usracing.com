<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-12">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 
<h1>Horse Racing Industry Resources</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->



<p>US Racing endorses the following organizations and companies involved with the North American and International horse racing industry.  Each of these groups provide critical services and products to both industry participants as well as thoroughbred racing fans.</p>

<p>The following organizations and companies either provide services to US Racing and/or are a source of horse racing information that US Racing feels is valuable to our members and fans of horse racing.</p> 

<p>We encourage you to visit these groups to learn more about the horse racing industry.</p>  

<p>If you would like your website to be included on our Horse Racing Industry links page, please email us cs (@) usracing.com.</p>


<div class="rowImg">
<hr />

<div class="row">
<div class="col-md-2"><img alt="ntra" src="/img/resources/ntra.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>NTRA</h3>
<p>Founded in 1998, the National Thoroughbred Racing Association (NTRA) is a membership-based trade association for the Thoroughbred racing and breeding industry. The NTRA is served by a Board of Directors that includes the NTRA President and CEO, along with representatives from member racetracks, horsemen's groups, owners and breeders.</p>
<p><a href="http://www.ntra.com" target="_blank" rel="nofollow">www.ntra.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="equibase" src="/img/resources/equibase.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Equibase</h3>
<p>Equibase Company LLC is a general partnership of subsidiaries of the TRA (Thoroughbred Racing Associations of North America) and The Jockey Club. The Company was formed in 1990 to provide the Thoroughbred racetracks of North America with a uniform, industry-owned database of racing information and statistics. In 1998, Equibase became the sole data collection agency and provider of racing data to the Daily Racing Form.  Equibase will continue to leverage information to serve the fan base and help promote the sport of horse racing, regardless of the medium through which fans access information.</p>
<p><a href="http://www.equibase.com/" target="_blank" rel="nofollow">www.equibase.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="bloodhorse" src="/img/resources/bloodhorse.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Bloodhorse</h3>
<p>Founded in 1916, the BloodHorse has blossomed into the Thoroughbred industry's premier international weekly news and information magazine and web site for racing and breeding. Coverage includes race reporting, comprehensive analysis, events, trends, debate, farm management, pedigrees, people, profiles, medication issues, investigative reports, breeding news and information, and anything newsworthy and important to the racing and breeding industry. </p>
<p><a href="http://www.bloodhorse.com/" target="_blank" rel="nofollow">www.bloodhorse.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="tra" src="/img/resources/tra.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>TRA</h3>
<p>The Thoroughbred Racing Associations (TRA) was formed in 1942 when World War II threatened to force a halt to all racing in the country. Realizing the Thoroughbred industry needed guidance and a plan to continue some racing to ensure the continuation of Thoroughbred breeding, Alfred G. Vanderbilt, then president of Pimlico and Belmont Park, planted the seed to form a commission of racetracks. The first conference held in Chicago on March 19, 1942, attracted a who's who of racing at the time, including Jockey Club chairman William Woodward, prominent breeder A.B. Hancock, and track presidents Henry Parr III and Carleton F. Burke, among others. From that seed, the TRA grew. In 2011, the TRA has 48 member associations conducting racing at 41 racetracks in the United States and Canada.</p>

<p><a href="http://www.tra-online.com" title="TRA" target="_blank" rel="nofollow">www.tra-online.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="usta" src="/img/resources/usta.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>USTA</h3>
<p>Founded in 1939, the U.S. Trotting Association is a not-for-profit association of Standardbred owners, breeders, drivers, trainers, and officials, organized to provide administrative, rulemaking, licensing and breed registry services to its members.</p>
<p><a href="http://www.ustrotting.com/" target="_blank" rel="nofollow">www.ustrotting.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="jockeyclub" src="/img/resources/jockeyclub.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Jockey Club</h3>
<p>Since 1894, The Jockey Club has pursued its mission as an organization dedicated to the improvement of Thoroughbred breeding and racing, and in the course of the ensuing 100-plus years it has earned widespread recognition as an industry leader for its active involvement in vital industry issues as well as its proficiency as a technology solutions and information provider.</p>
<p><a href="http://www.jockeyclub.com/" target="_blank" rel="nofollow">www.jockeyclub.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="hbpa" src="/img/resources/hbpa.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>HBPA</h3>
<p>Founded in 1940, a group of committed horsemen from New England brought into existence what is now known as the Horsemen's Benevolent and Protective Association.  Today there are over 30 thousand (30,000) owner and trainer members throughout the United States and Canada focused on a common goal -- the betterment of racing on all levels. </p>
<p><a href="http://www.hbpa.org/" target="_blank" rel="nofollow">www.hbpa.org</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="horseracingnation" src="/img/resources/horseracingnation.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Horse Racing Nation</h3>
<p>Horse Racing Nation was developed as a free, wiki community focused on racing information, insights and discussion for passionate horse racing fans. The "Nation" for the first time, provides a place where racing fans from around the world can share their love of horse racing with their fellow community members. The information, stories, photos, videos and comments on Horse Racing Nation are all provided by horse racing fans and organized for logical and easy access.</p>
<p><a href="http://www.horseracingnation.com/" target="_blank" rel="nofollow">www.horseracingnation.com</a></p>
</div>
</div><!-- end/row -->


<hr />

<div class="row">
<div class="col-md-2"><img alt="horseracingradionetwork" src="/img/resources/horseracingradionetwork.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Horse Racing Radio Network</h3>
<p>The Horse Racing Radio Network is your home for Eclipse Award-winning coverage of racing's biggest events throughout the year. In 2012, HRRN broadcast more than 90 stakes races from racetracks across the country and was the exclusive radio partner of the Kentucky Derby and Preakness Stakes. HRRN brings you live on-site coverage of the biggest Kentucky Derby prep races, as well as the Haskell Invitational, Arlington Million, Travers, Jockey Club Gold Cup, Belmont Stakes and Breeders' Cup World Championships.</p>
<p><a href="http://horseracingradio.net/" target="_blank" rel="nofollow">horseracingradio.net</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="derbywars" src="/img/resources/derbywars.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>DerbyWars</h3>
<p>Derby Wars is a revolutionary horse racing game combining skilled tournament play with a social game experience to appeal to both tournament veterans and casual fans.  Residents of the United States and Canada are eligible to participate in Derby Wars tournaments except for those residing in Arizona, Iowa, Louisiana, Montana, Tennessee, Vermont and Washington. Players must be at least 18 years old except for in the states of Alabama, Missouri and Nebraska where the players must be 21 years old.</p>
<p><a href="https://www.derbywars.com/" target="_blank" rel="nofollow">www.derbywars.com</a></p>
</div>
</div><!-- end/row -->


<hr />

<div class="row">
<div class="col-md-2"><img alt="timeformus" src="/img/resources/timeformus.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Time Form US</h3>
<p>TimeformUS is a new kind of horse racing Past Performances, optimized for tablets and PCs.  TimeformUS is focused on helping the horseplayer understand races faster.</p>
<p><a href="https://timeformus.com/" target="_blank" rel="nofollow">timeformus.com</a></p>
</div>
</div><!-- end/row -->



<hr />

<div class="row">
<div class="col-md-2"><img alt="thorobid" src="/img/resources/thorobid.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>ThoroBid</h3>
<p>ThoroBid explores the behavior of the betting public, which has proven to be a highly reliable predictor of race outcomes. Other content providers focus on speed, past performances and lineage. Thorobid is a completely new approach, meant to supplement existing resources and provide a fun, informative place for you to handicap with the ThoroBid community.</p>
<p><a href="https://thorobid.com/" target="_blank" rel="nofollow">horobid.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="horseracesnow" src="/img/resources/horseracesnow.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Horse Races Now!</h3>
<p>After watching a television show on MSNBC that foretold the future importance of smartphone applications entitled "Planet of the Apps", Kenny McPeek decided to see what kind of horse racing apps were available. After a search turned up nothing, the idea to create Horse Races Now was born. The app's mission is to increase the shrinking fan base of horse racing by creating an easy-to-use mobile application that allows the user to follow their favorites through push notification reminders. Kenny and Sue McPeek are determined to provide a full compliment of live racing feeds, race replays, current news and handicapping tools. Their belief is that by allowing both existing and potential fans easy access to watch horse racing whenever and wherever they go, the industry as a whole will benefit. Their personal investment in this project is their way to "give back" to a sport they love and support. </p>
<p><a href="http://www.horseracesnow.com/" target="_blank" rel="nofollow">www.horseracesnow.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="americasbestracing" src="/img/resources/americasbestracing.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>America's Best Racing</h3>
<p>America's Best Racing is a multi-media new fan development and awareness-building platform, initiated by The Jockey Club, designed to increase the profile and visibility of North America's best Thoroughbred racing events, with a primary focus on the sport's lifestyle and competition.</p>
<p><a href="http://www.americasbestracing.net" target="_blank" rel="nofollow">www.americasbestracing.net</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="rtip" src="/img/resources/rtip.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Race Track Industry Program</h3>
<p>The Race Track Industry Program (RTIP) at the University of Arizona is unique in the world. RTIP offers two paths based on student interest. The Business Path prepares students for employment in the areas of race track management, regulation and pari-mutuel racing organizations. The Equine Management Path prepares students for employment in areas dealing with racing and breeding animals.  The mission of the RTIP is to offer students a broad-based university education with emphasis on the pari-mutuel racing industry and provide support for the industry through a variety of outreach programs.</p>
<p><a href="http://www.ua-rtip.org" target="_blank" rel="nofollow">www.ua-rtip.org</a></p>
</p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="paulickreport" src="/img/resources/paulickreport.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Paulick Report</h3>
<p>PaulickReport.com covers all thoroughbred horse racing and thoroughbred industry news, including editorials by ESPN contributor and former Bloodhorse editor Ray Paulick. Paulick Report contains daily reviews of thoroughbred racing, thoroughbred breeding, thoroughbred business, and thoroughbred people, including thoroughbred farms, owners, trainers, jockeys, and personalities. Paulick Report looks at gaming and gambling issues facing the thoroughbred industry, the performance of the governing boards such as The Jockey Club, NRTA, Churchill Downs, the Breeders' Cup, as well as the news around major thoroughbred sales organizations such as Keeneland, Saratoga, Tattersalls, and Ocala. Ray Paulick was the 2013 recipient of the Stan Bergstein Writing Award and is widely respected for his integrity and his thoughtful and thorough coverage of the horse racing industry.</p>

<p><a href="http://www.paulickreport.com" target="_blank" rel="nofollow">www.paulickreport.com</a></p>
</div>
</div><!-- end/row -->



<hr />

<div class="row">
<div class="col-md-2"><img alt="drf" src="/img/resources/drf.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>DRF</h3>
<p>Daily Racing Form, "America's Turf Authority since 1894" for Thoroughbred racing horseplayers and professionals throughout North America, is the Thoroughbred industry's dominant multi-channel media company. Launched in Chicago on November 17, 1894, Daily Racing Form is the only daily newspaper in the U.S. dedicated solely to the coverage of a single sport, publishing up to 2,000 unique pages of statistical and editorial copy every day, in as many as 30 daily editions, 364 days a year (with the exception of Christmas Day). Its companion website, <a href="http://www.drf.com" target="_blank" rel="nofollow">www.drf.com</a>, is the most heavily trafficked horseracing destination, providing players with interactive past performances and exclusive handicapping and wagering tools.</p>
<p><a href="http://www.drf.com/" target="_blank" rel="nofollow">www.drf.com</a></p>
</div>
</div><!-- end/row -->

<hr />

<div class="row">
<div class="col-md-2"><img alt="aws" src="/img/resources/aws.gif" class="img-responsive" width="100%" /></div>
<div class="col-md-10">
<h3>Amazon Web Services</h3>
<p>Amazon Web Services is a collection of remote computing services that together make up a cloud computing platform, offered over the Internet by Amazon.com. The most central and well-known of these services are Amazon EC2 and Amazon S3.  US Racing is the first horse racing gaming company to use Amazon  Web Services.</p>
<p><a href="http://aws.amazon.com/" target="_blank" rel="nofollow">aws.amazon.com</a></p>
</div>
</div><!-- end/row -->

</div><!-- end/rowImg -->





<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



 
</div><!-- end/row -->
</div><!-- end/container -->
