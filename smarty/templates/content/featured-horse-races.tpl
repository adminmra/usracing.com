{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
            
{include file='menus/horsebetting.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

<div class="headline"><h1>Featured Horse Races</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<h2>Horse Race of the Day</h2>

<p>US Racing offers members the opportunity to double their winnings up to $25 on a horse racing wager every day of the week with our special Horse Race of the Day promotion.</p>

<h2>Horse Race of the Week</h2>

<p>Every Sunday horse bettors have the chance to double your winnings on a horse racing wager with our Horse Race of the Week.Place a “To Win” wager on a horse with 2:1 or greater odds in our weekly Race of the Week and we’ll double your winnings up to $100 if that horse wins.</p>

  


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



