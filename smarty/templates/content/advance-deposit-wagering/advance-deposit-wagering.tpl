
      
      
            <!-- begin: #col2 second float column -->
      <div id="col2">
        
      <!-- end: #col2 -->
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>ADWs - Advance Deposit Wagering</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p><strong>&nbsp;</strong></p>
<p><strong>Advance Deposit Wagering - ADWs</strong></p>
<p>Advance Deposit Wagering, also known as "ADWs" and "Account Wagering" include online facilities that accept wagers on horse races once the bettor's account has been funded-- ADWs should not be confused with "Credit Shops" which allows the bettor to place wagers on credit and then settle at the end of the month.</p>
<p>US Racing does not extend credit to its members-- all accounts must be funded before members can begin betting for real money on live horse racing.</p>
<p><strong>4 state tracks reach deal on advance-deposit bets</strong></p>
<dl class="byline">
<dd><span class="story">By Neil Milbert</span> | <span class="story">Tribune reporter</span>&nbsp; </dd>
<dd>7:54 PM CST, January 31, 2008</dd>
</dl>
<p style="padding-left: 60px;">Balmoral Park, Maywood Park, Hawthorne Race Course and Fairmount Park in Collinsville, Ill., have entered into an agreement with Youbet.com to offer advance-deposit wagering and online betting.</p>
<p>"We're planning a spring launch," Balmoral President John Johnston said Thursday. "Advance wagering has been going on in Illinois for years. We've been sticking our heads in the sand."</p>
<p>The agreement calls for the betting revenue to be split among between the tracks, the horsemen and Youbetillinois.com, and the state will receive its cut just as it does on in-state and out-of-state simulcasts to off-track betting parlors.</p>
<p style="padding-left: 60px;">In November, a subsidiary of Arlington Park's parent company Churchill Downs Inc. called TwinSpires.com, bought Winticket.com, BrisBET.com and Tsn.BET.com for $80 million, took over their advance accounts and started accepting wagers in Illinois.</p>
<p>Under the TwinSpires format, the horsemen's share is subject to collective bargaining.</p>
<p>"We have to go forward to compete," said Jim Hannon, director of simulcasting at Balmoral and Maywood. "Advance-deposit operators have been doing business in Illinois since 1999. We estimate close to $100 million left the state in 2007.</p>
<p>"Our position is that it's legal in Illinois. However, it needs to be clarified through legislation so the state, tracks and horsemen get their fair share and so that it can be regulated by the Illinois Racing Board. Right now it's going on and the state, tracks and horsemen aren't getting anything. We have to compete because other people are doing it, but it's not the savior of racing."</p>
<p>Among the Youbet board of directors are two high-profile Illinois residents, former Gov. Jim Edgar and Jay Pritzker, chairman of the Chicago Olympic Committee and a member of Northwestern's board of trustees.</p>
<p>Edgar's political connections may come in handy because in 2001 former Illinois Atty. Gen. Jim Ryan issued a 21-page opinion stating that account wagering violated state law. However, out-of-state entities ignored the opinion and continued to accept telephone and Internet bets.</p>
<p>"We're a gray state," said Illinois Racing Board Executive Director Marc Laino when asked about the statutory legality of the Youbet and TwinSpires ventures. "There's nothing expressly prohibiting it, and there's nothing authorizing it."</p>
<p>&nbsp;</p>
        
        

  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    