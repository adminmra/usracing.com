{literal}
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite", 
  "name" : "US Racing",
  "url" : "https://www.usracing.com/",
  "potentialAction" : {
    "@type" : "SearchAction",
    "target" : "https://www.usracing.com/?s={search_term}",
    "query-input" : "required name=search_term"
  }                     
}
</script>
<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "legalName" : "US Racing",
  "url" : "https://www.usracing.com/",
  "logo" : "https://www.usracing.com/img/usracing-betonhorses.gif",
  "sameAs" : [ "https://www.facebook.com/usracingtoday",
    "https://twitter.com/usracingtoday",
    "https://www.instagram.com/usracingtoday/"]
}
</script>
{/literal}

{****************************************KENTUCKY DERBY*******************************************************}

{if $KD_Switch_Early} {*One month out*}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-derby-early.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  

{elseif $KD_Switch_Main || $KD_Switch_Race_Day} {*Two weeks out*}
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-slider-kentucky-derby-main.tpl"}	
	{include file="/home/ah/allhorse/public_html/kd/schema.tpl"}		
	{include file="/home/ah/allhorse/public_html/kd/block-countdown-new.tpl"}													
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-main-kentucky-derby.tpl"}  				
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/blue-signup-bonus-kentucky-derby.tpl"} 			
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-free-bet.tpl"}								
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-trainers.tpl"}								
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-jockeys.tpl"}								
	{include file="/home/ah/allhorse/public_html/usracing/block_kentucky-derby/block-final-cta.tpl"}					



{****************************************PREAKNESS STAKES*******************************************************}

{elseif $PS_Switch_Early}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-slider-preakness-stakes.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  

{elseif $PS_Switch_Main || $PS_Switch_Race_Day}
{*
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-slider-preakness-stakes.tpl"} 
	{include file="/home/ah/allhorse/public_html/ps/block-countdown-new.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/main-ps.tpl"}  
	{include file="/home/ah/allhorse/public_html/ps/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/ps/block-triple-crown.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/blue-signup-bonus-ps.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/ps/block-exciting.tpl"}	
*}
	
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-slider-preakness-stakes.tpl"} 
	{include file="/home/ah/allhorse/public_html/ps/block-countdown-new.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-main-preakness-stakes.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-blue-signup-bonus-ps.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-triple-crown.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-testimonial-other.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_preakness/block-final-cta.tpl"}	
	
{****************************************BELMONT STAKES*******************************************************}

{elseif $BS_Switch_Early}{* Updates hero with BS Slider, nothing else.*}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-slider-belmont-stakes-bs-switch.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  


{elseif $BS_Switch_Main || $BS_Switch_Race_Day} {*Full BS Homepage Hard Sell Content*}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-slider-belmont-stakes.tpl"} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/belmont/block-countdown-new.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-main-belmont-stakes.tpl"}  
	{include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-props.tpl"} 
	{include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-blue-signup-bonus-belmont-stakes.tpl"} 
{* 	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-bet-trainers-belmont.tpl"}  *}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-final-cta.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-testimonial-belmont-stakes.tpl"}  
	{include file="/home/ah/allhorse/public_html/belmont/block-exciting.tpl"} 

	
{elseif $BS_Switch_Phase_1}{*Full BS Homepage*}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-slider-belmont-stakes.tpl"} 
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl" *}
	{include file="/home/ah/allhorse/public_html/belmont/block-countdown-new.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-main-belmont-stakes.tpl"}  
{* 	{include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"} *}
	{include file="/home/ah/allhorse/public_html/belmont/block-signup-bonus.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-props.tpl"} 
{* 	{include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"} *}
{* 	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-blue-signup-bonus-belmont-stakes.tpl"}  *}
{* 	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-bet-trainers-belmont.tpl"}  *}
{* 	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"}  *}
{* 	{include file="/home/ah/allhorse/public_html/usracing/block_belmont/block-testimonial-belmont-stakes.tpl"}  *}
	{include file="/home/ah/allhorse/public_html/belmont/block-exciting.tpl"} 

{****************************************BREEDERS CUP*******************************************************}


{elseif $BC_Switch_Early}
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-slider-breeders.tpl"} {*Expires Nov 3*}
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  
	

{elseif $BC_Switch_Main || $BC_Switch_Race_Day}
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-slider-breeders.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-main-breeders.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-blue-signup-bonus-breeders.tpl"}     
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-mobile-breeders.tpl"} 
	{*{include file="/home/ah/allhorse/public_html/usracing/block-bet-on-football.tpl"} *}  
	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-final-cta.tpl"}
	{*include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on.tpl"*}
	{*include file="/home/ah/allhorse/public_html/usracing/block-testimonial-breeders.tpl"*} {*Expires Nov 3 *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-countdown-breeders.tpl"*}  {*Expires Nov 3 *}    
	{*include file="/home/ah/allhorse/public_html/usracing/block-countdown-breeders.tpl"*} {*Belmont*} 
{* 	{include file="/home/ah/allhorse/public_html/usracing/block_breeders/block-main-breeders.tpl"}  *}
 {*Expires Nov 3 *}
	{*include file="/home/ah/allhorse/public_html/usracing/block-free-bet-breeders.tpl"*}  {*Expires Nov 3 *}
{*     {include file="/home/ah/allhorse/public_html/usracing/block-10-free-chips-breeders.tpl"} *}

{***********************************************************************************************}


{elseif $PWC_Switch} 
	{include file="/home/ah/allhorse/public_html/usracing/block-slider-pegasus.tpl"}
	{include file="/home/ah/allhorse/public_html/pegasus/block-countdown-new.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block-main-pegasus.tpl" }
	{include file="/home/ah/allhorse/public_html/usracing/block-blue-signup-bonus-pegasus.tpl"}    
	{* {include file="/home/ah/allhorse/public_html/usracing/block-mobile-pegasus.tpl"}  *}
	{* {include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}  *}
	{include file="/home/ah/allhorse/public_html/pegasus/block-specials.tpl"}
	{include file="/home/ah/allhorse/public_html/pegasus/block-testimonial-pace-advantage.tpl"}
	{* {include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} *}
	{include file="/home/ah/allhorse/public_html/pegasus/block-experts.tpl"}
	{include file="/home/ah/allhorse/public_html/pegasus/block-exciting.tpl"}
	{* {include file="/home/ah/allhorse/public_html/usracing/block-testimonial-pegasus.tpl" }  *}

{*
{elseif $SAUDI_Switch}
	{include file="/home/ah/allhorse/public_html/usracing/block-slider-saudi.tpl" } 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/common/block-rebates.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/common/block-mobile.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-racetracks.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/common/block-experts.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block-exciting.tpl"} 

*}

{*
{elseif $DWC_Switch_2PM} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/slider-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/main-dubai.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/rebates-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/blue-signup-bonus-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/mobile-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/racetracks-dubai.tpl"}
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/experts-dubai.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/block_dubai/exciting-dubai.tpl"}
		{include file="/home/ah/allhorse/public_html/usracing/block_dubai/testimonial-dubai.tpl"} 
*}

{else} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-slider.tpl"} 
	{include file="/home/ah/allhorse/public_html/usracing/index/block-main.tpl"}  
	{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  

{/if}

