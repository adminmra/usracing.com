{include file="/home/ah/allhorse/public_html/usracing/schema/harness-racing/australia.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }
       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }
       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }
     </style>
{/literal}
{*End*}
{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref1}" rel="nofollow">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the section for the odds tables*}
  <section class="kd usr-section" style="padding-bottom: 20px;">
    <div class="container">
      <div class="kd_content">
        <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		   <h3 class="kd_subheading">Best Australia Harness Off Track Betting Deals & Promotions</h3>
        {*The path for the main copy of the page*}
        <p>{include file='/home/ah/allhorse/public_html/racetracks/salesblurb-track-primary.tpl'} </p>
        {*end*}
        <p>Get your Australia Harness Horse Betting action Today! </p>
        {*The path for the year of the event*}
         <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
                    {*end*}
                {*The path for the odds table most like you will need to ask for the new table created and the btn whit the ref*}
           {*end*}
        {*Second Copy of the page*}
         <h2>Australia Harness Betting Notable Races</h2>
		  <table class="data table table-condensed table-striped table-bordered ordenable" width="100%" cellpadding="0"
        cellspacing="0" border="0" title="Kawasaki Racecourse Major Races"
        summary="Major races for Australia Harness. Only available at BUSR.">
        <caption>Australia Harness Notable Races</caption>
        <thead>
            <tr>
                <th> Month </th>
                <th> Race </th>
                <th> Class </th>
                <th> Track </th>
				<th> Group </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Fremantle Pacing Cup </td>
                <td data-title="Class" style="word-break: keep-all !important;"> Open </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Gloucester Park (WA) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
            </tr>
            <tr>
                <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> WA Pacing Cup </td>
                <td data-title="Class" style="word-break: keep-all !important;"> Grand Circuit </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Gloucester Park (WA) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
            </tr>
			  <tr>
               <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Downbytheseaside Victoria Derby Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
            </tr>
				  <tr>
                 <td data-title="Month">Jan</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pryde's Easifeed Victoria Oaks Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y f </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
            </tr>
				  <tr>
               <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Chariots of Fire</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 4y </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
				  <tr>
               <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> NSW Oaks Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y f </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Feb</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Ladyship Mile</td>
                <td data-title="Class" style="word-break: keep-all !important;"> Invitational (Mares) </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Ainsworth Miracle Mile</td>
                <td data-title="Class" style="word-break: keep-all !important;"> Grand Circuit </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Mar</td>
                <td data-title="Race" style="word-break: keep-all !important;"> NSW Derby Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y (c&g) </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Apr</td>
                <td data-title="Race" style="word-break: keep-all !important;"> WA Derby</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Gloucester Park (WA) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">May</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Australian Pacing Gold Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 2y (c&f) </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">May</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Australian Pacing Gold Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 2y f </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">May</td>
                <td data-title="Race" style="word-break: keep-all !important;"> The NSW $1 Million Pace</td>
                <td data-title="Class" style="word-break: keep-all !important;"> open </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> NSW Breeders Challenge Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y (c&g) </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
               <td data-title="Month">Jun</td>
                <td data-title="Race" style="word-break: keep-all !important;"> NSW Breeders Challenge Final</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 3y </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
                <td data-title="Month">Jul</td>
                <td data-title="Race" style="word-break: keep-all !important;"> TAB The Blacks A Fake Queensland Championship (Open M0+)</td>
                <td data-title="Class" style="word-break: keep-all !important;"> Grand Circuit </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Albion Park (QLD) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
                <td data-title="Month">Oct</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Pryde's Easifeed Victoria Cup (Open NR 90 to 120)</td>
                <td data-title="Class" style="word-break: keep-all !important;"> Grand Circuit </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Tabcorp Park (VIC) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
			<tr>
                 <td data-title="Month">Dec</td>
                <td data-title="Race" style="word-break: keep-all !important;"> Golden Nugget</td>
                <td data-title="Class" style="word-break: keep-all !important;"> 4y </td>
                <td data-title="Track" style="word-break: keep-all !important;"> Gloucester Park (WA) </td>
				 <td data-title="Group" style="word-break: keep-all !important;"> 1 </td>
             </tr>
        </tbody>
    </table>
            &nbsp;
        {*end*}
        <br>
        </li>
        </ul>
		   {*Secondary Copy*}
		  <h2>Harness Racing Australia - Off Track Betting</h2>
<p>Australia is one of the countries with the most packed harness racing calendars in the globe. Meetings take place all across the Aussie territory for most of the year.</p>
		  <h3>State Harness Racing Governing Bodies:</h3>
		  <li>Victoria: Harness Racing Victoria </li>
	  <li>New South Wales: Harness Racing New South Wales</li>
	  <li>Queensland: Racing Queensland</li>
	  <li>South Australia: Harness Racing South Australia</li>
	  <li>Western Australia: Racing & Wagering WA</li>
	  <li>Tasmania: Tasracing</li>
	  <h3>Harness Racing Australia: Group 1</h3>
<p>This is a yearly calendar that comes with enough quantity as well as quality. <p/>
<p>The top races taking place in the country are denominated Group Races, and the highest quality competitors are those that belong to Group 1.<p/>
<p>From a breeding point-of-view, to win a Group 1 race can make a horse extremely valuable.<p/>
<p>Harness racing differs from thoroughbred racing in that the winners of heats, and those finishing in designated positions from multiple meetings will earn a spot in the final.<p/>
<p>Additional to this, a lot of Group 1 feature series also include races where horses go against their own sex or age group. With the Breeders'Crown Finals at Tabcorp Park being a great example.</p>

<p><b>There are 4 Group 1 finals:</b><br>
<li>Two-year-old colts and geldings</li>
<li>Two-year-old fillies</li>
<li>Three-year-old colts and geldings</li>
<li>Three-year-old fillies</li></p>
<p>Get your Australia Harness Betting action now!</p>
		  <br>
		    <p><a href="/signup?ref={$ref2}" rel="nofollow"
            class="btn-xlrg fixed_cta">I Want To Bet Now</a></p>
        {*end*}
        <br>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}
  {* Block for the testimonial  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-testimonial-pace-advantage.tpl"}
{* End *}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-signup-bonus.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-150.tpl"}
{* End*}
{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/racetracks/block-REBATE.tpl"}
{* End*}
{* Block for the block exciting  and path *}
{include file="/home/ah/allhorse/public_html/racetracks/block-exciting.tpl"}
{* end *}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}