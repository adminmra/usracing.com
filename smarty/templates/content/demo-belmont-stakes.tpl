{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}

<p>{include_php file='/home/ah/allhorse/public_html/belmont/odds_88.php'}</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

<h2>The Final Jewel of the Triple Crown</h2>
<div class="justify-left"><p>The {$BELMONT_RUNNING} running of the Belmont Stakes will be held at Belmont Park on {include_php file='/home/ah/allhorse/public_html/belmont/racedate.php'} with first-race post just before noon ET. NBC will providing live coverage of the day’s events.</p>

<p> {*Unfortunately, with Nyquist's loss to Exaggerator at the Preakness Stakes, there will be no chance for a Triple Crown winner in 2016.  In fact, an elevated fever has forced Nyquist's connections to pull him from racing at the Belmont Stakes against Exaggerator which, of course, brings up the question of who will be able to beat Exaggerator?  The Belmont will be a fantastic race-- Triple Crown or not-- and you can bet on the Belmont on your mobile phone with US Racing.</p>

<p>The first Belmont in the United States was not the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a race horse that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby.*}</p>
<p><strong>The Belmont's Age</strong></p>
<p> One thing the Belmont does have over the Derby is that it is the oldest of the three Triple Crown events. The Belmont predates the Preakness by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings.</p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
{*include file='/home/ah/allhorse/public_html/belmont/links.tpl'*}
<p><span><strong>Some Monumental Belmont Moments</strong></span></p>

<p> In 1890, the Belmont was moved from Jerome Park to Morris Park, a mile and three-eighths track located a few miles east of what is now Van Cortlandt Park in the Bronx. The Belmont was held at Morris Park until Belmont Park's opening in 1905.</p>
<p><ul>
<li>Here's a tidbit you didn't see in Derby or Preakness history. When Grey Lag won the Belmont in 1921, it marked the first running of the Belmont Stakes in the counter-clockwise manner of American fashion. This 53rd running was a mile and three-eighths over the main course; previous editions at Belmont Park had been run clockwise, in accordance with English custom, over a fish-hook course which included part of the training track and the main dirt oval.</li>
<li>The first post parade in this country came in the 14th running of the Belmont in 1880. Until then the horses went directly from paddock to post.</li>
<li>The Belmont has been run at various distances. From 1867 tp 1873 it was 1 5/8 miles; from 1874 to 1889 it was 1 1/2 miles; from 1890 through 1892, and in 1895, it was held at 1 1/4 miles; from 1896 through 1925 it was 1 5/8 miles; since 1925 the Belmont Stakes has been a race of 1 1/2 miles.</li>
</ul></p>
<p><strong>Champion Sires</strong></p>

<p> As we saw in the breeding section of the Call To The Derby Post Betting How-To Page, champions horses breed champion horses. This certainly holds form in the Belmont Stakes. A total of eleven Belmont Stakes winners have sired at least one other Belmont winner.</p>
<p><ul>
<li>Man o' War heads the list of Belmont champion sires. Not only did he win the race himself in 1920, but three of his subsequent sires won it as well: American Flag in 1925, Crusader in 1926 and War Admiral in 1937, who went on to win the Triple Crown.</li>
<li>Commando won the 1901 running, then sired Peter Pan, the 1907 champ and the Colin, the 1908 winner.<br /> 1930 champion Gallant Fox sired both Omaha (1935) and Granville (1936).</li>
<li>Count Fleet won the 1943 edition, and then sired back-to-back Belmont winners with Counterpoint (1951) and One Count (1952).</li>
<li>1977 Triple Crown winner Seattle Slew sired a Call To The Derby Post favorite in Swale, who won both the Derby and the Belmont in 1984, as well as A.P. Indy, who won the Belmont in 1992. 1999 Belmont winner Lemon Drop Kid is also a descendant of the Slew.</li>
<li>The following horses have sired one Belmont winner each: Duke of Magenta of 1878 sired Eric (1889); Spendthrift of 1879 sired Hastings (1896); Hastings then followed his again by siring Masterman, the 1902 winner. The Finn of 1915 sired Zev (1923); Sword Dancer of 1959 sired Damascus (1967); last but not least, Triple Crown winner Secretariat of 1973 sired Risen Star, the 1988 winner.</li></ul></p>
<p><strong>Money at the Belmont</strong></p>
<p> Oh, have times changed. The purse for the first running of the Belmont was $1,500 added with a total purse of $2,500, with the winner's share taken by the filly Ruthless. The lowest winner's share in Belmont history was the $1,825 earned by The Finn in 1915. The Belmont set an opposite record in 1992, in which the richest Belmont purse ever totaled 1,764,800. Five times in Belmont history only two horses entered the race: 1887, 1888, 1892, 1910 and sadly, 1920, the year Man O'War triumphed. The largest field, on the other hand, was 15 in 1983, when Caveat defeated Slew O' Gold. In 1875 14 horses ran, when Calvin out-dueled stablemate Aristides, that year's winner of the inaugural Kentucky Derby. The Belmont's lowest paid winner: Count Fleet in 1943, who paid a paltry $2.10. The Belmont's highest winner: Sherluck in 1961, who dished out $132.10. A favorite's race: Of the 129 Belmont runnings through 1997, the favorite had won 58 times, including 9 out of the last 25. There have been some strange twists of betting in Belmont history. Since the advent of mutuels in New York in 1940 there have been six times when no place or show betting was taken on the Belmont Stakes. The last time there was no show wagering was in 1978 when Affirmed and Alydar held their famous confrontation. There was also no show betting when Secretariat won his Triple Crown in 1973; no wonder--Secretariat won by a record 31 lengths. Show betting was also eliminated in 1957 when Gallant Man defeated Bold Ruler, and also in 1953 when Native Dancer won. In 1943, believe it or not, there was no place or show wagering when Triple Crown winner Count Fleet went off $.05 to the dollar and won by 25 lengths. To wrap it up, Whirlaway completed his Triple Crown victory in 1941 without show betting. In other words, by the time horses dominate the Derby and Preakness, there just might not be that many challengers when the horse goes to complete the sweep. Since 1940 there have also been 30 horses listed as odds-on favorites in the Belmont Stakes. In 1957, there were two: Gallant Man, who won at 19-20, and Bold Ruler, who finished third at 17-20. Of these 30, only 12 went on to win. The highest on-track mutuel handle on the Belmont: 1993. A total of $2,793,320 was bet on the Belmont that year, with $1,409,970 wagered on win, place and show betting, and $1,293,954 on the daily double, exacta and triple.</p>
<p> <strong>The Fastest Belmont</strong></p>
<p> Who else?&nbsp;<a title="Secretariat" href="/famous-horses/secretariat">Secretariat</a>&nbsp;set a world-record that still stands for the mile and a half distance on a dirt track at 2:24. (He had finished a mile and a quarter at 1:59, faster than his own Derby record of 1:59 2/5.)</p>
<!--<p> <strong>Belmont Trophies</strong></p>--><!--hung-test-->
<p> <strong>Belmont Tropheis</strong></p>
<p>"The Belmont Stakes trophy is a Tiffany-made silver bowl, with cover, 18 inches high, 15 inches across and 14 inches at the base. Atop the cover is a silver figure of Fenian, winner of the third running of the Belmont Stakes in 1869. The bowl is supported by three horses representing the three foundation thoroughbreds--Eclipse, Herod and Matchem. The trophy, a solid silver bowl originally crafted by Tiffany's, was presented by the Belmont family as a perpetual award for the Belmont Stakes in 1926. It was the trophy August Belmont's Fenian won in 1869 and had remained with the Belmont family since that time. The winning owner is given the option of keeping the trophy for the year their horse reigns as Belmont champion."</p>
</div>
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>        

<!--hung-test-->
<a href="/famous-horses/secretarait">.</a>
<!--end-hung-test-->


{*Page Unique content ends here*******************************}
{*include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"*}


</div></div></section>   

<section class="bs-bonus"><div class="container"><div class="row"><div class="col-xs-12 col-sm-8 col-md-10 col-lg-8">
<h2 class="bs-bonus_heading">{$BONUS_WELCOME} Belmont Stakes Bonus</h2>
<h3 class="bs-bonus_subheading">Exceptional New Member Reward for the Belmont Stakes</h3>
<p>{* Use the promocode <strong>KENTUCKY</strong>  and receive*}Get up to {$BONUS_WELCOME} free! How does it work? You'll receive  a {$BONUS_WELCOME_PERCENTAGE} cash bonus  on your first deposit of $100 or more. The more you deposit, the bigger the bonus! {*  It doesn't get any better with BUSR. *}  Join today!</p><a href="/signup?ref={$ref}" rel="nofollow" class="bs-bonus_btn">Sign Me up!</a>
</div></div></div></section>


  <section class="bs-card">
      <div class="bs-card_wrap">
        <div class="bs-card_half bs-card_content"><img src="/img/index-bs/icon-belmont-stakes-betting.png" alt="Bet on Belmont Stakes" class="bs-card_icon">
          <h2 class="bs-card_heading">Belmont Stakes Bet</h2>
          <h3 class="bs-card_subheading">$10 FREE Belmont Stakes Bets for Members</h3>
          <p>Place a wager of $25 or more on the Belmont Stakes and you will get $10 in casino chips! </p>
          <p>You can only win with BUSR</i>!</p><a href="/signup?ref={*Free-*}Preakness-Stakes {*Bet*}" class="bs-card_btn">Join Now</a>
        </div>
        <div class="bs-card_half bs-card_hide-mobile">
          <a href="/signup?ref=Preakness-Stakes">
            <img src="" data-src-img="/img/index-bs/belmont-stakes-betting.jpg" alt="Belmont Stakes Betting" class="bs-card_img">
          </a>
        </div>
      </div>
    </section>


    {literal}
    <style>
		@media (min-width: 768px) and (max-width: 797px) {
			.fixed_cta {
                font-size: 17px;
            }
		}
        @media (min-width: 889px) and (max-width: 933px) {
			.fixed_cta {
                font-size: 22px;
            }
		}
        @media (min-width: 1024px) and (max-width: 1033px) {
            .fixed_cta {
                font-size: 23px;
            }
        }
	</style>
{/literal}

<section class="card">
  <div class="card_wrap">
    <div class="card_half card_hide-mobile">
      <a href="/belmont-stakes/props">
        <img src="" data-src-img="/img/index-kd/bet-kentucky-derby-jockeys.jpg" alt="Belmont Stakes Props"
          class="card_img"></a></div>
    <div class="card_half card_content"><a href="/belmont-stakes/props">
        <img src="/img/index-kd/icon-bet-kentucky-derby-jockeys.png" alt="Belmont  Props" class="card_icon"></a>
      <h2 class="card_heading">Belmont Stakes Props</h2>
      <p>You'll get great fixed odds with amazing payouts on the leading Belmont Stakes horses.</p>
      <p> Bet on the prop wagers such as margin of victory, head-to-head matchups and more.</p>
      <ahref="/belmont-stakes/props" class="btn-xlrg fixed_cta">See the Belmont Stakes Props</a>
    </div>
  </div>
</section>


  <section class="card card--red">
      <div class="container">
        <div class="card_wrap">
          <div class="card_half card_content card_content--red">
{*             <h2 class="card_heading card_heading--no-cap">Bet on the  Belmont Stakes  at BUSR!</h2> *}
               <h2 class="card_heading card_heading--no-cap">Bet on the  Belmont Stakes  Today!</h2>
            <a href="/signup?ref={$ref}" class="card_btn card_btn--default">Join Now</a>
          </div>
        </div>
      </div>
    </section>




      

    
