<div id="footer">
<div id="footer-inner" class="region region-footer">
 
<!-- /block-inner, /block -->

<div id="block-menu-menu-footer-links" class="block block-menu region-even odd region-count-2 count-7">


<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<ul class="menu">
<li><a href="/" title="Horse Racing" class="active">Horse Racing</a></li>
{*<li><a href="/otb" title="Off Track Betting">Off Track Betting</a></li>*}
<li><a href="/kentucky-derby" title="Kentucky Derby" class="active">Kentucky Derby</a></li>
<li><a href="/preakness-stakes" title="Preakness Stakes" class="active">Preakness Stakes</a></li>
<li><a href="/belmont-stakes" title="Belmont Stakes" class="active">Belmont Stakes</a></li>
<li><a href="/advance-deposit-wagering" title="Advance Deposit Wagering">Advance Deposit Wagering</a></li>
{* <li><a href="/responsibilities" title="Responsible Gaming">Responsible Gaming</a></li>
<li><a href="/privacy_policy" title="Privacy Policy">Privacy Policy</a></li> *}
<li><a href="/" title="Online Horse Racing" class="active">Online Horse Racing</a></li>
</ul>

<div class="bottomlinks primary">
<a href="/about" >About Us</a> &#149;  
<a href="/faqs" >FAQs</a> &#149;  
<a href="/terms">Terms and Conditions</a> &#149; 
<a href="/privacy">Privacy Policy</a> &#149; 
<a href="/responsiblegaming">Responsible Gambling</a> &#149;  
<a href="/support">Support</a>
<a href="/signup/">Join US Racing</a>
</div>

<div class="bottomlinks secondary">
<a href="/road-to-the-roses">Road to the Roses</a> &#149; 
<a href="/bet-on-horses" >Bet on Horses </a> &#149;  
<a href="/mobile">Mobile Horse Racing</a> &#149;  
<a href="/odds">Horse Racing Odds</a> &#149;  
<a href="/online-horse-wagering" >Online Horse Wagering</a> &#149; 
<a href="/online-horse-racing">Online Horse Racing</a> 
{*Iphone Horse Racing &#149; IPad Horse Racing &#149; Android Horse Betting &#149; Blackberry Horse Racing &#149; *}
</div>





</div>
<div class="logos">
<img src="/themes/images/usracing-poweredbytvg.png" alt="Online Horse Racing" title="US Racing - Online Horse Betting" />
<img alt="Footer logos" src="/themes/images/footer-logos.png" />
</div> 







 
</div>
</div><!-- /block-inner, /block -->

</div>
</div> <!-- /#footer-inner, /#footer -->

</div>
</div> <!-- /#page-inner, /#page -->
<div class="copyright">
	<div align="center">Copyright {'Y'|date}  <a href="/">US Racing</a>, All Rights Reserved</div>	
</div> 


 {literal} 
 <script type="text/javascript">
$(function() {
$(".slidetabs").tabs(".banner-images > div", 


{

// enable "cross-fading" effect
	effect: "fade",
	fadeInSpeed: "slow",	
	fadeOutSpeed: "fast",	

// start from the beginning after the last tab
	rotate: true

// use the slideshow plugin. It accepts its own configuration
}

).slideshow({
	autoplay: true, interval: 5000,
	clickable: false,
	autopause: false
	}); 


});



</script>      
  <script type="text/javascript">
<!--//--><![CDATA[//><!--
  $(function() {
      // setup ul.tabs to work as tabs for each div directly under div.panes
     $("#track-results-block ul.tabs").tabs("div.panes > div");
   });
   
//--><!]]>
</script>
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-742771-29', 'usracing.com');
  ga('send', 'pageview');

</script>
<!-- Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-Wt36A_TH_gV10"
});
</script>

<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-Wt36A_TH_gV10.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!-- End Quantcast tag -->

{/literal}
