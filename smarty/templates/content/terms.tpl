<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Terms and Conditions</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->  


<p class="h6">Last Modified: March 28, 2017</p>

<p>
    <strong>1. Acceptance of the Terms of Use </strong>
</p>
<p>
    Welcome to the website of US Racing (“Company”, “we” or “us”). The
    following terms and conditions, together with any documents they expressly
    incorporate by reference, (collectively, these “Terms of Use”), govern your
    access to and use of www.usracing.com and our Application, including any
content, functionality and services offered on or through www.usracing.com (collectively,
    the “Services”), whether as a guest or a registered user.
</p>
<p>
    Please read the Terms of Use carefully before you start to use the
    Services.
    <strong>
        By using the Services or by clicking to accept or agree to the Terms of
        Use when this option is made available to you, you accept and agree to
        be bound and abide by these Terms of Use and our Privacy Policy,
        incorporated herein by reference.
    </strong>
    If you do not want to agree to these Terms of Use or the Privacy Policy,
    you must not access or use the Services.
</p>
<p>
    The Services are offered and available to users who are 18 years of age or
    older and who reside in any jurisdiction where use of such services is
    permitted by law. By using the Services, you represent and warrant that you
    are of legal age to form a binding contract with the Company and meet all
    of the foregoing eligibility requirements. If you do not meet all of these
    requirements, you must not access or use the Services.
</p>
<p>
    <strong>2. Changes to the Terms of Use </strong>
</p>
<p>
    We may revise and update these Terms of Use from time to time in our sole
    discretion. All changes are effective immediately when we post them, and
    apply to all access to and use of the Services thereafter. However, any
    changes to the dispute resolution provisions set forth in Governing Law and
    Jurisdiction<em> </em>will not apply to any disputes for which the parties
    have actual notice on or prior to the date the change is posted on the
    Services.
</p>
<p>
    Your continued use of the Services following the posting of revised Terms
    of Use means that you accept and agree to the changes. You are expected to
    check this page each time you access the Services so you are aware of any
    changes, as they are binding on you.
</p>
<p>
    <strong>3. Accessing the Services and Account Security </strong>
</p>
<p>
    We reserve the right to withdraw or amend the Services, and any service or
    material we provide on the Services, in our sole discretion without notice.
    We will not be liable if for any reason all or any part of the Services is
    unavailable at any time or for any period. From time to time, we may
    restrict access to some parts of the Services, or the entire Services, to
    users, including registered users.
</p>
<p>
    You are responsible for:
</p>
<ul>
    <li>
        Making all arrangements necessary for you to have access to the
        Services.
    </li>
    <li>
        Ensuring that all persons who access the Services through your internet
        connection are aware of these Terms of Use and comply with them.
    </li>
</ul>
<p>
    To access the Services or some of the resources it offers, you may be asked
    to provide certain registration details or other information. It is a
    condition of your use of the Services that all the information you provide
    on the Services is correct, current and complete. You agree that all
    information you provide to register with the Services or otherwise,
    including but not limited to through the use of any interactive features on
    the Services, is governed by our Privacy Policy, and you consent to all
    actions we take with respect to your information consistent with our
    Privacy Policy.
</p>
<p>
    If you choose, or are provided with, a user name, password or any other
    piece of information as part of our security procedures, you must treat
    such information as confidential, and you must not disclose it to any other
    person or entity. You also acknowledge that your account is personal to you
    and agree not to provide any other person with access to the Services or
    portions of it using your user name, password or other security
    information. You agree to notify us immediately of any unauthorized access
    to or use of your user name or password or any other breach of security.
    You also agree to ensure that you exit from your account at the end of each
    session. You should use particular caution when accessing your account from
    a public or shared computer so that others are not able to view or record
    your password or other personal information.
</p>
<p>
    We have the right to disable any user name, password or other identifier,
    whether chosen by you or provided by us, at any time in our sole discretion
    for any or no reason, including if, in our opinion, you have violated any
    provision of these Terms of Use.
</p>
<p>
    <strong>4. Intellectual Property Rights </strong>
</p>
<p>
    The Services and its contents, features, and functionality (including but
    not limited to all information, software, text, displays, images, video and
    audio, and the design, selection and arrangement thereof), are owned by the
    Company, its licensors or other providers of such material and are
    protected by United States and international copyright, trademark, patent,
    trade secret and other intellectual property or proprietary rights laws.
</p>
<p>
    You must not reproduce, distribute, modify, create derivative works of,
    publicly display, publicly perform, republish, download, store or transmit
    any of our IP, except as follows:
</p>
<ul>
    <li>
        Your computer may temporarily store copies of such materials in RAM
        incidental to your accessing and viewing those materials.
    </li>
    <li>
        You may store files that are automatically cached by your Web browser
        for display enhancement purposes.
    </li>
    <li>
        You may print or download one copy of a reasonable number of pages of
        the Services for your own personal, non-commercial use and not for
        further reproduction, publication or distribution.
    </li>
    <li>
        If we provide desktop, mobile or other applications for download, you
        may download a single copy to your computer or mobile device solely for
        your own personal, noncommercial use, provided you agree to be bound by
        our end user license agreement for such applications.
    </li>
    <li>
        If we provide social media features with certain content, you make take
        such actions as are enabled by such features.
    </li>
</ul>
<p>
    You must not:
</p>
<ul>
    <li>
        Modify copies of any materials from this site.
    </li>
    <li>
        Use any illustrations, photographs, video or audio sequences or any
        graphics separately from the accompanying text.
    </li>
    <li>
        Delete or alter any copyright, trademark or other proprietary rights
        notices from copies of materials from this site.
    </li>
</ul>
<p>
    If you wish to make any use of our IP other than that set out in this
    section, please address your request to: cs@usracing.com
</p>
<p>
    If you print, copy, modify, download or otherwise use or provide any other
    person with access to any part of the Services in breach of the Terms of
    Use, your right to use the Services will cease immediately and you must, at
    our option, return or destroy any copies of the materials you have made. No
    right, title or interest in or to our IP is transferred to you, and all
    rights not expressly granted are reserved by the Company. Any use of the
    Services not expressly permitted by these Terms of Use is a breach of these
    Terms of Use and may violate copyright, trademark and other laws.
</p>
<p>
    <strong>5. Trademarks</strong>
</p>
<p>
    The Company name, the terms US Racing ™, the Company logo and all related
    names, logos, product and service names, designs and slogans are trademarks
    of the Company or its affiliates or licensors. You must not use such marks
    without the prior written permission of the Company. All other names,
    logos, product and service names, designs and slogans on the Services are
    the trademarks of their respective owners.
</p>
<p>
    US Racing provides free information, odds, facts and commentary about horse
    racing and betting, in general. Third party marks may be referenced in a
    transformative, editorial, informational, nominative, critical, analytical
    or comparative context. US Racing may reference marks belonging to third parties pursuant to our right to engage in fair use, fair comment, statutory fair use or trade mark fair use doctrine. As such, US Racing does not contribute to any dilution of any trade or service marks. US
    Racing provides this information in an effort to educate and grow the sport
    of thoroughbred racing in North America with an emphasis on attracting new
    fans of the sport.
</p>
<p>
    <strong>6. Prohibited Uses</strong>
</p>
<p>
    You may use the Services only for lawful purposes and in accordance with
    these Terms of Use. You agree not to use the Services:
</p>
<ul>
    <li>
        In any way that violates any applicable federal, state, local or
        international law or regulation.
    </li>
    <li>
        For the purpose of exploiting, harming or attempting to exploit or harm
        minors in any way by exposing them to inappropriate content, asking for
        personally identifiable information or otherwise.
    </li>
    <li>
        To send, knowingly receive, upload, download, use or re-use any
        material which does not comply with the Content Standards set out in
        these Terms of Use.
    </li>
    <li>
        To transmit, or procure the sending of, any advertising or promotional
        material without our prior written consent, including any “junk mail”,
        “chain letter” or “spam” or any other similar solicitation.
    </li>
    <li>
        To impersonate or attempt to impersonate the Company, a Company
        employee, another user or any other person or entity (including,
        without limitation, by using e-mail addresses or screen names
        associated with any of the foregoing).
    </li>
    <li>
        To engage in any other conduct that restricts or inhibits anyone’s use
        or enjoyment of the Services, or which, as determined by us, may harm
        the Company or users of the Services or expose them to liability.
    </li>
</ul>
<p>
    Additionally, you agree not to:
</p>
<ul>
    <li>
        Use the Services in any manner that could disable, overburden, damage,
        or impair the site or interfere with any other party’s use of the
        Services, including their ability to engage in real time activities
        through the Services.
    </li>
    <li>
        Use any robot, spider or other automatic device, process or means to
        access the Services for any purpose, including monitoring or copying
        any of the material on the Services.
    </li>
    <li>
        Use any manual process to monitor or copy any of the material on the
        Services or for any other unauthorized purpose without our prior
        written consent.
    </li>
    <li>
        Use any device, software or routine that interferes with the proper
        working of the Services.
    </li>
    <li>
        Introduce any viruses, trojan horses, worms, logic bombs or other
        material which is malicious or technologically harmful.
    </li>
    <li>
        Attempt to gain unauthorized access to, interfere with, damage or
        disrupt any parts of the Services, the server on which the Services is
        stored, or any server, computer or database connected to the Services.
    </li>
    <li>
        Attack the Services via a denial-of-service attack or a distributed
        denial-of-service attack. Otherwise attempt to interfere with the
        proper working of the Services.
    </li>
</ul>
<p>
    <strong></strong>
    <br/>
</p>
<p>
    <strong>7. User Contributions </strong>
</p>
<p>
    The Services may contain message boards, chat rooms, personal web pages or
    profiles, forums, bulletin boards news articles, social posting via
    facebook, twitter and other social networks, and other interactive features
    (collectively, “Interactive Services”) that allow users to post, submit,
    publish, display or transmit to other users or other persons (hereinafter,
    “post”) content or materials (collectively, “User Contributions”) on or
    through the Services.
</p>
<p>
    All User Contributions must comply with the Content Standards set out in
    these Terms of Use.
</p>
<p>
    Any User Contribution you post to the site will be considered
    non-confidential and non-proprietary. By providing any User Contribution on
    the Services, you grant us and our affiliates and service providers, and
    each of their and our respective licensees, successors and assigns the
    right to use, reproduce, modify, perform, display, distribute and otherwise
    disclose to third parties any such material for any purpose.
</p>
<p>
    You represent and warrant that:
</p>
<ul>
    <li>
        You own or control all rights in and to the User Contributions and have
        the right to grant the license granted above to us and our affiliates
        and service providers, and each of their and our respective licensees,
        successors and assigns.
    </li>
    <li>
        All of your User Contributions do and will comply with these Terms of
        Use.
    </li>
    <li>
        You understand and acknowledge that you are responsible for any User
        Contributions you submit or contribute, and you, not the Company, have
        fully responsibility for such content, including its legality,
        reliability, accuracy and appropriateness.
    </li>
    <li>
        We are not responsible, or liable to any third party, for the content
        or accuracy of any User Contributions posted by you or any other user
        of the Services.
    </li>
</ul>
<p>
    <strong></strong>
    <br/>
</p>
<p>
    <strong>8. Monitoring and Enforcement; Termination </strong>
</p>
<p>
    We have the right to:
</p>
<ul>
    <li>
        Remove or refuse to post any User Contributions for any or no reason in
        our sole discretion.
    </li>
    <li>
        Take any action with respect to any User Contribution that we deem
        necessary or appropriate in our sole discretion, including if we
        believe that such User Contribution violates the Terms of Use,
        including the Content Standards, infringes any intellectual property
        right or other right of any person or entity, threatens the personal
        safety of users of the Services or the public or could create liability
        for the Company.
    </li>
    <li>
        Disclose your identity or other information about you to any third
        party who claims that material posted by you violates their rights,
        including their intellectual property rights or their right to privacy.
    </li>
    <li>
        Take appropriate legal action, including without limitation, referral
        to law enforcement, for any illegal or unauthorized use of the
        Services.
    </li>
    <li>
        Terminate or suspend your access to all or part of the Services for any
        or no reason, including without limitation, any violation of these
        Terms of Use.
    </li>
</ul>
<p>
    Without limiting the foregoing, we have the right to fully cooperate with
    any law enforcement authorities or court order requesting or directing us
    to disclose the identity or other information of anyone posting any
    materials on or through the Services. YOU WAIVE AND HOLD HARMLESS THE
    COMPANY AND ITS AFFILIATES, LICENSEES AND SERVICE PROVIDERS FROM ANY CLAIMS
    RESULTING FROM ANY ACTION TAKEN BY THE COMPANY/ANY OF THE FOREGOING PARTIES
    DURING OR AS A RESULT OF ITS INVESTIGATIONS AND FROM ANY ACTIONS TAKEN AS A
    CONSEQUENCE OF INVESTIGATIONS BY EITHER THE COMPANY/SUCH PARTIES OR LAW
    ENFORCEMENT AUTHORITIES.
</p>
<p>
    However, we do not undertake to review all material before it is posted on
    the Services, and cannot ensure prompt removal of objectionable material
    after it has been posted. Accordingly, we assume no liability for any
    action or inaction regarding transmissions, communications or content
    provided by any user or third party. We have no liability or responsibility
    to anyone for performance or nonperformance of the activities described in
    this section.
</p>
<p>
    <strong>9. Content Standards</strong>
</p>
<p>
    These content standards apply to any and all User Contributions and use of
    Interactive Services.
</p>
<p>
    User Contributions must in their entirety comply with all applicable
    international, federal, state and local laws and regulations. Without
    limiting the foregoing, User Contributions must not:
</p>
<ul>
    <li>
        Contain any material which is defamatory, obscene, indecent, abusive,
        offensive, harassing, violent, hateful, inflammatory or otherwise
        objectionable.
    </li>
    <li>
        Promote sexually explicit or pornographic material, violence, or
        discrimination based on race, sex, religion, nationality, disability,
        sexual orientation or age.
    </li>
    <li>
        Infringe any patent, trademark, trade secret, copyright or other
        intellectual property or other rights of any other person.
    </li>
    <li>
        Violate the legal rights (including the rights of publicity and
        privacy) of others or contain any material that could give rise to any
        civil or criminal liability under applicable laws or regulations or
        that otherwise may be in conflict with these Terms of Use and our
        Privacy Policy.
    </li>
    <li>
        Be likely to deceive any person.
    </li>
    <li>
        Promote any illegal activity, or advocate, promote or assist any
        unlawful act.
    </li>
    <li>
        Impersonate any person, or misrepresent your identity or affiliation
        with any person or organization.
    </li>
    <li>
        Involve contests, sweepstakes and other sales promotions, barter or
        advertising.
    </li>
    <li>
        Give the impression that they emanate from or are endorsed by us or any
        other person or entity, if this is not the case.
    </li>
</ul>
<p>
    <strong></strong>
    <br/>
</p>
<p>
    <strong>10. Copyright Infringement </strong>
</p>
<p>
    If you believe that any User Contributions violate your copyright, please
    contact our copyright agent at cs@usracing.com. In accordance with the
    Digital Millennium Copyright Act (“DMCA”) and other applicable law, we have
    adopted a policy of terminating, in appropriate circumstances and at our
    sole discretion, subscribers or account holders who are deemed to be repeat
    infringers. We may also at our sole discretion limit access to our Web site
    and/or terminate the accounts of any users who infringe any intellectual
    property rights of others, whether or not there is any repeat infringement.
</p>
<p>
    US Racing provides free information, odds, facts and commentary about horse
    racing and betting, in general. Third party marks may be referenced in a
    transformative, editorial, informational, nominative, critical, analytical
    or comparative context. US Racing may reference marks belonging to third parties pursuant to our right to engage in fair use, fair comment, statutory fair use or trade mark fair use doctrine. As such, US Racing does not contribute to any dilution of any trade or service marks.</p>
<p>
    <strong>11. Reliance on Information Posted</strong>
</p>
<p>
    The information presented on or through the Services is made available
    solely for general information purposes. We do not warrant the accuracy,
    completeness or usefulness of this information. Any reliance you place on
    such information is strictly at your own risk. We disclaim all liability
    and responsibility arising from any reliance placed on such materials by
    you or any other visitor to the Services, or by anyone who may be informed
    of any of its contents.
</p>
<p>
    The Services may include content provided by third parties, including
    materials provided by other users. All statements and/or opinions expressed
    in these materials, and all articles and responses to questions and other
    content, other than the content provided by the Company, are solely the
    opinions and the responsibility of the person or entity providing those
    materials. These materials do not necessarily reflect the opinion of the
    Company. We are not responsible, or liable to you or any third party, for
    the content or accuracy of any materials provided by any third parties,
    including staff of US Racing who contribute the aforesaid services.
</p>
<p>
    <strong>12. Changes to the Services </strong>
</p>
<p>
    We may update the content on the Services from time to time, but its
    content is not necessarily complete or up-to-date. Any of the material on
    the Services may be out of date at any given time, and we are under no
    obligation to update such material.
</p>
<p>
    <strong>13. Information About You and Your Visits to the Services</strong>
</p>
<p>
    All information we collect on the Services is subject to our Privacy
    Policy. By using the Services, you consent to all actions taken by us with
    respect to your information in compliance with the Privacy Policy.
</p>
<p>
    <strong>14. Linking to the Services and Social Media Features </strong>
</p>
<p>
    You may link to our homepage, provided you do so in a way that is fair and
    legal and does not damage our reputation or take advantage of it, but you
    must not establish a link in such a way as to suggest any form of
    association, approval or endorsement on our part without our express
    written consent.
</p>
<p>
    The Services may provide certain social media features that enable you to:
</p>
<ul>
    <li>
        Link from your own or certain third-party websites to certain content
        on the Services.
    </li>
    <li>
        Send e-mails or other communications with certain content, or links to
        certain content, on the Services.
    </li>
    <li>
        Cause limited portions of content on the Services to be displayed or
        appear to be displayed on your own or certain third-party websites.
    </li>
</ul>
<p>
    You may use these features solely as they are provided by us, and solely
    with respect to the content they are displayed with and otherwise in
    accordance with any additional terms and conditions we provide with respect
    to such features. Subject to the foregoing, you must not:
</p>
<ul>
    <li>
        Establish a link from any website that is not owned by you.
    </li>
    <li>
        Cause the Services or portions of it to be displayed, or appear to be
        displayed by, for example, framing, deep linking or in-line linking, on
        any other site.
    </li>
    <li>
        Link to any part of the Services other than the homepage.
    </li>
    <li>
        Otherwise take any action with respect to the materials on the Services
        that is inconsistent with any other provision of these Terms of Use.
    </li>
</ul>
<p>
    The Services from which you are linking, or on which you make certain
    content accessible, must comply in all respects with the Content Standards
    set out in these Terms of Use.
</p>
<p>
    You agree to cooperate with us in causing any unauthorized framing or
    linking immediately to cease. We reserve the right to withdraw linking
    permission without notice.
</p>
<p>
    We may disable all or any social media features and any links at any time
    without notice in our discretion.
</p>
<p>
    <strong>15. Links from the Services</strong>
</p>
<p>
    If the Services contain links to other sites and resources provided by
    third parties, these links are provided for your convenience only. This
    includes links contained in advertisements, including banner advertisements
    and sponsored links. We have no control over the contents of those sites or
    resources, and accept no responsibility for them or for any loss or damage
    that may arise from your use of them. If you decide to access any of the
    third party websites linked to the Services, you do so entirely at your own
    risk and subject to the terms and conditions of use for such websites. US
    Racing reviews and recommends websites that provide horse betting and
    gambling products.
</p>
<p>
    <strong>16. Usage Agreement:</strong>
    <strong> </strong>
    Only persons who agree to these conditions may access and make use of this
    site.
</p>
<p>
    US Racing contains links and informational content on horse betting
    services. Horse betting is not legal in certain areas. Consult your local
    authorities prior to registering with any online betting site service.
</p>
<p>
    By playing or signing up under any of the links to racebooks posted on this
    website you assume full responsibility for your actions and will not hold
    US Racing or any associated or affiliated websites or the creators or
    employees responsible for any losses. Information on this site is collected
    from outside sources and/or is opinion and is offered "as is" without
    warranties of accuracy of any kind. Under no circumstances; under no cause
    of action or legal theory, shall the owners, creators, associates or
    employees of this website be liable to you or any other person or entity
    for any direct, indirect, special, incidental, or consequential damages of
    any kind whatsoever.<strong> </strong>This website does not constitute an
    offer, solicitation or invitation by US Racing for the use of or
    subscription to betting or other services in any jurisdiction in which such
    activities are prohibited by law. US Racing and the website usracing.com do
    not provide any gambling services or financial instruments which allow for
    the transactions of wagers over the internet.
    <strong>
        Only persons who agree to these conditions may access and make use of
        this site.
    </strong>
</p>
<p>
    <strong>17. Geographic Restrictions </strong>
</p>
<p>
    Access to the Services may not be legal by certain persons or in certain
    countries. If you access the Services, you do so on your own initiative and
    are responsible for compliance with your local laws.
</p>
<p>
    <strong>18. Disclaimer of Warranties</strong>
</p>
<p>
    You understand that we cannot and do not guarantee or warrant that files
    available for downloading from the internet or the Services will be free of
    viruses or other destructive code. You are responsible for implementing
    sufficient procedures and checkpoints to satisfy your particular
    requirements for anti-virus protection and accuracy of data input and
    output, and for maintaining a means external to our site for any
    reconstruction of any lost data. WE WILL NOT BE LIABLE FOR ANY LOSS OR
    DAMAGE CAUSED BY A DISTRIBUTED DENIAL-OF-SERVICE ATTACK, VIRUSES OR OTHER
    TECHNOLOGICALLY HARMFUL MATERIAL THAT MAY INFECT YOUR COMPUTER EQUIPMENT,
    COMPUTER PROGRAMS, DATA OR OTHER PROPRIETARY MATERIAL DUE TO YOUR USE OF
    THE SERVICES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES OR TO
    YOUR DOWNLOADING OF ANY MATERIAL POSTED ON IT, OR ON ANY SERVICES LINKED TO
    IT.
</p>
<p>
    YOUR USE OF THE SERVICES, ITS CONTENT AND ANY SERVICES OR ITEMS OBTAINED
    THROUGH THE SERVICES IS AT YOUR OWN RISK. THE SERVICES, ITS CONTENT AND ANY
    SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES ARE PROVIDED ON AN “AS IS”
    AND “AS AVAILABLE” BASIS, WITHOUT ANY WARRANTIES OF ANY KIND, EITHER
    EXPRESS OR IMPLIED. NEITHER THE COMPANY NOR ANY PERSON ASSOCIATED WITH THE
    COMPANY MAKES ANY WARRANTY OR REPRESENTATION WITH RESPECT TO THE
    COMPLETENESS, SECURITY, RELIABILITY, QUALITY, ACCURACY OR AVAILABILITY OF
    THE SERVICES. WITHOUT LIMITING THE FOREGOING, NEITHER THE COMPANY NOR
    ANYONE ASSOCIATED WITH THE COMPANY REPRESENTS OR WARRANTS THAT THE
    SERVICES, ITS CONTENT OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE
    SERVICES WILL BE ACCURATE, RELIABLE, ERROR-FREE OR UNINTERRUPTED, THAT
    DEFECTS WILL BE CORRECTED, THAT OUR SITE OR THE SERVER THAT MAKES IT
    AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS OR THAT THE
    SERVICES OR ANY SERVICES OR ITEMS OBTAINED THROUGH THE SERVICES WILL
    OTHERWISE MEET YOUR NEEDS OR EXPECTATIONS.
</p>
<p>
    THE COMPANY HEREBY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR
    IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO ANY
    WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR PARTICULAR
    PURPOSE.
</p>
<p>
    THE FOREGOING DOES NOT AFFECT ANY WARRANTIES WHICH CANNOT BE EXCLUDED OR
    LIMITED UNDER APPLICABLE LAW.
</p>
<p>
    <strong>19. Limitation on Liability </strong>
</p>
<p>
    IN NO EVENT WILL THE COMPANY, ITS AFFILIATES OR THEIR LICENSORS, SERVICE
    PROVIDERS, EMPLOYEES, AGENTS, OFFICERS OR DIRECTORS BE LIABLE FOR DAMAGES
    OF ANY KIND, UNDER ANY LEGAL THEORY, ARISING OUT OF OR IN CONNECTION WITH
    YOUR USE, OR INABILITY TO USE, THE SERVICES, ANY WEBSITE LINKED TO IT, ANY
    CONTENT ON THE SERVICES OR SUCH OTHER WEBSITE OR ANY SERVICES OR ITEMS
    OBTAINED THROUGH THE SERVICES OR SUCH OTHER WEBSITE, INCLUDING ANY DIRECT,
    INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING
    BUT NOT LIMITED TO, PERSONAL INJURY, PAIN AND SUFFERING, EMOTIONAL
    DISTRESS, LOSS OF REVENUE, LOSS OF PROFITS, LOSS OF BUSINESS OR ANTICIPATED
    SAVINGS, LOSS OF USE, LOSS OF GOODWILL, LOSS OF DATA, AND WHETHER CAUSED BY
    TORT (INCLUDING NEGLIGENCE), BREACH OF CONTRACT OR OTHERWISE, EVEN IF
    FORESEEABLE.
</p>
<p>
    THE FOREGOING DOES NOT AFFECT ANY LIABILITY WHICH CANNOT BE EXCLUDED OR
    LIMITED UNDER APPLICABLE LAW.
</p>
<p>
    <strong>20. Indemnification</strong>
</p>
<p>
    You agree to defend, indemnify and hold harmless the Company, its
    affiliates, licensors and service providers, and its and their respective
    officers, directors, employees, contractors, agents, licensors, suppliers,
    successors and assigns from and against any claims, liabilities, damages,
    judgments, awards, losses, costs, expenses or fees (including reasonable
    attorneys’ fees) arising out of or relating to your violation of these
    Terms of Use or your use of the Services, including, but not limited to,
    your User Contributions, any use of the Services’ content, services and
    products other than as expressly authorized in these Terms of Use or your
    use of any information obtained from the Services.
</p>
<p>
    <strong>21. Governing Law and Jurisdiction </strong>
</p>
<p>
    All matters relating to the Services and these Terms of Use and any dispute
    or claim arising therefrom or related thereto (in each case, including
    non-contractual disputes or claims), shall be governed by and construed in
    accordance with the internal laws of the Isle of Man without giving effect
    to any choice or conflict of law provision or rule (whether of the Isle of
    Man or any other jurisdiction).
</p>
<p>
    Any legal suit, action or proceeding arising out of, or related to, these
    Terms of Use or the Services shall be instituted exclusively in the courts
    of the Isle of Man or the courts, although we retain the right to bring any
    suit, action or proceeding against you for breach of these Terms of Use in
    your country of residence or any other relevant country. You waive any and
    all objections to the exercise of jurisdiction over you by such courts and
    to venue in such courts.
</p>
<p>
    <strong>22. Limitation on Time to File Claims </strong>
</p>
<p>
    ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE ARISING OUT OF OR RELATING TO
    THESE TERMS OF USE OR THE SERVICES MUST BE COMMENCED WITHIN ONE (1) YEAR
    AFTER THE CAUSE OF ACTION ACCRUES, OTHERWISE, SUCH CAUSE OF ACTION OR CLAIM
    IS PERMANENTLY BARRED.
</p>
<p>
    <strong>23. Waiver and Severability </strong>
</p>
<p>
    No waiver of by the Company of any term or condition set forth in these
    Terms of Use shall be deemed a further or continuing waiver of such term or
    condition or a waiver of any other term or condition, and any failure of
    the Company to assert a right or provision under these Terms of Use shall
    not constitute a waiver of such right or provision.
</p>
<p>
    If any provision of these Terms of Use is held by a court or other tribunal
    of competent jurisdiction to be invalid, illegal or unenforceable for any
    reason, such provision shall be eliminated or limited to the minimum extent
    such that the remaining provisions of the Terms of Use will continue in
    full force and effect.
</p>
<p>
    <strong>24. Entire Agreement</strong>
</p>
<p>
    The Terms of Use and our Privacy Policy, and Terms of Sale (if any)
    constitute the sole and entire agreement between you and US Racing with
    respect to the Services and supersede all prior and contemporaneous
    understandings, agreements, representations and warranties, both written
    and oral, with respect to the Services.
</p>
<p>
    <strong>25. Your Comments and Concerns</strong>
</p>
<p>
    The Services are provided by US Racing or third parties.
</p>
<p>
    All notices of copyright infringement claims should be sent to cs@usracing.com. 
</p>
<p>
    All feedback, comments, requests for technical support and other communications relating to the Services should be directed to: cs@usracing.com.
</p>


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div><!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 
