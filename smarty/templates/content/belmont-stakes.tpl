{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}

{include_php file='/home/ah/allhorse/public_html/belmont/odds_88_2021.php'} 
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

<h2>The Final Jewel of the Triple Crown</h2>
<div class="justify-left"><p>The {$BELMONT_RUNNING} running of the Belmont Stakes will be held at Belmont Park on {include_php file='/home/ah/allhorse/public_html/belmont/racedate.php'} with first-race post just before noon ET. NBC will providing live coverage of the day’s events.</p>

<p><strong>The Belmont's Age</strong></p>
<p> One thing the Belmont does have over the Derby is that it is the oldest of the three Triple Crown events. The Belmont predates the Preakness by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings.</p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
{*include file='/home/ah/allhorse/public_html/belmont/links.tpl'*}
<p><span><strong>Some Monumental Belmont Moments</strong></span></p>

<p> In 1890, the Belmont was moved from Jerome Park to Morris Park, a mile and three-eighths track located a few miles east of what is now Van Cortlandt Park in the Bronx. The Belmont was held at Morris Park until Belmont Park's opening in 1905.</p>
<p><ul>
<p>Here's a tidbit you didn't see in Derby or Preakness history. When Grey Lag won the Belmont in 1921, it marked the first running of the Belmont Stakes in the counter-clockwise manner of American fashion. This 53rd running was a mile and three-eighths over the main course; previous editions at Belmont Park had been run clockwise, in accordance with English custom, over a fish-hook course which included part of the training track and the main dirt oval.</p>
<p>The first post parade in this country came in the 14th running of the Belmont in 1880. Until then the horses went directly from paddock to post.</p>
<p>The Belmont has been run at various distances. From 1867 tp 1873 it was 1 5/8 miles; from 1874 to 1889 it was 1 1/2 miles; from 1890 through 1892, and in 1895, it was held at 1 1/4 miles; from 1896 through 1925 it was 1 5/8 miles; since 1925 the Belmont Stakes has been a race of 1 1/2 miles.</p>
</ul></p>
<p><strong>Champion Sires</strong></p>

<p> As we saw in the breeding section of the Call To The Derby Post Betting How-To Page, champions horses breed champion horses. This certainly holds form in the Belmont Stakes. A total of eleven Belmont Stakes winners have sired at least one other Belmont winner.</p>
<p><ul>
<p>Man o' War heads the list of Belmont champion sires. Not only did he win the race himself in 1920, but three of his subsequent sires won it as well: American Flag in 1925, Crusader in 1926 and War Admiral in 1937, who went on to win the Triple Crown.</p>
<p>Commando won the 1901 running, then sired Peter Pan, the 1907 champ and the Colin, the 1908 winner.<br /> 1930 champion Gallant Fox sired both Omaha (1935) and Granville (1936).</p>
<p>Count Fleet won the 1943 edition, and then sired back-to-back Belmont winners with Counterpoint (1951) and One Count (1952).</p>
<p>1977 Triple Crown winner Seattle Slew sired a Call To The Derby Post favorite in Swale, who won both the Derby and the Belmont in 1984, as well as A.P. Indy, who won the Belmont in 1992. 1999 Belmont winner Lemon Drop Kid is also a descendant of the Slew.</p>
<p>The following horses have sired one Belmont winner each: Duke of Magenta of 1878 sired Eric (1889); Spendthrift of 1879 sired Hastings (1896); Hastings then followed his again by siring Masterman, the 1902 winner. The Finn of 1915 sired Zev (1923); Sword Dancer of 1959 sired Damascus (1967); last but not least, Triple Crown winner Secretariat of 1973 sired Risen Star, the 1988 winner.</p></ul></p>
<p><strong>Money at the Belmont</strong></p>
<p> Oh, have times changed. The purse for the first running of the Belmont was $1,500 added with a total purse of $2,500, with the winner's share taken by the filly Ruthless. The lowest winner's share in Belmont history was the $1,825 earned by The Finn in 1915.</p> 
<p> Belmont set an opposite record in 1992, in which the richest Belmont purse ever totaled 1,764,800. Five times in Belmont history only two horses entered the race: 1887, 1888, 1892, 1910 and sadly, 1920, the year Man O'War triumphed. The largest field, on the other hand, was 15 in 1983, when Caveat defeated Slew O' Gold. In 1875 14 horses ran, when Calvin out-dueled stablemate Aristides, that year's winner of the inaugural Kentucky Derby. The Belmont's lowest paid winner: Count Fleet in 1943, who paid a paltry $2.10.</p> 
<p>The Belmont's highest winner: Sherluck in 1961, who dished out $132.10. A favorite's race: Of the 129 Belmont runnings through 1997, the favorite had won 58 times, including 9 out of the last 25. There have been some strange twists of betting in Belmont history. Since the advent of mutuels in New York in 1940 there have been six times when no place or show betting was taken on the Belmont Stakes. The last time there was no show wagering was in 1978 when Affirmed and Alydar held their famous confrontation.</p> 
<p>There was also no show betting when Secretariat won his Triple Crown in 1973; no wonder--Secretariat won by a record 31 lengths. Show betting was also eliminated in 1957 when Gallant Man defeated Bold Ruler, and also in 1953 when Native Dancer won. In 1943, believe it or not, there was no place or show wagering when Triple Crown winner Count Fleet went off $.05 to the dollar and won by 25 lengths.</p>  
<p>To wrap it up, Whirlaway completed his Triple Crown victory in 1941 without show betting. In other words, by the time horses dominate the Derby and Preakness, there just might not be that many challengers when the horse goes to complete the sweep.</p>  
<p>Since 1940 there have also been 30 horses listed as odds-on favorites in the Belmont Stakes. In 1957, there were two: Gallant Man, who won at 19-20, and Bold Ruler, who finished third at 17-20. Of these 30, only 12 went on to win. The highest on-track mutuel handle on the Belmont: 1993. A total of $2,793,320 was bet on the Belmont that year, with $1,409,970 wagered on win, place and show betting, and $1,293,954 on the daily double, exacta and triple.</p>
<p> <strong>The Fastest Belmont</strong></p>
<p> Who else?&nbsp;<a title="Secretariat" href="/famous-horses/secretariat">Secretariat</a>&nbsp;set a world-record that still stands for the mile and a half distance on a dirt track at 2:24. (He had finished a mile and a quarter at 1:59, faster than his own Derby record of 1:59 2/5.)</p>
<p> <strong>Belmont Trophies</strong></p>
<p>"The Belmont Stakes trophy is a Tiffany-made silver bowl, with cover, 18 inches high, 15 inches across and 14 inches at the base. Atop the cover is a silver figure of Fenian, winner of the third running of the Belmont Stakes in 1869. The bowl is supported by three horses representing the three foundation thoroughbreds--Eclipse, Herod and Matchem. The trophy, a solid silver bowl originally crafted by Tiffany's, was presented by the Belmont family as a perpetual award for the Belmont Stakes in 1926. It was the trophy August Belmont's Fenian won in 1869 and had remained with the Belmont family since that time. The winning owner is given the option of keeping the trophy for the year their horse reigns as Belmont champion."</p>
</div>
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>        



{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}

      

    
