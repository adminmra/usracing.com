    {include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

      
         
          
 {include file='menus/horsebetting.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div> 
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


           
                                        
          
<div class="headline"><h1>Off Track Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p><a href="/off-track-betting"><img class="img-responsive" src="/img/off-track-betting.jpg" alt="Off Track Betting">  </a></p>
     <p>  You can bet from your home computer, mobile or tablet at BUSR.  Get up to an 8% <a href="/rebates">horse betting rebate</a> and there is a <a href="/cash-bonus">$150 Bonus</a> for new members. <a href="/signup/">Join </a> Today!</p>              
                    <h2 > What is Off Track Betting?</h2>
                    
                    <p>Off-track betting, aka, offtrackbetting, aka, offtrack betting and, finally, aka (also known as) OTB.  </p>
                    <p>Did you know that once upon a time (circa 1970) -- not in a galaxy far, far away but in the United States of America,  the City of New York legalized off track betting after many   previous failed attempts. The New York Off-Track Betting corporation was taking in OVER a BILLION DOLLARS in bets yearly!  The &quot;OTB&quot; employed thousands of New Yorkers and basically had a monopoly on legal gambling in New York.</p>
<p>Sidenote: Before the '70s, OTBs were only permitted in the state of   Nevada. And, in case you don't already know, at an off-track betting parlor, bettors wager on horse races   without having to visit the racetrack in-person but their  bets are commingled with on-track betting pools. <br />
</p>
<p>Fast-forward to 2010 when the NY OTB went bust.  Out of business. No pulse.  Finito.  Done like a turkey on Thanksgiving.  The more interesting question is WHY?<br />
</p>
<p>How could a bonafide moneymaker go belly up?  Did the politicians do it?  Did the Offtrack Betting Corporation of New York become a contractor's piggy bank?<br />
</p>
<p>Some of these questions will probably be explored in a documentary called, &quot;Finish Line: The Rise and Demise of Off-Track Betting&quot; being developed by Joseph Fusco.  You can read more about it on <a href="http://www.imdb.com/title/tt2333960/" target="_blank">IMDB</a> and <a href="http://www.kickstarter.com/projects/1925783744/finish-line-the-rise-and-demise-of-off-track-betti" target="_blank">Kickstarter</a>.<br />
</p>
<p>Although NY State couldn't successfully operate their business (when can a local or federal entity run anything profitably, right?), there are many states that offer off track betting. Basically that means you are making bets on horse racing that takes place in other cities or states (or countries) than the one where you might be actually placing the bet.  And the Internet.  Ah, online horse betting is alive and VERY well.  Did you know that over 3 BILLION dollars is wagered in the United States online (legally) on horse racing?  The total handle in the United States is about 14 BILLION dollars each year--- which includes online transactions and bets that are placed directly at the track (track handle) and at off-track betting locations. Long live off-track betting!<br />
</p>
<p>Tom Franklin<br />US Racing</p>
<p>
</p>
<p>&nbsp;</p>
        
        


  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 

