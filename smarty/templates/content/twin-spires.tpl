{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/kentuckyderby.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<!--<div class="container-fluid">
	<a href="/signup?ref=kentucky-derby">
		<img class="img-responsive" src="/img/kentuckyderby/2017-Kentucky-Derby-betting.jpg" alt="Kentucky Derby Betting">
	</a>
</div> -->      
<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
       
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            
                                        
          
<div class="headline"><h1>Twin Spires Betting</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{*include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'*} 

{* <a href="/twin-spires"><img class="img-responsive" src="/img/kentuckyderby/churchilldowns.jpg" alt="Twin Spires Betting"  />  </a> *}
          
<p><Strong>Wouldn't you rather get paid to play?</strong> Every day when you log into your account at BUSR  you will  find an 8% or 5% rebate on your of your exotic wagers (everything but win, place and show) for qualified tracks in your account. For straight wagers (win, place and show) you will find a  3% rebate in  your account. Each day they will deposit the total rebate into your account for the preceding day of all qualified wagers, win or lose! The rebate table is listed below:</p>
	<p>		  {include file='/home/ah/allhorse/public_html/shared/rebates-categories-rebates.php'}     </p>      
          
  <p align="center"><a href="/rebates?ref={$ref}" class="btn-xlrg ">Let Me See the Tracks</a></p>   
          
<h2>Churchill Downs' Twin Spires</h2>

<br />
<p>The Twin Spires are a recognized throughout the world as the symbol of Churchill Downs and its most famous race, the Kentucky Derby. They are so well known that a mere glance at those Twin Spires can inspire stories of big bets, mint juleps, famous horses and everything else surrounding "the most exciting 2 minutes in sports" to horse racing fans everywhere.</p>
<p>Churchill Downs formally opened May 17, 1875 with only four races scheduled. On November 24, 1894 William F. Schulte was appointed president who oversaw the construction of a new grandstand for a reported cost of $100,000.</p>
<p>The Twin Spires were constructed between the fall of 1894 & the spring of 1895 and were brought to life by a young draftsman named Joseph Dominic Baldez.</p>
<p>Baldez was asked to draft the design for the new for Churchill Downs' grandstand and in his original drawings, he referred to the Twin Spires as “towers” which he included to give the new grandstand a more striking appearance. Horse racing fans would have to agree that he was successful in his mission!</p>
<p>The hexagonal spires are a great example of 19th century architecture that preferred buildings to have great presence, symmetry and balance rather than considering more practical use of space.</p>
<p>The Twin Spires continue to be the most recognizable aspect of Churchill Downs and the Kentucky Derby. </p>
{* <p>Twinspires.com is the official betting site of the Kentucky Derby and the Breeders' Cup.  Twinspires.com is owned by Churchill Downs and is one of the leading ADW's based in the United States.  Check them out!</p> *}

        
        
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   
  
            
            
  
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
	{include file='inc/rightcol-calltoaction.tpl'} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
<div class="row" >
<div class="col-md-12">
{include file='inc/disclaimer-kd.tpl'}
</div>
</div>

</div><!-- /#container --> 