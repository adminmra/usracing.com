{literal}
<link type="text/css" rel="stylesheet" media="all" href="/themes/css/lp.css" />
{/literal}

<div id="lp">

<div class="titlebar">          
          
<div class="headline"><h1><span class="white">HEY!</span> YOU MADE THE RIGHT MOVE.</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->




<div class="feature">
<h2>NOW GO FOR THE BIG WIN!</h2>
<h3><a href="/signup/" title="US Racing | Online Horse Racing">Sign Up</a> and explore the easiest platform for legal online horse betting.</h3>

<div class="box">
<h3 class="white">US Racing allows you to deposit and wager on horses. Sometimes simple is better. </h3>
    
<ul>
<li>Bet on the Kentucky Derby today!</li>
<li>Straightforward betting interface.</li>
<li>Change your mind about a bet? It's easy to cancel.</li>
<li>Instant results and free handicapping information.</li>
<li>Check out the previous races of your horse and jockey.</li>
</ul>

</div><!-- end box --> 
</div><!-- end feature --> 

<div><a class="actionBtn" href="/signup/" title="US Racing | Online Horse Racing">SIGN UP FOR FREE!</a></div>



</div><!-- end content --> 
</div><!-- end lp --> 
<!-- end main --> 

