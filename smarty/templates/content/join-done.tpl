<div id="main" class="logged-in">
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->





<div class="content">
<!-- --------------------- content starts here ---------------------- -->




<div><h2 class="title">Cashier</h2></div>		
<ul class="menu">
<li class="leaf first"><a href="javascript:void(0);" onclick="open_banking();" title="Make a Deposit">MAKE A DEPOSIT</a></li>
<li><a href="javascript:void(0);" onclick="open_banking();" title="Make a Withdrawal">MAKE A WITHDRAWAL</a></li>
</ul>

</div>

  

<!-- /block-inner, /block -->



<div id="block-menu-menu-support" class="block block-menu region-even even region-count-2 count-2">
<h2 class="title">Support</h2>
  



<ul class="menu"><li class="leaf first"><a href="/contact" title="">CONTACT US</a></li>
<li><a href="/support/banking-deposits" title="">BANKING DEPOSITS</a></li>
<li><a href="/support/banking-withdrawls" title="">BANKING WITHDRAWLS</a></li>
<li><a href="/horseracing/bettinglimits" title="">HORSE RACING RULES</a></li>
</ul></li>
<li><a href="/responsibilities" title="">RESPONSIBLE GAMING</a></li>
</ul>  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Welcome!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="field field-type-text field-field-body-logged-in">
    
            
                    



<h2>Welcome  {$smarty.request.firstName} {$smarty.request.lastName}! </h2>

<div style="padding: 0 34px;">
<p>&nbsp;</p>
<br />
<strong><span style="font-size: 20px;" class="DarkBlue">Thank You for Choosing US Racing!</span></strong>
<br />
<br />
<strong>Your registration is now complete.</strong>
<br />
<br />
<p>&nbsp;</p>
<p><strong><span style="font-size: 17px;">Get ready to enjoy the races!</span></strong></p>

<!--
<p><strong><span style="font-size: 15px;">Good luck at the Belmont Stakes - June 5th, 2010</span></strong></p>
<p><strong>Bet on it</strong> - Belmont Stakes wagering is available now in the <a href="/racebook" title="Racebook">RACEBOOK</a>. <strong>Select Belmont Park, race #11</strong> to make your wager today!</p>-->
<br />
<br />
<p><img title="Welcome to US Racing" alt="Welcome to US Racing" src="/themes/images/joindone.gif" border="0" /></p>
<br />
<br />
<p><strong>{$smarty.request.firstName}</strong>, when our website is live you will simply login with your Username and Password at the top left corner of our website.</p>


<p>To make your horse picks we have multiple betting options for you! <strong>Win, place, show and EVERY possible horse betting combination is available at US Racing</strong></b></a></p>

<p><strong>Live entries, instant results, Live horse racing video and race replays</strong> are available in the <a href="/" title="Horse Futures"><b>Club House</b></a></p>

<p><strong>Place you bets on your iPhone or Android powered smart phone and watch the races live like you were at the racetrack in person.  Check out the great options</strong> available for <a href="/mobile" title="Mobile Horse Betting"><b>Mobile Horse Betting</b></a></p>

<p><strong>{$smarty.request.firstName}</strong>, if you have any problems or questions, please contact our <a title="Support Centre" href="/contact"><strong>Support Centre</strong></a></p>
<p>&nbsp;</p>

<p>&nbsp;</p>
</div>




        
        
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> <!-- end: #main.logged-in -->
 <!-- end: LOGGED IN ----------------------------------------------------------------- -->
 
 
 
<!-- start: LOGGED OUT ----------------------------------------------------------------- -->
<div id="main" class="logged-out">
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->





<div class="content">
<!-- --------------------- content starts here ---------------------- -->




<div><h2 class="title">Cashier</h2></div>		
<ul class="menu">
<li class="leaf first"><a href="javascript:void(0);" onclick="Javascript: alert('You are not logged in.');" title="Make a Deposit">MAKE A DEPOSIT</a></li>
<li><a href="javascript:void(0);" onclick="Javascript: alert('You are not logged in.');" title="Make a Withdrawal">MAKE A WITHDRAWAL</a></li>
</ul>

</div>

  

<!-- /block-inner, /block -->



<div id="block-menu-menu-support" class="block block-menu region-even even region-count-2 count-2">
<h2 class="title">Support</h2>
  



<ul class="menu"><li class="leaf first"><a href="/contact" title="">CONTACT US</a></li>

</ul>  </div>

  

</div><!-- /block-inner, /block -->



          
       
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
                                                
                                      
          
<div class="headline"><h1>Welcome!</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>Welcome  {$smarty.request.firstName} {$smarty.request.lastName}! </h2>

<div style="padding: 0 34px;">
<p>&nbsp;</p>
<br />
<strong><span style="font-size: 20px;" class="DarkBlue">Thank You for Choosing US Racing!</span></strong>
<br />
<br />
<strong>Your registration is now complete.</strong>
<br />
<br />
<p>&nbsp;</p>
<p><strong><span style="font-size: 17px;">Get ready to enjoy the races!</span></strong></p>

<!--
<p><strong><span style="font-size: 15px;">Good luck at the Belmont Stakes - June 5th, 2010</span></strong></p>
<p><strong>Bet on it</strong> - Belmont Stakes wagering is available now in the <a href="/racebook" title="Racebook">RACEBOOK</a>. <strong>Select Belmont Park, race #11</strong> to make your wager today!</p>-->
<br />
<br />
<p><img title="Welcome to US Racing" alt="Welcome to US Racing" src="/themes/images/joindone.gif" border="0" /></p>
<br />
<br />
<p><strong>{$smarty.request.firstName}</strong>, when our website is live you will simply login with your Username and Password at the top left corner of our website.</p>


<p>To make your horse picks we have multiple betting options for you! <strong>Win, place, show and EVERY possible horse betting combination is available at US Racing</strong></b></a></p>

<p><strong>Live entries, instant results, Live horse racing video and race replays</strong> are available in the <a href="/" title="Club House"><b>Club House</b></a></p>

<p><strong>Place you bets on your iPhone or Android powered smart phone and watch the races live like you were at the racetrack in person.  Check out the great options</strong> available for <a href="/mobile" title="Mobile Horse Betting"><b>Mobile Horse Betting</b></a></p>


<p><strong>{$smarty.request.firstName}</strong>, if you have any problems or questions, please contact our <a title="Support Centre" href="/contact"><strong>Support Centre</strong></a></p>
<p>&nbsp;</p>

<p>&nbsp;</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> <!-- end: #main.logged-out -->
 <!-- end: LOGGED OUT -->