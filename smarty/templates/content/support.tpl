{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/horsebetting.tpl'}

<!-- ---------------------- end left menu contents ------------------- -->
</div>



<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

<div class="headline"><h1>US Racing Support</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p><img class="img-responsive" src="//www.usracing.com/img/usracing-support-lrg.jpg" alt="US Racing Support"  />  </p>

<p>Here is some information that you may find helpful.</p>
<p>If you still have questions, please send us an email about your racing account.  It doesn't matter what site your play with, US Racing is here to help members of all ADWs and racebooks, including Bet America, BUSR, Bet Online, Bovada, Twin Spires, TVG and XpressBet. <a href="mailto:cs@usracing.com?Subject=Account Question">cs@usracing.com</a> </p>
<hr />
<a href="/faqs"><strong>FAQS</strong></a><br />
<hr />
<a href="/horse-racing-terms"><strong>Horse Racing Terms</strong></a><br />
<hr />
<a href="/how-to/bet-on-horses"><strong>How to Bet on horses</strong></a><br />
<hr />


<!--Customer Support: {include file='inc/phone-all.tpl'}<br/>-->



<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'}
</div><!-- end: #right-col -->


</div><!-- end/row -->
</div><!-- /#container -->