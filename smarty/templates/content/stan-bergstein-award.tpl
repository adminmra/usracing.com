<div id="main" class="container">
<div class="row">
<div id="left-col" class="col-md-9">

          
          
<div class="headline">
<!-- ---------------------- h1 MAIN TITLE contents ---------------------------------------------- --> 



<h1>Award Winning Horse Racing Content</h1>


<!-- ---------------------- end h1 MAIN TITLE contents ------------------------------------------ --> 
</div><!-- end/headline -->







<div class="content">
<!-- --------------------- CONTENT starts here --------------------------------------------------- -->


<h2>Top Awards and Honors for USRacing.com</h2>
<img alt="betonhorses" src="/img/usracing-betonhorses.gif" class="pull-left overflow-hidden img-responsive" width="200" style="margin:0 20px 10px 0;" />

<p>US Racing is honored to be recognized for its contributions to the horse racing industry.</p>  <p>As contributor to horse rescue or retired jockey organisations, US Racing also makes a commitment to provide insightful and accurate reviews not only of ADWs and racebooks but of the horse racing industry as a whole.  Sometimes the opinions of US Racing run counter to the "standard" industry talking points or economic models championed by some groups; but it is the goal of US Racing to challenge established practices in order to improve the end product for the fans and on a larger scale, the overall appetite and excitement for horse racing.</p>
<p>In 2016, US Racing's Margaret Ransom broke the story of former Run Happy trainer, Maria Borell in her article <a href="https://www.usracing.com/news/analysis/shocking-untold-story-maria-borell/" target="_blank">The Shocking Untold Story of Maria Borell</a>. This expose lead to a <a href="https://www.gofundme.com/AbandonedHorseFund/" target="_blank">Gofundme</a> effort that raised $20,000 USD to help with the assitance and care of over 40 neglected horses.  It also lead to charges made by the state of Kentucky against Maria Borell and her father, Chuck Borell.</p>
<p>Margaret Ransom's tireless efforts were recognized by the industry as evidenced by her winning the 2016 Stan Bergstein Writing Award presented by Team Valor.</p>
<img alt="stan-bergstein-award" src="/img/stan-bergstein-award.jpg" class="pull-right overflow-hidden img-responsive" width="400" style="margin:0 20px 10px 0;" />
<p>The Stan Bergstein Award is awarded annually by Team Valor International to encourage the the type of writing that Stan Bergstein came to represent-- hard hitting and unflinching journalism</p>
<p>"There are two ways to deal with uncomfortable things. One is to sweep them under the carpet and hope nobody notices. The other way is to shine a light on them in hopes that it will better the situation."  -- Kathleen M. Irwin</p>
<p>"This was an unsettling story brought to my attention by my wife, Kathleen. It exposed a problem and led to action, which is what this award is all about, so Margaret Ransom deserves credit and recognition for her work." -- Barry Irwin, Ceo Team Valor International</p>
<p>"The initial reaction of many following the firing of Maria Borell was to come to her defense. However, Margaret Ransom's thorough research revealed there was much, much more to this story than the popular press initially reported. The article took a trending news item and went after the unanswered questions. While all entries did a good job taking readers beyond the obvious, this one dealt with a topic of intense emotional interest to an audience exceeding the bounds of racing insiders, and did so in a reader-friendly way." -- Maryjean Wall, Lexington Herald-Leader Turf Writer, Pulitzer Prize nominee and three-time Media Eclipse Award winner</p>
<p>In 2017, US Racing's Margaret Ransom received an honorable mention for the NTRA <a href="http://www.bloodhorse.com/horse-racing/articles/218808/voss-wins-media-eclipse-award-for-news-enterprise/" target="_blank">Media Eclipse Award</a> for the same story that won the 2016 <a href="http://www.thoroughbreddailynews.com/ransom-wins-bergstein-award/" target="_blank">Stan Bergstein Writing Award</a> </p>


<p>How does US Racing compare other racebooks? Depending on what kind of wagering experience you are after, US Racing can help guide your decision by providing in depth and insider information on the top ADWs and racebooks around the world. Compare <a href="/best-horse-racing-site">racebook features</a>.</p>



</ul>

<!-- ------------------------ CONTENT ends ------------------------------------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->







<div id="right-col" class="col-md-3">
<!-- ---------------- RIGHT COLUMN CONTENT starts here ------------------------------------------- -->
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 


<!-- ---------------- end RIGHT COLUMN CONTENT --------------------------------------------------- -->
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- end/container -->
