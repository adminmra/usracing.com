{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
	 {include file='menus/kentuckyderby.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

              
<div class="headline"><h1>Road to the Roses Videos</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<img  class="img-responsive" width="100%" src="/img/kentuckyderby/kentucky-derby-roses.jpg" alt="Road to the Roses" /><br>
{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}


<div class="ytFeed">
     <div id="yt-list4" class="yt-feed" data-plumtube data-plum-max-results="50" data-plum-show-title="link" data-plum-playlist="PLEKyEB4uQTtBajmB0IbVJyGznYFXS0m5S" data-plum-modal="true"  data-plum-large-thumb="true"></div>
</div>
                        
     
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-kd.tpl'} 


{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-kd.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 