{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
	<!-- ---------------------- left menu contents ---------------------- -->
	{include file='menus/horsebetting.tpl'}
	<!-- ---------------------- end left menu contents ------------------- -->         
</div>

<div id="main" class="container">
	<div class="row">
	
		<div id="left-col" class="col-md-9">
			<div class="headline"><h1>Graded Stakes Races</h1></div>
			<div class="content">
				<!-- --------------------- content starts here ---------------------- -->
				{include file='/home/ah/allhorse/public_html/kd/wherewhenwatch.tpl'}
				<h2>2013 GRADED STAKES RACE RESULTS</h2>
				{include file='includes/ahr_block_graded-schedule-2013.tpl'}
				<!-- ------------------------ content ends -------------------------- -->
			</div> <!-- end/ content -->
		</div> <!-- end/ #left-col -->
		
		<div id="right-col" class="col-md-3">
			{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
		</div><!-- end: #right-col --> 
		
	</div><!-- end/row -->
</div><!-- /#container --> 



