{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">


<!-- ---------------------- left menu contents ---------------------- -->

        
          
           
{include file='menus/horsebetting101.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
            

  



 
                                        
          
<div class="headline"><h1>Handicapping</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<p>For most fans, a day at the races is a way to get out and let loose, brag about their winnings, and just enjoy themselves.</p>
<p class="ui" style="display: block;">The job of the racetrack is to put on a show that lets the fans have a great time, and part of that is keeping them informed. Spectators and bettors alike need to be able to identify the players and evaluate the horses and then bet on the winners.</p>
<p class="ui" style="display: block;">We have therefore created this section of the site to make it easy for you to find the information you need, no matter your level of understanding.</p>
        
        

  </div>
              
            
            
                      
        
       <!-- end: #col3 -->


      
    </div>