{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/specialraces.tpl'}         

<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Melbourne Cup</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->



<p><strong>Where:</strong> Flemington Park, Melbourne, Australia<br /><strong>When:&nbsp;</strong> The 154th running of 'the race that stops the nation' is in November, 2014</p>
<p>What horse race is capable of stopping a nation? There is only one, and everybody knows the answer...It is Australia's great tradition. It may not officially be Australia's national day of celebration, but unofficially it is. The Tooheys New Melbourne Cup is the most unique race in the world, and the day itself has no counterpart. No other event on the Australian calendar comes close, whether it's a sporting or entertainment event. And it's not just a day for racing fans, it is a day in which racing is celebrated by everybody. The entire country grinds to a halt for the running of the race. Parties and barbecues are held throughout the nation to celebrate.</p>
<p>The home of the race and the center point of a nation for the day - Flemington - is a sea of color and vibrancy from one end of the course to the other. People start arriving at 8am and the crowds continue to build until the start of the race when up to 100,000 will be in attendance to watch the great event. In the Members' Car Park boot-parties are the order of the day. Many men arrive in tails and top hats, but in the crowd, tails and football shorts add another dimension to the occasion. For the women it is still about looking one's elegant best and they come dressed to impress. To be at Flemington is to feel pure excitement, which builds to a crescendo at race time. Each buildup to the Tooheys New Melbourne Cup follows the same pattern.</p>
<p>Before the race a hush descends upon the course, shattered by a roar that engulfs the crowd when the gates open to begin the 3200-metre event. At the winning post the first time around, the leading horse is greeted with a cheer. The noise quickens as the field wheels out of the straight with a lap to go. As they turn into the straight with 600 meters left, the murmur builds again into a roar at crescendo as the horses take their last few gut-wrenching strides.</p>
<p><strong>The History of the Melbourne Cup</strong></p>
<p>The first Melbourne Cup held in 1861 was certainly a dramatic event. According to legend, Archer the first horse to win the Melbourne Cup was reported to have walked 850 km (560 miles) from Nowra to Flemington to be a part of a race, that one day would capture the spirit of a nation.</p>
<p>While at the time the news of the death of the explorers Burke and Wills may have kept people away from the race, a modest crowd of four thousand watched the seventeen starters and the thrilling lead up. Before the race commenced, Twilight started early and was captured only after he had run the whole course, and tragically, two horses, Dispatch and Medora, had died after a fall.</p>
<p>The following year Archer raced again to win his second successive Melbourne Cup. Although it is rumored, he again had walked from Nowra to Flemington, many people thought it was more likely that he had traveled anonymously by ship.</p>
<p>Despite his owner's intention to race Archer for a third Melbourne Cup, he was unable to do so because of a technical error. Archer's acceptance nomination to race failed to arrive in time because it was delayed in the post. As a result, owners scratched many horses, in a show of solidarity. This left a starting field of only seven horses that history shows was to be the smallest field of horses to race in the Melbourne Cup.</p>
<p>At ten years of age, Archer fell in a race and was retired to stud but his place in history was forever cemented. The first Melbourne Cup winner was immortalized in the <a href="http://www.imdb.com/title/tt0088735/" target="_blank">1985 film "Archer's Adventure."</a></p>
        

  
            
            
                      
        


             
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-bs.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



      
    