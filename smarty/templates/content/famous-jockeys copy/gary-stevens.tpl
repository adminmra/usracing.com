{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Gary Stevens</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/gary-stevens.jpg" alt="Gary Stevens" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>On October 4, 2000, Gary Stevens resumed his riding career after a ten-month retirement. In late 1999, Stevens underwent arthroscopic surgery to remove debris in a knee. He began riding again on December 26, 1999, opening day at Santa Anita, then shocked the racing world by announcing he was retiring.&nbsp;</p>
<p>During much of his retirement, Stevens worked as an assistant trainer for Alex Hassinger of The Thoroughbred Corp. He returned to riding after an insurance settlement was reached with Lloyd's of London, which had written a disability policy on him.</p>
<p>Stevens enjoyed a big day on October 28 at Churchill Downs, riding champion Chilukki to win the Churchill Downs Distaff Handicap (G2) and piloting War Chant to capture the Breeders' Cup Mile (G1). He finished 2000 with 36 wins and earnings of $3,167,958. Stevens, who was voted into the Racing Hall of Fame in 1997, earned an Eclipse Award as outstanding rider in 1998. Stevens began his riding career in 1979.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    