{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Eddie Delahoussaye</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/eddie-delayhoussaye.gif" alt="Eddie Delahoussaye" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>A one-time regular in the national earnings top-10, Eddie Delahoussaye rebounded from some ordinary--by his lofty standards--years last year to finish 22nd in earnings ($7,026,480) while winning 113 races. The Louisiana native has been making headlines for nearly three decades since winning his first race in his native state. Delahoussaye rode throughout the Midwest before striking out for Southern California in the late 1970s and developing into one of the nation's leading riders.&nbsp;&nbsp;</p>
<p>Known for his patience, Delahoussaye guided longshot Gato Del Sol to victory in the 1982 Kentucky Derby (G1) from post position 18. Although Delahoussaye has discounted the importance of awards, his trophy case is well-stocked with three of the sport's highest honors for a jockey:&nbsp;The 1978 Eclipse Award, the 1991 George Woolf Award, and a plaque in the Racing Hall of Fame.</p>
<p>Delahoussaye leaves California to ride less frequently these days, though he did ride First American to a 16th-place finish in the 1999 Kentucky Derby.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    