{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Rick Wilson</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/rick-wilson.gif" alt="Rick Wilson" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>Rick Wilson, 47, decided to become a jockey based on the suggestion of Dr. W. P. Lerblance, the man who attended Wilson's birth in McAlester, Oklahoma. At 12, Wilson started his career riding Quarter Horses at area bush tracks and at 19, in 1972, scored his first official victory at La Mesa Park in Raton, New Mexico. After moving his tack to Oaklawn Park in Arkansas and concentrating on Thoroughbred racing, Wilson continued to head east, where he has been based ever since. Wilson has been a multiple leading rider at Philadelphia Park, Garden State Park, and Monmouth Park.&nbsp;</p>
<p>Wilson has had two mounts in the Kentucky Derby (G1). In 1984, he rode Raja's Shark to finished 14th.&nbsp;</p>
<p>In 1993, he rode Wood Memorial Invitational Stakes (G1) winner Storm Tower to finish 16th. This year, Wilson already has won three stakes aboard Xtra Heat, including the Cicada Stakes (G3) on March 25 at Aqueduct.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    