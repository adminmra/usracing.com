{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Pat Day</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/pat-day.jpg" alt="Pat Day" /></div>
<div style="margin-left: 150px; display: block;">
<p>A favorite fixture on the Midwest circuit, particularly Churchill Downs, Racing Hall of Fame Pat Day has been winning races since July, 1973, when he won his first at Prescott Downs in Arizona. The Colorado native has since won the Eclipse Award four times as North America's outstanding jockey, and in the summer of 1997 became only the fourth rider in history to notch 7,000 career victories.&nbsp;</p>
<p>He has piloted numerous champions and/or classic winners, including Easy Goer, Lady's Secret, Unbridled, Summer Squall, Tabasco Cat, Paradise Creek, Turkoman, and Timber County. Among the nation's leading riders since 1982, Day captured the inaugural Breeders' Cup Classic in 1984 aboard Wild Again and won the past two renewals of the race with Awesome Again and Cat Thief.&nbsp;</p>
<p>His Triple Crown record is equally as impressive; Day has finished first or second with 18 of his 45 classic mounts.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    