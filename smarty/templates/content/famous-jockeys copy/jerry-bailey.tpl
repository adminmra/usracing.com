{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Jerry Bailey</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/jerry-bailey.jpg" alt="Jery Bailey" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>Jerry Bailey is one of this country's best big-race riders, having won four Breeders' Cup Classics during a five-year period from 1991-'95, and four Triple Crown races. In 1997, he took home his third consecutive Eclipse Award as North America's outstanding reinsman-the only jockey ever so honored. He added a fourth Eclipse Award for the 1999 season. A native of Dallas, Texas, Bailey launched his storied riding career at New Mexico's Sunland Park, where he won with his first mount, Fetch, in 1974. After successful stints on the Midwest, Southwest, Florida, and New Jersey circuits, Bailey shifted his base to New York in 1982.&nbsp;</p>
<p>Seven years later, he achieved national prominence with his performance aboard Proper Reality in the 1989 Metropolitan Mile Handicap.&nbsp;</p>
<p>A near-flawless technical rider known for patient execution of strategy, Bailey has won more than 4,500 races in his career to date. His first Triple Crown ride was in 1982, when he rode in all three races and finished third in the Preakness Stakes aboard Cut Away. Bailey's Triple Crown breakthrough came nine years later, when he won the Preakness and Belmont Stakes with Hansel. Was unable to hit the board in all three races last year.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    