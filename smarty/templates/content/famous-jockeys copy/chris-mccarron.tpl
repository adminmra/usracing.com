{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Chris McCarron</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/chris-mccarron.gif" alt="Chris McCarron" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>Just call him Chris McCarron the Spoiler. His reputation as one of racing's most gifted riders was enhanced in 1996, when he guided Alphabet Soup to victory in the Breeders' Cup Classic, ruining Cigar's farewell, and the following year, when he and Touch Gold won the Belmont Stakes to spoil Silver Charm's Triple Crown bid. Born in Dorchester, Massachusetts, in 1955, McCarron exploded onto the racing scene in 1974 when he set a then-single season record of 546 wins-a record that would stand for 15 years. He won an Eclipse Award that season as top apprentice, and his laurels have since included a second Eclipse statue (1980), the George Woolf and Mike Venezia Memorial Awards, and induction into racing's Hall of Fame.&nbsp;</p>
<p>McCarron, who has won two Kentucky Derbys, two Preakness Stakes, and two Belmonts, has reigned as the nation's leading rider by money won four times and by races won on three occasions. In 1994 he became the 11th rider in history to pass the 6,000-victory mark. His Triple Crown record has been exemplary, with 12 first- or second-place finishes from 38 mounts. Last year's Triple Crown ended for McCarron a week after a 14th-place finish in the Derby aboard Stephen Got Even when he was injured in a training accident at Hollywood Park.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    