{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>John Velazquez</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/john-velazquez.gif" alt="John Velazquez" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>When Shaun Bridgmohan moved to New York from Miami in late January, 1998, his intent was not solely to win an Eclipse Award as leading apprentice jockey. But, like so many other talented young riders before him who have cut their teeth on the tough New York Racing Association circuit, the 19-year-old jockey gained both experience and a title. He won the Eclipse Award by winning 199 races, 171 as an apprentice, in 1998.</p>
<p>He finished the year with $5,353,890 in purses and won a pair of Grade 3 races at Aqueduct. Bridgmohan remained in New York throughout 1999, finishing the year with 130 victories and $4,986,267 in earnings. He has yet to appear in a Triple Crown race.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    