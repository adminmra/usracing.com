{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Victor Espinoza</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/victor-espinoza.jpg" alt="Victor Espinoza" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>To call 2000 a breakthrough year for Victor Espinoza would be like calling Secretariat a good horse. After winning 93 races with earnings of $3.7-million in 1999 (52nd on the earnings list), Espinoza rode 243 winners last year (including riding titles at Hollywood Park and Del Mar) and had earnings of $13,286,705 to rank seventh nationally. He also rode his 1,000th career winner in early February at Santa Anita Park. The Mexico City native began his career in his hometown and collected his first winner in 1992 at Hippodromo de las Americas.&nbsp;</p>
<p>He rode 79 winners in four months at Hippodromo before coming to the United Stakes in 1993. He was leading apprentice at Bay Meadowns during the 1993-`94 meet and at the `94 Golden Gate Meet. He was leading rider at Fairplex in 1996 and '97. Espinoza, who rode Spain to victory in the Breeders' Cup Distaff (G1) last year, rode in his first classic race last year, finishing sixth in the Preakness Stakes (G1) aboard Hugh Hefner.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    