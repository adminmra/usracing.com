{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Jon Court</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/jon-court.gif" alt="Jon Court" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>The well-traveled Jon Court has ridden horses at numerous racetracks across the United States. Court, 40, was born in Gainesville, Florida, where his family had horses. Inspired by Bill Shoemaker, he began wanting to be a jockey when he was a child.</p>
<p>He started galloping Thoroughbreds at Sunnygrove Farm near Ocala and in 1980 began his riding career at Centennial Park, a now-defunct racetrack in Colorado. Court ranks as the all-time winningest jockey at Birmingham Race Course, which no longer conducts horse racing. He also ranks as the all-time winningest jockey at Hoosier Park in Indiana and has earned leading rider titles at Ellis Park in Kentucky and Oaklawn Park in Arkansas.&nbsp;</p>
<p>In 1998, Court ranked tenth among all riders by victories, with 268, and he finished ninth with 292 wins the following year. In his career, Court has <br />won more than 2,300 races and ridden the earners of more than $32-million. He has scored the richest wins of his career in a trio of $200,000 stakes: the Goldenrod Stakes (G3) at Churchill Downs and the Kentucky Cup Mile Handicap at Kentucky Downs in 1999 and the West Virginia Derby at Mountaineer Park in 1998. </p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    