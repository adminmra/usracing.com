{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Jorge Chavez</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/jorges-chavez.gif" alt="JORGE CHAVEZ" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>Many top jockeys were born to the saddle, hailing from racing backgrounds, and blessed with the advantage of early introductions to riding. Such was not the case with Jorge Chavez, who had never even been on a horse when a friend took him to the races in Peru in 1981-at the age of 20. The tiny Chavez-at 4-foot-10-was soon inspired to try his own hand at race riding, the result being back-to-back Peruvian jockey championships in 1986-`87.&nbsp;</p>
<p>Ambitious and ready to tackle the North American sport, Chavez turned up in Florida in 1988, but spent much of his first season sitting out from a series of suspensions. The situation improved for him as he learned the game, and he headed north to reign as New York's leading rider of 1994, 1995, 1996, and 1997. In the latter year, Chavez ranked among the top ten jockeys in the nation by money won, a total boosted by his 20 New York stakes victories during the season.</p>
<p>In 1999, Chavez reached a career zenith, finishing third in earnings ($17,013,337) while winning 319 races, including the Breeders' Cup Sprint aboard Artax and the BC Distaff with Beautiful Pleasure. He was honored as the Eclipse Award winner as leading jockey for his accomplishments. Last year, Chavez finished fourth on the earnings list with more than $14.4-million. Chavez has yet to hit the board from 11 Triple Crown mounts.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    