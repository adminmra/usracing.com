{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
             {include file='menus/jockeys.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Donnie Meche</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<div class="box-text">
<div><img style="float: left;" src="/themes/images/jockeys/donnie-meche.gif" alt="Donnie Meche" width="130" height="180" /></div>
<div style="margin-left: 150px; display: block;">
<p>Donnie Meche is one of a long list of riders to come from the Bayou State of Louisiana, but one of the few who can claim a twin brother, in his case Lonnie, is a fellow member of the rider's fraternity. Donnie Meche, 25, rode his first winner at Evangeline Downs in May, 1993, and was leading apprentice rider at Louisiana Downs in 1994. Among Meche's main clients is trainer Steve Asmussen, for whom he has been riding regularly for since the 1998 Hawthorne Race Course meeting.&nbsp;</p>
<p>During the 1999 Lone Star Park meet, he and Asmussen, who was the meet's leading trainer, combined for five stakes victories. Meche finished second in the Lone Star Park standings after a tight battle with Corey Lanerie, winning 90 races. Meche finished 1999 with 217 victories in 1,160 starts and earnings of $4,447,127.</p>
</div>

             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    