{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}
 <h3 class="text-center text-uppercase"> {$BELMONT_YEAR} Belmont Stakes Odds</h3>
 {*include_php file='/home/ah/allhorse/public_html/belmont/odds_88_2021.php'*} 
 {include_php file='/home/ah/allhorse/public_html/belmont/odds_88.php'} 
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
<div class="justify-left"><p><strong>Where can I  bet on the Belmont Stakes? </strong> <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> offers Futures Wagers, Match Races and all track betting options for the Belmont Stakes. This year the race will be  held on {$BELMONT_DATE}.</p> 


<p><strong>How can I bet? </strong> There are many different ways to place your wager on the Belmont. At BUSR, you just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR sportsbook, you can bet on Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. {* You will also find   all kinds bets including Future Wagers where you can bet the Belmont Stakes weeks, if not months, in advance. *}</p>

<p><strong>Where can I Watch the Belmont Stakes? </strong> You can watch the race at 5 p.m. EST on TV on NBC or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Belmont Stakes today? </strong> Visit <a href="/belmont-stakes/odds">Belmont Stakes Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>
 <p  class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">{$button_cta}</a></p> 
{* <p>So, you want to bet on the&nbsp;<a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a>?&nbsp; <i>bet  	&#9733; usracing</i> offers Futures Wagers, Match Races and track betting options for the Belmont Stakes. </p> *}
{*include file='/home/ah/allhorse/public_html/belmont/odds_88.php'*} 
{*include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"*}
{* <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   *}
{* <hr> *}{*
<p ><span>The first Belmont in the United States was not the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a race horse that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby.</span></p>
{* <p><strong>Where do I bet on the Belmont Stakes?</strong></p> *}
{*<p>The Belmont is the oldest of the three Triple Crown events. The Belmont predates the&nbsp;<a title="Preakness" href="/preakness-stakes">Preakness</a>&nbsp;by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings. </p>*}
{*include file='/home/ah/allhorse/public_html/belmont/odds.php'*} 

</div>
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}