{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}
 {*include_php file='/home/ah/allhorse/public_html/belmont/odds_88_2021.php'*} 
 {include_php file='/home/ah/allhorse/public_html/belmont/odds_88.php'} 
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>  


<div class="justify-left">
<p><strong>Belmont Trivia to impress your friends:</strong>  The first Belmont Stakes race in the United States was <em>not</em> the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a <em>race horse</em> that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby.      
</p></div>	
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}