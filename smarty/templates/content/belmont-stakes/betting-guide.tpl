{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
     <!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>

 	<div class="text text-md">{$h2}
	 	<br>
	 		{*<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>*}
			<a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->     
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


<div class="content">
<!-- --------------------- content starts here ---------------------- -->


 
 <div class="headline"><h1>Free Belmont Stakes Betting Guide</h1></div>
{*include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'*} 
{*include file='inc/belmontstakes-slider.tpl'*}

 <p>
 Free to all members, you'll get a comprehensive betting guide for all you need to know for the Belmont Stakes:
 <ul>
	 
	<li>Unique Stats and Insights</li>
	<li>Complete Horse by Horse Analysis</li>
	<li>Powerful and Effective Betting Trends</li>
	<li>As well as an exclusive, limited time, Bonus Offer inside</li>
 </ul>
 </p>
	  <p><b>Mobile Users Please Note: </b>the Betting Guide is a chock-full of goodies and is approximately 12mb in size. This may use up data on limited cellular plans.</p>
<br>
<p align="center"><a href="/files/Belmont_Stakes_Betting_Guide_10.pdf" class="btn-xlrg ">Get My Guide</a></p>  





        
       <br><br>
 <p> {include file='inc/disclaimer-bs.tpl'}            

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    