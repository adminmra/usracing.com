{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
         
<!--------- NEW HERO - MAY 2017 ---------------------------------------->

<div class="newHero" style="background-image: url({$hero});">
 <div class="text text-xl">{$h1}</div>
 <div class="text text-xl" style="margin-top:0px;">{$h1b}</div>

 	<div class="text text-md">{$h2}
	 	<br>
	 		<a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
	</div>
 </div>
 
<!--------- NEW HERO END ---------------------------------------->        
 <!------------------------------------------------------------------>    
{include file="/home/ah/allhorse/public_html/usracing/as_seen_on.tpl"}
<!------------------------------------------------------------------>      

     

   <section class="bs-kd bs-kd--default usr-section">
      <div class="container">
        <div class="bs-kd_content">
          <img src="/img/index-bs/bs.png" alt="Online Horse Betting" class="bs-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
    {* <p>{include file='/home/ah/allhorse/public_html/belmont/salesblurb.tpl'} </p> *}

{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

<p>BUSR has  the odds for the {include file='/home/ah/allhorse/public_html/belmont/year.php'}  Belmont Stakes.  The Belmont Stakes is the third and final jewel of the Triple Crown. Belmont Stakes odds typically available shortly after the results of the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>.&nbsp; Here are the latest <a title="Belmont Stakes Odds" href="/belmont-stakes/odds">odds for the Belmont Stakes</a>.</p>

{*include file='/home/ah/allhorse/public_html/belmont/odds.php'*} 
{*include file='/home/ah/allhorse/public_html/belmont/odds_88.php'*}

      </div>
      </div>
<!--    </section>
 <section> -->
 <div class="container">
 <div class="bs-kd_content">
 	<div class="table-responsive">
		<table class="data table table-condensed table-striped table-bordered ordenable"  cellpadding="0" cellspacing="0" border="0"  title="Belmont Stakes Odds"  summary="The latest odds for the Belmont Stakes available for wagering now at BUSR.">
			<!--<caption>Horses - Belmont Stakes - To Win </caption> -->
            <caption>2018 Belmont Stakes Odds</caption>
                        <tfoot>
                <tr>
                    <td class="dateUpdated center" colspan="3">
                        <em id='updateemp'>Updated May 29, 2018 06:59:12.</em>
                        <br />
                        <!-- BUSR - Official <a href="https://www.usracing.com/belmont-stakes/odds">Belmont Stakes Odds</a>. <br /> -->
                        All odds are fixed odds prices.
                    </td>
                </tr>
            </tfoot>
            <!--<thead>
            <tr><th colspan="3" class="center">Horses - Belmont Stakes - To Win  - Jun 09</th></tr>
			<tr><th colspan="3" class="center">Run Or Not All Wagers Have Action</th></tr>
            <tr><th colspan="3" class="center">2018 Belmont Stakes - To Win</th></tr>
            </thead> -->
			<tbody>


	<tr class='head_title'><th><!--Team--></th>
    <th>Fractional</th>
    <th>American</th></tr>
    <tr><td>Blended Citizen</td>
      <td>12/1</td>
    <td>+1200</td></tr>
    <tr><td>Bravazo</td>
      <td>43/10</td>
    <td>+430</td></tr>
    <tr><td>Gronkowski</td>
      <td>33/1</td>
    <td>+3300</td></tr>
    <tr><td>Hofburg</td>
      <td>7/1</td>
    <td>+700</td></tr>
    <tr><td>Just Whistle</td>
      <td>60/1</td>
    <td>+6000</td></tr>
    <tr><td>Justify</td>
      <td>20/23</td>
    <td>-115</td></tr>
    <tr><td>Tenfold</td>
      <td>29/5</td>
      <td>+580</td>
    </tr><tr><td>Vino Rosso</td>
      <td>11/1</td>
      <td>+1100</td></tr>
    <tr><td>Free Drop Billy</td>
      <td>30/1</td>
    <td>+3000</td></tr>
    <tr><td>Restoring Hope</td>
      <td>45/1</td>
    <td>+4500</td></tr>
    <tr><td>Bandua</td>
      <td>50/1</td>
    <td>+5000</td></tr>
    <tr><td>Noble Indy</td>
      <td>30/1</td>
    <td>+3000</td></tr>
			</tbody>
		</table>
	</div>
{literal}
        <style>
            table.ordenable tbody th{cursor: pointer;}
			.table-bordered > thead > tr th { background:#1571BA !important; border: 1px solid #ccc; border-right:0; border-top:0; border-bottom:0;color:#fff; }
			.table-bordered > tbody > tr td, .table-bordered > tbody > tr th { text-align: center !important; }
        </style>
        <script src="//www.usracing.com/assets/js/jquery.sortElements.js"></script>
        <!-- <script src="//www.usracing.com/assets/js/sort_table.js"></script> -->
        <script type="text/javascript" >
		jQuery(document).ready(function(){
	var table = jQuery('table.ordenable tbody');

	jQuery('table.ordenable tbody th').append('&nbsp;<i class="fa fa-sort" title="Sort by this column"/>').each(function(){
		var th = jQuery(this),
		thIndex = th.index(),
		inverse = false;

		th.click(function(){
			table.find('td').filter(function(){
				return jQuery(this).index() === thIndex;
			}).sortElements(function(a, b){
				t_a = jQuery.text([a]);
				t_b = jQuery.text([b]);

				//sorting fractional
				if( t_a.indexOf('/') > 0  &&  t_b.indexOf('/') > 0 ){
					arr_a = t_a.split('/');
					arr_b = t_b.split('/');
					itemA_num = parseInt(arr_a[0]);
					itemB_num = parseInt(arr_b[0]);
					itemA_den = parseInt(arr_a[1]);
					itemB_den = parseInt(arr_b[1]);

					if(itemA_num >= itemA_den){
						americanA = "+"+(itemA_num/itemA_den) * 100;
					} else {
						americanA = "-"+(itemA_num/itemA_den) * 100;
					}

					if(itemB_num >= itemB_den){
						americanB = "+"+(itemB_num/itemB_den) * 100;
					} else {
						americanB = "-"+(itemB_num/itemB_den) * 100;
					}

					itemA = parseInt(americanA);
					itemB = parseInt(americanB);

					return itemA > itemB ? inverse ? -1 : 1 : inverse ? 1 : -1;
				} if( (t_a[0] == '-'  ||  t_a[0] == '+') && (t_b[0] == '-'  ||  t_b[0] == '+') ){
					//sorting american
					itemA = parseInt(t_a);
					itemB = parseInt(t_b);
					console.log(itemA + " > " + itemB);
					return itemA > itemB ? inverse ? -1 : 1 : inverse ? 1 : -1;
				}else {
					return jQuery.text([a]) > jQuery.text([b]) ? inverse ? -1 : 1 : inverse ? 1 : -1;
				}
			}, function(){
				// parentNode is the element we want to move
				return this.parentNode; 
			});
			inverse = !inverse;
		});
	});

	jQuery('table.ordenable tbody th:first').trigger('click');
});
		</script>

{/literal}
 <br />

 
 
 
 {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
 </div>
	</div>
 </section>         
      <!------------------------------------------------------------------>  
    <section class="bs-belmont_"><img src="/img/index-bs/bg-belmont.jpg" alt="Belmont Park" class="bs-belmont_img"></section>
    
  <!------------------------------------------------------------------>      
 {* {include file="/home/ah/allhorse/public_html/belmont/block-free-bet.tpl"} *}
{* {include file="/home/ah/allhorse/public_html/belmont/block-triple-crown.tpl"} *}

{* {include file="/home/ah/allhorse/public_html/belmont/block-testimonial-other.tpl"} *}
{* {include file="/home/ah/allhorse/public_html/usracing/common/block-blue-signup-bonus.tpl"}   *}
{* {include file="/home/ah/allhorse/public_html/usracing/block-50-states.tpl"}   *}
{*
  {include file="/home/ah/allhorse/public_html/belmont/block-countdown.tpl"}
{include file="/home/ah/allhorse/public_html/belmont/block-exciting.tpl"} *}

  


<div id="main" class="container">
 {include file='inc/disclaimer-bs.tpl'} 
</div><!-- /#container --> 
