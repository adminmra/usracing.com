{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}
 {*include_php file='/home/ah/allhorse/public_html/belmont/odds_88_2021.php'*} 
 {include_php file='/home/ah/allhorse/public_html/belmont/odds_88.php'} 
{* <p><i>bet  	&#9733; usracing</i> has  the odds for the {$BELMONT_YEAR}  Belmont Stakes.  The Belmont Stakes is the third and final jewel of the Triple Crown. Belmont Stakes odds typically available shortly after the results of the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>.&nbsp; Here are the latest <a title="Belmont Stakes Odds" href="/belmont-stakes/odds">odds for the Belmont Stakes</a>.</p> *}
<p>The Belmont Stakes is a Grade I stakes race for three year-old Thoroughbreds at Belmont Park {* usually *} run on the second Saturday in June.  {* The race is also known as "The run for the Carnations", "the Test of the Champion " and the "Final Jewel of the Triple Crown". *}  This year, the race {* has been postponed a week and is *} will be held on {$BELMONT_DATE}.  You can watch the Belmont Stakes at 5 p.m. EST on TV on NBC or live stream on NBCSports.com. </p>
<p>The  odds are updated daily and are available for wagering. <br>Bet early and bet often!</p>
{*include file='/home/ah/allhorse/public_html/belmont/odds.php'*} 

 {include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
 {*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}