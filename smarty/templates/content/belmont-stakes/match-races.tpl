{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}

{* Make head-to-head bets on horses at the Belmont!  *}

<h2>{$BELMONT_YEAR}  Belmont Stakes Match Races</h2>
<p><strong>Make head-to-head bets on horses at the Belmont! </strong></p>
<p>What are match races and why would you want to bet them at the Belmont Stakes?</p><p>Match races are simple wagers where you bet whether one particular horse will beat another horse.  Sometimes this is called head-to-head betting. At the Preakness Stakes, bettors have the option to place win, place, show and exotic bets like trifecta and superfectas-- but match races are another way to bet at the track and are lots of fun, too.</p>
<p>Let's review: What are match races in horse racing?  Answer: Match races are bets that punters can place on individual horses in a particular race.  Instead of betting on whether a horse wins, places or shows, you are instead betting whether one horse will beat another horse!  Match races are extremely popular in the United Kingdom.</p>
<p>{include file='/home/ah/allhorse/public_html/belmont/match_races_odds_1311.php'}</p>
<p>Good luck with your bets and when we get closer to the start of the <a href="/belmont-stakes/betting">Belmont Stakes</a>, you will find match more race odds posted online.</p>
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}