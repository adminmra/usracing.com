{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Belmont Stakes Odds</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 
<p></p>



<p>US Racing will post the odds for the {include file='/home/ah/allhorse/public_html/belmont/year.php'}  Belmont Stakes after we know the field of horses racing.  The Belmont Stakes the third and final jewel of the Triple Crown. Belmont Stakes odds typically available shortly after the results of the <a title="Preakness Stakes" href="/preakness-stakes">Preakness Stakes</a>.&nbsp; Here are the latest <a title="Belmont Stakes Odds" href="/belmont-stakes/odds">odds for the Belmont Stakes</a>.</p>

{include file='/home/ah/allhorse/public_html/belmont/odds.php'} 




        
        

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-bs.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



