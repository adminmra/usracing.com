{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Belmont Stakes Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 
{include file='inc/belmontstakes-slider.tpl'}

 <p></p>
 


<p align="justify"><span>The first Belmont in the United States was not the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a race horse that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby.</span></p>

<p><span><strong><span>Where do I bet on the Belmont Stakes?</span></strong><br /><br />

So, you want to bet on the&nbsp;<a title="Belmont Stakes" href="/belmont-stakes">Belmont Stakes</a>?&nbsp; US Racing offers Futures Wagers, Match Races and track betting options for the Belmont Stakes. The Belmont is the oldest of the three Triple Crown events. The Belmont predates the&nbsp;<a title="Preakness" href="/preakness-stakes">Preakness</a>&nbsp;by six years, the Kentucky Derby by eight. The first running of the Belmont Stakes was in 1867 at Jerome Park, on, believe it or not, a Thursday. At a mile and five furlongs, the conditions included an entry fee of $200, half forfeit with $1,500 added. Furthermore, not only is the Belmont the oldest Triple Crown race, but it is the fourth oldest race overall in North America. The Phoenix Stakes, now run in the fall at Keeneland as the Phoenix Breeders' Cup, was first run in 1831. The Queen's Plate in Canada made its debut in 1860, while the Travers in Saratoga opened in 1864. However, since there were gaps in sequence for the Travers, the Belmont is third only to the Phoenix and Queen's Plate in total runnings.</span></p>
<p></p>
{include file='/home/ah/allhorse/public_html/belmont/odds.php'}
<br><br>
  {include file='/home/ah/allhorse/public_html/belmont/links.tpl'} 

        
       
             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{include file='includes/ahr_block-racing-news-right-sidebar.tpl'} 
{include file='inc/disclaimer-bs.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    