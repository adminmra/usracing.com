{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">

          
           
                                        
          
<div class="headline"><h1>Belmont Stakes Contenders</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 
<p></p>



<br>
<p>Official <a title="Belmont Stakes Contenders" href="/belmont-stakes/contenders">Belmont Stakes Contenders</a> provided by US Racing.</p>

{include file='/home/ah/allhorse/public_html/belmont/odds.php'}

<p><strong>Belmont Trivia to impress your friends:</strong>  The first Belmont Stakes race in the United States was <em>not</em> the famous stakes race or even the man for whom it is named. Rather, the first Belmont was a <em>race horse</em> that arrived in California in 1853 from his breeding grounds of Franklin, Ohio. The Belmont Stakes, however, are named after August Belmont, a financier who made quite a name and fortune for himself in New York politics and society. Obviously, Mr. Belmont was also quite involved in horse racing, and his imprint is even intertwined within the history of the Kentucky Derby. 
        


<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
{include file='inc/disclaimer-bs.tpl'} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 



