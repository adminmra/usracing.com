{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

        
          
          
                  {include file='menus/belmontstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


		{literal}
	<script type="application/ld+json">

{
  "@context": "http://schema.org",
  "@type": "Event",
  "name": "Belmont Stakes Schedule",
  "description": "The Belmont Stakes is an American Grade I stakes Thoroughbred horse race held on the first or second Saturday in June at Belmont Park in Elmont, New York. It is a 1.5-mile-long horse race, open to three-year-old Thoroughbreds. Colts and geldings carry a weight of 126 pounds; fillies carry 121 pounds",
  "image": "https://www.usracing.com/img/belmontstakes/belmont-stakes-winner.jpg",
  "startDate": "2019-06-07T00:00",
  "endDate": "2019-06-08T23:55",
  "performer": {
    "@type": "PerformingGroup",
    "name": "Belmont Stakes"
  },
  "location": {
    "@type": "Place",
    "name": "Belmont Stakes",
    "address": { 
      "@type": "PostalAddress",
      "streetAddress": "Belmont Park",
      "addressLocality": "Elmont",
      "addressRegion": "NY",
      "postalCode": "11003",
      "addressCountry": "US"
    }
  }
}

</script>
{/literal}
          
           
                                        
          
<div class="headline"><h1>Belmont Stakes Schedule</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} 
{include file='inc/belmontstakes-slider.tpl'}

<h2>RACE DAY INFORMATION - Saturday, June 7, 2014</h2>

<p>First race is 11:35 a.m.</p>



<p><strong>$1.5 million Belmont Stakes (G1)</strong><br>
3-year-olds, 1-1/2 Mile Dirt</p>

<p><strong>$1.25 million Metropolitan Mile (G1)</strong><br>
3-year-olds &amp; up, 1 Mile Dirt</p>

<p><strong>$1 million Knob Creek Manhattan Handicap (G1)</strong><br>
4-year-olds &amp; up, 1-1/4 Mile Turf<br>
<br>
<strong>$1 million Ogden Phipps (G1)</strong><br>
F&amp;M 4-year-olds &amp; up, 1-1/2 Mile Dirt</p>

<p><strong>$750,000 Longines Just A Game (G1)</strong><br>
F&amp;M 4-year-olds &amp; up,1 Mile Turf</p>

<p><strong>$750,000 TVG Acorn (G1)</strong><br>
Fillies 3-year-olds, 1 Mile Dirt<br>
<br>
<strong>$500,000 Brooklyn Invitational (G2)</strong><br>
4-year-olds &amp; up, 1-1/2 Mile Dirt<br>
<br>
<strong>$300,000 Jaipur Invitational (G3)</strong><br>
4-year-olds &amp; up, 6 Furlongs Turf</p>

<p><strong>$150,000 Easy Goer</strong><br>
3-year-olds, 1-1/16th Mile Dirt<br>
<br></p>

<h2>Two-Day Daily Double Wagers (June 6-7)</h2>
<p>
<strong>1)</strong> True North - Metroplitan Handicap<br />
<strong>2)</strong> Belmont Gold Cup - Belmont Stakes
</p>


<p>* Trifecta Wagering on all races that qualify
Minimum five betting interests carded.</p>

<p>* Superfecta Wagering on all races that qualify
Minimum six betting interests carded. No entries or fields.</p>

<h2>Guaranteed Pools<br></h2>

<p>$500,000 Guaranteed Pick 5<br>
$1 Million Guaranteed Pick 6<br>
$1.5 Million Guaranteed Pick 4</p>



<h2>Saturday, June 7, 2014 - Belmont Stakes Day</h2>
<table width="100%" cellspacing="0" cellpadding="0" border="0" summary="Belmont Stakes Odds" class="data table table-condensed table-striped table-bordered">
  <tbody>
    <tr>
      <td valign="top"><strong>Race</strong></td>

      <td valign="top"><strong>Post</strong></td>

      <td valign="top"><strong>Wagering</strong></td>
    </tr>

    <tr>
      <td valign="top">1</td>

      <td valign="top">11:35 a.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, <strong>P5</strong>*</td>
    </tr>

    <tr>
      <td valign="top">2 &nbsp;<strong>Easy Goer&nbsp;</strong></td>

      <td valign="top">12:07 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, <strong>P4</strong>, QUI,</td>
    </tr>

    <tr>
      <td valign="top">3<strong>&nbsp; Brooklyn Invitational&nbsp;</strong></td>

      <td valign="top">12:40 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3</td>
    </tr>

    <tr>
      <td valign="top">4 &nbsp;<strong>Jaipur Invitational&nbsp;</strong></td>

      <td valign="top">1:16 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, QUI</td>
    </tr>

    <tr>
      <td valign="top">5 &nbsp;<strong>Woody Stephens&nbsp;</strong></td>

      <td valign="top">1:54 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3</td>
    </tr>

    <tr>
      <td valign="top">6 &nbsp;<strong>TVG&nbsp;Acorn&nbsp;</strong></td>

      <td valign="top">2:32 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, <strong>P6**</strong>, GS</td>
    </tr>

    <tr>
      <td valign="top">7 &nbsp;<strong>Ogden Phipps</strong></td>

      <td valign="top">3:14 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, GS</td>
    </tr>

    <tr>
      <td valign="top">8 &nbsp;<strong>Longines Just a Game</strong></td>

      <td valign="top">4:00 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, <strong>P4***</strong></td>
    </tr>

    <tr>
      <td valign="top">9 &nbsp;<strong>Metropolitan&nbsp;</strong></td>

      <td valign="top">4:48 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3</td>
    </tr>

    <tr>
      <td valign="top">10 <strong>Knob Creek Manhattan</strong></td>

      <td valign="top">5:42 p.m.</td>

      <td valign="top">WPS, EX, TRI, SU, DD, P3, <strong>P4</strong></td>
    </tr>

    <tr>
      <td valign="top">11 <strong>Belmont Stakes</strong></td>

      <td valign="top">6:52 p.m.</td>

      <td valign="top">WPS, EX, TRI, SPR, DD</td>
    </tr>

    <tr>
      <td valign="top">12</td>

      <td valign="top">7:40 p.m.</td>

      <td valign="top">WPS, EX, TRI, SPR, DD</td>
    </tr>

    <tr>
      <td valign="top">13</td>

      <td valign="top">8:12 p.m.</td>

      <td valign="top">WPS, EX, TRI, SPR, DD,</td>
    </tr>
  </tbody>
</table>
        
       
<h2>Television Coverage</h2>

<p>
<strong>Wednesday, June 4th:</strong> 6:30 p.m. California Chrome: The Unlikely Champion (NBC Sports Network)&nbsp;<br>


<strong>Wednesday, June 4th:</strong> 10:30 p.m. California Chrome: The Unlikely Champion (Encore) (NBC Sports Network)&nbsp;<br>


<strong>Thursday, June 5th:</strong> 5:30 p.m. Belmont Stakes Classics (NBC Sports Network)&nbsp;<br>


<strong>Thursday, June 5th:</strong> 6:30 p.m. Belmont Stakes Classics (NBC Sports Network)&nbsp;<br>


<strong>Thursday, June 5th:</strong> 7:30 p.m. California Chrome: The Unlikely Champion (NBC Sports Network)<br>


<strong>Friday, June 6th:</strong> 3:30 p.m. California Chrome: The Unlikely Champion (NBC Sports Network)<br>


<strong>Friday, June 6th:</strong> 4:00 p.m. Belmont Stakes Classics (NBC Sports Network)<br>


<strong>Friday, June 6th:</strong> 5:00 p.m. Belmont Stakes Access (NBC Sports Network)<br>


<strong>Friday, June 6th:</strong> 8:00 p.m. California Chrome: The Unlikely Champion (NBC Sports Network)<br>


<strong>Friday, June 6th:</strong> 10:30 p.m. Belmont Stakes Access (Encore) (NBC Sports Network)<br>


<strong>Saturday, June 7th:</strong> 12:00 a.m. California Chrome: The Unlikely Champion (NBC Sports Network)<br>


<strong>Saturday, June 7th:</strong> 2:30 - 4:30 p.m. Belmont Park Live Coverage (NBC Sports Network)<br>


<strong>Saturday, June 7th:</strong> 4:30 p.m. Belmont Park Live Coverage (NBC)<br>


<strong>Saturday, June 7th:</strong> 7:00 p.m. Belmont Post-Race Show (NBC Sports Network)<br>
</p>

{* <p><strong><a href="http://stream.nbcsports.com/horse-racing/index_belmont-auth.html?pid=15646&amp;referrer=" target="_blank">Click here for live streaming of NBC&nbsp;Coverage</a></strong></p>         *}
{include file='inc/disclaimer-bs.tpl'} 
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction-bs.tpl'}
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 

</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 




      
    
