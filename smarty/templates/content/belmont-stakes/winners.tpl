{include file="/home/ah/allhorse/public_html/usracing/block_belmont/top-of-page-blocks.tpl"}

{*Unique copy goes here**************************************}
 
{*<p>{include file='/home/ah/allhorse/public_html/belmont/wherewhenwatch.tpl'} </p>*}

<p>{include_php file='/home/ah/allhorse/public_html/belmont/pastwinners.php'} </p>
 
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
<div class="justify-left"><p><strong>Where can I  bet on the Belmont Stakes? </strong> <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> offers Futures Wagers, Match Races and all track betting options for the Belmont Stakes. This year the race will be  held on {$BELMONT_DATE}.</p> 


<p><strong>How can I bet? </strong> There are many different ways to place your wager on the Belmont. At BUSR you just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR sportsbook, you can bet on Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. {* You will also find   all kinds bets including Future Wagers where you can bet the Belmont Stakes weeks, if not months, in advance. *}</p>

<p><strong>Where can I Watch the Belmont Stakes? </strong> You can watch the race at 5 p.m. EST on TV on NBC or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Belmont Stakes today? </strong> Visit <a href="/belmont-stakes/odds">Belmont Stakes Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>
 <p  class="cta-center"><a href="/signup?ref={$ref}" rel="nofollow" class="btn-xlrg ">{$button_cta}</a></p> 


</div>
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_belmont/end-of-page-blocks.tpl"}
{*include file="/home/ah/allhorse/public_html/belmont/block-countdown-white-belmont.tpl"*}

<!----{include file="/home/ah/allhorse/public_html/usracing/index/end-of-page-blocks.tpl"}  Turn on post race-------------------------------------------------------------->          
     



					        



      
    

