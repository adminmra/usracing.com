{literal} 
<script type="application/ld+json">
	 {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[1]/td[@class='center']/table[@id='o0t']/tbody/tr[1]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[1]/td[@class='center']/table[@id='o0t']/tbody/tr[2]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[2]/td[@class='center']/table[@id='o1t']/tbody/tr[1]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[3]/td[@class='center']/table[@id='o3t']/tbody/tr[1]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[4]/td[@class='center']/table[@id='o0t']/tbody/tr[1]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath":"/html/body[@class='not-front  page page-racebook section-racebook ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[2]/table[@class='oddstable']/tbody/tr/td[@class='center']/table[@id='o9t']/tbody/tr[1]/th[@class='center']"
    }
	</script>
  <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Event",
        "location": {
            "@type": "Place",
            "address": {
                "@type": "PostalAddress"
            }
        },
        "name": "U.S. Presidential Election",
        "startDate": "2020-11-03",
        "endDate": "2020-11-03",
        "image": [
          "https://www.usracing.com/img/presidential-election/us-presidential-election-odds-lg.jpg",
          "https://www.usracing.com/img/presidential-election/us-presidential-election-odds-md.jpg",
          "https://www.usracing.com/img/presidential-election/us-presidential-election-odds-sm.jpg"
        ],
        "description": "U.S. Presidential Election",
        "offers": {
            "@type": "Offer",
            "name": "NEW MEMBERS GET UP TO $500 CASH!",
            "price": "500.00",
            "priceCurrency": "USD",
            "url": "https://www.usracing.com/promos/cash-bonus-20",
            "availability": "http://schema.org/InStock"
        },
        "url": "https://www.usracing.com/odds/us-presidential-election"
    }
	</script>
{/literal}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
span#cd-hours {
    display: none;
}
table#o63t tbody tr:nth-child(1) th {
    background: #8c8484 !important;
}
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
.first-th {
            width: 50%;
        }
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

{* {literal}
    <style>
      @media (min-width: 992px) {
        .newHero {
          background-image: url({/literal}{$hero}{literal});
        }
      }
      @media (max-width: 991px) {
        .newHero {
          background-image: url({/literal}{$hero_mobile}{literal});
          background-position: center;
        }
      }
    </style>
{/literal} *}

{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (max-width: 364px) {
         .fixed_cta {
           font-size: 13px !important;
         }
       }

       @media(min-width: 501px) and (max-width: 648px) {
         .fixed_cta {
           font-size: 16px !important;
         }
       }
     </style>
{/literal}

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref2}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref2}">{$signup_cta}</a>
    </div>
</div>


{include file="/home/ah/allhorse/public_html/presidential/block-countdown-new.tpl"}
<!------------------------------------------------------------------>
<section class="presidential kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
      <p>{include file='/home/ah/allhorse/public_html/presidential/salesblurb.tpl'} </p>
      <p>{include_php file='/home/ah/allhorse/public_html/presidential/odds_us_presidential.php'} </p>
<p align="center"><a href="/signup?ref={$ref11}" class="btn-xlrg fixed_cta" >Bet on the U.S. Presidential Election</a></p>

<p> {*include_php file='/home/ah/allhorse/public_html/breeders/odds_us_presidential_election_xml_1441-neww.php'*} </p>
{*<p align="center"><a href="/signup?ref={$ref12}" class="btn-xlrg fixed_cta">Bet on the U.S. Presidential Election</a></p>*}

<h1>The {include_php file='/home/ah/allhorse/public_html/presidential/running.php'} Quadrennial U.S. Presidential Elcetion<!--hung-test--> runs on {include file='/home/ah/allhorse/public_html/presidential/year-date.php'} </h1>
      <p><a href="/signup?ref={$ref13}">BUSR</a> has the <a href="/signup?ref={$ref14}">latest odds</a> for all potential candidates (including a few names that may surprise you).
       All data will be updated throughout the many midterms, scandals, and surprise announcements that are sure to come between now and election night. Buckle up! It’s sure to get a little bumpy.</p>
    </div>
  </div>
   <!--hung-test-->
    <a href="/famous-horses/secretarait">.</a>
    <!--end-hung-test-->
</section>
{include file="/home/ah/allhorse/public_html/presidential/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/presidential/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/presidential/block-president-special.tpl"}
{include file="/home/ah/allhorse/public_html/presidential/block-exciting.tpl"}



{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
for(var i=0; i<=120; i++) {
addSort('o'+i+'t');
}

  document.querySelector('#signupBtn').href = '/signup?ref=us-presidential-elections-odds-header-1';

</script>
{/literal}
