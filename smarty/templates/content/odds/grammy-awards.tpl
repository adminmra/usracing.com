{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "location": {
        "@type": "Place",
        "address": {
            "@type": "PostalAddress",
            "addressRegion": "LA",
            "addressLocality": "California"
        },
        "name": "Staples Center"
    },
    "name": "2020 Grammy Awards",
    "startDate": "2020-01-26 20:00",
    "subevent": [{
            "name": "2019 Album of the Year",
            "location": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressRegion": "LA",
                    "addressLocality": "California"
                },
                "name": "Staples Center"
            },
            "startDate": "2020-01-26 20:00"
        },
        {
            "name": "2019 Record of the Year",
            "location": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressRegion": "LA",
                    "addressLocality": "California"
                },
                "name": "Staples Center"
            },
            "startDate": "2020-01-26 20:00"
        },
        {
            "name": "2019 Song of the Year",
            "location": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressRegion": "LA",
                    "addressLocality": "California"
                },
                "name": "Staples Center"
            },
            "startDate": "2020-01-26 20:00"
        },
        {
            "name": "2019 Best New Artist",
            "location": {
                "@type": "Place",
                "address": {
                    "@type": "PostalAddress",
                    "addressRegion": "LA",
                    "addressLocality": "California"
                },
                "name": "Staples Center"
            },
            "startDate": "2020-01-26 20:00"
        }
    ]
}
  </script>
{/literal}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

{literal}
    <style>
      @media (min-width: 992px) {
        .newHero {
          background-image: url({/literal}{$hero}{literal});
        }
      }
      @media (max-width: 991px) {
        .newHero {
          background-image: url({/literal}{$hero_mobile}{literal});
          background-position: center;
        }
      }
    </style>
{/literal}

<!--------- NEW HERO  ---------------------------------------->
<div class="newHero" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-md">{$h2}
    <br>
    <a href="/signup?ref={$ref}">
      <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a>
  </div>
</div>
<!--------- NEW HERO END ----------------------------------------> 

{include file="/home/ah/allhorse/public_html/grammy/block-countdown-white.tpl"}

<!------------------------------------------------------------------>
<section class="grammy kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
    <p>{include file='/home/ah/allhorse/public_html/grammy/salesblurb.tpl'} </p>
        <h2>{include file='/home/ah/allhorse/public_html/grammy/year.php'} Grammy Awards Odds</h2>
        <p>{include_php file='/home/ah/allhorse/public_html/grammy/grammy_odds_951_xml.php'}</p>
        <p align="center"><a href="/login?ref={$ref}&to=sports?leagueId=951" rel="nofollow" class="btn-xlrg ">Bet Now on the Grammy Awards Odds</a></p> 
    </div>
  </div>
</section>
<!------------------------------------------------------------------>

<!------------------------------------------------------------------>
{include file="/home/ah/allhorse/public_html/grammy/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/grammy/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/grammy/block-exciting.tpl"}
<!------------------------------------------------------------------>

