{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Event",
      "location": {
          "@type": "Place",
          "address": {
              "@type": "PostalAddress",
              "addressRegion": "LA",
              "addressLocality": "California"
          },
          "name": "Dolby Theatre"
      },
      "name": "Academy Awards 2020",
      "startDate": "2020-02-09 18:30:00",
      "subevent": [{
              "name": "Best Picture",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          },
          {
              "name": "Best Actor",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          },
          {
              "name": "Best Actress",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          },
          {
              "name": "Best Director",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          },
          {
              "name": "Best Supporting Actor",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          },
          {
              "name": "Best Supporting Actress",
              "location": {
                  "@type": "Place",
                  "address": {
                      "@type": "PostalAddress",
                      "addressRegion": "LA",
                      "addressLocality": "California"
                  },
                  "name": "Dolby Theatre"
              },
              "startDate": "2020-02-09 18:30:00"
          }
      ]
  }
  </script>
{/literal}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

{literal}
    <style>
      @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

      /* @media (min-width: 992px) {
        .newHero {
          background-image: url({/literal}{$hero}{literal});
        }
      }
      @media (max-width: 991px) {
        .newHero {
          background-image: url({/literal}{$hero_mobile}{literal});
          background-position: top;
        }
      } */

      @media (max-width: 372px){
        .fixed_cta {
          font-size: 13px !important;
        }
      }

      @media (min-width: 501px) and (max-width: 644px){
        .fixed_cta {
            font-size: 16px!important;
        }
      }
      
      @media (max-width: 324px) {
        .usr-section .container {
          padding: 0px;
        }
      }
      
      .kd_heading {
        font-size: 40px;
      }
	    .first-th {
            width: 70%;
        }
    </style>
{/literal}

<!--------- NEW HERO  ---------------------------------------->
{* <div class="newHero" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-xl" style="margin-top: 0px;">{$h1b}</div>
  <div class="text text-md">{$h2}
    <br>
    <a href="/signup?ref={$ref}">
      <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a>
  </div>
</div> *}
<!--------- NEW HERO END ----------------------------------------> 

<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup?ref={$ref}">{$signup_cta}</a>
    </div>
</div>

{include file="/home/ah/allhorse/public_html/academy-awards/block-countdown.tpl"}

<!------------------------------------------------------------------>
<section class="academy-awards kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
      <p>{include file='/home/ah/allhorse/public_html/academy-awards/salesblurb.tpl'} </p>
        <h2 class="table-title">{include file='/home/ah/allhorse/public_html/academy-awards/year.php'} Academy Awards "Oscars" Odds</h2>
        {include_php file='/home/ah/allhorse/public_html/academy-awards/academy-awards_946.php'}
        <p align="center"><a href="/login?ref={$ref}&to=sports?leagueId=946" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Academy Awards "Oscars"</a></p>
        <h1>The {include_php file='/home/ah/allhorse/public_html/academy-awards/running.php'} Academy Awards "Oscars" runs on {include_php file='/home/ah/allhorse/public_html/academy-awards/date.php'}</h1>
        <p>The {include_php file='/home/ah/allhorse/public_html/academy-awards/running.php'} Academy Awards ceremony, which honors the best films of 2019, will be held on {include_php file='/home/ah/allhorse/public_html/academy-awards/date.php'}. <br> The ceremony will be broadcast on ABC and it will take place at the Dolby Theatre in Los Angeles, California for the 18th consecutive year.</p>
    </div>
  </div>
</section>
<!------------------------------------------------------------------>

<!------------------------------------------------------------------>
{include file="/home/ah/allhorse/public_html/academy-awards/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/academy-awards/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/academy-awards/block-exciting.tpl"}
<!------------------------------------------------------------------>

