{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>

{/literal}

{literal}
    <style>
      @media (min-width: 992px) {
        .newHero {
          background-image: url({/literal}{$hero}{literal});
        }
      }
      @media (max-width: 991px) {
        .newHero {
          background-image: url({/literal}{$hero_mobile}{literal});
          background-position: center;
        }
      }
      @media (max-width: 364px) {
         .fixed_cta {
           font-size: 13px !important;
         }
       }
    </style>
{/literal}

<!--------- NEW HERO  ---------------------------------------->
<div class="newHero" alt="{$hero_alt}">
  <div class="text text-xl">{$h1}</div>
  <div class="text text-md">{$h2}
    <br>
    <a href="/signup?ref={$ref}">
      <div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div>
    </a>
  </div>
</div>
<!--------- NEW HERO END ----------------------------------------> 

{include file="/home/ah/allhorse/public_html/golden-globes/block-countdown-new.tpl"}

<!------------------------------------------------------------------>
<section class="golden-globes kd usr-section">
  <div class="container">
    <div class="kd_content">
      <h1 class="kd_heading">{$h1}</h1>
      <p>{include file='/home/ah/allhorse/public_html/golden-globes/salesblurb.tpl'} </p>
        <h2 class="table-title">Golden Globes Awards Odds</h2>
{include_php file='/home/ah/allhorse/public_html/golden-globes/odds_golden_globes_1112.php'}
        <p align="center"><a href="/login?ref={$ref}&to=sports?leagueId=1112" rel="nofollow" class="btn-xlrg fixed_cta">Bet Now on the Golden Globes Awards</a></p> 

        <h1>The {include_php file='/home/ah/allhorse/public_html/golden-globes/running.php'} Golden Globe Awards runs on {include_php file='/home/ah/allhorse/public_html/golden-globes/date.php'}.</h1>
        <p>US Racing has the latest odds for the Golden Globes.

<br>
The Odds are up right now for the {include_php file='/home/ah/allhorse/public_html/golden-globes/running.php'} Golden Globes that will be announced on {include_php file='/home/ah/allhorse/public_html/golden-globes/date.php'} at the Beverly Hilton in Beverly Hills, California. The broadcast on NBC begins at 8 p.m. Eastern Time and will be hosted by Ricky Gervais.
<br>
Nominees for the 77th Golden Globes were announced in mid-December and odds went up for some of the most popular categories.</p>
    </div>
  </div>
</section>
<!------------------------------------------------------------------>

<!------------------------------------------------------------------>
{include file="/home/ah/allhorse/public_html/golden-globes/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/golden-globes/block-signup-bonus.tpl"}
{include file="/home/ah/allhorse/public_html/golden-globes/block-exciting.tpl"}
<!------------------------------------------------------------------>

