{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}



{literal}
<link rel="stylesheet" href="/assets/css/sbstyle.css">
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
<style type="text/css" >
.infoBlocks .col-md-4 .section {
    height: 250px !important;
	margin-bottom:16px;
}
@media screen and (max-width: 989px) and (min-width: 450px){
.infoBlocks .col-md-4 .section {
    height: 165px !important;
}
}
</style>
{/literal}
 {*schema for the page*}
{literal}
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/h1[@class='kd_heading']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[1]/td[@class='center']/table[@id='o0t']/tbody/tr[1]/th[@class='center']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[1]/td[@class='center']/table[@id='o0t']/tbody/tr[2]/th[@class='center']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[2]/td[@class='center']/table[@id='o1t']/tbody/tr[1]/th[@class='center']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[3]/td[@class='center']/table[@id='o3t']/tbody/tr[1]/th[@class='center']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[1]/table[@class='oddstable']/tbody/tr[4]/td[@class='center']/table[@id='o0t']/tbody/tr[1]/th[@class='center']"
    }
  </script>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org/",
      "@type": "SpeakableSpecification",
      "xpath": "/html/body[@class='not-front ']/section[@class='presidential kd usr-section']/div[@class='container']/div[@class='kd_content']/div[2]/table[@class='oddstable']/tbody/tr/td[@class='center']/table[@id='o9t']/tbody/tr[1]/th[@class='center']"
    }
  </script>
{/literal}
{*end of the shema*}


{*styles for the new hero dont touch*}
{literal}
     <style>
       @media (min-width: 1024px) {
         .usrHero {
           background-image: url({/literal} {$hero_lg} {literal});
         }
       }

       @media (min-width: 481px) and (max-width: 1023px) {
         .usrHero {
           background-image: url({/literal} {$hero_md} {literal});
         }
       }

       @media (min-width: 320px) and (max-width: 480px) {
         .usrHero {
           background-image: url({/literal} {$hero_sm} {literal});
         }
       }

       @media (max-width: 479px) {
         .break-line {
           display: none;
         }
       }

       @media (min-width: 320px) and (max-width: 357px) {
         .fixed_cta {
           font-size: 13px !important;
           }
        }
     </style>
{/literal}
{*End*}

{*Hero seacion change the xml for the text and hero*}
<div class="usrHero" alt="{$hero_alt}">
    <div class="text text-xl">
        <span>{$h1_line_1}</span>
        <span>{$h1_line_2}</span>
    </div>
    <div class="text text-md copy-md">
        <span>{$h2_line_1}</span>
        <span>{$h2_line_2}</span>
        <a class="btn btn-red" href="/signup-presidential?ref={$ref1}">{$signup_cta}</a>
    </div>
    <div class="text text-md copy-sm">
        <span>{$h2_line_1_sm}</span>
        <span>{$h2_line_2_sm}</span>
        <a class="btn btn-red" href="/signup-presidential?ref={$ref1}">{$signup_cta}</a>
    </div>
</div>
{*End*}
 {*This is the path for the countdown*}
  {include file="/home/ah/allhorse/public_html/presidential/block-countdown-new.tpl"}
  {*end*}
  {*This is the section for the odds tables*}
  <section class="presidential kd usr-section">
    <div class="container">
      <div class="kd_content">
         <h1 class="kd_heading">{$h1}</h1>{*This h1 is change in the xml whit the tag h1*}
		  <h3 class="kd_subheading">The Most Donald Trump Bet Options Anywhere</h3>
        {*The path for the main copy of the page*}
        <p>Looking to bet on Trump? <a href="/signup-presidential?ref={$ref2}">BUSR</a> is the leading site where you can bet on <a href="/signup-presidential?ref={$ref3}">Donald Trump</a>.</p>
		  <p><a href="/signup-presidential?ref={$ref4}">Donald Trump's</a> presidency is unarguably an ever trending topic. Everyone has an opinion about his actions as they have a fundamental effect on global politics. Everything from his audacious tweets to his business and private life are constantly on the spotlight. Admittedly, formulating theories about what could happen next is something many like doing.</p>
        <p>The {include_php file='/home/ah/allhorse/public_html/presidential/year.php'} U.S. <a href="/signup-presidential?ref={$ref5}">Presidential Election odds</a>, Props and Specials are live. </p>
        {*end*}
<a href="/signup-presidential?ref={$ref6}" class="btn-xlrg fixed_cta">Bet on the U.S. Presidential Election</a>
        {*end*}
        
     <p> {include_php file='/home/ah/allhorse/public_html/breeders/odds_us_presidential_election_xml-neww.php'} </p>
      <p align="center"><a href="/signup-presidential?ref={$ref7}" class="btn-xlrg fixed_cta">Bet on the U.S. Presidential Election</a></p>
		   <p> {include_php file='/home/ah/allhorse/public_html/breeders/odds_us_presidential_election_xml_1441-neww.php'} </p>
      <p align="center"><a href="/signup-presidential?ref={$ref8}" class="btn-xlrg fixed_cta">Bet Now on the U.S. Presidential Election</a></p>
        {*end*}

        {*Second Copy of the page*}
        <h3>The {include_php file='/home/ah/allhorse/public_html/presidential/running.php'} U.S. Presidential Election runs on {include file='/home/ah/allhorse/public_html/presidential/year-date.php'}</h3>
        <p><a href="/signup-presidential?ref={$ref9}">BUSR</a> provides you with fresh out of the oven futures and props on <a href="/signup-presidential?ref={$ref10}">Donald Trump's</a> next moves. Stay put and keep track of conflicts, decisions and never-ending controversy as they are key to what you'll find available. Perhaps your predictions are the ones!</p>
        {*end*}
        <br>
<br></p>
        </li>
        </ul>
      </div>
    </div>
  </section>
  {*This is the section for the odds tables*}

{* Block for the signup bonus and path*}
{include file="/home/ah/allhorse/public_html/presidential/block-testimonial-equidaily.tpl"}
{include file="/home/ah/allhorse/public_html/presidential/block-president-special.tpl"}
{include file="/home/ah/allhorse/public_html/presidential/block-exciting.tpl"}
{literal}
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sort_odds.js"></script>
<script type="text/javascript">
    addSort('o0t');
    addSort('o1t');
</script>
{/literal}