{assign var="ref" value="top-jockeys"}
{assign var="cta" value="Top Jockeys!"}
{assign var="cta_sub" value="Top Jockeys Sub"}

{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">

<!-- ---------------------- left menu contents ---------------------- -->

{include file='menus/leaders.tpl'}
      
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
      
<div id="main" class="container">
<div class="row">

{*<div id="left-col" class="col-md-9">*}
<div id="left-col" style="padding-right: 15px; padding-left: 15px;">
      
<div class="headline"><h1>Top Jockeys in Horse Racing</h1></div>

<div class="content">
<!-- --------------------- content starts here ---------------------- -->

<p>US Racing presents the top ranking jockeys in horse racing. </p>


{assign var="cta_append" value=" at any of these jockeys"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
{*include file="includes/ahr_block_top-jockey.tpl"*}
{*include file="/home/ah/allhorse/public_html/generated/leaders/jockeys_new.php"*}
   
{include file="/home/ah/allhorse/public_html/generated/leaders/jockeys_new_v2.php"}
{* <br><br><br><br><br>
<center>
<p>Thank you for your patience as we update the data for the top jockeys. </p>
</center>
<br><br> *}
<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{*include file='inc/rightcol-calltoaction.tpl'*} 

{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 

