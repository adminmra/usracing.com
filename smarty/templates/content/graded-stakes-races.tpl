{*literal}
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Fort Lauderdale Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 15, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "My Charmer",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 15, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Rampart Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 15, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Sugar Swirl Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 15, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Asahi Hai Futurity Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 16, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Japan Racing Association",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Mr. Prospector Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 16, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },

    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
{/literal *}
{include file='/home/ah/allhorse/public_html/generated/graded_stakes/graded_stakes_races_json_schema.php'}
{include file='inc/left-nav-btn.tpl'} <div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/stakes.tpl'}
{include file='menus/horsebetting.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->


</div> <div id="main" class="container">
<div class="row"> <div id="left-col" class="col-md-9">

<!--
<div class="headline"><h1>{'Y'|date}  Stakes Races dule</h1></div>
-->
<div class="headline"><h1>Graded  Stakes Races Schedule</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->
<p>US Racing presents the complete graded stakes races schedule for {'Y'|date}.  </p>


{assign var="cta_append" value=" at any of these harness races"}
<p>{include file="/home/ah/allhorse/public_html/usracing/calltoaction_text.tpl"}</p>

<p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">Bet Now</a></p>
{*include file='includes/ahr_block_graded_stakes_schedule.tpl'*}
<h2>Upcoming Stakes Races</h2>

{include file='includes/ahr_block_graded-schedule-new.tpl'}
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<h2>Stakes Results</h2>

{include file='includes/ahr_block_graded-schedule-old.tpl'}
 <p align="center"><a href="/signup?ref={$ref}" class="btn-xlrg ">{$button_cta}</a></p>   

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->

<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*}
 </div><!-- end: #right-col -->

</div><!-- end/row -->
</div><!-- /#container -->

{literal}
    <style type="text/css">
	.table-responsive{ overflow-x:hidden ; }
        table.ordenableResult th{cursor: pointer}
    </style>
<script src="/assets/js/jquery.sortElements.js"></script>
<script src="/assets/js/sorttable_graded.js"></script>
<link rel="stylesheet" type="text/css" href="/assets/css/no-more-tables.css">
{/literal}
