    
      
      
            
      
      
      <div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">


           
                                        
          
<div class="headline"><h1>OTB - Off-Track Betting</h1></div>



<div class="content">
<!-- --------------------- content starts here ---------------------- -->


<h2>&nbsp;</h2>
<h2>&nbsp;</h2>
<h3><span class="DarkBlue">What is Off Track Betting?</span></h3>
<p>&nbsp;</p>
<p>Often referred to as “OTB,” off-track betting is a sanctioned (or legal) way to bet on horses outside of the racetrack. OTBs are usually run by the tracks, management companies or by independent companies licensed by the governing State. Wagers at OTB locations are commingled with on-track betting pools which in turn benefits the entire horseracing industry.</p>
<p>In 1970, New York City was able to legalize off track betting after many previous failed attempts.  Before that, OTBs were only permitted in the state of Nevada. In 1978, the Interstate Horseracing Act was passed to ensure revenues from off track betting would be distributed among the tracks, horse owners and the state. By 1980, New York City had hundreds of off track betting locations.</p>
<p>At an off-track betting parlor, bettors are free to bet on horse races without having to visit the racetrack directly. The convenience of the off-track betting service is second only to the convenience of placing wagers online with accredited and easy to use racebooks, such as US Racing.<br />
</p>
<p style="padding-top: 6px;">&nbsp;</p>
<p style="padding-top: 6px;"><span class="DarkBlue"><strong>Is This the End of Off-Track Betting?</strong></span></p>
<p style="padding-top: 6px;"><span class="post" style="color: #575757;">By Corey Kilgannon/New York Times/February 2008</span></p>
<p>The green façade of the Off-Track Betting parlor is a familiar presence on New York City’s storefront landscape. In addition to being wagering spots, the 61 branches, 8 restaurant locations and 3 theater-style betting centers across the city also serve as unofficial social clubs for core groups of regulars, with each branch reflecting the makeup of the neighborhood.</p>
<p>It is an ever-narrowing slice of New York that still belongs to the hustler and the old-timer. Soon it may be extinct, with the board overseeing city betting parlors having voted on Tuesday to shut them down by mid-June.</p>
<p>Mayor Michael R. Bloomberg has pushed for the closing, which is being contested by officials that oversee off-track betting for New York State. The mayor believes the city is being shortchanged by Off-Track Betting because of state regulations the agency must follow. <a id="more-2118"></a></p>
<p>By noon the narrow storefront branch on Steinway Street in Astoria, Queens — one of the two branches scheduled to close first (the other is on Hylan Boulevard in New Dorp on Staten Island) — is typically packed with local men speaking Greek and reading The New York Post.</p>

<p>The regulars show up, sip coffee, scribble on racing forms and step up to the betting windows or computerized kiosks to place their bets. With a backdrop of television screens showing simulcasts of races across the country, the customers chat and joke until post time and then spend a minute screaming at a TV set together. With each race, the floor gets littered with a fresh batch of failed betting slips.</p>
<p>Many bettors said they were skeptical that O.T.B. would truly shut down in the city, calling the board’s vote a bluff to force state officials to address problems in the betting operation. Some said they would probably stop betting the horses, while others said they would wager at Aqueduct or Belmont racetracks, or call a bookmaker, or place bets by phone.</p>

<p>&nbsp;</p>
<p>*In December 2010, the NYC OTB closed due to lack of profitability.<br />
</p>
<p>&nbsp;</p>
        
        


  
            
            
                      
        


             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div> <!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 

 
</div><!-- end/row -->
</div><!-- /#container --> 

