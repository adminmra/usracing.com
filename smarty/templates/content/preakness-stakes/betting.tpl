{include file="/home/ah/allhorse/public_html/usracing/block_preakness/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  
{* <p>{include_php file='/home/ah/allhorse/public_html/ps/2019_odds_983_new.php'} </p> *}
<p>{*include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'*} </p>
<p>{include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'} </p>
<p>{*{include_php file='/home/ah/allhorse/public_html/ps/odds_ps_hardcoded.php'*} </p>
 <p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}</p>
<div class="justify-left"><p><strong>Where can I  bet on the Preakness Stakes? </strong> <a href="/signup?ref={$ref}" rel="nofollow">BUSR</a> offers Futures Wagers, Match Races and all track betting options for the Preakness Stakes. This year the race will be  held on {$PREAKNESS_DATE}.</p> 


<p><strong>How can I bet? </strong> There are many different ways to place your wager on the Preakness. At BUSR, you just sign up (or log in), make a deposit, and place your bet on your favorite horse. Thats it!</p> 

<p>For more advanced players, you can place your bet in the racebook.  There you have the option of exotic wagers such as Exactas, Trifectas and all the advanced wagers you can find at the racetrack.</p>

<p>In the BUSR sportsbook, you can bet on Match Races and other props: horse vs horse bets, winning time of the race, color of the jockey jersey and so much more. {* You will also find   all kinds bets including Future Wagers where you can bet the Preakness Stakes weeks, if not months, in advance. *}</p>

<p><strong>Where can I Watch the Preakness Stakes? </strong> You can watch the race at 3 p.m. EST on TV on NBC or live stream on NBCSports.com.   </p>

<p><strong>What are the odds for the Preakness Stakes today? </strong> Visit <a href="/preakness-stakes/odds">Preakness Stakes Odds</a> to get the most current odds that are updated all day long and the most trusted source for odds.   </p>

</div>

{*  <p><i>BUSR</i> offers a bets on all the Triple Crown Races. Did you know that on May 23, 1873 the first edition of the Preakness took place?&nbsp;Two years before the inaugural&nbsp;<a title="Kentucky Derby" href="/kentucky-derby">Kentucky Derby</a>, the folks at Pimlico were busy working on a new three-year old stakes race of their own. Pimlico, which since its opening in 1870 had conducted all of its racing in the fall, ran its first Spring Meet in the year of 1873, with the initial running of the Preakness held on May 23. The Preakness was founded by then-Maryland governor Oden Bowie. Governor Bowie's term had actually ended in 1872, yet that did not prevent him from naming the then- mile and a half race in honor of the colt who won the Dinner Party Stakes in 1870 on the occasion of Pimlico's opening. At an 1868&nbsp;<a title="Saratoga" href="/saratoga">Saratoga</a>&nbsp;party hosted by a Milton Sanford, Bowie guaranteed that Maryland would have a track available for a race that was later dubbed the Dinner Party Stakes and had been instantly promoted by Bowie at the party when he offered $15,000 as a purse--no small sum at the time.</p> *}
 
{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_preakness/end-of-page-blocks.tpl"}