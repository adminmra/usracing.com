{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}
{literal}
  <style>
    @media (min-width: 1024px) and (max-width: 1150px) {
      .newHero {
        background-position-x: 80%;
      }
    }
    @media (min-width: 581px) and (max-width: 767px) {
      .newHero {
        background-position-x: 55%;
      }
    }
    @media (min-width: 320px) and (max-width: 580px) {
      .newHero {
        background-position-x: 100%;
      }
    }
                        .background{
        background-image: url("/img/index-ps/bg-pimlico.jpg");
        background-size: contain;
        background-repeat: no-repeat;
        background-position: 0% 110%;
    }

    /* 
  ##Device = Desktops
  ##Screen = 1281px to higher resolution desktops
*/

@media (min-width: 1281px) {
  .background{
    background-position: 0% 126%;
  }
}

/* 
  ##Device = Laptops, Desktops
  ##Screen = B/w 1025px to 1280px
*/

@media (min-width: 1025px) and (max-width: 1280px) {
  .background{
    background-position: 0% 100%;
  }
}

/* 
  ##Device = Tablets, Ipads (portrait)
  ##Screen = B/w 768px to 1024px
*/

@media (min-width: 768px) and (max-width: 1024px) {
  .background{
    background-position: 0% 100%;
  }
}
/*
  ##Device = Low Resolution Tablets, Mobiles (Landscape)
  ##Screen = B/w 481px to 767px
*/

@media (min-width: 481px) and (max-width: 767px) {
  .background{
    background-position: 0% 93%;
  }
}

/* 
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
*/

@media (min-width: 320px) and (max-width: 480px) {
  .background{
    background-position: 0% 93%;
  }
}
  </style>
{/literal}
{* literal}

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "name": "WISEVU",
  "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "2.1",
    "reviewCount": "2"
  },
  "review": [
    {
      "@type": "Review",
      "author": "Good Magic",
      "datePublished": "2018-12-10",
      "description": "Jose Ortiz",
      "name": "Good Magic",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "4",
        "worstRating": "1"
      }
    },
    {
      "@type": "Review",
      "author": "Tenfold",
      "datePublished": "2018-12-10",
      "description": "Victor Espinoza",
      "name": "Tenfold",
      "reviewRating": {
        "@type": "Rating",
        "bestRating": "5",
        "ratingValue": "3",
        "worstRating": "1"
      }
    }
  ]
}
</script>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Event",
    "name": "Fort Lauderdale Stakes",
    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",
    "startDate": "Monday, December 15, 2018  8:00 PM",
    "endDate": "2019-01-07T12:20",
  "performer": {
    "@type": "PerformingGroup",
    "name": "US Racing"
  },
    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",
    "location": {
      "@type": "Place",
      "name": "Gulfstream Park",
      "url": "https://www.usracing.com/graded-stakes-races",
      "address": {
      "@type": "PostalAddress",
        "streetAddress": "",
        "addressLocality": "",
        "addressRegion": "",
        "postalCode": ""
      }
    },
"offers": {
  "@type": "Offer",
  "availability": "http://schema.org/InStock",
  "price": "30",
  "priceCurrency": "USD",
  "url": "https://www.usracing.com/graded-stakes-races",
  "validFrom": "2017-01-20T16:20-08:00"
}
  }
</script>
{/literal *}
{include file='inc/left-nav-btn.tpl'}
<div id="left-nav">
<!-- ---------------------- left menu contents ---------------------- -->
{include file='menus/preaknessstakes.tpl'}
<!-- ---------------------- end left menu contents ------------------- -->         
</div>
<!-- HERO -->
<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">
	<div class="text text-xl">{$h1}</div>
     	<div class="text text-md">{$h2}
          	<br>
               <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>
         </div>
</div>
{include file="/home/ah/allhorse/public_html/ps/block-countdown-white-preakness.tpl"}
<!-- End HERO -->
<!--<div class="container-fluid">

	<a href="/signup?ref={$ref}"><img class="img-responsive visible-md-block hidden-sm hidden-xs" src="{$hero}" alt="{$hero_alt}"></a>

	<a href="/signup?ref={$ref}"><img class="img-responsive visible-sm-block hidden-md hidden-lg" src="{$hero_mobile}" alt="{$hero_alt}"> </a>

</div> -->      
<!------------------------------------------------------------------>    
    <section class="ps-kd ps-kd--default usr-section background">
      <div class="container">
        <div class="ps-kd_content">
          <img src="/img/index-ps/preakness-stakes-icon.png" alt="Online Horse Betting" class="ps-kd_logo img-responsive">
          <h1 class="kd_heading">{$h1}</h1>
          <h3 class="kd_subheading">{$h2}</h3>
        <p>{include file='/home/ah/allhorse/public_html/belmont/salesblurb.tpl'}</p>
        <br>
        <br>
        {include file='/home/ah/allhorse/public_html/ps/results.php'} 
        
        


{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}


        </div>



      </div>



    </section>



      {* <section class="ps-pimlico"><img src="/img/index-ps/bg-pimlico.jpg" alt="Pimlico" class="ps-pimlico_img"></section> *}



<!------------------------------------------------------------------>  











{include file="/home/ah/allhorse/public_html/ps/block-free-bet.tpl"}



{include file="/home/ah/allhorse/public_html/ps/block-triple-crown.tpl"}



{* {include file="/home/ah/allhorse/public_html/ps/block-testimonial-other.tpl"} *}


{* {include file="/home/ah/allhorse/public_html/ps/block-countdown.tpl"} *}



{include file="/home/ah/allhorse/public_html/ps/block-exciting.tpl"}







<!------------------------------------------------------------------>  







<div id="main" class="container">

<div class="row" >

<div class="col-md-12">



 {*{include file='inc/disclaimer-ps.tpl'} *}

</div>

</div>

</div><!-- /#container --> 







      



    



