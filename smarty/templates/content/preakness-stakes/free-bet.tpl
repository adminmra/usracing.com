{include file="/home/ah/allhorse/public_html/usracing/block_preakness/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  

<div class="headline"><h1>How to get your Free Bet on the Preakness Stakes</h1></div>

{* {include_php file='/home/ah/allhorse/public_html/shared/promos/free-preakness-bet.php'}  *}

<div class="justify-left">	 
<p>As a member, you'll can qualify for Free $10 Bet on the Preakness. When you sign up and make make your first deposit at BUSR, you will receive a Free $10 Bet to place on the Preakness Stakes.</p>
<p><strong>  Here is how it works:</strong></p>
<p>
1. Sign up to BUSR and make your first deposit using promocode WELCOME20.<br>
2. Your account will be credited with a {$BONUS_WELCOME_PERCENTAGE} Cash Bonus up to {$BONUS_WELCOME} PLUS a $10 Free Bet.<br>
3. Simply navigate to the Racebook to place your bet on the Preakness Stakes!</p>
<hr>		


<p><strong>Terms and Conditions</strong></p>
<div class="list-text">	 
 <div class="list">
	<p class="list-elem">The Free Bet is available to New BUSR clients Only</p>
	<p class="list-elem">The Free Bet will be credited to your racing account with your first deposit of $100 or more.</p>
	<p class="list-elem">Management reserves the right to modify or cancel this promotion at any time.</p>
	<p class="list-elem">General house rules/terms and conditions apply.</p>
</div>

</div>
<p>{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}  </p>      				

{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_preakness/end-of-page-blocks.tpl"}