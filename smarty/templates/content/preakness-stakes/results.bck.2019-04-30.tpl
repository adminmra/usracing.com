{include file="/home/ah/allhorse/public_html/usracing/schema/web-page.tpl"}

{literal}

  <style>

    @media (min-width: 1024px) and (max-width: 1150px) {

      .newHero {

        background-position-x: 80%;

      }

    }



    @media (min-width: 581px) and (max-width: 767px) {

      .newHero {

        background-position-x: 55%;

      }

    }



    @media (min-width: 320px) and (max-width: 580px) {

      .newHero {

        background-position-x: 100%;

      }

    }

  </style>

{/literal}



{literal}

<script type="application/ld+json">

{

  "@context": "http://schema.org",

  "@type": "LocalBusiness",

  "name": "WISEVU",

  "image": "https://www.usracing.com/img/usracing-betonhorses.gif",

  

  "aggregateRating": {

    "@type": "AggregateRating",

    "ratingValue": "2.1",

    "reviewCount": "2"

  },

  

  "review": [

    {

      "@type": "Review",

      "author": "Good Magic",

      "datePublished": "2018-12-10",

      "description": "Jose Ortiz",

      "name": "Good Magic",

      "reviewRating": {

        "@type": "Rating",

        "bestRating": "5",

        "ratingValue": "4",

        "worstRating": "1"

      }

    },

    {

      "@type": "Review",

      "author": "Tenfold",

      "datePublished": "2018-12-10",

      "description": "Victor Espinoza",

      "name": "Tenfold",

      "reviewRating": {

        "@type": "Rating",

        "bestRating": "5",

        "ratingValue": "3",

        "worstRating": "1"

      }

    }

  ]

}

</script>

<script type="application/ld+json">

  {

    "@context": "http://schema.org",

    "@type": "Event",

    "name": "Fort Lauderdale Stakes",

    "description": "This year's graded Stakes races from $100,000 and up.  Bet horses with 200+ racetracks, $150 Bonus and upd to 8% rebates paid daily.  Mobile phone betting, bet where and when you want.  Online Horse Betting",

    "startDate": "Monday, December 15, 2018  8:00 PM",

    "endDate": "2019-01-07T12:20",

  "performer": {

    "@type": "PerformingGroup",

    "name": "US Racing"

  },



    "image": "https://www.usracing.com/img/usracing-betonhorses.gif",

    "location": {

      "@type": "Place",

      "name": "Gulfstream Park",

      "url": "https://www.usracing.com/graded-stakes-races",

      "address": {

      "@type": "PostalAddress",

        "streetAddress": "",

        "addressLocality": "",

        "addressRegion": "",

        "postalCode": ""

      }

    },

"offers": {

  "@type": "Offer",

  "availability": "http://schema.org/InStock",

  "price": "30",

  "priceCurrency": "USD",

  "url": "https://www.usracing.com/graded-stakes-races",

  "validFrom": "2017-01-20T16:20-08:00"

}

  }

</script>

{/literal}

{include file='inc/left-nav-btn.tpl'}

<div id="left-nav"> 

  

  <!-- ---------------------- left menu contents ---------------------- --> 

  

  {include file='menus/preaknessstakes.tpl'} 

  <!-- ---------------------- end left menu contents ------------------- --> 

</div>



<!-- HERO -->

<div class="newHero hidden-sm hidden-xs" style="background-image: url({$hero});">

	<div class="text text-xl">{$h1}</div>

     	<div class="text text-md">{$h2}

          	<br>

               <a href="/signup?ref={$ref}"><div class="btn btn-red"><i class="fa fa-thumbs-o-up left"></i>{$signup_cta}</div></a>

         </div>

</div>

<!-- End HERO -->



<div id="main" class="container">

  <div class="row">

    <div id="left-col" class="col-md-9">

      <div class="content"> 

        <!-- --------------------- content starts here ---------------------- --> 

        <!--<p>{include file='/home/ah/allhorse/public_html/ps/results_video.php'} </p>--> 

        

        {*include file='/home/ah/allhorse/public_html/ps/wherewhenwatch.tpl'*}

        <div class="headline">

          <h1>Preakness Stakes Results</h1>

        </div>

        {include file='/home/ah/allhorse/public_html/belmont/salesblurb.tpl'}<br>
        <br>
        {include file='/home/ah/allhorse/public_html/ps/results.php'} 

        

        {*include file='inc/disclaimer-bs.tpl'*} 

        <!-- ------------------------ content ends -------------------------- --> 

      </div>

      <!-- end/ content --> 

    </div>

    <!-- end/ #left-col -->

    

    <div id="right-col" class="col-md-3"> {include file='inc/rightcol-calltoaction-bs.tpl'} 

      

      {*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} </div>

    <!-- end: #right-col --> 

    

  </div>

  <!-- end/row --> 

</div>

<!-- /#container --> 



