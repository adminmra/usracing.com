{include file="/home/ah/allhorse/public_html/usracing/block_preakness/top-of-page-blocks.tpl"}
{*Unique copy goes here**************************************}  
{* <p>{include_php file='/home/ah/allhorse/public_html/ps/2019_odds_983_new.php'} </p> *}
<p>{*include_php file='/home/ah/allhorse/public_html/ps/odds_983.php'*} </p>
<p>{include_php file='/home/ah/allhorse/public_html/ps/odds_ps_hardcoded.php'} </p>
<p>The <a title="Preakness Stakes Odds" href="/preakness-stakes/odds">odds for the Preakness Stakes</a> Futures are updated and may change as we approach race day.  {*Will Mike Smith and Justify go on to win the Preakness Stakes and then the Preakness Stakes?  Place your bets and find out!*}</p>
<p>The Preakness Stakes is a Grade I stakes race for three year-old Thoroughbreds at Pimlico Race Course {* usually *} run on the third Saturday in May.  {* The race is also known as "The run for the Carnations", "the Test of the Champion " and the "Final Jewel of the Triple Crown". *}  This year, the race {* has been postponed a week and is *} will be held on {$PREAKNESS_DATE}.  You can watch the Preakness Stakes at 5 p.m. EST on TV on NBC or live stream on NBCSports.com. </p>
<p>The  odds are updated daily and are available for wagering. <br>Bet early and bet often!</p>




{include file="/home/ah/allhorse/public_html/usracing/cta_button.tpl"}
{*Page Unique content ends here*******************************}
{include file="/home/ah/allhorse/public_html/usracing/block_preakness/end-of-page-blocks.tpl"}