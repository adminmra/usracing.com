<div id="main" class="container">
<div class="row">


<div id="left-col" class="col-md-9">



                                      
          
<div class="headline"><h1>Responsible Gaming</h1></div>
<div class="content">
<!-- --------------------- content starts here ---------------------- -->





<p >US Racing is committed to responsible gaming. For the vast majority of players, online gaming is a fun and recreational pastime. We also realize that, for some, it can become a problem that adversely affects their lives and the lives of those around them.</p>

<p >While we pride ourselves on marketing services and products that are accessible to our visitors whenever and wherever they are, unfortunately these services and products have the potential to be abused. Like so many other activities, when online gaming becomes compulsive and uncontrolled, it becomes a problem. </p>

<p >Here is how we promote responsible gaming:</p>
<p ><b>Age Limits:</b></p>

<p >Online gaming is a form of adult entertainment and it is illegal for anyone under the age of 18 to play on the products or services provided by our recommended third party websites.  In some cases, recommended third party websites will not accept members under the age of 21. US Racing will never promote an ADW, racebook or any other website that provides any kind of gambling or fantasy sports gaming which accepts customers under the age of 18.</p>

<p >To prevent underage access we recommend the Internet's most trusted parental control filtering sites:</p>
<ul >
  <li ><a href="http://www.netnanny.com" target="_blank" rel="nofollow">www.netnanny.com</a></li>
  <li ><a href="http://www.cybersitter.com" target="_blank" rel="nofollow">www.cybersitter.com</a></li>
  <li ><a href="http://www.cyberpatrol.com" target="_blank" rel="nofollow">www.cyberpatrol.com</a></li>
  <li ><a href="http://www.contentwatch.com" target="_blank" rel="nofollow">www.contentwatch.com</a></li>
</ul><br>
<p ><b>Getting Help:</b></p>
<p >If you believe you have a gaming problem there are always people who understand your issue and help is always available. The Nevada Council on Problem Gambling has a 24-hour, toll-free hot line at 1-800-522-4700. You can also visit the <a href="http://www.gamblersanonymous.org" target="_blank">Gamblers Anonymous.</a> This is a non-profit organization that is in no way affiliated with US Racing.</p>
<p ><b>Working Together:</b></p>

<p >If you have any queries about these policies or our stance on responsible gaming, please <a href="/support">Contact Us</a>.



             

<!-- ------------------------ content ends -------------------------- -->
</div> <!-- end/ content -->
</div><!-- end/ #left-col -->



<div id="right-col" class="col-md-3">
{include file='inc/rightcol-calltoaction.tpl'} 
{*include file='includes/ahr_block-racing-news-right-sidebar.tpl'*} 
</div><!-- end: #right-col --> 
 

 
</div><!-- end/row -->
</div><!-- /#container --> 
