<h2>Horse Racing Help</h2>

<ul class="navbar-nav">
<li><a href="/handicapping-horses" title="Handicapping">Handicapping <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/legend" title="Horse Racing Legend">Horse Racing Legend <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/horse-racing-terms" title="Horse Racing Terms">Horse Racing Glossary <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/horse-racing-bets" title="Horse Racing Wagers">Horse Racing Wagers <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/horse-racing-history" title="History Of Horse Racing">Horse Racing History <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/famous-horses" title="Famous Horses">Famous Horses <i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/famous-jockeys" title="Famous Jockeys">Famous Jockeys <i class="Fa Fa-Angle-Right"></i></a></li>
</ul>