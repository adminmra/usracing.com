

<h2>Promotions</h2>


<ul class="menu">
<li class="leaf first {if (preg_match('/^\/promotions$/i',$arg_path))} active-trail{/if}"><a href="/promotions" title="Current Promotions, Specials and Bonuses" class="{if (preg_match('/^\/promotions$/i',$arg_path))} active{/if}">CURRENT</a></li>
<li><a class="{if (preg_match('/^\/promotions\/horseracing$/i',$arg_path))} active{/if}" href="/promotions/horseracing" title="Horse Racing Promotions and Rebates">HORSE RACING</a></li>

<li class="expanded {if (preg_match('/^\/promotions\/sportsbook\//i',$arg_path))} active-trail{/if}"><a class="{if (preg_match('/^\/promotions\/sportsbook\/football$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/football" title="Sportsbook Rebates and Promotions">SPORTSBOOK</a>
	<ul class="menu">
    <li><a class="{if (preg_match('/^\/promotions\/sportsbook\/football$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/football" title="">NFL &amp; NCAA FOOTBALL</a></li>
	<li><a class="{if (preg_match('/^\/promotions\/sportsbook\/mlb$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/mlb" title="MLB Baseball Rebates and Promotions">MLB</a></li>
	<li><a class="{if (preg_match('/^\/promotions\/sportsbook\/nba$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/nba" title="NBA Promotions">NBA</a></li>
    <li><a class="{if (preg_match('/^\/promotions\/sportsbook\/NCAA-Hoops$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/NCAA-Hoops" title="NCAA Promotions">NCAA</a></li>
	<li><a class="{if (preg_match('/^\/promotions\/sportsbook\/nhl$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/nhl" title="NHL Rebates and Promotions">NHL</a></li>
	<li><a class="{if (preg_match('/^\/promotions\/sportsbook\/sports-cashback$/i',$arg_path))} active{/if}" href="/promotions/sportsbook/sports-cashback" title="5% Sports Cash Back Rebate">5% SPORTS CASH BACK</a></li>
	</ul>
</li>

<li class="expanded {if (preg_match('/^\/promotions\/casino\//i',$arg_path))} active-trail{/if}"><a class="{if (preg_match('/^\/promotions\/casino\/casino/casino-cashback$/i',$arg_path))} active{/if}" href="/promotions/casino/casino-cashback">CASINO</a>
	<ul class="menu">
    
	<li><a class="{if (preg_match('/^\/promotions\/casino\/casino-cashback$/i',$arg_path))} active{/if}" href="/promotions/casino/casino-cashback" title="">50% CASINO CASH BACK</a></li>    
	<li><a class="{if (preg_match('/^\/promotions\/casino\/blackjack-lucky7s$/i',$arg_path))} active{/if}" href="/promotions/casino/blackjack-lucky7s" title="Blackjack Bonuses and Rebates">BLACKJACK LUCKY 7s&#039; BONUS</a></li>    
	<li><a class="{if (preg_match('/^\/promotions\/casino\/paigow-challenge$/i',$arg_path))} active{/if}" href="/promotions/casino/paigow-challenge" title="Pai Gow Challenge Rebates and Promotions">PAI GOW CHALLENGE</a></li>
	</ul>
</li>

<li><a class="{if (preg_match('/^\/promotions\/deposit-specials$/i',$arg_path))} active{/if}" href="/promotions/deposit-specials" title="US Racing Deposit Specials">DEPOSIT SPECIALS</a></li>

<li class="expanded {if (preg_match('/^\/poker\//i',$arg_path))} active-trail{/if}"><a class="{if (preg_match('/^\/poker\/derbyfreeroll$/i',$arg_path))} active{/if}" href="/poker/derbyfreeroll" title="Poker Promotions">POKER</a>
	<ul class="menu">
  <li><a class="{if (preg_match('/^\/poker\/derbyfreeroll$/i',$arg_path))} active{/if}" href="/poker/derbyfreeroll" title="The Kentucky Derby $5,000 Freeroll">$5000 DERBY FREE ROLL</a></li>     
	<li><a class="{if (preg_match('/^\/poker\/megamillions$/i',$arg_path))} active{/if}" href="/poker/megamillions" title="Mega Millions Poker Promotion">MEGA MILLIONS</a></li>    
	<li><a class="{if (preg_match('/^\/poker\/tournaments$/i',$arg_path))} active{/if}" href="/poker/tournaments" title="Poker Tournaments">POKER TOURNAMENTS</a></li>
	</ul>
</li>
    

</ul>

<!-- end content -->  
<!-- end block-inner -->
<!-- end block -->



    