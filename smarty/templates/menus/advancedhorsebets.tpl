
<h2>Advanced  Bets</h2>
<ul class="navbar-nav">
  <li><a href="/horse-betting/across-the-board" title="Across The Board Bets">Across The Board <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/place-pick-all" title="Place Pick All Bets">Place Pick All <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/box-bet" title="Box Horse Bets">Box Bets <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/key-a-horse" title="Key A Horse">Key A Horse <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/pick-three" title="Pick 3 Horse Bets">Pick Three <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/pick-four" title="Pick 4 Bets">Pick Four <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/pick-six" title="Pick 6 Horse Bets">Pick Six <I class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-racing-terms" title="Horse Racing Terms And Terminology">Horse Racing Terms <I class="Fa Fa-Angle-Right"></i></a></li>
</ul>
