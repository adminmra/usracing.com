<h2> Breeders' Cup</h2>
<ul class="navbar-nav">
  <li><a href="/breeders-cup" title="{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup" >{include file='/home/ah/allhorse/public_html/breeders/year.php'} Breeders' Cup</a></li>
  <li><a href="/breeders-cup/betting"  >Breeders' Cup Betting</a></li>
  <li><a href="/breeders-cup/odds"  >Breeders' Cup Odds</a></li>
  <li><a href="/breeders-cup/contenders" >Breeders' Contenders</a></li>
{*   <li><a href="/breeders-cup/challenge"  >Breeders' Cup Challenge</a></li> *}
 {*  <li><a href="/breeders-cup/winners" >Breeders' Winners</a></li> *}
  {* <li><a href="/breeders-cup/races"   data-toggle="dropdown" Data-Hover="dropdown"data-delay="0" data-close-others="false">Breeders' Cup Races <i class="fa fa-angle-down"></i></A> *}
  <li><a href="#"   data-toggle="dropdown" Data-Hover="dropdown"data-delay="0" data-close-others="false">Breeders' Cup Races <i class="fa fa-angle-down"></i></A>


{* last updated Oct 28, 20 - TF.  Ordered by importance as determined by bc website.  Race orders change anually so check*}
  	<ul class="menu">
    <li><a href="/breeders-cup/juvenile"  >Juvenile</a></li>											{* Friday *}     
    <li><a href="/breeders-cup/juvenile-turf"  >Juvenile Turf</a></li>  								{* Friday *}
    <li><a href="/breeders-cup/juvenile-fillies"  >Juvenile Fillies</a></li>							{* Friday *} 
    <li><a href="/breeders-cup/juvenile-fillies-turf"  >Juvenile Fillies Turf</a></li>					{* Friday *}
    <li><a href="/breeders-cup/juvenile-turf-sprint"  >Juvenile Turf Sprint</a></li>					{* Friday *}
  

    <li><a href="/breeders-cup/classic"  >Classic</a></li>												{* Saturday *}  
    <li><a href="/breeders-cup/turf"  >Turf</a></li>													{* Saturday *}      
    <li><a href="/breeders-cup/distaff" >Distaff</a></li>												{* Saturday *}
    <li><a href="/breeders-cup/mile"  >Mile</a></li>													{* Saturday *}              
 	<li><a href="/breeders-cup/sprint"  >Sprint</a></li>												{* Saturday *}             
    <li><a href="/breeders-cup/filly-mare-turf"  >Filly & Mare Turf</a></li>							{* Saturday *}             
    <li><a href="/breeders-cup/dirt-mile" >Dirt Mile</a></li>											{* Saturday *}
    <li><a href="/breeders-cup/turf-sprint"  >Turf Sprint</a></li>										{* Saturday *}    
    <li><a href="/breeders-cup/filly-mare-sprint" >Filly & Mare Sprint</a></li>							{* Saturday *}  

  	</ul>
  </li>
  <li><a href="/bet-on/breeders-cup"  >Bet On Breeders' Cup</a></li>
</ul>
