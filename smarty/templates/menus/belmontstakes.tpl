<ul>
<li>
<a title="Kentucky Derby">Kentucky Derby <i class="fa fa-angle-down"></i></a>
{include file='menus/kentuckyderby_sub.tpl'}
</li>


<li>
<a title="Preakness Stakes">Preakness Stakes <i class="fa fa-angle-down"></i></a>
{include file='menus/preaknessstakes_sub.tpl'}
</li>



<li class="active">
<a title="Belmont Stakes">Belmont Stakes <i class="fa fa-angle-down"></i></a>
{include file='menus/belmontstakes_sub.tpl'}
</li>


<li>
{include file='menus/triplecrown_sub.tpl'}
</li>
</ul><!-- end/menu -->