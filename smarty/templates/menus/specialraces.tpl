<h2>Special Races</h2>
<ul class="menu">
<li><a href="/royal-ascot" title="">Royal Ascot <i class="fa fa-angle-right"></i></a></li>
<li><a href="/breeders-cup" title="">Breeders&#039; Cup <i class="fa fa-angle-right"></i></a></li>
<li><a href="/dubai-world-cup" title="">Dubai World Cup <i class="fa fa-angle-right"></i></a></li>
<li><a href="/hong-kong-cup" title="">Hong Kong Cup <i class="fa fa-angle-right"></i></a></li>
<li><a href="/kentucky-oaks" title="">Kentuck Oaks <i class="fa fa-angle-right"></i></a></li>
<li><a href="/prix-de-larc-de-triomphe" title="">Prix De L&#039;Arc De Triomphe <i class="fa fa-angle-right"></i></a></li>
<li><a href="/melbourne-cup" title="" class="active">Melbourne Cup <i class="fa fa-angle-right"></i></a></li>
</ul>
