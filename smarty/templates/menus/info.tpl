
<h2>Info</h2>
<ul class="navbar-nav">
  <li><a href="/rebates" title="8% Rebate On Bets">8% REBATE ON BETS <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/how-to/placeabet" title="How to bet on horses">HOW TO BET ON HORSES <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/horse-racing/betting-limits" title="Rules">RULES <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/handicapping-horses" title="Horse Race Betting Tips">HORSE RACING TIPS <i class="fa fa-angle-right"></i></a></li>
</ul>
