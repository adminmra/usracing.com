      <h2>Racetracks</h2>
      
         <ul class="navbar-nav">
            <li><a class="dropdown-toggle" href="/arizona" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Arizona <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                   
                  <li><a href="/turf-paradise" title="">Turf Paradise</a></li>
               </ul>
            </li>
            
            <li><a class="dropdown-toggle" href="/arkansas" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Arkansas <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/oaklawn-park" title="">Oaklawn Park</a></li>
               </ul>
            </li>
            
            <li><a class="dropdown-toggle" href="/california" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">California <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/hollywood-park" title="">Hollywood Park</a></li>
                  <li><a href="/cal-expo" title="">Cal Expo</a></li>
                  <li><a href="/delmar" title="">Delmar</a></li>
                  <li><a href="/fairplex-park" title="">Fairplex</a></li>
                  <li><a href="/ferndale" title="">Ferndale</a></li>
                  <li><a href="/fresno" title="">Fresno</a></li>
                  <li><a href="/golden-gate-fields" title="">Golden Gate Fields</a></li>
                  <li><a href="/los-alamitos" title="">Los Alamitos</a></li>
                  <li><a href="/pleasanton" title="">Pleasanton</a></li>
                  <li><a href="/sacramento" title="">Sacramento</a></li>
                  <li><a href="/santa-anita-park" title="">Santa Anita Park</a></li>
                  <li><a href="/santa-rosa" title="">Santa Rosa</a></li>
                  <li><a href="/stockton" title="">Stockton</a></li>
               </ul>
            </li>
            
            <li><a href="/colorado" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Colorado <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/arapahoe-park" title="">Arapahoe Park</a></li>
               </ul>
            </li>
            
            <li><a href="/delaware" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Delaware <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/delaware-park" title="">Delaware Park</a></li>
                  <li><a href="/dover-downs" title="">Dover Downs</a></li>
                  <li><a href="/harrington" title="">Harrington Raceway</a></li>
               </ul>
            </li>
            
            <li><a href="/florida" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Florida <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/calder-race-course" title="">Calder</a></li>
                  <li><a href="/gulfstream-park" title="">Gulfstream Park</a></li>
                  <li><a href="/hialeah-park" title="">Hialeah Park</a></li>
                  <li><a href="/pompano-park" title="">Pompano Park</a></li>
                  <li><a href="/tampa-bay-downs" title="">Tampa Bay Downs</a></li>
               </ul>
            </li>

            <li><a href="/illinois" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Illinois <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/arlington-park" title="">Arlington Park</a></li>
                  <li><a href="/balmoral-park" title="">Balmoral Park</a></li>
                  <li><a href="/fairmount-park" title="">Fairmount Park</a></li>
                  <li><a href="/hawthorne-race-course" title="">Hawthorne Race Course</a></li>
                  <li><a href="/maywood-park" title="">Maywood Park</a></li>
               </ul>
            </li>

            <li><a href="/indiana" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Indiana <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/hoosier-park" title="">Hoosier Park</a></li>
                  <li><a href="/indiana-downs" title="">Indiana Downs</a></li>
               </ul>
            </li>

            <li><a href="/iowa" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Iowa <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/prairie-meadows" title="">Prairie Meadows</a></li>
               </ul>
            </li>
            <li><a href="/kentucky" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Kentucky <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                 <li><a href="/churchill-downs" title="">Churchill Downs</a></li>
                  <li><a href="/ellis-park" title="">Ellis Park</a></li>
                  <li><a href="/keeneland" title="">Keenland</a></li>
                  <li><a href="/kentucky-downs" title="">Kentucky Downs</a></li>
                  <li><a href="/turfway-park" title="">Turfway Park</a></li>
               </ul>
            </li>

            <li><a href="/louisiana" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Louisiana <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/delta-downs" title="">Delta Downs</a></li>
                  <li><a href="/evangeline-downs" title="">Evangeline Downs</a></li>
                  <li><a href="/fair-grounds-race-course" title="">Fair Grounds Race Course</a></li>
                  <li><a href="/louisiana-downs" title="">Louisiana Downs</a></li>
               </ul>
            </li>

            <li><a href="/maine" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Maine <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/hollywood-slots-raceway" title="">Hollywood Slots Raceway</a></li>
               </ul>
            </li>
            <li><a href="/maryland" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Maryland <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/laurel-park" title="">Laurel Park</a></li>
                  <li><a href="/pimlico" title="">Pimlico</a></li>
                  <li><a href="/rosecroft-raceway" title="">Rosecroft Raceway</a></li>
               </ul>
            </li>

            <li><a href="/massachusetts" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Massachusetts <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/suffolk-downs" title="">Suffolk Downs</a></li>
               </ul>
            </li>

            <li><a href="/minnesota" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Minnesota <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                 <li><a href="/canterbury-park" title="">Canterbury Park</a></li>
                  <li><a href="/running-aces" title="">Running Aces</a></li>
               </ul>
            </li>

            <li><a href="/nebraska" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Nebraska <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/fonner-park" title="">Fonner Park</a></li>
               </ul>
            </li>

            <li><a href="/new-jersey" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">New Jersey <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/atlantic-city-race-course" title="">Atlantic City Race Course</a></li>
                  <li><a href="/freehold-raceway" title="">Freehold Raceway</a></li>
                  <li><a href="/meadowlands" title="">Meadowlands</a></li>
                  <li><a href="/monmouth-park" title="">Monmouth Park</a></li>
               </ul>
            </li>

            <li><a href="/new-mexico" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">New Mexico <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/ruidoso-downs" title="">Ruidoso Downs</a></li>
                  <li><a href="/sunland-park" title="">Sunland Park</a></li>
                  <li><a href="/zia-park" title="">Zia Park</a></li>
               </ul>
            </li>

            <li><a href="/new-york" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">New York <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/aqueduct" title="">Aqueduct</a></li>
                  <li><a href="/belmont-park" title="">Belmont Park</a></li>
                  <li><a href="/buffalo-raceway" title="">Buffalo Raceway</a></li>
                  <li><a href="/finger-lakes" title="">Finger Lakes </a></li>
                  <li><a href="/saratoga" title="">Saratoga</a></li>
                  <li><a href="/tioga-downs" title="">Tioga Downs</a></li>
                  <li><a href="/vernon-downs" title="">Vernon Downs</a></li>
                  <li><a href="/yonkers-raceway" title="">Yonkers Raceway</a></li>
               </ul>
            </li>

            <li><a href="/ohio" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Ohio <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/northfield-park" title="">Northfield Park</a></li>
                  <li><a href="/raceway-park" title="">Raceway Park</a></li>
                  <li><a href="/scioto-downs" title="">Scioto Downs</a></li>
               </ul>
            </li>

            <li><a href="/oklahoma" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Oklahoma <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/fair-meadows" title="">Fair Meadows</a></li>
                  <li><a href="/remington-park" title="">Remington Park</a></li>
                  <li><a href="/will-rogers-downs" title="">will Rogers Downs</a></li>
               </ul>
            </li>

            <li><a href="/oregon" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Oregon <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/portland-meadows" title="">Portland Meadows</a></li>
               </ul>
            </li>

            <li><a href="/pennsylvania" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Pennsylvania <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/harrahs-philadelphia" title="">Harrahs Philadelphia</a></li>
                  <li><a href="/parx-racing" title="">Parx Racing</a></li>
                  <li><a href="/penn-national" title="">Penn National</a></li>
                  <li><a href="/pocono-downs" title="">Pocono Downs</a></li>
                  <li><a href="/presque-isle-downs" title="">Presque Isle Downs</a></li>
                  <li><a href="/the-meadows-racetrack" title="">The Meadows Racetrack</a></li>
               </ul>
            </li>

            <li><a href="/texas" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Texas <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/lone-star-park" title="">Lone star Park</a></li>
                  <li><a href="/retama-park" title="">Retama Park</a></li>
                  <li><a href="/sam-houston-race-park" title="">Sam Houston Race Park</a></li>
               </ul>
            </li>
            <li><a href="/virginia" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Virginia <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/colonial-downs" title="">Colonial Downs</a></li>
               </ul>
            </li>
            <li><a href="/washington" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Washington <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/emerald-downs" title="">Emerald Downs</a></li>
               </ul>
            </li>

            <li><a href="/west-virginia" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">West Virginia <i class="fa fa-angle-down"></i></a>
               <ul class="menu">
                  <li><a href="/charles-town" title="">Charles Town</a></li>
                  <li><a href="/mountaineer-park" title="">Mountaineer Park</a></li>
               </ul>
            </li>
            <li>
         </ul>


