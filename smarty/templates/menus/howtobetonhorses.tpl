
<h2>Horse Betting 101</h2>
  <ul class="navbar-nav">
    <li><a href="/horse-betting/straight-wager">Straight Wager<i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/win">Win<i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/place">Place<i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/show">Show<i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/daily-double">Daily Double<i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/trifecta">Trifecta <i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/superfecta">Superfecta <i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/quinella">Quinella <i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/exacta">Exacta <i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/exotic">Exotic Horse Bet <i class="Fa Fa-Angle-Right"></i></a></li>
  </ul>
