<ul class="navbar-nav">
<li><a href="/kentucky-derby"  >{'Y'|date} Kentucky Derby<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/results"  >Kentucky Derby Results<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/betting"  >Kentucky Derby Betting<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/contenders" >Kentucky Derby Contenders<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/odds"  >Kentucky Derby Odds<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/jockey-betting"  >Kentucky Derby Jockey Betting Odds<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/trainer-betting"  >Kentucky Derby Trainer Betting Odds<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/match-races">Kentucky Derby Match Races<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/props">Kentucky Derby Prop Bets<i class="Fa Fa-Angle-Right"></i></a></li> 
<li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Margin of Victory Bets<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/prep-races" >Derby Prep Races<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/future-wager"  >Derby Future Wager<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/road-to-the-roses"  >Road to the Roses<i class="Fa Fa-Angle-Right"></i></a></li>
{*
<li><a href="/road-to-the-roses-videos"  >Road to the Roses Race Videos<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-derby/video" >Kentucky Derby Video<i class="Fa Fa-Angle-Right"></i></a></li>
*}
<li><a href="/kentucky-derby/winners"  >Kentucky Derby Winners<i class="Fa Fa-Angle-Right"></i></a></li>

<li><a href="/mint-julep-recipe" >Mint Julep Recipe<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/churchill-downs" >Churchill Downs<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/twin-spires"  >Twin Spires<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-oaks" >Kentucky Oaks<i class="Fa Fa-Angle-Right"></i></a></li>
<li><a href="/kentucky-oaks/betting" >Kentucky Oaks Betting<i class="Fa Fa-Angle-Right"></i></a></li>
{* <li><a href="/kentucky-derby/free-bet" >FREE DERBY BET<i class="Fa Fa-Angle-Right"></i></a></li> *}
</ul>
