<ul class="navbar-nav">
  <li><a href="/famous-jockeys" title="Famous Jockeys" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Famous Jockeys <i class="fa fa-angle-down"></i></a>
  <ul class="menu">
    <li><a href="/famous-jockeys/gary-stevens">Gary Stevens</a></li>
    <li><a href="/famous-jockeys/jerry-bailey">Jerry Bailey</a></li>
    <li><a href="/famous-jockeys/pat-day">Pat Day</a></li>
    <li><a href="/famous-jockeys/chris-mccarron">Chris Mccarron</a></li>
    <li><a href="/famous-jockeys/aaron-gryder">Aaron Gryder</a></li>
    <li><a href="/famous-jockeys/jorge-chavez">Jorge Chavez</a></li>
    <li><a href="/famous-jockeys/jon-court">Jon Court</a></li>
    <li><a href="/famous-jockeys/victor-espinoza">Victor Espinoza</a></li>
    <li><a href="/famous-jockeys/eddie-delahoussaye">Eddie Delahoussaye</a></li>
    <li><a href="/famous-jockeys/david-flores">David Flores</a></li>
    <li><a href="/famous-jockeys/shaun-bridgmohan">Shaun Bridgmohan</a></li>
    <li><a href="/famous-jockeys/john-velazquez">John Velazquez</a></li>
    <li><a href="/famous-jockeys/rick-wilson">Rick Wilson</a></li>
    <li><a href="/famous-jockeys/laffit-pincay-jr">Laffit Pincay Jr.</a></li>
    <li><a href="/famous-jockeys/donnie-meche">Donnie Meche</a></li>
    <li><a href="/famous-jockeys/bug-boy">Bug Boy</a></li>
  </Ul>
  </li>
  <li><a href="/famous-trainers">Famous Horse Trainers </a></li>
  <li><a href="/famous-owners">Famous Owners </a></li>
  <li><a href="/famous-breeders">Famous Breeders </a></li>
  <li><a href="/famous-horses"  class="dropdown-toggle"  data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Famous Horses <i class="fa fa-angle-down"></i></a>
  	<ul class="menu">
    <li><a href="/famous-horses/affirmed">Affirmed</a></li>
    <li><a href="/famous-horses/cigar">Cigar</a></li>
    <li><a href="/famous-horses/northern-dancer">Northern Dancer</a></li>
    <li><a href="/famous-horses/seattle-slew">Seattle Slew</a></li>
    <li><a href="/famous-horses/ruffian">Ruffian</a></li>
    <li><a href="/famous-horses/seabiscuit">Seabiscuit</a></li>
    <li><a href="/famous-horses/native-dancer">Native Dancer</a></li>
    <li><a href="/famous-horses/manowar">Man O&#039; War</a></li>
    <li><a href="/famous-horses/secretariat">Secretariat</a></li>
  	</ul>
  </li>
</ul>
