<h2>Stakes Races</h2>
<ul class="navbar-nav">
  <li><a href="/haskell-stakes" title="">Haskell Stakes <i class="fa fa-angle-right"></i></a></li>
    <li><a href="/whitney" title="">Whitney Handicap Stakes <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/travers-stakes" title="">Travers Stakes <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/santa-anita-derby" title="">Santa Anita Derby <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/arkansas-derby" title="">Arkansas Derby <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/illinois-derby" title="">Illinois Derby <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/florida-derby" title="">Florida Derby <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/bluegrass-stakes" title="">Blue Grass Stakes <i class="fa fa-angle-right"></i></a></li>
</ul>
