<h2>Horse Betting 102</h2>
<ul class="navbar-nav">
  <li><a href="/horse-betting/beginner-tips" >Beginner Tips <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/legend">Horse Racing Legend <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/thoroughbred-tips" >Thoroughbred Tips <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/quarter-horse-tips" >Quarter Horse Tips <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/money-management"  >Money Management <i class="Fa Fa-Angle-Right"></i></a></li>
    <li><a href="/horse-betting/kelly-advantage-calculator"  >Kelly Advantage Calculator <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/maiden-race"  >Maiden Race <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/allowance-race"  >Allowance Races <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/starter-allowance" >Starter Allowance <i class="Fa Fa-Angle-Right"></i></a></li>
  <!-- <li class="leaf active"><a href="/horse-betting/claiming-race" title="Claiming Race" >Claiming Race <i class="Fa Fa-Angle-Right"></i></a></li> --> 
  <li><a href="/horse-betting/claiming-race"   >Claiming Race <i class="Fa Fa-Angle-Right"></i></a></li>
  <li><a href="/horse-betting/handicapping-stakes-races"  >Stakes and Handicap <i class="Fa Fa-Angle-Right"></i></a></li>
</ul>
