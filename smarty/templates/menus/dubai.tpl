
<h2>Dubai World Cup Betting</h2>
<ul class="navbar-nav">
  <li><a href="/royal-ascot" title="">ASCOT <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/breeders-cup" title="">BREEDERS&#039; CUP <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/dubai-world-cup" title="" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">DUBAI WORLD CUP <i class="fa fa-angle-down"></i></a>
  	<ul class="menu">
    <li><a href="/dubai-world-cup/winners" title="">DUBAI WORLD WINNERS</a></li>
  	</ul>
  </li>
  <li><a href="/hong-kong-cup" title="">HONG KONG CUP <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/kentucky-oaks" title="">KENTUCKY OAKS <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/melbourne-cup" title="">MELBOURNE CUP <i class="fa fa-angle-right"></i></a></li>
  <li><a href="/prix-de-larc-de-triomphe" title="">PRIX DE L&#039;ARC DE TRIOMPHE <i class="fa fa-angle-right"></i></a></li>
</ul>
