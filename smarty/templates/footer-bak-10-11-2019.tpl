{literal}
<style type="text/css">
</style>
{/literal}
<!-- === Footer ================================================== -->
<br><br>
	<center>Proudly featured on:
{* <img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center> *}
{include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on-new.tpl"}
	</center>
{include file="$disclaimer"}
<div class="footer">
<div class="container">
    <div class="row">
      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Horse Betting</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
       <li><a href="/off-track-betting">Off Track Betting</a></li>
{*        <li><a href="/online-horse-wagering" >Online Horse Wagering</a></li> *}
       <li><a href="/odds">Horse Racing Odds</a></li>
        <li><a href="/horse-racing-schedule">Horse Racing Schedule</a></li>
        <li><a href="/results">Horse Racing Results</a></li>
       <li><a href="/bet-on-horses">Bet on Horses</a></li>
{*   <li><a href="/advance-deposit-wagering">Advance Deposit Wagering</a></li> *}
       <li><a href="/harness-racing">Harness Racing</a></li>
       <li><a href="/hong-kong-racing">Hong Kong Racing</a></li>
              <li><a href="/news">Horse Racing News</a></li>
                            <li><a href="/">Horse Racing </a></li>
{*        <li><a href="/pegasus-world-cup/odds">Pegasus World Cup</a></li> *}
{*        <li><a href="/texas">Texas Horse Betting</a></li> *}
{*        <li><a href="/royal-ascot">Royal Ascot Betting</a></li> *}
{*        <li><a href="/dubai-world-cup/odds" >Dubai World Cup Odds</a></li> *}
       {if $BC_Switch} <!--    --> {/if}       
       {* if $PWC_Switch}
       <!-- Pegasus World -->
         <li><a href="/pegasus-world-cup/betting">Pegasus World Cup Betting</a></li>
         <li><a href="/pegasus-world-cup/odds">Pegasus World Cup Odds</a></li>
         <li><a href="/pegasus-world-cup/winners">Pegasus World Cup Winners</a></li>
         <li><a href="/pegasus-world-cup/results">Pegasus World Cup Results</a></li>
       {/if *}                 
       {if $DWC_Switch_2PM}
{*
           <li><a href="/dubai-world-cup">Dubai World Cup</a></li> 
         <li><a href="/dubai-world-cup/betting" >Dubai World Cup Betting</a></li>
*}
       {/if}
       
       {if $DWC_Switch}                   
{*
         <li><a href="/dubai-world-cup/winners" >Dubai World Cup Winners</a></li>  
         <li><a href="/dubai-world-cup/results" >Dubai World Cup Results</a></li> 
*}
       {/if}
       
       {if $KD_Switch} <!--   --> {/if}
       
       {if $PS_Switch} <!--   --> {/if}
       
       {if $BS_Switch} <!--  --> {/if}
       
       {if $Default} <!--  --> {/if}

{* 		<!--</li>-->
         <li><a href="/breeders-cup/betting" >Breeders' Cup Betting</a>
         <li><a href="/breeders-cup/odds" >Breeders' Cup Odds</a>
*}
        </ul>  <!-- End of list -->
      </div> <!--/col-md-3-->
      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Kentucky Derby</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">        
        <li><a href="/kentucky-derby">Kentucky Derby</a></li>
        <li><a href="/kentucky-derby/betting">Kentucky Derby Betting</a></li>
        <li><a href="/kentucky-derby/odds">Kentucky Derby Odds</a></li>
        <li><a href="/bet-on/kentucky-derby">Bet on the Kentucky Derby </a></li>        
{*         <li><a href="/road-to-the-roses">Road To the Roses</a></li> *}
        <li><a href="/kentucky-derby/contenders">Kentucky Derby Contenders</a></li>
        <li><a href="/kentucky-derby/winners">Kentucky Derby Winners</a></li>
        <li><a href="/kentucky-derby/results">Kentucky Derby Results</a></li>
{*        <li><a href="/kentucky-derby/free-bet">Kentucky Derby Free Bet</a></li>
        <li><a href="/kentucky-derby/jockey-betting">Kentucky Derby Jockey Betting</a></li>
        <li><a href="/kentucky-derby/trainer-betting">Kentucky Derby Trainer Betting</a></li>
        <li><a href="/kentucky-derby/match-races">Kentucky Derby Match Races</a></li>
        <li><a href="/kentucky-derby/margin-of-victory">Kentucky Derby Margin of Victory</a></li>*}
        <li><a href="/kentucky-derby/prep-races">Kentucky Derby Prep Races</a></li>
        <li><a href="/kentucky-derby/racing-schedule">Kentucky Derby Racing Schedule</a></li>
{*         <li><a href="/kentucky-derby/video">Kentucky Derby Video</a></li> *}
{*         <li><a href="/kentucky-oaks">Kentucky Oaks</a></li> *}
        <li><a href="/kentucky-oaks/odds">Kentucky Oaks Odds</a></li>
        <li><a href="/kentucky-oaks/betting">Kentucky Oaks Betting</a></li>
        <li><a href="/twin-spires">Twin Spires</a></li> 
        </ul>
      </div>
        <!--/col-md-3-->
     <div class="col-md-3 margin-bottom-30">
     <div class="headline">
      <h3>Triple Crown</h3>
     </div>
       <ul class="list-unstyled margin-bottom-20">
{*     <li><a href="/kentucky-derby" >Kentucky Derby</a></li> *}
    <li><a href="/preakness-stakes" >Preakness Stakes</a></li>
    <li><a href="/bet-on/preakness-stakes" >Bet on Preakness Stakes </a></li>
    <li><a href="/preakness-stakes/betting" >Preakness Stakes Betting</a></li>
    <li><a href="/preakness-stakes/odds" >Preakness Stakes Odds</a></li>
    <li><a href="/preakness-stakes/results" >Preakness Stakes Results</a></li>
    <li><a href="/preakness-stakes/winners" >Preakness Stakes Winners</a></li>
    <li><a href="/belmont-stakes" >Belmont Stakes</a></li>
    <li><a href="/bet-on/belmont-stakes" >Bet on Belmont Stakes </a></li>
    <li><a href="/belmont-stakes/betting" >Belmont Stakes Betting</a></li>
    <li><a href="/belmont-stakes/odds" >Belmont Stakes Odds</a></li>
    {if $BS_Switch}<li><a href="/belmont-stakes/match-races"  >Belmont Stakes Match Races</a></li> {/if}
    <li><a href="/belmont-stakes/results" >Belmont Stakes Results</a></li>
    <li><a href="/belmont-stakes/winners" >Belmont Stakes Winners</a></li> 
    <li><a href="/bet-on/triple-crown">Triple Crown Betting</a></li>
       </ul>
    </div>
      <!--/col-md-3-->
{*      <!--/col-md-3-->
           <!--<div class="col-md-3 margin-bottom-30">
                <div class="headline">
                  <h3>US Horse Racing</h3>
                </div>
                <ul class="list-unstyled margin-bottom-20">
                  <li><a href="/about" >About Us</a></li>-->

                  <!--  <li><a href="/cash-bonus" >CASH BONUSES</a></li>
               <li><a href="#" >Our Guarantee</a></li>
                    <li><a href="/faqs" >FAQs</a></li>
                    <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->

        <!--<li><a href="/signup/" >Sign Up Today</a></li>
                </ul>
            </div> -->
      <!--/col-md-3-->
*}
      <div class="col-md-3 margin-bottom-30">
        <div class="headline">
          <h3>Follow</h3>
        </div>
        <ul class="social-icons">
          {*<li><a href="https://www.facebook.com/usracingtoday" target="_blank" data-original-title="Facebook" title="Like us on Facebook" class="social_facebook"></a></li>*}
          <li><a href="https://twitter.com/usracingtoday" target="_blank" data-original-title="Twitter" title="Follow us on Twitter" class="social_twitter"></a></li>
{* <li> <a href="https://plus.google.com/+Usracingcom" target="_blank" data-original-title="Google+"  title="Google+" class="social_google" rel="publisher"></a></li> *}
         <li><a href="https://www.instagram.com/usracingtoday/" target="_blank" data-original-title="Instagram"  title="Instagram" class="social_instagram" rel="publisher"></a></li>
         <li><a href="https://www.pinterest.com/usracingtoday/" target="_blank" data-original-title="Pinterest" title="Pinterest" class="social_pinterest" rel="publisher"></a></li>
 {*  <li class="social_pintrest" title="Pin this Page"><a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fwww.usracing.com&media=http%3A%2F%2Fwww.usracing.com%2Fimg%2Fusracing-pintrest.jpg&description=US%20Racing%20-%20America%27s%20Best%20in%20Off%20Track%20Betting%20(OTB).%0ASimply%20the%20easiest%20site%20for%20online%20horse%20racing."  data-pin-do="buttonBookmark" data-pin-config="none" data-original-title="Pintrest" ></a></li>
*}
        </ul>
        <br>   <br>
        <div class="headline">
          <h3>US  Racing</h3>
        </div>
        <ul class="list-unstyled margin-bottom-20">
          <li><a href="/about" >About Us</a></li>
{*
          <!--  <li><a href="/cash-bonus" >CASH BONUSES</a></li>
       <li><a href="#" >Our Guarantee</a></li>
            <li><a href="/faqs" >FAQs</a></li>
            <li><a href="/how-to/bet-on-horses" >How to Bet at US Racing</a></li>-->
*}
<li><a href="/signup?ref={$ref}" rel="nofollow" >Sign Up Today</a></li>
<li><a href="/usracing-reviews">USRacing.com Reviews</a></li>
{* <!--<li><a href="http://www.cafepress.com/betusracing" rel="nofollow" >Shop for US Racing Gear</a></li>--> *}
        </ul>
      </div> <!--/col-md-3-->
</div><!--/row-->
{*
<!--<div class="row friends" style="overflow: visible;">-->
<!--<img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center>-->
<!--<div class="col-md-9">
        <a class="ntra"></a>
        <a target="_blank" class="bloodhorse"></a>
        <a class="equibase"></a>
        <a class="amazon"></a>
        <a class="credit" ></a>
        <a class="ga"></a>
        </div>
       <div class="col-md-3">
        <a href="/" class="us" title="US Online Horse Racing"></a>
--> <!--</div>-->
*}
</div>
{*<center>Proudly featured on: *}
{* <img src="/img/as-seen-on.png" alt="US Racing As Seen On" class="img-responsive" style="width:95%; max-width:1400px;" ><center> *}
{* {include file="/home/ah/allhorse/public_html/usracing/block-as-seen-on-new.tpl"} *}
</div><!--/container-->
</div><!--/footer-->
<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="brand"><br><p>US Racing is not a racebook or ADW, and does not accept or place wagers of any 
        type. This website does not endorse or encourage illegal gambling. All information provided by this website 
        is for entertainment purposes only. This website assumes no responsibility for the actions by and makes no 
        representation or endorsement of any activities offered by any reviewed racebook or ADW. Please confirm the 
        wagering regulations in your jurisdiction as they vary from state to state, province to province and country 
        to country.  Any use of this information in violation of federal, state, provincial or local laws is strictly 
        prohibited.</p>
<p>Copyright {'Y'|date}  <a href="/">US Racing</a>, All Rights Reserved.  </span>  | <a href="/privacy" rel="nofollow">Privacy Policy</a> |  <a href="/terms" rel="nofollow">Terms and Conditions</a> | <a href="/disclaimer" rel="nofollow">Disclaimer</a> | <a href="/responsible-gaming" rel="nofollow" >Responsible Gambling</a> | <a href="/preakness-stakes/betting" >Preakness Stakes Betting</a> | <a href="/belmont-stakes/betting" >Belmont Stakes Betting</a> | <a href="/kentucky-derby/betting" >Kentucky Derby Betting</a>    </div>
    </div><!--/row-->
  </div><!--/container-->
</div><!--/copyright-->
<div class="wp-hide">{include file='scripts-footer.tpl'}</div>
