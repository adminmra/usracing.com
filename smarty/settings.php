<?php	 	
// define smarty lib directory
define('SMARTY_DIR', '../SmartyCore/');

//Web Hosting Directory
define('TEMPLATE_DIR', '../smarty/');
define('LIBS_DIR', '../smarty/libs/');
define('HTDOCS_DIR', '../htdocs/');
// put full path to Smarty.class.php
require(SMARTY_DIR.'SmartyBC.class.php');
//require(LIBS_DIR.'common.inc.php');
// smarty configuration

class Display_Smarty extends SmartyBC { 
    function Display_Smarty() {

            parent::__construct();
            
		  $this->template_dir = TEMPLATE_DIR . 'templates';
                $this->compile_dir = TEMPLATE_DIR . 'templates_c';
                $this->config_dir = TEMPLATE_DIR . 'configs';
                $this->cache_dir = TEMPLATE_DIR . 'cache';

		$this->caching = false;

    }
}
// define other application directory
// Search App Directory
//require(LIBS_DIR . 'search6/search6.lib.php');
//require(LIBS_DIR . 'news/news.lib.php');
// Logout App Directory
//require(LIBS_DIR . 'logout/logout.lib.php');
// Casino App Directory
//require(LIBS_DIR . 'casino/casino.lib.php');
// Mini Casino App Directory
//require(LIBS_DIR . 'minicasino/minicasino.lib.php');


?>
